<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use Mapper;
use DateTime;
use Telegram;
use Validator;
use App\DA\EmployeeModel;
use App\DA\MapModel;

class MapController extends Controller
{
  public function test()
  {
    Mapper::map(53.381128999999990000, -1.470085000000040000);
    return view('map.test');
  }

  public function mapDashboard($tgl)
  {
    // if post
    $sektor = Input::get('area');

    // kehadiran
    $jumlah_teknisi = EmployeeModel::jml_teknisi_hadir_approved("1", $tgl,$sektor);
    $jumlah_teknisi = $jumlah_teknisi[0]->jumlah;

    // sektor
    $get_area = MapModel::getSektor();

    // get last location
    $query = MapModel::getLastLocationTech($sektor);

    // get data starclick
    $starclick_ao_pi = MapModel::getStarclickPI("AO");
    $starclick_ao_fwfm = MapModel::getStarclickFWFM("AO");
    $starclick_ao_actcomp = MapModel::getStarclickActcomp("AO");
    $starclick_ao_ps = MapModel::getStarclickPS("AO",$tgl);

    // view
    return view('map.dashboard',compact('sektor','query','get_area','jumlah_teknisi','starclick_ao_pi','starclick_ao_fwfm','starclick_ao_actcomp','starclick_ao_ps'));
  }

  public function trial()
  {
    return view('map.trial');
  }

  public function monitoring($tgl)
  {
    $get_ro = DB::SELECT('select
    *,
    (SELECT count(*) from Data_Pelanggan_Starclick dps1 left join maintenance_datel md1 on dps1.sto=md1.sto where md1.HERO=a.HERO AND dps1.jenisPsb LIKE "AO%" AND date(dps1.orderDatePs) = "'.$tgl.'") as jumlah_PS,
    (SELECT count(*) from Data_Pelanggan_Starclick dps2 left join maintenance_datel md2 on dps2.sto=md2.sto left join dispatch_teknisi dt2 ON dps2.orderId = dt2.NO_ORDER left join psb_laporan pl1 ON dt2.id = pl1.id_tbl_mj where pl1.status_kendala IS NULL AND md2.HERO=a.HERO AND dps2.jenisPsb LIKE "AO%" AND dps2.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND year(dps2.orderDate)>="2022") as sisa_wo,
    (SELECT count(*) from Data_Pelanggan_Starclick dps2 left join maintenance_datel md2 on dps2.sto=md2.sto left join dispatch_teknisi dt2 ON dps2.orderId = dt2.NO_ORDER left join psb_laporan pl1 ON dt2.id = pl1.id_tbl_mj where pl1.status_kendala IS NOT NULL AND md2.HERO=a.HERO AND dps2.jenisPsb LIKE "AO%" AND dps2.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND year(dps2.orderDate)>="2022") as kendala,
    (SELECT count(*) from Data_Pelanggan_Starclick dps2 left join maintenance_datel md2 on dps2.sto=md2.sto left join dispatch_teknisi dt2 ON dps2.orderId = dt2.NO_ORDER left join psb_laporan pl1 ON dt2.id = pl1.id_tbl_mj where pl1.status_kendala IS NOT NULL AND md2.HERO=a.HERO AND dps2.jenisPsb LIKE "AO%" AND dps2.orderStatus IN ("OSS PONR","WFM ACTIVATION COMPLETE") AND year(dps2.orderDate)>="2022") as ogp
    from maintenance_datel a where a.witel = "KALSEL" group by a.HERO ORDER BY jumlah_PS desc');
    return view('map.monitoring',compact('get_ro'));
  }

  public function map_order_provisioning()
  {
    $sektor         = Input::get('sektor') ?? 'ALL';
    $date           = Input::get('date') ?? date('Y-m-d');

    $get_sektor     = DB::table('group_telegram')->where([ ['ket_posisi', 'PROV'], ['title', '!=', 'Tim Dedicated NTE'] ])->orderBy('urut', 'ASC')->get();

    $data_teknisi   = MapModel::map_data_teknisi($sektor);
    $data_pelanggan = MapModel::map_data_pelanggan($sektor, $date);

    return view('map.map_order_provisioning', compact('sektor', 'date', 'get_sektor', 'data_teknisi', 'data_pelanggan'));
  }


}
?>
