<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\HomeModel;
use App\DA\ReturnDropcoreModel;
class ReturnDropcoreController extends Controller
{

  protected $photoInputs = [
    'returnevidence'
  ];

  public function dashboard($periode){
    $query = ReturnDropcoreModel::list_by_TL($periode);
    return view('dashboard.returnDropcore',compact('periode','query'));
  }

  public function home($periode){
    $auth =   $nik = session('auth');
    $nik = $auth->id_user;
    $TL_NIK = $nik;
    $group_telegram = HomeModel::group_telegram($nik);
    $list_TL = ReturnDropcoreModel::list_TL($periode,$TL_NIK);
    
    return view('returndropcore.home',compact('group_telegram','list_TL'));
  }

  public function input($in){
    $tiket = ReturnDropcoreModel::get_tiket($in);
    $photoInputs = $this->photoInputs;
    if (count($tiket)>0){
      $tiket = $tiket[0];
    }
    return view('returndropcore.input',compact('tiket','in','photoInputs'));
  }

  public function save(Request $request){

    $save = ReturnDropcoreModel::save($request);
    $this->handleFileUpload($request, $request->Input('Incident'));
    if ($save){
      return redirect('/returnDropcore/'.date('Y-m'))->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES mengupdate pengembalian dropcore</strong>']
      ]);
    } else {
      return redirect('/returnDropcore/'.date('Y-m'))->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL mengupdate pengembalian dropcore</strong>']
      ]);
    }
  }

  public function loadMetdata(){
    $img = new \Imagick("https://psb.tomman.info/upload/evidence/252080/Hasil_Ukur_OPM.jpg");
    //echo "test";
    $data = (exif_read_data("https://psb.tomman.info/upload/evidence/252080/Hasil_Ukur_OPM.jpg"));
    echo $data['DateTimeOriginal'];
  }

  private function handleFileUpload($request, $id){
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        //dd($input);
        $path = public_path().'/upload/returnevidence/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }


}
?>
