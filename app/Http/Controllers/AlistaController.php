<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Telegram;
Use App\DA\BAModel;
use DB;
class AlistaController extends Controller
{
  function list_request(){
    $data = BAModel::getAllRequest();
    // dd($data);
    return view('alista.list',compact('data'));
  }
  function save(Request $req){
    $auth = session('auth');
    $query = ["sc_id"=>$req->sc_id,"ba_id"=>$req->ba_id,"alasan"=>$req->alasan, "request"=>$req->request_type,"request_by"=>$auth->id_user,"request_at"=>DB::raw('now()')];
    BAModel::insert($query);
    return redirect('/alista/request');
  }
  function execute(Request $req){
    $auth = session('auth');
    // $query = ["sc_id"=>$req->sc_id,"ba_id"=>$req->ba_id,"alasan"=>$req->alasan, "request"=>$req->request_type,"request_by"=>$auth->id_user,"request_at"=>DB::raw('now()')];
    // BAModel::insert($query);
    return redirect('/alista/request');
  }
  function view(Request $req){
    $u = DB::table('akun')->where('user', '17920262')->first();
    $auth = session('auth');
    $exist = BAModel::getByOrderNo($req->order_no);
    if($exist){
      BAModel::insert(["sc_id"=>$req->order_no,"request"=>1,"request_at"=>DB::raw('now()'),"request_by"=>$auth->id_user]);
    }
    $login = self::cekloginalista($u->cookie_alista);
    if(!str_contains($login, "RISWANDI ADHA")){
      self::login();
    }
    // self::getPemakaianList($u->cookie_alista, $req->order_no);
    $listba = null;
    $data = self::getBA($u->cookie_alista,$req->order_no);
    if(!str_contains($data,"CException")){
      $listba = self::getBAList($u->cookie_alista,$req->order_no);
    }
    // dd($listba);

    $pemakaianlist = self::getPemakaianList($u->cookie_alista,$req->order_no);
    // dd($data,$pemakaianlist);
    return view('alista.view', compact('data', 'pemakaianlist', 'listba'));
  }
  function getPemakaianList($cookie, $order_no){
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Pemakaian%5Bpemakaian_id%5D=&Pemakaian%5Bnik_pemakai%5D=&Pemakaian%5Bregional%5D=&Pemakaian%5Bwitel%5D=&Pemakaian%5Bpemakaian_date%5D=&Pemakaian%5Bwo_number%5D='.$order_no.'&Pemakaian%5Bisactive%5D=&Pemakaian_page=1&ajax=pemakaian-grid&r=pemakaian%2Fupdatematerial');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    if(str_contains($result, "No results found.")){
      return false;
    }
    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'pemakaian',
            'nik',
            'regional',
            'witel',
            'date',
            'wo_number',
            'is_aktif'
        );
    $result = array();
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    }
    return $result;
  }
  function getBAList($cookie, $order_no){
    
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?PemakaianBa%5Bpemakaian_id%5D=&PemakaianBa%5Bnik%5D=&PemakaianBa%5Bno_wo%5D='.$order_no.'&PemakaianBa%5Bnama_regional%5D=&PemakaianBa%5Bnama_witel%5D=&PemakaianBa%5Bnama_fiberzone%5D=&PemakaianBa%5Bcreate_dtm%5D=&PemakaianBa%5Bisactive%5D=&PemakaianBa_page=1&ajax=pemakaian-grid&r=pemakaian%2Fadminba');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    if(str_contains($result, "No results found.")){
      return false;
    }
    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'pemakaian',
            'nik',
            'wo_number',
            'regional',
            'witel',
            'fiberzone',
            'date',
            'is_aktif'
        );
    $result = array();
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $data = array();
        $cells = $rows->item($i)->getElementsByTagName('td');
        $options = $rows->item($i)->getElementsByTagName('a');
        $data['ba_id'] =null;
        for($h=1; $h<$options->length;$h++){
          $ba_id=$options->item($h)->getAttribute("onclick");
          if(str_contains($ba_id, "cancelpemakaian")){
            $ba_id = str_replace('cancelpemakaian("', '', $ba_id);
            $ba_id = str_replace('")', '', $ba_id);
            $data['ba_id'] = $ba_id;

            break;
          }
        }
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
        // dd($data);
    }
    return $result;
  }
  function getBA($cookie,$order_no){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=pemakaian/viewba&id='.$order_no);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
  function getMaterial($cookie,$pemakaian){
    // https://alista.telkomakses.co.id/index.php?r=pemakaian/view&id=88481492
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=pemakaian/view&id='.$pemakaian);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
  function cekloginalista($cookie){
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=pemakaian/viewba&id=524113373');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
  function login(){
    $u = DB::table('akun')->where('user', '17920262')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$u->user."&LoginForm%5Bpassword%5D=".$u->pwd."&yt0=");
    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    if($cookiesOut){
      DB::table('akun')->where('user', '17920262')->update(["cookie_alista"=>$cookiesOut]);
    }
  }
}
