<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Telegram;
use DB;
use Excel;
use DateTime;
use Illuminate\Support\Facades\Input;
use App\DA\DispatchModel;
use App\DA\AssuranceModel;
use App\DA\ApiModel;
use App\DA\PsbModel;

date_default_timezone_set('Asia/Makassar');

class DispatchController extends Controller
{

  public function gaul($periode){
      $query = DB::SELECT('
        SELECT
        *
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'"
        ORDER by ngd.TROUBLE_NUMBER
      ');
      $query1 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 1
        GROUP BY d.JENIS
      ');
      $query2 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 2
        GROUP BY d.JENIS
      ');
      $query3 = DB::SELECT('
        SELECT
        *,
        d.JENIS as action,
        count(*) as jumlah
        FROM
          nonatero_detil_gaul ngd
        LEFT JOIN dispatch_teknisi a ON ngd.TROUBLE_NO = a.Ndem
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        WHERE
          ngd.THNBLN = "'.$periode.'" AND
          ngd.GAUL = 3
        GROUP BY d.JENIS
      ');
      $num = 1;
      $title = '';
      $action = '';
      $action_num = 0;
      foreach ($query as $result){

        if ($title <> $result->NCLI){
          $title = $result->NCLI;
          $num = 1;
        } else {
          $num++;
        }
       DB::table('nonatero_detil_gaul')->where('TROUBLE_NO',$result->TROUBLE_NO)
            ->update(['GAUL'=>$num]);
      }
      return view('dashboard.gaul',compact('query','query1','query2','query3'));
    }

    public function matrixText(){
      $date = Input::get('date');
      $area = Input::get('sektor');
      if ($area==""){ $area = 'ALL'; }
      if ($date==""){ $area = date('Y-m-d'); }

      $get_sektor = DispatchModel::get_sektor();
      $get_area = DispatchModel::group_telegram($area);
      $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
      echo "CREW | TOTAL TIKET | CLOSE | SISA<br />";
      foreach ($getTeamMatrix as $result){
        $sisa = $result->jumlah-$result->UP;
        echo $result->crewid." | ".$result->jumlah." | ".$result->UP." | ".$sisa."<br />";
      }
    }

  public function checksc(){
    $title = "Check SC";
    return view('dispatch.checkscform',compact('title'));
  }

  public function resultchecksc(){
      $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1513823387084&ScNoss=true&SearchText=7143775&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=10&sort=%5B%7B%22property%22%3A%22WITEL%22%2C%22direction%22%3A%22DESC%22%7D%5D";
      $result = json_decode(@file_get_contents($link));
  }

  public function nousage(){
    $query = DB::SELECT('SELECT * FROM nousage1 a LEFT JOIN Data_Pelanggan_Starclick b ON a.NOINET LIKE CONCAT("%",b.ndemSpeedy,"%")');
    foreach ($query as $data){
      echo $data->NOINET."<br />";
    }
  }

  public function index()
  {
    $this->WorkOrder('ALL');
  }

  public function psb_laporan(){
    $get_laporan = DB::SELECT('
      SELECT
        b.Ndem,
        a.kordinat_pelanggan,
        a.kordinat_odp,
        a.nama_odp
      FROM
        psb_laporan a
      LEFT JOIN dispatch_teknisi b ON a.id_tbl_mj = b.id
    ');
    $number = 1;
    $table = '
    <table>
      <tr>
        <td>No</td>
        <td>Ndem</td>
        <td>Kordinat Pelanggan</td>
        <td>Kordinat ODP</td>
        <td>Nama ODP</td>
      </tr>
    ';
    foreach ($get_laporan as $laporan) :
      $result = substr_replace($laporan->kordinat_pelanggan, '', 0, strpos($laporan->kordinat_pelanggan, "q=")+2);

      $table .= '
        <tr>
          <td>'.$number++.'</td>
          <td>'.$laporan->Ndem.'</td>
          <td>'.$laporan->kordinat_pelanggan.'</td>
          <td>'.$laporan->kordinat_odp.'</td>
          <td>'.$laporan->nama_odp.'</td>
        </tr>
      ';
    endforeach;
    $table .= '</table>';
    echo $table;
  }

  public function dashboard(){
    $list = 5;
    $data = DispatchModel::CountWO('READY');
    $READY = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NAL');
    $NAL = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP');
    $MBSP = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP_CLUSTER');

    $MBSP_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('MBSP_NONCLUSTER');
    $MBSP_NONCLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER');
    $REGULER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER_CLUSTER');
    $REGULER_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('REGULER_NONCLUSTER');
    $REGULER_NONCLUSTER = $data[0]->jumlah;

    $data = DispatchModel::CountWO('NONNAL');
    $NONNAL = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NONNAL_CLUSTER');
    $NONNAL_CLUSTER = $data[0]->jumlah;
    $data = DispatchModel::CountWO('NONNAL_NONCLUSTER');
    $NONNAL_NONCLUSTER = $data[0]->jumlah;


    return view('dispatch.dashboard',compact('NONNAL','NONNAL_CLUSTER','NONNAL_NONCLUSTER','READY','NAL','MBSP','MBSP_CLUSTER','MBSP_NONCLUSTER','REGULER','REGULER_CLUSTER','REGULER_NONCLUSTER'));
  }

  public function WorkOrder($id)
  {
    $list = DispatchModel::WorkOrder($id,NULL);
    $statusProv = null;
    return view('dispatch.list', compact('list'));
  }

  public function Transaksi(){
    $list = DispatchModel::Transaksi();
    return view('dispatch.list3', compact('list'));
  }

  public function FollowUp($id)
  {
    $list = DispatchModel::WorkOrder($id,NULL);
    return view('dispatch.list2', compact('list'));
  }

  public function QueryCheck($id){
    $list = DispatchModel::WorkOrder($id,NULL);
    echo $list;
  }
  public function matrix3(){
    $date = Input::get('date');
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    if ($date==""){ $area = date('Y-m-d'); }

    $get_sektor = DispatchModel::get_sektor();
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $data = array();
    $lastRegu = '';

    foreach($getTeamMatrix as $team) :
      $listWO = array();
      $listWOData = DispatchModel::listWO($date,$team->id_regu);

      foreach ($listWOData as $listData) :
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60 && $listData->status_laporan<>69 && $listData->status_laporan<>68 && $listData->status_laporan<>61 ){
            $listWO[] = $listData;
        };
      endforeach;

      $ketNik1='-';
      $ketNik2='-';

      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[$team->mainsector][] = array(
        "mitra" => $team->mitra,
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "ketNik1" =>$ketNik1,
        "ketNik2" =>$ketNik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;

    if ($area=='ALL'){
        $ketSektor = 'ALL';
    }
    else{
        $ketSektor = 'PROVISIONING';
    }

    return view('dispatch.matrix3',compact('get_area','data','get_sektor','date','area','ketSektor'));
  }

  public function matrix($date)
  {
    $layout = "layout";
    $get_sektor = DispatchModel::get_sektor_prov();
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = DispatchModel::get_sektor_matrix_team_prov_old($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = DispatchModel::get_sektor_matrix_team_order_prov($team->id_regu,$date);
      }
    }

    return view('dispatch.matrix_v2',compact('date','get_sektor','layout','matrix'));
  }

  public function matrixKhusus($date)
  {
    $layout = "layout";
    $get_sektor = DispatchModel::get_sektor_prov();
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = DispatchModel::get_sektor_matrix_team_prov_khusus($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = DispatchModel::get_sektor_matrix_team_order_khusus($team->id_regu,$date);
      }
    }

    return view('dispatch.matrix_khusus',compact('date','get_sektor','layout','matrix'));
  }

  public function matrixGo($date)
  {
    $layout = "layout";
    $get_sektor = DispatchModel::get_sektor_prov();
    $matrix = array();
    foreach ($get_sektor as $sektor)
    {
      $matrix[$sektor->chat_id] = DispatchModel::get_sektor_matrix_team_prov($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team)
      {
        $matrix[$sektor->chat_id][$team->id_regu] = DispatchModel::get_sektor_matrix_team_order_prov_go($team->id_regu,$date);
      }
    }

    return view('dispatch.matrix_v2',compact('date','get_sektor','layout','matrix'));
  }

  public function matrixQC($date){
    $layout = "layout";
    $get_sektor = DispatchModel::get_sektor_prov();
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = AssuranceModel::get_sektor_matrix_team_prov_qc($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = AssuranceModel::get_sektor_matrix_team_order_prov_qc($team->id_regu,$date);
      }
    }

    return view('dispatch.matrix_v2',compact('date','get_sektor','layout','matrix'));
  }

  public function matrixGoPublic(){
    $date = Input::get('date');
    $area = Input::get('area');

    $get_sektor = DB::SELECT('
      SELECT * FROM group_telegram a WHERE a.sektorx = "'.$area.'" AND a.sms_active_prov = 1 ORDER BY urut ASC
    ');
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = DispatchModel::get_sektor_matrix_team_prov($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = DispatchModel::get_sektor_matrix_team_order_prov_go($team->id_regu,$date);
      }
    }

    return view('dispatch.matrixGoPublic',compact('date','area','get_sektor','matrix'));
  }

  public function simpunipetarika(){
    $query = DB::SELECT('SELECT * FROM psb_laporan WHERE DATE(created_at) >= "2020-03-01"');
    foreach ($query as $result){
      echo $result->id.'|'.str_replace(' ','',$result->kordinat_pelanggan)."<br />";
      $update = DB::table('psb_laporan')->where('id',$result->id)->update([
        'kordinat_pelanggan' => str_replace(' ','',$result->kordinat_pelanggan)
      ]);
    }
  }

  public function approveQC($id,$sc,$id_regu){

    $otoritas = session('auth')->level;
    if ($otoritas==2 || $otoritas==15){
      DB::table('psb_laporan_log')->insert([
             'created_at'      => DB::raw('NOW()'),
             'created_by'      => session('auth')->id_karyawan,
             'status_laporan'  => '1',
             'id_regu'         => $id_regu,
             'catatan'         => 'Approved Quality Control Tomman by '.session('auth')->id_karyawan,
             'Ndem'            => $sc,
             'psb_laporan_id'  => $id
          ]);
        DB::table('psb_laporan')
          ->where('id', $id)
          ->update([
              'status_laporan' => '1',
              'status_qc' => '1',
              'status_qc_by' => session('auth')->id_karyawan,
              'status_qc_date' => date('Y-m-d H:i:s'),
              'status_qc_reject' => '0',

          ]);
          return back()->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> melakukan approval']
          ]);
    } else {
      echo "ANDA TIDAK DIIZINKAN UNTUK MELAKUKAN APPROVAL";
    }
  }


  public function rejectQC($id,$sc,$id_regu){

    $otoritas = session('auth')->level;
    // $cek = DB::table('qc_reject_log')->where('id_dt',$id)->first();
    $cek = DB::select('
      SELECT *
      FROM qc_reject_log
      WHERE id_dt = "'.$id.'"

    ');
    if(count($cek)){
    $cek = $cek[0];
    } else {
    if ($otoritas==2 || $otoritas==15){
      DB::table('qc_reject_log')->insert([
        'id_dt' => $id,
        'sc'       => $sc,
        'rejected_by' => session('auth')->id_karyawan,
        'id_regu'  => $id_regu,
        'log'        => 'Order rejected By '.session('auth')->id_karyawan,
        'updated_at' => DB::raw('NOW()')
     ]);


      DB::table('psb_laporan')
           ->where('id_tbl_mj', $id)
           ->update([
               'status_qc_reject' => '1',
               'status_qc_reject_by' => session('auth')->id_karyawan,
               'status_qc_reject_date' => date('Y-m-d H:i:s')
           ]);

        return redirect('/dispatch/'.$sc)->with('alerts', [
          ['type' => 'success', 'text' => '<strong>PROGRESS</strong> Reject QC !']
    ]);
    } else {
      echo "ANDA TIDAK DIIZINKAN UNTUK MELAKUKAN REJECT WO";
    }
  }
  return redirect('/dispatch/'.$sc)->with('alerts', [
    ['type' => 'success', 'text' => '<strong>PROGRESS</strong> Reject QC !']
]);
}


  public function viewrejectQC(){

    $regu = session('auth')->id_regu;
    $regux = DB::select('
      SELECT qr.*,
      dte.id as id_dte,
      dte.Ndem,
      pls.laporan_status
      FROM qc_reject_log qr
      INNER JOIN dispatch_teknisi dte ON qr.id_dt = dte.id
      LEFT JOIN psb_laporan pl ON dte.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE qr.id_regu = ? AND pl.status_qc_reject = 1
      ORDER BY qr.updated_at DESC
    ',[
      $regu
    ]);
     return view('psb.viewrejectqc',compact('regux'));

  }


  public function matrix_old(){
    $date = Input::get('date');
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    if ($date==""){ $area = date('Y-m-d'); }

    $get_sektor = DispatchModel::get_sektor();
    #$get_area = DispatchModel::group_telegram($area);
    #$getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $data = array();
    $lastRegu = '';

    foreach($getTeamMatrix as $team) :
      $listWO = array();
      $listWOData = DispatchModel::listWO($date,$team->id_regu);

      foreach ($listWOData as $listData) :
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60 && $listData->status_laporan<>69 && $listData->status_laporan<>68 && $listData->status_laporan<>61 ){
            $listWO[] = $listData;
        };
      endforeach;

      $ketNik1='-';
      $ketNik2='-';

      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[$team->mainsector][] = array(
        "mitra" => $team->mitra,
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "ketNik1" =>$ketNik1,
        "ketNik2" =>$ketNik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;

    if ($area=='ALL'){
        $ketSektor = 'ALL';
    }
    else{
        $ketSektor = 'PROVISIONING';
    }

    return view('dispatch.matrix2',compact('get_area','data','get_sektor','date','area','ketSektor'));
  }

  public function matrixPublic(){
    $date = Input::get('date');
    $area = Input::get('area');

    $jam_now = date("H");
    $get_sektor = DB::SELECT('
      SELECT * FROM group_telegram a WHERE a.ioan = "'.$area.'" AND a.ioan_check = 1 AND a.sms_active = 1 ORDER BY urut ASC
    ');
    $matrix = array();
    foreach ($get_sektor as $sektor){
      $matrix[$sektor->chat_id] = DispatchModel::get_sektor_matrix_team($sektor->chat_id,$date);
      foreach ($matrix[$sektor->chat_id] as $team){
        $matrix[$sektor->chat_id][$team->id_regu] = DispatchModel::get_sektor_matrix_team_order($team->id_regu,$date);
      }
    }

    return view('dispatch.matrixPublic',compact('date','jam_now','area','mainsector','get_sektor','matrix'));
  }

  public function matrixHD($date, DispatchModel $dispatchModel){

    $getHdMatrix = DispatchModel::getHdMatrix($date);
    $hdFallout = DispatchModel::getHdFalloutMatrix($date);

    $data = array();
    $lastHD = '';
    $count = 0;
    foreach ($getHdMatrix as $hd){
      if($lastHD == $hd->updated_by){
        $data[count($data)-1]['jumlahWO'] += 1;
        $data[count($data)-1]['WO'][] = array("sc"=>$hd->Ndem, "status_laporan"=>$hd->status_laporan);
        //status
        if ($hd->status_laporan==6 || $hd->status_laporan==NULL){
          $data[count($data)-1]['NO_UPDATE'] += 1;
        } else if ($hd->status_laporan<>5 && $hd->status_laporan<>6 && $hd->status_laporan<>1 && $hd->status_laporan<>4) {
          $data[count($data)-1]['KENDALA'] += 1;
        } else if ( $hd->status_laporan==5){
          $data[count($data)-1]['PROGRESS'] += 1;
        } else if ($hd->status_laporan==1){
          $data[count($data)-1]['UP'] += 1;
        }
      }else{
        $data[] = array('nama' => $hd->nama,'jumlahWO'=>1,'PROGRESS'=>0,'KENDALA'=>0,'NO_UPDATE'=>0,'UP'=>0, 'WO' => array(array("sc"=>$hd->Ndem, "status_laporan"=>$hd->status_laporan)));
        if ($hd->status_laporan==6 || $hd->status_laporan==NULL){
          $data[count($data)-1]['NO_UPDATE'] = 1;
        } else if ($hd->status_laporan<>5 && $hd->status_laporan<>6 && $hd->status_laporan<>1 && $hd->status_laporan<>4) {
          $data[count($data)-1]['KENDALA'] = 1;
        } else if ( $hd->status_laporan==5){
          $data[count($data)-1]['PROGRESS'] = 1;
        } else if ($hd->status_laporan==1){
          $data[count($data)-1]['UP'] = 1;
        }

      }
      $lastHD = $hd->updated_by;
    }

    $fout = array();
    $lastHD = '';

    foreach ($hdFallout as $hd){
      if($lastHD == $hd->workerId){
          $fout[count($fout)-1]['jumlahWO'] += 1;
          $fout[count($fout)-1]['keterangan'] = $hd->ket;
          $fout[count($fout)-1]['WO'][] = array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket);
          //status

          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0" ){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }
           else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }


      }else{
          $fout[] = array('nama' => $hd->workerName,'jumlahWO'=>1,'PROGRESS'=>0,'UP'=>0, 'UP_PDA'=>0,'keterangan'=>$hd->ket,'WO' => array(array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket)));
          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0"){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }
      }
      $lastHD = $hd->workerId;
    }

    $periode = date('Y-m',strtotime($date));
    $manjas  = $dispatchModel->jumlahManjaByUser($periode);

    return view('dispatch.matrixHd',compact('data', 'fout', 'manjas'));

  }
  public function produktifitastek2($date,$area){
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }

    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::produktifitastek2($date,$area);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    echo "1";
    foreach ($getTeamMatrix as $data){
      echo $data->nik." - ".$data->nama." (".$data->jumlah_regu.")<br />";
    }
  //  return view('dispatch.produktifitas2',compact('get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function produktifitastek()
  {
    $dateAwal = Input::get('dateAwal');
    $date = Input::get('date');
    $area = Input::get('area');
    $sektor = Input::get('sektor');
    $provider = Input::get('provider');

    $get_provider = [
      (object)['id' => 'DCS', 'text' => 'DCS'],
      (object)['id' => 'NON_DCS', 'text' => 'NON_DCS'],
      (object)['id' => 'ALL', 'text' => 'ALL'],
    ];

    $get_closing = DispatchModel::closingAverage($sektor, $provider);
    $get_listsektor = DB::SELECT('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_sektor = 1 AND sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND sms_active_prov = 1 ORDER BY urut ASC');
    $getTeamMatrix = DispatchModel::produktifitastek3($dateAwal,$date,$area,$sektor);
    $layout = 'layout';
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','dateAwal','date','konstanta','area','getTeamMatrix','get_listsektor', 'get_provider', 'provider', 'get_closing'));
  }

  public function produktifitassektor2($date,$area,$mitra){


    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek4($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $layout = 'layout';
    return view('dispatch.produktifitassektor',compact('layout','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function tempQ(){
    $query = DB::SELECT('SELECT a.internet,b.manja,b.alamatSales FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      WHERE a.orderStatus = "Completed (PS)" AND a.internet IN ("161214200316",
"162226200994",
"161201207185",
"162219202021",
"162206903716",
"161201202235",
"162204301845",
"161203207723",
"162204212542",
"161201218696",
"162222302864",
"162204213968",
"161201209043",
"161231202688",
"161231200507",
"162229203455",
"161201212067",
"161231201939",
"161214200725",
"162206903844",
"161203204698",
"162229303745",
"162222302791",
"162206901048",
"161201212682",
"162211300064",
"162204212801",
"162222303604",
"161203213664",
"161203212113",
"162204214425",
"162206900639",
"161201205102",
"161231200716",
"162204900703",
"162204214387",
"162228900581",
"161203207932",
"162204212001",
"161201214972",
"162204302178",
"162228900928",
"161203205732",
"161231202878",
"161201214121",
"161201214034",
"161214200108",
"161628500362",
"161203213015",
"161203209112",
"162204212046",
"162204213010",
"161201218137",
"162214302304",
"161201204954",
"161628500540",
"162204209646",
"161203207901",
"161212200147",
"162204214421",
"162204218203",
"162214302232",
"162222200828",
"162222303395",
"162204212583",
"161201215131",
"162203202692",
"162223202483",
"161203213550",
"162206900882",
"161201210573",
"161203209109",
"162206903655",
"162204216224",
"161201205934",
"161201207904",
"162223200786",
"161231200365",
"161221202140",
"161212202569",
"161628501815",
"162204217331",
"161203209267",
"161201215583",
"162204217769",
"162228800012",
"161201208789",
"162218202368",
"161201207112",
"161201304166",
"162204206864",
"162206900348",
"162206301690",
"162204212393",
"162204217607",
"162211300367",
"162204217054",
"161201213960",
"161201212612",
"162204212619",
"162222303590",
"161203213863",
"162222201661",
"162204217317",
"161201209524",
"161201208402",
"161203213315",
"162222303402",
"162229303508",
"162204211291",
"162222303962",
"162204220167",
"162204217703",
"162229202837",
"162224200971",
"161203213391",
"162203800327",
"162204217058",
"162206900458",
"162201300701",
"162228900583",
"162206903676",
"162229303783",
"162204214188",
"161231202551",
"161201211346",
"161201208443",
"161203204783",
"162218203453",
"161201214944",
"162204217948",
"162229203433",
"162219203688",
"162204200582",
"161203204866",
"161221200943",
"161201215117",
"162228900174",
"162204213183",
"162228800664",
"161203208013",
"161201204839",
"161221200793",
"162222302641",
"161202207730",
"161212202053",
"162206904145",
"162229202608",
"161201218250",
"162223202659",
"162228900139",
"161201212173",
"161212201001",
"161214200019",
"161201213494",
"162218202848",
"162202800433",
"161201208220",
"161201203237",
"162203902108",
"162218200794",
"162204216952",
"161203212363",
"162204206229",
"161209200041",
"162218202585",
"161201213750",
"162205203530",
"161203213518",
"162204213630",
"162219203626",
"161201210636",
"161201212639",
"162204213107",
"162204212884",
"162222201974",
"162204800513",
"162204217259",
"162204212293",
"162204208489",
"162229203020",
"161201211037",
"162204211432",
"161209200038",
"161201217867",
"161201213235",
"162222203038",
"162204212382",
"162204209521",
"162204220226",
"162204211513",
"162219203273",
"161628501289",
"162222302243",
"162214302484",
"161203203533",
"162204212125",
"162222202775",
"161201214800",
"161628500683",
"162229203173",
"161201212156",
"162204209960",
"161203208030",
"161201217915",
"162229202859",
"162204213870",
"161201219231",
"162211300038",
"162204209481",
"162203200099",
"161201210411",
"161628501214",
"161203208937",
"162222303472",
"161201217218",
"161201219372",
"162204301201",
"162204208454",
"162203300836",
"162222203018",
"161201211541",
"162204213029",
"161203208274",
"161203208139",
"162204212849",
"162204302910",
"162204208977",
"161201217153",
"162226200692",
"161202201229",
"162224200835",
"162229201646",
"161203206971",
"161628501694",
"162204211427",
"161203208250",
"161203207333",
"162229201732",
"162219201538",
"162219203318",
"161201211188",
"161201206276",
"161203204244",
"161201218626",
"162219201235",
"161221200950",
"162222201908",
"161201217023",
"162204218150",
"162231200190",
"162204214450",
"161203213660",
"162229203411",
"162229202542",
"161201214634",
"162222202037",
"162204217755",
"161201214186",
"162204216432",
"162222201024",
"162226200803",
"162206903602",
"162204212219",
"162229203571",
"161628500162",
"162229301003",
"162204214771",
"161201214124",
"162229301391",
"162204211460",
"162222302741",
"162204206180",
"162204212512",
"162229201985",
"161203213204",
"161201218893",
"161203205750",
"162219203615",
"162206904981",
"161203204904",
"161201201554",
"162229201600",
"162204220284",
"161221201118",
"161203207880",
"162204214344",
"162229302167",
"162222202091",
"162204220458",
"162204211960",
"162229202236",
"161201211953",
"162204211207",
"161201219205",
"161201209171",
"162204217707",
"162204217647",
"161203208755",
"162204213665",
"162229201483",
"161203213634",
"161203207876",
"161203209023",
"162229300617",
"162204212322",
"162226200819",
"162228900404",
"161201217444",
"162229203379",
"161201215443",
"161203208676",
"161201207001",
"162206903649",
"161203209353",
"161201215176",
"162222201017",
"162223900169",
"161201217807",
"161203212191",
"162229301892",
"161201205066",
"162204214578",
"162214302272",
"162204213989",
"161203204713",
"161201217874",
"162222303795",
"162219203496",
"162206903670",
"162229302163",
"162204800710",
"161231202485",
"162204216424",
"162204211174",
"161203209943",
"162204213437",
"162204217687",
"161201217415",
"161201217328",
"162222201288",
"161203205903",
"162203900353",
"162214300301",
"162204212241",
"161201200837",
"162204208189",
"162204204869",
"161201218906",
"161203212262",
"162204211857",
"162206900406",
"161203208323",
"162229202788",
"162231900165",
"161212202245",
"162204211970",
"162222202790",
"161202204964",
"162206904374",
"161628500340",
"161201203546",
"161201208413",
"161201206534",
"162218203326",
"161221200966",
"161201208405",
"161201208753",
"162204209339",
"161203208458",
"161212202208",
"162222201006",
"161203204303",
"162205203010",
"161202203854",
"162228800512",
"162206900397",
"162204212066",
"162223300380",
"161201215516",
"161201212490",
"162204217746",
"161201210192",
"162204212523",
"161203213093",
"162222201978",
"161203209064",
"161201206024",
"161201213229",
"161201213111",
"162204216782",
"161201218370",
"161203213319",
"161203209840",
"162204213799",
"161231202088",
"161203212719",
"162222303150",
"162204217543",
"161628500681",
"162229201931",
"162204220151",
"161212202465",
"162222201239",
"161201212424",
"161203209970",
"162206904300",
"162218202413",
"162204213701",
"162222303459",
"162204216042",
"161203205100",
"162214302336",
"162204217830",
"162204217750",
"162222202075",
"161221200972",
"162229203710",
"162222303995",
"162222303078",
"161202204422",
"161201214778",
"161201203362",
"161201211797",
"161203213301",
"162229201613",
"161201211215",
"162222302949",
"161201214081",
"162206903997",
"161628501270",
"161203207729",
"161201213029",
"162222301218",
"162206901045",
"162222900199",
"162222202089",
"162204217265",
"161201203512",
"162204212092",
"162204211205",
"161628501756",
"161203209256",
"162229202514",
"161201211301",
"162204212975",
"161201209530",
"161201209928",
"162204208919",
"161201211747",
"161203204520",
"161203207663",
"162218203576",
"162222302631",
"161203209663",
"161201217774",
"162222200641",
"162218202489",
"162214302318",
"161201218548",
"161203208503",
"161203204752",
"161231201043",
"161203205266",
"162204204499",
"162229202461",
"162223202680",
"162222301621",
"161201203780",
"162203902190",
"162223900665",
"161201212167",
"162204213209",
"161201303966",
"162229303788",
"162214302518",
"161203205489",
"162204216257",
"162228900828",
"162204206192",
"161212200821",
"162229203343",
"161202204092",
"161201205982",
"161203207765",
"162226201180",
"162204204455",
"162205203528",
"162222300232",
"162229301433",
"162226201009",
"162226200877",
"161201207274",
"162204209299",
"162204206037",
"162204213348",
"162204206550",
"162218202061",
"162204212061",
"161212202179",
"162204217694",
"162203800529",
"162222302878",
"162222202201",
"162204212186",
"162204900273",
"161628500135",
"162222302279",
"161201211076",
"162204212867",
"162219203788",
"162219203140",
"162214302665",
"162206904131",
"162206903506",
"162218202981",
"161221201042",
"161203213964",
"162203903006",
"161221200313",
"162204212725",
"161201218431",
"161201209990",
"161201210709",
"162204217751",
"162204301603",
"161221200174",
"161201208759",
"162229201576",
"162201203636",
"162204213035",
"162204213077",
"162204212254",
"161201211044",
"162204212164",
"161203208242",
"161201303327",
"161203213245",
"161203204766",
"162214302389",
"162204211418",
"162222202247",
"161221200586",
"161203203547",
"162222201313",
"161201214341",
"162206301882",
"162204208115",
"162226200703",
"162219203716",
"162229203312",
"162222202514",
"162219203660",
"161203209205",
"161203206832",
"161201212235",
"161628501015",
"162223300124",
"162214302451",
"162204211882",
"162204211673",
"162204206916",
"162204217658",
"162222201964",
"162222303235",
"161201208638",
"162222303137",
"162206903832",
"162229301290",
"162214302201",
"161203207900",
"161201205757",
"161212200315",
"162219203532",
"161212200500",
"162214302683",
"162204212115",
"162219200401",
"162222201945",
"162204206071",
"162204213407",
"162204214650",
"162228900830",
"162206301461",
"162204212256",
"162222203023",
"161201201688",
"161201217093",
"162205203529",
"162222201080",
"161203209174",
"161628501011",
"162229202866",
"161203206483",
"162222302644",
"162204213574",
"161201214017",
"162218203603",
"162204212618",
"161203209266",
"162204302224",
"161201219365",
"162218201413",
"161201213056",
"162201301500",
"162218202280",
"162222202097",
"161628501740",
"161201204483",
"162228900561",
"162225200084",
"161212202995",
"161212202884",
"162228900053",
"161201206268",
"161201217928",
"161203207131",
"161201213103",
"161203207260",
"162228800562",
"162222303363",
"162203902176",
"161201206919",
"162214302255",
"161203204940",
"162219203541",
"162203902506",
"161201218333",
"162222201720",
"162204216401",
"161201204638",
"162222300828",
"162224200753",
"162203901713",
"162204217973",
"162204211922",
"162218203651",
"162214302083",
"161203213917",
"162222201783",
"162204204283",
"162204211073",
"162204209707",
"162222301601",
"161221200734",
"161201218770",
"161201207500",
"162204211682",
"161628501088",
"162204800172",
"162204216635",
"161201215691",
"161212202996",
"162222202242",
"161201212128",
"162214302322",
"162204216101",
"162222201956",
"161201210427",
"162204800328",
"162222203041",
"162222202772",
"162204213246",
"161201215603",
"162204212213",
"162204214031",
"161203204213",
"161201217405",
"161201209310",
"162204214416",
"162204213111",
"162218202782",
"161201304763",
"161203212497",
"161201208101",
"162202902430",
"162204217728",
"162204211501",
"161628501662",
"162214302513",
"162224200414",
"162228800498",
"161203204815",
"162204213552",
"161201214230",
"161203207949",
"162219203702",
"161203208862",
"162203200419",
"162204206172",
"161203209588",
"162204800839",
"162228800612",
"162222303573",
"162229301737",
"161201211341",
"161203208810",
"162204211015",
"161202208314",
"162203902189",
"162202202368",
"162206301346",
"162222202077",
"162206300206",
"161628500439",
"161201208424",
"162203800273",
"162229303260",
"162204213969",
"162229203280",
"161201210685",
"161201303156",
"161201217097",
"161201212759",
"162222300444",
"162204212818",
"162206904043",
"162204217700",
"161628501267",
"161201303821",
"162229301787",
"162228900632",
"162218205011",
"162222301430",
"162204213105",
"161203205778",
"162222302675",
"162226200631",
"162226200196",
"161201304790",
"161201209128",
"161203208983",
"161214200786",
"161201212960",
"162229203614",
"161212202939",
"162218203216",
"162206904722",
"162204213678",
"162226200870",
"162222201287",
"162206903136",
"162229202975",
"162204212843",
"161201211228",
"162222303004",
"162214302079",
"162204301111",
"161203213361",
"162204214775",
"161203212243",
"161214200790",
"161203205779",
"162229303510",
"162224200752",
"161203213449",
"162222202895",
"162204212533",
"162203901955",
"162229303567",
"161201204191",
"161221202111",
"161212202295",
"161201215112",
"162204216174",
"162204212614",
"162229303363",
"162222303187",
"162204213058",
"161201203142",
"162204209879",
"162231900064",
"161203209190",
"161203208512",
"161203212995",
"161201215265",
"161203212636",
"162204216106",
"161201209856",
"161201202308",
"162204217218",
"162218203411",
"161212202674",
"162206903277",
"162222302074",
"162204209296",
"162222202167",
"161201213232",
"161201217992",
"161203208243",
"162204212332",
"162218202832",
"162222202652",
"162228900933",
"162220200464",
"162204214657",
"162204212597",
"162228900895",
"161201212155",
"161203206662",
"161201212111",
"162206904453",
"161201210154",
"162214302217",
"162204211769",
"161202208349",
"162204213951",
"162218203169",
"162203902220",
"162204209607",
"162204209216",
"162228800526",
"161203208068",
"161201210035",
"161203207455",
"162203901897",
"162202800524",
"161201303301",
"161231202453",
"161203212179",
"162204213402",
"162204211481",
"162222303755",
"161201212115",
"161201211222",
"162204211349",
"162222201653",
"162204220140",
"161201203865",
"162218203522",
"162219203515",
"162204212559",
"162204204026",
"162219201335",
"161201205784",
"161201206770",
"162229303243",
"161203207843",
"161202204268",
"162204216167",
"161201209729",
"161203212304",
"161203204299",
"161201214660",
"162204209673",
"161201218817",
"162222202042",
"161201300364",
"162218202828",
"162206904769",
"162218203289",
"162226201105",
"162204204565",
"162229203349",
"161231202511",
"161201207624",
"162228900761",
"162222202027",
"162218203750",
"161628501242",
"161201214356",
"162204206067",
"162204900547",
"162224200961",
"162204217512",
"161203205817",
"162204212947",
"161201217066",
"162222301273",
"161201215507",
"162228900143",
"161201214775",
"161203203432",
"161212202073",
"162204217129",
"161201212974",
"162214300034",
"162222201885",
"162222303821",
"161203207501",
"162206903062",
"161203212369",
"161203204375",
"162204217420",
"161628501050",
"161201214346",
"162204211095",
"161201206405",
"162211200041",
"162204209221",
"161201303372",
"162214302271",
"161201212069",
"162204202327",
"161214200995",
"162222302091",
"161203207607",
"161203212391",
"162220200413",
"161214200524",
"162211300422",
"162224200362",
"161201215165",
"162204211023",
"161201211851",
"161203212999",
"162204212551",
"161201214218",
"161201217916",
"161203209658",
"161201215546",
"161201219169",
"161231202889",
"162228900756",
"162204302824",
"161201300001",
"161628500997",
"162219203491",
"161231202729",
"162206900652",
"161201212916",
"161203214004",
"161203209578",
"161209200003",
"162204220452",
"161201215225",
"162204216926",
"162204216916",
"161201205783",
"161201206408",
"162222202305",
"162204212172",
"161201204533",
"161201213631",
"162205203512",
"162204217685",
"161201213233",
"162222202191",
"161201219064",
"161203213057",
"162204212911",
"161212202779",
"162229202288",
"161212202268",
"162206900922",
"162222301415",
"161203212900",
"162219203850",
"161221202141",
"161201217492",
"161201302044",
"162204209168",
"161201202040",
"161201208562",
"162222303346",
"162204206743",
"162204212755",
"162204208317",
"161201207019",
"161203213086",
"162204801424",
"162228900633",
"162204214694")');
  echo '<table><tr><td>Internet</td><td>Alamat 1</td><td>Alamat 2</td></tr>';
    foreach ($query as $result){
      echo "<tr><td>".$result->internet."</td><td>".$result->manja."</td><td>".$result->alamatSales."</td>";
    }
  }

public function produktifitasteksewa($date,$area,$mitra){


    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek3($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $dataLastUpdate = array();
    foreach ($getTeamMatrix as $team) {
    $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
    if (count($getLastUpdate)>0){
      $dataLastUpdate[$team->id_regu] = $getLastUpdate;
    } else {
      $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
    }
    }
    $layout = 'layout';
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }


  public function produktifitastekA($date,$area,$mitra){


      // $get_area = DispatchModel::group_telegram($area);
      $get_area = DispatchModel::getGroupTelegramProv();
      $getTeamMatrix = DispatchModel::produktifitastekA($date,$area,$mitra);
      $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
      $data = array();
      $lastRegu = '';
      $dataLastUpdate = array();
      foreach ($getTeamMatrix as $team) {
      $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
      if (count($getLastUpdate)>0){
        $dataLastUpdate[$team->id_regu] = $getLastUpdate;
      } else {
        $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
      }
      }
      $layout = 'layout';
      return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
    }

    public function produktifitassektor($date,$mode){

      $getTeamMatrix = DispatchModel::produktifitassektor($date,$mode);
      $layout = 'layout';
      return view('dispatch.produktifitassektor',compact('getTeamMatrix','date','layout'));
    }
    public function produktifitassektorlist($date,$sektor,$mode){

      $get_produktifitasteklist = DispatchModel::produktifitassektorlist($date,$sektor,$mode);
      $title = "LIST ORDER ".$sektor." ".$date." ".$mode;
      $layout = 'layout';
      return view('dispatch.produktifitasteklist2',compact('get_produktifitasteklist','sektor','mode','date','layout','title'));
    }


    public function produktifitastek_public($date,$area,$mitra){


    // $get_area = DispatchModel::group_telegram($area);
    $get_area = DispatchModel::getGroupTelegramProv();
    $getTeamMatrix = DispatchModel::produktifitastek3($date,$area,$mitra);
    $get_hari_lembur = DB::table('hari_lembur')->where('lembur', $date)->first();
    $data = array();
    $lastRegu = '';
    $dataLastUpdate = array();
    foreach ($getTeamMatrix as $team) {
    $getLastUpdate = DispatchModel::lastupdate($team->id_regu);
    if (count($getLastUpdate)>0){
      $dataLastUpdate[$team->id_regu] = $getLastUpdate;
    } else {
      $dataLastUpdate[$team->id_regu] = "Starting Progress at 09.00 AM";
    }
    }
    $layout = "public_layout_2";
    return view('dispatch.produktifitastek2',compact('layout','dataLastUpdate','get_hari_lembur','date','konstanta','area','get_area','getTeamMatrix','area'));
  }

  public function produktifitasteklist($dateAwal,$date,$id_regu,$status){
    $title = "LIST ORDER $status BY DATE $dateAwal s/d $date";
    $get_produktifitasteklist = DispatchModel::produktifitasteklist($dateAwal,$date,$id_regu,$status);
    return view('dispatch.produktifitasteklist',compact('get_produktifitasteklist','title','dateAwal','date','id_regu','status'));
  }

  public function produktifitas($date,$area){
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }

    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrix1($date,$area);
    $data = array();
    $lastRegu = '';

    return view('dispatch.produktifitas',compact('get_area','getTeamMatrix','area','konstanta'));
  }

  public function timeslot($date,$area){

    $timeslot = DB::SELECT('
      SELECT * FROM dispatch_teknisi_timeslot
    ');
    $dispatch_timeslot = DB::SELECT('
      SELECT
        a.*,
        b.*,
        e.*,
        c.status_laporan,
        c.catatan,
        c.modified_at,
        c.created_at,
        d.uraian,
        d.id_regu,
        e.orderName,
        g.laporan_status,
        c.id_tbl_mj,
        f.title as area
      FROM
        dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN regu d ON a.id_regu = d.id_regu
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN roc ee ON a.Ndem = ee.no_tiket
      LEFT JOIN group_telegram f ON d.mainsector = f.chat_id
      LEFT JOIN psb_laporan_status g ON c.status_laporan = g.laporan_status_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      ORDER BY d.id_regu,f.title
    ');
    $data       = array();
    $lastRegu   = '';
    $lastArea   = '';
    $listTim    = array();

    foreach ($dispatch_timeslot as $data_timeslot) :
        $timeCount = 0;
        $timetoCount = '';
        if ($data_timeslot->status_laporan==5){
          if (empty($data_timeslot->modified_at))
          $timetoCount = $data_timeslot->created_at;
          else
          $timetoCount = $data_timeslot->modified_at;
          $timeCount = $this->timeCount($timetoCount);
        }
        if (empty($data_timeslot->modified_at))
        $timetoCount = $data_timeslot->created_at;
        else
        $timetoCount = $data_timeslot->modified_at;
        if ($data_timeslot->status_laporan==6 || empty($data_timeslot->status_laporan)){

          $timeslot_h = new DateTime(date('Y-m-d ').$data_timeslot->dispatch_teknisi_timestart);
          $current = new DateTime(date('Y-m-d H:i:s'));
          if ($current>$timeslot_h)
          $timeCount = $this->timeCount(date('Y-m-d ').$data_timeslot->dispatch_teknisi_timestart);
        }
        $manja = "<b>Keterangan Manja</b> : <br />".html_entity_decode($data_timeslot->manja)."<br /><b>Status Teknisi</b> : <br />$data_timeslot->laporan_status / <small>$timetoCount</small><br /><b>Catatan Teknisi</b> : <br />".$data_timeslot->catatan;
        $manja .= "<br /><a class='label btn-info' href='/$data_timeslot->id_tbl_mj'>Detail</a>";
        if ($lastRegu <> $data_timeslot->id_regu) {
          if ($data_timeslot->dispatch_teknisi_timeslot_id <> NULL){
            $data[$data_timeslot->area][$data_timeslot->id_regu][$data_timeslot->dispatch_teknisi_timeslot_id][] = array("manja"=>$manja,"orderName"=>$data_timeslot->orderName,"timeCount" => $timeCount,"SC" => $data_timeslot->Ndem, "status_laporan" => $data_timeslot->status_laporan);
          }
        } else {
          $data[$data_timeslot->area][$data_timeslot->id_regu][$data_timeslot->dispatch_teknisi_timeslot_id] = array("manja"=>$manja,"orderName"=>$data_timeslot->orderName,"timeCount" => $timeCount, "SC" => $data_timeslot->Ndem, "status_laporan" => $data_timeslot->status_laporan);
          $lastRegu = $data_timeslot->id_regu;
        }
    endforeach;

    $listTim = DB::SELECT('
      SELECT
        a.id_regu,
        b.uraian,
        e.title as area,
        count(*) as jumlah_wo,
        SUM(CASE WHEN a.manja_status = 1 THEN 1 ELSE 0 END) as jumlah_manja,
        SUM(CASE WHEN a.manja_status = 2 THEN 1 ELSE 0 END) as jumlah_manja_nok
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN roc cc ON a.Ndem = cc.no_tiket
      LEFT JOIN mdf d ON c.sto = d.mdf
      LEFT JOIN group_telegram e ON b.mainsector = e.chat_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      GROUP BY b.id_regu
    ');

    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND a.area = '".$area."'";
    }
    $get_area = DB::SELECT('
      SELECT
        a.title as area,
        a.title as DESKRIPSI
      FROM
        group_telegram a

      GROUP BY a.title
    ');

    // print_r($data);

    return view('dispatch.timeslot',compact('timeslot','data','listTim','get_area'));
  }

  public function timeCount($startDatex){


    // $endDate = date("Y-m-d H:i:s");
    // $result = round(abs($endDate - $startDate) / 60,2);
    $startDate = new DateTime($startDatex);
    $endDate = new DateTime();
    $since_start = $startDate->diff($endDate);
    if ($since_start->h>0) $h = $since_start->h."h "; else $h = '';
    if ($since_start->i>0) $i = $since_start->i."min "; else $i = '';
    $result = $h. $since_start->i."min";
    // return $endDate->format('Y-m-d H:i:s')."<br />".$startDatex."<br /> ".$result;

    return $result;
  }

  public function reportPsb2($tgl)
  {
  if (empty($tgl)){
    $tgl = date('Y-m-d');
  }

    $auth = session('auth');

    $get_list = DB::select('
      SELECT
        dps.*,
        i.*,
        emp.nama as HD,
        d.id as id_dt,
        d.updated_at,
        d.updated_at_timeslot,
        j.uraian,
        i.status_laporan,
        ii.laporan_status as status_wo_by_hd,
        ii.laporan_status as Sebab,
        i.catatan as Ket_Sebab,
        dps.orderId,
        cc.area as area,
        dps.orderNcli as Ncli,
        dps.ndemSpeedy as ND_Speedy,
        dps.orderName as Nama,
        (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = i.id) as jumlah_material,
        cc.mdf,
        k.dispatch_teknisi_timeslot
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN mdf cc ON dps.sto = cc.mdf
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN psb_laporan_status ii ON i.status_laporan = ii.laporan_status_id
      LEFT JOIN regu j ON d.id_regu = j.id_regu
      LEFT JOIN dispatch_teknisi_timeslot k ON d.updated_at_timeslot = k.dispatch_teknisi_timeslot_id
      LEFT JOIN 1_2_employee emp ON d.updated_by = emp.nik
        WHERE
        d.tgl like "'.$tgl.'%" and
        j.id_regu is not null
        ORDER BY j.id_regu
    ');
    $get_report = DB::select('
    SELECT
SUM(
          CASE WHEN i.status_laporan = 6 OR i.status_laporan is NULL
          THEN 1
          ELSE 0
          END ) AS BELUM_SURVEY,
          SUM(
          CASE WHEN i.status_laporan = 8
          THEN 1
          ELSE 0
          END ) AS SURVEY_OK,

          SUM(
          CASE WHEN i.status_laporan = 2
          THEN 1
          ELSE 0
          END ) AS ODP_LOS,

          SUM(
          CASE WHEN i.status_laporan = 9
          THEN 1
          ELSE 0
          END ) AS FOLLOW_UP_SALES,
          SUM(
          CASE WHEN i.status_laporan = 5
          THEN 1
          ELSE 0
          END ) AS OGP,
          SUM(
          CASE WHEN i.status_laporan =3
          THEN 1
          ELSE 0
          END ) AS KEND_PELANGGAN,

          SUM(
          CASE WHEN i.status_laporan =10
          THEN 1
          ELSE 0
          END ) AS PROGRESS_PT23,


          SUM(
          CASE WHEN i.status_laporan =12
          THEN 1
          ELSE 0
          END ) AS UP_DG_SC_LAIN,

          SUM(
          CASE WHEN i.status_laporan =13
          THEN 1
          ELSE 0
          END ) AS INPUT_ULANG,

          SUM(
          CASE WHEN i.status_laporan =14
          THEN 1
          ELSE 0
          END ) AS ANOMALI,

          SUM(
          CASE WHEN i.status_laporan =15
          THEN 1
          ELSE 0
          END ) AS KEND_SISTEM,


          SUM(
          CASE WHEN i.status_laporan =16
          THEN 1
          ELSE 0
          END ) AS PERMINTAAN_BATAL,


          SUM(
          CASE WHEN i.status_laporan =11
          THEN 1
          ELSE 0
          END ) AS INSERT_TIANG,
          SUM(
          CASE WHEN i.status_laporan =4
          THEN 1
          ELSE 0
          END ) AS HR,
          SUM(
          CASE WHEN i.status_laporan = 1
          THEN 1
          ELSE 0
          END ) AS UP,
          SUM(
          CASE WHEN i.status_laporan = 7
          THEN 1
          ELSE 0
          END ) AS RESCHEDULE,
      count(*) as jumlah,
      cc.area
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
      LEFT JOIN dispatch_teknisi_timeslot k ON d.updated_at_timeslot = k.dispatch_teknisi_timeslot_id
        WHERE
        d.tgl like "'.$tgl.'%" AND
        j.id_regu is not null AND
        cc.area is not null
        GROUP BY cc.area,c.area

    ');

    return view('dispatch.report2', compact('get_list','get_report'));
  }

  public function searchform()
  {
    $data = "";
    $regu = "";
    return view('dispatch.searchform2',compact('data','regu'));
  }

  public function search(Request $request)
  {
    $id     = $request->input('Search');
    if ($id == "")
    {
      $list = NULL;
    }
    else
    {
      $list   = DispatchModel::WorkOrder('SEARCH', $id);

      PsbModel::saveLogSearchId($id, session('auth')->id_karyawan, session('auth')->nama);

      if (count($list) > 0)
      {
        foreach ($list as $k => $v)
        {
          $log_dispatch[$v->orderId] = DB::table('psb_laporan_log AS pll')
          ->leftJoin('psb_laporan_status AS pls', 'pll.status_laporan', '=', 'pls.laporan_status_id')
          ->select('pls.laporan_status', 'pll.*')
          ->where('pll.Ndem', $v->orderId)
          ->groupBy('pll.psb_laporan_log_id')
          ->orderBy('pll.created_at', 'DESC')
          ->get();
        }
      }
    }


    // if (!empty($list)){
    //     $list = DispatchModel::QueryBuilderTambahan($list[0]->orderKontak);
    // };
    $data   = "Trying to search '".$id."'";
    $jumlah = count($list);

    // $dataUnsc = dispatchModel::getUnscByUnsc($id);
    return view('dispatch.searchform2',compact('data', 'list', 'jumlah', 'dataUnsc', 'id', 'material_alista', 'material_tambahan', 'log_dispatch'));
  }

  public static function monitoringTeknisi($type)
  {
    $get_data = DB::SELECT('
      SELECT
        dt.Ndem as order_id,
        pls.laporan_status,
        pl.modified_at,
        r.uraian as tim,
        gt.title as sektor,
        gt.TL_Username
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
        gt.chat_id <> "-1001210513589" AND
        dps.orderStatusId = "1500" AND
        dt.jenis_order IN ("SC","MYIR") AND
        DATE(dt.tgl) = "'.date('Y-m-d').'" AND
        TIMESTAMPDIFF( HOUR , pl.modified_at, "'.date('Y-m-d H:i:s').'") >= 1 AND
        pl.status_laporan IN (28,29,5,31)
      GROUP BY dt.Ndem
      ORDER BY pl.modified_at ASC
    ');

    if (count($get_data) > 0)
    {
      $messaggio   = "<b>MONITORING ORDER TEKNISI PROVISIONING [".date('d M Y H:i:s')."]</b>\n\n";

      foreach($get_data as $data) {

        $awal  = date_create($data->modified_at);
        $akhir = date_create();
        $diff  = date_diff( $awal, $akhir );

        if ($diff->d > 0) {
              $waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
        } else {
              $waktu = $diff->h.' Jam '.$diff->i.' Menit';
        }

        $messaggio  .= $data->sektor."\n";
        $messaggio  .= "[ <b>".$waktu."</b> - ".$data->laporan_status." ] // ".$data->order_id." // <b>".$data->tim."</b>";

      }

      Telegram::sendMessage([
        'chat_id' => "-1001661694038",
        'parse_mode' => 'html',
        'text' => $messaggio
      ]);

      // if ($type == "notif")
      // {
      //   $group_report = ['-1001082577132', '-429302864', '-443513821', '-533441763', '-434359285', '-459207105', '-579021569', '-566656887'];

      //   foreach ($group_report as $chatID) {
      //     Telegram::sendMessage([
      //       'chat_id' => $chatID,
      //       'parse_mode' => 'html',
      //       'text' => $messaggio
      //     ]);
      //   }
      // } elseif ($type == "remind") {
      //   Telegram::sendMessage([
      //       'chat_id' => "-568912202",
      //       'parse_mode' => 'html',
      //       'text' => $messaggio
      //     ]);
      // }

    }

    print_r("Finish! \n");
  }

  public static function monitoringActComp(){

    $get_data = DB::SELECT('
    SELECT
    dps.orderId,
    r.uraian as tim,
    dps.orderStatus,
    gt.title as sektor
    FROM Data_Pelanggan_Starclick dps
    LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    DATE(dps.orderDate) >= "2020-11-01"
    AND dps.jenisPsb LIKE "AO%"
    AND dps.orderStatusId IN ("1300","1205")
    ORDER BY dps.orderDate ASC
    ');

    if (count($get_data)>0) {
      $messaggio   = "MONITORING ACTCOMP PROVISIONING [".date('d M Y H:i:s')."]\n\n";

      foreach($get_data as $numb => $data) {
        $messaggio  .= $data->sektor."\n";
        $messaggio  .= "#$numb // ".$data->orderId." // ".$data->tim." // ".$data->orderStatus."\n\n";
      }

      Telegram::sendMessage([
        'chat_id' => "-1001127807166",
        'text' => $messaggio
      ]);
    }
  }

  public static function confirmOrder()
  {
    $get_data = DB::SELECT('
      SELECT
        pco.*,
        dt.jenis_order,
        pls.laporan_status,
        r.uraian as tim,
        gt.title as sektor
      FROM psb_confirm_order pco
      LEFT JOIN dispatch_teknisi dt ON pco.dt_id = dt.id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON pco.dt_id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
        pco.status_confirm = 1
      GROUP BY pco.id_order
      ORDER BY pco.created_at ASC
    ');

    if (count($get_data) > 0)
    {
      $messaggio   = "ORDER BELUM DIKONFIRMASI [".date('d M Y H:i:s')."]\n\n";

      foreach($get_data as $numb => $data) {
        if ($data->laporan_status <> NULL)
        {
          $status = $data->laporan_status;
        } else {
          $status = "ANTRIAN";
        }
        $messaggio  .= $data->sektor."\n";
        $messaggio  .= "#$numb // ".$data->id_order." // ".$status." // ".$data->jenis_order." // ".$data->created_at." // ".$data->tim."\n";
        $messaggio  .= "------------------------------------------------\n";
      }

      Telegram::sendMessage([
        'chat_id' => "-1001082577132",
        'text' => $messaggio
      ]);
    }
  }

  public function getOrdersDll($type)
  {
    $data = DispatchModel::getOrdersDll($type);

    return $data;

  }

  public function multiMigrasiStbPremium()
  {
    $regu = DispatchModel::getReguProv();
    $nopel = [];

    return view('dispatch.multiMigrasiStbPremium', compact('regu', 'nopel'));
  }

  public function multiCabutNte()
  {
    $regu = DispatchModel::getReguProv();

    $regu1 = $regu;
    $regu2 = $regu;

    $regu_terdispatch = DispatchModel::getReguTerdispatchCabutNte();

    $nopel = [];

    return view('dispatch.multiCabutNte',compact('regu1','regu2','regu_terdispatch', 'nopel'));
  }

  public function multiCabutNteSave(Request $req)
  {
    $this->validate($req,[
      'id_regu'         => 'required',
      'tgl_dispatch'    => 'required'
    ],[
      'id_regu.required'      => 'Wajib Pilih Regu !!',
      'tgl_dispatch.required' => 'Wajib Masukan Tanggal Dispatch !!'
    ]);

    Excel::load($req->file('multi'), function($reader) use($req) {
      $excels = $reader->toArray();
      $auth = session('auth');
      foreach($excels as $excel)
      {
        if ($excel['nopel'] != null)
        {
          $nopel = str_replace(".0","",$excel['nopel']);
          // dd($req->input('id_regu'),$req->input('tgl_dispatch'),$nopel);
          DB::transaction(function() use($auth, $req, $nopel) {

            $cno = DB::table('cabut_nte_order')->where('wfm_id', $nopel)->first();
            $dt = DB::table('dispatch_teknisi')->where('jenis_order','CABUT_NTE')->where('NO_ORDER', $nopel)->first();
            if ($cno==null && $dt==null) {
              $a = "undispatch_insert";
            } elseif ($dt==null && count($cno)>0) {
              $a = "undispatch";
            } elseif ($cno==null && count($dt)>0) {
              $a = "insert_undispatch";
            } else {
              $a = "dispatch";
            }

            if (count($cno)>0 && $a=="dispatch") {
              DB::table('cabut_nte_order')->where('wfm_id', $nopel)->update([
                'wfm_id'         => $nopel,
                // 'order_id'       => $order_id,
                'id_regu'        => $req->input('id_regu'),
                'nama_pelanggan' => $cno->nama_pelanggan,
                'tgl_order'      => DB::raw('NOW()'),
                'order_by'       => $auth->id_karyawan
              ]);

              DB::table('dispatch_teknisi')->where('NO_ORDER', $nopel)->where('jenis_order', 'CABUT_NTE')->update([
                'created_at'       => DB::raw('NOW()'),
                'updated_at'       => DB::raw('NOW()'),
                'updated_by'       => $auth->id_karyawan,
                'tgl'              => $req->input('tgl_dispatch'),
                'id_regu'          => $req->input('id_regu'),
                'Ndem'             => $nopel,
                'NO_ORDER'         => $nopel,
                'jenis_layanan_id' => "41",
                'jenis_layanan'    => "CABUT_NTE",
                'step_id'          => "1.0",
                'jenis_order'      => "CABUT_NTE"
              ]);

              DB::table('psb_laporan')->where('id_tbl_mj', $dt->id)->update([
                'status_laporan'  => 6
              ]);

              DB::table('psb_laporan_log')->insert([
                'created_at'      => DB::raw('NOW()'),
                'created_by'      => $auth->id_karyawan,
                'created_name'    => $auth->nama,
                'status_laporan'  => 6,
                'id_regu'         => $req->input('id_regu'),
                'jadwal_manja'    => DB::raw('NOW()'),
                'catatan'         => "REDISPATCH at ".date('Y-m-d'),
                'Ndem'            => $nopel
              ]);

            } elseif (count($cno)>0 && $a=="undispatch") {

              DB::table('cabut_nte_order')->where('wfm_id', $nopel)->update([
                'wfm_id'         => $nopel,
                // 'order_id'       => $order_id,
                'id_regu'        => $req->input('id_regu'),
                'nama_pelanggan' => $cno->nama_pelanggan,
                'tgl_order'      => DB::raw('NOW()'),
                'order_by'       => $auth->id_karyawan
              ]);

              DB::table('dispatch_teknisi')->insert([
                'created_at'       => DB::raw('NOW()'),
                'updated_at'       => DB::raw('NOW()'),
                'updated_by'       => $auth->id_karyawan,
                'tgl'              => $req->input('tgl_dispatch'),
                'id_regu'          => $req->input('id_regu'),
                'Ndem'             => $nopel,
                'NO_ORDER'         => $nopel,
                'jenis_layanan_id' => "41",
                'jenis_layanan'    => "CABUT_NTE",
                'step_id'          => "1.0",
                'jenis_order'      => "CABUT_NTE"
              ]);

              DB::table('psb_laporan_log')->insert([
                'created_at'      => DB::raw('NOW()'),
                'created_by'      => $auth->id_karyawan,
                'created_name'    => $auth->nama,
                'status_laporan'  => 6,
                'id_regu'         => $req->input('id_regu'),
                'jadwal_manja'    => DB::raw('NOW()'),
                'catatan'         => "DISPATCH at ".date('Y-m-d'),
                'Ndem'            => $nopel
              ]);

            } elseif ($a=="undispatch_insert") {

              DB::table('cabut_nte_order')->insert([
                'wfm_id'         => $nopel,
                // 'order_id'       => $order_id,
                'id_regu'        => $req->input('id_regu'),
                'nama_pelanggan' => $cno->nama_pelanggan,
                'tgl_order'      => DB::raw('NOW()'),
                'order_by'       => $auth->id_karyawan
              ]);

              DB::table('dispatch_teknisi')->insert([
                'created_at'       => DB::raw('NOW()'),
                'updated_at'       => DB::raw('NOW()'),
                'updated_by'       => $auth->id_karyawan,
                'tgl'              => $req->input('tgl_dispatch'),
                'id_regu'          => $req->input('id_regu'),
                'Ndem'             => $nopel,
                'NO_ORDER'         => $nopel,
                'jenis_layanan_id' => "41",
                'jenis_layanan'    => "CABUT_NTE",
                'step_id'          => "1.0",
                'jenis_order'      => "CABUT_NTE"
              ]);

              DB::table('psb_laporan_log')->insert([
                'created_at'      => DB::raw('NOW()'),
                'created_by'      => $auth->id_karyawan,
                'created_name'    => $auth->nama,
                'status_laporan'  => 6,
                'id_regu'         => $req->input('id_regu'),
                'jadwal_manja'    => DB::raw('NOW()'),
                'catatan'         => "DISPATCH at ".date('Y-m-d'),
                'Ndem'            => $nopel
              ]);
            } elseif ($a=="insert_undispatch") {

              DB::table('cabut_nte_order')->insert([
                'wfm_id'         => $nopel,
                // 'order_id'       => $order_id,
                'id_regu'        => $req->input('id_regu'),
                'nama_pelanggan' => $cno->nama_pelanggan,
                'tgl_order'      => DB::raw('NOW()'),
                'order_by'       => $auth->id_karyawan
              ]);

              DB::table('dispatch_teknisi')->where('NO_ORDER', $nopel)->where('jenis_order', 'CABUT_NTE')->update([
                'created_at'       => DB::raw('NOW()'),
                'updated_at'       => DB::raw('NOW()'),
                'updated_by'       => $auth->id_karyawan,
                'tgl'              => $req->input('tgl_dispatch'),
                'id_regu'          => $req->input('id_regu'),
                'Ndem'             => $nopel,
                'NO_ORDER'         => $nopel,
                'jenis_layanan_id' => "41",
                'jenis_layanan'    => "CABUT_NTE",
                'step_id'          => "1.0",
                'jenis_order'      => "CABUT_NTE"
              ]);
            }

            DB::transaction(function () use ($req, $auth, $nopel) {
              DB::table('dispatch_teknisi_log')->insert([
                'updated_at'          => DB::raw('NOW()'),
                'updated_by'          => $auth->id_karyawan,
                'tgl'                 => $req->input('tgl_dispatch'),
                'id_regu'             => $req->input('id_regu'),
                'Ndem'                => $nopel,
                'NO_ORDER'            => $nopel,
                'jenis_order'         => 'CABUT_NTE'
              ]);
            });

          });
        }
      }
    });

    return redirect('/multi/cabutNte')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch Cabut NTE']
    ]);

  }

  public function multiCabutNtePindah(Request $req)
  {
    $this->validate($req,[
      'id_regu_a'       => 'required',
      'tgl_dispatch'    => 'required',
      'id_regu_b'       => 'required'
    ],[
      'id_regu_a.required'      => 'Wajib Pilih Regu A !!',
      'tgl_dispatch.required'   => 'Wajib Masukan Tanggal Dispatch !!',
      'id_regu_b.required'      => 'Wajib Pilih Regu B !!'
    ]);

    $regu_a = $req->input('id_regu_a');
    $regu_b = $req->input('id_regu_b');
    $auth = session('auth');

    $get_data = DB::SELECT('
        SELECT
          cno.wfm_id,
          dt.id as id_dt,
          dt.id_regu,
          r.uraian,
          pl.status_laporan
        FROM dispatch_teknisi dt
        LEFT JOIN cabut_nte_order cno ON dt.NO_ORDER = cno.wfm_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        WHERE
          dt.id_regu = "'.$regu_a.'" AND
          dt.jenis_order = "CABUT_NTE" AND
          (pl.status_laporan = 6 OR pl.status_laporan IS NULL)
        ORDER BY cno.tgl_order
    ');
    // dd($get_data);

    foreach($get_data as $data){
        DB::table('cabut_nte_order')->where('wfm_id', $data->wfm_id)->update([
          'id_regu'        => $regu_b,
          'tgl_order'      => DB::raw('NOW()'),
          'order_by'       => $auth->id_karyawan
        ]);

        DB::table('dispatch_teknisi')->where('NO_ORDER', $data->wfm_id)->where('jenis_order', 'CABUT_NTE')->update([
          'created_at'       => DB::raw('NOW()'),
          'updated_at'       => DB::raw('NOW()'),
          'updated_by'       => $auth->id_karyawan,
          'tgl'              => $req->input('tgl_dispatch'),
          'id_regu'          => $regu_b,
          'jenis_layanan_id' => "41",
          'jenis_layanan'    => "CABUT_NTE",
          'step_id'          => "1.0",
          'jenis_order'      => "CABUT_NTE"
        ]);

        DB::table('psb_laporan_log')->insert([
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'created_name'    => $auth->nama,
          'status_laporan'  => 6,
          'id_regu'         => $regu_b,
          'jadwal_manja'    => DB::raw('NOW()'),
          'catatan'         => "REDISPATCH at ".date('Y-m-d'),
          'Ndem'            => $data->wfm_id
        ]);
    }

    return redirect('/multi/cabutNte')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> pindah order Cabut NTE']
    ]);
  }

  public function multiOntPremium()
  {
    $regu = DispatchModel::getReguProv();

    return view('dispatch.multiOntPremium',compact('regu'));
  }

  public function multiOntPremiumSave(Request $req){

    $this->validate($req,[
      'id_regu'         => 'required',
      'tgl_dispatch'    => 'required',
      'multi'           => 'required'
    ],[
      'id_regu.required'      => 'Wajib Masukan Regu !!',
      'tgl_dispatch.required' => 'Wajib Masukan Tanggal Dispatch !!',
      'multi.required'        => 'Perhatikan Format yang di Gunakan !!'
    ]);

    $multi = $req->input('multi');
    $xmulti = explode(';', $multi);
    $auth = session('auth');

    foreach($xmulti as $xm){
      $xmultix   = explode(',',$xm);
      $inet    = str_replace(array(" ","\r\n"),"",$xmultix[0]);
      $voice  = str_replace(array("SC"," "),"", $xmultix[1]);
      $pelanggan = str_replace(array(",",".",";")," ",$xmultix[2])."-PR";
      // dd($inet,$voice,$pelanggan);
      DB::transaction(function() use($req, $auth, $inet, $voice, $pelanggan) {
        $opo = DB::table('ont_premium_order')->where('no_inet', $inet)->first();
        $dt = DB::table('dispatch_teknisi')->where('jenis_order','ONT_PREMIUM')->where('NO_ORDER', $inet)->first();
        if ($opo==null && $dt==null) {
          $a = "undispatch_insert";
        } elseif ($dt==null && count($opo)>0) {
          $a = "undispatch";
        } elseif ($opo==null && count($dt)>0) {
          $a = "insert_undispatch";
        } else {
          $a = "dispatch";
        }
        // dd($opo,$dt,$inet,$a);

        if (count($opo)>0 && $a=="dispatch") {
          DB::table('ont_premium_order')->where('no_inet', $inet)->update([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => $req->input('id_regu'),
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => $auth->id_karyawan
          ]);

          DB::table('dispatch_teknisi')->where('NO_ORDER', $inet)->where('jenis_order', 'ONT_PREMIUM')->update([
            'created_at'       => DB::raw('NOW()'),
            'updated_at'       => DB::raw('NOW()'),
            'updated_by'       => $auth->id_karyawan,
            'tgl'              => $req->input('tgl_dispatch'),
            'id_regu'          => $req->input('id_regu'),
            'Ndem'             => $inet,
            'NO_ORDER'         => $inet,
            'jenis_layanan_id' => "42",
            'jenis_layanan'    => "ONT_PREMIUM",
            'step_id'          => "1.0",
            'jenis_order'      => "ONT_PREMIUM"
          ]);

          DB::table('psb_laporan')->where('id_tbl_mj', $dt->id)->update([
            'status_laporan'  => 6
          ]);

          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'created_name'    => $auth->nama,
            'status_laporan'  => 6,
            'id_regu'         => $req->input('id_regu'),
            'jadwal_manja'    => DB::raw('NOW()'),
            'catatan'         => "REDISPATCH at ".date('Y-m-d'),
            'Ndem'            => $inet
          ]);

        } elseif (count($opo)>0 && $a=="undispatch") {

          DB::table('ont_premium_order')->where('no_inet',$inet)->update([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => $req->input('id_regu'),
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => $auth->id_karyawan
          ]);

          DB::table('dispatch_teknisi')->insert([
            'created_at'       => DB::raw('NOW()'),
            'updated_at'       => DB::raw('NOW()'),
            'updated_by'       => $auth->id_karyawan,
            'tgl'              => $req->input('tgl_dispatch'),
            'id_regu'          => $req->input('id_regu'),
            'Ndem'             => $inet,
            'NO_ORDER'         => $inet,
            'jenis_layanan_id' => "42",
            'jenis_layanan'    => "ONT_PREMIUM",
            'step_id'          => "1.0",
            'jenis_order'      => "ONT_PREMIUM"
          ]);

          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'created_name'    => $auth->nama,
            'status_laporan'  => 6,
            'id_regu'         => $req->input('id_regu'),
            'jadwal_manja'    => DB::raw('NOW()'),
            'catatan'         => "DISPATCH at ".date('Y-m-d'),
            'Ndem'            => $inet
          ]);

        } elseif ($a=="undispatch_insert") {

          DB::table('ont_premium_order')->insert([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => $req->input('id_regu'),
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => $auth->id_karyawan
          ]);

          DB::table('dispatch_teknisi')->insert([
            'created_at'       => DB::raw('NOW()'),
            'updated_at'       => DB::raw('NOW()'),
            'updated_by'       => $auth->id_karyawan,
            'tgl'              => $req->input('tgl_dispatch'),
            'id_regu'          => $req->input('id_regu'),
            'Ndem'             => $inet,
            'NO_ORDER'         => $inet,
            'jenis_layanan_id' => "42",
            'jenis_layanan'    => "ONT_PREMIUM",
            'step_id'          => "1.0",
            'jenis_order'      => "ONT_PREMIUM"
          ]);

          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'created_name'    => $auth->nama,
            'status_laporan'  => 6,
            'id_regu'         => $req->input('id_regu'),
            'jadwal_manja'    => DB::raw('NOW()'),
            'catatan'         => "DISPATCH at ".date('Y-m-d'),
            'Ndem'            => $inet
          ]);

          } elseif ($a=="insert_undispatch") {

          DB::table('ont_premium_order')->insert([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => $req->input('id_regu'),
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => $auth->id_karyawan
          ]);

          DB::table('dispatch_teknisi')->where('NO_ORDER', $inet)->where('jenis_order', 'ONT_PREMIUM')->update([
            'created_at'       => DB::raw('NOW()'),
            'updated_at'       => DB::raw('NOW()'),
            'updated_by'       => $auth->id_karyawan,
            'tgl'              => $req->input('tgl_dispatch'),
            'id_regu'          => $req->input('id_regu'),
            'Ndem'             => $inet,
            'NO_ORDER'         => $inet,
            'jenis_layanan_id' => "42",
            'jenis_layanan'    => "ONT_PREMIUM",
            'step_id'          => "1.0",
            'jenis_order'      => "ONT_PREMIUM"
          ]);

          DB::table('psb_laporan')->where('id_tbl_mj', $dt->id)->update([
            'status_laporan'  => 6
          ]);

          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'created_name'    => $auth->nama,
            'status_laporan'  => 6,
            'id_regu'         => $req->input('id_regu'),
            'jadwal_manja'    => DB::raw('NOW()'),
            'catatan'         => "DISPATCH at ".date('Y-m-d'),
            'Ndem'            => $inet
          ]);

        }

        DB::transaction(function () use ($req, $auth, $inet) {
          DB::table('dispatch_teknisi_log')->insert([
            'updated_at'          => DB::raw('NOW()'),
            'updated_by'          => $auth->id_karyawan,
            'tgl'                 => $req->input('tgl_dispatch'),
            'id_regu'             => $req->input('id_regu'),
            'Ndem'                => $inet,
            'NO_ORDER'            => $inet,
            'jenis_order'         => 'ONT_PREMIUM'
          ]);
        });

        $check_starclick = DB::table('Data_Pelanggan_Starclick')->where('internet', $inet)->orderBy('orderDate','desc')->first();
        if ($check_starclick <> NULL) {
          $jenisPsb = substr($check_starclick->jenisPsb , 0, 2);
          if ($jenisPsb <> "AO"){
            $replace = str_replace($jenisPsb,"AO", $check_starclick->jenisPsb);
            DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger', $check_starclick->orderIdInteger)->update([
              'jenisPsb'    => $replace
            ]);
          }
        }

      });
    }

    return redirect('/multi/ontPremium')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch ONT Premium']
    ]);

  }

  public function undispatchOrder()
  {

    return view('dispatch.undispatchOrder');
  }

  public function undispatch_cabut_nte(Request $req){

    $this->validate($req,[
      'format'            => 'required'
    ],[
      'format.required'   => 'Perhatikan Format yang di Gunakan !!'
    ]);

    $format = $req->input('format');
    $xformat = explode(';', $format);
    $auth = session('auth');

    foreach($xformat as $fm){
      $xformatx   = explode(',',$fm);
      $wfm_id    = str_replace(array(" ","\r\n"),"",$xformatx[0]);
      $order_id  = str_replace(array("SC"," "),"", $xformatx[1]);
      $pelanggan = str_replace(array(",",".",";")," ",$xformatx[2])."-CBT";
      // dd($wfm_id,$order_id,$pelanggan);
      DB::transaction(function() use($req, $auth, $wfm_id, $order_id, $pelanggan) {
        $cno = DB::table('cabut_nte_order')->where('wfm_id', $wfm_id)->first();

        if(count($cno)>0){
          DB::table('cabut_nte_order')->where('wfm_id', $wfm_id)->update([
            'wfm_id'         => $wfm_id,
            'order_id'       => $order_id,
            'id_regu'        => 0,
            'nama_pelanggan' => $pelanggan,
            'tgl_order'      => DB::raw('NOW()'),
            'order_by'       => "UNDISPATCH_".$auth->id_karyawan
          ]);

        }else{

          DB::table('cabut_nte_order')->insert([
            'wfm_id'         => $wfm_id,
            'order_id'       => $order_id,
            'id_regu'        => 0,
            'nama_pelanggan' => $pelanggan,
            'tgl_order'      => DB::raw('NOW()'),
            'order_by'       => "UNDISPATCH_".$auth->id_karyawan
          ]);

        }
      });
    }

    return redirect('/multi/undispatchOrder')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Undispatch Cabut NTE']
    ]);

  }

  public function undispatch_ont_premium(Request $req){

    $this->validate($req,[
      'format'           => 'required'
    ],[
      'format.required'        => 'Perhatikan Format yang di Gunakan !!'
    ]);

    $format = $req->input('format');
    $xformat = explode(';', $format);
    $auth = session('auth');

    foreach($xformat as $fm){
      $xformatx  = explode(',',$fm);
      $inet      = str_replace(array(" ","\r\n"),"",$xformatx[0]);
      $voice     = str_replace(" ","",$xformatx[1]);
      $alamat    = str_replace("\r\n","",$xformatx[2]);
      $pelanggan = str_replace(array(",",".",";")," ",$xformatx[3])."-PR";
      // dd($inet,$voice,$pelanggan);
      DB::transaction(function() use($req, $auth, $inet, $voice, $alamat, $pelanggan) {
        $opo = DB::table('ont_premium_order')->where('no_inet', $inet)->first();

        if(count($opo)>0){
          DB::table('ont_premium_order')->where('no_inet', $inet)->update([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => 0,
            'alamat'         => $alamat,
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => "UNDISPATCH_".$auth->id_karyawan
          ]);

        }else{

          DB::table('ont_premium_order')->insert([
            'no_inet'        => $inet,
            'no_voice'       => $voice,
            'id_regu'        => 0,
            'alamat'         => $alamat,
            'nama_pelanggan' => $pelanggan,
            'tanggal_order'  => DB::raw('NOW()'),
            'dispatch_by'    => "UNDISPATCH_".$auth->id_karyawan
          ]);

        }
      });
    }

    return redirect('/multi/undispatchOrder')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Undispatch ONT Premium']
    ]);

  }

  public function deleteDispatch(Request $req,  $order, $dt, $pl)
  {
      DispatchModel::deleteDispatch($dt);

      DB::table('dispatch_teknisi_delete_log')->insert([
        'order_id'                => $order,
        'id_dispatch_teknisi'     => $dt,
        'id_psb_laporan'          => $pl,
        'deleted_by'              => session('auth')->id_karyawan,
        'deleted_named'           => session('auth')->nama,
        'deleted_at'              => DB::raw('NOW()')
     ]);

      return redirect('/dispatch/search')->with('alerts',[['type'=>'success', 'text'=>'Dispatch Sukses Dihapus di Tim ini ...']]);
  }

  public function deleteIbooster(Request $req, $id){
      DispatchModel::deleteIbooster($id);

      DB::table('ibooster_log')->insert([
             'order_id'   => $id,
             'deleted_by' => session('auth')->id_karyawan,
             'log'        => 'Order '.$id.' Success Delete By '.session('auth')->id_karyawan,
             'deleted_at' => DB::raw('NOW()')
          ]);

      return back()->with('alerts',[['type' => 'success', 'text' => 'Ukur Ibooster berhasil dihapus']]);
  }

  public function deleteCabutanNte($id){

    DB::table('cabut_nte_order')->where('wfm_id', $id)->delete();
    DB::table('dispatch_teknisi')->where('NO_ORDER', $id)->delete();

    return back()->with('alerts',[['type' => 'success', 'text' => 'Order Deleted Successfully']]);
  }

  public function reportCFC()
  {
    $auth = session('auth');

    $get_list = DB::select('
      SELECT
        dps.*,
        b.*,
        e.*,
        i.*,
        d.id as id_dt,
        j.uraian,
        i.status_laporan,
        case i.status_laporan
          when "1" then "UP"
          when "2" then "KENDALA TEKNIS"
          when "3" then "KENDALA PELANGGAN"
          when "4" then "HR"
          when "5" then "OGP"
        else "BELUM SURVEY"
        end as status_wo_by_hd,
        dps.orderId,
        c.area,
        cc.area as areaSC,
        (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = i.id) as jumlah_material,
        b.mdf,
        a.orderId as orderIdMS2N,
        a.jenisPsb as jenisPsbMS2N
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
        WHERE
        d.updated_at like "'.date('Y-m-d').'%" and
        j.id_regu is not null AND
        (dps.kcontact LIKE "%(CFC)%" OR a.kcontact LIKE "%(CFC)%") AND
        j.uraian NOT LIKE "%SHVA LAN%"
        GROUP BY b.ND_Speedy,dps.ndemSpeedy
        ORDER BY j.id_regu
    ');
    $get_report = DB::select('
    SELECT
        case i.status_laporan
          when "1" then "UP"
          when "2" then "KENDALA TEKNIS"
          when "3" then "KENDALA PELANGGAN"
          when "4" then "HR"
          when "5" then "OGP"
        else "BELUM SURVEY"
        end as status_wo_by_hd,
      count(*) as jumlah
      FROM
        dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick dps ON d.Ndem = dps.orderId
      LEFT JOIN ms2n b ON d.Ndem = b.Ndem
      LEFT JOIN Data_Pelanggan_Starclick a ON a.orderNcli = b.Ncli
      LEFT JOIN mdf c ON b.mdf = c.mdf
      LEFT JOIN mdf cc ON cc.mdf = dps.sto
      LEFT JOIN psb_laporan i ON d.id = i.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON e.ms2n_sebab = b.Sebab
      LEFT JOIN ms2n_status_hd h on b.Ndem = h.Ndem
      LEFT JOIN regu j ON d.id_regu = j.id_regu
        WHERE
        d.updated_at like "2016-07-25%" AND
        (dps.kcontact LIKE "%(CFC)%" OR a.kcontact LIKE "%(CFC)%") AND
        j.id_regu is not null
        GROUP BY i.status_laporan
        ORDER BY j.id_regu

    ');

    return view('dispatch.report2', compact('get_list'));
  }

  public function reportPsb()
  {
    $auth = session('auth');
    $list = DB::select('
      SELECT ms2n.*, id_regu AS id_r, mdf.area,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, Status_Indihome, status_laporan, ms2n_sebab_value, ms2n_sebab_pic,
      case psb_laporan.status_laporan
        when "1" then "UP"
        when "2" then "ODP LOS"
        when "11" then "INSERT TIANG"
        when "10" then "PROGRESS PT23"
        when "3" then "KENDALA PELANGGAN"
        when "4" then "HR"
        when "5" then "OGP"
        when "7" then "RESCHEDULE"
        when "6" then "BELUM SURVEY"
        when "8" then "SURVEY OK"
        when "9" then "FOLLOW UP SALES"
        when "12" then "UP DG SC LAIN"
        when "13" then "INPUT ULANG"
        when "14" then "ANOMALI"
        when "15" then "KENDALA SISTEM"
        when "16" then "PERMINTAAN BATAL"
        else "BELUM SURVEY"
      end as status_wo_by_hd,
      dispatch_teknisi.id as id_dt,
      (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = psb_laporan.id) as jumlah_material

      FROM ms2n
        LEFT JOIN dispatch_teknisi ON ms2n.Ndem = dispatch_teknisi.Ndem
        LEFT JOIN psb_laporan on dispatch_teknisi.id=psb_laporan.id_tbl_mj
        LEFT JOIN mdf on ms2n.MDF = mdf.mdf
        LEFT join ms2n_sebab on ms2n.Sebab = ms2n_sebab.ms2n_sebab
        LEFT JOIN ms2n_status_hd h on ms2n.Ndem = h.ndem
      WHERE  dispatch_teknisi.updated_at like "'.date('Y-m-d').'%" and id_regu is not null order by ms2n.mdf
    ');

    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();
    $lastArea = '';
    foreach($list as $row) {

      if($row->area != 'BJM'){
        if ($lastArea == $row->area) {
          $outer[count($outer)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt,  'STATUS' => $row->status_wo_by_hd ,'NDEM' => $row->Ndem, 'detil'=>$row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama);
        }
        else {
          if($row->ms2n_sebab_value <> 'Kendala'){
          $outer[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy, 'ID_DT'=> $row->id_dt, 'STATUS' => $row->status_wo_by_hd, 'NDEM'=>$row->Ndem, 'detil' => $row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama)));
          $lastArea = $row->area;
          }
        }
        $countOuter++;
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->uraian , $y->uraian);
    });
    $tim = array();
    $lastTim = '';
    if($auth->id_user =='wandiy99'){
      //return $list;
    }
    $i=0;
    foreach($list as $row) {
      if ($row->jumlah_material>0 && $row->status_wo_by_hd=='UP') {
        $status_material = "MATERIAL OK";
      } else {
        $status_material = "";
      }
      if($row->area == 'BJM'){
        if ($lastTim == $row->uraian) {
          $tim[count($tim)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy, 'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material,'STATUS' => $row->status_wo_by_hd ,'NDEM' => $row->Ndem, 'detil'=>$row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama);
          //$tim[count($tim)-1]['WO'][]['ND'] = $row->ND;
        }
        else {
          $tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'NDEM'=>$row->Ndem, 'detil' => $row->ND.' ~ '.$row->ND_Speedy.' : '.$row->Nama)));
//           $tim[] = array('head' => $row->uraian, 'WO' => array( 'ND' => $row->ND));
          $lastTim = $row->uraian;
        }
        $countInner++;
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->ms2n_sebab_pic , $y->ms2n_sebab_pic);
    });
    $kendala = array();
    $lastKendala = '';
    $bs = 0;
    $up = 0;
    $ogp = 0;
    $kdl = 0;
    foreach($list as $row) {
      if($row->ms2n_sebab_value == 'Kendala'){
        if ($lastKendala == $row->ms2n_sebab_pic) {
          $kendala[count($kendala)-1]['WO'][] = $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')';
        }
        else {
          $kendala[] = array('head' => $row->ms2n_sebab_pic, 'WO' => array( $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')'));
          $lastKendala = $row->ms2n_sebab_pic;
        }

      }
      if($row->status_wo_by_hd == 'KENDALA'){
        $kdl++;
      }if($row->status_wo_by_hd == 'BELUM SURVEY'){
        $bs++;
      }if($row->status_wo_by_hd == 'OGP'){
        $ogp++;
      }if($row->status_wo_by_hd == 'UP'){
        $up++;
      }
    }
    $counting = array('KENDALA' => $kdl, 'BELUM SURVEY' => $bs, 'OGP' => $ogp, 'UP' => $up, 'INNER' => $countInner, 'OUTER' => $countOuter, 'WO' => $countWo);
    //return $kendala;
    //return ($outer);
    return view('dispatch.report', compact('tim', 'outer', 'kendala', 'counting'));
  }
  private function array_remval($val, &$arr)
    {
          $array_remval = $arr;
          for($x=0;$x<count($array_remval);$x++)
          {
              $i=array_search($val,$array_remval);
              if (is_numeric($i)) {
                  $array_temp  = array_slice($array_remval, 0, $i );
                $array_temp2 = array_slice($array_remval, $i+1, count($array_remval)-1 );
                $array_remval = array_merge($array_temp, $array_temp2);
              }
          }

          return $array_remval;
    }
  public function input($id)
  {
    // $check_cutoff_psb = DB::table('psb_cutoff_rekon')->where('order_id', $id)->whereIn('status_teknisi', ['UP', 'HR'])->first();
    // if (count($check_cutoff_psb) > 0)
    // {
    //   return back()->with('alerts', [
    //     ['type' => 'danger', 'text' => 'Order Sudah Masuk Data Cut-Off Rekon!']
    //   ]);
    // }

    $ketDispatch = '';

    $exists = DB::select('
      SELECT
      b.kordinatPel,
      b.alamatSales,
      b.kpro_timid,
      b.manja_status,
      a.*,
      b.id_regu,
      r.crewid as rcrewid,
      b.crewid,
      b.id,
      b.tgl,
      b.manja,
      a.orderName as Nama,
      c.*,
      dtjl.jenis_layanan,
      d.koorPelanggan,
      d.alamatLengkap
      FROM Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderIdInteger = b.NO_ORDER
      LEFT JOIN regu r ON b.id_regu = b.id_regu
      LEFT JOIN dispatch_teknisi_timeslot c ON b.updated_at_timeslot = c.dispatch_teknisi_timeslot_id
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan_id = dtjl.jenis_layanan_id
      LEFT JOIN psb_myir_wo d ON a.myir = d.myir
      WHERE
      (b.jenis_order IN ("SC", "MYIR") OR b.jenis_order IS NULL) AND
      a.orderIdInteger = ?
    ',[
      $id
    ]);
    if (count($exists)==0)
    {
      $exists = DB::select('
        SELECT
        dispatch_teknisi.alamatSales,
        dispatch_teknisi.kordinatPel,
        dispatch_teknisi.kpro_timid,
        dispatch_teknisi.manja_status,
        dispatch_teknisi.manja,
        dispatch_teknisi.updated_at_timeslot,
        a.orderName as Nama,
        id_regu,
        r.crewid,
        id,
        tgl
        FROM Data_Pelanggan_Starclick a
        LEFT JOIN dispatch_teknisi on a.orderId = dispatch_teknisi.Ndem
        LEFT JOIN regu r ON dispatch_teknisi.id_regu = r.id_regu
        WHERE
        (dispatch_teknisi.jenis_order IN ("SC", "MYIR") OR a.jenis_order IS NULL) AND
        a.orderId = ?
      ',[
        $id
      ]);
    }
    if (count($exists)==0)
    {
      $exists = DB::select('
        SELECT
        b.kpro_timid,
        b.alamatSales,
        b.kordinatPel,
        b.manja_status,
        b.manja,
        b.updated_at_timeslot,
        a.NAMA as Nama,
        b.id_regu,
        r.crewid,
        b.id,
        b.tgl
        FROM dossier_master a
        LEFT JOIN dispatch_teknisi b on a.ND = b.Ndem
        LEFT JOIN regu r ON dispatch_teknisi.id_regu = r.id_regu
        WHERE a.ND = ?
      ',[
        $id
      ]);

      $ketDispatch = 'migrasi';
    }
    $data = $exists[0];
    
    // if (str_contains($data->kcontact, 'PTGSTDKDTG') == true)
    // {
    //   return back()->with('alerts', [
    //     ['type' => 'danger', 'text' => 'Maaf, Order Terbaca Petugas Tidak Datang!']
    //   ]);
    // }

    $get_jenis_layanan = DB::table('dispatch_teknisi_jenis_layanan')->where('active_layanan', 1)->orderBy('jenis_layanan', 'asc')->get();

    $semalam = date('Y-m', strtotime("-1 month"));
    $regu = DB::select('
      SELECT r.id_regu as id,r.uraian as text, r.job,
      SUM(CASE WHEN (DATE(qbt.dtPs) LIKE "'.$semalam. '%") AND qbt.tag_unit LIKE "%ASO%" THEN 1 ELSE 0 END) as jml_qc
      FROM regu r
      LEFT JOIN group_telegram b ON r.mainsector = b.chat_id
      LEFT JOIN dispatch_teknisi dt ON r.id_regu = dt.id_regu
      LEFT JOIN qc_borneo_tr6 qbt ON dt.NO_ORDER = qbt.sc
      WHERE
      r.ACTIVE = 1 AND b.sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND b.sms_active_prov = 1 AND b.ket_posisi IS NOT NULL
      GROUP BY text
    ');
    $timeslot = DB::select('
      SELECT *,dispatch_teknisi_timeslot_id as id, dispatch_teknisi_timeslot as text FROM dispatch_teknisi_timeslot
    ');
    $kpro_tim = DB::select('
      SELECT *, timid as id, nama_tim as text FROM kpro_tim
    ');
    $get_manja_status = DB::SELECT('
      SELECT
        a.manja_status_id as id,
        a.manja_status as text
      FROM
        manja_status a
    ');

    $get_bundling = DB::table('Data_Pelanggan_Starclick')->where('kcontact','LIKE','%TREGVI%')->where('orderId', $id)->first();

    $dataUnscc = dispatchModel::getUnscByUnsc($id);
    if (!empty($dataUnscc))
    {
        $dataUnsc = $dataUnscc;
    } else {
        $dataUnsc = [];
    }

    return view('dispatch.input', compact('id','data', 'get_jenis_layanan','regu', 'timeslot','get_manja_status', 'kpro_tim', 'dataUnsc', 'ketDispatch','get_bundling'));

  }

  public function save(Request $request, $id)
  {

    // echo "Trial Integrasi K-Pro<br />";

    // validasi
    $this->validate($request,[
        'jenis_layanan' => 'required',
        'id_regu'       => 'required',
        'crewid'       => 'required',
        'tgl'           => 'required',
        'kordinatPel'   => 'required',
        'alamatSales'   => 'required'
    ],[
        'jenis_layanan.required'  => 'Jenis Layanan Kosong!',
        'id_regu.required'        => 'Regu Kosong!',
        'crewid.required'         => 'Crew ID Kosong!',
        'tgl.required'            => 'Tanggal Dispatch Kosong!',
        'kordinatPel.required'    => 'Kordinat Pelanggan Kosong!',
        'alamatSales.required'    => 'ALamat Sales Kosong!'
    ]);

    if ($request->ketDispatch=="migrasi")
    {
        $this->validate($request,[
            'ketOrder'  => 'required'
        ],
        [
            'ketOrder.required' => 'Harus Diisi Jika WO Migrasi',
        ]);
    }

    $auth = session('auth');
    if ($request->input('manja')<>"")
    {
      $manja = $request->input('manja');
    } else {
      $manja = null;
    };

    $bypass = DB::table('dispatch_teknisi_bypass')->where('nik', session('auth')->id_karyawan)->first();
    if (count($bypass) != 1) {
      if ( date('H') < 18) {
        if ($request->input('id_regu') <> "") {
            // cek absen tim
            $cek_tim_absen = DB::table('regu')->where('id_regu', $request->input('id_regu'))->first();
            $jml_tek = 0;

            if ($cek_tim_absen->nik1 <> "" && $cek_tim_absen->nik2 <> "")
            {
              $jml_tek = "2";
            } elseif ($cek_tim_absen->nik1 <> "" && $cek_tim_absen->nik2 == NULL || $cek_tim_absen->nik1 == NULL && $cek_tim_absen->nik2 <> "") {
              $jml_tek = "1";
            }

            $cek_absen_tim = DB::SELECT('
              SELECT
              r.id_regu,
              a.nik,
              a.tglAbsen,
              a.approval,
              a.date_approval
              FROM absen a
              LEFT JOIN regu r ON (r.nik1 = a.nik OR r.nik2 = a.nik)
              WHERE
              (DATE(a.tglAbsen) = "'.date('Y-m-d').'") AND
              r.id_regu = '.$request->input('id_regu').'
              GROUP BY a.nik
            ');

            if (count($cek_absen_tim) != $jml_tek)
            {
              return back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Absensi Tim Belum Lengkap !']
              ]);
            }
        }
      }
    }

    $exists = DB::table('dispatch_teknisi')
    ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
    ->leftJoin('regu', 'dispatch_teknisi.id_regu', '=', 'regu.id_regu')
    ->where('Ndem', $id)
    ->select('dispatch_teknisi.*', 'psb_laporan.status_laporan', 'regu.mainsector')
    ->first();

    // insert into log status
    $status_laporan = 6;
    if (count($exists) > 0)
    {
      if ($exists->status_laporan == 4)
      {
        $status_laporan = 4;
      }
    };

    $cek_jenis_layanan = DB::table('dispatch_teknisi_jenis_layanan')->where('jenis_layanan',$request->input('jenis_layanan'))->first();
		$jenis_layanan = $cek_jenis_layanan->jenis_layanan_id;

    DB::transaction(function() use($request, $id, $auth, $manja, $status_laporan) {
       DB::table('psb_laporan_log')->insert([
         'created_at'      => DB::raw('NOW()'),
         'created_by'      => $auth->id_karyawan,
         'created_name'    => $auth->nama,
         'status_laporan'  => $status_laporan,
         'id_regu'         => $request->input('id_regu'),
         'catatan'         => "DISPATCH at ".date('Y-m-d'),
         'Ndem'            => $id
       ]);

        DB::table('dispatch_teknisi_log')->insert([
          'updated_at'          => DB::raw('NOW()'),
          'updated_by'          => $auth->id_karyawan,
          'tgl'                 => $request->input('tgl'),
          'id_regu'             => $request->input('id_regu'),
          'manja'               => $manja,
          'manja_status'        => $request->input('manja_status'),
          'updated_at_timeslot' => $request->input('timeslot'),
          'Ndem'                => $id,
          'NO_ORDER'            => $id
        ]);
     });

    // reset status if redispatch
    if (count($exists) > 0)
    {
      DB::transaction(function() use($request, $exists, $auth, $id, $status_laporan, $manja, $jenis_layanan)
      {
        DB::table('psb_laporan')->where('id_tbl_mj', $exists->id)->update([
            'status_laporan' => $status_laporan,
            'status_qc_reject' => '1',
            'status_qc_reject_by' => session('auth')->id_karyawan,
            'status_qc_reject_date' => date('Y-m-d H:i:s')
          ]);

          DB::table('psb_laporan_log')->insert([
             'created_at'      => DB::raw('NOW()'),
             'created_by'      => $auth->id_karyawan,
             'status_laporan'  => $status_laporan,
             'id_regu'         => $request->input('id_regu'),
             'catatan'         => "REDISPATCH",
             'Ndem'            => $id,
             'psb_laporan_id'  => $exists->id
           ]);

          DB::table('dispatch_teknisi')->where('id', $exists->id)->update([
              'NO_ORDER'              => $id,
              'updated_at'            => DB::raw('NOW()'),
              'updated_by'            => $auth->id_karyawan,
              'tgl'                   => $request->input('tgl'),
              'manja'                 => $manja,
              'manja_status'          => $request->input('manja_status'),
              'updated_at_timeslot'   => $request->input('timeslot'),
              'id_regu'               => $request->input('id_regu'),
              'crewid'                => $request->input('crewid'),
              'jenis_layanan'         => $request->input('jenis_layanan'),
              'jenis_layanan_id'      => $jenis_layanan,
              'kpro_timid'            => $request->input('kpro_timid'),
              'step_id'               => "1.0",
              'kordinatPel'           => $request->input('kordinatPel'),
              'alamatSales'           => $request->alamatSales,
              'ketOrder'              => $request->input('ketOrder'),
              'jenis_order'           => "SC",
              'dispatch_sto'          => $request->input('sto')
            ]);

        DB::table('qc_reject_log')->insert([
            'id_dt'       => $exists->id,
            'sc'          => $id,
            'rejected_by' => session('auth')->id_karyawan,
            'id_regu'     => $request->input('id_regu'),
            'log'         => 'Order redispatch By '.session('auth')->id_karyawan,
            'updated_at'  => DB::raw('NOW()')
          ]);

      });

    } else {

      // check if error
      $check = DB::table('dispatch_teknisi')->where('Ndem', $id)->whereIn('jenis_order', ["SC", "MYIR"])->get();
      if (count($check) > 0) {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Order Terbaca Duplikat!']
        ]);
      }

      DB::transaction(function() use($request, $id, $auth, $manja, $jenis_layanan) {
        DB::table('dispatch_teknisi')->insert([
          'NO_ORDER'            => $id,
          'updated_at'          => DB::raw('NOW()'),
          'updated_by'          => $auth->id_karyawan,
          'tgl'                 => $request->input('tgl'),
          'id_regu'             => $request->input('id_regu'),
          'crewid'              => $request->input('crewid'),
          'manja'               => $manja,
          'created_at'          => DB::raw('NOW()'),
          'manja_status'        => $request->input('manja_status'),
          'updated_at_timeslot' => $request->input('timeslot'),
          'jenis_layanan'       => $request->input('jenis_layanan'),
          'jenis_layanan_id'    => $jenis_layanan,
          'Ndem'                => $id,
          'kpro_timid'          => $request->input('kpro_timid'),
          'step_id'             => "1.0",
          'kordinatPel'         => $request->kordinatPel,
          'alamatSales'         => $request->alamatSales,
          'ketOrder'            => $request->input('ketOrder'),
          'jenis_order'         => "SC",
          'dispatch_sto'        => $request->input('sto')
        ]);
      });

    }
    print_r("Success");

    // update data_pelanggan_startclick ketDis
    DB::table('Data_Pelanggan_Starclick')->where('orderId', $id)->update(['ketDis' => '1']);

    // if ($manja<>"EMPTY"){
    //     exec('cd ..;php artisan sendTelegramSSC1 '.$id.' '.$auth->id_user.' > /dev/null &');
    // }

    $fitur = DB::table('fitur')->first();

    // kirim data comparin
    switch ($fitur->comparin_dispatch) {
      case '1':
          $id_regu = $request->input('id_regu');
          $this->dispatchComparin($id, $id_regu);
        break;
    }

    // kirim data tactical pro
    // switch ($fitur->tacticalpro) {
    //   case '1':
    //       ApiModel::uploadPickupOnline($id);
    //     break;
    // }

    return redirect('/dispatch/search')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUCCESS</strong> Dispatch Order!']
    ]);
  }

  private function dispatchComparin($sc, $id_regu)
  {
    $regu = DB::table('regu')->where('id_regu', $id_regu)->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.128.16.65/comparin/controller/api/tomman_dispatch.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'order_id='.$sc.'&order_type=SC&id_regu='.$regu->id_regu.'&teknisi='.$regu->nik1.','.$regu->nik2.'&token=c0815b4a7cd87885df4f0548939e9e6e5a8962a7844f1dcb7b3f85888fc4db75',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $decode = json_decode($response);
    DB::table('dispatch_comparin_log')->insert([
      'order_id'      => $sc,
      'type_order'    => 'SC',
      'id_regu'       => $regu->id_regu,
      'teknisi'       => $regu->nik1.','.$regu->nik2,
      'status_api'    => @$decode->status,
      'message_api'   => @$decode->message,
      'created_by'    => session('auth')->id_karyawan
    ]);

    return $response;
  }

  public static function dataLogDispatchComparin()
  {


    $get_data = DB::SELECT('
      SELECT
      dt.jenis_order,
      dt.Ndem as order_id,
      dt.id_regu,
      r.nik1,
      r.nik2,
      r.uraian as nama_regu,
      ma.mitra_amija_pt as mitra_regu,
      gt.title as sektor_regu,
      dt.created_at as tgl_awal_dispatch,
      dt.tgl as tgl_order_dispatch,
      pls.laporan_status as status_teknisi,
      dps.orderStatus as status_starclickncx
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      dt.jenis_order IN ("SC","MYIR") AND
      DATE(dt.created_at) LIKE "'.date('Y-m').'%"
      ORDER BY dt.created_at DESC
    ');

    dd(count($get_data));

    foreach($get_data as $data)
    {
      $that = new DispatchController();
      $that->sendLogDispatchComparin($data->jenis_order, $data->order_id, $data->id_regu, $data->tgl_awal_dispatch , $data->status_teknisi, $data->status_starclickncx);
    }
  }

  private function sendLogDispatchComparin($jenis, $id, $id_regu, $tgl, $statustek, $statusorder)
  {
    $regu = DB::table('regu')->where('id_regu', $id_regu)->first();

    if ($jenis == "MYIR")
    {
      $order_id = "MYIR-".$id;
    } elseif ($jenis == "SC") {
      $order_id = $id;
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.128.16.65/comparin/controller/api/tomman_dispatch.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'order_type='.$jenis.'&order_id='.$order_id.'&id_regu='.$regu->id_regu.'&teknisi='.$regu->nik1.','.$regu->nik2.'&tgl_dispatch='.$tgl.'&token=c0815b4a7cd87885df4f0548939e9e6e5a8962a7844f1dcb7b3f85888fc4db75',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $decode = json_decode($response);
    DB::transaction(function() use($jenis, $order_id, $tgl, $statustek, $statusorder, $regu, $decode) {
      DB::table('dispatch_comparin_log2')->insert([
        'order_id'        => $order_id,
        'type_order'      => $jenis,
        'nama_regu'       => $regu->uraian,
        'id_regu'         => $regu->id_regu,
        'teknisi'         => $regu->nik1.','.$regu->nik2,
        'tgl_dispatch'    => $tgl,
        'status_teknisi'  => $statustek,
        'status_order'    => $statusorder,
        'status_api'      => $decode->status,
        'message_api'     => $decode->message
      ]);
    });

    print_r("send $order_id status $decode->status \n");
    return $response;
  }

  public static function redispatchDismantleOntPremium()
  {
    $cek_progress_dism = DB::SELECT('
      SELECT
          dt.id as dt_id,
          pl.id as id_pl,
          dt.Ndem,
          dt.id_regu
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          pl.status_laporan IN (5,6,28,29) AND
          dt.jenis_order = "CABUT_NTE"
      ORDER BY cno.tgl_order DESC
    ');

    foreach ($cek_progress_dism as $cpd)
    {
      self::redispatchTodayTicket($cpd->dt_id,$cpd->id_pl,$cpd->Ndem,6,$cpd->id_regu);
      DB::transaction(function() use($cpd)
      {
        DB::table('psb_laporan')->where('id', $cpd->id_pl)->update([
          'modified_at'     => date('Y-m-d H:i:s'),
          'status_laporan'  => 6
        ]);
      });
      print_r("$cpd->Ndem Progress Dismantle \n");
    }

    $cek_noupdate_dismantle = DB::SELECT('
      SELECT
          dt.id as dt_id,
          pl.id as id_pl,
          dt.Ndem,
          dt.id_regu
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          dt.jenis_order = "CABUT_NTE" AND
          pl.id_tbl_mj IS NULL
      ORDER BY cno.tgl_order DESC
    ');

    foreach ($cek_noupdate_dismantle as $cnd)
    {
      self::redispatchTodayTicket($cnd->dt_id,0,$cnd->Ndem,6,$cnd->id_regu);
      print_r("$cnd->Ndem No Update Dismantling \n");
    }

    $cek_noupdate_ontpremium = DB::SELECT('
      SELECT
          dt.id as dt_id,
          pl.id as id_pl,
          dt.Ndem,
          dt.id_regu
      FROM ont_premium_order opo
      LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          dt.Ndem <> "" AND
          pl.id_tbl_mj IS NULL AND
          dt.jenis_order = "ONT_PREMIUM"
      ORDER BY opo.tanggal_order DESC
    ');

    foreach ($cek_noupdate_ontpremium as $cno)
    {
      self::redispatchTodayTicket($cno->dt_id,0,$cno->Ndem,6,$cno->id_regu);
      print_r("$cno->Ndem No Update ONTPremium \n");
    }

    $cek_progress_ontpremium = DB::SELECT('
      SELECT
          dt.id as dt_id,
          pl.id as id_pl,
          dt.Ndem,
          dt.id_regu
      FROM ont_premium_order opo
      LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          pl.status_laporan IN (5,6,28,29,101) AND
          dt.jenis_order = "ONT_PREMIUM"
      ORDER BY opo.tanggal_order DESC
    ');

    foreach ($cek_progress_ontpremium as $cpo) {
      self::redispatchTodayTicket($cpo->dt_id, $cpo->id_pl, $cpo->Ndem, 6, $cpo->id_regu);
      DB::transaction(function () use ($cpo) {
        DB::table('psb_laporan')->where('id', $cpo->id_pl)->update([
          'modified_at'     => date('Y-m-d H:i:s'),
          'status_laporan'  => 6
        ]);
      });
      print_r("$cpo->Ndem Progress ONTPremium \n");
    }
  }

  public static function redispatchToday()
  {
    $cek_sisa = DB::SELECT('
      SELECT
          dt.id as dt_id,
          dt.Ndem,
          dt.id_regu,
          pl.id as id_pl,
          pl.status_laporan,
          pls.laporan_status,
          dt.created_at,
          dt.tgl
      FROM dispatch_teknisi dt
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          dt.Ndem NOT LIKE "IN%" AND
          dt.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
          pl.status_laporan IN ("4", "6", "23", "46", "48", "49", "50", "55", "99") AND
          (DATE(dt.tgl) = "'.date('Y-m-d').'")
    ');

    foreach ($cek_sisa as $k)
    {
      self::redispatchTodayTicket($k->dt_id,$k->id_pl,$k->Ndem,$k->status_laporan,$k->id_regu);
      print_r($k);
    }

    print_r("Sukses Redispatch tiket sisa");

    $cek_noupdate = DB::SELECT('
      SELECT
          dt.id as dt_id,
          dt.Ndem,
          dt.id_regu,
          pls.laporan_status,
          dt.created_at,
          dt.tgl
      FROM dispatch_teknisi dt
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          dt.Ndem NOT LIKE "IN%" AND
          dt.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
          (DATE(dt.tgl) = "'.date('Y-m-d').'") AND
          dt.created_at IS NOT NULL AND
          pl.id_tbl_mj IS NULL
    ');

    foreach ($cek_noupdate as $n)
    {
      $status = 6; $id_pl = 0;
      self::redispatchTodayTicket($n->dt_id,$id_pl,$n->Ndem,$status,$n->id_regu);
      print_r($n);
    }

    print_r("Sukses Redispatch tiket No Update");

  }

  public static function redispatchTodayTicket($id,$id_pl,$ndem,$status,$regu)
  {
    $tomorrow = date('Y-m-d',strtotime("+1 days"));
    if ($id_pl == 0)
    {
      $pl = 'NULL';
    } else {
      $pl = $id_pl;
    }

    DB::transaction(function() use($id,$tomorrow,$status,$regu,$ndem,$pl) {
      DB::table('dispatch_teknisi')->where('id',$id)->update([
        'tgl'        => $tomorrow,
        'step_id'    => '1.0',
        'updated_by' => 20981020,
        'updated_at' => DB::raw('NOW()')
      ]);

      DB::table('psb_laporan_log')->insert([
        'created_at'      => DB::raw('NOW()'),
        'created_by'      => 20981020,
        'created_name'    => "MAHDIAN",
        'status_laporan'  => $status,
        'id_regu'         => $regu,
        'catatan'         => "REDISPATCH at ".date('Y-m-d'),
        'Ndem'            => $ndem,
        'psb_laporan_id'  => $pl
      ]);
    });
  }

  public function starclick(){
    ini_set('xdebug.var_display_max_depth', 5);
    ini_set('xdebug.var_display_max_children', 1024);
    ini_set('xdebug.var_display_max_data', 1024);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://starclick.telkom.co.id/backend/public/user/authenticate');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'username'=> "84153707",
        'password'=> "telkom135"
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    $request_content = substr($result, 0, $header['http_code']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    var_dump($header_content);
    echo "<br />";
    curl_setopt($ch, CURLOPT_URL, 'https://starclick.telkom.co.id/backend/public/order/load-inbox');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    curl_setopt($ch, CURLOPT_POST, true);
    $fields_string1 = "draw=1&columns%5B0%5D%5Bdata%5D=IDENTITY&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=ORDER_DTM&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=NAME&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=ALPRONAME&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=TNNUMBER&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=STATUSNAME&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=XS7&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=APPOINTMENTTIME&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=XN6&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=ACTION&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false";
    echo $fields_string1;
    echo "<br />";
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string1);
    $result = curl_exec($ch);
    print_r($result);

  }

  //update from console : php artisan kpro id regu
  public static function update_kpro($id, $jenis){
    try{
      echo "Order Id : ".$id."<br />";
      $url = "http://prov.rockal.id:7777/";
      //$url = "http://prov.rockal.id";
      $number = 1;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      $fields = array(
          'usr'=> "97158960",
          'pwd'=> "telkomakses",
          'b_submit' => ""
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      $result = curl_exec($ch);
      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);
      $header_content = substr($result, 0, $header['header_size']);
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0];
      echo "<br />";
      $query = DB::select('
        SELECT
          *
        FROM dispatch_teknisi a
        LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
        LEFT JOIN regu c ON a.id_regu = c.id_Regu
        LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
        LEFT JOIN regu f ON a.id_regu = f.id_regu
        WHERE ndem = "'.$id.'"
      ')[0];
      if($jenis == "Manja"){
        $manja_content = $query->manja_status." | ".$query->tgl." | ".$query->uraian. " | ".$query->manja;
        //manja ok
        if ($query->manja_status==1){
          $tl = "Management Janji";
        }
        //manja no ok
        else if ($query->manja_status==2){
          $tl = "Kendala Pelanggan";
        }
      }else{
        $manja_content = "[".$query->laporan_status."]
                           ".$query->uraian." |
                           ".$query->catatan." |
                           Koor.Pel : ".$query->kordinat_pelanggan. " |
                           Koor.ODP : ".$query->kordinat_odp;
        $tl = $query->status_kpro;
      }
      if($tl){
        // Telegram::sendMessage(["chat_id" => 52369916,
        //   "text" => "update kpro!!"
        // ]);
        //do manja/kendala kpro
        curl_setopt($ch, CURLOPT_URL, $url.'/ajax_tindaklanjut.php?jenis=savetdkljt');
        curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        curl_setopt($ch, CURLOPT_POST, true);
        $fields = array(
            'order_id'=> $id,
            'pilih_tl'=> $tl,
            'ket_tdk_ljt' => $manja_content
        );
        $fields_string = http_build_query($fields);
        echo $fields_string;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result1 = curl_exec($ch);
        curl_close($ch);
        // Telegram::sendMessage(["chat_id" => 52369916,
        //   "text" => $result1
        // ]);
        echo "<br />";
      }
    }catch (\Exception $e) {
      // Telegram::sendMessage(["chat_id" => 52369916,
      //           "text" => $e->getMessage()
      //       ]);
    }

  }
  public static function assign_kpro($id){
    try{
      echo "Order Id : ".$id."<br />";
      $url = "http://prov.rockal.id:7777/";
      //$url = "http://prov.rockal.id";
      $number = 1;
      $query = DB::select('
        SELECT
          *
        FROM dispatch_teknisi a
        LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
        LEFT JOIN regu c ON a.id_regu = c.id_Regu
        WHERE ndem = "'.$id.'"
      ')[0];
        $manja_content = $query->manja_status." | ".$query->tgl." | ".$query->uraian." | ".$query->manja;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      $fields = array(
          'usr'=> "97158960",
          'pwd'=> "telkomakses",
          'b_submit' => ""
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      $result = curl_exec($ch);
      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);
      $header_content = substr($result, 0, $header['header_size']);
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0];
      echo "<br />";
      //do assign kpro
      curl_setopt($ch, CURLOPT_URL, $url.'/ajax_timeschedule.php?jenis=saveassign');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      curl_setopt($ch, CURLOPT_POST, true);
      //manja ok
      $fields = array(
          'pilih_order'=> $id,
          'asg_timid'=> $query->kpro_timid,
          'asg_slot' => $query->timeslot_kpro,
          'asg_tgl' => $query->tgl
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result1 = curl_exec($ch);
      curl_close($ch);
      // Telegram::sendMessage(["chat_id" => 52369916,
      //     "text" => $result1
      // ]);
      echo "<br />";
    }catch (\Exception $e) {
      // Telegram::sendMessage(["chat_id" => 52369916,
      //     "text" => $e->getMessage()
      // ]);
    }

  }
  public function kpro($id,$regu,$jenis,$text){
    echo "Order Id : ".$id."<br />";
    $url = "http://prov.rockal.id:7777/";
    // $url = "http://prov.rockal.id";
    $number = 1;
    $query = DB::select('
      SELECT
        *
      FROM karyawan
      WHERE id_regu = "'.$regu.'"
    ');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'usr'=> "97158960",
        'pwd'=> "telkomakses",
        'b_submit' => ""
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    // print_r($header['header_size']);
    echo "<br />";
    if ($jenis=="dispatch") {
      foreach ($query as $result){
        if ($result->idkpro<>0) {
          curl_setopt($ch, CURLOPT_URL, $url.'/ajax_teknisi.php?jenis=savetek');
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          curl_setopt($ch, CURLOPT_POST, true);

          $fields = array(
              'tek'=> $number,
              'order_id'=> $id,
              'daftartech' => $result->idkpro
          );
          $fields_string = http_build_query($fields);
          echo $fields_string;
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $result1 = curl_exec($ch);
          print_r($result1);
          echo "Tek ".$number." | ".$result->idkpro." | ".$result->id_karyawan."<br />";
          echo "<br />";

          $number++;
        }
      }
    } else if ($jenis=="manja"){
      curl_setopt($ch, CURLOPT_URL, $url.'/ajax_tindaklanjut.php?jenis=savetdkljt');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      curl_setopt($ch, CURLOPT_POST, true);
      if ($regu==1){
        $tl = "Management Janji";
      } else if ($regu==2){
        $tl = "Kendala Pelanggan";
      }
      $fields = array(
          'order_id'=> $id,
          'pilih_tl'=> $tl,
          'ket_tdk_ljt' => $text
      );
      $fields_string = http_build_query($fields);
      echo $fields_string;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result1 = curl_exec($ch);
      print_r($result1);
      // echo "Tek ".$number." | ".$result->idkpro." | ".$result->id_karyawan."<br />";
      echo "<br />";
    }

}
public function getTimKpro(){
    $url = "http://prov.rockal.id:7777/";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url.'/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $fields = array(
        'usr'=> "97158960",
        'pwd'=> "telkomakses",
        'b_submit' => ""
    );
    $fields_string = http_build_query($fields);
    echo $fields_string;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $result = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    var_dump($header_content);
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0];
    // print_r($header['header_size']);
    echo "<br />";
    curl_setopt($ch, CURLOPT_URL, $url.'/timeschedule.php?tgl='.date("Y-m-d").'&asg=all&witel=KALSEL');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $result1 = curl_exec($ch);
    curl_close($ch);
    $columns = array(
            0 => 'no',
            'action',
            'timid',
            'nama_tim',
            'sto',
            'delapan',
            'sepuluh',
            'duabelas',
            'empatbelas',
            'enambelas',
            'delapanbelas',
            'tot',
            'reassign'
        );

    $result1 = str_replace("<a class='badge bg-blue' style='margin-top: 3px; margin-bottom: 3px;' onclick='status_tim(","",$result1);

    $result1 = str_replace(")'>Sta<br>Tus</a>","",$result1);

    $dom = @\DOMDocument::loadHTML(trim($result1));

    $table = $dom->getElementsByTagName('tbody')->item(0);

    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 0, $count = $rows->length-1; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }

        $result[] = $data;

    }
    $data = $result;
    DB::table('kpro_tim')->truncate();
    DB::table('kpro_tim')->insert($data);
}

  public function destroy($id)
  {
    //$this->decrementStokTeknisi($id);
    // cari filenya

    $data = DB::table('dispatch_teknisi')->where('Ndem',$id)->first();

    DB::table('dispatch_teknisi_log')->insert([
       'updated_at'          => DB::raw('NOW()'),
       'updated_by'          => session('auth')->id_karyawan,
       'tgl'                 => date('Y-m-d'),
       'id_regu'             => $data->id_regu,
       'manja'               => 'Dispatch Dihapus',
       'manja_status'        => $data->manja_status,
       'updated_at_timeslot' => $data->updated_at_timeslot,
       'Ndem'                => $id
    ]);

    DB::transaction(function() use($id) {
      DB::table('dispatch_teknisi')
          ->where('Ndem', [$id])->delete();
    });

    return redirect('/dispatch/workorder/READY')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Dispatch']
    ]);
  }

  //status by hd
  public function updateStatus($id)
  {
    $data = DB::select('
      SELECT m.Ndem, Nama, h.status_wo_by_hd, h.id
      FROM ms2n m
      LEFT JOIN ms2n_status_hd h on m.Ndem = h.ndem
      WHERE m.Ndem = ?
    ',[
      $id
    ])[0];
    return view('dispatch.status', compact('data'));
  }

  public function sendMessages(){


    /*
    echo "sending message to " . $chatID . "\n";
    $token = "bot187041523:AAGL_P6YmZiUryD2dVDpMasx1zGWgxke3YM";

    $url = "https://api.telegram.org/" . $token . "/sendMessage?chat_id=" . $chatID;
    $url = $url . "&text=" . urlencode($messaggio);
    $ch = curl_init();
    $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
    */
    //send
    $messaggio = "Test send Message via Tomman PSB";
    $chatID = "-137029837";
    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);
  }

  public function saveStatus(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM ms2n_status_hd
      WHERE ndem = ?
    ',[
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
        DB::table('ms2n_status_hd')
          ->where('id', $data->id)
          ->update([
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'status_wo_by_hd' => $request->input('status')
          ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    else {
        DB::table('ms2n_status_hd')->insert([
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'status_wo_by_hd' => $request->input('status'),
          'ndem'            => $id
        ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    return $exists;
  }

  public function sendsms($orderId){
    $auth = session('auth');
    $get_data = DB::select('
      SELECT
        a.*
      FROM
        Data_Pelanggan_Starclick a
      WHERE
        a.orderId = "'.$orderId.'"
      ')[0];
    $text = "Pelanggan Telkom Yth.Kami berencana melakukan intalasi Fiber Optik di lokasi Anda. Mhn infrmasi waktu plaksanaannya dg me-reply sms ini. Kami Siap melayani Anda.";
    if (substr($get_data->orderNotel,0,2)=="08") {
      $notel = $get_data->orderNotel;
    } else if (substr($get_data->orderKontak,0,2)=="08"){
      $notel = $get_data->orderKontak;
    } else {
      $notel = NULL;
    }

    if ($notel<>NULL){
      $id_dshr = DB::connection('mysql2')->table('outbox')->insertGetId([
          'DestinationNumber' => $notel,
          'TextDecoded' => $text
      ]);
    }

    $messaggio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";

    if ($notel<>NULL){
      $messaggio .= "SENT SMS to ".$get_data->orderKontak."\nFrom Work Order : \nSC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact;
    } else {
      $messaggio .= "FAILED SMS to ".$get_data->orderKontak."\nFrom Work Order : \nSC".$get_data->orderId."\n".$get_data->orderName."\n".$get_data->kcontact;
      $messageio .= "\nPHONE NUMBER NOT FOUND\n";
    }
    $chatID = "-184620664";

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);

    return redirect('/dispatch/workorder/READY')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim SMS']
      ]);
  }

  public function manja ($id)
  {
      // dd($id);
      $ketSc = '';
      $exists = DB::select('
      SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status,a.*,id_regu,id,tgl,manja,a.orderName as Nama, c.*
      FROM Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi ON a.orderId = dispatch_teknisi.Ndem
      LEFT JOIN dispatch_teknisi_timeslot c ON dispatch_teknisi.updated_at_timeslot = c.dispatch_teknisi_timeslot_id
      WHERE a.orderId = ?
      ',[
        $id
      ]);

      if (count($exists)==0){
        $exists = DB::select('
          SELECT dispatch_teknisi.*,dispatch_teknisi.kpro_timid,dispatch_teknisi.manja_status, dispatch_teknisi.manja, dispatch_teknisi.updated_at_timeslot,a.orderName as Nama,id_regu,id,tgl
          FROM Data_Pelanggan_Starclick a left join dispatch_teknisi
          on a.orderId=dispatch_teknisi.Ndem
          WHERE a.orderId = ?
        ',[
          $id
        ]);
      }

      if (count($exists)==0){
        $exists = DB::select('
          SELECT
          b.*,
           b.kpro_timid,
          b.manja_status,b.manja,b.updated_at_timeslot,a.NAMA as Nama,b.id_regu,b.id,b.tgl
          FROM dossier_master a left join dispatch_teknisi b
          on a.ND=b.Ndem
          WHERE a.ND = ?
        ',[
          $id
        ]);
      }

      if (count($exists)==0){
          $exists = dispatchModel::manjaSearchByMyir($id);
          $ketSc = 'myir';
      }

      $data = $exists[0];

      $get_manja_status = DB::SELECT('
        SELECT
          a.manja_status_id as id,
          a.manja_status as text
        FROM
          manja_status a
      ');

      return view('dispatch.manja', compact('data', 'get_manja_status', 'id', 'ketSc'));
  }

  public function saveManja(Request $req, $id)
  {
      // dd($req);
      // dd(session('auth'));

      $this->validate($req,[
          'manja_status' => 'required',
          'manja'        => 'required',
      ]);

      $exist = DispatchModel::getData($id);
      if ($exist){
          DispatchModel::updateManja($req, $exist->id, session('auth')->id_karyawan);

          // simpan log
          DB::table('dispatch_teknisi_log')->insert([
             'updated_at'          => DB::raw('NOW()'),
             'updated_by'          => session('auth')->id_karyawan,
             'tgl'                 => $exist->tgl,
             'id_regu'             => $exist->id_regu,
             'manja'               => $req->input('manja'),
             'manja_status'        => $req->input('manja_status'),
             'updated_at_timeslot' => $req->input('timeslot'),
             'Ndem'                => $id
          ]);

          // $auth = session('auth');
          $dataRegu = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);
          $namaDispatch = DB::select('SELECT * FROM 1_2_employee where nik= ?',[$dataRegu[0]->updated_by]);

          if (!empty($dataRegu[0]->id_regu)){
              // get regu
              $get_regu = DB::SELECT('
                SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
              ',[ $dataRegu[0]->id_regu ]);
              $get_regu = $get_regu[0];

              // get manja
              $get_manja_status = DB::SELECT('
                SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
              ',[ $dataRegu[0]->manja_status ]);
              $get_manja_status = $get_manja_status[0];

              // get data
              $ketSc = '';
              $get_data = DB::SELECT('
                SELECT
                  a.*,
                  b.mdf_grup_id
                FROM
                  Data_Pelanggan_Starclick a
                  LEFT JOIN mdf b ON a.sto = b.mdf
                WHERE a.orderId = ?
              ',[ $id ]);

              if (count($get_data)==0) {
                $get_data = DB::SELECT('
                  SELECT
                   a.*,
                   b.mdf_grup_id,
                   a.ND as orderId,
                   a.NAMA as orderName,
                   a.LQUARTIER as kcontact,
                   0 as lat,
                   0 as lon
                  FROM
                    dossier_master a
                  LEFT JOIN mdf b ON a.STO = b.mdf
                  WHERE
                    a.ND = ?
                ', [ $id ]);
              };

              if (count($get_data)==0){
                  $get_data = dispatchModel::manjaSearchByMyir($id);
                  $ketSc = 'myir';
              };


              // send to Telegram SSC1
              $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              if (!empty($namaDispatch)){
                  $messageio = "Pengirim : ".$namaDispatch[0]->nik."-".$namaDispatch[0]->nama."\n\n";
              }
              else{
                  $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              };

              $messageio .= "Manajemen Janji\n";
              $messageio .= "Regu : ".$get_regu->uraian."\n";
              $messageio .= "Tanggal : ".$dataRegu[0]->tgl."\n";
              $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
              // $messageio .= "Timeslot : ".$request_timeslot."\n";
              $messageio .= "Keterangan : \n".$dataRegu[0]->manja."\n\n";
              if ($ketSc=='myir'){
                  $messageio .= "Related to MYIR".$get_data[0]->myir."\n";
                  $messageio .= $get_data[0]->customer;
              }
              else{
                  $messageio .= "Related to SC".$get_data[0]->orderId."\n".$get_data[0]->orderName."\n".$get_data[0]->kcontact;
              };

              $get_data[0];
              $chatID = "-199400365";
              // // $chatID = "192915232";
              if (!empty($namaDispatch)){
                  $messageio = "Pengirim : ".$namaDispatch[0]->nik."-".$namaDispatch[0]->nama."\n\n";
              }
              else{
                  $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              };

              $messageio .= "Manajemen Janji\n";
              $messageio .= "Regu : ".$get_regu->uraian."\n";
              $messageio .= "Tanggal : ".$dataRegu[0]->tgl."\n";
              $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
              // $messageio .= "Timeslot : ".$request_timeslot."\n";
              $messageio .= "Keterangan : \n".$dataRegu[0]->manja."\n\n";
              if ($ketSc=='myir'){
                  $messageio .= "Related to MYIR".$get_data[0]->myir."\n";
                  $messageio .= $get_data[0]->customer;
              }
              else{
                  $messageio .= "Related to SC".$get_data[0]->orderId."\n".$get_data[0]->orderName."\n".$get_data[0]->kcontact;
              };

              $get_data[0];
              $chatID = "-199400365";
              $chatID = "192915232";
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);

              if ($ketSc<>'myir'){
                  if ($get_data[0]->lat<>NULL){
                    Telegram::sendMessage([
                      'chat_id' => $chatID,
                      'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                    ]);
                    Telegram::sendLocation([
                      'chat_id' => $chatID,
                      'latitude' => $get_data[0]->lat,
                      'longitude' => $get_data[0]->lon
                    ]);
                  }
                  if ($get_regu->mainsector<>NULL){
                    $chatID = $get_regu->mainsector;
                  } else {
                    $chatID = "-".$get_data[0]->mdf_grup_id;
                  }

                  Telegram::sendMessage([
                    'chat_id' => $chatID,
                    'text' => $messageio
                  ]);
                  Telegram::sendMessage([
                    'chat_id' => $chatID,
                    'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                  ]);
                  Telegram::sendLocation([
                    'chat_id' => $chatID,
                    'latitude' => $get_data[0]->lat,
                    'longitude' => $get_data[0]->lon
                  ]);
              }
            };

          return redirect('/manja/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'MANJA Sudah <strong>Disimpan</strong>']]);
      }
      else {
           DispatchModel::simpanManja($req, session('auth')->id_karyawan, $id);

          // simpan log
          DB::table('dispatch_teknisi_log')->insert([
             'updated_at'          => DB::raw('NOW()'),
             'updated_by'          => session('auth')->id_karyawan,
             'tgl'                 => $exist->tgl,
             'id_regu'             => $exist->id_regu,
             'manja'               => $req->input('manja'),
             'manja_status'        => $req->input('manja_status'),
             'updated_at_timeslot' => $req->input('timeslot'),
             'Ndem'                => $id
          ]);

          $auth = session('auth');
          $dataRegu = DB::select('SELECT * FROM dispatch_teknisi WHERE Ndem = ?',[$id]);
          if (!empty($dataRegu->id_regu)){

              // get regu
              $get_regu = DB::SELECT('
                SELECT a.uraian,a.mainsector FROM regu a WHERE a.id_regu = ?
              ',[ $dataRegu->id_regu ]);
              $get_regu = $get_regu[0];

              // get manja
              $get_manja_status = DB::SELECT('
                SELECT a.manja_status FROM manja_status a WHERE a.manja_status_id = ?
              ',[ $dataRegu->manja_status ]);
              $get_manja_status = $get_manja_status[0];

              // get data
              $get_data = DB::SELECT('
                SELECT
                  a.*,
                  b.mdf_grup_id
                FROM
                  Data_Pelanggan_Starclick a
                  LEFT JOIN mdf b ON a.sto = b.mdf
                WHERE a.orderId = ?
              ',[ $id ]);

              if (count($get_data)==0) {
                $get_data = DB::SELECT('
                  SELECT
                   a.*,
                   b.mdf_grup_id,
                   a.ND as orderId,
                   a.NAMA as orderName,
                   a.LQUARTIER as kcontact,
                   0 as lat,
                   0 as lon
                  FROM
                    dossier_master a
                  LEFT JOIN mdf b ON a.STO = b.mdf
                  WHERE
                    a.ND = ?
                ', [ $id ]);
              };

              // send to Telegram SSC1
              $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n\n";
              $messageio .= "Manajemen Janji\n";
              $messageio .= "Regu : ".$get_regu->uraian."\n";
              $messageio .= "Tanggal : ".$dataRegu[0]->tgl."\n";
              $messageio .= "Status Manja : ".$get_manja_status->manja_status."\n";
              // $messageio .= "Timeslot : ".$request_timeslot."\n";
              $messageio .= "Keterangan : \n".$dataRegu->manja."\n\n";
              $messageio .= "Related to SC".$get_data[0]->orderId."\n".$get_data[0]->orderName."\n".$get_data[0]->kcontact;

              $chatID = "-199400365";
              // $chatID = "192915232";
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);
              if ($get_data[0]->lat<>NULL){
                Telegram::sendMessage([
                  'chat_id' => $chatID,
                  'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
                ]);
                Telegram::sendLocation([
                  'chat_id' => $chatID,
                  'latitude' => $get_data[0]->lat,
                  'longitude' => $get_data[0]->lon
                ]);
              }
              if ($get_regu->mainsector<>NULL){
                $chatID = $get_regu->mainsector;
              } else {
                $chatID = "-".$get_data[0]->mdf_grup_id;
              }

              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => $messageio
              ]);
              Telegram::sendMessage([
                'chat_id' => $chatID,
                'text' => "Lokasi SC".$get_data[0]->orderId." ".$get_data[0]->orderName." 👇🏻"
              ]);
              Telegram::sendLocation([
                'chat_id' => $chatID,
                'latitude' => $get_data[0]->lat,
                'longitude' => $get_data[0]->lon
              ]);
            };
      }

      return redirect('/manja/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'MANJA sudah <strong>Disimpan</strong>']]);
  }

  public function getJmlWo($tgl)
  {
      $getWoSektors = DispatchModel::getJmlWoBySektor($tgl);
      return view('dashboard.listWoSektor', compact('getWoSektors', 'tgl'));
  }

  public function getJmlWoSingle($tgl, $sto, $idRegu)
  {
      $data = DispatchModel::getJmlWoBySektorIdRegu($tgl, $sto, $idRegu);
      dd($data);
  }

  public function listManja($tgl, DispatchModel $modelDispatch, Request $req)
  {
      $this->validate($req,[
          'q' => 'numeric',
      ]);
      date_default_timezone_set("Asia/Makassar");
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');

      $listManjas = []; //$modelDispatch->listManja($startdate,$enddate);
      $myir       = '';

      if ($req->has('q')){
          $cari = $req->input('q');
          $listManjas = $modelDispatch->manjaSearchBySc(trim($cari));
          $ketSc = 'sc';

          if (count($listManjas)==0){
              $listManjas = $modelDispatch->manjaSearchByMyir(trim($cari));
              $ketSc = 'myir';

              if (count($listManjas)==0){
                  $dataMyir = dispatchModel::cariMyir($cari);
                  if ($dataMyir->ket==1){
                      $scMyir = $dataMyir->sc;

                      $listManjas = $modelDispatch->manjaSearchBySc(trim($scMyir));
                      $ketSc = 'sc';
                      $myir  = $cari;
                  }
              }
          };

          // dd($listManjas);
      };

      return view('dispatch.listManja',compact('listManjas','ketSc','myir'));
  }

  public function jamBerangkatProv($tgl)
  {
      $sql = 'SELECT a.Ndem, b.status_laporan
              from
                  dispatch_teknisi a
              LEFT JOIN
                  psb_laporan_log b on a.Ndem=b.Ndem
              where
                  a.Ndem is not null and a.tgl like "%'.$tgl.'%" LIMIT 5';
      $hasil = DB::select($sql);
      dd($hasil);
  }

  public static function matrikTidakHadir(){

      $date = date('Y-m-d');
      $getTeamMatrix = DispatchModel::getTeamMatrixNot($date);
      $data = array();
      $lastRegu = '';
      $jmlProv = 0; $jmlAssr = 0;

      foreach($getTeamMatrix as $team) :
        $listWO = array();
        $listWOData = DispatchModel::listWO($date,$team->id_regu);

        foreach ($listWOData as $listData) :
          if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
              $listWO[] = $listData;
          };
        endforeach;

        $ketNik1='-';
        $ketNik2='-';

        if ($team->nik1<>NULL){
            $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
            if(count($statusNik1)<>NULL){
                $ketNik1 = "HADIR";
            };
        }

        if($team->nik2<>NULL){
          $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
          if(count($statusNik2)<>NULL){
              $ketNik2 = "HADIR";
          };
        }


        // echo $team->nik1."\n";
        // echo $statusNik2->nik.' '.$ketNik2."\n";

        $ketRegu = '';
        if ($ketNik1=="-" && $ketNik2=="-"){
            $data[$team->mainsector][] = array(
              "mitra"     => $team->mitra,
              "uraian"    => $team->uraian,
              "kemampuan" => $team->kemampuan,
              "nik1"      => $team->nik1,
              "nik2"      => $team->nik2,
              "ketNik1"   => $ketNik1,
              "ketNik2"   => $ketNik2,
              "jns"       => "-",
              "ketRegu"   => 'TIDAK HADIR',
              "chat_id"   => $team->chat_id,
              "title"     => $team->title,
              'statusWo'  => $team->sttsWo,
              "listWO"    => $listWO
            );
        };

      endforeach;

      $pesan = "List TIm Tidak Hadir Ada Wo ".date('Y-m-d')."\n";
      $pesan .= "====================================\n";
      $pesan .= "Provisioning\n";
      foreach ($data as $key=>$dt){
        if ($key<>'-1001133390186'){
          foreach($dt as $d){
            if ($d['statusWo']==1){
              if (count($d['listWO'])<>0){
                  $jmlProv += 1;
                  $pesan .= $d['title'].' - '.$d['uraian']."\n";
                  foreach($d['listWO'] as $wo){
                      $status = "NEED PROGRESS";
                      if ($wo->laporan_status<>''){
                          $status = $wo->laporan_status;
                      };

                      $pesan .= '-'.$wo->Ndem.' '.$status."\n";
                  }
                  $pesan .= "\n";
              }
            }
          }
        }
      }

      // $chatId = "102050936";
      $chatId = "192915232";
      if ($jmlProv<>0){
        Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $pesan
        ]);
      }

      $pesan = '';
      $pesan = "List TIm Tidak Hadir Ada Wo ".date('Y-m-d')."\n";
      $pesan .= "====================================\n";
      $pesan .= "Assurance\n";
      foreach ($data as $key=>$dt){
        if ($key<>'-1001133390186'){
          foreach($dt as $d){
            if ($d['statusWo']==0){
              $jmlAssr += 1;
              if (count($d['listWO'])<>0){
                  $pesan .= $d['title'].' - '.$d['uraian']."\n";
                  foreach($d['listWO'] as $wo){
                      $status = "NEED PROGRESS";
                      if ($wo->laporan_status<>''){
                          $status = $wo->laporan_status;
                      };

                      $pesan .= '-'.$wo->Ndem.' '.$status."\n";
                  }
                  $pesan .= "\n";
              }
            }
          }
        }
      }

      // $chatId = "102050936";
      $chatId = "192915232";
      if ($jmlAssr<>0){
        Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $pesan
        ]);
      }
  }

  public function matrixZainal($date){

    $area = 'PROV';
    $getTeamMatrix = DispatchModel::getTeamMatrix($date,$area);
    $get_area = DispatchModel::group_telegram($area);
    $data = array();
    $lastRegu = '';

    foreach($getTeamMatrix as $team) :
      $listWO = array();
      $listWOData = DispatchModel::listWO($date,$team->id_regu);

      foreach ($listWOData as $listData) :
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
            $listWO[] = $listData;
        };
      endforeach;

      $ketNik1='';
      $ketNik2='';

      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[] = array(
        "mitra" => $team->mitra,
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;

    if ($area=='ALL'){
        $ketSektor = 'ALL';
    }
    else{
        $ketSektor = 'PROVISIONING';
    }

    return view('dispatch.matrixZainal',compact('get_area','data','date','area','ketSektor'));
  }

  public function matrixReboundary($tgl){

    $date = $tgl;
    $area = Input::get('sektor');
    if ($area==""){ $area = 'ALL'; }
    $get_sektor = DispatchModel::get_sektor();
    $get_area = DispatchModel::group_telegram($area);
    $getTeamMatrix = DispatchModel::getTeamMatrixReboundary($date);
    $data = array();
    $lastRegu = '';

    foreach($getTeamMatrix as $team) :
      $listWO = array();
      $listWOData = DispatchModel::listWOReboundary($date,$team->id_regu);

      foreach ($listWOData as $listData) :
        if ($listData->status_laporan<>52 && $listData->status_laporan<>57 && $listData->status_laporan<>58 && $listData->status_laporan<>59 && $listData->status_laporan<>60){
            $listWO[] = $listData;
        };
      endforeach;

      $ketNik1='-';
      $ketNik2='-';

      if ($team->nik1<>NULL){
          $statusNik1 = DispatchModel::statusAbsen($team->nik1, $date);
          if(count($statusNik1)<>NULL){
              $ketNik1 = "HADIR";
          };
      }

      if($team->nik2<>NULL){
        $statusNik2 = DispatchModel::statusAbsen($team->nik2, $date);
        if(count($statusNik2)<>NULL){
            $ketNik2 = "HADIR";
        };
      }


      // echo $team->nik1."\n";
      // echo $statusNik2->nik.' '.$ketNik2."\n";

      $ketRegu = '';
      if ($ketNik1=="HADIR" || $ketNik2=="HADIR"){
          $ketRegu = "HADIR";
      };

      $data[$team->mainsector][] = array(
        "mitra" => $team->mitra,
        "uraian"=>$team->uraian,
        "kemampuan"=>$team->kemampuan,
        "nik1"=>$team->nik1,
        "nik2"=>$team->nik2,
        "ketNik1" =>$ketNik1,
        "ketNik2" =>$ketNik2,
        "jns"       => "-",
        "ketRegu"   => $ketRegu,
        "listWO" =>$listWO
      );
    endforeach;
    $ketSektor = 'REBOUNDARY';

    return view('dispatch.matrixReboundary',compact('get_area','data','get_sektor','date','area','ketSektor'));
  }

  public function matrixHDFoto($date, DispatchModel $dispatchModel){

    $hdFallout = DispatchModel::getHdFalloutMatrix($date);
    //dd($hdFallout);
    $fout = array();
    $lastHD = '';

    foreach ($hdFallout as $hd){
      if($lastHD == $hd->workerId){
          $fout[count($fout)-1]['jumlahWO'] += 1;
          $fout[count($fout)-1]['keterangan'] = $hd->ket;
          $fout[count($fout)-1]['WO'][] = array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket);
          //status

          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0" ){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }
           else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }


      }else{
          $fout[] = array('nama' => $hd->workerName,'jumlahWO'=>1,'PROGRESS'=>0,'UP'=>0, 'UP_PDA'=>0,'keterangan'=>$hd->ket,'WO' => array(array("sc"=>$hd->orderId, "status_laporan"=>$hd->orderStatus, "keteranganWo"=>$hd->ket)));
          if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="0"){
            $fout[count($fout)-1]['UP'] += 1;
          }
          else if (($hd->orderStatus=="Process OSS (Activation Completed)" || $hd->orderStatus=="Completed (PS)" || $hd->orderStatus=="Process ISISKA (RWOS)") && $hd->ket=="1" ){
            $fout[count($fout)-1]['UP_PDA'] += 1;
          }else{
            $fout[count($fout)-1]['PROGRESS'] += 1;
          }
      }
      $lastHD = $hd->workerId;
    }


    //dd($data);

    $periode = date('Y-m',strtotime($date));
    $manjas  = $dispatchModel->jumlahManjaByUser($periode);

    return view('dispatch.matrixHdFoto',compact('fout', 'manjas'));
  }

  public function listKendalaUp($tgl)
  {
      $sql = 'SELECT
                *,
              a.created_at as tglDispatch,
              a.tgl as tglReDispatch,
              a.id as id_dt,
              f.sto as area
              FROM
                dispatch_teknisi a
              LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem=f.orderId
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
              LEFT JOIN regu d ON a.id_regu=d.id_regu
              LEFT JOIN group_telegram g ON d.mainsector=g.chat_id
              LEFT JOIN mdf h ON f.sto = h.mdf
              WHERE
                a.dispatch_by IS NULL AND
                f.orderId<>"" AND
                b.status_kendala IS NOT NULL AND
                a.tgl like "%'.$tgl.'%"
              ORDER BY
                b.status_laporan ASC
              ';

      $data = DB::select($sql);

      $sql2 = 'SELECT
                *,
              a.created_at as tglDispatch,
              a.tgl as tglReDispatch,
              a.id as id_dt,
              f.sto as area
              FROM
                dispatch_teknisi a
              LEFT JOIN psb_myir_wo f ON a.Ndem=f.myir
              LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
              LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
              LEFT JOIN regu d ON a.id_regu=d.id_regu
              LEFT JOIN group_telegram g ON d.mainsector=g.chat_id
              LEFT JOIN mdf h ON f.sto = h.mdf
              WHERE
                a.dispatch_by=5 AND
                f.myir<>"" AND
                b.status_kendala IS NOT NULL AND
                a.tgl like "%'.$tgl.'%"
              ORDER BY
                b.status_laporan ASC
              ';

      $dataMyir = DB::select($sql2);
      $noMyir   = count($data)+1;
      return view('dispatch.listKendalaUp',compact('data', 'dataMyir', 'noMyir'));

  }

  public function tarikOrderDismanlte()
  {
    $get_data = DB::SELECT('
      SELECT
        cno.id_cno,
        cno.wfm_id,
        cno.nama_pelanggan,
        dt.id as id_dt,
        dt.tgl as tgl_dt,
        pl.status_laporan
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
        pl.status_laporan = "6" OR pl.id IS NULL AND dt.jenis_order = "CABUT_NTE"
      ORDER BY cno.tgl_order DESC
    ');

    foreach($get_data as $data)
    {
      DB::transaction(function() use($data) {
        DB::table('dispatch_teknisi')->where('id', $data->id_dt)->delete();

        DB::table('cabut_nte_order')->where('id_cno', $data->id_cno)->update([
          'id_regu' => null,
          'nama_pelanggan' => str_replace("-CBT","",$data->nama_pelanggan)
        ]);

      });

      print_r("Tarik Order $data->wfm_id\n");
    }

    $total = count($get_data);
    $datetime = date('Y-m-d H:i:s');
    $nama = session('auth')->nama;

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Selesai Tarik Order Dismantle <b>Total $total</b>\n\n<i>$nama\n$datetime</i>"
    ]);

    print_r("Selesai Tarik Order Dismantle Total $total $datetime");

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Tarik Order Dismantle Total <strong>'.$total.'</strong> dari Teknisi']
    ]);
  }

  public function tarikOrderontPremium()
  {
    $get_data = DB::SELECT('
      SELECT
        opo.id_opo,
        opo.no_inet,
        opo.nama_pelanggan,
        dt.id as id_dt,
        dt.tgl as tgl_dt,
        pl.status_laporan
      FROM ont_premium_order opo
      LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
        pl.status_laporan = "6" OR pl.id IS NULL AND dt.jenis_order = "ONT_PREMIUM"
      ORDER BY opo.tanggal_order DESC
    ');

    foreach($get_data as $data)
    {
      DB::transaction(function() use($data) {
        DB::table('dispatch_teknisi')->where('id', $data->id_dt)->delete();

        DB::table('ont_premium_order')->where('id_opo', $data->id_opo)->update([
          'id_regu' => 0,
          'nama_pelanggan' => str_replace("-PR","",$data->nama_pelanggan)
        ]);

      });

      print_r("Tarik Order $data->no_inet\n");
    }

    $total = count($get_data);
    $datetime = date('Y-m-d H:i:s');
    $nama = session('auth')->nama;

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Selesai Tarik Order ONT Premium <b>Total $total</b>\n\n<i>$nama\n$datetime</i>"
    ]);

    print_r("Selesai Tarik Order ONT Premium Total $total $datetime");

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Tarik Order ONT Premium Total <strong>'.$total.'</strong> dari Teknisi']
    ]);
  }

  public function orderMigrasiSTB()
  {

    $data = DB::table('migrasi_stb_premium')->select('no_internet as id', 'no_internet as text')->get();
    $regu = DB::table('regu')->where('ACTIVE', 1)->whereNull('mainsector_marina')->where('hold_order', 0)->get();

    return view('dispatch.orderMigrasiSTB', compact('data', 'regu'));
  }

  public static function all_alert_umur_pi()
  {
    self::alert_umur_pi('Saldo_PI_Lebih_4_Hours');
    print_r("Saldo_PI_Lebih_4_Hours\n\n");

    sleep(1);

    self::alert_umur_pi('TTR_PI_4_Hours');
    print_r("TTR_PI_4_Hours\n\n");

    sleep(1);

    self::alert_umur_pi('TTI_3_Days_TSEL');
    print_r("TTI_3_Days_TSEL\n\n");

    sleep(1);
  }

  public static function alert_umur_pi($type)
  {
    $dtm  = date('j F Y H:i:s');
    
    $data = DispatchModel::alert_umur_pi($type);

    $main_data = $send_message = [];

    foreach($data as $k => $v)
    {
        $main_data[$v->nama_sektor][$v->nama_tim] = $v;
    }

    foreach ($main_data as $k => $v)
    {
        $message = "<b>==== $k ====</b>\n\n";
        foreach($v as $kk => $value)
        {
            $orderDatePI_wita      = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime($value->orderDatePI)));
            $orderDateActComp_wita = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime($value->orderDateActComp)));
            $orderDateRegist_wita  = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime($value->orderDateRegist)));

            if ($type == 'TTI_3_Days_TSEL')
            {
              $tglAwal = new \DateTime($orderDateRegist_wita);
            }
            else
            {
              $tglAwal = new \DateTime($orderDatePI_wita);
            }

            if ($type == 'TTR_PI_4_Hours')
            {
              $tglAkhir = new \DateTime($orderDateActComp_wita);
            }
            else
            {
              $tglAkhir = new \DateTime(date('Y-m-d H:i:s'));
            }

            $umur_jam = $tglAkhir->diff($tglAwal)->format('%h.%i');

            if ($type == 'Saldo_PI_Lebih_4_Hours')
            {
              if ($umur_jam >= 4)
              {
                $message .= "<b>$kk</b>\n";

                $message .= "$value->orderId / $value->orderStatus / $umur_jam Jam\n\n";

                $send_message[$k]['context'] = $message;
                $sector_message[] = $value->chatid_sektor;
              }
            }
            else if ($type == 'TTR_PI_4_Hours')
            {
              if ($umur_jam >= 2 && $umur_jam <= 4)
              {
                $message .= "<b>$kk</b>\n";

                $message .= "$value->orderId / $value->orderStatus / $umur_jam Jam\n\n";

                $send_message[$k]['context'] = $message;
                $sector_message[] = $value->chatid_sektor;
              }
            }
            else if ($type == 'TTI_3_Days_TSEL')
            {
              if ($umur_jam >= 48 && $umur_jam <= 67)
              {
                $message .= "<b>$kk</b>\n";

                $message .= "$value->orderId / $value->orderStatus / $umur_jam Jam\n\n";

                $send_message[$k]['context'] = $message;
                $sector_message[] = $value->chatid_sektor;
              }
            }

            
        }
    }

    foreach ($send_message as $k => $v)
    {
      $final_message = "<b>🔥🔥 Alert <i>$type</i> 🔥🔥</b>\n\n";
      $final_message .= $v['context'];
      $final_message .= "\n<i>$dtm</i>\n";

      print_r($final_message);

      Telegram::sendMessage([
        'chat_id'    => '-1001661694038',
        'parse_mode' => 'html',
        'text'       => $final_message
      ]);

      sleep(10);
    }
  }

}
