<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\KproModel;
class KproController extends Controller
{
    public function dashboard()
    {
        $log_kpro = DB::table('provi_kpro_tr6')->select('last_grab')->where('witel', 'BANJARMASIN')->orderBy('last_grab', 'desc')->first();
        $query2p3p_potensi_ps = KproModel::dashboard('AO','2p3p','potensi_ps');
        $query2p3p_sisa_pi = KproModel::dashboard('AO','2p3p','sisa_pi');
        $query2p3p_sisa_fwfm = KproModel::dashboard('AO','2p3p','sisa_fwfm');
        return view('kpro.dashboard',compact('query2p3p_potensi_ps', 'query2p3p_sisa_pi', 'query2p3p_sisa_fwfm', 'log_kpro'));
    }
}
