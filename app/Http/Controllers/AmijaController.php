<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\AmijaModel;
class AmijaController extends Controller
{
  public function list($mitra){
    $query = AmijaModel::list($mitra);
    return view('amija.list',compact('query'));
  }
}
?>
