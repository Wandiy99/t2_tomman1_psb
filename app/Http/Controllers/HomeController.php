<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\HomeModel;
use App\DA\AlkersarkerModel;
use App\DA\AlproModel;
use DB;
use Validator;
use Telegram;

date_default_timezone_set('Asia/Makassar');

class HomeController extends Controller
{

	protected $photoInputs = [
		'absen_Hadir','absen_Pulang'
	  ];
	
	  private function handleFileUploadAbsensi($request, $auth, $id, $photo)
	  {
		foreach($photo as $name) {
		  $input = 'photo_'.$name;
		  if ($request->hasFile($input)) {
			// dd($input);
			$table = DB::table('hdd')->get()->first();
		  	$upload = $table->public;
			$path = public_path().'/'.$upload.'/absensi/'.$auth.'/'.$id.'/';
			if (!file_exists($path)) {
			  if (!mkdir($path, 0770, true))
				return 'Gagal Menyimpan Foto Absensi :(';
			}
			$file = $request->file($input);
			// $ext = $file->guessExtension();
			$ext = 'jpg';
			//TODO: path, move, resize
			try {
			  $moved = $file->move("$path", "$name.$ext");
	
			  $img = new \Imagick($moved->getRealPath());
			  $img->scaleImage(100, 150, true);
			  $img->writeImage("$path/$name-th.$ext");
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
			  return 'Gagal Menyimpan Foto Absensi :( '.$name;
			}
		  }
		}
	  }

	public function home2(){
		return view('home.home2x');
	}

	public function	index(){
		

		$this->home();
	}

	public function updatesquad($nik){
		$query = DB::SELECT('select * from 1_2_employee a left join employee_squad b ON a.squad = b.squad_id WHERE nik = "'.$nik.'"');
		$query_squad = DB::table('employee_squad')->get();
		return view('home.updatesquad',compact('nik','query','query_squad'));
	}

	public function updatesquad_save(Request $request){
		$simpan = DB::table('1_2_employee')->where('nik',$request->input('nik'))->update([
			'squad' => $request->input('squad')
		]);
		return redirect('/updatesquad/'.$request->input('nik'))->with('alerts',[
				['type'=>'success','text'=> 'UPDATE SQUAD SUCCESS']
			]);
	}

	public function alpro(){
		$nik = session('auth')->id_karyawan;
		$group_telegram = HomeModel::group_telegram($nik);
		$get_alpro = AlproModel::get_alpro();
		return view('alpro.list',compact('group_telegram','get_alpro','nik'));
	}

	public function absen_decline($id){
		

		$status_absen_by_id = HomeModel::status_absen_by_id($id)[0];
		return view('home.absen_decline',compact('status_absen_by_id'));
	}

	public function absen_decline_save(Request $req){
		

		$update_absen = HomeModel::update_absen_by_id($req->input('absen_id'),$req->input('keterangan'),3);
		if ($update_absen){
			return redirect('/home')->with('alerts',[
				['type'=>'success','text'=> 'ABSEN DECLINE SUCCESS']
			]);
		} else {
			return redirect()->back()->with('alerts',[
				['type'=>'danger','text'=> 'ABSEN DECLINE FAILED. TRY AGAIN IN A MINUTE']
			]);
		}
		// $decline =
	}


	public function absen_approval($nik,$date){
		

		$approve_absen = HomeModel::approve_absen($nik,$date);
		return redirect()->back()->with('alerts',[
			['type'=>'success','text'=> $nik.' ABSEN APPROVED']
		]);
	}

	public function home()
	{
    	$datex = date('Y-m-d H:i:s');
		$session = session('auth');
		$jamNow = date('H:i:s');
		$jam5 = "17:00:00";

		if($session->ACTIVE == 0)
		{
			return redirect('/login')->with('alerts',[
				['type'=>'danger','text'=> 'User is Not Active!']
			]);
		}
		
		if($session->level == 88 || $session->ACTIVE == 2)
		{
			return redirect('/login')->with('alerts',[
				['type'=>'danger','text'=> 'User is Banned!']
			]);
		}
		else if ($session->level == 0)
		{
			return redirect('/login')->with('alerts',[
					['type'=>'danger','text'=> 'User is Not Active!']
			]);
		}

		$jumlah_hadir = 0;
		if ($session->level == 10) {

			$upload_event = HomeModel::uploadEvent("Sertifikat_Vaksin");

			// if (count($upload_event)>0)
			// {
			// 	return back()->with('alerts',[['type' => 'success', 'text' => 'Rekan Terbaca Sudah Meng-upload Photo Event']]);
			// }

			$status_absen = HomeModel::status_absen($session->id_user);
			$regu_teknisi = HomeModel::regu_teknisi($session->id_user);

			// $alkersarker = AlkersarkerModel::list_teknisi(session('auth')->id_user);
			// $statusProv = HomeModel::cekStatusTekProv($session->id_user);
			// $status_absenEsok = HomeModel::status_absenEsok($session->id_user);

			return view('home.new_home_teknisi',compact('status_absen','datex','alkersarker','statusProv', 'status_absenEsok','jamNow','jam5','upload_event', 'ket_regu_teknisi', 'session'));
		} else {
			$active_team = HomeModel::active_team($session->id_user);
			// if ($session->id_user=='88159353' || $session->id_user == "720428"){
			// 	$active_team = HomeModel::active_team_all();
			// };

			// if ($session->id_user=='96158953'){
			// 	$active_team = HomeModel::active_team_all_atasan();
			// }

			$get_kesehatan = [
				(object)['id' => 'Sehat', 'text' => 'Sehat'],
				(object)['id' => 'Sakit', 'text' => 'Sakit'],
				(object)['id' => 'Izin', 'text' => 'Izin']
			  ];

			foreach($active_team as $at)
			{
				if ($at->status_kehadiran == "HADIR") $jumlah_hadir++;
			}
			$group_telegram = HomeModel::group_telegram($session->id_user);
			$absen_approval = HomeModel::absen_approval($session->id_user);
			// $inbox_xcheck_material_tl = HomeModel::inbox_xcheck_material_tl($session->id_user);

			$data_alker = array();
			foreach ($absen_approval as $absen)
			{
				$get_alker = AlkersarkerModel::list_teknisi_avai($absen->nik);
				foreach ($get_alker as $alker):
				$data_alker[$absen->nik][] = $alker->alkersarker;
				endforeach;
			}

			$data_team = array();
			foreach ($active_team as $query_active_team) :
				if ($query_active_team->NIK<>NULL)
				$data_team[$query_active_team->NIK] = HomeModel::active_team_p($query_active_team->NIK,$session->id_user);
			endforeach;

			// return view('home.home')
			return view('home.home',compact('data_alker','jumlah_hadir','data_team','inbox_xcheck_material_tl','absen_approval','group_telegram','active_team', 'get_kesehatan', 'session'));
		}
	}

	public function absensi_upload(){
		
		$auth = session('auth');
		$date = date('d-m-Y');

		return view('home.absensi',compact('auth','date'));
	}

	public function absensi_upload_save(Request $req){
		$auth = session('auth')->id_karyawan;
		$date = date('Y-m-d%');
		$input = $req->only([
		'status_absen'
		]);
		$rules = array(
		'status_absen' => 'required'
		);
		$messages =
		[
			'status_absen.required'			=>	'Pilih Status Absen !',
			'status_kesehatan.required'		=>	'Pilih Status Kesehatan !',
			'photo_absen_Hadir.required'	=>	'Upload Foto Absen Hadir !',
			'photo_absen_Pulang.required'	=>	'Upload Foto Absen Pulang !'
		];

		$validator = Validator::make($req->all(), $rules, $messages);
		$s = $req->input('status_absen');

		if ( $s=='HADIR' ) {
			$validator->sometimes('status_kesehatan', 'required', function ($input) {
				return true;
			});
			$validator->sometimes('photo_absen_Hadir', 'required', function ($input) {
				return true;
			});
		}

		if ( $s=='PULANG' ) {
			$validator->sometimes('status_kesehatan', 'required', function ($input) {
				return true;
			});
			$validator->sometimes('photo_absen_Pulang', 'required', function ($input) {
				return true;
			});
		}

		$cekData = DB::table('absen')->where('nik',$auth)->where('status',$req->status_absen)->where('tglAbsen','LIKE',$date)->first();
		if(count($cekData)==1 || $req->photo_absen_Hadir <> '' || $req->photo_absen_Pulang <> '' ) {
			$id = HomeModel::simpanUpload($req);
			$photo = ['absen_Hadir','absen_Pulang'];
			$this->handleFileUploadAbsensi($req, $auth, $id, $photo);
		} else {
			return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Mengupdate Absensi :(']]);
		};
		return back()->with('alerts',[['type' => 'success', 'text' => 'Sukses Mengupdate Absensi']]);
	}

	public static function absen(Request $req)
	{
		$status = $req->input('status');
		$nik    = $req->input('nik');
		$nama   = $req->input('nama');

		if ($req->hasFile('evidence_absen') != false)
		{
			$name = date('Ymd');
			$path = public_path().'/upload4/absensi/'.$nik.'/';
			if (!file_exists($path))
			{
				if (!mkdir($path, 0777, true))
				return 'gagal menyiapkan folder foto absensi';
			}
			$file = $req->file('evidence_absen');
			$ext = 'jpg';
			$filemime = @exif_read_data($file);
			
			if ($filemime == false)
			{
				return back()->with('alerts', [
					['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
				]);
			} 

			if (@array_key_exists("Software", $filemime))
			{
				if (!in_array(@$filemime['Software'] ,["Timestamp Camera","Timestamp Camera ENT","Timestamp Camera Enterprise"]))
				{
					return back()->with('alerts', [
						['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
					]);
				} else {
					try {
						$moved = $file->move("$path", "$name.$ext");
		
						$img = new \Imagick($moved->getRealPath());
		
						$img->scaleImage(100, 150, true);
						$img->writeImage("$path/$name-th.$ext");
					}
					catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
						return 'gagal menyimpan foto absensi '.$name;
					}
				}

				$name = date('Ymd');
				$path = public_path().'/upload4/absensi/'.$nik;
				$th = "$path/$name-th.jpg";

				if (file_exists(public_path().$th))
				{
					$path = "$path/$name";
				}

				if($path)
				{
					$img    = "$path/$name.jpg";
					$th     = "$path/$name-th.jpg";
				} else {
					$img    = null;
					$th     = null;
				}

				$caption = $nama." / ".$nik."\n\nABSEN ".date('Y-m-d H:i:s');

				// Telegram::sendPhoto([
				// 	'chat_id' => '-1001857868409',
				// 	'parse_mode' => 'html',
				// 	'caption' => $caption,
				// 	'photo' => $img
				// ]);
			} else {
				return back()->with('alerts', [
					['type' => 'danger', 'text' => 'Foto yang Dilaporkan Tidak Menggunakan TimeStamp!']
				]);
			}

		} else {
			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Foto Absen Belum Dilaporkan!']
			]);
		}

		if ($req->hasFile('PeduliInfra') != false)
		{
			$name = 'PeduliInfra_'.date('Ymd');
			$path = public_path().'/upload4/peduli_infra/'.$nik.'/';
			if (!file_exists($path))
			{
				if (!mkdir($path, 0777, true))
				return 'gagal menyiapkan folder foto peduli_infra';
			}
			$file = $req->file('PeduliInfra');
			$ext = 'jpg';
			
			try {
				$moved = $file->move("$path", "$name.$ext");

				$img = new \Imagick($moved->getRealPath());

				$img->scaleImage(100, 150, true);
				$img->writeImage("$path/$name-th.$ext");
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan foto peduli_infra '.$name;
			}

			$name = 'PeduliInfra_'.date('Ymd');
			$path = public_path().'/upload4/peduli_infra/'.$nik;
			$th = "$path/$name-th.jpg";

			if (file_exists(public_path().$th))
			{
				$path = "$path/$name";
			}

			if($path)
			{
				$img    = "$path/$name.jpg";
				$th     = "$path/$name-th.jpg";
			} else {
				$img    = null;
				$th     = null;
			}

			$caption = $nama." / ".$nik."\n\nPEDULI INFRA ".date('Y-m-d H:i:s');

			// Telegram::sendPhoto([
			// 	'chat_id' => '-1001857868409',
			// 	'parse_mode' => 'html',
			// 	'caption' => $caption,
			// 	'photo' => $img
			// ]);
		}

		$status_timx = DB::select('SELECT b.kat, gt.ket_posisi FROM regu a LEFT JOIN group_telegram gt ON a.mainsector = gt.chat_id LEFT JOIN mitra_amija b ON a.mitra = b.mitra_amija WHERE (a.nik1 = "'.$nik.'" OR a.nik2 = "'.$nik.'") AND a.ACTIVE = 1 ORDER BY a.date_created DESC LIMIT 1');
		
		if(count($status_timx) > 0 )
		{
			$status_tim = $status_timx[0];
			// if ($status_tim->ket_posisi == "PROV")
			// {
			// 	$absenComparin = self::absenComparin($nik);
			// } else {
			// 	$absenComparin = null;
			// }

			$absen = HomeModel::absen($nik, $status);

			// if ($absenComparin <> null)
			// {
			// 	if ($absenComparin->status == "OK")
			// 	{
			// 		return redirect('/home')->with('alerts', [
			// 			['type' => 'info', 'text' => 'NIK Anda Terdaftar pada Data Comparin'],
			// 			['type' => 'success', 'text' => $absenComparin->message]
			// 		]);
			// 	} else {
			// 		return redirect('/home')->with('alerts', [
			// 			['type' => $absen['status'], 'text' => $absen['message']]
			// 		]);
			// 	}
			// } else {
				return redirect('/home')->with('alerts', [
					['type' => 'success', 'text' => 'Anda berhasil absen']
				]);
			// }
		} else {
			$absen = HomeModel::absen($nik, $status);

			return redirect('/home')->with('alerts', [
				['type' => $absen['status'], 'text' => $absen['message']]
			]);
		}
	}

	public static function absenComparin($nik)
	{	
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://10.128.16.65/comparin/controller/api/tomman_absen.php',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => 'tech_id='.$nik.'&token=c0815b4a7cd87885df4f0548939e9e6e5a8962a7844f1dcb7b3f85888fc4db75',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/x-www-form-urlencoded'
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$result = json_decode($response);

		//insert log
		$check = DB::table('absen_comparin_log')->where('nik', $nik)->where('tgl_absen','like','"%'.date('Y-m-d').'%"')->first();
			if (count($check)>0) {

				DB::transaction(function() use($check) {

					DB::table('absen_comparin_log')->where('id_absen', $check->id_absen)->update([
						'tgl_absen'		=> date('Y-m-d H:i:s')
					]);

				});

			} else {

				DB::transaction(function() use($nik) {

					DB::table('absen_comparin_log')->insert([
						'nik'			=> $nik,
						'tgl_absen'		=> date('Y-m-d H:i:s')
					]);

				});

			}

		return $result;
	}

	public function absenBesok(){
		
		$nik = session('auth')->id_user;
		$esok    = mktime(date("H",strtotime("08")),0,0,date("m"),date("d")+1,date("Y"));
		$tglEsok = date('Y-m-d H:i:s',$esok);

		$absen = HomeModel::simpanAbsenBesok($nik,$tglEsok);
		return redirect()->back()->with('alerts', [
			['type' => $absen['status'], 'text' => $absen['message']]
		]);
	}

	public function absenManualProv($nik){
        
        $tglAbsen  = mktime(date("H",strtotime("08")),0,0,date("m"),date("d"),date("Y"));
        DB::table('absen')->insert([
            'nik'            => $nik,
            'date_created'    => date('Y-m-d H:i:s',$tglAbsen),
            'status'        => 'HADIR',
            'approval'        => '1',
            'date_approval'    => date('Y-m-d H:i:s',$tglAbsen),
            'tglAbsen'         => date('Y-m-d H:i:s',$tglAbsen),
            'absenEsok'        => '2'
        ]);

        echo 'sukses absen';
    }

    public function approveMassal(Request $req)
	{	
    	$getData 	= $req->approveMassal;
        $tglAbsen  	= date('Y-m-d');
        $auth 		= session('auth');

    	if (count($getData) == 0)
		{
    		return back()->with('alerts',[['type' => 'danger', 'text' => 'Nik Belum Dichecklist']]);
    	};

    	foreach($getData as $num => $data)
		{
			$kesehatan = $req->input('kesehatan');
    		
			DB::table('absen')->where('nik', $data)->whereDate('date_created',$tglAbsen)->update([
    			'approval' => 1,
    			'date_approval' => DB::raw('now()'),
				'kesehatan' => $kesehatan[$data],
    			'approve_by' => $auth->id_user
    		]);

			++$num;
    	}

    	return back()->with('alerts',[['type' => 'success', 'text' => 'Approve Massal Sukses']]);
    }

	public function uploadFile()
	{
		$event = 'Sertifikat_Vaksin';
		$check_data = HomeModel::uploadEvent($event);

		if (count($check_data)>0)
		{
			return back()->with('alerts',[['type' => 'success', 'text' => 'Rekan Terbaca Sudah Meng-upload Photo Event']]);
		}

		return view('home.uploadFile');
	}

	public function uploadFileSave(Request $request)
	{
		

		// dd($request->input(),$request->file(),session('auth'));
		$this->validate($request,[
			'photo_hasil_screenshot'			=>	'required'
		],[
			'photo_hasil_screenshot.required'	=>	'Upload Foto !'
		]);

		DB::table('log_uploadFile')->insert([
			'keterangan'			=> $request->input('keterangan'),
			'created_by'			=> session('auth')->id_karyawan
		]);

		$this->handleFileUpload($request, "photo_hasil_screenshot");

		$caption = 'Sertifikat Vaksin '.session('auth')->id_karyawan.' '.session('auth')->nama.' '.date('d M Y H:i:s');
		$table = DB::table('hdd')->get()->first();
		$upload = $table->public;
		$file = public_path()."/".$upload."/profile/".session('auth')->id_karyawan."/photo_hasil_screenshot.jpg";
		if (file_exists($file)){
			Telegram::sendPhoto([
			'chat_id' => '-485726620',
			'caption' => $caption,
			'photo' => $file
			]);
		}

		return redirect('/home')->with('alerts',[['type' => 'success', 'text' => '<strong>BERHASIL</strong> Upload Foto']]);
	}

	private function handleFileUpload($request, $input)
	{
		if ($request->hasFile($input))
		{
			$table = DB::table('hdd')->get()->first();
			$upload = $table->public;
			$path = public_path().'/'.$upload.'/profile/'.session('auth')->id_karyawan.'/';
			if (!file_exists($path)) {
			if (!mkdir($path, 0770, true))
				return 'Gagal Menyiapkan Folder Foto';
			}
			$file = $request->file($input);
			// $ext = $file->guessExtension();
			$ext = 'jpg';
			//TODO: path, move, resize
			try {
			$moved = $file->move("$path", "$input.$ext");

			$img = new \Imagick($moved->getRealPath());
			$img->scaleImage(100, 150, true);
			$img->writeImage("$path/$input-th.$ext");
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
			return 'Gagal Menyimpan Foto '.$input;
			}
		}
	}

}
