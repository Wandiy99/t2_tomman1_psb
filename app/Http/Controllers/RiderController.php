<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\HomeModel;
use App\DA\briefingModel;
use Telegram;
use DB;

class RiderController extends Controller
{
  public function dashboard(){
    $tgl = date('d M Y');
    return view('rider.dashboard',compact('tgl'));
  }
  public function input($nik){
    $data = DB::SELECT('SELECT * FROM 1_2_employee WHERE nik = "'.$nik.'"')[0];
    return view('rider.input',compact('data','nik'));
  }
  public function formSearch(){
  	return view('rider.search');
  }
  public function search(){
  	
  }
  public function save(Request $req){
  	$NIK = $req->input('NIK');
  	$tgl = $req->input('tgl');
  	$waktuMulai = $req->input('waktuMulai');
  	$waktuSelesai = $req->input('waktuSelesai');
  	$assessor = $req->input('assessor');
  	$nilaiMaterial = $req->input('nilaiMaterial');
  	$ketMaterialDropcore = $req->input('ketMaterialDropcore');
  	$ketMaterialSOC = $req->input('ketMaterialSOC');
  	$ketMaterialSClamp = $req->input('ketMaterialSClamp');
  	$ketOneClickCleaner = $req->input('ketOneClickCleaner');
  	$ketKabelTies = $req->input('ketKabelTies');
  	$nilaiPerlengkapan = $req->input('nilaiPerlengkapan');
  	$ketLakban = $req->input('ketLakban');
  	$ketTangPotong = $req->input('ketTangPotong');
  	$ketGuntingCutter = $req->input('ketGuntingCutter');
  	$ketTanggaTelescopic = $req->input('ketTanggaTelescopic');
  	$ketOPM = $req->input('ketOPM');
  	$ketOLS = $req->input('ketOLS');
  	$ketVFL = $req->input('ketVFL');
  	$ketSafetyHelmet = $req->input('ketSafetyHelmet');
  	$ketSafetyGlove = $req->input('ketSafetyGlove');
  	$ketSafetyBelt = $req->input('ketSafetyBelt');
  	$nilaiSklem = $req->input('nilaiSklem');
  	$ketSklem = $req->input('ketSklem');
  	$nilaiOTP = $req->input('nilaiOTP');
  	$ketOTP = $req->input('ketOTP');
  	$nilaiODP = $req->input('nilaiODP');
  	$ketODP = $req->input('ketODP');
  	$nilaiPotonganDropcore = $req->input('nilaiPotonganDropcore');
  	$ketPotonganDropcre = $req->input('ketPotonganDropcre');
  	$nilaiPemakaianAPD = $req->input('nilaiPemakaianAPD');
  	$ketPemakaianAPD = $req->input('ketPemakaianAPD');
  	$nilaiKebersihanPeralatan = $req->input('nilaiKebersihanPeralatan');
  	$ketKebersihanPeralatan = $req->input('ketKebersihanPeralatan');
  	$nilaiSalam = $req->input('nilaiSalam');
  	$ketSalam = $req->input('ketSalam');
  	$nilaiMemperkenalkanDiri = $req->input('nilaiMemperkenalkanDiri');
  	$ketMemperkenalkanDiri = $req->input('ketMemperkenalkanDiri');
  	$nilaiMaksuddanTujuan = $req->input('nilaiMaksuddanTujuan');
  	$ketMaksuddanTujuan = $req->input('ketMaksuddanTujuan');
  	$nilaiSeragamIndihome = $req->input('nilaiSeragamIndihome');
  	$ketSeragamIndihome = $req->input('ketSeragamIndihome');
  	$nilaiTips = $req->input('nilaiTips');
  	$ketTips = $req->input('ketTips');
  	$nilaiRambut = $req->input('nilaiRambut');
  	$ketRambut = $req->input('ketRambut');
  	$nilaiPengetahuanProduk = $req->input('nilaiPengetahuanProduk');
  	$ketPengetahuanProduk = $req->input('ketPengetahuanProduk');
  	$user_created = session('auth')->id_karyawan;
  	$check = DB::table('rider_brevet_indihome')->where('NIK',$NIK)->where('tgl',$tgl)->get();
  	if (count($check)>0){
  		$update = DB::table('rider_brevet_indihome')->where('NIK',$NIK)->where('tgl',$tgl)->update([
  			"NIK" => $NIK,
  			"tgl" => $tgl,
  			"waktuMulai" => $waktuMulai,
  			"waktuSelesai" => $waktuSelesai,
  			"assessor" => $assessor,
  			"nilaiMaterial" => $nilaiMaterial,
  			"ketMaterialDropcore" => $ketMaterialDropcore,
  			"ketMaterialSOC" => $ketMaterialSOC,
  			"ketMaterialSClamp" => $ketMaterialSClamp,
  			"ketOneClickCleaner" => $ketOneClickCleaner,
  			"ketKabelTies" => $ketKabelTies,
  			"nilaiPerlengkapan" => $nilaiPerlengkapan,
  			"ketLakban" => $ketLakban,
  			"ketTangPotong" => $ketTangPotong,
  			"ketGuntingCutter" => $ketGuntingCutter,
  			"ketTanggaTelescopic" => $ketTanggaTelescopic,
  			"ketOPM" => $ketOPM,
  			"ketOLS" => $ketOLS,
  			"ketVFL" => $ketVFL,
  			"ketSafetyHelmet" => $ketSafetyHelmet,
  			"ketSafetyGlove" => $ketSafetyGlove,
  			"ketSafetyBelt" => $ketSafetyBelt,
  			"nilaiSklem" => $nilaiSklem,
  			"ketSklem" => $ketSklem,
  			"nilaiOTP" => $nilaiOTP,
  			"ketOTP" => $ketOTP,
  			"nilaiODP" => $nilaiODP,
  			"ketODP" => $ketODP,
  			"nilaiPotonganDropcore" => $nilaiPotonganDropcore,
  			"ketPotonganDropcre" => $ketPotonganDropcre,
  			"nilaiPemakaianAPD" => $nilaiPemakaianAPD,
  			"ketPemakaianAPD" => $ketPemakaianAPD,
  			"nilaiKebersihanPeralatan" => $nilaiKebersihanPeralatan,
  			"ketKebersihanPeralatan" => $ketKebersihanPeralatan,
  			"nilaiSalam" => $nilaiSalam,
  			"ketSalam" => $ketSalam,
  			"nilaiMemperkenalkanDiri" => $nilaiMemperkenalkanDiri,
  			"ketMemperkenalkanDiri" => $ketMemperkenalkanDiri,
  			"nilaiMaksuddanTujuan" => $nilaiMaksuddanTujuan,
  			"ketMaksuddanTujuan" => $ketMaksuddanTujuan,
  			"nilaiSeragamIndihome" => $nilaiSeragamIndihome,
  			"ketSeragamIndihome" => $ketSeragamIndihome,
  			"nilaiTips" => $nilaiTips,
  			"ketTips" => $ketTips,
  			"nilaiRambut" => $nilaiRambut,
  			"ketRambut" => $ketRambut,
  			"nilaiPengetahuanProduk" => $nilaiPengetahuanProduk,
  			"ketPengetahuanProduk" => $ketPengetahuanProduk,
  			"user_created" => $user_created
  		]);
  	} else {
  		$update = DB::table('rider_brevet_indihome')->insert([
  			"NIK" => $NIK,
  			"tgl" => $tgl,
  			"waktuMulai" => $waktuMulai,
  			"waktuSelesai" => $waktuSelesai,
  			"assessor" => $assessor,
  			"nilaiMaterial" => $nilaiMaterial,
  			"ketMaterialDropcore" => $ketMaterialDropcore,
  			"ketMaterialSOC" => $ketMaterialSOC,
  			"ketMaterialSClamp" => $ketMaterialSClamp,
  			"ketOneClickCleaner" => $ketOneClickCleaner,
  			"ketKabelTies" => $ketKabelTies,
  			"nilaiPerlengkapan" => $nilaiPerlengkapan,
  			"ketLakban" => $ketLakban,
  			"ketTangPotong" => $ketTangPotong,
  			"ketGuntingCutter" => $ketGuntingCutter,
  			"ketTanggaTelescopic" => $ketTanggaTelescopic,
  			"ketOPM" => $ketOPM,
  			"ketOLS" => $ketOLS,
  			"ketVFL" => $ketVFL,
  			"ketSafetyHelmet" => $ketSafetyHelmet,
  			"ketSafetyGlove" => $ketSafetyGlove,
  			"ketSafetyBelt" => $ketSafetyBelt,
  			"nilaiSklem" => $nilaiSklem,
  			"ketSklem" => $ketSklem,
  			"nilaiOTP" => $nilaiOTP,
  			"ketOTP" => $ketOTP,
  			"nilaiODP" => $nilaiODP,
  			"ketODP" => $ketODP,
  			"nilaiPotonganDropcore" => $nilaiPotonganDropcore,
  			"ketPotonganDropcre" => $ketPotonganDropcre,
  			"nilaiPemakaianAPD" => $nilaiPemakaianAPD,
  			"ketPemakaianAPD" => $ketPemakaianAPD,
  			"nilaiKebersihanPeralatan" => $nilaiKebersihanPeralatan,
  			"ketKebersihanPeralatan" => $ketKebersihanPeralatan,
  			"nilaiSalam" => $nilaiSalam,
  			"ketSalam" => $ketSalam,
  			"nilaiMemperkenalkanDiri" => $nilaiMemperkenalkanDiri,
  			"ketMemperkenalkanDiri" => $ketMemperkenalkanDiri,
  			"nilaiMaksuddanTujuan" => $nilaiMaksuddanTujuan,
  			"ketMaksuddanTujuan" => $ketMaksuddanTujuan,
  			"nilaiSeragamIndihome" => $nilaiSeragamIndihome,
  			"ketSeragamIndihome" => $ketSeragamIndihome,
  			"nilaiTips" => $nilaiTips,
  			"ketTips" => $ketTips,
  			"nilaiRambut" => $nilaiRambut,
  			"ketRambut" => $ketRambut,
  			"nilaiPengetahuanProduk" => $nilaiPengetahuanProduk,
  			"ketPengetahuanProduk" => $ketPengetahuanProduk,
  			"user_created" => $user_created
  		]);
  	}
  	return redirect('/dashboardRider')->with('alerts',[['type'=>'success', 'text'=>'Success Update Input Data Brevet']]);
 
  }
}
