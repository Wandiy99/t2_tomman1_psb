<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
class CocController extends Controller
{
	public function inbox($tglStart,$tglEnd){
		$query = DB::SELECT('SELECT * FROM Video_Role_Play WHERE TGL BETWEEN "'.$tglStart.'" AND "'.$tglEnd.'" ');
		return view('coc.inbox',compact('tglStart','tglEnd','query'));
	}
	public function input(){
		return view('coc.input');
	}
	public function save(Request $req){
		$NIK1 = $req->input('nik1');
		$TGL  = $req->input('tgl');
		$NOTE = $req->input('note');
		$LINK = $req->input('linkyoutube');

		$save = DB::table('Video_Role_Play')->insertGetId([
			"NIK1" 	=> $NIK1,
			"TGL"	=> $TGL,
			"NOTE"	=> $NOTE,
			"LINK" 	=> $LINK,
			"USER_CREATED" => session('auth')->id_karyawan
		]);
		if ($save<>0){
			return redirect('/coc/'.date('Y-m-01').'/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);
		} else {
			return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Gagal Disimpan']]);
		}
	}
}