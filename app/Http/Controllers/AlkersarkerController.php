<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\HomeModel;
use App\DA\AlkersarkerModel;
class AlkersarkerController extends Controller
{
  public function input(){
    return view('alkersarker.input');
  }
  public function QC(){
    $nik = session('auth')->id_user;
    $group_telegram = HomeModel::group_telegram($nik);
    $get_alkersarkerx = AlkersarkerModel::get_alkersarker();
    $get_alkersarker_employee = AlkersarkerModel::get_alkersarker_employee($nik);
    $result = array();
    foreach ($get_alkersarker_employee as $alkersarker_employee) {
      $get_alkersarker = DB::SELECT('SELECT * FROM alkersarker a LEFT JOIN alkersarker_employee b ON a.alkersarker_id = b.alkersarker_id WHERE b.nik = "'.$alkersarker_employee->nik.'" ');
      $result[$alkersarker_employee->nik]['nik'] = $alkersarker_employee->nik;
      foreach ($get_alkersarker as $alkersarker){
        $result[$alkersarker_employee->nik][$alkersarker->alkersarker_id] = $alkersarker->qty;
      }

    }
    return view('alkersarker.QC',compact('group_telegram','get_alkersarkerx','get_alkersarker_employee','result'));
  }
  public function save(Request $request){
    $nik = session('auth')->id_user;
    $alkersarker = $request->input('alkersarker');
    $save = AlkersarkerModel::save($nik,$alkersarker);
    if ($save){
      return redirect('/home')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES mengupdate Alker Sarker</strong>']
      ]);
    } else {
      return redirect('/home')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL mengupdate Alker Sarker</strong>']
      ]);
    }
  }
}
?>
