<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class McoreController extends Controller
{
    public function home(){
        $date = date('Y-m-d');
        $query = "";
        return view('mcore.home',compact('date','query'));
    }

    public function check(Request $req){
        $date = date('Y-m-d');
        $id = $req->input('search');
        if ($id==""){
            return back()->with('alerts',[['type' => 'danger', 'text' => 'EMPTY SEARCH']]);
            $query = "";
        } else {
            $contain = explode('/',$id);
            $query = DB::SELECT('SELECT * FROM mcore_lite WHERE ODC = "'.$contain[0].'" AND KABEL = "'.$contain[1].'"');
        }
        return view('mcore.home',compact('date','query','id'));
    }
}