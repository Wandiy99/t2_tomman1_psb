<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\DA\marina\Saldo;
use App\DA\marina\User;
use Telegram;

date_default_timezone_set('Asia/Makassar');

class SaldoController extends Controller
{
    public function index()
    {
        // dd(session('auth'));
        $outstanding = Saldo::getListOutstanding();
        return view('marina.saldo.index', compact('outstanding'));
    }
    public function form()
    {
        $nik = session('auth')->id_user;
        $idregu = DB::table('regu')->where('nik1', $nik)->orWhere('nik2', $nik)->first();
        $regu = DB::table('regu')->select('*', 'uraian as text')->get();
        return view('marina.saldo.form', compact('regu', 'idregu'));
    }
    public function searchandsave(Request $req)
    {
        if($req->status == 1){
            $data = Saldo::getKeluar($req);
        }else{
            $data = Saldo::getKembali($req);
        }
        if(count($data)){
            //cek exist
            if(!Saldo::cekRfc($data[0]->no_rfc)){
                $regu = User::getReguById($req->regu);
                $insert=[];
                foreach($data as $d){
                    $insert[] = array("id_pengeluaran"=>$d->alista_id,"rfc"=>$d->no_rfc,"regu_id"=>$regu->id_regu,"regu_name"=>$regu->uraian,"nik1"=>$regu->nik1,"nik2"=>$regu->nik2,"niktl"=>$regu->TL,"value"=>$d->jumlah,"id_item"=>$d->id_barang,"action"=>1, "created_at"=>DB::raw('now()'), "created_by"=>session('auth')->id_karyawan, "bantu"=>$d->no_rfc."#".$d->id_barang);
                }
                Saldo::insert($insert);
                return redirect()->back()->with('alerts', [
                  ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menyimpan data!']
                ]);
            }else{
                return redirect()->back()->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong> RFC sudah ada di saldo teknisi!']
                ]);
            }
        }else{
            exec('cd ..;php artisan grabAlistaByRfc '.$req->rfc);
            $data = Saldo::getKeluar($req);
            if(count($data)){
                //cek exist
                if(!Saldo::cekRfc($data[0]->no_rfc)){
                    $regu = User::getReguById($req->regu);
                    $insert=[];
                    foreach($data as $d){
                        $insert[] = array("id_pengeluaran"=>$d->alista_id,"rfc"=>$d->no_rfc,"regu_id"=>$regu->id_regu,"regu_name"=>$regu->uraian,"nik1"=>$regu->nik1,"nik2"=>$regu->nik2,"niktl"=>$regu->TL,"value"=>$d->jumlah,"id_item"=>$d->id_barang,"action"=>1, "created_at"=>DB::raw('now()'), "created_by"=>session('auth')->id_karyawan, "bantu"=>$d->no_rfc."#".$d->id_barang);
                    }
                    Saldo::insert($insert);
                    return redirect()->back()->with('alerts', [
                      ['type' => 'success', 'text' => '<strong>Berhasil</strong> Menyimpan data!']
                    ]);
                }else{
                    return redirect()->back()->with('alerts', [
                      ['type' => 'danger', 'text' => '<strong>GAGAL</strong> RFC sudah ada di saldo teknisi!']
                    ]);
                }
            }else{
                return redirect()->back()->with('alerts', [
                  ['type' => 'danger', 'text' => '<strong>GAGAL</strong>, Tidak ada data ini2!']
                ]);
            }
        }
        return redirect()->back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong>, Tidak ada data ini!']
        ]);
    }


    public function matrik()
    {
        $data = Saldo::getListMatrik();
        // dd($data);
        return view('marina.saldo.listoutstanding', compact('data'));
    }
    public function outstanding()
    {
        $data = Saldo::getListOutstanding();
        dd($data);
    }
    public function listbyrfc($rfc)
    {
        $data = Saldo::listbyrfc(str_replace('*','/',$rfc));
        return view('marina.saldo.modallist', compact('data'));
    }
    public function getMaterial4laporan($rfc)
    {
        $data = Saldo::listbyrfc(str_replace('*','/',$rfc));
        return view('marina.saldo.modallist', compact('data'));
    }
    
    //grab
    public static function grabAlistaByRfc($rfc){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      $data = DB::table('akun')->where('id', '8')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
      $result = curl_exec($ch);
      //dd($result);
      echo"logged in \n";
      $result = array();


      // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.$rfc.'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1');

      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.urlencode($rfc).'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1&r=gudang%2Fhistorypengeluaranproject');
      $als = curl_exec($ch);
      // dd($als);
      if(!strpos($als, 'No results found.')){
        $columns = array(
          'alista_id',
          'project',
          'nama_gudang',
          'tgl',
          'requester',
          'pengambil',
          'no_rfc',
          'nik_pemakai',
          'mitra',
          'id_permintaan'
        );
        $cals = str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
        $als = str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
        $dom = @\DOMDocument::loadHTML(trim($als));
        // dd($als);
        echo"convert string to variable \n";
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        for ($i = 2, $count = $rows->length; $i < $count; $i++)
        {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
        }
      }
      // dd($result);
      $material = array();
      foreach($result as $noooooo => $r){
        echo "\nambil data ".$noooooo;
        curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);

        $mtr = curl_exec($ch);
        $columnsx= array(2=>"id_barang", "nama_barang", "jumlah");
        $dom = @\DOMDocument::loadHTML(trim($mtr));
        $table = $dom->getElementsByTagName('table')->item(1);
        $rows = $table->getElementsByTagName('tr');
        
        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
          $cells = $rows->item($i)->getElementsByTagName('td');
          for ($j = 2, $jcount = 4; $j <= $jcount; $j++)
          {
            $td = $cells->item($j);
            if($j==4){
              $data[$columnsx[$j]] =  explode(' ', $td->nodeValue)[0];
            }else{
              $data[$columnsx[$j]] =  $td->nodeValue;
            }
          }
          $r['id_item_bantu'] = $data['id_barang']."_".$r['no_rfc'];
          $material[] = array_merge($data,$r);
          // dd($material);
        }
      }
      // dd($material);
      DB::table('alista_material_keluar')->where('no_rfc', $rfc)->delete();
      DB::table('alista_material_keluar')->insert($material);
  }

  public static function inventoryOutMaterial()
  {    
    $startDate = "01-10-2021";
    $endDate = date('m-d-Y');

    $cookies = 'ci_session=a%3A6%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%22f37a5860ffe9498d5d33dd58e117dd33%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A12%3A%2236.75.66.221%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A115%3A%22Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F102.0.5005.63+Safari%2F537.36%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1654184400%3Bs%3A9%3A%22user_data%22%3Bs%3A0%3A%22%22%3Bs%3A9%3A%22logged_in%22%3Ba%3A5%3A%7Bs%3A12%3A%22is_logged_in%22%3Bi%3A1%3Bs%3A6%3A%22userid%22%3Bs%3A8%3A%2285190001%22%3Bs%3A8%3A%22username%22%3Bs%3A8%3A%2285190001%22%3Bs%3A10%3A%22profile_id%22%3Bi%3A1%3Bs%3A12%3A%22profile_name%22%3Bs%3A4%3A%22USER%22%3B%7D%7D2d5d1d0d78327820976edb271288650c';

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://dashboard.telkomakses.co.id/inventory/index.php/report/download_out_material/ALL/ALL/ALL/' . $startDate . '/' . $endDate . '/AREA/KALSEL',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: ' . $cookies . ''
      ),
    ));

    $response = str_replace('"','',curl_exec($curl));

    curl_close($curl);

    $result = [];
    foreach(explode("\n", $response) as $val)
    {
      $result[] = explode(',',$val);
    }
    unset($result[0]);
    // dd($result);

    if (count($result) > 0)
    {
      DB::transaction(function () use ($result, $startDate, $endDate) {
        $expStartDate = explode("-", $startDate);
        $expEndDate = explode("-", $endDate);
        $start = $expStartDate[2] . '-' . $expStartDate[0] . '-' . $expStartDate[1];
        $end = $expEndDate[2] . '-' . $expEndDate[0] . '-' . $expEndDate[1];
        // dd($startDate, $endDate, $start, $end);

        DB::table('inventory_material_log')->whereBetween('posting_datex', [$start, $end])->delete();

        $insert = [];
        foreach ($result as $data) {
          if ($data[0] != 0) {
            $data[] .= date('Y-m-d', strtotime(@$data[14]));
            // dd($data);

            $insert[] = [
              'no_reservasi' => $data[0],
              'no_rfc' => $data[1],
              'plant' => @$data[2],
              'wbs_element' => @$data[3],
              'description' => @$data[4],
              'type' => @$data[5],
              'program' => @$data[6],
              'regional' => @$data[7],
              'witel' => @$data[8],
              'mitra' => @$data[9],
              'requester' => @$data[10],
              'nama_requester' => @$data[11],
              'nik_pemakai' => @$data[12],
              'nama_pemakai' => @$data[13],
              'posting_date' => @$data[14],
              'material' => @$data[15],
              'satuan' => @$data[16],
              'quantity' => @$data[17],
              'posting_datex' => @$data[18]
            ];
          }
        }

        $total = count($insert);

        $array_chunk = array_chunk($insert, 500);

        foreach ($array_chunk as $numb => $chunk)
        {
          DB::table('inventory_material_log')->insert($chunk);

          print_r("saved page $numb and sleep (1)\n");

          sleep(1);
        }

        print_r("\nFinish Syncron Dashboard Inventory $start $end Total $total\n");

        //insert to data master
        sleep(10);
        DB::statement('DELETE FROM `inventory_material` WHERE DATE(posting_datex) BETWEEN "' . $start .'" AND "' . $end . '"');
        DB::statement('INSERT INTO `inventory_material` SELECT * FROM `inventory_material_log`');

        Telegram::sendMessage([
          'chat_id' => '-306306083',
          'parse_mode' => 'html',
          'text' => "Finish Syncron Dashboard Inventory $start $end <b>Total $total</b>"
        ]);
      });
    }
  }
}
