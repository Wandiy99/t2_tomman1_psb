<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Telegram;
use App\DA\ApiModel;
use App\DA\BotModel;
use Illuminate\Support\Facades\Session;

date_default_timezone_set('Asia/Makassar');


class LaporOdpBotController extends Controller
{ 
    protected static $TOKEN      = "5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w";
    protected static $usernamebot = "@LaporPeduli_ODP_TR6_Bot";
    protected static $debug = true;

    public static function request_url($method)
    {
        return "https://api.telegram.org/bot" . self::$TOKEN . "/". $method;
    }
    
    // fungsi untuk meminta pesan 
    // bagian ebook di sesi Meminta Pesan, polling: getUpdates
    public static function get_updates($offset){
        $url = self::request_url("getUpdates")."?offset=".$offset;
            $resp = file_get_contents($url);
            $result = json_decode($resp, true);
            if ($result["ok"]==1)
                return $result["result"];
            return array();
    }


    // fungsi untuk mebalas pesan, 
    public static function send_reply($chatid, $msgid, $text){
        global $debug;
        $data = array(
            'chat_id' => $chatid,
            'text'  => $text
            //'reply_to_message_id' => $msgid   // <---- biar ada reply nya balasannya, opsional, bisa dihapus baris ini
        );
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options); 
        $result = file_get_contents(self::request_url('sendMessage'), false, $context);

        if ($debug) 
            print_r($result);
    }

    
    // fungsi mengolahan pesan, menyiapkan pesan untuk dikirimkan
    public static function create_response($text, $message)
    {
        // inisiasi variable hasil yang mana merupakan hasil olahan pesan
        $hasil = '';  

        $fromid = $message["from"]["id"]; // variable penampung id user
        $chatid = $message["chat"]["id"]; // variable penampung id chat
        $pesanid= $message['message_id']; // variable penampung id message


        // variable penampung username nya user
        isset($message["from"]["username"])
            ? $chatuser = $message["from"]["username"]
            : $chatuser = '';
        

        // variable penampung nama user

        isset($message["from"]["last_name"]) 
            ? $namakedua = $message["from"]["last_name"] 
            : $namakedua = '';   
        $namauser = $message["from"]["first_name"]. ' ' .$namakedua;

        // ini saya pergunakan untuk menghapus kelebihan pesan spasi yang dikirim ke bot.
        $textur = preg_replace('/\s\s+/', ' ', $text); 

        // memecah pesan dalam 2 blok array, kita ambil yang array pertama saja
        $command = explode(' ',$textur,2); //

        // identifikasi perintah (yakni kata pertama, atau array pertamanya)
        switch ($command[0]) {

            case '/daftar':
                $hasil = "Ok! Sekarang tuliskan nama lengkapmu!";
                session::get('chat_id', $chatid);
                session::get('step', 'nama');
                break;

            // balasan default jika pesan tidak di definisikan
            default:
                //if(isset(session::get('chatid'))){
                    if(session::get('step')){
                        switch(session::get('step')){
                            case 'nama':
                                //tampung dulu namanya ke SESSION
                                session::get('nama', $text);
                                $hasil = "Ok, {$text}. Kirimkan nomer telepon yang bisa dihubungi!";
                                session::put('chat_id', $chatid);
                                session::put('step', 'telepon');
                                break;
                            case 'telepon':
                                session::put('telepon', $text);
                                $hasil = "Terima kasih, sekarang kirimkan alamat emailmu!";
                                session::put('chat_id', $chatid);
                                session::put('step', 'email');
                                break;
                            case 'email':
                                session::put('email', $text);
                                $hasil = "Nama {session::get('nama')}, telp: {session::get('telepon')}, email: {session::get('email')}. Apakah sudah benar? (ya/tidak)";
                                session::put('chat_id', $chatid);
                                session::put('step', 'verifikasi');
                                break;
                            case 'verifikasi':
                                if($text == 'ya'){
                                    $hasil = "Terima kasih sudah bergabung di Klub Belajar Linux (KBL) Bumigora STMIK Bumigora Mataram. Silahkan bergabung ke grup https://telegram.me/kbl_bumigora";
                                    session::put('step', 'selesai');
                                } else {
                                    $hasil = "Silahkan ulangi dengan perintah /daftar";
                                    session_destroy();
                                }
                                
                                break;
                            case 'selesai':
                                // simpan ke database
                                
                                session_destroy();
                                break;
                            
                        }
                        
                        
                    } else {
                        $hasil = 'Hi, Saya Bot yang akan membantumu mendaftar KBL Bumigora.';
                    }
                //}
                break;
                
        }
        
        print_r(session()->all());
        return $hasil;
    }

    // fungsi pesan yang sekaligus mengupdate offset 
    // biar tidak berulang-ulang pesan yang di dapat 
    public static function process_message($message)
    {
        $updateid = $message["update_id"];
        $message_data = $message["message"];
        if (isset($message_data["text"])) {
        $chatid = $message_data["chat"]["id"];
            $message_id = $message_data["message_id"];
            $text = $message_data["text"];
            $response = self::create_response($text, $message_data);
            if (!empty($response))
            self::send_reply($chatid, $message_id, $response);
        }
        return $updateid;
    }
    
    // hapus baris dibawah ini, jika tidak dihapus berarti kamu kurang teliti!
    //die("Mohon diteliti ulang codingnya..\nERROR: Hapus baris atau beri komen line ini yak!\n");
    
    // hanya untuk metode poll
    // fungsi untuk meminta pesan 
    public static function process_one()
    {
        global $debug;
        $update_id  = 0;
        echo "-";
    
        if (file_exists("last_update_id")) 
            $update_id = (int)file_get_contents("last_update_id");
    
        $updates = self::get_updates($update_id);

        // jika debug=0 atau debug=false, pesan ini tidak akan dimunculkan
        if ((!empty($updates)) and (self::$debug) )  {
            echo "\r\n===== isi diterima \r\n";
            print_r($updates);
        }
    
        foreach ($updates as $message)
        {
            if (isset($message["message"]))
            {
                echo '+';
                $update_id = self::process_message($message);
            } elseif (isset($message["callback_query"]["data"]))
            {
                $message["message"] = $message["callback_query"]["data"];
                $update_id = self::process_message($message);
            }
        }
        
        // @TODO nanti ganti agar update_id disimpan ke database
        // update file id, biar pesan yang diterima tidak berulang
        file_put_contents("last_update_id", $update_id + 1);
    }
    public static function startBot()
    {
        // metode webhook
        // secara normal, hanya bisa digunakan secara bergantian dengan polling
        // aktifkan ini jika menggunakan metode webhook
        // $entityBody = file_get_contents('php://input');
        // $pesanditerima = json_decode($entityBody, true);
        // self::process_message($pesanditerima);
        while (true) {
            self::process_one();
            sleep(1);
        }
    }
}