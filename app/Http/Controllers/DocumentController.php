<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DA\DocumentModel;
use File;
date_default_timezone_set('Asia/Makassar');

class DocumentController extends Controller
{
    public function upload(){
        $get_documentUnit = DocumentModel::documentUnit();
        $get_documentType = DocumentModel::documentType();
        $get_documentExternal = DocumentModel::documentExternal();
        $get_documentInternal = DocumentModel::documentInternal();
        return view('document.upload_1',compact('get_documentUnit','get_documentType','get_documentInternal','get_documentExternal'));
    }

    public function uploadSave(Request $req){
        // param
        $auth   = session('auth');
        $input  = $req->input();

        // validasi
        if ($req->input("documentTitle")==""){
            return redirect()->back()->withInput($req->input())->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>"TITLE" tidak boleh kosong</strong>']
        ]);
        }
        if ($req->input("documentDate")==""){
            return redirect()->back()->withInput($req->input())->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>"DATE" tidak boleh kosong</strong>']
            ]);
        }
        if ($req->input("documentType")==""){
            return redirect()->back()->withInput($req->input())->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>"TYPE" tidak boleh kosong</strong>']
            ]);
        }
        if ($req->input("documentUnit")==""){
            return redirect()->back()->withInput($req->input())->with('alerts', [
                ['type' => 'danger', 'text' => '<strong>"UNIT" tidak boleh kosong</strong>']
            ]);
        }

        // store data
        $save = DocumentModel::uploadSave($req);
        return redirect()->back()->with('alerts', [
            ['type' => 'success', 'text' => '<strong>DATA berhasil disimpan</strong>']
        ]);

        // debug
        // print_r($input);
    }

    public function dashboard($dateStart,$dateEnd){
        $get_documentType = DocumentModel::documentType();
        $get_documentDashboardExternal = DocumentModel::documentDashboardExternal($dateStart,$dateEnd);
        $jml_documentType = count($get_documentType);
        return view('document.dashboard',compact('dateStart','dateEnd','get_documentDashboardExternal','get_documentType','jml_documentType'));
    }

    public function dashboardExternalList($dateStart,$dateEnd,$mitra,$type){
      $get_documentTypeDetail = DocumentModel::documentTypeDetail($type);
      $get_documentMitraDetail = DocumentModel::documentMitraDetail($mitra);
      $get_documentExternalList = DocumentModel::documentExternalList($dateStart,$dateEnd,$mitra,$type);
      return view('document.dashboardExternalList',compact('get_documentMitraDetail','get_documentTypeDetail','dateStart','dateEnd','mitra','type','get_documentExternalList'));
    }

    public function yours(){
        $NIK = session('auth')->id_karyawan;
        $get_job = DocumentModel::documentJob($NIK);
        $get_document = DocumentModel::documentGet($NIK);
        $get_documentInternalList = DocumentModel::documentInternalList($NIK);
        return view('document.yours',compact('get_job','get_document','get_documentInternalList'));
    }

    public function details($documentID){
      $files = File::allFiles('/mnt/hdd4/upload/employee_document/'.$documentID);
      $get_documentDetails = DocumentModel::documentDetails($documentID);
      $get_documentDetailsInternal = DocumentModel::documentDetailsInternal($documentID);
      $get_documentDetailsExternal = DocumentModel::documentDetailsExternal($documentID);
      return view('document.details',compact('documentID','get_documentDetails','get_documentDetailsInternal','get_documentDetailsExternal','files'));
    }

    public function jsonDocumentInternal(){
        $query = DocumentModel::jsonDocumentInternal();
        echo (json_encode($query));
        exit;
    }
}
