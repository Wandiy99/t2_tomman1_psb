<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\MigrasiModel;
use File;
use DB;

class MigrasiController extends Controller
{
  protected $fileMigration = [
    'KML', 'Excel'
  ];

  public function index($date)
  {
    $list = DB::select('
      SELECT m.*,ms.Sebab,ms.Ket_Sebab,ms.Due_Date_Solusi, id_regu AS id_r, (
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian
      FROM micc_wo m
      left join ms2n ms on m.ND = ms.ND
      left join dispatch_teknisi d on m.ND=d.Ndem
      WHERE
      (DATE(m.created_at) LIKE "'.$date.'%")
      order by m.created_at asc
    ');
    return view('migrasi.list', compact('list'));
  }

  public function searchform(){
    $data = "";
	  return view('migrasi.searchform',compact('data'));
  }

  public function search(Request $request){
    $id     = $request->input('Search');
    if ($id==""){ $list = NULL; } else {
      $list   = MigrasiModel::search($id);
    }
    $data   = "Trying to search '".$id."'";
    $jumlah = count($list);

    return view('migrasi.searchform',compact('data','list','jumlah'));
  }

  public function listCcan()
  {
    $list = DB::select('
      SELECT *, id_regu AS id_r, (
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian
      FROM micc_wo m
      left join dispatch_teknisi d on m.ND=d.Ndem
      WHERE loker="CCAN" order by m.id desc
    ');
    return view('migrasi.list-ccan', compact('list'));
  }
  public function save(Request $request)
  {
  	$wos = json_decode($request->input('wos'));
    $insertMigrasi = "insert ignore into micc_wo(
            ND,
            ND_Speedy,
            MDF,
            Nama,Alamat,ODP,OLT) values";
	foreach($wos as $no => $wo){
		$sparator = ", ";
      	if($no == 0){
        	$sparator = "";
      	}
    //$insertMigrasi .= $sparator."($wo->nd?:0,$wo->nd_speedy?:0,'$wo->mdf?:-','$wo->nama?:-','$wo->alamat?:-','$wo->odp?:-','$wo->olt?:0')";
    $mdf = substr($wo->mdf,3);
		$insertMigrasi .= $sparator."(".($wo->nd?:0).",".($wo->nd_speedy?:0).",'".($mdf?:'-')."',".DB::connection()->getPdo()->quote($wo->nama).",".DB::connection()->getPdo()->quote($wo->alamat).",'".($wo->odp?:'-')."','".($wo->olt?:0)."')";
	}
	DB::statement($insertMigrasi);
	return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Data']
    ]);
  }
  public function upload()
  {
    return view('migrasi.input');
  }

  public function dispatch($id){
    $data = DB::select('
      SELECT m.*,d.*,id_regu AS id_r, (
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian
      FROM micc_wo m left join dispatch_teknisi d on m.ND = d.Ndem
      WHERE m.ND = ?
    ',[
      $id
    ])[0];
    $regu = DB::select('
      SELECT id_regu as id,uraian as text, telp
      FROM regu 
      WHERE active="1"
    ');

    return view('migrasi.dispatch', compact('data','regu'));
  }
  public function dispatchSave(Request $request, $id){
    $auth = session('auth');
    $exists = DB::select('select * from dispatch_teknisi where Ndem = ? and dispatch_by = 1',[
      $id
    ]);
    if(count($exists)){
      $data = $exists[0];
      DB::table('dispatch_teknisi')
          ->where('id', $data->id)
          ->update([
            'updated_at'   => DB::raw('NOW()'),
            'updated_by'   => $auth->id_karyawan,
            'tgl'          => $request->input('tgl'),
            'id_regu'      => $request->input('id_regu'),
            'step_id'      => '1.0',
            'jenis_order'  => 'MIGRASI_CCAN'
          ]);

      DB::table('psb_laporan')
          ->where('id_tbl_mj','=',$data->id)
          ->update([
              'catatan'         => '',
              'status_laporan'  => '6'
          ]);

    }else{
        DB::table('dispatch_teknisi')->insert([
          'updated_at' => DB::raw('NOW()'),
          'updated_by' => $auth->id_karyawan,
          'tgl'          => $request->input('tgl'),
          'id_regu'    => $request->input('id_regu'),
          'Ndem'       => $id,
          'dispatch_by'=> 1,
          'jenis_order'  => 'MIGRASI_CCAN'
        ]);
    }
    
    return redirect('/migrasi'.$request->input('tgl'))->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch Migrasi']
      ]);
  }
  public function report(){
    $auth = session('auth');
    $list = DB::select('
      SELECT ms2n.*, id_regu AS id_r, mdf.area,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, Status_Indihome, status_laporan, ms2n_sebab_value, ms2n_sebab_pic, m.Kcontact as mKcontact,m.Nama as mNama,m.Alamat as mAlamat,m.ND as mND,m.ND_Speedy as mND_Speedy,
      case h.status_wo_by_hd
        when "1" then "UP"
        when "2" then "OGP"
        when "3" then "KENDALA"
        when "5" then "RESCHEDULE"
        else "BELUM SURVEY"
      end as status_wo_by_hd,
      dispatch_teknisi.id as id_dt,
      (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = psb_laporan.id) as jumlah_material

      FROM micc_wo m
        LEFT JOIN dispatch_teknisi ON m.ND = dispatch_teknisi.Ndem
        LEFT JOIN psb_laporan on dispatch_teknisi.id=psb_laporan.id_tbl_mj
        LEFT JOIN mdf on m.MDF = mdf.mdf
        LEFT JOIN ms2n on m.ND = ms2n.ND
        LEFT join ms2n_sebab on ms2n.Sebab = ms2n_sebab.ms2n_sebab
        LEFT JOIN ms2n_status_hd h on m.ND = h.ndem
      WHERE dispatch_teknisi.updated_at like "'.date("Y-m-d").'%" and id_regu is not null order by dispatch_teknisi.id_regu
    ');



    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();

    $lastArea = '';
    foreach($list as $row) {
      if($row->area != 'BJM'){
        if ($lastArea == $row->area) {
          $outer[count($outer)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy,'NDEM'=> $row->Ndem,'ID_DT'=> $row->id_dt,  'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->ND.' ~ '.$row->ND_Speedy.' : '.$row->mNama);
        }
        else {
          $outer[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy, 'NDEM'=> $row->Ndem, 'ID_DT'=> $row->id_dt, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mND_Speedy.' : '.$row->mNama)));
          $lastArea = $row->area;
        }
        $countOuter++;
      }
    }
//     $lastArea = '';
    $tim = array();
    $lastTim = "";
    foreach($list as $row) {
			if ($row->jumlah_material>0 && $row->status_wo_by_hd=='UP') {
		    $status_material = "MATERIAL OK";
	    } else {
		    $status_material = "";
	    }
      if($row->area == 'BJM'){
	      if ($lastTim == $row->uraian) {
					$tim[count($tim)-1]['WO'][] = array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt,'NDEM'=> $row->Ndem,  'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->mND.' ~ '.$row->mND_Speedy.' : '.$row->mNama);
	        $countInner++;
        }
        else {
        	$countInner++;
          $tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND_Speedy,'ID_DT'=> $row->id_dt,'NDEM'=> $row->Ndem, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mND_Speedy.' : '.$row->mNama)));
//        		$tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->mND_Speedy,'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'NDEM'=>$row->Ndem, 'detil' => $row->mND.' ~ '.$row->mND_Speedy.' : '.$row->mNama)));

		      $lastTim = $row->uraian;
		    }
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->uraian , $y->uraian);
    });

    if($auth->id_user =='wandiy99'){
      //return $list;
    }

    $kendala = array();
    $lastKendala = '';
    $bs = 0;
    $up = 0;
    $ogp = 0;
    $kdl = 0;
    foreach($list as $row) {
      if($row->ms2n_sebab_value == 'Kendala'){
        if ($lastKendala == $row->ms2n_sebab_pic) {
          $kendala[count($kendala)-1]['WO'][] = $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')';
        }
        else {
          $kendala[] = array('head' => $row->ms2n_sebab_pic, 'WO' => array( $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')'));
          $lastKendala = $row->ms2n_sebab_pic;
        }

      }
      if($row->status_wo_by_hd == 'KENDALA'){
        $kdl++;
      }if($row->status_wo_by_hd == 'BELUM SURVEY'){
        $bs++;
      }if($row->status_wo_by_hd == 'OGP'){
        $ogp++;
      }if($row->status_wo_by_hd == 'UP'){
        $up++;
      }
    }
    $counting = array('KENDALA' => $kdl, 'BELUM SURVEY' => $bs, 'OGP' => $ogp, 'UP' => $up, 'INNER' => $countInner, 'OUTER' => $countOuter, 'WO' => $countWo);
    //var_dump($outer);
    //return $kendala;
    return view('migrasi.report', compact('tim', 'outer', 'kendala', 'counting'));
  }
  public function reportCcan(){
    $auth = session('auth');
    $list = DB::select('
      SELECT ms2n.*, id_regu AS id_r, mdf.area,(
        SELECT uraian
        FROM regu
        WHERE id_regu = id_r
        ) AS uraian, Status_Indihome, status_laporan, ms2n_sebab_value, ms2n_sebab_pic, m.Kcontact as mKcontact,m.Nama as mNama,m.Alamat as mAlamat,m.ND as mND,m.ND_Speedy as mND_Speedy,
      case h.status_wo_by_hd
        when "1" then "UP"
        when "2" then "OGP"
        when "3" then "KENDALA"
        when "5" then "RESCHEDULE"
        else "BELUM SURVEY"
      end as status_wo_by_hd,
      dispatch_teknisi.id as id_dt,
      (select count(*) from psb_laporan_material plm where plm.psb_laporan_id = psb_laporan.id) as jumlah_material

      FROM micc_wo m
        LEFT JOIN dispatch_teknisi ON m.ND = dispatch_teknisi.Ndem
        LEFT JOIN psb_laporan on dispatch_teknisi.id=psb_laporan.id_tbl_mj
        LEFT JOIN mdf on m.MDF = mdf.mdf
        LEFT JOIN ms2n on m.ND = ms2n.ND
        LEFT join ms2n_sebab on ms2n.Sebab = ms2n_sebab.ms2n_sebab
        LEFT JOIN ms2n_status_hd h on m.ND = h.ndem
      WHERE dispatch_teknisi.updated_at like "'.date("Y-m-d").'%" and id_regu is not null order by dispatch_teknisi.id_regu
    ');



    $countWo = count($list);
    $countOuter = 0;
    $countInner = 0;
    $outer = array();

    $lastArea = '';
    foreach($list as $row) {
      if($row->area != 'BJM'){
        if ($lastArea == $row->area) {
          $outer[count($outer)-1]['WO'][] = array('ND_Speedy'=>$row->ND,'NDEM'=> $row->Ndem,'ID_DT'=> $row->id_dt,  'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->mND.' ~ '.$row->mNama);
        }
        else {
          $outer[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND, 'NDEM'=> $row->Ndem, 'ID_DT'=> $row->id_dt, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mNama)));
          $lastArea = $row->area;
        }
        $countOuter++;
      }
    }
//     $lastArea = '';
    $tim = array();
    $lastTim = "";
    foreach($list as $row) {
      if ($row->jumlah_material>0 && $row->status_wo_by_hd=='UP') {
        $status_material = "MATERIAL OK";
      } else {
        $status_material = "";
      }
      if($row->area == 'BJM'){
        if ($lastTim == $row->uraian) {
          $tim[count($tim)-1]['WO'][] = array('ND_Speedy'=>$row->ND,'ID_DT'=> $row->id_dt, 'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd ,'ND' => $row->mND, 'detil'=>$row->mND.' ~ '.$row->mNama);
          $countInner++;
        }
        else {
          $countInner++;
          $tim[] = array('head' => $row->uraian, 'WO' => array(array('ND_Speedy'=>$row->ND,'ID_DT'=> $row->id_dt,'MATERIAL' => $status_material, 'STATUS' => $row->status_wo_by_hd, 'ND'=>$row->mND, 'detil' => $row->mND.' ~ '.$row->mNama)));
          $lastTim = $row->uraian;
        }
      }
    }
    usort($list, function($x, $y) {
      return strcasecmp($x->uraian , $y->uraian);
    });

    if($auth->id_user =='wandiy99'){
      //return $outer;
    }

    $kendala = array();
    $lastKendala = '';
    $bs = 0;
    $up = 0;
    $ogp = 0;
    $kdl = 0;
    foreach($list as $row) {
      if($row->ms2n_sebab_value == 'Kendala'){
        if ($lastKendala == $row->ms2n_sebab_pic) {
          $kendala[count($kendala)-1]['WO'][] = $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')';
        }
        else {
          $kendala[] = array('head' => $row->ms2n_sebab_pic, 'WO' => array( $row->ND.' : '.$row->Nama.' ('.$row->Sebab.')'));
          $lastKendala = $row->ms2n_sebab_pic;
        }

      }
      if($row->status_wo_by_hd == 'KENDALA'){
        $kdl++;
      }if($row->status_wo_by_hd == 'BELUM SURVEY'){
        $bs++;
      }if($row->status_wo_by_hd == 'OGP'){
        $ogp++;
      }if($row->status_wo_by_hd == 'UP'){
        $up++;
      }
    }
    $counting = array('KENDALA' => $kdl, 'BELUM SURVEY' => $bs, 'OGP' => $ogp, 'UP' => $up, 'INNER' => $countInner, 'OUTER' => $countOuter, 'WO' => $countWo);
    //var_dump($outer);
    //return $kendala;
    return view('migrasi.report-ccan', compact('tim', 'outer', 'kendala', 'counting'));
  }
  public function updateStatus($id)
  {
    $data = DB::select('
      SELECT m.ND, Nama, h.status_wo_by_hd, h.id
      FROM micc_wo m
      LEFT JOIN ms2n_status_hd h on m.ND = h.ndem
      WHERE m.ND = ?
    ',[
      $id
    ])[0];
    return view('migrasi.status', compact('data'));
  }
  public function saveStatus(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM ms2n_status_hd
      WHERE ndem = ?
    ',[
      $id
    ]);
    if (count($exists)) {
      $data = $exists[0];
        DB::table('ms2n_status_hd')
          ->where('id', $data->id)
          ->update([
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'status_wo_by_hd' => $request->input('status')
          ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> update status']
      ]);
    }
    else {
        DB::table('ms2n_status_hd')->insert([
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'status_wo_by_hd' => $request->input('status'),
          'ndem'            => $id
        ]);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> insert status']
      ]);
    }
    return $exists;
  }
  public function saveCcan(Request $request)
  {
    $wos = json_decode($request->input('wos'));
    $insertMigrasi = "insert ignore into micc_wo(ND,MDF,
            Nama,Alamat,jenis_layanan,layanan,service,sid,pic,loker) values";
    foreach($wos as $no => $wo){
      $sparator = ", ";
      if($no == 0){
        $sparator = "";
      }
      $insertMigrasi .= $sparator."('".($wo->ao)."','".($wo->mdf?:'-')."',".DB::connection()->getPdo()->quote($wo->nama).",
        ".DB::connection()->getPdo()->quote($wo->alamat).",'".($wo->jenis_layanan?:'-')."','".($wo->layanan?:'-')."','".($wo->service?:'-')."',
        '".($wo->sid?:'-')."',".DB::connection()->getPdo()->quote($wo->pic).",'CCAN')";
    }
    DB::statement($insertMigrasi);
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Data']
    ]);
  }
  public function uploadCcan()
  {
    return view('migrasi.ccan');
  }

  public function migrasiMonitoring()
  {
      return view('migrasi.monMigrasi');
  }

  public function migrasiMonitoringLok1(MigrasiModel $migrasi)
  {
      $clusters = $migrasi->listCluster();
      return view('migrasi.monitoringMigrasi', compact('clusters'));
  }

  public function migrasiMonitoringForm()
  {
      @$fileMigration    = $this->fileMigration;
      
      return view('migrasi.formTambahCluster', compact('fileMigration'));
  }

  public function migrasiMonitoringSimpan(request $req, MigrasiModel $migrasi)
  {
      $this->validate($req,[
          'nmCluster'   => 'required'
      ]);

      @$fileMigration = $this->fileMigration;

      $migrasi->saveCluster($req);
      $this->handleFileUpload($req, $req->nmCluster, $fileMigration, 0);
      return redirect('/migrasi/monitoring')->with('alerts',[['type' => 'success', 'text' => 'Sukses Menambah Cluster']]);
  }

  private function handleFileUpload($req, $id, $files, $nama)
  {
    foreach($files as $name) {
      if ($req->hasFile($name)) {
          $file = $req->file($name);
          $path = public_path().'/upload4/cluster/'.$id.'/';
          
          if (!file_exists($path)) {
            if (!mkdir($path, 0770, true))
              return 'gagal menyiapkan folder file';
          }
        
        // $ext = $req->$name->extension();
        $ext = $req->$name->getClientOriginalExtension();
        $namaFile = "$name.$ext";
        if($nama){
          // var_dump($req->$name);
          // $ext = $req->$name->extension();
          $ext = $req->$name->getClientOriginalExtension();
          $namaFile = "$nama.$ext";
        }
        
        try {
          $moved = $file->move("$path", $namaFile);
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan file ';
        }
      }
    }
    
  }

  public function listProgress(MigrasiModel $migrasi)
  {   
      $listCluster  = $migrasi->listCluster();
      $listProgress = $migrasi->listProgress();
      return view('migrasi.listprogress',compact('listProgress', 'listCluster'));
  }

  public function formTambah(MigrasiModel $migrasi)
  { 
      $listCluster  = $migrasi->listCluster();
      $listRegu     = $migrasi->getRegu();
    
      return view('migrasi.formTambah', compact('listCluster', 'listRegu'));
  }

  public function formTambahSimpan(Request $req, MigrasiModel $migrasi)
  {
      $this->validate($req,[
          'nmCluster'   => 'required',
          'regu'        => 'required',
          'progress'    => 'required|numeric'
      ]);

      // simpan
      $cek = $migrasi->cekRegu($req->regu);
      if(!empty($cek)){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Regu Sudah Ada']]);
      }
      else{
          $migrasi->simpanProgress($req);
          return redirect('/migrasi/monitoring')->with('alerts',[['type' => 'success', 'text' => 'Sukses Menambah Progress']]); 
      }
  }

  public function editForm($idProgress, MigrasiModel $migrasi)
  {
      $dataProgress = $migrasi->getDataByIdProgress($idProgress);
      return view('migrasi.editForm',compact('dataProgress'));
  }

  public function editFormSimpan(Request $req, $idProgress, MigrasiModel $migrasi)
  {
      $this->validate($req,[
          'progress'  => 'required|numeric'
      ]);

      $migrasi->ubahProgress($req, $idProgress);
      return redirect('/migrasi/monitoring')->with('alerts',[['type' => 'success', 'text' => 'Sukses Mengubah Progress']]);
  }

}