<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\DA\DashboardModel;
use App\DA\TicketingModel;
use App\DA\EmployeeModel;
use App\DA\WitelModel;
use App\Http\Requests;
use App\DA\HomeModel;
use App\DA\Tele;
use Telegram;
use DB;
use TG;
use URL;
use Illuminate\Support\Facades\Session;

date_default_timezone_set('Asia/Makassar');

class DashboardController extends Controller
{
    protected $periode = [
      'HOLD','x0dan2jam','x2dan4jam','x4dan6jam','x6dan8jam','x8dan10jam','x10dan12jam','x12jam'
    ];

    protected $photokpro = [
    'Berita_Acara',
    'Speedtest',
    'SN_ONT',
    'ODP',
    'Lokasi',
    'Telephone_Incoming',
    'Foto_Pelanggan_dan_Teknisi',
    'LABEL',
    'KLEM_S',
    'K3',
    'Redaman_ODP',
    'SN_STB',
    'Live_TV'
  ];

  public function dashboardPI()
  {
    $tgl = date('Y-m-d');

    switch (session('auth')->nama_witel) {
      case 'KALSEL':
          // $mappingSCAO = DashboardModel::mappingScAO();
          $potensiPS_egbis   = DashboardModel::new_potensi_ps_egbis('PROV');

          $potensiPS_hero   = DashboardModel::new_potensi_ps_all('HERO');
          $potensiPS_prov   = DashboardModel::new_potensi_ps_all('PROV');          

          $log_psre = DB::table('selfie_kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();
          $psre_all = DashboardModel::psreKpro('ALL', 'ALL');
          $psre = DashboardModel::psreKpro('BANJARMASIN', 'REGULER');
          $psre_sektor = DashboardModel::psreKpro('SEKTOR', 'REGULER');
          $psre_mitra = DashboardModel::psreKpro('MITRA', 'REGULER');

          $log_pi_kpro = DB::table('pi_kpro_tr6')->orderBy('last_updated_at', 'desc')->select('last_updated_at')->first();
          $pi_kpro = DashboardModel::progress_totalpi();

          $ffg = DashboardModel::dashboardFFG();

          $log_comparin = DB::table('comparin_data_api')->orderBy('last_grab_at', 'desc')->select('last_grab_at')->first();
          $ttr = DashboardModel::TTRcomparin();

          // $SCAO_byumur = DashboardModel::SCAO_byumur();
          $jumlah_teknisi = DashboardModel::jumlah_teknisi($tgl);
          $jumlah_ps = DashboardModel::jumlah_ps($tgl);
          $jumlah_ps = count($jumlah_ps);
          $jumlah_teknisi = count($jumlah_teknisi);

          $get_jml_teknisi = EmployeeModel::jml_teknisi("1");
          $get_jml_teknisi_hadir = EmployeeModel::jml_teknisi_hadir("1", $tgl);
          $get_jml_teknisi_hadir_approved = EmployeeModel::jml_teknisi_hadir_approved("1", $tgl);
          $get_jml_teknisi_hadir_not_approved = EmployeeModel::jml_teknisi_hadir_not_approved("1", $tgl);
        break;

      case 'BALIKPAPAN':
        $log_psre = DB::table('selfie_kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();
          $psre_all = DashboardModel::psreKpro('ALL', 'REGULER');
          $psre_sektor = DashboardModel::psreKpro('BALIKPAPAN_SEKTOR', 'REGULER');
          $psre = DashboardModel::psreKpro('BALIKPAPAN_STO', 'REGULER');
        break;

      default:
          $log_psre = DB::table('selfie_kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();
          $psre_all = DashboardModel::psreKpro('ALL', 'REGULER');
        break;
    }


    return view('scbe.dashboardPI',compact('potensiPS', 'tgl', 'jumlah_teknisi', 'jumlah_ps', 'get_jml_teknisi_hadir_approved', 'get_jml_teknisi_hadir_not_approved', 'get_jml_teknisi', 'get_jml_teknisi_hadir', 'psre', 'psre_all', 'ffg', 'log_comparin', 'ttr', 'log_psre', 'potensiPS_hero', 'potensiPS_prov', 'log_pi_kpro', 'pi_kpro', 'psre_sektor', 'week', 'psre_mitra', 'potensiPS_egbis'));
  }

  public function dashboardPsreDbsDgs()
  {
    $month = Input::get('month');

    $data = DashboardModel::psreDbsDgs($month);
    
    return view('dashboard.psreDbsDgs', compact('month', 'data'));
  }

  public function dashboardPsreDbsDgsDetail()
  {
    $sektor = Input::get('sektor');
    $month = Input::get('month');
    $day = Input::get('day');

    $data = DashboardModel::psreDbsDgsDetail($sektor, $month, $day);

    return view ('dashboard.psreDbsDgsDetail', compact('sektor', 'month', 'day', 'data'));
  }

  public function dashboardPotensiPS()
  {
    $group = Input::get('group');

    $potensiPS = DashboardModel::new_potensi_ps_all($group);

    return view('dashboard.dashboardPotensiPS', compact('group', 'potensiPS'));
  }

  public function detailPotensiPS($group, $area, $status)
  {
    $getData = DashboardModel::detailPotensiPS($group, $area, $status);

    return view('dashboard.detailPotensiPS', compact('group', 'area', 'status', 'getData'));
  }

  public function dashboardPSRE_1($periode){
    $psre = DashboardModel::psreKpro_1($periode);
    $log_psre = DB::table('selfie_kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();

    return view('dashboard.dashboardPSRE_1',compact('psre','periode','log_psre'));
  }

  public function dashboardPSRE($type, $sektor, $status, $segmen)
  {
    $query = DashboardModel::dashboardPSRE($type, $sektor, $status, $segmen);

    if ($type != 'WITEL' && $status == 'UNSC')
    {
      foreach ($query as $value)
      {
        $sclama[$value->NCLI] = $this->get_sc_lama($value->NCLI);
        $wfm[$value->ORDER_ID] = $this->get_wfmid($value->ORDER_ID);
      }
    }

    return view('dashboard.dashboardPSRE', compact('query', 'type', 'sektor', 'status', 'segmen', 'sclama', 'wfm'));
  }

  public static function get_sc_lama($id_ncli)
  {
    $data = DB::select('
    SELECT
      dps.orderIdInteger,
      dt.id as dt_id,
      pl.kordinat_pelanggan,
      pls.laporan_status
    FROM Data_Pelanggan_Starclick dps
    LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
      dps.orderNcli = "' . $id_ncli . '"
      ORDER BY dps.orderDate ASC
    LIMIT 1');

    if (!empty($data))
    {
      $result = $data[0];
    } else {
      $result = [];
    }

    return $result;
  }

  public function dashboardFFGTTI($sektor, $status)
  {
    $query = DashboardModel::dashboardFFGTTI($sektor, $status);

    return view('dashboard.dashboardFFGTTI', compact('query','sektor','status'));
  }

  public function dashboardProgressSCBE(){
    $mappingSCBE = DashboardModel::dashboardProgressSCBE();
    return view('scbe.dashboardProgressSCBE',compact('mappingSCBE'));
  }

  public function scbe_undispatch($sektor){
    $title = "SCBE UNDISPATCH";
    $query = DashboardModel::scbe_undispatch($sektor);
    return view('scbe.dashboardSCList3',compact('query','sektor','title'));
  }

  public function umur_pi($grup,$umur,$sektor){
    $umur_pi = DashboardModel::umur_pi($grup,$umur,$sektor);
    $title = "UMUR PI ";
    return view('scbe.umur_pi',compact('umur_pi','umur','sektor','title','grup'));
  }

  public function umur_fwfm($grup,$umur,$sektor){
    $umur_pi = DashboardModel::umur_fwfm($grup,$umur,$sektor);
    $title = "UMUR FWFM ";
    return view('scbe.umur_pi',compact('umur_pi','umur','sektor','title','grup'));
  }


  public function pi_all($grup,$sektor){
    $title = "PI ALL";
    $query = DashboardModel::pi_all($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }

  public function fwfm_all($grup,$sektor){
    $title = "FWFM ALL";
    $query = DashboardModel::fwfm_all($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }


  public function undispatch_sc($kat,$sektor)
  {
    if ($kat == "H1")
    {
      $katt = "H-1";
    } elseif ($kat == "HI")
    {
      $katt = "HI";
    }
    $title = "UNDISPATCH ".$katt;
    $query = DashboardModel::undispatch_sc($kat,$sektor);
    return view('scbe.dashboardSCList',compact('query','sektor','title'));
  }

  public function undispatch_kpro($kat,$sektor)
  {
    if ($kat == "H1")
    {
      $katt = "H-1";
    } elseif ($kat == "HI")
    {
      $katt = "HI";
    }
    $title = "UNDISPATCH SC BY KPRO ".$katt;
    $query = DashboardModel::undispatch_kpro($kat,$sektor);
    return view('scbe.dashboardUndispatchKpro',compact('query','sektor','title'));
  }


  public function pi_needprogress($grup,$sektor){
    $title = "PI NEED PROGRESS";
    $query = DashboardModel::pi_needprogress($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }
  public function pi_progress($grup,$sektor){
    $title = "PI PROGRESS";
    $query = DashboardModel::pi_progress($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }
  public function pi_kendala($grup,$sektor,$status){
    $title = "PI KENDALA ".$status;
    $query = DashboardModel::pi_kendala($grup,$sektor,$status);
    $wfm = array();
    foreach ($query as $result){
      if (session('auth')->id_user=="91153709" || session('auth')->id_user=="94150248" || session('auth')->id_user=="22031995") {
      $wfm[$result->orderId] = $this->get_wfmid($result->orderId);
      }
    }
    return view('scbe.dashboardSCList2',compact('query','sektor','status','title','wfm','grup'));
  }
  public function pi_hr($grup,$sektor){
    $title = "PI HR";
    $query = DashboardModel::pi_hr($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }

  public function fwfm_needprogress($grup,$sektor){
    $title = "fwfm NEED PROGRESS";
    $query = DashboardModel::fwfm_needprogress($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }
  public function fwfm_progress($grup,$sektor){
    $title = "fwfm PROGRESS";
    $query = DashboardModel::fwfm_progress($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }
  public function fwfm_kendala($grup,$sektor,$status){
    $title = "fwfm KENDALA ".$status;
    $query = DashboardModel::fwfm_kendala($grup,$sektor,$status);
    $wfm = array();
    foreach ($query as $result){
      if (session('auth')->id_user=="91153709" || session('auth')->id_user=="94150248" || session('auth')->id_user=="22031995") {
      $wfm[$result->orderId] = $this->get_wfmid($result->orderId);
      }
    }
    return view('scbe.dashboardSCList2',compact('query','sektor','status','title','wfm','grup'));
  }
  public function fwfm_hr($grup,$sektor){
    $title = "fwfm HR";
    $query = DashboardModel::fwfm_hr($grup,$sektor);
    return view('scbe.dashboardSCList2',compact('query','sektor','title','grup'));
  }

  public function cancelOrder_all($grup, $sektor)
  {
    $title = "CANCEL ORDER ALL";
    $query = DashboardModel::cancelOrder_all($grup, $sektor);
    return view('scbe.dashboardSCList2',compact('query', 'sektor', 'title', 'grup'));
  }

  public function trialStarclick(){
    $data = '{"guid":0,"code":0,"info":"OK","data":{"LIST":[{"ORDER_ID":"505635266","ORDER_DATE":"01-Jul-20 08:07:55","ORDER_STATUS":"1500","ORDER_DATE_PS":"01-Jul-20 09:08:29","EXTERN_ORDER_ID":"TLKM-2007-00069","NCLI":"53242097","CUSTOMER_NAME":"STELLA MONICA","WITEL":"KALSEL","AGENT_ID":"18950175","JENISPSB":"AO|INTERNET+IPTV","SOURCE":"NOSS","STO":"BJM","SPEEDY":"161203221191","POTS":null,"PACKAGE_NAME":null,"KODEFIKASI_SC":"COMPLETED","STATUS_RESUME":"COMPLETED","STATUS_CODE_SC":"1500","USERNAME":"18950175","ORDER_ID_NCX":"3-325410586180","CUSTOMER_ADDR":"Jl. Belitung Darat No.38, Kuin Cerucuk, Kec. Banjarmasin Bar., Kota Banjarmasin, Kalimantan Selatan 70128, Indonesia","KCONTACT":"MI;MYIR-10203867330001;SPSCY97-B1977PIU;STELLA MONICA;085651053790","INS_ADDRESS":"KODYA BANJARMASIN,KUIN CERUCUK BJM BARAT,GG. AL HIDAYAH, 01,70129, RT 00, RW 00","CITY_NAME":"KODYA BANJARMASIN","ND_INTERNET":"161203221191","ND_POTS":null,"GPS_LATITUDE":"-3.308198414","GPS_LONGITUDE":"114.58316606","TN_NUMBER":null,"LOC_ID":"ODP-BJM-FBN\/018","ODP_ID":null,"RESERVE_TN":null,"RESERVE_PORT":"16239236456"}],"CNT":0}}';
    header("Content-Type: application/json");
    print_r(json_decode($data));
  }

  public function GerakanPeduliAkses()
  {
    $date = date('Y-m-d');

    $list = DB::table('pedulialpro')
            ->leftJoin('pedulialpro_kordinat', 'pedulialpro.pedulialpro_id', '=', 'pedulialpro_kordinat.message_id')
            ->whereNotNull('pedulialpro_kordinat.lat')
            ->get();

    $whereisteknisi = DB::table('psb_laporan AS pl')
            ->leftJoin('dispatch_teknisi AS dt', 'pl.id_tbl_mj', '=', 'dt.id')
            ->leftJoin('psb_laporan_status AS pls', 'pl.status_laporan', '=', 'pls.laporan_status_id')
            ->leftJoin('1_2_employee AS emp', 'pl.modified_by', '=', 'emp.nik')
            ->leftJoin('user AS u', 'emp.nik', '=', 'u.id_karyawan')
            ->leftJoin('regu AS r', 'emp.id_regu' , '=', 'r.id_regu')
            ->leftJoin('group_telegram AS gt', 'r.mainsector', '=', 'gt.chat_id')
            ->where('u.level', 10)
            ->where(function($query) {
              $query->whereNotNull('pl.startLatTechnition')
                  ->orwhereNotNull('pl.startLonTechnition');
              })
            ->where(function($query) use($date) {
              $query->where('pl.modified_at', 'LIKE', $date.'%')
                  ->orWhere('pl.created_at', 'LIKE', $date.'%');
              })
            ->select('pl.startLatTechnition AS lat', 'pl.startLonTechnition AS lon', 'dt.Ndem', 'emp.nama', 'r.uraian AS tim', 'gt.title AS sektor', 'gt.ket_posisi')
            ->orderBy('pl.modified_at', 'DESC')
            ->groupBy('pl.modified_by')
            ->get();

    return view('gerakanpeduliakses.home',compact('list', 'whereisteknisi'));
  }

  public function dashboardComparin($tgl){
    $data = file_get_contents('https://biawak.tomman.app/get_dashboardComparin/'.$tgl.'/KALSEL');
    $data_bpp = file_get_contents('https://psb.tomman.app/get_dashboardComparin/'.$tgl.'/BALIKPAPAN');
    $data_bpp = json_decode($data_bpp);
    return view('dashboard.comparin',compact('tgl','data','data_bpp'));
  }

  public function dashboardComparinNew($tgl){
    $query = DashboardModel::pi_pagi($tgl);
    $pi_pagi_sisa = DashboardModel::pi_pagi_sisa();
    $get_sektor_prov = DB::SELECT('select sektor_prov from maintenance_datel where witel="KALSEL" group by sektor_prov');
    return view('dashboard.comparinNew',compact('tgl','query','pi_pagi_sisa','get_sektor_prov'));
  }

  public function comparinListNew($type,$status,$sektor,$tgl){
    $get_list = DashboardModel::comparinListNew($type,$status,$sektor,$tgl);
    return view('dashboard.comparinListNew',compact('type','status','sektor','tgl','get_list'));
  }

  public function dashboardComparinList($witel,$tgl,$status){
    if ($witel=="KALSEL"){
      $base_url = 'https://biawak.tomman.app';
    } else {
      $base_url = 'https://psb.tomman.app';
    }
    $data = file_get_contents($base_url.'/get_dashboardComparinList/'.$witel.'/'.$tgl.'/'.$status);
    return view('dashboard.comparinList',compact('witel','data','tgl','status'));
  }

  public function cuaca($provinsi){


    $tgl = date('Y-m-d');
    $getCuaca = DB::SELECT('SELECT a.kota,
      b.cuaca_status as status_jam0,
      b.icon as icon_jam0,
      c.cuaca_status as status_jam6,
      c.icon as icon_jam6,
      d.cuaca_status as status_jam12,
      d.icon as icon_jam12,
      e.cuaca_status as status_jam16,
      e.icon as icon_jam16
      FROM cuaca a
      LEFT JOIN cuaca_status b ON a.jam0 = b.cuaca_status_id
      LEFT JOIN cuaca_status c ON a.jam6 = c.cuaca_status_id
      LEFT JOIN cuaca_status d ON a.jam12 = d.cuaca_status_id
      LEFT JOIN cuaca_status e ON a.jam16 = e.cuaca_status_id
      WHERE a.cuaca_tgl = "'.$tgl.'"');
    return view('dashboard.cuaca',compact('tgl','getCuaca','provinsi'));

  }

  public function dashboardSales($date,$spv){
	  $query = DashboardModel::dashboardSales($date,$spv);
    $get_psb_laporan_status = DB::SELECT('SELECT * FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND stts_dash <> "progress" order by stts_dash');
    $layout = 'layout';

    $title = 'Dashboard Kendala Sales by SPV';
	  return view('dashboard.dashboardSales',compact('layout','date','query','get_psb_laporan_status','spv','title'));
  }

  public function dashboardSalesBySPV($date){
	  $query = DashboardModel::dashboardSalesBySPV($date);
    $get_psb_laporan_status = DB::SELECT('SELECT * FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND stts_dash <> "progress" order by stts_dash');
    $layout = 'layout';
    $spv = 'all';
    $title = 'Dashboard Kendala Sales by SPV';
	  return view('dashboard.dashboardSalesbySPV',compact('layout','date','query','get_psb_laporan_status','spv','title'));
  }

  public function dashboardSalesList($status,$date,$spv,$sales){
    $query = DashboardModel::dashboardSalesList($status,$date,$spv,$sales);
    return view('dashboard.listOrder2',compact('date','query','sales','spv','status'));
  }

  public function sisa_order_pra($datel){
    $where_datel = ' AND ee.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (ee.datel = "'.$datel.'" OR ee.sektor_prov = "'.$datel.'")';
    }
    $query = DB::SELECT('
    select *,aa.alamatLengkap as alamat,"OK" as alamat1,"PRE" as ls, "PRA-PI" as orderStatus,aa.myir as orderId,aa.orderDate,aa.namaOdp as alproname,aa.layanan as jenisLayanan, aa.kcontack as akcontack, ee.sto as esto from psb_myir_wo aa
    LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
    LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
    LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
    LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
    LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
    LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
    where
    date(aa.orderDate) >= "2020-04-01" AND
    ff.orderId IS NULL AND
    aa.sc IS NULL AND
    aa.status_approval <> "2" AND
    aa.layanan <> "PDA" AND
    (cc.id is NULL OR dd.grup IN ("SISA","OGP"))
    '.$where_datel.'
    ORDER BY aa.approve_date ASC
    ');
    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function sisa_order_pda($datel){
    $where_datel = ' AND ee.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (ee.datel = "'.$datel.'" OR ee.sektor_prov = "'.$datel.'")';
  }
    $query = DB::SELECT('
    select *,aa.alamatLengkap as alamat, "OK" as alamat1,"PRE" as ls, "PRA-PI" as orderStatus,aa.myir as orderId,aa.orderDate,aa.namaOdp as alproname,aa.layanan as jenisLayanan, aa.kcontack as akcontack, ee.sto as esto from psb_myir_wo aa
    LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
    LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
    LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
    LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
    LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
    LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
    where
    date(aa.orderDate) >= "2020-04-01" AND
    ff.orderId IS NULL AND
    aa.sc IS NULL AND
    aa.status_approval <> "2" AND
    aa.layanan = "PDA" AND
    (cc.id is NULL OR dd.grup IN ("SISA","OGP"))
    '.$where_datel.'
    ORDER BY aa.approve_date ASC
    ');
    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function sisa_order($datel){
    $where_datel = ' AND ee.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (ee.datel = "'.$datel.'" OR ee.sektor_prov = "'.$datel.'")';
    }
    $query = DB::SELECT('
    select *,aa.orderAddr as alamat, "OK" as alamat1,ff.alamatLengkap as alamat1,ddd.laporan_status as ls,aa.myir,aa.orderDate,aa.orderName as customer,aa.jenisPsb as jenisLayanan, aa.kcontact as akcontack, ee.sto as esto from Data_Pelanggan_Starclick aa
    LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
    LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
    LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
    LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
    LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
    LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
    LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
    LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
    LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
    where
    aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
    date(aa.orderDate) >= "2020-04-01" AND
    aa.jenisPsb LIKE "AO%" AND
    ((cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND (ccc.id is NULL OR ddd.grup IN ("SISA","OGP")))
    '.$where_datel.'
    ORDER BY aa.orderDate ASC
    ');

    // $wfm = array();
    //   foreach ($query as $result){
    //     $wfm[$result->orderId] = $this->get_wfmid($result->orderId);
    //     }

    return view('dashboard.listOrder_AO',compact('datel','query'));
  }
  public function sisa_order_mo($datel){
    $where_datel = ' AND ee.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (ee.datel = "'.$datel.'" OR ee.sektor_prov = "'.$datel.'")';
  }
    $query = DB::SELECT('
    select *,aa.orderAddr as alamat,"OK" as alamat1, "MO" as approve_date,"MO" as status_approval,"MO" as ls,aa.myir,aa.orderDate,aa.orderName as customer,aa.jenisPsb as jenisLayanan, aa.kcontact as akcontack, ee.sto as esto from Data_Pelanggan_Starclick aa

    LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
    LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
    LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
    LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
    LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
    where
    aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
    date(aa.orderDate) >= "2020-04-01" AND
    aa.jenisPsb LIKE "MO%" AND
    (cc.id is NULL OR dd.grup IN ("SISA","OGP"))
    '.$where_datel.'
    ORDER BY aa.orderDate ASC
    ');
    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function wo_hr($datel){
    $where_datel = ' AND g.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (g.datel = "'.$datel.'" OR g.sektor_prov = "'.$datel.'")';
  }
    $query1 = DB::SELECT('
    SELECT *,d.alamatLengkap as alamat,"OK" as alamat1,  aa.Ndem as orderId,"PRA-PI" as orderStatus,d.namaOdp as alproname,d.layanan as jenisLayanan,"PRA" as ls, d.kcontack as akcontack, g.sto as esto FROM dispatch_teknisi aa
    LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    c.`laporan_status`="HR" AND
    d.sc IS NULL AND
    d.myir IS NOT NULL
    '.$where_datel.'
    ');
    $query2 = DB::SELECT('
    SELECT *,d.orderAddr as alamat,dd.alamatLengkap as alamat1,"OK" as ls,d.orderDate,d.orderName as customer,d.jenisPsb as jenisLayanan , d.kcontact as akcontack, g.sto as esto FROM dispatch_teknisi aa
    LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
    LEFT JOIN psb_myir_wo dd ON d.myir = dd.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON d.sto = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    d.orderDatePs = "" AND
    c.`laporan_status`="HR" AND
    d.orderid IS NOT NULL AND
    d.orderStatusId NOT IN ("1500","7","1300","1205") AND
    d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET")
    '.$where_datel.'
    ');

    $query = array_merge($query1,$query2);

    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function odp_full($datel){
    $where_datel = ' AND g.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (g.datel = "'.$datel.'" OR g.sektor_prov = "'.$datel.'")';
    }
    $query1 = DB::SELECT('
    SELECT *,d.alamatLengkap as alamat,"OK" as alamat1,  aa.Ndem as orderId,"PRA-PI" as orderStatus,d.namaOdp as alproname,d.layanan as jenisLayanan,"PRA" as ls, d.kcontack as akcontack, g.sto as esto FROM dispatch_teknisi aa
    LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    c.`laporan_status_id`="24" AND
    d.sc IS NULL AND
    d.myir IS NOT NULL
    '.$where_datel.'
    ');
    $query2 = DB::SELECT('
    SELECT *,d.orderAddr as alamat,dd.alamatLengkap as alamat1,"OK" as ls,d.orderDate,d.orderName as customer,d.jenisPsb as jenisLayanan , d.kcontact as akcontack,g.sto as esto FROM dispatch_teknisi aa
    LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
    LEFT JOIN psb_myir_wo dd ON d.myir = dd.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON d.sto = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    d.orderDatePs = "" AND
    c.`laporan_status_id`="24" AND
    d.orderid IS NOT NULL AND
    d.orderStatusId NOT IN ("1500","7","1300","1205") AND
    d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET")
    '.$where_datel.'
    ');

    $query = array_merge($query1,$query2);

    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function kendala_jalur($datel){
    $where_datel = ' AND g.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (g.datel = "'.$datel.'" OR g.sektor_prov = "'.$datel.'")';
    }
    $query1 = DB::SELECT('
    SELECT *,d.alamatLengkap as alamat,"OK" as alamat1,  aa.Ndem as orderId,"PRA-PI" as orderStatus,d.namaOdp as alproname,d.layanan as jenisLayanan,"PRA" as ls, d.kcontack as akcontack, g.sto as esto FROM dispatch_teknisi aa
    LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    c.`laporan_status_id`="42" AND
    d.sc IS NULL AND
    d.myir IS NOT NULL
    '.$where_datel.'
    ');
    $query2 = DB::SELECT('
    SELECT *,d.orderAddr as alamat,dd.alamatLengkap as alamat1,"OK" as ls,d.orderDate,d.orderName as customer,d.jenisPsb as jenisLayanan , d.kcontact as akcontack FROM dispatch_teknisi aa
    LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
    LEFT JOIN psb_myir_wo dd ON d.myir = dd.myir
    LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
    LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
    LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
    LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
    LEFT JOIN maintenance_datel g ON d.sto = g.sto
    WHERE
    aa.tgl >= "2020-04-01" AND
    d.orderDatePs = "" AND
    c.`laporan_status_id`="42" AND
    d.orderid IS NOT NULL AND
    d.orderStatusId NOT IN ("1500","7","1300","1205") AND
    d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET")
    '.$where_datel.'
    ');

    $query = array_merge($query1,$query2);

    return view('dashboard.listOrder',compact('datel','query'));
  }


  public function wo_ps($group, $datel, $date)
  {
    switch ($group) {
      case 'SEKTOR':

        $select = 'f.title as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = '';
            break;

          case 'NON AREA':
            $where_datel = 'AND f.title IS NULL';
            break;

          default:
            $where_datel = 'AND f.title = "' . $datel . '"';
            break;
        }
        break;

      case 'PROV':

        $select = 'aa.sektor_prov as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = '';
            break;

          case 'NON AREA':
            $where_datel = 'AND aa.sektor_prov IS NULL';
            break;

          default:
            $where_datel = 'AND aa.sektor_prov = "' . $datel . '"';
            break;
        }
        break;

      case 'EGBIS':

        $select = 'aa.sektor_prov as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = 'AND dps.provider NOT LIKE "DCS%"';
            break;

          case 'NON AREA':
            $where_datel = 'AND dps.provider NOT LIKE "DCS%" AND aa.sektor_prov IS NULL';
            break;

          default:
            $where_datel = 'AND dps.provider NOT LIKE "DCS%" AND aa.sektor_prov = "' . $datel . '"';
            break;
        }
        break;

      case 'DATEL':

        $select = 'aa.datel as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = '';
            break;

          case 'NON AREA':
            $where_datel = 'AND aa.datel IS NULL';
            break;

          default:
            $where_datel = 'AND aa.datel = "' . $datel . '"';
            break;
        }
        break;

      case 'HERO':

        $select = 'aa.HERO as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = '';
            break;

          case 'NON AREA':
            $where_datel = 'AND aa.HERO IS NULL';
            break;

          default:
            $where_datel = 'AND aa.HERO = "' . $datel . '"';
            break;
        }
        break;
    }

    $query = DB::SELECT('
      SELECT
      *,
      ' . $select . '
      dpsa.orderAddr as alamat,
      bb.alamatLengkap as alamat1,
      dpsa.orderDate,
      "OK" as ls,
      dpsa.orderName as customer,
      dpsa.jenisPsb as jenisLayanan,
      dpsa.kcontact as akcontack,
      aa.sto as esto
      FROM Data_Pelanggan_Starclick dpsa
      LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto
      LEFT JOIN psb_myir_wo bb ON dpsa.myir = bb.myir
      LEFT JOIN dispatch_teknisi b ON dpsa.orderIdInteger = b.NO_ORDER AND b.jenis_order IN ("SC", "MYIR")
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN regu e ON b.id_regu = e.id_regu
      LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
      WHERE
      dpsa.jenisPsb LIKE "AO%" AND dpsa.orderStatusId IN ("1500","1203","7") AND ((DATE(dpsa.orderDatePs) = "' . $date . '") OR (DATE(dpsa.orderDate) = "' . $date .'") AND dpsa.orderDatePs = "0000-00-00 00:00:00")
      ' . $where_datel . '
    ');
    return view('dashboard.listOrder',compact('datel','query'));
  }

  public function QCborneoTeknisi($date,$dateend)
  {
    $log_sync = DB::table('qc_borneo_tr6_log')->first();

    $query = DB::SELECT('
      SELECT
      e.uraian,
      e.mitra,
      g.title,
      SUM(CASE WHEN qbt.tag_unit IS NOT NULL THEN 1 ELSE 0 END) as jml_order,
      SUM(CASE WHEN qbt.tag_unit LIKE "%ASO%" THEN 1 ELSE 0 END) as tidak_lolos,
      SUM(CASE WHEN qbt.tag_unit IN ("Valid;","Valid;;Not Integrity") THEN 1 ELSE 0 END) as lolos
      FROM qc_borneo_tr6 qbt
      LEFT JOIN dispatch_teknisi b ON qbt.sc = b.NO_ORDER
      LEFT JOIN regu e ON b.id_regu = e.id_regu
      LEFT JOIN group_telegram g On e.mainsector = g.chat_id
      WHERE
      (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'")
      GROUP BY e.uraian
      ORDER BY tidak_lolos DESC
    ');
    return view('dashboard.qcborneo',compact('query','log_sync','date','dateend'));
  }

  public function QCborneoTeknisiList($date,$dateend,$teknisi,$jenis)
  {
    if ($teknisi == "ALL") {
      $where_teknisi = '';
    } elseif ($teknisi == "NA") {
      $where_teknisi = 'AND e.uraian IS NULL';
    } else {
      $where_teknisi = 'AND e.uraian = "'.$teknisi.'"';
    }

    if ($jenis == "NOK") {
      $where_jenis = 'AND qbt.tag_unit LIKE "%ASO%"';
    } elseif($jenis == "ORDER") {
      $where_jenis = 'AND qbt.tag_unit IS NOT NULL';
    }

    $query = DB::SELECT('
    SELECT
    e.uraian,
    e.mitra,
    qbt.nama as customer,
    qbt.*,
    b.id as id_dt,
    c.kordinat_pelanggan,
    a.orderStatus,
    c.status_laporan,
    g.title
    FROM qc_borneo_tr6 qbt
    LEFT JOIN Data_Pelanggan_Starclick a ON qbt.sc = a.orderIdInteger
    LEFT JOIN dispatch_teknisi b ON a.orderIdInteger = b.NO_ORDER
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN regu e ON b.id_regu = e.id_regu
    LEFT JOIN group_telegram g ON e.mainsector = g.chat_id
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'" )
    '.$where_teknisi.'
    '.$where_jenis.'
    ORDER BY qbt.datePs DESC
  ');

  $getFoto = ['Lokasi' => 'foto_rumah', 'Foto_Pelanggan_dan_Teknisi' => 'foto_teknisi', 'ODP' => 'foto_odp', 'Redaman_Pelanggan' => 'foto_redaman', 'Speedtest' => 'foto_cpe_layanan', 'Berita_Acara' => 'foto_berita_acara', 'Surat_Pernyataan_Deposit' => 'foto_surat', 'Profile_MYIH' => 'foto_profile_myih'];

  return view('dashboard.qcborneolist',compact('query','date','dateend','teknisi','getFoto'));

  }

  public function QCborneoTeknisiListDone($date,$dateend,$teknisi)
  {
    if ($teknisi == "ALL"){
      $where_teknisi = '';
    } elseif($teknisi == "NA") {
      $where_teknisi = 'e.uraian IS NULL AND ';
    } else {
      $where_teknisi = 'e.uraian = "'.$teknisi.'" AND ';
    }

    $query = DB::SELECT('
    SELECT
    e.uraian,
    e.mitra,
    qbt.nama as customer,
    qbt.*,
    b.id as id_dt,
    c.kordinat_pelanggan,
    a.orderStatus,
    c.status_laporan,
    g.title
    FROM qc_borneo_tr6 qbt
    LEFT JOIN Data_Pelanggan_Starclick a ON qbt.sc = a.orderIdInteger
    LEFT JOIN dispatch_teknisi b ON a.orderIdInteger = b.NO_ORDER
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN regu e ON b.id_regu = e.id_regu
    LEFT JOIN group_telegram g ON e.mainsector = g.chat_id
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'" ) AND
    '.$where_teknisi.'
    qbt.tag_unit IN ("Valid;","Valid;;Not Integrity")
    ORDER BY qbt.datePs DESC
  ');

  $getFoto = ['Lokasi' => 'foto_rumah', 'Foto_Pelanggan_dan_Teknisi' => 'foto_teknisi', 'ODP' => 'foto_odp', 'Redaman_Pelanggan' => 'foto_redaman', 'Speedtest' => 'foto_cpe_layanan', 'Berita_Acara' => 'foto_berita_acara', 'Surat_Pernyataan_Deposit' => 'foto_surat', 'Profile_MYIH' => 'foto_profile_myih'];

  return view('dashboard.qcborneolist',compact('query','date','dateend','teknisi','getFoto'));

  }

  public function QCBorneoView($sc){
    $get_project = DB::SELECT('
      SELECT
      *,
      a.lat as latSC,
      a.lon as lonSC,
      a.orderStatus as orderstatus,
      g.LATITUDE as latODP,
      g.LONGITUDE as lonODP,
      b.id as id_dt,
      bb.id as id_laporan,
      bb.dropcore_label_code as qrcode
      FROM
        Data_Pelanggan_Starclick a
      INNER JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      INNER JOIN psb_laporan bb ON b.id = bb.id_tbl_mj
      INNER JOIN regu c ON b.id_regu = c.id_regu
      INNER JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN qc_borneo e ON a.orderId = e.sc
      LEFT JOIN psb_myir_wo f ON a.orderId = f.sc
      LEFT JOIN 1_0_master_odp g ON a.alproname = g.odp_name
      WHERE
        a.orderId = "'.$sc.'"
    ');

    return view('dashboard.qcborneoview',compact('get_project','sc'));
  }

  public function QCBorneoView_byMYIR($myir){
    $get_project = DB::SELECT('
      SELECT
      *,
      a.GPS_LATITUDE as latSC,
      a.GPS_LONGITUDE as lonSC,
      a.STATUS_RESUME as orderstatus,
      g.LATITUDE as latODP,
      g.LONGITUDE as lonODP,
      b.id as id_dt,
      bb.id as id_laporan,
      bb.dropcore_label_code as qrcode,
      a.ORDER_CODE as orderId,
      a.CUSTOMER_NAME as orderName,
      a.NCLI as orderNcli,
      a.PACKAGE_NAME as jenisPsb,
      a.ND_POTS as noTelp,
      a.ND_INTERNET as internet,
      a.LOC_ID as alproname,
      a.INS_ADDRESS as orderAddr,
      a.KCONTACT as kcontact,
      0 as foto_rumah,
      0 as ket_rumah
      FROM
        Data_Pelanggan_Starclick_Backend a
      LEFT JOIN dispatch_teknisi b ON SUBSTR(a.ORDER_CODE,6,30) = b.Ndem
      LEFT JOIN psb_laporan bb ON b.id = bb.id_tbl_mj
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_myir_wo f ON SUBSTR(a.ORDER_CODE,6,30) = f.myir
      LEFT JOIN 1_0_master_odp g ON a.LOC_ID = g.odp_name
      WHERE
        a.ORDER_CODE LIKE "%'.$myir.'"
    ');
    $sc = $myir;
    return view('dashboard.qcview2',compact('get_project','sc'));
  }

  public function QCBorneoCheck(Request $req){

    $level = session('auth')->level;

    if ($level==10) {
    $layout = 'new_tech_layout';
    } else {
      $layout = 'layout';
    }

    if($req->has('id')){

      $id = $req->input('id');

      $getData = DB::SELECT('
        SELECT
        dt.id as id_dt,
        dt.Ndem,
        dps.orderId as sc,
        dps.orderName,
        dps.email as dps_email,
        kt.EMAIL as kt_email,
        dps.orderPackageName,
        kt.PACKAGE_NAME as kt_package_name,
        dps.internet,
        dps.noTelp,
        dps.alproname,
        dps.orderStatus,
        r.uraian as tim,
        ma.mitra_amija_pt as mitra,
        gt.title as sektor,
        pl.id as id_pl,
        pl.nama_odp as odp,
        pl.kordinat_pelanggan,
        pls.laporan_status,
        pls.laporan_status_id,
        qbt.*
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN qc_borneo_tr6 qbt ON dps.orderIdInteger = qbt.sc
        LEFT JOIN kpro_tr6 kt ON qbt.sc = kt.ORDER_ID
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
        WHERE
        dt.jenis_order IN ("SC","MYIR") AND
        qbt.tag_unit IS NOT NULL AND
        dt.NO_ORDER IN ('.$id.')
        LIMIT 50
      ');

      if(count($getData)>0)
      {
        $data = $getData;

      foreach($data as $key => $d)
      {
        if ($d->mitra==null || $d->tim==null)
        {
            if ($d->id_pl <> null)
            {
              $logTeknisi[$key] = DashboardModel::logTeknisiQC($d->id_pl);
            } else {
              $logTeknisi = [];
            }
        }
      }

        $get_foto = ['Lokasi' => 'foto_rumah', 'Meteran_Rumah' => 'foto_meteran_rumah', 'ID_Listrik_Pelanggan' => 'foto_meteran_rumah', 'Foto_Pelanggan_dan_Teknisi' => 'foto_teknisi', 'ODP' => 'foto_odp', 'Redaman_Pelanggan' => 'foto_redaman', 'Speedtest' => 'foto_cpe_layanan', 'Live_TV' => 'foto_cpe_layanan', 'TVOD' => 'foto_cpe_layanan', 'Telephone_Incoming' => 'foto_cpe_layanan', 'Telephone_Outgoing' => 'foto_cpe_layanan', 'SN_ONT' => 'foto_sn', 'SN_STB' => 'foto_sn', 'Berita_Acara' => 'foto_berita_acara', 'Surat_Pernyataan_Deposit' => 'foto_surat', 'Profile_MYIH' => 'foto_profile_myih', 'additional_1' => 'foto_sn', 'additional_2' => 'foto_sn', 'additional_3' => 'foto_sn', 'additional_4' => 'foto_sn', 'additional_5' => 'foto_sn'];
      } else {
        $data = [];
        $logTeknisi = [];
        $get_foto = [];
      }
    } else {
      $data = [];
      $logTeknisi = [];
      $get_foto = [];

    }
    // dd($data,$logTeknisi,$get_foto);

    return view('dashboard.QCBorneoCheck', compact('data','logTeknisi','get_foto','layout','level'));
  }

  public function qcFotoAmo(Request $req)
  {
    $level = session('auth')->level;

    if ($level == 10)
    {
      $layout = 'new_tech_layout';
    } else {
      $layout = 'layout';
    }

    if($req->has('odp'))
    {
      $odp = $req->input('odp');

      $getData = DB::SELECT('
        SELECT
          dt.id as id_dt,
          dt.NO_ORDER,
          dn1l.Incident,
          dps.ndemSpeedy,
          dps.ndemPots,
          dps.orderName,
          dps.alproname,
          dps.orderDatePs,
          pl.kordinat_pelanggan,
          pl.dropcore_label_code,
          pl.nama_odp,
          pl.kordinat_odp
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN data_nossa_1_log dn1l ON dps.internet = dn1l.no_internet
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        WHERE
          dt.jenis_order IN ("SC", "MYIR") AND
          pl.nama_odp LIKE "%'.$odp.'%"
        ORDER BY pl.modified_at, dn1l.Reported_Datex DESC
      ');

      if(count($getData) > 0)
      {
        $data = $getData;

        $get_foto = ['ODP', 'Berita_Acara' , 'Lokasi'];
      } else {
        $data = [];
        $get_foto = [];
      }
    }else{
      $data = [];
      $get_foto = [];
    }

    return view('dashboard.qcFotoAmo', compact('data','get_foto','layout'));
  }

  public function dashboardUTOnline($date, $datex)
  {
    $log = DB::table('utonline_tr6')->orderBy('last_updated_at', 'desc')->select('last_updated_at')->first();

    $xdate = date('m/d/Y', strtotime($date));
    $xdatex = date('m/d/Y', strtotime($datex));
    $data_mitra = DashboardModel::dashboardUTOnline('MITRA', $date, $datex);
    $data_sektor = DashboardModel::dashboardUTOnline('SEKTOR', $date, $datex);

    return view('dashboard.dashboardUTOnline', compact('data_mitra', 'data_sektor', 'date', 'datex', 'xdate', 'xdatex', 'log'));
  }

  public function dashboardUTOnlineDetail($type, $area, $date, $datex, $status)
  {
    $getData = DashboardModel::dashboardUTOnlineDetail($type, $area, $date, $datex, $status);
    // dd(count($getData));

    if(!in_array($status, ['ps_ao', 'ps_mo']))
    {
      return view('dashboard.dashboardUTOnlineDetail', compact('getData', 'type', 'area', 'date', 'datex', 'status'));
    } else {
      return view('dashboard.dashboardPSkpro', compact('getData', 'type', 'area', 'date', 'datex', 'status'));
    }
  }

  public function dashboardQC($date,$datex)
  {

    $log_sync = DB::table('qc_borneo_tr6_log')->first();
    $xdate = date('m/d/Y', strtotime($date));
    $xdatex = date('m/d/Y', strtotime($datex));
    $get_data = DashboardModel::dashboardQC($date,$datex);
    $get_nok = DashboardModel::dashboardQCNok($date,$datex);
    $get_foto = DashboardModel::dashboardQCFoto($date,$datex);
    return view('dashboard.dashboardQC', compact('get_data','get_nok','get_foto','date','datex','xdate','xdatex','log_sync'));
  }

  public function dashboardQCdetail($mitra,$date,$datex,$tag)
  {

    $getData = DashboardModel::dashboardQCdetail($mitra,$date,$datex,$tag);

    return view('dashboard.dashboardQCdetail', compact('getData','mitra','date','datex','tag'));
  }

    public function QCborneoTeknisiListRequestUp($date,$dateend,$teknisi){
    if ($teknisi <> "ALL"){
      $where_teknisi = 'e.uraian = "'.$teknisi.'" AND ';
    } else {
      $where_teknisi = '';
    }

    $query = DB::SELECT('
    SELECT
    e.uraian,
    e.mitra,
    a.sc,
    a.customer,
    f.*,
    b.id as id_dt,
    c.kordinat_pelanggan,
    b.jenis_layanan as status_revisi,
    c.status_laporan,
    g.title,
    d.laporan_status as status
    FROM psb_myir_wo a
      LEFT JOIN dispatch_teknisi b ON a.sc = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN regu e ON b.id_regu = e.id_regu
      LEFT JOIN qc_borneo f ON a.sc = f.sc
      LEFT JOIN group_telegram g On e.mainsector = g.chat_id
    WHERE
    (DATE(b.tgl) BETWEEN "'.$date.'" AND "'.$dateend.'" ) AND
    '.$where_teknisi.'
    f.unit LIKE "%aso%" AND
    f.sc IS NOT NULL AND
    b.jenis_layanan = "QC" AND
    c.status_laporan = 74
  ');

  return view('dashboard.qcborneorequp',compact('query','date','dateend','teknisi'));

  }

  public function mapping($date){
    $get_datel = DB::SELECT('
      SELECT
      datel
      FROM
      maintenance_datel
      WHERE
      witel = "'.session('witel').'"
      GROUP BY datel
    ');
    $get_scbe = array();
    $get_starclick = array();
    foreach ($get_datel as $datel){
      $get_scbe[$datel->datel] = DB::SELECT('
        SELECT
        count(*) as jumlah
        FROM
          Data_Pelanggan_Starclick_Backend a
          LEFT JOIN dispatch_teknisi b ON SUBSTR(a.ORDER_CODE,6,30) = b.Ndem
          LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
          LEFT JOIN psb_myir_wo d ON SUBSTR(a.ORDER_CODE,6,30) = d.myir
          LEFT JOIN maintenance_datel e ON d.sto = e.sto
        WHERE
          c.id IS NULL AND
          a.STATUS_RESUME IN ("booked","open") AND
          e.datel = "'.$datel->datel.'"
        GROUP BY e.datel
      ');
      $get_starclick[$datel->datel] = DB::SELECT('
        SELECT
          count(*) as jumlah
          FROM
            Data_Pelanggan_Starclick a
            LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
            LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
            LEFT JOIN maintenance_datel e ON a.sto = e.sto
          WHERE
            c.id IS NULL AND
            (a.orderStatus = "Fallout (WFM)") AND
            YEAR(a.orderDate) >= "2020" AND
            e.datel = "'.$datel->datel.'"
          GROUP BY e.datel
      ');
    }
    return view('dashboard.mapping',compact('date','get_datel','get_scbe','get_starclick'));
  }

  public function DashboardProv($date){
    $check = DB::connection('pg')->SELECT("
    SELECT
    a.order_id as ORDER_ID,
    a.create_dtm as ORDER_DATE,
    a.order_code as EXTERN_ORDER_ID,
    a.xn4 as NCLI,
    a.customer_desc as CUSTOMER_NAME,
    a.xs3 as WITEL,
    a.create_user_id as AGENT,
    a.xs2 as SOURCE,
    a.xs12 as STO,
    a.xs10 as SPEEDY,
    a.xs11 as POTS,
    a.order_status_id as STATUS_CODE_SC,
    a.xs7 as USERNAME,
    a.xs8 as ORDER_ID_NCX,
    c.xs11 as KCONTACT,
    b.xs3 as ORDER_STATUS,
    a.order_subtype_id
    FROM
      sc_new_orders a
    LEFT JOIN sc_new_order_activities b ON a.order_id = b.order_id AND a.order_status_id = b.xn2
    LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id
    WHERE
      a.xs3 = 'KALSEL' AND
      date(a.create_dtm) = '2020-02-04'
    ");
    header("Content-Type: application/json");
		print_r(json_encode($check));
  }

  public function sendkprobydate($sektor,$date){
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId,$num);
      sleep(5);
    }
  }

  public function sendkprodaily($sektor){
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.date('Y-m-d').'" AND a.jenisPsb="AO|BUNDLING"');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId,$num);
    }
  }

    public function kehadiran($date){
      $data = DashboardModel::kehadiran2($date);
      if (count($data)<>0){
        foreach($data as $dt){
           $detail = DashboardModel::detailJumlahAbsen($dt->mainsector, $date);
           $detail = $detail[0];
           $list[] = array(
                      'chat_id'       => $dt->mainsector,
                      'sektor'        => $dt->title,
                      'jumlahTim'     => $detail->requestAsr+$detail->requestProv,
                      'requestAsr'    => $detail->requestAsr,
                      'approvalAsr'   => $detail->approveAsr,
                      'declineAsr'    => $detail->declineAsr,
                      'requestProv'   => $detail->requestProv,
                      'approvalProv'  => $detail->approveProv,
                      'declineProv'   => $detail->declineProv
                  );

        }
      }
      else{
        $list[]='';
      }


      // dd(count($list));
      return view('dashboard.kehadiran',compact('date','data', 'list'));
    }

    public function public_kehadiran(){
      $date = date('Y-m-d');
      $data = DashboardModel::kehadiran2($date);
      if (count($data)<>0){
        foreach($data as $dt){
           $detail = DashboardModel::detailJumlahAbsen($dt->mainsector, $date);
           $detail = $detail[0];
           $list[] = array(
                      'chat_id'       => $dt->mainsector,
                      'sektor'        => $dt->title,
                      'jumlahTim'     => $detail->requestAsr+$detail->requestProv,
                      'requestAsr'    => $detail->requestAsr,
                      'approvalAsr'   => $detail->approveAsr,
                      'declineAsr'    => $detail->declineAsr,
                      'requestProv'   => $detail->requestProv,
                      'approvalProv'  => $detail->approveProv,
                      'declineProv'   => $detail->declineProv
                  );

        }
      }
      else{
        $list[]='';
      }


      // dd(count($list));
      return view('dashboard.kehadiran_public',compact('date','data', 'list'));
    }

    public function autosendtobotkpro($tgl,$jenis,$status,$so,$ket){
      $data = DashboardModel::scbeList($tgl,$jenis,$status,$so,$ket);
      foreach ($data as $num => $result){
        $this->sendtobotkpro($result->orderId,++$num);
      }
      // return redirect('/dashboard/scbeList/'.$tgl.'/'.$jenis.'/'.$status.'/'.$so.'/'.$ket)->with('alerts', [
      //   ['type' => 'success', 'text' => '<strong>SUKSES</strong> sync kpro']
      // ]);
    }

    public function autosendtobotkprofix(){
      ini_set('max_execution_time', '3000');
      $data = DB::SELECT('SELECT * FROM dispatch_teknisi WHERE Ndem IN ("16612140",
"16613547",
"16615818",
"16616359",
"16617553",
"16619878",
"16619914",
"16620366",
"16621818",
"16619242",
"16623991",
"16634123",
"16636107",
"16636626",
"16637019",
"16641856",
"16642766",
"16642880",
"16647287",
"16647964",
"16648526",
"16649242",
"16650604",
"16650627",
"16662987",
"16666092",
"16666577",
"16666677",
"16667739",
"16668513",
"16668701",
"16670196",
"16671867",
"16672623",
"16672724",
"16673427",
"16673949",
"16675316",
"16675904",
"16677654",
"16687513",
"16687679",
"16687937",
"16688618",
"16688793",
"16690370",
"16690394",
"16690443",
"16690663",
"16690736",
"16691144",
"16691587",
"16692972",
"16693021",
"16693124",
"16693305",
"16693501",
"16694166",
"16695701",
"16696247",
"16706185",
"16707087",
"16707502",
"16707942",
"16709238",
"16710877",
"16712152",
"16712363",
"16713827",
"16714420",
"16714796",
"16716566",
"16717092",
"16719008",
"16719585",
"16719629",
"16719654",
"16724137",
"16725173",
"16726903",
"16727172",
"16728327",
"16728516",
"16729014",
"16634144",
"16709739",
"16719206",
"16725462",
"16730055",
"16739782",
"16740681",
"16741438",
"16742709",
"16743843",
"16743973",
"16746732",
"16747924",
"16748241",
"16748761",
"16749257",
"16749510",
"16749663",
"16752072",
"16752843",
"16752938",
"16753497",
"16754832",
"16756873",
"16759060",
"16762931",
"16764956",
"16765934",
"16768136",
"16688883",
"16719190",
"16753709",
"16780556",
"16780972",
"16781387",
"16781564",
"16783294",
"16784069",
"16785217",
"16786071",
"16786432",
"16786407",
"16787105",
"13022080",
"16788835",
"16788936",
"16791786",
"16791877",
"16792506",
"16792646",
"16792744",
"16792861",
"16793698",
"16795076",
"16797140",
"16798954",
"16799061",
"16799507",
"16799820",
"16800536",
"16801342",
"16802076",
"16802338",
"16803793",
"16804490",
"16785178",
"16790343",
"16816703",
"16817024",
"16818934",
"16819562",
"16819706",
"16819761",
"16821188",
"16821366",
"16822762",
"16822904",
"16823309",
"16824251",
"16825467",
"16826334",
"16828039",
"16828117",
"16828824",
"16830274",
"16830728",
"16831841",
"16832154",
"16832327",
"16832700",
"16837201",
"16837295",
"16841444",
"16865008",
"16947496",
"16861156",
"16949880",
"16921354",
"16893245",
"16875955",
"16963864",
"16954310",
"16929165",
"16298927",
"16898780",
"16894534",
"16864802",
"16853566",
"16865011",
"16861444",
"16842254",
"16852677",
"16956906",
"16957565",
"16955242",
"16952809",
"16926480",
"16867519",
"16321924",
"16864047",
"16820672",
"16854257",
"16897261",
"16921571",
"16892311",
"16902112",
"16962617",
"16950094",
"16928557",
"16897014",
"16956320",
"16860058",
"16922881",
"16924804",
"16928671",
"16407803",
"16852552",
"16854525",
"16852635",
"16850992",
"16860356",
"16959901",
"16903090",
"16891318",
"16855873",
"16874221",
"16852100",
"16944941",
"16904158",
"16890497",
"16856339",
"16899087",
"16875199",
"16872126",
"16901641",
"16875110",
"16889762",
"16376008",
"16898679",
"16901950",
"16868095",
"16861908",
"16955232",
"16891194",
"16959155",
"16924780",
"16534968",
"16855644",
"16893818",
"16954383",
"16947842",
"16890313",
"16925978",
"16858270",
"16834995",
"16941953",
"16947763",
"16922912",
"16948581",
"16928935",
"16966921",
"16861759",
"16893170",
"16888347",
"16956476",
"16958381",
"16961295",
"16983095",
"16983611",
"16983895",
"16985683",
"16986020",
"16986982",
"16987027",
"16988664",
"16988889",
"16989466",
"16990035",
"16992815",
"16992788",
"16993938",
"16993862",
"16994145",
"6679945",
"16994759",
"16994763",
"16995538",
"16995910",
"16999328",
"17000861",
"17001760",
"17004742",
"17006121",
"17006516",
"17006590",
"17006791",
"17008992",
"16907415",
"16949709",
"16951164",
"16960474",
"16961014",
"16985976",
"16990642",
"16994048",
"17002953",
"17006588",
"17017240",
"17017956",
"17018500",
"17018740",
"17019923",
"17020351",
"17020433",
"17021312",
"17021679",
"17022725",
"17023685",
"17024034",
"17024110",
"17024628",
"17024757",
"17024824",
"17027595",
"17028061",
"17028085",
"17028102",
"17028329",
"17028331",
"17028536",
"17028557",
"17028962",
"17029337",
"17030786",
"17030911",
"17031629",
"17032324",
"17033262",
"17033574",
"17034814",
"17036116",
"17037032",
"17037416",
"17038293",
"17038384",
"17038738",
"17039608",
"16941421",
"16959186",
"16983937",
"16984555",
"16988142",
"16995466",
"16998712",
"17000106",
"17002825",
"17017813",
"17018518",
"17021333",
"17021999",
"17023001",
"17024889",
"17029696",
"17034743",
"17035690",
"17038346",
"17038490",
"17038499",
"17052637",
"17053024",
"17053107",
"17053211",
"17053727",
"17054239",
"17054726",
"17054801",
"17055637",
"17055670",
"17056012",
"17056051",
"17056338",
"17056856",
"17056881",
"17057159",
"17057107",
"17057753",
"17057670",
"17057700",
"17058009",
"17058217",
"17058245",
"17059099",
"17059100",
"17060285",
"17060712",
"17061290",
"17061512",
"17061547",
"17062372",
"17062299",
"17063644",
"17064918",
"17064883",
"17066023",
"17066134",
"17066546",
"17066496",
"17067956",
"17069824",
"17069800",
"17070232",
"17070364",
"17073070",
"17073428",
"17073792",
"17074321",
"17075817",
"17087416",
"17088615",
"17088617",
"17088635",
"17088777",
"17088885",
"17088923",
"17088946",
"17089027",
"17088963",
"17088989",
"17089097",
"17089230",
"17089157",
"17089255",
"17089265",
"17089421",
"17089567",
"17089674",
"17089728",
"17089846",
"17089899",
"17089907",
"17089940",
"17089970",
"17090532",
"17090588",
"17091123",
"17091135",
"17091227",
"17091337",
"17091285",
"17091765",
"17091841",
"17092416",
"17093002",
"17093056",
"17093134",
"17093075",
"17093175",
"17093444",
"17093949",
"17094669",
"17094770",
"17094938",
"17094949",
"17095111",
"17095153",
"17095422",
"17095523",
"17095596",
"17095861",
"17095694",
"17096522",
"17097459",
"17098026",
"17098269",
"17098995",
"17099561",
"17100193",
"17100421",
"17100434",
"17100615",
"17100564",
"17101983",
"17102076",
"17102482",
"17102664",
"17102675",
"17102854",
"17102862",
"17102904",
"17103286",
"17103656",
"17103693",
"17104026",
"17103946",
"17103991",
"17104690",
"17104774",
"17106142",
"17106357",
"17106895",
"17107185",
"17107555",
"17107902",
"17107998",
"17108524",
"17109258",
"17109407",
"17109525",
"17112060",
"17112445",
"17125217",
"17125157",
"17125204",
"17125349",
"17126039",
"17126118",
"17126829",
"17127016",
"17127045",
"17127052",
"17126995",
"17127600",
"17127713",
"17128235",
"17128403",
"17128452",
"17128606",
"17128812",
"17128813",
"17128816",
"17128935",
"17129010",
"17129142",
"17129748",
"17129775",
"17130020",
"17130521",
"17132783",
"17133996",
"17135454",
"17135501",
"17135597",
"17135645",
"17135738",
"17136028",
"17136515",
"17136556",
"17136817",
"17136958",
"17137735",
"17137836",
"17137958",
"17138622",
"17138867",
"17139579",
"17139680",
"17140371",
"17140628",
"17141758",
"17143048",
"17129185")');
    foreach ($data as $result){
        $this->sendtobotkpro($result->id);
      }
    }

   public function sendtobotkpro($id,$urutan){

      ini_set('max_execution_time', '300000');
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
    foreach($this->photokpro as $num => $name) {
      if (count($get_sc)>0){
        echo "num.".$num." ".date('Y-m-d H:i:s');
        $path = '/mnt/hdd4/upload/evidence/'.$id.'/'.$name.'.jpg';
        $path2 = '/mnt/hdd3/upload/evidence/'.$id.'/'.$name.'.jpg';

        if (file_exists($path)) {
          $path_kpro = "/mnt/hdd4/upload/evidence/".$id."/".$name."-kpro.jpg";
          // if(!file_exists($path_kpro)){
            $img = new \Imagick($path);
            $img->scaleImage(600, 800, true);

             $watermark = new \Imagick('/mnt/hdd4/upload/watermark/watermark-1.png');
             $x = 0;
             $y = 0;
            $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
            $img->writeImage();
          // }
          $foto = 'https://biawak.tomman.app/upload4/evidence/'.$id.'/'.$name.'-kpro.jpg';
          #$foto = public_path().'/upload/evidence/'.$id.'/'.$name.'-kpro.jpg';
          echo $foto;
      try {
        //TG::sendMsg('@kprofoto_bot', 'order '.$urutan.' foto '.$num.' : '.$result->orderId);
        TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
        # echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @mnorrifani /mnt/hdd1/upload/evidence/'.$id.'/'.$name.'-kpro.jpg ['.$result->orderId.'] "');

      } catch(Exception $e) {
        echo "gagal";
      TG::sendMessage('@kprofoto_bot', 'failed');
      }
  }
      #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }elseif (file_exists($path2)){
          #$img = new \Imagick($path2);
          #$img->scaleImage(600, 800, true);

          // $watermark = new \Imagick('/mnt/hdd2/upload/watermark/watermark-1.png');
          // $x = 0;
          // $y = 0;
          // $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
          #$img->writeImage("/mnt/hdd2/upload/evidence/".$id."/".$name."-kpro.jpg");
          #$foto = 'https://psb.tomman.info/upload2/evidence/'.$id.'/'.$name.'-kpro.jpg';
          echo $foto;
          try {
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
      } catch(Exception $e) {
        echo "gagal";
      TG::sendMessage('@kprofoto_bot', 'failed');
      }

      }
      echo "<br />";
    }
  // return redirect('/'.$id)->with('alerts', [
    //     ['type' => 'success', 'text' => '<strong>SUKSES</strong> sync kpro']
    //   ]);
  }

    public function kpisektor(){
      $get_sektor = DB::SELECT('
          SELECT * FROM group_telegram a
          WHERE
            a.ket_sektor = 1
          ORDER BY a.urut ASC
        ');
      $data = array();
      foreach ($get_sektor as $sektor){
        $get_gaul_sektor = DB::SELECT('select count(*) as jumlah from detail_gangguan_close a LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem LEFT JOIN regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'"');
        $get_1ds = DB::SELECT('select count(*) as jumlah from detail_gangguan_close_3on3 a LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem LEFT JOIN regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'" and a.ttr12_jam = 0');

        $get_saldo = DB::SELECT('select count(*) as jumlah from rock_excel a LEFT JOIN dispatch_teknisi b ON a.trouble_no = b.Ndem LEFT JOIN regu c on b.id_regu = c.id_regu where c.mainsector = "'.$sektor->chat_id.'"');
        $data[$sektor->chat_id]['gaul']  = $get_gaul_sektor[0];
        $data[$sektor->chat_id]['saldo']  = $get_saldo[0];
        $data[$sektor->chat_id]['1ds']  = $get_1ds[0];
      }
      return view('dashboard.kpisektor',compact('get_sektor','data'));

    }

    public function panjangtarikan($periode){
      $query = DashboardModel::panjangtarikan($periode);
      return view('dashboard.panjangtarikan',compact('periode','query'));
    }

    public function index()
    {
      echo "apa cenganng2";
    }

    public function unspec(){
      $query = DashboardModel::unspec();
      return view('dashboard.unspec',compact('query'));
    }

    public function unspeclist($sto){
      $query = DashboardModel::unspeclist($sto);
      return view('dashboard.unspeclist',compact('query','sto'));
    }

    public function mon_nossa($date){
      $getMonNossa = DashboardModel::mon_nossa($date);
      return view('dashboard.mon_nossa',compact('getMonNossa','date'));
    }

    public function unspec_all($date){
      $unspec_prov = DashboardModel::unspec_prov($date);
      $unspec_prov_null = DashboardModel::unspec_prov_null($date);
      $unspec_asr = DashboardModel::unspec_asr($date);
      $unspec_asr_null = DashboardModel::unspec_asr_null($date);
      return view('dashboard.unspec_all',compact('unspec_prov','unspec_asr','unspec_prov_null','unspec_asr_null','date'));
    }

    public function unspeclist_prov($sektor,$date){
      $query = DashboardModel::unspeclist_prov($sektor,$date);
      return view('dashboard.unspeclist_prov',compact('query','sektor','date'));
    }

    public function unspeclist_prov_null($sektor,$date){
      $query = DashboardModel::unspeclist_prov_null($sektor,$date);
      return view('dashboard.unspeclist_prov',compact('query','sektor','date'));
    }

    public function unspeclist_asr($sektor,$date){
      $query = DashboardModel::unspeclist_asr($sektor,$date);
      return view('dashboard.unspeclist_asr',compact('query','sektor','date'));
    }

    public function unspeclist_asr_null($sektor,$date){
      $query = DashboardModel::unspeclist_asr_null($sektor,$date);
      return view('dashboard.unspeclist_asr',compact('query','sektor','date'));
    }

    public function assuranceStatusOpen($witel){
      $query = DashboardModel::get_assuranceStatusOpen($witel);
      return view('assurance.dashboardStatus',compact('query'));
    }

    public function nossa($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = 'a.Witel';
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = 'b.sektor_asr';
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      return view('dashboard.assuranceNossa',compact('query','query_myi','lastsync','title'));
    }
    public function nossasektor($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = 'a.Witel';
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = 'b.sektor_asr';
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      return view('dashboard.assuranceNossa',compact('query','query_myi','lastsync','title'));
    }

    public function nossaNew($witel){
      if ($witel=="ALL"){
        $title = "MONITORING 1DS FZ KALIMANTAN 2";
        $org = "a.Witel";
      } else {
        $title = "MONITORING 1DS WITEL ".strtoupper($witel);
        $org = "b.sektor_asr";
      }
      $query = DashboardModel::getNossa('ALL',$witel,$org);
      $query_myi = DashboardModel::getNossa('6',$witel,$org);
      $lastsync = DashboardModel::get_last_sync_nossa();
      $data = array();
      foreach ($query as $result){
        foreach ($this->periode as $per) {
          $data[$result->ORG][$per] = DashboardModel::getNossaList('ALL',$per,' h.sektor_asr = "'.$result->ORG.'"');
        }
      }
      $periode = $this->periode;
      return view('dashboard.assuranceNossaNew',compact('query','query_myi','lastsync','title','data','periode'));
    }


    public function nossaList($channel,$periode,$witel){
      $query = DashboardModel::getNossaList($channel,$periode,$witel);
      return view('dashboard.assuranceNossaList',compact('query','periode','witel'));
    }

    public function nossaListText($channel,$periode,$witel){
      $query = DashboardModel::getNossaList($channel,$periode,$witel);
      return view('dashboard.assuranceNossaListText',compact('query','periode','witel'));
    }


    public function pbs($type){
      switch ($type) {
        case 'teamleader':
          $title = "PBS TEAM LEADER";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS-TL/DASHBOARD_TL?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
        case 'pbs_hd' :
          $title = "PBS HELPDESK";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS_HELPDESK/Dashboard1?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
        case 'sm' :
          $title = 'PBS SITE MANAGER';
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/m-PBS-SM/Dashboard2?:embed=y&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_count=no&amp;:showVizHome=no';
          break;
        default:
          $title = "PBS TEAM LEADER";
          $link = 'https://analytics.telkomakses.co.id/t/Tableau/views/PBS-TL/DASHBOARD_TL?:embed=y&amp;:showVizHome=no&amp;:host_url=https%3A%2F%2Fanalytics.telkomakses.co.id%2F&amp;:embed_code_version=2&amp;:tabs=no&amp;:toolbar=yes&amp;:showAppBanner=false&amp;:showShareOptions=true&amp;:display_spinner=no&amp;:loadOrderID=0';
          break;
      }
      return view('dashboard.pbs',compact('link','title'));
    }

    public function PI(){
      $dateStart = Input::get('dateStart');
      $dateEnd = Input::get('dateEnd');
      $transaksi = Input::get('transaksi');
      $query = DashboardModel::PI($transaksi,$dateStart,$dateEnd);
      return view('dashboard.PI',compact('transaksi','dateStart','dateEnd','query'));
    }

    public function PIlist($witel,$jam){
      $dateStart = Input::get('dateStart');
      $dateEnd = Input::get('dateEnd');
      $query = DashboardModel::PIlist($witel,$jam,$dateStart,$dateEnd);
      return view('dashboard.PIlist',compact('witel','jam','dateStart','dateEnd','query'));
    }

    public function kpi_assurance_mitra(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::by_actual_solution('SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::by_actual_solution('JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance_mitrax($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_jenis_ggn','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez','get_by_actual_solution'));
    }

    public function kpi_assurance_mitra_tech(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::by_actual_solution('SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::by_actual_solution('JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance_mitra_tech($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_jenis_ggn','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez','get_by_actual_solution'));
    }

    public function kpi_assurance(){
      $periode = Input::get('periode');
      $mitrax = Input::get('mitra');
      if ($periode=="") { $periode = date('Ym'); }
      if ($mitrax=="") { $mitrax = "ALL"; }
      $get_by_actual_solution = DashboardModel::nonatero_group_by('b.SUB_KATEGORI_PERBAIKAN',$periode,$mitrax);
      $get_by_segmentasi = DashboardModel::nonatero_group_by('a.SEGMENTASI',$periode,$mitrax);
      $get_by_jenis_ggn = DashboardModel::nonatero_group_by('a.JENIS_GGN',$periode,$mitrax);
      $get_kpi_assurance = DashboardModel::kpi_assurance($periode,$mitrax);
      $get_periodez = DashboardModel::kpi_assurance_periode();
      $get_mitra = DashboardModel::kpi_assurance_mitra($periode);
      $last_update = DashboardModel::get_last_update_kpi_assurance();
      return view('dashboard.kpi_assurance',compact('get_by_actual_solution','get_by_jenis_ggn','get_by_actual_solution','mitrax','get_mitra','get_kpi_assurance','periode','last_update','get_periodez'));
    }

    public function kpi_assuranceList($tipe,$sektor,$periode){
      $query = DashboardModel::kpi_assuranceList($tipe,$sektor,$periode);
      return view('dashboard.kpi_assuranceList',compact('tipe','sektor','periode','query'));
    }
    public function kpi_assuranceListGaul($sektor,$periode){
      $tipe = 'GAUL';
      $query = DashboardModel::kpi_assuranceListGaul($sektor,$periode);
      return view('dashboard.kpi_assuranceListGAUL',compact('tipe','sektor','periode','query'));
    }

    public function provisioning_wallboard(){
      $query = DashboardModel::provisioning_wallboard_byjam();
      $query_hari = DashboardModel::provisioning_wallboard_byhari();
      $query2_hari = DashboardModel::provisioning2_wallboard_byhari();
       $lastsync = DB::connection('mysql4')->table('4_0_master_order_1_lastsync')->get();

      return view('dashboard.provisioning_wallboard',compact('lastsync','query','query_hari','query2_hari'));
    }

    public function provisioningbyjam($id,$witel){
      $query = DashboardModel::provisioning_wallboard_list($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }

    public function provisioningbyhari($id,$witel){
      $query = DashboardModel::provisioning_wallboard_list_by_hari($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }
    public function provisioning2byhari($id,$witel){
      $query = DashboardModel::provisioning2_wallboard_list_by_hari($id,$witel);
      return view('dashboard.provisioning_wallboard_list',compact('query'));
    }
    public function listbyumur($type,$sektor,$durasi,$channel){
      $getAssuranceList = DashboardModel::listbyumur($type,$sektor,$durasi,$channel);
      return view('dashboard.listbyumur',compact('getAssuranceList','type','sektor','durasi','channel'));
    }
    public function listbyumur_public($sektor){
      // $sektor = 'ALL';
      $durasi = 'lebih24';
      $channel = 'ALL';
      $getAssuranceList = DashboardModel::listbyumur($sektor,$durasi,$channel);
      return view('dashboard.listbyumurPublic',compact('getAssuranceList','sektor','durasi'));
    }
    public function umurTiket(){
      $getumurTiket = DashboardModel::umurTiket();
      return view('dashboard.umurTiket',compact('getumurTiket'));
    }

    public function rekon($date,$dateend){
      $rekonProvMitra = DashboardModel::rekonProvMitra($date,$dateend);
      $rekonMitraData = DashboardModel::rekonMitra($date,$dateend);
      // $datams2n = DashboardModel::ms2n($date);
      // $datams2nmigrasi = DashboardModel::ms2nmigrasi($date);
      // $dataRekonMintraDC = DashboardModel::rekonMitraDc($date);
      // $rekonMitraProv    = DashboardModel::rekonMitrProv($date);
			$get_qc_borneo 		 = DashboardModel::qc_borneo($date,$dateend);
      // $rekonMitra = array();
      // foreach($rekonMitraData as $rekon){
      //     foreach($dataRekonMintraDC as $rekonDc){
      //         if ($rekon->mitra == $rekonDc->mitra){
      //             $rekonMitra[] = [
      //                 'mitra'               => $rekon->mitra,
      //                 'jumlah2'             => $rekon->jumlah,
      //                 'jumlah_prov2'        => $rekon->jumlah_prov,
      //                 'jumlah_prov_anomali' => $rekon->jumlah_prov_anomali,
      //                 'jumlah_migrasi'      => $rekon->jumlah_migrasi,
      //                 'jumlah_asr'          => $rekon->jumlah_asr,
      //                 'jumlahDc'            => $rekonDc->dc_preconn
      //             ];
      //         };
      //     };
      // }

      // $data = array();
      // foreach($rekonMitra as $mit){
      //     foreach($rekonMitraProv as $prov){
      //         if($prov->mitra==$mit['mitra']){
      //             $mit    = array_merge($mit, array("jumlah_prov"=>$prov->jumlah_prov,"jumlah"=>$mit['jumlah2'] + $prov->jumlah_prov));
      //             $data[] = $mit;
      //         };
      //     }

      //     if ($mit['jumlah_prov2']==0){
      //         $mit    = array_merge($mit, array("jumlah_prov"=>0,"jumlah"=>$mit['jumlah2'] + $prov->jumlah_prov));
      //         $data[] = $mit;
      //     }
      // };

      // $rekonMitra = $data;
      return view('dashboard.rekon',compact('get_qc_borneo','date','dateend','data','rekonProvMitra','rekonMitraData','datams2n','datams2nmigrasi','tglAwal','tglAkhir'));
    }

    public function warehouse(){
      $date = Input::get('date');
      $get_data = DashboardModel::warehouse($date);
      $get_datax = DashboardModel::warehouseMaterial($date);
      return view('dashboard.warehouse', compact('date','get_data','get_datax'));
    }

    public function warehouseRecap(){
      $date = Input::get('date');
      $type = Input::get('type');
      $nte = Input::get('nte');
      $area = Input::get('area');
      $query = DashboardModel::warehouseRecap($date,$type,$nte,$area);
      return view('dashboard.warehouseRecap',compact('date','type','nte','area','query'));
    }

    public function warehouseData(){
      $date = Input::get('date');
      $material = Input::get('material');
      $area = Input::get('area');
      $query = DashboardModel::warehouseData($date,$material,$area);
      return view('dashboard.warehouseData', compact('date','material','area','query'));
    }

    public function listJumlahDc($mitra, $tgl){
        $getData = DashboardModel::rekonMitraDcList($mitra, $tgl);
        return view('dashboard.RekonDcList',compact('getData', 'mitra', 'tgl'));
    }

    public function listms2n($tgl,$so,$jenis,$status){
      $title = "MS2N NAL";
      $query = DashboardModel::listms2n($tgl,$so,$jenis,$status);
      return view('dashboard.listms2n',compact('tgl','so','jenis','title','query'));
    }

    public function rekonList($jenis,$date){
      $data = DashboardModel::getRekonList($jenis,$date);
      return view('dashboard.rekonList',compact('jenis','date','data'));
    }

    public function listTimMitra($jenis,$date,$dateend){
      $data = DashboardModel::listTimMitra($jenis,$date,$dateend);
      return view('dashboard.listTimMitra',compact('jenis','date','dateend','data'));
    }

    public function mitra($tgl){
      $query = DashboardModel::getMitra($tgl);
      return view('dashboard.mitra',compact('tgl','query'));
    }

    public function migrasiList($tgl,$jenis,$status,$so,$order){
      $data = DashboardModel::getMigrasiStatusList($tgl,$jenis,$status,$so,$order);
      $title = "MIGRASI";

      // return view('dashboard.provisioningList',compact('title','data','tgl','jenis','status','so'));
      return view('dashboard.migrasiListDashboard',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasi($tgl,$jenis){
      // $query = DashboardModel::getMigrasi($tgl);
      $datax = DashboardModel::getMigrasiStatus($tgl,$jenis);

      $order = array();
      foreach($datax as $data){
          $order[] = $data->ketOrder;
      };

      $ketOrder = array_unique($order);
      return view('dashboard.migrasi',compact('tgl','datax','jenis', 'ketOrder'));
    }

    public function absenkanbosku(){

      $absenkanbosku = DashboardModel::absenkanbosku();
      $content = "ABSENKANBOSKU<br />";
        $content .= "
          <table>
            <tr>
              <td>No</td>
              <td>Laborcode</td>
              <td>Nama</td>
              <td>Status</td>
              <td>Autoabsen</td>
            </tr>
        ";
      $number = 0;
      echo " (".count($absenkanbosku).") ";
      foreach ($absenkanbosku as $data){
        $autoabsen = $this->autoabsen($data->laborcode);
        $content .= "
            <tr>
              <td>".++$number."</td>
              <td>".$data->laborcode."</td>
              <td>".$data->teknisi."</td>
              <td>".$data->hadir."</td>
              <td>".$autoabsen."</td>
            </tr>
        ";
      }
        $content .= "
          </table>
        ";
        echo $content;
    }

    public function autoabsen($id){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_update_absen.php');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"laborcode=".$id."&reasoncode=HADIR&run=SUBMIT");
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $c = curl_exec($ch);
      return "DONE";
    }

    public function provisioning($tgl,$jenis, Request $req){
      // dashboard potensi undispatch
      $area   = array('INNER','BBR','KDG','BLC','TJL');
      $jumlah = count($area);
      for($aa=0; $aa<=$jumlah-1; $aa++){
          $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
          $enddate = date('Y-m-d');
          $datas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');

          // simpan data ke table psb_dashboard_prov
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);

          $kendalas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is not NULL AND dps.orderStatusId=16 THEN 1 ELSE 0 END) as jumlah_kendala
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7) and m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['kendala' => $kendalas[0]->jumlah_kendala]);
              // (DATE(dps.orderDate) = "'.$enddate.'")
      }

      $reportPotensi = DB::table('psb_dashboard_prov')->get();
      $listStatus    = DB::table('psb_laporan_status')->get();

      return view('dashboard.provisioning',compact('jenis','tgl','reportPotensi','listStatus'));
      // return view('dashboard.provisioning',compact('data','jenis','tgl','listStatus'));
    }

    public function provisioning2($tgl,$jenis, Request $req){
      $data = DashboardModel::provisioning($jenis,$tgl);
      $sqlWhere2 = '';

      if ($req->has('status')){
        $tes = $req->input('status');
        $jml = count($tes);

        $sqlWhere = '(';
        for($a=0;$a<$jml;$a++){
            $sqlWhere .= "'$tes[$a]'".',';
            $sqlWhere2 .= $tes[$a].'-';
        }
        $sqlWhere = substr($sqlWhere, 0, -1);
        $sqlWhere .= ')';

        $data = DashboardModel::provisioningCari($tgl,$sqlWhere);
      };

      // $reportPotensi = DashboardModel::reportPotensi();

      // dashboard potensi jumlah_undispatch
      $area   = array('INNER','BBR','KDG','BLC','TJL');
      $jumlah = count($area);
      for($aa=0; $aa<=$jumlah-1; $aa++){
          $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
          $enddate = date('Y-m-d');
          $datas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');

          // simpan data ke table psb_dashboard_prov
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);

          $kendalas = DB::SELECT('
          SELECT
              m.area_migrasi,
              SUM(CASE WHEN dt.id is not NULL AND dps.orderStatusId=16 THEN 1 ELSE 0 END) as jumlah_kendala
          FROM
              Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE
              (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
              AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7) and m.mdf<>""
              And m.area_migrasi="'.$area[$aa].'"
          ');
          DB::table('psb_dashboard_prov')->where('area',$area[$aa])->update(['kendala' => $kendalas[0]->jumlah_kendala]);
              // (DATE(dps.orderDate) = "'.$enddate.'")
      }

      $reportPotensi = DB::table('psb_dashboard_prov')->get();
      $listStatus    = DB::table('psb_laporan_status')->get();

      return view('dashboard.provisioning',compact('data','jenis','tgl','reportPotensi','listStatus','sqlWhere2'));
      // return view('dashboard.provisioning',compact('data','jenis','tgl','listStatus'));
    }

    public function listpotensi($date, $area, $jenis){
      $data = DashboardModel::listpotensi($jenis,$area);
      return view('dashboard.potensiList',compact('date','jenis','data'));
    }

    public function provisioningList($tgl,$jenis,$status,$so,$order,Request $req){
      ini_set('memory_limit','10G');
      $q = $req->q;
      if ($q==NULL){
          $data = DashboardModel::provisioningList($tgl,$jenis,$status,$so);
      }
      else{
          $q   = substr($q, 0, -1);
          $tes = explode('-', $q);
          $jml = count($tes);

          $sqlWhere = '(';
          for($a=0;$a<$jml;$a++){
              $sqlWhere .= "'$tes[$a]'".',';
          }
          $sqlWhere = substr($sqlWhere, 0, -1);
          $sqlWhere .= ')';

          $data = DashboardModel::provisioningListFilter($tgl,$jenis,$status,$so,$sqlWhere);
          // dd($data);
      }

      if ($order=='PROV'){
          $title = "PROVISIONING";
      }
      else{
          $title = "SCBE";
      };

      // Excel::create("List Material Keluar", function($excel) use ($datas) {
      //       //  set property
      //       $excel->setTitle("Data Project");

      //       $excel->sheet('List Material Keluar', function($sheet) use ($datas){
      //           $sheet->mergeCells('A1:J1');
      //           $sheet->cells('A1', function ($cells) use ($datas){
      //               $cells->setFontWeight('bold');
      //               $cells->setValue('List Material Keluar');
      //           });

      //           $row = 3;
      //           $sheet->row($row, [
      //               'No',
      //               'SM',
      //               'TL',
      //               'SC',
      //               'Tanggal WO',
      //               'Tanggal Pasang',
      //               'Tanggal PS',
      //               'STO',
      //               'Telepon',
      //               'Inet / Datin',
      //               'Nama',
      //               'Alamat',
      //               'Jenis Mutasi',
      //               'Layanan',
      //               'Nama Modem',
      //               'Type / Series',
      //               'S/N or Mac Address',
      //               'merk',
      //               'Mode Gelaran',
      //               'DC',
      //               'Patchcore',
      //               'UTP Car 6',
      //               'Tiang',
      //               'Pullstrap',
      //               'Conduit',
      //               'Tray Cable',
      //               'ODP',
      //               'Kord. Pelanggan',
      //               'Area',
      //               'Mitra',
      //               'Status SC',
      //               'Status Tek',
      //               'Tambah Tiang',
      //               'Catatan Tek'
      //             ]);

      //           foreach($datas as $no=>$data){
      //             $sheet->row(++$row,[
      //                 ++$no,
      //                 'MUHAMMAD NOR RIFANI',
      //                 $data->TL,
      //                 $data->uraian,
      //                 $data->uraian,
      //                 $data->orderId,
      //                 $data->tanggal_dispatch,
      //                 $data->modified_at,
      //                 $data->orderDatePs,
      //                 $data->sto,
      //                 $data->ndemPots,
      //                 $data->ndemSpeedy,
      //                 $data->orderName,
      //                 $data->orderAddr.' '.$data->kordinat_pelanggan,
      //                 $data->jenisPsb,
      //                 $data->jenis_layanan,
      //                 '~',
      //                 $data->typeont.' ~ '.$data->typestb,
      //                 $data->snont.' ~ '.$data->snstb.
      //                 '~',
      //                 $data->dc_preconn ? : $data->dc_roll ? : '~',
      //                 $data->patchcore,
      //                 $data->utp,
      //                 $data->tiang,
      //                 $data->pullstrap,
      //                 $data->conduit,
      //                 $data->tray_cable,
      //                 strtoupper($data->nama_odp),
      //                 $data->kordinat_pelanggan,
      //                 $data->area_migrasi,
      //                 $data->mitra,
      //                 $data->orderStatus,
      //                 $data->laporan_status
      //               ]);
      //           }
      //       });
      //   })->export('xlsx');

      return view('dashboard.provisioningList',compact('title','data','tgl','jenis','status','so'));
    }

    public function provisioningListMitra($jenis,$status,$tglAwal,$tglAkhir){
      $data = DashboardModel::provisioningListMitra($jenis,$status,$tglAwal,$tglAkhir);
      $title = "PROVISIONING MITRA";
      $so = "NULL";

      return view('dashboard.provisioningList2',compact('title','data','tgl','tglAwal','tglAkhir','jenis','status','so'));
    }

    public function provisioningListMitraX($jenis,$tglAwal,$tglAkhir){
      $data = DashboardModel::provisioningListMitraX($jenis,$tglAwal,$tglAkhir);
      $title = "PROVISIONING MITRA";
      $so = "NULL";

      return view('dashboard.provisioningList2',compact('title','data','tgl','tglAwal','tglAkhir','jenis','status','so'));
    }

    public function new_rekon_prov($tglAwal,$tglAkhir){

      $data = DashboardModel::new_rekon($tglAwal,$tglAkhir);

      return view('dashboard.new_rekon',compact('data','tglAwal','tglAkhir'));
    }

    public function migrasiListMitra($jenis,$tgl){
      $data = DashboardModel::migrasiListMitra($jenis,$tgl);
      $title = "MIGRASI MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList2',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasiListMitraMs2n($jenis,$tgl){
      $data = DashboardModel::migrasiListMitraMs2n($jenis,$tgl);
      $title = "MIGRASI MITRA MS2N";
      $so = "NULL";
      return view('dashboard.provisioningList2',compact('title','data','tgl','jenis','status','so'));
    }


    public function assuranceListMitra($jenis,$tglNow){
      $data = DashboardModel::assuranceListMitra($jenis,$tglNow);
      $title = "ASSURANCE MITRA";
      $so = "NULL";
      return view('dashboard.assuranceListMitraS',compact('title','data','tglNow','jenis','status','so'));
    }

    public function assuranceListMitrawithexcel($jenis,$tglNow){
      $data = DashboardModel::assuranceListMitra($jenis,$tglNow);
      $title = "ASSURANCE MITRA";
      $so = "NULL";
      return view('dashboard.assuranceListMitra',compact('title','data','tglNow','jenis','status','so'));
    }

    public function provisioning1($jenis){
      $query = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-07%" GROUP BY datex');
      $juni = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-06%" GROUP BY datex');
      $mei = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-05%" GROUP BY datex');
      $april = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-04%" GROUP BY datex');
      $maret = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-03%" GROUP BY datex');
      $februari = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a WHERE orderDatePs LIKE "2017-02%" GROUP BY datex');
      $januari = DB::SELECT('select date_format(orderDatePs,"%d") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a LEFT JOIN mdf b ON a.sto = b.mdf WHERE b.area <> "" AND  a.orderDatePs LIKE "2017-01%" GROUP BY datex');
      $ytd = DB::SELECT('select date_format(orderDatePs,"%M") as datex, count(*) as jumlah from Data_Pelanggan_Starclick a LEFT JOIN mdf b ON a.sto = b.mdf Where b.area <> "" AND a.orderDatePs LIKE "2017%" group by datex ORDER BY a.orderDatePs');
      return view('dashboard.provisioning1',compact('jenis','query','juni','mei','april','maret','februari','januari','ytd'));
    }

    public function assurance()
    {

      $date = Input::get('date');
      $witel = Input::get('witel');
      $date_now = date('Y-m-d H:i:s');

      switch ($witel) {
        case 'KALSEL':
          $tgl = date('d-m-Y', strtotime($date));
          // $getRocActive = DashboardModel::RocActive();
          $getAction = DashboardModel::Action($date);
          $getStatus = DashboardModel::Status($date);
          $getTiket = DashboardModel::Tiket();
          $getTiket2 = DashboardModel::Tiket2();
          $getTiket_INT = DashboardModel::Tiket_INT($date);
          $getTiket_OpenINT = DashboardModel::assuranceOpen("INT_SEKTOR");
          // $getNonaterobyPic = DashboardModel::nonatero_active_by_pic();
          $getRockbyUmur = DashboardModel::assuranceOpen("IN_SEKTOR");
          // $getRockbyUmur2 = DashboardModel::rock_active_by_umur_myi();
          $getAverageClose = DashboardModel::averageClose($date);
          $getSisaTiket = DashboardModel::sisaTiketNossa();
          $getManjaManual = DashboardModel::manjaManual();
          $getGaul = DashboardModel::get_gaul($tgl);
          $getSaldoUnspec = DashboardModel::getSaldoUnspec($date);
          $get_dailyreport = DashboardModel::reportDailyClosingOrder();
          $allOpenTiket = DashboardModel::allOpenTiket($witel,$date);
          $get_sektor = DashboardModel::get_sektor($date);

          $getOpen_Hero_IN = DashboardModel::assuranceOpen("IN_HERO");
          $getOpen_Hero_INT = DashboardModel::assuranceOpen("INT_HERO");

          return view('dashboard.assurance',compact('tgl','date_now','getSaldoUnspec','getTiket_OpenINT','getTiket_INT','get_sektor','getRockbyUmur','getRockbyUmur2','getNonatero','date','getTiket2','getTiket','getStatus','jenis','getRocActive','getAction','getAverageClose','getGaul','getSisaTiket','getManjaManual','allOpenTiket','witel', 'getOpen_Hero_IN', 'getOpen_Hero_INT', 'get_dailyreport'));
          break;

        case 'BALIKPAPAN':
          $allOpenTiket = DashboardModel::allOpenTiket($witel,$date);
          $getData = DashboardModel::assuranceBPP($date);

          return view('dashboard.assurance',compact('getData','allOpenTiket','date','witel'));
          break;
      }
    }

    public function reportDailyClosingOrderDetail()
    {
      $sektor = Input::get('sektor');
      $status = Input::get('status');

      $data = DashboardModel::reportDailyClosingOrderDetail($sektor, $status);

      return view('assurance.reportDailyClosingOrderDetail', compact('sektor', 'status', 'data'));
    }

    public function sisaTiketNossaPublic(){

      $date = date('Y-m-d');
      $getSisaTiket = DashboardModel::sisaTiketNossa();
      $getData = DashboardModel::manjaManual();

      return view('dashboard.sisaTiketNossaPublic', compact('getSisaTiket','getData','date'));
    }

    public function sisaTiketNossaDetail($ioan,$durasi)
    {
      $getData = DashboardModel::sisaTiketNossaDetail($ioan,$durasi);

      return view('dashboard.sisaSaldoTiket', compact('getData','ioan','durasi'));
    }

    public function ttrManualManja($sektor,$durasi)
    {
      $getData = DashboardModel::ttrManualManja($sektor,$durasi);

      return view('dashboard.ttrManualManja',compact('getData','sektor','durasi'));
    }

    public function saldoUnspec($sektor,$order,$date){

      $getData = DashboardModel::SaldoUnspec($sektor,$order,$date);

      return view('dashboard.saldoUnspec',compact('getData','sektor','order','date'));
    }

    public function gaul_detil($sektor, $date){

      $getData = DashboardModel::gaul_detil($sektor, $date);
      $view = '0';

      return view('dashboard.gaulDetil',compact('getData','sektor','date','view'));
    }

    public function gaul_detil_count($count, $inet){

      $getData = DashboardModel::gaul_detil_count($count, $inet);
      $view = '1';

      return view('dashboard.gaulDetil',compact('getData','count','inet','view'));
    }

    public function averageCloseDetail($date, $chatID, $order, $status){

      $getData = DashboardModel::averageCloseDetail($date, $chatID, $order , $status);

      return view('dashboard.averageCloseDetail',Compact('getData','date','chatID','order','status'));
    }

    public function assranceListRoc($sektor, $status){
        $getData = DashboardModel::Tiket2List($sektor, $status);

        return view('dashboard.ListActiveRock',compact('getData'));

    }

    public function assranceListINT($sektor, $status, $date){
      $getData = DashboardModel::Tiket2ListINT($sektor, $status, $date);

      return view('dashboard.ListActiveRock',compact('getData'));

  }

    public function assuranceList($date,$status){

      $getAssuranceList = DashboardModel::getAssuranceListx($date,$status);

      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }

    public function assuranceList2($date,$status,$area){
      $getAssuranceList = DashboardModel::getAssuranceList2($date,$status,$area);
      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }
    public function assuranceListbyAction($date,$status,$source,$sektor){
      $getAssuranceList = DashboardModel::getAssuranceListbyAction($date,$status,$source,$sektor);
      return view('dashboard.assuranceList',compact('date','status','getAssuranceList'));
    }

    public function MBSP(){
      $GetMBSP = DashboardModel::GetQuery("MBSP",NULL);
      return view('dashboard.mbsp',compact('GetMBSP'));
    }

    public function GetList($jenis, $status, $area){
      $GetList = DashboardModel::GetQueryList($jenis,$status,$area);
      return view('dashboard.list',compact('GetList'));
    }

    public function dashboardTelco($id){
      $tgl = date("Y-m-d");
      $kehadiran = DB::table('a2s_kehadiran')->where('tgl', $id)->orderBy('div', 'asc')->get();
      $dash = "";
      $lastDiv = '';
      foreach($kehadiran as $row) {
        if($row->div == $lastDiv){
          $dash[count($dash)-1]['total'] += 1;
          if($row->jadwal == 'MASUK')
            $dash[count($dash)-1]['masuk'] += 1;
          if($row->hadir == 'ABSEN')
            $dash[count($dash)-1]['absen'] += 1;
          else
            $dash[count($dash)-1]['tdk_absen'] += 1;
        }
        else {
          $dash[] = array("div"=>$row->div,"total"=>0, "masuk"=>0,"absen"=>0,"tdk_absen"=>0);
          $dash[count($dash)-1]['total'] += 1;
          if($row->jadwal == 'MASUK')
            $dash[count($dash)-1]['masuk'] += 1;
          if($row->hadir == 'ABSEN')
            $dash[count($dash)-1]['absen'] += 1;
          else
            $dash[count($dash)-1]['tdk_absen'] += 1;
          $lastDiv = $row->div;
        }
      }
      $prov = DB::TABLE('time_by_time_kpro')->orderBy('id', 'desc')->first();
      $asr = DB::TABLE('time_by_time_mdashboard')->orderBy('id', 'desc')->first();
      $pi = DB::TABLE('Data_Pelanggan_Starclick_Pi')->count();
      //dd($pi);
      $ndstb = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('Data_Pelanggan_Starclick.jenisPsb', "MO|IPTV")
        ->orderBy('laporan_status', 'desc')
        ->get();
      $ogp = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('dispatch_teknisi.dispatch_by', 2)
        ->where('psb_laporan.status_laporan', 5)
        ->count();
      $tc = DB::table('dispatch_teknisi')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.tgl', date("Y-m-d"))
        ->where('dispatch_teknisi.dispatch_by', 2)
        ->where('psb_laporan.status_laporan', 1)
        ->count();
      //dd($ndstb);
      $nd = array();
      $lastNama = 'asd';
      $no=0;
      $head = array();
      foreach ($ndstb as $no => $m){
        if(!empty($m->sto)){
          $head[] = $m->sto;
        }
      }
      $head = array_unique($head);
      foreach ($ndstb as $no => $m){
        if($lastNama == $m->laporan_status){
          $nd[count($nd)-1]['STO'][$m->sto] += 1;
          $no++;
        }else{
          $nd[] = array("status" => $m->laporan_status,"STO" => array());
          foreach($head as $h){
            if($h == $m->sto){
              $nd[count($nd)-1]['STO'][$h] = 1;
            }else{
              $nd[count($nd)-1]['STO'][$h] = 0;
            }
          }
          $no=0;
        }
        $lastNama = $m->laporan_status;
      }
      //dd($data);
      $sync = DB::table('f_sync')->orderBy('id', 'desc')->first();

      $da = DB::select('SELECT *,b.source_name,
        (select value from f_master where kategori_id = a.id and f_witel_id=3 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALSEL,
        (select value from f_master where kategori_id = a.id and f_witel_id=2 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALTENG,
        (select value from f_master where kategori_id = a.id and f_witel_id=1 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALBAR,
        (select value from f_master where kategori_id = a.id and f_witel_id=4 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as BALIKPAPAN,
        (select value from f_master where kategori_id = a.id and f_witel_id=5 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as SAMARINDA,
        (select value from f_master where kategori_id = a.id and f_witel_id=6 and ts like "'.$id.'%" order by f_sync_id desc limit 0, 1) as KALTARA
        FROM  f_kategori a LEFT JOIN f_source b on a.f_source_id = b.id
        WHERE urutan !=0 order by urutan asc');
      //dd($da);
      return view('laporan.dashboardTelco', compact('dash', 'prov', 'asr', 'pi', 'nd', 'head', 'ogp', 'tc', 'da', 'sync'));
    }
    public function detilTelco($id, $div ,$jadwal, $hadir){
      $detil = DB::table('a2s_kehadiran')->where('tgl', $id)
        ->when($div, function ($query) use ($div) {
            return $query->where('div', $div);
        })
        ->when($jadwal, function ($query) use ($jadwal) {
            return $query->where('jadwal', $jadwal);
        })
        ->when($hadir, function ($query) use ($hadir) {
            return $query->where('hadir', $hadir);
        })
        ->get();
      return view('laporan.kehadiranDetil', compact('detil'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function provisioningDua($tgl,$jenis, DashboardModel $DashboardModel){
      $data = DashboardModel::provisioning($jenis,$tgl);
      $reportPotensi = $DashboardModel->reportPotensiDua();

      return view('dashboard.provisioning2',compact('data','jenis','tgl','reportPotensi'));
    }

    public function reportUndispatch()
    {
      exec('cd ..;php artisan sendReportUndispacth> /dev/null &');
      // $data = Tele::sendReportUndispacth();
      // dd($data);
    }

    public function dashboardGuarante($tgl)
    {
       $getAction = DashboardModel::ActionGuarante($tgl);
       return view('dashboard.fulfillmentGuarante',compact('getAction', 'tgl'));
    }

    public function guaranteListbyAction($date,$status){
      $getGuaranteList = DashboardModel::getGuaranteListbyAction($date,$status);
      return view('dashboard.guarantelist',compact('date','status','getGuaranteList'));
    }

    public function rekonSebelumRfc($date){
      $data = DashboardModel::getRekon($date);
      $rekonMitra = DashboardModel::rekonMitra($date);
      $datams2n = DashboardModel::ms2n($date);
      $datams2nmigrasi = DashboardModel::ms2nmigrasi($date);
      return view('dashboard.rekonSebelumRfc',compact('date','data','rekonMitra','datams2n','datams2nmigrasi'));
    }

    public function provisioningListMitraSebelumRfc($jenis,$tgl){
      $data = DashboardModel::provisioningListMitraSebelumRfc($jenis,$tgl);
      $title = "PROVISIONING MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList3',compact('title','data','tgl','jenis','status','so'));
    }

    public function migrasiListMitraSebelumRfc($jenis,$tgl){
      $data = DashboardModel::migrasiListMitraSebelumRfc($jenis,$tgl);
      $title = "MIGRASI MITRA";
      $so = "NULL";
      return view('dashboard.provisioningList3',compact('title','data','tgl','jenis','status','so'));
    }

    public function progressProv($date){
      $get_month = explode('-',$date);

      $query = DB::SELECT('
        SELECT
          f.title,
          f.chat_id,
          count(*) as jumlah,
          SUM(CASE WHEN (e.grup = "SISA" OR d.id IS NULL) THEN 1 ELSE 0 END) as NP,
          SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
          SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
          SUM(CASE WHEN e.grup = "HR" THEN 1 ELSE 0 END) as HR,
          SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
          SUM(CASE WHEN DATE(ne.orderDatePs) = "'.$date.'" AND ne.orderStatusId = 1500 THEN 1 ELSE 0 END) as PS,
          SUM(CASE WHEN DATE(ne.orderDatePs) < "'.$date.'" AND DATE(ne.orderDatePs) <> "" AND d.status_laporan = 1 THEN 1 ELSE 0 END) as PSmin,
          SUM(CASE WHEN d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'" THEN 1 ELSE 0 END) as UP
        FROM
        dispatch_teknisi c
        LEFT JOIN Data_Pelanggan_Starclick ne ON ne.orderId = c.Ndem
        LEFT JOIN mdf a  ON a.mdf = ne.sto
        LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
        LEFT JOIN regu g ON c.id_regu = g.id_regu
        LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
        WHERE
          (
            e.grup IN ("SISA","KP","KT","OGP","HR") OR
            d.id IS NULL OR
            (d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'")
          )
          AND (ne.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET"))
          AND ne.orderId <> ""
          AND YEAR(c.tgl) >= "2020"
          AND MONTH(c.tgl) = "'.$get_month[1].'"
        GROUP BY f.title
        ORDER BY jumlah DESC
      ');

      $query2 = DB::SELECT('
        SELECT
          f.title,
          f.chat_id,
          count(*) as jumlah,
          SUM(CASE WHEN (e.grup = "SISA" OR d.id IS NULL) THEN 1 ELSE 0 END) as NP,
          SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
          SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
          SUM(CASE WHEN e.grup = "HR" THEN 1 ELSE 0 END) as HR,
          SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
          SUM(CASE WHEN d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'" THEN 1 ELSE 0 END) as UP
        FROM
        dispatch_teknisi c
        LEFT JOIN psb_myir_wo ne ON ne.myir = c.Ndem
        LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
        LEFT JOIN regu g ON c.id_regu = g.id_regu
        LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
        WHERE
          (
            e.grup IN ("SISA","KP","KT","OGP","HR") OR
            d.id IS NULL OR
            (d.status_laporan = 1 AND date(d.modified_at) = "'.$date.'")
          )
          AND ne.myir <> ""
          AND YEAR(c.tgl) >= "2020"
          AND MONTH(c.tgl) = "'.$get_month[1].'"
        GROUP BY f.title
        ORDER BY jumlah DESC
      ');
      return view('dashboard.progressprov',compact('date','query','query2'));
    }

    public function actcomp($group, $datel)
    {
      switch ($group) {
        case 'SEKTOR':

        $select = 'gt.title as area,';

          switch ($datel) {
            case 'ALL':
              $where_datel = '';
              break;

            case 'NON AREA':
              $where_datel = 'AND gt.title IS NULL';
              break;

            default:
              $where_datel = 'AND gt.title = "' . $datel . '"';
              break;
          }
          break;

        case 'PROV':

        $select = 'aa.sektor_prov as area,';

          switch ($datel) {
            case 'ALL':
              $where_datel = '';
              break;

            case 'NON AREA':
              $where_datel = 'AND aa.sektor_prov IS NULL';
              break;

            default:
              $where_datel = 'AND aa.sektor_prov = "' . $datel . '"';
              break;
          }
        break;

        case 'DATEL':

        $select = 'aa.datel as area,';

        switch ($datel) {
          case 'ALL':
            $where_datel = '';
            break;

          case 'NON AREA':
            $where_datel = 'AND aa.datel IS NULL';
            break;

          default:
            $where_datel = 'AND aa.datel = "' . $datel . '"';
            break;
        }
        break;

        case 'HERO':

        $select = 'aa.HERO as area,';

          switch ($datel) {
            case 'ALL':
              $where_datel = '';
              break;

            case 'NON AREA':
              $where_datel = 'AND aa.HERO IS NULL';
              break;

            default:
              $where_datel = 'AND aa.HERO = "' . $datel . '"';
              break;
          }
          break;

          case 'EGBIS':

            $select = 'aa.sektor_prov as area,';
    
              switch ($datel) {
                case 'ALL':
                  $where_datel = 'AND dps.provider NOT LIKE "DCS%"';
                  break;
    
                case 'NON AREA':
                  $where_datel = 'AND dps.provider NOT LIKE "DCS%" AND aa.sektor_prov IS NULL';
                  break;
    
                default:
                  $where_datel = 'AND dps.provider NOT LIKE "DCS%" AND aa.sektor_prov = "' . $datel . '"';
                  break;
              }
            break;
      }

      $query = DB::SELECT('
          SELECT
          *,
          ' .$select . '
          cda.WFM_ID as cda_wfm_id
          FROM Data_Pelanggan_Starclick dpsa
          LEFT JOIN comparin_data_api cda ON dpsa.orderIdInteger = cda.ORDER_ID
          LEFT JOIN dispatch_teknisi dt ON dpsa.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN ibooster i ON dpsa.orderId = i.order_id
          WHERE
          YEAR(dpsa.orderDate) = "' . date('Y') . '"
          AND dpsa.jenisPsb LIKE "AO%"
          AND dpsa.orderStatusId IN ("1300","1205")
          ' . $where_datel . '
      ');
      //   $wfm = array();
      // foreach ($query as $result){
      //   if (session('auth')->id_user=="91153709" || session('auth')->id_user=="94150248" || session('auth')->id_user=="22031995") {
      //   $wfm[$result->orderId] = $this->get_wfmid($result->orderId);
      //   }
      // }
      return view('dashboard.listWO1',compact('query','datel','wfm'));
    }

    public function get_wfmid($sc)
    {
      $startclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();

      $ch = curl_init();
      //get session
      curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_exec($ch);

      curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/ordersc/load-order');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIE, $startclick->cookies);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_POST, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $data = '{"scid":"'.$sc.'"}';
      curl_setopt($ch, CURLOPT_POSTFIELDS, "code=0&giud=0&data=".urlencode($data));
      $result = curl_exec($ch);

      curl_close($ch);
      $detail = json_decode($result);

      $return = $detail->data->detail->appointment->xs9;

      return $return;
    }

    public function dashboardProvSektor($tgl){
      $data   = DashboardModel::scbe($tgl);
      $dataUp2 = DashboardModel::scbeUP2($tgl);
      $dataReportPsStar     = DashboardModel::reportPotensiPsStar($tgl);
      $dataReporPsStarMo    = DashboardModel::reportPotensiPsStarMo($tgl);
      $dataReportTransaksi  = DashboardModel::dashboardTransaksi($tgl);
      $dataReportPsStarUp   = DashboardModel::reportPotensiPsStarUp($tgl);
      $dataKendala = DashboardModel::scbeKendala(date('Y-m',strtotime($tgl)));
      $bulan = $this->bulanIndo(date('m',strtotime($tgl))).' '.date('Y',strtotime($tgl));
      $dataSmartIndome      = DashboardModel::dashboardTransaksiSmartIndihome($tgl);
      $dataKendalaPt1 = DashboardModel::scbeKendalaPt1(date('Y',strtotime($tgl)));
      $get_sektor = DB::table('group_telegram')->where('sms_active_prov','1')->get();
      if (count($dataKendalaPt1)<>0){
          array_push($dataKendala,$dataKendalaPt1[0]);
      }

      // grafik
      $total = 0;
      $label = [];
      foreach($data as $dt){
            array_push($label, $dt->laporan_status);
            $total += $dt->jumlah;
      };

      $nilai = [];
      foreach($data as $dt){
        array_push($nilai, $dt->jumlah);
      };

      return view('scbe.dashboardProvSektor',compact('get_sektor','datel','data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp2', 'dataKendala', 'bulan', 'dataSmartIndome', 'label','nilai'));

    }

    public function listOrder($datel,$jenis){
      if ($jenis=="SISA"){
        $query = DB::SELECT('
        SELECT *
        FROM Data_Pelanggan_Starclick aa
        LEFT JOIN dispatch_teknisi b ON aa.orderId = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.`id_tbl_mj`
        LEFT JOIN psb_laporan_status d ON c.`status_laporan` = d.`laporan_status_id`
         LEFT JOIN maintenance_datel g ON aa.sto = g.sto
        WHERE
        aa.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND
        DATE(aa.`orderDate`) >= "2020-03-01" AND
        (d.`grup` IN ("SISA","OGP","HR")) AND g.datel="'.$datel.'"
        ');
      }
      return view('dispatch.listorder',compact('jenis','datel','query'));
    }

    public function dashboardScbe3($tgl){
      $witel = session('witel');
      $datel = DB::table('maintenance_datel')->where('witel',session('witel'));
      if ($witel=="KALSEL"){
          $data   = DashboardModel::scbe($tgl);
          $data_after   = DashboardModel::scbe_after($tgl);
          // $dataUp = DashboardModel::scbeUp($tgl);
          $dataReportPsStar     = DashboardModel::reportPotensiPsStar($tgl);
          $dataReporPsStarMo    = DashboardModel::reportPotensiPsStarMo($tgl);
          $dataReportTransaksi  = DashboardModel::dashboardTransaksi($tgl);
          $dataReportPsStarUp   = DashboardModel::reportPotensiPsStarUp($tgl);
          $potensiPS   = DashboardModel::new_potensi_ps3($tgl);
          $dataKendala = DashboardModel::scbeKendala_pra(date('Y-m',strtotime($tgl)));
          $dataKendala_after = DashboardModel::scbeKendala_after(date('Y-m',strtotime($tgl)));
          $bulan = $this->bulanIndo(date('m',strtotime($tgl))).' '.date('Y',strtotime($tgl));
          $dataSmartIndome      = DashboardModel::dashboardTransaksiSmartIndihome($tgl);
          $dataKendalaPt1 = DashboardModel::scbeKendalaPt1(date('Y',strtotime($tgl)));
          if (count($dataKendalaPt1)<>0){
              array_push($dataKendala,$dataKendalaPt1[0]);
          }

          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          $witelx = DB::SELECT('select * from maintenance_datel where witel = "KALSEL" group by datel');

          return view('scbe.dashboardScbe3',compact('potensiPS','data_after','dataKendala_after','witelx','datel','data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataKendala', 'bulan', 'dataSmartIndome', 'label','nilai'));
      }
      else{
          $data               = DashboardModel::scbeWitel($tgl,null,session('witel'));
          // $data_no_update     = DashboardModel::scbeWitel_no_update($tgl,null,session('witel'));
          $get_datel          = WitelModel::get_datel(session('witel'));
          $get_potensi_ps_up  = DashboardModel::get_potensi_ps_up($tgl,null,session('witel'));
          $get_potensi_ps     = DashboardModel::get_potensi_ps($tgl,null,session('witel'));
          $dataKendala = DashboardModel::scbeKendalaWitel(date('Y-m'));
          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          return view('scbe.dashboardScbeBalikpapan',compact('dataKendala','get_potensi_ps','get_potensi_ps_up','get_datel','data', 'label', 'nilai', 'tgl'));

      }
    }

    public function dashboardScbe($tgl){
      $witel = session('witel');
      $datel = DB::table('maintenance_datel')->where('witel',session('witel'));
      if ($witel=="KALSEL")
      {
          $data   = DashboardModel::scbe($tgl);
          $data_after   = DashboardModel::scbe_after($tgl);
          // $dataUp = DashboardModel::scbeUp($tgl);
          $dataReportPsStar     = DashboardModel::reportPotensiPsStar($tgl);
          $dataReporPsStarMo    = DashboardModel::reportPotensiPsStarMo($tgl);
          $dataReportTransaksi  = DashboardModel::dashboardTransaksi($tgl);
          $dataReportPsStarUp   = DashboardModel::reportPotensiPsStarUp($tgl);
          $potensiPS   = DashboardModel::new_potensi_ps($tgl);
          // $mappingSCAO   = DashboardModel::mappingScAO($tgl);
          $dataKendala = DashboardModel::scbeKendala_pra($tgl);
          $pt2_master = DashboardModel::pt2_master($tgl);
          $dataKendala_kurang3hari = DashboardModel::scbeKendala_pra_range('kurang3hari');
          $dataKendala_x3sd7hari = DashboardModel::scbeKendala_pra_range('x3sd7hari');
          $dataKendala_lebih7hari = DashboardModel::scbeKendala_pra_range('lebih7hari');
          // $dataKendala_after = DashboardModel::scbeKendala_after($tgl);
          $bulan = $this->bulanIndo(date('m',strtotime($tgl))).' '.date('Y',strtotime($tgl));
          $dataSmartIndome      = DashboardModel::dashboardTransaksiSmartIndihome($tgl);
          $dataKendalaPt1 = DashboardModel::scbeKendalaPt1(date('Y',strtotime($tgl)));
          if (count($dataKendalaPt1)<>0){
              array_push($dataKendala,$dataKendalaPt1[0]);
          }

          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          $witelx = DB::SELECT('select * from maintenance_datel where witel = "KALSEL" group by datel');

          return view('scbe.dashboardScbe',compact('mappingSCAO','pt2_master','dataKendala_lebih7hari','dataKendala_x3sd7hari','dataKendala_kurang3hari','potensiPS','data_after','witelx','datel','data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataKendala', 'bulan', 'dataSmartIndome', 'label','nilai'));
      } else {
          $data               = DashboardModel::scbeWitel($tgl,null,session('witel'));
          // $data_no_update     = DashboardModel::scbeWitel_no_update($tgl,null,session('witel'));
          $get_datel          = WitelModel::get_datel(session('witel'));
          $get_potensi_ps_up  = DashboardModel::get_potensi_ps_up($tgl,null,session('witel'));
          $get_potensi_ps     = DashboardModel::get_potensi_ps($tgl,null,session('witel'));
          $dataKendala = DashboardModel::scbeKendalaWitel(date('Y-m'));
          // grafik
          $total = 0;
          $label = [];
          foreach($data as $dt){
                array_push($label, $dt->laporan_status);
                $total += $dt->jumlah;
          };

          $nilai = [];
          foreach($data as $dt){
            array_push($nilai, $dt->jumlah);
          };

          return view('scbe.dashboardScbeBalikpapan',compact('dataKendala','get_potensi_ps','get_potensi_ps_up','get_datel','data', 'label', 'nilai', 'tgl'));

      }
    }

     public function dashboardScbeNew($tgl){
      $data   = DashboardModel::scbeNew($tgl);
      $dataUp = DashboardModel::scbeUP2($tgl);
      $dataReportPsStar     = DashboardModel::reportPotensiPsStar($tgl);
      $dataReportPsScbe     = DashboardModel::reportPotensiPsScbe($tgl);
      $dataReporPsStarMo    = DashboardModel::reportPotensiPsStarMo($tgl);
      $dataReportTransaksi  = DashboardModel::dashboardTransaksi($tgl);
      $dataReportPsStarUp   = DashboardModel::reportPotensiPsStarUp($tgl);

      return view('scbe.dashboardScbe2',compact('data', 'tgl', 'dataReportPsStar', 'dataReportPsScbe', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp'));
    }

    public function pt2($datel, $status, $tgl){

      $data = DashboardModel::pt2($datel, $status, $tgl);

      return view('scbe.pt2',compact('data','datel','status','tgl'));
    }

    public function scbeList($tgl, $jenis, $status, $so, $ket)
    {
        // $dataUp = '';
        // if ($status=='ALL' && $ket=='PROV'){
        //     $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so);
        // };

        // $year = date('Y');

        // if($tgl == $year)
        // {
        //   return back()->with('alerts',[['type' => 'danger', 'text' => 'Maaf Kami Kembalikan, Ada Indikasi Membuat Kinerja Server Menjadi Lambat']]);
        // }

        // dd($tgl, $jenis, $status, $so, $ket);
        $data = DashboardModel::scbeList_myir($tgl, $jenis, $status, $so, $ket);
        $title = "SCBE";

        return view('scbe.dashboardScbeListS',compact('data','data_myir','tgl','jenis','status','so','title','dataUp','ket'));
    }

    public function scbeListwithexcel($tgl,$jenis,$status,$so,$ket){
      $dataUp = '';
      // if ($status=='ALL' && $ket=='PROV'){
      //     $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so);
      // };

      $data = DashboardModel::scbeList_myir($tgl,$jenis,$status,$so,$ket);
      $title = "SCBE";

      return view('scbe.dashboardScbeList',compact('data','data_myir','tgl','jenis','status','so','title','dataUp','ket'));
  }

    public function scbeListAfter($tgl,$jenis,$status,$so,$ket){
        $dataUp = '';
        // if ($status=='ALL' && $ket=='PROV'){
        //     $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so);
        // };

        // $data = DashboardModel::scbeList_after($tgl,$jenis,$status,$so,$ket);
        $title = "SCBE";

        return view('errors.maintenance_site',compact('data','data_myir','tgl','jenis','status','so','title','dataUp','ket'));
    }


    public function potensiPsList($tgl,$jenis,$status,$so){
        $data   = DashboardModel::potensiPs($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO";

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListPs($tgl,$jenis,$status,$so){
        $data   = DashboardModel::potensiPsUp($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO";

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsScbeList($tgl,$jenis,$status,$so){
        $data = DashboardModel::potensiPsScbe($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS";
        // dd($data);
        return view('scbe.potensiPsScbe',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListMo($tgl,$jenis,$status,$so,$jnsPsb){
        $data   = DashboardModel::potensiPsMo($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS MO";

        // dd($data);
        return view('scbe.potensiPsMo',compact('data','tgl','jenis','status','so','title'));
    }

    public function transaksiScbeList($tgl,$jenis,$status,$so){
        $data = DashboardModel::reportDashboardTransaksi($tgl,$jenis,$status,$so);
        $title = "Report Dashboard transaksi";
        // dd($data);
        return view('scbe.transaksiScbe',compact('data','tgl','jenis','status','so','title'));
    }

    public function transaksiScbeListIndihome($tgl,$jenis,$status,$so){
        $data = DashboardModel::reportDashboardTransaksiSmartIndhome($tgl,$jenis,$status,$so);
        $title = "Report Dashboard Transaksi SmartIndihome";
        // dd($data);
        return view('scbe.transaksiSmartIndihome',compact('data','tgl','jenis','status','so','title'));
    }

    public function dashboardTeritori($tgl, Request $req){
        $nik = session('auth')->id_user;
        $dataKetDashboard = DashboardModel::getKetDashboard($nik);

        $data = '';
        $group_telegram = HomeModel::group_telegram($nik);
        $ketDashboard = '';

        if (count($dataKetDashboard)<>0){
            $ketDashboard = $dataKetDashboard->ket_dashboard;
            $data = DashboardModel::dashboardTeritory($tgl,$ketDashboard);

        }

        if ($req->has('sektor')){
            $ketDashboard = $req->input('sektor');
            $data = DashboardModel::dashboardTeritory($tgl,$ketDashboard);
        }

        return view('dashboard.dashboardTl',compact('data', 'nik', 'group_telegram','ketDashboard', 'tgl'));

    }

    public function dashboardTeritoriList($tgl,$jenis,$status,$so){
        $data = DashboardModel::listDashboardTeritori($tgl,$jenis,$status,$so);
        $title = 'Dashboard TL ';

        return view('dashboard.dashboardTlList',compact('data', 'title', 'tgl', 'jenis', 'status', 'so'));
    }

    public function scbeListTotal($tgl, $so){
        $dataUp       = DashboardModel::potensiPsUp($tgl,'ALL','ALL',$so);
        $dataComp     = DashboardModel::potensiPs($tgl,'ALL','ALL',$so);
        $title        = "Report Potensi PS AO TOTAL";
        $status       = '';
        // $dataScbe     = DashboardModel::potensiPsScbe($tgl,'ALL','potensiScbe',$so);

        return view('scbe.potensiPsTotal',compact('title', 'dataUp', 'dataComp', 'tgl', 'status'));
    }

    private function bulanIndo($bulan){
        if($bulan==1){
            $blnIndo = 'Januari';
        }
        elseif($bulan==2){
            $blnIndo = 'Februari';
        }
        elseif($bulan==3){
            $blnIndo = 'Maret';
        }
        elseif($bulan==4){
            $blnIndo = 'April';
        }
        elseif($bulan==5){
            $blnIndo = 'Mei';
        }
        elseif($bulan==6){
            $blnIndo = 'Juni';
        }
        elseif($bulan==7){
            $blnIndo = 'Juli';
        }
        elseif($bulan==8){
            $blnIndo = 'Agustus';
        }
        elseif($bulan==9){
            $blnIndo = 'September';
        }
        elseif($bulan==10){
            $blnIndo = 'Oktober';
        }
        elseif($bulan==11){
            $blnIndo = 'November';
        }
        elseif($bulan==12){
            $blnIndo = 'Desember';
        }
        else{
          $blnIndo = '';
        }

        return $blnIndo;
    }

    public function dashDispatchReboundary($tgl){
        $dataDispatch = DashboardModel::dashReboundaryDispatch($tgl);
        $progressReboundary = DashboardModel:: progressReboundary($tgl);

        return view ('reboundary.dashboard',compact('dataDispatch', 'tgl', 'progressReboundary'));
    }

    public function listDispatchReboundary($area, $tgl, $status){
        $data = DashboardModel::listDispatchReboundary($area, $tgl, $status);
        // dd($data);
        return view('reboundary.listDispatch',compact('data'));
    }

     public function reboundaryList($tgl,$jenis,$status,$so){
        $data = DashboardModel::reboundaryList($tgl,$jenis,$status,$so);
        $title = "Reboundary";

        return view('reboundary.dashboardReboundaryList',compact('data','tgl','jenis','status','so','title','dataUp','ket'));
    }

     public function dashboardScbeTw($tgl, $ket){
      $ketTw = null;
      $title = '';
      if ($ket=='tw1'){
        $awal  = date_create($tgl."/1/1");
        $akhir = date_create($tgl."/3/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 1';
      }
      elseif ($ket=='tw2'){
        $awal  = date_create($tgl."/4/1");
        $akhir = date_create($tgl."/6/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 2';
      }
      elseif ($ket=='tw3'){
        $awal  = date_create($tgl."/7/1");
        $akhir = date_create($tgl."/9/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 3';
      }
      elseif ($ket=='tw4'){
        $awal  = date_create($tgl."/10/1");
        $akhir = date_create($tgl."/12/31");

        $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
        $title = $tgl. ' TRIWULAN 4';
      }

      $data   = DashboardModel::scbe($tgl, $ketTw);
      $dataUp = DashboardModel::scbeUp($tgl, $ketTw);
      $dataReportPsStar     = DashboardModel::reportPotensiPsStar($tgl, $ketTw);
      $dataReportPsStarUp   = DashboardModel::reportPotensiPsStarUp($tgl, $ketTw);
      $dataReporPsStarMo    = DashboardModel::reportPotensiPsStarMo(date('Y-m-d'));
      $dataReportTransaksi  = DashboardModel::dashboardTransaksi(date('Y-m-d'));
      $dataKendala = DashboardModel::scbeKendala(date('Y-m'));
      $bulan = $this->bulanIndo(date('m')).' '.date('Y');
      $tgl2  = date('Y-m-d');

      // $dataReportPsScbe     = DashboardModel::reportPotensiPsScbe($tgl);
      // $dataTes = DashboardModel::reportPotensiPsStarFalloutWfm($tgl);
      // dd($dataTes);

      return view('scbe.dashboardScbeTw',compact('data', 'tgl', 'dataReportPsStar', 'dataReporPsStarMo', 'dataReportTransaksi', 'dataReportPsStarUp', 'dataUp', 'dataKendala', 'bulan', 'title', 'tgl2', 'ket'));
    }

     public function scbeListTw($tgl,$jenis,$status,$so,$ket,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        }

        $dataUp = '';
        if ($status=='ALL' && $ket=='PROV'){
            $dataUp = DashboardModel::provisioningList($tgl,$jenis,'UP',$so,$ketTw);
        };

        $data = DashboardModel::scbeList($tgl,$jenis,$status,$so,$ket,$ketTw);
        $title = "SCBE PERIODE ".$tgl.' '.$tw;

        return view('scbe.dashboardScbeList',compact('data','tgl','jenis','status','so','title','dataUp','ket'));
    }

    public function potensiPsListPsTw($tgl,$jenis,$status,$so,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $data   = DashboardModel::potensiPsUp($tgl,$jenis,$status,$so,$ketTw);
        $title = "Report Potensi PS AO PERIODE ".$tgl." ".$tw;

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function potensiPsListTw($tgl,$jenis,$status,$so,$ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $data   = DashboardModel::potensiPs($tgl,$jenis,$status,$so);
        $title = "Report Potensi PS AO PERIODE ".$tgl." ".$tw;

        // dd($data);
        return view('scbe.potensiPs',compact('data','tgl','jenis','status','so','title'));
    }

    public function scbeListTotalTw($tgl, $so, $ketTww){
        $ketTw = null;
        if ($ketTww=='tw1'){
          $awal  = date_create($tgl."/1/1");
          $akhir = date_create($tgl."/3/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 1';
        }
        elseif ($ketTww=='tw2'){
          $awal  = date_create($tgl."/4/1");
          $akhir = date_create($tgl."/6/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 2';
        }
        elseif ($ketTww=='tw3'){
          $awal  = date_create($tgl."/7/1");
          $akhir = date_create($tgl."/9/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 3';
        }
        elseif ($ketTww=='tw4'){
          $awal  = date_create($tgl."/10/1");
          $akhir = date_create($tgl."/12/31");

          $ketTw = date_format($awal, "Y-m-d").'_'.date_format($akhir, "Y-m-d");
          $tw = 'TRIWULAN 4';
        };

        $dataUp       = DashboardModel::potensiPsUp($tgl,'ALL','ALL',$so, $ketTw);
        $dataComp     = DashboardModel::potensiPs($tgl,'ALL','ALL',$so, $ketTw);
        $title        = "Report Potensi PS AO TOTAL PERIODE ".$tgl." ".$tw;
        $status       = '';
        // $dataScbe     = DashboardModel::potensiPsScbe($tgl,'ALL','potensiScbe',$so);

        return view('scbe.potensiPsTotal',compact('title', 'dataUp', 'dataComp', 'tgl', 'status'));
    }

    public function dashboadWifi($tgl){
        $data = DashboardModel::dashboadWifiId($tgl);
        $dataDatin = DashboardModel::dashboadDatin($tgl);

        $area   = array('INNER','BBR','KDG','BLC','TJL');
        $jumlah = count($area);
        for($aa=0; $aa<=$jumlah-1; $aa++){
            $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
            $enddate = date('Y-m-d');
            $datas = DB::SELECT('
            SELECT
                m.area_migrasi,
                SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND m.area_migrasi="'.$area[$aa].'"
            ');
            // simpan data ke table psb_dashboard_prov
            DB::table('psb_dashboard_ccan')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);
        }

        // matrix hd ccan

        $user = ['92153701','96154577','95151848','92153706','92157603','92154583'];
        $matrik = array();
        $list   = array();

        foreach ($user as $u){
            $list = [];
            $jml  = 0;
            $getData = DashboardModel::getMatrixCcan($tgl, $u);
            if ($getData){
                foreach ($getData as $d){
                    $list[] = [
                        'Ndem'  => $d->Ndem
                    ];
                };

                $jml = count($getData);
            };

            $nama = '';
            $getNama = DB::table('1_2_employee')->where('nik',$u)->first();
            if($getNama){
                $nama = $getNama->nama;
            };

            $matrik[] = [
                'nik'     => $u,
                'nama'    => $nama,
                'jumlah'  => $jml,
                'dataWo'  => $list
            ];

        };

        $dataundispatch = DB::table('psb_dashboard_ccan')->get();

        return view('dashboard.DashboardCcanWifiId',compact('data', 'tgl', 'dataDatin','dataundispatch', 'matrik'));
    }

     public function dashboadWifiAjax($ket){
        $where_ket = '';
        if ($ket<>'ALL'){
            $where_ket = 'AND dps.layanan LIKE "%'.$ket.'%"';
        };

        $area   = array('INNER','BBR','KDG','BLC','TJL');
        $jumlah = count($area);
        for($aa=0; $aa<=$jumlah-1; $aa++){
            $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
            $enddate = date('Y-m-d');
            $datas = DB::SELECT('
            SELECT
                m.area_migrasi,
                SUM(CASE WHEN dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND m.area_migrasi="'.$area[$aa].'"
                '.$where_ket.'
            ');

            // simpan data ke table psb_dashboard_prov
            DB::table('psb_dashboard_ccan')->where('area',$area[$aa])->update(['undispatch' => $datas[0]->jumlah_undispatch]);
        }

        $dataundispatch = DB::table('psb_dashboard_ccan')->get();

        if ($ket=="" || $ket=="ALL"){
            $jenisPilihan = "ALL";
        }
        else{
          $jenisPilihan = $ket;
        };

        return view('dashboard.DashboardCcanWifiIdAjax',compact('dataundispatch','jenisPilihan'));
    }

    public function listDetailWifiId($tgl, $status, $area){
        $data = DashboardModel::listWifiId($area, $tgl, $status);
        $title = 'List Dashboard WIFI.ID';

        return view('dashboard.listDashboardCcan',compact('area', 'tgl', 'status', 'data', 'title'));
    }

    public function listDetailDatin($tgl, $status, $area){
        $data = DashboardModel::listDatin($area, $tgl, $status);
        $title = 'List Dashboard DATIN';

        return view('dashboard.listDashboardCcan',compact('area', 'tgl', 'status', 'data', 'title'));
    }

    public function listundispatchCcan($tgl,$area,$ket){
        $data = DashboardModel::getListUndispatchCcan($tgl,$area,$ket);
        return view('dashboard.listUndispatchCcan',compact('data'));
    }


    public function chartKendalUp($tgl, Request $req){
       DB::table('psb_kendala_up')->truncate();

       $area = 'ALL';
       if ($req->has('area')){
          $area = $req->input('area');
       };

       $where_area = ' AND m.area_migrasi="'.$area.'"';
       if ($area=='ALL'){
          $where_area = '';
       };

       $sql = 'SELECT
              c.laporan_status_id, c.laporan_status, b.status_kendala,
              SUM(CASE WHEN f.orderStatus="Completed (PS)" THEN 1 ELSE 0 END) as jumlahPs,
              SUM(CASE WHEN f.orderStatus<>"Completed (PS)" THEN 1 ELSE 0 END) as jumlahBelumPs
            FROM
              dispatch_teknisi a
            LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem=f.orderId
            LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
            LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
            LEFT JOIN mdf m ON f.sto=m.mdf
            WHERE
              a.dispatch_by IS NULL  AND
              f.orderId<>"" AND
              b.status_kendala IS NOT NULL AND
              a.tgl like "%'.$tgl.'%"'
              .$where_area.'
            GROUP BY
              b.status_kendala ASC
            ';

      $data = DB::select($sql);
      foreach ($data as $dt){
          $simpan = [
              'status_laporan' => $dt->status_kendala,
              'jumlah'         => $dt->jumlahPs + $dt->jumlahBelumPs,
              'jumlahPs'       => $dt->jumlahPs,
              'jumlahBelumPs'  => $dt->jumlahBelumPs,
          ];

          DB::table('psb_kendala_up')->insert($simpan);
      }

      $sql2 = 'SELECT
              c.laporan_status_id, c.laporan_status, b.status_kendala,
              count(*) as jumlahBelumPs,
              0 as jumlahPs
            FROM
              dispatch_teknisi a
            LEFT JOIN psb_myir_wo f ON a.Ndem=f.myir
            LEFT JOIN psb_laporan b ON a.id=b.id_tbl_mj
            LEFT JOIN psb_laporan_status c ON b.status_laporan=c.laporan_status_id
            LEFT JOIN mdf m ON f.sto=m.mdf
            WHERE
              a.dispatch_by = 5  AND
              b.status_kendala IS NOT NULL AND
              a.tgl like "%'.$tgl.'%"'
              .$where_area.'
            GROUP BY
              b.status_kendala ASC
            ';

      $dataMyir = DB::select($sql2);
      foreach($dataMyir as $myir){
        $exist = DB::table('psb_kendala_up')->where('status_laporan',$myir->status_kendala)->first();
        if ($exist){
            DB::table('psb_kendala_up')->where('status_laporan',$myir->status_kendala)->update([
                'jumlah'    => $exist->jumlah + ($myir->jumlahPs + $myir->jumlahBelumPs),
                'jumlahPs'  => $exist->jumlahPs + $myir->jumlahPs,
                'jumlahBelumPs'  => $exist->jumlahBelumPs + $myir->jumlahBelumPs,

            ]);
        }
        else{
          $simpan = [
              'status_laporan' => $myir->status_kendala,
              'jumlah'         => $myir->jumlahPs + $myir->jumlahBelumPs,
              'jumlahPs'       => $myir->jumlahPs,
              'jumlahBelumPs'  => $myir->jumlahBelumPs,
          ];

          DB::table('psb_kendala_up')->insert($simpan);
        }
      }

      $status = [];
      $jumlah = [];
      $ps     = [];
      $belumPs= [];

      $getData = DB::table('psb_kendala_up')->orderBy('jumlah','DESC')->get();
      foreach($getData as $data){
          array_push($status, $data->status_laporan);
          array_push($jumlah, $data->jumlah);
          array_push($ps, $data->jumlahPs);
          array_push($belumPs, $data->jumlahBelumPs);
      };

     //  foreach($data as $dt){
     //      array_push($status, $dt->laporan_status);
     //      // array_push($jumlah , ['status'=>$dt->laporan_status, 'jumlah'=> $dt->jumlahPs + $dt->jumlahBelumPs]);
     //      array_push($jumlah , $dt->jumlahPs + $dt->jumlahBelumPs);
     //      array_push($ps,$dt->jumlahPs );
     //      array_push($belumPs,$dt->jumlahBelumPs);

     //  };

      return view('dashboard.DashboardKendalaUpChart',compact('status', 'jumlah', 'ps', 'belumPs', 'area'));
    }

    public function listPsDownload($tgl){
        $sql = 'SELECT
                  a.Ndem,
                  b.orderId,
                  b.internet,
                  b.alproname,
                  c.nama_odp
                FROM
                  dispatch_teknisi a
                LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
                LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
                WHERE
                  a.dispatch_by IS NULL AND
                  b.orderId<>"" AND
                  b.orderStatusId IN (7,52) AND
                  a.created_at="'.$tgl.'"
                ';
        $data = DB::select($sql);
        return view('dashboard.ListPsDownload',compact('data'));
    }

    public function showData($id){
        $data = DB::table('psb_laporan')->where('id_tbl_mj',$id)->first();
        return response()->json($data);
    }

    public function saveData(Request $req, $id){
        $this->validate($req,[
            'sc' => 'numeric'
        ],[
            'sc.numeric' => 'Diisi Angka'
        ]);


        DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
            'sc_unsc'       => $req->sc,
            'kcontact_unsc' => $req->kcontact
        ]);

        $getMyir = DB::table('dispatch_teknisi')->where('id',$id)->first();
        DB::table('Data_Pelanggan_UNSC')->where('MYIR_ID',$getMyir->Ndem)->update([
            'orderId' => $req->sc
        ]);

        return 'sukses';
    }

    public static function getPsStatusHr($bulan){
        if ($bulan=='today'){
            $bulan = date('Y-m');
        }

        $data = DashboardModel::getHrPs($bulan);
        $pesan = "LIST HR COMPLETED (PS)\n";
        $pesan .= "===========================\n\n";

        $head = array();
        foreach($data as $dt){
          if (!in_array($dt->title, $head)){
            array_push($head, $dt->title);
          }
        };

        foreach($head as $h){
            $pesan .= $h."\n";
            foreach ($data as $dt){
                if ($h==$dt->title){
                    $pesan .= $dt->Ndem.' '.$dt->uraian.' '.$dt->tglStatus."\n";
                };
            };
          $pesan .= "\n";
        };

        $pesan .= 'Total '.count($data);

        if(count($data)<>0){
          Telegram::sendMessage([
              'chat_id' => '-1001309386322',
              'text' => $pesan
          ]);
        };

        echo $pesan;
    }

    public function listPsbMo(){
        // $getData = DB::table('Data_Pelanggan_Starclick')
        //             ->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderId','=','dispatch_teknisi.Ndem')
        //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
        //             ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
        //             ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        //             ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
        //             ->select('dispatch_teknisi.Ndem','group_telegram.title','Data_Pelanggan_Starclick.sto','regu.uraian','dispatch_teknisi.jenis_layanan','Data_Pelanggan_Starclick.jenisPsb','Data_Pelanggan_Starclick.orderStatus','psb_laporan_status.laporan_status','dispatch_teknisi.tgl')
        //             ->where('dispatch_teknisi.tgl','LIKE','%2019%')
        //             ->orderBy('dispatch_teknisi.tgl','ASC')
        //             ->orderBy('psb_laporan.status_laporan','ASC')
        //             ->get();

        // $getData = DB::table('psb_myir_wo')
        //             ->leftJoin('dispatch_teknisi','psb_myir_wo.myir','=','dispatch_teknisi.Ndem')
        //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
        //             ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
        //             ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        //             ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
        //             ->select('dispatch_teknisi.Ndem','group_telegram.title','psb_myir_wo.sto','regu.uraian','dispatch_teknisi.jenis_layanan','psb_laporan_status.laporan_status','dispatch_teknisi.tgl')
        //             ->where('dispatch_teknisi.tgl','LIKE','%2019%')
        //             ->where('psb_myir_wo.ket','0')
        //             ->where('dispatch_teknisi.dispatch_by','5')
        //             ->orderBy('dispatch_teknisi.tgl','ASC')
        //             ->orderBy('psb_laporan.status_laporan','ASC')
        //             ->get();

        $sql = 'select dispatch_teknisi.Ndem, maintaince.no_tiket, dispatch_teknisi.tgl, psb_laporan.status_laporan, psb_laporan_status.laporan_status, maintaince.status, regu.uraian, group_telegram.title, Data_Pelanggan_Starclick.sto as stoStarclick, psb_myir_wo.sto as stoMyir
                from dispatch_teknisi
                LEFT JOIN Data_Pelanggan_Starclick ON dispatch_teknisi.Ndem = Data_Pelanggan_Starclick.orderId
                LEFT JOIN psb_myir_wo ON dispatch_teknisi.Ndem = psb_myir_wo.myir
                LEFT JOIN maintaince ON maintaince.no_tiket = dispatch_teknisi.Ndem
                LEFT JOIN psb_laporan ON dispatch_teknisi.id = psb_laporan.id_tbl_mj
                LEFT JOIN regu ON dispatch_teknisi.id_regu = regu.id_regu
                LEFT JOIN group_telegram ON regu.mainsector = group_telegram.chat_id
                LEFT JOIN psb_laporan_status ON psb_laporan.status_laporan = psb_laporan_status.laporan_status_id
                WHERE
                maintaince.status="close" AND
                dispatch_teknisi.tgl LIKE "%2019%" AND
                psb_laporan.status_laporan NOT IN ("1","37","38") AND
                maintaince.order_from="PSB" AND
                (dispatch_teknisi.dispatch_by IS NULL OR dispatch_teknisi.dispatch_by=5) AND
                (Data_Pelanggan_Starclick.orderId IS NULL OR psb_myir_wo.myir IS NULL)
                ORDER BY `dispatch_teknisi`.`Ndem`  DESC';

        $getData = DB::select($sql);

        return view('tool.listPsbMo',compact('getData'));
    }

    public function woPsQrcode($tgl){
        $data = DashboardModel::getWOPs($tgl);
        return view('dashboard.woPsQr',compact('data', 'tgl'));
    }

    public function woPsQrcodeDetail($tgl, $chatid, $status){
        $data = DashboardModel::getWoPsDetail($tgl, $chatid, $status);
        return view('dashboard.woPsQrDetail',compact('data'));
    }

    public function undispatchSC($orderDate)
    {
      $reguler = DashboardModel::undispatchSC('reguler', $orderDate);
      $backend_sc = DashboardModel::undispatchSC('backend_sc', $orderDate);
      $risma = DashboardModel::undispatchSC('risma', $orderDate);
      $ex_kendala = DashboardModel::undispatchSC('ex_kendala', $orderDate);

      return view('dashboard.undispatchSC', compact('orderDate', 'reguler', 'risma', 'backend_sc', 'ex_kendala'));
    }

    public function dashboardDismanlteOntPremium()
    {
      $type = Input::get('type');
      $date = Input::get('date');
      $dismantle_area = DashboardModel::dashboardDismanlteArea($type,$date);
      $dismantling_berhasil = DashboardModel::dismantlingBerhasil($date);
      $ont_premium = DashboardModel::dashboardOntPremium($date);

      return view('dashboard.DismanlteOntPremium', compact('type','date','dismantle_area','dismantling_berhasil','ont_premium'));
    }

    public function detailDismanlteOntPremium($kat, $group, $sektor, $type, $status, $date)
    {
      switch ($kat) {
        case 'DISMANTLE':

            $query = DashboardModel::detailDismanlte($group, $sektor, $type, $status, $date);
            return view('dashboard.detailDismanlte', compact('query', 'kat', 'group', 'sektor', 'type', 'status', 'date'));

          break;

        case 'ONTPREMIUM':

            $query = DashboardModel::detailOntPremium($group, $sektor, $type, $status, $date);
            return view('dashboard.detailOntPremium', compact('query', 'kat', 'group', 'sektor', 'type', 'status', 'date'));
          break;
      }
    }

    public function comparinData($type, $group, $area)
    {
      $getData = DashboardModel::comparinData($type, $group, $area);

      return view('scbe.comparinData', compact('getData', 'type', 'group', 'area'));
    }

    public function publicPSRE()
    {
      $log_psre = DB::table('selfie_kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();
      $data = DashboardModel::psreKpro('SEKTOR', 'REGULER');

      return view('dashboard.publicPSRE', compact('data', 'log_psre'));
    }

    public function racingPS()
    {
      // $start = Input::get('startDate');
      $start = "2021-08-01";
      // $end = Input::get('endDate');
      $end = "2021-12-31";
      $query = DashboardModel::racingPS($start, $end);
      // dd($start, $end, $query);
      // save to table temp
      // DB::table('racing_poin')->truncate();

      // foreach ($query as $nik => $result){
      //   $order_aovalid = @$result['jml_order_ao_nik1'] + @$result['jml_order_ao_nik2'];
      //   $order_solken = @$result['jml_order_sk_nik1'] + @$result['jml_order_sk_nik2'];
      //   $order_mo = @$result['jml_order_mo_nik1'] + @$result['jml_order_mo_nik2'];
      //   $order_dismantling = @$result['jml_order_dism_nik1'] + @$result['jml_order_dism_nik2'];
      //   $order_ffg = @$result['jml_order_ffg_nik1'] + @$result['jml_order_ffg_nik2'];
      //   $point_tech = ($order_aovalid * 4) + ($order_solken * 3) + ($order_mo * 2) + ($order_dismantling * 1) - ($order_ffg * 4);

      //   DB::table('racing_poin')->insert([
      //     'NIK' => $nik,
      //     'NAMA' => @$result['nama'],
      //     'AO_VALID' => $order_aovalid,
      //     'SOLUSI_KENDALA' => $order_solken,
      //     'MO' => $order_mo,
      //     'DISMANTLING' =>  $order_dismantling,
      //     'FFG' => $order_ffg,
      //     'POIN' => $point_tech
      //   ]);
      // }
      return view('dashboard.racingPS', compact('start','end','query'));
    }

    public static function updateRacingPS()
    {
      $start = "2021-08-01";
      $end = "2021-12-31";

      $query = DashboardModel::racingPS($start, $end);
      // dd(count($query), $query);

      DB::table('prov_point_tech')->truncate();

      foreach ($query as $k => $v) {
        $df[$k] = $v;
        $order_aovalid = @$v['jml_order_ao_nik1'] + @$v['jml_order_ao_nik2'];
        $order_solken = @$v['jml_order_sk_nik1'] + @$v['jml_order_sk_nik2'];
        $order_mo = @$v['jml_order_mo_nik1'] + @$v['jml_order_mo_nik2'];
        $order_dismantling = @$v['jml_order_dism_nik1'] + @$v['jml_order_dism_nik2'];
        $order_ffg = @$v['jml_order_ffg_nik1'] + @$v['jml_order_ffg_nik2'];
        $point_tech = ($order_aovalid * 4) + ($order_solken * 3) + ($order_mo * 2) + ($order_dismantling * 1) - ($order_ffg * 4);

        print_r("$k -- $point_tech ");

        $check_data = DB::table('prov_point_tech')->where('tech_nik', $k)->first();
        if (!$check_data)
        {
          DB::table('prov_point_tech')->insert([
            'tech_nik' => $k,
            'point_now' => $point_tech,
            'point_old' => $point_tech,
            'order_ao_valid' => $order_aovalid,
            'order_solusi_kendala' => $order_solken,
            'order_mo' => $order_mo,
            'order_dismantling' => $order_dismantling,
            'order_ffg' => $order_ffg,
          ]);
        } else {
          DB::table('prov_point_tech')->where('tech_nik', $k)->update([
            'point_old' => $point_tech,
            'order_ao_valid' => $order_aovalid,
            'order_solusi_kendala' => $order_solken,
            'order_mo' => $order_mo,
            'order_dismantling' => $order_dismantling,
            'order_ffg' => $order_ffg,
          ]);
        }

      $status = true;
      $vip = $reguler = 0;

      while ($status) {
        if(($point_tech - 200) > 0)
        {
          $jenis = 'vip';
        }
        elseif(($point_tech - 125) > 0)
        {
          $jenis = 'regular';
        }
        else
        {
          $status = false;
        }

        if ($point_tech - 200 < 0 && $point_tech - 125 < 0) {
          $status = false;
        } else {
          if ($jenis == 'vip') {
            if (($point_tech - 200) > 0) {
              $vip += 1;
              $point_tech = $point_tech - 200;
              $jenis = 'regular';
            }
          } elseif ($jenis == 'regular') {
            if ($point_tech - 125 > 0) {
              $reguler += 1;
              $point_tech = $point_tech - 125;
              $jenis = 'vip';
            }
          } else {
            $status = false;
          }
        }
      }

        DB::table('prov_point_tech')->where('tech_nik', $k)->update([
          'point_now' => $point_tech,
          'coupon_vip' => $vip,
          'coupon_reguler' => $reguler
        ]);

        print_r("Update Point $k \n");

        $data = DB::table('prov_point_tech')->where('tech_nik', $k)->first();

        // redeem ticket vip
        for($x=1; $x<=$vip ;$x++) {
          $get_code = $k.''.$data->id_point_tech;
          $redeem = md5(uniqid($get_code, true));

          DB::table('redeem_point_tech')->insert([
            'voucher_redeem' => $redeem,
            'status_ticket' => 'VIP',
            'nik_tech' => $k
          ]);
        }

        // redeem ticket reguler
        for ($x = 1; $x <= $reguler; $x++) {
          $get_code = $k . '' . $data->id_point_tech;
          $redeem = md5(uniqid($get_code, true));

          DB::table('redeem_point_tech')->insert([
            'voucher_redeem' => $redeem,
            'status_ticket' => 'REGULER',
            'nik_tech' => $k
          ]);
        }

      }


      print_r("Finish Update Point Racing PS !!! \n");
    }

    public function monitoring_nte_bot()
    {
      $status = Input::get('status');
      $date = Input::get('date');

      $getData = DashboardModel::monitoring_nte_lama($status, $date);

      return view('dashboard.monitoring_nte_lama', compact('status', 'date', 'getData'));
    }

    public function pickupOnline()
    {
      $source = Input::get('source');
      $witel = Input::get('witel');

      if ($source == null || $witel == null)
      {
        dd("Maaf silahkan isi parameter pada kolom url!");
      }

      switch ($witel) {
        case 'KALSEL':
          $type = 'SEKTOR';
          $witel_ms2n = 'BANJARMASIN';
          switch ($source) {
            case 'kpro':
              $witelx = $witel_ms2n;
              $data_sektor = DashboardModel::pickupOnline($type, $source, $witel_ms2n);

              $month = date('Y-m');
              $report_witel = DashboardModel::reportPickupOnline('witel', $witel_ms2n, $month);
              $report_sektor = DashboardModel::reportPickupOnline('sektor', $witel_ms2n, $month);
              break;
            case 'starclick':
              $witelx = $witel;
              $data_sektor = DashboardModel::pickupOnline($type, $source, $witel);
              break;
          }
          // $data_sto = DashboardModel::pickupOnline('STO', $source);
          break;

        case 'BALIKPAPAN':
          $type = 'STO';
          $witelx = $witel_ms2n = 'BALIKPAPAN';
          $data_sektor = DashboardModel::pickupOnline($type, $source, $witel_ms2n);
          $report_witel = [];
          $report_sektor = [];
          break;
      }

      $log_kpro = DB::table('provi_kpro_tr6')->select('last_grab')->where('witel', $witel_ms2n)->orderBy('last_grab', 'desc')->first();
      $log_tactical = DB::table('tacticalpro_tr6')->select('lastsync_at')->where('witel_ms2n', $witel)->orderBy('lastsync_at', 'desc')->first();

      if ($data_sektor == null)
      {
        $data_sektor = [];
        // $data_sto = [];
        $report = [];
      }

      return view('dashboard.pickupOnline', compact('source', 'witel', 'witel_ms2n', 'month', 'type', 'data_sto', 'data_sektor', 'log_kpro', 'log_tactical', 'witelx', 'report_witel', 'report_sektor'));
    }

    public function pickupOnlineDetail()
    {
      $source = Input::get('source');
      $witel = Input::get('witel');
      $type = Input::get('type');
      $area = Input::get('area');
      $status = Input::get('status');

      $getData = DashboardModel::pickupOnlineDetail($source, $witel, $type, $area, $status);

      return view('dashboard.pickupOnlineDetail', compact('source', 'witel', 'type', 'area', 'status', 'getData'));
    }

    public function reportQC2()
    {
      $area = Input::get('area');
      $source = Input::get('source');
      $start = Input::get('startDate');
      $end = Input::get('endDate');

      $log = DB::table('report_qc2_tr6')->orderBy('last_updated', 'desc')->select('last_updated')->first();
      $log_ut = DB::table('utonline_tr6')->orderBy('last_updated_at', 'desc')->select('last_updated_at')->first();
      $data = DashboardModel::reportQC2($area, $source, $start, $end);

      return view('dashboard.reportQC2', compact('source', 'area', 'start', 'end', 'log', 'log_ut', 'data'));
    }

    public function reportQC2Detail()
    {
      $area = Input::get('area');
      $source = Input::get('source');
      $group = Input::get('group');
      $start = Input::get('startDate');
      $end = Input::get('endDate');
      $status = Input::get('status');

      $getData = DashboardModel::reportQC2Detail($area, $source, $group, $start, $end, $status);

      return view('dashboard.reportQC2Detail', compact('source', 'area', 'group', 'start', 'end', 'status', 'getData'));
    }

    public function dashboardAddonComparin()
    {
      $start = Input::get('startDate');
      $end = Input::get('endDate');

      $addon_2p3p = DashboardModel::dashboardAddonComparin($start, $end, '2p3p');
      $addon_upgrade = DashboardModel::dashboardAddonComparin($start, $end, 'upgrade');
      $addon_second_stb = DashboardModel::dashboardAddonComparin($start, $end, 'second_stb');
      $addon_wifi_extender = DashboardModel::dashboardAddonComparin($start, $end, 'wifi_extender');

      return view('dashboard.dashboardAddonComparin', compact('start', 'end', 'addon_2p3p', 'addon_upgrade', 'addon_second_stb', 'addon_wifi_extender'));
    }

    public function dashboardAddonComparinDetail()
    {
      $datel = Input::get('datel');
      $status = Input::get('status');
      $start = Input::get('startDate');
      $end = Input::get('endDate');
      $waktu = Input::get('waktu');

      $getData = DashboardModel::dashboardAddonComparinDetail($datel, $status, $start, $end, $waktu);

      return view('dashboard.dashboardAddonComparinDetail', compact('source', 'datel', 'status', 'start', 'end', 'waktu', 'getData'));
    }

    public function dashboardAddon()
    {
      $source = Input::get('source');
      $start = Input::get('startDate');
      $end = Input::get('endDate');

      $addon_1p2p = DashboardModel::dashboardAddon($source, $start, $end, '1p2p');
      $addon_2p3p = DashboardModel::dashboardAddon($source, $start, $end, '2p3p');
      $addon_upgrade = DashboardModel::dashboardAddon($source, $start, $end, 'upgrade');
      $addon_pda = DashboardModel::dashboardAddon($source, $start, $end, 'pda');
      $addon_minipack = DashboardModel::dashboardAddon($source, $start, $end, 'minipack');
      $addon_ott = DashboardModel::dashboardAddon($source, $start, $end, 'ott');

      return view('dashboard.dashboardAddon', compact('source', 'start', 'end', 'addon_1p2p', 'addon_2p3p', 'addon_upgrade', 'addon_pda', 'addon_minipack', 'addon_ott'));
    }

    public function dashboardAddonDetail()
    {
      $source = Input::get('source');
      $datel = Input::get('datel');
      $status = Input::get('status');
      $start = Input::get('startDate');
      $end = Input::get('endDate');
      $waktu = Input::get('waktu');

      $getData = DashboardModel::dashboardAddonDetail($source, $datel, $status, $start, $end, $waktu);

      return view('dashboard.dashboardAddonDetail', compact('source', 'datel', 'status', 'start', 'end', 'waktu', 'getData'));
    }

    public function newDashboardDismantling()
    {
      $start = Input::get('startDate');
      $end = Input::get('endDate');

      $getData = DashboardModel::newDashboardDismantling($start, $end);

      return view('dashboard.newDashboardDismantling', compact('start', 'end', 'getData'));
    }

    public function newDashboardDismantlingDetail()
    {
      $tim = Input::get('tim');
      $start = Input::get('startDate');
      $end = Input::get('endDate');
      $status = Input::get('status');
      $type = Input::get('type');


      $getData = DashboardModel::newDashboardDismantlingDetail($tim, $start, $end, $status, $type);

      return view('dashboard.newDashboardDismantlingDetail', compact('tim', 'start', 'end', 'status', 'type', 'getData'));
    }

    public function progress_totalpi_detail()
    {
      $sektor = Input::get('sektor');
      $type = Input::get('type');
      $status = Input::get('status');

      $getData = DashboardModel::progress_totalpi_detail($sektor, $type, $status);

      return view('dashboard.progress_totalpi_detail', compact('sektor', 'type', 'status', 'getData'));
    }

    public function kendala_alpro()
    {
      $date = Input::get('date');
      $getData = DashboardModel::kendala_alpro($date);

      return view('dashboard.kendalaAlpro', compact('date', 'getData'));
    }

    public function raportPickupOnline()
    {
      $nper = Input::get('nper');

      $getData = DashboardModel::raportPickupOnline($nper);

      return view('dashboard.raportPickupOnline', compact('nper', 'getData'));
    }

    public function kendalaSaber()
    {
      $startDate = Input::get('startDate');
      $endDate = Input::get('endDate');

      $getData = DashboardModel::kendalaSaber($startDate, $endDate);

      return view('dashboard.kendalaSaber', compact('startDate', 'endDate', 'getData'));
    }

    public function giveawayPS()
    {
      $date = Input::get('date');

      $data_ps = DashboardModel::giveawayPS($date);

      return view('dashboard.giveawayPS', compact('data_ps', 'date'));
    }

    public function giveawayPSdetail()
    {
      $tim = Input::get('tim');
      $date = Input::get('date');

      $data = DashboardModel::giveawayPSdetail($tim, $date);

      return view('dashboard.giveawayPSdetail', compact('tim', 'date', 'data'));
    }

    public function produktifTeam()
    {
      $show_data = Input::get('show_data');
      $area = Input::get('area');
      $start = Input::get('startDate');
      $end = Input::get('endDate');

      $get_show_data = [
        (object)['id' => 'SEKTOR', 'text' => 'SEKTOR'],
        (object)['id' => 'MITRA', 'text' => 'MITRA'],
      ];

      switch ($show_data) {
        case 'SEKTOR':
            $get_area = DB::select('SELECT title as id, title as text FROM group_telegram WHERE ket_sektor = 1 AND sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND sms_active_prov = 1 ORDER BY urut ASC');
          break;

        case 'MITRA':
            $get_area = DB::select('SELECT mitra_amija_pt as id, mitra_amija_pt as text FROM mitra_amija WHERE kat = "DELTA" AND mitra_status = "ACTIVE" AND witel = "KALSEL" GROUP BY mitra_amija_pt');
          break;

        default:
            $get_area = [];
          break;
      }

      $get_data = DashboardModel::produktifTeam($show_data, $area, $start, $end);

      return view('dashboard.produktifTeam', compact('show_data', 'area', 'start', 'end', 'get_show_data', 'get_area', 'get_data'));
    }

    public function produktifTeamDetail()
    {
      $show_data = Input::get('show_data');
      $area = Input::get('area');
      $team = Input::get('team');
      $start = Input::get('startDate');
      $end = Input::get('endDate');
      $status = Input::get('status');

      $data = DashboardModel::produktifTeamDetail($show_data, $area, $team, $start, $end, $status);

      return view('dashboard.produktifTeamDetail', compact('show_data' , 'area', 'team', 'start', 'end', 'status', 'data'));
    }

    public function kpi7_psb_generate($periode)
    {
      $exp = explode('-',$periode);
      $new_periode = $exp[0].$exp[1];
      $last_periode = date('Ym', strtotime(date($periode)."first day of last month"));
      DB::table('kpi_sector_prov')->where('periode',$periode)->delete();
      $target_tti = 99;
      $target_pspi = 98;
      $target_ffg = 99;
      $target_ttr_ffg = 99;
      $target_uji_petik = 99;
      $target_qc_return = 99;

      $tti_comp = $tti_ps = $kpips = $kpipi = $realtti = $achtti = $realpspi = $achpspi = $kpiffg = $realffg = $achffg = $realvalcor = $achvalcor = $realttrffg = $achttrffg = $realujipetik = $achujipetik = $realqc = $achqc = $last_ps = $avg_two_month = $kpittrffgcomply = $kpittrffgnotcomply = $realujipetik = $achujipetik = $realqcreturn = $achqcreturn = 0;

      // Kalsel
      $get_sektor = DB::SELECT('select * from group_telegram where ket_posisi = "PROV" AND id NOT IN (97,103)');
      foreach ($get_sektor as $r_sektor)
      {
      $tti = DB::SELECT('
        SELECT
          gt.title,
          SUM(CASE WHEN pkt.header = "COMPTTI" THEN 1 ELSE 0 END) as COMPTTI,
          SUM(CASE WHEN pkt.header = "PSTTI" THEN 1 ELSE 0 END) as PSTTI
        FROM `prabac_kpiprov_tti` pkt
        left join dispatch_teknisi dt ON pkt.no_sc_int = dt.NO_ORDER
        left join regu r ON dt.id_regu = r.id_regu
        left join group_telegram gt ON gt.chat_id = r.mainsector AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
        where gt.id = "'.$r_sektor->id.'" AND periode="'.$new_periode.'" group by gt.title
      ');
      $pspi = DB::SELECT('
      SELECT
        gt.title,
        SUM(CASE WHEN pkt.header = "KPIPS" THEN 1 ELSE 0 END) as KPIPS,
        SUM(CASE WHEN pkt.header = "KPIPI" THEN 1 ELSE 0 END) as KPIPI
      FROM `prabac_kpiprov_pspi` pkt
      left join dispatch_teknisi dt ON pkt.no_sc_int = dt.NO_ORDER
      left join regu r ON dt.id_regu = r.id_regu
      left join group_telegram gt ON gt.chat_id = r.mainsector AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
      where gt.id = "'.$r_sektor->id.'" AND periode="'.$new_periode.'" group by gt.title
      ');
      $ps_last_month = DB::SELECT('
      SELECT
        gt.title,
        SUM(CASE WHEN pkt.header = "KPIPS" THEN 1 ELSE 0 END) as KPIPS,
        SUM(CASE WHEN pkt.header = "KPIPI" THEN 1 ELSE 0 END) as KPIPI
      FROM `prabac_kpiprov_pspi` pkt
      left join dispatch_teknisi dt ON pkt.no_sc_int = dt.NO_ORDER
      left join regu r ON dt.id_regu = r.id_regu
      left join group_telegram gt ON gt.chat_id = r.mainsector AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
      where gt.id = "'.$r_sektor->id.'" AND periode="'.$last_periode.'" group by gt.title
      ');
      $ffg = DB::SELECT('
      SELECT
        gt.title as area,
        SUM(CASE WHEN pwf.witel = "KALSEL" AND pwf.periode LIKE "'.$new_periode.'%" THEN 1 ELSE 0 END) as jumlah
      FROM dispatch_teknisi dt
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
      LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
      WHERE
        gt.id = "'.$r_sektor->id.'"
      GROUP BY gt.title
      ');
      $ttrffg = DB::SELECT('
      SELECT
        gt.title as area,
        SUM(CASE WHEN (pwf.witel = "KALSEL" AND pwf.periode LIKE "'.$new_periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
        SUM(CASE WHEN (pwf.witel = "KALSEL" AND pwf.periode LIKE "'.$new_periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
      FROM dispatch_teknisi dt
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
      LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
      WHERE
      gt.id = "'.$r_sektor->id.'"
      GROUP BY gt.title
      ');
      foreach ($tti as $r_tti) {
        $tti_comp = $r_tti->COMPTTI;
        $tti_ps = $r_tti->PSTTI;
        $realtti = ($tti_comp/$tti_ps)*100;
        $achtti = ($realtti/$target_tti)*100;
      }
      foreach ($pspi as $r_pspi) {
        $kpips = $r_pspi->KPIPS;
        $kpipi = $r_pspi->KPIPI;
        $realpspi = ($kpips/$kpipi)*100;
        $achpspi = ($realpspi/$target_pspi)*100;
      }
      foreach ($ps_last_month as $l_periode) {
        $last_ps = $l_periode->KPIPS;
      }
      foreach ($ffg as $r_ffg) {
        $kpiffg = $r_ffg->jumlah;
      }
      foreach ($ttrffg as $r_ttrffg){
        $kpittrffgcomply = $r_ttrffg->ffg_comply;
        $kpittrffgnotcomply = $r_ttrffg->ffg_notcomply;
        $x = $kpittrffgcomply+$kpittrffgnotcomply;
        if ($x==0) {
          $realttrffg = 100;
        } else {
          $realttrffg = @($kpittrffgcomply/($x))*100;
        }
        $achttrffg = ($realttrffg/$target_ttr_ffg)*100;
      }
      $avg_two_month = ($kpips+$last_ps)/2;
      $realffgx = 100-(($kpiffg/$avg_two_month)*100);
      $achffg = ($realffgx/$target_ffg)*100;

      $realvalcor = 100;
      $achvalcor = 100;
      $realujipetik = 100;
      $achujipetik = ($realujipetik/$target_uji_petik)*100;

      $realqcreturn = 100;
      $achqcreturn = ($realqcreturn/$target_qc_return)*100;

      $insert_kpi = DB::table('kpi_sector_prov')->insert([
        'PERIODE' => $periode,
        'SEKTOR' => $r_sektor->title,
        'WITEL' => 'KALSEL',
        'PSTTI' => $tti_ps,
        'PSTTICOMP' => $tti_comp,
        'REALTTI' => round($realtti,2),
        'ACHTTI' => round($achtti,2),
        'KPIPS' => $kpips,
        'KPIPI' => $kpipi,
        'REALPSPI' => round($realpspi,2),
        'ACHPSPI' => round($achpspi,2),
        'AVG_PS_LAST_TWO_MONTH' => $avg_two_month,
        'KPIFFG' => $kpiffg,
        'REALFFG' => round($realffgx,2),
        'ACHFFG' => $achffg,
        'REALVALCOR' => $realvalcor,
        'ACHVALCOR' => $achvalcor,
        'KPITTRFFGCOMPLY' => $kpittrffgcomply,
        'KPITTRFFGNOTCOMPLY' => $kpittrffgnotcomply,
        'REALTTRFFG' => round($realttrffg,2),
        'ACHTTRFFG' => round($achttrffg,2),
        'REALUJIPETIK' => round($realujipetik,2),
        'ACHUJIPETIK' => round($achujipetik,2),
        'REALQCRETURN' => round($realqcreturn,2),
        'ACHQCRETURN' => round($achqcreturn,2),
        'POIN' => $achpspi+$achtti+$achffg+$achvalcor+$achttrffg+$achujipetik
      ]);
      }

      print_r("\ngenerate kalsel done!\n");

      // Balikpapan
      $get_sektor = DB::table('maintenance_datel')->where('witel', 'BALIKPAPAN')->select('sektor_prov as sektor_prov')->groupBy('sektor_prov')->get();
      foreach ($get_sektor as $r_sektor) {
        $tti = DB::SELECT('
          SELECT
            md.sektor_prov as title,
            SUM(CASE WHEN pkt.header = "COMPTTI" THEN 1 ELSE 0 END) as COMPTTI,
            SUM(CASE WHEN pkt.header = "PSTTI" THEN 1 ELSE 0 END) as PSTTI
          FROM `prabac_kpiprov_tti` pkt
          LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
          where md.sektor_prov = "'.$r_sektor->sektor_prov.'" AND periode="'.$new_periode.'" group by md.sektor_prov
        ');
        $pspi = DB::SELECT('
        SELECT
          md.sektor_prov as title,
          SUM(CASE WHEN pkt.header = "KPIPS" THEN 1 ELSE 0 END) as KPIPS,
          SUM(CASE WHEN pkt.header = "KPIPI" THEN 1 ELSE 0 END) as KPIPI
        FROM `prabac_kpiprov_pspi` pkt
        LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
        where md.sektor_prov = "'.$r_sektor->sektor_prov.'" AND periode="'.$new_periode.'" group by md.sektor_prov
      ');
      $ps_last_month = DB::SELECT('
      SELECT
        md.sektor_prov as title,
        SUM(CASE WHEN pkt.header = "KPIPS" THEN 1 ELSE 0 END) as KPIPS,
        SUM(CASE WHEN pkt.header = "KPIPI" THEN 1 ELSE 0 END) as KPIPI
      FROM `prabac_kpiprov_pspi` pkt
      LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
      where md.sektor_prov = "'.$r_sektor->sektor_prov.'" AND periode="'.$last_periode.'" group by md.sektor_prov
    ');

      $ffg = DB::SELECT('
      SELECT
        md.sektor_prov as area,
        SUM(CASE WHEN pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%" THEN 1 ELSE 0 END) as jumlah
      FROM maintenance_datel md
      LEFT JOIN prabac_wecare2022_ffg pwf ON pwf.sto = md.sto
      WHERE
        md.sektor_prov = "'.$r_sektor->sektor_prov.'"
      GROUP BY md.sektor_prov
      ');
      $ttrffg = DB::SELECT('
      SELECT
        md.sektor_prov as area,
        SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
        SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
      FROM maintenance_datel md
      LEFT JOIN prabac_wecare2022_ffg pwf ON md.sto = pwf.sto
      LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
      WHERE
      md.sektor_prov = "'.$r_sektor->sektor_prov.'"
      GROUP BY md.sektor_prov
      ');

        foreach ($tti as $r_tti) {
          $tti_comp = $r_tti->COMPTTI;
          $tti_ps = $r_tti->PSTTI;
          $realtti = ($tti_comp/$tti_ps)*100;
          $achtti = ($realtti/$target_tti)*100;
        }
        foreach ($pspi as $r_pspi) {
          $kpips = $r_pspi->KPIPS;
          $kpipi = $r_pspi->KPIPI;
          $realpspi = ($kpips/$kpipi)*100;
          $achpspi = ($realpspi/$target_pspi)*100;
        }
        foreach ($ps_last_month as $l_periode) {
          $last_ps = $l_periode->KPIPS;
        }
        foreach ($ffg as $r_ffg) {
          $kpiffg = $r_ffg->jumlah;
        }
        foreach ($ttrffg as $r_ttrffg){
          $kpittrffgcomply = $r_ttrffg->ffg_comply;
          $kpittrffgnotcomply = $r_ttrffg->ffg_notcomply;
          $x = $kpittrffgcomply+$kpittrffgnotcomply;
          if ($x==0) {
            $realttrffg = 100;
          } else {
            $realttrffg = @($kpittrffgcomply/($x))*100;
          }
          $achttrffg = ($realttrffg/$target_ttr_ffg)*100;
        }
        $avg_two_month = ($kpips+$last_ps)/2;
        $realffgx = 100-(($kpiffg/$avg_two_month)*100);
        $achffg = ($realffgx/$target_ffg)*100;

        $realvalcor = 100;
        $achvalcor = 100;
        $realujipetik = 100;
        $realqcreturn = 100;
        $achqcreturn = ($realqcreturn/$target_qc_return)*100;
        $achujipetik = ($realujipetik/$target_uji_petik)*100;
        $insert_kpi = DB::table('kpi_sector_prov')->insert([
          'PERIODE' => $periode,
          'SEKTOR' => $r_sektor->sektor_prov,
          'WITEL' => 'BALIKPAPAN',
          'PSTTI' => $tti_ps,
          'PSTTICOMP' => $tti_comp,
          'REALTTI' => round($realtti,2),
          'ACHTTI' => round($achtti,2),
          'KPIPS' => $kpips,
          'KPIPI' => $kpipi,
          'REALPSPI' => round($realpspi,2),
          'ACHPSPI' => round($achpspi,2),
          'AVG_PS_LAST_TWO_MONTH' => $avg_two_month,
          'KPIFFG' => $kpiffg,
          'REALFFG' => round($realffgx,2),
          'ACHFFG' => $achffg,
          'REALVALCOR' => $realvalcor,
          'ACHVALCOR' => $achvalcor,
          'KPITTRFFGCOMPLY' => $kpittrffgcomply,
          'KPITTRFFGNOTCOMPLY' => $kpittrffgnotcomply,
          'REALTTRFFG' => round($realttrffg,2),
          'ACHTTRFFG' => round($achttrffg,2),
          'REALUJIPETIK' => round($realujipetik,2),
          'ACHUJIPETIK' => round($achujipetik,2),
          'REALQCRETURN' => round($realqcreturn,2),
          'ACHQCRETURN' => round($achqcreturn,2),
          'POIN' => $achpspi+$achtti+$achffg+$achvalcor+$achttrffg+$achujipetik
        ]);
      }

      print_r("generate balikpapan done!\n");

    }

    public function kpi7_sector_psb($periode)
    {
      $data = DB::table('kpi_sector_prov')->where('PERIODE', $periode)->orderBy('POIN', 'DESC')->get();
      return view('dashboard.kpi7_psb',compact('periode','data'));
    }

    public function kpi7_psb()
    {
      $witel = Input::get('witel');
      $type = Input::get('type'); // SEKTOR atau MITRA atau ALL
      $area_show = Input::get('area_show'); // nama sektor atau nama mitra atau ALL
      $periode = Input::get('periode'); // format nya date('Y-m')

      $data = DashboardModel::kpi7_psb($witel, $type, $area_show, $periode);
      dd($data);

      return view('dashboard.kpi7_psb', compact('witel', 'type', 'area_show', 'periode', 'data'));
    }

    public function newDashboardKendala()
    {
      $view = Input::get('view');
      $startDate = Input::get('startDate');
      $endDate = Input::get('endDate');

      $get_view = [
        (object)['id' => 'DATEL', 'text' => 'DATEL'],
        (object)['id' => 'SEKTOR', 'text' => 'SEKTOR'],
      ];

      $data1 = DashboardModel::newDashboardKendala(1, $view, $startDate, $endDate);
      $data2 = DashboardModel::newDashboardKendala(2, $view, $startDate, $endDate);

      return view('dashboard.newDashboardKendala', compact('view', 'startDate', 'endDate', 'get_view', 'data1', 'data2'));
    }

    public function newDashboardKendalaDetail()
    {
      $view = Input::get('view');
      $area = Input::get('area');
      $startDate = Input::get('startDate');
      $endDate = Input::get('endDate');
      $status = Input::get('status');

      dd($view, $area, $startDate, $endDate, $status);
    }

    public function needCancelEvaluasi()
    {
      $getData = DashboardModel::needCancelEvaluasi();
      $getPhotos = ['Lokasi', 'Lokasi_1', 'Lokasi_2'];

      return view('dashboard.needCancelEvaluasi', compact('getData', 'getPhotos'));
    }

    public function reportProvToday()
    {
      $date = Input::get('date');
      
      if ($date == null)
      {
        $date = date('Y-m-d');
      }
      
      $bjm = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BJM')->whereDate('reported_date', $date)->first();
      $kyg = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KYG')->whereDate('reported_date', $date)->first();
      $bjmkyg = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BJMKYG')->whereDate('reported_date', $date)->first();
      $uli = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'ULI')->whereDate('reported_date', $date)->first();
      $gmb = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'GMB')->whereDate('reported_date', $date)->first();
      $uligmb = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'ULIGMB')->whereDate('reported_date', $date)->first();
      $bbr = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BBR')->whereDate('reported_date', $date)->first();
      $ple = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'PLE')->whereDate('reported_date', $date)->first();
      $btb = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BTB')->whereDate('reported_date', $date)->first();
      $tki = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'TKI')->whereDate('reported_date', $date)->first();
      $plebtbtki = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'PLEBTBTKI')->whereDate('reported_date', $date)->first();
      $blc = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BLC')->whereDate('reported_date', $date)->first();
      $ser = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'SER')->whereDate('reported_date', $date)->first();
      $trj = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'TRJ')->whereDate('reported_date', $date)->first();
      $blcsertrj = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BLCSERTRJ')->whereDate('reported_date', $date)->first();
      $kpl = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KPL')->whereDate('reported_date', $date)->first();
      $pgt = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'PGT')->whereDate('reported_date', $date)->first();
      $sti = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'STI')->whereDate('reported_date', $date)->first();
      $kip = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KIP')->whereDate('reported_date', $date)->first();
      $pgtstikip = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'PGTSTIKIP')->whereDate('reported_date', $date)->first();
      $tjl = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'TJL')->whereDate('reported_date', $date)->first();
      $pgn = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'PGN')->whereDate('reported_date', $date)->first();
      $amt = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'AMT')->whereDate('reported_date', $date)->first();
      $tjlpgnamt = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'TJLPGNAMT')->whereDate('reported_date', $date)->first();
      $mtp = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'MTP')->whereDate('reported_date', $date)->first();
      $mrb = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'MRB')->whereDate('reported_date', $date)->first();
      $mtpmrb = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'MTPMRB')->whereDate('reported_date', $date)->first();
      $lul = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'LUL')->whereDate('reported_date', $date)->first();
      $bri = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'BRI')->whereDate('reported_date', $date)->first();
      $kdg = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KDG')->whereDate('reported_date', $date)->first();
      $neg = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'NEG')->whereDate('reported_date', $date)->first();
      $kdgneg = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KDGNEG')->whereDate('reported_date', $date)->first();
      $rta = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'RTA')->whereDate('reported_date', $date)->first();
      $kdgnegrta = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', 'KDGNEGRTA')->whereDate('reported_date', $date)->first();

      return view('dashboard.reportProvToday', compact('date', 'bjm', 'kyg', 'bjmkyg', 'uli', 'gmb', 'uligmb', 'bbr', 'ple', 'btb', 'tki', 'plebtbtki', 'blc', 'ser', 'trj', 'blcsertrj', 'kpl', 'pgt', 'sti', 'kip', 'pgtstikip', 'tjl', 'pgn', 'amt', 'tjlpgnamt', 'mtp', 'mrb', 'mtpmrb', 'lul', 'bri', 'kdg', 'neg', 'kdgneg', 'rta', 'kdgnegrta'));
    }

  public function reportProvTodaySave(Request $req)
  {
    DashboardModel::reportProvTodaySave($req);

    return back()->with('alerts',[
      [
        'type' => 'success',
        'text' => '<strong>SUKSES</strong> Simpan Data !'
      ]
    ]);
  }
}
