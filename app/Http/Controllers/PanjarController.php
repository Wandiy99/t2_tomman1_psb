<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class PanjarController extends Controller
{

  public function index()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT *
      FROM request_panjar
      WHERE updater = ?
    ', [
      $auth->id_karyawan
    ]);

    return view('panjar.list', compact('list'));
  }
  
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM request_panjar
      WHERE id = ?
    ',[
      $id
    ]);
    $data = $exists[0];
    return view('panjar.input', compact('data'));
  }
  public function create()
  {
    //$data->id=new Panjar();
    $data = new \stdClass();
    $data->id = null;
    $data->pj = null;
    $data->keperluan = null;
    $data->nominal = null;
    return view('panjar.input', compact('data'));
  }
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM request_panjar
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      DB::transaction(function() use($request, $data, $auth) {
        DB::table('request_panjar')
          ->where('id', $data->id)
          ->update([
            'ts'        => DB::raw('NOW()'),
            'updater'   => $auth->id_karyawan,
            'keperluan' => $request->input('keperluan'),
            'pj'        => $request->input('pj'),
            'nominal'   => $request->input('nominal')
          ]);
      });
      return redirect('/panjar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth) {
        $insertId = DB::table('request_panjar')->insertGetId([
          'ts'        => DB::raw('NOW()'),
          'updater'   => $auth->id_karyawan,
          'pj'        => $request->input('pj'),
          'keperluan' => $request->input('keperluan'),
          'nominal'   => $request->input('nominal')
        ]);
      });
      return redirect('/panjar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
      ]);
    }
  }
  public function destroy($id)
  {
    DB::table('request_panjar')
          ->where('id', [$id])->delete();

    return redirect('/panjar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
      ]);
  }
}
