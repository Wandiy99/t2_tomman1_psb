<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use File;
use DateTime;
use Telegram;
use App\DA\GrabModel;
use App\DA\DashboardModel;
use App\DA\DshrModel;
use TG;

date_default_timezone_set('Asia/Makassar');

class GrabController extends Controller
{

  public function getBMKG($provinsi){

    
    $myXMLData = "https://data.bmkg.go.id/datamkg/MEWS/DigitalForecast/DigitalForecast-KalimantanSelatan.xml";

    $xml= simplexml_load_string(file_get_contents($myXMLData)) or die("Error: Cannot create object");

    DB::table('cuaca')->where('cuaca_tgl',date('Y-m-d'))->delete();

    foreach ($xml->forecast->area as $result){
      $data = $result;
      // $cuaca = $data->parameter;
      DB::table('cuaca')->insert([
        "cuaca_tgl" => date('Y-m-d'),
        "kota" => $data->name,
        "jam0" => $data->parameter[6]->timerange[4]->value,
        "jam6" => $data->parameter[6]->timerange[5]->value,
        "jam12" => $data->parameter[6]->timerange[6]->value,
        "jam16" => $data->parameter[6]->timerange[7]->value,
      ]);
      echo "Kota : ".$data->name."<br />";
      // echo "Jam 0 : ".$data->parameter[6]->timerange[0]->value."<br />";
      // echo "Jam 6 : ".$data->parameter[6]->timerange[1]->value."<br />";
      // echo "Jam 12 : ".$data->parameter[6]->timerange[2]->value."<br />";
      // echo "Jam 16 : ".$data->parameter[6]->timerange[3]->value."<br />";
      echo "Jam 0 : ".$data->parameter[6]->timerange[4]->value."<br />";
      echo "Jam 6 : ".$data->parameter[6]->timerange[5]->value."<br />";
      echo "Jam 12 : ".$data->parameter[6]->timerange[6]->value."<br />";
      echo "Jam 16 : ".$data->parameter[6]->timerange[7]->value."<br />";
    }
  }

  public function get_sc_not_sync($date){
    $get_db = DB::SELECT('
        SELECT
        *
        FROM
        psb_myir_wo a
        LEFT JOIN dispatch_teknisi b ON a.sc = b.Ndem
        WHERE
          a.orderDate LIKE "'.$date.'%" AND
          a.sc <> 0 AND
          a.sc IS NOT NULL AND
          b.id IS NULL
    ');
    foreach ($get_db as $result){
      $this->add_sc($result->sc,$result->myir);
    }
  }

  public function add_sc($sc,$id){
    // $this->validate($req,[
    //     'sc'          => 'required|numeric',
    //     'no_inet'     => 'required|numeric'
    // ],[
    //     'sc.required'      => 'No. SC Jangan Kosong',
    //     'sc.numeric'       => 'Diisi Angka',
    //     'no_inet.required' => 'No. Internet Jangan Kosonng',
    //     'no_inet.numeric'  => 'Disi Angka'
    // ]);

    // ubah di table psb_myir_wo
    $get_sc = DB::SELECT('
      SELECT
      *
      FROM
        Data_Pelanggan_Starclick a
      WHERE
        a.jenisPsb LIKE "AO%" AND
        a.orderId = "'.$sc.'"
      ORDER BY a.orderDate DESC
    ');
    if (count($get_sc)>0){
    $get_sc = $get_sc[0];
    $getRegu = DB::table('dispatch_teknisi')->where('Ndem',$id)->get();
    if (count($getRegu)>0){
      $getRegu = $getRegu[0];
    DB::table('psb_myir_wo')->where('myir',$id)->update([
        'sc'    => $sc,
        'ket'   => '1',
        'no_internet'   => $get_sc->internet,
        'no_telp'       => $get_sc->noTelp
    ]);

    // ubah table dispatch
    DB::table('dispatch_teknisi')->where('Ndem',$id)->update([
        'Ndem'    => $sc,
        'dispatch_by' => NULL
    ]);



    $data = DB::table('psb_laporan_log')->where('Ndem',$sc)->first();
    if (count($data)==0){
      $getData = DB::table('psb_laporan_log')->where('Ndem',$id)->orderBy('psb_laporan_log_id','DESC')->first();
      DB::table('psb_laporan_log')->insert([
          'id_regu'   => $getRegu->id_regu,
          'Ndem'      => $sc,
          'psb_laporan_id' => $getData->psb_laporan_id,
          'status_laporan' => $getData->status_laporan,
          'catatan'        => 'SINKRON SC DARI MYIR-'.$id.' KE SC '.$sc,
          'created_by'     => 'TOMMAN',
          'mttr'           => '0',
          'penyebabId'     => NULL,
          'action'         => 0
      ]);
    };
    echo $sc."SYNC<br />";
    } else {
      echo $sc."NOTSYNC<br />";
    }
    } else {
      echo $id."UNDISPATCH<br />";
    }
  }

	public function check_sc($id){
		$check = DB::connection('pg')->SELECT("
			SELECT
			a.order_id as ORDER_ID,
			a.create_dtm as ORDER_DATE,
			a.order_code as EXTERN_ORDER_ID,
			a.xn4 as NCLI,
			a.customer_desc as CUSTOMER_NAME,
			a.xs3 as WITEL,
			a.create_user_id as AGENT,
			a.xs2 as SOURCE,
			a.xs12 as STO,
			a.xs10 as SPEEDY,
			a.xs11 as POTS,
			a.order_status_id as STATUS_CODE_SC,
			a.xs7 as USERNAME,
			a.xs8 as ORDER_ID_NCX,
			c.xs11 as KCONTACT,
			b.xs3 as ORDER_STATUS
			FROM
				sc_new_orders a
			LEFT JOIN sc_new_order_activities b ON a.order_id = b.order_id AND a.order_status_id = b.xn2
			LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id
			WHERE
				a.xs3 = 'KALSEL' AND
				date(a.create_dtm) = '2020-02-04'
		");
		header("Content-Type: application/json");
		print_r(json_encode($check));
  }

	public function no_hp_onecall($witel,$tglAwal,$tglAkhir){
		$link = "http://36.92.189.82/onecall/api/sales-order?witel=".$witel."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
		$file = @file_get_contents($link);
		$get = @json_decode($file);
		foreach ($get as $result){
			echo $result->myir;

			$check = DB::SELECT('select * from psb_myir_wo where myir = "'.$result->myir.'"');
			if (count($check)>0){
				$query = DB::table('psb_myir_wo')->where('myir',$result->myir)->update([
					'picPelanggan' => $result->no_hp
				]);
				echo " DONE";
			} else {
				echo " SKIP";
			}
			echo "<br />";
		}
  }

  public function update_starclick_up(){
    $date = date('Y-m');
    $check = DB::SELECT('
      SELECT
      *
      FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      WHERE
        a.orderDate LIKE "'.$date.'%" AND
        a.orderStatus <> "Completed" AND
        c.status_laporan = 1
    ');
    foreach ($check as $result) {
      echo $result->orderId." | ".$result->orderStatus."<br />";
      $this->sync_ax($result->orderId);
    }
  }

  public static function update_starclick_up_x($date){
    $check = DB::SELECT('
      SELECT
      *
      FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      WHERE
        a.orderDate LIKE "'.$date.'%" AND
        a.orderStatus <> "Completed" AND
        c.status_laporan = 1
    ');
    foreach ($check as $result) {
      echo $result->orderId." | ".$result->orderStatus."<br />";
      $this->sync_ax($result->orderId);
    }
  }

  public static function get_sc_new_orders()
  {
    

    $tglAwal = date('Y-m-d',strtotime("-6 days"));
    $tglAkhir = date('Y-m-d',strtotime("+1 days"));

    $query = DB::connection('pg')->SELECT("select * from sc_new_orders where create_dtm >= '".$tglAwal." 00:00:00'::timestamp AND  create_dtm <  '".$tglAkhir." 00:00:00'::timestamp");

    $allRecord = array();

    foreach ($query as $num => $result)
    {
      $allRecord[] = ((array)$result);
      print_r("ORDER ID $result->order_id \n");
      // echo ++$num."<br />";
    }

    $srcarr = array_chunk($allRecord,500);
    foreach($srcarr as $item)
    {
      self::new_insertOrUpdate($item,'sc_new_orders');
    }
  }

  public static function get_sc_new_order_activities()
  {
    

    $tglAwal = date('Y-m-d',strtotime("-6 days"));
    $tglAkhir = date('Y-m-d',strtotime("+1 days"));

    $query = DB::connection('pg')->SELECT("select * from sc_new_order_activities where update_dtm >= '".$tglAwal." 00:00:00'::timestamp AND  update_dtm <  '".$tglAkhir." 00:00:00'::timestamp");

    $allRecord = array();

    foreach ($query as $num => $result)
    {
      $allRecord[] = ((array)$result);
      print_r("ORDER ACTIVITIES ID $result->order_activities_id \n");
      // echo ++$num."<br />";
    }

    $srcarr = array_chunk($allRecord,500);
    foreach($srcarr as $item)
    {
      self::new_insertOrUpdate($item,'sc_new_order_activities');
    }
  }

  public static function get_sc_new_order_details()
  {
    

    $tglAwal = date('Y-m-d',strtotime("-6 days"));
    $tglAkhir = date('Y-m-d',strtotime("+1 days"));

    $query = DB::connection('pg')->SELECT("select * from sc_new_order_details where update_dtm >= '".$tglAwal." 00:00:00'::timestamp AND  update_dtm <  '".$tglAkhir." 00:00:00'::timestamp");

    $allRecord = array();

    foreach ($query as $num => $result)
    {
      $allRecord[] = ((array)$result);
      print_r("ORDER DETAIL ID $result->order_detail_id \n");
      // echo ++$num."<br />";
    }

    $srcarr = array_chunk($allRecord,500);
    foreach($srcarr as $item)
    {
      self::new_insertOrUpdate($item,'sc_new_order_details');
    }
  }

	public static function sync_starclick_dbmirror()
  {
    

    // $datestart = date('Y-m-d',strtotime("-6 days"));
    // $dateend = date('Y-m-d',strtotime("+1 days"));

		$check = DB::SELECT("
			SELECT
			a.order_id as order_id,
			a.create_dtm as order_date,
      a.xd3 as order_date_ps,
			a.order_code as exter_order_id,
			a.xn4 as ncli,
			a.customer_desc as customer_name,
			a.xs3 as witel,
			a.create_user_id as agent,
			a.xs2 as source,
			a.xs12 as sto,
			a.xs10 as speedy,
			a.xs11 as pots,
			a.order_status_id as status_code_sc,
			a.xs7 as username,
			a.xs8 as order_id_ncx,
      c.xs11 as kcontact,
      c.xs26 as orderaddr,
      c.xs16 as ordercity,
      cc.xs2 as lat,
      cc.xs3 as lon,
      cc.xs4 as alproname
			FROM
				sc_new_orders a
			LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id AND c.order_detail_type_id = 1
      LEFT JOIN sc_new_order_details cc ON a.order_id = c.order_id AND c.order_detail_type_id = 4

			WHERE
        a.xs3 = 'KALSEL' AND
			 (DATE(a.create_dtm) = '".date('Y-m-d')."')
    ");

    foreach ($check as $result)
    {

      $dbsc = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->get();

      if (count($dbsc)>0)
      {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->update([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            'orderDatePs' => $result->order_date_ps,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->ordercity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderaddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      } else {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->insert([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            'orderDatePs' => $result->order_date_ps,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->orderCity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderAddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      }

      print_r("RESULT SC $result->order_id \n");
    }

    $total = count($check);

    print_r("\nFINISH UPDATE STARCLICK TOTAL $total BY DB MIRROR\n");

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Finish Update Starclick by DB Mirror"
    ]);

    DB::table('sc_new_orders')->truncate();

    DB::table('sc_new_order_activities')->truncate();

    DB::table('sc_new_order_details')->truncate();

		// header("Content-Type: application/json");

		// print_r(json_encode($check));

	}

  public function custom_sync_starclick_dbmirror(Request $req)
  {
    $order_id = $req->sc;
    // dd($order_id);
    // dd(DB::connection('pg')->table('sc_new_orders')->where('order_id', $order_id)->first());

    $check = DB::connection('pg')->SELECT("
      SELECT
      a.order_id as order_id,
      a.create_dtm as order_date,
      ccc.update_dtm as order_date_ps,
      a.order_code as exter_order_id,
      a.xn4 as ncli,
      ccc.xs3 as status,
      a.customer_desc as customer_name,
      a.xs3 as witel,
      a.create_user_id as agent,
      a.xs2 as source,
      a.xs12 as sto,
      a.xs10 as speedy,
      a.xs11 as pots,
      a.order_status_id as status_code_sc,
      a.xs7 as username,
      a.xs8 as order_id_ncx,
      c.xs11 as kcontact,
      c.xs26 as orderaddr,
      c.xs16 as ordercity,
      cc.xs2 as lat,
      cc.xs3 as lon,
      cc.xs4 as alproname
      FROM
        sc_new_orders a
      LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id AND c.order_detail_type_id = 1
      LEFT JOIN sc_new_order_details cc ON a.order_id = cc.order_id AND c.order_detail_type_id = 4
      LEFT JOIN sc_new_order_activities ccc ON a.order_id = ccc.order_id AND a.order_status_id = ccc.xn3
      WHERE
        a.order_id = '".$order_id."'
      ORDER BY ccc.order_activities_id DESC
      LIMIT 1
    ");

    // dd($check);

    foreach ($check as $result)
    {

      $dbsc = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->first();

      if (count($dbsc)>0)
      {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->update([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'orderStatus' => $result->status,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            // 'orderDatePs' => $result->order_date_ps,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->ordercity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderaddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      } else {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->insert([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'orderStatus' => $result->status,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            // 'orderDatePs' => $result->order_date_ps,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->orderCity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderAddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      }

      if ($result->status == "Completed") {
        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->where('orderId',$result->order_id)->update([
            "orderDatePs" => $result->order_date_ps,
            "orderStatus" => $result->status,
            "orderStatusId" => $result->status_code_sc
          ]);
        });
      }

    }

    // header("Content-Type: application/json");

    if (count($check)>0)
    {
      return redirect('/customGrab')->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUCCESS</strong> SYNCRON SC-'.$order_id.'']]);

    } else {

      return redirect('/customGrab')->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>NOT FOUND :(</strong> MAYBE SC NOTHING IN DATABASE']]);

    }

  }

  public function multi()
  {
    if (session('auth')->level==10) {
    $layout = 'tech_layout';
    } else {
      $layout = 'layout';
    }

    return view('grab.multi',compact('layout'));
  }

  public function multi_sync_starclick_dbmirror(Request $req)
  {
    $order_id = $req->sc;

    $check = DB::connection('pg')->SELECT("
      SELECT
      a.order_id as order_id,
      a.create_dtm as order_date,
      a.order_code as exter_order_id,
      a.xn4 as ncli,
      a.customer_desc as customer_name,
      a.xs3 as witel,
      a.create_user_id as agent,
      a.xs2 as source,
      a.xs12 as sto,
      a.xs10 as speedy,
      a.xs11 as pots,
      a.order_status_id as status_code_sc,
      a.xs7 as username,
      a.xs8 as order_id_ncx,
      c.xs11 as kcontact,
      c.xs26 as orderaddr,
      c.xs16 as ordercity,
      cc.xs2 as lat,
      cc.xs3 as lon,
      cc.xs4 as alproname
      FROM
        sc_new_orders a
      LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id AND c.order_detail_type_id = 1
      LEFT JOIN sc_new_order_details cc ON a.order_id = c.order_id AND c.order_detail_type_id = 4

      WHERE
        a.order_id IN ($order_id)
    ");

    foreach ($check as $result)
    {

      $dbsc = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->get();

      if (count($dbsc)>0)
      {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger',$result->order_id)->update([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->ordercity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderaddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      } else {

        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->insert([
            'orderId' => $result->order_id,
            'orderIdInteger' => $result->order_id,
            'orderName' => $result->customer_name,
            'orderNcli' => $result->ncli,
            'witel' => $result->witel,
            'agent_id' => $result->agent,
            'orderDate' => $result->order_date,
            'sto' => $result->sto,
            'ndemSpeedy' => $result->speedy,
            'internet' => $result->speedy,
            'ndemPots' => $result->pots,
            'noTelp' => $result->pots,
            'orderStatusId' => $result->status_code_sc,
            'username' => $result->username,
            'orderCity' => @$result->orderCity,
            'orderNotel' => @$result->pots,
            'orderAddr' => @$result->orderAddr,
            'kcontact' => @$result->kcontact,
            'lat' => $result->lat,
            'lon' => $result->lon,
            'alproname' => $result->alproname
          ]);
        });

      }

      if ($result->order_status == "Completed") {
        DB::transaction(function() use($result) {
          DB::table('Data_Pelanggan_Starclick')->where('orderId',$result->order_id)->update([
            "orderDatePs" => $result->update_dtm,
            "orderStatus" => $result->order_status,
            "orderStatusId" => $result->order_status_id
          ]);
        });
      }

    }

    // header("Content-Type: application/json");
    // print_r(json_encode($check));

    return redirect('/multi/customGrab')->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUCCESS</strong> SYNCRON SC "'.$order_id.'" ']]);
  }

	public function grabQCBorneo($bulan){
    

    if($bulan=="bulan"){
      $date   = date('Y-m-01');
      $datex  = date('Y-m-d');
    }elseif($bulan=="2020P1"){
      $date   = "2020-01-01";
      $datex  = "2020-06-30";
    }elseif($bulan=="2020P2"){
      $date   = "2020-07-01";
      $datex  = "2020-12-31";
    }elseif($bulan=="2019P1"){
      $date   = "2019-01-01";
      $datex  = "2019-06-30";
    }elseif($bulan=="2019P2"){
      $date   = "2019-07-01";
      $datex  = "2019-12-31";
    }

		$get_data = DB::SELECT('SELECT
    e.uraian,
    a.orderId as sc,
    d.laporan_status,
    a.jenisPsb
    FROM Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderIdInteger = b.NO_ORDER
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN regu e ON b.id_regu = e.id_regu
    WHERE
    (DATE(a.orderDatePs) BETWEEN "'.$date.'" AND "'.$datex.'") AND
    c.status_laporan NOT IN ("6","5","2","29","28") AND a.jenisPsb LIKE "AO%"');

    // dd($get_data);
    $chunk = array_chunk($get_data,500);
  foreach($chunk as $chunck_data){
    // dd($chunck_data);
		foreach ($chunck_data as $data){
      // dd($data);
			$get_status_qc = file_get_contents("http://10.128.18.14/proactive-caring/api/quality_control.php?sc=".$data->sc);
      $get_status_qc  = json_decode($get_status_qc);
      if ($get_status_qc<>""){
				// print_r($get_status_qc->data);
				$insert = $get_status_qc->data;
				if ($insert<>"SC TIDAK DITEMUKAN/BELUM DIVALIDASI"){
				if (count($insert)){
				foreach ($insert as $qc){
					$cek_db = DB::table('qc_borneo')->where('sc',$qc->sc)->get();
					if (count($cek_db)==0){
					$insert = DB::table('qc_borneo')->insert([
						'sc' => $qc->sc,
						'status_qc' => $qc->status_qc,
						'unit' => $qc->unit,
						'foto_rumah' => $qc->foto_rumah,
						'ket_rumah' => $qc->ket_rumah,
						'foto_teknisi' => $qc->foto_rumah,
						'ket_teknisi' => $qc->ket_teknisi,
						'foto_odp' => $qc->foto_odp,
						'ket_odp' => $qc->ket_odp,
						'foto_redaman' => $qc->foto_redaman,
						'ket_redaman' => $qc->ket_redaman,
						'foto_ont' => $qc->foto_ont,
						'ket_ont' => $qc->ket_ont,
						'foto_berita' => $qc->foto_berita,
						'ket_berita' => $qc->ket_berita,
						'tagging_lokasi' => $qc->tagging_lokasi,
						'ket_tagging' => $qc->ket_tagging,
						'last_sync' => date('Y-m-d H:i:s')
					]);
					} else {
						$insert = DB::table('qc_borneo')->where('sc',$qc->sc)->update([
						'sc' => $qc->sc,
						'status_qc' => $qc->status_qc,
						'unit' => $qc->unit,
						'foto_rumah' => $qc->foto_rumah,
						'ket_rumah' => $qc->ket_rumah,
						'foto_teknisi' => $qc->foto_rumah,
						'ket_teknisi' => $qc->ket_teknisi,
						'foto_odp' => $qc->foto_odp,
						'ket_odp' => $qc->ket_odp,
						'foto_redaman' => $qc->foto_redaman,
						'ket_redaman' => $qc->ket_redaman,
						'foto_ont' => $qc->foto_ont,
						'ket_ont' => $qc->ket_ont,
						'foto_berita' => $qc->foto_berita,
						'ket_berita' => $qc->ket_berita,
						'tagging_lokasi' => $qc->tagging_lokasi,
						'ket_tagging' => $qc->ket_tagging,
						'last_sync' => date('Y-m-d H:i:s')
					]);
					}
				}
				}
				}
			}
    }
  }

    echo "Finish Grab QC Borneo $date $datex";
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Finish Grab QC Borneo $date $datex"
    ]);
	}

  public function index()
  {
    if (session('auth')->level==10) {
    $layout = 'tech_layout';
    } else {
      $layout = 'layout';
    }

    return view('grab.index',compact('layout'));
  }

  public function date_loop(){
    
    echo date('Y-m-d')."<br />";
    for ($i=0;$i<=7;$i++){
      $date = date('d/m/Y',strtotime("-$i days"));
      echo $date."<br />";
    }
  }

  public function GrabNossaFix(){
    $this->GrabNossaFixQuery('data_nossa_1');
    $this->GrabNossaFixQuery('data_nossa_1_log');
  }

  public static function grabstarclickncx_insert($link){
    echo "SYNC TO STARCLICKNCX</n></n>";
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => $link
    ]);
    $ch = curl_init();
    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    echo urldecode($link);
    $headers = array();
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Encoding: gzip, deflate, br';
    $headers[] = 'Sec-Fetch-Site: same-origin';
    $headers[] = 'Sec-Fetch-Mode: cors';
    $headers[] = 'Sec-Fetch-Dest: empty';
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Date: Fri, 03 Jul 2020 03:50:28 GMT';
    $headers[] = 'Expires: Thu, 19 Nov 1981 08:52:00 GMT';
    $headers[] = 'Host: starclickncx.telkom.co.id';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = 'Accept-Language: en-US,en;q=0.9';
    $headers[] = 'Vary: User-Agent';
    $headers[] = 'Transfer-Encoding: chunked';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    // print_r($result);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->ND_INTERNET<>''){
                $int = explode('~', $data->ND_INTERNET);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->ND_INTERNET;
                }
            };
            if ($data->ND_POTS<>''){
                $telp = explode('~', $data->ND_POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->ND_POTS;
                }
            };
            echo "store ke array\n";
            //get myir
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }
            //get STO
            $get_sto = explode('-',@$data->LOC_ID);
            if (count($get_sto)>0){
              $STO = @$get_sto[1];
            } else {
              $STO = "N";
            }
            echo $data->LOC_ID;
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderIdInteger"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->ND_INTERNET,
              "ndemPots"=>@$data->ND_POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "myir_no"=>$myir,
              "witel"=>@$data->WITEL,
              "agent_id"=>@$data->AGENT_ID
            );
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }

    }
    curl_close($ch);
  }

  public static function grabstarclickncx_insert_2($witel, $datex, $x, $start, $cookies)
  {
    echo "SYNC TO STARCLICKNCX</n></n>";
    $ch = curl_init();

    //get session
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);

    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page='.$x.'&start='.$start.'&limit=10';
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookies);
    curl_setopt($ch, CURLOPT_HEADER, false);
    // echo urldecode($link);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
        curl_close($ch);
    } else {
      curl_close($ch);
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->ND_INTERNET<>''){
                $int = explode('~', $data->ND_INTERNET);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->ND_INTERNET;
                }
            };
            if ($data->ND_POTS<>''){
                $telp = explode('~', $data->ND_POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->ND_POTS;
                }
            };
            // echo "store ke array\n";
            //get myir
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }
            //get STO
            $get_sto = explode('-',@$data->LOC_ID);
            if (count($get_sto)>0){
              $STO = @$get_sto[1];
            } else {
              $STO = "N";
            }
            // echo $data->LOC_ID;
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderIdInteger"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->ND_INTERNET,
              "ndemPots"=>@$data->ND_POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "myir_no"=>$myir,
              "witel"=>@$data->WITEL,
              "agent_id"=>@$data->AGENT_ID
            );
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      
      $srcarr = array_chunk($allRecord,500);
      foreach($srcarr as $item) {
        self::insertOrUpdate($item);
      }

    }
  }

  public static function newStarclick($datex)
  {
    
    $witel = "KALSEL";
    $starclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();
    $ch = curl_init();

    //get session
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22:%22'.$witel.'%22,%22Field%22:%22ORG%22,%22Fieldstatus%22:null,%22Fieldtransaksi%22:null,%22StartDate%22:%22'.$datex.'%22,%22EndDate%22:%22'.$datex.'%22,%22start%22:null,%22source%22:%22NOSS%22,%22typeMenu%22:%22TRACKING%22%7D&&page=1&start=0&limit=10');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $starclick->cookies);
    curl_setopt($ch, CURLOPT_HEADER, false);

    $response = curl_exec($ch);
    curl_close($ch);

    // dd($response);
    $result = json_decode($response);

    // dd($result);

    if ($result == NULL)
    {
      print_r("Session expired, please update! newStarclick\n");
    } else {
      if (count($result))
      {
        $list = $result->data;
        $jumlahpage = round(@(int)$list->CNT/10);
        print_r("grab date: $datex \n");
        print_r("waiting ... \n");
        if (isset($list->CNT) && isset($list->LIST)){
          $list = @$list->LIST;
          $start = 0;
          for ($x=1;$x<=$jumlahpage;$x++){
            // $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page='.$x.'&start='.$start.'&limit=10';
            if ($x==1){
              $start = $start+11;
            } else {
              $start = $start+10;
            }
            exec('php /srv/htdocs/tomman1_psb/artisan grabSCByLink '.$witel.' '.$datex.' '.$x.' '.$start.' "'. $starclick->cookies.'" > /dev/null &');
            print_r("success ...\n");
          }
          print_r("total page $jumlahpage\n");
        }
      }
    }
  }

  public static function updatePIstarclick()
  {
    $data = DB::select('SELECT DATE(orderDate) as tgl_order, COUNT(*) as jml_sc  FROM Data_Pelanggan_Starclick WHERE orderStatus = "OSS PROVISIONING ISSUED" GROUP BY DATE(orderDate) ORDER BY tgl_order ASC');

    foreach ($data as $value)
    {
      self::newStarclick($value->tgl_order);
    }

    print_r("\n\n done all");
  }

  public static function grabstarclickncx($witel)
  {
    
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Start newsc Grab SC"
    ]);

    $starclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();
    $txtmsg = "";
    
    for ($i=0;$i<=14;$i++) {
      $datex = date('d/m/Y',strtotime("-$i days"));
      $ch = curl_init();

      //get session
      curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_exec($ch);

      $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';
      curl_setopt($ch, CURLOPT_URL, $link);
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIE, $starclick->cookies);
      curl_setopt($ch, CURLOPT_HEADER, false);
      $result = curl_exec($ch);

      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
          curl_close($ch);
      } else {
        curl_close($ch);
        $result = json_decode($result);

        if($result == null){
          Telegram::sendMessage([
          'chat_id' => '-306306083',
          'parse_mode' => 'html',
          'text' => "Login expired session NCXSC"
          ]);

          return redirect('/')->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>LOGIN EXPIRED!</strong>']
          ]);
        }

        if (count($result)){
          $list = $result->data;
          // print_r(@$list->CNT);
          // echo "<n/>";
          $jumlahpage = round(@(int)$list->CNT/10);
          $txtmsg .= "\n".$datex." total page : ".$jumlahpage;
          // echo "grab tgl:".$datex." page:".$jumlahpage."<n/>";
          // print_r($list);
          // echo "jumlah".count($list);
          if (isset($list->CNT) && isset($list->LIST)){
            $list = @$list->LIST;
            $start = 0;
            for ($x=1;$x<=$jumlahpage;$x++){
              $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$witel.'","Field":"ORG","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"'.$datex.'","EndDate":"'.$datex.'","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page='.$x.'&start='.$start.'&limit=10';
              if ($x==1){
                $start = $start+11;
              } else {
                $start = $start+10;
              }
              // self::grabstarclickncx_insert($link);
              exec('php /srv/htdocs/tomman1_psb/artisan grabSCByLink '.$witel.' '.$datex.' '.$x.' '.$start.' "'. $starclick->cookies.'" > /dev/null &');
              // echo "link:".$link."<n/>";
            }
            // echo "jumlah page : ".$jumlahpage."<br />";
          }
        }
      }
      sleep(1);
    }

    DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
      'last_sync' => date('Y-m-d H:i:s')
    ]);

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => $txtmsg
    ]);
  }

  public static function grab_detail_sc()
  {
    
    $orderDate = date('Y-m-d');
    $get_sc = DB::table('Data_Pelanggan_Starclick')->whereDate('orderDate', $orderDate)->orderBy('orderDate', 'desc')->get();
    $startclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();

    $total = count($get_sc);
    print_r("start update total $total \n");

    foreach($get_sc as $gsc)
    {
      $orderId = $gsc->orderId;
      $ch = curl_init();
      //get session
      curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_exec($ch);

      curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/ordersc/load-order');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIE, $startclick->cookies);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_POST, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $data = '{"scid":"'.$orderId.'"}';
      curl_setopt($ch, CURLOPT_POSTFIELDS, "code=0&giud=0&data=".urlencode($data));
      $result = curl_exec($ch);

      curl_close($ch);
      $detail = json_decode($result);

      $wfm_id = $detail->data->detail->appointment->xs9;
      $appointment_desc = $detail->data->detail->appointment->xs1;
      $order_code = $detail->data->entity->order_code;
      $order_notel = $detail->data->detail->appointment->xs3;
      $order_kontak = $detail->data->detail->appointment->xs4;
      $package_name = $detail->data->detail->paket_preview->xs2;
      $provider = $detail->data->detail->CA->xs3;
      $id_ncx = $detail->data->entity->xs8;

      // dd($orderId, $wfm_id, $appointment_desc, $order_code, $order_notel, $order_kontak, $package_name, $provider, $id_ncx);
      
      DB::transaction(function() use($orderId, $wfm_id, $appointment_desc, $order_code, $order_notel, $order_kontak, $package_name, $provider, $id_ncx)
      {
        DB::table('Data_Pelanggan_Starclick')->where('orderId', $orderId)->update([
          'wfm_id'            => $wfm_id,
          'appointment_desc'  => $appointment_desc,
          'orderNo'           => $order_code,
          'orderNotel'        => $order_notel,
          'orderKontak'       => $order_kontak,
          'email'             => $order_kontak,
          'orderPackageName'  => $package_name,
          'provider'          => $provider,
          'orderIdNcx'        => $id_ncx
        ]);
      });

      print_r("update $orderId \n");
    }

    print_r("finish update \n");

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Finish Grab Detail StarclickNCX $orderDate <b>Total $total</b>"
    ]);
  }

public static function autoapprove_qc1_onecall($witel){

  
  $tglAwal = date('Y-m-01');
  $tglAkhir = date('Y-m-d',strtotime("+1 days"));
  if (session('witel')=="KALSEL"){
    $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
  $file = @file_get_contents($link);
  } else {
    $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
  $file = @file_get_contents($link);
  }
  $getx  = json_decode($file);
  if (count($getx)){
    foreach ($getx as $get){
      $myir = explode('-', $get->myir);
      if (count($myir)>1){
          $myir = $myir[1];
      }
      else{
          $myir = $myir[0];
      };
      echo $myir;
      $getMyir = DB::SELECT('select * from Data_Pelanggan_Starclick_Backend where ORDER_CODE = "MYIR-'.$myir.'"');
      if (count($getMyir)>0){
        echo "ADA";
        $getPsb = DB::SELECT('select * from psb_myir_wo where myir = "'.$myir.'"');
        if (count($getPsb)>0){

        } else {
        echo "INSERT";
        $getData = [
            'myir'            => $get->myir,
            'nama_pelanggan'  => $get->nama_pelanggan,
            'namaOdp'         => $get->odp,
            'sto'             => $get->sto,
            'pic_pelanggan'   => $get->alamat,
            'koor_pelanggan'  => $get->latitude.','.$get->longitude,
            'alamat_detail'   => $get->alamat,
            'layanan'         => $get->layanan,
            'psb'             => NULL,
            'koordinatOdp'    => $get->odp_latitude.','.$get->odp_longitude,
            'email'           => $get->email,
            'paket'           => NULL,
            'sales'           => $get->kode_sales,
            'status_approval' => 1,
            'approve_by'      => "TOMMAN",
            'approve_date'    => date('Y-m-d H:i:s')
        ];
        $getData = (object)$getData;
        $id = DshrModel::simpanSalesQC($getData, $getData->sales);
        }

      } else {
        echo "TIDAKADA";
      }
      echo "<br />";

    }

  }
}

public static function grabRISMA(){
  // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, 'http://10.194.5.20/Risma/load-data-consume-list-outlet-witel.php');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "login=930277&status-data=0");
  curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

  $headers = array();
  $headers[] = 'Connection: keep-alive';
  $headers[] = 'Accept: */*';
  $headers[] = 'X-Requested-With: XMLHttpRequest';
  $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36';
  $headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8';
  $headers[] = 'Origin: http://10.194.5.20';
  $headers[] = 'Referer: http://10.194.5.20/Risma/';
  $headers[] = 'Accept-Language: en-US,en;q=0.9,id;q=0.8';
  $headers[] = 'Cookie: PHPSESSID=l6tm1733v557huehms1g372svu';
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  $result = curl_exec($ch);

  $dom = @\DOMDocument::loadHTML(trim($result));
  $table = $dom->getElementsByTagName('table')->item(0);
  $rows = $table->getElementsByTagName('tr');
  $output = array();
  for ($i = 1, $count = $rows->length; $i < $count; $i++)
  {
    $cells = $rows->item($i)->getElementsByTagName('td');


    $trackid = @$cells->item(2)->nodeValue;
    $data[] = [
      'trackid' => $trackid,
      'apply_date' => @$cells->item(1)->nodeValue,
      'nama_pelanggan' => @$cells->item(3)->nodeValue,
      'handphone' => @$cells->item(4)->nodeValue,
      'email' => @$cells->item(5)->nodeValue,
      'survey' => @$cells->item(6)->nodeValue,
      'odp' => @$cells->item(7)->nodeValue,
      'kordinat' => @$cells->item(8)->nodeValue,
      'witel' => @$cells->item(9)->nodeValue,
      'status_data' => @$cells->item(10)->nodeValue
    ];
    // $data['apply_date'] = $cells->item(1)->nodeValue;
    // $data['nama_pelanggan'] = $cells->item(3)->nodeValue;
    // $data['handphone'] = $cells->item(4)->nodeValue;
    // $data['email'] = $cells->item(5)->nodeValue;
    // $data['survey'] = $cells->item(6)->nodeValue;
    // $data['odp'] = $cells->item(7)->nodeValue;
    // $data['kordinat'] = $cells->item(8)->nodeValue;
    // $data['witel'] = $cells->item(9)->nodeValue;
    // $data['status_data'] = $cells->item(10)->nodeValue;
  }
  print_r($data);
  self::insertOrUpdateTable($data,'data_risma');

  if (curl_errno($ch)) {
      echo 'Error:' . curl_error($ch);
  }
  curl_close($ch);
}

public static function grabstarclickncx_backend($witel)
{
  

  Telegram::sendMessage([
    'chat_id' => '-306306083',
    'parse_mode' => 'html',
    'text' => "Start newsc Grab Backend SC"
  ]);

  $starclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();
  for ($i=0;$i<=6;$i++) {
    $datex = date('d-m-Y',strtotime("-$i days"));
    $ch = curl_init();

    //get session
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);

    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/backend/myindihome/tracking-naf-inbox';
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $starclick->cookie);
    curl_setopt($ch, CURLOPT_HEADER, false);
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "draw=1&columns%5B0%5D%5Bdata%5D=EXTERN_ORDER_ID&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=ORDER_DATE&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=CUSTOMER_NAME&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=LOC_ID&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=ND_POTS&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=STATUS_RESUME&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=WITEL&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=APPOINMENT_DATE&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=KCONTACT&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=INS_ADDRESS&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=PACKAGENAME&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=SCID&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=false&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=USERNAME&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B13%5D%5Bdata%5D=ACTION&columns%5B13%5D%5Bname%5D=&columns%5B13%5D%5Bsearchable%5D=true&columns%5B13%5D%5Borderable%5D=false&columns%5B13%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B13%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=1000&search%5Bvalue%5D=&search%5Bregex%5D=false&type=AO&output=dataTable&inbox=BE&source=2%2C3%2C4&menu=inquiry&Field=ORG&SearchText=".$witel."&StartDate=".$datex."&EndDate=".$datex."&Fieldstatus=");
    $result_x = curl_exec($ch);
    curl_close($ch);

    $result = json_decode($result_x);
    $total = count($result);

    if ($result == null) {
      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Login expired session NCXSC"
      ]);

      return redirect('/')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>LOGIN EXPIRED!</strong>']
      ]);
    }

    $allRecord = array();

    $list = $result->data;
    if(is_array($list) || is_object($list))
    {
      foreach($list as $data)
      {
        if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
        if (!empty($data->APOINTMENT_DATE)) $apointment_date = date('Y-m-d H:i:s',strtotime(@$data->APOINTMENT_DATE));
        $orderCodeINT = (substr(@$data->ORDER_CODE,5,15));

        $allRecord[] = array(
          "ORDER_ID" => @$data->ORDER_ID,
          "ORDER_DATE" => @$orderDate,
          "ORDER_TYPE_ID" => @$data->ORDER_TYPE_ID,
          "EXTERN_ORDER_ID" => @$data->EXTERN_ORDER_ID,
          "NCLI" => @$data->NCLI,
          "CUSTOMER_NAME" => @$data->CUSTOMER_NAME,
          "WITEL" => @$data->WITEL,
          "AGENT_ID" => @$data->AGENT_ID,
          "JENISPSB" => @$data->JENISPSB,
          "SOURCE" => @$data->SOURCE,
          "KODEFIKASI_SC" => @$data->KODEFIKASI_SC,
          "STATUS_RESUME" => @$data->STATUS_RESUME,
          "STATUS_CODE_SC" => @$data->STATUS_CODE_SC,
          "USERNAME" => @$data->USERNAME,
          "CREATEUSERID" => @$data->CREATEUSERID,
          "ORDER_CODE" => @$data->ORDER_CODE,
          "ORDER_CODE_INT" => $orderCodeINT,
          "PACKAGE_NAME" => @$data->PACKAGENAME,
          "STO" => @$data->STO,
          "ODP_ID" => @$data->ODP_ID,
          "RESERVE_TN" => @$data->RESERVE_TN,
          "RESERVE_PORT" => @$data->RESERVE_PORT,
          "ND_INTERNET" => @$data->ND_INTERNET,
          "ND_POTS" => @$data->ND_POTS,
          "GPS_LATITUDE" => @$data->GPS_LATITUDE,
          "GPS_LONGITUDE" => @$data->GPS_LONGITUDE,
          "TN_NUMBER" => @$data->TN_NUMBER,
          "CUSTOMER_NUMBER" => @$data->CUSTOMER_NUMBER,
          "CUSTOMER_ADDR" => @$data->CUSTOMER_ADDR,
          "KCONTACT" => @$data->KCONTACT,
          "APOINTMENT_DATE" => @$apointment_date,
          "INS_ADDRESS" => @$data->INS_ADDRESS,
          "SCID" => @$data->SCID,
          "CITY_NAME" => @$data->CITY_NAME,
          "ACTION" => @$data->ACTION,
          "LOC_ID" => @$data->LOC_ID
        );
        print_r("saving $data->ORDER_ID \n");
      }
    }
      self::insertOrUpdate_backend($allRecord);
      sleep(1);
    }
    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Finish Grab Backend SC Total $total"
    ]);
  }

  public function GrabNossaFixQuery($table){
    // $query = DB::table($table)->where('ODP',NULL)->where('Witel','KALSEL (BANJARMASIN)')->get();
    // foreach ($query as $result){
    //   $DATEK = $result->Datek;
    //   $split = explode(' ',$DATEK);
    //   if (count($split)>1){
    //     DB::table($table)->where('Incident',$result->Incident)->update([
    //       'ODP' => $split[2]
    //     ]);
    //     echo $split[2]."<br />";
    //   }
    // }
    DB::statement("update ".$table." set ODP = SUBSTRING_INDEX(SUBSTRING_INDEX(Datek, ' ', 3), ' ', -1) WHERE ODP is null");

  }

  public function starclick(){
    # default
    $start = date('d/m/Y');
    $end = date('d/m/Y');
    $witel = "Kalsel";
    $status = '';

    # get from link
    // $start = Input::get('start');
    // $end = Input::get('end');
    // $witel = Input::get('witel');
    $status = Input::get('status');

    # get from starclick link
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$witel."&Field=ORG&Fieldstatus=".$status."&Fieldwitel=&StartDate=".$start."&EndDate=".$end."&page=1&start=0&limit=10000";
    $result = json_decode(@file_get_contents($link));
    echo $link."<br />";

    # generate result
    var_dump((array)$result->data);
    $array = (array)$result->data;

    # insert ignore
    DB::connection('mysql4')->table('4_0_master_order_1_log_pi')->insertIgnore($array);

  }

  public static function backupSyncNossa(){
    $get_data = DB::SELECT('
    SELECT
    dt.id,
    dt.Ndem,
    dn1l.Incident,
    dt.created_at
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
    WHERE
    dt.jenis_order = "IN" AND
    dn1l.Incident IS NULL AND
    DATE(dt.created_at) BETWEEN "2021-05-18" AND "2021-05-21"
    ');

    foreach($get_data as $data){
      $that = new GrabController();
      $sync = $that->nossaIN($data->Ndem);
      print_r("done $data->Ndem\n");
      sleep(5);
    }

  }

  public function nossaIN($in)
  {
    $user_nossa = DB::table('user_nossa')->where('active',1)->first();

    if($user_nossa == null){
      return back()->with('alerts',[['type' => 'danger', 'text' => 'Maaf <strong>Tidak Ada User yang Active</strong> untuk Digunakan']]);
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $login = curl_exec($ch);

    echo "Login ... &nbsp;";

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    // print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);
    // dd($result);

    echo "Search Incident ... &nbsp;";

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx".$user_nossa->incident,
      "value"=>"=".$in,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"filterrows",
      "targetId"=>"mx".$user_nossa->statusx,
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx".$user_nossa->owner,
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );

    //print_r($postdata);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    // print_r($result);
    //
    // dd($result);
    $posisi = strpos($result,'<component vis="true" id="mx306_holder" compid="mx306"><![CDATA[<a ctype="label"  id="mx306"  href="')+100;
    //echo "posisi : ".$posisi;
    $asd = substr_replace($result, "", 0, $posisi);
    $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx306"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));
    // echo "test ".($asd)." #";
    curl_setopt($ch, CURLOPT_URL, $asd);

    $result = curl_exec($ch);
    curl_close ($ch);
    $result=str_replace('&nbsp;', ' ', $result);
    $columns = array("Incident","Customer_Name","Contact_Name","Contact_Phone","Contact_Email","Summary","Owner_Group","Owner","Last_Updated_Work_Log","Last_Work_Log_Date","Count_CustInfo","Last_CustInfo","Assigned_to","Booking_Date","Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID","External_Ticket_Status","Segment","Channel","Customer_Segment","Customer_Type","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Related_to_Global_Issue","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Pending_Reason","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","Incident_Domain","ONU_Rx_Before_After","SCC_fisik_inet","SCC_logic","Complete_WO_by","Kode_Produk","ID");
    // dd($columns);
    $dom = @\DOMDocument::loadHTML(trim($result));
    // dd($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++){
        $td = $cells->item($j);
        if($j<>0){
          if ($j==13) {
            $new_datetime = "";
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>1){
              $old_date = explode('-', $old_datetime[0]);
							if (count($old_date)>0){
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];
              $data[$columns[$j]] = $new_datetime;
							}
            }
          }

          if ($j==34) {
            $old_datetime = explode(' ', $td->nodeValue);
            if (count($old_datetime)>1){
              $old_date = explode('-', $old_datetime[0]);
              $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
              $new_datetime = $new_date.' '.$old_datetime[1];

              $getTime = explode(':', $old_datetime[1]);

              if ($getTime[0]>=17 || $getTime[0]<8) {
                $data['Hold'] = '1';
              } else {
                $data['Hold'] = '0';
              }

              if ($getTime[0]>=17) {
                $newDatetime = new DateTime($new_date);
                $newDatetime->modify('+1 day');
                $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
              } else if ($getTime[0]<8) {
                $data['UmurDatetime'] = date($new_date.' 08:00:02');

              } else {
                $data['UmurDatetime'] = $new_datetime;
              }

              $data[$columns[$j]] = $new_datetime;

            } else {
              $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
            }
          } else {
          $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
          }

        }
          if($j==0) {
            $data['ID'] =  substr($td->nodeValue,2);
            $data['Incident'] = $in;
          }
      }
      if($data['Last_Work_Log_Date']==""){
        $data['Last_Work_Log_Datetime'] = "0000-00-00 00:00:00";
      }else{
        $LWLD = strtotime($data['Last_Work_Log_Date']);
        $data['Last_Work_Log_Datetime'] = date('Y-m-d H:i:s', $LWLD);
      }
      $data['Reported_Datex'] = date('Y-m-d H:i:s', strtotime($data['Reported_Date']));
      $data['Booking_Datex'] = date('Y-m-d H:i:s', strtotime($data['Booking_Date']));
      $data['update_grab'] = date('Y-m-d H:i:s');
      $result[] = $data;
    }

    echo "Result Incident ...";

    $result = $data;
    // dd($result);

    echo "Grab Incident ... &nbsp;";
    $auth = session('auth');

    $bypass = DB::table('sync_nossa_bypass')->where('nik', $auth->id_karyawan)->first();
    if(count($bypass) <> 1)
    {
      if($data['Assigned_to']=="" && !in_array($data['Customer_Segment'], ["DES","DBS","DGS","DSO"]))
      {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Tiket Terbaca Belum di Assign ke Teknisi. Assign Dulu Baru Dispatch !!! Pintar lah']]);
      }
    }

    // $sto = ($data['Workzone']);
    // if($sto == "KDG" || $sto == "PGT" || $sto == "TKI" || $sto == "KPL" || $sto == "RTA" || $sto == "BRI" || $sto == "PLE" || $sto == "BTB"){
    //   $odp = $sto;
    // }else{
    //   $substr_odp = (substr($data['Datek'],12,7));
    //   $exp_odp = (explode("/",$substr_odp));
    //   $odp = $exp_odp[0];
    // }

    DB::table('data_nossa_1_log')->where('ID',$data['ID'])->delete();
    DB::table('data_nossa_1_log')->insert($result);
    // print_r($result);

    echo "Success Grab Incident ...";

    return redirect('/assurance/dispatch/'.$in)->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> UPDATE']
    ]);

    // $check_fitur = DB::table('fitur')->first();
    // $check_tim = DB::SELECT('SELECT * FROM regu r WHERE r.fitur = "ACTIVE" AND r.area LIKE "%'.$odp.'%" ');
    // if($check_fitur->disp_assurance==1 && $odp<>NULL && (count($check_tim)>0)){

    //   return redirect('/assurance/auto/dispatch/'.$odp.'/'.$in);

    // }else{

    //   return redirect('/assurance/dispatch/'.$in)->with('alerts', [
    //     ['type' => 'success', 'text' => '<strong>SUKSES</strong> UPDATE']
    //   ]);

    // }
  }

  public function nossaINx($in){
    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);

    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    //$value = $input->item($i)->getAttribute("value");
    print_r($input);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

    $event = json_encode(array((object)array(
      "type"=>"setvalue",
      "targetId"=>"mx".$user_nossa->incident,
      "value"=>"=".$in,
      "requestType"=>"ASYNC",
      "csrftokenholder"=>$csrftokenholder
    ),(object)array(
      "type"=>"filterrows",
      "targetId"=>"mx454",
      "value"=>"",
      "requestType"=>"SYNC",
      "csrftokenholder"=>$csrftokenholder)));

    $postdata = http_build_query(
      array(
          "uisessionid" => $uisesid,
          "csrftoken" => $csrftokenholder,
          "currentfocus" => "mx1077",
          "scrollleftpos" => "0",
          "scrolltoppos" => "0",
          "requesttype" => "SYNC",
          "responsetype" => "text/xml",
          "events" => $event
      )
    );

    //print_r($postdata);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    $result = curl_exec($ch);
    //print_r($result);
    //
    //dd($result);
    $posisi = strpos($result,'<component vis="true" id="mx306_holder" compid="mx306"><![CDATA[<a ctype="label"  id="mx306"  href="')+100;
    //echo "posisi : ".$posisi;
    $asd = substr_replace($result, "", 0, $posisi);
    $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx306"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));
    //echo "test ".($asd)." #";
    curl_setopt($ch, CURLOPT_URL, $asd);

    $result = curl_exec($ch);
    curl_close ($ch);
    $result=str_replace('&nbsp;', ' ', $result);
    //dd($result);

        $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

    //$result = ;
    $dom = @\DOMDocument::loadHTML(trim($result));
    //print_r($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $incident = '';
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
      $new_datatime = "";
      $cells = $rows->item($i)->getElementsByTagName('td');
      $old_datetime = explode(' ', $cells->item(13)->nodeValue);
      if (count($old_datetime)>0){
        $old_date = explode('-', $old_datetime[0]);
        $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
        $new_datetime = $new_date.' '.$old_datetime[1];
      }
      $data = array();
      $incident = $cells->item(0)->nodeValue;
      $data['Owner_Group'] = $cells->item(6)->nodeValue;
      $data['Owner'] = $cells->item(7)->nodeValue;
      $data['Last_Work_Log_Date'] = $cells->item(9)->nodeValue;
      $data['Assigned_To'] = $cells->item(12)->nodeValue;
      $data['Booking_Date'] = $new_datetime;
      $data['Assigned_By'] = $cells->item(14)->nodeValue;
      $data['TTR_Customer'] = $cells->item(34)->nodeValue;
      $data['TTR_Nasional'] = $cells->item(35)->nodeValue;
      $data['TTR_Regional'] = $cells->item(36)->nodeValue;
      $data['TTR_Witel'] = $cells->item(37)->nodeValue;
      $data['TTR_Mitra'] = $cells->item(38)->nodeValue;
      $data['TTR_Agent'] = $cells->item(39)->nodeValue;
      $data['TTR_Pending'] = $cells->item(40)->nodeValue;
      $data['Status'] = $cells->item(41)->nodeValue;
      $data['Last_Update_Ticket'] = $cells->item(44)->nodeValue;
      $data['Status_Date'] = $cells->item(45)->nodeValue;
    }

    //print_r($result);
    if ($incident<>''){
      DB::table('data_nossa_1_log')->where('Incident',$incident)->update($data);
    }
    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> UPDATE']
    ]);
  }

  public static function grabUnspecSaldoSemesta()
  {  
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_URL, 'https://access-quality.telkom.co.id/rekap_unspec_sektor/dashboard_semesta/export_detail_all.php?jenis=SEKTORWOI&regional=6&witel=KALSEL&sektor=ALL');
    $result = curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
      1 => 'REG', 'WITEL', 'SEKTOR', 'NODE_IP', 'SHELF|SLOT|PORT|ONU_ID', 'CMDF', 'RK', 'DP', 'ND', 'NAMA', 'ALAMAT', 'TANGGAL_PS', 'STATUS_INET', 'ONU_RX_POWER', 'TANGGAL_UKUR', 'NOMOR_TIKET', 'STATUS_TIKET', 'TYPE_PELANGGAN', 'PRIORITAS'
  );
  $result = array();
  for ($i = 1, $count = $rows->length; $i < $count; $i++)
  {
    $cells = $rows->item($i)->getElementsByTagName('td');
    $data = array();
    for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
    {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
    }
    $data['TGL_GRAB'] = date('Y-m-d H:i:s');
    $IN = (substr($data['NOMOR_TIKET'],2,8));
    $data['ID_ORDER'] = $IN;
    $result[] = $data;
  }
  $srcarr=array_chunk($result,500);
  DB::table('unspec_saldo_semesta')->truncate();
  foreach($srcarr as $item) {
      DB::table('unspec_saldo_semesta')->insert($item);
  }

  print_r("finish grab unspec saldo semesta");

  Telegram::sendMessage([
    'chat_id' => '-306306083',
    'parse_mode' => 'html',
    'text' => "Finish Grab Unspec Saldo Semesta"
  ]);

  dd($result);
  }

  public static function generateTokenAPIQC()
  {
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.128.21.37/qc-borneo-validation/oauth/token',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => array('grant_type' => 'password','client_id' => '2','client_secret' => '9AFchxyBCovMrpdcJ6pDT9BQVE1uxreq66cTsBsa','username' => 'qc_borneo@qc.com','password' => 'Qcborneo123!','scope' => '*'),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    print_r("$response\n");
    $token = json_decode($response);

    DB::transaction(function() use($token) {
      DB::table('qc_borneo_tr6_api')->update([
        'token_type'    => $token->token_type,
        'expires_in'    => $token->expires_in,
        'access_token'  => $token->access_token,
        'refresh_token' => $token->refresh_token,
        'updated_at'    => date('Y-m-d H:i:s')
      ]);
    });

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Success Generate Token API QC Borneo ".date('Y-m-d H:i:s')
    ]);
  }

  public static function qcborneo_tr6($bulan,$kat)
  {
    

    if ($bulan=="bulan") { $datex = date('Y-m-01'); $date = date("Y-m-t", strtotime( date('Y-m-01'))); } elseif ($bulan=="2021") { $datex = date('Y-01-01'); $date = date('Y-m-d'); } elseif ($bulan=="2020") { $datex = "2020-01-01"; $date = "2020-12-31"; } elseif ($bulan=="2019") { $datex = "2019-01-01"; $date = "2019-12-31"; } elseif ($bulan=="2019B1P1") { $datex = "2019-01-01"; $date = "2019-01-15"; } elseif ($bulan=="2019B1P2") { $datex = "2019-01-16"; $date = "2019-01-31"; } elseif ($bulan=="2019B2P1") { $datex = "2019-02-01"; $date = "2019-02-14"; } elseif ($bulan=="2019B2P2") { $datex = "2019-02-15"; $date = "2019-02-28"; } elseif ($bulan=="2019B3P1") { $datex = "2019-03-01"; $date = "2019-03-15"; } elseif ($bulan=="2019B3P2") { $datex = "2019-03-16"; $date = "2019-03-31"; } elseif ($bulan=="2019B4P1") { $datex = "2019-04-01"; $date = "2019-04-15"; } elseif ($bulan=="2019B4P2") { $datex = "2019-04-16"; $date = "2019-04-30"; } elseif ($bulan=="2019B5P1") { $datex = "2019-05-01"; $date = "2019-05-15"; } elseif ($bulan=="2019B5P2") { $datex = "2019-05-16"; $date = "2019-05-31"; } elseif ($bulan=="2019B6P1") { $datex = "2019-06-01"; $date = "2019-06-15"; } elseif ($bulan=="2019B6P2") { $datex = "2019-06-16"; $date = "2019-06-30"; } elseif ($bulan=="2019B7P1") { $datex = "2019-07-01"; $date = "2019-07-15"; } elseif ($bulan=="2019B7P2") { $datex = "2019-07-16"; $date = "2019-07-31"; } elseif ($bulan=="2019B8P1") { $datex = "2019-08-01"; $date = "2019-08-15"; } elseif ($bulan=="2019B8P2") { $datex = "2019-08-16"; $date = "2019-08-31"; } elseif ($bulan=="2019B9P1") { $datex = "2019-09-01"; $date = "2019-09-15"; } elseif ($bulan=="2019B9P2") { $datex = "2019-09-16"; $date = "2019-09-30"; } elseif ($bulan=="2019B10P1") { $datex = "2019-10-01"; $date = "2019-10-15"; } elseif ($bulan=="2019B10P2") { $datex = "2019-10-16"; $date = "2019-10-31"; } elseif ($bulan=="2019B11P1") { $datex = "2019-11-01"; $date = "2019-11-15"; } elseif ($bulan=="2019B11P2") { $datex = "2019-11-16"; $date = "2019-11-30"; } elseif ($bulan=="2019B12P1") { $datex = "2019-12-01"; $date = "2019-12-15"; } elseif ($bulan=="2019B12P2") { $datex = "2019-12-16"; $date = "2019-12-31"; } elseif ($bulan=="2020B1P1") { $datex = "2020-01-01"; $date = "2020-01-15"; } elseif ($bulan=="2020B1P2") { $datex = "2020-01-16"; $date = "2020-01-31"; } elseif ($bulan=="2020B2P1") { $datex = "2020-02-01"; $date = "2020-02-15"; } elseif ($bulan=="2020B2P2") { $datex = "2020-02-16"; $date = "2020-02-29"; } elseif ($bulan=="2020B3P1") { $datex = "2020-03-01"; $date = "2020-03-15"; } elseif ($bulan=="2020B3P2") { $datex = "2020-03-16"; $date = "2020-03-31"; } elseif ($bulan=="2020B4P1") { $datex = "2020-04-01"; $date = "2020-04-15"; } elseif ($bulan=="2020B4P2") { $datex = "2020-04-16"; $date = "2020-04-30"; } elseif ($bulan=="2020B5P1") { $datex = "2020-05-01"; $date = "2020-05-15"; } elseif ($bulan=="2020B5P2") { $datex = "2020-05-16"; $date = "2020-05-31"; } elseif  ($bulan=="2020B6P1") { $datex = "2020-06-01"; $date = "2020-06-15"; } elseif ($bulan=="2020B6P2") { $datex = "2020-06-16"; $date = "2020-06-30"; } elseif ($bulan=="2020B7P1") { $datex = "2020-07-01"; $date = "2020-07-15"; } elseif ($bulan=="2020B7P2") { $datex = "2020-07-16"; $date = "2020-07-31"; } elseif ($bulan=="2020B8P1") { $datex = "2020-08-01"; $date = "2020-08-15"; } elseif ($bulan=="2020B8P2") { $datex = "2020-08-16"; $date = "2020-08-31"; } elseif ($bulan=="2020B9P1") { $datex = "2020-09-01"; $date = "2020-09-15"; } elseif ($bulan=="2020B9P2") { $datex = "2020-09-16"; $date = "2020-09-30"; } elseif ($bulan=="2020B10P1") { $datex = "2020-10-01"; $date = "2020-10-15"; } elseif ($bulan=="2020B10P2") { $datex = "2020-10-16"; $date = "2020-10-31"; } elseif ($bulan=="2020B11P1") { $datex = "2020-11-01"; $date = "2020-11-15"; } elseif ($bulan=="2020B11P2") { $datex = "2020-11-16"; $date = "2020-11-30"; } elseif ($bulan=="2020B12P1") { $datex = "2020-12-01"; $date = "2020-12-15"; } elseif ($bulan=="2020B12P2") { $datex = "2020-12-16"; $date = "2020-12-31"; } elseif ($bulan=="2021B1P1") { $datex = "2021-01-01"; $date = "2021-01-15"; } elseif ($bulan=="2021B1P2") { $datex = "2021-01-16"; $date = "2021-01-31"; } elseif ($bulan=="2021B2P1") { $datex = "2021-02-01"; $date = "2021-02-14"; } elseif ($bulan=="2021B2P2") { $datex = "2021-02-15"; $date = "2021-02-28"; } elseif ($bulan=="2021B3P1") { $datex = "2021-03-01"; $date = "2021-03-15"; } elseif ($bulan=="2021B3P2") { $datex = "2021-03-16"; $date = "2021-03-31"; } elseif ($bulan=="2021B4P1") { $datex = "2021-04-01"; $date = "2021-04-15"; } elseif ($bulan=="2021B4P2") { $datex = "2021-04-16"; $date = "2021-04-30"; } elseif ($bulan=="2021B5P1") { $datex = "2021-05-01"; $date = "2021-05-15"; } elseif ($bulan=="2021B5P2") { $datex = "2021-05-16"; $date = "2021-05-31"; } elseif ($bulan=="2021B6P1") { $datex = "2021-06-01"; $date = "2021-06-15"; } elseif ($bulan=="2021B6P2") { $datex = "2021-06-16"; $date = "2021-06-30"; } elseif ($bulan=="2021B7P1") { $datex = "2021-07-01"; $date = "2021-07-15"; } elseif ($bulan=="2021B7P2") { $datex = "2021-07-16"; $date = "2021-07-31"; } elseif ($bulan=="2021B8P1") { $datex = "2021-08-01"; $date = "2021-08-15"; } elseif ($bulan=="2021B8P2") { $datex = "2021-08-16"; $date = "2021-08-31"; } elseif ($bulan=="2021B9P1") { $datex = "2021-09-01"; $date = "2021-09-15"; } elseif ($bulan=="2021B9P2") { $datex = "2021-09-16"; $date = "2021-09-30"; } elseif ($bulan=="2021B10P1") { $datex = "2021-10-01"; $date = "2021-10-15"; } elseif ($bulan=="2021B10P2") { $datex = "2021-10-16"; $date = "2021-10-31"; } elseif ($bulan=="2021B11P1") { $datex = "2021-11-01"; $date = "2021-11-15"; } elseif ($bulan=="2021B11P2") { $datex = "2021-11-16"; $date = "2021-11-30"; } elseif ($bulan=="2021B12P1") { $datex = "2021-12-01"; $date = "2021-12-15"; } elseif ($bulan=="2021B12P2") { $datex = "2021-12-16"; $date = "2021-12-31"; }

    if ($kat=="VALID1_ASO") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=aso'; } elseif ($kat=="VALID1_DAMAN") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=daman'; } elseif ($kat=="VALID1_ROCDAMAN") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=cc'; } elseif ($kat=="VALID1_HS") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=hs'; } elseif ($kat=="VALID1_AMO") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=amo'; } elseif ($kat=="DAPROS") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=dapros'; } elseif ($kat=="LOLOS") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=valid_include'; } elseif ($kat=="VALID2") { $link = 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$datex.'&endDate='.$date.'&column=validasi2'; }

    $oauth = DB::table('qc_borneo_tr6_api')->first();
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => ''.$link.'',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: '.$oauth->token_type.' '.$oauth->access_token
      ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);

    $decode = json_decode($response);
    if (count(@$decode->data)>0) {
      $data = $decode->data;
      $total = count($data);
      $insert = [];
      // DB::table('qc_borneo_tr6')->where('kategori',$kat)->whereBetween('dtPs', [$datex,$date])->delete();
      foreach ($data as $d)
      {
          if ($d->foto_rumah <> "OK") {
            $foto_rumah = 1;
          } else {
            $foto_rumah = 0;
          }
          if ($d->foto_teknisi <> "OK") {
            $foto_teknisi = 1;
          } else {
            $foto_teknisi = 0;
          }
          if ($d->foto_odp <> "OK") {
            $foto_odp = 1;
          } else {
            $foto_odp = 0;
          }
          if ($d->foto_redaman <> "OK") {
            $foto_redaman = 1;
          } else {
            $foto_redaman = 0;
          }
          if ($d->foto_ont <> "OK") {
            $foto_ont = 1;
          } else {
            $foto_ont = 0;
          }
          if ($d->foto_berita <> "OK") {
            $foto_berita = 1;
          } else {
            $foto_berita = 0;
          }
          if ($d->tagging_lokasi <> "OK") {
            $tagging_lokasi = 1;
          } else {
            $tagging_lokasi = 0;
          }
          if ($d->foto_pernyataan <> "OK") {
            $foto_pernyataan = 1;
          } else {
            $foto_pernyataan = 0;
          }
          if ($d->foto_svm <> "OK") {
            $foto_svm = 1;
          } else {
            $foto_svm = 0;
          }

          if ($d->tag_unit <> "")
          {
            $tag_unit = $d->tag_unit;
          } else {
            $tag_unit = null;
          }

          $nok = ($foto_rumah + $foto_teknisi + $foto_odp + $foto_redaman + $foto_ont + $foto_berita + $tagging_lokasi + $foto_pernyataan + $foto_svm);

          $insert[] = [
            'witel'             =>   $d->witel,
            'datel'             =>   $d->datel,
            'sto'               =>   $d->sto,
            'sc'                =>   $d->sc,
            'no_inet'           =>   $d->nomor_internet,
            'no_voice'          =>   $d->nomor_telepon,
            'nama'              =>   $d->nama_pelanggan,
            'datePs'            =>   $d->date_ps,
            'dtPs'              =>   date('Y-m-d', strtotime($d->date_ps)),
            'id_sales'          =>   $d->kd_sales,
            'foto_rumah'        =>   $d->foto_rumah,
            'foto_teknisi'      =>   $d->foto_teknisi,
            'foto_odp'          =>   $d->foto_odp,
            'foto_redaman'      =>   $d->foto_redaman,
            'foto_cpe_layanan'  =>   $d->foto_ont,
            'foto_berita_acara' =>   $d->foto_berita,
            'tag_lokasi'        =>   $d->tagging_lokasi,
            'foto_surat'        =>   $d->foto_pernyataan,
            'foto_profile_myih' =>   $d->foto_svm,
            'tag_unit'          =>   $tag_unit,
            'last_updated'      =>   $d->date_update,
            'date_created'      =>   $d->date_create,
            'kategori'          =>   $kat,
            'nok'               =>   $nok
          ];
      }

      // dd($insert);

      $chunk = array_chunk($insert,500);
      foreach($chunk as $data)
      {
        self::insertOrUpdateTable($data,'qc_borneo_tr6');
      }

      DB::statement(' UPDATE qc_borneo_tr6 SET tag_unit = null WHERE tag_unit = "" ');

      print_r("Finish Grab QCBorneo TR6 $bulan $kat Total $total \n");
    } else {
      print_r("Failed Grab QCBorneo TR6 $bulan $kat \n");
    }
    sleep(1);
  }

  public static function allQCBorneoTR6()
  {
    
    $that = new GrabController();

    $date_periode = ['2019B1P1','2019B1P2','2019B2P1','2019B2P2','2019B3P1','2019B3P2','2019B4P1','2019B4P2','2019B5P1','2019B5P2','2019B6P1','2019B6P2','2019B7P1','2019B7P2','2019B8P1','2019B8P2','2019B9P1','2019B9P2','2019B10P1','2019B10P2','2019B11P1','2019B11P2','2019B12P1','2019B12P2','2020B1P1','2020B1P2','2020B2P1','2020B2P2','2020B3P1','2020B3P2','2020B4P1','2020B4P2','2020B5P1','2020B5P2','2020B6P1','2020B6P2','2020B7P1','2020B7P2','2020B8P1','2020B8P2','2020B9P1','2020B9P2','2020B10P1','2020B10P2','2020B11P1','2020B11P2','2020B12P1','2020B12P2','2021B1P1','2021B1P2','2021B2P1','2021B2P2','2021B3P1','2021B3P2','2021B4P1','2021B4P2','2021B5P1','2021B5P2','2021B6P1','2021B6P2','2021B7P1','2021B7P2','2021B8P1','2021B8P2','2021B9P1','2021B9P2','2021B10P1','2021B10P2','2021B11P1','2021B11P2','2021B12P1','2021B12P2'];

    $new_periode = ['2020B12P1','2020B12P2','2021B1P1','2021B1P2','2021B2P1','2021B2P2','2021B3P1','2021B3P2','2021B4P1','2021B4P2','2021B5P1','2021B5P2','2021B6P1','2021B6P2','2021B7P1','2021B7P2','2021B8P1','2021B8P2','2021B9P1','2021B9P2','2021B10P1','2021B10P2','2021B11P1','2021B11P2'];

    $tag_unit = ['DAPROS','LOLOS','VALID1_DAMAN','VALID1_ROCDAMAN','VALID1_HS','VALID1_AMO','VALID1_ASO','VALID2'];

    DB::table('qc_borneo_tr6')->truncate();

    foreach ($new_periode as $periode) {
      foreach ($tag_unit as $tag) {
        $that->qcborneo_tr6($periode, $tag);
      }
    }

    // delete duplicate
    print_r("Checking Duplicate QCBorneo TR6 \n");
    $duplicated = DB::SELECT('SELECT sc, datePs, COUNT(sc) FROM qc_borneo_tr6 GROUP BY sc HAVING COUNT(sc)>1 ORDER BY datePs DESC');
    foreach($duplicated as $duplicate)
    {
      $data = DB::table('qc_borneo_tr6')->where('sc', $duplicate->sc)->orderBy('last_updated','ASC')->limit(1)->first();
      DB::table('qc_borneo_tr6')->where('id', $data->id)->delete();
    }
    // DB::statement('DELETE a1 FROM qc_borneo_tr6 a1, qc_borneo_tr6 a2 WHERE a1.id > a2.id AND a1.sc = a2.sc');

    DB::transaction( function(){
      DB::table('qc_borneo_tr6_log')->where('id','1')->update([
        'datetime'    => date('Y-m-d H:i:s')
      ]);
    });
  }

  public static function QCBorneoTR6All()
  {
    

    $startDate = "2020-12-01";
    $endDate = date('Y-m-d');

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $oauth = DB::table('qc_borneo_tr6_api')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.128.21.37/qc-borneo-validation/api/v1/qc-detail?witel=KALSEL&startDate='.$startDate.'&endDate='.$endDate.'&column=all',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: '.$oauth->token_type.' '.$oauth->access_token
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $decode = json_decode($response);

    if (count(@$decode->data)>0)
    {
      $data = $decode->data;
      $total = count($data);

      print_r("Start Grab All QCBorneo TR6 $startDate $endDate Total $total \n");

      $insert = [];

      DB::table('qc_borneo_tr6')->truncate();

      foreach ($data as $d)
      {
          if ($d->foto_rumah <> "OK") {
            $foto_rumah = 1;
          } else {
            $foto_rumah = 0;
          }
          if ($d->foto_teknisi <> "OK") {
            $foto_teknisi = 1;
          } else {
            $foto_teknisi = 0;
          }
          if ($d->foto_odp <> "OK") {
            $foto_odp = 1;
          } else {
            $foto_odp = 0;
          }
          if ($d->foto_redaman <> "OK") {
            $foto_redaman = 1;
          } else {
            $foto_redaman = 0;
          }
          if ($d->foto_ont <> "OK") {
            $foto_ont = 1;
          } else {
            $foto_ont = 0;
          }
          if ($d->foto_berita <> "OK") {
            $foto_berita = 1;
          } else {
            $foto_berita = 0;
          }
          if ($d->tagging_lokasi <> "OK") {
            $tagging_lokasi = 1;
          } else {
            $tagging_lokasi = 0;
          }
          if ($d->foto_pernyataan <> "OK") {
            $foto_pernyataan = 1;
          } else {
            $foto_pernyataan = 0;
          }
          if ($d->foto_svm <> "OK") {
            $foto_svm = 1;
          } else {
            $foto_svm = 0;
          }

          if ($d->tag_unit <> "")
          {
            $tag_unit = $d->tag_unit;
          } else {
            $tag_unit = null;
          }

          $nok = ($foto_rumah + $foto_teknisi + $foto_odp + $foto_redaman + $foto_ont + $foto_berita + $tagging_lokasi + $foto_pernyataan + $foto_svm);

          $insert[] = [
            'witel'             =>   $d->witel,
            'datel'             =>   $d->datel,
            'sto'               =>   $d->sto,
            'sc'                =>   $d->sc,
            'no_inet'           =>   $d->nomor_internet,
            'no_voice'          =>   $d->nomor_telepon,
            'nama'              =>   $d->nama_pelanggan,
            'datePs'            =>   $d->date_ps,
            'dtPs'              =>   date('Y-m-d', strtotime($d->date_ps)),
            'id_sales'          =>   $d->kd_sales,
            'foto_rumah'        =>   $d->foto_rumah,
            'foto_teknisi'      =>   $d->foto_teknisi,
            'foto_odp'          =>   $d->foto_odp,
            'foto_redaman'      =>   $d->foto_redaman,
            'foto_cpe_layanan'  =>   $d->foto_ont,
            'foto_berita_acara' =>   $d->foto_berita,
            'tag_lokasi'        =>   $d->tagging_lokasi,
            'foto_surat'        =>   $d->foto_pernyataan,
            'foto_profile_myih' =>   $d->foto_svm,
            'tag_unit'          =>   $tag_unit,
            'last_updated'      =>   $d->date_update,
            'date_created'      =>   $d->date_create,
            // 'kategori'          =>   $kat,
            'nok'               =>   $nok
          ];
      }

      $chunk = array_chunk($insert,500);
      foreach($chunk as $data)
      {
        self::insertOrUpdateTable($data,'qc_borneo_tr6');
      }

      DB::statement(' UPDATE qc_borneo_tr6 SET tag_unit = null WHERE tag_unit = "" ');

      DB::statement(' DELETE a1 FROM qc_borneo_tr6 a1, qc_borneo_tr6 a2 WHERE a1.id > a2.id AND a1.sc = a2.sc ');

      DB::transaction( function(){
        DB::table('qc_borneo_tr6_log')->where('id','1')->update([
          'datetime'    => date('Y-m-d H:i:s')
        ]);
      });

      print_r("Finish Grab All QCBorneo TR6 $startDate $endDate Total $total \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Finish Grab All QCBorneo TR6 $startDate $endDate Total <b>$total</b>"
      ]);

    } else {
      print_r("Failed Grab All QCBorneo TR6 $startDate $endDate \n");
    }
  }

  public static function daprosOntPremium()
  {
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://dns.warriors.id/menu/dashboard_ont_premium_exp.php?kode=valid&witel=KALSEL',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
      1 =>
      'reg', 'witel', 'nama_pelanggan', 'alamat_pelanggan', 'no_tlp', 'no_internet', 'kw', 'type_ont', 'average_arpu', 'no_hp', 'jenis_dapros'
    );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $result[] = $data;
    }

    foreach ($result as $value)
    {
      $check_data = DB::table('ont_premium_order')->where('no_inet', $value['no_internet'])->orderBy('tanggal_order', 'DESC')->first();
      if (!empty($check_data))
      {
        if ($check_data != 'UNDISPATCH_DNS')
        {
          $dispatch_by = 'UNDISPATCH_DNS';
        } else {
          $dispatch_by = $check_data->dispatch_by;
        }
      } else {
        $dispatch_by = NULL;
      }

      $check_history = DB::table('Data_Pelanggan_Starclick')
      ->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderIdInteger','=','dispatch_teknisi.NO_ORDER')
      ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      ->select('Data_Pelanggan_Starclick.internet','dispatch_teknisi.manja','psb_laporan.noPelangganAktif')
      ->where('Data_Pelanggan_Starclick.internet', $value['no_internet'])
      ->orderBy('Data_Pelanggan_Starclick.orderDate','desc')
      ->groupBy('Data_Pelanggan_Starclick.internet')
      ->first();

      $insert = [
        'nama_pelanggan' => $value['nama_pelanggan'],
        'alamat' => $value['alamat_pelanggan'],
        'no_voice' => $value['no_tlp'],
        'no_inet' => $value['no_internet'],
        'type_ont' => $value['type_ont'],
        'no_hp' => $value['no_hp'] ?? $check_history->noPelangganAktif,
        'jenis_dapros' => $value['jenis_dapros'],
        'tanggal_order' => date('Y-m-d H:i:s'),
        'ket_manja' => $check_history->manja,
        'dispatch_by' => $dispatch_by
      ];

      $check = DB::table('ont_premium_order')->where('no_inet', $value['no_internet'])->first();

      if (count($check) > 0)
      {
        DB::table('ont_premium_order')->where('no_inet', $value['no_internet'])->update($insert);
      } else {
        DB::table('ont_premium_order')->insert($insert);
      }
    }

    print_r("Finish Grab Dapros ONT Premium  \n");
    sleep(1);
    
  }

  public function grabNossa(){
    $this->nossa('KALSEL (BANJARMASIN)',1);
    // $this->nossa('TA HD WITEL KALBAR',0);
    // $this->nossa('TA HD WITEL KALTENG',0);
  }

  public function grabNossaMaintenance(){
    $this->nossa('ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)',1);
    // $this->nossa('ACCESS MAINTENANCE WITEL KALBAR (PONTIANAK)',0);
    // $this->nossa('ACCESS MAINTENANCE WITEL KALTENG (PALANGKARAYA)',0);
  }

  public function grabNossaWOC(){
    $this->nossa('WOC FULFILLMENT KALSEL (BANJARMASIN)',1);
    // $this->nossa('WOC FULFILLMENT KALBAR (PONTIANAK)',0);
    // $this->nossa('WOC FULFILLMENT KALTENG (PALANGKARAYA)',0);
  }

  public function testNossa1(){

  }

  public function testNossa(){
      
    $user_nossa = GrabModel::user_nossa()[0];
    $ch = curl_init();
    // Reset all previously set options
    curl_reset($ch);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $login = curl_exec($ch);
    // if ($user_nossa->umurlogin>1){
    // print_r($login);
    $dom = @\DOMDocument::loadHTML(trim($login));
    $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");
    echo "umur ($user_nossa->umurlogin)";

    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
    $result = curl_exec($ch);
    // print_r($result);
    curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
    $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");
    DB::table('user_nossa')->update([
      'uisesid' => $uisesid,
      'csrftokenholder' => $csrftokenholder,
      'updatetime' => date('Y-m-d H:i:s')
    ]);
  // } else {
  //   $uisesid = $user_nossa->uisesid;
  //   $csrftokenholder = $user_nossa->csrftokenholder;
  // }

          $event = json_encode(array((object)array(
            "type"=>"setvalue",
            "targetId"=>"mx".$user_nossa->owner,
            "value"=>"=TA HD WITEL KALSEL",
            "requestType"=>"ASYNC",
            "csrftokenholder"=>$csrftokenholder
          ),(object)array(
            "type"=>"setvalue",
            "targetId"=>"mx".$user_nossa->statusx,
            "value"=>"=QUEUED,=SLAHOLD,=BACKEND",
            "requestType"=>"ASYNC",
            "csrftokenholder"=>$csrftokenholder
          ),(object)array(
            "type"=>"filterrows",
            "targetId"=>"mx452",
            "value"=>"",
            "requestType"=>"SYNC",
            "csrftokenholder"=>$csrftokenholder)));

          $postdata = http_build_query(
            array(
                "uisessionid" => $uisesid,
                "csrftoken" => $csrftokenholder,
                "currentfocus" => "mx4733",
                "scrollleftpos" => "0",
                "scrolltoppos" => "0",
                "requesttype" => "SYNC",
                "responsetype" => "text/xml",
                "events" => $event
            )
          );
          print_r($event);
          echo "<br />";
          print_r($postdata);
          curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
          $result = curl_exec($ch);
          print_r($result);

  }


  public function nossa($loker,$kalsel){
      echo "test";
      $user_nossa = GrabModel::user_nossa()[0];
      $ch = curl_init();
      // Reset all previously set options
      curl_reset($ch);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $login = curl_exec($ch);
      // print_r($login);
      $dom = @\DOMDocument::loadHTML(trim($login));
      $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
      $result = curl_exec($ch);
      // print_r($result);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
      curl_setopt($ch, CURLOPT_POST, false);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
      $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

      $event = json_encode(array((object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->owner,
        "value"=>"=".$loker,
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->statusx,
        "value"=>"=QUEUED,=SLAHOLD,=BACKEND,=FINALCHECK,=MEDIACARE,=SALAMSIM,=RESOLVED",
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"filterrows",
        "targetId"=>"mx452",
        "value"=>"",
        "requestType"=>"SYNC",
        "csrftokenholder"=>$csrftokenholder)));

      $postdata = http_build_query(
        array(
            "uisessionid" => $uisesid,
            "csrftoken" => $csrftokenholder,
            "currentfocus" => "mx".$user_nossa->incident,
            "scrollleftpos" => "0",
            "scrolltoppos" => "0",
            "requesttype" => "SYNC",
            "responsetype" => "text/xml",
            "events" => $event
        )
      );

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      $result = curl_exec($ch);
      // dd($result);
      $posisi = strpos($result,'<component vis="true" id="mx306_holder" compid="mx306"><![CDATA[<a ctype="label"  id="mx306"  href="')+100;
      // print_r($result);
      $asd = substr_replace($result, "", 0, $posisi);
      $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx306"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));

      curl_setopt($ch, CURLOPT_URL, $asd);

      $result = curl_exec($ch);
      curl_close ($ch);
      $result=str_replace('&nbsp;', ' ', $result);
      // dd($result);

      $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

      $dom = @\DOMDocument::loadHTML(trim($result));
      $table = $dom->getElementsByTagName('table')->item(0);
      // dd($table);
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      $roc = array();
      $rock = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');

        // if ($cells->item(48)->nodeValue=="KALSEL (BANJARMASIN)"){
        //   $old_datetime1 = explode(' ', $cells->item(29)->nodeValue);
        //   if (count($old_datetime1)>0){
        //     $old_date1 = explode('-', $old_datetime1[0]);
        //     $new_date1 = $old_date1[2].'-'.$old_date1[1].'-'.$old_date1[0];
        //     $new_datetime1 = $new_date1.' '.$old_datetime1[1];
        //   } else {
        //     $new_datetime1 = "0";
        //   }
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //   $roc[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $roc[$i]['tglOpen'] = $new_datetime1;
        //   $roc[$i]['tgl_open'] = $new_datetime1;
        //   $roc[$i]['CMDF'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto_trim'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['Channel'] = $cells->item(18)->nodeValue;
        //   $roc[$i]['laborcode'] = $cells->item(11)->nodeValue;
        //   $roc[$i]['RK'] = $cells->item(26)->nodeValue;
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //
        //   $rock[$i]['trouble_no'] = $cells->item(0)->nodeValue;
        //   $rock[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $rock[$i]['trouble_opentime'] = $new_datetime1;
        //   // $rock[$i]['tgl_open'] = $new_datetime1;
        //   $rock[$i]['cmdf'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['channel'] = $cells->item(18)->nodeValue;
        //   $rock[$i]['keluhan_desc'] = $cells->item(47)->nodeValue;
        //   $rock[$i]['rk'] = $cells->item(26)->nodeValue;
        //   $rock[$i]['regional'] = $cells->item(46)->nodeValue;
        //   $rock[$i]['witel'] = $cells->item(45)->nodeValue;
        //
        //   $startDate = new DateTime($new_datetime1);
        //   $endDate = new DateTime();
        //   $since_start = $startDate->diff($endDate);
        //
        //   $rock[$i]['hari'] = $cells->item(31)->nodeValue;
        // }
        $data = array();

        for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);

						if ($j==13){
							$old_datetime = explode(' ',$td->nodeValue);
							if (count($old_datetime)>0){

							}
						}

            if ($j==34) {
              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', $old_datetime[0]);
                $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
                $new_datetime = $new_date.' '.$old_datetime[1];

                $getTime = explode(':', $old_datetime[1]);

                if ($getTime[0]>=17 || $getTime[0]<8) {
                  $data['Hold'] = '1';
                } else {
                  $data['Hold'] = '0';
                }

                if ($getTime[0]>=17) {
                  $newDatetime = new DateTime($new_date);
                  $newDatetime->modify('+1 day');
                  $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                  echo $newDatetime->format('Y-m-d 08:00:01');
                } else if ($getTime[0]<8) {
                  $data['UmurDatetime'] = date($new_date.' 08:00:02');

                } else {
                  $data['UmurDatetime'] = $new_datetime;
                }

                $data[$columns[$j]] = $new_datetime;

              } else {
                $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
              }
            } else {
            $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
            }
            if($j==0) {
              $data['ID'] =  substr($td->nodeValue,2);
                echo $td->nodeValue."<br />";

            }
            if ($j==13) {
              $new_datetime = "";

              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', @$old_datetime[0]);
                $new_date = @$old_date[2].'-'.@$old_date[1].'-'.@$old_date[0];
                $new_datetime = $new_date.' '.@$old_datetime[1];
                $data[$columns[$j]] = $new_datetime;
                // $data[$columns[$j]] = $td->nodeValue;
              }

            }
        }
        $result[] = $data;
      }
      //print_r($result);
      DB::table('data_nossa_1')->where('Witel',$loker)->delete();
      if ($kalsel == 1){
        DB::table('roc_active')->truncate();
        DB::table('roc_active')->insert($roc);
        #DB::table('rock_excel')->truncate();
        #DB::table('rock_excel')->insert($rock);
      }
      print_r($roc);
      foreach (array_chunk($result,1000) as $t) {
        DB::table('data_nossa_1')->insertIgnore($t);
        DB::table('data_nossa_1_log')->insertIgnore($t);
      }
  }

public function nossaCust($loker,$kalsel){
      echo "test";
      $user_nossa = GrabModel::user_nossa()[0];
      $ch = curl_init();
      // Reset all previously set options
      curl_reset($ch);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/webclient/login/login.jsp');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookiex.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      $login = curl_exec($ch);
      // print_r($login);
      $dom = @\DOMDocument::loadHTML(trim($login));
      $input = $dom->getElementsByTagName('input')->item(3)->getAttribute("value");

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/login');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "allowinsubframe=null&mobile=false&login=jsp&loginstamp=".$input."&username=".$user_nossa->username."&password=".$user_nossa->password);
      $result = curl_exec($ch);
      // print_r($result);
      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/?event=loadapp&value=incident');
      curl_setopt($ch, CURLOPT_POST, false);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $uisesid = $dom->getElementById('uisessionid')->getAttribute("value");
      $csrftokenholder = $dom->getElementById('csrftokenholder')->getAttribute("value");

      $event = json_encode(array((object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->owner,
        "value"=>"=".$loker,
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"setvalue",
        "targetId"=>"mx".$user_nossa->statusx,
        "value"=>"=QUEUED,=SLAHOLD,=BACKEND,=FINALCHECK,=MEDIACARE,=SALAMSIM,=RESOLVED",
        "requestType"=>"ASYNC",
        "csrftokenholder"=>$csrftokenholder
      ),(object)array(
        "type"=>"filterrows",
        "targetId"=>"mx452",
        "value"=>"",
        "requestType"=>"SYNC",
        "csrftokenholder"=>$csrftokenholder)));

      $postdata = http_build_query(
        array(
            "uisessionid" => $uisesid,
            "csrftoken" => $csrftokenholder,
            "currentfocus" => "mx".$user_nossa->incident,
            "scrollleftpos" => "0",
            "scrolltoppos" => "0",
            "requesttype" => "SYNC",
            "responsetype" => "text/xml",
            "events" => $event
        )
      );

      curl_setopt($ch, CURLOPT_URL, 'https://nossa.telkom.co.id/maximo/ui/maximo.jsp');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      $result = curl_exec($ch);

      $posisi = strpos($result,'<component vis="true" id="mx306_holder" compid="mx306"><![CDATA[<a ctype="label"  id="mx306"  href="')+100;
      //print_r($result);
      $asd = substr_replace($result, "", 0, $posisi);
      $asd = substr_replace($asd, "", strpos($asd,'" onmouseover="return noStatus();" target="_blank"   noclick="1"   targetid="mx306"    class="text tht  " style="display:block;;;;;" title="Download"'), strlen($asd));

      curl_setopt($ch, CURLOPT_URL, $asd);

      $result = curl_exec($ch);
      curl_close ($ch);
      $result=str_replace('&nbsp;', ' ', $result);


      $columns = array("Incident", "Customer_Name", "Contact_Name","Contact_Phone","Contact_Email","Summary", "Owner_Group", "Owner", "Last_Updated_Work_Log", "Last_Work_Log_Date", "Count_CustInfo", "Last_CustInfo", "Assigned_to","Booking_Date", "Assigned_by","Reported_Priority","Source","Subsidiary","External_Ticket_ID", "External_Ticket_Status","Segment","Channel","Customer_Segment","Closed_By","Customer_ID","Service_ID","Service_No","Service_Type","Top_Priority","SLG","Technology","Datek","RK_Name","Induk_Gamas","Reported_Date","LAPUL","GAUL","TTR_Customer","TTR_Nasional","TTR_Regional","TTR_Witel","TTR_Mitra","TTR_Agent","TTR_Pending","Status","Hasil_Ukur","OSM_Resolved_Code","Last_Update_Ticket","Status_Date","Resolved_By","Workzone","Witel","Regional","Incident_Symptom","Solution_Segment","Actual_Solution","ID");

      $dom = @\DOMDocument::loadHTML(trim($result));
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      $roc = array();
      $rock = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');

        // if ($cells->item(48)->nodeValue=="KALSEL (BANJARMASIN)"){
        //   $old_datetime1 = explode(' ', $cells->item(29)->nodeValue);
        //   if (count($old_datetime1)>0){
        //     $old_date1 = explode('-', $old_datetime1[0]);
        //     $new_date1 = $old_date1[2].'-'.$old_date1[1].'-'.$old_date1[0];
        //     $new_datetime1 = $new_date1.' '.$old_datetime1[1];
        //   } else {
        //     $new_datetime1 = "0";
        //   }
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //   $roc[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $roc[$i]['tglOpen'] = $new_datetime1;
        //   $roc[$i]['tgl_open'] = $new_datetime1;
        //   $roc[$i]['CMDF'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto_trim'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $roc[$i]['Channel'] = $cells->item(18)->nodeValue;
        //   $roc[$i]['laborcode'] = $cells->item(11)->nodeValue;
        //   $roc[$i]['RK'] = $cells->item(26)->nodeValue;
        //   $roc[$i]['no_tiket'] = $cells->item(0)->nodeValue;
        //
        //   $rock[$i]['trouble_no'] = $cells->item(0)->nodeValue;
        //   $rock[$i]['headline'] = $cells->item(4)->nodeValue;
        //   $rock[$i]['trouble_opentime'] = $new_datetime1;
        //   // $rock[$i]['tgl_open'] = $new_datetime1;
        //   $rock[$i]['cmdf'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['sto'] = $cells->item(44)->nodeValue;
        //   $rock[$i]['channel'] = $cells->item(18)->nodeValue;
        //   $rock[$i]['keluhan_desc'] = $cells->item(47)->nodeValue;
        //   $rock[$i]['rk'] = $cells->item(26)->nodeValue;
        //   $rock[$i]['regional'] = $cells->item(46)->nodeValue;
        //   $rock[$i]['witel'] = $cells->item(45)->nodeValue;
        //
        //   $startDate = new DateTime($new_datetime1);
        //   $endDate = new DateTime();
        //   $since_start = $startDate->diff($endDate);
        //
        //   $rock[$i]['hari'] = $cells->item(31)->nodeValue;
        // }
        $data = array();

        for ($j = 0, $jcount = count($columns)-1; $j < $jcount; $j++)
        {
            $td = $cells->item($j);

            if ($j==34) {
              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', $old_datetime[0]);
                $new_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];
                $new_datetime = $new_date.' '.$old_datetime[1];

                $getTime = explode(':', $old_datetime[1]);

                if ($getTime[0]>=17 || $getTime[0]<8) {
                  $data['Hold'] = '1';
                } else {
                  $data['Hold'] = '0';
                }

                if ($getTime[0]>=17) {
                  $newDatetime = new DateTime($new_date);
                  $newDatetime->modify('+1 day');
                  $data['UmurDatetime'] = $newDatetime->format('Y-m-d 08:00:01');
                  echo $newDatetime->format('Y-m-d 08:00:01');
                } else if ($getTime[0]<8) {
                  $data['UmurDatetime'] = date($new_date.' 08:00:02');

                } else {
                  $data['UmurDatetime'] = $new_datetime;
                }

                $data[$columns[$j]] = $new_datetime;

              } else {
                $data[$columns[$j]] = str_replace('&nbsp;', '', $td->nodeValue);
              }
            } else {
            $data[$columns[$j]] =  str_replace('&nbsp;', '', $td->nodeValue);
            }
            if($j==0) {
              $data['ID'] =  substr($td->nodeValue,2);
                echo $td->nodeValue."<br />";

            }
            if ($j==13) {
              $new_datetime = "";

              $old_datetime = explode(' ', $td->nodeValue);
              if (count($old_datetime)>0){
                $old_date = explode('-', @$old_datetime[0]);
                $new_date = @$old_date[2].'-'.@$old_date[1].'-'.@$old_date[0];
                $new_datetime = $new_date.' '.@$old_datetime[1];
                $data[$columns[$j]] = $new_datetime;
                // $data[$columns[$j]] = $td->nodeValue;
              }

            }
        }
        $result[] = $data;
      }
      //print_r($result);
      DB::table('data_nossa_1')->where('Owner_Group',$loker)->delete();
      if ($kalsel == 1){
        DB::table('roc_active')->truncate();
        DB::table('roc_active')->insert($roc);
        #DB::table('rock_excel')->truncate();
        #DB::table('rock_excel')->insert($rock);
      }
      print_r($roc);
      foreach (array_chunk($result,1000) as $t) {
        DB::table('data_nossa_1')->insertIgnore($t);
        DB::table('data_nossa_1_log')->insertIgnore($t);
      }
  }


  public function nonatero_gaul($periode){
    $filename = "upload3/nonatero/monthly_gaul_line_dtl_".$periode."_R6.txt";
    try
    {
      $content = File::get($filename);
      $grab = explode("\n", $content);
      $grab = str_replace('"', '', $grab);
      echo count($grab);
      foreach($grab as $key=>$line) {
        if ($key<>0){
          $array[$key] = explode('|', $line);
          if (array_key_exists(10,$array[$key]) && $array[$key][10]=="44") {
            echo "OK#";
            $data = $array[$key];
            $data_to_insert[] = [
              'THNBLN'	=>	$data[0],
              'THNBLNTGL_OPEN'	=>	$data[1],
              'JENIS_GGN'	=>	$data[2],
              'KATEGORI'	=>	$data[3],
              'KAT_GAUL'	=>	$data[4],
              'PLBLCL'	=>	$data[5],
              'KAT_PLG'	=>	$data[6],
              'STAT_INDI'	=>	$data[7],
              'ALPRO_1'	=>	$data[8],
              'REG'	=>	$data[9],
              'CWITEL'	=>	$data[10],
              'CDATEL'	=>	$data[11],
              'CMDF'	=>	$data[12],
              'MDF'	=>	$data[12],
              'ND_GROUP'	=>	$data[13],
              'ND_REFERENCE'	=>	$data[14],
              'TROUBLE_NUMBER'	=>	$data[15],
              'FLAG_GAUL'	=>	$data[16],
              'TROUBLE_NO'	=>	$data[17],
              'TROUBLE_OPENTIME'	=>	$data[18],
              'TROUBLE_CLOSETIME'	=>	$data[19],
              'SUBSEGMENTASI'	=>	$data[20],
              'TROUBLE_NO_B4'	=>	$data[21],
              'TROUBLE_OPENTIME_B4'	=>	$data[22],
              'TROUBLE_CLOSETIME_B4'	=>	$data[23],
              'SUBSEGMENTASI_B4'	=>	$data[24],
              'TGL_UPDATE' =>	$data[25],
              'REGIONAL' =>	$data[26],
              'WITEL'	=>	$data[27],
              'DATEL'	=>	$data[28]
            ];
          }
          else {
            echo "NOK#";
          }
        }
      }
      $records = array_chunk($data_to_insert, 500);
      DB::table('nonatero_detil_gaul')->where('THNBLN',$periode)->delete();
      foreach ($records as $batch) {
              print_r($batch);
              DB::table('nonatero_detil_gaul')
              ->insert($batch);
      }
      echo "Done !!!";
    } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
    {
        die("The file doesn't exist");
    }
  }

  public function nonatero($periode){
    $filename = "upload3/nonatero/daily_detail_".$periode."_R6.txt";
    try
    {
      $content = File::get($filename);
      $grab = explode("\n", $content);
      $grab = str_replace('"', '', $grab);
      foreach($grab as $key=>$line) {
        if ($key<>0){
               $array[$key] = explode('|', $line);
                if (array_key_exists(35,$array[$key]) && $array[$key][35]=="Telkom KALSEL") {
                    $data = $array[$key];
                    $data_to_insert[] = [
                      'THNBLN'	=>	$data[0],
                      'THNBLN_OPEN'	=>	$data[1],
                      'THNBLNTGL_OPEN'	=>	$data[2],
                      'JAM_OPEN'	=>	$data[3],
                      'THNBLN_CLOSE'	=>	$data[4],
                      'THNBLNTGL_CLOSE'	=>	$data[5],
                      'JAM_CLOSE'	=>	$data[6],
                      'IS_GAMAS'	=>	$data[7],
                      'TROUBLENO_PARENT'	=>	$data[8],
                      'UNIT'	=>	$data[9],
                      'PLBLCL'	=>	$data[10],
                      'FLAG_TEKNIS'	=>	$data[11],
                      'JENIS_GGN'	=>	$data[12],
                      'STAT_INDI'	=>	$data[13],
                      'P'	=>	$data[14],
                      'CPROD'	=>	$data[15],
                      'PRODUK'	=>	$data[16],
                      'TROUBLE_OPENTIME'	=>	$data[17],
                      'TROUBLE_CLOSETIME'	=>	$data[18],
                      'TGL_TECH_CLOSE'	=>	$data[19],
                      'DATE_CLOSE'	=>	$data[20],
                      'STATUS'	=>	$data[21],
                      'ALPRO'	=>	$data[22],
                      'CHANNEL_ID'	=>	$data[23],
                      'CHANNEL'	=>	$data[24],
                      'SEGMENTASI'	=>	$data[25],
                      'SUBSEGMENTASI'	=>	$data[26],
                      'ACTUAL_SOLUTION'	=>	$data[27],
                      'TROUBLE_NO'	=>	$data[28],
                      'NCLI'	=>	$data[29],
                      'NDOS'	=>	$data[30],
                      'TROUBLE_NUMBER'	=>	$data[31],
                      'ND_REFERENCE'	=>	$data[32],
                      'REG'	=>	$data[33],
                      'CWITEL'	=>	$data[34],
                      'WITEL'	=>	$data[35],
                      'CDATEL'	=>	$data[36],
                      'DATEL'	=>	$data[37],
                      'STO'	=>	$data[38],
                      'CMDF'	=>	$data[39],
                      'MDF'	=>	$data[40],
                      'RK'	=>	$data[41],
                      'DP'	=>	$data[42],
                      'LAST_TCLOSE_LOKER_ID'	=>	$data[43],
                      'RESULT_HOLDRESOLVED'	=>	$data[44],
                      'AUTO_ASSIGNED'	=>	$data[45],
                      'JML_LAPUL'	=>	$data[46],
                      'IS_REOPEN'	=>	$data[47],
                      'TTR'	=>	$data[48],
                      'TTR_CONS'	=>	$data[49],
                      'KAT_PLG'	=>	$data[50],
                      'ONT_MANUFACTURE'	=>	$data[51],
                      'IS_3HS'	=>	$data[52],
                      'IS_6HS'	=>	$data[53],
                      'IS_12HS'	=>	$data[54],
                      'IS_24HS'	=>	$data[55],
                      'IS_36HS' =>  $data[56],
                      'IS_CLOSE_3JAM'	=>	$data[57],
                      'SYMPTOM_LV0'	=>	$data[58],
                      'SOLUTION1'	=>	$data[59],
                      'SOLUTION0'	=>	$data[60],
                      'LOKASI_GGN'	=>	$data[61],
                      'KONDISI_JARINGAN'	=>	$data[62],
                      'DESC_CATEGORY_SYMPTOM'	=>	$data[63],
                      'TK_WORKZONE'	=>	$data[64],
                      'WORKZONE_NAME'	=>	$data[65],
                      'AMCREW'	=>	$data[66],
                      'HEADLINE_GAMAS_INDUK'	=>	$data[67],
                      'UPDATE_TIME'	=>	$data[68]
                    ];


                }

        }
      }
      $records = array_chunk($data_to_insert, 500);
      DB::table('nonatero_detil')->where('THNBLN',$periode)->delete();
      foreach ($records as $batch) {
              DB::table('nonatero_detil')
              ->insert($batch);
      }
      echo "Done !!!";
    } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception)
    {
        die("The file doesn't exist");
    }
  }

  public static function clean_up(){
    $get = DB::SELECT('SELECT * FROM psb_myir_wo WHERE (sc IS NOT NULL AND sc <> "0") AND ket = 0');
    foreach ($get as $result){
      $update = DB::table('psb_myir_wo')->where('myir',$result->myir)->update([
        "ket" => 1
      ]);
    }
  }

  public static function sync_starclick_to_myir_2($date){
    $check_sc = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a WHERE a.jenisPsb LIKE "AO%" AND a.myir = "'.$result->myir.'" ORDER BY a.orderDate DESC limit 1');
    if (count($check_sc)>0){
      $update = DB::table('psb_myir_wo')->where('myir',$result->myir)->update([
        "sc" => $check_sc[0]->orderId,
        "ket" => 1
      ]);
      $update = DB::table('dispatch_teknisi')->where('Ndem',$result->myir)->update([
        "Ndem" => $check_sc[0]->orderId
      ]);
      echo $result->myir.";DONE<br />";
    } else {
      echo $result->myir.";FAILED<br />";
    }
  }

  public function sync_starclick_to_myir($date){
    $check = DB::SELECT('SELECT * FROM psb_myir_wo a WHERE a.sc IS NULL AND a.`orderDate` LIKE "'.$date.'%" AND a.`lokerJns` IN ("myir","sales")');
    foreach ($check as $result){
      $check_sc = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a WHERE a.jenisPsb LIKE "AO%" AND a.myir = "'.$result->myir.'" ORDER BY a.orderDate DESC limit 1');
      if (count($check_sc)>0){
        // $update = DB::table('psb_myir_wo')->where('myir',$result->myir)->update([
        //   "sc" => $check_sc[0]->orderId,
        //   "ket" => 1
        // ]);
        // $update = DB::table('dispatch_teknisi')->where('Ndem',$result->myir)->update([
        //   "Ndem" => $check_sc[0]->orderId
        // ]);
        $this->add_sc($check_sc[0]->orderId,$result->myir);
        // echo $result->myir.";DONE<br />";
      } else {
        echo $result->myir.";FAILED<br />";
      }
    }
  }

  public function get_myir_from_kcontact($date){
    $get_sc = DB::SELECT('
      SELECT
      *
      FROM
      Data_Pelanggan_Starclick a
      WHERE
      a.orderDate LIKE "'.$date.'%"
    ');
    foreach ($get_sc as $result){
      $get_myir = explode(';',$result->kcontact);
      if (count($get_myir)>0){
        if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
          $get_myir_2 = explode('-',$get_myir[1]);
          $myir = $get_myir_2[1];
        } else {
          $myir = "\N";
        }
        } else {
          $myir = "\N";
      }
      $update = DB::table('Data_Pelanggan_Starclick')->where('orderId',$result->orderId)->update([
        "myir" => $myir
      ]);
      echo $result->orderId." - ".$myir."<br />";
    }
  }

	public static function check_sc_not_syncx(){
    // dimatikan untuk selamanya
		
		$check = DB::SELECT('
			SELECT
			*
			FROM psb_myir_wo a
			LEFT JOIN dispatch_teknisi b ON a.sc = b.Ndem
			LEFT JOIN Data_Pelanggan_Starclick c ON a.sc = c.orderId
			WHERE
			b.tgl LIKE "'.date('Y-m').'%" AND
			c.orderId IS NULL
		');
		// print_r($check);
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    $cookie = $get_cookie[0]->cookie;
		if (count($check)>0){
			foreach ($check as $result){
				GrabController::sync2($result->sc,$cookie);
				echo $result->sc."<br />";
			}
		}
	}

	public static function sync2($id,$cookie){
    $order_id = $id;
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Language: en-US,en;q=0.5';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
			if (count($list)){
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->SPEEDY,
              "ndemPots"=>@$data->POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "witel"=>@$data->WITEL);
        echo $data->ORDER_ID."<br />";
      }
      }
			}
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      return redirect('/customGrab')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
      ]);
    }
    curl_close($ch);
  }
  public function sync_X($sc){
    $order_id = $sc;
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Accept: */*';
    $headers[] = 'Sec-Fetch-Site: same-origin';
    $headers[] = 'Sec-Fetch-Mode: cors';
    $headers[] = 'Sec-Fetch-Dest: empty';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = 'Accept-Language: en-US,en;q=0.9,id;q=0.8';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->ND_INTERNET<>''){
                $int = explode('~', $data->ND_INTERNET);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->ND_INTERNET;
                }
            };
            if ($data->ND_POTS<>''){
                $telp = explode('~', $data->ND_POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->ND_POTS;
                }
            };
            echo "store ke array\n";
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }

            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->ND_INTERNET,
              "ndemPots"=>@$data->ND_POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "witel"=>@$data->WITEL,
              "agent_id"=>@$data->AGENT_ID
            );
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      return back()->with('alerts',[['type'=>'success', 'text'=>'SUCCESS SYNC SC !']]);
    }
    curl_close($ch);
  }

  public function fix_sto(){
    $query = DB::SELECT('select * from Data_Pelanggan_Starclick a WHERE a.sto = "" AND DATE(a.orderDate) BETWEEN "2020-06-01" AND "2020-06-31" ');
    foreach ($query as $result){
      $order_id = $result->orderId;
      $ch = curl_init();
      $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

      // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
      $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
      curl_setopt($ch, CURLOPT_URL, $link);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
      // echo urldecode($link);
      $headers = array();
      $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0';
      $headers[] = 'Accept: */*';
      $headers[] = 'Accept-Language: en-US,en;q=0.5';
      $headers[] = 'X-Requested-With: XMLHttpRequest';
      $headers[] = 'Connection: keep-alive';
      $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
      $headers[] = $get_cookie[0]->cookie;
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $result = curl_exec($ch);

      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      } else {
        $result = json_decode($result);
        $allRecord = array();
        if (count($result)){
        $list = $result->data;

        if (count($list)){
        $list = $list->LIST;
        foreach ($list as $data){
              //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
              $orderDate = "";
              $orderDatePs = "";
              if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
              if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
              if ($data->ND_INTERNET<>''){
                  $int = explode('~', $data->ND_INTERNET);
                  if (count($int)>1){
                      $internet = $int[1];
                  }else{
                      $internet = $data->ND_INTERNET;
                  }
              };
              if ($data->ND_POTS<>''){
                  $telp = explode('~', $data->ND_POTS);
                  if (count($telp)>1){
                      $noTelp = $telp[1];
                  }else{
                      $noTelp = $data->ND_POTS;
                  }
              };
              echo "store ke array\n";
              $get_myir = explode(';',$data->KCONTACT);
              if (count($get_myir)>0){
                if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                  $get_myir_2 = explode('-',$get_myir[1]);
                  $myir = $get_myir_2[1];
                } else {
                  $myir = "\N";
                }
                } else {
                  $myir = "\N";
              }

              //get STO
              $get_sto = explode('-',@$data->LOC_ID);
              echo @$data->LOC_ID;
              if (count($get_sto)>1){
                $STO = $get_sto[1];
              } else {
                $STO = "N";
              }

              $update  = DB::table('Data_Pelanggan_Starclick')->where('orderId',$order_id)->update([
                'sto' => $STO
              ]);
          echo $data->ORDER_ID."<br />";
        }
        }
      }
    }
  }

  }

  public function sync_by_sc($order_id){
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Accept: */*';
    $headers[] = 'Sec-Fetch-Site: same-origin';
    $headers[] = 'Sec-Fetch-Mode: cors';
    $headers[] = 'Sec-Fetch-Dest: empty';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = 'Accept-Language: en-US,en;q=0.9';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->ND_INTERNET<>''){
                $int = explode('~', $data->ND_INTERNET);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->ND_INTERNET;
                }
            };
            if ($data->ND_POTS<>''){
                $telp = explode('~', $data->ND_POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->ND_POTS;
                }
            };
            echo "store ke array\n";
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }

            //get STO
            $get_sto = explode('-',$data->LOC_ID);
            if (count($get_sto)>0){
              $STO = $get_sto[1];
            } else {
              $STO = "N";
            }

            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->ND_INTERNET,
              "ndemPots"=>@$data->ND_POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "witel"=>@$data->WITEL,
              "agent_id"=>@$data->AGENT_ID
            );
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }

    }
    curl_close($ch);
  }

  public function sync(Request $req){
    $order_id = $req->sc;
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    // curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22KALSEL%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22Fieldchannel%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=10&sort=%5B%7B%22property%22%3A%22SPEEDY%22%2C%22direction%22%3A%22ASC%22%7D%5D');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Encoding: gzip, deflate, br';
    $headers[] = 'Sec-Fetch-Site: same-origin';
    $headers[] = 'Sec-Fetch-Mode: cors';
    $headers[] = 'Sec-Fetch-Dest: empty';
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Date: '.date('D').', '.date('d M Y').' 03:50:28 GMT';
    $headers[] = 'Expires: Thu, 19 Nov 1981 08:52:00 GMT';
    $headers[] = 'Host: starclickncx.telkom.co.id';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = 'Accept-Language: en-US,en;q=0.9';
    $headers[] = 'Vary: User-Agent';
    $headers[] = 'Transfer-Encoding: chunked';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    print_r($result);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->ND_INTERNET<>''){
                $int = explode('~', $data->ND_INTERNET);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->ND_INTERNET;
                }
            };
            if ($data->ND_POTS<>''){
                $telp = explode('~', $data->ND_POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->ND_POTS;
                }
            };
            echo "store ke array\n";
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }

            //get STO
            $get_sto = explode('-',$data->LOC_ID);
            if (count($get_sto)>0){
              $STO = $get_sto[1];
            } else {
              $STO = "N";
            }

            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->ND_INTERNET,
              "ndemPots"=>@$data->ND_POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "witel"=>@$data->WITEL,
              "agent_id"=>@$data->AGENT_ID
            );
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      print_r($srcarr);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      if (count($srcarr)>0){
      return redirect('/customGrabOld')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> SYNC SC!']
      ]);
      } else {
      return redirect('/customGrabOld')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> SYNC SC!']
      ]);

      }
    }
    curl_close($ch);
  }

    public function sync_ax($order_id){
    $ch = curl_init();
    $link = 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data={"SearchText":"'.$order_id.'","Field":"ORDER_ID","Fieldstatus":null,"Fieldtransaksi":null,"StartDate":"","EndDate":"","start":null,"source":"NOSS","typeMenu":"TRACKING"}&&page=1&start=0&limit=10';

    // $link = "https://starclickncx.telkom.co.id/newsc/api/public/starclick/api/tracking-naf?_dc=1593610106271&ScNoss=true&guid=0&code=0&data=%7B%22SearchText%22%3A%22BALIKPAPAN%22%2C%22Field%22%3A%22ORG%22%2C%22Fieldstatus%22%3Anull%2C%22Fieldtransaksi%22%3Anull%2C%22StartDate%22%3A%22%22%2C%22EndDate%22%3A%22%22%2C%22start%22%3Anull%2C%22source%22%3A%22NOSS%22%2C%22typeMenu%22%3A%22TRACKING%22%7D&page=1&start=0&limit=100";
    $get_cookie = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->get();
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    // echo urldecode($link);
    $headers = array();
    $headers[] = 'Connection: keep-alive';
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';
    $headers[] = 'X-Requested-With: XMLHttpRequest';
    $headers[] = 'Accept: */*';
    $headers[] = 'Accept-Encoding: gzip, deflate, br';
    $headers[] = 'Sec-Fetch-Site: same-origin';
    $headers[] = 'Sec-Fetch-Mode: cors';
    $headers[] = 'Sec-Fetch-Dest: empty';
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Date: Fri, '.date('d M Y').' 03:50:28 GMT';
    $headers[] = 'Expires: Thu, 19 Nov 1981 08:52:00 GMT';
    $headers[] = 'Host: starclickncx.telkom.co.id';
    $headers[] = 'Referer: https://starclickncx.telkom.co.id/newsc/index.php';
    $headers[] = 'Accept-Language: en-US,en;q=0.9';
    $headers[] = 'Vary: User-Agent';
    $headers[] = 'Transfer-Encoding: chunked';
    $headers[] = $get_cookie[0]->cookie;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    } else {
      $result = json_decode($result);
      $allRecord = array();
      if (count($result)){
      $list = $result->data;

      if (count($list)){
      $list = $list->LIST;
      foreach ($list as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE_PS));
            if (!empty($data->ORDER_DATE)) $orderDate = date('Y-m-d H:i:s',strtotime(@$data->ORDER_DATE));
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $get_myir = explode(';',$data->KCONTACT);
            if (count($get_myir)>0){
              if ($get_myir[0]=="MYIR" || $get_myir[0]=="MI"){
                $get_myir_2 = explode('-',$get_myir[1]);
                $myir = $get_myir_2[1];
              } else {
                $myir = "\N";
              }
              } else {
                $myir = "\N";
            }

            $allRecord[] = array(
              "orderId"=>@$data->ORDER_ID,
              "orderName"=>str_replace("'","",@$data->CUSTOMER_NAME),
              "orderAddr"=>str_replace("'","",@$data->INS_ADDRESS),
              "orderNotel"=>@$data->POTS,
              "orderDate"=>$orderDate,
              "orderDatePs"=>$orderDatePs,
              "orderCity"=>str_replace("'","",@$data->CUSTOMER_ADDR),
              "orderStatus"=>@$data->STATUS_RESUME,
              "orderStatusId"=>@$data->STATUS_CODE_SC,
              "orderNcli"=>@$data->NCLI,
              "ndemSpeedy"=>@$data->SPEEDY,
              "ndemPots"=>@$data->POTS,
              "orderPaketID"=>@$data->ODP_ID,
              "kcontact"=>str_replace("'"," ",@$data->KCONTACT),
              "username"=>@$data->USERNAME,
              "alproName"=>@$data->LOC_ID,
              "tnNumber"=>@$data->POTS,
              "reservePort"=>@$data->ODP_ID,
              "jenisPsb"=>@$data->JENISPSB,
              "sto"=>@$data->STO,
              "lat"=>@$data->GPS_LATITUDE,
              "lon"=>@$data->GPS_LONGITUDE,
              "internet"=>@$internet,
              "noTelp"=>@$noTelp,
              "orderPaket"=>@$data->PACKAGE_NAME,
              "myir"=>$myir,
              "witel"=>@$data->WITEL);
        echo $data->ORDER_ID."<br />";
      }
      }
      }
      echo "saving\n";
      $srcarr=array_chunk($allRecord,500);
      foreach($srcarr as $item) {
          //DB::table('proaktif')->insert($item);
          self::insertOrUpdate($item);
          if (count($srcarr)>0){
            DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
              'last_sync' => date('Y-m-d H:i:s')
            ]);
          }
      }
      if (count($srcarr)>0){
        echo "success";
      } else {
        echo "failed";
      }
    }
    curl_close($ch);
  }

  public function syncx(Request $req)
  {
    //processing
     $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        )
    );

    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$req->sc."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
      $result = json_decode(@file_get_contents($link, false, stream_context_create($arrContextOptions)));
      echo $link."<br />";
      print_r(count($result->data));

      $order = $result->data;
      foreach ($order as $data){
        if ($data->ORDER_ID == "####"){
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'warning', 'text' => '<strong>gagal</strong> ORDER ID tidak boleh Angka!']
          ]);
        }

        $get_where = DB::table("Data_Pelanggan_Starclick")->where("orderId", $data->ORDER_ID)->first();

        $orderDate = "";
        $orderDatePs = "";
        if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
        if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
        // dd($get_where)
        // dd($data);

        $KCONTACT  = str_replace("\\", '', $data->KCONTACT);
        $KCONTACT  = str_replace("'", '', $KCONTACT);
        $orderName = str_replace("'",'',$data->CUSTOMER_NAME);
        // dd($KCONTACT);

        if (!empty($get_where->orderId)){
          echo $data->ORDER_ID." UPDATED\n";
          $SQL = "
          UPDATE
            Data_Pelanggan_Starclick
          SET
            orderId='".$data->ORDER_ID."',
            orderKontak='".$data->PHONE_NO."',
            orderDate='".$orderDate."',
            orderDatePs='".$orderDatePs."',
            orderStatus='".$data->STATUS_RESUME."',
            orderStatusId='".$data->STATUS_CODE_SC."',
            orderNcli='".$data->NCLI."',
            orderPaket='".$data->PACKAGE_NAME."',
            lat = '".$data->GPS_LATITUDE."',
            lon = '".$data->GPS_LONGITUDE."',
            orderAddr = '".$data->INS_ADDRESS."',
            orderCity = '".$data->CUSTOMER_ADDR."',
            orderNotel = '".$data->POTS."',
            ndemSpeedy = '".$data->SPEEDY."',
            ndemPots = '".$data->POTS."',
            orderPaketID = '".$data->ODP_ID."',
            kcontact = '".$KCONTACT."',
            username = '".$data->USERNAME."',
            alproname = '".$data->LOC_ID."',
            tnNumber = '".$data->TN_NUMBER."',
            reservePort = '".$data->ODP_ID."',
            jenisPsb = '".$data->JENISPSB."',
            sto = '".$data->XS2."',
            orderName = '".$orderName."',
            reserveTn = '".$data->RNUM."'
          WHERE
            orderId='".$data->ORDER_ID."'
          ";
          // echo $SQL."\n";
          $update = DB::statement($SQL);
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
          ]);
        } else {
          echo $data->ORDER_ID." INSERTED\n";
          $SQL = "
          INSERT IGNORE INTO
            Data_Pelanggan_Starclick
          (orderId,orderName,orderAddr,orderNotel,orderKontak,orderDate,orderDatePs,orderCity,orderStatus,orderStatusId,orderNcli,ndemSpeedy,ndemPots,orderPaketID,kcontact,username,alproName,tnNumber,reserveTn,reservePort,jenisPsb,sto,lat,lon)
          VALUES
          ('".$data->ORDER_ID."','".str_replace("'","",$data->CUSTOMER_NAME)."',
          '".str_replace("'","",$data->INS_ADDRESS)."','".$data->POTS."','".$data->PHONE_NO."','".$orderDate."','".$orderDatePs."','".str_replace("'","",$data->CUSTOMER_ADDR)."','".$data->STATUS_RESUME."',
          '".$data->STATUS_CODE_SC."','".$data->NCLI."','".$data->SPEEDY."','".$data->POTS."','".$data->ODP_ID."','".$KCONTACT."','".$data->USERNAME."','".$data->LOC_ID."','".$data->POTS."',
          '".$data->RNUM."','".$data->ODP_ID."','".$data->JENISPSB."','".$data->XS2."','".$data->GPS_LATITUDE."','".$data->GPS_LONGITUDE."')
          ";
          $insert = DB::statement($SQL);
          echo $SQL."\n";
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> menambah SC baru!']
          ]);
        }
      }

    //to view
    //return view('grab.index');
    return redirect('/customGrab')->with('alerts', [
      ['type' => 'warning', 'text' => '<strong>Gagal</strong> SC tidak ditemukan dalam kurun waktu 30 hari kebelakang!']
    ]);
  }
  public function a2s_absensi(){
    $this->a2s_absen(1);
    $this->a2s_absen(2);
  }

  public function autononwork($ps){
      
      $date = date('Y-m-d');
      $date2 = date('d/m/Y');

      // $check_ps = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.witel = "KALSEL" AND ((dpsa.orderStatusId in ("1500","7")  AND DATE(dpsa.orderDatePs) = "'.$date.'" ) OR (DATE(dpsa.orderDate) >="2020-03-01" AND dpsa.orderStatusId IN ("1203","1401"))) ');
      $check_hadir = DB::SELECT('SELECT * FROM a2s_kehadiran a WHERE a.div = "2" AND a.tgl = "'.$date.'"');
      $hadir = count($check_hadir);
      // $ps = count($check_ps);
      $auto_non_work = $hadir-round($ps/2);
      echo "PS : ".$ps;
      echo "<br />hadir : ".$hadir;
      echo "<br />auto nonwork : ".$auto_non_work;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/login.php');
      curl_setopt($ch, CURLOPT_POSTFIELDS,"username=915999&password=db7712c632445bc5981297ef2a8ca73f");
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);
      $c = curl_exec($ch);

      echo "<br />";
      $check_hadir = DB::SELECT('SELECT * FROM a2s_kehadiran a WHERE a.div = "2" AND a.tgl = "'.$date.'" limit '.$auto_non_work);

      foreach ($check_hadir as $num=>$result){
        ++$num;
        if ($num<=$auto_non_work){
        echo $num." ".$result->laborcode."<br />";
        curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_update_p_modavail.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"start_date=".$date2."&laborcode=".$result->laborcode."&reasoncode=NON-WORK&run=SUBMIT");
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $c = curl_exec($ch);
        print_r($c);
        }
      }


  }

  public function a2s_absen($id){
    
    $param = "end_date=".date("d")."%2F".date("m")."%2F".date("Y")."&p_regional_ta=%25&p_witel=44&alpro=".$id."&done=Done";
    $ch = curl_init();
    $url = 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date='.date("d")."%2F".date("m")."%2F".date("Y").'&alpro='.$id.'&lama=0';
    echo $url;
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"username=915999&password=db7712c632445bc5981297ef2a8ca73f");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $c = curl_exec($ch);
    curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $c = curl_exec($ch);
    echo $c;
    $columns = array(
            1 =>
            'reg',
            'witel',
            'laborcode',
            'teknisi',
            'nik',
            'supervisor',
            'jenis_teknisi',
            'no_hp',
            'workzone',
            'jadwal',
            'hadir',
            'tgl',
            'div'
        );
    $dom = @\DOMDocument::loadHTML(trim($c));
    $tgl = date('Y-m-d');
    $div = $id;
    $table = $dom->getElementsByTagName('table')->item(1);
    print_r($table);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
          if($j == 12){
            $data[$columns[$j]] = $tgl;
          }else if($j == 13){
            $data[$columns[$j]] = $div;
          }else{
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
        }

        $result[] = $data;
    }
    print_r($result);
    //return $result;
    DB::table('a2s_kehadiran')->where('tgl', $tgl)->where('div', $div)->delete();
    DB::table('a2s_kehadiran')->insert($result);
  }
  public static function odp(){
    ini_set('max_execution_time', 60000);
    //DB::table('1_0_master_odp')->truncate();
    GrabController::odp_regional(6);

  }
  public static function odp_regional($reg){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/odpe2e/".date('Ym')."04/odp_compliance_".date('Ym')."04_Regional 6.xlsx";
    //$handle = fopen($filename, "r");
    //$data_to_insert = array();
    $content = file_get_contents($filename);

    var_dump($content);
    dd();

    while(!feof($handle)){
      $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'         => $data[0],
              'ODP_INDEX'       => $data[1],
              'ODP_NAME'        => $data[2],
              'LATITUDE'        => $data[3],
              'LONGITUDE'       => $data[4],
              'CLUSNAME'        => $data[5],
              'CLUSTERSTATUS'   => $data[6],
              'AVAI'            => $data[7],
              'USED'            => $data[8],
              'RSV'             => $data[9],
              'RSK'             => $data[10],
              'IS_TOTAL'        => $data[11],
              'REGIONAL'        => $data[12],
              'WITEL'           => $data[13],
              'DATEL'           => $data[14],
              'STO'             => $data[15],
              'STO_DESC'        => $data[16],
              'ODP_INFO'        => $data[17],
              'UPDATE_DATE'     => $data[18]
            ];

       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional ".$reg;
    DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    foreach ($records as $batch) {
          try {
            DB::table('1_0_master_odp')
            ->insert($batch);
          }
          catch (\Exception $e) {

          }
    }

  }
  public function scDetail($id){
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$id."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
    $result = json_decode(@file_get_contents($link));
    return $order = $result->data;
  }

  public static function odpRegional(){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/ODPOkupansi/".date('Y')."/".date('Ym')."/REPORT_ODP_TREG6_".date('Ymd').".txt";
    $handle = fopen($filename, "r");
    $data_to_insert = array();
    while(!feof($handle)){
       $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'          => $data[0],
              'ODP_INDEX'        => $data[1],
              'ODP_NAME'         => $data[2],
              'LATITUDE'         => $data[3],
              'LONGITUDE'        => $data[4],
              'CLUSNAME'         => $data[5],
              'CLUSTERSTATUS'    => $data[6],
              'AVAI'             => $data[7],
              'USED'             => $data[8],
              'RSV'              => $data[9],
              'RSK'              => $data[10],
              'IS_TOTAL'         => $data[11],
              'REGIONAL'         => $data[12],
              'WITEL'            => $data[13],
              'DATEL'            => $data[14],
              'STO'              => $data[15],
              'STO_DESC'         => $data[16],
              'ODP_INFO'         => $data[17],
              'UPDATE_DATE'      => $data[18]
            ];
       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional 6";
    // DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    DB::table('1_0_master_odp')->truncate();
    foreach ($records as $batch) {
      try {
              DB::table('1_0_master_odp')
               ->insert($batch);
          }
             catch (\Exception $e) {

          }
    }
    // DB::table('1_0_master_odp')->where('REGIONAL', 'REGIONAL')->delete();
  }

  public function grab_kpro_val(){
    // perform header
    
    ini_set('max_execution_time', 60000);
    $ch = curl_init();

    $headers[] = "Accept: */*";
    $headers[] = "Connection: Keep-Alive";
    $headers[] = "Content-type: application/x-www-form-urlencoded;charset=UTF-8";
    curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
    curl_setopt($ch, CURLOPT_HEADER,  0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where status_resume = 'Process OSS (Provision Issued)' and (tindak_lanjut like 'Management Janji' or tindak_lanjut is null)  and  regional='6' and  1=1  and  1=1  and  type_trans='NEW SALES' and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "val";
        $data['jam'] = $jam;
        $result[] = $data;
    }

    DB::table("kpro_pi")->insert($result);
  }

  public function grab_kpro_tot(){
    // perform header
    
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
            from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id
            LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where ((status_resume not like 'Completed (PS)' and status_resume not like 'UN%' and lower(status_resume) not like ('%cancel%') and lower(status_resume) not like ('%revoke%')) or (status_resume like 'Completed (PS)' and TO_CHAR(last_updated_date,'yyyy-mm-dd') = TO_CHAR(CURRENT_DATE,'yyyy-mm-dd')))  and  regional='6' and  witel='BANJARMASIN' and  1=1  and  1=1  and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);

    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "tot";
        $data['jam'] = $jam;
        $result[] = $data;
    }

    DB::table("kpro_pi")->insert($result);
  }

  public function grapA2s(){
    ini_set('max_execution_time', 60000);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    // curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $hasil=curl_exec($ch);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/index.php?treg=6&witel=all&psb=NEW%20SALES&play=23p&hari=all&tl=all&mode=sto');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $context=curl_exec($ch);
    // curl_close($ch);

    // $dom = @\DOMDocument::loadHTML(trim($context));
    // $columns = array("witel","unsc","re","not_va","va","prov_awl","prov_mj","prov_as_ex","prov_as_hi","prov_as_hplus","prov_val","prov_nok","prov_tot","acomp_ok","acomp_nok","prv_com","rw_os","comp_hi","comp_est","comp_dev","comp_h_1","fall_wfm","fall_uim","fall_asp","fall_osm","fall_nok","fall_tot","total_order");

    // dd($dom);
    // // dd($dom);
    // $table = $dom->getElementsByTagName('table')->item(0);
    // $rows = $table->getElementsByTagName('tr');
    // $result = array();

    // for ($i = 1, $count = $rows->length; $i < $count; $i++)
    // {
    //     $cells = $rows->item($i)->getElementsByTagName('td');
    //     $data = array();
    //     for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
    //     {
    //         $td = $cells->item($j);
    //         $data[$columns[$j]] =  $td->nodeValue;
    //     }
    //     $result[] = $data;
    // };

    // dd($result);

  /////////////////////////////////////////////////////

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


    // assurance
    // curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=1&lama=3');

    // provisioning
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=2&lama=3');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","regional","witel","laborcode","teknisi","nik","supervisor","jenis_teknisi","no_hp","workzone","jadwal","hadir");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };
    // dd($result);
    if (empty($result)){
      $text = 'Semua Teknisi Sudah Absen';
    }
    else{
        $text ='';
        foreach($result as $data){
          $text .= 'laborcode : '.$data['laborcode']."<br>".'NIK : '.$data['nik']."<br>".'Teknisi : '.$data['teknisi']."<br><br>";
        };
    }

    echo $text;
  }

  public function a2sGgnOpen()
  {
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/assurance/rta_detil_ggn_open_idh.php?p_witel=44&lama=121');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","ticketid", "nd", "jns_alpro", "lama_ggn", "witel", "workzone", "tgl_beckend", "trouble_headline", "laborcode", "schedule_date", "nama_plg", "alamat", "jns_ggn", "emosi", "kcontack", "datek");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into a2s_detail_ggn_open_bank (no,ticketid, nd, jns_alpro, lama_ggn, witel, workzone, tgl_beckend, trouble_headline, laborcode, schedule_date, nama_plg, alamat, jns_ggn, emosi, kcontack, datek) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $sql .= $sparator."('".$result[$a]["no"]."','".$result[$a]["ticketid"]."','".$result[$a]["nd"]."','".$result[$a]["jns_alpro"]."','".$result[$a]["lama_ggn"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["tgl_beckend"]."','".$result[$a]["trouble_headline"]."','".$result[$a]["laborcode"]."','".$result[$a]["schedule_date"]."','".$result[$a]["nama_plg"]."','".$result[$a]["alamat"]."','".$result[$a]["jns_ggn"]."','".$result[$a]["emosi"]."','".$result[$a]["kcontack"]."','".$result[$a]["datek"]."'),";
    }

    $sql = substr($sql, 0, -1);

    DB::table('a2s_detail_ggn_open')->truncate();
    DB::table("a2s_detail_ggn_open")->insert($result);
    DB::statement($sql);
  }

  public static function grabAlista($dt){
    if($dt == 'today'){
      $dt = date('Y-m-d');
    }
    elseif($dt == 'thismonth'){
      $dt = date('Y-m');
    };

    $gudang = array(
        ["id_gudang" => "GD0245", "nama_gudang" => "WH SO INV BANJARMASIN 2 (A.YANI)"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO INV BANJARMASIN 1 (BJM CENTRUM)"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO INV BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO INV BARABAI"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO INV TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO INV BANJARBARU"]
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
    $result = curl_exec($ch);

    echo"logged in \n";
    foreach($gudang as $g){
        $result = array();
        $no = 1;
        do {
          echo"\nke halaman ".$no." gudang ".$g['nama_gudang']." \n";
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D='.str_replace(" ", "+", $g["nama_gudang"]).'&Permintaanpengambilanbarang%5Btgl_permintaan%5D='.$dt.'&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D=&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page='.$no);
          $als = curl_exec($ch);
          // dd($als);
          if(!strpos($als, 'No results found.')){
            $columns = array(
              'alista_id',
              'project',
              'nama_gudang',
              'tgl',
              'requester',
              'pengambil',
              'no_rfc',
              'nik_pemakai',
              'mitra',
              'id_permintaan'
            );
            $cals = str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
            $als = str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
            // dd($als);
            $dom = @\DOMDocument::loadHTML(trim($als));

            echo"convert string to variable ".$g['nama_gudang']." \n".strpos($als, "Last");
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            for ($i = 2, $count = $rows->length; $i < $count; $i++)
            {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
              {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }
            $no++;
          }
        }while(strpos($als, '<li class="last">'));
        // dd($result);
        $material = array();
        foreach($result as $noooooo => $r){
          echo "\nambil data ".$noooooo;
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);
          $mtr = curl_exec($ch);
          $columnsx= array(2=>"id_barang", "nama_barang", "jumlah");
          $dom = @\DOMDocument::loadHTML(trim($mtr));
          $table = $dom->getElementsByTagName('table')->item(1);
          $rows = $table->getElementsByTagName('tr');

          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            for ($j = 2, $jcount = 4; $j <= $jcount; $j++)
            {
              $td = $cells->item($j);
              if($j==4){
                $data[$columnsx[$j]] =  explode(' ', $td->nodeValue)[0];
              }else{
                $data[$columnsx[$j]] =  $td->nodeValue;
              }
            }
            $material[] = array_merge($data,$r);
            // dd($material);
          }
        }
        DB::table('alista_material_keluar')->where('nama_gudang', $g["nama_gudang"])->where('tgl', 'LIKE', '%'.$dt.'%')->delete();
        DB::table('alista_material_keluar')->insert($material);
    }

    // add
    $data = DB::table('alista_material_keluar')->where('tgl',$dt)->get();
    foreach ($data as $d){
        $idItemBantu = $d->id_barang.'_'.$d->no_rfc;
        DB::table('alista_material_keluar')
            ->where('alista_id',$d->alista_id)
            ->where('id_barang',$d->id_barang)
            ->update([
                'id_item_bantu' => $idItemBantu
            ]);
    }
  }

  public function grabAlista2()
  {
      $date = date('Y-m-d');
      ini_set('max_execution_time', 60000);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      $data = DB::table('akun')->where('user', '850056')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=18950795&LoginForm%5Bpassword%5D=@telkom1234&yt0=");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $rough_content = curl_exec($ch);
      // var_dump($rough_content);
      // dd($rough_content);

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      // // $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      // // preg_match_all ($pattern, $header_content, $matches);
      // $cookiesOut = $matches['cookie'][1] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      // $header['cookies'] = $cookiesOut;
      $cookie = null;
      $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"]);
      $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';
      foreach($gudang as $asd => $g){
        curl_setopt($ch, CURLOPT_URL, 'http://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$date."&Gudang%5Benddate%5D=".$date."&yt0=");
        // curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        $result = curl_exec($ch);
        // var_dump($result);
        // dd($result);

        $dom = @\DOMDocument::loadHTML($result);
        $table = $dom->getElementsByTagName('table')->item(1);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                0 =>'no',
                'id',
                'tgl',
                'nama_gudang',
                'project',
                'pengambil',
                'mitra',
                'id_barang',
                'nama_barang',
                'jumlah',
                'no_rfc'
            );
        $result = array();
        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
            {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
            }
            $result[] = $data;
        }
        $data = $result;
        $count = count($data);
        if($count){
          for ($i = 0; $i < $count; $i++) {
            $sparator = ", ";
            // if($i==0 && $asd == 0){
            if($i==0){
              $sparator = "";
            }

            $proaktif_id = explode(" ",$data[$i]["project"])[0];
            $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
              ,"'.$data[$i]['project'].'"
              ,"'.$data[$i]['pengambil'].'"
              ,"'.$data[$i]['mitra'].'"
              ,"'.$data[$i]['id_barang'].'"
              ,"'.$data[$i]['nama_barang'].'"
              ,"'.$data[$i]['jumlah'].'"
              ,"'.$data[$i]['no_rfc'].'"
              ,"'.$proaktif_id.'")';
          }
        }
      }
      echo $sql;
      // die;

      DB::table("alista_material_keluar")
      ->where("tgl", $date)
      ->delete();

      DB::statement($sql);

      // DB::connection('t1')->table("alista_material_keluar")
      // ->where("tgl", $date)
      // ->delete();

      // DB::connection('t1')->statement($sql);
  }

  public static function grabAlistaDate($start)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('user', '850056')->first();
    $username = $akun->user;
    $password = $akun->pwd;

    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    DB::table("alista_material_keluar")
    ->where("tgl", $tgl)
    ->delete();
    foreach($gudang as $asd => $g){
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(1);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'no',
              'id',
              'tgl',
              'nama_gudang',
              'project',
              'pengambil',
              'mitra',
              'id_barang',
              'nama_barang',
              'jumlah',
              'no_rfc'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;
      $count = count($data);
      if($count){
        $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

        for ($i = 0; $i < $count; $i++) {
          $sparator = ", ";
          if($i==0){
            $sparator = "";
          }
          $proaktif_id = explode(" ",$data[$i]["project"])[0];
          $pengambil = explode('#',$data[$i]["pengambil"]);
          if (count($pengambil)<2){
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = '';
          }
          else{
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = trim($pengambil[1]);
          };
          $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
            ,"'.$data[$i]['project'].'"
            ,"'.$pengambil_nik.'"
            ,"'.$pengambil_nama.'"
            ,"'.$data[$i]['mitra'].'"
            ,"'.$data[$i]['id_barang'].'"
            ,"'.$data[$i]['nama_barang'].'"
            ,"'.$data[$i]['jumlah'].'"
            ,"'.$data[$i]['no_rfc'].'"
            ,"'.$proaktif_id.'")';
        }
        echo $sql;
        DB::statement($sql);
      }
    }
  }

  public static function grabAlistaKeluar()
  {
    $start = date('Y/m/d');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('user', '850056')->first();
    $username = $akun->user;
    $password = $akun->pwd;

    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    DB::table("alista_material_keluar")
    ->where("tgl", $tgl)
    ->delete();
    foreach($gudang as $asd => $g){
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      dd($result);
      $table = $dom->getElementsByTagName('table')->item(1);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'no',
              'id',
              'tgl',
              'nama_gudang',
              'project',
              'pengambil',
              'mitra',
              'id_barang',
              'nama_barang',
              'jumlah',
              'no_rfc'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;
      $count = count($data);
      if($count){
        $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

        for ($i = 0; $i < $count; $i++) {
          $sparator = ", ";
          if($i==0){
            $sparator = "";
          }
          $proaktif_id = explode(" ",$data[$i]["project"])[0];

          $pengambil = explode('#',$data[$i]["pengambil"]);
          if (count($pengambil)<2){
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = '';
          }
          else{
              $pengambil_nik  = $pengambil[0];
              $pengambil_nama = trim($pengambil[1]);
          };

          $sql .= $sparator.'("'.$data[$i]['id'].'","'.$data[$i]['tgl'].'","'.$data[$i]['nama_gudang'].'"
            ,"'.$data[$i]['project'].'"
            ,"'.$pengambil_nik.'"
            ,"'.$pengambil_nama.'"
            ,"'.$data[$i]['mitra'].'"
            ,"'.$data[$i]['id_barang'].'"
            ,"'.$data[$i]['nama_barang'].'"
            ,"'.$data[$i]['jumlah'].'"
            ,"'.$data[$i]['no_rfc'].'"
            ,"'.$proaktif_id.'")';
        }
        echo $sql;
        DB::statement($sql);
      }
    }
  }
  public static function grabAlistaKembalian()
  {

    $start = date('Y/m/d');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $akun = DB::table('akun')->where('id',8)->first();
    $username = $akun->user;
    $password = $akun->pwd;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$username."&LoginForm%5Bpassword%5D=".$password."&yt0=");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    var_dump($rough_content);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookie = null;
    $gudang = array(["id_gudang" => "GD0245", "nama_gudang" => "WH SO BANJARMASIN 2"],
      ["id_gudang" => "GD0244", "nama_gudang" => "WH SO BANJARMASIN 1"],
      ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
      ["id_gudang" => "GD0246", "nama_gudang" => "WH SO BATULICIN"],
      ["id_gudang" => "GD0247", "nama_gudang" => "WH SO KANDANGAN"],
      ["id_gudang" => "GD0248", "nama_gudang" => "WH SO TANJUNG TABALONG"],
      ["id_gudang" => "GD0243", "nama_gudang" => "WH SO BANJARBARU"],
      ["id_gudang" => "GD175", "nama_gudang" => "WH SO INV BARABAI"]);

    //<option value="GD175">WH SO INV BARABAI</option>
    $tgl = str_replace('/', '-', $start);
    // DB::table("alista_material_keluar")
    // ->where("tgl", $tgl)
    // ->delete();
    DB::statement('delete from alista_material_kembali where Tgl_Pengembalian = "'.$tgl.'"');
    foreach($gudang as $asd => $g){
      echo "get data gudang ".$g['nama_gudang']."\n";
      //$end = date("Y-m-t", strtotime($a_date));
      //curl_setopt($ch, CURLOPT_URL, "https://alista.telkomakses.co.id/index.php?r=gudang/downloadreportingtransaksi&idgudang=".$g['id_gudang']."&tglawal=".$a_date."&tglakhir=".$end."&id=3");
      curl_setopt($ch, CURLOPT_URL, "https://alista.telkomakses.co.id/index.php?r=gudang/downloadreportingtransaksi&idgudang=".$g['id_gudang']."&tglawal=".$tgl."&tglakhir=".$tgl."&id=3");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=".$g['id_gudang']."&Gudang%5Bstartdate%5D=".$start."&Gudang%5Benddate%5D=".$start."&yt0=");
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              1 =>'ID_Pengembalian',
              'Tgl_Pengembalian',
              'Nama_Gudang',
              'Project',
              'Petugas_Gudang',
              'ID_Barang',
              'Nama_Barang',
              'jumlah',
              'Satuan',
              'NO_RFC'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      $data = $result;

      $count = count($data);

      if($count){
        //dd($data);
        echo "saving\n";
        $srcarr=array_chunk($data,500);
        foreach($srcarr as $no => $insert) {
          echo "saving ".++$no."\n";
          self::insertOrUpdateWithTable($insert, 'alista_material_kembali');
        }

      }
    }
  }

  public function detailGangguanClose(){
    
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    $link = 'https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3_gaul.php?loker_group=1&jns_channel=selected&jenis_ggn=1&workzone=&lama=5&p_witel=44&start_date='.$awal.'&end_date='.$sekarang.'&alpro=%25';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "is_gaul", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, is_gaul, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["is_gaul"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
  }

  public static function grabAlistaStokNasionalBanjarmasinArea(){
    // $url = "https://alista.telkomakses.co.id/index.php?Stokgudangta%5Bnama_gudang%5D=Banjarmasin+-+Area&Stokgudangta%5Bnama_witel%5D=&Stokgudangta%5Bregional%5D=&Stokgudangta%5Bid_barang%5D=&Stokgudangta%5Bnama_barang%5D=&Stokgudangta%5Bnama_satuan%5D=&ajax=yw1&r=gudang%2Finfostoknasional&stokgudang_page=";

    echo"logging in\n";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $data = DB::table('akun')->where('user', '850056')->first();
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
    $result = curl_exec($ch);
    echo"logged in\n";

    echo"downloading\n";
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/downloadinfostoknasional');
    curl_setopt($ch, CURLOPT_POST, false);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            1 =>'nama_gudang',
            'regional',
            'id_barang',
            'nama_barang',
            'kategori',
            'total_stok',
            'nama_satuan'
        );
    $result = array();
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        if($data['nama_gudang']=='Banjarmasin - Area'){
          $result[] = $data;
        }
    }

    echo"downloaded\n";
    //dd($result);

    DB::table('alista_stok')->truncate();
    DB::table('alista_stok')->insert($result);
    $alista = DB::table('alista_stok')->orderBy('id_barang', 'asc')->get();
    $msg = "Stok Gudang WH Banjarmasin-Area\n================================\n";
    foreach($alista as $da){
      $msg .= "<b>".$da->id_barang."</b> VOL : ".$da->total_stok."\n";
    }
    Telegram::sendMessage([
      'chat_id' => '-125769259',
      'text' => $msg,
      'parse_mode' => 'html'
    ]);
    // $dataarray = array();
    // for($h=1;$h<=17;$h++){
    //   curl_setopt($ch, CURLOPT_URL, $url.''.$h);
    //   curl_setopt($ch, CURLOPT_POST, false);
    //   $result = curl_exec($ch);

    //   $dom = @\DOMDocument::loadHTML($result);
    //     $table = $dom->getElementsByTagName('table')->item(0);
    //     $rows = $table->getElementsByTagName('tr');
    //     $columns = array(
    //             0 =>'nama_gudang',
    //             'nama_witel',
    //             'regional',
    //             'id_barang',
    //             'nama_barang',
    //             'total_stok',
    //             'nama_satuan'
    //         );
    //     $result = array();
    //     for ($i = 2, $count = $rows->length; $i < $count; $i++)
    //     {
    //         $cells = $rows->item($i)->getElementsByTagName('td');
    //         $data = array();
    //         for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
    //         {
    //             $td = $cells->item($j);
    //             $data[$columns[$j]] =  $td->nodeValue;
    //         }
    //         $result[] = $data;
    //         $dataarray[] = $data;
    //     }
    //     var_dump($result);
    //   DB::table('alista_stok')->insert($result);
    // }
    //asort($dataarray);
    // $msg = "Stok Gudang WH Banjarmasin-Area\n================================\n";
    // foreach($dataarray as $da){
    //   $msg .= "<code>".$da['id_barang']."</code> VOL : ".$da['total_stok']."\n";
    // }
    // Telegram::sendMessage([
    //   'chat_id' => '-125769259',
    //   'text' => $msg,
    //   'parse_mode' => 'html'
    // ]);
  }

  public function detailGangguanClose3on3(){
    
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=&jenis_ggn=&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%";

    echo $link;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "ttr3_jam", "ttr12_jam", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close_3on3 (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, ttr3_jam, ttr12_jam, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["ttr3_jam"]."','".$result[$a]["ttr12_jam"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close_3on3")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
  }

  public static function starclickMin(){
        $startSync=30;
        $allRecord =[];
        for($i=1;$i<=5;$i++){
            $end = $startSync-7;
            echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
            echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
            if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
                $enddate = "31/12/2017";
            } else {
                $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
            }
            //ALL STO except GMB ULI

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalsel&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
            $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
            echo $link."\n";

            // print_r(count($result->data));
            $order = $result->data;
            foreach ($order as $data){
                //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
                $orderDate = "";
                $orderDatePs = "";
                if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
                if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
                if ($data->SPEEDY<>''){
                    $int = explode('~', $data->SPEEDY);
                    if (count($int)>1){
                        $internet = $int[1];
                    }else{
                        $internet = $data->SPEEDY;
                    }
                };
                if ($data->POTS<>''){
                    $telp = explode('~', $data->POTS);
                    if (count($telp)>1){
                        $noTelp = $telp[1];
                    }else{
                        $noTelp = $data->POTS;
                    }
                };
                echo "store ke array\n";
                $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalsel");
            }
            $startSync=$end-1;
        }
        echo "saving\n";
        $srcarr=array_chunk($allRecord,500);
        foreach($srcarr as $item) {
            //DB::table('proaktif')->insert($item);
            self::insertOrUpdate($item);
        }
    }

    public static function idosier()
    {
      ini_set('max_execution_time', 60000);
      $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Bulanan/IDossierDatek/".date('Y-')."03/Data_I_Dossier_Datek_".date('Y-')."03_44_KALSEL.zip";
      file_put_contents("Tmpfile.zip", file_get_contents($filename));
      $zip = new \ZipArchive;
      $res = $zip->open('Tmpfile.zip');
      if ($res === TRUE) {
        $zip->extractTo(public_path().'/');
        $zip->close();
        echo "selesai extract \n";
      } else {
        echo 'doh!';
      }
      echo "reformat data \n";
      $content = file_get_contents(public_path()."/Data_I_Dossier_Datek_".date('Y-')."03_44_KALSEL.txt");

      $data_to_insert = array();
      $columns = array();
      foreach(explode("\r\n", $content) as $no => $line){
        $data = explode("|", $line);
        if($no){
          if (count($data)==81){
            foreach($data as $n => $d){
              $perform[$columns[$n]] = $d;
            }
            $data_to_insert[] = $perform;
          }
        }else{
          $columns = $data;
        }
      }
      DB::table('I_DOSSIER')->truncate();
      foreach (array_chunk($data_to_insert,500) as $tno => $t) {
        echo "insert batch data ke ".$tno."\n";
        DB::table('I_DOSSIER')->insert($t);
      }
      // dd($data_to_insert);
    }

    public static function new_insertOrUpdate(array $rows,$table){
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function insertOrUpdate(array $rows){
        $table = 'Data_Pelanggan_Starclick';
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function insertOrUpdate_backend(array $rows){
      $table = 'Data_Pelanggan_Starclick_Backend';
      $first = reset($rows);
      $columns = implode( ',',
          array_map( function( $value ) { return "$value"; } , array_keys($first) )
      );
      $values = implode( ',', array_map( function( $row ) {
              return '('.implode( ',',
                  array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
              ).')';
          } , $rows )
      );
      $updates = implode( ',',
          array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
      );
      $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
      return \DB::statement( $sql );
  }

    public static function insertOrUpdateUnsc(array $rows){
        $table = 'Data_Pelanggan_UNSC';
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function insertOrUpdateWithTable(array $rows, $table){
        $first = reset($rows);
        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );
        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                    array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                ).')';
            } , $rows )
        );
        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }

    public static function grabAlistaByRfc($rfc){
      ini_set('max_execution_time', 60000);
      // $noRfc   = str_replace('-', '/', $rfc);
      // $gudangs = str_replace(' ', '%20', $gudang);
      $noRfc   = $rfc;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
      $data = DB::table('akun')->where('user', '850056')->first();
      curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
      //curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=18950795&LoginForm%5Bpassword%5D=@telkom1234&yt0=");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HEADER, true);

      $rough_content = curl_exec($ch);
      // var_dump($rough_content);
      // dd($rough_content);

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      // $header['cookies'] = $cookiesOut;
      $cookie = null;

      // alista id
      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D='.$noRfc.'&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1&r=gudang%2Fhistorypengeluaranproject');

      $result = curl_exec($ch);

      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr')->item(2)->getElementsByTagName('td');
      $idAlista = $rows->item(0)->nodeValue;

      //

      $err = curl_errno($ch);
      $errmsg = curl_error($ch);
      $header = curl_getinfo($ch);

      $header_content = substr($rough_content, 0, $header['header_size']);
      $body_content = trim(str_replace($header_content, '', $rough_content));
      $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
      preg_match_all ($pattern, $header_content, $matches);
      $cookiesOut = $matches['cookie'][0] ;
      $header['errno'] = $err;
      $header['errmsg'] = $errmsg;
      $header['headers'] = $header_content;
      $cookie = null;

      $sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

      curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/downloadpengeluaranmaterial&a=&b=&c=&d=&e=&f='.$noRfc.'&g=&h=');
      curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
      $result = curl_exec($ch);
      // var_dump($result);
      // dd($result);

      $dom = @\DOMDocument::loadHTML($result);
      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $columns = array(
              0 =>'alista_id',
              'tgl',
              'ket',
              'project',
              'no_rfc',
              'id_gudang',
              'nama_gudang',
              'regional',
              'id_barang',
              'nama_barang',
              'jumlah',
              'harga',
              'harga_total',
              'requester',
              'pengambilMaterial',
              'nik_pemakai',
              'mitra'
          );
      $result = array();
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $data['alista_id'] = $idAlista;
          $result[] = $data;
      }
      $data = $result;

      if (count($data)<>0){
          DB::table("alista_material_keluar")->where("no_rfc", $noRfc)->delete();
          foreach($data as $datax){
              $dataPengambil = explode('#', $datax['pengambilMaterial']);
              if (count($dataPengambil)>1){
                  $nikPengambil  = $dataPengambil[0];
                  $namaPengambil = $dataPengambil[1];
              }
              else{
                  $nikPengambil  = $dataPengambil[0];
                  $namaPengambil = '';
              };

              $idItemBanntu = $datax['id_barang'].'_'.$datax['no_rfc'];
              $dataNikPengambil = array("pengambil" => $nikPengambil, "pengambil_nama"  => $namaPengambil, "id_item_bantu" => $idItemBanntu);

              $datax = array_merge_recursive($datax, $dataNikPengambil);
              unset($datax['ket'], $datax['regional'], $datax['harga'], $datax['harga_total'], $datax['id_gudang'], $datax['pengambilMaterial']);

              DB::table("alista_material_keluar")->insert($datax);
          }
        echo "sukses";
      }
      else{
        echo 'gagal';
      }
  }

  public static function baDigital()
  {
    
    $date = date('Y-m-d',strtotime("-15 days"));
    $datex = date('Y-m-d');

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Syncron BA Online $date $datex"
    ]);

    // ini_set('max_execution_time', 60000);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/dashboard_amalia/index.php?r=report/showWitel&excel=1&witel=75&tgl_mulai='.$date.'&tgl_selesai='.$datex.'&tipe_teknisi=all_teknisi');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $result = curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML($result);
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'sto',
            'regional',
            'fiberzone',
            'witel',
            'tgl',
            'mitra',
            'pelanggan',
            'no_wo',
            'noTelp_pelanggan1',
            'noTelp_pelanggan2',
            'no_inet',
            'layanan',
            'nikTeknisi'
        );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        // echo "data ".$i." save \n";
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['no_wo_int'] = str_replace(array('SC.','SC',' ','Sc','sc','.','SX','1-'),'',$data['no_wo']);
        $data['tglPs'] = date("Y-m-d", strtotime($data['tgl']));
        $data['grab_at'] = date('Y-m-d H:i:s');
        $result[] = $data;
    }

    DB::table('ba_online')->whereBetween('tglPs',[$date,$datex])->delete();

    $srcarr = array_chunk($result,500);
    foreach($srcarr as $item)
    {
      DB::table('ba_online')->insert($item);
    }

    DB::table('ba_online')->where('no_wo_int',0)->delete();

    DB::statement('DELETE a1 FROM ba_online a1, ba_online a2 WHERE a1.id > a2.id AND a1.no_wo_int = a2.no_wo_int');

    print_r("Finish Syncron BA Digital $date $datex \n");

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Finish Syncron BA Online $date $datex"
    ]);
  }

  public function dnsCheklistTl(){

  }

   public function detailGangguanClose3on3Fisik(){
    
    $sekarang = date('d/m/Y');
    $awal     = date('d/m/Y', mktime(0, 0, 0, date("m"),1, date("Y")));

    ini_set('max_execution_time', 60000);
    // $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=&jenis_ggn=&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%";
    $link = "https://apps.telkomakses.co.id/assurance/rta_detil_ggn_close_3on3.php?loker_group=1&jns_channel=selected&jenis_ggn=1&workzone=&lama=4&p_witel=44&start_date=".$awal."&end_date=".$sekarang."&alpro=%25";
    echo $link;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $link);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch);
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "ticket_id", "nd", "tgl_open", "status_manja", "tgl_manja", "tgl_close", "lama_ggn_jam", "ttr3_jam", "ttr12_jam", "jenis_gangguan", "regional_ta", "witel", "workzone", "crew", "nama_teknisi", "trouble_headline", "layanan_ggn", "channel", "alpro", "datek", "actual_solution");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into detail_gangguan_close_3on3_fisik (ticket_id, nd, tgl_open, status_manja, tgl_manja, tgl_close, lama_ggn_jam, ttr3_jam, ttr12_jam, jenis_gangguan, regional_ta, witel, workzone, crew, nama_teknisi, trouble_headline, layanan_ggn, channel, alpro, datek, actual_solution) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";

    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $namaTeknisi      = str_replace("'", '', $result[$a]['nama_teknisi']);
        $troubleHeadline  = str_replace("'", '', $result[$a]['trouble_headline']);

        $sql .= $sparator."('".$result[$a]["ticket_id"]."','".$result[$a]["nd"]."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_open']))."','".$result[$a]["status_manja"]."','".$result[$a]['tgl_manja']."','".date('Y-m-d H:i:s',strtotime($result[$a]['tgl_close']))."','".$result[$a]["lama_ggn_jam"]."','".$result[$a]["ttr3_jam"]."','".$result[$a]["ttr12_jam"]."','".$result[$a]["jenis_gangguan"]."','".$result[$a]["regional_ta"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["crew"]."','".$namaTeknisi."','".$troubleHeadline."','".$result[$a]["layanan_ggn"]."','".$result[$a]["channel"]."','".$result[$a]["alpro"]."','".$result[$a]["datek"]."','".$result[$a]["actual_solution"]."'),";
    }

    $sql = substr($sql, 0, -1);
    // dd($sql);

    DB::table("detail_gangguan_close_3on3_fisik")->truncate();
    // DB::table("detail_gangguan_close")->insert($result);
    DB::statement($sql);
    echo "done";
  }

  public static function grabAlistaVersi2($dt){
    if($dt == 'today'){
      $dt = date('Y-m-d');
    }
    elseif($dt == 'thismonth'){
      $dt = date('Y-m');
    };

    $gudang = array(
        ["id_gudang" => "GD0245", "nama_gudang" => "WH SO INV BANJARMASIN 2 (A.YANI)"],
        ["id_gudang" => "GD0244", "nama_gudang" => "WH SO INV BANJARMASIN 1 (BJM CENTRUM)"],
        ["id_gudang" => "G16", "nama_gudang" => "Banjarmasin - Area"],
        ["id_gudang" => "GD0246", "nama_gudang" => "WH SO INV BATULICIN"],
        ["id_gudang" => "GD0247", "nama_gudang" => "WH SO INV BARABAI"],
        ["id_gudang" => "GD0248", "nama_gudang" => "WH SO INV TANJUNG TABALONG"],
        ["id_gudang" => "GD0243", "nama_gudang" => "WH SO INV BANJARBARU"]
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $data = DB::table('akun')->where('id', '8')->first();
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$data->user."&LoginForm%5Bpassword%5D=".$data->pwd."&yt0=");
    // curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=850056&LoginForm%5Bpassword%5D=KalselHibat188&yt0=");
    $result = curl_exec($ch);

    echo"logged in \n";
    $data = DB::table('alista_material_keluar')->where('tgl', 'LIKE', '%'.$dt.'%')->delete();

    foreach($gudang as $g){
        $result = array();
        $no = 1;
        do {
          echo"\nke halaman ".$no." gudang ".$g['nama_gudang']." \n";
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/historypengeluaranproject&Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D='.str_replace(" ", "+", $g["nama_gudang"]).'&Permintaanpengambilanbarang%5Btgl_permintaan%5D='.$dt.'&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D=&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page='.$no);
          $als = curl_exec($ch);
          // dd($als);
          if(!strpos($als, 'No results found.')){
            $columns = array(
              'alista_id',
              'project',
              'nama_gudang',
              'tgl',
              'requester',
              'pengambil',
              'no_rfc',
              'nik_pemakai',
              'mitra',
              'id_permintaan'
            );
            $cals = @str_replace('<a class="view" title="Lihat Detail Permintaan" href="javascript:detailpermintaan(&quot;', '', $als);
            $als = @str_replace('&quot;)"><img src="images/find.png" alt="Lihat Detail Permintaan" /></a>', '', $cals);
            // dd($als);
            $dom = @\DOMDocument::loadHTML(trim($als));

            echo"convert string to variable ".@$g['nama_gudang']." \n".strpos($als, "Last");
            $table = @$dom->getElementsByTagName('table')->item(0);
            $rows = @$table->getElementsByTagName('tr');
            for ($i = 2, $count = $rows->length; $i < $count; $i++)
            {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
              {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }
            $no++;
          }
        }while(strpos($als, '<li class="last">'));
        // dd($result);
        $material = array();
        foreach($result as $noooooo => $r){
          echo "\nambil data ".$noooooo." Alista ID ".$r['alista_id'];
          // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/showdetailpermintaanoperation&id='.$r['id_permintaan']);
          // curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/viewdetailpengambilanbarang&id='.$r['alista_id']);
          curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/downloadpengeluaranmaterial&a='.$r['alista_id'].'&b=&c=&d=&e=&f=&g=&h=');

          $mtr = curl_exec($ch);
          $columnsx= array(2=>"ket", "project", "rfc", "id_gudang", "nama_gudang", "regional", "id_barang", 'nama_barang', "jumlah", "harga", "harga_total", "req", "pengambil", "nik_pemakai", "peruntukan");
          $dom = @\DOMDocument::loadHTML(trim($mtr));
          $table = $dom->getElementsByTagName('table')->item(0);
          $rows = $table->getElementsByTagName('tr');

          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            for ($j = 2, $jcount = 16; $j <= $jcount; $j++)
            {
              $td = $cells->item($j);
              $data[$columnsx[$j]] =  $td->nodeValue;
            }

            unset($data['ket'],$data['rfc'],$data['id_gudang'],$data['regional'],$data['req'],$data['peruntukan']);
            $material[] = array_merge($data,$r);
          }
        }
        // dd($material);
        // DB::table('alista_material_keluar')->where('nama_gudang', $g["nama_gudang"])->where('tgl', 'LIKE', '%'.$dt.'%')->delete();
        DB::table('alista_material_keluar')->insert($material);
    }

    // add
    $data = DB::table('alista_material_keluar')->where('tgl',$dt)->get();
    foreach ($data as $d){
        $idItemBantu = $d->id_barang.'_'.$d->no_rfc;
        DB::table('alista_material_keluar')
            ->where('alista_id',$d->alista_id)
            ->where('id_barang',$d->id_barang)
            ->update([
                'id_item_bantu' => $idItemBantu
            ]);
    }
  }

  public static function insertOrUpdateTable(array $rows,$table)
  {
    $first = reset($rows);
    $columns = implode( ',',
        array_map( function( $value ) { return "$value"; } , array_keys($first) )
    );
    $values = implode( ',', array_map( function( $row ) {
            return '('.implode( ',',
                array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
            ).')';
        } , $rows )
    );
    $updates = implode( ',',
        array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
    );
    $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
    return \DB::statement( $sql );
  }

  public static function starclickMinKalbar(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalbar&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalbar");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinBalikpapan(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Balikpapan&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Balikpapan");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinKaltara(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kaltara&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kaltara");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinSamarinda(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Samarinda&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Samarinda");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function starclickMinKalteng(){
    $startSync=30;
    $allRecord =[];
    for($i=1;$i<=5;$i++){
        $end = $startSync-7;
        echo $i."\nstart:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")));
        echo "\nend:".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        if (date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")))>'31/12/2017'){
            $enddate = "31/12/2017";
        } else {
            $enddate = date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$end, date("Y")));
        }
        //ALL STO except GMB ULI

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=Kalteng&Field=ORG&Fieldstatus=&Fieldwitel=&StartDate=".date('d/m/Y', mktime(0, 0, 0, date("m"), date("d")-$startSync, date("Y")))."&EndDate=".$enddate."&page=1&start=0&limit=10000";
        $result = json_decode(file_get_contents($link, false , stream_context_create($arrContextOptions)));
        echo $link."\n";

        // print_r(count($result->data));
        $order = $result->data;
        foreach ($order as $data){
            //$get_where = mysqli_query($db,"SELECT orderId FROM Data_Pelanggan_Starclick WHERE orderId = '".$data->ORDER_ID."'");
            $orderDate = "";
            $orderDatePs = "";
            if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
            if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
            if ($data->SPEEDY<>''){
                $int = explode('~', $data->SPEEDY);
                if (count($int)>1){
                    $internet = $int[1];
                }else{
                    $internet = $data->SPEEDY;
                }
            };
            if ($data->POTS<>''){
                $telp = explode('~', $data->POTS);
                if (count($telp)>1){
                    $noTelp = $telp[1];
                }else{
                    $noTelp = $data->POTS;
                }
            };
            echo "store ke array\n";
            $allRecord[] = array("orderId"=>$data->ORDER_ID,"orderName"=>str_replace("'","",$data->CUSTOMER_NAME),"orderAddr"=>str_replace("'","",$data->INS_ADDRESS),"orderNotel"=>$data->POTS,"orderKontak"=>$data->PHONE_NO,"orderDate"=>$orderDate,"orderDatePs"=>$orderDatePs,"orderCity"=>str_replace("'","",$data->CUSTOMER_ADDR),"orderStatus"=>$data->STATUS_RESUME,"orderStatusId"=>$data->STATUS_CODE_SC,"orderNcli"=>$data->NCLI,"ndemSpeedy"=>$data->SPEEDY,"ndemPots"=>$data->POTS,"orderPaketID"=>$data->ODP_ID,"kcontact"=>str_replace("'"," ",$data->KCONTACT),"username"=>$data->USERNAME,"alproName"=>$data->LOC_ID,"tnNumber"=>$data->POTS,"reserveTn"=>$data->RNUM,"reservePort"=>$data->ODP_ID,"jenisPsb"=>$data->JENISPSB,"sto"=>$data->XS2,"lat"=>$data->GPS_LATITUDE,"lon"=>$data->GPS_LONGITUDE,"email"=>$data->EMAIL,"internet"=>@$internet,"noTelp"=>@$noTelp,"orderPaket"=>$data->PACKAGE_NAME,"witel"=>"Kalteng");
        }
        $startSync=$end-1;
    }
    echo "saving\n";
    $srcarr=array_chunk($allRecord,500);
    foreach($srcarr as $item) {
        //DB::table('proaktif')->insert($item);
        self::insertOrUpdate($item);
    }
  }

  public static function updateFWFMDashboardSC()
  {
    
    $get_data = DB::SELECT('
      SELECT
      *
      FROM Data_Pelanggan_Starclick
      WHERE
      YEAR(orderDate) = "'.date('Y-m-d').'" AND
      jenisPsb LIKE "AO%" AND
      orderStatus = "Fallout (WFM)"
      GROUP BY orderDate
      ORDER BY orderDate ASC
    ');

    foreach($get_data as $data)
    {
      $date = date('Y:m:d', strtotime($data->orderDate));
      exec('php /srv/htdocs/tomman1_psb/artisan newStarclick '.$date.'');
    }

    print_r("Selesai \n");
  }

  public static function getComparinType($type, $grab)
  {
    $total = 0;

    if (in_array($type, ['FO_PAGI','PI_BARU','PI_PAGI']))
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://10.128.16.65/comparin/controller/api/tomman_wo_pagi.php?token=5ebabd69df0c7e36dfe6dd14c7d068156af5146a4c3867ef056b564f22cfd925&type='.$type,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
      ));

      $response = curl_exec($curl);

      curl_close($curl);

      $response = json_decode($response);
      if ($response->status == 'OK')
      {
        // dd($response->data);

        DB::table('comparin_data_api')->where('type_grab', $type)->delete();
        $total = count($response->data);

        if ($total > 0)
        {
          foreach($response->data as $resp)
          {
            // dd($resp);
            $insert[] = [
              'ORDER_ID' => $resp->ORDER_ID,
              'AGENT_ID' => @$resp->AGENT_ID,
              'ALPRO_LATITUDE' => @$resp->ALPRO_LATITUDE,
              'ALPRO_LONGITUDE' => @$resp->ALPRO_LONGITUDE,
              'APPOINTMENT_DATE' => @$resp->APPOINTMENT_DATE,
              'CAT' => @$resp->CAT,
              'CHANNEL' => @$resp->CHANNEL,
              'CITY_NAME' => @$resp->CITY_NAME,
              'CUSTOMER_ADDR' => @$resp->CUSTOMER_ADDR,
              'CUSTOMER_NAME' => @$resp->CUSTOMER_NAME,
              'DATEL' => @$resp->DATEL,
              'DEVICE_ID' => @$resp->DEVICE_ID,
              'EMAIL' => @$resp->EMAIL,
              'EXTERN_ORDER_ID' => @$resp->EXTERN_ORDER_ID,
              'GPS_LATITUDE' => @$resp->GPS_LATITUDE,
              'GPS_LONGITUDE' => @$resp->GPS_LONGITUDE,
              'INITIAL_PI_INDEX' => json_encode(@$resp->INITIAL_PI_INDEX),
              'INS_ADDRESS' => @$resp->INS_ADDRESS,
              'IS_PI' => @$resp->IS_PI,
              'JENISPSB' => @$resp->JENISPSB,
              'JENISPSB_QUERY' => @$resp->JENISPSB_QUERY,
              'KCONTACT' => @$resp->KCONTACT,
              'KODEFIKASI_SC' => @$resp->KODEFIKASI_SC,
              'KONTAK' => @$resp->KONTAK,
              'LAST_CHECK_DATE' => date('Y-m-d H:i:s', strtotime(@$resp->LAST_CHECK_DATE)),
              'LAST_UPDATED_DATE' => @$resp->LAST_UPDATED_DATE,
              'LAST_UPDATED_DATE_DAY' => @$resp->LAST_UPDATED_DATE_DAY,
              'LAST_UPDATED_DATE_QUERY' => json_encode(@$resp->LAST_UPDATED_DATE_QUERY),
              'LOC_ID' => @$resp->LOC_ID,
              'NCLI' => @$resp->NCLI,
              'ND_INTERNET' => @$resp->ND_INTERNET,
              'ND_POTS' => @$resp->ND_POTS,
              'ORDER_DATE' => @$resp->ORDER_DATE,
              'ORDER_DATE_DAY' => @$resp->ORDER_DATE_DAY,
              'ORDER_DATE_PS' => @$resp->ORDER_DATE_PS,
              'ORDER_DATE_QUERY' => json_encode(@$resp->ORDER_DATE_QUERY),
              'ORDER_ID_NCX' => @$resp->ORDER_ID_NCX,
              'ORDER_STATUS' => @$resp->ORDER_STATUS,
              'PACKAGE_NAME' => @$resp->PACKAGE_NAME,
              'PAKET_DESC' => @$resp->PAKET_DESC,
              'POTS' => @$resp->POTS,
              'PREVIEW_PAKET' => @$resp->PREVIEW_PAKET,
              'PROVIDER' => @$resp->PROVIDER,
              'REGIONAL' => @$resp->REGIONAL,
              'SOURCE' => @$resp->SOURCE,
              'SPEEDY' => @$resp->SPEEDY,
              'STATUS_CODE_SC' => @$resp->STATUS_CODE_SC,
              'STATUS_MESSAGE' => @$resp->STATUS_MESSAGE,
              'STATUS_RESUME' => @$resp->STATUS_RESUME,
              'STO' => @$resp->STO,
              'TGL_PS' => @$resp->TGL_PS,
              'TN_NUMBER' => @$resp->TN_NUMBER,
              'TYPE_LAYANAN' => @$resp->TYPE_LAYANAN,
              'USERNAME' => @$resp->USERNAME,
              'WFM_ID' => @$resp->WFM_ID,
              'WITEL' => @$resp->WITEL,
              'isFinal' => @$resp->isFinal,
              // 'log_fcc' => json_encode(@$resp->log_fcc),
              'monitor_date' => date('Y-m-d', strtotime(@$resp->monitor_date)),
              'monitor_re_date' => @$resp->monitor_re_date,
              'valid_monitor' => @$resp->valid_monitor,
              'monitor_re_status' => @$resp->monitor_re_status,
              'INITIAL_PI_DATE' => @$resp->INITIAL_PI_DATE,
              'JENIS_ADDON' => str_replace(['[',']','"'], '', json_encode(@$resp->JENIS_ADDON)),
              'JENIS_DIGCHANNEL' => @$resp->JENIS_DIGCHANNEL,
              // 'REVOKE_HIST' => json_encode(@$resp->REVOKE_HIST),
              // 'log_custom_scncx' => json_encode(@$resp->log_custom_scncx),
              // 'log_kpro' => json_encode(@$resp->log_kpro),
              // 'log_scncx' => json_encode(@$resp->log_scncx),
              // 'log_scncx_raw' => json_encode(@$resp->log_scncx_raw),
              // 'summary_wfm' => json_encode(@$resp->summary_wfm),
              // 'wfm_details' => json_encode(@$resp->wfm_details),
              // 'log_pi_progress' => json_encode(@$resp->log_pi_progress),
              // 'PI_LOG' => json_encode(@$resp->PI_LOG),
              'is_dispatched' => @$resp->is_dispatched,
              'last_dispatch_date' => @$resp->last_dispatch_date,
              // 'log_dispatch' => json_encode(@$resp->log_dispatch),
              'type_grab' => @$type
            ];
            // next time insert log
            // log_custom_scncx , log_fcc , log_kpro , log_pi_progress , log_scncx , log_scncx_raw , summary_wfm , wfm_details , PI_LOG , last_survey_coor
          }

          $chunk = array_chunk($insert,500);
          foreach($chunk as $numb => $data)
          {
            DB::table('comparin_data_api')->insert($data);
            if ($grab  == 'log')
            {
              DB::table('comparin_data_api_log')->insert($data);
            }

            print_r("saved type $type page $numb success!\n");
          }
        }

      } else {

        print_r("\nAPI Status Response NOK\n\n");

      }

      print_r("\nFinish Update $type Total $total!\n\n");

    } elseif ($type == 'ADDON') {
      $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://10.128.16.65/comparin/controller/monitor/get_data_api.php?core=get_addon&area=KALSEL&subarea=ALL&umur=ALL&mode=monitor&status=TOTAL&jenisaddon=TOTAL_REV&logstatus=&tahun=ALL&token=Z2V0X2RhdGFfYXBpX2NvbXBhcmlu',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);

        $total = count($result->data);
        
        if ($total > 0)
        {
          DB::table('comparin_data_api')->where('type_grab', 'ADDON')->delete();

          foreach ($result->data as $data)
          {
            $insert[] = [
              'ORDER_ID' => $data->ORDER_ID,
              'AGENT_ID' => @$data->AGENT_ID,
              'ALPRO_LATITUDE' => @$data->ALPRO_LATITUDE,
              'ALPRO_LONGITUDE' => @$data->ALPRO_LONGITUDE,
              'APPOINTMENT_DATE' => @$data->APPOINTMENT_DATE,
              'CAT' => @$data->CAT,
              'CHANNEL' => @$data->CHANNEL,
              'CITY_NAME' => @$data->CITY_NAME,
              'CUSTOMER_ADDR' => @$data->CUSTOMER_ADDR,
              'CUSTOMER_NAME' => @$data->CUSTOMER_NAME,
              'DATEL' => @$data->DATEL,
              'DEVICE_ID' => @$data->DEVICE_ID,
              'EMAIL' => @$data->EMAIL,
              'EXTERN_ORDER_ID' => @$data->EXTERN_ORDER_ID,
              'GPS_LATITUDE' => @$data->GPS_LATITUDE,
              'GPS_LONGITUDE' => @$data->GPS_LONGITUDE,
              'INITIAL_PI_INDEX' => json_encode(@$data->INITIAL_PI_INDEX),
              'INS_ADDRESS' => @$data->INS_ADDRESS,
              'IS_PI' => @$data->IS_PI,
              'JENISPSB' => @$data->JENISPSB,
              'JENISPSB_QUERY' => @$data->JENISPSB_QUERY,
              'KCONTACT' => @$data->KCONTACT,
              'KODEFIKASI_SC' => @$data->KODEFIKASI_SC,
              'KONTAK' => @$data->KONTAK,
              'LAST_CHECK_DATE' => date('Y-m-d H:i:s', strtotime(@$data->LAST_CHECK_DATE)),
              'LAST_UPDATED_DATE' => @$data->LAST_UPDATED_DATE,
              'LAST_UPDATED_DATE_DAY' => @$data->LAST_UPDATED_DATE_DAY,
              'LAST_UPDATED_DATE_QUERY' => json_encode(@$data->LAST_UPDATED_DATE_QUERY),
              'LOC_ID' => @$data->LOC_ID,
              'NCLI' => @$data->NCLI,
              'ND_INTERNET' => @$data->ND_INTERNET,
              'ND_POTS' => @$data->ND_POTS,
              'ORDER_DATE' => @$data->ORDER_DATE,
              'ORDER_DATE_DAY' => @$data->ORDER_DATE_DAY,
              'ORDER_DATE_PS' => @$data->ORDER_DATE_PS,
              'ORDER_DATE_QUERY' => json_encode(@$data->ORDER_DATE_QUERY),
              'ORDER_ID_NCX' => @$data->ORDER_ID_NCX,
              'ORDER_STATUS' => @$data->ORDER_STATUS,
              'PACKAGE_NAME' => @$data->PACKAGE_NAME,
              'PAKET_DESC' => @$data->PAKET_DESC,
              'POTS' => @$data->POTS,
              'PREVIEW_PAKET' => @$data->PREVIEW_PAKET,
              'PROVIDER' => @$data->PROVIDER,
              'REGIONAL' => @$data->REGIONAL,
              'SOURCE' => @$data->SOURCE,
              'SPEEDY' => @$data->SPEEDY,
              'STATUS_CODE_SC' => @$data->STATUS_CODE_SC,
              'STATUS_MESSAGE' => @$data->STATUS_MESSAGE,
              'STATUS_RESUME' => @$data->STATUS_RESUME,
              'STO' => @$data->STO,
              'TGL_PS' => @$data->TGL_PS,
              'TN_NUMBER' => @$data->TN_NUMBER,
              'TYPE_LAYANAN' => @$data->TYPE_LAYANAN,
              'USERNAME' => @$data->USERNAME,
              'WFM_ID' => @$data->WFM_ID,
              'WITEL' => @$data->WITEL,
              'isFinal' => @$data->isFinal,
              // 'log_fcc' => json_encode(@$data->log_fcc),
              'monitor_date' => date('Y-m-d', strtotime(@$data->monitor_date)),
              'monitor_re_date' => @$data->monitor_re_date,
              'valid_monitor' => @$data->valid_monitor,
              'monitor_re_status' => @$data->monitor_re_status,
              'INITIAL_PI_DATE' => @$data->INITIAL_PI_DATE,
              'JENIS_ADDON' => str_replace(['[',']','"'], '', json_encode(@$data->JENIS_ADDON)),
              'JENIS_DIGCHANNEL' => @$data->JENIS_DIGCHANNEL,
              // 'REVOKE_HIST' => json_encode(@$data->REVOKE_HIST),
              // 'log_custom_scncx' => json_encode(@$data->log_custom_scncx),
              // 'log_kpro' => json_encode(@$data->log_kpro),
              // 'log_scncx' => json_encode(@$data->log_scncx),
              // 'log_scncx_raw' => json_encode(@$data->log_scncx_raw),
              // 'summary_wfm' => json_encode(@$data->summary_wfm),
              // 'wfm_details' => json_encode(@$data->wfm_details),
              // 'log_pi_progress' => json_encode(@$data->log_pi_progress),
              // 'PI_LOG' => json_encode(@$data->PI_LOG),
              'is_dispatched' => @$data->is_dispatched,
              'last_dispatch_date' => @$data->last_dispatch_date,
              // 'log_dispatch' => json_encode(@$data->log_dispatch),
              'type_grab' => 'ADDON'
            ];
          }

          $chunk = array_chunk($insert,500);
          foreach($chunk as $numb => $data)
          {
            DB::table('comparin_data_api')->insert($data);

            if ($grab == 'log')
            {
              DB::table('comparin_data_api_log')->insert($data);              
            }

            print_r("saved type ADDON page $numb success!\n");
          }
        }
    }
  }

  public static function getComparin($grab)
  {
    self::getComparinType('FO_PAGI', $grab);
    sleep(1);

    self::getComparinType('PI_BARU', $grab);
    sleep(1);

    self::getComparinType('PI_PAGI', $grab);
    sleep(1);

    self::getComparinType('ADDON', $grab);
    sleep(1);
  }

  public static function scWithLogIn()
  {
    
    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $ch = curl_init();
    //get login page
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $rough_content = curl_exec($ch);

    //get session
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'param=0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    print_r($matches['cookie']);
    $cookiesOut = "";
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookiesOut = implode("; ", $matches['cookie']);

    if($cookiesOut){
      DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->update([
        'cookies'  => $cookiesOut
      ]);
    }

    $starclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();
    //send OTP
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/send-otp');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'guid=0&code=0&data=%7B%22code%22%3A%22'.$starclick->username.'%22%7D');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $rough_content = curl_exec($ch);

    //get captcha img
    $fp = fopen('sc.jpg', 'wb');
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/actor/captcha/image');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, false);

    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    curl_setopt($ch, CURLOPT_FILE, $fp);
    $rough_content = curl_exec($ch);

    //send captcha to telegram
    fclose($fp);

    $chatIDs = ['-306306083', '109025591'];
    
    foreach ($chatIDs as $chatID) {
      Telegram::sendPhoto([
        'chat_id' => $chatID,
        'parse_mode' => 'html',
        'photo' => 'sc.jpg',
        'caption' => "Kode Captcha Starclick " . date('Y-m-d H:i:s') . ""
      ]);
    }

    print_r("input captcha:\n");
    $captcha = 0;
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);

    if(trim($line) == 'cancel'){
        print_r("ABORTING!\n");
        exit;
    }

    $captcha = trim($line);
    fclose($handle);
    // print_r("response $captcha");

    //input otp dan captcha
    $otp = 0;
    print_r("\ninput otp:\n");
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    
    if(trim($line) == 'cancel'){
        print_r("ABORTING!\n");
        exit;
    }
    
    $otp = trim($line);
    fclose($handle);

    //login
    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/authenticate');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'guid=0&code=0&data=%7B%22code%22%3A%22'.$starclick->username.'%22%2C%22password%22%3A%22'. $starclick->password.'%22%2C%22otp%22%3A%22'.$otp.'%22%2C%22captcha%22%3A%22'.$captcha.'%22%7D');
    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    $rough_content = curl_exec($ch);
    curl_close($ch);

    dd(json_decode($rough_content));
  }
  public static function scRefresher()
  {
    
    $starclick = DB::table('cookie_systems')->where('application', 'starclick')->where('witel', 'KALSEL')->first();
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/api/public/starclick/user/get-session');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://starclickncx.telkom.co.id/newsc/index.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $starclick->cookies);

    $response = curl_exec($ch);
    curl_close($ch);
    dd($response);
  }

  public static function grabRacoonDismantling()
  {
    
    $cookie = DB::table('cookie_systems')->where('application', 'raccoon')->where('witel', 'KALSEL')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://raccoon.telkomsigma.co.id/ta/index.php/Monitoring/show_dapros',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'level=witel&id_area=28&jenis=sisa&tipe_dapros=ALL&tahun_dapros=ALL&nte_merk=ALL&nte_type=ALL&nte_classification=ALL',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: '.$cookie->cookies.''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    // print_r("$response \n");
    $decode = json_decode($response);
    if (count(@$decode->data)>0)
    {

      DB::table('racoonDismantling')->truncate();

      $data = $decode->data;
      $total = count($data);

      foreach($data as $d)
      {
        $insert[] = [
          'treg' => $d->treg,
          'witel' => $d->witel,
          'sto' => $d->sto,
          'ncli' => $d->ncli,
          'nopel' => $d->nopel,
          'namapelanggan' => $d->namapelanggan,
          'alamat' => $d->alamat,
          'telepon' => $d->telepon,
          'tahun_dapros' => $d->tahun_dapros,
          'tipe_dapros' => $d->tipe_dapros,
          'tanggal_janji_ambil' => $d->tanggal_janji_ambil
        ];

        $check = DB::table('cabut_nte_order')->where('wfm_id', $d->nopel)->first();
        if (count($check)>0)
        {
          DB::transaction(function() use($d) {
            DB::table('cabut_nte_order')->where('wfm_id', $d->nopel)->update([
              'no_hp' => $d->telepon,
              'dapros_tahun' => $d->tahun_dapros,
              'alamat' => $d->alamat,
              'sto' => $d->sto,
              'ambil_janji_tgl' => $d->tanggal_janji_ambil,
              'jenis_dismantling' => $d->tipe_dapros
            ]);
          });
        } else {
          DB::transaction(function() use($d) {
            DB::table('cabut_nte_order')->insert([
              'wfm_id' => $d->nopel,
              'no_hp' => $d->telepon,
              'dapros_tahun' => $d->tahun_dapros,
              'alamat' => $d->alamat,
              'sto' => $d->sto,
              'nama_pelanggan' => $d->namapelanggan,
              'ambil_janji_tgl' => $d->tanggal_janji_ambil,
              'tgl_order' => date('Y-m-d H:i:s'),
              'order_by' => "UNDISPATCH_RACOON",
              'jenis_dismantling' => $d->tipe_dapros
            ]);
          });
        }
      }

      $chunk = array_chunk($insert,500);
      foreach($chunk as $data)
      {
        DB::table('racoonDismantling')->insert($data);
      }

      print_r("Finish Grab Raccoon Dapros Dismantling Total $total \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Finish Grab <b>Raccoon Dapros</b> Dismantling <b>Total $total</b>\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    } else {

      print_r("Failed Grab Raccoon Dapros Dismantling !!! \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Failed Grab <b>Raccoon Dapros</b> Dismantling !!!\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    }


  }

  public static function collectedRacoonDismantling()
  {
    
    $cookie = DB::table('cookie_systems')->where('application', 'raccoon')->where('witel', 'KALSEL')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://raccoon.telkomsigma.co.id/ta/index.php/Monitoring/show_detail',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'jenis_nte=ONT,STB&progress=collected&level=witel&id_area=28&tanggal=&tipe_dapros=ALL&tahun_dapros=ALL&nte_merk=ALL&nte_type=ALL&nte_classification=ALL',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: '.$cookie->cookies.''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $decode = json_decode($response);

    if (count(@$decode->data)>0)
    {

      DB::table('collectedRacoonDismantling')->truncate();

      $data = $decode->data;
      $total = count($data);

      foreach($data as $d)
      {
        $insert[] = [
          'treg' => $d->treg,
          'witel' => $d->witel,
          'jenis_nte' => $d->jenis_nte,
          'sn' => $d->sn,
          'nopel' => $d->nopel,
          'namapelanggan' => $d->namapelanggan,
          'nik' => $d->nik,
          'namapetugasctb' => $d->namapetugasctb,
          'tanggalcreate' => $d->tanggalcreate,
          'tanggalmasukgudang' => $d->tanggalmasukgudang,
          'nama_wh' => $d->nama_wh,
          'petugas_wh' => $d->petugas_wh,
          'tanggalapproval' => $d->tanggalapproval,
          'tanggalkasir' => $d->tanggalkasir,
          'tipe_dapros' => $d->tipe_dapros,
          'tahun_dapros' => $d->tahun_dapros
        ];
      }

      $chunk = array_chunk($insert,500);
      foreach($chunk as $data)
      {
        DB::table('collectedRacoonDismantling')->insert($data);
      }

      print_r("Finish Grab Collected Raccoon Dismantling Total $total \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Finish Grab <b>Collected Raccoon</b> Dismantling <b>Total $total</b>\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    } else {

      print_r("Failed Grab Collected Raccoon Dismantling !!! \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Failed Grab <b>Collected Raccoon</b> Dismantling !!!\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    }

  }

  public static function visitRacoonDismantling()
  {
    
    $cookie = DB::table('cookie_systems')->where('application', 'raccoon')->where('witel', 'KALSEL')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://raccoon.telkomsigma.co.id/ta/index.php/Monitoring/show_visit',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'level=witel&id_area=28&tanggal=&tipe_dapros=ALL&tahun_dapros=ALL&nte_merk=ALL&nte_type=ALL&nte_classification=ALL',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: '.$cookie->cookies.''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $decode = json_decode($response);

    if (count(@$decode->data)>0)
    {

      DB::table('visitRacoonDismantling')->truncate();

      $data = $decode->data;
      $total = count($data);

      foreach($data as $d)
      {
        $insert[] = [
          'treg' => $d->treg,
          'witel' => $d->witel,
          'nopel' => $d->nopel,
          'namapelanggan' => $d->namapelanggan,
          'nik' => $d->nik,
          'namapetugasctb' => $d->namapetugasctb,
          'nama_yg_ditemui' => $d->nama_yg_ditemui,
          'alasan' => $d->alasan,
          'tanggalcreate' => $d->tanggalcreate,
          'ont' => $d->ont,
          'stb' => $d->stb,
          'plc' => $d->plc,
          'wifiextender' => $d->wifiextender,
          'indibox' => $d->indibox,
          'indihomesmart' => $d->indihomesmart,
          'no_ba' => $d->no_ba,
          'tgl_janji_bayar' => $d->tgl_janji_bayar,
          'tahun_dapros' => $d->tahun_dapros,
          'tipe_dapros' => $d->tipe_dapros
        ];
      }

      $chunk = array_chunk($insert,500);
      foreach($chunk as $data)
      {
        DB::table('visitRacoonDismantling')->insert($data);
      }

      DB::statement('UPDATE visitRacoonDismantling vrd LEFT JOIN Data_Pelanggan_Starclick dps ON vrd.nopel = dps.internet AND dps.jenisPsb LIKE "AO%" SET vrd.id_sto = dps.sto');

      print_r("Finish Grab Visited Raccoon Dismantling Total $total \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Finish Grab <b>Visited Raccoon</b> Dismantling <b>Total $total</b>\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    } else {

      print_r("Failed Grab Visited Raccoon Dismantling !!! \n");

      Telegram::sendMessage([
        'chat_id' => '-306306083',
        'parse_mode' => 'html',
        'text' => "Failed Grab <b>Visited Raccoon</b> Dismantling !!!\n\n<i>".date('Y-m-d H:i:s')."</i>"
      ]);

    }
  }

  public static function getXproTelkom()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://xpro.telkom.co.id/hsi/downloadreportscbe.php?tipe=UNSC&prev=scbe.php&treg=6&mode=STO&unit=all&psb=NEW+SALES&play=all&category=&package=all&deposit=&channel=&hari=ALL',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: PHPSESSID=n32lra8a0pptgbvu5isiuqcap3'
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $replace_1 = (str_replace(array("\n", "\r\n", "\t"), "", $response));
    $removed_header = ' <table border="1"><tr><th style="background-color:black; color:white;">NO</th>               <th style="background-color:black; color:white;">REGIONAL</th>       <th style="background-color:black; color:white;">WITEL</th> <th style="background-color:black; color:white;">DATEL</th>  <th style="background-color:black; color:white;">STO</th>      <th style="background-color:black; color:white;">ORDER ID</th>   <th style="background-color:black; color:white;">TYPE TRANSAKSI</th> <th style="background-color:black; color:white;">JENIS LAYANAN</th>  <th style="background-color:black; color:white;">ALPRO</th>   <th style="background-color:black; color:white;">NCLI</th>    <th style="background-color:black; color:white;">POTS</th>    <th style="background-color:black; color:white;">INTERNET</th>  <th style="background-color:black; color:white;">STATUS RESUME</th>    <th style="background-color:black; color:white;">STATUS MESSAGE</th> <th style="background-color:black; color:white;">ORDER DATE</th>    <th style="background-color:black; color:white;">LAST UPDATE STATUS</th>  <th style="background-color:black; color:white;">DURASI V1</th>  <th style="background-color:black; color:white;">NAMA CUST</th>    <th style="background-color:black; color:white;">NO HP</th>    <th style="background-color:black; color:white;">ALAMAT</th> <th style="background-color:black; color:white;">K-CONTACT</th>    <th style="background-color:black; color:white;">LONG</th>    <th style="background-color:black; color:white;">LAT</th>       <th style="background-color:black; color:white;">WFM ID</th>    <th style="background-color:black; color:white;">STATUS WFM</th><th style="background-color:black; color:white;">DESK TASK</th>    <th style="background-color:black; color:white;">STATUS TASK</th>    <th style="background-color:black; color:white;">TGL INSTALL</th>     <th style="background-color:black; color:white;">AMCREW</th>       <th style="background-color:black; color:white;">TEKNISI1</th>    <th style="background-color:black; color:white;">PERSONID</th>   <th style="background-color:black; color:white;">TEKNISI2</th>    <th style="background-color:black; color:white;">PERSONID2</th> <th style="background-color:black; color:white;">TINDAK LANJUT</th> <th style="background-color:black; color:white;">KETERANGAN</th> <th style="background-color:black; color:white;">USER</th> <th style="background-color:black; color:white;">TGL TINDAK LANJUT</th><th style="background-color:black; color:white;">PACKAGE NAME</th><th style="background-color:black; color:white;">ESTIMASI DROP CORE</th><th style="background-color:black; color:white;">PROVIDER</th></tr>';
    $replace_2 = str_replace(array($removed_header, "</tr>"), "", $replace_1);
    $explode_replace = explode("<tr>", $replace_2);

    foreach ($explode_replace as $expl) {
      if ($expl <> "") {
        $expl_data = str_replace("<td>", "", (explode("</td>", $expl)));
        dd($expl_data);
      }
    }
  }

  private function underspecOrder($tahun)
  {
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://access-quality.telkom.co.id/rekap_unspec_sektor/dashboard_semesta/export_detail_unspec_sektor_all.php?tanggal=' . date('Y-m-d') . '&jenis=' . $tahun . '&regional=6&sektor=SEKTOR%2520-%2520KALSEL%2520-%2520ULIN%25202&witel=KALSEL',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
      1 => 'reg', 'witel', 'sektor', 'node_id', 'shelf_slot_port', 'cmdf', 'rk', 'dp', 'nd', 'nama', 'alamat', 'status_inet', 'onu_rx_power', 'nomor_tiket', 'status_tiket', 'lgest'
    );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $data['date_updated'] = date('Ymd');
      $data['nomor_tiket_int'] = (substr($data['nomor_tiket'], 2, 8));
      $data['kode_order'] = $tahun;
      $result[] = $data;
    }

    $total = count($result);

    if ($tahun == '2019') {
      $table = 'underspecProv_tr6';
      $title = 'Provisioning';
    } elseif ($tahun == '2018') {
      $table = 'underspecMaintenance_tr6';
      $title = 'Maintenance';
    }

    DB::table($table)->whereDate('last_updated', date('Y-m-d'))->delete();

    $srcarr = array_chunk($result, 500);
    foreach ($srcarr as $item) {
      DB::table($table)->insert($item);
    }

    print_r("Success Grab Underspec $title Total $total\n");

    Telegram::sendMessage([
      'chat_id' => '-306306083',
      'parse_mode' => 'html',
      'text' => "Success Grab Underspec $title <b>Total $total</b> <i>" . date('Y-m-d H:i:s') . "</i>"
    ]);
  }

  public static function updateUnderspecSemesta()
  {
    $that = new GrabController();
    // grab underspec provisioning
    $that->underspecOrder("2019");

    // grab underspec maintenance
    $that->underspecOrder("2018");

    print_r("Done!! \n");
  }

  public static function workOrderListTactical()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/api/get/workorder/list',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => array('reg' => 'REG6','source' => 'ALL'),
      CURLOPT_HTTPHEADER => array(
        'apisecret: SndaaFJwcDNXM2JBVkRaTG43WXdlZz09'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $result = json_decode($response);

    if ($result->error == false)
    {
      if (count($result->data) > 0)
      {
        DB::table('tacticalpro_tr6_log')->truncate();

        foreach ($result->data as $value) {
          $insert[] = [
            'id' => $value->id,
            'reg' => $value->reg,
            'witel' => $value->witel,
            'sc_id' => $value->sc_id,
            'layanan' => $value->layanan,
            'sto' => $value->sto,
            'nama_pelanggan' => $value->nama_pelanggan,
            'alamat_pelanggan' => $value->alamat_pelanggan,
            'order_date' => $value->order_date,
            'status_wo' => $value->status_wo,
            'kcontact' => $value->kcontact
          ];
        }

        $chunk = array_chunk($insert, 500);
      
        foreach ($chunk as $numb => $data)
        {
          DB::table('tacticalpro_tr6_log')->insert($data);

          print_r("saved page $numb success!\n");
        }

        print_r("finish!\n");
      }
    }
  }

  public static function workorderTactical($witel)
  {
    $startDate = '2023-05-01';
    $endDate = date('Y-m-d');

    $tacticalpro = DB::table('cookie_systems')->where('application', 'tacticalpro')->where('witel', $witel)->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/workorder/get',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'draw=1&columns%5B0%5D%5Bdata%5D=&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=durasi&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=track_id&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=sc_id&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=jenis_paket&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=layanan&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=no_inet&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=order_date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=alpro&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=status_wo&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=teknisi&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=sto&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=false&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=mitra&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B13%5D%5Bdata%5D=witel&columns%5B13%5D%5Bname%5D=&columns%5B13%5D%5Bsearchable%5D=true&columns%5B13%5D%5Borderable%5D=false&columns%5B13%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B13%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B14%5D%5Bdata%5D=jenis_pekerjaan&columns%5B14%5D%5Bname%5D=&columns%5B14%5D%5Bsearchable%5D=true&columns%5B14%5D%5Borderable%5D=false&columns%5B14%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B14%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B15%5D%5Bdata%5D=ps_date&columns%5B15%5D%5Bname%5D=&columns%5B15%5D%5Bsearchable%5D=true&columns%5B15%5D%5Borderable%5D=false&columns%5B15%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B15%5D%5Bsearch%5D%5Bregex%5D=false&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&filter_date='.$startDate.'+s%2Fd+'.$endDate.'&id=index&breakdown=SHJncnlhemZjVWM9&source=',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $tacticalpro->cookies . ''
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($response);

    if ($result != null)
    {
      if ($result->recordsFiltered > 0)
      {
        print_r("start grab from records total $result->recordsFiltered\n\n");

        DB::table('tacticalpro_tr6')->where('witel_ms2n', $witel)->whereBetween('created_at_format', [$startDate, $endDate])->delete();
      
        $rows = @$result->recordsFiltered;

        $split = 700;

        for ($i = 0; $i <= $rows; $i++)
        {
          $output[] = $i;
        }

        $output = array_chunk($output, $split);
        // $fd = [];

        foreach($output as $k => $v)
        {
            if ($k == 0)
            {
              $current = 0;
            }
            else
            {
              $current = current($v);
            }

            // $fd[$k][0] = current($v);
            // $fd[$k][1] = end($v);

            print_r("pages_workorderTactical $current to $split\n");

            self::pages_workorderTactical($witel, $startDate, $endDate, $current, $split);
        }
      }
    }
  }

  public static function pages_workorderTactical($witel, $startDate, $endDate, $x, $y)
  {
    $tacticalpro = DB::table('cookie_systems')->where('application', 'tacticalpro')->where('witel', $witel)->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/workorder/get',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'draw=1&columns%5B0%5D%5Bdata%5D=&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=durasi&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=track_id&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=sc_id&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=jenis_paket&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=layanan&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=no_inet&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=order_date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=alpro&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=status_wo&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=teknisi&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=sto&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=false&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=mitra&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B13%5D%5Bdata%5D=witel&columns%5B13%5D%5Bname%5D=&columns%5B13%5D%5Bsearchable%5D=true&columns%5B13%5D%5Borderable%5D=false&columns%5B13%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B13%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B14%5D%5Bdata%5D=jenis_pekerjaan&columns%5B14%5D%5Bname%5D=&columns%5B14%5D%5Bsearchable%5D=true&columns%5B14%5D%5Borderable%5D=false&columns%5B14%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B14%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B15%5D%5Bdata%5D=ps_date&columns%5B15%5D%5Bname%5D=&columns%5B15%5D%5Bsearchable%5D=true&columns%5B15%5D%5Borderable%5D=false&columns%5B15%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B15%5D%5Bsearch%5D%5Bregex%5D=false&start='.$x.'&length='.$y.'&search%5Bvalue%5D=&search%5Bregex%5D=false&filter_date='.$startDate.'+s%2Fd+'.$endDate.'&id=index&breakdown=SHJncnlhemZjVWM9&source=',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $tacticalpro->cookies . ''
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($response);

    $insert[] = [];

    if ($result <> null)
    {
      foreach ($result->data as $data)
      {
        $insert[] = [
          'durasi'                    => $data->durasi,
          'tgl_pickup'                => $data->tgl_pickup,
          'tgl_berangkat_kerja'       => $data->tgl_berangkat_kerja,
          'tgl_mulai_kerja'           => $data->tgl_mulai_kerja,
          'tgl_selesai_kerja'         => $data->tgl_selesai_kerja,
          'keterangan'                => $data->keterangan,
          'last_status'               => $data->last_status,
          'enc_id'                    => $data->enc_id,
          'witel_ms2n'                => $data->witel_ms2n,
          'id'                        => $data->id,
          'id_pickup_order_survey'    => $data->id_pickup_order_survey,
          'id_pickup_order_instalasi' => $data->id_pickup_order_instalasi,
          'sc_id'                     => $data->sc_id,
          'track_id'                  => $data->track_id,
          'order_date'                => $data->order_date,
          'ps_date'                   => $data->ps_date,
          'nama_pelanggan'            => $data->nama_pelanggan,
          'nohp_pelanggan'            => $data->nohp_pelanggan,
          'titik_koordinat'           => $data->titik_koordinat,
          'alpro'                     => $data->alpro,
          'id_sto'                    => $data->id_sto,
          'no_telp'                   => $data->no_telp,
          'no_inet'                   => $data->no_inet,
          'tgl_manja_pelanggan'       => $data->tgl_manja_pelanggan,
          'kcontact'                  => $data->kcontact,
          'alamat_pelanggan'          => $data->alamat_pelanggan,
          'layanan'                   => $data->layanan,
          'info_tambahan'             => $data->info_tambahan,
          'qrcode'                    => $data->qrcode,
          'teknisi_1'                 => $data->teknisi_1,
          'teknisi_2'                 => $data->teknisi_2,
          'amcrew'                    => $data->amcrew,
          'status_wo'                 => $data->status_wo,
          'id_witel'                  => $data->id_witel,
          'reg'                       => $data->reg,
          'source'                    => $data->source,
          'jenis_paket'               => $data->jenis_paket,
          'tanggal_fo'                => $data->tanggal_fo,
          'tanggal_fulfill'           => $data->tanggal_fulfill,
          'datestamp'                 => $data->datestamp,
          'revoke'                    => $data->revoke,
          'created_at_format'         => date('Y-m-d', strtotime($data->created_at)),
          'created_at'                => $data->created_at,
          'created_by'                => $data->created_by,
          'updated_at'                => $data->updated_at,
          'updated_by'                => $data->updated_by,
          'deleted_at'                => $data->deleted_at,
          'onwork_1'                  => $data->onwork_1,
          'onwork_2'                  => $data->onwork_2,
          'teknisi'                   => $data->teknisi,
          'sto'                       => $data->sto,
          'mitra'                     => $data->mitra,
          'uc_by'                     => $data->uc_by,
          'uc_at'                     => $data->uc_at,
          'disable_assign'            => $data->disable_assign,
          'witel'                     => $data->witel,
          'detail_revoke'             => $data->detail_revoke,
          'recycle'                   => $data->recycle
        ];
      }

      if (array_filter($insert))
      {
        $chunk = array_chunk(array_filter($insert), 350);

        foreach ($chunk as $numb => $dump)
        {
          DB::table('tacticalpro_tr6')->insert($dump);

          print_r("saved page $numb success from $x to $y!\n");
        }

        DB::statement('DELETE a1 FROM tacticalpro_tr6 a1, tacticalpro_tr6 a2 WHERE a1.id_tacticalpro_tr6 > a2.id_tacticalpro_tr6 AND a1.sc_id = a2.sc_id');

        print_r("Finish Syncron TacticalProvisioning Witel $witel Page $x to $y\n");
      }
    }
    else
    {
      print_r("Miss Syncron TacticalProvisioning Witel $witel Page $x to $y Total null :(\n");
    }
  }

  public static function refresh_tacticalpro($witel)
  {
    $tacticalpro = DB::table('cookie_systems')->where('application', 'tacticalpro')->where('witel', $witel)->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/workorder/dashboard',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'draw=1&columns%5B0%5D%5Bdata%5D=num&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=name&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=order_basket&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=order_sdh_pickup&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=order_blm_pickup&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=order_manja&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=order_manja1jam&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=teknisi_masuk&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=teknisi_sdh_pickup&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=teknisi_blm_pickup&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=proses_survey&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=survey_3jam&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=false&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=proses_instalasi&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B13%5D%5Bdata%5D=instalasi_3jam&columns%5B13%5D%5Bname%5D=&columns%5B13%5D%5Bsearchable%5D=true&columns%5B13%5D%5Borderable%5D=false&columns%5B13%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B13%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B14%5D%5Bdata%5D=completed_teknisi&columns%5B14%5D%5Bname%5D=&columns%5B14%5D%5Bsearchable%5D=true&columns%5B14%5D%5Borderable%5D=false&columns%5B14%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B14%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B15%5D%5Bdata%5D=completed_kpro&columns%5B15%5D%5Bname%5D=&columns%5B15%5D%5Bsearchable%5D=true&columns%5B15%5D%5Borderable%5D=false&columns%5B15%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B15%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B16%5D%5Bdata%5D=order_kendala&columns%5B16%5D%5Bname%5D=&columns%5B16%5D%5Bsearchable%5D=true&columns%5B16%5D%5Borderable%5D=false&columns%5B16%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B16%5D%5Bsearch%5D%5Bregex%5D=false&start=0&length=-1&search%5Bvalue%5D=&search%5Bregex%5D=false&breakdown=SHJncnlhemZjVWM9&id=index&filter_dashboard=' . date('Y-m-d') . '%20%20s%2Fd%20' . date('Y-m-d') . '',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $tacticalpro->cookies . ''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    print_r("$response");
  }

  public static function login_tacticalpro()
  {
    $akun = DB::table('akun')->where('app', 'tacticalpro')->first();

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/auth/login',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));
    $response = curl_exec($curl);

    $err = curl_errno($curl);
    $errmsg = curl_error($curl);
    $header = curl_getinfo($curl);
    $header_content = substr($response, 0, $header['header_size']);
    trim(str_replace($header_content, '', $response));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    print_r($matches['cookie']);
    $cookiesOut = "";
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    $cookiesOut = implode("; ", $matches['cookie']);

    if($cookiesOut)
    {
      DB::table('cookie_systems')
      ->where('application', 'tacticalpro')
      ->where('witel', 'KALSEL')
      ->update([
        'cookies'  => $cookiesOut
      ]);
    }

    $dom = @\DOMDocument::loadHTML(trim($response));

    $targetInputName = 'csrf_test_name';
    $inputElements = $dom->getElementsByTagName('input');
    $csrf_test_name = '';
    foreach ($inputElements as $input) {
        if ($input->hasAttribute('name') && $input->getAttribute('name') === $targetInputName)
        {
            $csrf_test_name = $input->getAttribute('value');
            break;
        }
    }

    $img_captcha = $dom->getElementById('CaptchaDiv')->getAttribute("src");

    file_put_contents(public_path().'/tacticalpro_captcha.jpg', file_get_contents($img_captcha));

    Telegram::sendPhoto([
      'chat_id'    => 401791818,
      'parse_mode' => 'html',
      'photo'      => public_path().'/tacticalpro_captcha.jpg',
      'caption'    => "Kode Captcha TacticalPro ".date('Y-m-d H:i:s')
    ]);

    print_r("input captcha:\n");
    $captcha = 0;
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);

    if(trim($line) == 'cancel')
    {
        print_r("ABORTING!\n");
        exit;
    }

    $captcha = trim($line);
    fclose($handle);
    print_r("response $captcha\n\n");
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/auth/dologin/v2',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'csrf_test_name='.$csrf_test_name.'&username='.$akun->user.'&password='.$akun->pwd.'&type_login=1&CaptchaInput='.$captcha,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: '.$cookiesOut
      ),
    ));
    curl_exec($curl);

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/auth/otp',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: '.$cookiesOut
      ),
    ));
    $response = curl_exec($curl);

    print_r("input otp:\n");
    $otp = 0;
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);

    if(trim($line) == 'cancel')
    {
        print_r("ABORTING!\n");
        exit;
    }

    $otp = trim($line);
    fclose($handle);
    print_r("response $otp\n\n");

    if (count(str_split($otp)) == 4)
    {
      $code1 = str_split($otp)[0];
      $code2 = str_split($otp)[1];
      $code3 = str_split($otp)[2];
      $code4 = str_split($otp)[3];

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://tacticalpro.co.id/auth/otp/check',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'code_1='.$code1.'&code_2='.$code2.'&code_3='.$code3.'&code_4='.$code4,
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/x-www-form-urlencoded',
          'Cookie: '.$cookiesOut
        ),
      ));
      curl_exec($curl);

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://tacticalpro.co.id/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: '.$cookiesOut
        ),
      ));
      $response = curl_exec($curl);
      curl_close($curl);

      self::refresh_tacticalpro('KALSEL');
    }
  }

  public static function refresh_ibooster()
  {
    $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.62.165.58/home.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: ' . $ibooster->cookies . ''
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    dd($response);
  }

  private function rtwh_tactical($type)
  {
    $tacticalpro = DB::table('cookie_systems')->where('application', 'tacticalpro')->where('witel', 'KALSEL')->first();

    $start_filter = 'QXkyblNOSjdMNmc9'; // 2022-01
    $end_filter = 'NjE1Rys3U21xRjA9'; // 2023-12

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://tacticalpro.co.id/return_to_wh/get_detail',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'draw=2&columns%5B0%5D%5Bdata%5D=edit&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=no&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=reg&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=witel_ms2n&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=no_inet&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=nik_teknisi&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=nama_pelanggan&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=sn&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=vendor&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=tipe_dapros&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=false&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=flag&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=false&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=warehouse&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=false&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=source&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B13%5D%5Bdata%5D=generated_at&columns%5B13%5D%5Bname%5D=&columns%5B13%5D%5Bsearchable%5D=true&columns%5B13%5D%5Borderable%5D=false&columns%5B13%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B13%5D%5Bsearch%5D%5Bregex%5D=false&start=0&length=-1&search%5Bvalue%5D=&search%5Bregex%5D=false&start_filter=' . $start_filter . '&end_filter=' . $end_filter . '&id=TllxSGhmQkdUZnA0REdMMHVRZ29wdz09&type=' . $type . '&breakdown=SHJncnlhemZjVWM9',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $tacticalpro->cookies . ''
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($response);
    
    if (count($result->data) > 0)
    {
      DB::table('tacticalpro_rtwh')->where('type_rtwh', $type)->delete();

      foreach ($result->data as $value)
      {
        if ($value->nik_teknisi == ' ')
        {
          $nik_teknisi = null;
        } else {
          $nik_teknisi = $value->nik_teknisi;
        }
        $insert[] = [
          'nik_teknisi' => $nik_teknisi,
          'reg' => $value->reg,
          'witel_ms2n' => $value->witel_ms2n,
          'no_inet' => $value->no_inet,
          'nama_pelanggan' => $value->nama_pelanggan,
          'generated_at' => $value->generated_at,
          'flag' => $value->flag,
          'tipe_dapros' => $value->tipe_dapros,
          'rtwh' => $value->rtwh,
          'vendor' => $value->vendor,
          'created_by' => $value->created_by,
          'created_at' => $value->created_at,
          'sn' => $value->sn,
          'source' => $value->source,
          'warehouse' => $value->warehouse,
          'collected_source' => $value->collected_source,
          'type_rtwh' => $type
        ];
      }

      $total = count($insert);

      $chunk = array_chunk($insert, 500);

      foreach ($chunk as $numb => $data)
      {
        DB::table('tacticalpro_rtwh')->insert($data);

        print_r("saved page $numb and sleep (1)\n");

        sleep(1);
      }

      print_r("Done Download RTWH $type Total $total\n");
    }
  }

  public static function download_rtwh()
  {
    $that = new GrabController();

    $key = ['rtwh_total', 'uncollected_total'];

    foreach ($key as $value) {
      $that->rtwh_tactical($value);
    }

    print_r("Finish Download RTWH\n");
  }

  public static function workorderListPickupOnline()
  {
    $sto = ['AMT', 'BBR', 'BRR', 'BJM', 'BLC', 'BRI', 'BTB', 'GMB', 'KDG', 'KIP', 'KPL', 'KYG', 'LUL', 'MRB', 'MTP', 'NEG', 'PGN', 'PGT', 'PLE', 'RTA', 'SER', 'STI', 'TJL', 'TKI', 'ULI'];

    foreach ($sto as $value)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://tacticalpro.co.id/api/get/workorder/list',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('id_sto' => $value, 'source' => 'nonbackend'),
        CURLOPT_HTTPHEADER => array(
          'apisecret: SndaaFJwcDNXM2JBVkRaTG43WXdlZz09'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      print_r("$response");
    }
  }

  public static function all_sync_dns_warriors()
  {
    $get_witel = ['KALSEL', 'BALIKPAPAN'];
    foreach ($get_witel as $witel)
    {
      self::ontPremiumMigrasiStb('ont_premium', 'valid', $witel);
      sleep(5);
      self::ontPremiumMigrasiStb('ont_premium', 'ps', $witel);
      sleep(5);
      self::ontPremiumMigrasiStb('migrasi_stb', 'valid', $witel);
      sleep(5);
      self::ontPremiumMigrasiStb('migrasi_stb', 'ps', $witel);
      sleep(5);
    }
  }

  private static function ontPremiumMigrasiStb($order, $kode, $witel)
  {
    switch ($order) {
      case 'ont_premium':
        switch ($kode) {
          case 'valid':
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://dns.warriors.id/menu/dashboard_ont_premium_exp.php?kode='.$kode.'&reg=&witel='.$witel.'&no_inet=&add=&flag=',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $dom = @\DOMDocument::loadHTML(trim($response));
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            $columns = array(
              1 => 'reg', 'witel', 'nama_pelanggan', 'alamat_pelanggan', 'no_telp', 'no_internet', 'kw', 'type_ont', 'average_arpu', 'no_hp', 'jenis_dapros'
            );
            $result = array();
            for ($i = 1, $count = $rows->length; $i < $count; $i++) {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }

            if (count($result) > 0)
            {
              $total = count($result);

              DB::table('ont_premium_log')->where('kode', $kode)->where('witel', $witel)->delete();

              foreach ($result as $value)
              {
                $insert[] = [
                  'reg' => $value['reg'],
                  'witel' => $value['witel'],
                  'nama_pelanggan' => $value['nama_pelanggan'],
                  'alamat_pelanggan' => $value['alamat_pelanggan'],
                  'no_telp' => $value['no_telp'],
                  'no_internet' => $value['no_internet'],
                  'kw' => $value['kw'],
                  'type' => $value['type_ont'],
                  'average_arpu' => $value['average_arpu'],
                  'no_hp' => $value['no_hp'],
                  'jenis_dapros' => $value['jenis_dapros'],
                  'kode' => $kode
                ];
              }

              $chunk = array_chunk($insert, 500);
              foreach ($chunk as $numb => $data)
              {
                DB::table('ont_premium_log')->insert($data);

                print_r("saved page $numb and sleep (1)\n");

                sleep(1);
              }

              print_r("Finish Grab Order ONT Premium Kode $kode Witel $witel Total $total\n");
            }
            break;
          
          case 'ps':
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://dns.warriors.id/menu/dashboard_ont_premium_exp.php?kode='.$kode.'&reg=&witel='.$witel.'&no_inet=&add=&flag=',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $dom = @\DOMDocument::loadHTML(trim($response));
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            $columns = array(
              1 => 'wo_id', 'reg', 'witel', 'nama_pelanggan', 'alamat_pelanggan', 'no_telp', 'no_internet', 'ont_type_eksisting', 'sn_ont_eksisting', 'ont_type_baru', 'sn_ont_baru', 'type', 'helpdesk_approve', 'amcrew', 'nik_teknisi_1', 'nik_teknisi_2', 'no_hp', 'jenis_dapros', 'tanggal_usage'
            );
            $result = array();
            for ($i = 1, $count = $rows->length; $i < $count; $i++) {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }

            if (count($result) > 0)
            {
              $total = count($result);

              DB::table('ont_premium_log')->where('kode', $kode)->where('witel', $witel)->delete();

              foreach ($result as $value)
              {
                $insert[] = [
                  'wo_id' => $value['wo_id'],
                  'reg' => $value['reg'],
                  'witel' => $value['witel'],
                  'nama_pelanggan' => $value['nama_pelanggan'],
                  'alamat_pelanggan' => $value['alamat_pelanggan'],
                  'no_telp' => $value['no_telp'],
                  'no_internet' => $value['no_internet'],
                  'ont_type_eksisting' => $value['ont_type_eksisting'],
                  'sn_ont_eksisting' => $value['sn_ont_eksisting'],
                  'ont_type_baru' => $value['ont_type_baru'],
                  'sn_ont_baru' => $value['sn_ont_baru'],
                  'type' => $value['type'],
                  'helpdesk_approve' => $value['helpdesk_approve'],
                  'amcrew' => $value['amcrew'],
                  'nik_teknisi_1' => $value['nik_teknisi_1'],
                  'nik_teknisi_2' => $value['nik_teknisi_2'],
                  'no_hp' => $value['no_hp'],
                  'jenis_dapros' => $value['jenis_dapros'],
                  'tanggal_usage' => $value['tanggal_usage'],
                  'kode' => $kode
                ];
              }

              $chunk = array_chunk($insert, 500);
              foreach ($chunk as $numb => $data)
              {
                DB::table('ont_premium_log')->insert($data);

                print_r("saved page $numb and sleep (1)\n");

                sleep(1);
              }

              print_r("Finish Grab Order ONT Premium Kode $kode Witel $witel Total $total\n");
            }
            break;
        }
        break;
      
      case 'migrasi_stb':
        switch ($kode) {
          case 'valid':
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://dns.warriors.id/menu/dashboard_stb_premium_exp.php?kode='.$kode.'&witel='.$witel,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $dom = @\DOMDocument::loadHTML(trim($response));
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            $columns = array(
              1 => 'reg', 'witel', 'sto', 'no_internet', 'nd_pots', 'mac_address', 'nama_pelanggan', 'alamat_pelanggan', 'no_hp_pelanggan'
            );
            $result = array();
            for ($i = 1, $count = $rows->length; $i < $count; $i++) {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }

            if (count($result) > 0)
            {
              $total = count($result);

              DB::table('migrasi_stb_premium_log')->where('kode', $kode)->where('witel', $witel)->delete();

              foreach ($result as $value)
              {
                $insert[] = [
                  'reg' => $value['reg'],
                  'witel' => $value['witel'],
                  'sto' => $value['sto'],
                  'no_internet' => $value['no_internet'],
                  'nd_pots' => $value['nd_pots'],
                  'mac_address' => $value['mac_address'],
                  'nama_pelanggan' => $value['nama_pelanggan'],
                  'alamat_pelanggan' => $value['alamat_pelanggan'],
                  'no_hp_pelanggan' => $value['no_hp_pelanggan'],
                  'kode' => $kode
                ];
              }

              $chunk = array_chunk($insert, 500);
              foreach ($chunk as $numb => $data)
              {
                DB::table('migrasi_stb_premium_log')->insert($data);

                print_r("saved page $numb and sleep (1)\n");

                sleep(1);
              }

              print_r("Finish Grab Order Migrasi STB Premium Kode $kode Witel $witel Total $total\n");
            }
            break;
          
          case 'ps':
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://dns.warriors.id/menu/dashboard_stb_premium_exp.php?kode='.$kode.'&reg=&witel='.$witel.'&no_inet=&add=',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $dom = @\DOMDocument::loadHTML(trim($response));
            $table = $dom->getElementsByTagName('table')->item(0);
            $rows = $table->getElementsByTagName('tr');
            $columns = array(
              1 => 'wo_id' , 'reg', 'witel', 'sto', 'no_internet', 'nd_pots', 'mac_address', 'nama_pelanggan', 'alamat_pelanggan', 'no_hp_pelanggan', 'helpdesk_assign', 'amcrew', 'nik_teknisi_1', 'nik_teknisi_2', 'merk', 'stb_id', 'mac_address_baru', 'tanggal_assign', 'tanggal_visited', 'tanggal_usage'
            );
            $result = array();
            for ($i = 1, $count = $rows->length; $i < $count; $i++) {
              $cells = $rows->item($i)->getElementsByTagName('td');
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
                $td = $cells->item($j);
                $data[$columns[$j]] =  $td->nodeValue;
              }
              $result[] = $data;
            }

            if (count($result) > 0)
            {
              $total = count($result);

              DB::table('migrasi_stb_premium_log')->where('kode', $kode)->where('witel', $witel)->delete();

              foreach ($result as $value)
              {
                $insert[] = [
                  'wo_id' => $value['wo_id'],
                  'reg' => $value['reg'],
                  'witel' => $value['witel'],
                  'sto' => $value['sto'],
                  'no_internet' => $value['no_internet'],
                  'nd_pots' => $value['nd_pots'],
                  'mac_address' => $value['mac_address'],
                  'nama_pelanggan' => $value['nama_pelanggan'],
                  'alamat_pelanggan' => $value['alamat_pelanggan'],
                  'no_hp_pelanggan' => $value['no_hp_pelanggan'],
                  'helpdesk_assign' => $value['helpdesk_assign'],
                  'amcrew' => $value['amcrew'],
                  'nik_teknisi_1' => $value['nik_teknisi_1'],
                  'nik_teknisi_2' => $value['nik_teknisi_2'],
                  'merk' => $value['merk'],
                  'stb_id' => $value['stb_id'],
                  'mac_address_baru' => $value['mac_address_baru'],
                  'tanggal_assign' => $value['tanggal_assign'],
                  'tanggal_visited' => $value['tanggal_visited'],
                  'tanggal_usage' => $value['tanggal_usage'],
                  'kode' => $kode
                ];
              }

              $chunk = array_chunk($insert, 500);
              foreach ($chunk as $numb => $data)
              {
                DB::table('migrasi_stb_premium_log')->insert($data);

                print_r("saved page $numb and sleep (1)\n");

                sleep(1);
              }

              print_r("Finish Grab Order Migrasi STB Premium Kode $kode Witel $witel Total $total\n");
            }
            break;
        }
        break;
    }
  }

  public static function valins_psb($witel)
  {
    $periode = date('F-Y');

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://access-quality.telkom.co.id/valdat2022/export_detail.php?treg=&witel='.$witel.'&sto=&code=valins_psb&filter='.$periode,
      CURLOPT_SSL_VERIFYHOST => 0,
      CURLOPT_SSL_VERIFYPEER => 0,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    
    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
      1 =>
      'treg','witel','sto','odp_name','panel_odp','port_odp','valins_odp','node_id','node_ip','slot','port','onu_id','onu_detected_at','onu_sn','no_inet_valins','sip1_valins','sip2_valins','no_inet_discovery','sip1_discovery','sip2_discovery','qr_code_dc','info','info_discovery','tgl_valins','valins_type','stp_target','stp_port','sp_target','sp_port','ticket_id','amcrew','nama_teknisi'
    );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $data['periode'] = $periode;
      $result[] = $data;
    }

    $total = count($result);

    if ($total > 0)
    {
      DB::table('validasi_core_valins_psb')->where('periode', $periode)->delete();

      $srcarr = array_chunk($result, 500);
      foreach ($srcarr as $numb => $item)
      {
        DB::table('validasi_core_valins_psb')->insert($item);

        print_r("saved page $numb and sleep (1)\n");

        sleep(1);
      }

      print_r("Success Grab Validasi Core Valins Total $total\n");
    }
  }

  public static function absen_all_naker_hrmista()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.telkomakses.co.id/API/absen/get_all_data_absen.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($response);

    DB::table('absen_hrmista')->where('present_dat', date('Y-m-d'))->delete();

    foreach ($result->data as $key => $value)
    {
      $insert[] = [
        'nik' => $value->nik,
        'present_dat' => $value->present_dat,
        'absen_masuk' => $value->absen_masuk
      ];
    }

    $arraychunk = array_chunk($insert, 500);
    foreach ($arraychunk as $numb => $item)
    {
      DB::table('absen_hrmista')->insert($item);

      print_r("saved page $numb and sleep (1)\n");

      sleep(1);
    }

    print_r("Success Grab Absen All Naker HRMISTA\n");
  }

  public static function detail_utonline()
  {
    $details_id = DB::table('find_sc_latlon')->where('ut_order_id', '!=', 0)->get();

    $utonline = DB::table('cookie_systems')->where('application', 'utonline')->where('witel', 'KALSEL')->first();

    foreach ($details_id as $value)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://utonline.telkom.co.id/evidence/order/detail?id='.$value->ut_order_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: '.$utonline->cookies
        ),
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      $dom = @\DOMDocument::loadHTML(trim($response));
      $finder = new \DomXPath($dom);

      $class_koordinat = "col-sm-9 text-secondary text-semi-bold text-right";
      $class_foto = "css-18c4yhp";

      $spaner1 = $finder->query("//*[contains(@class, '$class_koordinat')]");
      
      $koordinat_pelanggan = explode(',', str_replace(array('Koordinat:', 'http://maps.google.com/?q=',' '), '', $spaner1->item(10)->nodeValue));
      $lat_pelanggan = @$koordinat_pelanggan[0];
      $long_pelanggan = @$koordinat_pelanggan[1];

      $koordinat_odp = explode(',', str_replace(array('Koordinat:', 'http://maps.google.com/?q=', ' '), '', $spaner1->item(21)->nodeValue));
      $lat_odp = @$koordinat_odp[0];
      $long_odp = @$koordinat_odp[1];

      $spaner2 = $finder->query("//*[contains(@class, '$class_foto')]");

      $rumah_pelanggan = explode(',', str_replace(array('Koordinat:', 'http://maps.google.com/?q=',' '), '', $spaner2->item(4)->nodeValue));
      $lat_rumah_pelanggan = @$rumah_pelanggan[0];
      $long_rumah_pelanggan = @$rumah_pelanggan[1];

      $depan_odp = explode(',', str_replace(array('Koordinat:', 'http://maps.google.com/?q=', ' '), '', $spaner2->item(12)->nodeValue));
      $lat_depan_odp = @$depan_odp[0];
      $long_depan_odp = @$depan_odp[1];

      DB::table('find_sc_latlon')->where('ut_order_id', $value->ut_order_id)->update([
        'ut_lat_pelanggan'     => $lat_pelanggan,
        'ut_long_pelanggan'    => $long_pelanggan,
        'ut_lat_odp'           => $lat_odp,
        'ut_long_odp'          => $long_odp,
        'lat_rumah_pelanggan'  => $lat_rumah_pelanggan,
        'long_rumah_pelanggan' => $long_rumah_pelanggan,
        'lat_depan_odp'        => $lat_depan_odp,
        'long_depan_odp'       => $long_depan_odp
      ]);

      print_r("done $value->ut_order_id \n\n");
    }

    print_r("done all");
  }

  public static function scbe_hsi_xpro($witel)
  {
    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://xpro.telkom.co.id/hsi/downloadreportscbe.php?tipe=TOT_OGP&prev=scbe.php&treg=6&witel='.$witel.'&mode=STO&unit=all&psb=NEW+SALES&play=all&category=&package=all&deposit=&channel=&hari=ALL',
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');

    $columns = array(1 => 'regional','witel','datel','sto','order_id','type_transaksi','jenis_layanan','alpro','ncli','pots','internet','status_resume','status_message','order_date','last_update_status','durasi_v1','nama_cust','no_hp','alamat','kcontact','long','lat','wfm_id','status_wfm','desk_task','status_task','tgl_install','amcrew','teknisi1','personid','teknisi2','personid2','tindak_lanjut','keterangan','user','tgl_tindak_lanjut','package_name','estimasi_dropcore','provider');

    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $data['durasi_v1'] = self::datediff($data['order_date']);
      $data['tgl_install'] = date('Y-m-d H:i:s', strtotime($data['tgl_install']));
      $data['tgl_tindak_lanjut'] = date('Y-m-d H:i:s', strtotime($data['tgl_tindak_lanjut']));
      $result[] = str_replace(array("\r\n", "\t\t\t"), "", $data);
    }

    if (!empty($result))
    {
      $total = count($result);

      foreach ($result as $v)
      {
        $insert[] = [
          'regional' => $v['regional'],
          'witel' => $v['witel'],
          'datel' => $v['datel'],
          'sto' => $v['sto'],
          'order_id' => $v['order_id'],
          'type_transaksi' => $v['type_transaksi'],
          'jenis_layanan' => $v['jenis_layanan'],
          'alpro' => $v['alpro'],
          'ncli' => $v['ncli'],
          'pots' => $v['pots'],
          'internet' => $v['internet'],
          'status_resume' => $v['status_resume'],
          'status_message' => $v['status_message'],
          'order_date' => $v['order_date'],
          'last_update_status' => $v['last_update_status'],
          'durasi_v1' => $v['durasi_v1'],
          'nama_cust' => $v['nama_cust'],
          'no_hp' => $v['no_hp'],
          'alamat' => $v['alamat'],
          'kcontact' => $v['kcontact'],
          'long' => $v['long'],
          'lat' => $v['lat'],
          'wfm_id' => $v['wfm_id'],
          'status_wfm' => $v['status_wfm'],
          'desk_task' => $v['desk_task'],
          'status_task' => $v['status_task'],
          'tgl_install' => $v['tgl_install'],
          'amcrew' => $v['amcrew'],
          'teknisi1' => $v['teknisi1'],
          'personid' => $v['personid'],
          'teknisi2' => $v['teknisi2'],
          'personid2' => $v['personid2'],
          'tindak_lanjut' => $v['tindak_lanjut'],
          'keterangan' => $v['keterangan'],
          'user' => $v['user'],
          'tgl_tindak_lanjut' => $v['tgl_tindak_lanjut'],
          'package_name' => $v['package_name'],
          'estimasi_dropcore' => $v['estimasi_dropcore'],
          'provider' => $v['provider']
        ];
      }

      DB::table('scbe_hsi_xpro')->truncate();

      $srcarr = array_chunk($insert, 500);
      foreach ($srcarr as $numb => $item)
      {
        DB::table('scbe_hsi_xpro')->insert($item);

        print_r("saved page $numb and sleep (1)\n");

        sleep(1);
      }

      print_r("Success Grab SCBE HSI X-PRO Witel $witel Total $total\n");
    }
  }

  public static function datediff($dtm)
  {
    $now = new \DateTime(date('Y-m-d H:i:s'));
    $then = new \DateTime(date('Y-m-d H:i:s', strtotime($dtm)));
    
    return $now->diff($then)->days.' Hari';
  }

  public static function datin_ogp_xpro()
  {
    $startdate = date('Y-m-01');
    $enddate = date('Y-m-d');

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://xpro.telkom.co.id/downloadreporttask.php?tipe=ttl&prev=durasi.php&reg=REG-6&witel=KALSEL+(BANJARMASIN)&viewproduk=all&produk[]=all&status=&approval=&order=all&startdate='.$startdate.'&end='.$enddate.'&ordertype[]=New+Install',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');

    $columns = array(1 => 'osm_orderid', 'sc_ncx', 'sc_ncx_status', 'osm_status', 'osm_task', 'wfm_orderid', 'wonum', 'description', 'wfm_status', 'wfm_approval', 'ownergroup', 'schedstart', 'ncx_orderid', 'oh_status', 'mile', 'li_status', 'productname', 'order_type', 'order_date', 'date_provcomp', 'ttd', 'schedstart', 'customer_name', 'segment', 'service_region', 'service_witel', 'sto', 'sto_name', 'address', 'total_scaling', 'nama_am', 'ticket_fo', 'status_tl', 'keterangan_tl', 'user_tl', 'tgl_tl', 'li_sid', 'worklog_create_by', 'worklog_create_date', 'tgl_masuk_wfm', 'log_description', 'log_detail_description', 'status_jt', 'ncx_reason', 'tti_kontak_berlangganan', 'target_nas', 'target_treg', 'target_witel', 'target_dso', 'target_dit', 'target_dss');

    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $data['schedstart'] = date('Y-m-d H:i:s', strtotime($data['schedstart']));
      $data['order_date'] = date('Y-m-d H:i:s', strtotime($data['order_date']));
      $result[] = str_replace(array("\r\n", "\t\t\t"), "", $data);
    }

    dd($result);
  }

  public static function wms_egbis_ogp_xpro()
  {
    $startdate = date('Y-m-01');
    $enddate = date('Y-m-d');

    ini_set('memory_limit', '-1');
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://xpro.telkom.co.id/wifi/downloadreporttask.php?tipe=ttl&prev=durasi.php&reg=REG-6&witel=KALSEL+(BANJARMASIN)&status=&approval=&produk=all&startdate='.$startdate.'&end='.$enddate.'&ordertype[]=New+Install&cfutype[]=DBS&cfutype[]=DES&cfutype[]=DGS&cfutype[]=UNSEGMENTED&sourcetype[]=all&sort=order',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');

    $columns = array(1 => 'osm_orderid', 'osm_status', 'osm_task', 'wfm_orderid', 'description', 'wfm_status', 'wfm_approval', 'ownergroup', 'schedstart', 'ncx_orderid', 'jml_ap', 'oh_status', 'mile', 'ncx_status', 'productname', 'order_type', 'order_date', 'date_provcomp', 'ttd', 'customer_name', 'segment', 'service_region', 'service_witel', 'sto', 'sto_name', 'address', 'total_scaling', 'nama_am', 'ticket_fo', 'status_tl', 'keterangan_tl', 'user_tl', 'tgl_tl', 'sid');

    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++) {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
        $td = $cells->item($j);
        $data[$columns[$j]] =  $td->nodeValue;
      }
      $result[] = str_replace(array("\r\n", "\t\t\t"), "", $data);
    }

    dd($result);
  }
}