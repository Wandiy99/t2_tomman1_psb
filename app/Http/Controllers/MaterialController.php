<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use DB;
use Telegram;
use Validator;
use App\DA\HomeModel;
use App\DA\MaterialModel;
class MaterialController extends Controller
{
  public function home($periode, Request $req){
    $auth = session('auth');
    $nik = $auth->id_user;
    $group_telegram = HomeModel::group_telegram($nik);
    $get_list_TL = MaterialModel::list_TL($periode,$nik);

    $periode = '';
    $sektorPilih = '-1001318576095';
    if ($req->has('date')){
    	$periode = $req->input('date');
        $sektorPilih  = $req->input('sektor');

    	$get_list_TL = MaterialModel::listMaterial($periode,$sektorPilih);
    };

    $sektor = DB::select('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_sektor=1');
    return view('material.home',compact('group_telegram','nik','get_list_TL', 'nik', 'sektor', 'auth', 'periode', 'sektorPilih'));
  }
  public function dashboard()
  {
    $program = Input::get('program');
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    $query = MaterialModel::get_material_periode($program,$start,$end);
    // dd($query);
    
    return view('material.dashboard',compact('program','start','end','query'));
  }

  public function dashboardDetail()
  {
    $sektor = Input::get('sektor');
    $order = Input::get('order');
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    $query = MaterialModel::dashboardDetail($sektor,$order,$start,$end);
    // dd($query);

    return view('material.dashboardDetail',compact('sektor','order','start','end','query'));
  }
}
?>
