<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\tmpodpModel;
use DB;

class importController extends Controller
{
    public function importCsv(){
    	  	if (($handle = fopen (public_path().'/tmpodp.csv','r')) ) {
        	  while ( ($data = fgetcsv ($handle, 1000, ',')) ) {
        		$csv = new tmpodpModel();
        		$csv->divre 			 = $data[1];
        		$csv->witel 			 = $data[2];
        		$csv->cmdf 				 = $data[3];
        		$csv->rk 				 = $data[4];
        		$csv->dp 				 = $data[5];
        		$csv->no_speedy 		 = $data[6];
        		$csv->node_id			 = $data[7];
        		$csv->node_ip			 = $data[8];
        		$csv->slot 				 = $data[9];
        		$csv->port 				 = $data[10];
        		$csv->onu 				 = $data[11];
        		$csv->onu_desc 			 = $data[12];
        		$csv->onu_type 			 = $data[13];
        		$csv->onu_sn 			 = $data[14];
        		$csv->fiber_length		 = $data[15];
        		$csv->olt_rx_power 		 = $data[16];
        		$csv->olt_rx_power_akhir = $data[17];
        		$csv->onu_rx_power 		 = $data[18];
        		$csv->onu_rx_power_akhir = $data[19];
        		$csv->tgl_ukur_akhir 	 = $data[20];
        		$csv->status 			 = $data[21];
        		$csv->alamat 			 = $data[22];
        		$csv->status_warranty 	 = $data[23];
        		$csv->tgl_pelaksanaan 	 = $data[24];
        		$csv->is_cabut 			 = $data[25];	
        		$csv->is_kw1 			 = $data[26];
        		$csv->save();
          	}
          	fclose($handle);
     	}

     	DB::table('tmpodp')->where('divre','=','0')->delete();
     	echo "Creating Query has Finish !";
    }
}
