<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use Storage;
Use App\DA\BackupModel;

class BackupController extends Controller
{
  function run($periode){
    $query = BackupModel::get_backup($periode);
    foreach ($query as $result) :
      echo substr($result->Ndem,0,2);
      if (substr($result->Ndem,0,2)=="IN") {
        $folder = "asurance";
        @$this->get_folder($result->id,$folder);
      } elseif (substr($result->Ndem,0,2)=="DP") {
        $folder = "skip/";
      } else {
        $folder = "evidence";
        @$this->get_folder($result->id,$folder);
      }
      echo $folder."/".$result->id." // ".$result->tgl."<br />";
    endforeach;
  }
  function get_folder($id,$folder){
    $get_folder_content = Storage::disk('public')->files('/upload/'.$folder.'/'.$id.'/');
    foreach ($get_folder_content as $filename) :
      $file_local = Storage::disk('public')->get($filename);
      $file_ftp = Storage::disk('ftp')->put($filename, $file_local);
    endforeach;
  }
  function run_delete($periode){
    $query = BackupModel::get_delete($periode);
    foreach ($query as $result){
      echo substr($result->Ndem,0,2);
      if (substr($result->Ndem,0,2)=="IN") {
        $folder = "asurance";
        @$this->get_delete($result->id,$folder);
      } elseif (substr($result->Ndem,0,2)=="DP") {
        $folder = "skip/";
      } else {
        $folder = "evidence";
        @$this->get_delete($result->id,$folder);
      }
      echo $folder."/".$result->id." // ".$result->tgl."<br />";
    }
  }
  function get_delete($id,$folder){
    $link = public_path().'/upload/'.$folder.'/'.$id;
    echo $link."<br />";
    // $delte = unlink($link);
    \File::deleteDirectory($link);
    echo "ok";
  }
}
