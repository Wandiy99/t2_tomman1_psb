<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use File;
use DateTime;
use Telegram;
use App\DA\GrabModel;
use App\DA\DashboardModel;
use App\DA\DshrModel;
use TG;
date_default_timezone_set('Asia/Makassar');

class GrandPrizeController extends Controller
{

  public function gathering_q1($hadiah)
  {
    if ($hadiah == 'Topi_Kaos_Kaki')
    {
      $img = '/image/gathering_q1/Topi_Kaos_Kaki.png';
      $judul = 'TOPI & KAOS KAKI';
    } elseif ($hadiah == 'Tas_Ransel')  {
      $img = '/image/gathering_q1/Tas_Ransel.png';
      $judul = 'TAS RANSEL';
    } elseif ($hadiah == 'Uang_Tunai') {
      $img = '/image/gathering_q1/Uang_Tunai.png';
      $judul = 'UANG TUNAI';
    } elseif ($hadiah == 'Voucher_Indomaret') {
      $img = '/image/gathering_q1/Voucher_Indomaret.png';
      $judul = 'VOUCHER INDOMARET';
    } elseif ($hadiah == 'Powerbank') {
      $img = '/image/gathering_q1/Powerbank.png';
      $judul = 'POWERBANK';
    } elseif ($hadiah == 'Tumbler_Eiger') {
      $img = '/image/gathering_q1/Tumbler_Eiger.png';
      $judul = 'TUMBLER EIGER';
    } elseif ($hadiah == 'Speaker_Bluetooth') {
      $img = '/image/gathering_q1/Speaker_Bluetooth.png';
      $judul = 'SPEAKER BLUETOOTH';
    } elseif ($hadiah == 'Kipas_Angin') {
      $img = '/image/gathering_q1/Kipas_Angin.png';
      $judul = 'KIPAS ANGIN';
    } elseif ($hadiah == 'Jam_Tangan') {
      $img = '/image/gathering_q1/Jam_Tangan.png';
      $judul = 'JAM TANGAN';
    } elseif ($hadiah == 'Topi_Hush_Puppies') {
      $img = '/image/gathering_q1/Topi_Hush_Puppies.png';
      $judul = 'TOPI HUSH PUPPIES';
    } elseif ($hadiah == 'Kerudung_Ayu_Lestari') {
      $img = '/image/gathering_q1/Kerudung_Ayu_Lestari.png';
      $judul = 'KERUDUNG AYU LESTARI';
    } elseif ($hadiah == 'TV') {
      $img = '/image/gathering_q1/tv.png';
      $judul = 'TV 32"';
    } elseif ($hadiah == 'all') {
      $img = '/image/gathering_q1/error.png';
      $judul = 'PILIH HADIAH!';
    } else {
      $img = '/image/gathering_q1/error.png';
      $judul = 'ERROR! REFRESH';
    }

    $sve_data = [];
    $no = 0;

    if (in_array($hadiah, ["Tas_Ransel", "Topi_Hush_Puppies"]))
    {
      $query = DB::table('gathering_q1_2023')->where([
        ['mitra', 'PT TELKOM AKSES'],
        ['total_kupon', 1],
        ['jk', 'Laki-Laki']
      ])
      ->groupBy('nik')
      ->get();
    } elseif ($hadiah == "Kerudung_Ayu_Lestari") {
      $query = DB::table('gathering_q1_2023')->where([
        ['mitra', 'PT TELKOM AKSES'],
        ['total_kupon', 1],
        ['jk', 'Perempuan']
      ])
      ->groupBy('nik')
      ->get();
    } else {
      $query = DB::table('gathering_q1_2023')->where([
        ['mitra', 'PT TELKOM AKSES'],
        ['total_kupon', 1]
      ])
      ->groupBy('nik')
      ->get();
    }
    foreach($query as $v)
    {
      if ($v->total_kupon > 0 && $v->total_kupon != $v->kupon_terpakai)
      {
        for ($i = 0; ($v->total_kupon - $v->kupon_terpakai) > $i; ++$i)
        {
          $numb = ++$no;
          $sve_data[$numb]['nik'] = $v->nik;
          $sve_data[$numb]['nama'] = $v->nama;
        }
      }
    }

    return view('tool.gathering_q1', compact('hadiah', 'img', 'judul', 'query', 'sve_data', 'numb'));
  }

  public function gathering_q1_save($nik)
  {
    $expl = explode(',', $nik);

    foreach (array_filter($expl) as $value)
    {
      DB::table('gathering_q1_2023')->where('nik', $value)->update([
        'total_kupon'    => 0,
        'kupon_terpakai' => 1
      ]);
    }    

    return redirect('/grandprize/gathering_q1/all')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>Selamat</strong> Kepada Pemenang Dengan NIK '.$nik]
    ]);
  }

  public function undian($jenis_undian, $hadiah)
  {
    // dd($jenis_undian, $kupon, $hadiah);

    if ($hadiah == 'TABLET')
    {
      $img = '/image/grandprize/samsung tablet.png';
      $judul = 'SAMSUNG TABLET';
    } elseif ($hadiah == 'SMARTPHONE')  {
      $img = '/image/grandprize/smartphone.png';
      $judul = 'SMARTPHONE';
    } elseif ($hadiah == 'POWERBANK') {
      $img = '/image/grandprize/powerbank.png';
      $judul = 'POWER BANK';
    } elseif ($hadiah == 'LAPTOP') {
      $img = '/image/grandprize/laptop hp.png';
      $judul = 'LAPTOP';
    } elseif ($hadiah == 'SEPEDA') {
      $img = '/image/grandprize/sepeda gunung.png';
      $judul = 'SEPEDA GUNUNG';
    } elseif ($hadiah == 'TAS') {
      $img = '/image/grandprize/tas hitam.png';
      $judul = 'TAS EXCLUSIVE';
    } elseif ($hadiah == 'MOTOR') {
      $img = '/image/grandprize/honda scoopy.png';
      $judul = 'SEPEDA MOTOR';
    } elseif ($hadiah == 'all') {
      $img = '';
      $judul = 'PILIH HADIAH!';
    } else {
      $img = '';
      $judul = 'ERROR! REFRESH';
    }

    $sve_data = [];
    $no = 0;

    if(strcasecmp($jenis_undian, 'sobi') == 0)
    {
      $query = DB::table('grandprize_sf_2022')->get();
      foreach($query as $v)
      {
        if ($v->total_kupon > 0 && $v->total_kupon != $v->kupon_terpakai)
        {
          for ($i = 0; ($v->total_kupon - $v->kupon_terpakai) > $i; ++$i)
          {
            $numb = ++$no;
            $sve_data[$numb]['id'] = $v->id;
            $sve_data[$numb]['nama'] = $v->nama;
            $sve_data[$numb]['partner'] = $v->kodepartner;
            $sve_data[$numb]['jenis'] = 'sobi';
          }
        }
      }
    } elseif (strcasecmp($jenis_undian, 'grandprize_citizen') == 0) {
        $query = DB::table('grandprize_sales_2022')->where('kupon_citizen', '!=', 0)->get();
        foreach ($query as $v) {
          if ($v->kupon_citizen > 0 && $v->kupon_citizen != $v->kupon_citizen_terpakai) {
            for ($i = 0; ($v->kupon_citizen - $v->kupon_citizen_terpakai) > $i; ++$i) {
              $numb = ++$no;
              $sve_data[$numb]['id'] = $v->id;
              $sve_data[$numb]['nama'] = $v->nama_sales;
              $sve_data[$numb]['partner'] = $v->nama_sales;
              $sve_data[$numb]['jenis'] = 'grandprize_citizen';
            }
          }
        }
    } elseif (strcasecmp($jenis_undian, 'grandprize_vip') == 0) {
      $query = DB::table('grandprize_sales_2022')->where('kupon_vip', '!=', 0)->get();
      foreach ($query as $v) {
        if ($v->kupon_vip > 0 && $v->kupon_vip != $v->kupon_vip_terpakai) {
          for ($i = 0; ($v->kupon_vip - $v->kupon_vip_terpakai) > $i; ++$i) {
            $numb = ++$no;
            $sve_data[$numb]['id'] = $v->id;
            $sve_data[$numb]['nama'] = $v->nama_sales;
            $sve_data[$numb]['partner'] = $v->nama_sales;
            $sve_data[$numb]['jenis'] = 'grandprize_vip';
          }
        }
      }
    } elseif (strcasecmp($jenis_undian, 'bukber_2022') == 0) {
      $query = DB::table('grandprize_bukber_2022')->get();
      foreach ($query as $v) {
        if ($v->kupon_jml > 0 && $v->kupon_jml != $v->kupon_terpakai) {
          for ($i = 0; ($v->kupon_jml - $v->kupon_terpakai) > $i; ++$i) {
            $numb = ++$no;
            $sve_data[$numb]['id'] = $v->id;
            $sve_data[$numb]['nama'] = $v->nama;
            $sve_data[$numb]['partner'] = $v->partner;
            $sve_data[$numb]['jenis'] = 'bukber_2022';
          }
        }
      }
    }

    // $query = DB::SELECT('select * from redeem_point_tech where status_ticket = "'.$type.'" and winner IS NULL order by rand() limit 1');

    return view('psb.undian',compact('sve_data','query', 'jenis_undian', 'img', 'judul'));
  }

  public function updateGrandPrize($jenis, $id)
  {
    $exp_id = explode(',', $id);
    $kodepart = [];

    if (strcasecmp($jenis, 'sobi') == 0)
    {
      foreach ($exp_id as $v) {
        $data = DB::table('grandprize_sf_2022')->where('id', $v)->first();
        $kupon = $data->kupon_terpakai + 1;
        if (count($data) > 0) {
          DB::table('grandprize_sf_2022')->where('id', $v)->update([
            'kupon_terpakai' => $kupon
          ]);
          $kodepart[] = $data->kodepartner;
        }
      }
    } elseif (strcasecmp($jenis, 'grandprize_citizen') == 0) {
      foreach ($exp_id as $v) {
        $data = DB::table('grandprize_sales_2022')->where('id', $v)->first();
        $kupon = $data->kupon_citizen_terpakai + 1;
        if (count($data) > 0) {
          DB::table('grandprize_sales_2022')->where('id', $v)->update([
            'kupon_citizen_terpakai' => $kupon
          ]);
          $kodepart[] = $data->nama_sales;
        }
      }
    } elseif (strcasecmp($jenis, 'grandprize_vip') == 0) {
      foreach ($exp_id as $v) {
        $data = DB::table('grandprize_sales_2022')->where('id', $v)->first();
        $kupon = $data->kupon_vip_terpakai + 1;
        if (count($data) > 0) {
          DB::table('grandprize_sales_2022')->where('id', $v)->update([
            'kupon_vip_terpakai' => $kupon
          ]);
          $kodepart[] = $data->nama_sales;
        }
      }
    } elseif (strcasecmp($jenis, 'bukber_2022') == 0) {
      foreach ($exp_id as $v) {
        $data = DB::table('grandprize_bukber_2022')->where('id', $v)->first();
        $kupon = $data->kupon_terpakai + 1;
        if (count($data) > 0) {
          DB::table('grandprize_bukber_2022')->where('id', $v)->update([
            'kupon_terpakai' => $kupon
          ]);
          $kodepart[] = $data->nama;
        }
      }
    }

    $kodepart = implode(', ', $kodepart);

    return redirect('/undian/'.$jenis.'/all')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>Selamat</strong> kepada '.$kodepart.' dengan ID '.$v.'']
    ]);
  }

  public function pointek(){
    // $query = DB::table('racing_poin')->where('NIK',session('auth')->id_karyawan)->get();
    $result = DB::table('prov_point_tech')->leftJoin('1_2_employee', 'prov_point_tech.tech_nik','=', '1_2_employee.nik')->where('tech_nik', session('auth')->id_karyawan)->first();
    $query_voucher_vip = DB::table('redeem_point_tech')->where('status_ticket', 'VIP')->where('nik_tech', session('auth')->id_karyawan)->get();
    $query_voucher_reguler = DB::table('redeem_point_tech')->where('status_ticket', 'REGULER')->where('nik_tech', session('auth')->id_karyawan)->get();
    // dd($query_point , $query_voucher_vip, $query_voucher_reguler);

    return view('psb.pointek',compact('result', 'query_voucher_vip', 'query_voucher_reguler'));
  }

  public function home(){
    $date = '2021-07-26';
    $dateend = date('Y-m-d');
    $query = DB::SELECT('
      SELECT
      e.uraian,
      e.mitra,
      g.title,
      (SELECT COUNT(*) FROM prabac_tr6 pt LEFT JOIN dispatch_teknisi dt2 ON pt.SC_ID_INT = dt2.Ndem WHERE dt2.id_regu = e.id_regu AND (DATE(dt2.tgl) BETWEEN "'.$date.'" AND "'.$dateend.'") ) as jumlah_ffg,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps1 LEFT JOIN dispatch_teknisi dt1 ON dps1.orderId = dt1.Ndem LEFT JOIN psb_laporan pl1 ON dt1.id = pl1.id_tbl_mj WHERE pl1.status_laporan = 1 AND dt1.id_regu = e.id_regu AND dps1.jenisPsb LIKE "MO%" AND (DATE(dt1.tgl) BETWEEN "'.$date.'" AND "'.$dateend.'") ) as jumlah_mo,
      SUM(CASE WHEN (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'") AND qbt.kategori IN ("LOLOS","VALID1_ASO","VALID1_DAMAN","VALID1_ROCDAMAN","HS","VALID1_AMO") THEN 1 ELSE 0 END) as jml_order,
      SUM(CASE WHEN (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'") AND qbt.tag_unit LIKE "%ASO%" THEN 1 ELSE 0 END) as tidak_lolos,
      SUM(CASE WHEN (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$dateend.'") AND qbt.kategori = "LOLOS" THEN 1 ELSE 0 END) as lolos
      FROM
        regu e
        LEFT JOIN dispatch_teknisi b ON e.id_regu = b.id_regu
        LEFT JOIN qc_borneo_tr6 qbt ON qbt.sc = b.NO_ORDER
        LEFT JOIN group_telegram g On e.mainsector = g.chat_id

      WHERE
1
      GROUP BY e.uraian
      ORDER BY lolos DESC
    ');
    return view('dashboard.grandprize',compact('query','date','dateend'));
  }
}

?>
