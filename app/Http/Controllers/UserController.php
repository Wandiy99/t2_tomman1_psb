<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
class UserController extends Controller
{
  public static function get_karyawan()
  {
    return DB::table('1_2_employee')->select('nik as id, nama as text')->where('ACTIVE', 1)->where('Witel_New', session('auth')->nama_witel)->get();
  }

  public static function get_witel()
  {
    return DB::table('witel')->select('id_witel as id', 'nama_witel as text')->where('active', 1)->orderBy('urut', 'ASC')->get();
  }

  public static function get_level()
  {
    return DB::table('level')->select('id_level as id', 'nama as text')->where('active', 0)->orderBy('nama', 'ASC')->get();
  }

  public function index()
  {

    $auth = session('auth');
    if ($auth->level=='2' || ($auth->level=='46' && $auth->id_user=='89150015')) {
    $list = DB::select('
      SELECT
        u.id_user, u.id_karyawan, k.nama,l.nama as level
      FROM (user u
      LEFT JOIN
        1_2_employee k
        ON u.id_karyawan = k.nik)
      LEFT JOIN
        level l
        ON u.level=l.id_level
        left join witel w ON u.witel = w.id_witel
      WHERE
        k.ACTIVE = 1 AND
        w.nama_witel = "'.session('witel').'"
    ');

    return view('user.list', compact('list'));
    } else {
      echo "NOT ALLOWED!!!";
    }
  }
  public function create()
  {
    if (in_array(session('auth')->level, [2, 46]))
    {
      $karyawan = self::get_karyawan();
      $witel = self::get_witel();
      $level = self::get_level();
      
      $data = new \stdClass();
      $data->id_user = null;
      $data->id_karyawan = null;
      $data->witel = null;
      $data->level = null;

      return view('user.input', compact('data', 'karyawan', 'witel', 'level'));
    } else {
      echo "fuck you !!!";
    }
  }
  public function input($id)
  {
    if (session('auth')->level=='2')
    {
      $data = DB::table('user')->where('id_user', $id)->first();
      $witel = self::get_witel();
      $level = self::get_level();
      $karyawan = self::get_karyawan();
      
      return view('user.input', compact('data', 'karyawan', 'witel', 'level'));
    } else {
      echo "fuck you !!!";
    }
  }
  public function save(Request $request)
  {
    date_default_timezone_set("Asia/Makassar");
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM user
      WHERE id_user = ?
    ',[
      $request->input('id_user')
    ]);
    if (count($exists)>0) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth) {
        DB::table('user')
          ->where('id_user', $data->id_user)
          ->update([
            'id_karyawan'         => $request->input('karyawan'),
            'psb_remember_token'  => NULL,
            'witel'               => $request->input('witel'),
            'level'               => $request->input('level'),
            'status'              => 0,
            'updated_user'        => $auth->id_karyawan,
            'datetime_updated'    => date('Y-m-d H:i:s')
          ]);
        if($request->input('password')!=''){
          DB::table('user')
          ->where('id_user', $data->id_user)
          ->update([
            'password'            => md5($request->input('password')),
            'psb_remember_token'  => NULL
          ]);
        }
      });
      return redirect('/user/v2')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
      ]);
    }
    else {
      DB::transaction(function() use($request) {
        DB::table('user')->insert([
          'id_user'     => $request->input('id_user'),
          'password'    => md5($request->input('password')),
          'id_karyawan' => $request->input('karyawan'),
          'witel'       => $request->input('witel'),
          'level'       => $request->input('level')
        ]);
      });
      return redirect('/user/v2')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
      ]);
    }
  }

  public function destroy($id){
    echo "DISABLED";
  }

  public function destroyx($id)
  {
    if ($auth->level=='2') {
    DB::table('user')
          ->where('id_user', [$id])->delete();

    return redirect('/user/v2')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
      ]);
    } else {
      echo "fuck you !!!";
    }
  }

  public function grab_roc($url){
    //personal
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.166.52/login.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "usr=roc_nas&pwd=roc%23nas");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    $answer = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    //ccan:http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL
    //personal:http://10.62.166.52/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL
    //all:http://10.62.166.52/nonadetail.php?reg=6&cust=rkap&waktu=&wtl=KALSEL
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "");
    $result = curl_exec($ch);
    if (curl_error($ch)) {
        echo curl_error($ch);
    }
    $columns = array(
            1 => 'No_Ticket',
            'Alpro',
            'Tgl_Open',
            'Jam',
            'Layanan',
            'Status_Link',
            'Cooper',
            'Ftth',
            'Usage',
            'Loker'
        );
    $html = str_replace("<br>","+",$result);
    $html = str_replace("<span class='label label-danger'>","+",$html);
    $html = str_replace("<span class='label label-warning'>","+",$html);
    $html = str_replace("<span class='label label-success'>","+",$html);
    //var_dump($html);

    $dom = @\DOMDocument::loadHTML(trim($html));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        //print_r($cells);
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }

        $result[] = $data;
    }
    return $result;
  }
  public function insert_roc($results, $ta){
    //var_dump($results);
    $sql = "insert ignore into roc(no_tiket,no_telp,no_speedy,tgl_open,tgl_ukur,tgl_lv,headline,lapul,loker,loker_ta,alpro,sto,paket,line_profile) values";
    $count=count($results);
    for ($i = 0; $i < $count; $i++) {
      $sparator = ", ";
      if($i==0){
        $sparator = "";
      }
      $data = $results["$i"]["No_Ticket"];
      $tiket = explode("+", $data);
      $no_tiket = trim($tiket[0]);
      $no_telp = trim($tiket[3]);
      $no_speedy = trim($tiket[4]);
      $data = $results["$i"]["Tgl_Open"];
      $data = explode("+", $data);
      $tgl_open = str_replace("OP : ","",$data[0]);
      $tgl_uk = "";
      $tgl_lv = "";
      if(isset($data[2])){
        $tgl_uk = str_replace("UK : ","",$data[2]);
      }
      if(isset($data[3])){
        $tgl_lv = str_replace("LV : ","",$data[3]);
      }
      $data = $results["$i"]["Layanan"];
      $headline = $data;
      $headline .= "+".$results["$i"]["Status_Link"]."+".$results["$i"]["Cooper"]."+".$results["$i"]["Ftth"]."+".$results["$i"]["Usage"];
      $cari = strpos($data, "Lapul : ");
      $cari += 8;
      $lapul = substr($data,$cari,1);
      $loker = $results["$i"]["Loker"];
      $data = $results["$i"]["Alpro"];
      $data = explode("+", $data);
      if(isset($data[0]))
        $alpro = $data[0];
      if(isset($data[1]))
        $sto = str_replace("/", "", $data[1]);
      if(isset($data[3]))
        $paket = $data[3];
      if(isset($data[4]))
        $line_profile = $data[4];
      $sql .= $sparator."('".$no_tiket."','".$no_telp."','".$no_speedy."','".$tgl_open."','".$tgl_uk."','".$tgl_lv."',".DB::connection()->getPdo()->quote(trim($headline)).",'".$lapul."','".$loker."',".$ta.",'".$alpro."','".$sto."','".$paket."','".$line_profile."')";
    }
    //var_dump($sql);

    DB::statement($sql);
  }
  public function sync_roc_personal(){
    $result = $this->grab_roc('http://10.62.166.52/nonadetail.php?reg=6&cust=psnl&waktu=&wtl=KALSEL');
    $this->insert_roc($result, 1);
  }
  public function sync_roc_corp(){
    $result = $this->grab_roc('http://10.62.166.52/nonadetail.php?reg=6&cust=corp&jam=&wtl=KALSEL');
    $this->insert_roc($result, 2);
  }
  public function roc_sync(){
    DB::table('roc')->truncate();
    $this->sync_roc_corp();
    $this->sync_roc_personal();
    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menyingkron ROC']
    ]);
  }
}
