<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\DismantlingModel;
class DismantlingController extends Controller
{
    public function dashboard(){
        $dashboard_1 = DismantlingModel::dashboard_1();
        return view('dismantling.dashboard',compact('dashboard_1'));
    }

    public function autodispatch_dismantling(){
        $date = date('Y-m-d');
        // cek teknisi 
        $query_teknisi = DB::SELECT('
            SELECT * FROM absen a LEFT JOIN 1_2_employee b ON a.nik = b.nik WHERE date(a.date_approval) = "'.$date.'" AND 
        ');
        return view('dismantling.autodispatch',compact('date','query_teknisi'));
    }

}