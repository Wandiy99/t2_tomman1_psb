<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class MaterialKeluarController extends Controller
{
  
  public function index()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT *
      FROM gudang_keluar
    ');

    return view('materialKeluar.list', compact('list'));
  }
  
  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM gudang_keluar
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          gudang_keluar_material m
          ON i.id_item = m.id_item
          AND m.gudang_keluar_id = ?
        WHERE 1
        ORDER BY id_item
      ', [
        $data->id
      ]);
    }
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $programs = DB::select('
      SELECT id as id, program as text
      FROM program_ta where 1
    ');
    return view('materialKeluar.input', compact('data', 'materials', 'gudangs', 'programs'));
  }
  
  public function create()
  {
    $data = new \StdClass;
    $data->id = null;
    $data->gudang_id = null;
    $data->tgl = null;
    $data->catatan = null;
    $data->status_material = null;
    $data->program_id = null;
    // TODO: order by most used
    $materials = DB::select('
      SELECT id_item, nama_item, 0 AS qty
      FROM item
      WHERE 1
      ORDER BY id_item
    ');
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $programs = DB::select('
      SELECT id as id, program as text
      FROM program_ta where 1
    ');
   // var_dump($materials);
    return view('materialKeluar.input', compact('data', 'materials', 'gudangs', 'programs'));
  }
  
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $materials = json_decode($request->input('materials'));
    $gudang_id = $request->input('gudang');
    
    $exists = DB::select('
      SELECT *
      FROM gudang_keluar
      WHERE id = ?
    ',[
      $id
    ]);
    
    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth, $materials,$gudang_id,$id) {
        $this->incrementStokGudang($id);
        DB::table('gudang_keluar')
          ->where('id', $data->id)
          ->update([
            'ts'   => DB::raw('NOW()'),
            'updater'   => $auth->id_karyawan,
            'tgl'       => $request->input('tanggal'),
            'gudang_id' => $gudang_id,
            'program_id'=> $request->input('program')
          ]);

        DB::table('gudang_keluar_material')
          ->where('gudang_keluar_id', $data->id)
          ->delete();
        foreach($materials as $material) {
          DB::table('gudang_keluar_material')->insert([
            'gudang_keluar_id' => $data->id,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        $this->decrementStokGudang($data->gudang_id, $materials);

      });
      return redirect('/material-keluar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan material Keluar']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials) {
        $insertId = DB::table('gudang_keluar')->insertGetId([
          'ts'        => DB::raw('NOW()'),
          'updater'   => $auth->id_karyawan,
          'gudang_id' => $request->input('gudang'),
          'tgl'       => $request->input('tanggal'),
          'program_id'=> $request->input('program')
        ]);
        foreach($materials as $material) {
          DB::table('gudang_keluar_material')->insert([
            'gudang_keluar_id' => $insertId,
            'id_item' => $material->id_item,
            'qty' => $material->qty
          ]);
        }
        $this->decrementStokGudang($request->input('gudang'), $materials);
        //$this->incrementStokTeknisi($pj, $materials);
      });
      return redirect('/material-keluar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Material Keluar']
      ]);
    }
    
  }
  public function destroy($id)
  {
    DB::transaction(function() use($id) {
      $this->incrementStokGudang($id);
      DB::table('gudang_keluar')
          ->where('id', [$id])->delete();
      DB::table('gudang_keluar_material')
          ->where('gudang_keluar_id', [$id])->delete();
    });
    return redirect('/material-keluar')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }

  private function decrementStokGudang($id, $materials){
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_gudang
        WHERE id_item = ? and id_gudang = ?
      ',[
        $material->id_item, $id
      ]);
      if(count($exists)){
        $data = $exists[0];
        DB::table('stok_material_gudang')
          ->where('id', $data->id)
          ->decrement('stok', $material->qty);
      }else{
        DB::table('stok_material_gudang')->insert([
          'id_item'   => $material->id_item,
          'id_gudang' => $id,
          'stok'      => $material->qty
        ]);
      }
    }
  }

  private function incrementStokGudang($id){
    $gudang = DB::select('
        SELECT *
        FROM gudang_keluar
        WHERE id = ?
    ',[
      $id
    ]);
    $data=$gudang[0];
    $materials = DB::select('
        SELECT *
        FROM gudang_keluar_material
        WHERE gudang_keluar_id = ?
    ',[
      $id
    ]);
    foreach($materials as $material) {
      DB::table('stok_material_gudang')
      ->where('id_item', $material->id_item)
      ->where('id_gudang', $data->gudang_id)
      ->increment('stok', $material->qty);
    }
  }
}
