<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\ResponhdModel;

class ResponhdController extends Controller
{
    public function createKeluhanForm(){
    	$getKeluhan = ResponhdModel::getKeluhan();	
    	return view('ResponHd.FormSendKeluhan', compact('getKeluhan'));
    }
}
