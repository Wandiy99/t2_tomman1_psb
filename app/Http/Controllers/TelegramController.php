<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Telegram;

class TelegramController extends Controller
{

	public static function index(){
		$group_telegram = DB::table('group_telegram')
		->select('chat_id as id','title as text')
		->get();
		return view('telegram.index', compact('group_telegram'));
	}

	public static function send(Request $req){
		Telegram::sendMessage([
	      'chat_id' => $req->group,
	      'text' => $req->message
	    ]);
		return back();
	}

}
