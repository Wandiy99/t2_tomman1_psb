<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\scbe;
use DB;

class scbeController extends Controller
{

	public function dashboard_new($tgl){
		$witel = session('witel');
		$datel = scbe::datel($witel);
		$data_progress_scbe = array();
		foreach ($datel as $result){
		$data_progress_scbe[$result->datel] = scbe::data_progress_scbe($tgl,$result->datel);
		}
		return view('scbe.dashboardNew',compact('tgl','data_progress_scbe','datel'));
	}

	public function download_scbe($tgl)
	{
		$query = scbe::download_scbe($tgl);
		return view('scbe.download_scbe',compact('query','tgl'));
	}

	public function download_qc(){
		$query = scbe::download_qc();
		return view('scbe.download_qc',compact('query'));
	}

	public function dashboard_new_list($tgl,$datel,$status){
		$query = scbe::dashboard_new_list($tgl,$datel,$status);
		return view('scbe.dashboardNewList',compact('tgl','datel','status','query'));
	}

	public function dashboard_new_list_pra($tgl,$datel,$status){
		$query = scbe::dashboard_new_list($tgl,$datel,$status);
		return view('scbe.dashboardNewList',compact('tgl','datel','status','query'));
	}


    public function seacrhForm(Request $req)
	{
    	// $this->validate($req,[
    	// 	'scbeq'	=> 'numeric'
    	// ],[
    	// 	'scbeq.numeric'	=> 'Inputan Angka',
    	// ]);

    	$datas = [];
    	$list  = [];
    	if($req->has('scbeq')){
    		$cari  = $req->input('scbeq');
    		$datas = scbe::cariMyir($cari);
    		$ket   = '0';
    		$sc    = 0;
    		$myirAda = 0;

    		$list = array();
    		if(count($datas)){
    			$list = [
    				'myir'					=> $datas[0]->myir,
					'customer'				=> $datas[0]->customer,
					'jenis_layanan' 		=> $datas[0]->layanan,
					'kordinatPel'   		=> $datas[0]->koorPelanggan,
					'alamatSales'   		=> $datas[0]->alamatSales ?: $datas[0]->alamatLengkap,
					'namaOdp'    			=> $datas[0]->namaOdp,
					'odpByTeknisi'			=> $datas[0]->odpByTeknisi,
					'picPelanggan'  		=> $datas[0]->picPelanggan,
					'idMyir'				=> $datas[0]->idMyir,
					'id_dt'					=> $datas[0]->id_dt,
					'Ndem'					=> $datas[0]->Ndem,
					'uraian'				=> $datas[0]->uraian,
					'tgl'					=> $datas[0]->tgl,
					'tgl_awal_dispatch'		=> $datas[0]->tgl_awal_dispatch,
					'orderDate'				=> $datas[0]->orderDate,
					'laporan_status'		=> $datas[0]->laporan_status,
					'sc'		    		=> $datas[0]->sc,
					'valins_id'				=> $datas[0]->valins_id,
					'dropcore_label_code' 	=> $datas[0]->dropcore_label_code,
					'odp_label_code' 		=> $datas[0]->odp_label_code,
					'id'					=> $datas[0]->id_dt,
					'ketInput' 				=> $datas[0]->ket_input,
					'ket'					=> $datas[0]->ket,
					'no_internet'   		=> $datas[0]->no_internet,
					'no_telp' 				=> $datas[0]->no_telp,
					'catatan' 				=> $datas[0]->catatan,
					'modified_at'			=> $datas[0]->modified_at,
					'id_pl'					=> $datas[0]->id_pl,
					'addSc'					=> '0',
					'laporan_status_id' 	=> $datas[0]->laporan_status_id,
					'kcontact'				=> $datas[0]->dps_kcontact,
					'orderStatus'			=> $datas[0]->dps_orderStatus,
					'mc_nama_order'			=> $datas[0]->mc_nama_order,
					'mc_status'				=> $datas[0]->mc_status,
					'mc_action'				=> $datas[0]->mc_action,
					'mc_headline'			=> $datas[0]->mc_headline,
					'mc_port_used'			=> $datas[0]->mc_port_used,
					'mc_port_idle'			=> $datas[0]->mc_port_idle,
					'tgl_selesai'			=> $datas[0]->tgl_selesai,
					'dispatch_regu_name'	=> $datas[0]->dispatch_regu_name,
					'mc_modif_by'			=> $datas[0]->mc_modif_by,
					'mc_modif_at'			=> $datas[0]->mc_modif_at,
    			];

    			$myirAda = 0;
    		}
    		else{
    			$datas = scbe::getMyirByMyir($cari);
    			if(count($datas)){
	    			$list = [
						'id'				=> $datas->id,
	    				'myir'				=> $datas->myir,
						'customer'			=> $datas->customer,
						'jenis_layanan' 	=> $datas->layanan,
						'kordinatPel'   	=> $datas->koorPelanggan,
						'alamatSales'   	=> $datas->alamatLengkap,
						'namaOdp'    		=> $datas->namaOdp,
						'odpByTeknisi'		=> '-',
						'picPelanggan'  	=> $datas->picPelanggan,
						'idMyir'			=> $datas->idMyir,
						'id_dt'				=> null,
						'Ndem'				=> '-',
						'tgl_awal_dispatch'	=> null,
						'orderDate'			=> $datas->orderDate,
						'uraian'			=> '-',
						'tgl'				=> '-',
						'laporan_status'	=> '-',
						'sc'		    	=> $datas->sc,
						'id'				=> $datas->id,
						'ketInput' 			=> $datas->ket_input,
						'ket'				=> $datas->ket,
						'no_internet'   	=> $datas->no_internet,
						'no_telp' 			=> $datas->no_telp,
						'addSc'				=> '1',
						'laporan_status_id' => '-',
						'kcontact'			=> $datas->kcontack,
						'orderStatus'		=> '-',
						'mc_nama_order'		=> '-',
						'mc_status'			=> '-',
						'mc_action'			=> '-',
						'mc_headline'		=> '-',
						'mc_port_used'		=> '-',
						'mc_port_idle'		=> '-',
						'tgl_selesai'		=> '-',
						'dispatch_regu_name'=> '-',
						'mc_modif_by'		=> '-',
						'mc_modif_at'		=> '-',
	    			];

    				$myirAda = 1;
    			}

    		}

    		if (count($datas)){
	    		// if(@$datas[0]->ket==0){
	    		// 	$cekSc = DB::table('Data_Pelanggan_Starclick')->where('kcontact','LIKE','%'.$cari.'%')->whereNotIN('orderStatus',['UNSC','REVOKE ORDER UNSC','REVOKE ORDER','CANCEL COMPLETED','Cancel Order'])->first();
	    		// 	if(count($cekSc)){
	    		// 		$sc = 1;
	    		// 	}
	    		// };
    		}

    		// if (empty($datas)){
    		// 	$ada = scbe::getMyirByMyir($cari);
    		// 	if ($ada){
    		// 		$ket = $ada[0]->ket;
	    	// 		// $datas = scbe::cariMyirStatusSatu($ada->myir);
	    	// 		$datas = $ada;
	    	// 		$sc = 1;
    		// 	};
    		// }

    		// sementra starclick error
    		// dd($list);


    	};

    	return view('scbe.search',compact('ket', 'sc', 'list', 'myirAda'));
    }

    public function input(Request $req)
	{
    	$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        a.ACTIVE = 1 AND b.sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND b.sms_active_prov = 1 AND b.ket_posisi IS NOT NULL
	    ');

	    $get_manja_status = DB::SELECT('
	      SELECT
	        a.manja_status_id as id,
	        a.manja_status as text
	      FROM
	        manja_status a
	    ');

	     $dataSektor = DB::SELECT('
	      SELECT
	      a.chat_id as id,
	      a.title as text
	      FROM
	        group_telegram a
	      WHERE
	      	a.Witel_New = "'.session('witel').'"
	    ');

	    $sto = DB::select('SELECT sto as id, sto as text from maintenance_datel where witel = "' . session('witel') . '"');
	    $odpSektor = $odp = $lat = $long = '';
	    
		if ($req->has('searchOdp'))
		{
	    	$cari = $req->input('searchOdp');

	    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$cari.'" GROUP BY b.title';

	    	$odpSektor = DB::SELECT($sql);
	    	$odp = $cari;

	    	if (count($odpSektor)<>0){
	    		$lat  = $odpSektor[0]->LATITUDE;
	    		$long = $odpSektor[0]->LONGITUDE;
	    	}

		};
		
		$jenis_layanan = DB::table('dispatch_teknisi_jenis_layanan')->select('jenis_layanan_id as id', 'jenis_layanan as text')->where('active_layanan', 1)->orderBy('jenis_layanan', 'asc')->get();

	   	$dataSales = DB::select('SELECT id, kode_sales, nama_sales, kode_sales as text FROM psb_sales where ket=1');
    	return view('scbe.input',compact('regu', 'get_manja_status', 'sto', 'odpSektor', 'odp', 'lat', 'long', 'dataSektor', 'dataSales','jenis_layanan'));
    }

    public function dispatchSimpan(Request $req)
	{
  		date_default_timezone_set('Asia/Makassar');
    	$this->validate($req,[
    		'myir'			=> 'numeric',
    		'nmCustomer'	=> 'required',
    		'picPelanggan'	=> 'required',
    		'picPelanggan'	=> 'numeric|digits_between:11,14',
    		'nama_odp'		=> 'required',
    		'input-sektor'	=> 'required',
    		'id_regu'		=> 'required',
			'crewid'		=> 'required',
    		'sales'			=> 'required',
    		'jenis_layanan'	=> 'required',
			'source_order'	=> 'required'
    	],[
    		'myir.numeric' => 'Inputan Angka',
    		'nmCustomer.required' => 'Nama Pelanggan Diisi Bang !',
    		'picPelanggan.required' => 'PIC Pelanggan Diisi Bang !',
    		'nama_odp.required' => 'ODP Diisi Bang ! ',
    		'input-sektor.required' => 'Sektor Diisi Bang !',
    		'id_regu.required' => 'Regu Diisi',
			'crewid.required' => 'Crew ID Diisi',
    		'picPelanggan.numeric' => 'Inputan Hanya Angka',
    		'picPelanggan.digits_between' => 'Inputan Diperbolehan 11 - 14 Digit',
    		'sales.required' => 'Sales Diisi',
    		'jenis_layanan.required' => 'Jenis Layanan Diisi !',
			'source_order.required' => 'Source Order Kosong!'
    	]);

	    // insert into log status
	    $auth = session('auth');
		$exists = DB::table('dispatch_teknisi')
		->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
		->leftJoin('regu', 'dispatch_teknisi.id_regu', '=', 'regu.id_regu')
		->where('dispatch_teknisi.Ndem', $req->myir)
		->select('dispatch_teknisi.*', 'psb_laporan.status_laporan', 'regu.mainsector')
		->first();

	    $status_laporan = 6;
    	if (count($exists) > 0)
		{
			if ($exists->status_laporan == 4)
			{
				$status_laporan = 4;
			}
		};

		$bypass = DB::table('dispatch_teknisi_bypass')->where('nik', session('auth')->id_karyawan)->first();
		if (count($bypass) != 1) {
			if ( date('H') < 18) {
				if ($req->input('id_regu') <> "") {
					// cek absen tim
					$cek_tim_absen = DB::table('regu')->where('id_regu', $req->input('id_regu'))->first();
					$jml_tek = 0;

					if ($cek_tim_absen->nik1 <> "" && $cek_tim_absen->nik2 <> "") {
						$jml_tek = "2";
					} elseif ($cek_tim_absen->nik1 <> "" && $cek_tim_absen->nik2 == NULL || $cek_tim_absen->nik1 == NULL && $cek_tim_absen->nik2 <> "") {
						$jml_tek = "1";
					}

					$cek_absen_tim = DB::SELECT('
					SELECT
					r.id_regu,
					a.nik,
					a.tglAbsen,
					a.approval,
					a.date_approval
					FROM absen a
					LEFT JOIN regu r ON (r.nik1 = a.nik OR r.nik2 = a.nik)
					WHERE
					(DATE(a.tglAbsen) = "'.date('Y-m-d').'") AND
					r.id_regu = '.$req->input('id_regu').'
					GROUP BY a.nik
					');

					if (count($cek_absen_tim) != $jml_tek) {
						return back()->with('alerts', [
						['type' => 'danger', 'text' => 'Absensi Tim Belum Lengkap !']
						]);
					}
				}
			}
		}

		$cek_jenis_layanan = DB::SELECT('SELECT * FROM dispatch_teknisi_jenis_layanan WHERE jenis_layanan_id = "'.$req->input('jenis_layanan').'" ');
		$jenis_layanan = $cek_jenis_layanan[0];

	    DB::transaction(function() use($req, $auth, $status_laporan) {

	       DB::table('psb_laporan_log')->insert([
	         'created_at'      => DB::raw('NOW()'),
			 'created_by'      => $auth->id_karyawan,
			 'created_name'    => $auth->nama,
	         'status_laporan'  => $status_laporan,
	         'id_regu'         => $req->input('id_regu'),
	         'catatan'         => "DISPATCH at ".date('Y-m-d'),
	         'Ndem'            => $req->myir
	       ]);

			DB::table('dispatch_teknisi_log')->insert([
				'updated_at'          => DB::raw('NOW()'),
				'updated_by'          => $auth->id_karyawan,
				'tgl'                 => $req->input('tgl'),
				'id_regu'             => $req->input('id_regu'),
				'manja'               => $req->input('manja'),
				'manja_status'        => $req->input('manja_status'),
				'updated_at_timeslot' => $req->input('timeslot'),
				'Ndem'                => $req->myir
			]);

	     });

	    // reset status if redispatch
	    if (count($exists) > 0 ) {
	      DB::transaction(function() use($req, $exists, $auth, $status_laporan, $jenis_layanan) {

			DB::table('psb_laporan')->where('id_tbl_mj', $exists->id)->update([
				'status_laporan' => $status_laporan
			]);

			DB::table('psb_laporan_log')->insert([
				'created_at'      => DB::raw('NOW()'),
				'created_by'      => $auth->id_karyawan,
				'status_laporan'  => $status_laporan,
				'id_regu'         => $req->input('id_regu'),
				'catatan'         => "REDISPATCH",
				'Ndem'            => $req->myir,
				'psb_laporan_id'  => $exists->id
			]);

			DB::table('dispatch_teknisi')->where('id', $exists->id)->update([
				'NO_ORDER'				=> $req->input('myir'),
				'updated_at'            => DB::raw('NOW()'),
				'updated_by'            => $auth->id_karyawan,
				'tgl'                   => $req->input('tgl'),
				'updated_at_timeslot'   => $req->input('timeslot'),
				'id_regu'               => $req->input('id_regu'),
				'crewid'               	=> $req->input('crewid'),
				'step_id'               => "1.0",
				'kordinatPel'           => $req->input('kordinatPel'),
				'jenis_layanan'			=> $jenis_layanan->jenis_layanan,
				'jenis_layanan_id'      => $req->input('jenis_layanan'),
				'source_order'			=> $req->input('source_order'),
				'jenis_order'			=> "MYIR",
				'alamatSales'           => $req->alamatSales,
				'dispatch_sto'    	 	=> $req->input('sto')
			]);

	      });

	      scbe::simpanDispatch($req, '1');

	    } else {
 	     // cari berdsarkan pic kalo sudah ad sinkron kan
	   	 $getPic = DB::table('psb_myir_wo')->where('picPelanggan',$req->picPelanggan)->first();
	     if ($getPic)
		 {
	    	if ($getPic->ketMyirGangguan == 1)
			{
				return back()->with('alerts', [['type' => 'danger', 'text' => 'Pic Pelanggan sudah Terdispatch di Myir - ' . $getPic->myir . '
	            <a href="/sinkron-myir/' . $getPic->picPelanggan . '/' . $req->myir . '" class="btn btn-primary">Sinkronkan MYIR</a>']]);
			}
	     };

	     // cek myir kalo sudah ada
	     $getMyirExist = DB::table('psb_myir_wo')->where('myir',$req->myir)->first();
	     if(count($getMyirExist) > 0)
		 {
	     	return back()->with('alerts',[['type' => 'danger', 'text' => 'Myir Sudah Ada, Periksa Lagi Myir']]);
	     };

	     DB::transaction(function() use($req, $auth, $jenis_layanan) {
	      	$dispatchBy = '5';

	    	if ($req->jenis_layanan == 'SMARTINDIHOME')
			{
	    		$dispatchBy = '7';
	    	};

	        DB::table('dispatch_teknisi')->insert([
			'NO_ORDER'				=> $req->input('myir'),
			'updated_at'			=> DB::raw('NOW()'),
			'updated_by'			=> $auth->id_karyawan,
			'tgl'					=> $req->input('tgl'),
			'id_regu'				=> $req->input('id_regu'),
			'crewid'				=> $req->input('crewid'),
			'created_at'			=> DB::raw('NOW()'),
			'updated_at_timeslot'	=> $req->input('timeslot'),
			'jenis_layanan'			=> $jenis_layanan->jenis_layanan,
			'jenis_layanan_id'    	=> $req->input('jenis_layanan'),
			'jenis_order'			=> "MYIR",
			'source_order'			=> $req->input('source_order'),
			'Ndem'					=> $req->myir,
			'step_id'				=> "1.0",
			'kordinatPel'			=> $req->kordinatPel,
			'alamatSales'			=> $req->alamatSales,
			'dispatch_by'			=> $dispatchBy,
			'dispatch_sto'			=> $req->input('sto')
	        ]);
	      });

			scbe::simpanDispatch($req,'0');
			print_r("Success");
	    }


	    // ubah status dispacth tabel psb_myir_wo
	    // scbe::updateDispatch($req->myir);

	    $id = $req->myir;
	    exec('cd ..;php artisan sendTelegramSSC1 '.$id.' '.$auth->id_user.' > /dev/null &');

		// kirim data comparin
		$fitur = DB::table('fitur')->first();
		if ($fitur->comparin_dispatch == 1)
		{
		  $id_regu = $req->input('id_regu');
		  $this->dispatchComparin($id, $id_regu);
		}

     	return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'Dispatch Sukses']]);
    }

	private function dispatchComparin($myir, $id_regu)
	{
		$regu = DB::table('regu')->where('id_regu', $id_regu)->first();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://10.128.16.65/comparin/controller/api/tomman_dispatch.php',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => 'order_id=MYIR-'.$myir.'&order_type=MYIR&id_regu='.$regu->id_regu.'&teknisi='.$regu->nik1.','.$regu->nik2.'&token=c0815b4a7cd87885df4f0548939e9e6e5a8962a7844f1dcb7b3f85888fc4db75',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$decode = json_decode($response);
		DB::transaction(function() use($myir, $regu, $decode) {
			DB::table('dispatch_comparin_log')->insert([
				'order_id'      => 'MYIR-'.$myir,
				'type_order'    => 'MYIR',
				'id_regu'       => $regu->id_regu,
				'teknisi'       => $regu->nik1.','.$regu->nik2,
				'status_api'    => $decode->status,
				'message_api'   => $decode->message,
				'created_by'    => session('auth')->id_karyawan
			]);
		});

		return $response;
	}

    public function edit(Request $req, $id){
    	$regu = DB::select('
	      SELECT a.id_regu as id,a.uraian as text, b.title
	      FROM regu a
	        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
	      WHERE
	        ACTIVE = 1
	    ');

	    $sto = DB::select('SELECT sto as id, sto as text from maintenance_datel');
		$jenis_layanan = DB::table('dispatch_teknisi_jenis_layanan')->select('jenis_layanan_id as id', 'jenis_layanan as text')->where('active_layanan', 1)->orderBy('jenis_layanan', 'asc')->get();

		$datas = scbe::getMyir($id);

    	$dataSektor = DB::SELECT('
	      SELECT
	      a.chat_id as id,
	      a.title as text
	      FROM
	        group_telegram a
	    ');

    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$datas[0]->namaOdp.'" GROUP BY b.title';

	    $odpSektor = DB::SELECT($sql);

	    if (count($odpSektor)==0){
	    	$odp = '';
	    	$lat = '';
	    	$long = '';
	    }
	    else{
		    $odp = $odpSektor[0]->alproname;
		    $lat = $odpSektor[0]->LATITUDE;
		    $long = $odpSektor[0]->LONGITUDE;
	    };

	    if ($req->has('searchOdp')){
	    	$cari = $req->input('searchOdp');

	    	$sql = 'SELECT b.title, a.alproname, c.LATITUDE, c.LONGITUDE FROM
	    			alpro_owner a
	    			LEFT JOIN group_telegram b ON a.nik = b.TL_NIK
	    			LEFT JOIN 1_0_master_odp c ON a.alproname = c.ODP_NAME
	    			WHERE a.alproname = "'.$cari.'" GROUP BY b.title';

	    	$odpSektor = DB::SELECT($sql);
	    	$odp = $cari;

	    	if (count($odpSektor)<>0){
	    		$lat  = $odpSektor[0]->LATITUDE;
	    		$long = $odpSektor[0]->LONGITUDE;
	    	}

	    };

	   	$dataSales = DB::select('SELECT id, kode_sales, nama_sales, kode_sales as text FROM psb_sales where ket=1');
    	return view('scbe.edit',compact('regu', 'datas', 'sto', 'odp', 'lat', 'long', 'odpSektor', 'dataSektor', 'dataSales','jenis_layanan'));
    }

    public function hapusSc($myir){
    	scbe::hapusMyir($myir);
    	scbe::hapusNdemMyir($myir);
    	return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'Data Sudah Dihapus']]);
    }

    public function sinkSc($myir){
    	 $auth = session('auth');
    	// cari myir di data_startclick
    	$dataStartclik = scbe::getDataStarClickByMyir($myir);
    	if ($dataStartclik){
	    	// cek di table dispact_teknisi
	    	$dataDispacth = scbe::getDataDispatchByNdem($dataStartclik->orderId);

	    	if ($dataDispacth){
	    		scbe::hapusNdemMyir($myir);
	    		scbe::ubahKetScMyir($myir, $dataStartclik->orderId);
	    		exec('cd ..;php artisan sendTelegramSSC1SyncSc '.$dataStartclik->orderId.' '.$auth->id_user.' > /dev/null &');
	    		return back()->with('alerts',[['type'=>'success', 'text'=>'Syncron SC Berhasil !!!']]);

	    	}
	    	else{
	    		// ubah Ndem myir jadi sc
	    		DB::table('dispatch_teknisi')->where('Ndem',$myir)->update([
	    			'Ndem'			=> $dataStartclik->orderId,
	    			'dispatch_by'	=> NUll
	    		]);

	    		scbe::ubahKetScMyir($myir, $dataStartclik->orderId);

	    		// kirim ssc
	    		exec('cd ..;php artisan sendTelegramSSC1SyncSc '.$dataStartclik->orderId.' '.$auth->id_user.' > /dev/null &');
    			return back()->with('alerts',[['type'=>'success', 'text'=>'Syncron SC Berhasil !!!']]);
	    	}
    	}
    	else{
    		return back()->with('alerts',[['type'=>'danger', 'text'=>'Syncron SC Gagal, Sc Tidak Ditemukan !!!']]);
    	}
    }

    public function searchOdpFull(Request $req){
    	$datas = [];
    	$cari  = '';
    	if ($req->has('odpq')){
    		$cari = $req->input('odpq');

    		$datas = scbe::getOdpFull($cari);
    	}

    	return view('scbe.searchOdp',compact('datas', 'cari'));
    }

    public function inputOdpFull(){
    	return view('scbe.inputOdp');
    }

    public function simpanOdpFull(Request $req){

    }

    public function sinkronMyir($pic, $myir){
    	// update psb_myir_wo
	    $auth = session('auth');
    	$getMyirLama = DB::table('psb_myir_wo')->where('picPelanggan',$pic)->first();
    	DB::table('dispatch_teknisi')->where('Ndem',$getMyirLama->myir)->update([
    		'Ndem'	=> $myir
    	]);

    	$getPsbLog = DB::table('psb_laporan_log')->where('Ndem',$getMyirLama->myir)->orderBY('psb_laporan_log_id','DESC')->first();
    	DB::table('psb_laporan_log')->insert([
    		'id_regu'				=> $getPsbLog->id_regu,
    		'Ndem'					=> $myir,
    		'psb_laporan_id'		=> $getPsbLog->psb_laporan_id,
    		'status_laporan'		=> $getPsbLog->status_laporan,
    		'catatan'				=> 'Perubahan Myir',
    		'created_at'			=> DB::raw('NOW()'),
    		'created_by'			=> $auth->id_user,
    		'mttr'					=> $getPsbLog->mttr,
    		'penyebabId'			=> $getPsbLog->penyebabId,
    		'action'				=> $getPsbLog->action,
    		'jadwal_manja'			=> $getPsbLog->jadwal_manja,
    	]);

    	DB::table('psb_myir_wo')->where('picPelanggan',$pic)->update([
    		'myir'		=> $myir
    	]);

    	return back()->with('alerts',[['type' => 'success', 'text' => 'Sinkron Myir Sukses']]);
    }
}
