<?php

namespace App\Http\Controllers;

use App\DA\ProfileModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use Telegram;

class ProfileController extends Controller
{
  public function index()
  {
    if (session('auth')->level != 10)
    {
      $layout = 'layout';
    } else {
      $layout = 'new_tech_layout';
    }

    $get_gender = [
      (object)['id' => 'Laki-Laki', 'text' => 'Laki-Laki'],
      (object)['id' => 'Perempuan', 'text' => 'Perempuan']
    ];

    $get_mitra = DB::table('mitra_amija')
    ->where('kat', 'DELTA')
    ->where('mitra_status', 'ACTIVE')
    ->where('witel', session('auth')->nama_witel)
    ->select('mitra_amija as id', 'mitra_amija_pt as text')
    ->get();

    $get_regu = DB::table('regu')
    ->where('ACTIVE', 1)
    ->whereNull('jenis_regu')
    ->select('id_regu as id', 'uraian as text')
    ->orderBy('uraian', 'ASC')
    ->get();

    $get_sto = DB::table('area_alamat')
    ->select('kode_area as id', 'kode_area as text')
    ->orderBy('kode_area', 'ASC')
    ->get();

    $data = ProfileModel::index();
    if (isset($data))
    {
      $profile = $data[0];
    } else {
      $profile = [];
    }

    return view('profile.index', compact('layout', 'profile', 'get_gender', 'get_mitra', 'get_regu', 'get_sto'));
  }

  public function profileSave(Request $req)
  {
    ProfileModel::profileSave($req);

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menyimpan Data Diri']
    ]);
  }
}
