<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
use Excel;
Use App\DA\DashboardModel;
use App\DA\PsbModel;
use App\DA\ApiModel;
use App\DA\BotModel;

date_default_timezone_set('Asia/Makassar');

class ApiController extends Controller
{

  protected $QC_foto = [
  'Berita_Acara',
  'BA_Digital',
  'RFC_Form',
  'Profile_MYIH',
  'Surat_Pernyataan_Deposit',
  'Proses_TTD_Surat_Pernyataan',
  'Lokasi',
  'Lokasi_1',
  'Lokasi_2',
  'Meteran_Rumah',
  'ID_Listrik_Pelanggan',
  'Foto_Pelanggan_dan_Teknisi',
  'ODP',
  'LABEL',
  'Redaman_ODP',
  'Redaman_Pelanggan',
  'SN_ONT',
  'SN_STB',
  'Live_TV',
  'TVOD',
  'Speedtest',
  'Telephone_Incoming',
  'Telephone_Outgoing',
  'additional_1',
  'additional_2',
  'additional_3',
  'additional_4',
  'additional_5'
  ];

  public static function batch_send_to_dalapa($date){
    $get_sc = PsbModel::batch_get_data_for_dalapa($date);
    foreach ($get_sc as $result){
      ApiController::send_to_dalapa($result->id_dt);
      echo "NDEM : ".$result->Ndem." ; SC.".$result->orderId." <br />";
    }
  }

  public static function dashboardComparinList($witel,$tgl,$status){
    $field_after = '
        aa.orderId as ORDER_ID,
        aa.orderName as ORDERNAME,
        aa.alproname as ALPRONAME,
        aa.sto as STO,
        aa.orderAddr as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN';
    $field_pra = '
        aa.myir as ORDER_ID,
        aa.customer as ORDERNAME,
        aa.namaOdp as ALPRONAME,
        aa.sto as STO,
        aa.alamatLengkap as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN
        ';
    $table_pra = '
        FROM dispatch_teknisi bb
        LEFT JOIN psb_myir_wo aa ON bb.`Ndem` = aa.myir
        LEFT JOIN psb_laporan cc ON bb.id = cc.`id_tbl_mj`
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.`laporan_status_id`
        LEFT JOIN regu gg ON bb.`id_regu` = gg.`id_regu`
        LEFT JOIN group_telegram f ON gg.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON SUBSTR(aa. namaOdp,5,3) = g.sto';
    $table_after = '
        FROM dispatch_teknisi bb
        LEFT JOIN Data_Pelanggan_Starclick aa ON bb.`Ndem` = aa.orderId
        LEFT JOIN psb_laporan cc ON bb.id = cc.`id_tbl_mj`
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.`laporan_status_id`
        LEFT JOIN regu gg ON bb.`id_regu` = gg.`id_regu`
        LEFT JOIN group_telegram f ON gg.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON aa.sto = g.sto';
    switch ($status) {
      case "BELUM_DISPATCH_PRA" :
        $SQL = '
        SELECT
        aa.myir as ORDER_ID,
        aa.customer as ORDERNAME,
        aa.namaOdp as ALPRONAME,
        aa.sto as STO,
        aa.alamatLengkap as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        "" as ID_REGU,
        "" as CREW_ID,
        "" as ALIAS,
        "" as ID_STATUS,
        "" as STATUS_TEKNISI,
        "" as TGL_STATUS,
        "" as CATATAN
        from psb_myir_wo aa
        LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
        LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
        LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
        LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
        where
        date(aa.orderDate) >= "2020-04-01" AND
        ff.orderId IS NULL AND
        aa.status_approval <> "2" AND
        bb.id IS NULL AND
        aa.sc IS NULL AND
        aa.layanan <> "PDA" AND
        ee.witel = "'.$witel.'"
        ';
      break;
      case "BELUM_DISPATCH_AFTER" :
        $SQL = '
        SELECT
        aa.orderId as ORDER_ID,
        aa.orderName as ORDERNAME,
        aa.alproname as ALPRONAME,
        aa.sto as STO,
        aa.orderAddr as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        "" as ID_REGU,
        "" as CREW_ID,
        "" as ALIAS,
        "" as ID_STATUS,
        "" as STATUS_TEKNISI,
        "" as TGL_STATUS,
        "" as CATATAN
        from Data_Pelanggan_Starclick aa
        LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
        LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
        LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
        LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
        LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
        LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
        LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
        where
        ee.witel = "'.$witel.'" AND
        aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
        aa.jenisPsb LIKE "AO%" AND
        bb.id IS NULL AND
        date(aa.orderDate) >= "2020-04-01" AND
        ((cc.id is NULL OR dd.grup IN ("SISA")) AND (ccc.id is null OR ddd.grup IN ("SISA")))
        ';
      break;
      case "SUDAH_DISPATCH_PRA" :
        $SQL = '
        SELECT
        aa.myir as ORDER_ID,
        aa.customer as ORDERNAME,
        aa.namaOdp as ALPRONAME,
        aa.sto as STO,
        aa.alamatLengkap as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN
        from psb_myir_wo aa
        LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
        LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
        LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
        LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
        LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
        where
        date(aa.orderDate) >= "2020-04-01" AND
        ff.orderId IS NULL AND
        aa.status_approval <> "2" AND
        bb.id IS NOT NULL AND
        aa.sc IS NULL AND
        aa.layanan <> "PDA" AND
        bb.tgl = "'.$tgl.'" AND
        ee.witel = "'.$witel.'"
        ';
      break;
      case "SUDAH_DISPATCH_AFTER" :
        $SQL = '
        SELECT
        aa.orderId as ORDER_ID,
        aa.orderName as ORDERNAME,
        aa.alproname as ALPRONAME,
        aa.sto as STO,
        aa.orderAddr as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN
        from Data_Pelanggan_Starclick aa
        LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
        LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
        LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
        LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
        LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
        LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
        LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
        LEFT JOIN regu gg ON bb.id_regu = gg.id_regu
        where
        ee.witel = "'.$witel.'" AND
        aa.jenisPsb LIKE "AO%" AND
        bb.id IS NOT NULL AND
        date(aa.orderDate) >= "2020-04-01" AND
        bb.tgl = "'.$tgl.'"
        ';
      break;
      case "NEED_PROGRESS_PRA" :
        $SQL = '
        SELECT
        aa.myir as ORDER_ID,
        aa.customer as ORDERNAME,
        aa.namaOdp as ALPRONAME,
        aa.sto as STO,
        aa.alamatLengkap as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN
        FROM dispatch_teknisi bb
        LEFT JOIN psb_myir_wo aa ON bb.`Ndem` = aa.myir
        LEFT JOIN psb_laporan cc ON bb.id = cc.`id_tbl_mj`
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.`laporan_status_id`
        LEFT JOIN regu gg ON bb.`id_regu` = gg.`id_regu`
        LEFT JOIN group_telegram f ON gg.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON SUBSTR(aa. namaOdp,5,3) = g.sto
        WHERE
        bb.tgl >= "2020-04-01" AND
        (cc.id IS NULL OR cc.status_laporan = 6 OR cc.status_laporan=45) AND
        aa.sc IS NULL AND
        aa.myir IS NOT NULL AND g.witel= "'.$witel.'"
        ';
      break;
      case "NEED_PROGRESS_AFTER" :
        $SQL = '
        SELECT
        aa.orderId as ORDER_ID,
        aa.orderName as ORDERNAME,
        aa.alproname as ALPRONAME,
        aa.sto as STO,
        aa.orderAddr as ORDERADDRESS,
        bb.tgl as TGL_DISPATCH,
        bb.id_regu as ID_REGU,
        gg.crewid as CREW_ID,
        gg.uraian as ALIAS,
        cc.status_laporan as ID_STATUS,
        dd.laporan_status as STATUS_TEKNISI,
        cc.modified_at as TGL_STATUS,
        cc.catatan as CATATAN
        FROM dispatch_teknisi bb
        LEFT JOIN Data_Pelanggan_Starclick aa ON bb.`Ndem` = aa.orderId
        LEFT JOIN psb_laporan cc ON bb.id = cc.`id_tbl_mj`
        LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.`laporan_status_id`
        LEFT JOIN regu gg ON bb.`id_regu` = gg.`id_regu`
        LEFT JOIN group_telegram f ON gg.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON aa.sto = g.sto
        WHERE
        bb.tgl >= "2020-04-01" AND
        aa.orderDatePs = "" AND
        (cc.id IS NULL OR cc.status_laporan = 6 OR cc.status_laporan=45) AND
        aa.orderid IS NOT NULL AND
        aa.orderStatusId NOT IN ("1500","7","1300","1205") AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
      case "OGP_PRA" :
        $SQL = '
        SELECT
        '.$field_pra.'
        '.$table_pra.'
        WHERE
        bb.tgl >= "2020-04-01" AND
        dd.`grup`="OGP" AND
        aa.sc IS NULL AND
        aa.myir IS NOT NULL AND g.witel="'.$witel.'"
        ';
      break;
      case "OGP_AFTER" :
        $SQL = '
        SELECT
        '.$field_after.'
        '.$table_after.'
        WHERE
        bb.tgl >= "2020-04-01" AND
        aa.orderDatePs = "" AND
        dd.`grup`="OGP" AND
        aa.orderid IS NOT NULL AND
        aa.orderStatusId NOT IN ("1500","7","1300","1205") AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
      case "HR_PRA" :
        $SQL = '
        SELECT
        '.$field_pra.'
        '.$table_pra.'
        WHERE
        bb.tgl >= "2020-04-01" AND
        dd.`laporan_status`="HR" AND
        aa.sc IS NULL AND
        aa.myir IS NOT NULL AND g.witel="'.$witel.'"
        ';
      break;
      case "HR_AFTER" :
        $SQL = '
        SELECT
        '.$field_after.'
        '.$table_after.'
        WHERE
        bb.tgl >= "2020-04-01" AND
        aa.orderDatePs = "" AND
        dd.`laporan_status`="HR" AND
        aa.orderid IS NOT NULL AND
        aa.orderStatusId NOT IN ("1500","7","1300","1205") AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
      case "UP" :
        $SQL = '
        SELECT
        '.$field_after.'
        '.$table_after.'
        WHERE
        (bb.tgl = "'.$tgl.'" OR DATE(cc.modified_at) = "'.$tgl.'") AND
        dd.`grup`="UP" AND
        DATE(aa.orderDatePs) >= "'.$tgl.'" AND
        aa.orderid IS NOT NULL AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
      case "KP_PRA" :
        $SQL = '
        SELECT
        '.$field_pra.'
        '.$table_pra.'
        WHERE
        bb.tgl = "'.$tgl.'" AND
        dd.`grup`="KP" AND
        aa.sc IS NULL AND
        aa.myir IS NOT NULL AND g.witel="'.$witel.'"
        ';
      break;
      case "KP_AFTER" :
        $SQL = '
        SELECT
        '.$field_after.'
        '.$table_after.'
        WHERE
        bb.tgl = "'.$tgl.'" AND
        aa.orderDatePs = "" AND
        dd.`grup`="KP" AND
        aa.orderid IS NOT NULL AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
      case "KT_PRA" :
        $SQL = '
        SELECT
        '.$field_pra.'
        '.$table_pra.'
        WHERE
        bb.tgl = "'.$tgl.'" AND
        dd.`grup`="KT" AND
        aa.sc IS NULL AND
        aa.myir IS NOT NULL AND g.witel="'.$witel.'"
        ';
      break;
      case "KT_AFTER" :
        $SQL = '
        SELECT
        '.$field_after.'
        '.$table_after.'
        WHERE
        bb.tgl = "'.$tgl.'" AND
        aa.orderDatePs = "" AND
        dd.`grup`="KT" AND
        aa.orderid IS NOT NULL AND
        aa.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel="'.$witel.'"
        ';
      break;
    }
    $query = DB::SELECT($SQL);
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  public static function dashboardComparin($tgl,$witel){
    $query = DB::SELECT('
      SELECT
      a.witel,

      (select count(*) from Data_Pelanggan_Starclick aa
      LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
      LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
      LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
      LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
      LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
      where
      ee.witel = a.witel AND
      aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
      aa.jenisPsb LIKE "AO%" AND
      bb.id IS NULL AND
      date(aa.orderDate) >= "2020-04-01" AND
      ((cc.id is NULL OR dd.grup IN ("SISA")) AND (ccc.id is null OR ddd.grup IN ("SISA")))) as BELUM_DISPATCH_AFTER,

      (select count(*) from Data_Pelanggan_Starclick aa
      LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
      LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
      LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
      LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
      LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
      where
      ee.witel = a.witel AND
      aa.jenisPsb LIKE "AO%" AND
      bb.id IS NOT NULL AND
      date(aa.orderDate) >= "2020-04-01" AND
      bb.tgl = "'.$tgl.'") as SUDAH_DISPATCH_AFTER,

      (select count(*) from psb_myir_wo aa
      LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
      LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
      where
      date(aa.orderDate) >= "2020-04-01" AND
      ff.orderId IS NULL AND
      aa.status_approval <> "2" AND
      bb.id IS NULL AND
      aa.sc IS NULL AND
      aa.layanan <> "PDA" AND
      ee.witel = a.witel) as BELUM_DISPATCH_PRA,

      (select count(*) from psb_myir_wo aa
      LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
      LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
      where
      date(aa.orderDate) >= "2020-04-01" AND
      ff.orderId IS NULL AND
      aa.status_approval <> "2" AND
      bb.id IS NOT NULL AND
      aa.sc IS NULL AND
      aa.layanan <> "PDA" AND
      bb.tgl = "'.$tgl.'" AND
      ee.witel = a.witel) as SUDAH_DISPATCH_PRA,

      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      c.`laporan_status`="HR" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.witel=a.witel) as HR_PRA,

      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      c.`grup`="OGP" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.witel=a.witel) as OGP_PRA,

      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan=45) AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.witel=a.witel) as NEED_PROGRESS_PRA,

      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl = "'.$tgl.'" AND
      c.`grup`="KT" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.witel=a.witel) as KT_PRA,

      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl = "'.$tgl.'" AND
      c.`grup`="KP" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.witel=a.witel) as KP_PRA,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      d.orderDatePs = "" AND
      c.`laporan_status`="HR" AND
      d.orderid IS NOT NULL AND
      d.orderStatusId NOT IN ("1500","7","1300","1205") AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as HR_AFTER,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl = "'.$tgl.'" AND
      d.orderDatePs = "" AND
      c.`grup`="KT" AND
      d.orderid IS NOT NULL AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as KT_AFTER,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl = "'.$tgl.'" AND
      d.orderDatePs = "" AND
      c.`grup`="KP" AND
      d.orderid IS NOT NULL AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as KP_AFTER,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      d.orderDatePs = "" AND
      c.`grup`="OGP" AND
      d.orderid IS NOT NULL AND
      d.orderStatusId NOT IN ("1500","7","1300","1205") AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as OGP_AFTER,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      d.orderDatePs = "" AND
      (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan=45) AND
      d.orderid IS NOT NULL AND
      d.orderStatusId NOT IN ("1500","7","1300","1205") AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as NEED_PROGRESS_AFTER,

      (SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      (aa.tgl = "'.$tgl.'" OR DATE(b.modified_at) = "'.$tgl.'") AND
      c.`grup`="UP" AND
      DATE(d.orderDatePs) >= "'.$tgl.'" AND
      d.orderid IS NOT NULL AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.witel=a.witel) as UP


      FROM
      maintenance_datel a
      where a.witel = "'.$witel.'"
      GROUP BY a.witel
      ');
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  public static function testAlpro(){

  }

  public static function cuaca(){

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://weatherbit-v1-mashape.p.rapidapi.com/current?lang=en&lon=114.589152&lat=-3.320817",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "x-rapidapi-host: weatherbit-v1-mashape.p.rapidapi.com",
        "x-rapidapi-key: 4e5f4ca5eamsh885e4ce86af7483p1edd82jsn1a8524f3e3ab"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  public static function get_progress_teknisi($start_date,$end_date){
    $query = DB::SELECT('select `a`.`orderId` AS `ORDER_ID`,`b`.`tgl` AS `TGL_DISPATCH`,`e`.`id_regu` AS `ID_REGU`,`e`.`crewid` AS `CREW_ID`,`e`.`uraian` AS `ALIAS`,`d`.`laporan_status_id` AS `ID_STATUS`,`d`.`laporan_status` AS `STATUS_TEKNISI`,`c`.`modified_at` AS `TGL_STATUS`,`c`.`catatan` AS `CATATAN` from ((((`t1`.`Data_Pelanggan_Starclick` `a` join `t1`.`dispatch_teknisi` `b` on((`a`.`orderId` = `b`.`Ndem`))) join `t1`.`psb_laporan` `c` on((`b`.`id` = `c`.`id_tbl_mj`))) join `t1`.`psb_laporan_status` `d` on((`c`.`status_laporan` = `d`.`laporan_status_id`))) join `t1`.`regu` `e` on((`b`.`id_regu` = `e`.`id_regu`))) WHERE b.tgl BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
    header("Content-Type: application/json");
    echo json_encode($query);
  }
  public static function get_progress_teknisi_pra($start_date,$end_date){
    $query = DB::SELECT('select `a`.`myir` AS `MYIR`,`b`.`tgl` AS `TGL_DISPATCH`,`e`.`id_regu` AS `ID_REGU`,`e`.`crewid` AS `CREW_ID`,`e`.`uraian` AS `ALIAS`,`d`.`laporan_status_id` AS `ID_STATUS`,`d`.`laporan_status` AS `STATUS_TEKNISI`,`c`.`modified_at` AS `TGL_STATUS`,`c`.`catatan` AS `CATATAN` from ((((`t1`.`psb_myir_wo` `a` join `t1`.`dispatch_teknisi` `b` on((`a`.`myir` = `b`.`Ndem`))) join `t1`.`psb_laporan` `c` on((`b`.`id` = `c`.`id_tbl_mj`))) join `t1`.`psb_laporan_status` `d` on((`c`.`status_laporan` = `d`.`laporan_status_id`))) join `t1`.`regu` `e` on((`b`.`id_regu` = `e`.`id_regu`))) where (isnull(`a`.`sc`) and (`a`.`status_approval` = 1)) AND b.tgl BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  public static function get_progress_by_myir($id){
    $query = DB::SELECT('
    select `a`.`myir` AS `MYIR`,`b`.`tgl` AS `TGL_DISPATCH`,`e`.`id_regu` AS `ID_REGU`,`e`.`crewid` AS `CREW_ID`,`e`.`uraian` AS `ALIAS`,`d`.`laporan_status_id` AS `ID_STATUS`,`d`.`laporan_status` AS `STATUS_TEKNISI`,`c`.`modified_at` AS `TGL_STATUS`,`c`.`catatan` AS `CATATAN` from ((((`t1`.`psb_myir_wo` `a` join `t1`.`dispatch_teknisi` `b` on((`a`.`myir` = `b`.`Ndem`))) join `t1`.`psb_laporan` `c` on((`b`.`id` = `c`.`id_tbl_mj`))) join `t1`.`psb_laporan_status` `d` on((`c`.`status_laporan` = `d`.`laporan_status_id`))) join `t1`.`regu` `e` on((`b`.`id_regu` = `e`.`id_regu`)))
    WHERE a.myir = "'.$id.'"
    ');
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  public static function get_progress_by_sc($id){
    $query = DB::SELECT('select `a`.`orderId` AS `ORDER_ID`,`b`.`tgl` AS `TGL_DISPATCH`,`e`.`id_regu` AS `ID_REGU`,`e`.`crewid` AS `CREW_ID`,`e`.`uraian` AS `ALIAS`,`d`.`laporan_status_id` AS `ID_STATUS`,`d`.`laporan_status` AS `STATUS_TEKNISI`,`c`.`modified_at` AS `TGL_STATUS`,`c`.`catatan` AS `CATATAN`,`c`.`typeont` AS `TIPE_ONT`,`c`.`snont` AS `SN_ONT`,`c`.`typestb` AS `TIPE_STB`,`c`.`snstb` AS `SN_STB`,`c`.`noPelangganAktif` AS `NO_PELANGGAN_AKTIF`,`c`.`snstb` AS `SN_STB`,`c`.`nama_odp` AS `NAMA_ODP`,`c`.`kordinat_pelanggan` AS `KOORDINAT_PELANGGAN`,`c`.`odp_label_code` AS `ODP_LABEL_CODE`,`c`.`dropcore_label_code` AS `DROPCORE_LABEL_CODE` from ((((`t1`.`Data_Pelanggan_Starclick` `a` join `t1`.`dispatch_teknisi` `b` on((`a`.`orderId` = `b`.`Ndem`))) join `t1`.`psb_laporan` `c` on((`b`.`id` = `c`.`id_tbl_mj`))) join `t1`.`psb_laporan_status` `d` on((`c`.`status_laporan` = `d`.`laporan_status_id`))) join `t1`.`regu` `e` on((`b`.`id_regu` = `e`.`id_regu`)))
    WHERE a.orderId = "'.$id.'"');
    header("Content-Type: application/json");
    echo json_encode($query);
  }


  public static function send_to_dalapa($id){
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL,"https://dalapa.id/api/dalapa/port");
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS);

    $get_sc = PsbModel::get_data_for_dalapa($id);

    if (count($get_sc)){
      $result = $get_sc[0];
      $curl = curl_init();
      $get_sc = json_encode($result);
      // print_r($get_sc);

      $kord = explode(',',$result->KORDINAT);
      if (count($kord)>1){
        $latitude = $kord[0];
        $longitude = $kord[1];
      } else {
        $latitude = '';
        $longitude = '';
      }
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://dalapa.id/api/dalapa/port",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
          "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0"
        ),
        CURLOPT_POSTFIELDS => array(
          'odp_location' => $result->ODP_LOCATION,
          'port_number' => $result->PORT_NUMBER,
          'latitude' => $latitude,
          'longitude' => $longitude,
          'sn_ont' => $result->SN_ONT,
          'nama_pelanggan' => $result->NAMA_PELANGGAN,
          'voice_number' => $result->VOICE_NUMBER,
          'internet_number' => $result->INTERNET_NUMBER,
          'date_validation' => $result->DATE_VALIDATION,
          'qr_port_odp' => $result->QR_PORT_ODP,
          'qr_dropcore' => $result->QR_DROPCORE,
          'nama_jalan' => $result->NAMA_JALAN,
          'contact' => $result->CONTACT,
          'type' => $result->TYPE
      )));

      $response = curl_exec($curl);

      curl_close($curl);

      $response = json_decode($response);
      print_r($response);

      if ($response<>""){
        if (property_exists($response,'status')){
        $update = DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
          'dalapa_status' => $response->status,
          'dalapa_message' => $response->message
        ]);
        }
        if (property_exists($response,'success')){
        $update = DB::table('psb_laporan')->where('id_tbl_mj',$id)->update([
          'dalapa_status' => $response->success,
          'dalapa_message' => implode($response->message)
        ]);
        }

      }


    } else {
      echo "DATA BELUM DI-SYNC";
    }

  }

  function workorder(){
    $mitra = Input::get('mitra');
    $tglps = Input::get('tglps');
    $witel = Input::get('witel');
    $datel = Input::get('datel');
    $sto    = Input::get('sto');
    $query = DashboardModel::provisioningListMitra_API($mitra,$tglps,$witel,$datel,$sto);
    header("Content-Type: application/json");
    echo json_encode($query);
  }

  public function getFoto($sc, $filename)
  {
    $data = DB::table('dispatch_teknisi')->where('NO_ORDER', $sc)->select('id')->first();
    $foto = str_replace("-kpro", "", $filename);

    $path1 = "/upload/evidence/$data->id";
    $kpro1   = "$path1/$foto";
    $path2 = "/upload2/evidence/$data->id";
    $kpro2   = "$path2/$foto";
    $path3 = "/upload3/evidence/$data->id";
    $kpro3   = "$path3/$foto";
    $path4 = "/upload4/evidence/$data->id";
    $kpro4   = "$path4/$foto";
    $path  = null;

    if(file_exists(public_path().$kpro1)){
        $path = "$path1/$foto";
    }elseif(file_exists(public_path().$kpro2)){
        $path = "$path2/$foto";
    }elseif(file_exists(public_path().$kpro3)){
        $path = "$path3/$foto";
    }elseif(file_exists(public_path().$kpro4)){
        $path = "$path4/$foto";
    }
    // dd($path);


    if(!empty($path) && file_exists(public_path().$path)){
      $imgx = new \Imagick(public_path().$path);
      $imgx->scaleImage(960, 1280, true);
      $imgx->writeImage(public_path().$path);
      header("Content-Type: image/jpeg");
      readfile(public_path().$path) or die('IMAGE FILE NOT FOUND');
    }else{
      echo "IMAGE FILE NOT FOUND";
    }

  }

  function foto_api_by_date(){
    $date_start = Input::get('date_start');
    $date_end = Input::get('date_end');
    $opsi = Input::get('opsi');
    $check = DB::connection('pg')->SELECT("
    SELECT
    a.order_id as ORDER_ID,
    a.create_dtm as ORDER_DATE,
    a.order_code as EXTERN_ORDER_ID,
    a.xn4 as NCLI,
    a.customer_desc as CUSTOMER_NAME,
    a.xs3 as WITEL,
    a.create_user_id as AGENT,
    a.xs2 as SOURCE,
    a.xs12 as STO,
    a.xs10 as SPEEDY,
    a.xs11 as POTS,
    a.order_status_id as STATUS_CODE_SC,
    a.xs7 as USERNAME,
    a.xs8 as ORDER_ID_NCX,
    c.xs11 as KCONTACT,
    b.xs3 as ORDER_STATUS
    FROM
      sc_new_orders a
    LEFT JOIN sc_new_order_activities b ON a.order_id = b.order_id AND a.order_status_id = b.xn2
    LEFT JOIN sc_new_order_details c ON a.order_id = c.order_id
    WHERE
      a.xs3 IN ('KALSEL','BALIKPAPAN') AND
      (date(a.create_dtm) BETWEEN '".$date_start."' AND '".$date_end."')
  ");
  // print_r(json_encode($check));
  $result = array();
  foreach ($check as $num => $result_x){
  $get = DB::SELECT('select * from dispatch_teknisi a left join psb_laporan pl ON a.id = pl.id_tbl_mj where pl.status_laporan = 1 AND a.Ndem = "'.$result_x->order_id.'"');
  if (count($get)>0){
    $get_result = $get[0];
    $result[$num]['status'] = "TRUE";
    $result[$num]['SC'] = $result_x->order_id;
    $result[$num]['kordinat_pelanggan'] = $get_result->kordinat_pelanggan;
    if ($opsi<>"kordinat_only"){
    foreach ($this->QC_foto as $foto){
      $result[$num][$foto] = "https://psb.tomman.app/foto/$result_x->order_id/".$foto.".jpg";
    }
    }
  }
  // else{
  //   $link = "https://biawak.tomman.app/get_sc_kalsel/$result_x->order_id";
	// 		$file = @file_get_contents($link);
	// 		$sc_kalsel = json_decode($file);
  //   	if (@property_exists($sc_kalsel[0],'kordinat_pelanggan')){
  //       $result[$num]['status'] = "TRUE";
  //       $result[$num]['SC'] = $result_x->order_id;
  //   		$result[$num]['kordinat_pelanggan'] = @$sc_kalsel[0]->kordinat_pelanggan;
	// 	    foreach ($this->QC_foto as $foto){
	// 	      $result[$num][$foto] = "https://biawak.tomman.app/foto/$result_x->order_id/".$foto."-kpro.jpg";
	// 	    }
  //   	} else {
  //   		$result[$num]['status'] = "FALSE";
  //   	}
  // }
  }
  print_r(json_encode($result));
  }

  public function foto_api_new()
  {
    $id = Input::get('id');
    $key = Input::get('key');
    
    if ($key=="a2ed39c417316adbd5cd1d0211a5d711")
    {
      $result = array();

      $data = ApiModel::fotoQCBorneo($id);

      if (count($data)>0)
      {
        if ($data->witel == 'KALSEL')
        {
          $result['status'] = 'TRUE';
          $result['kordinat_pelanggan'] = @$data->kordinat_pelanggan;
          foreach ($this->QC_foto as $foto)
          {
            if ($foto == "Profile_MYIH")
            {
              $result[$foto] = "https://biawak.tomman.app/foto/$id/".$foto.".jpg";
            } else {
              $result[$foto] = "https://biawak.tomman.app/foto/$id/".$foto."-kpro.jpg";  
            }
          }
        } elseif ($data->witel == 'BALIKPAPAN') {
          $result['status'] = 'TRUE';
          $result['kordinat_pelanggan'] = $data->kordinat_pelanggan;
          foreach ($this->QC_foto as $foto)
          {
            $result[$foto] = "https://psb.tomman.app/foto/$id/".$foto."-kpro.jpg";
          }
        }
      } else {
        $result['status'] = 'FALSE';
      }
      print_r(str_replace("\\","",json_encode($result)));
    }
  }

  function status_order(){
    $key = "EpwvzxlvDpDDtEtxptmmADulJl";
    $result = array();

    $id = Input::get('id');
    $key_get = Input::get('key');
    if ($key == $key_get){
    $get = DB::SELECT('select
    a.Ndem as id,
    c.laporan_status as status_order,
    b.modified_at as time
    from dispatch_teknisi a
    left join psb_laporan b On a.id = b.id_tbl_mj
    left join psb_laporan_status c ON b.status_laporan = c.laporan_status_id
    WHERE a.Ndem = "'.$id.'" ');
    if ($get){
      $status = "TRUE";

    } else {
      $status = "FALSE";
    }
    array_push($result,array('status' => $status));
    array_push($result,array('data' => $get));
  } else {
    $status = "FALSE";
    array_push($result,array('status' => $status));
  }

    $result = json_encode($result);
    print_r($result);
  }

  public function bulkWhatsapp()
  {  
    $date = date('Y-m-d');
    
    $log_terkirim = DB::table('whatsapp_api_log')->where('response','Successfully Sent')->whereDate('send_at', $date)->get();
    $log_gagal = DB::table('whatsapp_api_log')->where('response','The Number in Not Registered WhatsApp')->whereDate('send_at', $date)->get();

    $raport = ApiModel::raportMessageWhatsapp();
    $final_raport = [];
    foreach($raport as $k => $v)
    {
      $isi_data = json_decode(json_encode($v), TRUE);

      if(array_sum($isi_data) != 0)
      {
        $final_raport[$k]['id_sender'] = $v->id_sender;
        $final_raport[$k]['isi'] = json_decode(json_encode($v), TRUE);
      }
    }

    // $user_go = DB::table('1_2_employee')->where('status_telegram',1)->get();

    // $jml_terkirim = count($log_terkirim);
    // $jml_gagal = count($log_gagal);

    return view('api.bulkWhatsapp',compact('jml_terkirim', 'jml_gagal', 'date', 'user_go', 'final_raport'));
  }

  public function detailMessageWhatsapp()
  {
    $response = Input::get('response');
    $date = Input::get('date');
    $id_sender = Input::get('id_sender');

    $get_data = ApiModel::detailMessageWhatsapp($response, $date, $id_sender);

    return view('api.detailMessage', compact('response', 'date', 'id_sender', 'get_data'));
  }

  public function bulkWhatsappSaveMessage(Request $req){
    $this->validate($req,[
      'sender'    => 'required',
      'upload' => 'required'
    ],[
      'sender.required'     => 'ID Sender is Empty !!',
      'upload.required'  => 'File Upload is Empty !!'
    ]);

    Excel::load($req->file('upload'), function($reader) use($req) {
      $bulks = $reader->toArray();

      foreach($bulks as $bulk)
      {
        if ($bulk['numbers'] != null)
        {
          $sender = $req->input('sender');
          $var = array('(VAR1)', '(VAR2)', '(VAR3)', '(VAR4)');
          $bulk_var = array(@$bulk['var1'], @$bulk['var2'], @$bulk['var3'], @$bulk['var4']);
          $msg = str_replace($var, $bulk_var, $req->input('message'));

          ApiModel::sendWhatsapp($sender, $bulk['numbers'], $msg, "null", "message");
        }
      }

    });

    return redirect('/whatsapp-api')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUCCESS</strong> Send Message']
    ]);
  }

  public function bulkWhatsappSaveMedia(Request $req){
    $this->validate($req,[
      'sender'    => 'required',
      'caption'   => 'required',
      'url'       => 'required',
      'upload'    => 'required'
    ],[
      'sender.required'  => 'ID Device is Empty !!',
      'caption.required' => 'Caption is Empty !!',
      'url.required'     => 'URL Media is Empty !!',
      'upload.required'  => 'File Upload is Empty !!'
    ]);

    Excel::load($req->file('upload'), function($reader) use($req) {
      $bulks = $reader->toArray();
    
      foreach($bulks as $bulk)
      {
        if ($bulk['numbers'] != null)
        {
          $sender = $req->input('sender');
          $var = array('(VAR1)','(VAR2)','(VAR3)','(VAR4)');
          $bulk_var = array(@$bulk['var1'],@$bulk['var2'],@$bulk['var3'],@$bulk['var4']);
          $msg = str_replace($var,$bulk_var,$req->input('caption'));
          $url   = $req->input('url');

          ApiModel::sendWhatsapp($sender, $bulk['numbers'], $msg, $url, "media");
        }
      }

    });

    return redirect('/whatsapp-api')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUCCESS</strong> Send Message Media']
    ]);
  }

  public function dalapa_comparin_tr6(Request $req)
  {
    $wo_id      = $req->input('wo_id');
    $status     = $req->input('status');
    $note       = null;
    $auth       = "191fd7f49df2e3e04ed32a13e07ca77c9e8c9a98c9ba88617cf1de93fd22a5fefff5dadc56d926feab61bd0ccdffd5b28b7310f7d97d998e5b2f7d41c6124846";
    $result     = array();
    
    if ($wo_id <> null && $status <> null) {

      $note     = $req->input('note');

      if ($req->input('token') == $auth) {

        $check_data = DB::table('dalapa_comparin_tr6')->where('dalapa_wo_id', $wo_id)->first();
        $expl_wo_id = explode("-",$wo_id);
        $order_id   = $expl_wo_id[0];
        if(count($check_data)>0)
        {
  
          DB::transaction( function() use($wo_id,$order_id,$status,$note)
          {
            DB::table('dalapa_comparin_tr6')->where('dalapa_wo_id', $wo_id)->update([
              'dalapa_order_id'      => $order_id,
              'dalapa_wo_id'         => $wo_id,
              'dalapa_status'        => $status,
              'dalapa_note'          => $note
            ]);
          });

          $check_laporan = DB::table('dispatch_teknisi')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.Ndem', $order_id)->whereIn('psb_laporan.dalapa_checklist', [1,2])->select('dispatch_teknisi.id as id_dt','psb_laporan.catatan')->first();
          
          DB::transaction( function() use($check_laporan,$status,$note)
          {
            if (count($check_laporan)>0) {
              if ($status <> "VALID") {
  
                DB::table('psb_laporan')->where('id_tbl_mj', $check_laporan->id_dt)->update([
                  'status_laporan'    => 6,
                  'modified_at'       => date('Y-m-d H:i:s'),
                  'catatan'           => "[ HD TREG6 DALAPA : ".$note." (".date('Y-m-d H:i:s').") ]  ".$check_laporan->catatan
                ]);
  
                DB::table('dispatch_teknisi')->where('id', $check_laporan->id_dt)->update([
                  'tgl'               => date('Y-m-d'),
                  'updated_at'        => date('Y-m-d H:i:s')
                ]);
  
                $this->sendReportDalapa($order_id);
  
              } else {
  
                DB::table('psb_laporan')->where('id_tbl_mj', $check_laporan->id_dt)->update([
                  'modified_at'       => date('Y-m-d H:i:s'),
                  'catatan'           => "[ HD TREG6 DALAPA : ".$note." (".date('Y-m-d H:i:s').") ]  ".$check_laporan->catatan
                ]);
  
              }
  
              DB::table('dalapa_comparin_tr6')->where('dalapa_wo_id', $wo_id)->update([
                'dt_id'               => $check_laporan->id_dt
              ]);
  
            }
          });
          
        } else {

          DB::transaction( function() use($wo_id,$order_id,$status,$note)
          {
            DB::table('dalapa_comparin_tr6')->insert([
              'dalapa_order_id'      => $order_id,
              'dalapa_wo_id'         => $wo_id,
              'dalapa_status'        => $status,
              'dalapa_note'          => $note
            ]);
          });
  
          $check_laporan = DB::table('dispatch_teknisi')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.Ndem', $order_id)->whereIn('psb_laporan.dalapa_checklist', [1,2])->select('dispatch_teknisi.id as id_dt','psb_laporan.catatan')->first();

          if (count($check_laporan)>0) {
            if ($status <> "VALID") {

              DB::transaction( function() use($check_laporan,$note)
              {
                DB::table('psb_laporan')->where('id_tbl_mj', $check_laporan->id_dt)->update([
                  'status_laporan'    => 6,
                  'modified_at'       => date('Y-m-d H:i:s'),
                  'catatan'           => "[ HD TREG6 DALAPA : ".$note." (".date('Y-m-d H:i:s').") ] -- ".$check_laporan->catatan
                ]);
  
                DB::table('dispatch_teknisi')->where('id', $check_laporan->id_dt)->update([
                  'tgl'               => date('Y-m-d'),
                  'updated_at'        => date('Y-m-d H:i:s')
                ]);
              });

              $this->sendReportDalapa($order_id);

            } else {
              DB::table('psb_laporan')->where('id_tbl_mj', $check_laporan->id_dt)->update([
                'modified_at'       => date('Y-m-d H:i:s'),
                'catatan'           => "[ HD TREG6 DALAPA : ".$note." (".date('Y-m-d H:i:s').") ] -- ".$check_laporan->catatan
              ]);
            }

            DB::table('dalapa_comparin_tr6')->where('dalapa_wo_id', $wo_id)->update([
              'dt_id'               => $check_laporan->id_dt
            ]);

          }

        }
  
        $data                 = DB::table('dalapa_comparin_tr6')->where('dalapa_wo_id', $wo_id)->first();
        $result['status']     = "true";
        $result['message']    = "Success";
        $result['data']       = $data;
      } else {
        $result['status']     = "false";
        $result['message']    = "Anda tidak memiliki hak akses";
        $result['data']       = [];
      }
    } else {
      $result['status']       = "false";
      $result['message']      = "Field Kosong :(";
      $result['data']         = [];
    }
    return $result;
  }

  private function sendReportDalapa($order_id)
  {
    

    $get_data = ApiModel::dataReportDalapa($order_id);

    if (count($get_data)>0) {

      $data        = $get_data[0];

      // send report to group TL

      $messaggio   = "<b>⚠️ INVALID ORDER ⚠️</b>\n\n";
      $messaggio  .= "<b>ORDER ID :</b> <i>".$data->Ndem."</i> \n";
      $messaggio  .= "<b>WO ID :</b> <i>".$data->dalapa_wo_id."</i> \n";
      $messaggio  .= "<b>ORDER STATUS :</b> <i>".$data->orderStatus."</i> \n";
      $messaggio  .= "<b>STATUS TEKNISI :</b> <i>".$data->laporan_status."</i> \n";
      $messaggio  .= "<b>TIM :</b> <i>".$data->uraian."</i> \n";
      $messaggio  .= "<b>SEKTOR :</b> <i>".$data->sektor."</i> \n";
      $messaggio  .= "<b>TL :</b> <i>".$data->TL."</i> \n";
      $messaggio  .= "<b>STATUS HD TREG6 :</b> <i> ".$data->dalapa_status."</i> \n";
      $messaggio  .= "<b>CATATAN HD TREG6 :</b> \n\n";
      $messaggio  .= "<i>".$data->dalapa_note."</i> \n\n";
      $messaggio  .= "<i>cc : ".$data->TL_Username."</i> \n";

      Telegram::sendMessage([
        'chat_id' => '-547162761',
        'parse_mode' => 'html',
        'text' => $messaggio
      ]);

      print_r("Succes Send Report to Group");

      // send report to bot technition

      $get_tim = DB::table('1_2_employee')->whereIn('nik',[$data->nik1,$data->nik2])->select('nik','nama','chat_id')->get();

      if (count($get_tim)>0)
      {
        foreach($get_tim as $tim)
        {
          $messaggio   = "<b>⚠️ INVALID ORDER ⚠️</b>\n\n";
          $messaggio  .= "<b>ORDER ID :</b> <i>".$data->Ndem."</i> \n";
          $messaggio  .= "<b>WO ID :</b> <i>".$data->dalapa_wo_id."</i> \n";
          $messaggio  .= "<b>ORDER STATUS :</b> <i>".$data->orderStatus."</i> \n";
          $messaggio  .= "<b>STATUS TEKNISI :</b> <i>".$data->laporan_status."</i> \n";
          $messaggio  .= "<b>TIM :</b> <i>".$data->uraian."</i> \n";
          $messaggio  .= "<b>SEKTOR :</b> <i>".$data->sektor."</i> \n";
          $messaggio  .= "<b>TL :</b> <i>".$data->TL."</i> \n";
          $messaggio  .= "<b>STATUS HD TREG6 :</b> <i> ".$data->dalapa_status."</i> \n";
          $messaggio  .= "<b>CATATAN HD TREG6 :</b> \n\n";
          $messaggio  .= "<i>".$data->dalapa_note."</i> \n\n";
          $messaggio  .= "<i>cc : ".$tim->nama." / ".$tim->nik."</i> \n\n";
          $messaggio  .= "https://biawak.tomman.app/".$data->id_dt." \n";

          BotModel::sendMessageTommanGO($tim->chat_id,$messaggio);    
        }

        print_r("Succes Send Report to Technition");

      }
      
    } else {
      
      print_r("Data is Not Found!");

    }

  }

  public function broadcastBot(Request $req)
  {
    $this->validate($req,[
      'message'           => 'required'
    ],[
      'message.required'  => 'Message is Empty !!'
    ]);

    $get_data = DB::table('1_2_employee')->where('status_telegram',1)->get();

    foreach($get_data as $data)
    {
      BotModel::sendBroadcastTommanGO($data->chat_id,$req->input('message'));
    }

    return redirect('/whatsapp-api')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUCCESS</strong> Send Broadcast Message BOT']
    ]);
  }

  public function profileTeknisi()
  {

    $ket = Input::get('keterangan');
    $posisi = Input::get('posisi');

    if (in_array($ket,["data","absensi"]))
    {
      if (in_array($posisi,["PROV","IOAN"]))
      {
        $data = ApiModel::profileTeknisi($ket,$posisi);

        $result['status']     = "true";
        $result['message']    = "OK";
        $result['data']       = $data;
      }
      else
      {
        $result['status']     = "false";
        $result['message']    = "NOK";
        $result['data']       = [];
      }
    }
    else
    {
      $result['status']     = "false";
      $result['message']    = "NOK";
      $result['data']       = [];
    }

    return $result;

  }

  public static function bulkWhatsappProvi()
  {
    $whatsapp = DB::table('whatsapp_api_login')->where('divisi', 'provisioning')->first();
      
      if ($whatsapp->status == 1)
      {
        $data = DB::select('
          SELECT
          pl.noPelangganAktif,
          pls.laporan_status,
          dps.orderName,
          dps.orderCity,
          dps.internet
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order = "SC"
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE
          (DATE(dps.orderDatePs) = "' . date('Y-m-d', strtotime("-1 days")) . '") AND
          dps.orderStatus = "COMPLETED" AND
          dps.jenisPsb LIKE "AO%" AND
          pl.noPelangganAktif IS NOT NULL
        ');

        foreach ($data as $value)
        {
          $msg = 'Pelanggan Yth, 

Selamat Layanan IndiHome nya Sudah Terpasang, dengan detail data berikut :

Nama : ' . $value->orderName . '
No Internet : ' . $value->internet . '
Alamat Pemasangan : ' . $value->orderCity . '

Untuk Layanan yang Lebih Baik Kedepan nya, Tolong Bantu Infokan ke Kami ya Darimana Dapat Informasi Terkait Pemasangan IndiHome nya di Link Dibawah. 

https://forms.gle/yHJPhccfbQzQMoPR8 

Sebagai Informasi juga Jika Dikemudian Hari Ada Terjadi Gangguan Silahkan Hubungi Hotline Customer Service Kami di bawah ya gan 

https://wa.me/6281254284618

Terimakasih, Salam Hangat Kami INDIHOME BANJARMASIN';
          ApiModel::sendWhatsapp($whatsapp->sender, $value->noPelangganAktif, $msg, "null", "message");

          print_r("\n send to $value->noPelangganAktif \n");
        }
      } else {
        print_r("Whatsapp API belum login :(");
      }
  }

  public static function orderPs3Month()
  {
    $data = ApiModel::orderPs3Month();

    if (count($data) > 0)
    {
      $result['status'] = true;
      $result['message'] = "OK";
      $result['rows'] = count($data);
      $result['data'] = $data;
    } else {
      $result['status'] = false;
      $result['message'] = "NOK";
      $result['rows'] = 0;
      $result['data'] = [];
    }

    return $result;
  }

  public static function sendWhatsapp(Request $req)
  {
    $id_sender = $req->input('id_sender');
    $numb_hp = $req->input('numb_hp');
    $message = $req->input('message');
    // dd($req->input('id_sender'), $req->input('numb_hp'), $req->input('message'));

    if (!empty($id_sender) || !empty($numb_hp) || !empty($message))
    {
      $response = ApiModel::sendWhatsapp($id_sender, $numb_hp, $message, "null", "message");

      print_r(json_encode($response));
    } else {
      print_r("false!");
    }
  }

  public static function bulkUploadPickupOnline()
  {
    $startdate = date('Y-m-d', strtotime('-30 days'));
    $enddate = date('Y-m-d');

    $data = DB::table("Data_Pelanggan_Starclick")
    ->whereRaw("
      orderStatusId != 1500 AND
      SUBSTRING_INDEX(jenisPsb, '|', 1) IN ('AO', 'MO', 'PDA', 'PDA LOCAL') AND
      kcontact NOT LIKE '%WMS%' AND
      (DATE(orderDate) BETWEEN '$startdate' AND '$enddate')
    ")
    ->orderBy("orderDate", "DESC")
    ->get();

    foreach ($data as $value)
    {
      ApiModel::uploadPickupOnline($value->orderIdInteger);

      print_r("\n$value->orderIdInteger\n");
    }

    dd("finish upload pickup online!");
  }

  public static function refresh_valins()
  {
    $valins = DB::table('cookie_systems')->where('application', 'valins')->where('witel', 'KALSEL')->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://valins.telkom.co.id/home.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: '.$valins->cookies
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    print_r($response);
  }

  public static function checkValins($id)
  {
    $valins = DB::table('cookie_systems')->where('application', 'valins')->where('witel', 'KALSEL')->first();

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://valins.telkom.co.id/home.php?page=PG'.date('ymd').'103',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => array('id_summary' => $id),
      CURLOPT_HTTPHEADER => array(
        'Cookie: '.$valins->cookies
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    if (strpos($response, "login.php"))
    {
        $result['status']     = false;
        $result['message']    = 'Gagal login ke Valins ID!';
    } else {
      $dom = @\DOMDocument::loadHTML(trim($response));
      $table = $dom->getElementsByTagName('div')->item(34);

      if ($table != null)
      {
        $result['status']     = true;
        $result['message']    = $table->nodeValue;
      } else {
        $result['status']     = false;
        $result['message']    = 'Valins ID yang anda masukan tidak valid!';
      }

    }
    
    return $result;
  }

  public static function bulkCheckValins()
  {
    $valinss = ['1357656','12553983','15510873','15735240','16409264','17055171','17067935','17124130','17132444','17174678','17180298','17250000','17269387','17317022','17344227','17368750','17472235','17490632','17553090','16478215','17100642','14529201','16688193','1120118','16589456','17111314','17096617','17113175','17173670','17144879','15843771','13419934','16922356','17221076','17117948','16329488','11161638','10509543','13872097','14876424','17172725','17196000','14772323','17196068','11400993','14840153','15681638','17227130','15091842','17276898','14121562','17165120','13793322','1965553','17319142','16336976','17351501','17358320','17438490','17429207','17425176','17480657','17461644','17471254','17483406','17433330','15387509','17190700','17485200','17503449','17355246','17245284','17515883','16229361','13903577','16852959','12603784','13574648','17300517','17316508','17556762','11163266','2173099','15710535','17572567','17570135','17059722','14893937','14421457','17068552','17091101','17090991','16370983','1357656','15794455','14203709','16294339','17113361','17070844','17117007','17118009','17095443','17117330','15660061','17122235','17112235','17129552','17130216','17116751','17119272','15427654','17111062','17130460','17148823','17144015','17131244','17136764','15956854','15040444','14821333','17153361','17110146','16531404','17154766','17162917','17154319','17149944','17153751','16899834','17177189','16941960','17068876','16807683','17186677','17196866','17203836','17202687','16669825','17195843','17199006','17203177','17107371','17204791','1160970','11501091','17218667','17200949','17219526','17157624','17217549','17185811','17218458','14597026','17210344','17218728','17228574','15978168','17246006','17222776','17245580','13988560','17234757','15599955','17174678','17238938','14391688','17248874','17250719','15735240','17260109','15653039','16850087','17271818','17250192','17247372','16730589','11474709','17284402','17283897','17278312','17286803','17259188','14574072','15388477','17281072','13473801','17263519','17286466','14771682','17301460','17287112','17264892','17304012','17296279','17299393','17301834','17295772','17308350','17312436','17304198','17283179','17262089','14046784','17296742','17324742','17327508','17309271','17319085','17323557','17329240','17318854','17302062','17264688','17342661','17337618','1324356','17319747','17340183','16398741','17361790','17366151','17366299','17357773','17367101','17366953','17368820','17358723','17372102','17372200','17373360','17374498','17370950','17374481','17372895','17322405','17268868','17366563','12923522','11097553','17356857','17345207','17355790','14132206','17374406','17387712','17386488','17356971','17367333','16556263','13521949','15289762','16481220','17388060','17387146','17408342','17387133','17383684','17427531','15879374','17431953','14942878','17490061','17400360','17422505','17437415','17439062','17446830','17436722','17446527','17449490','17427425','17451425','17444854','14044637','14237197','17445787','17435583','17450175','12532908','16969659','16887156','13837593','17451231','17436102','17472287','17449039','17479944','17471915','17481700','17453804','17441516','17485123','17471808','17489177','17473225','17489360','17502221','17502049','17495390','11890187','11099785','17504946','17495208','17493376','17496230','17496090','17497131','17420205','17484927','17500952','17502422','17489878','17490708','17515255','17503230','17503844','17518078','17520793','17489749','17514222','17498798','17523380','17535071','11115642','13493151','17536448','17537988','17523295','17533523','17522533','17550732','17551080','17532952','17557893','17557875','17516746','17409112','17489422','17411556','17560224','17537863','17499782','15276826','14602802','17541771','17541152','17372723','17552433','17577190','17549755','17551572','17566059','17528174','17528564'];

    foreach ($valinss as $idvalins)
    {
      DB::table('bank_valins')->where('id_valins', $idvalins)->delete();
      DB::table('bank_valins_log')->where('id_valins', $idvalins)->delete();

      $check = self::checkValins($idvalins);
      $get_data = substr($check['message'], (strpos($check['message'], 'port_odp') + 9) );
      
  
      $get_time = trim(substr($check['message'], (strpos($check['message'], 'Time:') + 5) ) );
      $get_time = substr($get_time, 0, 19);
  
      $get_odp = trim(substr($check['message'], (strpos($check['message'], 'ODP:') + 5) ) );
      $split_get_odp = explode(' ', $get_odp)[0];
      $split_get_odp = explode('/', $split_get_odp);
      $odp = '';
  
      if(count($split_get_odp) == 2)
      {
        $odp = $split_get_odp[0] .'/'. sprintf("%03d", (int)$split_get_odp[1]);
      }
      $explode_new_line = explode(PHP_EOL, $get_data)[0];
      $explode_new_line2 = explode('ODP Power Checking', $explode_new_line)[0];
      $explode_new_line3 = explode('|', $explode_new_line2);
      $result_new = array_chunk($explode_new_line3, 5);
  
      unset($result_new[0]);
  
      foreach($result_new as $k => $v)
      {
        $insert[] = [
          'app'          => null,
          'id_wo'        => 0,
          'jenis_valins' => 0,
          'port_odp'     => $k,
          'id_valins'    => $idvalins,
          'time_valins'  => $get_time,
          'odp'          => $odp,
          'onu_id'       => trim($v[0]),
          'onu_sn'       => trim($v[1]),
          'sip1'         => trim($v[2]),
          'sip2'         => trim($v[3]),
          'no_hsi1'      => trim($v[4]),
          'created_by'   => 20981020
        ];
      }

      print_r("success valins $idvalins\n");
    }

    DB::table('bank_valins')->insert($insert);
    DB::table('bank_valins_log')->insert($insert);
  }

  public static function validateValins($app, $idwo, $status, $inet, $odp, $idvalins)
  {
    $check = self::checkValins($idvalins);

    if ($check['message'] != "Gagal login ke Valins ID!")
    {
      $message = $insert = [];

      if (strpos($check['message'], "Valins ID yang anda masukan tidak valid!"))
      {
        $message['type'] = 'danger';
        $message['text'] = 'Valins ID yang anda masukan tidak valid!';
      }
      if (!strpos($check['message'], $odp))
      {
        $message['type'] = 'danger';
        $message['text'] = 'Valins ID yang anda masukan tidak sesuai dengan ODP!';
      }
      if ($status == 24)
      {
        if (!strpos($check['message'], 'NONE'))
        {
          $message['type'] = 'danger';
          $message['text'] = 'Valins ID yang anda masukan terbaca masih ada port IDLE!';
        }
      }
  
      $get_data = substr($check['message'], (strpos($check['message'], 'port_odp') + 9) );
  
      $get_time = trim(substr($check['message'], (strpos($check['message'], 'Time:') + 5) ) );
      $get_time = substr($get_time, 0, 19);
  
      $get_odp = trim(substr($check['message'], (strpos($check['message'], 'ODP:') + 5) ) );
      $split_get_odp = explode(' ', $get_odp)[0];
      $split_get_odp = explode('/', $split_get_odp);
      $odp = '';
  
      if(count($split_get_odp) == 2)
      {
        $odp = $split_get_odp[0] .'/'. sprintf("%03d", (int)$split_get_odp[1]);
      }
      $explode_new_line = explode(PHP_EOL, $get_data)[0];
      $explode_new_line2 = explode('ODP Power Checking', $explode_new_line)[0];
      $explode_new_line3 = explode('|', $explode_new_line2);
      $result_new = array_chunk($explode_new_line3, 5);
  
      unset($result_new[0]);
  
      foreach($result_new as $k => $v)
      {
        // if ($inet != null)
        // {
        //   $id_inet = strval(trim($v[4]));
  
        //   if (!strpos($check['message'], $inet))
        //   {
        //     $message['type'] = 'danger';
        //     $message['text'] = 'Valins ID yang anda masukan tidak sesuai dengan No Internet!';
        //   }
        // }
        DB::Table('bank_valins')->where([
          ['app', $app],
          ['id_wo', $idwo]
        ])->delete();
  
        $insert[] = [
          'app'          => $app,
          'id_wo'        => $idwo,
          'jenis_valins' => 0,
          'port_odp'     => $k,
          'id_valins'    => $idvalins,
          'time_valins'  => $get_time,
          'odp'          => $odp,
          'onu_id'       => trim($v[0]),
          'onu_sn'       => trim($v[1]),
          'sip1'         => trim($v[2]),
          'sip2'         => trim($v[3]),
          'no_hsi1'      => trim($v[4]),
          'created_by'   => session('auth')->id_karyawan
        ];
      }
  
      DB::table('bank_valins')->insert($insert);
      DB::table('bank_valins_log')->insert($insert);
  
      return $message;
    }
  }

  public function get8pelanggan()
  {
    $type = Input::get('type');
    $sto = Input::get('sto');
    $lat = Input::get('lat');
    $long = Input::get('long');

    if (!in_array($type, ['', null]))
    {
      if ($type == 'order')
      {
        if (!in_array($lat, ['', null]) && !in_array($long, ['', null]))
        {
          $data = ApiModel::get8pelanggan($type, $sto, $lat, $long);

          $result['status']     = true;
          $result['code']       = 200;
          $result['rows']       = count($data);
          $result['data']       = $data;   
        } else {
          $result['status']     = false;
          $result['message']    = 404;
          $result['rows']       = 0;
          $result['data']       = [];
        }
      } elseif ($type == 'sto') {
        if (!in_array($sto, ['', null]))
        {
          $data = ApiModel::get8pelanggan($type, $sto, $lat, $long);

          $result['status']     = true;
          $result['code']       = 200;
          $result['rows']       = count($data);
          $result['data']       = $data;
        } else {
          $result['status']     = false;
          $result['message']    = 404;
          $result['rows']       = 0;
          $result['data']       = [];
        }
      }
    } else {
      $result['status']     = false;
      $result['message']    = 404;
      $result['rows']       = 0;
      $result['data']       = [];
    }

    return $result;
  }

  public function dorongPickupOnline()
  {
    $id = Input::get('id');

    $data = ApiModel::orderPickupOnline('DORONG', $id);

    if (count($data) > 0)
    {
      $postfields['source'] = 'nonbackend';

      $payload = new \stdClass();
      $payload->sc_id = $data[0]->sc_id;
      $payload->order_date = $data[0]->order_date;
      $payload->ps_date = $data[0]->ps_date;
      $payload->no_order = $data[0]->no_order;
      $payload->id_ncx = $data[0]->id_ncx;
      $payload->nama_pelanggan = $data[0]->nama_pelanggan;
      $payload->alamat_pelanggan = $data[0]->alamat_pelanggan ?: 'ALAMAT-';
      $payload->no_inet = $data[0]->no_inet;
      $payload->no_telp = $data[0]->no_telp;
      $payload->alpro = $data[0]->alpro ?: 'ODP-';
      $payload->witel = $data[0]->witel;
      $payload->kcontact = $data[0]->kcontact;
      $payload->status_sc = $data[0]->status_sc;
      $payload->user_id = $data[0]->user_id;
      $payload->layanan = $data[0]->layanan;
      $payload->jenis_transaksi = $data[0]->jenis_transaksi;
      $payload->id_sto = $data[0]->id_sto ?: 'STO';

      $postfields['data'] = [$payload];

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://tacticalpro.co.id/api/workorder/creates',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($postfields),
        CURLOPT_HTTPHEADER => array(
          'apisecret: bnB3WWhYSmE4ZmQvcnh6dnd6QnV1dz09',
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      $result = json_decode($response);
      
      DB::table('log_tacticalpro_tr6')->insert([
        'order_id'      => $id,
        'code'          => $result->code,
        'message'       => $result->message,
        'created_by'    => session('auth')->id_karyawan
      ]);

      if ($result->code == 200)
      {
        return redirect('/dispatch/search')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>BERHASIL</strong> Dorong Order ke TacticalPro']
        ]);
      } else {
        return redirect('/dispatch/search')->with('alerts', [
          ['type' => 'danger', 'text' => 'TacticalPro ....<br /><br />'.$result->message]
        ]);
      }

    } else {
      return redirect('/dispatch/search')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Order Tidak Ditemukan!']
      ]);
    }
  }

  public function update_jointer(Request $req)
  {
    $order = $req->input('order');

    $order_decode = json_decode($order);

    $check = DB::table('dispatch_teknisi as dt')
    ->leftJoin('psb_laporan as pl', 'dt.id', '=', 'pl.id_tbl_mj')
    ->leftJoin('psb_laporan_status as pls', 'pl.status_laporan', '=', 'pls.laporan_status_id')
    ->select('dt.*', 'pls.grup as grup_status', 'pls.laporan_status_id as id_status_teknisi', 'pls.laporan_status as status_teknisi', 'pl.nama_odp', 'pl.noPelangganAktif')
    ->whereIn('dt.NO_ORDER', $order_decode)
    ->get();

    if (count($check) > 0)
    {
      $orders = [];
      
      foreach ($check as $value2)
      {
        if (in_array($value2->grup_status, ['SISA', 'KT', 'KP']))
        {
          $orders[] = [
            'id_order' => $value2->id,
            'order' => $value2->NO_ORDER,
            'tgl_dispatch' => $value2->tgl,
            'jenis_order' => $value2->jenis_order,
            'grup_status' => $value2->grup_status,
            'id_status_teknisi' => $value2->id_status_teknisi,
            'status_teknisi' => $value2->status_teknisi,
            'odp_teknisi' => $value2->nama_odp,
            'no_hp_pelanggan' => $value2->noPelangganAktif,
            'response_at' => date('Y-m-d H:i:s')
          ];

          // DB::table('dispatch_teknisi')->where('id', $value2->id)->delete();

          DB::table('dispatch_teknisi')->where('id', $value2->id)->update([
            'tgl' => date('Y-m-d'),
            'updated_by' => 20981020,
            'updated_name' => 'API',
            'step_id' => '1.0'
          ]);

          DB::table('dispatch_teknisi_log')->insert([
            'updated_at'          => date('Y-m-d H:i:s'),
            'updated_by'          => 20981020,
            'tgl'                 => date('Y-m-d'),
            'id_regu'             => $value2->id_regu,
            'Ndem'                => $value2->Ndem,
            'NO_ORDER'            => $value2->NO_ORDER,
            'jenis_order'         => $value2->jenis_layanan
          ]);

          DB::table('psb_laporan')->where('id_tbl_mj', $value2->id)->update([
            'modified_at' => date('Y-m-d H:i:s'),
            'modified_by' => 20981020,
            'status_laporan' => 119 // PT2 OK
          ]);
        }
      }

      DB::table('api_jointer_psb')->insert($orders);

      $result['code'] = 200;
      $result['status'] = true;
      $result['data'] = $orders;

    } else {

      $result['code'] = 404;
      $result['status'] = false;
      $result['data'] = 'Order not found!';

    }

    return $result;
  }

  public static function get_ipayment()
  {
    $phone = Input::get('phone');

    $cookies = "";

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.60.165.60/script/intag_login.php?uid=20981020&pwd=Y2RlMGYw',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
    ));

    $response = curl_exec($curl);
    $header = curl_getinfo($curl);
    $header_content = substr($response, 0, $header['header_size']);
    trim(str_replace($header_content, '', $response));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);

    $cookies = implode("; ", $matches['cookie']);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.60.165.60/script/intag_search_trems.php?phone='.$phone.'&rname=&raddr=&rphone=&via=TREMS',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: '.$cookies
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    
    $template_html = '
    <!DOCTYPE html>
    <html>
      <head>
        <style>
          /* MENUBAR .......................................................................... */

          * html div#menubar {
            height: 1%;
          }

          div#menubar {
            width: 100%;
            height: 33px;
            margin: 0;
            background-image: url("menubar.jpg");
            background-repeat: repeat-x;
          }

          div#menubar ul {
            margin: 0;
            padding: 0;
            border-left: 1px solid RGB(0, 102, 167);
          }

          div#menubar li {
            float: left;
            list-style: none;
            margin: 0;
          }

          div#menubar a {
            color: silver;
            padding: 0.6em 1em 0.6em 1.4em;
            margin: 0;
            text-decoration: none;
            display: block;
          }

          div#menubar li a:hover {
            background-image: url("active-menubar.jpg");
            color: yellow;
          }

          div#menubar a.activeparent:hover {
            color: #18507c;
          }

          /* active parent, that is the first-level parent of a child page that is the current page */
          div#menubar li.activeparent a {
            color: silver;
          }

          div#menubar li.currentpage {
            color: Yellow;
            font-weight: bold;
            padding: 0.6em 1em 0.6em 1.4em;
            margin: 0;

            color: #fff;
          }

          div#menubar h3 {
            padding: 0.6em 1em 0.6em 1.4em;
            margin: 0;
            text-decoration: none;
            color: #fff;

            display: block;
            font-size: 1em;
          }

          /* TABLE ........................................................................ */

          td.longline {
            background-image: url("line.bmp");
            background-repeat: repeat-x;
          }

          td.top {
            background-image: url("top.bmp");
            height: 12px;
          }

          td.header {
            background-color: rgb(0, 102, 167);
            height: 90px;
          }

          td.center {
            background-image: url("center.bmp");
            background-repeat: repeat-y;
          }

          td.footer {
            color: silver;
            background-image: url("center.bmp");
            padding: 3px 3px 3px 3px;
          }

          td.bottom {
            background-color: rgb(0, 255, 255);
            height: 1px;
            background-image: url("bottom.bmp");
          }

          td.key {
            color: black;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
          }
          td.value {
            color: gray;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
          }

          th.grid {
            background-color: gray;
            color: white;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
            font-weight: normal;
          }
          td.grid {
            background-color: silver;
            color: black;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
          }

          th.mgrid {
            background-color: rgb(0, 102, 167);
            color: white;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
            font-weight: normal;
          }
          td.mgrid {
            background-color: rgb(121, 202, 255);
            color: black;
            padding: 5px 5px 5px 5px;
            line-height: 1.3em;
          }

          /* GENERAL ................................................................................ */

          * {
            margin: 0;
            padding: 0;
          }

          body {
            text-align: left;
            font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
            font-size: 10pt;
            background-color: white;
            color: black;

            margin: 1em;
          }

          img {
            border: 0;
          }

          a,
          a:link a:active {
            text-decoration: underline;
            background-color: inherit;
            color: #18507c;
          }

          a:visited {
            text-decoration: underline;
            background-color: inherit;
            color: #18507c;
          }

          a:hover {
            text-decoration: none;
            background-color: #c3d4df;
            color: #385c72;
          }

          /* BASIC LAYOUT */
          div#pagecontent {
            margin: 0 auto;
          }

          ndiv#content {
            margin: 1.5em auto 1em 0; /*some air above and under menu and content */
          }

          div#main {
            margin-left: 1%; /* this will give room for sidebar to be on the left side, make sure this space is bigger than sidebar width */
            margin-right: 1%; /* and some air on the right */
          }

          div#footer {
            clear: both; /*keep footer below content and menu */
            color: #fff;
            height: 70px;
          }

          div#footer p {
            font-size: 0.8em;
            padding: 1.5em; /* some air for footer */
            text-align: center; /* centered text */
            margin: 0;
          }

          div#footer p a {
            color: #fff; /* needed becouse footer link would be same color as background otherwise */
          }

          /* as we hid all hr for accessibility we create new hr with extra div element */
          div.hr {
            height: 1px;
            padding: 1em;
            border-bottom: 1px dotted black;
            margin: 1em;
          }

          hr.line {
            border-top: 1px silver;
          }

          /* CONTENT STYLING */
          div#content {
          }

          /* HEADINGS */
          div#content h1 {
            font-size: 2em; /* font size for h1 */
            line-height: 1em;
            margin: 0;
          }

          div#content h2 {
            color: black;
            text-align: center;
            padding-left: 0.5em;
            padding-bottom: 1px;
            border-bottom: 1px solid gray;
            font-size: 24px;
            line-height: 36px;
            font-weight: normal;
            margin: 0 0 0.5em 0;
          }

          div#content h3 {
            color: gray;
            font-size: 1.4em;
            line-height: 1.3em;
            margin: 0 0 0.5em 0;
            font-weight: normal;
          }
          div#content h4 {
            color: gray;
            font-size: 1em;
            line-height: 1.3em;
            margin: 0 0 0.25em 0;
          }
          div#content h5 {
            color: #294b5f;
            font-size: 1.1em;
            line-height: 1.3em;
            margin: 0 0 0.25em 0;
          }
          h6 {
            color: #294b5f;
            font-size: 1em;
            line-height: 1.3em;
            margin: 0 0 0.25em 0;
          }
          /* END HEADINGS */

          /* TEXT */
          p {
            font-size: 1em;
            margin: 0 0 1.5em 0; /* some air around p elements */
            line-height: 1.4em;
            padding: 0;
          }

          blockquote {
            border-left: 10px solid #ddd;
            margin-left: 10px;
          }
          strong,
          b {
            /* explicit setting for these */
            font-weight: bold;
          }
          em,
          i {
            /* explicit setting for these */
            font-style: italic;
          }

          /* Wrapping text in <code> tags. Makes CSS not validate */
          code,
          pre {
            white-space: pre-wrap; /* css-3 */
            white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            word-wrap: break-word; /* Internet Explorer 5.5+ */
            font-family: "Courier New", Courier, monospace;
            font-size: 1em;
          }

          pre {
            border: 1px solid #000; /* black border for pre blocks */
            background-color: #ddd;
            margin: 0 1em 1em 1em;
            padding: 0.5em;
            line-height: 1.5em;
            font-size: 90%;
          }

          /* Separating the divs on the template explanation page, with some bottom-border */
          div.templatecode {
            margin: 0 0 2.5em;
          }

          /* END TEXT */

          /* LISTS */
          /* lists in content need some margins to look nice */
          div#main ul,
          div#main ol,
          div#main dl {
            font-size: 1em;
            line-height: 1.4em;
            margin: 0 0 1.5em 0;
          }

          div#main ul li,
          div#main ol li {
            margin: 0 0 0.25em 3em;
          }

          /* definition lists topics on bold */
          div#main dl dt {
            font-weight: bold;
            margin: 0 0 0 1em;
          }
          div#main dl dd {
            margin: 0 0 1em 1em;
          }

          div#main dl {
            margin-bottom: 2em;
            padding-bottom: 1em;
            border-bottom: 1px solid #c0c0c0;
          }

          .title {
            color: white;
            font-size: 28px;
            line-height: 1em;
            padding-left: 20px;
            padding-top: 20px;
          }

          .subtitle {
            color: silver;
            padding-left: 21px;
            margin-top: -40px;
          }
        </style>
      </head>
      <body>
        <div>
          <table cellspacing="0" width="980px">
            <tr>
              <td class="top"></td>
            </tr>
            <tr>
              <td class="header">
                <p class="title">e-Info Payment</p>
                <p class="subtitle">Informasi tagihan jasa telekomunikasi</p>
              </td>
            </tr>

            <tr>
              <td class="center" valign="top">
                <div id="content">
                  <div id="main">
                    <div id="result">
                      '.$response.'
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td class="footer">&copy; 2008 PT Telkom</td>
            </tr>
            <tr>
              <td class="bottom"></td>
            </tr>
          </table>
        </div>
      </body>
    </html>';

    return $template_html;
  }

}