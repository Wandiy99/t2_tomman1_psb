<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use Mapper;
use DateTime;
use Telegram;
use Validator;
use App\DA\DashboardModel;
use App\DA\DispatchModel;
use App\DA\AmijaModel;
use App\DA\EmployeeModel;
use App\DA\MapModel;
class MonitoringController extends Controller
{
  public function provisioning($month){
    $exp = explode('-',$month);
    $maxDay = cal_days_in_month(CAL_GREGORIAN, $exp[1], $exp[0]);
    $get_re = array();
    $get_ps = array();
    $get_psre_completed = array();
    $teknisi = array();
    $teknisi_all = array();
    $get_re_all = DB::SELECT('
    SELECT
      DAY(ORDER_DATE) as tgl,count(*) as jumlah
    FROM
      selfie_kpro_tr6 a
    WHERE
      a.WITEL = "BANJARMASIN" AND
      date(a.ORDER_DATE) LIKE "'.$month.'%"
    GROUP BY DATE(a.ORDER_DATE)
    ');
    foreach ($get_re_all as $r){
      $get_re_all[$r->tgl] = $r->jumlah;
    }
    $get_ps_all = DB::SELECT('
    SELECT
      DAY(a.tgl_etat_dt) as tgl,count(*) as jumlah
    FROM
      prabac_psharian_indihome a
      LEFT JOIN maintenance_datel b ON a.sto = b.sto
    WHERE
      b.witel = "KALSEL" AND
      DATE(a.tgl_etat_dt) LIKE "'.$month.'%" AND
      a.status_order = "PS"
    GROUP BY
      DATE(a.tgl_etat_dt)
    ');
    $get_psre_completed_all = DB::SELECT('
    SELECT
      DAY(a.ORDER_DATE) as tgl,
      count(*) as jumlah
    FROM
      selfie_kpro_tr6 a
    WHERE
      a.WITEL = "BANJARMASIN" AND
      date(a.ORDER_DATE) LIKE "'.$month.'%" AND
      a.STATUS_MESSAGE = "Completed"
    GROUP BY DAY(a.ORDER_DATE)
    ');
    foreach ($get_psre_completed_all as $r){
      $get_psre_completed_all[$r->tgl] = $r->jumlah;
    }
    foreach ($get_ps_all as $r){
      $get_ps_all[$r->tgl] = $r->jumlah;
    }
    $get_datel = DB::SELECT('SELECT DATEL FROM selfie_kpro_tr6 where witel = "BANJARMASIN" group by DATEL');

    foreach ($get_datel as $num => $datel) {
      $get_psre_completed[$datel->DATEL] = DB::SELECT('
        SELECT
          DAY(a.ORDER_DATE) as tgl,count(*) as jumlah
        FROM
          selfie_kpro_tr6 a
        WHERE
         a.DATEL = "'.$datel->DATEL.'" AND
         date(a.ORDER_DATE) LIKE "'.$month.'%" AND
         a.STATUS_MESSAGE = "Completed"
         GROUP BY DATE(a.ORDER_DATE)
      ');

    $get_re[$datel->DATEL] = DB::SELECT('
      SELECT
        DAY(ORDER_DATE) as tgl,count(*) as jumlah
      FROM
        selfie_kpro_tr6 a
      WHERE
        a.DATEL = "'.$datel->DATEL.'" AND
        date(a.ORDER_DATE) LIKE "'.$month.'%"
      GROUP BY DATE(a.ORDER_DATE)
    ');
    foreach ($get_psre_completed[$datel->DATEL] as $r){
      $get_psre_completed[$datel->DATEL][$r->tgl] = $r->jumlah;
    }
    foreach ($get_re[$datel->DATEL] as $r){
      $get_re[$datel->DATEL][$r->tgl] = $r->jumlah;
    }
    $get_ps[$datel->DATEL] = DB::SELECT('
    SELECT
      DAY(a.tgl_etat_dt) as tgl,count(*) as jumlah
    FROM
      prabac_psharian_indihome a
      LEFT JOIN maintenance_datel b ON a.sto = b.sto
    WHERE
      b.datel = "'.$datel->DATEL.'" AND
      DATE(a.tgl_etat_dt) LIKE "'.$month.'%" AND
      a.status_order = "PS"
    GROUP BY
      DAY(a.tgl_etat_dt)
    ');
    foreach ($get_ps[$datel->DATEL] as $r){
      $get_ps[$datel->DATEL][$r->tgl] = $r->jumlah;
    }
    for ($x=1;$x<=$maxDay;$x++){
      $query = DB::SELECT('select
      bb.id_regu
      from
      Data_Pelanggan_Starclick a
        LEFT JOIN maintenance_datel b ON a.sto = b.sto
        left join dispatch_teknisi bb on a.orderIdInteger = bb.NO_ORDER
      where
      b.datel = "'.$datel->DATEL.'" AND
      bb.tgl = "'.$month.'-'.$x.'" AND
      a.jenisPsb LIKE "AO%"

      group by bb.id_regu');
      $teknisi[$datel->DATEL][$x] = count($query);
    }
    }
    for ($x=1;$x<=$maxDay;$x++){
      $query = DB::SELECT('select
      bb.id_regu
      from
      Data_Pelanggan_Starclick a
        LEFT JOIN maintenance_datel b ON a.sto = b.sto
        left join dispatch_teknisi bb on a.orderIdInteger = bb.NO_ORDER
      where
      b.witel = "KALSEL" AND
      bb.tgl = "'.$month.'-'.$x.'" AND
      a.jenisPsb LIKE "AO%"
      group by bb.id_regu');
      $teknisi_all[$x] = count($query);
    }


    return view('monitoring.provisioning',compact('get_psre_completed_all','get_psre_completed','teknisi_all','teknisi','maxDay','month','get_re','get_datel','get_re_all','get_ps','get_ps_all'));
  }
}
