<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Telegram;
use App\DA\Tele;
use DB;
use App\DA\GendongModel;

class GendongController extends Controller
{
  protected $photoInputs = [
    'Kondisi_ODP','ODP','Barcode_ODP'
  ];

  private function handleFileUploadGendong($request, $id, $photo)
  {
    foreach($photo as $name) {
      $input = 'photo_'.$name;
      if ($request->hasFile($input)) {
        // dd($input);
        $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
        $path = public_path().'/'.$upload.'/spl_odp/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }

  public function send_to_telegramGroup($id, $foto, $ket){
    $auth = session('auth');
    $list = DB::select('
      SELECT *, js.splitter as spl_text
      FROM Splitter_Gendong sg
      LEFT JOIN jenis_splitter js ON sg.splitter = js.splitter_id
      WHERE sg.order_id = ?
      order by sg.order_id desc
    ',[ $id ])[0];

    date_default_timezone_set('Asia/Makassar');
    $reported_date = date('Y-m-d H:i:s');
    $messageio = "Reported ODP Splitter Gendong\n";
    $messageio .= "===========================\n";
    $messageio .= "Pengirim : ".$auth->id_user."-".$auth->nama."\n";
    $messageio .= "Reported Date : ".$reported_date."\n";
    $messageio .= "ODP : ".$list->nama_odp."\n";
    $messageio .= "Koordinat ODP : ".$list->koordinat_odp."\n";
    $messageio .= "Jenis Splitter : ".$list->spl_text."\n";
    $messageio .= "Catatan : ".$list->catatan_order."\n";

	$chatID = "-1001322438792";

   Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messageio
    ]);

    $photoInputs = $foto;
    for($i=0;$i<count($photoInputs);$i++) {
      $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
      $file = public_path()."/".$upload."/spl_odp/".$id."/".$photoInputs[$i].".jpg";
      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i],
          'photo' => $file
        ]);
      }
    }
  }

  public function dashboard(){
    $query = DB::SELECT('
        SELECT
        sg.order_id as sg_id,
        sg.nama_odp as odp,
        sg.koordinat_odp as k_odp,
        SUBSTR(sg.nama_odp,5,3) as sto,
        m.datel as datel,
        sg.catatan_order as catatan,
        sg.updated_at as updated_at,
        sg.user_renew as user,
        emp.nama as user_name,
        js.splitter as spl
        FROM Splitter_Gendong sg
        LEFT JOIN jenis_splitter js ON sg.splitter = js.splitter_id
        LEFT JOIN 1_2_employee emp ON sg.user_renew = emp.nik
        LEFT JOIN maintenance_datel m ON m.sto = SUBSTR(sg.nama_odp,5,3)
        ORDER BY sg.order_id
      ');
    return view('gendong.dashboard',compact('query'));
  }

  public function gendong(){

    $get_laporan_splitter = DB::SELECT('SELECT splitter_id as id, splitter as text from jenis_splitter where active = "1" ');

  return view('gendong.index',compact('get_laporan_splitter'));
}

  public function gendongSave(Request $req){
  	$this->validate($req,[
  		'catatan_order'		=>	'required',
  		'nama_odp'		=>	'required',
  		'koordinat_odp'	=>	'required',
  		'splitter'		=>	'required',
  		'photo_Kondisi_ODP'	=>	'required',
  		'photo_ODP'			=>	'required',
  		'photo_Barcode_ODP'	=>	'required'
  	],[
  		'catatan_order.required'	=>	'Isi Catatan !',
  		'nama_odp.required'	=>	'Isi Nama ODP !',
  		'koordinat_odp.required'	=>	'Isi Koordinat ODP !',
  		'splitter.required'	=>	'Isi Splitter !',
  		'photo_Kondisi_ODP.required'	=>	'Upload Foto Kondisi ODP !',
  		'photo_ODP.required'	=>	'Upload Foto ODP !',
  		'photo_Barcode_ODP.required'	=>	'Upload Foto Barcode ODP !'
  	]);

      $cekData = DB::table('Splitter_Gendong')->where('nama_odp',$req->nama_odp)->first();
      if(count($cekData)==0){
        $id = GendongModel::simpanGendong($req);
        $auth = session('auth');
        $photo = ['Kondisi_ODP','ODP','Barcode_ODP'];
        $this->handleFileUploadGendong($req, $id, $photo);
        $this->send_to_telegramGroup($id, $photo, '0');
      };
       return back()->with('alerts',[['type' => 'info', 'text' => 'Terimakasih Anda Sudah Melaporkan Kondisi ODP Ini :)']]);
      }
}