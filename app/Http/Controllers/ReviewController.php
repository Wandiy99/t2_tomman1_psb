<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Curl;
use DateTime;
use Telegram;
use Validator;
Use App\DA\AbsensiModel;
class ReviewController extends Controller
{
  function review($ndem){
    echo "INVALID LINK";
    // return view('review.home',compact('ndem'));
  }
  function save_review(Request $req){
    // $save = DB::table('review')->insertIgnore([
    //   'Ndem' => $req->input('Ndem'),
    //   'Catatan' => $req->input('catatan'),
    //   'Review' => $req->input('input-1')
    // ]);
    // return view('review.thanks',$req);
    echo "INVALID LINK";
  }

  function dashboard($tgl)
  {
    $get_review = DB::SELECT('
        SELECT 
        *,
        AVG(a.Review) as review,
        count(*) as jumlah
        FROM 
          review a 
        LEFT JOIN dispatch_teknisi b ON a.Ndem = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        WHERE
          b.jenis_order IN ("SC", "MYIR", "IN", "INT") AND
          d.title IS NOT NULL AND 
          a.date_created LIKE "'.$tgl.'%"
        GROUP BY d.chat_id
        ORDER BY review DESC
      ');

    $get_top_tek = DB::SELECT('
        SELECT 
        *,
        AVG(CASE WHEN a.Review <> "" THEN a.Review ELSE 3 END) as review,
        count(*) as jumlah
        FROM
        dispatch_teknisi aa
          LEFT JOIN review a ON aa.Ndem = a.Ndem
          LEFT JOIN regu b ON aa.id_regu = b.id_regu
          LEFT JOIN psb_laporan c ON aa.id = c.id_tbl_mj
          WHERE
          c.status_laporan = 1 AND
          a.date_created LIKE "'.$tgl.'%" 
        GROUP BY b.id_regu
        ORDER BY review DESC
        LIMIT 10
      ');

    $get_all_review = DB::SELECT('
      SELECT 
      avg(a.review) as rating,
      sum(case when a.review <4 then 1 else 0 end) as underrate,
      count(*) as jumlah 
      FROM review a
      WHERE a.date_created like "'.$tgl.'%"
      ');

    $query1 = DB::SELECT('
    SELECT 
    sum(A1) as A1, sum(A2) as A2, sum(A3) as A3, sum(A4) as A4, sum(A5) as A5,
    sum(B1) as B1, sum(B2) as B2, sum(B3) as B3, sum(B4) as B4, sum(B5) as B5
     FROM review
    ');
    return view('review.dashboard',compact('tgl','get_review','get_top_tek','get_all_review','query1'));
  }

  function dashboardListTekUnderrate($tgl){
    $get_review = DB::SELECT('
      SELECT 
        *,
        a.date_created
      FROM
       review a 
          LEFT JOIN regu b ON a.id_regu = b.id_regu
          LEFT JOIN group_telegram d ON b.mainsector = d.chat_id
          LEFT JOIN data_nossa_1_log e ON a.Ndem = e.Incident
          LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem = f.orderId
          WHERE
            a.review < 4 AND 
            a.date_created LIKE "'.$tgl.'%"
          ORDER BY a.date_created DESC 
      ');  
       return view('review.dashboardList',compact('get_review'));
 
  }

  function dashboardListTek($tgl,$id_regu){
    $get_review = DB::SELECT('
      SELECT 
        *,
        aa.Ndem,
        a.date_created
      FROM
        dispatch_teknisi aa
          LEFT JOIN review a ON aa.Ndem = a.Ndem
          LEFT JOIN regu b ON aa.id_regu = b.id_regu
          LEFT JOIN psb_laporan c ON aa.id = c.id_tbl_mj
          LEFT JOIN group_telegram d ON b.mainsector = d.chat_id
          LEFT JOIN data_nossa_1_log e ON aa.Ndem = e.Incident
          LEFT JOIN Data_Pelanggan_Starclick f ON aa.Ndem = f.orderId
          WHERE
          c.status_laporan = 1 AND
          aa.tgl LIKE "'.$tgl.'%" AND 
          aa.id_regu = "'.$id_regu.'"
      ');
    return view('review.dashboardList',compact('tgl','get_review','id_regu'));
  }

  function dashboardList($tgl, $chat_id)
  {
    switch ($chat_id) {
      case 'NONSEKTOR':
        $sektor = 'AND d.chat_id IS NULL';
        break;
      
      case 'ALL':
        $sektor = '';
        break;
      
      default:
        $sektor = 'AND d.chat_id = "'.$chat_id.'"';
        break;
    }
    $get_review = DB::SELECT('
        SELECT 
        *,
        a.date_created
        FROM 
          review a 
        LEFT JOIN dispatch_teknisi b ON a.Ndem = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        LEFT JOIN data_nossa_1_log e ON a.Ndem = e.Incident
        LEFT JOIN Data_Pelanggan_Starclick f ON a.Ndem = f.orderId
        LEFT JOIN psb_laporan g ON b.id = g.id_tbl_mj
        LEFT JOIN roc h ON b.Ndem = h.no_tiket
        WHERE
          b.jenis_order IN ("SC", "MYIR", "IN", "INT") AND
          d.title IS NOT NULL AND
          a.date_created LIKE "'.$tgl.'%"
          '.$sektor.'
      ');
    return view('review.dashboardList',compact('tgl','get_review'));
  }

  function check(){
    $query = DB::SELECT('
      SELECT
      *
      FROM
        review a
      LEFT JOIN dispatch_teknisi b ON a.Ndem = b.Ndem
      WHERE
        a.id_regu is NULL
    ');
    foreach ($query as $result) {
      $update = DB::table('review')->where('Ndem',$result->Ndem)
      ->update([
        'id_dt' => $result->id,
        'id_regu' => $result->id_regu
      ]);
      echo $result->Ndem."<br />";
    }
  }

  function ranking(){
    $query = DB::SELECT('
      SELECT
        b.uraian,
        AVG(CASE WHEN a.Review <> "" THEN a.Review ELSE 3 END) as Review,
        COUNT(*) as jumlah,
        aa.id_regu
      FROM
        dispatch_teknisi aa
      LEFT JOIN review a ON aa.Ndem = a.Ndem
      LEFT JOIN regu b ON aa.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON aa.id = c.id_tbl_mj
      WHERE
      c.status_laporan = 1 AND
      (DATE(aa.tgl) BETWEEN "2019-02-14" AND "'.date('Y-m-d').'")
      GROUP BY b.uraian
      ORDER BY Review DESC
    ');
    foreach ($query as $num => $result){
      echo "Rank ".++$num." | ".$result->uraian." | ".round($result->Review,2)." | ".$result->jumlah." | ".$result->id_regu."<br />";
    }
  }

  function review_2($id){
    $get_id = DB::table('dispatch_teknisi')->where('id',$this->decrypt($id))->get();
    if (count($get_id)>0){
      $ndem = $get_id[0]->Ndem;
      $id_regu = $get_id[0]->id_regu;
      $id_dt = $get_id[0]->id;
      return view('review.home',compact('ndem','id_regu','id_dt'));
    } else {
      echo "<h3>Invalid Link</h3>";
    }
  }
  function save_review_2(Request $req)
  {
    if ($req->input('A1')=="checked") { $A1 = 1; } else { $A1 = 0; }
    if ($req->input('A2')=="checked") { $A2 = 1; } else { $A2 = 0; }
    if ($req->input('A3')=="checked") { $A3 = 1; } else { $A3 = 0; }
    if ($req->input('A4')=="checked") { $A4 = 1; } else { $A4 = 0; }
    if ($req->input('A5')=="checked") { $A5 = 1; } else { $A5 = 0; }
  
    if ($req->input('B1')=="checked") { $B1 = 1; } else { $B1 = 0; }
    if ($req->input('B2')=="checked") { $B2 = 1; } else { $B2 = 0; }
    if ($req->input('B3')=="checked") { $B3 = 1; } else { $B3 = 0; }
    if ($req->input('B4')=="checked") { $B4 = 1; } else { $B4 = 0; }
    if ($req->input('B5')=="checked") { $B5 = 1; } else { $B5 = 0; }

    // dd($req->all(), $A1, $A2, $A3, $A4, $A5, $B1, $B2, $B3, $B4, $B5);
  
    DB::table('review')->insert([
      'Ndem' => $req->input('Ndem'),
      'Catatan' => $req->input('komentar'),
      'id_dt' => $req->input('id_dt'),
      'id_regu' => $req->input('id_regu'),
      'Review' => $req->input('input-1'),
      'A1' => $A1,
      'A2' => $A2,
      'A3' => $A3,
      'A4' => $A4,
      'A5' => $A5,
      'B1' => $B1,
      'B2' => $B2,
      'B3' => $B3,
      'B4' => $B4,
      'B5' => $B5
    ]);
    return view('review.thanks',$req);
  }

  function encrypt($id){
    echo "enkrip : ".$id."<br />";
    $en1 = str_replace('1','K',$id);
    $en2 = str_replace('2','a',$en1);
    $en3 = str_replace('3','N',$en2);
    $en4 = str_replace('4','t',$en3);
    $en5 = str_replace('5','u',$en4);
    $en6 = str_replace('6','r',$en5);
    $en7 = str_replace('7','J',$en6);
    $en8 = str_replace('8','f',$en7);
    $en9 = str_replace('9','X',$en8);
    $en0 = str_replace('0','Y',$en9);
    echo $en0."<br />";
  }

  function decrypt($id){
    $en1 = str_replace('K','1',$id);
    $en2 = str_replace('a','2',$en1);
    $en3 = str_replace('N','3',$en2);
    $en4 = str_replace('t','4',$en3);
    $en5 = str_replace('u','5',$en4);
    $en6 = str_replace('r','6',$en5);
    $en7 = str_replace('J','7',$en6);
    $en8 = str_replace('f','8',$en7);
    $en9 = str_replace('X','9',$en8);
    $en0 = str_replace('Y','0',$en9);
    return $en0;
  }

  function link(){
    //Contoh URL Panjang
    $url_panjang = 'http://tomman.info/asas';

    // Diisi dengan API Key yang telah sobat dapatkan tadi
    $apiKey = 'AIzaSyDUPW3xOSDQlFtdE1k2dUzqqLnhmIlqDyw';

    $postData = array('longUrl' => $url_panjang, 'key' => $apiKey);
    $jsonData = json_encode($postData);

    $curlObj = curl_init();

    curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key=' . $apiKey);
    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlObj, CURLOPT_HEADER, 0);
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
    curl_setopt($curlObj, CURLOPT_POST, 1);
    curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

    $response = curl_exec($curlObj);

    $json = json_decode($response);

    curl_close($curlObj);

    echo 'URL Panjang : '.$url_panjang;
    echo "<br>";
    print_r($json);
    // echo 'Hasil URL Yang Telah Diperpendek : '.$json->id;
    // return view('review.link');
  }

  public function followupList($date){
      $list = DB::table('review')
              ->leftJoin('regu','review.id_regu','=','regu.id_regu')
              ->whereDate('review.date_created',$date)
              ->get();

      return view('review.listUp',compact('list'));
  }

  public function followupDetail($id){
      return view('review.followup');
  }

  public function followupSave(Request $req, $id){
      DB::table('review')->where('id_dt',$id)->update([
          'followup'  => $req->followup
      ]);

      return redirect('/review/followup/'.date('Y-m-d'));
  }
}
