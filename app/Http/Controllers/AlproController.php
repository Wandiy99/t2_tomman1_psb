<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\HomeModel;
use App\DA\AlkersarkerModel;
use App\DA\AlproModel;
use DB;

class AlproController extends Controller
{

  public function dashboard(){
    $nik = session('auth')->id_karyawan;
    $data = AlproModel::dashboard();
    $all_alpro = AlproModel::all_alpro('KALSEL');
    $all_alpro_booked = AlproModel::all_alpro_owner('BOOKED');
    $chatIdd = AlproModel::getChartId($nik);
    $chatId  = '';
    if(!empty($chatIdd)){
      $chatId  = $chatIdd[0]->chat_id;
    };

    return view('alpro.dashboard',compact('nik','data','all_alpro','all_alpro_booked'));
  }

  public function add(){
    $nik = session('auth')->id_karyawan;
    $get_alproname = array();
    $chatIdd = AlproModel::getChartId($nik);
    $chatId  = '';
    if(!empty($chatIdd)){
      $chatId  = $chatIdd[0]->chat_id;
    };

    return view('alpro.add',compact('nik','get_alproname'));
  }

  public function post(Request $req){
    $nik = session('auth')->id_karyawan;
    $get_alproname = AlproModel::get_alproname($req);
    $chatIdd = AlproModel::getChartId($nik);
    $chatId  = '';
    if(!empty($chatIdd)){
      $chatId  = $chatIdd[0]->chat_id;
    };
    
    return view('alpro.add',compact('nik','get_alproname'));
  }

  public function book(Request $req){
    $book = $req->input('book');
    $nik = $req->input('nik');
    $chatIdd = AlproModel::getChartId($nik);
    $chatId  = '';
    if(!empty($chatIdd)){
      $chatId  = $chatIdd[0]->chat_id;
    };

    $book_alproname = AlproModel::book_alproname($book,$nik,$chatId);
    if ($book_alproname){
      return redirect('/alpro')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES melakukan Booking Alpro. Silahkan tunggu Approval SM ASSURANCE</strong>']
      ]);
    } else {
      return redirect('/alpro')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL melakukan Booking Alpro. Silahkan hubungi @mnorrifani</strong>']
      ]);
    }
  }

	public function alpro(){
		$nik = session('auth')->id_karyawan;
		$group_telegram = HomeModel::group_telegram($nik);
    $chatIdd = AlproModel::getChartId($nik);
    $chatId  = '';
    if(!empty($chatIdd)){
      $chatId  = $chatIdd[0]->chat_id;
    };
    $get_alpro = AlproModel::get_alpro($chatId);

		return view('alpro.list',compact('group_telegram','get_alpro','nik'));
	}
}
