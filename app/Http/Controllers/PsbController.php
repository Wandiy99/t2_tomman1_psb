<?php

namespace App\Http\Controllers;

use App\DA\ApiModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Telegram;
use DB;
use Validator;
use App\DA\PsbModel;
use App\DA\HomeModel;
use App\DA\QcondeskModel;
use App\DA\GrabModel;
use TG;

date_default_timezone_set('Asia/Makassar');

class PsbController extends Controller
{
  // protected $photoInputs = [
  //   'Foto_Action','Lokasi', 'ODP', 'Redaman_ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'LiveTV',
  //   'TVOD','RFC_Form', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Foto_Pelanggan', 'Pullstrap', 'Tray_Cable', 'K3','MyIndihome_Pelanggan','Login_MyIndihome','KLEM_S', 'LABEL','QC_1','QC_2','QC_3','Excheck_Helpdesk'
  // ];

  protected $photoInputs = [
    'Berita_Acara',
    'BA_Digital',
    'RFC_Form',
    'Profile_MYIH',
    'Surat_Pernyataan_Deposit',
    'Bukti_Pelanggan_Bayar_Deposit',
    'Proses_TTD_Surat_Pernyataan',
    'Lokasi',
    'Lokasi_1',
    'Lokasi_2',
    'Meteran_Rumah',
    'ID_Listrik_Pelanggan',
    'Foto_Pelanggan_dan_Teknisi',
    'ODP',
    'LABEL',
    'Redaman_ODP',
    'Redaman_Pelanggan',
    'SN_ONT',
    'SN_STB',
    'Live_TV',
    'TVOD',
    'Speedtest',
    'Telephone_Incoming',
    'Telephone_Outgoing',
    'Wifi_Analyzer',
    'Foto_Action',
    'K3',
    'Instalasi_Kabel',
    'Tray_Cable',
    'Pullstrap',
    'KLEM_S',
    'Excheck_Dalapa',
    'Excheck_Helpdesk',
    'Stiker_Hotline',
    'TV_Tuner',
    'Test_Pen',
    'Splicer',
    'Patchcore',
    'Stopper',
    'Breket',
    'Roset',
    'OTP',
    'PREKSO',
    'EVIDENCE_RNA_CONTACT_PELANGGAN'
  ];

  protected $photoReboundary = [
    'Lokasi_Pelanggan', 'Redaman_Ont', 'Berita_Acara', 'ODP_Baru', 'ODP_Lama', 'Label', 'Sn_Ont_Baru', 'Sn_Ont_Lama', 'Port_Odp_Lama', 'Action_Penurunan_Dropcore', 'Ex_Dropcore','Speedtest', 'Test_Live_TV'
  ];

  protected $photokpro = [
    'BA_Digital',
    'Berita_Acara',
    'Speedtest',
    'SN_ONT',
    'ODP',
    'Lokasi',
    'Telephone_Incoming',
    'Foto_Pelanggan_dan_Teknisi',
    'LABEL',
    'KLEM_S',
    'K3',
    'Redaman_ODP',
    'SN_STB',
    'Live_TV',
    'Profile_MYIH'
  ];

  protected $assuranceInputs = [
    'Foto_Action','Berita_Acara','Rumah_Pelanggan','Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK'
  ];

  protected $photoCommon = ['Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi'];
  protected $photoRemo = ['ODP-dan-Redaman-GRID', 'Sebelum', 'Progress', 'Sesudah', 'Pengukuran', 'Pelanggan_GRID', 'BA-Remo'];
  protected $photoOdpLoss = ['ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'];
  private $process;
	private $pipes;
	private $core;

  public function wo_ps_qc($datel,$date){
    $where_datel = ' AND aa.witel = "KALSEL"';
    if ($datel<>"ALL"){
      $where_datel = ' AND (aa.datel = "'.$datel.'" OR aa.sektor_prov = "'.$datel.'")';
  }
  $photoInputs = $this->photoInputs;
    $query = DB::SELECT('
    SELECT *,c.id as id_dt,dpsa.orderAddr as alamat, bb.alamatLengkap as alamat1,dpsa.orderDate,"OK" as ls,dpsa.orderName as customer,dpsa.jenisPsb as jenisLayanan, dpsa.kcontact as akcontack, aa.sto as esto FROM Data_Pelanggan_Starclick dpsa
    LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto
    LEFT JOIN psb_myir_wo bb ON dpsa.myir = bb.myir
    LEFT JOIN dispatch_teknisi b ON dpsa.orderId = b.Ndem
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN regu e ON b.id_regu = e.id_regu
    WHERE
    dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET")
    '.$where_datel.' AND ((dpsa.orderStatusId in ("1500","7")  AND DATE(dpsa.orderDatePs) = "'.$date.'" ) OR (DATE(dpsa.orderDate) >="2020-03-01" AND dpsa.orderStatusId IN ("1203","1401")))
    ');
    return view('dashboard.listOrder_QC',compact('datel','query','photoInputs'));
  }

// cek history teknisi dimatikan
  public function check_history(){
    $input = '';
    $alert = '';
    $alertText = '';
    $alert_asr = '';
    $alertText_asr = '';

    return view('psb.check_history',compact('input','alert','alertText','alert_asr','alertText_asr'));
  }
// check history teknisi
  public function exec_check_history(Request $req){
    $input = $req->input('input');
    $check_tomman = DB::SELECT('
      SELECT *,b.id as id FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
      WHERE
        a.ndemSpeedy LIKE "'.$input.'%" OR
        a.internet = "'.$input.'"
      ORDER by a.orderDate DESC
    ');
    if (count($check_tomman)>0){
      $alert = "success";
      $alertText = "Data ditemukan";
    } else {
      $alert = "danger";
      $alertText = "Data tidak ditemukan";
    }
    $check_tomman_asr = DB::SELECT('
      SELECT *,b.id as id FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
      WHERE
        a.Service_ID LIKE "%'.$input.'%"
    ');
    if (count($check_tomman_asr)>0){
      $alert_asr = "success";
      $alertText_asr = "Data ditemukan";
    } else {
      $alert_asr = "danger";
      $alertText_asr = "Data tidak ditemukan";
    }
    return view('psb.check_history',compact('input','alert','alertText','check_tomman','alert_asr','alertText_asr','check_tomman_asr'));
  }

  public function sendkprobySC(){
    $date = date('Y-m-d');
    return view('psb.sendkpro',compact('date'));
  }

  public function sendkprobySC_save(Request $req){
    $data = explode(",", $req->input('SC'));
    if (count($data)>0){
      for ($i=0;$i<count($data);$i++){
        echo $data[$i]."<br />";
        $sc = str_replace(array(" ","\n","\r","\r\n"), "", $data[$i]);
        $this->sendtobotkpro($sc);
        sleep(20);
      }
    } else {
      echo "NO DATA FOUND";
    }
  }

  public function sendkprodaily($sektor){
       ini_set('max_execution_time', '30000000000000');
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.date('Y-m-d').'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
        echo ++$num.". ";
        echo $result->orderId." | ".$result->id."<br />";
        $this->sendtobotkpro($result->orderId);
        sleep(20);
      }
    }

    public function sendbyfotoname($name,$sektor,$date){
      ini_set('max_execution_time', '30000000000000');
      $query = DB::SELECT('
        select
          *
        from
          Data_Pelanggan_Starclick a
        LEFT JOIN mdf b ON a.sto = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
        WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
      foreach ($query as $num => $result){
        echo ++$num.". ";
        echo $result->orderId." | ".$result->id."<br />";
        $this->sendtobotkpro2($result->orderId,$name);
        sleep(3);
      }
    }

  public function sendkprobydate($sektor,$date){
    ini_set('max_execution_time', '30000000000000');
    $query = DB::SELECT('
      select
        *
      from
        Data_Pelanggan_Starclick a
      LEFT JOIN mdf b ON a.sto = b.mdf
      LEFT JOIN dispatch_teknisi c ON a.orderId = c.Ndem
      WHERE b.sektor_asr = "'.$sektor.'" AND DATE(a.orderDatePs)="'.$date.'" AND c.Ndem <> ""');
    foreach ($query as $num => $result){
      echo ++$num.". ";
      echo $result->orderId." | ".$result->id."<br />";
      $this->sendtobotkpro($result->orderId);
      sleep(20);
    }
  }

  public function refixkordinat($id,$sc){
    $query = DB::SELECT('SELECT * FROM psb_laporan a WHERE a.id_tbl_mj = '.$id);
    if (count($query)>0) {
      $result = $query[0];
      $kordinat_lama = $result->kordinat_pelanggan;
      $cekpos = strpos($kordinat_lama,'ttp://maps.google.com/?q=');
      $cleantext = substr_replace($kordinat_lama,"",0,$cekpos+25);
      $cleantext = str_replace(" ","",$cleantext);
      $splitting = explode(",",$cleantext);
      echo "test..";
      if (count($splitting)>1){
        $kordinat_baru = $splitting[0].",".$splitting[1];
        DB::table('psb_laporan')->where('id_tbl_mj',$id)->update(['kordinat_pelanggan' => $cleantext]);
        TG::sendLocation('Kpro_Kordinat',$splitting[0],$splitting[1]);
        TG::sendMsg('Kpro_Kordinat',"@kprofoto_bot ".$sc);
      } else {
        $kordinat_baru = "INVALID";
      }
      $get_new_kordinat = DB::table('psb_laporan')->where('id_tbl_mj',$id)->get();
      $kordinat_baru = $get_new_kordinat[0]->kordinat_pelanggan;
      if ($get_new_kordinat[0]->sendtokpro==1){
        echo "unable to send";
      } else {
        echo "sending...";
        $splitting = explode(',',$kordinat_baru);
        if (count($splitting)>0){
          TG::sendLocation('@kprofoto_bot',$splitting[0],$splitting[1]);
          TG::sendMsg('@kprofoto_bot',$sc);
        }
      }
      echo $kordinat_baru." |<br />";
    }
  }

  public function batchrefixkordinat($periode){
    $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem WHERE a.orderDatePs LIKE "'.$periode.'%" AND b.id IS NOT NULL');
    foreach ($query as $result){
      echo "| $result->orderId | ";
      $this->refixkordinat($result->id,$result->orderId);
    }
  }

  public function sendtobotkpro2($id,$name){
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
      if (count($get_sc)>0){
        $table = DB::table('hdd')->get()->first();
        $upload = $table->public;
        $hdd = $table->active_hdd;
        echo "oke ".date('Y-m-d H:i:s');
        $path = '/mnt/'.$hdd.'/'.$upload.'/evidence/'.$id.'/'.$name.'.jpg';
        $path_kpro = '/mnt/'.$hdd.'/'.$upload.'/evidence/'.$id.'/'.$name.'-kpro.jpg';

        if (file_exists($path)) {
          if (file_exists($path_kpro)){
            $img = new \Imagick($path);

            $img->scaleImage(768, 1024, true);

            $watermark = new \Imagick('/mnt/hdd4/upload/watermark/watermark-1.png');
            $x = 0;
            $y = 0;
            $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
            $img->writeImage("/mnt/".$hdd."/".$upload."/evidence/".$id."/".$name."-kpro.jpg");
          }
          $foto = 'https://biawak.tomman.app/'.$upload.'/evidence/'.$id.'/'.$name.'-kpro.jpg';

          echo $foto;
          TG::sendMsg('@mnorrifani',$id);
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
          #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }
      }
  }

  public function sendtobotkpro($id){
    $get_sc = DB::SELECT('
      SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id = "'.$id.'"
    ');

    if (count($get_sc)==0){
      $get_sc = DB::SELECT('
        SELECT * FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.Ndem = "'.$id.'"
      ');

      if($get_sc){
          $id = $get_sc[0]->id;
      };
    };

    $result = $get_sc[0];
    foreach($this->photokpro as $name) {
      if (count($get_sc)>0){
        echo "oke ".date('Y-m-d H:i:s');
        $table = DB::table('hdd')->get()->first();
        $hdd = $table->active_hdd;
        $upload = $table->public;

        $path = '/mnt/'.$hdd.'/'.$upload.'/evidence/'.$id.'/'.$name.'.jpg';

        if (file_exists($path)) {
          $img = new \Imagick($path);

          $img->scaleImage(768, 1024, true);

          $watermark = new \Imagick('/mnt/hdd4/upload/watermark/watermark-1.png');
          $x = 0;
          $y = 0;
          $img->compositeImage($watermark,\Imagick::COMPOSITE_OVER, $x, $y);
          $img->writeImage("/mnt/".$hdd."/".$upload."/evidence/".$id."/".$name."-kpro.jpg");
          $foto = 'https://biawak.tomman.app/'.$upload.'/evidence/'.$id.'/'.$name.'-kpro.jpg';

          echo $foto;
          TG::sendMsg('@mnorrifani',$id);
          TG::sendPhoto('@kprofoto_bot', $foto,true,$result->orderId);
          #echo exec('/usr/share/tg/bin/telegram-cli -WR -e "send_photo @kprofoto_bot /mnt/hdd2/upload/evidence/'.$id.'/Lokasi.jpg ['.$result->orderId.'] "');
        }
      }
    }
  }

  public function batchtokpro($date){
    $get_sc = DB::SELECT('
      SELECT *,dt.id as id_dt FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id IS NOT NULL AND a.orderDatePs LIKE "'.$date.'%"
    ');
    foreach ($get_sc as $result){
      $id = $result->id_dt;
      $img = new \Imagick('/mnt/hdd4/upload/evidence/'.$id.'/Lokasi.jpg');
      $img->scaleImage(300, 450, true);
      $img->writeImage("/mnt/hdd4/upload/evidence/".$id."/Lokasi-kpro.jpg");
      TG::sendPhoto('@kprofoto_bot', 'https://biawak.tomman.app/upload4/evidence/'.$id.'/Lokasi-kpro.jpg',true,$result->orderId);

    }
  }

  public function mapping_order_cabutan(){
    $query = DB::SELECT('SELECT f.laporan_status,a.order_id, b.lat, b.lon, d.kordinat_pelanggan, b.orderDatePs,e.uraian FROM `cabut_nte_order` a left join Data_Pelanggan_Starclick b ON a.order_id = b.orderIdInteger left join dispatch_teknisi c ON b.orderIdInteger = c.NO_ORDER left join psb_laporan d ON c.id = d.id_tbl_mj left join dispatch_teknisi cc ON a.wfm_id = cc.NO_ORDER left join regu e ON cc.id_regu = e.id_regu left join psb_laporan dd ON cc.id = dd.id_tbl_mj left join psb_laporan_status f ON dd.status_laporan = f.laporan_status_id WHERE cc.jenis_order = "CABUT_NTE" AND (f.grup IN ("NP","OGP") OR f.laporan_status_id is NULL)');

    return view('dispatch.mapping_order',compact('query'));
  }

  public function vhaiklah($datel){
    $list = DB::SELECT('
SELECT 0 as jarak, a.*, b.*,
g.uraian,
f.laporan_status,
a.lat,a.lon, DATE(b.approve_date) as umurApprove, a.orderDate as umur, a.sto as STO,
a.orderDate
FROM Data_Pelanggan_Starclick a
LEFT JOIN psb_myir_wo b ON a.myir = b.myir AND YEAR(a.orderDate) >= "2020"
LEFT JOIN maintenance_datel c ON a.sto = c.sto
LEFT JOIN dispatch_teknisi d ON (a.orderId = d.Ndem OR d.Ndem = a.myir) AND YEAR(a.orderDate) >= "2020"
LEFT JOIN psb_laporan e ON d.id = e.id_tbl_mj
LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
LEFT JOIN regu g ON d.id_regu = g.id_regu
LEFT JOIN dispatch_teknisi dd ON b.sc = dd.Ndem
LEFT JOIN psb_laporan ee ON dd.id = ee.id_tbl_mj
LEFT JOIN psb_laporan_status ff ON ee.status_laporan = ff.laporan_status_id
WHERE
a.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
(f.grup IN ("SISA","OGP") OR d.id IS NULL) AND
(ff.grup IN ("SISA","OGP") OR dd.id IS NULL) AND
DATE(a.orderDate) >= "'.date('Y-m').'-01" AND
c.sto = "'.$datel.'" AND
a.jenisPsb LIKE "AO%"
ORDER BY a.orderDate
    ');
    $list_scbe = DB::SELECT('
      SELECT 0 as jarak, a.*, b.*, a.myir as ORDER_CODE,0 as umur, 0 as umurApprove,SUBSTR(a.namaOdp,5,3) as STO,g.uraian,f.laporan_status
      FROM psb_myir_wo a
      LEFT JOIN Data_Pelanggan_Starclick_Backend b ON a.myir = SUBSTR(b.ORDER_CODE,6,30)
      LEFT JOIN maintenance_datel c ON  SUBSTR(a.namaOdp,5,3) = c.sto
      LEFT JOIN dispatch_teknisi d ON a.myir = d.Ndem
      LEFT JOIN psb_laporan e ON d.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      LEFT JOIN regu g ON d.id_regu = g.id_regu
      WHERE
      (b.STATUS_RESUME IN ("open","booked") OR a.ket_input = 0) AND
      c.sto = "'.$datel.'" AND
      (f.grup IN ("SISA","OGP") OR d.id IS NULL)  AND
      a.sc IS NULL
      ORDER BY a.orderDate
    ');
    $lat = '-3.31987';
    $lon = '114.59075';
    return view('dispatch.nearest',compact('list','list_scbe','lat','lon'));
  }
  public function mapping_order($datel){
    $list = DB::SELECT('
SELECT 0 as jarak, a.*, b.*,
g.uraian,
f.laporan_status,
a.lat,a.lon, DATE(b.approve_date) as umurApprove, a.orderDate as umur, a.sto as STO,
a.orderDate
FROM Data_Pelanggan_Starclick a
LEFT JOIN psb_myir_wo b ON a.myir = b.myir AND YEAR(a.orderDate) >= "2020"
LEFT JOIN maintenance_datel c ON a.sto = c.sto
LEFT JOIN dispatch_teknisi d ON (a.orderId = d.Ndem OR d.Ndem = a.myir) AND YEAR(a.orderDate) >= "2020"
LEFT JOIN psb_laporan e ON d.id = e.id_tbl_mj
LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
LEFT JOIN regu g ON d.id_regu = g.id_regu
LEFT JOIN dispatch_teknisi dd ON b.sc = dd.Ndem
LEFT JOIN psb_laporan ee ON dd.id = ee.id_tbl_mj
LEFT JOIN psb_laporan_status ff ON ee.status_laporan = ff.laporan_status_id
WHERE
a.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
(f.grup IN ("SISA","OGP") OR d.id IS NULL) AND
(ff.grup IN ("SISA","OGP") OR dd.id IS NULL) AND
DATE(a.orderDate) >= "'.date('Y-m').'-01" AND
c.datel = "'.$datel.'" AND
a.jenisPsb LIKE "AO%"
ORDER BY a.orderDate
    ');
    $list_scbe = DB::SELECT('
      SELECT 0 as jarak, a.*, b.*, a.myir as ORDER_CODE,0 as umur, 0 as umurApprove,SUBSTR(a.namaOdp,5,3) as STO,g.uraian,f.laporan_status
      FROM psb_myir_wo a
      LEFT JOIN Data_Pelanggan_Starclick_Backend b ON a.myir = SUBSTR(b.ORDER_CODE,6,30)
      LEFT JOIN maintenance_datel c ON  SUBSTR(a.namaOdp,5,3) = c.sto
      LEFT JOIN dispatch_teknisi d ON a.myir = d.Ndem
      LEFT JOIN psb_laporan e ON d.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      LEFT JOIN regu g ON d.id_regu = g.id_regu
      WHERE
      (b.STATUS_RESUME IN ("open","booked") OR a.ket_input = 0) AND
      c.datel = "'.$datel.'" AND
      (f.grup IN ("SISA","OGP") OR d.id IS NULL)  AND
      a.sc IS NULL
      ORDER BY a.orderDate
    ');
    $lat = '-3.31987';
    $lon = '114.59075';
    $zoom = '12';
    return view('dispatch.nearest',compact('list','list_scbe','lat','lon','zoom'));
  }

  public function whereismytech(){
    $query = DB::SELECT('SELECT a.regu_nama,a.modified_at,a.startLatTechnition as lat,a.startLonTechnition as lon FROM `psb_laporan` a WHERE a.modified_at LIKE "'.date('Y-m-d').'%" AND a.startLatTechnition IS NOT NULL GROUP BY a.regu_nama ORDER BY a.modified_at DESC');
    return view('psb.whereismytech',compact('query'));
  }

  public function get_nearest_order($sc){
    $get_sc = DB::table('Data_Pelanggan_Starclick')->where('orderId',$sc)->get();
    $lat = '-3.31987';
    $lon = '114.59075';
    if (count($get_sc)>0){
      $lat = $get_sc[0]->lat;
      $lon = $get_sc[0]->lon;
        // echo "DATA DITEMUKAN DI STARCLICK<br />";
    }
    $get_sc_backend = DB::SELECT('
      SELECT
      *
      FROM
        Data_Pelanggan_Starclick_Backend a
      WHERE
        a.ORDER_CODE LIKE "%'.$sc.'"
      LIMIT 1
    ');
    if (count($get_sc_backend)>0){
      $lat = $get_sc_backend[0]->GPS_LATITUDE;
      $lon = $get_sc_backend[0]->GPS_LONGITUDE;
    }
    $list = DB::select("SELECT *,
        FORMAT(6371 * acos(cos(radians(".$lat."))
        * cos(radians(a.lat)) * cos(radians(a.lon)
        - radians(".$lon.")) + sin(radians(".$lat."))
        * sin(radians(a.lat))),3)
        AS jarak,
        a.myir,
        a.lat,
        a.lon,
        a.orderDate,
        a.orderDate as umur,
        g.approve_date as umurApprove
        FROM Data_Pelanggan_Starclick a
        LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem OR a.myir = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN Data_Pelanggan_Starclick_Backend d ON a.kcontact = d.kcontact
        LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
        LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
        LEFT JOIN psb_myir_wo g ON a.orderId = g.sc
        WHERE
          a.orderStatus IN ('OSS PROVISIONING ISSUED','Fallout (WFM)') AND
          YEAR(a.orderDate) >= '2020' AND
          a.jenisPsb LIKE '%AO%' AND
          a.orderId <> '".$sc."' AND
          MONTH(a.orderDate) >= 3
        HAVING jarak <= 1
        ORDER BY jarak");
    //   echo "Ditemukan ".count($list)." order, radius 1km dari lokasi ORDER SC.".$sc."<br />";
    // echo "DATA STARCLICK : <br />";
    // foreach ($list as $data){
    //   echo ($data->jarak*1000)."m | SC.".$data->orderId." | ".$data->orderStatus." | ".$data->ORDER_CODE." | ".$data->uraian." | ".$data->orderDate." | ".$data->orderAddr." | ".$data->orderStatus." | ".$data->lat.",".$data->lon."<br />";
    // }

    $list_scbe = DB::select("SELECT *,
        FORMAT(6371 * acos(cos(radians(".$lat."))
        * cos(radians(a.GPS_LATITUDE)) * cos(radians(a.GPS_LONGITUDE)
        - radians(".$lon.")) + sin(radians(".$lat."))
        * sin(radians(a.GPS_LATITUDE))),3)
        AS jarak,
        TIMESTAMPDIFF( DAY , bb.approve_date,  '".date('Y-m-d H:i:s')."' ) as umur,
        SUBSTR(a.ORDER_CODE,6,30) as myir,
        a.GPS_LATITUDE as lat,
        a.GPS_LONGITUDE as lon
        FROM Data_Pelanggan_Starclick_Backend a
        LEFT JOIN dispatch_teknisi b ON SUBSTR(a.ORDER_CODE,6,30)  = b.Ndem
        LEFT JOIN psb_myir_wo bb ON b.Ndem = bb.myir
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
        LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
        WHERE
          a.STATUS_RESUME IN ('Open','booked')
        HAVING jarak <= 1
        ORDER BY jarak");
    //   echo "Ditemukan ".count($list)." order, radius 1km dari lokasi ORDER SC.".$sc."<br />";
    // echo "DATA SCBE : <br />";
    //   foreach ($list_scbe as $data){
    //     echo ($data->jarak*1000)."m | ".$data->ORDER_CODE." | ".$data->uraian." | ".$data->ORDER_DATE." | ".$data->INS_ADDRESS." | ".$data->STATUS_RESUME." | ".$data->GPS_LATITUDE.",".$data->GPS_LONGITUDE."<br />";
    //   }
      $zoom = 13;
    return view('dispatch.nearest',compact('list','list_scbe','lat','lon','zoom'));
  }

  public function get_odp($id){
    $plg = DB::table("dispatch_teknisi")
      ->leftJoin("Data_Pelanggan_Starclick", "dispatch_teknisi.Ndem", "=", "Data_Pelanggan_Starclick.orderId")
      ->where("id", $id)->first();
    //dd($plg);

    $lon = $plg->lon;
    if ($plg->lon=='NaN' || $plg->lon==''){
      $lon = 0;
    };

    $lat = $plg->lat;
    if($plg->lat=='NaN' || $plg->lat==''){
      $lat = 0;
    };

    $list = DB::select("SELECT *,
        (6371 * acos(cos(radians(".$lat."))
        * cos(radians(latitude)) * cos(radians(longitude)
        - radians(".$lon.")) + sin(radians(".$lat."))
        * sin(radians(latitude))))
        AS jarak
        FROM odp_compliance
        HAVING jarak <= 3
        ORDER BY jarak
        LIMIT 3");
    //dd($list);
    return json_encode($list);
  }

  public function get_odp_point($lat,$lng){

    $list = DB::select("SELECT *,
        (6371 * acos(cos(radians(".$lat."))
        * cos(radians(latitude)) * cos(radians(longitude)
        - radians(".$lng.")) + sin(radians(".$lat."))
        * sin(radians(latitude))))
        AS jarak
        FROM odp_compliance
        HAVING jarak <= 0.2
        ORDER BY jarak
        LIMIT 3");
    //dd($list);
    return json_encode($list);
  }

  public function WorkOrder($id, Request $req){
    $bulan = date('Y-m');
    if ($req->has('bulan')){
        $bulan = $req->input('bulan');
    };
    $indexCount = "0";
    $today = date('Y-m-d');
    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->get();
    $list = PsbModel::WorkOrder($id,$bulan);

    // if ($id<>"UP") {
    //   $list = PsbModel::WorkOrder($id,$bulan);
    // } else {
    //   $list = array();
    // }

    $updatenote = PsbModel::updatenote();
    $checkONGOING = PsbModel::checkONGOING();
    $cekManja = PsbModel::WorkOrder('NEED_PROGRESS_MANJA', $today);
    if(count($cekManja)>0){
      foreach ($cekManja as $manja){
        $create_manja = date_create($manja->jam_manja);
        $jam_manja = date_format($create_manja, "H");
      }
    }else{
      $jam_manja = null;
    }
    $checkQCOndesk = QcondeskModel::checkQCOndesk();
    $list_ffg = PsbModel::WorkOrder('NEED_PROGRESS_FFG', $bulan);

    $status_absen = HomeModel::status_absen(session('auth')->id_user);
    if($status_absen==null){
      return redirect('/home  ')->with('alerts', [
      ['type' => 'danger', 'text' => '<strong>ABSEN</strong> DULU YA :) ...']]);
    }
    $statusProv = HomeModel::cekStatusTekProv($status_absen[0]->nik);
    $status_absenEsok = HomeModel::status_absenEsok($status_absen[0]->nik);

    return view('psb.newList', compact('indexCount','cekManja','jam_manja','today','updatenote','checkQCOndesk','list','group_telegram','get_laporan_status','checkONGOING','list_ffg','statusProv','status_absenEsok'));
  }

  public function workorderQC()
  {
    $bulan = Input::get('bulan');
    $list = PsbModel::checkQC($bulan);

    return view('psb.newListQC', compact('list','bulan'));
  }

  public function workorderCabutanNte(){
    $checkONGOING = PsbModel::checkONGOING();
    $list = PsbModel::checkCabutanNte();

    return view('psb.newListCNO', compact('list','checkONGOING'));
  }

  public function workorderOntPremium(){
    $checkONGOING = PsbModel::checkONGOING();
    $list = PsbModel::checkOntPremium();

    return view('psb.newListOPO', compact('list','checkONGOING'));
  }

  public function HDFallout(){
    $start = date('Y-m', strtotime(date('Y-m')." -1 month"));
    $end = date('Y-m');

    $listFallout = DB::select("SELECT a.*,b.workerName,b.workerId,b.updated_at as takeTime FROM  Data_Pelanggan_Starclick a LEFT JOIN dispatch_hd b ON a.orderId = b.orderId WHERE (orderDate like  '".$start."%' OR orderDate like '".$end."%') AND  orderStatus LIKE  'Fallout (Activation)'");

    $listFallout2 = DB::select("SELECT a.*,b.workerName,b.workerId,b.updated_at as takeTime FROM  Data_Pelanggan_Starclick a LEFT JOIN dispatch_hd b ON a.orderId = b.orderId WHERE (orderDate like  '".$start."%' OR orderDate like '".$end."%') AND b.ket=1");

    $jumlahList1 = count($listFallout);
    return view('hd.listFallout', compact('listFallout', 'listFallout2', 'jumlahList1'));
  }

  public function orderHDFallout(Request $req){
    $auth = session('auth');
    if($req->status == "store"){
      $exists = DB::table('dispatch_hd')->where('orderId', $req->orderId)->first();
      if($exists){
        return redirect('/hdfallout')->with('alerts', [
          ['type' => 'warning', 'text' => '<strong>Gagal</strong> mengambil order, sudah ditikung orang!']
        ]);
      }else{
        DB::table('dispatch_hd')->insert([
          "orderId"=> $req->orderId,
          "workerId"=> $auth->id_karyawan,
          "workerName"=> $auth->nama,
          "updated_at"=>DB::raw('NOW()')
        ]);
        return redirect('/hdfallout')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengambil order']
        ]);
      }
    }else{
      DB::table('dispatch_hd')->where('orderId', $req->orderId)->delete();
      return redirect('/hdfallout')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>SUKSES</strong> mendrop order']
      ]);
    }
  }
  public function index()
  {
    $day         = date('D');
    $bulan       = date('Y-m');
    $today       = date('Y-m-d');
    $level       = session('auth')->level;
    $user_active = session('auth')->ACTIVE;

    if($user_active == 0){
			return redirect('/login')->with('alerts',[
				['type'=>'danger','text'=> 'User is Not Active!']
			]);
		}

    if($level == 88){
			return redirect('/login')->with('alerts',[
				['type'=>'danger','text'=> 'User is Banned!']
			]);
		}elseif($level == 0){
      return redirect('/login')->with('alerts',[
				['type'=>'danger','text'=> 'User is Not Active!']
			]);
    }

    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->where('sektor','<>','')->get();
    $auth = session('auth');

    $cek_regu = DB::SELECT('SELECT a.uraian,b.kat FROM regu a LEFT JOIN mitra_amija b ON a.mitra = b.mitra_amija WHERE (a.nik1 ="'.$auth->id_karyawan.'" OR a.nik2= "'.$auth->id_karyawan.'") AND a.ACTIVE = 1 ORDER BY a.date_created DESC LIMIT 1');

    // cek order provisioning
    $cek_PROV_WEEKEND = PsbModel::WorkOrder('NEED_PROGRESS_PROV_WEEKEND', $today);
    $cek_PROV_UMURLAMA = PsbModel::WorkOrder('PROV_UMUR_LAMA', $today);
    $list_ffg = PsbModel::WorkOrder('NEED_PROGRESS_FFG', $bulan);
    $cek_PROV = PsbModel::WorkOrder('NEED_PROGRESS_PROV', $bulan);
    $cek_PROV_PI = PsbModel::WorkOrder('NEED_PROGRESS_PROV_PI', $today);
    $cek_PROV_FWFM = PsbModel::WorkOrder('NEED_PROGRESS_PROV_FWFM', $today);
    $cek_PROV_MO_2P3P = PsbModel::WorkOrder('NEED_PROGRESS_PROV_MO_2P3P', $today);
    $cek_PROV_MO_ADDON = PsbModel::WorkOrder('NEED_PROGRESS_PROV_MO_ADDON', $today);

    // cek order assurance
    $cek_MANJA = PsbModel::WorkOrder('NEED_PROGRESS_MANJA', $today);
    $cek_IN_HVC_MIN_3JAM = PsbModel::WorkOrder('NEED_PROGRESS_IN_HVC_MIN_3JAM', $today);
    $cek_IN_REGULER_MIN_3JAM = PsbModel::WorkOrder('NEED_PROGRESS_IN_REGULER_MIN_3JAM', $today);
    $cek_IN_HVC_MAX_3JAM = PsbModel::WorkOrder('NEED_PROGRESS_IN_HVC_MAX_3JAM', $today);
    $cek_IN_REGULER_MAX_3JAM = PsbModel::WorkOrder('NEED_PROGRESS_IN_REGULER_MAX_3JAM', $today);
    $cek_INT = PsbModel::WorkOrder('NEED_PROGRESS_INT', $today);
    $cek_IN_SQM = PsbModel::WorkOrder('NEED_PROGRESS_IN_SQM', $today);

    $prioritas = "";
    if (count($cek_regu)>0 && $cek_regu[0]->kat == "DELTA")
    {
        if (in_array($day, ["Sat", "Sun"]) && count($cek_PROV_WEEKEND) > 0)
        {
          $prioritas = "NP_PROV_WEEKEND";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV_WEEKEND', $bulan);
        } elseif (count($list_ffg) > 0) {
          $prioritas = "NP_PROV_FFG";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_FFG', $bulan);
        } elseif (count($cek_PROV_UMURLAMA) > 0) {
          $prioritas = "NP_UMUR_LAMA";
          $list = PsbModel::WorkOrder('PROV_UMUR_LAMA', $bulan);
        } elseif (count($cek_PROV_PI) > 0) {
          $prioritas = "NP_PROV_PI";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV_PI', $today);
        } elseif (count($cek_PROV_FWFM) > 0) {
          $prioritas = "NP_PROV_FWFM";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV_FWFM', $today);
        } elseif (count($cek_PROV_MO_2P3P) > 0) {
          $prioritas = "NP_PROV_MO_2P3P";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV_MO_2P3P', $today);
        } elseif (count($cek_PROV_MO_ADDON) > 0) {
          $prioritas = "NP_PROV_MO_ADDON";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV_MO_ADDON', $today);
        } else {
          $prioritas = "NP_PROV";
          $list = PsbModel::WorkOrder('NEED_PROGRESS_PROV', $bulan);
        }
    // } elseif ( (count($cekManja)==1) || (count($cekManja)==2) ) {
      // echo "NP_MANJA";
      // $list = PsbModel::WorkOrder('NEED_PROGRESS_MANJA', $today);
      // foreach ($list as $lists) {
      //  $create_manja = date_create($lists->jam_manja);
      //  $jam_manja = date_format($create_manja, "H");
      // }
    // } elseif (count($cek_IN_HVC_MIN_3JAM)>0){
    //   $prioritas = "NP_IN_HVC_MIN_3JAM";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_IN_HVC_MIN_3JAM', $today);
    // } elseif (count($cek_IN_REGULER_MIN_3JAM)>0){
    //   $prioritas = "NP_IN_REGULER_MIN_3JAM";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_IN_REGULER_MIN_3JAM', $today);
    // } elseif (count($cek_IN_HVC_MAX_3JAM)>0){
    //   $prioritas = "NP_IN_HVC_MAX_3JAM";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_IN_HVC_MAX_3JAM', $today);
    // } elseif (count($cek_IN_REGULER_MAX_3JAM)>0){
    //   $prioritas = "NP_IN_REGULER_MAX_3JAM";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_IN_REGULER_MAX_3JAM', $today);
    // } elseif (count($cek_INT)>0){
    //   $prioritas = "NP_INT";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_INT', $today);
    // } elseif (count($cek_IN_SQM)>0){
    //   $prioritas = "NP_IN_SQM";
    //   $list = PsbModel::WorkOrder('NEED_PROGRESS_IN_SQM', $today);
    } else {
      $prioritas = "NORMAL";
      $list = PsbModel::WorkOrder('NEED_PROGRESS', $bulan);
    }
    // dd($prioritas,$list);
    $data1 = new \stdClass();
    $statusProv = '';
    $status_absenEsok = '';
    if(session('auth')->level == 49 || session('auth')->level == 50 || session('auth')->level == 51 ) {
  	  return view('dshr.transaksi.list');
  	} else {
      $status_absen = HomeModel::status_absen(session('auth')->id_user);
      if (count($status_absen)>0){
          if (session('auth')->level == 10 && $status_absen[0]->approval == 1){
            $indexCount = "1";
            $updatenote = PsbModel::updatenote();
            $checkONGOING = PsbModel::checkONGOING();
            $statusProv = HomeModel::cekStatusTekProv($status_absen[0]->nik);
            $status_absenEsok = HomeModel::status_absenEsok($status_absen[0]->nik);

            $upload_event = HomeModel::uploadEvent("Sertifikat_Vaksin");

            $OGPX = PsbModel::checkSTATUS("OGP");
            $KENDALAX = PsbModel::checkSTATUS("KENDALA");
            $HRX = PsbModel::checkSTATUS("HR");
            $UPX = PsbModel::checkSTATUS("UP");
            $QCX = PsbModel::checkQC("ALL");
            $CNOX = PsbModel::checkCabutanNte();
            $OPOX = PsbModel::checkOntPremium();
            $countNP = count($list);
            $countOGP = count($OGPX);
            $countKENDALA = count($KENDALAX);
            $countHR = count($HRX);
            $countUP = count($UPX);
            $countQC = "0";
            if($QCX<>null){
              $countQC = count($QCX);
            }
            $countFFG = count($list_ffg);
            $countCNO = count($CNOX);
            $countOPO = count($OPOX);

  		        return view('psb.newList', compact('indexCount','countNP','countOGP','countKENDALA','countHR','countUP','countQC','countFFG','countCNO','countOPO','cekManja','jam_manja','today','updatenote','list','list_ffg','group_telegram','get_laporan_status','checkONGOING','statusProv','status_absenEsok','bulan','prioritas','upload_event'));
          } else {
            return redirect('/home');
          }
      }
      elseif (session('auth')->level == 55){
        return redirect('/psbview/'.date('Y-m-d'));
      }
      elseif(session('auth')->level == 61 || session('auth')->level == 59){
        $tglAwal = date('Y-m-01');
        $tglAkhir = date('Y-m-d');
        return redirect('/dshr/plasa-sales/list-wo-by-sales/ALL/'.$tglAwal.'/'.$tglAkhir);
      }
      else {
        return redirect('/home');
      }
  	}
  }
  public function sendtotelegram3($id){
    // Tele::sendReportTelegram($id);
  }
  public function sendtotelegram($id,$chatID){
	  // data WO
  	$auth = session('auth');
    $order = DB::select('
      SELECT
      	*,
      	ms2n.*,
      	d.id as id_dt,
      	d.updated_at as tgl_dispatch,
      	r.uraian as nama_tim,
      	m.Kcontact as mKcontact,
      	m.ND as mND,
      	m.ND_Speedy as mND_Speedy,
      	m.Nama as mNama,
      	m.Alamat as mAlamat,
      	p2.mdf_grup_id as grup_id_psb,
      	m2.mdf_grup_id as grup_id_migrasi,
      	p2.mdf_grup as grup_psb,
      	m2.mdf_grup as grup_migrasi,
      	a2.mdf_grup as grup_starclick,
      	a2.mdf_grup_id as grup_id_starclick,
      	a.orderName,
      	a.ndemPots,
      	a.ndemSpeedy,
      	a.sto,
      	a.jenisPsb,
      	a.Kcontact as KcontactSC,
        yy.laporan_status,
        roc.no_telp,
        roc.no_tiket,
        roc.no_speedy,
        roc.headline,
        roc.umur,
          ra.no_tiket as status_tiket,
          dd.manja_status,
          ddd.dispatch_teknisi_timeslot,
          dm.*
      FROM dispatch_teknisi d
      LEFT JOIN manja_status dd ON d.manja_status = dd.manja_status_id
      LEFT JOIN dispatch_teknisi_timeslot ddd ON d.updated_at_timeslot = ddd.dispatch_teknisi_timeslot_id
      LEFT JOIN regu r ON (d.id_regu=r.id_regu)
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
      LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN mdf m1 ON m.MDF = m1.mdf
      LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
      LEFT JOIN Data_Pelanggan_Starclick a ON d.Ndem = a.orderId
      LEFT JOIN psb_laporan zz ON d.id = zz.id_tbl_mj
      LEFT JOIN psb_laporan_status yy ON zz.status_laporan = yy.laporan_status_id
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN roc_active ra ON roc.no_tiket = ra.no_tiket
      LEFT JOIN mdf a1 ON a.sto = a1.mdf
      LEFT JOIN mdf_grup a2 ON a1.mdf_grup_id = a2.mdf_grup_id
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      WHERE d.id = ?
    ',[
      $id
    ])[0];
 	  // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];
    //data material
    $get_materials = DB::select('
    	SELECT a.*
    	FROM
    		psb_laporan_material a
    	LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
    	WHERE id_tbl_mj = ?
    ',[$id]);

    $materials = "";
    if (count($get_materials)>0) {
	    foreach ($get_materials as $material) :
				if ($material->id_item<>'')
					$materials .= $material->id_item." : ".$material->qty."\n";
	    endforeach;
    }  else {
	    $materials = "";
    }

    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
	    $new_nama_tim .= $nama_tim[$i];
    }
    $messaggio = "Pengirixm : ".$auth->id_user."-".$auth->nama."\n";
    //$messaggio .= strtoupper($order->grup_psb ? : $order->grup_migrasi ? : $order->grup_starclick ? : 'UNKNOWN')."\n\n".$order->nama_tim."\n\n";
// 		$chatID = "-143383971";
    if ($order->no_tiket<>NULL) {
      $noorder = $order->no_tiket;
    } else if ($order->ND<>NULL) {
      $noorder = "MIGRASI AS IS ".$order->ND;
    } else {
      $noorder = "SC".$order->orderId;

    }
    $messaggio = "Reporting Order ".$noorder."\n\n";
	  $messaggio .= "Dispatch WO Pada : ".$order->tgl_dispatch;
	  $messaggio .= "\n";
    $get_group = DB::table('group_telegram')->where('chat_id',$chatID)->first();
    $messaggio .= "<i>".$get_group->title."</i>\n\n";
	  $messaggio .= "STATUS ORDER : ".$order->laporan_status;
    $messaggio .= "\n";
    if ($order->no_tiket==NULL) {
    } else {
      $messaggio .= "\nMTTR : ".$order->mttr."h";
      $messaggio .= "\nACTION : ".$order->action;
      $messaggio .= "\n";
      }
    if ($order->no_tiket<>NULL) {
      $id = $order->id_dt;
      $s = $order->no_tiket;
      $messaggio .= "WO Assurance \n";
      $messaggio .= "<b>".$order->no_tiket."</b> | ";

      if ($order->status_tiket<>NULL){
        $messaggio .= "<b>Open </b>";
      } else {
        $messaggio .= "<b>Close </b>";
      }
      if ($order->umur<>''){
        $umur = $order->umur;
      } else {
        $umur = '0';
      }
      $messaggio .= "<b>| ".$umur."h</b>\n";
      $messaggio .= $order->no_telp."\n";
      $messaggio .= $order->no_speedy."\n";
      $headline = explode('+',$order->headline);
      foreach ($headline as $val){
        $messaggio .= trim($val)."\n";
      }
      $messaggio .= "\n<b>Status Link </b> \nLink : ";
      $messaggio .= "<b>".@$headline[7]."</b>\n";
      $messaggio .= "INET : ";
      $messaggio .= "<b>".@$headline[9]."</b>\n";
      $messaggio .= @$headline[10]."\n";
      $messaggio .= "\n";
    } else if ($order->ND<>NULL) {
      $s = "MIG-ASIS ".$order->ND;
      $messaggio .= "\n<b>WO MIGRASI AS IS</b>\n<b>".$order->NAMA."</b>\n";
      $messaggio .= $order->ND."\n";
      $messaggio .= $order->ND_REFERENCE."\n";
      $messaggio .= $order->LVOIE."\n";
      $messaggio .= $order->LCOM."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";
      $messaggio .= "Manja : ".$order->manja ? : '--'."\n";
      $messaggio .= "\n";

    }else {
      $s = "SC".$order->orderId;
      $messaggio .= "\n<b>WO Provisioning</b>\n<b>".$order->orderName."</b>\n";
      $messaggio .= "SC".$order->orderId."\n";
      $messaggio .= $order->ndemPots."\n";
      $messaggio .= $order->ndemSpeedy."\n";
      $messaggio .= $order->kcontact."\n";
      $messaggio .= $order->jenisPsb."\n";
      $messaggio .= "\n";
      $messaggio .= "<b>Appointment</b>\n";
      $messaggio .= "Timeslot : ".$order->dispatch_teknisi_timeslot."\n";
      $messaggio .= "Manja : ".$order->manja ? : '--'."\n";
      $messaggio .= "\n";
    }

    if ($exists->status_laporan=="8"){
      if ($exists->catatan<>''){
  		  $messaggio .= "CATATAN : \n";
  		  $messaggio .= $exists->catatan."\n\n";
  	  }

  	  $s .= " #".strtolower($new_nama_tim);
    } else {

	  if ($exists->catatan<>''){
		  $messaggio .= "CATATAN : \n";
		  $messaggio .= $exists->catatan."\n\n";
	  }

	  $s .= " #".strtolower($new_nama_tim);

			$messaggio .= "\n";
    if ($exists->kordinat_pelanggan<>'')
		{
    	$messaggio .= "Kordinat Pelanggan : \n";
			$messaggio .= $exists->kordinat_pelanggan."\n";
    }
    else
    	$messaggio .= "Tidak ada info kordinat pelanggan\n";

		  $messaggio .= "\n";

    if ($exists->nama_odp<>'')
    {
	  	$messaggio .= "Nama ODP : \n";
			$messaggio .= $exists->nama_odp."\n";
    }
    else
    	$messaggio .= "Tidak ada info nama ODP pelanggan\n";

      $messaggio .= "\n";
	  if ($exists->kordinat_odp<>'')
	  {
	    $messaggio .= "Kordinat ODP : \n";
			$messaggio .= $exists->kordinat_odp."\n";
    }
    else
    	$messaggio .= "Tidak ada info kordinat ODP pelanggan\n";



    $messaggio .= "\n";
    $messaggio .= "#report".strtolower($new_nama_tim)."\n";

    $messaggio .= "\n";


    $messaggio .= $materials;

  }
//
    if ($exists->status_laporan=="8"){
      $chatID = "-140569787";
    }
    Telegram::sendMessage([
      'chat_id' => $chatID,
      'parse_mode' => 'html',
      'text' => $messaggio
    ]);

    if ($order->no_tiket<>NULL){
      $photoInputs = $this->assuranceInputs;
      $folder = "asurance";
    } else {
      $photoInputs = $this->photoInputs;
      $folder = "evidence";
    }
		for($i=0;$i<count($photoInputs);$i++) {
    	$file = public_path()."/upload4/".$folder."/".$id."/".$photoInputs[$i].".jpg";
      // Telegram::sendMessage([
      //   'chat_id' => $chatID,
      //   'caption' => $photoInputs[$i]." ".$s,
      //   'text' => $file
      // ]);
    	if (file_exists($file)){
		    Telegram::sendPhoto([
		      'chat_id' => $chatID,
		      'caption' => $photoInputs[$i]." ".$s,
		      'photo' => $file
		    ]);

// 			echo $file;
		  }
    }

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
      ]);

  }

  function status_wo($id){
	  switch ($id){
		  case 1 :
		  	$result = "SELESAI";
		  	break;
		  case 2 :
		  	$result = "KENDALA TEKNIS";
		  	break;
		  case 3 :
		  	$result = "KENDALA PELANGGAN";
		  	break;
		  case 4 :
		  	$result = "HR";
		  	break;
		  case 5 :
		  	$result = "OGP";
		  	break;
		  	break;
		  case 6 :
		  	$result = "BELUM SURVEY";
		  	break;
		  	break;
		  case 7 :
		  	$result = "RESCHEDLUE";
		  	break;
		  	break;
		  case 8 :
		  	$result = "SURVEY OK";
		  	break;
        case 9 :
  		  	$result = "FOLLOW UP SALES";
  		  	break;


		  default :
		  	$result = "BELUM SURVEY";
				break;
	  }
	  return $result;
  }

  public function stokTeknisi()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT m.*,i.nama_item,i.unit_item
      FROM stok_material_teknisi m
      LEFT JOIN item i
      ON m.id_item=i.id_item
      WHERE m.id_karyawan = ?
    ',[
      $auth->id_karyawan
    ]);
    $ntes = DB::select('
      SELECT *
      FROM nte
      WHERE position_key = ?
    ',[
      $auth->id_karyawan
    ]);
    return view('psb.stok', compact('list', 'ntes'));
  }

  public function get_kpro_foto($id){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.qcborneo.id/tomman.php" );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST,           1 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, '{
    "token": "1q2w3e4r5t",
    "id": "'.$id.'"
    }' );
    curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

    $result=curl_exec ($ch);
    return $result;
  }

  public function input($id)
  {
    $get_status_qc = "";
    $auth = session('auth');

    if (session('auth')->level == 10)
    {
      $layout = 'tech_layout';
    } else {
      $layout = 'layout';
    }

    $get_splitter = DB::SELECT('SELECT splitter_id as id, splitter as text FROM jenis_splitter WHERE active = "1"');

    $get_hd_manja = [
      (object)['id' => 'TIARA', 'text' => 'TIARA'],
      (object)['id' => 'HENNY', 'text' => 'HENNY'],
    ];

    $get_hd_fallout = [
      (object)['id' => 'KEMAS RIZKY H', 'text' => 'KEMAS RIZKY H'],
      (object)['id' => 'M RIYAN HIDAYAT', 'text' => 'M RIYAN HIDAYAT'],
      (object)['id' => 'RAHMANI', 'text' => 'RAHMANI'],
      (object)['id' => 'MUHAMMAD NANDI AZIZ MOCHTAR', 'text' => 'MUHAMMAD NANDI AZIZ MOCHTAR'],
      (object)['id' => 'SYAHRIDAN MUSTAQIM', 'text' => 'SYAHRIDAN MUSTAQIM'],
      (object)['id' => 'MUHAMMAD ABDUL RAJIB', 'text' => 'MUHAMMAD ABDUL RAJIB'],
      (object)['id' => 'OSAMA ABDULLAH A', 'text' => 'OSAMA ABDULLAH A'],
      (object)['id' => 'ANDRIAN S', 'text' => 'ANDRIAN S'],
      (object)['id' => 'ALVIAN WAHYUDI', 'text' => 'ALVIAN WAHYUDI'],
      (object)['id' => 'ZIRIN', 'text' => 'ZIRIN'],
      (object)['id' => 'ALIEF PANJI GUMELAR', 'text' => 'ALIEF PANJI GUMELAR'],
      (object)['id' => 'FURQAN IDIFASI', 'text' => 'FURQAN IDIFASI'],
    ];

    $exists = DB::select('
      SELECT
      dt.jenis_order,
      pl.*
      FROM dispatch_teknisi dt
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE pl.id_tbl_mj = ?
    ',[
      $id
    ]);

    if (!in_array(session('auth')->id_karyawan, ['16951115', '16002401']))
    {
      $cek_ongoing = PsbModel::checkONGOING();
      if (count($cek_ongoing) > 0)
      {
        if (count($cek_ongoing) == 1)
        {
          $ongoing = $cek_ongoing[0];
          if ($ongoing->id_dt <> $id)
          {
            return back()->with('alerts',[['type' => 'danger', 'text' => 'Harap kerjakan terlebih dahulu progress order sebelumnya rekan']]);
          }
        }
      }
    }

    //cek order yang belum di konfirmasi
    if (session('auth')->level == 10)
    {
      $check_confirm = DB::table('psb_confirm_order')->where('dt_id', $id)->where('status_confirm', 1)->first();
      if (count($check_confirm) > 0)
      {
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Order <strong>'.$check_confirm->id_confirm.'</strong> Belum di Konfirmasi oleh Rekan Helpdesk']]);
      }
    }

    $cek_dispatch = DB::table('dispatch_teknisi')->where('id',$id)->first();

    // if (count($cek_dispatch) > 0)
    // {
    //   if (session('auth')->id_karyawan != '20981020')
    //   {
    //     $get_cutoff_psb = DB::table('psb_cutoff_rekon')->where('order_id', $cek_dispatch->NO_ORDER)->where('status_teknisi', 'UP')->first();
    //   } else {
    //     $get_cutoff_psb = null;
    //   }
    // } else {
    //   $get_cutoff_psb = null;
    // }

    if (@$cek_dispatch->jenis_order == 'CABUT_NTE' || @$cek_dispatch->jenis_layanan == 'CABUT_NTE')
    {
      $cek_cno = DB::SELECT('
        SELECT
        dt.id as id_dt,
        dt.Ndem as ndem_dt,
        pl.kordinat_pelanggan,
        dps.*
        FROM cabut_nte_order cno
        LEFT JOIN Data_Pelanggan_Starclick dps ON cno.wfm_id = dps.internet
        LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        WHERE
        dps.jenisPsb LIKE "AO%" AND
        dt.jenis_order IN ("SC","MYIR") AND
        cno.wfm_id = "'.$cek_dispatch->NO_ORDER.'"
        ORDER BY dps.orderDate DESC
        LIMIT 1
      ');
      // dd($cek_cno);
      if (count($cek_cno)>0)
      {
        $foto_cno = $cek_cno[0];
      } else {
        $foto_cno = [];
      }
    } else {
      $foto_cno = [];
    }
    // dd($foto_cno);

    $jenis_ont = DB::table('nte_jenis')->select('nte_jenis as id','nte_jenis as text')->where('nte_jenis_kat','typeont')->get();
    $jenis_stb = DB::table('nte_jenis')->select('nte_jenis as id','nte_jenis as text')->where('nte_jenis_kat','typestb')->get();
    $jenis_plc = DB::table('nte_jenis')->select('nte_jenis as id', 'nte_jenis as text')->where('nte_jenis_kat', 'typeplc')->get();
    $jenis_wifiext = DB::table('nte_jenis')->select('nte_jenis as id', 'nte_jenis as text')->where('nte_jenis_kat', 'typewifiext')->get();
    $jenis_indibox = DB::table('nte_jenis')->select('nte_jenis as id', 'nte_jenis as text')->where('nte_jenis_kat', 'typeindibox')->get();
    $jenis_indihomesmart = DB::table('nte_jenis')->select('nte_jenis as id', 'nte_jenis as text')->where('nte_jenis_kat', 'typeindihomesmart')->get();

    $checkListMaterials = PsbModel::checkListMaterials($id);

    if (count($cek_dispatch)>0){


    $odpAlternatif = '';
    $kpro_foto = '';

    // cek data distarclick
    $ada = DB::table('Data_Pelanggan_Starclick')
      ->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderId','=','dispatch_teknisi.Ndem')
      ->leftjoin('psb_myir_wo','Data_Pelanggan_Starclick.myir','=','psb_myir_wo.myir')
      ->select('dispatch_teknisi.Ndem','dispatch_teknisi.dispatch_by','Data_Pelanggan_Starclick.orderId','Data_Pelanggan_Starclick.myir','psb_myir_wo.id as pmw_id','psb_myir_wo.ket_input')
      ->where('dispatch_teknisi.id',$id)
      ->first();
    $cekMyir = DB::table('dispatch_teknisi')
      ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
      ->select('psb_myir_wo.id as pmw_id','psb_myir_wo.myir','psb_myir_wo.ket_input')
      ->where('dispatch_teknisi.id',$id)->first();
    if($ada){
    //   $kpro_foto = self::get_kpro_foto($ada->orderId);
      $myir = $ada->myir;
      if($ada->dispatch_by==NULL){
          $odpAlternatif = json_decode($this->get_odp($id));
      }
    } else {
      if ($cekMyir){
        $myir = $cekMyir->myir;
      } else {
        $myir = "";
      }
    }

    // cek foto onecall
    if ($myir <> "")
    {
      // $onecall = @file_get_contents("http://10.128.16.66/onecall/api/sales-order/" . $myir, TRUE);
      $onecall = "";
    } else {
      $onecall = "";
    }
    $onecall = json_decode($onecall);
    // dd($onecall);

    // $tglWo = DB::table('dispatch_teknisi')->where('id',$id)->first();
    // $periode = date('m/Y',strtotime($tglWo->tgl));

    // $bulanEnd = date('m',strtotime($tglWo->tgl));
    // if ($bulanEnd==1){
    //     $endBulan = '12';
    //     $endTahun = date('Y',strtotime($tglWo->tgl));

    //     $peiodeEnd = $endBulan.'/'.$endTahun-1;
    // }
    // else{
    //     $endBulan = $bulanEnd-1;
    //     $endTahun = date('Y',strtotime($tglWo->tgl));

    //     $peiodeEnd = $endBulan.'/'.$endTahun;
    // };

    // dd($tglWo);
    // if ($tglWo->dispatch_by=='5'){
    //     $dt = DB::table('psb_myir_wo')->where('myir',$tglWo->Ndem)->first();
    //     $periode = date('m/Y',strtotime($dt->orderDate));
    // }
    // else if ($tglWo->dispatch_by==NULL){
    //     $dt = DB::table('Data_Pelanggan_Starclick')->where('orderId',$tglWo->Ndem)->first();
    //     if (count($dt)<>0){
    //       if (substr($dt->orderDatePs, 0, 4)=="0000"){
    //           $periode = date('m/Y',strtotime($dt->orderDate));
    //       }
    //       else{
    //           $periode = date('m/Y',strtotime($dt->orderDatePs));
    //       }
    //     }
    //     else{
    //         $periode = date('m/Y',strtotime($tglWo->tgl));
    //     }
    // };
  // $cek_qc_borneo = DB::table('fitur')->first();
  // if($cek_qc_borneo->get_qc==1){
  //   if (@count($ada)){

  //     $get_status_qc = file_get_contents("http://10.128.18.14/proactive-caring/api/quality_control.php?sc=".$ada->Ndem);
  //     $get_status_qc  = json_decode($get_status_qc);

  //     }
  // }

  $dc_roll = DB::TABLE('psb_laporan')
    ->select('psb_laporan.material_psb','psb_laporan_material.qty')
    ->leftJoin('psb_laporan_material','psb_laporan.id','=','psb_laporan_material.psb_laporan_id')
    ->leftJoin('item','psb_laporan_material.id_item','=','item.id_item')
    ->where('psb_laporan.id_tbl_mj',$id)
    ->where('psb_laporan_material.id_item','AC-OF-SM-1B')
    ->first();

  $get_dismantling_status = DB::SELECT('SELECT id_status as id, status_dismantling as text FROM psb_laporan_dismatling_status');

    if (count($exists)) {
      $data = $exists[0];
      // echo $data->id;
      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item, i.unit_item,
          COALESCE(m.qty, 0) AS qty,
          0 as rfc,
          0 as saldo
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
          NOT i.id_item IN("AC-OF-SM-1B","Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-50-NonAcc","Preconnectorized-1C-100-NonAcc","Preconnectorized-1C-75-NonAcc","Preconnectorized-70M","Preconnectorized-1C-80") AND
        	i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      $materials2 = DB::select('
        SELECT
          i.id_item, i.nama_item, i.unit_item,
          COALESCE(m.qty, 0) AS qty,
          0 as rfc,
          0 as saldo
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
          i.id_item IN("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-50-NonAcc","Preconnectorized-70M","Preconnectorized-1C-100-NonAcc","Preconnectorized-1C-80","AC-OF-SM-1B") AND
        	i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      // tes ja
      // dd($data->id);
      // $periode = date('m/Y');
      // if ($auth->level==2 || $auth->level==15 || $auth->level==46){
      //   //   $materials = DB::select('
      //   //   SELECT
      //   //     i.id_item, i.nama_item,
      //   //     COALESCE(i.jmlTerpakai, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
      //   //   FROM rfc_sal i
      //   //   where
      //   //     i.psb_laporan_id = ?
      //   //   ORDER BY i.rfc
      //   // ', [
      //   //   $data->id
      //   // ]);
      //
      //   $materials = DB::select('
      //     SELECT
      //       i.id_item, i.nama_item,
      //       COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
      //     FROM rfc_sal i
      //     LEFT JOIN
      //       psb_laporan_material m
      //       ON i.id_item_bantu = m.id_item_bantu
      //     Where
      //       (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'" OR i.rfc LIKE "%12/2019%") AND
      //       m.psb_laporan_id = ?
      //     ORDER BY i.rfc asc, i.id_item asc
      //   ', [
      //     $data->id
      //   ]);
      //
      //   if (empty($materials)){
      //         $materials = DB::select('
      //           SELECT * FROM psb_laporan_material where type = 1 AND psb_laporan_id = ?
      //         ', [
      //           $data->id
      //         ]);
      //   }
      // }
      // else{
      //   $materials = DB::select('
      //     SELECT
      //       i.id_item, i.nama_item,
      //       COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo, i.jmlTerpakai
      //     FROM rfc_sal i
      //     LEFT JOIN
      //       psb_laporan_material m
      //       ON i.id_item_bantu = m.id_item_bantu
      //       AND m.psb_laporan_id = ?
      //     WHERE
      //      (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'" OR i.rfc LIKE "%12/2019%") AND
      //      i.nik="'.$auth->id_user.'"
      //     ORDER BY i.rfc asc, i.id_item asc
      //   ', [
      //     $data->id
      //   ]);
      // }
      $get_detail_sc = DB::table('dispatch_teknisi')->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.NO_ORDER','=','Data_Pelanggan_Starclick.orderIdInteger')->where('id', $id)->first();
    }
    else {
      // echo "0";
      $data = new \StdClass;
      $data->id = null;
      $data->jenis_order = null;
      $data->rfc_number = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;
      $data->type_plc = null;
      $data->sn_plc = null;
      $data->type_wifiext = null;
      $data->sn_wifiext = null;
      $data->type_indibox = null;
      $data->sn_indibox = null;
      $data->type_indihomesmart = null;
      $data->sn_indihomesmart = null;
      $data->material_psb = null;
      $data->jenis_odp = null;
      $data->redaman_in = null;
      $data->hd_manja = null;
      $data->hd_fallout = null;
      $data->SC_New = null;
      $data->valins_id = null;
      $data->detek = null;
      // TODO: order by most used

      $get_detail_sc = DB::table('dispatch_teknisi')->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.NO_ORDER','=','Data_Pelanggan_Starclick.orderIdInteger')->where('id', $id)->first();

      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item, i.unit_item,
          COALESCE(m.qty, 0) AS qty,
          0 as rfc,
          0 as saldo
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
          NOT i.id_item IN("AC-OF-SM-1B","Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-50-NonAcc","Preconnectorized-1C-100-NonAcc","Preconnectorized-1C-75-NonAcc","Preconnectorized-70M","Preconnectorized-1C-80") AND
        	i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      $materials2 = DB::select('
        SELECT
          i.id_item, i.nama_item, i.unit_item,
          COALESCE(m.qty, 0) AS qty,
          0 as rfc,
          0 as saldo
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
          i.id_item IN("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-50-NonAcc","Preconnectorized-70M","Preconnectorized-1C-100-NonAcc","Preconnectorized-1C-80") AND
        	i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      // $materials = DB::select('
      //   SELECT a.id_item, a.nama_item, 0 AS qty,  0 as rfc,
      //     0 as saldo

      //   FROM item a left join psb_laporan_material b ON a.id_item=b.id_item WHERE a.grup = 1
      //   GROUP BY a.id_item
      //   ORDER BY a.id_item
      // ');

      // $materials = DB::select('
      //   SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc, jmlTerpakai
      //   FROM rfc_sal i where (i.rfc LIKE "%'.$periode.'" OR i.rfc LIKE "%'.$peiodeEnd.'") AND nik="'.$auth->id_user.'" AND jumlah<>0
      //   ORDER BY rfc
      // ');


      // $materials = DB::select('
      //   SELECT
      //     i.id_item, i.nama_item,
      //     COALESCE(m.qty, 0) AS qty
      //   FROM item i
      //   LEFT JOIN
      //     psb_laporan_material m
      //     ON i.id_item = m.id_item
      //     AND m.psb_laporan_id = ?
      //   WHERE 1
      //   ORDER BY id_item
      // ', [
      //   $data->id
      // ]);

    }

    $check_nte = DB::table('nte')->where('id_dispatch_teknisi', $id)->first();
    if (count($check_nte)>0) {
      $nte = DB::SELECT('SELECT nte.sn as id, nte.sn as text, COALESCE(nte.qty, 0) AS qty, nte.jenis_nte, nte_jenis.nte_jenis_kat, nte.nik1, nte.nik2 FROM nte LEFT JOIN nte_jenis ON nte.jenis_nte = nte_jenis.nte_jenis WHERE (nte.id_dispatch_teknisi = "'.$id.'" OR nte.status = 2) AND (nte.nik1 = "'.$auth->id_karyawan.'" OR nte.nik2 = "'.$auth->id_karyawan.'")');
    } else {
      $nte = DB::SELECT('SELECT nte.sn as id, nte.sn as text, COALESCE(nte.qty, 0) AS qty, nte.jenis_nte, nte_jenis.nte_jenis_kat, nte.nik1, nte.nik2 FROM nte LEFT JOIN nte_jenis ON nte.jenis_nte = nte_jenis.nte_jenis WHERE nte.status = 2 AND (nte.nik1 = "'.$auth->id_karyawan.'" OR nte.nik2 = "'.$auth->id_karyawan.'")');
    }

    // dd(session('auth')->id_karyawan,$nte);


    $project = DB::select('
      SELECT
      	d.id as id_dt,
        d.id_regu,
        d.manja,
        d.alamatSales,
        d.dispatch_sto,
				c.ndemPots,
				c.ndemSpeedy,
				c.Kcontact as KcontactSC,
				c.jenisPsb,
				c.sto as csto,
        c.alproname as calproname,
        c.lon,c.lat,
        d.jenis_layanan,
        my.*,
        pl.*,
        d.Ndem as NdemId,
        my.sc,
        my.myir,
        my.no_internet,
        my.no_telp,
        my.customer,
        my.namaOdp,
        c.lat as latSC,
        c.orderId,
        c.orderName,
        c.orderNcli,
        c.noTelp,
        c.internet,
        c.orderAddr,
        c.alproname,
        c.lon as lonSC,
        cno.nama_pelanggan as cno_pelanggan,
        dtjl.jenis_transaksi as dtjl_jenis_transaksi
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.NO_ORDER = c.orderIdInteger
      LEFT JOIN psb_myir_wo my ON (d.Ndem = my.myir OR d.Ndem = my.sc)
      LEFT JOIN cabut_nte_order cno ON d.NO_ORDER = cno.wfm_id
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id = pl.id_tbl_mj
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON d.jenis_layanan_id = dtjl.jenis_layanan_id
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];

    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next
    // $get_Ndem = DB::table('dispatch_teknisi')
    //               ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
    //               ->select('psb_laporan.id','psb_laporan.id_tbl_mj','dispatch_teknisi.Ndem', 'dispatch_teknisi.dispatch_by')
    //               ->where('psb_laporan.id_tbl_mj',$id)
    //               ->first();

    // if (@count($get_Ndem)==0){
    //     $get_Ndem = DB::table('dispatch_teknisi')->where('id',$id)->first();
    //     $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    // }
    // else{
    //     $ndemIn  = DB::table('psb_laporan_log')->where('psb_laporan_id',$get_Ndem->id)->groupBy('Ndem')->get();
    //     $ndemNew = '';
    //     if (count($ndemIn)<>0){
    //         if (count($ndemIn)==1){
    //             $ndemNew  = "'".$ndemIn[0]->Ndem."'";
    //             $ndemMyir = DB::table('psb_myir_wo')->where('sc',$ndemIn[0]->Ndem)->first();
    //             if (count($ndemMyir)<>0){
    //                 $ndemNew  .= ",'".$ndemMyir->myir."'";
    //             };
    //         }
    //         else{
    //           foreach($ndemIn as $ndem){
    //               $ndemNew .= "'".$ndem->Ndem."'".',';
    //           }

    //           $ndemNew = substr($ndemNew, 0, -1);
    //         }

    //         $ndemNew = '('.$ndemNew.')';
    //         $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem in '.$ndemNew.') AND';
    //     }
    //     else{
    //         $ndemMyir = DB::table('psb_myir_wo')->where('sc',$get_Ndem->Ndem)->first();
    //         // $cari = 'a.psb_laporan_id = "'.$get_Ndem->id.'" AND';
    //         if (count($ndemMyir)==0){
    //             $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    //         }
    //         else{
    //             $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem ="'.$ndemMyir->myir.'") AND';
    //         }

    //     }
    //     // dd($cari);
    // };

    // $cek_log = DB::table('psb_laporan')->leftJoin('psb_laporan_log','psb_laporan.id','=','psb_laporan_log.psb_laporan_id')->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')->where('psb_laporan.id_tbl_mj', $id)->orderByDesc('psb_laporan_log.created_at')->first();
    // if ($cek_log == null)
    // {
    //   $data_log = DB::SELECT('
    //   SELECT
    //   pll.*
    //   FROM dispatch_teknisi dt
    //   LEFT JOIN psb_laporan_log pll ON dt.Ndem = pll.Ndem
    //   WHERE
    //   dt.id = "'.$id.'"
    //   ORDER BY pll.created_at DESC
    // ');
    // } else {
    //   $data_log = DB::SELECT('
    //   SELECT
    //   pll.*,
    //   pls.laporan_status
    //   FROM psb_laporan pl
    //   LEFT JOIN psb_laporan_log pll ON pl.id = pll.psb_laporan_id
    //   LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    //   WHERE
    //   pl.id_tbl_mj = "'.$id.'"
    //   ORDER BY pll.created_at DESC
    // ');
    // }

    $data_log = DB::SELECT('
      SELECT
      pll.*,
      pls.laporan_status
      FROM psb_laporan_log pll
      LEFT JOIN psb_laporan_status pls ON pll.status_laporan = pls.laporan_status_id
      WHERE
      pll.Ndem = "'.$cek_dispatch->Ndem.'"
      GROUP BY pll.psb_laporan_log_id
      ORDER BY created_at DESC
    ');

    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');

    if ($auth->owner_group == "AMO")
    {
      $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE (jenis_order IN ("ALL","PROV","PROVVA","PROVA","PROVB","PROVC","AMO","PROVV") AND sts="0") order by urutan asc, laporan_status asc');
    } elseif ($auth->owner_group == "DAMAN") {
      $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE laporan_status_id IN (52, 119, 120) order by urutan asc, laporan_status asc');
    } elseif ($auth->level == 10 && $project->jenis_layanan == "CABUT_NTE") {
      $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE laporan_status_id IN (6,28,29,5,89,95,90,91,96,92,93,94,23) AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    } elseif ($auth->level == 10 && $project->jenis_layanan == "ONT_PREMIUM") {
      $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE laporan_status_id IN (6,28,29,5,42,97,98,100,101,102,103,115,23) AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    } else {
      if (in_array($auth->level, [15, 2])) {
        $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV","PROVVA","PROVA","PROVB","PROVC","CABUTAN","ONT_PREM","PROVV","DAMAN") AND sts="0" order by urutan asc, laporan_status asc');
      } else {
        if ($auth->level == 10) {
          $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE NOT laporan_status_id IN ("76","75","104") AND jenis_order IN ("ALL","PROV","PROVVA","PROVA", "PROVB", "PROVC") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
        }
      }
      if ($project->jenis_layanan=="ADDONSTB" || $project->jenis_layanan=="CHANGESTB" || $project->jenis_layanan=="2ndSTB"){
          if ($project->id_regu=="789" || $project->id_regu=="790" || $project->id_regu=="920" || $project->id_regu=="930" || $project->id_regu=="931" || $project->id_regu=="937" || $project->id_regu=="938" || $project->id_regu=="939"){
              // dd('ok');
               $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE manja="1" order by laporan_status asc');
          }
      }

      // daman
      if ($project->status_laporan=="24"){
          $nikData = PsbModel::getAkses($auth->id_user, 'daman');
          if (count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVA") OR laporan_status_id=61 order by laporan_status asc');
          };

          $nikData = PsbModel::getAkses($auth->id_user, 'lanjutpt1');
          if (count($nikData)<>0){
             $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=61 order by laporan_status asc');
          }
      }

      // aso
      if ($project->status_laporan=="11"){
          $nikData = PsbModel::getAkses($auth->id_user, 'aso');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVB") order by laporan_status asc');
          }
      }

      // amo
      if ($project->status_laporan=="2"){
          $nikData = PsbModel::getAkses($auth->id_user, 'amo');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV", "PROVC") order by laporan_status asc');
          }
      }

      // kendala
      if ($project->status_laporan=="25" || $project->status_laporan=="2" || $project->status_laporan=="34" || $project->status_laporan=="42" || $project->status_laporan=="60"){
          $nikData = PsbModel::getAkses($auth->id_user, 'kendala');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE (jenis_order in ("ALL PROVV") OR laporan_status_id in (57,2)) order by laporan_status asc');
          }
      }

      // rna
      if ($project->status_laporan=="49"){
          $nikData = PsbModel::getAkses($auth->id_user, 'rna');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=16 order by laporan_status asc');
          }
      }

      // odp full nok
      if ($project->status_laporan=="61"){
          $nikData = PsbModel::getAkses($auth->id_user, 'odpNok');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=57 order by laporan_status asc');
          };

          $nikData = PsbModel::getAkses($auth->id_user, 'lanjutpt1');
          if (count($nikData)<>0){
             $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") order by laporan_status asc');
          }

      }

      // unsc
      if ($project->status_laporan=="57"){
          $nikData = PsbModel::getAkses($auth->id_user, 'unsc');
          if(count($nikData)<>0){
              $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text, definisi_status FROM psb_laporan_status WHERE jenis_order in ("ALL PROVV") OR laporan_status_id=56 OR laporan_status_id=16 OR laporan_status_id=49 order by laporan_status asc');
          }
      }


    }

    // dd($get_laporan_status, $project->jenis_layanan, $auth->level, $step_next);



    $dispatch_teknisi_id = $id;
    // $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();
    $getNdem = DB::table('dispatch_teknisi')->where('id',$id)->first();
    $photoInputs = $this->photoInputs;
    // dd($photoInputs);
    if ($getNdem->dispatch_by==6){
        $photoInputs = $this->photoReboundary;
    };
    $getStepId = $getNdem->step_id;
    $Ndem      = $getNdem->Ndem;
    $redamanIboster = NULL;
    if ($exists){
        $redamanIboster = $exists[0]->redaman_iboster;
    };

    // cek kalo ada di table psb_myir_wo
    $ketInput = '';
    $idMyir   = '';
    if ($ada){
      $ketInput = $ada->ket_input;
      $idMyir   = $ada->pmw_id;
    }else{
      $ketInput = $cekMyir->ket_input;
      $idMyir   = $cekMyir->pmw_id;
    }

    $photoInfo = '';
    if ($ketInput==1){
        $photoInfo = ['ktp','odp','rumah'];
    }
    else if ($ketInput==0){
        $photoInfo = ['ktp','ttd','ktp_pelanggan','capture_odp'];
    }

    $table = DB::table('hdd')->get()->first();

    $getProject = $project;

    if(json_encode($project) != false)
    {
      $getProject = $project;
    }
    else
    {
      $getProject = (object)preg_replace('/[[:^print:]]/', '', (array)$project);

      foreach($getProject as $k => $v)
      {
        if($v == '')
        {
          $getProject->$k = null;
        }
      }
    }

    return view('psb.input', compact('id','dc_roll', 'kpro_foto','table','onecall','get_status_qc','layout','data', 'project', 'getProject', 'materials','materials2', 'photoInputs', 'nte', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang', 'data_log', 'odpAlternatif', 'getStepId', 'Ndem','redamanIboster', 'ketInput', 'photoInfo', 'idMyir','ont_nte','stb_nte','getMasterOdp','foto_cno','get_detail_sc','cek_dispatch','jenis_ont','jenis_stb','checkListMaterials','get_dismantling_status', 'jenis_plc', 'jenis_wifiext', 'jenis_indibox', 'jenis_indihomesmart', 'get_splitter', 'get_hd_manja', 'get_hd_fallout', 'get_cutoff_psb'));
  } else {
    return view('errors.404');
  }
  }

  public function save(Request $request, $id)
  {
    // if (in_array($id, [419513, 457657, 404667, 264721]))
    // {
        // $this->handleFileUpload2($request, $id);
    // } else {
        $upload = $this->handleFileUpload($request, $id);
        if (@$upload['type'] == "danger")
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => $upload['message']]
        ]);
    // }

    $a = $request->input('status');
    $auth = session('auth');
    $owner_group = session('auth')->owner_group;
    $fitur = DB::table('fitur')->first();

    $disp = DB::table('dispatch_teknisi')
    ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.NO_ORDER', '=', 'Data_Pelanggan_Starclick.orderIdInteger')
    ->leftJoin('dispatch_teknisi_jenis_layanan', 'dispatch_teknisi.jenis_layanan_id', '=', 'dispatch_teknisi_jenis_layanan.jenis_layanan_id')
    ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
    ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
    ->leftJoin('step_tracker', 'psb_laporan_status.step_id', '=', 'step_tracker.step_tracker_id')
    ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.orderId', 'Data_Pelanggan_Starclick.jenisPsb', 'dispatch_teknisi_jenis_layanan.jenis_transaksi', 'Data_Pelanggan_Starclick.internet', 'step_tracker.*', 'psb_laporan.redaman_iboster')
    ->where('dispatch_teknisi.id', $id)
    ->first();

    $jenisPsb = $jenisLayanan = '';

    if (@$disp->jenisPsb)
    {
      $jenisPsb = substr($disp->jenisPsb, 0, 2);
    }
    if (@$disp->jenis_layanan)
    {
      $jenisLayanan = $disp->jenis_layanan;
    }

    $jeniswo = $disp->dispatch_by;

    // check valins
    if (@$disp->internet)
    {
      if (in_array($request->input('status'), [1, 4, 24]) && $fitur->check_valins == 1)
      {
        $check_valins = ApiController::validateValins('PSB', $disp->Ndem, $a, $disp->internet, $request->input('nama_odp'), $request->input('valins_id'));

        if ($check_valins)
        {
          return back()->with('alerts', [
            ['type' => $check_valins['type'], 'text' => $check_valins['text']]
          ]);
        }
      }
    }

    // check order tacticalpro pickup online
    // if (@$disp->NO_ORDER)
    // {
    //   if (in_array($request->input('status'), [1, 4]))
    //   {
    //     $check_pickupOnline = PsbModel::checkPickupOnline($disp->NO_ORDER);

    //     if (count($check_pickupOnline) > 0)
    //     {
    //       $validation_pickupOnline = PsbModel::validationPickupOnline($disp->NO_ORDER);

    //       if (count($validation_pickupOnline) == 0)
    //       {
    //         return back()->with('alerts', [['type' => 'danger', 'text' => '<strong>GAGAL</strong> Harap Update Order di Bot Tactical Sampai COMPLETED (PS) !']]);
    //       }
    //     }
    //   }
    // }

    // $this->handleFileUpload($request, $id);
    $input = $request->only([
      'status'
    ]);
    $rules = array(
      'status' => 'required',
      'noPelangganAktif'  => 'required|min:11'
    );
    $messages = [
      'dropcore_label_code.required'                  => 'Silahkan isi kolom "Dropcore Label Code" Bang!',
      'odp_label_code.required'                       => 'Silahkan isi kolom "ODP Label Code" Bang!',
      'status.required'                               => 'Silahkan isi kolom "Status" Bang!',
      'splitter.required'                             => 'Silahkan isi kolom "Jenis Splitter" Bang!',
      'catatan.required'                              => 'Silahkan isi kolom "Catatan" Bang!',
      'detek.required'                                => 'Silahkan isi kolom "Detek" Bang!',
      'hd_manja.required'                             => 'Silahkan isi kolom "HD Manja" Bang!',
      'hd_fallout.required'                             => 'Silahkan isi kolom "HD Fallout" Bang!',
      'kordinat_pelanggan.required'                   => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
      'nama_odp.required'                             => 'Silahkan isi kolom "nama_odp" Bang!',
      'odp_plan.required'                             => 'Silahkan isi kolom "ODP Plan" Bang!',
      'kordinat_odp.required'                         => 'Silahkan isi kolom "kordinat_odp" Bang!',
      'koordinat_tiang.required'                      => 'Silahkan Masukan Rute Penarikan Bang!',
      'flag_Berita_Acara.required'                    => 'Silahkan isi kolom upload "Berita Acara" Bang!',
      'flag_Redaman_Pelanggan.required'               => 'Silahkan upload "Redaman Pelanggan" Bang!',
      'flag_Redaman_ODP.required'                     => 'Silahkan upload "Hasil Ukur Redaman ODP" Bang!',
      'flag_Lokasi.required'                          => 'Silahkan upload "Foto Lokasi" Bang!',
      'flag_ODP.required'                             => 'Silahkan upload "Foto ODP" Bang!',
      'tbTiang.required'                              => 'Silahakan isi kolom "Tambah Tiang" Bang!',
      'flag_Foto_Action.required'                     => 'Silahkan upload "Foto Action" Bang !',
      'flag_Foto_Pelanggan_dan_Teknisi.required'      => 'Silahkan upload "Foto Pelanggan dan Teknisi" Bang !',
      'flag_Excheck_Helpdesk.required'                => 'Silahkan upload "Excheck Helpdesk" Bang !',
      'flag_Excheck_Dalapa.required'                  => 'Silahkan upload "Excheck Dalapa" Bang !',
      'flag_Stiker_Hotline.required'                  => 'Silahkan upload "Stiker_Hotline" Bang !',
      'catatanRevoke.required'                        => 'Silahkan isi kolom "Catatan Revoke" Bang !',
      'flag_KLEM_S.required'                          => 'Silahkan upload "Foto KLEM S" Bang !',
      'flag_Patchcore.required'                       => 'Silahkan Upload "Foto Patchcore" Bang !',
      'flag_Stopper.required'                         => 'Silahkan Upload "Foto Stopper" Bang !',
      'flag_Breket.required'                          => 'Silahkan Upload "Foto Breket" Bang !',
      'flag_LABEL.required'                           => 'Silahkan Upload "Foto LABEL" Bang !',
      'flag_Roset.required'                           => 'Silahkan Upload "Foro Roset" Bang !',
      'noPelangganAktif.required'                     => 'Silahkan Isi "No. Hp Pelanggan Aktif !',
      'noPelangganAktif.numeric'                      => 'Inputan Hanya Boleh Angka',
      'flag_Pullstrap.required'                       => 'Silahkan Upload "Foto Pullstrap" Bang !',
      'flag_Speedtest.required'                       => 'Silahkan Upload "Foto Speedtest" Bang !',
      'flag_SN_ONT.required'                          => 'Silahkan Upload "Foto SN ONT" Bang !',
      'flag_SN_STB.required'                          => 'Silahkan Upload "Foro SN STB" Bang !',
      'flag_Telephone_Incoming.required'              => 'Silahkan Upload "Foto Telephone Incoming" Bang !',
      'flag_K3.required'                              => 'Silahkan Upload "Foto K3" Bang !',
      'flag_Live_TV.required'                         => 'Silahkan Upload "Foto Live Tv" Bang !',
      'flag_BA_Digital.required'                      => 'Silahkan Upload "Foto BA Digital" Bang !',
      'flag_RFC_Form.required'                        => 'Silahkan Upload "Foto RFC Form" Bang !',
      'rfc_number.required'                           => 'Silahkan isi "NO RFC" Bang !',
      'type_ont.required'                             => 'Silahkan isi "Type ONT" Bang !',
      'sn_ont.required'                               => 'Silahkan isi kolom "SN ONT" Bang!',
      'type_stb.required'                             => 'Silahkan isi "Type STB" Bang !',
      'sn_stb.required'                               => 'Silahkan isi "SN STB" Bang !',
      'type_plc.required'                             => 'Silahkan isi "Type PLC" Bang !',
      'sn_plc.required'                               => 'Silahkan isi "SN PLC" Bang !',
      'type_wifiext.required'                         => 'Silahkan isi "Type WIFI EXT" Bang !',
      'sn_wifiext.required'                           => 'Silahkan isi "SN WIFI EXT" Bang !',
      'type_indibox.required'                         => 'Silahkan isi "Type INDIBOX" Bang !',
      'sn_indibox.required'                           => 'Silahkan isi "SN INDIBOX" Bang !',
      'type_indihomesmart.required'                   => 'Silahkan isi "Type INDIHOME SMART" Bang !',
      'sn_indihomesmart.required'                     => 'Silahkan isi "SN INDIHOME SMART" Bang !',
      'flag_Tray_Cable.required'                      => 'Silhkan Upload "Foto Tray" Bang !',
      'redaman.required'                              => 'Redaman Jangan Dikosongi !',
      'redamanOdp.required'                           => 'Redaman ODP Jangan Dikosongi !',
      'jenisOdp.required'                             => 'Jenis ODP Diisi',
      'redamanInOdp.required'                         => 'Redaman IN ODP Diisi',
      'port_number.required'                          => 'Port Number Diisi Bang',
      'SC_New.required'                               => 'Silahkan isi kolom "SC Baru" Bang!',
      'estimasiPending.required'                      => 'Silahkan masukan tanggal "Estimasi Pending Pengerjaan" !',
      'valins_id.required'                            => 'Silahkan isi kolom "Valins ID" Bang!',
      'material_psb.required'                         => 'Silahkan Plih Salah Satu Jenis Material Bang!',
      'dismantling_status.required'                   => 'Silahkan Plih " Penyebab Berhenti Berlangganan " Bang!',
      'flag_EVIDENCE_RNA_CONTACT_PELANGGAN.required'  => 'Silahkan Upload "Foto Evidence RNA Contact" Bang !',
      'flag_ID_Listrik_Pelanggan.required'            => 'Silahkan Upload "Foto ID Listrik Pelanggan" Bang !',
      'flag_TV_Tuner.required'                        => 'Silahkan Upload "Foto TV Tuner" Bang !',
      'flag_Splicer.required'                         => 'Silahkan Upload "Foto Splicer" Bang !',
      'flag_Test_Pen.required'                        => 'Silahkan Upload "Foto Test Pen" Bang !'
    ];


    $validator = Validator::make($request->all(), $rules, $messages);
    if ($a == NULL)
    {
      return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>GAGAL</strong> Pilih Status !']]);
    }
    $check_grup = DB::table('psb_laporan_status')->where('laporan_status_id', $a)->select('grup')->first();

    if(session('auth')->id_karyawan != '20981020')
    {
      // NTEPREMIUM
      if($jenisLayanan == "NTEPREMIUM")
      {
        $validator->sometimes('flag_SN_ONT', 'required', function ($input) {
          return true;
        });
      }

      // mandatory ONT_PREMIUM
      if($a == 98)
      {
        // $validator->sometimes('sn_ont', 'required', function ($input) {
        //   return true;
        // });
        $validator->sometimes('flag_SN_ONT', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Speedtest', 'required', function ($input) {
          return true;
        });
      }

      if($a == 97)
      {
        $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
          return true;
        });
      }

      if($a == 99)
      {
        $validator->sometimes('flag_EVIDENCE_RNA_CONTACT_PELANGGAN', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
      }

      if(in_array($a, [100, 102]))
      {
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if($a == 103)
      {
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Excheck_Helpdesk', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('detek', 'required', function ($input) {
          return true;
        });
      }

      // mandatory CABUT_NTE
      if($a == 90)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if($a == 91)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_ID_Listrik_Pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if(in_array($a, [92, 96, 114]))
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if($a == 93)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if($a == 94)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_SN_ONT', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('type_ont', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('sn_ont', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('dismantling_status', 'required', function ($input) {
          return true;
        });
      }

      if($a == 95)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
      }

      if($a == 104)
      {
        $validator->sometimes('catatan', 'required', function ($input) {
          return true;
        });
      }

      // ODP FULL & ODP FULL DAN PERLU INSERT TIANG
      if(in_array($a, [24, 83]))
      {
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_ODP', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Excheck_Helpdesk', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('splitter', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('valins_id', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('redamanOdp', 'required', function ($input) {
          return true;
        });
        // $validator->sometimes('jenisOdp', 'required', function ($input) {
        //         return true;
        // });
        // $validator->sometimes('redamanInOdp', 'required', function ($input) {
        //         return true;
        // });
      }

      // KENDALA JALUR
      if($a == 42)
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
      }

      // ODP TIDAK ADA
      if($a == 34)
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Excheck_Helpdesk', 'required', function ($input) {
          return true;
        });
      }

      // ODP LOSS / ODP RETI
      if(in_array($a, [2, 112]))
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('splitter', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_ODP', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
          return true;
        });
      }

      // GESER TAGGING BY SALES / RUKOS
      if(in_array($a, [105, 23]))
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('splitter', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
      }

      // ALAMAT TIDAK DITEMUKAN / ALAMAT TIDAK LENGKAP
      if(in_array($a, [55, 73]))
      {
        $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'numeric', function ($input) {
          return true;
        });
      }

      // PERMINTAAN BATAL PELANGGAN / PENDING DEPOSIT
      if(in_array($a, [16, 107]))
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'numeric', function ($input) {
          return true;
        });
      }

      // ONU VALINS LIMITASI >32
      if ($a == 113)
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('valins_id', 'required', function ($input) {
          return true;
        });
      }

      // PENDING H+
      if ($a == 48)
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if ($a == 106)
      {
        $validator->sometimes('flag_EVIDENCE_RNA_CONTACT_PELANGGAN', 'required', function ($input) {
          return true;
        });
      }

      if($a == 1 && $jenisPsb <> "MO")
      {
        $validator->sometimes('rfc_number', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('material_psb', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_BA_Digital', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_RFC_Form', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Profile_MYIH', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_ID_Listrik_Pelanggan', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Speedtest', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('flag_Stiker_Hotline', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
        // $validator->sometimes('flag_TV_Tuner', 'required', function ($input) {
        //   return true;
        // });
        // $validator->sometimes('flag_Splicer', 'required', function ($input) {
        //   return true;
        // });
        // $validator->sometimes('flag_Test_Pen', 'required', function ($input) {
        //   return true;
        // });
      }

      // PENDING HI / PENDING H+ / CP RNA ATAU TIDAK AKTIF
      if(in_array($a, [45, 48, 49]))
      {
        $validator->sometimes('flag_EVIDENCE_RNA_CONTACT_PELANGGAN', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
      }

      if($a == 48)
      {
        $validator->sometimes('estimasiPending', 'required', function ($input) {
          return true;
        });
      }

      if(in_array($a, [9, 77]))
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if($a == 75)
      {
        $validator->sometimes('odp_plan', 'required', function ($input) {
          return true;
        });
      }

      //GRUP KT (INVALID)
      if ($check_grup->grup == "KT")
      {
        $validator->sometimes('flag_Excheck_Dalapa', 'required', function ($input) {
          return true;
        });
      }

      if($jenisPsb <> "MO")
      {
        if(in_array($a, [1, 4, 74]))
        {
          $validator->sometimes('valins_id', 'required', function ($input) {
            return true;
          });
          $validator->sometimes('splitter', 'required', function ($input) {
            return true;
          });
        }
      }

      if(in_array($a, [56, 13]))
      {
        $validator->sometimes('SC_New', 'required', function ($input) {
          return true;
        });
      }

      if ($a == 4)
      {
        $validator->sometimes('detek', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('nama_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('kordinat_odp', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('splitter', 'required', function ($input) {
          return true;
        });
        // $validator->sometimes('type_ont', 'required', function ($input) {
        //   return true;
        // });
        // $validator->sometimes('sn_ont', 'required', function ($input) {
        //   return true;
        // });
      }

      if ($a == 28)
      {
        $validator->sometimes('hd_manja', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('hd_fallout', 'required', function ($input) {
          return true;
        });
      }

      //yg harus bekoordinat-pt1/pt2-selesai-kendala sistem-odp loss-insert tiang-hr
      if(in_array($a, [74, 2, 4, 25, 66]))
      {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
            return true;
        });
      }

      if($jeniswo == null && $jenisPsb != 'MO' && $a == 74)
      {
        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('dropcore_label_code', 'required', function ($input) {
          return true;
        });
        $validator->sometimes('odp_label_code', 'required', function ($input) {
          return true;
        });
      };

      if($owner_group != 'AMO')
      {
        if ((in_array($a, [74, 25, 61])) || $a == 4 && $jenisPsb!='MO')
        {
            $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
              return true;
            });

            $validator->sometimes('flag_ODP', 'required', function ($input) {
                return true;
            });
        }
      }

      if($owner_group != 'AMO')
      {
        if (in_array($a, [74, 25, 66, 4]))
        {
            $validator->sometimes('flag_Lokasi', 'required', function ($input) {
                return true;
            });
        };
      }

      if($a == 4)
      {
          $validator->sometimes('flag_Foto_Action', 'required', function ($input) {
              return true;
          });
      }

      if ($a == 741)
      {
          $validator->sometimes('tbTiang', 'required', function ($input) {
              return true;
          });
      }

      if($owner_group != 'AMO')
      {
        if (in_array($a, [25, 74, 66]))
        {
            $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
                return true;
            });
        }
      }


      if($owner_group != 'AMO')
      {
        if (in_array($a, [25, 66]))
        {
            $validator->sometimes('flag_Excheck_Helpdesk', 'required', function ($input) {
                return true;
            });
        }
      }

      //memastikan itu wo PSB
      if($jeniswo == null)
      {
      //filter non MODIFID
        if($jenisPsb != 'MO')
        {
          if(in_array($a, [1, 4, 25, 74, 66, 61]))
          {
            $validator->sometimes('nama_odp', 'required', function ($input) {
              return true;
            });
            $validator->sometimes('kordinat_odp', 'required', function ($input) {
                return true;
            });
            $validator->sometimes('splitter', 'required', function ($input) {
              return true;
            });
          }
        }
      }


      if($a == 74 && $jenisPsb != "MO")
      {
        $validator->sometimes('flag_Redaman_Pelanggan', 'required', function ($input) {
          return true;
        });
      }

      if(!in_array($a, [6, 28, 29, 5, 52, 57, 58, 59, 60]))
      {
        $validator->sometimes('catatan', 'required', function ($input) {
          return true;
        });

        $validator->sometimes('noPelangganAktif', 'required', function ($input) {
          return true;
        });
      }

      // if(in_array($a, [1, 4]))
      // {
      //   if (in_array($disp->jenis_transaksi, ['AO', 'BYOD', 'PDA', 'WMS', 'WMSL']) && in_array($disp->dispatch_sto, ['AMT','BBR','BJM','BLC','BRI','BTB','GMB','KDG','KIP','KPL','KYG','LUL','MRB','MTP','NEG','PGN','PGT','PLE','RTA','SER','STI','TJL','TKI','ULI']))
      //   {
      //     $validator->sometimes('koordinat_tiang', 'required', function ($input) {
      //       return true;
      //     });
      //   }
      // }

      $check_fitur = DB::table('fitur')->first();
      if ($check_fitur->barcode == '1')
      {
        if ((in_array($a, [74, 4]) || ($a==1 && session('witel')=="BALIKPAPAN" && $jenisPsb=="AO")) && $disp->jenis_layanan<>"QC" && session('auth')->level != 2 ) {
          $cek_qrcode = GrabModel::barcodePsb($request->input('dropcore_label_code'));
          if ($cek_qrcode=="FALSE"){
            echo $cek_qrcode;
            return redirect()->back()
                    ->withInput($request->input())
                            ->withErrors($validator)
                            ->with('alerts', [
                              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan QRCODE tidak terdeteksi di DAVAMAS']
                            ]);
          } elseif ($cek_qrcode<>"TRUE" && ($cek_qrcode<>$disp->orderId)){
            return redirect()->back()
                    ->withInput($request->input())
                            ->withErrors($validator)
                            ->with('alerts', [
                              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan QRCODE sudah digunakan untuk '.$cek_qrcode]
                            ]);
          }
        }
      }

      if($jenisPsb == "MO" && !in_array(session('auth')->level, [12,2]))
      {
        if(in_array($a, [74, 1]))
        {
          // $validator->sometimes('flag_Telephone_Incoming', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_LABEL', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_KLEM_S', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_K3', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_Redaman_Pelanggan', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_Lokasi', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_Foto_Pelanggan_dan_Teknisi', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_ODP', 'required', function ($input) {
          //   return true;
          // });
          // $validator->sometimes('flag_Redaman_ODP', 'required', function ($input) {
          //   return true;
          // });

          $validator->sometimes('flag_SN_ONT', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('flag_BA_Digital', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('flag_Live_TV', 'required', function ($input) {
            return true;
          });

          $validator->sometimes('flag_Stiker_Hotline', 'required', function ($input) {
            return true;
          });

          // $validator->sometimes('nama_odp', 'required', function ($input) {
          //       return true;
          // });

          // $validator->sometimes('flag_Tray_Cable', 'required', function ($input) {
          //       return true;
          // });

          // $validator->sometimes('sn_ont', 'required', function ($input) {
          //       return true;
          // });

        }
      }

      if (in_array($a, [43, 44]))
      {
        $validator->sometimes('catatanRevoke', 'required', function ($input) {
          return true;
        });
      }

      if (in_array($a, [1, 37, 38]))
      {
          $validator->sometimes('flag_KLEM_S', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Patchcore', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Stopper', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Breket', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_LABEL', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Roset', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Pullstrap', 'required', function ($input) {
              return true;
          });

          // $validator->sometimes('sn_ont', 'required', function ($input) {
          //     return true;
          // });

          // $validator->sometimes('type_ont', 'required', function ($input) {
          //     return true;
          // });

          $validator->sometimes('nama_odp', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('flag_Tray_Cable', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('dropcore_label_code', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('odp_label_code', 'required', function ($input) {
              return true;
          });

          $validator->sometimes('port_number', 'required', function ($input) {
              return true;
          });
      }

      if ($a == 66)
      {
          // $validator->sometimes('redaman', 'required', function ($input) {
          //   return true;
          // });

          $validator->sometimes('redamanOdp', 'required', function ($input) {
            return true;
          });
      }

      if ($validator->fails())
      {
        return redirect()->back()
          ->withInput($request->input())
          ->withErrors($validator)
          ->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
            ]);
      };

    }

    // if ($request->estimasi_jarak_rute != null)
    // {
    //   if ($request->estimasi_jarak_rute > 350)
    //   {
    //     return back()->with('alerts',[
    //       ['type' => 'danger', 'text' => 'Estimasi Jarak Rute Melebihi 350 Meter']
    //     ]);
    //   }
    // }

    if ($request->kordinat_pelanggan=="ERROR" || $request->kordinat_pelanggan=="Harap Tunggu...")
    {
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Kordinat Pelanggan Salah !. Hidupkan GPS Anda..']]);
    };

    $materials = json_decode($request->input('materials'));
    $dc_roll = [
      (object)
      ['id_item'=>'AC-OF-SM-1B','qty'=> json_decode($request->input('dc_roll')),'rfc'=>'norfc']
      ];
    $materials2 = json_decode($request->input('materials2'));
    $dc_roll_m = json_decode($request->input('dc_roll'));
    // dd($request->input('material_psb'),count($dc_roll_m),$dc_roll_m,count($materials2),$materials2,count($materials),$materials);

    // bypass mandatory untuk tim Saber
    $check_tim = DB::table('1_2_employee')->where('nik', session('auth')->id_karyawan)->where('saber', 1)->first();
    $materialsNte = [];

    if (count($check_tim) != 1)
    {
      if (!in_array($jenisPsb, ["MO","AS"]))
      {
        if ($a == 1 || $a == 4)
        {
          if ($request->input('material_psb') <> NULL)
          {
            if ($request->input('material_psb') == "DC_ROLL")
            {
              if ($dc_roll_m == 0)
              {
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Pilih Jenis Material !']]);
              }
            } elseif ($request->input('material_psb') == "PRECON") {
              if (count($materials2) == 0)
              {
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Pilih Jenis Material !']]);
              }
            }
          } else {
            return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Pilih Salah Satu Jenis Material !']]);
          }
        }
      }

      if(session('auth')->id_karyawan != '20981020')
      {
        if ($jenisPsb <> "MO")
        {
            if ($a == 1 || $a == 4 || $a == 37 || $a == 38)
            {
                if (count($materials) == 0)
                {
                    return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Material Tambahan !']]);
                }
            }
        }
      }

      // $materialsNte = json_decode($request->input('materialsNte'));
      // if ($a == 98)
      // {
      //   if ($materialsNte == [])
      //   {
      //     return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Stock NTE !']]);
      //   }
      // }
      // if (session('auth')->id_karyawan != '20981020')
      // {
      //   if (!in_array($jenisPsb, ["MO","AS"]))
      //   {
      //     if ($a == 1 || $a == 4 || $a == 74)
      //     {
      //       if ($materialsNte == [])
      //       {
      //         return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Stock NTE !']]);
      //       }
      //     }
      //   }
      // }
    }

    // validasi inputan koordinat
    if(in_array($a,[1,4,66,24,83,42,34,2,105,108,11,84,81,71,23,106,25]))
    {
      $cust_gps = explode(',',$request->input('kordinat_pelanggan'));
      if (count($cust_gps) == 1) {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Koordinat Pelanggan Tidak Benar!']
        ]);
      } elseif ($cust_gps[0] == "" || $cust_gps[1] == "") {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Koordinat Pelanggan Tidak Benar!']
        ]);
      }

      $odp_gps = explode(',',$request->input('kordinat_odp'));
      if (count($odp_gps)==1) {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Koordinat ODP Tidak Benar!']
        ]);
      } elseif ($odp_gps[0] == "" || $odp_gps[1] == "") {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Koordinat ODP Tidak Benar!']
        ]);
      }
    }

    // update deviasi
    $check_dt = DB::table('dispatch_teknisi')
    ->leftJoin('regu', 'dispatch_teknisi.id_regu', '=', 'regu.id_regu')
    ->leftJoin('group_telegram', 'regu.mainsector', '=', 'group_telegram.chat_id')
    ->where('dispatch_teknisi.id', $id)
    ->first();

    if ($check_dt->jenis_order == 'SC')
    {
      $check_dps = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger', $check_dt->NO_ORDER)->first();

      // if (count($check_dps) > 0)
      // {
      //   $get_data = DB::select('
      //     SELECT
      //       FORMAT(6371 * acos(cos(radians(pl.gps_latitude)) * cos(radians(dps.lat)) * cos(radians(dps.lon) - radians(pl.gps_longitude)) + sin(radians(pl.gps_latitude)) * sin(radians(dps.lat))),3) as jarak,
      //       dps.orderIdInteger,
      //       pl.id as id_pl,
      //       dt.id as id_dt
      //     FROM Data_Pelanggan_Starclick dps
      //     LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
      //     LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      //     WHERE
      //     dps.orderIdInteger = "'.$check_dps->orderIdInteger.'"
      //   ');

      //   if (count($get_data) > 0)
      //   {
      //     $data = $get_data[0];
      //     DB::table('psb_laporan')->where('id', $data->id_pl)->update([
      //       'deviasi_order'  => $data->jarak
      //     ]);
      //   }
      // }
    }

    //konfirmasi order sebelum lanjut
    if (date('H') < 16)
    {
      if(in_array($a, [28, 29]))
      {
        $check_dt = DB::table('dispatch_teknisi')
        ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
        ->leftJoin('dispatch_teknisi_jenis_layanan', 'dispatch_teknisi.jenis_layanan', '=', 'dispatch_teknisi_jenis_layanan.jenis_layanan')
        ->where('dispatch_teknisi.id', $id)
        ->select('dispatch_teknisi.Ndem', 'dispatch_teknisi.id_regu', 'dispatch_teknisi.jenis_order', 'dispatch_teknisi_jenis_layanan.jenis_transaksi')
        ->first();

        $check_order = DB::table('dispatch_teknisi')
        ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.NO_ORDER', '=', 'Data_Pelanggan_Starclick.orderIdInteger')
        ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
        ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
        ->where('dispatch_teknisi.id_regu', $check_dt->id_regu)
        ->whereIn('dispatch_teknisi.jenis_order', ["CABUT_NTE", "ONT_PREMIUM"])
        ->whereDate('dispatch_teknisi.tgl', date('Y-m-d'))
        ->select('psb_laporan.id_tbl_mj', 'psb_laporan_status.laporan_status_id', 'Data_Pelanggan_Starclick.jenisPsb')
        ->orderBy('dispatch_teknisi.updated_at', 'desc')
        ->first();

        if (!in_array($check_dt->jenis_order, ["MIGRASI_CCAN", "CABUT_NTE", "ONT_PREMIUM"]))
        {
          $check_confirm = DB::table('psb_confirm_order')->where('dt_id', $id)->first();
          if(count($check_confirm)>0)
          {
            if (in_array($check_confirm->status_confirm, [1, 3, 4]))
            {
              DB::table('psb_confirm_order')->where('dt_id', $id)->update([
                'created_at'      => date('Y-m-d H:i:s')
              ]);

              return back()->with('alerts', [
                ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
              ]);
            }
          } else {

            $check_dt = DB::table('dispatch_teknisi')
            ->leftJoin('dispatch_teknisi_jenis_layanan', 'dispatch_teknisi.jenis_layanan', '=', 'dispatch_teknisi_jenis_layanan.jenis_layanan')
            ->select('dispatch_teknisi.NO_ORDER', 'dispatch_teknisi.Ndem', 'dispatch_teknisi.jenis_order', 'dispatch_teknisi_jenis_layanan.jenis_transaksi')
            ->where('dispatch_teknisi.id', $id)->first();

            $checkAllOrder = PsbModel::checkAllOrder(session('auth')->id_karyawan);

            if (count($checkAllOrder) > 0) {

              $jml_addon = [];
              foreach ($checkAllOrder as $key => $value)
              {
                if ($value->jenis_transaksi == 'ADDON')
                {
                  $jml_addon[$key] = 1;
                }
              }

              if (array_sum($jml_addon) > 0)
              {
                DB::table('psb_confirm_order')->insert([
                  'dt_id'             => $id,
                  'id_order'          => $check_dt->Ndem,
                  'status_confirm'    => 4,
                  'id_laporan_status' => $a,
                  'created_at'        => date('Y-m-d H:i:s')
                ]);

                return back()->with('alerts', [
                  ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
                ]);
              }

            } elseif (in_array($check_dt->jenis_transaksi, ["MO", "ADDON"])) {

              DB::table('psb_confirm_order')->insert([
                'dt_id'             => $id,
                'id_order'          => $check_dt->Ndem,
                'status_confirm'    => 4,
                'id_laporan_status' => $a,
                'created_at'        => date('Y-m-d H:i:s')
              ]);

              return back()->with('alerts', [
                ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
              ]);

            } elseif (count($check_order) > 0) {

              if ($check_order->id_tbl_mj == null || $check_order->laporan_status_id == null ||$check_order->laporan_status_id == 6)
              {
                DB::table('psb_confirm_order')->insert([
                  'dt_id'           => $id,
                  'id_order'        => $check_dt->Ndem,
                  'status_confirm'  => 3,
                  'created_at'      => date('Y-m-d H:i:s')
                ]);

                return back()->with('alerts', [
                  ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
                ]);
              }

            } elseif ($check_dt->jenis_order == "SC") {
              $check_status = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger', $check_dt->NO_ORDER)->first();
              if (count($check_status) > 0)
              {
                if ($check_status->orderStatus <> "COMPLETED")
                {
                  DB::table('psb_confirm_order')->insert([
                    'dt_id'             => $id,
                    'id_order'          => $check_dt->Ndem,
                    'status_confirm'    => 1,
                    'id_laporan_status' => $a,
                    'created_at'        => date('Y-m-d H:i:s')
                  ]);

                  return back()->with('alerts', [
                    ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
                  ]);
                }
              }

            } else {

              DB::table('psb_confirm_order')->insert([
                'dt_id'             => $id,
                'id_order'          => $check_dt->Ndem,
                'status_confirm'    => 1,
                'id_laporan_status' => $a,
                'created_at'        => date('Y-m-d H:i:s')
              ]);

              return back()->with('alerts', [
                ['type' => 'warning', 'text' => '<strong>TUNGGU</strong> Konfirmasi Order Sudah Terkirim ke Helpdesk']
              ]);

            }

          }
        }
      }
    }

    // trigger to tactical
    switch ($fitur->tacticalpro) {
      case '1':
          switch ($request->input('status')) {
            case '28':
                ApiModel::uploadPickupOnline($check_dt->Ndem);
              break;
          }
        break;
    }

    // kalo belum ada sc jgn up
    if ($disp->dispatch_by == 5)
    {
      if ($a == 1)
      {
          $dataNotSc = PsbModel::cekDataMyirIfSc($id);
          if ($dataNotSc->id <> NULL)
          {
            if ($dataNotSc->jenis_order <> "SC")
            {
              if ($dataNotSc->ket == 0)
              {
                return back()->with('alerts', [['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan UP, Hubungi HDESK Untuk Sinkron SC']]);
              }
            }
          }
      };
    };
    //

    // cek ba_online
    if ($disp->dispatch_by==NULL && $jenisPsb!="MO"){
        if($a==1 || $a==37 || $a==38){
            $dataStart = DB::table('Data_Pelanggan_Starclick')->where('orderId',$disp->Ndem)->first();
            $dataWms = DB::table('Data_Pelanggan_Starclick')
                         ->where('orderId',$disp->Ndem)
                         ->where('orderName','like','%WMS%')
                         ->first();
          if(count($dataWms)<>1){
            if (count($dataStart)<>NULL){
              $cek_badig = DB::table('fitur')->first();
              if ($cek_badig->badig==1){
                $hasil = $this->getApiBaDigital($disp->Ndem);
                if ($hasil->status=='F'){
                  return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan UP, Upload BA Online Terlebih Dahulu']]);
                };
              }
            }
          }
        }

      $cek_hr = DB::table('fitur')->first();
      if($cek_hr->badig_hr==1){
        if ($a==4){
            $dataStart = DB::table('Data_Pelanggan_Starclick')
                         ->where('orderId',$disp->Ndem)
                         ->whereIn('orderStatus',["Fallout (Data)","Process ISISKA (RWOS)","Completed (PS)","Completed"])
                         ->first();

            if (count($dataStart)<>NULL AND $id<>"264721" AND $id<>"462319"){
                $hasil = $this->getApiBaDigital($disp->Ndem);
                if ($hasil->status=='F'){
                  return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan HR, Upload BA Online Terlebih Dahulu']]);
                };
            }
        }
      }
    };

    // ukur ibooster sebelm up
    if ($a==74 && $disp->jenisPsb <> "AO|WIFI" && !in_array(session('auth')->level, [15,2])){
        if ($disp->jenisPsb=="AO|IPTV"){
            $psb = 'MO';
        }
        else{
            $psb = substr($disp->jenisPsb, 0, 2);
        };

        // $psb   = substr($disp->jenisPsb, 0, 2);
        if ($psb=="AO"){
            if($disp->redaman_iboster==null){
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Ukur Ibooster Dulu']]);
            }
            else{
                if ($disp->redaman_iboster > -13 || $disp->redaman_iboster < -24){
                  return back()->with('alerts',[['type' => 'danger', 'text' => 'Tidak Bisa UP Redaman Ibooster Diijinkan  (> -13) - (< -23)']]);
                }
            }
        }
    }

    // jika status Double Input & Input Ulang harus mengisi SC Baru, dan sesuai sama orderId Data Pelanggan Starclick
    if ($a == 56 || $a == 13){
      $checkSCNew = PsbModel::checkSCNew($request->input('SC_New'));
      if (count($checkSCNew)==0){
        return back()->with('alerts',[['type' => 'danger', 'text' => 'SC Baru Tidak Terdaftar di Data Pelanggan Starclick']]);
      }
    }


    // if ($a==57){
    //     $simpan = array();
    //     $getData = DB::table('Data_Pelanggan_Starclick')->where('orderId',$disp->Ndem)->orwhere('kcontact','LIKE','%'.$disp->Ndem.'%')->first();
    //     $myir = '';
    //     if(count($getData)<>0){
    //         $simpan = [
    //             'MYIR_ID'     => $getData->myir,
    //             'orderId'     => $getData->orderId,
    //             'orderName'   => $getData->orderName,
    //             'orderAddr'   => $getData->orderAddr,
    //             'orderKontak' => $getData->orderKontak,
    //             'orderDate'   => $getData->orderDate,
    //             'alproname'   => $getData->alproname,
    //             'jenisPsb'    => $getData->jenisPsb,
    //             'sto'         => $getData->sto,
    //             'lon'         => $getData->lon,
    //             'lat'         => $getData->lat,
    //             'ndemSpeedy'  => $getData->ndemSpeedy,
    //             'ndemPots'    => $getData->ndemPots,
    //             'kcontact'    => $getData->kcontact,
    //             'internet'    => $getData->internet,
    //             'orderStatus' => $getData->orderStatus,
    //             'orderStatusId' => $getData->orderStatusId
    //         ];
    //     }
    //     else{
    //       $getData = DB::table('psb_myir_wo')->where('myir',$disp->Ndem)->first();
    //       $getKordinat = DB::table('dispatch_teknisi')
    //                 ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
    //                 ->leftJoin('psb_laporan','dispatch_teknisi.id','psb_laporan.id_tbl_mj')
    //                 ->select('dispatch_teknisi.Ndem', 'psb_laporan.kordinat_pelanggan')
    //                 ->where('dispatch_teknisi.Ndem',$getData->myir)
    //                 ->first();

    //       if ($getKordinat){
    //           $kordinatt = explode(',',$getKordinat->kordinat_pelanggan);
    //           if (count($kordinatt)>1){
    //             $lot = $kordinatt[0];
    //             $lat = $kordinatt[1];
    //           }
    //           else{
    //             $lot = $kordinatt[0];
    //             $lat = '';
    //           }
    //       }

    //       if (count($getData)<>0){
    //           $simpan = [
    //               'MYIR_ID'     => $getData->myir,
    //               'orderId'     => $getData->sc,
    //               'orderName'   => $getData->customer,
    //               'orderAddr'   => $getData->alamatLengkap,
    //               'orderKontak' => $getData->picPelanggan,
    //               'orderDate'   => $getData->orderDate,
    //               'alproname'   => $getData->namaOdp,
    //               'jenisPsb'    => null,
    //               'sto'         => $getData->sto,
    //               'lon'         => $lot,
    //               'lat'         => $lat,
    //               'ndemSpeedy'  => null,
    //               'ndemPots'    => null,
    //               'kcontact'    => $getData->kcontack,
    //               'internet'    => $getData->no_internet,
    //               'orderStatus' => null,
    //               'orderStatusId' => null
    //           ];

    //           $myir = $getData->myir;
    //       }
    //     };

    //     if (count($simpan)<>0){
    //         if ($myir<>''){
    //             DB::table('Data_Pelanggan_UNSC')->where('MYIR_ID',$myir)->delete();
    //         };

    //         DB::table('Data_Pelanggan_UNSC')->insert($simpan);
    //     }
    // };

    $get_next_step = DB::table('psb_laporan_status')
                      ->select('step_tracker.*')
                      ->leftJoin('step_tracker','psb_laporan_status.step_id','=','step_tracker.step_tracker_id')
                      ->where('psb_laporan_status.laporan_status_id',$request->input('status'))
                      ->first();

    $next_step = DB::table('dispatch_teknisi')
                     ->where('id',$id)
                     ->update([
                       'step_id' => $get_next_step->step_tracker_id
                     ]);

    $exists = DB::select('
      SELECT a.*,b.id_regu, b.Ndem, b.dispatch_by,b.jenis_layanan
      FROM psb_laporan a, dispatch_teknisi b
      WHERE a.id_tbl_mj=b.id AND a.id_tbl_mj = ?
    ',[
      $id
    ]);

    $sendReport = 0;
    $scid = NULL;

    $cust_gps = explode(',', $request->input('kordinat_pelanggan'));
    if (count($cust_gps) == 2)
    {
      $gps_latitude = @$cust_gps[0];
      $gps_longitude = @$cust_gps[1];
    } else {
      $gps_latitude = null;
      $gps_longitude = null;
    }

    // insert nog master log
    self::nogMaster($request);

    if (count($exists)) {
      $data = $exists[0];

      if ($data->dispatch_by == NULL)
      {
        $scid = $data->Ndem;
      } else {
        $scid = NULL;
      }

      if($data->status_laporan!=$request->input('status')){
        $sendReport = 1;
      }
      // $kpro_status = '';
      // if ($request->input('status')==2){
      //   $kpro_status = "Kendala Teknik";
      // } else if ($request->input('status')==10){
      //   $kpro_status = "Proses PT2/PT3";
      // } else if ($request->input('status')==3) {
      //   $kpro_status = "Kendala Pelanggan";
      // }
      //if ($kpro_status<>'')
      //$this->kpro($data->Ndem,$kpro_status,'manja',$request->input('catatan'));
      DB::transaction(function() use($request, $id, $data, $auth, $materials, $materials2, $materialsNte, $dc_roll, $scid, $gps_latitude, $gps_longitude) {
        $tgl_up = "";
        if($data->status_laporan!=$request->input('status') && $request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        DB::table('psb_laporan_log')->insert([
           'created_at'          => DB::raw('NOW()'),
           'created_by'          => $auth->id_karyawan,
           'created_name'        => $auth->nama,
           'regu_id'             => $auth->id_regu,
           'regu_nama'           => $auth->uraian,
           'status_laporan'      => $request->input('status'),
           'startLatTechnition'  => $request->input('startLatTechnition'),
           'startLonTechnition'  => $request->input('startLonTechnition'),
           'id_regu'             => $data->id_regu,
           'catatan'             => $request->input('catatan'),
           'Ndem'                => $data->Ndem,
           'psb_laporan_id'      => $data->id
         ]);

        DB::table('psb_laporan')
          ->where('id', $data->id)
          ->update([
            'modified_at'         => DB::raw('NOW()'),
            'modified_by'         => $auth->id_karyawan,
            'regu_id'             => $auth->id_regu,
            'regu_nama'           => $auth->uraian,
            'catatan'             => $request->input('catatan'),
            'detek'               => $request->input('detek'),
            'hd_manja'            => $request->input('hd_manja'),
            'hd_fallout'          => $request->input('hd_fallout'),
            'status_laporan'      => $request->input('status'),
            'dismantling_status'  => $request->input('dismantling_status'),
            'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
            'koordinat_tiang'     => $request->input('koordinat_tiang'),
            'estimasi_jarak_rute' => $request->input('estimasi_jarak_rute'),
            'gps_latitude'        => $gps_latitude,
            'gps_longitude'       => $gps_longitude,
            'startLatTechnition'  => $request->input('startLatTechnition'),
            'startLonTechnition'  => $request->input('startLonTechnition'),
            'nama_odp'            => $request->input('nama_odp'),
            'kordinat_odp'        => $request->input('kordinat_odp'),
            'odp_plan'            => $request->input('odp_plan'),
            'dropcore_label_code' => $request->input('dropcore_label_code'),
            'odp_label_code'      => $request->input('odp_label_code'),
            'port_number'         => $request->input('port_number'),
            'splitter'            => $request->input('splitter'),
            'redaman'             => $request->input('redaman'),
            'tgl_up'              => $tgl_up,
            'ttId'                => $request->tbTiang,
            'catatanRevoke'       => $request->input('catatanRevoke'),
            'noPelangganAktif'    => $request->input('noPelangganAktif'),
            'scid'                => $scid,
            'redaman_odp'         => $request->input('redamanOdp'),
            'jenis_odp'           => $request->input('jenisOdp'),
            'redaman_in'          => $request->input('redamanInOdp'),
            'SC_New'              => $request->input('SC_New'),
            'estimasiPending'     => date('Y-m-d', strtotime( $request->input('estimasiPending') ) ),
            'valins_id'           => $request->input('valins_id'),
            'material_psb'        => $request->input('material_psb'),
            'rfc_number'          => $request->input('rfc_number')
          ]);

        // if status odp loss, insert tiang, odp full
        // update field status_kendala dan tgl_status_kendala
        // hanya teknisi
        $dataNik = DB::table('psb_nik_scbe')->where('nik',$auth->id_karyawan)->first();
        if (count($dataNik) == 0)
        {
            $statusSc = '';
            $getStatusSc = DB::table('Data_Pelanggan_Starclick')->where('orderId',$data->Ndem)->first();
            if($getStatusSc)
            {
                $statusSc = $getStatusSc->orderStatus;
            };

            $dataKendala = DB::table('psb_laporan_status')->where('laporan_status_id',$request->input('status'))->first();
            if ($dataKendala)
            {
                if ($dataKendala->ketList == 'KT')
                {
                    DB::table('psb_laporan')
                        ->where('id',$data->id)
                        ->update([
                            'status_kendala'      => $dataKendala->laporan_status,
                            'tgl_status_kendala'  => date('Y-m-d H:i:s'),
                            'status_sc'           => $statusSc
                        ]);
                };
            };
        };

        // if ($request->input('status')==11 || $request->input('status')==24 || $request->input('status')==2){
        //     if ($request->input('status')==11){
        //         $statusKendala = "INSERT TIANG";
        //     }
        //     else if ($request->input('status')==24){
        //         $statusKendala = "ODP FULL";
        //     }
        //     else{
        //         $statusKendala = "ORDER MAINTENANCE";
        //     };

        //     DB::table('psb_laporan')
        //       ->where('id',$data->id)
        //       ->update([
        //           'status_kendala'      => $statusKendala,
        //           'tgl_status_kendala'  => date('Y-m-d H:i:s')
        //       ]);
        // };

        if ($data->jenis_layanan<>"QC") {
          $this->incrementStokTeknisi($data->id, $auth->id_karyawan);
        }
        // DB::table('psb_laporan_material')
        //   ->where('psb_laporan_id', $data->id)
        //   ->delete();
        // foreach($ntes as $nte) {
        //   DB::table('psb_laporan_material')->insert([
        //     'id' => $data->id.'-'.$nte,
        //     'psb_laporan_id' => $data->id,
        //     'id_item' => $nte,
        //     'qty' => 1,
        //     'type' =>2
        //   ]);
        //   DB::table('nte')
        //     ->where('id', $nte)
        //     ->update([
        //       'position_type'  => 3,
        //       'position_key'  => $data->id_tbl_mj,
        //       'status' => 1
        //     ]);
        // }
      if ($materials2){
        $this->insertMaterials($data->id, $materials2);
      }
      $cek_dc_roll = $request->input('dc_roll');
      if($cek_dc_roll<>0){
        if ($dc_roll){
          $this->insertMaterials($data->id, $dc_roll);
        }
      }
      if ($materials){
        $this->insertMaterials($data->id, $materials);
      }
      $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      foreach($materialsNte as $mNte) {
        if ($mNte->qty <> 1) {
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Jumlah Unit NTE dengan Benar !']]);
        }
        if ($mNte->kat == "typeont") {
          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'typeont'             => $mNte->jenis,
            'snont'               => $mNte->id
          ]);
        } elseif ($mNte->kat == "typestb") {
          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'typestb'             => $mNte->jenis,
            'snstb'               => $mNte->id
          ]);
        }
          DB::table('nte')->where('sn', $mNte->id)->update([
            'status'              => 1,
            'qty'                 => $mNte->qty,
            'id_dispatch_teknisi' => $id,
            'position_key'        => $auth->id_karyawan
          ]);
      }

      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
        'typeont'               => $request->input('type_ont'),
        'snont'                 => $request->input('sn_ont'),
        'typestb'               => $request->input('type_stb'),
        'snstb'                 => $request->input('sn_stb'),
        'type_plc'              => $request->input('type_plc'),
        'sn_plc'                => $request->input('sn_plc'),
        'type_wifiext'          => $request->input('type_wifiext'),
        'sn_wifiext'            => $request->input('sn_wifiext'),
        'type_indibox'          => $request->input('type_indibox'),
        'sn_indibox'            => $request->input('sn_indibox'),
        'type_indihomesmart'    => $request->input('type_indihomesmart'),
        'sn_indihomesmart'      => $request->input('sn_indihomesmart')
      ]);

      $check_dismantle = DB::table('dispatch_teknisi')->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')->leftJoin('gudang','group_telegram.gudang_nama','=','gudang.nama_gudang')->select('gudang.id_gudang','dispatch_teknisi.jenis_order','dispatch_teknisi.jenis_layanan','regu.id_regu','regu.nik1','regu.nik2')->where('dispatch_teknisi.id', $id)->first();
      if ($check_dismantle->jenis_order == "CABUT_NTE" || $check_dismantle->jenis_layanan == "CABUT_NTE") {

        // insert collect data nte
        PsbModel::updateCollectNTE($id);

        if ($request->input('type_ont') <> NULL && $request->input('sn_ont') <> NULL) {
          $check_ont = DB::table('nte')->where('sn' ,$request->input('sn_ont'))->first();
          if (count($check_ont)>0) {
            DB::table('nte')->where('sn', $request->input('sn_ont'))->update([
              'jenis_nte'             => $request->input('type_ont'),
              'sn'                    => $request->input('sn_ont'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => 20981020,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_ont'),
              'sn'                    => $request->input('sn_ont'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => 20981020,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_stb') <> NULL && $request->input('sn_stb') <> NULL) {
          $check_stb = DB::table('nte')->where('sn' ,$request->input('sn_stb'))->first();
          if (count($check_stb)>0) {
            DB::table('nte')->where('sn', $request->input('sn_stb'))->update([
              'jenis_nte'             => $request->input('type_stb'),
              'sn'                    => $request->input('sn_stb'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_stb'),
              'sn'                    => $request->input('sn_stb'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_plc') <> NULL && $request->input('sn_plc') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_plc'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_plc'))->update([
              'jenis_nte'             => $request->input('type_plc'),
              'sn'                    => $request->input('sn_plc'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_plc'),
              'sn'                    => $request->input('sn_plc'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_wifiext') <> NULL && $request->input('sn_wifiext') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_wifiext'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_wifiext'))->update([
              'jenis_nte'             => $request->input('type_wifiext'),
              'sn'                    => $request->input('sn_wifiext'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_wifiext'),
              'sn'                    => $request->input('sn_wifiext'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_indibox') <> NULL && $request->input('sn_indibox') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_indibox'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_indibox'))->update([
              'jenis_nte'             => $request->input('type_indibox'),
              'sn'                    => $request->input('sn_indibox'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_indibox'),
              'sn'                    => $request->input('sn_indibox'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_indihomesmart') <> NULL && $request->input('sn_indihomesmart') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_indihomesmart'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_indihomesmart'))->update([
              'jenis_nte'             => $request->input('type_indihomesmart'),
              'sn'                    => $request->input('sn_indihomesmart'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_indihomesmart'),
              'sn'                    => $request->input('sn_indihomesmart'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
      }

      });

      if ($a<>52 && $a<>57 && $a<>58 && $a<>59 && $a<>60){
        if($sendReport){
            if ($disp->dispatch_by==6){
                // exec('cd ..;php artisan sendReportReboundary '.$id.' > /dev/null &');
            }
            else{
                // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
            }
        };
      };

      // send tele grup KP
      $lapStatus = $request->input('status');
      $getGrup   = DB::table('psb_laporan_status')->where('laporan_status_id',$lapStatus)->first();
      if ($getGrup->grup=="KP"){
            // exec('cd ..;php artisan sendReportKp '.$id.' > /dev/null &');
      };

      if(in_array($a, [89, 90, 91, 92, 93, 94, 95, 96]))
      {
        $cno_data = DB::SELECT('
        SELECT
        cno.*,
        r.uraian as tim,
        dps.internet,
        pl.noPelangganAktif,
        pl.snont,
        pl.snstb,
        pl.catatan,
        pls.laporan_status
        FROM dispatch_teknisi dt
        LEFT JOIN cabut_nte_order cno ON dt.NO_ORDER = cno.wfm_id
        LEFT JOIN Data_Pelanggan_Starclick dps ON cno.order_id = dps.orderIdInteger
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        dt.id = "'.$id.'"
        ');

        $cno = $cno_data[0];

        $messageio  = "FORM PENGISIAN GDRIVE KENDALA CABUTAN\n";
        $messageio .= "REGIONAL 6\n";
        $messageio .= "WITEL KALSEL\n";
        $messageio .= "TIM : ".$cno->tim."\n";
        $messageio .= "\n";
        $messageio .= "WFM ID : ".$cno->wfm_id."\n";
        $messageio .= "SC ORDER : ".$cno->order_id."\n";
        $messageio .= "NO INET : ".$cno->internet."\n";
        $messageio .= "NAMA PELANGGAN : ".$cno->nama_pelanggan."\n";
        $messageio .= "NO HP PELANGGAN : ".$cno->noPelangganAktif."\n";
        $messageio .= "SN ONT : ".$cno->snont."\n";
        $messageio .= "SN STB : ".$cno->snstb."\n";
        $messageio .= "KETERANGAN KENDALA : ".$cno->catatan."\n";

        // TIM CABUTAN PROVISIONING
        $chatID = "-1001242865242";

        Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messageio
        ]);

        $photoInputs = $this->photoInputs;
        for($i=0;$i<count($photoInputs);$i++) {
          $table = DB::table('hdd')->get()->first();
          $upload = $table->public;
          $file = public_path()."/".$upload."/evidence/".$id."/".$photoInputs[$i].".jpg";
          if (file_exists($file)){
            Telegram::sendPhoto([
              'chat_id' => $chatID,
              'caption' => $photoInputs[$i],
              'photo' => $file
            ]);
          }
        }

      }

      // dd($data->dispatch_by);
      // if ($data->dispatch_by<>'5' && $cekNdemMigrasi<>'5'){
      //     $this->sendtobotkpro($id);
      // }

      // proses kirim sms
      // $getMyir = DB::table('Data_Pelanggan_Starclick')->where('orderId',$data->Ndem)->first();
      // if (!empty($getMyir)){
      //     $dataMyir  = $getMyir->kcontact;
      //     $pisah     = explode(';', $dataMyir);
      //     $myir      = $pisah[1];
      // };

      // $dataStatuslaporan = DB::table('psb_laporan_status')->where('laporan_status_id',$request->input('status'))->first();
      // if ($dataStatuslaporan->grup=="KT" || $dataStatuslaporan->grup=="KP"){
      //     $dataEksis = DB::table('psb_laporan_log')->where('Ndem',$data->Ndem)->where('status_laporan',$request->input('status'))->get();
      //     if (count($dataEksis)==0){
      //       $pesan = "Pelanggan Yth. \n";
      //       $pesan .= "Order PSB Anda dg nomor '".$myir."' Saat ini \n";
      //       $pesan .= "RESCHEDULE dengan alasan '".$dataStatuslaporan->laporan_status."' Jika sesuai abaikan sms ini, \n";
      //       $pesan .= "jika tidak silahkan sampaikan keterangan dg mereply sms ini.";

      //       // masukkan log
      //       DB::table('psb_laporan_log')->insert([
      //          'created_at'      => DB::raw('NOW()'),
      //          'created_by'      => $auth->id_karyawan,
      //          'status_laporan'  => $request->input('status'),
      //          'id_regu'         => $data->id_regu,
      //          'catatan'         => 'Pesan SMS! '.$pesan,
      //          'Ndem'            => $data->Ndem,
      //          'psb_laporan_id'  => $data->id
      //       ]);

      //       // kirim sms
      //       $psn = str_replace('\n', ' ', $pesan);
      //       $dataDikirim = '("'.$data->noPelangganAktif.'","'.$psn.'","true","3006")';
      //       $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
      //       DB::connection('mysql2')->insert($sql);
      //     }
      // };


      // if ($a == 74) {
      //   echo "kirim sms";
      //   $get_order = DB::SELECT('
      //     SELECT
      //     *,
      //     a.id as id_dt
      //     FROM
      //       dispatch_teknisi a
      //     LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      //     LEFT JOIN regu c ON a.id_regu = c.id_regu
      //     LEFT JOIN Data_Pelanggan_Starclick d ON a.Ndem = d.orderId
      //     LEFT JOIN group_telegram e ON c.mainsector = e.chat_id
      //     WHERE
      //       a.id = "'.$id.'"
      //       ')[0];

            // if ($get_order->orderId<>NULL && $get_order->sms_active_prov==1):
            // $pesan = "Pelanggan Yth. \n";
            // $pesan .= "Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut \n";
            // $pesan .= "http://tomman.info/rv/".$this->encrypt($get_order->id_dt)." \n";
            // $pesan .= "Jika layanan brmasalah hub 05116775775";

            // masukkan log
            // DB::table('psb_laporan_log')->insert([
            //    'created_at'      => DB::raw('NOW()'),
            //    'created_by'      => $auth->id_karyawan,
            //    'status_laporan'  => $request->input('status'),
            //    'id_regu'         => $get_order->id_regu,
            //    'catatan'         => 'Pesan SMS Terkirim ',
            //    'Ndem'            => $get_order->Ndem,
            //    'psb_laporan_id'  => $data->id
            // ]);

            // kirim sms
            // $psn = str_replace('\n', ' ', $pesan);
            // $dataDikirim = '("'.$get_order->orderKontak.'","'.$psn.'","true","3006")';
            // $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
            // DB::connection('mysql2')->insert($sql);
            // endif;
      // };

      // send dava
      // if($a==1 && $jenisPsb=="AO"){
      //     // $getData = DB::table('dispatch_teknisi')
      //     //       ->leftJoin('Data_Pelanggan_Starclick','dispatch_teknisi.Ndem','=','Data_Pelanggan_Starclick.orderId')
      //     //       ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //     //       ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.*', 'psb_laporan.*')
      //     //       ->where('dispatch_teknisi.id',$id)
      //     //       ->get();

      //     $getData = DB::table('dispatch_teknisi')
      //       ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
      //       ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //       ->select('dispatch_teknisi.*', 'psb_myir_wo.*', 'psb_laporan.*')
      //       ->where('dispatch_teknisi.id',$id)
      //       ->get();

      //     if (count($getData)){
      //         $getData = DB::table('dispatch_teknisi')
      //             ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.sc')
      //             ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
      //             ->select('dispatch_teknisi.*', 'psb_myir_wo.*', 'psb_laporan.*')
      //             ->where('dispatch_teknisi.id',$id)
      //             ->get();
      //     };

      //     $data = array();
      //     if(count($getData)<>null){
      //         foreach($getData as $dt){
      //             $korOdp = explode(',', $dt->kordinat_odp);
      //             $data = [
      //                   'id'              => $id,
      //                   'odp_location'    => $dt->nama_odp,
      //                   'port_number'     => $dt->port_number,
      //                   'latitude'        => '',
      //                   'longitude'       => '',
      //                   'sn_ont'          => $dt->snont,
      //                   'nama_pelanggan'  => $dt->customer,
      //                   'voice_number'    => $dt->picPelanggan,
      //                   'internet_number' => $dt->no_internet,
      //                   'date_validation' => date('Y-m-d'),
      //                   'qr_port_odp'     => '',
      //                   'qr_dropcore'     => $dt->dropcore_label_code,
      //                   'nama_jalan'      => $dt->alamatLengkap,
      //                   'contact'         => $dt->noPelangganAktif,
      //             ];
      //         }
      //     };

      //     $pesan = $this->insertQrCodeApi($data);
      //     $listPesan = json_decode($pesan);
      //     if (is_array($listPesan->message)){
      //         $pesan = '';
      //         foreach($listPesan->message as $psn){
      //           $pesan .= $psn.', ';
      //         }
      //         $pesan = substr($pesan, 0, -2);
      //         return back()->with('alerts',[['type'=>'danger', 'text'=>'Gagal Simpan Data "<b>'.$pesan.'"</b>']]);
      //     };
      // };

      // exec('cd ..;php artisan kpro '.$disp->Ndem.' Laporan > /dev/null &');

      // kirim log comparin
      $return_data = DB::table('dispatch_teknisi')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.id',$id)->first();

      // dd($return_data,$request->input('status'),$id);
      if (!in_array($return_data->jenis_order, ["CABUT_NTE", "ONT_PREMIUM"]) || !in_array($return_data->jenis_layanan, ["CABUT_NTE", "ONT_PREMIUM"]))
      {
        $fitur = DB::table('fitur')->first();
        if ($fitur->comparin_log_laporan == 1)
        {
          // if (in_array($request->input('status'),[6,28,29,5,31]))
          // {
            $this->logComparin($id);
          // }
        }
      }
      // if (in_array($request->input('status'),[24, 25, 42, 66, 77, 112, 113]))
      // {
        $laporan_dalapa = DB::table('psb_laporan')->where('id_tbl_mj', $id)->first();
        if($laporan_dalapa->dalapa_checklist == null)
        {
          $this->sendwoDalapa($request,$id,"createwo");
        } elseif (in_array($laporan_dalapa->dalapa_checklist, [1, 2])) {
          $this->sendwoDalapa($request,$id,"updatewo");
        }
      // }

      // send report to whatsapp
      if (session('auth')->level == 10 && $request->input('status') == 1)
      {
        $whatsapp = DB::table('whatsapp_api_login')->where('divisi', 'provisioning')->first();
        if ($whatsapp->status == 1)
        {

          DB::statement('UPDATE `dispatch_teknisi` SET jenis_order = "SC" WHERE jenis_order = "MYIR" AND Ndem LIKE "52%"');

          $check = DB::table('dispatch_teknisi')->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')->where('dispatch_teknisi.id', $id)->select('dispatch_teknisi.Ndem', 'dispatch_teknisi.NO_ORDER', 'dispatch_teknisi.id_regu', 'dispatch_teknisi.jenis_order', 'psb_laporan.id as pl_id')->first();
          if ($check->jenis_order == 'SC') {
            $data = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger', $check->NO_ORDER)->first();
            $inet = $data->internet;
            if (substr($data->orderNotel, 0, 4))
            {
              $number = $request->input('noPelangganAktif');
            } else {
              $number = ($data->orderNotel ?: $request->input('noPelangganAktif'));
            }
          } elseif ($check->jenis_order == 'MYIR') {
            $data = DB::table('psb_myir_wo')->where('myir', $check->Ndem)->first();
            $inet = $data->no_internet;
            $expl_number = explode('/', $data->picPelanggan);
            if (count($expl_number) > 1)
            {
              $number = str_replace(array(' ','+'), '', ($expl_number[0] ?: $expl_number[1]));
            } else {
              $number = str_replace(array(' ', '+'), '', ($data->picPelanggan ?: $request->input('noPelangganAktif')));
            }
          }

          $file = 'https://biawak.tomman.app/image/sticker-broadcast-wa.png';
          $msg = "Pelanggan Indihome yang kami hormati,\n\n";
          $msg .= "Untuk meningkatkan pelayanan kami, mohon bantuannya untuk memberikan penilaian atas Pemasangan Indihome dengan nomor *$inet* yang baru saja dilaksanakan.\n";
          $msg .= "Harap klik tautan dibawah ini, untuk mengisi penilaiannya.\n\n";
          $msg .= "Sebagai informasi tambahan jika di kemudian hari terjadi gangguan silahkan hubungi Whatsapp Customer service kami di link dibawah agar mendapat prioritas pelayanan terimakasih\n\nhttps://wa.me/6281254284618";
          $msg .= "Hormat Kami,\n\nTelkom Witel Kalsel";

          $msg2 = 'https://biawak.tomman.app/rv/'.$this->encrypt($id).'';
          ApiModel::sendWhatsapp($whatsapp->sender, $number, $msg, "null", "message");
          ApiModel::sendWhatsapp($whatsapp->sender, $number, $msg2, "null", "message");

          // insert log
          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => session('auth')->id_karyawan,
            'status_laporan'  => $request->input('status'),
            'id_regu'         => $check->id_regu,
            'catatan'         => 'Pesan WhatsApp Terkirim ',
            'Ndem'            => $check->Ndem,
            'psb_laporan_id'  => $check->pl_id
          ]);
        }
      }

      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials, $materials2, $materialsNte, $dc_roll, $scid, $gps_latitude, $gps_longitude) {
        $tgl_up = "";
        if($request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        $a = $request->input('status');

        $insertId = DB::table('psb_laporan')->insertGetId([
          'created_at'          => DB::raw('NOW()'),
          'modified_at'         => DB::raw('NOW()'),
          'created_by'          => $auth->id_karyawan,
          'regu_id'             => $auth->id_regu,
          'regu_nama'           => $auth->uraian,
          'id_tbl_mj'           => $id,
          'catatan'             => $request->input('catatan'),
          'detek'               => $request->input('detek'),
          'hd_manja'            => $request->input('hd_manja'),
          'hd_fallout'          => $request->input('hd_fallout'),
          'status_laporan'      => $request->input('status'),
          'dismantling_status'  => $request->input('dismantling_status'),
          'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
          'koordinat_tiang'     => $request->input('koordinat_tiang'),
          'estimasi_jarak_rute' => $request->input('estimasi_jarak_rute'),
          'gps_latitude'        => $gps_latitude,
          'gps_longitude'       => $gps_longitude,
          'startLatTechnition'  => $request->input('startLatTechnition'),
          'startLonTechnition'  => $request->input('startLonTechnition'),
          'nama_odp'            => $request->input('nama_odp'),
          'kordinat_odp'        => $request->input('kordinat_odp'),
          'odp_plan'            => $request->input('odp_plan'),
          'dropcore_label_code' => $request->input('dropcore_label_code'),
          'odp_label_code'      => $request->input('odp_label_code'),
          'port_number'         => $request->input('port_number'),
          'splitter'            => $request->input('splitter'),
          'redaman'             => $request->input('redaman'),
          'tgl_up'              => $tgl_up,
          'ttId'                => $request->tbTiang,
          'catatanRevoke'       => $request->input('catatanRevoke'),
          'noPelangganAktif'    => $request->input('noPelangganAktif'),
          'scid'                => $scid,
          'redaman_odp'         => $request->input('redamanOdp'),
          'jenis_odp'           => $request->input('jenisOdp'),
          'redaman_in'          => $request->input('redamanInOdp'),
          'SC_New'              => $request->input('SC_New'),
          'estimasiPending'     => date('Y-m-d', strtotime( $request->input('estimasiPending') ) ),
          'valins_id'           => $request->input('valins_id'),
          'material_psb'        => $request->input('material_psb'),
          'rfc_number'          => $request->input('rfc_number')
        ]);

        $check_dt = DB::table('dispatch_teknisi')->where('id', $id)->first();
        DB::table('psb_laporan_log')->insert([
          'created_at'          => DB::raw('NOW()'),
          'created_by'          => $auth->id_karyawan,
          'created_name'        => $auth->nama,
          'regu_id'             => $auth->id_regu,
          'regu_nama'           => $auth->uraian,
          'status_laporan'      => $request->input('status'),
          'startLatTechnition'  => $request->input('startLatTechnition'),
          'startLonTechnition'  => $request->input('startLonTechnition'),
          'id_regu'             => $check_dt->id_regu,
          'catatan'             => $request->input('catatan'),
          'Ndem'                => $check_dt->Ndem,
          'psb_laporan_id'      => $insertId
        ]);

        //$disp = @$disp[0];
        //print_r($disp);
      if ($materials2){
        $this->insertMaterials($insertId, $materials2);
      }
      $cek_dc_roll = $request->input('dc_roll');
      if($cek_dc_roll<>0){
        if ($dc_roll){
          $this->insertMaterials($id, $dc_roll);
        }
      }
      if ($materials){
          $this->insertMaterials($insertId, $materials);
      }
      $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      foreach($materialsNte as $mNte) {
        if ($mNte->qty <> 1) {
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Jumlah Unit NTE dengan Benar !']]);
        }
        if ($mNte->kat == "typeont") {
          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'typeont'             => $mNte->jenis,
            'snont'               => $mNte->id
          ]);
        } elseif ($mNte->kat == "typestb") {
          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'typestb'             => $mNte->jenis,
            'snstb'               => $mNte->id
          ]);
        }
          DB::table('nte')->where('sn', $mNte->id)->update([
            'status'              => 1,
            'qty'                 => $mNte->qty,
            'id_dispatch_teknisi' => $id,
            'position_key'        => $auth->id_karyawan
          ]);
      }

      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
        'typeont'               => $request->input('type_ont'),
        'snont'                 => $request->input('sn_ont'),
        'typestb'               => $request->input('type_stb'),
        'snstb'                 => $request->input('sn_stb'),
        'type_plc'              => $request->input('type_plc'),
        'sn_plc'                => $request->input('sn_plc'),
        'type_wifiext'          => $request->input('type_wifiext'),
        'sn_wifiext'            => $request->input('sn_wifiext'),
        'type_indibox'          => $request->input('type_indibox'),
        'sn_indibox'            => $request->input('sn_indibox'),
        'type_indihomesmart'    => $request->input('type_indihomesmart'),
        'sn_indihomesmart'      => $request->input('sn_indihomesmart')
      ]);

      $check_dismantle = DB::table('dispatch_teknisi')->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')->leftJoin('gudang','group_telegram.gudang_nama','=','gudang.nama_gudang')->select('gudang.id_gudang','dispatch_teknisi.jenis_order','dispatch_teknisi.jenis_layanan','regu.id_regu','regu.nik1','regu.nik2')->where('dispatch_teknisi.id', $id)->first();
      if ($check_dismantle->jenis_order == "CABUT_NTE" || $check_dismantle->jenis_layanan == "CABUT_NTE") {

        // insert collect data nte
        PsbModel::updateCollectNTE($id);

        if ($request->input('type_ont') <> NULL && $request->input('sn_ont') <> NULL) {
          $check_ont = DB::table('nte')->where('sn' ,$request->input('sn_ont'))->first();
          if (count($check_ont)>0) {
            DB::table('nte')->where('sn', $request->input('sn_ont'))->update([
              'jenis_nte'             => $request->input('type_ont'),
              'sn'                    => $request->input('sn_ont'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => 20981020,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_ont'),
              'sn'                    => $request->input('sn_ont'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => 20981020,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_stb') <> NULL && $request->input('sn_stb') <> NULL) {
          $check_stb = DB::table('nte')->where('sn' ,$request->input('sn_stb'))->first();
          if (count($check_stb)>0) {
            DB::table('nte')->where('sn', $request->input('sn_stb'))->update([
              'jenis_nte'             => $request->input('type_stb'),
              'sn'                    => $request->input('sn_stb'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_stb'),
              'sn'                    => $request->input('sn_stb'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_plc') <> NULL && $request->input('sn_plc') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_plc'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_plc'))->update([
              'jenis_nte'             => $request->input('type_plc'),
              'sn'                    => $request->input('sn_plc'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_plc'),
              'sn'                    => $request->input('sn_plc'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_wifiext') <> NULL && $request->input('sn_wifiext') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_wifiext'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_wifiext'))->update([
              'jenis_nte'             => $request->input('type_wifiext'),
              'sn'                    => $request->input('sn_wifiext'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_wifiext'),
              'sn'                    => $request->input('sn_wifiext'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_indibox') <> NULL && $request->input('sn_indibox') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_indibox'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_indibox'))->update([
              'jenis_nte'             => $request->input('type_indibox'),
              'sn'                    => $request->input('sn_indibox'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_indibox'),
              'sn'                    => $request->input('sn_indibox'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
        if ($request->input('type_indihomesmart') <> NULL && $request->input('sn_indihomesmart') <> NULL) {
          $check_stb = DB::table('nte')->where('sn', $request->input('sn_indihomesmart'))->first();
          if (count($check_stb) > 0) {
            DB::table('nte')->where('sn', $request->input('sn_indihomesmart'))->update([
              'jenis_nte'             => $request->input('type_indihomesmart'),
              'sn'                    => $request->input('sn_indihomesmart'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          } else {
            DB::table('nte')->insert([
              'jenis_nte'             => $request->input('type_indihomesmart'),
              'sn'                    => $request->input('sn_indihomesmart'),
              'position_type'         => $check_dismantle->id_gudang,
              'position_key'          => session('auth')->id_karyawan,
              'status'                => 2,
              'qty'                   => 0,
              'id_dispatch_teknisi'   => 0,
              'date_created'          => date('Y-m-d H:i:s'),
              'tanggal_keluar'        => date('Y-m-d H:i:s'),
              'petugas'               => session('auth')->id_karyawan,
              'id_regu'               => $check_dismantle->id_regu,
              'nik1'                  => $check_dismantle->nik1,
              'nik2'                  => $check_dismantle->nik2,
              'nik_scmt'              => 0,
              'kategori_nte'          => "DISMANTLE"
            ]);
          }
        }
      }
        //insert into log
        /*
        DB::transaction(function() use($request, $disp, $auth, $insertId) {
           DB::table('psb_laporan_log')->insert([
             'created_at'      => DB::raw('NOW()'),
             'created_by'      => $auth->id_karyawan,
             'status_laporan'  => $request->input('status'),
             'id_regu'         => $disp->id_regu,
             'catatan'         => $request->input('catatan'),
             'Ndem'            => $disp->Ndem,
             'psb_laporan_id'  => $insertId
           ]);
         });
         */

      });

      $table = DB::table('hdd')->get()->first();

      // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &', $out);
      if ($disp->dispatch_by==6){
          // exec('cd ..;php artisan sendReportReboundary '.$id.' > /dev/null &');
      }
      else{
          // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
      }

      if(in_array($a, [89, 90, 91, 92, 93, 94, 95, 96]))
      {
        $cno_data = DB::SELECT('
        SELECT
        cno.*,
        r.uraian as tim,
        dps.internet,
        pl.noPelangganAktif,
        pl.snont,
        pl.snstb,
        pl.catatan,
        pls.laporan_status
        FROM dispatch_teknisi dt
        LEFT JOIN cabut_nte_order cno ON dt.NO_ORDER = cno.wfm_id
        LEFT JOIN Data_Pelanggan_Starclick dps ON cno.order_id = dps.orderIdInteger
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        dt.id = "'.$id.'"
        ');

        $cno = $cno_data[0];

        $messageio  = "FORM PENGISIAN GDRIVE KENDALA CABUTAN\n";
        $messageio .= "REGIONAL 6\n";
        $messageio .= "WITEL KALSEL\n";
        $messageio .= "TIM : ".$cno->tim."\n";
        $messageio .= "\n";
        $messageio .= "WFM ID : ".$cno->wfm_id."\n";
        $messageio .= "SC ORDER : ".$cno->order_id."\n";
        $messageio .= "NO INET : ".$cno->internet."\n";
        $messageio .= "NAMA PELANGGAN : ".$cno->nama_pelanggan."\n";
        $messageio .= "NO HP PELANGGAN : ".$cno->noPelangganAktif."\n";
        $messageio .= "SN ONT : ".$cno->snont."\n";
        $messageio .= "SN STB : ".$cno->snstb."\n";
        $messageio .= "KETERANGAN KENDALA : ".$cno->catatan."\n";

        // TIM CABUTAN PROVISIONING
        $chatID = "-1001242865242";

        Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messageio
        ]);

        $photoInputs = $this->photoInputs;
        for($i=0;$i<count($photoInputs);$i++) {
          $table = DB::table('hdd')->get()->first();
          $upload = $table->public;
          $file = public_path()."/".$upload."/evidence/".$id."/".$photoInputs[$i].".jpg";
          if (file_exists($file)){
            Telegram::sendPhoto([
              'chat_id' => $chatID,
              'caption' => $photoInputs[$i],
              'photo' => $file
            ]);
          }
        }

      }

      // kirim log comparin
      $return_data = DB::table('dispatch_teknisi')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.id',$id)->first();

      // dd($return_data,$request->input('status'),$id);
      if(!in_array($return_data->jenis_order, ["CABUT_NTE", "ONT_PREMIUM"]) || !in_array($return_data->jenis_layanan, ["CABUT_NTE", "ONT_PREMIUM"]))
      {
        $fitur = DB::table('fitur')->first();
        if ($fitur->comparin_log_laporan == 1)
        {
          // if (in_array($request->input('status'),[6,28,29,5,31]))
          // {
            $this->logComparin($id);
          // }
        }
      }
      // if (in_array($request->input('status'),[24, 25, 42, 66, 77, 112, 113]))
      // {
        $laporan_dalapa = DB::table('psb_laporan')->where('id_tbl_mj', $id)->first();
        if($laporan_dalapa->dalapa_checklist == null)
        {
          $this->sendwoDalapa($request,$id,"createwo");
        } elseif (in_array($laporan_dalapa->dalapa_checklist, [1, 2])) {
          $this->sendwoDalapa($request,$id,"updatewo");
        }
      // }

      // send report to whatsapp
      if (session('auth')->level == 10 && $request->input('status') == 1)
      {
        $whatsapp = DB::table('whatsapp_api_login')->where('divisi', 'provisioning')->first();
        if ($whatsapp->status == 1)
        {

          DB::statement('UPDATE `dispatch_teknisi` SET jenis_order = "SC" WHERE jenis_order = "MYIR" AND Ndem LIKE "52%"');

          $check = DB::table('dispatch_teknisi')->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')->where('dispatch_teknisi.id', $id)->select('dispatch_teknisi.Ndem', 'dispatch_teknisi.NO_ORDER', 'dispatch_teknisi.id_regu', 'dispatch_teknisi.jenis_order', 'psb_laporan.id as pl_id')->first();
          if ($check->jenis_order == 'SC') {
            $data = DB::table('Data_Pelanggan_Starclick')->where('orderIdInteger', $check->NO_ORDER)->first();
            $inet = $data->internet;
            if (substr($data->orderNotel, 0, 4))
            {
              $number = $request->input('noPelangganAktif');
            } else {
              $number = ($data->orderNotel ?: $request->input('noPelangganAktif'));
            }
          } elseif ($check->jenis_order == 'MYIR') {
            $data = DB::table('psb_myir_wo')->where('myir', $check->Ndem)->first();
            $inet = $data->no_internet;
            $expl_number = explode('/', $data->picPelanggan);
            if (count($expl_number) > 1)
            {
              $number = str_replace(array(' ', '+'), '', ($expl_number[0] ?: $expl_number[1]));
            } else {
              $number = str_replace(array(' ', '+'), '', ($data->picPelanggan ?: $request->input('noPelangganAktif')));
            }
          }

          $file = 'https://biawak.tomman.app/image/sticker-broadcast-wa.png';
          $msg = 'Pelanggan Indihome yang kami hormati,

Untuk meningkatkan pelayanan kami, mohon bantuannya untuk memberikan penilaian atas Pemasangan Indihome dengan nomor [ ' . $inet . ' ] yang baru saja dilaksanakan. Harap klik tautan dibawah ini, untuk mengisi penilaiannya.

Terima kasih, penilaian Anda sangat membantu kami dalam meningkatkan pelayanan.

Hormat Kami,

Telkom Witel Kalsel';

          $msg2 = 'https://biawak.tomman.app/rv/'.$this->encrypt($id).'';
          ApiModel::sendWhatsapp($whatsapp->sender, $number, $msg, "null", "message");
          ApiModel::sendWhatsapp($whatsapp->sender, $number, $msg2, "null", "message");

          // insert log
          DB::table('psb_laporan_log')->insert([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => session('auth')->id_karyawan,
            'status_laporan'  => $request->input('status'),
            'id_regu'         => $check->id_regu,
            'catatan'         => 'Pesan WhatsApp Terkirim ',
            'Ndem'            => $check->Ndem,
            'psb_laporan_id'  => $check->pl_id
          ]);
        }
      }

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }

  }

  public static function bulkLogComparin()
  {
    $startDate = date('Y-m-d',strtotime("-1 days"));
    $endDate = date('Y-m-d');

    $data_progress = DB::SELECT('SELECT dt.id as id_dt, dt.NO_ORDER, dt.jenis_order, dt.tgl, pl.status_laporan FROM dispatch_teknisi dt LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj WHERE dt.jenis_order IN ("SC","MYIR") AND pl.status_laporan IN (6,52,45) AND DATE(dt.tgl) BETWEEN "'.$startDate.'" AND "'.$endDate.'"');

    foreach($data_progress as $data)
    {
      self::logComparin($data->id_dt);

      print_r("Update Progress Log Comparin $data->NO_ORDER \n");
    }

    $data_noupdate = DB::SELECT('SELECT dt.id as id_dt, dt.NO_ORDER, dt.jenis_order, dt.tgl, pl.status_laporan FROM dispatch_teknisi dt LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj WHERE dt.jenis_order IN ("SC","MYIR") AND pl.id IS NULL AND DATE(dt.tgl) BETWEEN "'.$startDate.'" AND "'.$endDate.'"');

    foreach($data_noupdate as $data)
    {
      self::logComparin($data->id_dt);

      print_r("Update No Update Log Comparin $data->NO_ORDER \n");
    }
  }

  public static function logComparin($id)
  {
    $comparin_data = DB::SELECT('
      SELECT
        dt.Ndem as order_id,
        DATE_FORMAT(ADDDATE(dt.tgl, INTERVAL 10 DAY), "%Y-%m-%d") as est_date,
        pl.estimasiPending,
        pls.status_comparin,
        pls.log_action_comparin,
        pls.log_uic_comparin,
        pl.catatan,
        pl.valins_id,
        pls.laporan_status_id,
        pls.laporan_status as status,
        pl.kordinat_pelanggan,
        pl.kordinat_odp,
        dt.jenis_order
      FROM dispatch_teknisi dt
      lEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
        dt.jenis_order IN ("SC", "MYIR") AND
        dt.id = "'.$id.'"
    ')[0];

    if(in_array($comparin_data->jenis_order, ["SC","MYIR"]))
    {
      $cust_gps = explode(',',$comparin_data->kordinat_pelanggan);
      $cust_gps_lat = @$cust_gps[0];
      $cust_gps_long = @$cust_gps[1];

      $odp_gps = explode(',',$comparin_data->kordinat_odp);
      $odp_gps_lat = @$odp_gps[0];
      $odp_gps_long = @$odp_gps[1];

      if($comparin_data->jenis_order == "SC")
      {
        $order_id = $comparin_data->order_id;
      } elseif ($comparin_data->jenis_order == "MYIR") {
        $order_id = 'MYIR-'.$comparin_data->order_id;
      }

      $a = $comparin_data->laporan_status_id;

      if(in_array($a, [4,6,5,28,29,1,31,37,38]))
      {
        $log_status = "OK";
      }else{
        $log_status = "NOK";
      }

      if ($a == 48)
      {
        if ($comparin_data->estimasiPending <> null)
        {
          $est_date = $comparin_data->estimasiPending;
        } else {
          $est_date = date('Y-m-d',strtotime("+1 days"));
        }
      } else {
        $est_date = date('Y-m-d');
      }

      $log_detail = "$comparin_data->catatan [ VALINS: $comparin_data->valins_id ] [$comparin_data->status]";

      // dd($comparin_data,$cust_gps_lat,$cust_gps_long,$odp_gps_lat,$odp_gps_long,$order_id,$log_status,$log_detail);

      $post = http_build_query(
        array(
          'order_id'      => $order_id,
          'log_status'    => $log_status,
          'log_cat'       => $comparin_data->status_comparin,
          'log_action'    => $comparin_data->log_action_comparin,
          'log_uic'       => $comparin_data->log_uic_comparin,
          'est_date'      => $est_date,
          'log_detail'    => $log_detail,
          'log_subcat'    => $comparin_data->status,
          'cust_gps_lat'  => $cust_gps_lat,
          'cust_gps_long' => $cust_gps_long,
          'odp_gps_lat'   => $odp_gps_lat,
          'odp_gps_long'  => $odp_gps_long,
          'token'         => 'c0815b4a7cd87885df4f0548939e9e6e5a8962a7844f1dcb7b3f85888fc4db75'
        )
      );

      $option = array('http' =>
        array(
          'method'  => 'POST',
          'header'  => 'Content-Type: application/x-www-form-urlencoded',
          'content' => $post
        )
      );

      $context = stream_context_create($option);
      $result  = file_get_contents('http://10.128.16.65/comparin/controller/api/tomman_update_log.php', false, $context);

      $hasil = json_decode($result);
      return $hasil;
    }
  }

  public static function sendwoDalapa($request, $id, $kat)
  {
    $check_data = DB::SELECT('
      SELECT
        dt.Ndem as order_id,
        pls.grup,
        pls.subcat_kendala_comparin,
        pl.catatan,
        pl.kordinat_pelanggan,
        pl.kordinat_odp,
        dps.orderName,
        pmw.customer,
        dpsx.orderName as orderNamex,
        pl.noPelangganAktif,
        dpsx.internet,
        pl.SC_New,
        FORMAT(6371 * acos(cos(radians(pl.gps_latitude)) * cos(radians(dps.lat)) * cos(radians(dps.lon) - radians(pl.gps_longitude)) + sin(radians(pl.gps_latitude)) * sin(radians(dps.lat))),3) as jarak_deviasi
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN Data_Pelanggan_Starclick dpsx ON pl.SC_New = dpsx.orderIdInteger
      WHERE
        dt.jenis_order IN ("SC", "MYIR") AND
        pl.id_tbl_mj = "'.$id.'" AND
        dps.orderStatusId IN (1200,1201,1202,51,1300,1700)
    ');

    if (count($check_data)>0)
    {
      $data = $check_data[0];

      $check_dalapa = DB::table('dalapa_comparin_tr6')->where('dalapa_order_id', $data->order_id)->first();

      if (count($check_dalapa) > 0)
      {
        if ($check_dalapa->dalapa_status == 'VALID')
        {
          $kat = 'createwo';
        }
      }

      if ($data->grup == "KT")
      {
        $cat_kendala = "TEKNIK";
      } elseif (in_array($data->grup, ["KP", "HR"])) {
        $cat_kendala = "PELANGGAN";
      }

      $subcat_kendala = $data->subcat_kendala_comparin;

      if ($data->jarak_deviasi > '0.250')
      {
        $cat_kendala = "TEKNIK";
        $subcat_kendala = "SALAH TAGGING";
      }

      $cust_gps = explode(',',$data->kordinat_pelanggan);
      $cust_gps_lat = @$cust_gps[0];
      $cust_gps_long = @$cust_gps[1];

      $odp_gps = explode(',',$data->kordinat_odp);
      $odp_gps_lat = @$odp_gps[0];
      $odp_gps_long = @$odp_gps[1];

      $kontak_pelanggan = ($data->orderName ? : $data->customer ? : $data->orderNamex);

      if ($kat == "createwo") {
        $order_id = "ORDER_ID";
        $id_order = $data->order_id;
      } elseif ($kat == "updatewo") {
        $order_id = "WO_ID";
        $check_id_order = DB::TABLE('dalapa_comparin_tr6')->where('dalapa_order_id', $data->order_id)->first();
        if (count($check_id_order)>0) {
          $id_order = $check_id_order->dalapa_wo_id;
        } else {
          $id_order = $data->order_id."-1";
        }
      }

      // POSTFIELDS
      $that = new PsbController;
      $no = 0;
      if ($cat_kendala)
      {
        if ($cat_kendala == "TEKNIK") {
          $postfields = array(
            $order_id                 => $id_order,
            'CAT_KENDALA'             => $cat_kendala,
            'SUBCAT_KENDALA'          => $subcat_kendala,
            'DETAIL_KENDALA'          => $data->catatan.' [TOMMAN]',
            'CUST_GPS_LAT'            => $cust_gps_lat,
            'CUST_GPS_LONG'           => $cust_gps_long,
            'ODP_GPS_LAT'             => $odp_gps_lat,
            'ODP_GPS_LONG'            => $odp_gps_long,
            'USER_ID'                 => session('auth')->id_karyawan,
            'token'                   => '5ebabd69df0c7e36dfe6dd14c7d068156af5146a4c3867ef056b564f22cfd925',);
  
          foreach($that->photoInputs as $key => $input){
            $path1 = "/upload/evidence/$id";
            $th1   = "$path1/$input-th.jpg";
            $path2 = "/upload2/evidence/$id";
            $th2   = "$path2/$input-th.jpg";
            $path3 = "/upload3/evidence/$id";
            $th3   = "$path3/$input-th.jpg";
            $path4 = "/upload4/evidence/$id";
            $th4   = "$path4/$input-th.jpg";
            $path  = null;
  
            if(@file_exists(public_path().$th1))
            {
                $path = "$path1/$input";
            } elseif (@file_exists(public_path().$th2)) {
                $path = "$path2/$input";
            } elseif (@file_exists(public_path().$th3)) {
                $path = "$path3/$input";
            } elseif (@file_exists(public_path().$th4)) {
                $path = "$path4/$input";
            }
            $th    = "$path-th.jpg";
            $img   = "$path.jpg";
            if(file_exists(public_path().$th))
            {
              $no_count = $no++;
              $postfields["EVIDENCES[$no_count]"] = new \CURLFILE(public_path().$img);
            }
          }
        } elseif ($cat_kendala == "PELANGGAN") {
          $postfields = array(
            $order_id                 => $id_order,
            'CAT_KENDALA'             => $cat_kendala,
            'SUBCAT_KENDALA'          => $subcat_kendala,
            'DETAIL_KENDALA'          => $data->catatan.' [TOMMAN]',
            'CUST_GPS_LAT'            => $cust_gps_lat,
            'CUST_GPS_LONG'           => $cust_gps_long,
            'ODP_GPS_LAT'             => $odp_gps_lat,
            'ODP_GPS_LONG'            => $odp_gps_long,
            'USER_ID'                 => session('auth')->id_karyawan,
            'token'                   => '5ebabd69df0c7e36dfe6dd14c7d068156af5146a4c3867ef056b564f22cfd925',
            'KONTAK_PELANGGAN'        => $kontak_pelanggan,
            'PIC_PELANGGAN'           => $data->noPelangganAktif,
            'NOMOR_SERVICE_LAMA'      => $data->internet,
            'NOMOR_SC_BARU'           => $data->SC_New,);
  
          foreach($that->photoInputs as $key => $input){
            $path1 = "/upload/evidence/$id";
            $th1   = "$path1/$input-th.jpg";
            $path2 = "/upload2/evidence/$id";
            $th2   = "$path2/$input-th.jpg";
            $path3 = "/upload3/evidence/$id";
            $th3   = "$path3/$input-th.jpg";
            $path4 = "/upload4/evidence/$id";
            $th4   = "$path4/$input-th.jpg";
            $path  = null;
  
            if(@file_exists(public_path().$th1))
            {
                $path = "$path1/$input";
            } elseif (@file_exists(public_path().$th2)) {
                $path = "$path2/$input";
            } elseif (@file_exists(public_path().$th3)) {
                $path = "$path3/$input";
            } elseif (@file_exists(public_path().$th4)) {
                $path = "$path4/$input";
            }
            $th    = "$path-th.jpg";
            $img   = "$path.jpg";
  
            if(file_exists(public_path().$th)){
              $no_count = $no++;
              $postfields["EVIDENCES[$no_count]"] = new \CURLFILE(public_path().$img);
            }
          }
        }
      }


      if ($kat == "createwo") {
        $link = "http://10.128.16.65/comparin/controller/api/tomman_create_wo_validasi.php";
      } elseif ($kat == "updatewo") {
        $link = "http://10.128.16.65/comparin/controller/api/tomman_update_wo_validasi.php";
      }

      // dd($postfields,$link);

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $postfields
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      // update log dalapa
      if ($kat == "createwo") {
        DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(['dalapa_checklist' => 1]);
      } elseif ($kat == "updatewo") {
        DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(['dalapa_checklist' => 2]);
      }

      $responses = json_decode($response);
      // dd($response);
      return $responses;

    } else {
      print_r("Gagal Create / Update WO Dalapa");
    }
  }

  private function insertMaterials($psbId, $materials)
  {
    $nik = session('auth')->id_user;
    foreach($materials as $material) {
      $exis = DB::table('psb_laporan_material')
                ->where('id',$psbId.'-'.$material->id_item)
                ->first();

      if ($exis){
        // hapus dulu
        $delete = DB::table('psb_laporan_material')
          ->where('id',$psbId.'-'.$material->id_item)
          ->delete();

        // input lagi
        $input = DB::table('psb_laporan_material')->insert([
          'id' => $psbId.'-'.$material->id_item,
          'psb_laporan_id' => $psbId,
          'id_item' => trim($material->id_item),
          'id_item_bantu' => trim($material->id_item).'_'.$psbId,
          'qty' => $material->qty,
          'type' => 1]);
        }
      else{
        DB::table('psb_laporan_material')->insert([
              'id' => $psbId.'-'.$material->id_item,
              'psb_laporan_id' => $psbId,
              'id_item' => trim($material->id_item),
              'id_item_bantu' => trim($material->id_item).'_'.$psbId,
              'qty' => $material->qty,
              'type' => 1
          ]);


          // simpan ke maintaince_saldo_rfc
          // $pakai = DB::table('psb_laporan')->where('id',$psbId)->first();
          // $dtSaldo     = DB::table('rfc_sal')->where('rfc',$material->rfc)->where('nik',$nik)->first();
          // $dataRegu    = DB::SELECT('SELECT a.*, b.*
          //                          FROM regu a
          //                          LEFT JOIN group_telegram b ON a.mainsector=b.chat_id
          //                          WHERE
          //                             a.ACTIVE=1 AND
          //                             (a.nik1="'.$nik.'" OR a.nik2="'.$nik.'")');
          //
          // $idRegu = NULL; $uraianRegu = NULL; $nik1   = NULL; $nik2 = NULL; $nikTl = NULL;
          // if(count($dataRegu)<>0){
          //     $idRegu     = $dataRegu[0]->id_regu;
          //     $uraianRegu = $dataRegu[0]->uraian;
          //     $nik1       = $dataRegu[0]->nik1;
          //     $nik2       = $dataRegu[0]->nik2;
          //     $nikTl      = $dataRegu[0]->TL_NIK;
          // }

            // DB::table('maintenance_saldo_rfc')->where('created_by',$nik)->where('bantu',$material->rfc.'#'.$material->id_item)->where('action',2)->where('transaksi',1)->delete();
            // DB::table('maintenance_saldo_rfc')->insert([
            //     'id_pengeluaran'    => $dtSaldo->alista_id,
            //     'rfc'               => $dtSaldo->rfc,
            //     'regu_id'           => $idRegu,
            //     'regu_name'         => $uraianRegu,
            //     'nik1'              => $nik1,
            //     'nik2'              => $nik2,
            //     'niktl'             => $nikTl,
            //     'created_at'        => DB::raw('NOW()'),
            //     'created_by'        => $nik,
            //     'id_pemakaian'      => $pakai->id_tbl_mj,
            //     'value'             => $material->qty * -1,
            //     'id_item'           => $material->id_item,
            //     'action'            => '2',
            //     'bantu'             => $material->rfc.'#'.$material->id_item,
            //     'transaksi'         => '1'
            // ]);
          ////
      }

      // send report
      // get id dari psbid
      $idPsb = PsbModel::getIdWoFrormPsbid($psbId);
      // exec('cd ..;php artisan sendReportMaterial '.$idPsb->id.' > /dev/null &');

      // cek data material di psb_laporan_material type 1 klo ada update saldo
      $dataMaterial = PsbModel::cekDataMaterial($psbId);
      if (count($dataMaterial)<>0){
          // update saldo terpakai
          $dataA = DB::table('psb_laporan_material')
                    ->where('id_item_bantu',$material->id_item)
                    ->get();
          $terpakai = 0;
          foreach($dataA as $aaa){
              $terpakai += $aaa->qty;
          }

          // update terpakai
          // DB::table('rfc_sal')->where('nik',$nik)->where('id_item_bantu',$dataA[0]->id_item_bantu)->update(['jmlTerpakai' => $terpakai]);
      }

    }

  }

  private function decrementStokTeknisi($id_karyawan, $materials)
  {
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_teknisi
        WHERE id_karyawan = ? and id_item = ?
      ',[
        $id_karyawan, $material->id_item
      ]);

      if (count($exists)) {
        DB::table('stok_material_teknisi')
          ->where('id_item', $material->id_item)
          ->where('id_karyawan', $id_karyawan)
          ->decrement('stok', $material->qty);
      }else{
        $insertId=DB::table('stok_material_teknisi')->insertGetId([
          'id_item' => $material->id_item,
          'id_karyawan' => $material->qty
        ]);
        DB::table('stok_material_teknisi')
          ->where('id', $insertId)
          ->decrement('stok', $material->qty);
      }

    }
  }
  private function incrementStokTeknisi($id, $idkaryawan)
  {
    $materials = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 1
      ',[
        $id
      ]);
    foreach($materials as $material) {
      DB::table('stok_material_teknisi')
        ->where('id_item', $material->id_item)
        ->where('id_karyawan', $idkaryawan)
        ->increment('stok', $material->qty);
    }
    $ntes = DB::select('
        SELECT *
        FROM psb_laporan_material
        WHERE psb_laporan_id = ? and type = 2
      ',[
        $id
      ]);
    foreach($ntes as $nte) {
      DB::table('nte')
        ->where('id', $nte->id_item)
        ->update([
            'position_type'  => 2,
            'position_key'  => $idkaryawan,
            'status' => 1
          ]);
    }
  }

  public static function getCoordinateCustomerCustom()
  {
    $data = DB::select("
      SELECT
        dt.id AS id_dt,
        dps.orderIdInteger AS order_id,
        pl.gps_latitude AS lat_pelanggan,
        pl.gps_longitude AS long_pelanggan,
        pl.nama_odp AS odp,
        SUBSTRING_INDEX(pl.kordinat_odp, ',', 1) AS lat_odp,
        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_odp, ',', -1), ',', 1) AS lon_odp,
        r.uraian AS nama_tim,
        ma.mitra_amija_pt AS nama_mitra
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ('SC', 'MYIR')
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = 'KALSEL'
      WHERE
        ma.mitra_amija_pt IS NOT NULL AND
        dps.jenisPsb LIKE 'AO%' AND
        dps.orderStatus IN ('Completed (PS)', 'COMPLETED') AND
        (DATE(dps.orderDate) BETWEEN '2016-01-01' AND '2019-12-31')
      GROUP BY dps.orderIdInteger
      ORDER BY dps.orderDate ASC
    ");

    foreach ($data as $k => $v)
    {
      $id = $v->id_dt;

      $list_photos = ["Lokasi","Lokasi_1","Lokasi_2", "ODP"];

      foreach ($list_photos as $photo)
      {
        $path = null;
        $path1 = public_path().'/upload/evidence/'.$id.'/'.$photo.'.jpg';
        $path2 = public_path().'/upload2/evidence/'.$id.'/'.$photo.'.jpg';
        $path3 = public_path().'/upload3/evidence/'.$id.'/'.$photo.'.jpg';
        $path4 = public_path().'/upload4/evidence/'.$id.'/'.$photo.'.jpg';
        
        if (file_exists($path1))
        {
          $path = $path1;
        }
        elseif (file_exists($path2))
        {
          $path = $path2;
        }
        elseif (file_exists($path3))
        {
          $path = $path3;
        }
        elseif (file_exists($path4))
        {
          $path = $path4;
        }

        if (file_exists($path))
        {
          self::getLatLonTimePhoto($id, $photo, $path);
        }
      }
    }
  }

  public static function updateCoordinateCustomerCustom()
  {
    $data = DB::select("
      SELECT
        dt.id AS id_dt,
        dps.orderIdInteger AS order_id,
        pl.gps_latitude AS lat_pelanggan,
        pl.gps_longitude AS long_pelanggan,
        
        pl.lat_lokasi,
        pl.lng_lokasi,
        pl.lat_lokasi_1,
        pl.lng_lokasi_1,
        pl.lat_lokasi_2,
        pl.lng_lokasi_2,
        pl.lat_odpp,
        pl.lng_odpp,

        pl.nama_odp AS odp,
        SUBSTRING_INDEX(pl.kordinat_odp, ',', 1) AS lat_odp,
        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_odp, ',', -1), ',', 1) AS lon_odp,
        r.uraian AS nama_tim,
        ma.mitra_amija_pt AS nama_mitra
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ('SC', 'MYIR')
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = 'KALSEL'
      WHERE
        ma.mitra_amija_pt IS NOT NULL AND
        dps.jenisPsb LIKE 'AO%' AND
        dps.orderStatus IN ('Completed (PS)', 'COMPLETED') AND
        (DATE(dps.orderDate) BETWEEN '2016-01-01' AND '2019-12-31')
      GROUP BY dps.orderIdInteger
      ORDER BY dps.orderDate ASC
    ");

    foreach ($data as $k => $v)
    {
      $id = $v->id_dt;
      if ($v->lat_lokasi != null)
      {
        $lat1 = $v->lat_lokasi;
        $lon1 = $v->lng_lokasi;

        if($v->lat_odpp != null)
        {
          $lat2 = $v->lat_odpp;
          $lon2 = $v->lng_odpp;

          $earthRadius = 6371000; // Earth's radius in meters

          $lat1Rad = deg2rad($lat1);
          $lon1Rad = deg2rad($lon1);
          $lat2Rad = deg2rad($lat2);
          $lon2Rad = deg2rad($lon2);

          $deltaLat = $lat2Rad - $lat1Rad;
          $deltaLon = $lon2Rad - $lon1Rad;

          $a = sin($deltaLat / 2) * sin($deltaLat / 2) +
              cos($lat1Rad) * cos($lat2Rad) *
              sin($deltaLon / 2) * sin($deltaLon / 2);

          $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

          $distance = round($earthRadius * $c, 2);

          DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
            'jarak_meter' => $distance,
          ]);

          print_r("$id $lat1 $lon1 $lat2 $lon2 $distance\n");
        }
      }
    }
  }

  public static function getLatLonTimePhoto($id, $name, $file)
  {
    $info = @exif_read_data($file);

    if (is_file($file))
    {
      if (isset($info['GPSLatitude']) && isset($info['GPSLongitude']) &&
            isset($info['GPSLatitudeRef']) && isset($info['GPSLongitudeRef']) &&
            in_array($info['GPSLatitudeRef'], array('E','W','N','S')) && in_array($info['GPSLongitudeRef'], array('E','W','N','S'))) {

            $GPSLatitudeRef  = strtolower(trim($info['GPSLatitudeRef']));
            $GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));

            $lat_degrees_a = explode('/',$info['GPSLatitude'][0]);
            $lat_minutes_a = explode('/',$info['GPSLatitude'][1]);
            $lat_seconds_a = explode('/',$info['GPSLatitude'][2]);
            $lng_degrees_a = explode('/',$info['GPSLongitude'][0]);
            $lng_minutes_a = explode('/',$info['GPSLongitude'][1]);
            $lng_seconds_a = explode('/',$info['GPSLongitude'][2]);

            $lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
            $lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
            $lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
            $lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
            $lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
            $lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];

            $lat = (float) $lat_degrees+((($lat_minutes*60)+($lat_seconds))/3600);
            $lng = (float) $lng_degrees+((($lng_minutes*60)+($lng_seconds))/3600);

            $GPSLatitudeRef  == 's' ? $lat *= -1 : '';
            $GPSLongitudeRef == 'w' ? $lng *= -1 : '';

            switch ($name) {
              case 'Lokasi':
                  $whr_lat = 'lat_lokasi';
                  $whr_lng = 'lng_lokasi';
                break;
              case 'Lokasi_1':
                  $whr_lat = 'lat_lokasi_1';
                  $whr_lng = 'lng_lokasi_1';
                break;
              case 'Lokasi_2':
                  $whr_lat = 'lat_lokasi_2';
                  $whr_lng = 'lng_lokasi_2';
                break;
              case 'ODP':
                  $whr_lat = 'lat_odpp';
                  $whr_lng = 'lng_odpp';
                break;
            }

            DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
              $whr_lat => $lat,
              $whr_lng => $lng
            ]);
            
            print_r("$id $name $lat $lng $file\n");
        }
    }
  }

  private function handleFileUpload($request, $id)
  {
   $HasilFoto = array();
   $HasilFoto['type'] = 'success';
   $HasilFoto['message'] = 'Berhasil';
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      $table = DB::table('hdd')->get()->first();
      $hdd = $table->active_hdd;
      $upload = $table->public;
      if ($request->hasFile($input)) {
        $path = public_path().'/'.$upload.'/evidence/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
             // $HasilFoto['type'] = 'danger';
             // $HasilFoto['message'] = 'Gagal membuat folder';
             //  return $HasilFoto;
          }
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //$HasilFoto = array();

        $filemime = @exif_read_data($file);

        if (in_array($name, ["Lokasi","Lokasi_1","Lokasi_2"]))
        {
          self::getLatLonTimePhoto($id, $name, $file);
        }

        if ($name <> "Surat_Pernyataan_Deposit" && $name <> "Berita_Acara" && $name <> "Telephone_Incoming" && $name <> "Telephone_Outgoing" && $name <> "Speedtest" && $name <> "Wifi_Analyzer" && $name <> "Excheck_Helpdesk" && $name<> "Redaman_Pelanggan" && $name <> 'BA_Digital' && $name <> 'Profile_MYIH' && $name <> 'ID_Listrik_Pelanggan' && $name <> 'EVIDENCE_RNA_CONTACT_PELANGGAN' && $name <> 'Test_Pen' && !in_array(session('auth')->level, [12,2])) {
          if (@array_key_exists("Software",$filemime)){
            // dd($filemime['Software']);
            if (!in_array(@$filemime['Software'] ,["Timestamp Camera","Timestamp Camera ENT","Timestamp Camera Enterprise"]))
            {
             $HasilFoto['type'] = 'danger';
             $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
              return $HasilFoto;
            }
          } else {
          //  $HasilFoto['type'] = 'danger';
          //  $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
          //   return $HasilFoto;
          }
        }
        $existing = "$path/$name";
        echo $existing;
        if (file_exists(public_path().$existing)) {
          $jpg = "$existing.jpg";
          $th = "$existing-th.jpg";
          $kpro = "$existing-kpro.jpg";

          unlink(public_path().$jpg);
          unlink(public_path().$th);
          if (file_exists(public_path().$kpro)) {
            unlink(public_path().$kpro);
          }
        }
        //TODO: path, move, resize
        try {

          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
          // return$HasilFoto;
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $HasilFoto;
        }
      }
    }
    return $HasilFoto;
    // foreach($this->photoInputs as $name) {
    //   $flag  = 'flag_'.$name;
    //   $input = 'photo-'.$name;
    //   // if ($request->$flag==1){
    //       if ($request->hasFile($input)) {
    //         $path = public_path().'/upload/evidence/'.$id.'/';
    //         if (!file_exists($path)) {
    //           if (!mkdir($path, 0770, true))
    //             return 'gagal menyiapkan folder foto evidence';
    //         }
    //         $file = $request->file($input);
    //         // $ext = $file->guessExtension();
    //         $ext = 'jpg';
    //         //TODO: path, move, resize
    //         try {
    //           $moved = $file->move("$path", "$name.$ext");

    //           $img = new \Imagick($moved->getRealPath());

    //           $img->scaleImage(100, 150, true);
    //           $img->writeImage("$path/$name-th.$ext");



    //         }
    //         catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
    //           return 'gagal menyimpan foto evidence '.$name;
    //         }
    //       }
    //   // }
    // }
  }

  public function deleteFoto($id,$foto){
    $path1 = "/upload/evidence/{$id}";
    $th1   = "$path1/$foto-th.jpg";
    $path2 = "/upload2/evidence/{$id}";
    $th2   = "$path2/$foto-th.jpg";
    $path3 = "/upload3/evidence/{$id}";
    $th3   = "$path3/$foto-th.jpg";
    $path4 = "/upload4/evidence/{$id}";
    $th4   = "$path4/$foto-th.jpg";

    if(file_exists(public_path().$th1)){
        $path = "$path1/$foto";
    }elseif(file_exists(public_path().$th2)){
        $path = "$path2/$foto";
    }elseif(file_exists(public_path().$th3)){
        $path = "$path3/$foto";
    }elseif(file_exists(public_path().$th4)){
        $path = "$path4/$foto";
    } else {
      $path = "$foto kosong";
    }

    $img    = "$path.jpg";
    $th     = "$path-th.jpg";
    $kpro   = "$path-kpro.jpg";
    $backup = "$path-backup.jpg";
    // dd($img,$th,$kpro,$backup);

    if (file_exists(public_path().$img)) {
      // exec("ls", $out);
      // dd($out,$img);

      $sourceFilePath=public_path()."$img";
      $destinationPath=public_path()."/$backup";
      $success = \File::copy($sourceFilePath,$destinationPath);

      unlink(public_path().$img);
      unlink(public_path().$th);

      if(file_exists(public_path().$kpro)){
        unlink(public_path().$kpro);
      }

    }

    return redirect('/'.$id)->with('alerts', [
      ['type' => 'success', 'text' => '<strong>Success</strong> Delete Photo '.$foto.' ']
    ]);


  }

  private function handleFileUploadReboundary($request, $id)
  {
   $HasilFoto = array();
   $HasilFoto['type'] = 'success';
   $HasilFoto['message'] = 'Berhasil';
    foreach($this->photoReboundary as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        $path = public_path().'/upload4/evidence/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true)){
             // $HasilFoto['type'] = 'danger';
             // $HasilFoto['message'] = 'Gagal membuat folder';
             //  return $HasilFoto;
          }
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //$HasilFoto = array();

        $filemime = @exif_read_data($file);

        if ($name <> "Speedtest" ) {
          if (@array_key_exists("Software",$filemime)){
            if (@$filemime['Software']<>"Timestamp Camera" && @$filemime['Software']<>"Timestamp Camera ENT")
            {
             $HasilFoto['type'] = 'danger';
             $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
              return $HasilFoto;
            }
          } else {
           $HasilFoto['type'] = 'danger';
           $HasilFoto['message'] = 'Foto '.$name.' tidak menggunakan timestamp Camera';
            return $HasilFoto;
          }
        }

        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
          // return$HasilFoto;
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return $HasilFoto;
        }
      }
    }
    return $HasilFoto;
  }

  public function sendtotelegram2($id, $cid){

    // data WO
    $order = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        d.updated_at as tgl_dispatch,
        r.uraian as nama_tim,
        m.Kcontact as mKcontact,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        p2.mdf_grup_id as grup_id_psb,
        m2.mdf_grup_id as grup_id_migrasi,
        p2.mdf_grup as grup_psb,
        m2.mdf_grup as grup_migrasi
      FROM dispatch_teknisi d
      LEFT JOIN regu r ON (d.id_regu=r.id_regu)
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN mdf p1 ON ms2n.mdf=p1.mdf
      LEFT JOIN mdf_grup p2 ON p1.mdf_grup_id = p2.mdf_grup_id
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN mdf m1 ON m.MDF = m1.mdf
      LEFT JOIN mdf_grup m2 ON m1.mdf_grup_id = m2.mdf_grup_id
      WHERE d.id = ?
    ',[
      $id
    ])[0];
    // Informasi teknisi
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ])[0];
    //data material
    $get_materials = DB::select('
      SELECT a.*
      FROM
        psb_laporan_material a
      LEFT JOIN psb_laporan b ON a.psb_laporan_id = b.id
      WHERE id_tbl_mj = ?
    ',[$id]);

    $materials = "";
    if (count($get_materials)>0) {
      foreach ($get_materials as $material) :
        if ($material->id_item<>'')
          $materials .= $material->id_item." : ".$material->qty."\n";
      endforeach;
    }  else {
      $materials = "";
    }

    $nama_tim = explode(' ',str_replace(array('-',':'),' ',$order->nama_tim));
    $new_nama_tim = '';
    for ($i=0;$i<count($nama_tim);$i++){
      $new_nama_tim .= $nama_tim[$i];
    }

    $messaggio = "CCAN"."\n\n".$order->nama_tim."\n\n";
    $chatID = $cid;

    $messaggio .= "Dispatch WO Pada : ".$order->tgl_dispatch;
    $messaggio .= "\n\n";

    $messaggio .= "STATUS ORDER : ".$this->status_wo($exists->status_laporan);
    $messaggio .= "\n\n";

    $s = '';
    if ($exists->catatan<>''){
      $messaggio .= "CATATAN : \n";
      $messaggio .= $exists->catatan."\n\n";
    }
    if ($order->Status_Indihome<>'') {
      $s = $order->Nama;
      $messaggio .= $order->Ndem."\n";
      $messaggio .= $order->ND_Speedy."\n";
      $messaggio .= $order->Nama."\n";
      $messaggio .= $order->Kcontact."\n";
      $messaggio .= $order->Jalan." ".$order->No_Jalan."\n";
      $messaggio .= $order->Distrik." ".$order->Kota."\n";
    } else {
      $s = $order->mNama;
      $messaggio .= $order->mND."\n";
      $messaggio .= $order->mND_Speedy."\n";
      $messaggio .= $order->mNama."\n";
      $messaggio .= $order->mAlamat."\n";
    }

    $s .= " #".strtolower($new_nama_tim);

      $messaggio .= "\n";
    if ($exists->kordinat_pelanggan<>'')
    {
      $messaggio .= "Kordinat Pelanggan : \n";
      $messaggio .= $exists->kordinat_pelanggan."\n";
    }
    else
      $messaggio .= "Tidak ada info kordinat pelanggan\n";

      $messaggio .= "\n";

    if ($exists->nama_odp<>'')
    {
      $messaggio .= "Nama ODP : \n";
      $messaggio .= $exists->nama_odp."\n";
    }
    else
      $messaggio .= "Tidak ada info nama ODP pelanggan\n";

      $messaggio .= "\n";
    if ($exists->kordinat_odp<>'')
    {
      $messaggio .= "Kordinat ODP : \n";
      $messaggio .= $exists->kordinat_odp."\n";
    }
    else
      $messaggio .= "Tidak ada info kordinat ODP pelanggan\n";



    $messaggio .= "\n";
    $messaggio .= "#report".strtolower($new_nama_tim)."\n";

    $messaggio .= "\n";


    $messaggio .= $materials;


//     $chatID = "-131356768"; SHVA

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);


    $photoInputs = $this->photoInputs;
    for($i=0;$i<count($photoInputs);$i++) {
      $file = public_path()."/upload4/evidence/".$id."/".$photoInputs[$i].".jpg";
      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);

//      echo $file;
      }
    }

      return redirect('/UP')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
      ]);

  }

  public function deleteMaterials($id)
  {


    DB::transaction( function() use($id) {
      DB::table('psb_laporan_material_log')->insert([
        'id_item_psb'   => $id,
        'deleted_by'    => session('auth')->id_karyawan,
        'deleted_at'    => DB::raw('NOW()')
      ]);

      DB::table('psb_laporan_material')
        ->where('id', $id)
        ->delete();
    });

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menghapus Material . . .']
    ]);
  }

  public function redaman($id){
    $plg = explode("~", $id);
    if (count($plg)>0){
    $no_speedy = @$plg[1];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://embassy.telkom.co.id/embassy/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $akun = DB::table('akun')->where('app', 'SSO Telkom')->first();
    $fields = array(
        'nik'=>$akun->user,
        'pass'=>$akun->pwd,
        'login'=> 'Login'
    );

    $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][0] ;
    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['headers'] = $header_content;
    $header['cookies'] = $cookiesOut;
    curl_setopt($ch, CURLOPT_URL, 'http://embassy.telkom.co.id/embassy/ukur_radonline.php');

    curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    $fields = array(
        'no_speedy'=>$no_speedy,
        'enter'=>'UKUR',
        'domain'=> 'telkom.net'
    );

    $fields_string = http_build_query($fields);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $result = trim(curl_exec($ch));
    curl_close($ch);
    $pos = strpos($result, "#CCDDEE>Rx Power")+50;
    $redaman = substr($result, $pos, 6);
    return $redaman;
  } else {
    return false;
  }
  }

  public function maintenanceOrder($id){
    $get_laporan_status = PsbModel::psb_laporan_status();
    $group_telegram = DB::table('group_telegram')->get();
    $checkONGOING = PsbModel::checkONGOING();
    $list = PsbModel::orderMaintenance($id);

    return view('psb.list2', compact('list','group_telegram','get_laporan_status','checkONGOING'));
  }

  public function maintenanceProgress($id){
      $foto = $this->photoCommon;
      $exists = DB::select('
        SELECT m.*, pl.nama_odp, pl.kordinat_odp, pl.id_tbl_mj, mr.niktl
        FROM maintaince m
        left join psb_laporan pl on m.psb_laporan_id = pl.id
        LEFT JOIN maintenance_regu mr on m.dispatch_regu_id=mr.id
        WHERE m.id = ?
      ', [
        $id
      ]);
      $komen = DB::table('maintenance_note')->where('mt_id', $id)->orderBy('id', 'desc')->get();
      $action_cause = DB::table('maintenance_cause_action')->select('action_cause as text', 'action_cause as id', 'status')->get();
      if (count($exists)) {
        $data = $exists[0];
        if($data->jenis_order == 3)
          $foto = $this->photoOdpLoss;
        else if($data->jenis_order == 4)
          $foto = $this->photoRemo;
        if($data->action_cause){
          $action_cause = DB::table('maintenance_cause_action')
            ->select('action_cause as text', 'action_cause as id', 'status')
            ->where('status', $data->status)->get();
        }
      }
      else{
        $data = new \stdClass();
      }

      $materials = DB::select('
          SELECT i.*, i.uraian as nama_item,COALESCE(m.qty, 0) AS qty FROM maintenance_mtr_program mmp
          LEFT JOIN maintaince_mtr m ON mmp.id_item = m.id_item
            AND m.maintaince_id = ?
          LEFT JOIN khs_maintenance i ON mmp.id_item = i.id_item
          WHERE program_id = ?
          ', [
            $id, $data->jenis_order
          ]);
      // print_r($materials);

      $maintenanceLevel = DB::table('user')->where('id_user','=',session('auth')->id_user)->first();
      $level            = $maintenanceLevel->maintenance_level;

      return view('psb.progress', compact('data', 'materials', 'foto', 'action_cause', 'komen', 'level'));
  }

  private function handleFileUploadMaintenance($request, $id){
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        //dd($input);
        $path = public_path().'/upload/maintenance/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }

  public function saveProgres(Request $request, $id){
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'status.required' => 'Silahkan pilih "Status" Bang!',
      'action_cause.required' => 'Silahkan pilih "Action/Cause" Bang!',
      'splitter.required' => 'Silahkan pilih "Jenis Splitter" Bang!',
      'flag_Sebelum.required' => 'Silahkan Isi Foto "Sebelum" Bang!',
      'flag_Progress.required' => 'Silahkan Isi Foto "Progress" Bang!',
      'flag_Sesudah.required' => 'Silahkan Isi Foto "Sesudah" Bang!',

      'flag_ODP.required' => 'Silahkan Isi Foto "ODP" Bang!',
      'flag_ODP-dan-Redaman-GRID.required' => 'Silahkan Isi Foto "ODP-dan-Redaman-GRID" Bang!',
      'flag_Pengukuran.required' => 'Silahkan Isi Foto "Pengukuran" Bang!',
      'flag_Pelanggan_GRID.required' => 'Silahkan Isi Foto "Pelanggan_GRID" Bang!',
      'flag_BA-Remo.required' => 'Silahkan Isi Foto "BA-Remo" Bang!',
      'flag_Foto-GRID.required' => 'Silahkan Isi Foto "Foto-GRID" Bang!',
      'flag_Denah-Lokasi.required' => 'Silahkan Isi Foto "Denah-Lokasi" Bang!'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);

    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM maintaince
      WHERE id = ?
    ',[
      $id
    ]);
    $this->handleFileUploadMaintenance($request, $id);
    $materials = json_decode($request->input('materials'));
    if (count($exists)) {
      $data = $exists[0];
      if($request->status == 'close' && $data->verify == null && $request->action_cause != "CANCEL ORDER"){
        $validator->sometimes('action_cause', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Sebelum', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Progress', 'required', function ($input) {
            return true;
        });
        $validator->sometimes('flag_Sesudah', 'required', function ($input) {
            return true;
        });
        if($data->jenis_order == 4){
          $validator->sometimes('flag_ODP-dan-Redaman-GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Pengukuran', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Pelanggan_GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_BA-Remo', 'required', function ($input) {
              return true;
          });
        }else if($data->jenis_order == 3){
          $validator->sometimes('flag_ODP', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Foto-GRID', 'required', function ($input) {
              return true;
          });
        }else{
          $validator->sometimes('flag_Foto-GRID', 'required', function ($input) {
              return true;
          });
          $validator->sometimes('flag_Denah-Lokasi', 'required', function ($input) {
              return true;
          });
        }
        if($data->jenis_order == 3 || $data->jenis_order == 4){
          $validator->sometimes('splitter', 'required', function ($input) {
              return true;
          });
        }
      }
      if ($validator->fails()) {
        return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan (Dokumen atau Isian kurang lengkap)']
            ]);
      }
      DB::transaction(function() use($request, $data, $auth, $materials, $id) {
        DB::table('maintaince_mtr')
          ->where('maintaince_id', $data->id)
          ->delete();
        $mtrcount = 0;
        foreach($materials as $material) {
          DB::table('maintaince_mtr')->insert([
            'maintaince_id' => $data->id,
            'id_item'       => $material->id_item,
            'qty'           => $material->qty,
          ]);
          $mtrcount++;
        }
        $total = DB::select('
          SELECT
          (SELECT sum((qty*material_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as material,
          (SELECT sum((qty*jasa_ta)) FROM maintaince_mtr mm left join khs_maintenance i on mm.id_item=i.id_item WHERE mm.maintaince_id = m.id) as jasa
          FROM maintaince m
          WHERE id = ?
        ', [$id])[0];
        $hasil_by_qc = $data->hasil_by_qc;
        $hasil_by_user = $data->hasil_by_user;
        if($request->input('status')=="close"){
          $hasil_by_user = $total->jasa + $total->material;
        }else if($request->input('status')=="qqc"){
          $hasil_by_qc = $total->jasa + $total->material;
        }
        $tgl_selesai = $data->tgl_selesai;
        $verify = $data->verify;
        //if($data->status!=$request->input('status') && $request->input('status')=='close'){
        if($data->status!=$request->input('status')){
          $tgl_selesai = date('Y-m-d H:i:s');
        }
        /*
          null = in TL
          1 = in SM
          2 = in TELKOM
          3 = siap rekon
        */
        if($request->verify != $data->verify){
          if($request->verify > $data->verify){
            //$verify = date('Y-m-d H:i:s');
            $status = "APPROVE";
          }
          else{
            //$verify = NULL;
            $status = "DECLINE";
          }
          if($request->catatan)
            $komen = $request->catatan;
          else
            $komen = $status;
          DB::table('maintenance_note')
          ->insert([
            'mt_id'     => $data->id,
            'catatan'   => $komen,
            'status'   => $status,
            'pelaku'    => $auth->id_karyawan,
            'ts'     => DB::raw('NOW()')
          ]);
        }
        DB::table('maintaince')
          ->where('id', $data->id)
          ->update([
            'status_name'     => $request->input('status'),
            'status'          => $request->input('status'),
            'tgl_selesai'     => $tgl_selesai,
            'verify'          => $request->verify,
            'mtrcount'        => $mtrcount,
            'action'          => $request->input('action'),
            'koordinat'       => $request->input('koordinat'),
            'modified_at'     => DB::raw('NOW()'),
            'modified_by'     => $auth->id_karyawan,
            'hasil_by_user'   => $hasil_by_user,
            'hasil_by_qc'     => $hasil_by_qc,
            'action_cause'    => $request->input('action_cause'),
            'splitter'        => $request->input('splitter'),
          ]);
      });
    }
    if($request->input('status') != $data->status){
      // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
    }
    return redirect('/tech')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
    ]);
  }

  public function maintenanceUpload($id){
    $path2 = public_path().'/upload/maintenance/'.$id.'th';
    $th = array();
    if (file_exists($path2)) {
      $photos = File::allFiles($path2);
      foreach ($photos as $photo){
        array_push($th,(string) $photo);
      }
    }

    return view('psb.photos', compact('id','th'));
  }

  public function uploadSave(Request $request, $id){
    // dd('tes');
    $files = $request->file('photo');
    $path = public_path().'/upload/maintenance/'.$id.'/';
    $th = public_path().'/upload/maintenance/'.$id.'th/';
    if (!file_exists($path)) {
      if (!mkdir($path, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    if (!file_exists($th)) {
      if (!mkdir($th, 0755, true))
        return 'gagal menyiapkan folder foto evidence';
    }
    foreach ($files as $file) {
        $name = $file->getClientOriginalName();
        try {
          $moved = $file->move("$path", "$name");
          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$th/$name");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
    }
    return redirect('/wo/maintenance')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Upload Photo']
    ]);
  }

  public function ubahStatusLaporan($ndem){
      $dispatchTeknisi = DB::table('dispatch_teknisi')->where('ndem','=',$ndem)->first();

      DB::table('dispatch_teknisi')
          ->where('ndem','=',$ndem)
          ->update([
              'step_id'   => '1.0'
          ]);

      DB::table('psb_laporan')
          ->where('id_tbl_mj','=',$dispatchTeknisi->id)
          ->update([
              'catatan'         => '',
              'status_laporan'  => '6'
          ]);
  }

  public function valTot($date, $jam){
      $dataVal = DB::SELECT("SELECT *, count(sto) as jumlah from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND ts LIKE '%".$date."%' group by sto");

      $jmlData = DB::select("SELECT Count(*) as total from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND ts LIKE '%".$date."%'");

      $dataTot = DB::select("SELECT *, SUM(case when status_resume like '%issued%' then 1 else 0 end) as issued, SUM(case when status_resume like '%fallout%' then 1 else 0 end) as fallout  from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND ts LIKE '%".$date.' '.$jam."%' group by sto");

      $jml = DB::select("SELECT *, SUM(case when status_resume like '%issued%' then 1 else 0 end) as issued, SUM(case when status_resume like '%fallout%' then 1 else 0 end) as fallout  from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND ts LIKE '%".$date.' '.$jam."%'");

      return view('laporan.kproval',compact('dataVal','jmlData', 'dataTot', 'jml', 'date', 'jam' ));
  }

  public function detVal($date, $jam, $sto){
      $dataVal = DB::SELECT("SELECT * from kpro_pi WHERE sts='val' AND witel='BANJARMASIN' AND sto='".$sto."' AND ts LIKE '%".$date."%'");

      return view('laporan.kprovalDet',compact('dataVal'));
  }

  public function detTot($date, $jns, $sto, $jam){
      $dataVal = DB::SELECT("SELECT * from kpro_pi WHERE sts='tot' AND witel='BANJARMASIN' AND status_resume like '%".$jns."%' AND sto='".$sto."' AND ts LIKE '%".$date.' '.$jam."%'");

      return view('laporan.kprototDet',compact('dataVal'));
  }

  public function livesearchSC(Request $req){
      $term = trim($req->q);
      if (empty($term)) {
          return \Response::json([]);
      }

      $fetchData = psbmodel::value_search($term);

      $formatted_tags = [];

      foreach ($fetchData as $row) {
          $formatted_tags[] = ["SC"=> $row->orderId];
      }

      return \Response::json($formatted_tags);
  }

   public function inputSebelumRfc($id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE
         i.grup = 1
        ORDER BY id_item
      ', [
        $data->id
      ]);

      // tes ja
      // dd($data->id);

      if ($auth->level==2){
        //   $materials = DB::select('
        //   SELECT
        //     i.id_item, i.nama_item,
        //     COALESCE(i.jmlTerpakai, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
        //   FROM rfc_sal i
        //   where
        //     i.psb_laporan_id = ?
        //   ORDER BY i.rfc
        // ', [
        //   $data->id
        // ]);

        // $materials = DB::select('
        //   SELECT
        //     i.id_item, i.nama_item,
        //     COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
        //   FROM rfc_sal i
        //   LEFT JOIN
        //     psb_laporan_material m
        //     ON i.id_item_bantu = m.id_item_bantu
        //   Where
        //     m.psb_laporan_id = ?
        //   ORDER BY i.id_item
        // ', [
        //   $data->id
        // ]);

      }
      else{
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
            AND m.psb_laporan_id = ?
          WHERE
           i.nik="'.$auth->id_user.'"
          ORDER BY id_item
        ', [
          $data->id
        ]);
      }
    }
    else {
      $data = new \StdClass;
      $data->id = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;
      // TODO: order by most used

      // $materials = DB::select('
      //   SELECT a.id_item, a.nama_item, 0 AS qty
      //   FROM item a left join psb_laporan_material b ON a.id_item=b.id_item WHERE a.grup = 1
      //   GROUP BY a.id_item
      //   ORDER BY a.id_item
      // ');

      $materials = DB::select('
        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc
        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
        ORDER BY rfc
      ');

/*
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          psb_laporan_material m
          ON i.id_item = m.id_item
          AND m.psb_laporan_id = ?
        WHERE 1
        ORDER BY id_item
      ', [
        $data->id
      ]);
*/
    }
    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  id as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
        *,
        pmw.email,
        pmw.no_ktp as no_ktp,
        ms2n.*,
        d.id as id_dt,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.mdf as mMDF,
        m.Kcontact as mKcontact,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.sto,
        e.id as id_dshr,
        dm.*,
        ne.STO as neSTO,
        ne.RK as neRK,
        ne.ND_TELP as neND_TELP,
        ne.ND_INT as neND_INT,
        ne.PRODUK_GGN as nePRODUK_GGN,
        ne.HEADLINE as neHEADLINE,
        ne.LOKER_DISPATCH as neLOKER_DISPATCH,
        ne.KANDATEL as neKANDATEL,
        c.lon,c.lat
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.Ndem = c.orderId
      LEFT JOIN psb_myir_wo pmw ON c.orderId = pmw.sc
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN roc on d.Ndem = roc.no_tiket
      LEFT JOIN nonatero_excel_bank ne ON d.Ndem = ne.TROUBLE_NO
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN dshr e ON c.orderKontak = e.pic
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id=pl.id_tbl_mj
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];

    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next

    $photoInputs = $this->photoInputs;
    $dispatch_teknisi_id = $id;
    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();

    return view('psb.input', compact('data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang'));
  }

  public function listWoTeknisi(){
      $bulan = Date('Y-m');
      $auth = session('auth');
      $list = PsbModel::WorkOrder('NEED_PROGRESS', $bulan);

      return view('psb.listWoTek', compact('list'));
  }

  private function handleFileUpload2($request, $id){
    foreach($this->photoInputs as $name) {
      $flag  = 'flag_'.$name;
      $input = 'photo-'.$name;
      // if ($request->$flag==1){
          if ($request->hasFile($input)) {
            $table = DB::table('hdd')->get()->first();
            $hdd = $table->active_hdd;
            $upload = $table->public;

            $path = public_path().'/'.$upload.'/evidence/'.$id.'/';
            if (!file_exists($path)) {
              if (!mkdir($path, 0770, true))
                return 'gagal menyiapkan folder foto evidence';
            }
            $file = $request->file($input);
            // $ext = $file->guessExtension();
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
              $moved = $file->move("$path", "$name.$ext");

              $img = new \Imagick($moved->getRealPath());

              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'gagal menyimpan foto evidence '.$name;
            }
          }
      // }
    }
  }

  public function kirimSatuKpro($ndem){
      $this->sendtobotkpro($ndem);
      return back()->with('alerts',[['type' => 'success', 'text' => 'Kirim Ke Bot K-Pro Sukses']]);
  }

  public function kirimKoordinatMassalForm(){
      return view('psb.koordinatMasalForm');
  }

  public function kirimKoordinatMassalKirim(Request $req){
      if (empty($req->q)){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Jangan Kosong Mang !']]);
      }

      $datas = explode(' ', $req->q);
      foreach ($datas as $data){
          $id = $data;
          $get_data = DB::SELECT('
            SELECT
              a.*,
              b.mdf_grup_id
            FROM
              Data_Pelanggan_Starclick a
              LEFT JOIN mdf b ON a.sto = b.mdf
            WHERE a.orderId = ?
          ',[ $id ]);

          $get_data = $get_data[0];

          $chatID = "-374194073";
          // $chatID = "192915232";

          if ($get_data->lat<>NULL){
            Telegram::sendLocation([
              'chat_id' => $chatID,
              'latitude' => $get_data->lat,
              'longitude' => $get_data->lon
            ]);

            Telegram::sendMessage([
              'chat_id' => $chatID,
              'text' => $get_data->orderId
            ]);
          }
      }

      return back()->with('alerts',[['type' => 'success', 'text' => 'Berhasil Ngirim, Sabar Nang Lah Jangan Dikajut 1000 kalo Garing Server ']]);
  }

  public function kirimFotoKpro($date){
    $get_sc = DB::SELECT('
      SELECT *,dt.id as id_dt FROM Data_Pelanggan_Starclick a LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem WHERE dt.id IS NOT NULL AND a.orderDatePs LIKE "'.$date.'%"
    ');

    foreach ($get_sc as $sc){
      $id = $sc->id;
      $wo = $sc->orderId;
      $photoInputs = $this->photokpro;
      for($i=0;$i<count($photoInputs);$i++) {
        $file = public_path()."/upload4/evidence/".$id."/".$photoInputs[$i].".jpg";
        if (file_exists($file)){
          Telegram::sendPhoto([
            'chat_id' => "-327333991",
            'caption' => $wo,
            'photo'   => $file
          ]);
        }
      };
    }
    echo "sukses ".$date;
  }

  public function kirimFotoKproScForm(){
      return view('psb.fotoKproMasalForm');
  }

  public function kirimFotoKproScKirim(Request $req){
      if (empty($req->q)){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Jangan Kosong Mang !']]);
      }

      $datas = explode(' ', $req->q);
      foreach ($datas as $data){
          $id = $data;
          $get_data = DB::SELECT('
            SELECT
              *,
              dt.id as id_dt
            FROM
              Data_Pelanggan_Starclick a
              LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem
            WHERE dt.id IS NOT NULL AND
              a.orderId = ?
          ',[ $id ]);

          $get_data = $get_data[0];

          $chatID = "-327333991";
          // $chatID = "192915232";

          $id = $get_data->id;
          $wo = $get_data->orderId;
          $photoInputs = $this->photokpro;
          for($i=0;$i<count($photoInputs);$i++) {
            $file = public_path()."/upload4/evidence/".$id."/".$photoInputs[$i].".jpg";
            if (file_exists($file)){
              Telegram::sendPhoto([
                'chat_id' => $chatID,
                'caption' => $wo,
                'photo'   => $file
              ]);
            }
          };
      }

      return back()->with('alerts',[['type' => 'success', 'text' => 'Berhasil Ngirim, Sabar Nang Lah Jangan Dikajut 1000 kalo Garing Server Foto Pang ini']]);
  }

  function encrypt($id){
      // echo "enkrip : ".$id."<br />";
      $en1 = str_replace('1','K',$id);
      $en2 = str_replace('2','a',$en1);
      $en3 = str_replace('3','N',$en2);
      $en4 = str_replace('4','t',$en3);
      $en5 = str_replace('5','u',$en4);
      $en6 = str_replace('6','r',$en5);
      $en7 = str_replace('7','J',$en6);
      $en8 = str_replace('8','f',$en7);
      $en9 = str_replace('9','X',$en8);
      $en0 = str_replace('0','Y',$en9);
      return $en0;
  }

  public function tes(){
      $data = PsbModel::workOrderProvNeedProgress();
      dd($data);
  }

  public function manualStarclick(){
    $data = new \stdClass();
    $data->orderId = null;
    $data->orderName = null;
    $data->orderAddr = null;
    $data->orderDate = null;
    $data->orderDatePs = null;
    $data->orderCity = null;
    $data->orderStatus = null;
    $data->orderNcli = null;
    $data->ndemSpeedy = null;
    $data->ndemPots = null;
    $data->kcontact = null;
    $data->alproname = null;
    $data->jenisPsb = null;
    $data->sto = null;
    $data->internet = null;
    $data->noTelp = null;
    $data->myir = null;
      return view('psb.manualStarclick',compact('data'));
  }

  public function manualStarclickSave(Request $req){
      $this->validate($req,[
          'orderId'          => 'required|numeric'
      ],[
          'orderId.required'      => 'No. SC Jangan Kosong !',
          'orderId.numeric'       => 'Diisi Angka !',
          'orderNcli.required'    => 'Masukan NCLI Starclick !',
          'orderName.required'  => 'Masukan Nama Pelanggan Starclick !',
          'ndemPots.required'  => 'Masukan POTS Starclick !',
          'ndemSpeedy.required'  => 'Masukan No Internet Starclick !',
          'internet.required'  => 'Masukan No Internet Starclick !',
          'sto.required'  => 'Masukan STO Starclick !',
          'jenisPsb.required' => 'Masukan Jenis PSB Starclick !',
          'alproname.required'  => 'Masukan ODP Starclick !',
          'orderAddr.required'  => 'Masukan Alamat Starclick !',
          'myir.required'  => 'Masukan MYIR Starclick !',
      ]);

    PsbModel::manualStarclickSave($req, $req->orderId);
      return redirect('/dispatch/search')->with('alerts',[['type' => 'success', 'text' => 'Data SC Straclick Berhasil Disimpan :)']]);
    }


   public function reboundaryList(Request $req, $tgl){
      $data = psbmodel::listReboundary($tgl);

      if ($req->has('q')){
          $cari = $req->input('q');
          $data = psbmodel::listReboundaryCari($cari);
      }

      return view('reboundary.list',compact('data'));
  }

  public function inputReboundary(){
      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');

      return view('reboundary.input',compact('dataSektor'));
  }

  public function simpanReboundary(Request $req){
      $this->validate($req,[
          'scid'            => 'required',
          'scid'            => 'numeric',
          'koorPel'         => 'required',
          'odpLama'         => 'required',
          'koorOdpLama'     => 'required',
          'odpBaru'         => 'required',
          'koorOdpBaru'     => 'required',
          'tarikanLama'     => 'required',
          'tarikanLama'     => 'numeric',
          'tarikanEstimasi' => 'required',
          'tarikanEstimasi' => 'numeric',
          'catatan'         => 'required',
          'sektor'          => 'required'

      ],[
          'scid.required'     => 'SCID Diisi',
          'scid.numeric'      => 'Angka Ja',
          'koorPel.required'  => 'Koordinat Pelanggan Jangan Kosong',
          'odpLama.required'  => 'ODP Lama Diisi',
          'koorOdpLama.required'  => 'Koordinat ODP Lama Diisi',
          'odpBaru.required'      => 'ODP Baru Diisi',
          'koorOdpBaru.required'  => 'koordinat ODP Baru DIisi',
          'tarikanLama.required'  => 'Tarikan Lama Diisi',
          'tarikanLama.numeric'   => 'Angka Ja',
          'tarikanEstimasi.required'  => 'Tarikan Estimasi Diisi',
          'tarikanEstimasi.numeric'   => 'Angka Ja',
          'catatan.required'          => 'Catatan Diisi',
          'sektor.required'           => 'Sektor Diisi'
      ]);

      // cek data reboundary kalo sudah ada
      $data = PsbModel::getDataReboundaryByScid($req->scid);
      if ($data){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Sudah Ada Men !']]);
      }
      else{
        $exists = PsbModel::cekSCInDataStarclick($req->scid);
        if ($exists){
            $nik = session('auth')->id_user;
            PsbModel::simpanReboundary($req,$nik,1);

            return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);
        }
        else{
            return back()->with('alerts',[['type' => 'danger', 'text' => 'SCID Tidak VALID !!!']]);
        }
      }

  }

  public function editReboundary($id){
      $data = PsbModel::getDataReboundaryById($id);
      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');

      return view('reboundary.edit',compact('data', 'dataSektor'));
  }

  public function editSimpanReboundary(Request $req, $id){
      $this->validate($req,[
          'scid'            => 'required',
          'scid'            => 'numeric',
          'koorPel'         => 'required',
          'odpLama'         => 'required',
          'koorOdpLama'     => 'required',
          'odpBaru'         => 'required',
          'koorOdpBaru'     => 'required',
          'tarikanLama'     => 'required',
          'tarikanLama'     => 'numeric',
          'tarikanEstimasi' => 'required',
          'tarikanEstimasi' => 'numeric',
          'catatan'         => 'required',
          'sektor'          => 'required'

      ],[
          'scid.required'     => 'SCID Diisi',
          'scid.numeric'      => 'Angka Ja',
          'koorPel.required'  => 'Koordinat Pelanggan Jangan Kosong',
          'odpLama.required'  => 'ODP Lama Diisi',
          'koorOdpLama.required'  => 'Koordinat ODP Lama Diisi',
          'odpBaru.required'      => 'ODP Baru Diisi',
          'koorOdpBaru.required'  => 'koordinat ODP Baru DIisi',
          'tarikanLama.required'  => 'Tarikan Lama Diisi',
          'tarikanLama.numeric'   => 'Angka Ja',
          'tarikanEstimasi.required'  => 'Tarikan Estimasi Diisi',
          'tarikanEstimasi.numeric'   => 'Angka Ja',
          'catatan.required'          => 'Catatan Diisi',
          'sektor.required'           => 'Sektor Diisi'
      ]);


      $nik = session('auth')->id_user;
      PsbModel::simpanReboundary($req,$nik,2,$id);

      return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);

  }

  public function dispatchReboundary($scid, $status){
      $getData = PsbModel::getDataReboundaryByScid($scid);
      $tiket   = '6'.date('m').$getData->scid;

      $regu = DB::select('
        SELECT a.id_regu as id,a.uraian as text, b.title
        FROM regu a
          LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        WHERE
          ACTIVE = 1
      ');

      $dataSektor = DB::SELECT('
        SELECT
        a.chat_id as id,
        a.title as text
        FROM
          group_telegram a
      ');


      return view('reboundary.dispatch',compact('getData', 'tiket', 'regu', 'dataSektor', 'status'));
  }

  public function simpanDispatchReboundary(Request $req){
    $this->validate($req,[
        'tglOpen' => 'required',
        'jamOpen' => 'required',
        'id_regu' => 'required',
        'tgl'     => 'required'
    ],[
        'tglOpen.required'    => 'Tgl Open Disii',
        'jamOpen.required'    => 'Jam Open Diisi',
        'id_regu.required'    => 'Regu Diisi',
        'tgl.required'        => 'Tanggal Dispath Diisi'
    ]);


   // insert into log status
   $auth = session('auth');
   $status_laporan = 6;

   DB::transaction(function() use($req, $auth, $status_laporan) {
       DB::table('psb_laporan_log')->insert([
         'created_at'      => DB::raw('NOW()'),
         'created_by'      => $auth->id_karyawan,
         'created_name'    => $auth->nama,
         'status_laporan'  => $status_laporan,
         'id_regu'         => $req->input('id_regu'),
         'catatan'         => "DISPATCH at ".date('Y-m-d'),
         'Ndem'            => $req->noTiketAsli
       ]);
   });

    DB::transaction(function() use($req, $auth) {
       DB::table('dispatch_teknisi_log')->insert([
         'updated_at'          => DB::raw('NOW()'),
         'updated_by'          => $auth->id_karyawan,
         'tgl'                 => $req->input('tgl'),
         'id_regu'              => $req->input('id_regu'),
         'Ndem'                => $req->noTiketAsli
       ]);
    });

    $now = $req->tglOpen.' '.$req->jamOpen.':00';
    $openTanggal = date('Y-m-d H:i:s',strtotime($now));

    $auth = session('auth');
    $check = DB::table('roc')
              ->where('no_tiket',$req->input('noTiketAsli'))
              ->first();

    if (count($check)>0) {
      DB::table('roc')->where('no_tiket',$req->input('no_tiket'))->update([
        'no_tiket' => $req->input('noTiketAsli'),
        'loker_ta' => $req->input('loker'),
        'alpro'    => $req->input('alpro'),
        'tglOpen'  => $openTanggal,
        'loker_ta' => 6,
        'refSc'    => $req->refSc
      ]);
    } else {
      DB::table('roc')->insert([
        'no_tiket' => $req->input('noTiketAsli'),
        'loker_ta' => $req->input('loker'),
        'alpro'    => $req->input('alpro'),
        'tglOpen'  => $openTanggal,
        'loker_ta' => 6,
        'refSc'    => $req->refSc
      ]);
    }

    // redispatc dan dispatch
    $exists = DB::select('
        SELECT a.*,b.status_laporan, c.mainsector
          FROM dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        WHERE a.Ndem = ?
      ',[
        $req->noTiketAsli
      ]);

      $status_laporan = 6;

      // reset status if redispatch
      if (count($exists)) {
        $data = $exists[0];
        DB::transaction(function() use($req, $data, $auth, $status_laporan) {
          DB::table('psb_laporan')
            ->where('id_tbl_mj', $data->id)
            ->update([
              'status_laporan' => $status_laporan
            ]);
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'created_name'    => $auth->nama,
               'status_laporan'  => $status_laporan,
               'id_regu'         => $req->input('id_regu'),
               'catatan'         => "REDISPATCH",
               'Ndem'            => $req->noTiketAsli,
               'psb_laporan_id'  => $data->id
             ]);
        });

        DB::transaction(function() use($req, $data, $auth) {
          DB::table('dispatch_teknisi')
            ->where('id', $data->id)
            ->update([
              'updated_at'            => DB::raw('NOW()'),
              'updated_by'            => $auth->id_karyawan,
              'tgl'                   => $req->input('tgl'),
              'id_regu'               => $req->input('id_regu'),
              'step_id'               => "1.0"
            ]);
        });
      }
      else {
        DB::transaction(function() use($req, $auth) {
          DB::table('dispatch_teknisi')->insert([
            'updated_at'          => DB::raw('NOW()'),
            'updated_by'          => $auth->id_karyawan,
            'tgl'                 => $req->input('tgl'),
            'id_regu'             => $req->input('id_regu'),
            'created_at'          => DB::raw('NOW()'),
            'Ndem'                => $req->noTiketAsli,
            'step_id'             => "1.0",
            'dispatch_by'         => 6
          ]);
        });

        DB::table('reboundary_survey')->where('scid',$req->refSc)->where('dispatch',0)->update(['dispatch' => 1]);
      }

      PsbModel::updateNoTiket($req->refSc, $req->noTiketAsli);

    return redirect('/reboundary/list/'.date('Y-m-d'))->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> men-dispatch WO Reboundary']
      ]);
  }

  public function inputLaporanReboundary($id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM psb_laporan
      WHERE id_tbl_mj = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];
      if ($auth->level==2){

        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
          Where
            m.psb_laporan_id = ?
          ORDER BY i.rfc asc, i.id_item asc
        ', [
          $data->id
        ]);

        if (empty($materials)){
              $materials = DB::select('
                SELECT * FROM psb_laporan_material where type = 1 AND psb_laporan_id = ?
              ', [
                $data->id
              ]);
        }
      }
      else{
        $materials = DB::select('
          SELECT
            i.id_item, i.nama_item,
            COALESCE(m.qty, 0) AS qty, i.rfc, (i.jumlah - i.jmlTerpakai) as saldo
          FROM rfc_sal i
          LEFT JOIN
            psb_laporan_material m
            ON i.id_item_bantu = m.id_item_bantu
            AND m.psb_laporan_id = ?
          WHERE
           i.nik="'.$auth->id_user.'"
          ORDER BY i.rfc asc, i.id_item asc
        ', [
          $data->id
        ]);
      }
    }
    else {
      $data = new \StdClass;
      $data->id = null;
      $data->typeont = null;
      $data->snont = null;
      $data->typestb = null;
      $data->snstb = null;


      $materials = DB::select('
        SELECT id_item, nama_item, 0 AS qty, (jumlah - jmlTerpakai) as saldo, rfc
        FROM rfc_sal where nik="'.$auth->id_user.'" AND jumlah<>0
        ORDER BY rfc
      ');
    }

    $nte1 = DB::select('select i.id as id, i.sn as text
        FROM nte i
        LEFT JOIN
          psb_laporan_material m
          ON i.id = m.id_item
          AND m.psb_laporan_id = ?
        WHERE m.type=2
        ORDER BY id_item',[
        $data->id
      ]);
    $no = 1;
    $ntes = '';
    foreach($nte1 as $n){
      if($no == 1){
        $ntes .= $n->id;
        $no = 2;
      }else{
        $ntes .= ', '.$n->id;
      }
    }
    $nte = DB::select('select  id as id, sn as text from nte
      where position_key = ?',[
        $auth->id_karyawan
      ]);
    $project = DB::select('
      SELECT
        *,
        ms2n.*,
        d.id as id_dt,
        m.ND as mND,
        m.ND_Speedy as mND_Speedy,
        m.mdf as mMDF,
        m.Kcontact as mKcontact,
        m.Nama as mNama,
        m.Alamat as mAlamat,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.sto,
        e.id as id_dshr,
        dm.*,
        ne.STO as neSTO,
        ne.RK as neRK,
        ne.ND_TELP as neND_TELP,
        ne.ND_INT as neND_INT,
        ne.PRODUK_GGN as nePRODUK_GGN,
        ne.HEADLINE as neHEADLINE,
        ne.LOKER_DISPATCH as neLOKER_DISPATCH,
        ne.KANDATEL as neKANDATEL,
        c.lon,c.lat,
        d.jenis_layanan,
        my.*,
        pl.*
      FROM dispatch_teknisi d
      LEFT JOIN Data_Pelanggan_Starclick c ON d.Ndem = c.orderId
      LEFT JOIN ms2n USING (Ndem)
      LEFT JOIN reboundary_survey rs ON d.Ndem = rs.no_tiket_reboundary
      LEFT JOIN nonatero_excel_bank ne ON d.Ndem = ne.TROUBLE_NO
      LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
      LEFT JOIN micc_wo m on d.Ndem = m.ND
      LEFT JOIN dshr e ON c.orderKontak = e.pic
      LEFT JOIN step_tracker st ON st.step_tracker_id = d.step_id
      LEFT JOIN psb_laporan  pl ON d.id=pl.id_tbl_mj
      LEFT JOIN psb_myir_wo my ON d.Ndem=my.myir
      WHERE
      d.id = ?
    ',[
      $id
    ])[0];


    // error step_id
    if (empty($project->step_next)){

        // cek psb_laporan_log
        $dispatch = DB::table('dispatch_teknisi')
                    ->where('id','=',$id)
                    ->first();

        $psbLog = DB::table('psb_laporan_log')
                    ->where('ndem','=',$dispatch->Ndem)
                    ->orderBy('psb_laporan_log_id','desc')
                    ->first();

        if (empty($psbLog->Ndem)){
            $stepId = '1.0';
        }
        else {
            $psbStatus = DB::table('psb_laporan_status')
                            ->where('laporan_status_id','=',$psbLog->status_laporan)
                            ->first();

            $stepId = $psbStatus->step_id;
        };

        DB::table('dispatch_teknisi')
            ->where('id',$id)
            ->update([
                'step_id'   => $stepId
            ]);

        // code untuk mengisi label
        $dataStep = DB::table('step_tracker')
                      ->where('step_tracker_id','=',$stepId)
                      ->first();

        $step_id   = $dataStep->step_tracker_id;
        $step_next = $dataStep->step_next;
        $step_name = $dataStep->step_name;
    }
    else {
        $step_id   = $project->step_id;
        $step_next = $project->step_next;
        $step_name = $project->step_name;
    }

    if ($step_next=='1.5'){
        $step_next = '1.4';
    }
    else{
      $step_next = $step_next;
    };
    // aslinya
    // $project->step_next
    // $get_Ndem = DB::table('dispatch_teknisi')
    //               ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
    //               ->select('psb_laporan.id','psb_laporan.id_tbl_mj','dispatch_teknisi.Ndem', 'dispatch_teknisi.dispatch_by')
    //               ->where('psb_laporan.id_tbl_mj',$id)
    //               ->first();

    // if (count($get_Ndem)==0){
    //     $get_Ndem = DB::table('dispatch_teknisi')->where('id',$id)->first();
    //     $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    // }
    // else{
    //     $ndemIn  = DB::table('psb_laporan_log')->where('psb_laporan_id',$get_Ndem->id)->groupBy('Ndem')->get();
    //     $ndemNew = '';
    //     if (count($ndemIn)<>0){
    //         if (count($ndemIn)==1){
    //             $ndemNew  = "'".$ndemIn[0]->Ndem."'";
    //             $ndemMyir = DB::table('psb_myir_wo')->where('sc',$ndemIn[0]->Ndem)->first();
    //             if (count($ndemMyir)<>0){
    //                 $ndemNew  .= ",'".$ndemMyir->myir."'";
    //             };
    //         }
    //         else{
    //           foreach($ndemIn as $ndem){
    //               $ndemNew .= "'".$ndem->Ndem."'".',';
    //           }

    //           $ndemNew = substr($ndemNew, 0, -1);
    //         }

    //         $ndemNew = '('.$ndemNew.')';
    //         $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem in '.$ndemNew.') AND';
    //     }
    //     else{
    //         $ndemMyir = DB::table('psb_myir_wo')->where('sc',$get_Ndem->Ndem)->first();
    //         // $cari = 'a.psb_laporan_id = "'.$get_Ndem->id.'" AND';
    //         if (count($ndemMyir)==0){
    //             $cari = 'a.Ndem = "'.$get_Ndem->Ndem.'" AND';
    //         }
    //         else{
    //             $cari = '(a.psb_laporan_id = "'.$get_Ndem->id.'" OR a.Ndem ="'.$ndemMyir->myir.'") AND';
    //         }

    //     }
    //     // dd($cari);
    // };

    // $data_log = DB::SELECT('
    //             SELECT
    //             *
    //             FROM psb_laporan_log a
    //             LEFT JOIN user b ON a.created_by = b.id_user
    //             LEFT JOIN 1_2_employee c ON b.id_karyawan = c.nik
    //             LEFT JOIN psb_laporan_status d ON a.status_laporan = d.laporan_status_id
    //             WHERE
    //               '.$cari.'
    //               a.created_by <> 0
    //             ORDER BY created_at DESC
    //           ');

    $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');

    $photoInputs = $this->photoReboundary;

    $dispatch_teknisi_id = $id;
    // $get_laporan_status = DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV") AND sts="0" AND step_id = "'.$step_next.'" order by urutan asc, laporan_status asc');
    $getTambahTiang     = psbmodel::getTambahTiang();

    // data reboundary
    $dataReboundary = '';
    $dataDispatch = DB::table('dispatch_teknisi')->where('id',$id)->first();
    if ($dataDispatch->dispatch_by==6){
        $dataReboundary = DB::table('dispatch_teknisi')
                            ->leftJoin('reboundary_survey','dispatch_teknisi.Ndem','=','reboundary_survey.no_tiket_reboundary')
                            ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                            ->where('dispatch_teknisi.Ndem',$dataDispatch->Ndem)
                            ->first();
    };

    return view('psb.inputReboundary', compact('data', 'project', 'materials', 'photoInputs', 'ntes', 'nte', 'nte1', 'dispatch_teknisi_id','get_laporan_status','step_id', 'step_name', 'getTambahTiang', 'data_log', 'dataReboundary'));
  }

  public function simpanLaporanReboundary(Request $request, $id){
    $upload = $this->handleFileUploadReboundary($request, $id);
    if (@$upload['type']=="danger")
    return back()->with('alerts', [
      ['type' => 'danger', 'text' => $upload['message']]
    ]);

    $disp = DB::table('dispatch_teknisi')
      ->select('dispatch_teknisi.*', 'Data_Pelanggan_Starclick.jenisPsb','step_tracker.*')
      ->leftJoin('Data_Pelanggan_Starclick', 'dispatch_teknisi.Ndem', '=', 'Data_Pelanggan_Starclick.orderId')
      ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
      ->leftJoin('psb_laporan_status', 'psb_laporan.status_laporan', '=', 'psb_laporan_status.laporan_status_id')
      ->leftJoin('step_tracker', 'psb_laporan_status.step_id', '=', 'step_tracker.step_tracker_id')
      ->where('dispatch_teknisi.id', $id)
      ->first();

    // $this->handleFileUpload($request, $id);
    $auth = session('auth');
    $input = $request->only([
      'status'
    ]);
    $rules = array(
      'status' => 'required'
    );
    $messages = [
      'status.required'  => 'Silahkan isi kolom "Status" Bang!',
      'sn_ont.required'  => 'Silahkan isi kolom "SN ONT" Bang!',
      'catatan.required' => 'Silahkan isi kolom "Catatan" Bang!',
      'kordinat_pelanggan.required' => 'Silahkan isi kolom "kordinat_pelanggan" Bang!',
      'nama_odp.required' => 'Silahkan isi kolom "nama_odp" Bang!',
      'kordinat_odp.required' => 'Silahkan isi kolom "kordinat_odp" Bang!',
      'noPelangganAktif.required'     => 'Silahkan Isi "No. Hp Pelanggan Aktif !',
      'noPelangganAktif.numeric'      => 'Inputan Hanya Boleh Angka',
      'redaman.required'              => 'Redaman Jangan Dikosongi !',
      'flag_Lokasi_Pelanggan.required'          => 'Uploaad Foto "Lokasi Pelanggan" Bang !',
      'flag_Redaman_Ont.required'               => 'Uploaad Foto "Redaman ONT" Bang !',
      'flag_Berita_Acara.required'              => 'Uploaad Foto "Berita Acara" Bang !',
      'flag_ODP_Baru.required'                  => 'Uploaad Foto "ODP Baru" Bang !',
      'flag_ODP_Lama.required'                  => 'Uploaad Foto "ODP Lama" Bang !',
      'flag_Label.required'                     => 'Uploaad Foto "LABEL" Bang !',
      'flag_Sn_Ont_Baru.required'               => 'Uploaad Foto "SN ONT Baru" Bang !',
      'flag_Sn_Ont_Lama.required'               => 'Uploaad Foto "SN ONT Lama" Bang !',
      'flag_Port_Odp_Lama.required'             => 'Uploaad Foto "Port ODP Lama" Bang !',
      'flag_Action_Penurunan_Dropcore.required' => 'Uploaad Foto "Action Penurunan Dropcore" Bang !',
      'flag_Ex_Dropcore.required'               => 'Uploaad Foto "Ex Dropcore" Bang !',
      'flag_Speedtest.required'                 => 'Uploaad Foto "Speedtest" Bang !',
      'flag_Test_Live_TV.required'              => 'Uploaad Foto "Test Live Tv" Bang !'
  ];

    $validator = Validator::make($request->all(), $rules, $messages);
    $a = $request->input('status');

    if($a == 1 || $a == 2 || $a == 4 || $a == 10 || $a == 11 || $a == 25 || $a == 24 || $a == 34 || $a == 42) {
        $validator->sometimes('kordinat_pelanggan', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('kordinat_odp', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('nama_odp', 'required', function ($input) {
            return true;
        });
    };


    if($a <> 6 && $a <> 28 && $a <> 29 && $a <> 5 && $a <> 52 && $a <> 57 && $a <> 58 && $a <> 59 && $a <> 60 ){
      $validator->sometimes('catatan', 'required', function ($input) {
        return true;
      });

      $validator->sometimes('noPelangganAktif', 'required', function ($input) {
        return true;
      });

      $validator->sometimes('noPelangganAktif', 'numeric', function ($input) {
        return true;
      });
    };

    // if ($a==24){
    //     $validator->sometimes('redaman', 'required', function ($input) {
    //       return true;
    //     });
    // };

    if ($a==1 || $a==38 || $a==37){
        $validator->sometimes('flag_Lokasi_Pelanggan', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Redaman_Ont', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Berita_Acara', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_ODP_Baru', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_ODP_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Label', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Sn_Ont_Baru', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Sn_Ont_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Port_Odp_Lama', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Action_Penurunan_Dropcore', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Ex_Dropcore', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Speedtest', 'required', function ($input) {
            return true;
        });

        $validator->sometimes('flag_Test_Live_TV', 'required', function ($input) {
            return true;
        });
    };

    if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
                        ]);
    };

    if ($request->kordinat_pelanggan=="ERROR" || $request->kordinat_pelanggan=="Harap Tunggu..."){
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Kordinat Pelanggan Salah !. Hidupkan GPS Anda..']]);
    };

    $materials = json_decode($request->input('materials'));
    // if ($a==1 || $a==37 || $a==38){
    //     if (count($materials)==''){
    //         return back()->with('alerts',[['type' => 'danger', 'text' => 'Gagal Menyimpan Laporan, Inputkan Material !']]);
    //     }
    // };

    $get_next_step = DB::table('psb_laporan_status')
                      ->select('step_tracker.*')
                      ->leftJoin('step_tracker','psb_laporan_status.step_id','=','step_tracker.step_tracker_id')
                      ->where('psb_laporan_status.laporan_status_id',$request->input('status'))
                      ->first();

    $next_step = DB::table('dispatch_teknisi')
                     ->where('id',$id)
                     ->update([
                       'step_id' => $get_next_step->step_tracker_id
                     ]);

    $ntes = explode(',', $request->input('nte'));
    $exists = DB::select('
      SELECT a.*,b.id_regu, b.Ndem, b.dispatch_by
      FROM psb_laporan a, dispatch_teknisi b
      WHERE a.id_tbl_mj=b.id AND a.id_tbl_mj = ?
    ',[
      $id
    ]);

    $sendReport = 0;
    $scid = NULL;

    if (count($exists)) {
      $data = $exists[0];

      if ($data->dispatch_by==NULL){
          $scid = $data->Ndem;
      }
      else{
        $scid = NULL;
      }

      if($data->status_laporan!=$request->input('status')){
        $sendReport = 1;
      }
      $kpro_status = '';
      if ($request->input('status')==2){
        $kpro_status = "Kendala Teknik";
      } else if ($request->input('status')==10){
        $kpro_status = "Proses PT2/PT3";
      } else if ($request->input('status')==3) {
        $kpro_status = "Kendala Pelanggan";
      }
      //if ($kpro_status<>'')
      //$this->kpro($data->Ndem,$kpro_status,'manja',$request->input('catatan'));
      DB::transaction(function() use($request, $data, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($data->status_laporan!=$request->input('status') && $request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'created_name'    => $auth->nama,
           'status_laporan'  => $request->input('status'),
           'id_regu'         => $data->id_regu,
           'catatan'         => $request->input('catatan'),
           'Ndem'            => $data->Ndem,
           'psb_laporan_id'  => $data->id
         ]);
        DB::table('psb_laporan')
          ->where('id', $data->id)
          ->update([
            'modified_at'         => DB::raw('NOW()'),
            'modified_by'         => $auth->id_karyawan,
            'catatan'             => $request->input('catatan'),
            'status_laporan'      => $request->input('status'),
            'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
            'nama_odp'            => $request->input('nama_odp'),
            'kordinat_odp'        => $request->input('kordinat_odp'),
            'typeont'             => $request->input('type_ont'),
            'snont'               => $request->input('sn_ont'),
            'typestb'             => $request->input('type_stb'),
            'snstb'               => $request->input('sn_stb'),
            'dropcore_label_code' => $request->input('dropcore_label_code'),
            'port_number'         => $request->input('port_number'),
            'redaman'             => $request->input('redaman'),
            'tgl_up'              => $tgl_up,
            'ttId'                => $request->tbTiang,
            'catatanRevoke'       => $request->input('catatanRevoke'),
            'noPelangganAktif'    => $request->input('noPelangganAktif'),
            'scid'                => $scid
          ]);
        $this->incrementStokTeknisi($data->id, $auth->id_karyawan);

        if ($materials){
            $this->insertMaterials($data->id, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);

      });

      if ($a<>52 && $a<>57 && $a<>58 && $a<>59 && $a<>60){
        if($sendReport){
            // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &');
        };
      };

      if ($a == 1) {
        // echo "kirim sms";
        // $get_order = DB::SELECT('
        //   SELECT
        //   *,
        //   a.id as id_dt
        //   FROM
        //     dispatch_teknisi a
        //   LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        //   LEFT JOIN regu c ON a.id_regu = c.id_regu
        //   LEFT JOIN Data_Pelanggan_Starclick d ON a.Ndem = d.orderId
        //   LEFT JOIN group_telegram e ON c.mainsector = e.chat_id
        //   WHERE
        //     a.id = "'.$id.'"
        //     ')[0];

            // if ($get_order->orderId<>NULL && $get_order->sms_active_prov==1):
            // $pesan = "Pelanggan Yth. \n";
            // $pesan .= "Mhn bantuan penilaian kpd Teknisi Indihome Kami. Silahkan klik link berikut \n";
            // $pesan .= "http://tomman.info/rv/".$this->encrypt($get_order->id_dt)." \n";
            // $pesan .= "Jika layanan brmasalah hub 05113360654";


            // masukkan log
            // DB::table('psb_laporan_log')->insert([
            //    'created_at'      => DB::raw('NOW()'),
            //    'created_by'      => $auth->id_karyawan,
            //    'status_laporan'  => $request->input('status'),
            //    'id_regu'         => $get_order->id_regu,
            //    'catatan'         => 'Pesan SMS Terkirim ',
            //    'Ndem'            => $get_order->Ndem,
            //    'psb_laporan_id'  => $data->id
            // ]);

            // kirim sms
            // $psn = str_replace('\n', ' ', $pesan);
            // $dataDikirim = '("'.$get_order->orderKontak.'","'.$psn.'","true","3006")';
            // $sql = 'INSERT INTO outbox (DestinationNumber,TextDecoded,MultiPart,CreatorID) values '.$dataDikirim;
            // DB::connection('mysql2')->insert($sql);
            // endif;
      }

      // exec('cd ..;php artisan kpro '.$disp->Ndem.' Laporan > /dev/null &');
      return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials, $ntes, $scid) {
        $tgl_up = "";
        if($request->input('status')==1){
          $tgl_up = date("Y-m-d");
        }

        $insertId = DB::table('psb_laporan')->insertGetId([
          'created_at'          => DB::raw('NOW()'),
          'modified_at'         => DB::raw('NOW()'),
          'created_by'          => $auth->id_karyawan,
          'id_tbl_mj'           => $id,
          'catatan'             => $request->input('catatan'),
          'status_laporan'      => $request->input('status'),
          'kordinat_pelanggan'  => $request->input('kordinat_pelanggan'),
          'nama_odp'            => $request->input('nama_odp'),
          'kordinat_odp'        => $request->input('kordinat_odp'),
          'typeont'             => $request->input('type_ont'),
          'snont'               => $request->input('sn_ont'),
          'typestb'             => $request->input('type_stb'),
          'snstb'               => $request->input('sn_stb'),
          'dropcore_label_code' => $request->input('dropcore_label_code'),
          'port_number'         => $request->input('port_number'),
          'redaman'             => $request->input('redaman'),
          'tgl_up'              => $tgl_up,
          'ttId'                => $request->tbTiang,
          'catatanRevoke'       => $request->input('catatanRevoke'),
          'noPelangganAktif'    => $request->input('noPelangganAktif'),
          'scid'                => $scid
        ]);
        foreach($ntes as $nte) {
          DB::table('psb_laporan_material')->insert([
            'id' => $insertId.'-'.$nte,
            'psb_laporan_id' => $insertId,
            'id_item' => $nte,
            'qty' => 1,
            'type' =>2
          ]);
          DB::table('nte')
            ->where('id', $nte)
            ->update([
              'position_type'  => 3,
              'position_key'  => $id,
              'status' => 1
            ]);
        }
        //$disp = @$disp[0];
        //print_r($disp);
        if ($materials){
            // $this->insertMaterials($data->id, $materials);
            $this->insertMaterials($insertId, $materials);
        };

        $this->decrementStokTeknisi($auth->id_karyawan, $materials);

        // exec('cd ..;php artisan sendReport '.$id.' > /dev/null &', $out);
      });

      return redirect('/')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan laporan']
      ]);
    }

  }

  public function simpanPda(Request $req){
      $this->validate($req,[
          'scPda' => 'required',
          'scPda' => 'numeric'
      ],[
          'scPda.required'  => 'Jangan Kosong',
          'scPda.numeric'   => 'Hanya Boleh Angka'
      ]);

      // cek wo PDA
      $dataPda = DB::table('dispatch_teknisi')->where('Ndem',$req->scPda)->first();
      if ($dataPda) {
          if ($dataPda->jenis_layanan=='PDA'){
              // cek sudah ada yg pernah meinput atau belum
              $cek = DB::table('dispatch_hd')->where('orderId', $dataPda->Ndem)->first();
              if (count($cek)==0){
                  // simpan data
                  $auth = session('auth');
                  DB::table('dispatch_hd')->insert([
                      'orderId'     => $req->scPda,
                      'workerId'    => $auth->id_user,
                      'workerName'  => $auth->nama,
                      'updated_at'  => DB::RAW('NOW()'),
                      'ket'         => 1
                  ]);

                  return back()->with('alerts',[['type' => 'success', 'text'  => 'SC Berhasil Disimpan']]);
              }
              else{
                  return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> SC Sudah Pernah Diinput']]);
              }
          }
          else{
            return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> Inputan Bukan SC PDA']]);
          }
      }
      else{
          return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal</strong> Sc Tidak Ditemukan']]);
      }
  }

  public function grabIboosterbySc($in,$menu){
    $query = DB::table('Data_Pelanggan_Starclick')->where('orderId',$in)->first();

    if (count($query)>0){
    $url   = DB::table('dispatch_teknisi')->where('Ndem',$query->orderId)->first();
      $result   = $query;
      $internet = $result->internet;

      if ($internet<>''){
          echo $internet;
          $result = @$this->grabIbooster($internet,$in,$menu);
          //dump($result);

          $inet = '';
          if(count($result)<>0){
            $inet = $result[0]['ND'];
          };

          DB::table('ibooster')->where('ND',$inet)->delete();
          DB::table('ibooster')->insert($result);

          // simpan ke psb laporan redaman_iboster
          DB::table('psb_laporan')->where('id_tbl_mj',$url->id)->update([
            'redaman_iboster' => $result[0]['ONU_Rx'],
          ]);

          return redirect('/'.$url->id)->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengukur WO Redaman <strong>'.$result[0]['ONU_Rx'].'<strong>']
            ]);
      }
      else {
        return redirect('/assurance/dispatch/'.$in)->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>Gagal</strong> mengukur Redaman WO']
          ]);
      }
    }
  }

  public function grabIboosterbyNotSc($in,$menu){
    $query = DB::table('psb_myir_wo')->where('sc',$in)->first();

    if (count($query)>0){
    $url   = DB::table('dispatch_teknisi')->where('Ndem',$query->sc)->first();
      $result   = $query;
      $internet = $result->no_internet;

      if ($internet<>''){
          echo $internet;
          $result = @$this->grabIbooster($internet,$in,$menu);
          dump($result);

          $inet = '';
          if(count($result)<>0){
            $inet = $result[0]['ND'];
          };

          DB::table('ibooster')->where('ND',$inet)->delete();
          DB::table('ibooster')->insert($result);

          // simpan ke psb laporan redaman_iboster
          DB::table('psb_laporan')->where('id_tbl_mj',$url->id)->update([
            'redaman_iboster' => $result[0]['ONU_Rx'],
          ]);

          return redirect('/'.$url->id)->with('alerts', [
              ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengukur WO Redaman <strong>'.$result[0]['ONU_Rx'].'<strong>']
            ]);
      }
      else {
        return redirect('/assurance/dispatch/'.$in)->with('alerts', [
            ['type' => 'danger', 'text' => '<strong>Gagal</strong> mengukur Redaman WO']
          ]);
      }
    }
  }

  public function grabIbooster($spd, $orderId, $menu)
  {
    $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.62.165.58/home.php?page=' . $ibooster->page . '',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'nospeedy=' . $spd . '&analis=ANALISA',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $ibooster->cookies . ''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $columns = array(
      1 =>
      'ND', 'IP_Embassy', 'Type', 'Calling_Station_Id', 'IP_NE', 'ADSL_Link_Status', 'Upstream_Line_Rate', 'Upstream_SNR', 'Upstream_Attenuation', 'Upstream_Attainable_Rate', 'Downstream_Line_Rate', 'Downstream_SNR', 'Downstream_Attenuation', 'Downstream_Attainable_Rate', 'ONU_Link_Status', 'ONU_Serial_Number', 'Fiber_Length', 'OLT_Tx', 'OLT_Rx', 'ONU_Tx', 'ONU_Rx', 'Gpon_Onu_Type', 'Gpon_Onu_VersionID', 'Gpon_Traffic_Profile_UP', 'Gpon_Traffic_Profile_Down', 'Framed_IP_Address', 'MAC_Address', 'Last_Seen', 'AcctStartTime', 'AccStopTime', 'AccSesionTime', 'Up', 'Down', 'Status_Koneksi', 'Nas_IP_Address'
    );

    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(1);
    if ($table) {
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      for ($i = 3, $count = $rows->length; $i < $count; $i++) {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j < $jcount; $j++) {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
        }
        $data['order_id'] = $orderId;
        $data['user_created'] = @session('auth')->id_user;
        $data['menu'] = $menu;
        $result[] = $data;
      }
      return ($result);
    } else {
      print_r("Maaf, Ibooster Gangguan!\n");
      return back()->with('alerts', [['type' => 'danger', 'text' => 'Maaf, Ibooster Gangguan!']]);
    }
  }

  public function getWOApi(Request $req){
      if ($req->has('no_wo')){
          $noWo = $req->input('no_wo');
          $data = DB::table('Data_Pelanggan_Starclick')->where('orderId',$noWo)->first();
          if ($data){
              return response()->json([
                  'status'    => 'T',
                  'message'   => 'Data Has Found',
                  'data'      => $data
              ],200);
          }
          else{
              return response()->json([
                  'status'    => 'F',
                  'message'   => 'Data Not Found',
                  'data'      => $data
              ],404);
          }
      }
      else{
        return response()->json([
            'status'  => 'F',
            'message' => 'error no_wo'
        ],401);
      }
  }

  private function getApiBaDigital($wo){
    $postData = http_build_query(
        array(
          'no_wo' => $wo,
          'key'   => 'EpwvzxlvDpDlllllll@ulJl'
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postData
        )
    );

    $context = stream_context_create($opts);
    $result  = file_get_contents('https://api.telkomakses.co.id/API/alista/get_ba_digital_by_no_wo.php', false, $context);

    $hasil = json_decode($result);
    return $hasil;
  }

  private function insertQrCodeApi($data){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://dalapa.id/api/dalapa/port",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => array(
            'odp_location'    => $data['odp_location'],
            'port_number'     => $data['port_number'],
            'latitude'        => $data['latitude'],
            'longitude'       => $data['longitude'],
            'sn_ont'          => $data['sn_ont'],
            'nama_pelanggan'  => $data['nama_pelanggan'],
            'voice_number'    => $data['voice_number'],
            'internet_number' => $data['internet_number'],
            'date_validation' => $data['date_validation'],
            'qr_port_odp'     => $data['qr_port_odp'],
            'qr_dropcore'     => $data['qr_dropcore'],
            'nama_jalan'      => $data['nama_jalan'],
            'contact'         => $data['contact'],
            'type'            => '1',
            'evidence'        => new \CURLFILE('/srv/htdocs/tomman1_psb/public/upload4/evidence/'.$data['id'].'/LABEL.jpg')),
      CURLOPT_HTTPHEADER => array(
        "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if ($err) {
      $pesan = "cURL Error #:" . $err;
    } else {
      $pesan = $response;
    }

    return $pesan;
  }

  public function checktiket(Request $req)
  {
    if(!in_array(session('auth')->id_karyawan, ["16840676","22840105","15899325","16951792","16011012","20981020"]))
    {
      return redirect('/home')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>Sorry :(</strong> Your Cant Access This Page']
      ]);
    }
    if($req->has('id'))
    {
      $id = $req->input('id');

      $get_data = DB::SELECT('
        SELECT
            dps.orderId,
            dps.orderNcli,
            dps.internet,
            dps.noTelp,
            dps.orderName,
            dps.orderStatus,
            dps.kcontact,
            dps.alproname,
            dps.orderAddr,
            dps.jenisPsb,
            dps.sto,
            dt.id as id_dt,
            r.uraian as tim,
            pl.kordinat_pelanggan,
            pl.catatan,
            pl.nama_odp,
            pl.kordinat_odp,
            pl.odp_label_code,
            pl.dropcore_label_code,
            pls.laporan_status
        FROM dispatch_teknisi dt
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        WHERE
            (dps.internet = "'.$id.'" OR
            dps.noTelp = "'.$id.'") AND
            dps.orderStatus IN ("Completed (PS)","COMPLETED")
        ORDER BY dps.orderDate DESC
    ');

      if(count($get_data) == 1)
      {
        $data = $get_data[0];

        PsbModel::saveLogSearchId($id, session('auth')->id_karyawan, session('auth')->nama);

        $get_foto = ['Lokasi', 'ODP', 'LABEL', 'Berita_Acara', 'SN_ONT', 'SN_STB'];

      } else {
         return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>Sorry :(</strong> Data is Not Found '.$id]
        ]);
      }
    } else {
      $data =[];
      $get_foto =[];
    }

    return view('psb.checktiket', compact('data','get_foto'));
  }

  public function alproCustomer(Request $req){
    if($req->has('alpro')){

      $alpro = $req->input('alpro');

      $get_data = DB::SELECT('
      SELECT
      dps.orderName as customer,
      dps.orderId as sc,
      dps.kcontact,
      pl.noPelangganAktif as no_hp,
      dps.orderNotel as no_hp2,
      dps.internet as no_inet,
      dps.ndemPots as no_voice,
      pl.nama_odp as odp,
      dps.orderStatus,
      dps.orderDatePs as tgl_ps
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
      dt.Ndem NOT LIKE "IN%" AND
      pl.nama_odp LIKE "%'.$alpro.'%"
      ORDER BY dps.orderDatePs DESC
      ');

      if(count($get_data)>0){
        $data = $get_data;
      }else{
        $data = [];
      }
    }else{
      $data = [];
    }

    return view('psb.alproCustomer', compact('data'));
  }

  public function multiWorkorder(Request $req)
  {

    if ($req->has('params'))
    {
      $type = $req->input('type');
      $params = str_replace(array("\r\n","\r","\n"),"",$req->input('params'));

      $get_data = PsbModel::multiWorkorder($type,$params);

      if (count($get_data)>0)
      {
        $data = $get_data;
      } else {
        $data = [];
      }

    } else {
      $data = [];
    }

    return view('psb.multiWorkorder', compact('data'));
  }

  public function ba_controller_inbox()
  {
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    $data = PsbModel::ba_controller_inbox($start, $end);

    return view('psb.ba_controller_inbox', compact('start', 'end', 'data'));
  }

  public function ba_controller($id)
  {
    $data = DB::table('utonline_tr6')->leftJoin('ba_controller_tr6', 'utonline_tr6.scId_int', '=', 'ba_controller_tr6.sc_id')->where('witel', 'KALSEL (BANJARMASIN)')->where('utonline_tr6.scId_int', $id)->first();

    if(!empty($data))
    {
      return view('psb.ba_controller', compact('id', 'data'));
    } else {
      print_r("failed, data is not found :(");
    }
  }

  public function ba_controller_save(Request $req)
  {
    $check = DB::table('ba_controller_tr6')->where('sc_id', $req->input('id'))->first();

    if ($check != null)
    {
      DB::table('ba_controller_tr6')->where('sc_id', $req->input('id'))->update([
        'status' => $req->input('status'),
        'alasan' => $req->input('alasan'),
        'request_by' => session('auth')->id_karyawan,
        'request_at' => date('Y-m-d H:i:s')
      ]);
    } else {
      DB::table('ba_controller_tr6')->insert([
        'sc_id' => $req->input('id'),
        'status' => $req->input('status'),
        'alasan' => $req->input('alasan'),
        'request_by' => session('auth')->id_karyawan,
        'request_at' => date('Y-m-d H:i:s')
      ]);
    }

    return redirect('/dispatch/search')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Request Reset BA SC-' . $req->input('id') . ' Terkirim']
    ]);
  }

  public static function nogMaster($req)
  {
    $check = DB::table('nog_master')->where('dt_id', $req->id_dispatch)->first();
    $splitter = DB::table('jenis_splitter')->where('splitter_id', $req->splitter)->first();

    if (count($splitter) > 0)
    {
      $spl = $splitter->splitter;

      if ($spl != '1:8')
      {
        if (count($check) > 0)
        {
          DB::table('nog_master')->where('dt_id', $req->id_dispatch)->update([
            'nama_odp'        => $req->nama_odp,
            'koordinat_odp'   => $req->kordinat_odp,
            'jml_port'        => $spl,
            'id_valins'       => $req->valins_id,
            'tindak_lanjut'   => null,
            'jenis_order'     => $req->jenis_order,
            'updated_by'      => session('auth')->id_karyawan
          ]);

          print_r("Success Update NOG Master!");
        } else {
          DB::table('nog_master')->insert([
            'dt_id'           => $req->id_dispatch,
            'nama_odp'        => $req->nama_odp,
            'koordinat_odp'   => $req->kordinat_odp,
            'jml_port'        => $spl,
            'id_valins'       => $req->valins_id,
            'tindak_lanjut'   => null,
            'jenis_order'     => $req->jenis_order,
            'created_by'      => session('auth')->id_karyawan,
            'created_at'      => date('Y-m-d H:i:s')
          ]);

          print_r("Success Insert NOG Master!");
        }
      }
    }
  }

  public static function updateCollectNTE($periode)
  {
    $data = DB::table('dispatch_teknisi')->where('jenis_order' , 'CABUT_NTE')->where('created_at', 'LIKE', $periode.'%')->orderBy('tgl', 'ASC')->get();

    foreach ($data as $value) {
      print_r("$value->Ndem tanggal $value->tgl (jenis_order)\n");
      PsbModel::updateCollectNTE($value->id);
    }

    $data = DB::table('dispatch_teknisi')->where('jenis_layanan' , 'CABUT_NTE')->where('created_at', 'LIKE', $periode.'%')->orderBy('tgl', 'ASC')->get();

    foreach ($data as $value) {
      print_r("$value->Ndem tanggal $value->tgl (jenis_layanan)\n");
      PsbModel::updateCollectNTE($value->id);
    }

    print_r("\n\nfinis update all!\n");
  }

  public static function update_suvery_ondesk(Request $req)
  {
    $check = DB::table('sc_survey_ondesk')->where('sc_order', $req->input('sc_order'))->first();

    if (count($check) > 0)
    {
      DB::table('sc_survey_ondesk')->where('sc_order', $req->input('sc_order'))->update([
        'status_survey' => $req->input('status_survey'),
        'updated_by' => session('auth')->id_karyawan,
        'updated_name' => session('auth')->nama
      ]);
    } else {
      DB::table('sc_survey_ondesk')->insert([
        'sc_order' => $req->input('sc_order'),
        'status_survey' => $req->input('status_survey'),
        'updated_by' => session('auth')->id_karyawan,
        'updated_name' => session('auth')->nama
      ]);
    }

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Update Status Survey SC-' . $req->input('sc_order') . '']
    ]);
  }

  public static function cutOffRekon()
  {
    $startDate = date('Y-m-d', strtotime("-14 days"));
    $endDate = date('Y-m-d');

    $get_data = PsbModel::cutOffRekon($startDate, $endDate);

    if (count($get_data) > 0)
    {

      DB::table('psb_cutoff_rekon')->whereBetween('tgl_ps', [$startDate, $endDate])->delete();

      $totalPreconn = 0;
      foreach ($get_data as $value)
      {
        $totalPrecon100 = ($value->precon100 * 100);
        $totalPrecon80 = ($value->precon80 * 80);
        $totalPrecon75 = ($value->precon75 * 75);
        $totalPrecon50 = ($value->precon50 * 50);
        $totalPrecon150nonAcc = ($value->precon150 * 150);
        $totalPreconn = $totalPrecon100 + $totalPrecon80 + $totalPrecon75 + $totalPrecon50 + $totalPrecon150nonAcc + $value->dc_roll;

        $inserts[] = [
          'id_dispatch' => $value->id_dispatch,
          'order_id' => $value->order_id,
          'no_voice' => $value->no_voice,
          'no_inet' => $value->no_inet,
          'nama_pelanggan' => $value->nama_pelanggan,
          'odp' => $value->odp ?? $value->odp2,
          'id_layanan' => $value->id_layanan,
          'alias_layanan' => $value->alias_layanan,
          'layanan' => $value->layanan,
          'status_teknisi' => $value->status_teknisi,
          'tgl_wo' => $value->tgl_wo,
          'tgl_ps' => $value->tgl_ps,
          'address' => $value->address,
          'regu_id' => $value->regu_id,
          'regu_name' => $value->regu_name,
          'mitra' => $value->mitra,
          'sto' => $value->sto,
          'sektor' => $value->sektor,
          'material' => $totalPreconn
        ];
      }

      foreach (array_chunk($inserts, 500) as $numb => $insert)
      {
        DB::table('psb_cutoff_rekon')->insert($insert);

        print_r("\nsave page $numb");
      }

      DB::statement('DELETE a1 FROM psb_cutoff_rekon a1, psb_cutoff_rekon a2 WHERE a1.id > a2.id AND a1.order_id = a2.order_id');

      print_r("\ndone save psb_cutoff_rekon\n");
    }
  }

  public function dailySteps()
  {
    $check = DB::table('dailySteps_log')->where('nik_pelapor', session('auth')->id_karyawan)->where('tgl_laporan', date('Y-m-d'))->first();

    return view('psb.dailySteps', compact('check'));
  }

  public function dailyStepsSave(Request $req)
  {
    $check = DB::table('dailySteps_log')->where('tgl_laporan', $req->input('tgl_laporan'))->where('nik_pelapor', $req->input('nik_pelapor'))->first();
    $chatID = '-1001665712751';

    if (count($check) > 0)
    {
      DB::table('dailySteps_log')
      ->where('tgl_laporan', $req->input('tgl_laporan'))
      ->where('nik_pelapor', $req->input('nik_pelapor'))
      ->update([
        'nik_pelapor' => $req->input('nik_pelapor'),
        'nama_pelapor' => $req->input('nama_pelapor'),
        'langkah_pelapor' => $req->input('langkah_pelapor'),
        'tgl_laporan' => $req->input('tgl_laporan')
      ]);
    } else {
      DB::table('dailySteps_log')
      ->insert([
        'nik_pelapor' => $req->input('nik_pelapor'),
        'nama_pelapor' => $req->input('nama_pelapor'),
        'langkah_pelapor' => $req->input('langkah_pelapor'),
        'tgl_laporan' => $req->input('tgl_laporan')
      ]);
    }

    // save file evidence
    if ($req->hasFile('evidence'))
    {
      $name = date('Ymd');
      $path = public_path().'/upload4/dailySteps/'.$req->input('nik_pelapor').'/';
      if (!file_exists($path))
      {
        if (!mkdir($path, 0775, true))
          return 'gagal menyiapkan folder foto dailySteps';
      }
      $file = $req->file('evidence');
      $ext = 'jpg';
      try {
        $moved = $file->move("$path", "$name.$ext");

        $img = new \Imagick($moved->getRealPath());

        $img->scaleImage(100, 150, true);
        $img->writeImage("$path/$name-th.$ext");
      }
      catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan foto dailySteps '.$name;
      }

      $check = DB::table('dailySteps_log')->where('tgl_laporan', date('Y-m-d', strtotime($req->input('tgl_laporan'))))->where('nik_pelapor', $req->input('nik_pelapor'))->first();

      $caption = $req->input('nama_pelapor')." / ".$req->input('nik_pelapor')."\n\nLangkah ".@$check->langkah_pelapor;

      Telegram::sendPhoto([
        'chat_id' => $chatID,
        'caption' => $caption,
        'photo' => public_path().'/upload4/dailySteps/'.session('auth')->id_karyawan.'/'.date('Ymd', strtotime($req->input('tgl_laporan'))).'.jpg'
      ]);
    }

    // send reporting to group
    $get_pelapor = DB::table('dailySteps_log')->where('tgl_laporan', $req->input('tgl_laporan'))->orderBy('langkah_pelapor', 'DESC')->get();
    if (count($get_pelapor) > 0)
    {
      $key = 1;
      $text = "Report Step Up Provisioning (".date('d/m/Y', strtotime($req->input('tgl_laporan'))).")\n\n";

      foreach ($get_pelapor as $value)
      {
        $numb = $key++;
        if ($value->langkah_pelapor >= 3000)
        {
          $status = 'OK';
        } else {
          $status = 'NOK';
        }
        if ($numb == 1)
        {
          $trophy = '🥇';
        } else if ($numb == 2) {
          $trophy = '🥈';
        } else if ($numb == 3) {
          $trophy = '🥉';
        } else {
          $trophy = '';
        }
        $text .= $numb.". ".strtoupper($value->nama_pelapor)." (".str_replace(',', '.', number_format($value->langkah_pelapor))." Langkah - <b>".$status."</b>) ".$trophy."\n";
      }

      Telegram::sendMessage([
        'chat_id' => $chatID,
        'parse_mode' => 'html',
        'text' => $text
      ]);
    }

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menyimpan Laporan']
    ]);
  }

  public function download_ps_ao()
  {
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    $getData = PsbModel::download_ps_ao($start, $end);

    return view('psb.download_ps_ao', compact('start', 'end', 'getData'));
  }

  public function ajx_tiang_all()
  {
    $get_tiang = PsbModel::ajx_tiang_all();

    return response()->json($get_tiang);
  }

  public function ajx_odp_all()
  {
    $get_odp = PsbModel::ajx_odp_all();

    return response()->json($get_odp);
  }

  public function get_rand_coor()
  {
    $odp = Input::get('odp');
    $dc = Input::get('dc');

    $minDc = sprintf("%0.3f",(($dc / 1000 ) - 0.020));
    $maxDc = sprintf("%0.3f",(($dc / 1000 ) + 0.020));

    $data = DB::table('odp_uim_ihld')->where('odp_name', $odp)->select('latitude as lat_odp', 'longitude as lon_odp')->first();

    return view('psb.get_rand_coor', compact('odp', 'data', 'dc', 'minDc', 'maxDc'));
  }

  public function get_deviasi_koor()
  {
    $lat1 = Input::get('lat1');
    $lon1 = Input::get('lon1');

    $lat2 = Input::get('lat2');
    $lon2 = Input::get('lon2');
    return view('psb.get_devias_koor', compact('lat1', 'lon1', 'lat2', 'lon2') );
  }

  public function getDistanceRoute($id)
  {
    $start = Input::get('start');
    $end = Input::get('end');

    $project = DB::table(DB::raw('Data_Pelanggan_Starclick dps'))
    ->select('dt.id as id_dt','dps.orderId','pls.laporan_status','pl.kordinat_pelanggan','pl.koordinat_tiang')
    ->leftJoin(DB::raw('dispatch_teknisi dt'),'dps.orderIdInteger','=','dt.NO_ORDER')
    ->leftJoin(DB::raw('psb_laporan pl'),'dt.id','=','pl.id_tbl_mj')
    ->leftJoin(DB::raw('psb_laporan_status pls'),'pl.status_laporan','=','pls.laporan_status_id')
    ->whereNotNull('dt.Ndem')
    ->where('dps.orderId', $id)
    ->first();

    return view('psb.getDistanceRoute', compact('start', 'end', 'project'));
  }

  public static function find_sc_distance()
  {
    $data = DB::table(DB::raw('Data_Pelanggan_Starclick dps'))
    ->select('dt.id as id_dt','dps.orderId','pls.laporan_status','pl.kordinat_pelanggan','pl.koordinat_tiang')
    ->leftJoin(DB::raw('dispatch_teknisi dt'),'dps.orderIdInteger','=','dt.NO_ORDER')
    ->leftJoin(DB::raw('psb_laporan pl'),'dt.id','=','pl.id_tbl_mj')
    ->leftJoin(DB::raw('psb_laporan_status pls'),'pl.status_laporan','=','pls.laporan_status_id')
    ->whereNotNull('dt.Ndem')
    ->where('dps.jenisPsb','LIKE','AO%')
    ->where('dps.orderStatus','=','COMPLETED')
    ->where('dps.orderDate','LIKE','2023%')
    ->get();

    foreach ($data as $value)
    {
      $raw_koor = $value->koordinat_tiang;
      $fix_koor_a =  json_decode($raw_koor);
      $fix_koor_b =  json_decode($raw_koor);

      if(count($fix_koor_a) > 1)
      {
        unset($fix_koor_b[0]);
        unset($fix_koor_a[count($fix_koor_a) - 1]);


        $fix_koor_b = array_values($fix_koor_b);
        $fd = [];

        $no = 0;
        foreach($fix_koor_a as $k => $v)
        {
          $no++;
          $fd[$k][$no] = $v;
          $fd[$k][++$no] = $fix_koor_b[$k];
        }

        $total = [];
        $fd = array_map(function($x){
          return array_values($x);
        }, $fd);

        foreach($fd as $k => $v)
        {
          $total[] = self::haversineGreatCircleDistance($v[0][0], $v[0][1], $v[1][0], $v[1][1]);
        }

        $total_jarak = (float)number_format(array_sum($total), 2, '.', '');

        DB::table('psb_laporan')->where('id_tbl_mj', $value->id_dt)->update([
          'estimasi_jarak_rute' => $total_jarak
        ]);

        print_r("update $value->orderId dengan jarak $total_jarak\n");
      }
    }
  }

  public static function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
  {
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
  }

  public static function find_sc_latlon()
  {
    $data = DB::table('find_sc_latlon')->where([
        ['selisih', 0],
        ['jarak', '']
      ])
      ->whereNotNull('lat_odp')
      ->orderBy('dc', 'DESC')
      ->get();

    DB::statement('UPDATE find_sc_latlon fsl LEFT JOIN odp_uim_ihld oui ON fsl.datek_odp = oui.odp_name LEFT JOIN odp_ihld oi ON oui.odp_name = oi.location_name SET fsl.datek_odp_uim = CONCAT(oi.location_name, " ", oi.nama), fsl.lat_odp = oui.latitude, fsl.long_odp = oui.longitude');

    $deviasi = 100;

    foreach ($data as $value)
    {
      print_r("$value->order_id $value->datek_odp $value->dc $deviasi\n");

      ini_set('memory_limit', '-1');
      ini_set('max_execution_time', '-1');

      $find = exec("cd /srv/htdocs/puppet_rend;nodejs get_random_coordinate.js $value->datek_odp $value->dc $deviasi");
      $result = json_decode($find);

      DB::table('find_sc_latlon')
      ->where('order_id', $value->order_id)
      ->update([
        'lat_pelanggan'  => $result->lat,
        'long_pelanggan' => $result->lon,
        'jarak'          => $result->jarak,
        'selisih'        => $result->selisih
      ]);

      print_r("update $value->order_id dc $value->dc jarak $result->jarak dengan selisih $result->selisih\n\n");
    }
  }

  public function reset_rute_tarikan($id)
  {
    DB::statement('UPDATE dispatch_teknisi dt LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj SET pl.reset_koor_tiang_by = "'.session('auth')->id_karyawan.'", pl.koordinat_tiang_before = pl.koordinat_tiang, pl.koordinat_tiang = "[]" WHERE dt.id = '.$id);

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Reset Data Rute Tarikan']
    ]);
  }
}
