<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use DB;

class LaporanController extends Controller
{
  protected $photogrid = [
    'Berita_Acara',
    'SN_ONT',
    'ODP',
    'Lokasi',
    'LABEL',
    'KLEM_S',
    'Redaman_ODP','Patchcore','Stopper','Breket','Roset', 'Redaman_ODP', 'Redaman_Pelanggan', 'Instalasi_Kabel',
  ];


  public function photogrid($ndem){
    $photogrid = $this->photogrid;
    $get_id = DB::SELECT('SELECT * FROM dispatch_teknisi WHERE Ndem  = "'.$ndem.'"')[0];
    return view('laporan.photogrid',compact('photogrid','get_id'));
  }

  public function teknisi()
  {
    $list = DB::select('
      SELECT i.id_item, i.nama_item, i.unit_item, COALESCE( m.stok, 0 ) AS qty
      FROM item i
      LEFT JOIN stok_material_teknisi m ON i.id_item = m.id_item
      WHERE i.kat = 9
      ORDER BY qty desc
    ');
    return view('laporan.stok-teknisi', compact('list'));
  }
  public function stokTeknisiDetail($id)
  {
    $list = DB::select('
      SELECT m.id_item, k.nama, m.stok
      FROM stok_material_teknisi m 
      LEFT JOIN 
      karyawan k ON m.id_karyawan = k.id_karyawan
      WHERE m.id_item = ?
      ORDER BY m.stok desc
    ',[
      $id
    ]);
    return view('laporan.stok-teknisi-detail', compact('list', 'id'));
  }
  public function gudang()
  {
    $list = DB::select('
      SELECT i.id_item, i.nama_item, i.unit_item, COALESCE( m.stok, 0 ) AS qty
      FROM item i
      LEFT JOIN stok_material_gudang m ON i.id_item = m.id_item
      WHERE i.kat = 9
      ORDER BY qty desc
    ');
    return view('laporan.stok-gudang', compact('list'));
  } 
  public function material(){
    return view('laporan.material-psb');
  }
  public function gallery(){
    return view('laporan.gallery');
  }
  
  public function materialmigrasi(){
    return view('laporan.material-migrasi');
  }
  
  public function dshr_report1(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $satu = DB::select('
      SELECT id_karyawan AS ns,nama,regu.uraian,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="va" and tanggal_deal = "'.$day.'"
      ) AS VA1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="tiket_created" and tanggal_deal = "'.$day.'"
      ) AS TC1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_deployer" and tanggal_deal = "'.$day.'"
      ) AS unsc_dep1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_nr2c" and tanggal_deal = "'.$day.'"
      ) AS unsc_nr2c1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="" and tanggal_deal = "'.$day.'"
      ) AS ts,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and tanggal_deal = "'.$day.'"
      ) AS ps1,(select VA1) + (select TC1) + (select unsc_dep1) + (select unsc_nr2c1) + (select ps1) + (select ts) as total
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by total desc, nama asc
    ');
    return view('laporan.ajax-dshr1', compact('satu'));
  }
  public function dshr_report2(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $dua = DB::select('
      SELECT uraian AS k,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="va" and tanggal_deal = "'.$day.'"
      ) AS VA1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="tiket_created" and tanggal_deal = "'.$day.'"
      ) AS TC1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="unsc_deployer" and tanggal_deal = "'.$day.'"
      ) AS unsc_dep1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="unsc_nr2c" and tanggal_deal = "'.$day.'"
      ) AS unsc_nr2c1,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="" and tanggal_deal = "'.$day.'"
      ) AS ts,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and tanggal_deal = "'.$day.'"
      ) AS ps1,
      (select TC1) + (select unsc_dep1) + (select unsc_nr2c1) + (select ps1) + (select VA1) + (select ts) as total
      FROM regu
      WHERE job = "DSHR" order by total desc, uraian
    ');
    return view('laporan.ajax-dshr2', compact('dua'));
  }
  public function dshr_report3(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $tiga = DB::select('
      SELECT DISTINCT id_karyawan AS ns,nama,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="va" and tanggal_deal like "%'.$m.'%"
      ) AS VA3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="tiket_created" and tanggal_deal like "%'.$m.'%"
      ) AS TC3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_deployer" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_dep3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_nr2c" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_nr2c3,(
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="Kendala_va" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_va3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="kendala_tc" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_tc3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="" and tanggal_deal like "%'.$m.'%"
      ) AS ts,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and tanggal_deal like "%'.$m.'%"
      ) AS ps3,(select VA3) + (select TC3) + (select unsc_dep3) + (select unsc_nr2c3) + (select ps3) + (select kendala_va3) + (select kendala_tc3) + (select ts) as total
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by total desc, nama asc
    ');
    return view('laporan.ajax-dshr3', compact('tiga'));
  }
  public function dshr_report4(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $empat = DB::select('
      SELECT uraian AS k,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="va" and tanggal_deal like "%'.$m.'%"
      ) AS VA3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="tiket_created" and tanggal_deal like "%'.$m.'%"
      ) AS TC3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="unsc_deployer" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_dep3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="unsc_nr2c" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_nr2c3,(
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="Kendala_va" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_va3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="kendala_tc" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_tc3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="" and tanggal_deal like "%'.$m.'%"
      ) AS ts,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and tanggal_deal like "%'.$m.'%"
      ) AS ps3, (select VA3) + (select TC3) + (select unsc_dep3) + (select unsc_nr2c3) + (select ps3) + (select kendala_va3) + (select kendala_tc3) + (select ts) as total
      FROM regu
      WHERE job = "DSHR" order by total desc, uraian
    ');
    return view('laporan.ajax-dshr4', compact('empat'));
  }
  public function dshr_report5(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $lima = DB::select('
      SELECT DISTINCT id_karyawan AS ns,nama,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="va" and s.created_at like "%'.$m.'%"
      ) AS va5,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="tiket_created" and s.created_at like "%'.$m.'%"
      ) AS tc5,(select va5) + (select tc5) as total
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by total desc, nama asc
    ');
    return view('laporan.ajax-dshr5', compact('lima'));
  }
  public function dshr_report6(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $enam = DB::select('
      SELECT uraian AS k,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="va" and s.created_at like "%'.$m.'%"
      ) AS va5,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="tiket_created" and s.created_at like "%'.$m.'%"
      ) AS tc5,(select va5) + (select tc5) as total
      FROM  regu
      WHERE job = "DSHR" order by total desc, uraian
    ');
    return view('laporan.ajax-dshr6', compact('enam'));
  }
  public function dshr_report7(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $tujuh = DB::select('
      SELECT DISTINCT id_karyawan AS ns,nama,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and jenis_layanan ="0P" and s.created_at like "%'.$m.'%"
      ) AS nolP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and jenis_layanan ="1P" and s.created_at like "%'.$m.'%"
      ) AS satuP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and jenis_layanan ="2P" and s.created_at like "%'.$m.'%"
      ) AS duaP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and jenis_layanan ="3P" and s.created_at like "%'.$m.'%"
      ) AS tigaP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and s.created_at like "%'.$m.'%"
      ) AS total7
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by total7 desc, nama asc
    ');
    return view('laporan.ajax-dshr7', compact('tujuh'));
  }
  public function dshr_report8(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $delapan = DB::select('
      SELECT uraian AS k,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and jenis_layanan ="0P" and s.created_at like "%'.$m.'%"
      ) AS nolP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and jenis_layanan ="1P" and s.created_at like "%'.$m.'%"
      ) AS satuP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and jenis_layanan ="2P" and s.created_at like "%'.$m.'%"
      ) AS duaP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and jenis_layanan ="3P" and s.created_at like "%'.$m.'%"
      ) AS tigaP7,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="ps" and s.created_at like "%'.$m.'%"
      ) AS total7
      FROM  regu
      WHERE job = "DSHR" order by total7 desc, uraian
    ');
    
    return view('laporan.ajax-dshr8', compact('delapan'));
  }
  public function dshr_report9(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $sembilan = DB::select('
      SELECT DISTINCT id_karyawan AS ns,nama,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="batal" and s.created_at like "%'.$m.'%"
      ) AS batal
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by batal desc, nama asc
    ');
    return view('laporan.ajax-dshr9', compact('sembilan'));
  }
  public function dshr_report10(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $sepuluh = DB::select('
      SELECT uraian AS k,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE korlap = k and status ="batal" and s.created_at like "%'.$m.'%"
      ) AS batal
      FROM  regu
      WHERE job = "DSHR" order by batal desc, uraian
    ');
    return view('laporan.ajax-dshr10', compact('sepuluh'));
  }
  public function dshr_report11(){
    $auth = session('auth');
    var_dump($auth);
    //return view('laporan.ajax-dshr11', compact('witel'));
  }
  public function dshr_report12(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    $visual = DB::select('
      SELECT d.id, tanggal_deal, nik_sales, EXTRACT(YEAR_MONTH from tanggal_deal) as bln
      FROM dshr d
      LEFT JOIN dshr_status s ON d.dshr_status_id = s.id
      WHERE tanggal_deal LIKE "%'.date("Y-m").'%"
      ORDER BY nik_sales
    ');
    $tiga = DB::select('
      SELECT DISTINCT id_karyawan AS ns,nama,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="va" and tanggal_deal like "%'.$m.'%"
      ) AS VA3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="tiket_created" and tanggal_deal like "%'.$m.'%"
      ) AS TC3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_deployer" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_dep3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="unsc_nr2c" and tanggal_deal like "%'.$m.'%"
      ) AS unsc_nr2c3,(
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="Kendala_va" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_va3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="kendala_tc" and tanggal_deal like "%'.$m.'%"
      ) AS kendala_tc3,
      (
      SELECT count( d.id )
      FROM dshr d left join dshr_status s
         on d.dshr_status_id = s.id
      WHERE nik_sales = ns and status ="ps" and tanggal_deal like "%'.$m.'%"
      ) AS ps3,(select VA3) + (select TC3) + (select unsc_dep3) + (select unsc_nr2c3) + (select ps3) + (select kendala_va3) + (select kendala_tc3) as total
      FROM karyawan LEFT JOIN regu on karyawan.id_regu = regu.id_regu
      WHERE regu.job="DSHR" order by total desc, nama asc
    ');

    return view('laporan.ajax-dshr12', compact('visual', 'tiga'));
  }

  public function dshr_report(){
    $day = date("Y-m-d");
    $m = date("Y-m");
    
//return $visual;
//return $witel;
    return view('laporan.dshr');
  }
  
  public function mt_tiket(){

    return view('laporan.maintenance');
  }
  public function mt_regu(){
    
    return view('laporan.mt_regu');
  }
  public function mt_rekon(){
    
    return view('laporan.mt_rekon');
  }
  public function dshrPs($id){
    $data = DB::select("
      SELECT m.nik_sales,k.nama, (select count(*) from ms2n where nik_sales = m.nik_sales and Tgl_PS like '%$id%' and Status = 'PS') as ps,
      (select count(*) from ms2n where nik_sales = m.nik_sales and Tgl_PS like '%$id%' and Status = 'VA') as va,
      (select count(*) from dshr where nik_sales = m.nik_sales and tanggal_deal like '%$id%') as tx
      FROM ms2n m left join karyawan k on m.nik_sales = k.id_karyawan
      WHERE Tgl_PS like '%$id%' and Chanel = 'Digital Sales Home Reach' group by nik_sales
    ");
    //var_dump($list);
    /*$karyawan = DB::select("
      SELECT nama,id_karyawan
      FROM karyawan WHERE 1
    ");

    $data = array();
    $lastNik = '';
    foreach($list as $r){
      $key = $this->findNik($data,$r->nik_sales);
      if(is_numeric($key)){
        $data[$key]['ps'][] = $r->Nama;
      }else{
        $data[] = array("nik"=>$r->nik_sales, "nama"=>$r->nama, "ps"=>array($r->Nama));
      }
    }
    usort($data, function($x, $y) {
      $a = count($x['ps']);
      $b = count($y['ps']);

      if ($a > $b) {
        return -1;
      }
      else if ($a < $b) {
        return 1;
      }
      else return 0;
    });*/
    return view('laporan.ajax-dshr', compact('data'));
  }
  private function findNik($data, $nik) {
    foreach($data as $index => $d) {
        if($d['nik'] == $nik) return $index;
    }
    return FALSE;
  }
  private function findNama($data, $nik) {
    foreach($data as $index => $d) {
        if($d->nik == $nik) return $d->nama;
    }
    return FALSE;
  }
  public function outExcel(){
    $head = array(array('REKAPITULASI RAB QE AKSES APRIL 2016 WITEL KALSEL','','','',''),array('STO : ','','','',''),array('','','','',''),array('NO','URAIAN / ITEM','STO','TOTAL HARGA','KETERANGAN'),array('','','','( Rp )',''));
    $data = array(
            array('1', 'data1'),
            array('2', 'data2')
        );
    $data = array_merge($head,$data);
    Excel::create('tes', function($excel) use($data){
    $excel->setTitle('Our new awesome title');
    $excel->setCreator('Maatwebsite')
          ->setCompany('Maatwebsite');
    $excel->setDescription('A demonstration to change the file properties');
    
    $excel->sheet('tesSheet', function($sheet) use($data) {
        $sheet->fromArray($data, null, 'A1', false, false);
        
    });
    })->export('xls');
  }
  public function rankingNoUpdate(){
    $tgl = date('Y-m-d');
    $SQL = '
        SELECT e.status_laporan, a.dispatch_by, g.TL, b.uraian, a.Ndem
            FROM dispatch_teknisi a
            LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            LEFT JOIN regu b ON a.id_regu = b.id_regu
            LEFT JOIN group_telegram g ON b.mainsector = g.chat_id
            WHERE e.status_laporan is null  and TL is not null and
            date(a.updated_at) = "'.$tgl.'" order by g.TL asc , uraian asc
        ';
    $list = DB::select($SQL);
    $dash = "";
    // dd($list);
    $lastTL = '';
    $lastRegu = '';
    foreach ($list as $no => $m){
      if ($m->TL <> ''){
          if($lastTL == $m->TL){
            $dash[count($dash)-1]['jumlah'] += 1;
            if($lastRegu == $m->uraian){
              $dash[count($dash)-1]['tim'][count($dash[count($dash)-1]['tim'])-1]["total"] += 1;
              $dash[count($dash)-1]['tim'][count($dash[count($dash)-1]['tim'])-1]["list"][] = $m->Ndem;
            }else{
              $dash[count($dash)-1]['tim'][] = array("uraian" => $m->uraian,"total" => 1,"list" => array($m->Ndem));
            }
          }else{
            $dash[] = array("TL" => $m->TL, "jumlah" => 1);
            $dash[count($dash)-1]['tim'][] = array("uraian" => $m->uraian,"total" => 1,"list" => array($m->Ndem));
          } 
          $lastTL = $m->TL;
          $lastRegu = $m->uraian;
      }
    }
    // dd($dash);  

    $this->array_sort_by_column($dash, 'jumlah');
    return view('laporan.rankingNoUpdate', compact('dash'));
  }
  function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
  }

  public function matrikMt($jns, $sts, $kandatel){
    if($jns == "none" && $sts == "none" && $kandatel == "none"){
            $matrix = 1;
            $data = DB::select("select m.id, m.status_psb, m.nama_order as jenis_order,
(select count(d.id) from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj where mc.id is null and pl.status_laporan = m.status_psb and pl.isCutOff is null) as xcheck,
(select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.jenis_order = m.id and mt.status = 'close') as close,
(select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.jenis_order = m.id and mt.status = 'close' and mt.tgl_selesai like '%".date('Y-m-d')."%') as closeHi,
(select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.jenis_order = m.id and mt.status = 'kendala teknis') as kendala_teknis,
(select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.jenis_order = m.id and mt.status = 'kendala pelanggan') as kendala_pelanggan,
(select count(mt.id) from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where mt.jenis_order = m.id and mt.status is null and mt.dispatch_regu_id is not null) as no_update,
(select count(mt.id) from maintaince mt where mt.jenis_order = m.id and mt.dispatch_regu_id is null) as undisp
from maintenance_jenis_order m where m.id = 3 or m.id=6");

            // tambahan disini
            DB::statement('truncate table psb_matrik');
            $jenisOrder = array('3','6'); 
            $datel      = array('BANJARMASIN', 'BANJARBARU', 'BATULICIN', 'TANJUNG', 'UNK DATEL', 'LAIN-LAIN');

            $undisp = array();

            // simpan data
            foreach ($datel as $kandatel){
              foreach ($jenisOrder as $order){
                  $sqlUndiSP = 'select mt.kandatel, m.nama_order, count(mt.id) as undisp, m.id from maintaince mt left join maintenance_jenis_order m on mt.jenis_order=m.id where mt.jenis_order = m.id and mt.dispatch_regu_id is null and m.id="'.$order.'"';

                  $sqlNoUpdate = 'select mt.kandatel, m.nama_order, count(mt.id) as no_update, m.id from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id left join maintenance_jenis_order m on m.id = mt.jenis_order where mt.jenis_order = m.id and mt.status is null and mt.dispatch_regu_id is not null and m.id = "'.$order.'"'; 

                  $sqlKendalaPelanggan = 'select mt.kandatel, m.nama_order, count(mt.id) as kendala_pelanggan, m.id from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id left join maintenance_jenis_order m on m.id = mt.jenis_order where mt.jenis_order = m.id and mt.status = "kendala pelanggan" and m.id = "'.$order.'"';

                  $sqlKendalaTeknis = 'select mt.kandatel, m.nama_order, count(mt.id) as kendala_teknis, m.id from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id left join maintenance_jenis_order m on m.id = mt.jenis_order where mt.jenis_order = m.id and mt.status = "kendala teknis" and m.id = "'.$order.'"'; 

                  $sqlCloseHi = 'select mt.kandatel, m.nama_order, count(mt.id) as closeHi, m.id from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id left join maintenance_jenis_order m on m.id = mt.jenis_order where mt.jenis_order = m.id and mt.status = "close" and mt.tgl_selesai like "%'.date("Y-m-d").'%" and m.id = "'.$order.'"';

                  $sqlClose = 'select mt.kandatel, m.nama_order, count(mt.id) as close, m.id from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id left join maintenance_jenis_order m on m.id = mt.jenis_order where mt.jenis_order = m.id and mt.status = "close" and m.id = "'.$order.'"';

                  $sqlXcheck = 'select count(d.id) as xchecx, mc.kandatel, m.nama_order, pl.status_laporan from dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket left join psb_laporan pl on d.id=pl.id_tbl_mj left join maintenance_jenis_order m on pl.status_laporan = m.status_psb where mc.id is null and pl.isCutOff is null and m.id = "'.$order.'"'; 

                  if ($kandatel=='LAIN-LAIN') {
                      $sqlUndiSP   .= ' and mt.kandatel is Null';
                      $sqlNoUpdate .= ' and mt.kandatel is Null';
                      $sqlKendalaPelanggan .= ' and mt.kandatel is Null';
                      $sqlKendalaTeknis    .= ' and mt.kandatel is Null';
                      $sqlCloseHi          .= ' and mt.kandatel is Null';
                      $sqlClose            .= ' and mt.kandatel is Null';
                      $sqlXcheck           .= ' and mc.kandatel is Null';
                  }
                  else if ( $kandatel=='TANJUNG' ){
                      $sqlUndiSP   .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlNoUpdate .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlKendalaPelanggan .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlKendalaTeknis    .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlCloseHi          .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlClose            .= ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                      $sqlXcheck           .= ' and mc.kandatel like '.'"%'.$kandatel.'%"';
                  }                 
                  else {
                      $sqlUndiSP   .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlNoUpdate .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlKendalaPelanggan .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlKendalaTeknis    .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlCloseHi          .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlClose            .= ' and mt.kandatel="'.$kandatel.'"';
                      $sqlXcheck           .= ' and mc.kandatel="'.$kandatel.'"';                      
                  };
                
                  $dataUndisp   = DB::select($sqlUndiSP);
                  $dataNoupdate = DB::select($sqlNoUpdate);
                  $dataKendalaPelanggan = DB::select($sqlKendalaPelanggan);
                  $dataKendalaTeknis    = DB::select($sqlKendalaTeknis);
                  $dataCloseHi          = DB::select($sqlCloseHi);
                  $dataClose            = DB::select($sqlClose);
                  $dataXcheck           = DB::select($sqlXcheck);

                  foreach ($dataUndisp as $dataUndispSimpan){
                      $nilaiUndisp = $dataUndispSimpan->undisp;
                  };

                  foreach ($dataNoupdate as $dataNoupdateSimpan){
                      $nilaiNoUpdate = $dataNoupdateSimpan->no_update;
                  }

                  foreach ($dataKendalaPelanggan as $dataKendalaPelangganSimpan){
                      $nilaiKendalaPelanggan = $dataKendalaPelangganSimpan->kendala_pelanggan;
                  }

                  foreach ($dataKendalaTeknis as $dataKendalaTeknisSimpan){
                      $nilaiKendalaTeknis = $dataKendalaTeknisSimpan->kendala_teknis;
                  }

                  foreach ($dataCloseHi as $dataCloseHiSimpan){
                      $nilaiCloseHi = $dataCloseHiSimpan->closeHi;
                  }


                  foreach ($dataClose as $dataCloseSimpan){
                      $nilaiClose     = $dataCloseSimpan->close;
                  }

                  foreach ($dataXcheck as $dataXcheckSimpan){
                      $nilaiXcheck = $dataXcheckSimpan->xchecx;
                  }

                  $total = $nilaiUndisp + $nilaiNoUpdate + $nilaiKendalaPelanggan + $nilaiKendalaTeknis + $nilaiCloseHi + $nilaiClose + $nilaiXcheck;

                  foreach($dataUndisp as $dataUndispSimpan){
                    $dataStatus = DB::table('maintenance_jenis_order')
                                    ->where('nama_order','=',$dataUndispSimpan->nama_order)
                                    ->first();

                    $status_laporan = $dataStatus->id;
                    $status_psb     = $dataStatus->status_psb;

                    DB::table('psb_matrik')->insert([
                       'jns_order'          => $dataUndispSimpan->nama_order,
                       'datel'              => $kandatel,
                       'no_dispatch'        => $nilaiUndisp,
                       'no_update'          => $nilaiNoUpdate,
                       'kendala_pelanggan'  => $nilaiKendalaPelanggan,
                       'kendala_teknis'     => $nilaiKendalaTeknis,
                       'close_hi'           => $nilaiCloseHi,
                       'close'              => $nilaiClose,
                       'xcheck'             => $nilaiXcheck,
                       'total'              => $total,
                       'status_laporan'     => $status_laporan,
                       'status_psb'         => $status_psb
                    ]);
                  }
              }
            }

            $dataOrder  = array('ODP LOSS', 'INSERT TIANG');
            $dataMatrik = DB::select('select * from psb_matrik order by jns_order desc, datel asc');
        }else{
            $matrix = 0;
            $sql = "";
            if ($kandatel=='TANJUNG'){
                $datelXcheck  = ' and mc.kandatel like '.'"%'.$kandatel.'%"';
                $datelCloseHi = ' and mt.kandatel like '.'"%'.$kandatel.'%"';
                $datelUndisp  = ' and mt.kandatel like '.'"%'.$kandatel.'%"';
            }
            else if ($kandatel=='LAIN-LAIN'){
                $datelXcheck  = ' and mc.kandatel is Null';
                $datelCloseHi = ' and mt.kandatel is Null';
                $datelUndisp  = ' and mt.kandatel is Null';
            }
            else {
                $datelXcheck  = ' and mc.kandatel="'.$kandatel.'"';
                $datelClosehi = ' and mt.kandatel="'.$kandatel.'"';
                $datelUndisp  = ' and mt.kandatel="'.$kandatel.'"';
            };
            if($sts == "xcheck"){
                if(($jns != 'all') and ($kandatel != 'all'))
                  $sql = " and pl.status_laporan = '".$jns."'".$datelXcheck;
                else
                  $sql = " and (pl.status_laporan = '2' or pl.status_laporan = '11')";

                $data = DB::select("select pls.laporan_status,pl.nama_odp, pl.kordinat_odp, pl.kordinat_pelanggan, d.id as id_dispatch, d.Ndem as no_tiket,
                 pl.kordinat_odp as nama_order, pl.catatan as headline, pl.created_by as dispatch_regu_name, d.id_regu, r.uraian, gt.TL from 
                 dispatch_teknisi d left join maintaince mc on d.Ndem = mc.no_tiket 
                 left join psb_laporan pl on d.id=pl.id_tbl_mj
                 left join psb_laporan_status pls on pl.status_laporan=pls.laporan_status_id 
                 left join regu r ON d.id_regu = r.id_regu
                 left join group_telegram gt ON r.mainsector = gt.chat_id
                 where pl.isCutOff is null and mc.id is null ".$sql);
            }else if($sts == "closeHi"){
              if ($kandatel != 'all'){
                  $sqlLagi = " and mt.kandatel = '".$kandatel."' and mt.jenis_order = '".$jns."'";
              }
              else {
                  $sqlLagi = " and (mt.jenis_order = '3' or mt.jenis_order = '6')";
              }

              $data = DB::select("select *,mt.action as aksi, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp, dt.id_regu, r.uraian, gt.TL from maintaince mt 
                left join maintaince_progres mp on mt.id = mp.maintaince_id 
                left join psb_laporan pl on mt.psb_laporan_id = pl.id
                left join dispatch_teknisi dt ON dt.Ndem=mt.no_tiket
                left join regu r ON dt.id_regu = r.id_regu
                left join group_telegram gt ON r.mainsector = gt.chat_id
                where mt.status = 'close' and mt.tgl_selesai like '%".date('Y-m-d')."%'".$sqlLagi);
              //echo("select * from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where ".$sql." ".$jenis_order);
            }
            else{
              $jenis_order = "";
              $sql = " mt.status = '".$sts."'";
              if( $jns != 'all') 
                $jenis_order = " and mt.jenis_order = '".$jns."'".$datelUndisp;
              else
                $jenis_order = " and (mt.jenis_order = '3' or mt.jenis_order = '6')";
              if($sts == 'null')
                $sql = " mt.status is ".$sts." and mt.dispatch_regu_id is not null";
              else if($sts == 'undisp')
                $sql = " mt.dispatch_regu_id is null";
              else if($sts == 'all')
                $sql = " 1 ";
            
              $data = DB::select("select *,mt.action as aksi, mt.id as id_mt, pl.nama_odp,mt.nama_odp as nama_dp, dt.id_regu, r.uraian, gt.TL from maintaince mt 
                left join maintaince_progres mp on mt.id = mp.maintaince_id 
                left join psb_laporan pl on mt.psb_laporan_id = pl.id
                left join dispatch_teknisi dt ON dt.Ndem=mt.no_tiket
                left join regu r ON dt.id_regu = r.id_regu
                left join group_telegram gt ON r.mainsector = gt.chat_id
                where ".$sql." ".$jenis_order);
              //echo("select * from maintaince mt left join maintaince_progres mp on mt.id = mp.maintaince_id where ".$sql." ".$jenis_order);
            }
        }
        $sto = DB::select("select sto as id, sto as text from maintenance_datel where 1");  
        
        return view('laporan.matrikMt', compact('data', 'matrix', 'sto', 'dataMatrik', 'dataOrder'));
  }
  public function checkedOrder($id){
    $auth = session('auth');
    $pl = DB::table('psb_laporan')
      ->select('psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem')
      ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
      ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
      ->where('id_tbl_mj', $id)->first();
    //dd($pl);
    if($pl->checked_by)
      return "<span class='label label-warning'>Sudah Di Cek Sanak</span>";
    else{
      if(substr($pl->Ndem,0,2) == "IN"){
        $order_from = "ASSURANCE";
        $asr = DB::table('nonatero_excel_bank')->where('TROUBLE_NO', $pl->Ndem)->first();
        $sto = $asr->STO;
        $kandatel = $asr->KANDATEL;
      }else{
        $psb = DB::table('Data_Pelanggan_Starclick')
          ->select('maintenance_datel.sto', 'maintenance_datel.datel')
          ->leftJoin('maintenance_datel', 'Data_Pelanggan_Starclick.sto', '=', 'maintenance_datel.sto')
          ->where('Data_Pelanggan_Starclick.orderId', $pl->Ndem)->first();
        $sto = $psb->sto;
        $kandatel = $psb->datel;
        $order_from = "PSB";
      }

      DB::connection('mysqlt2')->table('maintaince')->insert([
                "no_tiket"          =>$pl->Ndem,
                "jenis_order"       =>$pl->jenis_order,
                "nama_order"        =>$pl->nama_order,
                "headline"          =>$pl->catatan,
                "pic"               =>$auth->id_karyawan."/".$auth->nama,
                "koordinat"         =>$pl->kordinat_odp,
                "created_at"        =>DB::raw('now()'),
                "created_by"        =>$auth->id_karyawan,
                "psb_laporan_id"    =>$pl->id,
                "order_from"        =>$order_from,
                "kandatel"          =>$kandatel,
                "sto"               =>$sto,
                "nama_odp"          =>$req->$req->odp
      ]);
      DB::table('psb_laporan')->where('id_tbl_mj', $id)->update([
        'checked_by' => $auth->id_karyawan,
        'checked_at' => DB::raw('now()')
      ]);
      exec('cd ..;php artisan sendMt '.$id.' > /dev/null &');
      return "<span class='label label-success'>Sukses</span>";
    }
  }
  public function nextLoker($jns, $sts, Request $req, $kandatel){
    DB::table('psb_laporan')->where('id', $req->id_pl)->update([
      "catatan"             => $req->catatan,
      "nama_odp"            => $req->odp,
      "kordinat_odp"        => $req->koordinat,
      "kordinat_pelanggan"  => $req->koordinat_pelanggan
      ]);
    $auth = session('auth');
    $pl = DB::table('psb_laporan')
      ->select('psb_laporan.id','psb_laporan.checked_by','psb_laporan.kordinat_odp','psb_laporan.catatan', 'maintenance_jenis_order.nama_order', 'maintenance_jenis_order.id as jenis_order', 'dispatch_teknisi.Ndem', 'dispatch_teknisi.dispatch_by')
      ->leftJoin('dispatch_teknisi', 'psb_laporan.id_tbl_mj', '=', 'dispatch_teknisi.id')
      ->leftJoin('maintenance_jenis_order', 'maintenance_jenis_order.status_psb', '=', 'psb_laporan.status_laporan')
      ->where('id_tbl_mj', $req->id_tbl_mj)->first();
    

    // dd($pl->checked_by);
    // if($pl->checked_by){
    //   return redirect()->back()->with('alerts', [
    //       ['type' => 'warning', 'text' => 'Sudah Di Cek Sanak']
    //     ]);
    // }else{
      // dd($pl);
      if(substr($pl->Ndem,0,2) == "IN"){
        $order_from = "ASSURANCE";
        $asr = DB::table('nonatero_excel_bank')->where('TROUBLE_NO', $pl->Ndem)->first();
        $sto = @$asr->STO;
        $kandatel = @$asr->KANDATEL;
      }else{
        if ($pl->dispatch_by=='5'){
            $getDatel = DB::table('maintenance_datel')->where('sto',$req->sto)->first();

            $sto = $req->sto;
            $kandatel = $getDatel->datel;
            $order_from = "PSB";
        }
        else{
            $psb = DB::table('Data_Pelanggan_Starclick')
              ->select('maintenance_datel.sto', 'maintenance_datel.datel')
              ->leftJoin('maintenance_datel', 'Data_Pelanggan_Starclick.sto', '=', 'maintenance_datel.sto')
              ->where('Data_Pelanggan_Starclick.orderId', $pl->Ndem)->first();

            $migrasi = DB::table('dossier_master')
              ->select('maintenance_datel.sto', 'maintenance_datel.datel')
              ->leftJoin('maintenance_datel', 'dossier_master.STO', '=', 'maintenance_datel.sto')
              ->where('dossier_master.ND', $pl->Ndem)
              ->orWhere('dossier_master.ND_REFERENCE', $pl->Ndem)
              ->first();
            if(empty($psb)){
              $sto = $migrasi->sto;
              $kandatel = $migrasi->datel;
              $order_from = "MIGRASI";
            }else{
              $sto = $psb->sto;
              $kandatel = $psb->datel;
              $order_from = "PSB";
            }
        }
      }
     
      DB::table('maintaince')->insert([
                "no_tiket"          =>$pl->Ndem,
                "jenis_order"       =>$pl->jenis_order,
                "nama_order"        =>$pl->nama_order,
                "headline"          =>$pl->catatan,
                "pic"               =>$auth->id_karyawan."/".$auth->nama,
                "koordinat"         =>$pl->kordinat_odp,
                "created_at"        =>DB::raw('now()'),
                "created_by"        =>$auth->id_karyawan,
                "psb_laporan_id"    =>$pl->id,
                "order_from"        =>$order_from,
                "kandatel"          =>$kandatel,
                "sto"               =>$req->sto,
                "nama_odp"          =>$req->odp
      ]);
      DB::table('psb_laporan')->where('id_tbl_mj', $req->id_tbl_mj)->update([
        'checked_by' => $auth->id_karyawan,
        'checked_at' => DB::raw('now()')
      ]);
      exec('cd ..;php artisan sendMt '.$req->id_tbl_mj.' > /dev/null &');
      return redirect()->back()->with('alerts', [
          ['type' => 'success', 'text' => 'Berhasil Hore!!!']
        ]);
    // }
  }

  public function getJsonMaint($id){
       return json_encode(DB::table('psb_laporan')
          ->where('id_tbl_mj', $id)->first());
  }
}
