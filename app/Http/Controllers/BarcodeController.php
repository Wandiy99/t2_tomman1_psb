<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use File;
use DateTime;
use App\DA\PsbModel;
use App\DA\GrabModel;

class BarcodeController extends Controller
{
  public function index()
  {
    echo "INDEX";
  }
  public function search(){
    $result = "";
    $checkONGOING = PsbModel::checkONGOING();
    return view('barcode.search',compact('checkONGOING','result'));
  }

  public function searchpost(Request $req){
    $input = $req->input('barcode');
    $result = GrabModel::barcode($input);
    $checkONGOING = PsbModel::checkONGOING();
    return view('barcode.search',compact('checkONGOING','result','input'));
  }
}
