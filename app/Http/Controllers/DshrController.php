<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Telegram;
use DB;
use App\DA\DshrModel;

class DshrController extends Controller
{
  protected $photoInputs = [
    'Form_Pelanggan', 'KTP','Foto_Rumah_Pelanggan','Foto_ODP', 'Rute'
  ];

  public function download_myir($tgl){
		$query = DshrModel::download_myir($tgl);
		return view('dshr.download_myir',compact('query','tgl'));
	}

  public function belum_dispatch($getSto,$tglAwal,$tglAkhirr){
      $nik     = session('auth')->id_user;
      $tgl = date('Y-m');
      $tglAkhir = date('Y-m-d',strtotime("+1 days"));
      $getData = DshrModel::getAllSalesPlasaBelumDispatch('ALL', $getSto,$tglAwal, $tglAkhir, $nik);

      // $getData = DshrModel::getAllSalesPlasaBelumDispatch_qc1($getSto,'ALL', $tglAwal,$tglAkhir, $nik);
      date_default_timezone_set('Asia/Makassar');




      $active = "dispatch";
      $tgl = $tglAkhir;
      return view('dshr.plasasales.list',compact('active','getData', 'tgl','tglAwal','tglAkhirr'));
  }

  public function belum_dispatch_plasa($getSto,$tglAwal,$tglAkhirr){
    date_default_timezone_set('Asia/Makassar');
    $nik     = session('auth')->id_user;
    $tgl = date('Y-m');
    $tglAkhir = date('Y-m-d',strtotime("+1 days"));
    $getData = DshrModel::getAllSalesPlasaBelumDispatchPlasa('ALL', $getSto,$tglAwal, $tglAkhir, $nik);
    $active = "undispatch";
    $tgl = $tglAkhir;
    return view('dshr.plasasales.list2',compact('active','getData', 'tgl','tglAwal','tglAkhirr'));
}

  public function list_decline($getSto,$tglAwal,$tglAkhir){
      date_default_timezone_set('Asia/Makassar');
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaListDecline('ALL', $getSto,$tglAwal, $tglAkhir, $nik);
      $active = "list_decline";
      return view('dshr.plasasales.listDecline',compact('active','getData','tglAwal','tglAkhir'));
  }

  public function list_non_onecall($getSto,$tglAwal,$tglAkhirr){
    $nik     = session('auth')->id_user;
    $tgl = date('Y-m');
    $getData = DshrModel::getAllSalesPlasaBelumDispatch_non_onecall('ALL', $getSto,$tglAwal, $tglAkhirr, $nik);
    date_default_timezone_set('Asia/Makassar');
    $active = "non";
    return view('dshr.plasasales.listnon',compact('active','getData','tgl','tglAwal','tglAkhirr'));
}

  public function qc1_list($getSto){
      $nik     = session('auth')->id_user;
      $tgl = date('Y-m');
      $tglAwal = date('Y-m-d',strtotime("-7 days"));
      $tglAkhir = date('Y-m-d',strtotime("+1 days"));

      $getData = DshrModel::getAllSalesPlasaBelumDispatch_qc1($getSto,'ALL', $tglAwal,$tglAkhir, $nik);
      date_default_timezone_set('Asia/Makassar');


      if (session('witel')=="KALSEL"){
        $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = @file_get_contents($link);
      } else {
        $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = @file_get_contents($link);
      }
      $get  = json_decode($file);
       if (count($get)){
      foreach ($get as $g){
        $myir = explode('-', $g->myir);
        if (count($myir)>1){
            $myir = $myir[1];
        }
        else{
            $myir = $myir[0];
        };
        $getMyir = DB::SELECT('select * from psb_myir_wo where myir = "'.$myir.'" AND (status_approval = 1 OR sc <> "")');
        $realSTO = explode("-", $g->odp);
        if (count($realSTO)>0){
          $sto = @$realSTO[1];
        } else {
          $sto = "";
        }
        if(count($getMyir)==0){
          if ($getSto <>"ALL"){
            if ($sto == $getSto){
            $data = [
              "orderDate"       => $g->timestamp,
              "created_by"      => $g->kode_sales,
              "myir"            => $myir,
              "customer"        => $g->nama_pelanggan,
              "namaOdp"         => $g->odp,
              "sc"              => null,
              "ket"             => "0",
              "lokerJns"        => "myir",
              "sto"             => $sto,
              "picPelanggan"    => $g->no_hp,
              "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
              "picSales"        => null,
              "alamatLengkap"   => $g->alamat,
              "kelurahan"       => NULL,
              "channel"         => NULL,
              "layanan"         => $g->layanan,
              "sales_id"        => $g->kode_sales,
              "email"           => $g->email,
              "psb"             => null,
              "dispatch"        => 0,
              "ketMyirGangguan" => 0,
              "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
              "nama_tagihan"    => NULL,
              "tgl_lahir"       => NULL,
              "no_ktp"          => NULL,
              "terbit_ktp"      => NULL,
              "masa_ktp"        => NULL,
              "nama_ibu"        => NULL,
              "npwp"            => NULL,
              "paket_harga_id"  => NULL,
              "paket_sales_id"  => NULL,
              "ket_input"       => 2,
              "no_internet"     => NULL,
              "no_telp"         => NULL,
              "kcontack"        => NULL,
              "id_wo"           => NULL,
              "paket_harga"     => NULL,
              "paket_sales"     => NULL,
              "approve_date"    => NULL,
              "ORDER_DATE"      => NULL,
              "decline_by"      => NULL,
              "decline_date"    => NULL
          ];
          array_push($getData, (object)$data);

            }
          } else {
            $data = [
              "orderDate"       => $g->timestamp,
              "created_by"      => $g->kode_sales,
              "myir"            => $myir,
              "customer"        => $g->nama_pelanggan,
              "namaOdp"         => $g->odp,
              "sc"              => null,
              "ket"             => "0",
              "lokerJns"        => "myir",
              "sto"             => $sto,
              "picPelanggan"    => $g->no_hp,
              "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
              "picSales"        => null,
              "alamatLengkap"   => $g->no_hp,
              "kelurahan"       => NULL,
              "channel"         => NULL,
              "layanan"         => $g->layanan,
              "sales_id"        => $g->kode_sales,
              "email"           => $g->email,
              "psb"             => null,
              "dispatch"        => 0,
              "ketMyirGangguan" => 0,
              "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
              "nama_tagihan"    => NULL,
              "tgl_lahir"       => NULL,
              "no_ktp"          => NULL,
              "terbit_ktp"      => NULL,
              "masa_ktp"        => NULL,
              "nama_ibu"        => NULL,
              "npwp"            => NULL,
              "paket_harga_id"  => NULL,
              "paket_sales_id"  => NULL,
              "ket_input"       => 2,
              "no_internet"     => NULL,
              "no_telp"         => NULL,
              "kcontack"        => NULL,
              "id_wo"           => NULL,
              "paket_harga"     => NULL,
              "paket_sales"     => NULL,
              "approve_date"    => NULL,
              "ORDER_DATE"      => NULL,
              "decline_by"      => NULL,
              "decline_date"    => NULL
          ];
          array_push($getData, (object)$data);
          }


        }
        }
      }

      $active = "validasi";
      $tgl = $tglAkhir;
      return view('dshr.plasasales.listQC1',compact('active','getData', 'tgl'));
  }

  public function send_to_telegram($id){
	  $auth = session('auth');
		$list = DB::select('
		  SELECT a.uraian,d.*, b.nama as sales,b.sub_directorat
		  FROM dshr d
		  left join 1_2_employee b on d.nik_sales = b.nik
		  left join regu a on b.id_regu = a.id_regu
		  WHERE id = ?
		  order by d.id desc
		',[ $id ])[0];

		$s = $list->nama_pelanggan;
		$messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n";
		$messageio .= 'Tanggal Transaksi: '.$list->tanggal_deal."\n\n";
		$messageio .= 'Nama Sales : '.$list->sales."\n";
		$messageio .= 'NIK Sales : '.$list->nik_sales."\n";
		$messageio .= 'Tipe Transaksi : '.$list->tipe_transaksi."\n";
		$messageio .= '👤 : '.$list->nama_pelanggan."\n";
		$messageio .= '✉️ : '.$list->email."\n";
		$messageio .= '🏠 : '.$list->alamat."\n";
		$messageio .= '📱 : '.$list->pic."\n";
		$messageio .= '☎️ : '.$list->no_telp."\n\n";
		$messageio .= 'MYIR : '.$list->no_ktp."\n";
		$messageio .= 'Kordinat Pelanggan : '.$list->koor_pel."\n";
		$messageio .= 'Nama ODP : '.$list->nama_odp."\n";
		$messageio .= 'Kordinat ODP : '.$list->koor_odp."\n\n";
    if ($list->sub_directorat=="Avengers"){
	    $chatID  = "-1001227198531";
      $chatID2 = "-1001248567976";
	  } else {
      $chatID  = "-171167699";
      $chatID2 = "-1001248567976";
    }

	 Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messageio
    ]);

   // tes ja
  Telegram::sendMessage([
      'chat_id' => $chatID2,
      'text' => $messageio
    ]);

    $photoInputs = $this->photoInputs;
		for($i=0;$i<count($photoInputs);$i++) {
      $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
    	$file = public_path()."/".$upload."/dshr/".$id."/".$photoInputs[$i].".jpg";
    	if (file_exists($file)){
		    Telegram::sendPhoto([
		      'chat_id' => $chatID,
		      'caption' => $photoInputs[$i]." ".$s,
		      'photo' => $file
		    ]);

        // tes ja
        Telegram::sendPhoto([
          'chat_id' => $chatID2,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);

		  }
    }
    return redirect('/dshr-transaksi')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
    ]);
  }

  public function send_to_telegramSales($id, $foto, $ket){
    $auth = session('auth');
    $list = DB::select('
      SELECT *, m.area_migrasi, d.sto
      FROM psb_myir_wo d
      left join 1_2_employee b on d.sales_id = b.nik
      left join psb_paket_harga_baru pkhb on d.paket_harga_id = pkhb.id
      left join mdf m on d.sto=m.mdf
      left join maintenance_datel md ON m.mdf = md.sto
      WHERE d.id = ?
      order by d.id desc
    ',[ $id ])[0];

    $s = $list->customer;
    $messageio = "Pengirim : ".$auth->id_user."-".$auth->nama."\n";
    $messageio .= 'Tanggal Transaksi: '.$list->orderDate."\n\n";

    $datel = $list->datel;
    // if ($list->area_migrasi=='INNER'){
    //     $datel = "Banjarmasin";
    // }
    // elseif($list->area_migrasi=="BBR"){
    //     $datel = "Banjarbaru";
    // }
    // elseif($list->area_migrasi=="KDG"){
    //     $datel = "Kandangan";
    // }
    // elseif($list->area_migrasi=="TJL"){
    //     $datel = "Tanjung";
    // }
    // elseif($list->area_migrasi=="BLC"){
    //     $datel = "Batulicin";
    // };

    $messageio .= 'Datel : '.$datel."\n";
    if($ket==1){
        $messageio .= 'Nama Sales : '.$list->nama."\n";
        $messageio .= 'NIK Sales : '.$list->nik."\n";
        $messageio .= 'Tipe Transaksi : '.$list->layanan."\n";
        $messageio .= 'Source PSB : '.$list->psb."\n";
    }
    else if ($ket==0){
        $messageio .= 'Tipe Transaksi : '.$list->layanan."\n";
        $messageio .= 'Paket Harga : '.$list->paket_harga."\n";
    }

    $messageio .= '👤 : '.$list->customer."\n";
    $messageio .= '✉️ : '.$list->email."\n";
    $messageio .= '🏠 : '.$list->alamatLengkap."\n";
    $messageio .= '📱 : '.$list->picPelanggan."\n";
    $messageio .= '☎️ : '.$list->picPelanggan."\n\n";

    if ($ket==1){
        $messageio .= 'MYIR : '.$list->myir."\n";
    }
    else if ($ket==0){
        $messageio .= 'No. Order : '.$list->myir."\n";
    };

    $messageio .= 'Nama Tagihan : '.$list->nama_tagihan ."\n";
    $messageio .= 'Kordinat Pelanggan : '.$list->koorPelanggan."\n";
    $messageio .= 'Nama ODP : '.$list->namaOdp."\n";
    $messageio .= 'Kordinat ODP : '.$list->koorOdp."\n";
    $messageio .= 'No. Internet : '.$list->no_internet ."\n";
    $messageio .= 'K-kontak : '.$list->kcontack ."\n\n";

   $chatID = $list->grup_sales;
   // $chatID = '192915232';

   Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messageio
    ]);

    $photoInputs = $foto;
    for($i=0;$i<count($photoInputs);$i++) {
      $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
      $file = public_path()."/".$upload."/plasaSales/".$id."/".$photoInputs[$i].".jpg";
      if (file_exists($file)){
        Telegram::sendPhoto([
          'chat_id' => $chatID,
          'caption' => $photoInputs[$i]." ".$s,
          'photo' => $file
        ]);
      }
    }

    if ($list->area_migrasi=="KDG" || $list->area_migrasi=="TJL"){
        Telegram::sendMessage([
          // 'chat_id'   => '192915232',
          'chat_id' => '-223486016',
          'text' => $messageio
        ]);

        $photoInputs = $foto;
        for($i=0;$i<count($photoInputs);$i++) {
          $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
          $file = public_path()."/".$upload."/plasaSales/".$id."/".$photoInputs[$i].".jpg";
          if (file_exists($file)){
            Telegram::sendPhoto([
              'chat_id' => '-223486016',
              'caption' => $photoInputs[$i]." ".$s,
              'photo' => $file
            ]);
          }
        }
    }

    if($list->ket_input==0){
        if ($list->area_migrasi=="BBR"){
            Telegram::sendMessage([
              // 'chat_id'   => '192915232',
              'chat_id' => '-259870205',
              'text' => $messageio
            ]);

            $photoInputs = $foto;
            for($i=0;$i<count($photoInputs);$i++) {
              $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
              $file = public_path()."/".$upload."/plasaSales/".$id."/".$photoInputs[$i].".jpg";
              if (file_exists($file)){
                Telegram::sendPhoto([
                  'chat_id' => '-223486016',
                  'caption' => $photoInputs[$i]." ".$s,
                  'photo' => $file
                ]);
              }
            }
        }
    }

    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim laporan']
    ]);
  }

  public function index()
  {
    return view('dshr.transaksi.list');
  }
  public function indexc()
  {
    $count = DB::select('
    select
    	SUM(CASE WHEN (b.flagging LIKE "%BUMI MAS%") THEN 1 ELSE 0 END) as BUMI_MAS,
    	SUM(CASE WHEN (b.flagging LIKE "%SUTOYO%") THEN 1 ELSE 0 END) as SUTOYO
    from
    dshr b
    LEFT JOIN dshr_cluster_pelanggan a ON b.no_telp = a.nd
    ');
    $data = $count[0];
    return view('dshr.transaksi.cluster',compact('data'));
  }

   /*
  public function upload()
  {
    return view('dshr.transaksi.excel');
  }

  public function uploadSave(Request $request)
  {
    $auth = session('auth');
    $dshr = json_decode($request->input('dshr'));
    $sql_dshr = "insert into dshr(nama_pelanggan) value";
    $sql_status = "insert into dshr(nama_pelanggan) value";
    foreach ($dshr as $d) {
      $tgl_deal=$d->tgl_inp;
      $tgl_status=$d->tgl_status;
      if(strtoupper($d->status) == "UNSC"){
        if(strtoupper($d->kendala) == "DEPLOYER")
          $status = "unsc_deployer";
        else
          $status = "unsc_nr2c";
      }else if(strtoupper($d->status) == "UN-SC"){
        if(strtoupper($d->kendala) == "DEPLOYER")
          $status = "unsc_deployer";
        else
          $status = "unsc_nr2c";
      }else if(strtoupper($d->status) == "KENDALA VA"){
        $status = "kendala_va";
      }else if(strtoupper($d->status) == "KENDALA TC"){
        $status = "kendala_tc";
      }else if(strtoupper($d->status) == "TICKET CREATED"){
        $status = "tiket_created";
      }else{
        $status = strtolower($d->status);
      }
      if($d->korlap == "MAJID")
        $korlap = "MUHAMMAD AL MAJID";
      else if($d->korlap == "HILMAN")
        $korlap = "MARSEKAL HILMAN AL FARIS";
      else if($d->korlap == "RESTU")
        $korlap = "RESTU HIDAYATULLAH";
      else if($d->korlap == "MAULANA")
        $korlap = "MAULANA KHAIRIN FANSURI,SP";
      else
        $korlap = $d->korlap;
      $mitra = "DSHR";
      $buffer = explode("/", trim($d->sales));
      if(isset($buffer[1]))
        $mitra = $buffer[1];
      $exists = DB::select('
        SELECT *
        FROM dshr d left join dshr_status s on d.dshr_status_id=s.id
        WHERE (d.nama_pelanggan = ? and d.no_speedy = ? and d.no_telp = ?) or kode_sc = ?
      ',[
        $d->nama, $d->speedy, $d->telp, $d->sc
      ]);
      if(count($exists)){
        $exists=$exists[0];
        //update
        if($exists->status == $d->status){
          //no update status

        }else{
          //update status

        }
      }else{
        //insert with status

        //enable or diable

        DB::transaction(function() use($d, $auth, $status,$tgl_status,$tgl_deal, $korlap, $mitra) {
          $id_status = DB::table('dshr_status')->insertGetId([
            'status'      => $status,
            'tgl'  => $tgl_status,
            'created_by'  => $auth->id_karyawan
          ]);
          $id_dshr = DB::table('dshr')->insertGetId([
            'nik_sales'       => $d->nik,
            'pic'             => $d->pic,
            'alamat'          => $d->alamat,
            'jenis_layanan'   => $d->jenis,
            'nama_odp'        => $d->odp,
            'kode_sc'         => $d->sc,
            'harga_deal'      => $d->harga,
            'tanggal_deal'    => $tgl_deal,
            'nama_pelanggan'  => $d->nama,
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'dshr_status_id'  => $id_status,
            'korlap'          => $korlap,
            'email'           => $d->email,
            'paket_deal'      => $d->paket,
            'keterangan'      => $d->keterangan,
            'kodefikasi'      => $d->kodefikasi,
            'koor_pel'        => $d->koor_pel,
            'koor_odp'        => $d->koor_odp,
            'kendala'         => $d->kendala,
            'no_speedy'       => $d->speedy,
            'no_telp'         => $d->telp,
            'mitra'           => $mitra
          ]);
          DB::table('dshr_status')
            ->where('id', $id_status)
            ->update([
              'dshr_id'       => $id_dshr
          ]);
        });

      }

    }
      return redirect('/dshr-transaksi')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>sukses</strong>']
    ]);
  }
  */
  public function input($id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT a.*, a.status_hdesk as status_hdesk_id, b.status_hdesk as status_hdesk_text,c.nama
      FROM dshr a
	  LEFT JOIN dshr_status_hdesk b ON a.status_hdesk = b.status_hdesk_id
	  LEFT JOIN karyawan c ON a.nik_sales = c.id_karyawan
      WHERE id = ?
    ',[
      $id
    ]);
    if(count($exists)){
      $data = $exists[0];
    }else{
      $data = new \StdClass;
      $data->jenis_layanan = null;
      $data->id = null;
	    $data->status_hdesk = null;
	    $data->status_hdesk_id = null;
	    $data->status_hdesk_text = null;
	    $data->nama = null;
      $data->magang = null;
    }
    if($data->jenis_layanan == "0P"){
      $j = "p";
    }else{
      $j = "m";
    }
    $korlap = $this->getKorlap($auth->id_karyawan);
    $magang = DB::select('
      SELECT nama as id, nama as text
      FROM karyawan
      WHERE status_kerja = "MAGANG"
    ');
    $photoInputs = $this->photoInputs;
    return view('dshr.transaksi.input2', compact('data','korlap','photoInputs', 'magang'));
  }

  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM dshr
      WHERE id = ?
    ',[
      $id
    ]);

    $sendReport = 0;
    if (count($exists)) {
      $data = $exists[0];

		  if ($request->input('status_hdesk')<>''){
			  $status_hdesk = $request->input('status_hdesk');
		  } else {
			  $status_hdesk = $data->status_hdesk;
		  }
		  if ($request->input('kodefikasi')<>''){
			  $kodefikasi = $request->input('kodefikasi');
		  } else {
			  $kodefikasi = $data->kodefikasi;
		  }
		  if ($request->input('jenis_layanan')<>''){
			  $jenis_layanan = $request->input('jenis_layanan');
		  } else {
			  $jenis_layanan = $data->jenis_layanan;
		  }
		  if ($request->input('kode_sc')<>''){
			  $kode_sc = $request->input('kode_sc');
		  } else {
			  $kode_sc = $data->kode_sc;
		  }
		  if ($request->input('keterangan_hdesk')<>''){
			  $keterangan_hdesk = $request->input('keterangan_hdesk');
		  } else {
			  $keterangan_hdesk = $data->keterangan_hdesk;
		  }
	    if ($request->input('flagging')<>''){
			  $flagging = $request->input('flagging');
		  } else {
			  $flagging = $data->flagging;
		  }

      DB::transaction(function() use($flagging, $kode_sc, $jenis_layanan, $keterangan_hdesk, $kodefikasi, $status_hdesk, $request, $data, $auth, $id) {
        DB::table('dshr')
          ->where('id', $id)
          ->update([
            'pic'               => $request->input('pic'),
            'alamat'            => $request->input('alamat'),
            'paperless'         => $request->input('paperless'),
            'flagging'          => $flagging,
			      'tipe_transaksi'    => $request->input('tipe_transaksi'),
            'jenis_layanan'     => $jenis_layanan,
            'status_hdesk'      => $status_hdesk,
            'kode_sc'           => $kode_sc,
            'keterangan_hdesk'  => $keterangan_hdesk,
            'nama_odp'          => $request->input('odp'),
            'harga_deal'        => $request->input('harga'),
            'tanggal_deal'      => $request->input('tanggal_deal'),
            'nama_pelanggan'    => $request->input('nama'),
            'modified_at'       => DB::raw('NOW()'),
            'modified_by'       => $auth->id_karyawan,
            'email'             => $request->input('email'),
            'paket_deal'        => $request->input('paket'),
            'keterangan'        => $request->input('keterangan'),
            'kodefikasi'        => $kodefikasi,
            'koor_pel'          => $request->input('koor_pel'),
            'koor_odp'          => $request->input('koor_odp'),
            'no_internet'       => $request->input('no_internet'),
            'no_telp'           => $request->input('no_telp'),
            'magang'            => $request->input('magang'),
            'no_ktp'            => $request->input('no_ktp')
          ]);
          $this->handleFileUpload($request, $id);
          DB::table('dshr_status_hdesk_log')->insert([
            'dshr_id' => $id,
            'status_hdesk' => $status_hdesk,
            'updated_by' => $auth->id_karyawan
          ]);
      });
//       $this->send_to_telegram($id);
    }
    else {
      $sendReport = 1;
      DB::transaction(function() use($request, $id, $auth) {
        $id_dshr = DB::table('dshr')->insertGetId([
          'nik_sales'       => $auth->id_karyawan,
          'pic'             => $request->input('pic'),
          'alamat'          => $request->input('alamat'),
          'paperless'       => $request->input('paperless'),
          'tipe_transaksi'  => $request->input('tipe_transaksi'),
          'jenis_layanan'   => $request->input('jenis_layanan'),
          'status_hdesk'    => 7,
          'nama_odp'        => $request->input('odp'),
          'harga_deal'      => $request->input('harga'),
          'flagging'        => $request->input('flagging'),
          'tanggal_deal'    => $request->input('tanggal_deal'),
          'nama_pelanggan'  => $request->input('nama'),
          'created_at'      => DB::raw('NOW()'),
          'created_by'      => $auth->id_karyawan,
          'email'           => $request->input('email'),
          'paket_deal'      => $request->input('paket'),
          'keterangan'      => $request->input('keterangan'),
          'kodefikasi'      => $request->input('kodefikasi'),
          'koor_pel'        => $request->input('koor_pel'),
          'koor_odp'        => $request->input('koor_odp'),
          'no_internet'     => $request->input('no_internet'),
          'no_telp'         => $request->input('no_telp'),
          'korlap'          => $request->input('korlap'),
          'magang'          => $request->input('magang'),
          'no_ktp'          => $request->input('no_ktp')
        ]);
        $this->handleFileUpload($request, $id_dshr);
        DB::table('dshr_status_hdesk_log')->insert([
          'dshr_id' => $id_dshr,
          'status_hdesk' => 7,
          'updated_by' => $auth->id_karyawan
        ]);

      });
    }
    if($sendReport){
        exec('cd ..;php artisan sendReportDSHR '.$id.' > /dev/null &');
    }

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
    ]);
  }

  public function destroy($id)
  {
    DB::table('dshr')->where('id', [$id])->delete();
    return redirect('/dshr-transaksi')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }
  public function getSales($id)
  {
    $karyawan = DB::select('
      SELECT id_karyawan as id,nama as text
      FROM karyawan left join regu on karyawan.id_regu=regu.id_regu
      WHERE regu.uraian = ?
    ',[ $id ]);
    return json_encode($karyawan);
  }
  public function getStatus($id)
  {
    if($id == "0P"){
      $j="p";
    }else{
      $j="m";
    }
    $status = DB::select('
      SELECT id, status as text
      FROM dshr_combo
      WHERE jenis in("",?)
    ',[ $j ]);
    return json_encode($status);
  }
  private function getKorlap($nik){
    $exists = DB::select('
      SELECT *
      FROM karyawan a left join regu b on a.id_regu=b.id_regu
      WHERE id_karyawan = ? and job="DSHR"
    ',[ $nik ]);
    if(count($exists)){
      $data = $exists[0];
      return $data->uraian;
    }else{
      return '';
    }
  }
  private function getmagang(){
    $exists = DB::select('
      SELECT nama as id, nama as text
      FROM karyawan
      WHERE status_kerja = "MAGANG"
    ');
    if(count($exists)){
      $data = $exists[0];
      return $data;
    }else{
      return '';
    }
  }
  public function today()
  {
    $auth = session('auth');
    $day=date("Y-m-d");
	if(session('auth')->level == 49) {
		$list = DB::select('
		  SELECT
			a.uraian,
			d.*,
			b.nama as sales,
			c.status_hdesk as status_hdesk,
      e.orderStatus,
      e.orderId,
      e.kcontact
		  FROM dshr d
		  left join 1_2_employee b on d.nik_sales = b.nik
		  left join regu a on b.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE nik_sales = ?  AND tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		',[ $auth->id_karyawan ]);
	} else if (session('auth')->level == 50){
		$list = DB::select('
		  SELECT
			a.uraian,
			d.*,
			b.nama as sales,
			c.status_hdesk as status_hdesk,
		  e.orderStatus,
      e.orderId,
      e.kcontact
      FROM dshr d
      left join 1_2_employee b on d.nik_sales = b.nik
		  left join regu a on  a.id_regu = b.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  where a.id_regu = "'.session('auth')->id_regu.'" AND d.tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		');
	} else {
		$list = DB::select('
		  SELECT
			a.uraian,
			d.*,
			b.nama as sales,
			c.status_hdesk as status_hdesk,
			e.orderStatus,
		  e.orderId,
      e.kcontact
      FROM dshr d
		  left join karyawan b on b.id_karyawan = d.nik_sales
		  left join regu a on  a.id_regu = b.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  where d.tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		');
	}

    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function tm()
  {
    $day=date("Y-m");
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$list = DB::select('
		  SELECT
			a.uraian,
			d.*,
			b.nama as sales,
			c.status_hdesk as status_hdesk,
			e.orderStatus,
      e.orderId,
      e.kcontact
		  FROM dshr d
		  left join 1_2_employee b on d.nik_sales = b.nik
		  left join regu a on b.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.pic = e.orderKontak
		  WHERE nik_sales = ?  AND tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		',[ $auth->id_karyawan ]);
	} else if (session('auth')->level == 50) {
		$list = DB::select('
      SELECT
      a.uraian,
      d.*,
      b.nama as sales,
      c.status_hdesk as status_hdesk,
      e.orderStatus,
      e.orderId,
      e.kcontact
      FROM dshr d
      left join 1_2_employee b on d.nik_sales = b.nik
      left join regu a on b.id_regu = a.id_regu
      left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
      left join Data_Pelanggan_Starclick e ON d.pic = e.orderKontak
		  where a.id_regu = "'.session('auth')->id_regu.'" AND tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		');
	} else {
		$list = DB::select('
      SELECT
      a.uraian,
      d.*,
      b.nama as sales,
      c.status_hdesk as status_hdesk,
      e.orderStatus,
      e.orderId,
      e.kcontact
      FROM dshr d
      left join 1_2_employee b on d.nik_sales = b.nik
      left join regu a on b.id_regu = a.id_regu
      left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
      left join Data_Pelanggan_Starclick e ON d.pic = e.orderKontak
		  where tanggal_deal like "'.$day.'%"
		  order by d.status_hdesk desc
		');
	}
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function all(Request $req, $search)
  {
    /*$auth = session('auth');
	if(session('auth')->level == 49) {
		$list = DB::select('
		  SELECT
		  a.uraian,
		  d.*,
		  karyawan.nama as sales,
		  c.status_hdesk as status_hdesk,
		  e.orderStatus
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE nik_sales = ?
		  order by d.status_hdesk desc
		',[ $auth->id_karyawan ]);
	} else if (session('auth')->level==50){
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  where a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc
		');
	} else {
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  order by d.status_hdesk desc
		');

	}
    dd($list);
    return view('dshr.transaksi.search', compact('list'));
    */
    $list = null;
    if ($search) {
        $list = DB::select('
      SELECT
      a.uraian,
      d.*,
      karyawan.nama as sales,
      c.status_hdesk as status_hdesk,
      e.orderStatus, e.orderId, e.kcontact
      FROM dshr d
      left join karyawan on d.nik_sales = karyawan.id_karyawan
      left join regu a on karyawan.id_regu = a.id_regu
      left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
      left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
      WHERE kode_sc = ? OR no_telp = ? OR no_internet = ?
      order by d.status_hdesk desc
    ',[ $search, $search, $search ]);
    }
      if (session('auth')->level==49){
          return view('dshr.transaksi.all', compact('list'));
      }
      else{
          return view('dshr.transaksi.all2', compact('list'));
      };
  }

  public function uim()
  {
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE d.status_hdesk IN (7) AND nik_sales = ?
		  order by d.status_hdesk desc
		',[ $auth->id_karyawan ]);
	} else if (session('auth')->level == 50){
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			a.id_regu = "'.session('auth')->id_regu.'" AND
			d.status_hdesk IN (7)
		  order by d.status_hdesk desc
		');
	} else {
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (7)
		  order by d.status_hdesk desc
		');

	}
    // dd($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

   public function odpbelummuncul()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (4)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

   public function followupsales()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (3)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

   public function unscdeployer()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (5)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

   public function unscnr2g()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (6)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

   public function odpsudahmuncul()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (14)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function followupopen()
  {
	$sql = '
		SELECT f.*, a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM prospek_mbsp f
      left join dshr d ON d.no_telp = SUBSTR(f.ND,1,12)
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk is NULL AND
      f.Status_Inbox = "OPEN"
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    return view('dshr.transaksi.all1', compact('list'));
  }

  public function followupok()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (15)
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function kendalawebcare()
 {
 $sql = '
   SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
     FROM dshr d
     left join karyawan on d.nik_sales = karyawan.id_karyawan
     left join regu a on karyawan.id_regu = a.id_regu
     left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
     left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
     WHERE
     d.status_hdesk IN (17)
 ';
   $auth = session('auth');
 if(session('auth')->level == 49) {
   $sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
     order by d.status_hdesk desc';
 } else if (session('auth')->level == 50){
   $sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
     order by d.status_hdesk desc';
 } else {
   $sql .= '
     order by d.status_hdesk desc
   ';
 }
 $list = DB::select($sql);
   //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
 }

 public function belumwebcare()
{
$sql = '
  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
    FROM dshr d
    left join karyawan on d.nik_sales = karyawan.id_karyawan
    left join regu a on karyawan.id_regu = a.id_regu
    left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
    left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
    WHERE
    d.status_hdesk IN (18)
';
  $auth = session('auth');
if(session('auth')->level == 49) {
  $sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
    order by d.status_hdesk desc';
} else if (session('auth')->level == 50){
  $sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
    order by d.status_hdesk desc';
} else {
  $sql .= '
    order by d.status_hdesk desc
  ';
}
$list = DB::select($sql);
  //var_dump($list);
  if (session('auth')->level==49){
      return view('dshr.transaksi.all', compact('list'));
  }
  else{
      return view('dshr.transaksi.all2', compact('list'));
  };
}

public function berhasilinput()
{
$sql = '
 SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
   FROM dshr d
   left join karyawan on d.nik_sales = karyawan.id_karyawan
   left join regu a on karyawan.id_regu = a.id_regu
   left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
   left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
   WHERE
   d.status_hdesk IN (2)
';
 $auth = session('auth');
if(session('auth')->level == 49) {
 $sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
   order by d.status_hdesk desc';
} else if (session('auth')->level == 50){
 $sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
   order by d.status_hdesk desc';
} else {
 $sql .= '
   order by d.status_hdesk desc
 ';
}

$list = DB::select($sql);
 //var_dump($list);
  if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
}

public function mappingok()
{
$sql = '
 SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
   FROM dshr d
   left join karyawan on d.nik_sales = karyawan.id_karyawan
   left join regu a on karyawan.id_regu = a.id_regu
   left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
   left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
   WHERE
    nik_sales = ""
';
 $auth = session('auth');
if(session('auth')->level == 49) {
 $sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
   order by d.status_hdesk desc';
} else if (session('auth')->level == 50){
 $sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
   order by d.status_hdesk desc';
} else {
 $sql .= '
   order by d.status_hdesk desc
 ';
}
$list = DB::select($sql);
 //var_dump($list);
  if (session('auth')->level==49){
      return view('dshr.transaksi.all', compact('list'));
  }
  else{
      return view('dshr.transaksi.all2', compact('list'));
  };
}

public function sudahwebcare()
{
$sql = '
 SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
   FROM dshr d
   left join karyawan on d.nik_sales = karyawan.id_karyawan
   left join regu a on karyawan.id_regu = a.id_regu
   left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
   left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
   WHERE
   d.status_hdesk IN (19)
';
 $auth = session('auth');
if(session('auth')->level == 49) {
 $sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
   order by d.status_hdesk desc';
} else if (session('auth')->level == 50){
 $sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
   order by d.status_hdesk desc';
} else {
 $sql .= '
   order by d.status_hdesk desc
 ';
}
$list = DB::select($sql);
 //var_dump($list);
  if (session('auth')->level==49){
      return view('dshr.transaksi.all', compact('list'));
  }
  else{
      return view('dshr.transaksi.all2', compact('list'));
  };
}

public function cluster($id){
    if ($id<>"ALL"){
      $Where = 'AND (d.flagging LIKE "%'.$id.'%")';
    } else {
      $Where = '';
    }
    $sql = '
  		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
  		  FROM dshr d
  		  left join karyawan on d.nik_sales = karyawan.id_karyawan
  		  left join regu a on karyawan.id_regu = a.id_regu
  		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
  		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
        left join dshr_cluster_pelanggan f ON d.no_telp = f.nd
  		  WHERE
  			d.status_hdesk NOT IN ("X")
        '.$Where.'
  	';
      $auth = session('auth');
  	if(session('auth')->level == 49) {
  		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
  		  order by d.status_hdesk desc';
  	} else if (session('auth')->level == 50){
  		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
  		  order by d.status_hdesk desc';
  	} else {
  		$sql .= '
  			order by d.status_hdesk desc
  		';
  	}
  	$list = DB::select($sql);
      //var_dump($list);
      if (session('auth')->level==49){
          return view('dshr.transaksi.all', compact('list'));
      }
      else{
          return view('dshr.transaksi.all2', compact('list'));
      };
  }

  public function neworder()
  {
	$sql = '
		SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (1,"","BELUM DIEDIT")
	';
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$sql .= 'AND nik_sales = "'.$auth->id_karyawan.'"
		  order by d.status_hdesk desc';
	} else if (session('auth')->level == 50){
		$sql .= 'AND a.id_regu = "'.session('auth')->id_regu.'"
		  order by d.status_hdesk desc';
	} else {
		$sql .= '
			order by d.status_hdesk desc
		';
	}
	$list = DB::select($sql);
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function rfsoke()
  {
    $auth = session('auth');
	if(session('auth')->level == 49) {
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE d.status_hdesk IN (11) AND nik_sales = ?
		  order by d.status_hdesk desc
		',[ $auth->id_karyawan ]);
	} else if (session('auth')->level==50){
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			a.id_regu = "'.session('auth')->id_regu.'" AND
			d.status_hdesk IN (11)
		  order by d.status_hdesk desc
		');
	} else {
		$list = DB::select('
		  SELECT a.uraian,d.*, karyawan.nama as sales, c.status_hdesk as status_hdesk, e.orderStatus, e.orderId, e.kcontact
		  FROM dshr d
		  left join karyawan on d.nik_sales = karyawan.id_karyawan
		  left join regu a on karyawan.id_regu = a.id_regu
		  left join dshr_status_hdesk c ON d.status_hdesk = c.status_hdesk_id
		  left join Data_Pelanggan_Starclick e ON d.kode_sc = e.orderId
		  WHERE
			d.status_hdesk IN (11)
		  order by d.status_hdesk desc
		');

	}
    //var_dump($list);
    if (session('auth')->level==49){
        return view('dshr.transaksi.all', compact('list'));
    }
    else{
        return view('dshr.transaksi.all2', compact('list'));
    };
  }

  public function dm()
  {
    $day=date("Y-m-d");
    $ms2n = DB::select('
      SELECT ND_Speedy, Tgl_PS, m.Status, s.status as dshr_status, d.id, d.nama_pelanggan FROM ms2n m
      LEFT JOIN dshr d on m.ND_Speedy = d.no_speedy
      LEFT join dshr_status s on d.dshr_status_id = s.id
      WHERE Sebab="--" and Kcontact like "%PTTA%" and Tgl_PS=?
    ',[ $day ]);
    return view('dshr.transaksi.dm', compact('ms2n'));
  }

  private function handleFileUpload($request, $id)
  {
    foreach($this->photoInputs as $name) {
      $input = 'photo-'.$name;
      if ($request->hasFile($input)) {
        //dd($input);
        $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
        $path = public_path().'/'.$upload.'/dshr/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }

	public function status(){
		if (session('auth')->level==51){
			$list = DB::select('
				SELECT * from dshr_status_hdesk WHERE status_hdesk_level = 1
			');
		} else if (session('auth')->level==49){
			$list = DB::select('
				SELECT * from dshr_status_hdesk WHERE status_hdesk_level = 2
			');
		} else {
			$list = DB::select('
				SELECT * from dshr_status_hdesk
			');
		}
		foreach ($list as $data) :
			$result[] = array("id"=>$data->status_hdesk_id, "text"=>$data->status_hdesk);
		endforeach;
		echo json_encode($result);
	}

	public function substatus(){
		$term = $_GET['term'];
		$list = DB::select('
			SELECT * from dshr_sub_status_hdesk WHERE status_hdesk_id = ?
		',[$term]);

		foreach ($list as $data) :
			$result[] = array("id"=>$data->sub_status_hdesk_id, "text"=>$data->sub_status_hdesk);
		endforeach;
		echo json_encode($result);
	}

  public function listSplitter(Request $req){
      $data = DshrModel::listSplitter();
      if ($req->has('q')){
          $cari = $req->input('q');

          $data = DshrModel::listSplitterByTelp($cari);
      }

      return view('dshr.splitter.list',compact('data'));
  }

  public function inputSplitter(){
      $auth = session('auth');
      $dataKaryawan = DB::table('1_2_employee')->where('nik',$auth->id_user)->first();

      $nik = '-';
      if (count($dataKaryawan)<>0){
          $nik  = $dataKaryawan->nik.' [ '.$dataKaryawan->nama.' ] ';
      }

      $regu = DB::select('
                SELECT id_regu as id,uraian as text, telp, job
                FROM regu
                WHERE
                ACTIVE = 1
              ');

      return view('dshr.splitter.input',compact('nik', 'regu'));
  }

  public function simpanSplitter(Request $req){
      $this->validate($req,[
          'noTelp'   => 'required|numeric',
          'noTiket'  => 'required|',
      ],[
          'noTelp.required'    => 'No Telpon Jangan Kosonng',
          'noTelp.numeric'     => 'Angka Ja Kawanya',
          'noTiket.required'   => 'No Tiket Jangan Kosong',
      ]);

      if (session('auth')->nama<>''){
          $created = session('auth')->id_user.' [ '.session('auth')->nama.' ] ';
      }
      else{
          $created = session('auth')->id_user;
      };

      DshrModel::simpanData($req, $req->noTiket, $created);
      return redirect('/dshr/splitter/list')->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);
  }

  public function editSplitter($noTiket){
      $data = DshrModel::getDataDshrSplitterByNoTiket($noTiket);

      $regu = DB::select('
              SELECT id_regu as id,uraian as text, telp, job
              FROM regu
              WHERE
              ACTIVE = 1
            ');

      return view('dshr.splitter.edit',compact('data', 'noTiket', 'regu'));
  }

  public function editSimpanSplitter(Request $req, $noTiket){
      $this->validate($req,[
          'noTelp'   => 'required|numeric',
          'noTiket'  => 'required|'
      ],[
          'noTelp.required'    => 'No Telpon Jangan Kosonng',
          'noTelp.numeric'     => 'Angka Ja Kawanya',
          'noTiket.required'   => 'No Tiket Jangan Kosong'
      ]);

      if (session('auth')->nama<>''){
          $created = session('auth')->id_user.' [ '.session('auth')->nama.' ] ';
      }
      else{
          $created = session('auth')->id_user;
      };

      DshrModel::ubahSimpanData($req, $noTiket, $created);
      return redirect('/dshr/splitter/list')->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Diubah']]);
  }

  public function listSales($progress){
      $auth = session('auth');

      $jmlList = count(DshrModel::getListMyirByKodeSales($auth->id_user));
      $jmlHi   = count(DshrModel::getListMyirProses(date('Y-m-d'),$auth->id_user));
      $jmlKt   = count(DshrModel::getListMyirByKtKp($auth->id_user, date('Y-m-d'), 'listkt'));
      $jmlKp   = count(DshrModel::getListMyirByKtKp($auth->id_user, date('Y-m-d'), 'listkp'));;
      $jmlUp   = count(DshrModel::getListMyirUP($auth->id_user, date('Y-m-d')));
      $ket     = '';

      if($progress=='list'){
          $data = DshrModel::getListMyirByKodeSales($auth->id_user);
          $ket  = 'list';
      }
      elseif($progress=='listhi'){
          $data = DshrModel::getListMyirProses(date('Y-m-d'),$auth->id_user);
          $ket  = 'hi';
      }
      elseif($progress=='listkt'){
          $data = DshrModel::getListMyirByKtKp($auth->id_user, date('Y-m-d'), 'listkt');
          $ket  = 'kt';
      }
      elseif($progress=='listkp'){
          $data = DshrModel::getListMyirByKtKp($auth->id_user, date('Y-m-d'), 'listkp');
          $ket  = 'kp';
      }
      elseif($progress=='listup'){
          $data = DshrModel::getListMyirUP($auth->id_user, date('Y-m-d'));
          $ket  = 'up';
      }

      return view('dshr.sales.list',compact('data', 'jmlList', 'jmlHi', 'jmlKt', 'jmlKp', 'jmlUp', 'ket'));
  }

  public function inputSales(){
      $auth = session('auth');
      $kodeSales = $auth->id_user;

      return view('dshr.sales.input',compact('kodeSales'));
  }

  public function saveSales(Request $req){
      $this->validate($req,[
          'trackerId'     => 'required',
          'channel'       => 'required',
          'kodeSales'     => 'Required',
          'tglRegistrasi' => 'Required',
          'nmPelanggan'   => 'Required',
          'jnsLayanan'    => 'Required',
          'odp'           => 'Required',
          'koorPelanggan' => 'Required',
          'alamat'        => 'Required',
          'picPelanggan'  => 'Required',
          'transaksiPsb'  => 'Required'
      ],[
          'trackerId.required'      => 'TrackerId Belum Diisi',
          'channel.required'        => 'Channel Belum Diisi',
          'kodeSales.required'      => 'Kode Sales Belum Diisi',
          'tglRegistrasi'           => 'Tgl Registrasi Belum Diisi',
          'nmPelanggan.required'    => 'Nama Pelanggan Belum Diisi',
          'jnsLayanan.required'     => 'Jenis Layanan Belum Diisi',
          'odp.required'            => 'ODP Belum Diisi',
          'koorPelanggan.required'  => 'Koordinat Pelanggan Belum Diisi',
          'alamat.required'         => 'Alamat Belum Diisi',
          'picPelanggan.required'   => 'PIC Pelanggan Belum Diisi',
          'transaksiPsb.required'   => 'Transaksi PSB Belum Diisi'
      ]);

      DshrModel::simpanInputSales($req);
      return redirect('/sales/list')->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);
  }

  public function plasaForm(Request $req){
      date_default_timezone_set('Asia/Makassar');
      $paketHarga = DshrModel::getPaketHarga();
      $jenis_layanan = DshrModel::getJenisLayanan();
      $getSto     = DB::select('SELECT sto as id, sto as text from maintenance_datel where witel = "'.session('witel').'"');
      $tgl        = date('Y-m-d H:i:s');
      $level      = session('auth')->level;
      $nama_odp   = '';
      $kor_odp    = '';
      if ($req->has('OdpByInet')){
	    	$cari = $req->input('OdpByInet');

	    	$sql = 'SELECT pl.nama_odp, pl.kordinat_odp 
	    			FROM dispatch_teknisi dt
	    			LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
	    			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
	    			WHERE dps.internet = "'.$cari.'" OR dps.noTelp = "'.$cari.'" ORDER BY dps.orderDate ASC';

	    	$getOdp = DB::SELECT($sql);
	    	$nama_odp = $cari;

	    	if (count($getOdp)<>0){
          $nama_odp = $getOdp[0]->nama_odp;
	    		$kor_odp  = $getOdp[0]->kordinat_odp;
	    	}

	    };

      return view('dshr.plasasales.formPlasa',compact('paketHarga', 'getSto', 'tgl', 'level', 'nama_odp', 'kor_odp', 'getOdp', 'jenis_layanan'));
  }

  public function plasaFormSimpan(Request $req)
  {
      $this->validate($req,[
          'photo_ktp'              => 'required',
          'photo_ttd'              => 'required',
          'photo_ktp_pelanggan'    => 'required',
          'photo_capture_odp'      => 'required',
          // 'orderNo'                => 'required',
          'tglOrder'               => 'required',
          'nama_pemohon'           => 'required',
          'nama_tagihan'           => 'required',
          'ibuKandung'             => 'required',
          'tglLahir'               => 'required',
          'npwp'                   => 'required|numeric',
          'alamat'                 => 'required',
          'kelurahan'              => 'required',
          'koordinatPelanggan'     => 'required',
          'namaOdp'                => 'required',
          'koordinatOdp'           => 'required',
          'layanan'                => 'required',
          'paketHarga'             => 'required',
          'sto'                    => 'required',
          'email'                  => 'required|email',
          'picPelanggan'           => 'required',
          'terbitKtp'              => 'required',
          'masaKtp'                => 'required',
          'no_internet'            => 'required',
          'kcontack'               => 'required'
      ],[
          'photo_ktp.required'              => 'Foto KTP Diupload',
          'photo_ttd.required'              => 'Foto TTD Pelanggan Diupload',
          'photo_ktp_pelanggan.required'    => 'Foto KTP Pelanggan Diupload',
          'photo_capture_odp.required'      => 'Foto Captur ODP Diupload',
          // 'orderNo.required'                => 'No Order Diisi',
          'tglOrder.required'               => 'Tgl Order Diisi',
          'nama_pemohon.required'           => 'Nama Pemohon Diisi',
          'nama_tagihan.required'           => 'Nama Tagihan Diisi',
          'ibuKandung.required'             => 'Nama Ibu Kandung Diisi',
          'tglLahir.required'               => 'Tgl Lahir Diisi',
          'npwp.required'                   => 'NPWP DIisi',
          'npwp.numeric'                    => 'NPWP Hanya Boleh Angka',
          'alamat.required'                 => 'Alamat Diisi',
          'kelurahan.required'              => 'Kelurahan Diisi',
          'koordinatPelanggan.required'     => 'Koordinat Pelanggan Diisi',
          'namaOdp.required'                => 'Nama ODP Diisi',
          'koordinatOdp.required'           => 'Koordinat ODP Diisi',
          'layanan.required'                => 'Jenis Layanan Diisi',
          'paketHarga.required'             => 'Paket Harga DIisi',
          'sto.required'                    => 'STO Diisi',
          'email.required'                  => 'Email Diisi',
          'email.email'                     => 'Email Harus Valid',
          'picPelanggan.required'           => 'Pic Pelanggan Diisi',
          'terbitKtp.required'              => 'Tanggal Terbit KTP Diisi',
          'masaKtp.required'                => 'Tanggal Masa KTP Diisi',
          'no_internet.required'            => 'No Internet Diisi',
          'kcontack.required'               => 'Kcontack Diisi'
      ]);

      // simpan data
      $cekData = DB::table('psb_myir_wo')->where('myir',$req->orderNo)->first();
      if(count($cekData) == 0 )
      {
        $id = DshrModel::simpanPlasa($req);
        $photo = ['ktp','ttd','ktp_pelanggan','capture_odp','lokasi_rumah'];
        $this->handleFileUploadPlasaSales($req, $id, $photo);
        $this->send_to_telegramSales($id, $photo, '0');
      } else {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Karena Order Sudah Rekan!']
      ]);
      };
      $tglAwal = date('Y-m-01');
      $tglAkhir = date('Y-m-d');
      return redirect('/dshr/plasa-sales/list-wo-by-sales/ALL/'.$tglAwal.'/'.$tglAkhir)->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);

  }

  public function salesForm(Request $req){
      $getSto     = DB::select('SELECT sto as id, sto as text from maintenance_datel');
      $getPaket   = DB::select('SELECT id, paket_sales as text FROM psb_paket_sales');
      $level      = session('auth')->level;
      $nama_odp   = '';
      $kor_odp    = '';
      if ($req->has('OdpByInet')){
	    	$cari = $req->input('OdpByInet');

	    	$sql = 'SELECT pl.nama_odp, pl.kordinat_odp 
	    			FROM dispatch_teknisi dt
	    			LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
	    			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
	    			WHERE dps.internet = "'.$cari.'" OR dps.noTelp = "'.$cari.'" ORDER BY dps.orderDate ASC';

	    	$getOdp = DB::SELECT($sql);
	    	$nama_odp = $cari;

	    	if (count($getOdp)<>0){
          $nama_odp = $getOdp[0]->nama_odp;
	    		$kor_odp  = $getOdp[0]->kordinat_odp;
	    	}

	    };

      return view('dshr.plasasales.formSales',compact('getSto', 'level', 'getPaket','nama_odp','kor_odp','getOdp'));
  }

  public function salesFormSimpan(Request $req){
      $this->validate($req,[
          'myir'            => 'required|numeric',
          'nama_pelanggan'  => 'required',
          'pic_pelanggan'   => 'required',
          'koor_pelanggan'  => 'required',
          'sto'             => 'required',
          'layanan'         => 'required',
          'psb'             => 'required',
          'namaOdp'         => 'required',
          'koordinatOdp'    => 'required',
          'alamat_detail'   => 'required',
          'photo_odp'       => 'required',
          'photo_rumah'     => 'required',
          'photo_ktp'       => 'required',
          'email'           => 'required|email',
          'paket'           => 'required'
      ],[
          'myir.required'       => 'Myir Jangan Kosong',
          'myir.numeric'        => 'Inputan Hanya Boleh Angka',
          'nama_pelanggan.required' => 'Nama Pelanggan Diisi',
          'pic_pelanggan.required'  => 'Pic Pelaggan Diisi',
          'koor_pelanggan.required' => 'Koordinat Pelanggan Diisi',
          'sto.required'            => 'STO Diisi',
          'layanan.required'        => 'Layanan Diisi',
          'psb.required'            => 'PSB Diisi',
          'namaOdp.required'        => 'Nama ODP Diisi',
          'koordinatOdp.required'   => 'Koordinat ODP Diisi',
          'alamat_detail.required'  => 'Alamat Detail Diisi',
          'photo_odp.required'      => 'Foto ODP Diisi',
          'photo_rumah.required'    => 'Foto Rumah Pelanggan Diisi',
          'photo_ktp.required'      => 'Foto KTP + Pelanggan Diisi',
          'email.required'          => 'Email Jangan kosong',
          'email.email'             => 'Email Harus Valid',
          'paket.required'          => 'Paket Diisi'
      ]);

      // simpan
      $cekData = DB::table('psb_myir_wo')->where('myir',$req->myir)->first();
      if(count($cekData)==0){
        $auth = session('auth');
        $id = DshrModel::simpanSales($req, $auth->id_user);
        $photo = ['ktp','odp','rumah'];
        $this->handleFileUploadPlasaSales($req, $id, $photo);
        $this->send_to_telegramSales($id, $photo, '1');
      };

      $tglAwal = date('Y-m-01');
      $tglAkhir = date('Y-m-d');
      return redirect('/dshr/plasa-sales/list-wo-by-sales/ALL/'.$tglAwal.'/'.$tglAkhir)->with('alerts',[['type' => 'success', 'text' => 'Data Berhasil Disimpan']]);

  }

  public function listWoBySalesx(Request $reg){
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaBelumDispatch('ALL','ALL', $tgl, $nik);
    date_default_timezone_set('Asia/Makassar');

      $tglAwal  = date('Y-m').'-1';
      $tglAkhir = date('Y-m-d',strtotime("+1 days"));

      if (session('witel')=="KALSEL"){
        $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = file_get_contents($link);
      } else {
        $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = file_get_contents($link);
      }

      $get  = json_decode($file);
      foreach ($get as $g){
        $myir = explode('-', $g->myir);
        if (count($myir)>1){
            $myir = $myir[1];
        }
        else{
            $myir = $myir[0];
        };

        $getMyir = DB::table('psb_myir_wo')->where('myir',$myir)->first();
        $realSTO = explode("-", $g->odp);
        if (count($realSTO)>0){
          $sto = @$realSTO[1];
        } else {
          $sto = "";
        }
        $data = array();
        if(count($getMyir)<>1){
          $data = [
              "orderDate"       => $g->timestamp,
              "created_by"      => $g->kode_sales,
              "myir"            => $myir,
              "customer"        => $g->nama_pelanggan,
              "namaOdp"         => $g->odp,
              "sc"              => null,
              "ket"             => "0",
              "lokerJns"        => "myir",
              "sto"             => $sto,
              "picPelanggan"    => $g->no_hp,
              "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
              "picSales"        => null,
              "alamatLengkap"   => $g->alamat,
              "kelurahan"       => NULL,
              "channel"         => NULL,
              "layanan"         => $g->layanan,
              "sales_id"        => $g->kode_sales,
              "email"           => $g->email,
              "psb"             => null,
              "dispatch"        => 0,
              "ketMyirGangguan" => 0,
              "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
              "nama_tagihan"    => NULL,
              "tgl_lahir"       => NULL,
              "no_ktp"          => NULL,
              "terbit_ktp"      => NULL,
              "masa_ktp"        => NULL,
              "nama_ibu"        => NULL,
              "npwp"            => NULL,
              "paket_harga_id"  => NULL,
              "paket_sales_id"  => NULL,
              "ket_input"       => 2,
              "no_internet"     => NULL,
              "no_telp"         => NULL,
              "kcontack"        => NULL,
              "id_wo"           => NULL,
              "paket_harga"     => NULL,
              "paket_sales"     => NULL
          ];

          array_push($getData, (object)$data);
        }
      }

      return view('dshr.plasasales.list',compact('getData', 'tgl'));
  }

  public function order_qc1(Request $req, $tgl){
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaBelumDispatch_qc1('ALL', $tgl, $nik);
      date_default_timezone_set('Asia/Makassar');

      $tglAwal  = date('Y-m').'-1';
      $tglAkhir = date('Y-m-d',strtotime("+1 days"));

      if (session('witel')=="KALSEL"){
        $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = @file_get_contents($link);
      } else {
        $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      $file = @file_get_contents($link);
      }

      $get  = json_decode($file);
      if (count($get)){
      foreach ($get as $g){
        $myir = explode('-', $g->myir);
        if (count($myir)>1){
            $myir = $myir[1];
        }
        else{
            $myir = $myir[0];
        };
        $getMyir = DB::SELECT('select * from psb_myir_wo where myir = "'.$myir.'" AND (status_approval = 1 OR sc <> "")');
        $realSTO = explode("-", $g->odp);
        if (count($realSTO)>0){
          $sto = @$realSTO[1];
        } else {
          $sto = "";
        }
        if(count($getMyir)==0){
          $data = [
              "orderDate"       => $g->timestamp,
              "created_by"      => $g->kode_sales,
              "myir"            => $myir,
              "customer"        => $g->nama_pelanggan,
              "namaOdp"         => $g->odp,
              "sc"              => null,
              "ket"             => "0",
              "lokerJns"        => "myir",
              "sto"             => $sto,
              "picPelanggan"    => $g->no_hp,
              "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
              "picSales"        => null,
              "alamatLengkap"   => $g->alamat,
              "kelurahan"       => NULL,
              "channel"         => NULL,
              "layanan"         => $g->layanan,
              "sales_id"        => $g->kode_sales,
              "email"           => $g->email,
              "psb"             => null,
              "dispatch"        => 0,
              "ketMyirGangguan" => 0,
              "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
              "nama_tagihan"    => NULL,
              "tgl_lahir"       => NULL,
              "no_ktp"          => NULL,
              "terbit_ktp"      => NULL,
              "masa_ktp"        => NULL,
              "nama_ibu"        => NULL,
              "npwp"            => NULL,
              "paket_harga_id"  => NULL,
              "paket_sales_id"  => NULL,
              "ket_input"       => 2,
              "no_internet"     => NULL,
              "no_telp"         => NULL,
              "kcontack"        => NULL,
              "id_wo"           => NULL,
              "paket_harga"     => NULL,
              "paket_sales"     => NULL
          ];

          array_push($getData, (object)$data);
        }
        }
      }

      $active = "validasi";

      return view('dshr.plasasales.list',compact('active','getData', 'tgl'));
  }
  public function decline($myir){
    date_default_timezone_set('Asia/Makassar');
    $cek = DB::table('psb_myir_wo')->where('myir',$myir)->get();
    if (count($cek)>0){
    $update = DB::table('psb_myir_wo')->where('myir',$myir)->update([
      'dispatch'        => 0,
      'status_approval' => 2,
      'approve_by'      => NULL,
      'approve_date'    => NULL,
      'decline_status'  => 1,
      'decline_by'      => session('auth')->id_karyawan,
      'decline_date'    => date('Y-m-d H:i:s')
    ]);
    } else {
// simpan dalam psb_myir_wo
          if (session('witel')=="KALSEL"){
              $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$myir);
          }
          else {
              $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$myir);
          };

          $get = json_decode($file);

          $getData = [
              'myir'            => $get->myir,
              'nama_pelanggan'  => $get->nama_pelanggan,
              'namaOdp'         => $get->odp,
              'sto'             => $get->sto,
              'pic_pelanggan'   => $get->alamat,
              'koor_pelanggan'  => $get->latitude.','.$get->longitude,
              'alamat_detail'   => $get->alamat,
              'layanan'         => $get->layanan,
              'psb'             => NULL,
              'koordinatOdp'    => $get->odp_latitude.','.$get->odp_longitude,
              'email'           => $get->email,
              'paket'           => NULL,
              'sales'           => $get->kode_sales,
              'dispatch'        => 0,
              'status_approval' => 2,
              'approve_by'      => NULL,
              'approve_date'    => NULL,
              'decline_status'  => 1,
              'decline_by'      => session('auth')->id_karyawan,
              'decline_date' => date('Y-m-d H:i:s')
          ];

          $getData = (object)$getData;
    }
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Decline Order']
    ]);
  }

    public function return_myir($myir){
    date_default_timezone_set('Asia/Makassar');
    $cek = DB::table('psb_myir_wo')->where('myir',$myir)->get();
    if (count($cek)>0){
    $update = DB::table('psb_myir_wo')->where('myir',$myir)->update([
      'ket'             => 0,
      'dispatch'        => 0,
      'status_approval' => 0,
      'approve_by'      => NULL,
      'approve_date'    => NULL,
      'decline_status'  => 0,
      'decline_by'      => NULL,
      'decline_date'    => NULL
    ]);
    } else {
// simpan dalam psb_myir_wo
          if (session('witel')=="KALSEL"){
              $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$myir);
          }
          else {
              $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$myir);
          };

          $get = json_decode($file);

          $getData = [
              'myir'            => $get->myir,
              'nama_pelanggan'  => $get->nama_pelanggan,
              'namaOdp'         => $get->odp,
              'sto'             => $get->sto,
              'pic_pelanggan'   => $get->alamat,
              'koor_pelanggan'  => $get->latitude.','.$get->longitude,
              'alamat_detail'   => $get->alamat,
              'layanan'         => $get->layanan,
              'psb'             => NULL,
              'koordinatOdp'    => $get->odp_latitude.','.$get->odp_longitude,
              'email'           => $get->email,
              'paket'           => NULL,
              'sales'           => $get->kode_sales,
              'ket'             => 0,
              'dispatch'        => 0,
              'status_approval' => 0,
              'approve_by'      => NULL,
              'approve_date'    => NULL,
              'decline_status'  => 0,
              'decline_by'      => NULL,
              'decline_date'    => NULL
          ];

          $getData = (object)$getData;
    }
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Return Order']
    ]);
  }

  public function approval_qc1($myir){
    date_default_timezone_set('Asia/Makassar');
    $cek = DB::table('psb_myir_wo')->where('myir',$myir)->get();
    if (count($cek)>0){
    $update = DB::table('psb_myir_wo')->where('myir',$myir)->update([
      'status_approval' => 1,
      'approve_by' => session('auth')->id_karyawan,
      'approve_date' => date('Y-m-d H:i:s')
    ]);
    } else {
// simpan dalam psb_myir_wo
          if (session('witel')=="KALSEL"){
              $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$myir);
          }
          else {
              $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$myir);
          };

          $get = json_decode($file);

          $getData = [
              'myir'            => $get->myir,
              'nama_pelanggan'  => $get->nama_pelanggan,
              'namaOdp'         => $get->odp,
              'sto'             => $get->sto,
              'pic_pelanggan'   => $get->alamat,
              'koor_pelanggan'  => $get->latitude.','.$get->longitude,
              'alamat_detail'   => $get->alamat,
              'layanan'         => $get->layanan,
              'psb'             => NULL,
              'koordinatOdp'    => $get->odp_latitude.','.$get->odp_longitude,
              'email'           => $get->email,
              'paket'           => NULL,
              'sales'           => $get->kode_sales,
              'status_approval' => 1,
              'approve_by' => session('auth')->id_karyawan,
              'approve_date' => date('Y-m-d H:i:s')
          ];

          $getData = (object)$getData;
          $photo = ['ktp','odp','rumah'];
          $id = DshrModel::simpanSalesQC($getData, $getData->sales);
          $this->send_to_telegramSales($id, $photo, '1');
    }
    // return redirect('/dshr/plasa-sales/list-belum-validasi/'. date('Y-m') )->with('alerts',[['type'=>'success', 'text'=>'Approval Sukses']]);
    return back()->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Approve Order']
    ]);
  }
  

  public function salesPlasaList(Request $req, $tgl){
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaBelumDispatch('ALL', $tgl, $nik);
    date_default_timezone_set('Asia/Makassar');

      $tglAwal  = date('Y-m').'-1';
      $tglAkhir = date('Y-m-d',strtotime("+1 days"));

      // if (session('witel')=="KALSEL"){
      //   $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      // $file = @file_get_contents($link);
      // } else {
      //   $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir;
      // $file = @file_get_contents($link);
      // }

      // $get  = json_decode($file);
      // if (count($get)){
      // foreach ($get as $g){
      //   $myir = explode('-', $g->myir);
      //   if (count($myir)>1){
      //       $myir = $myir[1];
      //   }
      //   else{
      //       $myir = $myir[0];
      //   };

      //   $getMyir = DB::table('psb_myir_wo')->where('myir',$myir)->where('status_approval','1')->first();
      //   $realSTO = explode("-", $g->odp);
      //   if (count($realSTO)>0){
      //     $sto = @$realSTO[1];
      //   } else {
      //     $sto = "";
      //   }
      //   if(count($getMyir)>0){
      //     $data = [
      //         "orderDate"       => $g->timestamp,
      //         "created_by"      => $g->kode_sales,
      //         "myir"            => $myir,
      //         "customer"        => $g->nama_pelanggan,
      //         "namaOdp"         => $g->odp,
      //         "sc"              => null,
      //         "ket"             => "0",
      //         "lokerJns"        => "myir",
      //         "sto"             => $sto,
      //         "picPelanggan"    => $g->alamat,
      //         "koorPelanggan"   => $g->latitude.','.$g->longitude,
      //        "lat"             => $g->latitude,
      //        "lon"             => $g->longitude,
              
      //         "picSales"        => null,
      //         "alamatLengkap"   => $g->alamat,
      //         "kelurahan"       => NULL,
      //         "channel"         => NULL,
      //         "layanan"         => $g->layanan,
      //         "sales_id"        => $g->kode_sales,
      //         "email"           => $g->email,
      //         "psb"             => null,
      //         "dispatch"        => 0,
      //         "ketMyirGangguan" => 0,
      //         "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
      //         "nama_tagihan"    => NULL,
      //         "tgl_lahir"       => NULL,
      //         "no_ktp"          => NULL,
      //         "terbit_ktp"      => NULL,
      //         "masa_ktp"        => NULL,
      //         "nama_ibu"        => NULL,
      //         "npwp"            => NULL,
      //         "paket_harga_id"  => NULL,
      //         "paket_sales_id"  => NULL,
      //         "ket_input"       => 2,
      //         "no_internet"     => NULL,
      //         "no_telp"         => NULL,
      //         "kcontack"        => NULL,
      //         "id_wo"           => NULL,
      //         "paket_harga"     => NULL,
      //         "paket_sales"     => NULL
      //     ];

      //     array_push($getData, (object)$data);
      //   }
      //   }
      // }
      $active = "dispatch";
      return view('dshr.plasasales.list',compact('active','getData', 'tgl'));
  }

  public function salesPlasaListAjax($ket, $tgl){
     date_default_timezone_set('Asia/Makassar');
      if ($ket==''){
          $filter = 'ALL';
      }
      else{
          $filter = $ket;
      };
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaBelumDispatch($ket, $tgl, $nik);

      $tglAwal  = date('Y-m').'-1';
      $tglAkhir = date('Y-m-d');

      if ($ket<>"plasa"){
        if (session('witel')=="KALSEL"){

            $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir);
        } else {
            $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhir);
        }// $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order?witel=KALSEL&tanggal_transaksi_awal=2019-11-01&tanggal_transaksi_akhir=2019-11-30");

        $get  = json_decode($file);
        foreach ($get as $g){
          $myir = explode('-', $g->myir);
          if (count($myir)>1){
              $myir = $myir[1];
          }
          else{
              $myir = $myir[0];
          };
$realSTO = explode("-", $g->odp);
        if (count($realSTO)>0){
          $sto = @$realSTO[1];
        } else {
          $sto = "";
        }
          $getMyir = DB::table('psb_myir')->where('myir',$myir)->first();
          if(count($getMyir)<>1){
              $data = [
                  "orderDate"       => $g->timestamp,
                  "created_by"      => $g->kode_sales,
                  "myir"            => $myir,
                  "customer"        => $g->nama_pelanggan,
                  "namaOdp"         => $g->odp,
                  "sc"              => null,
                  "ket"             => "0",
                  "lokerJns"        => "myir",
                  "sto"             => $sto,
                  "picPelanggan"    => $g->no_hp,
                  "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
                  "picSales"        => null,
                  "alamatLengkap"   => $g->alamat,
                  "kelurahan"       => NULL,
                  "channel"         => NULL,
                  "layanan"         => $g->layanan,
                  "sales_id"        => $g->kode_sales,
                  "email"           => $g->email,
                  "psb"             => null,
                  "dispatch"        => 0,
                  "ketMyirGangguan" => 0,
                  "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
                  "nama_tagihan"    => NULL,
                  "tgl_lahir"       => NULL,
                  "no_ktp"          => NULL,
                  "terbit_ktp"      => NULL,
                  "masa_ktp"        => NULL,
                  "nama_ibu"        => NULL,
                  "npwp"            => NULL,
                  "paket_harga_id"  => NULL,
                  "paket_sales_id"  => NULL,
                  "ket_input"       => 2,
                  "no_internet"     => NULL,
                  "no_telp"         => NULL,
                  "kcontack"        => NULL,
                  "id_wo"           => NULL,
                  "paket_harga"     => NULL,
                  "paket_sales"     => NULL
              ];

              array_push($getData, (object)$data);
            };
          }
      }

      return view('dshr.plasasales.listAjax',compact('getData', 'ket'));
  }

  private function handleFileUploadPlasaSales($request, $id, $photo)
  {
    foreach($photo as $name) {
      $input = 'photo_'.$name;
      if ($request->hasFile($input)) {
        // dd($input);
        $table = DB::table('hdd')->get()->first();
      $upload = $table->public;
        $path = public_path().'/'.$upload.'/plasaSales/'.$id.'/';
        if (!file_exists($path)) {
          if (!mkdir($path, 0770, true))
            return 'gagal menyiapkan folder foto evidence';
        }
        $file = $request->file($input);
        // $ext = $file->guessExtension();
        $ext = 'jpg';
        //TODO: path, move, resize
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());
          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        }
        catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence '.$name;
        }
      }
    }
  }

  public function cetakPlasa($id){
      $getData = DshrModel::getPlasaById($id);
      return view('dshr.plasasales.cetakPlasa',compact('getData'));
  }

  public function salesPlasaListDispatch(Request $req, $tgl){
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasa('ALL', $tgl, $nik);
      $active = "";
      return view('dshr.plasasales.listDispatch',compact('active','getData', 'tgl'));
  }

  public function salesPlasaListAjaxDispatch($ket, $tgl){
      if ($ket==''){
          $filter = 'ALL';
      }
      else{
          $filter = $ket;
      };
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasa($ket, $tgl, $nik);
      return view('dshr.plasasales.listAjaxDispatch',compact('getData', 'ket'));
  }

  public function dispatchForm($ket, $id){
      $getData = DshrModel::getMyirBelumDispatch($id);

      if($ket=="sales-onecall"){
          if (session('witel')=="KALSEL"){
              $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$id);
          }
          else {
              $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$id);
          };

          $get = json_decode($file);
          $realSTO = explode("-", $get->odp);
        if (count($realSTO)>0){
          $sto = @$realSTO[1];
        } else {
          $sto = "";
        }
          $getData = [
              'id'          => $get->myir,
              'sales_id'    => $get->kode_sales,
              'customer'    => $get->nama_pelanggan,
              'namaOdp'     => $get->odp,
              'sto'         => $sto,
              'layanan'         => $get->layanan,
              'orderDate'       => $get->timestamp,
              'koorPelanggan'   => $get->latitude.','.$get->longitude,
              'alamatLengkap'   => $get->alamat,
              'picPelanggan'    => $get->alamat,
              'dispatch'        => 0,
              'ket'             => 'sales-onecall'
          ];

          $getData = (object)$getData;

      };

      $jenis_layanan = DB::SELECT('SELECT jenis_layanan_id as id, jenis_layanan as text FROM dispatch_teknisi_jenis_layanan WHERE active_layanan = 1');

      $getRegu = DB::select('SELECT a.id_regu as id, a.uraian as text FROM regu a LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        where a.ACTIVE=1 AND b.Witel_New = "'.session('witel').'"');

      return view('dshr.plasasales.dispatchForm',compact('getData', 'id', 'getRegu', 'ket','jenis_layanan'));
  }

  public function getRegu($id){
      $data =  DB::table('regu')
            ->leftJoin('group_telegram','regu.mainsector','=','group_telegram.chat_id')
            ->select('regu.*', 'group_telegram.title', 'group_telegram.chat_id')
            ->where('regu.id_regu',$id)
            ->first();
      return $data = json_encode($data);
  }

  public function dispatchFormSimpan(Request $req){
      date_default_timezone_set('Asia/Makassar');
      $this->validate($req,[
          'id_regu' => 'required'
      ],[
          'id_regu.required'  => 'Regu Jangan Kosong'
      ]);

      if($req->ket == "sales-onecall"){
          // simpan dalam psb_myir_wo
          if (session('witel')=="KALSEL"){
              $file = file_get_contents("http://36.92.189.82/onecall/api/sales-order/".$req->myir);
          }
          else {
              $file = file_get_contents("http://10.128.16.66/onecall/api/sales-order/".$req->myir);
          };

          $get = json_decode($file);

          $getData = [
              'myir'            => $get->myir,
              'nama_pelanggan'  => $get->nama_pelanggan,
              'namaOdp'         => $get->odp,
              'sto'             => $get->sto,
              'pic_pelanggan'   => $get->alamat,
              'koor_pelanggan'  => $get->latitude.','.$get->longitude,
              'alamat_detail'   => $get->alamat,
              'layanan'         => $get->layanan,
              'psb'             => NULL,
              'koordinatOdp'    => $get->odp_latitude.','.$get->odp_longitude,
              'email'           => $get->email,
              'paket'           => NULL,
              'sales'           => $get->kode_sales
          ];

          $getData = (object)$getData;
          $id = DshrModel::simpanSales($getData, $getData->sales);
      }

      // insert into log status
      $auth = session('auth');
      $exists = DB::select('
        SELECT a.*,b.status_laporan, c.mainsector
          FROM dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        WHERE a.Ndem = ?
      ',[
        $req->myir
      ]);

      $status_laporan = 6;
      if (count($exists)>0){
          if ($exists[0]->status_laporan==4){
            $status_laporan = 4;
          }
      };

      DB::transaction(function() use($req, $auth, $status_laporan) {
         DB::table('psb_laporan_log')->insert([
           'created_at'      => DB::raw('NOW()'),
           'created_by'      => $auth->id_karyawan,
           'created_name'    => $auth->nama,
           'status_laporan'  => $status_laporan,
           'id_regu'         => $req->input('id_regu'),
           'catatan'         => "DISPATCH at ".date('Y-m-d'),
           'Ndem'            => $req->myir
         ]);
       });

       DB::transaction(function() use($req, $auth) {
         DB::table('dispatch_teknisi_log')->insert([
           'updated_at'          => DB::raw('NOW()'),
           'updated_by'          => $auth->id_karyawan,
           'tgl'                 => $req->input('tgl'),
           'id_regu'             => $req->input('id_regu'),
           'manja'               => NULL,
           'manja_status'        => NULL,
           'updated_at_timeslot' => NULL,
           'Ndem'                => $req->myir
         ]);
       });

      // reset status if redispatch
      if (count($exists)) {
        $data = $exists[0];
        DB::transaction(function() use($req, $data, $auth, $status_laporan) {
          DB::table('psb_laporan')
            ->where('id_tbl_mj', $data->id)
            ->update([
              'status_laporan' => $status_laporan
            ]);
            DB::table('psb_laporan_log')->insert([
               'created_at'      => DB::raw('NOW()'),
               'created_by'      => $auth->id_karyawan,
               'created_name'    => $auth->nama,
               'status_laporan'  => $status_laporan,
               'id_regu'         => $req->input('id_regu'),
               'catatan'         => "REDISPATCH",
               'Ndem'            => $req->myir,
               'psb_laporan_id'  => $data->id
             ]);
        });

        DB::transaction(function() use($req, $data, $auth) {
          DB::table('dispatch_teknisi')
            ->where('id', $data->id)
            ->update([
              'NO_ORDER'              => $req->myir,
              'updated_at'            => $req->tglRedispatch,
              'updated_by'            => $auth->id_karyawan,
              'tgl'                   => $req->tglRedispatch,
              'updated_at_timeslot'   => $req->input('timeslot'),
              'id_regu'               => $req->input('id_regu'),
              'jenis_layanan'         => $req->input('jenis_layanan'),
              'step_id'               => "1.0",
              'kordinatPel'           => $req->input('kordinatPel'),
              'alamatSales'           => $req->alamatSales,
              'jenis_order'           => "MYIR"
            ]);
        });
      }
      else {
       DB::transaction(function() use($req, $auth) {
          DB::table('dispatch_teknisi')->insert([
            'updated_at'          => DB::raw('NOW()'),
            'updated_by'          => $auth->id_karyawan,
            'tgl'                 => $req->input('tgl'),
            'id_regu'             => $req->input('id_regu'),
            'created_at'          => DB::raw('NOW()'),
            'updated_at_timeslot' => $req->input('timeslot'),
            'jenis_layanan'       => $req->input('jenis_layanan'),
            'Ndem'                => $req->myir,
            'NO_ORDER'            => $req->myir,
            'step_id'             => "1.0",
            'kordinatPel'         => $req->kordinatPel,
            'alamatSales'         => $req->alamatSales,
            'dispatch_by'         => 5,
            'jenis_order'         => "MYIR"
          ]);
        });
      }

      // ubah status dispacth tabel psb_myir_wo
      DB::table('psb_myir_wo')->where('myir',$req->myir)->update([
          'dispatch'  => '1',
      ]);
      // scbe::updateDispatch($req->myir);

      $id = $req->myir;
      exec('cd ..;php artisan sendTelegramSSC1 '.$id.' '.$auth->id_user.' > /dev/null &');
      return back()->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> mengirim order']
      ]);
      // return redirect('/dshr/plasa-sales/list-belum-dispatch/'. date('Y-m-d') )->with('alerts',[['type'=>'success', 'text'=>'Dispatch Sukses']]);

  }

  public function listWoBySales(Request $req,$sto,$tglAwal,$tglAkhir){
      $nik    = session('auth')->id_user;
      $tglAll = date('Y-m-d');
      if($req->has('tglAll')){
          $tglAll = $req->input('tglAll');
      };

      $level = session('auth')->level;

      if (session('auth')->level==2 || session('auth')->level==15 || session('auth')->level==46 || session('auth')->level==62){

          $dataDispatch = DshrModel:: getWoDispatchBelum($tglAll,$nik);
          date_default_timezone_set('Asia/Makassar');
          $tgl = date('Y-m');

          $getData = DshrModel::getAllSalesPlasaBelumDispatch_qc1('ALL','ALL', $tglAwal,$tglAkhir, $nik);

          // $tglAwal  = date('Y-m').'-01';
          $tglAkhirx = date('Y-m-d',strtotime("+1 days"));

          // if (session('witel') == "KALSEL"){
          //   $link = "http://36.92.189.82/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhirx;
          // $file = @file_get_contents($link);
          // } else {
          //   $link = "http://10.128.16.66/onecall/api/sales-order?witel=".session('witel')."&tanggal_transaksi_awal=".$tglAwal."&tanggal_transaksi_akhir=".$tglAkhirx;
          // $file = @file_get_contents($link);
          // }

          $link = "http://10.128.16.66/onecall/api/sales-order?witel=" . session('witel') . "&tanggal_transaksi_awal=" . $tglAwal . "&tanggal_transaksi_akhir=" . $tglAkhirx;
          $file = @file_get_contents($link);

          $get  = json_decode($file);
          if (count($get)){
          foreach ($get as $g){
            $myir = explode('-', $g->myir);
            if (count($myir)>1){
                $myir = $myir[1];
            }
            else{
                $myir = $myir[0];
            };
            $getMyir = DB::SELECT('select * from psb_myir_wo where myir = "'.$myir.'" AND (status_approval = 1 OR sc <> "")');
            $realSTO = explode("-", $g->odp);
            if (count($realSTO)>0){
              $sto = @$realSTO[1];
            } else {
              $sto = "";
            }
            if(count($getMyir)==0){
              $data = [
                  "orderDate"       => $g->timestamp,
                  "created_by"      => $g->kode_sales,
                  "myir"            => $myir,
                  "customer"        => $g->nama_pelanggan,
                  "namaOdp"         => $g->odp,
                  "sc"              => null,
                  "ket"             => "0",
                  "lokerJns"        => "myir",
                  "sto"             => $sto,
                  "picPelanggan"    => $g->no_hp,
                  "koorPelanggan"   => $g->latitude.','.$g->longitude,
              "lat"             => $g->latitude,
              "lon"             => $g->longitude,
              
                  "picSales"        => null,
                  "alamatLengkap"   => $g->alamat,
                  "kelurahan"       => NULL,
                  "channel"         => NULL,
                  "layanan"         => $g->layanan,
                  "sales_id"        => $g->kode_sales,
                  "email"           => $g->email,
                  "psb"             => null,
                  "dispatch"        => 0,
                  "ketMyirGangguan" => 0,
                  "koorOdp"         => $g->odp_latitude.'.'.$g->odp_longitude,
                  "nama_tagihan"    => NULL,
                  "tgl_lahir"       => NULL,
                  "no_ktp"          => NULL,
                  "terbit_ktp"      => NULL,
                  "masa_ktp"        => NULL,
                  "nama_ibu"        => NULL,
                  "npwp"            => NULL,
                  "paket_harga_id"  => NULL,
                  "paket_sales_id"  => NULL,
                  "ket_input"       => 2,
                  "no_internet"     => NULL,
                  "no_telp"         => NULL,
                  "kcontack"        => NULL,
                  "id_wo"           => NULL,
                  "paket_harga"     => NULL,
                  "paket_sales"     => NULL
              ];

              array_push($getData, (object)$data);
            }
            }
          }
          $pivot = array();
          $belum_dispatch = array();
          $sudah_dispatch = array();
          $close_dispatch = array();
          $getSto = DB::table('maintenance_datel')->where('witel',session('witel'))->get();
          foreach ($getSto as $result){
            $pivot[$result->sto] = 0;
            $belum_dispatch[$result->sto] = 0;
            $sudah_dispatch[$result->sto] = 0;
            $close_dispatch[$result->sto] = 0;
          }
          foreach ($getData as $result){
            if ($result->sto<>""){
              @$pivot[$result->sto] += 1;
            }
          }
          $getDataBelumDispatch = DshrModel::getAllSalesPlasaBelumDispatch('ALL', 'ALL',$tglAwal, $tglAkhir, $nik);
          foreach ($getDataBelumDispatch as $result){
            if ($result->sto<>""){
              @$belum_dispatch[$result->sto] += 1;
            }
          }
          $getDataSudahDispatch = DshrModel::getSudahDispatch(session('witel'),$tglAwal,$tglAkhir);
          foreach ($getDataSudahDispatch as $result){
              @$sudah_dispatch[$result->sto] = $result->jumlah;
          }

          $getDataCloseDispatch = DshrModel::getCloseDispatch(session('witel'),$tglAwal,$tglAkhir);
          foreach ($getDataCloseDispatch as $result){
            @$close_dispatch[$result->sto] = $result->jumlah;
          }

          $active = "";
          return view('dshr.plasasales.listInputAll',compact('tglAwal','tglAkhir','close_dispatch','sudah_dispatch','belum_dispatch','getSto','pivot','tgl','active','tglAll','datel','dataDispatch'));
      }
      elseif(session('auth')->level==61 || session('auth')->level==59){
          $bulan            = date('Y-m', strtotime($tglAll));
          $woHi             = DshrModel::getAllWoBySales($tglAll, $nik);
          $jumlahProgress   = DshrModel::getAllWoBySalesProses($bulan, $nik);

          return view('dshr.plasasales.listInputAllSales',compact('tglAll','woHi','jumlahProgress', 'level'));
      }

  }

  public function listWoBySalesDetailToday($tgl){
      $nik  = session('auth')->id_user;
      $dataUndispatch = DshrModel:: getAllSalesPlasaBelumDispatch('ALL', $tgl, $nik);
      $dataDispatch   = DshrModel:: getAllSalesPlasa('ALL', $tgl, $nik);

      $jumlahUndispatch = count($dataUndispatch);

      return view('dshr.plasasales.listWoTodayDetail',compact('dataUndispatch', 'dataDispatch','jumlahUndispatch'));
  }

  public function listWoBySalesDetailProgress($tgl, $ket){
      $nik     = session('auth')->id_user;
      $getData = DshrModel::getAllSalesPlasaProgress($ket, $tgl, $nik);
      return view('dshr.plasasales.listWoProgress',compact('getData'));

  }

  public function pencarianForm(Request $req){
      $cari = '';
      $dataUnDispatch = array();
      $dataDispatch = array();
      if ($req->has('cari')){
          $cari = $req->input('cari');
          $nik = session('auth')->id_user;
          $dataUnDispatch = DshrModel::getAllSalesPlasaCariUndispacth($cari, $nik);
          $dataDispatch   = DshrModel::getAllSalesPlasaCariDispatch($cari, $nik);
      };

      return view('dshr.plasasales.pencarianForm', compact('dataUnDispatch', 'dataDispatch'));
  }

  public function inputSc($sc){
      $data = DB::table('psb_myir_wo')->where('myir',$sc)->first();
      return view('dshr.plasasales.inputSc',compact('data'));
  }

  public function saveSc(Request $req, $id){
      $this->validate($req,[
          'sc'          => 'required|numeric',
          'no_inet'     => 'required|numeric'
      ],[
          'sc.required'      => 'No. SC Jangan Kosong',
          'sc.numeric'       => 'Diisi Angka',
          'no_inet.required' => 'No. Internet Jangan Kosonng',
          'no_inet.numeric'  => 'Disi Angka'
      ]);

      // ubah di table psb_myir_wo
      DB::table('psb_myir_wo')->where('myir',$id)->update([
          'sc'    => $req->sc,
          'ket'   => '1',
          'no_internet'   => $req->no_inet,
          'no_telp'       => $req->no_telp
      ]);

      // ubah table dispatch
      DB::table('dispatch_teknisi')->where('Ndem',$id)->update([
          'Ndem'        => $req->sc,
          'NO_ORDER'    => $req->sc,
          'dispatch_by' => NULL
      ]);

      $data = DB::table('psb_laporan_log')->where('Ndem',$req->sc)->first();
      if (count($data)==0){
        $getData = DB::table('psb_laporan_log')->where('Ndem',$id)->orderBy('psb_laporan_log_id','DESC')->first();
        DB::table('psb_laporan_log')->insert([
            'id_regu'   => $getData->id_regu,
            'Ndem'      => $req->sc,
            'psb_laporan_id' => $getData->psb_laporan_id,
            'status_laporan' => $getData->status_laporan,
            'catatan'        => 'SINKRON SC DARI MYIR-'.$getData->Ndem.' KE SC '.$req->sc,
            'created_by'     => session('auth')->id_user,
            'created_name'   => session('auth')->nama,
            'mttr'           => '0',
            'penyebabId'     => NULL,
            'action'         => 0
        ]);
      };

      // exec('cd ..;php artisan sendTelegramSSC1SyncSc '.$dataStartclik->orderId.' '.$auth->id_user.' > /dev/null &');
      return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'SC Berhasil Ditambahkan']]);
  }

  public function addSc($id){
    $data = DB::table('psb_myir_wo')->where('id',$id)->first();
    return view('dshr.plasasales.addSc',compact('data'));
  }

  public function addsaveSc(Request $req, $id){
    date_default_timezone_set('Asia/Makassar');
    $tglAwal = date('Y-m-01');
    $tglAkhir = date('Y-m-d');
    $this->validate($req,[
        'sc'          => 'required|numeric',
        'no_inet'     => 'required|numeric'
    ],[
        'sc.required'      => 'No. SC Jangan Kosong',
        'sc.numeric'       => 'Diisi Angka',
        'no_inet.required' => 'No. Internet Jangan Kosonng',
        'no_inet.numeric'  => 'Disi Angka'
    ]);

    DB::table('psb_myir_wo')->where('id',$id)->update([
        'sc'            => $req->sc,
        'sc_n'          => $req->sc,
        'no_internet'   => $req->no_inet,
        'no_telp'       => $req->no_telp
    ]);

    return redirect('/belum_dispatch/plasa/ALL/'.$tglAwal.'/'.$tglAkhir)->with('alerts',[['type'=>'success', 'text'=>'SC Berhasil Ditambahkan']]);
  }

  public function editSC_MYIR($sc)
  {
      $data = DB::table('psb_myir_wo')->where('myir', $sc)->first();

      return view('dshr.plasasales.editSCMyir',compact('data'));
  }

  public function saveeditSC_MYIR(Request $req, $id)
  {
      $this->validate($req,[
          'sc'               => 'required|numeric'
      ],[
          'sc.required'      => 'No. SC Jangan Kosong',
          'sc.numeric'       => 'Diisi Angka'
      ]);

      DB::table('psb_myir_wo')->where('myir',$id)->update([
          'sc'            => $req->sc,
          'sc_n'          => $req->sc,
          'sc_lama'       => $req->sc_lama,
          'editSC_by'     => session('auth')->id_user,
          'no_internet'   => $req->no_inet,
          'no_telp'       => $req->no_telp
      ]);

      return redirect('/dshr/plasa-sales/edit-sc-dt/'.$req->sc_lama)->with('alerts',[['type'=>'success', 'text'=>'Sukses Tahap 1, silahkan lanjut Tahap 2 ...']]);
  }


  public function editSC_MYIR_DT($Ndem)
  {
      $data = DB::table('dispatch_teknisi')->where('Ndem', $Ndem)->first();

      return view('dshr.plasasales.editSCMyirDT',compact('data'));
  }

  public function saveeditSC_MYIR_DT(Request $req, $Ndem)
  {
      $this->validate($req,[
          'Ndem'             => 'required|numeric'
      ],[
          'Ndem.required'      => 'No. SC Jangan Kosong',
          'Ndem.numeric'       => 'Diisi Angka'
      ]);

      DB::table('dispatch_teknisi')->where('id',$req->id)->update([
          'id'              => $req->id,
          'id_regu'         => $req->id_regu,
          'Ndem'            => $req->Ndem,
          'NO_ORDER'        => $req->Ndem,
          'tgl'             => date('Y-m-d'),
          'jenis_order'     => "SC"
      ]);
      return redirect('/scbe/search')->with('alerts',[['type'=>'success', 'text'=>'SC Berhasil Dirubah...']]);
  }

  public function deleteMYIR($myir, $id){
    date_default_timezone_set('Asia/Makassar');

    $auth = session('auth')->level;
    if ( $auth==2 || $auth==15 ){
      DB::table('psb_myir_wo_log')->insert([
             'myir'       => $myir,
             'deleted_by' => session('auth')->id_karyawan,
             'log'        => 'MYIR Success Deleted By '.session('auth')->id_karyawan,
             'updated_at' => DB::raw('NOW()')
          ]);

      DB::table('psb_myir_wo')
          ->where('id', $id)
          ->delete();

          return back()->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menghapus MYIR . . .']
          ]);
    } else {
      echo "MINTA IZIN DULU KALO MAU HAPUS WKWK";
    }
  }

  public function inboxPlasa(){
    $datetime = date('Y-m');
    $data = DB::SELECT('
      SELECT
      dt.id as id_dt,
      pmw.id as id_pmw,
      dt.tgl as tgl_disp,
      r.uraian as tim,
      gt.title as sektor,
      pmw.myir as myir,
      pmww.myir as myir1,
      dps.orderId as sc,
      dpss.orderId as sc1,
      pls.laporan_status as status,
      pl.catatan as catatan,
      pmw.status_deposit as sts_depo,
      pmw.updated_deposit_by as udb,
      pmw.layanan,
      pl.modified_at
      FROM dispatch_teknisi dt
      LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.sc
      LEFT JOIN psb_myir_wo pmww ON dt.Ndem = pmww.myir
      LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
      LEFT JOIN Data_Pelanggan_Starclick dpss ON dt.Ndem = dpss.orderId
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
      pmw.ket_input = "0" AND
      pl.status_laporan IN ("1","74","78","4") AND
      (DATE(dt.tgl) LIKE "'.$datetime.'%")
      ORDER BY dt.tgl ASC
    ');

    return view('dshr.plasasales.inboxPlasa', compact('datetime', 'data'));
  }

  public function statusDeposit($id)
  {
    $data = DB::table('psb_myir_wo')->where('id',$id)->first();
    return view('dshr.plasasales.statusDeposit',compact('data'));
  }
  
  public function saveStatusDeposit(Request $req, $id)
  {
    date_default_timezone_set('Asia/Makassar');
    $this->validate($req,
    [
      'status_deposit.required'      => 'Kolom Manja Kosong !'
    ]);
    DB::table('psb_myir_wo')->where('id',$id)->update([
      'status_deposit'      => $req->status_deposit,
      'updated_deposit_by'  => session('auth')->id_user
    ]);
    return redirect('/dshr/plasa-sales/order/inbox')->with('alerts',[['type'=>'success', 'text'=>'Success Update Status Deposit ...']]);
  }

  public function doorToDoorDashboard()
  {
    $start_date = Input::get('start_date');
    $end_date = Input::get('end_date');

    if ($start_date == null)
    {
      $start_date = date('Y-m-01');
    }

    if ($end_date == null)
    {
      $end_date = date('Y-m-d');
    }

    $data = DshrModel::doorToDoorDashboard($start_date, $end_date);

    return view('dshr.doorToDoorDashboard', compact('start_date', 'end_date', 'data'));
  }

  public function doorToDoorMyOrder()
  {
    $date = Input::get('date');
    
    if ($date == null)
    {
      $date = date('Y-m-d');
    }

    $data = DshrModel::doorToDoorMyOrder($date);

    return view('dshr.doorToDoorMyOrder', compact('data', 'date'));
  }

  public function doorToDoorAssign()
  {
    $kelurahan = Input::get('kelurahan');

    $list_sales = DB::table('1_2_employee AS emp')
    ->leftJoin('user AS u', 'emp.nik', '=', 'u.id_karyawan')
    ->where([
      ['emp.ACTIVE', 1],
      ['emp.status_telegram', 1],
      ['u.level', 61]
    ])
    ->orderBy('emp.last_update', 'DESC')
    ->get();

    $list_kelurahan = DB::table('odp_uim_ihld')->where('validasi_kelurahan', '!=', "")->select('validasi_kelurahan')->groupBy('validasi_kelurahan')->orderBy('validasi_kelurahan', 'ASC')->get();

    $list_odp = DB::table('odp_uim_ihld')->where('validasi_kelurahan', $kelurahan)->get();

    return view('dshr.doorToDoorAssign', compact('list_sales', 'list_kelurahan', 'kelurahan', 'list_odp'));
  }

  public function doorToDoorAssignSave(Request $req)
  {
    $auth = session('auth');
    $dtm = date('Y-m-d H:i:s');

    $id = DB::table('dshr_doortodoor_assign')->insertGetId([
      'id_sales'    => $req->input('id_sales'),
      'list_odp'    => json_encode($req->input('list_odp')),
      'catatan'     => $req->input('catatan'),
      'date_assign' => $req->input('date_assign'),
      'assign_by'   => session('auth')->id_karyawan,
      'assign_dtm'  => date('Y-m-d H:i:s')
    ]);

    $sales = DB::table('1_2_employee')->where('nik', $req->input('id_sales'))->first();

    $message = "<b>=== Door To Door Sales ===</b>\n";
    $message .= "HERO : $auth->nama ($auth->id_karyawan)\n";
    $message .= "Sales : $sales->nama ($sales->nik)\n\n";
    foreach ($req->input('list_odp') as $numb => $odp)
    {
      DB::table('dshr_doortodoor_log')->insert([
        'assign_id'   => $id,
        'odp_name'    => $odp,
        'assign_date' => $req->input('date_assign')
      ]);
      
      $number = ++$numb;
      $check_odp = DB::table('odp_uim_ihld')->where('odp_name', $odp)->first();
      $message .= "ODP $number : $odp ($check_odp->latitude, $check_odp->longitude)\n";
    }
    $message .= "\nCatatan oleh HERO : \n<i>$req->catatan</i>\n\n";
    // $message .= "<a href='https://biawak.tomman.app/dshr/door-to-door/assign/update/$id'>Update Disini!</a>\n\n";
    $message .= "Semangat Rekan!\n<i>$dtm</i>";

    $lis_chat_id[] = '-1001846324221';
    // $lis_chat_id[] .= $sales->chat_id;
    $lis_chat_id[] .= '401791818';

    foreach ($lis_chat_id as $chat_id)
    {
      Telegram::sendMessage([
        'chat_id'    => $chat_id,
        'parse_mode' => 'html',
        'text'       => $message,
      ]);
    }

    return redirect('/dshr/door-to-door/assign')
    ->with('alerts',[[
      'type'=>'success', 'text'=>'Berhasil Kirim Order ke Sales'
    ]]);
  }

  public function updateDoorToDoorAssign($id)
  {
    $data = DB::table('dshr_doortodoor_assign AS dda')
    ->leftJoin('1_2_employee AS emp', 'dda.assign_by', '=', 'emp.nik')
    ->select('emp.nama AS assign_name', 'dda.*')
    ->where('dda.id_assign', $id)
    ->first();

    return view('dshr.updateDoorToDoorAssign', compact('id', 'data'));
  }

  public function updateDoorToDoorAssignSave(Request $req, $id)
  {
    $auth = session('auth');
    $dtm = date('Y-m-d H:i:s');

    $data = DB::table('dshr_doortodoor_assign AS dda')
    ->leftJoin('1_2_employee AS emp', 'dda.assign_by', '=', 'emp.nik')
    ->select('emp.nama AS assign_name', 'emp.chat_id', 'dda.*')
    ->where('dda.id_assign', $id)
    ->first();

    $lis_chat_id[] = '-1001846324221';
    $lis_chat_id[] .= $data->chat_id; 

    $sales = DB::table('1_2_employee')->where('nik', $data->id_sales)->first();

    $lis_chat_id[] .= $sales->chat_id; 

    DB::table('dshr_doortodoor_assign')->where('id_assign', $id)->update([
      'brosur'      => $req->brosur,
      'prospek'     => $req->prospek,
      'inputan'     => $req->inputan,
      'updated_by'  => $auth->id_karyawan,
      'updated_dtm' => date('Y-m-d H:i:s')
    ]);

    if ($req->hasFile('evidence'))
    {
      $path = public_path().'/upload4/doortodoorassign/';
      $file = $req->file('evidence');
      $ext = 'jpg';
      try {
        $moved = $file->move("$path", "$id.$ext");

        $img = new \Imagick($moved->getRealPath());

        $img->scaleImage(100, 150, true);
        $img->writeImage("$path/$id-th.$ext");
      }
      catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan foto doortodoorassign '.$id;
      }

      $caption = "HERO : $data->assign_name ($data->assign_by)\n";
      $caption .= "Sales : $sales->nama ($data->id_sales)\n\n";
      $caption .= "List ODP : \n";
      foreach (json_decode($data->list_odp) as $numb => $odp)
      {
        $number = ++$numb;
        $caption .= "ODP $number : $odp\n";
      }
      $caption .= "\nCatatan oleh HERO : $data->catatan\n";
      $caption .= "Brosur : $req->brosur\n";
      $caption .= "Prospek : $req->prospek\n";
      $caption .= "Inputan : $req->inputan\n\n<i>$dtm</i>";

      foreach ($lis_chat_id as $chat_id)
      {
        Telegram::sendPhoto([
          'chat_id'    => $chat_id,
          'parse_mode' => 'html',
          'caption'    => $caption,
          'photo'      => public_path().'/upload4/doortodoorassign/'.$id.'.jpg'
        ]); 
      }
    }

    return redirect('/dshr/door-to-door/assign/update/'.$id)
    ->with('alerts',[[
      'type'=>'success', 'text'=>'Berhasil Update!'
    ]]);
  }

  public function dashboard_new_sales()
  {
    $startDate = Input::get('startDate') ?? date('Y-m-01');
    $endDate = Input::get('endDate') ?? date('Y-m-d');

    $get_sto = DB::table('maintenance_datel')
    ->select('sto AS id', 'sto AS text')
    ->where('witel', 'KALSEL')
    ->get();

    $get_jenis_layanan = DB::table('dispatch_teknisi_jenis_layanan')
    ->select('jenis_layanan_id AS id', 'jenis_layanan AS text')
    ->where('active_layanan', 1)
    ->whereNotIn('jenis_transaksi', ['ONT_PREMIUM', 'CABUT_NTE'])
    ->get();

    $dashboard_laporan = DshrModel::data_new_sales('dashboard_laporan', $startDate, $endDate);
    $dashboard_dispatch = DshrModel::data_new_sales('dashboard_dispatch', $startDate, $endDate);
    $data_belum_terdispatch = DshrModel::data_new_sales('data_belum_terdispatch', $startDate, $endDate);
    $data_sudah_terdispatch = DshrModel::data_new_sales('data_sudah_terdispatch', $startDate, $endDate);

    return view('dshr.dashboard_new_sales', compact('startDate', 'endDate', 'get_sto', 'get_jenis_layanan', 'dashboard_laporan', 'dashboard_dispatch', 'data_belum_terdispatch', 'data_sudah_terdispatch'));
  }

  public function save_new_sales(Request $req)
  {
    DshrModel::save_new_sales($req);
    
    return redirect('/dshr/new-sales/dashboard')
    ->with('alerts',[[
      'type'=>'success', 'text'=>'Success Input!'
    ]]);
  }

}