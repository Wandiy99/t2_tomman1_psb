<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\HelpdeskModel;
use App\DA\BotModel;
use DB;
use Telegram;
use Validator;

date_default_timezone_set('Asia/Makassar');

class HelpdeskController extends Controller
{

	public function dashboard($date)
	{	
		$get_manja = HelpdeskModel::get_manja($date);
		$get_config = HelpdeskModel::get_config($date);
		$get_fallout = HelpdeskModel::get_fallout($date);
		$get_mediacarring = HelpdeskModel::get_mediacarring($date);

	return view('helpdesk.dashboard',compact('get_manja', 'get_config', 'get_fallout', 'get_mediacarring', 'date'));
	}

	public function get_fallout($hd,$date){
		if ($hd == "ALL"){
			$whereHD = '';
		  } else {
			$whereHD = 'AND hi.FUP_BY = "'.$hd.'"';
		  }

		$query = DB::SELECT('
		SELECT
		*
		FROM helpdesk_inbox hi
		WHERE
		hi.STATUS = "1" AND
		(DATE(hi.TGLOPEN) LIKE "'.$date.'%")
		'.$whereHD.'
		ORDER BY hi.TGLOPEN DESC
		');

		return view('helpdesk.prod_fallout',compact('hd','date','query'));
	}

  public function close($tgl){
    $getInbox = DB::SELECT('
      SELECT 
      a.*,
      b.STATUS,
      d.uraian,
      c.jenis_layanan,
	  e.nama as request_by
      FROM 
        helpdesk_inbox a
      LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
      LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
      LEFT JOIN regu d ON c.id_regu = d.id_regu
	  LEFT JOIN 1_2_employee e ON a.TELEGRAM_ID = e.chat_id
      WHERE 
        a.STATUS = "1" AND 
        a.TGLUPDATE LIKE "'.$tgl.'%"
      ');
  
  
  if (session('auth')->level==10) {
    $layout = 'tech_layout';
    } else {
      $layout = 'layout';
    }
    // $getInbox = DB::table('helpdesk_inbox')->where('STATUS','<>',1)->get();
    return view('helpdesk.inbox',compact('getInbox','layout'));
  }

  public function log($ID){
    $query = DB::SELECT('
      SELECT 
      a.*,
      b.STATUS,
      c.SC
      FROM 
        helpdesk_inbox_log a  
      LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
      LEFT JOIN helpdesk_inbox c ON a.INBOX_ID = c.ID 
      where
      a.INBOX_ID = "'.$ID.'"
      ');
    if (session('auth')->level==10) {
    $layout = 'tech_layout';
    } else {
      $layout = 'layout';
    }
    return view('helpdesk.log',compact('query','layout'));
  }

  public function inbox()
  {
	switch (session('auth')->level) {
		case '10':
				$layout = 'new_tech_layout';
			break;
		
		default:
				$layout = 'layout';
			break;
	}

  	$getInbox = DB::SELECT('
  		SELECT 
			a.*,
			b.STATUS,
			d.uraian,
			c.jenis_layanan,
			e.nama as request_by
  		FROM helpdesk_inbox a
  		LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
  		LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
  		LEFT JOIN regu d ON c.id_regu = d.id_regu
		LEFT JOIN 1_2_employee e ON a.TELEGRAM_ID = e.chat_id
  		WHERE 
			a.FUP_BY IS NULL AND
			a.DELETEX = 0 AND
			(DATE(a.TGLOPEN) = "'.date('Y-m-d').'")
		ORDER BY a.TGLOPEN ASC
	');

	// if (in_array(session('auth')->id_karyawan, ["20981020", "20941067"]))
	// {
	// 	$fup_by = 'a.FUP_BY IS NOT NULL';
	// } else {
	// 	$fup_by = 'a.FUP_BY = "' . session('auth')->id_karyawan . '"';
	// }

	$getBooked = DB::SELECT('
  		SELECT 
			a.*,
			b.STATUS,
			d.uraian,
			c.jenis_layanan,
			e.nama as request_by
  		FROM helpdesk_inbox a
  		LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
  		LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
  		LEFT JOIN regu d ON c.id_regu = d.id_regu
		LEFT JOIN 1_2_employee e ON a.TELEGRAM_ID = e.chat_id
  		WHERE 
			a.STATUS <> "1" AND 
			(DATE(a.TGLUPDATE) = "'.date('Y-m-d').'") AND
			a.FUP_BY IS NOT NULL
		ORDER BY a.TGLUPDATE DESC
	');

	$getCloosed = DB::SELECT('
		SELECT 
			a.*,
			b.STATUS,
			d.uraian,
			c.jenis_layanan,
			e.nama as request_by
		FROM helpdesk_inbox a
		LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
		LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
		LEFT JOIN regu d ON c.id_regu = d.id_regu
		LEFT JOIN 1_2_employee e ON a.TELEGRAM_ID = e.chat_id
		WHERE 
			a.STATUS = "1" AND 
			(DATE(a.TGLUPDATE) = "'.date('Y-m-d').'")
		ORDER BY a.TGLUPDATE DESC
	');
  	return view('helpdesk.inbox',compact('getInbox','getBooked','getCloosed','layout'));
  }

  public static function monitorOrderFallout()
  {
	  $get_data = DB::SELECT('
	  	SELECT 
			gt.title as sektor,
			a.SC,
			dps.orderStatusId,
			dps.orderStatus,
			dps.orderDate,
			b.STATUS,
			d.uraian as tim,
			a.TGLUPDATE,
			a.FUP_BY,
			a.FUP_NAME,
			TIMESTAMPDIFF(MINUTE,a.TGLUPDATE,NOW()) as selisih_menit
		FROM helpdesk_inbox a
		LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
		LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
		LEFT JOIN Data_Pelanggan_Starclick dps ON c.Ndem = dps.orderId
		LEFT JOIN regu d ON c.id_regu = d.id_regu
		LEFT JOIN `1_2_employee` e ON a.TELEGRAM_ID = e.chat_id
		LEFT JOIN group_telegram gt ON d.mainsector = gt.chat_id
		WHERE
			dps.orderStatusId <> "1500" AND
			a.DELETEX = 0 AND
			a.STATUS <> "1" AND
			TIMESTAMPDIFF( MINUTE , a.TGLUPDATE, "' . date('Y-m-d H:i:s') . '") >= 15
		GROUP BY a.SC
		ORDER BY selisih_menit
	  ');

	  if (count($get_data) > 0)
	  {

			$messaggio   = "<b>MONITORING ORDER FALLOUT PROV [" . date('j F Y H:i:s') . "]</b>\n\n";

			foreach ($get_data as $data)
			{
				$messaggio  .= $data->sektor . "\n";
				if ($data->FUP_NAME <> null)
				{
					$fup_name = $data->FUP_NAME;
				} else {
					$fup_name = '-';
				}
				$messaggio  .= "[ " . $data->selisih_menit . " Menit - " . $data->STATUS . " ] <b>" . $data->SC . "</b> // " . $data->orderStatus . " // " . $data->tim . " // <b>" . $fup_name . "</b> \n\n";
			}

			Telegram::sendMessage([
				'chat_id' => "-1001661694038",
				'parse_mode' => 'html',
				'text' => $messaggio
			]);
	  }

		print_r("Finish! \n");
  }

  public function hd_config()
  {
	

	if (session('auth')->level == 10) {
	$layout = 'new_tech_layout';
	} else {
		$layout = 'layout';
	}

	$title = "INBOX HELPDESK CONFIG ASSURANCE";
	if (in_array(session('auth')->level,[2,10])) {
		$where = 'hai.helpdesk_by IS NOT NULL AND';
	} else {
		$where = 'hai.helpdesk_by = "'.session('auth')->id_karyawan.'" AND';
	}
	$data = DB::SELECT('
		SELECT
		hai.*,
		his.STATUS,
		emp.nama,
		empp.nama as request_by
		FROM helpdesk_assurance_inbox hai
		LEFT JOIN 1_2_employee emp ON hai.helpdesk_by = emp.nik
		LEFT JOIN 1_2_employee empp ON hai.telegram_id = empp.chat_id
		LEFT JOIN helpdesk_inbox_status his ON hai.status_order = his.ID
		WHERE
		hai.status_order = 0
		ORDER BY hai.tgl_order ASC
	');
	$data_history = DB::SELECT('
		SELECT
		hai.*,
		his.STATUS,
		emp.nama,
		empp.nama as request_by
		FROM helpdesk_assurance_inbox hai
		LEFT JOIN 1_2_employee emp ON hai.helpdesk_by = emp.nik
		LEFT JOIN 1_2_employee empp ON hai.telegram_id = empp.chat_id
		LEFT JOIN helpdesk_inbox_status his ON hai.status_order = his.ID
		WHERE
		'.$where.'
		hai.status_order NOT IN (0,1,7)
		ORDER BY hai.tgl_order ASC
	');

	$get_hd = DB::SELECT('SELECT helpdesk_by as id, helpdesk_name as text FROM helpdesk_assurance_inbox WHERE helpdesk_by IS NOT NULL GROUP BY helpdesk_by');

	$get_status = DB::SELECT('SELECT his.ID as id, his.STATUS as text FROM helpdesk_inbox_status his WHERE his.ID <> 7');

	$get_command = DB::select('SELECT command_id as id, command_text as text FROM helpdesk_assurance_command');

	$check_to = DB::table('helpdesk_assurance_inbox')->where('helpdesk_by', session('auth')->id_karyawan)->where('status_order','<>', 1)->get();

	return view('helpdesk.config',compact('layout','data','data_history','title','get_hd','get_status','check_to','get_command'));
	}

	public function replyConfig($id)
	{
		
		$data = DB::table('helpdesk_assurance_inbox')->leftJoin('helpdesk_inbox_status','helpdesk_assurance_inbox.status_order','=','helpdesk_inbox_status.ID')->where('id_hai', $id)->first();

		if ($data->helpdesk_by == NULL)
		{
			DB::transaction(function() use($id) {
				DB::table('helpdesk_assurance_inbox_log')->insert([
					'id_order'			=> $id,
					'order_status'		=> 6,
					'status_log'		=> "BOOKED",
					'catatan'			=> NULL,
					'hd_by'				=> session('auth')->nama,
					'updated_at'		=> DB::raw('NOW()')
				]);
				DB::table('helpdesk_assurance_inbox')->where('id_hai', $id)->update([
					'status_order'		=> 6,
					'helpdesk_by'		=> session('auth')->id_user,
					'helpdesk_name'		=> session('auth')->nama,
					'helpdesk_reply'	=> NULL,
					'tgl_close'			=> DB::raw('NOW()')
				]);
			});
		} elseif ($data->helpdesk_by <> session('auth')->id_user) {
			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Maaf ID Order '.$id.' Sudah di Take Order oleh '.$data->helpdesk_name.'']
			]);
		}

		$msg = "<b>ID Order :</b> ".$data->id_hai."\n<b>Command :</b> ".$data->command."\n<b>Status :</b> ".$data->STATUS."\n<b>Balasan :</b>\n\n".$data->helpdesk_reply."\n\nSalam <b>".session('auth')->nama."</b>";

		BotModel::sendMessageReplyTommanGO($data->chat_id,$msg,$data->message_id);

		return redirect('helpdesk/assurance/config')->with('alerts', [
			['type' => 'success', 'text' => 'Success Take Order']
		]);
	}

	public function saveConfig(Request $req, $id)
	{
		// dd($req->input(),$id);
		

		$this->validate(
			$req,
			[
				'status_order.required'      => 'Status Jangan Kosong !'
			]
		);

		switch ($req->command) {
			case 'ganti_ont':
				if (str_contains($req->sn_lama, ' ') || str_contains($req->sn_baru, ' '))
				{
					return back()->with('alerts', [
						['type' => 'danger', 'text' => 'Mohon cek kembali penulisan SN Lama / SN Baru']
					]);
				}
				break;
		}

		//cek helpdesk login
		$cek_login = DB::table('1_2_employee')->where('nik', session('auth')->id_user)->first();
		if ($cek_login->chat_id == NULL) {
			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Login ke Bot Telegram dulu Rekan']
			]);
		}

		// return log
		$log = DB::table('helpdesk_assurance_inbox_log')->where('id_order', $id)->orderBy('updated_at', 'asc')->first();

		if ($log == NULL || $log->hd_by == session('auth')->nama || $req->input_transfer == 'YES')
		{
			
			DB::transaction(function() use($id,$req) {
				DB::table('helpdesk_assurance_inbox')->where('id_hai', $id)->update([
					'status_order'		=> $req->status_order,
					'helpdesk_by'		=> session('auth')->id_user,
					'helpdesk_name'		=> session('auth')->nama,
					'helpdesk_reply'	=> $req->helpdesk_reply,
					'command'			=> $req->command,
					'no_tiket'			=> $req->no_tiket,
					'inet'				=> $req->inet,
					'sn_lama'			=> $req->sn_lama,
					'sn_baru'			=> $req->sn_baru,
					'mac_stb'			=> $req->mac_stb,
					'tgl_close'			=> DB::raw('NOW()')
				]);
			});

			$data = DB::table('helpdesk_assurance_inbox')->leftJoin('helpdesk_inbox_status','helpdesk_assurance_inbox.status_order','=','helpdesk_inbox_status.ID')->where('id_hai', $id)->first();
			$teknisi = DB::table('1_2_employee')->where('chat_id', $data->chat_id)->first();

			// insert log
			DB::transaction(function() use($id,$req,$data) {
				DB::table('helpdesk_assurance_inbox_log')->insert([
					'id_order'			=> $id,
					'order_status'		=> $req->status_order,
					'status_log'		=> $data->STATUS,
					'catatan'			=> $req->helpdesk_reply,
					'hd_by'				=> session('auth')->nama,
					'updated_at'		=> DB::raw('NOW()')
				]);
			});

			// update log (transfer)
			if ($req->input_transfer == 'YES')
			{
				$check_log = DB::table('helpdesk_assurance_inbox_log')->where('id_order', $id)->get();
				$check_hd = DB::table('1_2_employee')->where('nik', $req->transfer_to)->first();

				DB::transaction(function() use($id,$req,$check_log,$check_hd) {
					// update order
					DB::table('helpdesk_assurance_inbox')->where('id_hai', $id)->update([
						'helpdesk_by'		=> $req->transfer_to,
						'helpdesk_name'		=> $check_hd->nama,
						'helpdesk_reply'	=> $req->helpdesk_reply,
						'command'			=> $req->command,
						'no_tiket'			=> $req->no_tiket,
						'inet'				=> $req->inet,
						'sn_lama'			=> $req->sn_lama,
						'sn_baru'			=> $req->sn_baru,
						'mac_stb'			=> $req->mac_stb,
						'transfer_by'		=> session('auth')->id_karyawan,
						'transfer_name'		=> session('auth')->nama
					]);

					// update log
					foreach($check_log as $log)
					{
						DB::table('helpdesk_assurance_inbox_log')->where('id_log', $log->id_log)->update([
							'hd_by'			=> $check_hd->nama
						]);
					}
				});

				
				// send log order
				if ($data->command == "ganti_ont")
				{
					$command = "Ganti ONT";
				} elseif ($data->command == "config_stb") {
					$command = "Config STB";
				} elseif ($data->command == "config_layanan") {
					$command = "Config Layanan";
				} elseif ($data->command == "pindah_odp") {
					$command = "Pindah ODP";
				} else {
					$command = "";
				}

				$txt = "<b>Reporting Order ID ".$id." - ".$command."</b>\n\nTransfer Order from <b>".session('auth')->nama."</b> to <b>".$check_hd->nama."</b>\n\n<i>".date('Y-m-d H:i:s')."</i>";
				
				Telegram::sendMessage([
					'chat_id' => '-1001459573787',
					'parse_mode' => 'html',
					'text' => $txt
				]);
			}
			
			if ($data->command == "ganti_ont")
			{
				$command = "Ganti ONT";
				$txt = "<b>Reporting Order ID ".$id." - ".$command."</b>\n\nTanggal Order : ".$data->tgl_order."\nNo Tiket : ".$data->no_tiket."\nSN Lama : ".$data->sn_lama."\nSN Baru : ".$data->sn_baru. "\Catatan Request : ".$data->catatan_request."\nRequest oleh : ".$teknisi->nama." / ".$data->username."\n\nStatus : ".$data->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$req->helpdesk_reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
			} elseif ($data->command == "config_stb") {
				$command = "Config STB";
				$txt = "<b>Reporting Order ID ".$id." - ".$command."</b>\n\nTanggal Order : ".$data->tgl_order."\nNo Tiket : ".$data->no_tiket."\nMac Address STB : ".$data->mac_stb."\nCatatan Request : ".$data->catatan_request."\nRequest oleh : ".$teknisi->nama." / ".$data->username."\n\nStatus : ".$data->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$req->helpdesk_reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
			} elseif ($data->command == "config_layanan") {
				$command = "Config Layanan";
				$txt = "<b>Reporting Order ID ".$id." - ".$command."</b>\n\nTanggal Order : ".$data->tgl_order."\nNo Tiket : ".$data->no_tiket."\nNo Internet : ".$data->inet."\nCatatan Request : ".$data->catatan_request."\nRequest oleh : ".$teknisi->nama." / ".$data->username."\n\nStatus : ".$data->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$req->helpdesk_reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
			} elseif ($data->command == "pindah_odp") {
				$command = "Pindah ODP";
				$txt = "<b>Reporting Order ID ".$id." - ".$command."</b>\n\nTanggal Order : ".$data->tgl_order."\nNo Tiket : ".$data->no_tiket."\nNama ODP : ".$data->nama_odp."\nValins ID : ".$data->valins_id."\nSN ONT : ".$data->sn_lama."\nLabelcode : ".$data->labelcode."\nRequest oleh : ".$teknisi->nama." / ".$data->username."\n\nStatus : ".$data->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$req->helpdesk_reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
			} else {
				$command = "";
				$txt = "";
			}

			// send log reporting
			Telegram::sendMessage([
				'chat_id' => '-1001459573787',
				'parse_mode' => 'html',
				'text' => $txt
			]);

			$msg = "<b>ID Order :</b> ".$data->id_hai."\n<b>Command :</b> ".$command."\n<b>Status :</b> ".$data->STATUS."\n<b>Balasan :</b>\n\n".$data->helpdesk_reply."\n\nSalam <b>".session('auth')->nama."</b>";

			BotModel::sendMessageReplyTommanGO($data->chat_id,$msg,$data->message_id);

			return redirect('helpdesk/assurance/config')->with('alerts', [
				['type' => 'success', 'text' => 'Success Update Order']
			]);

		} else {
			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Maaf ID Order '.$id.' Sudah di Take Order oleh '.$log->hd_by.' ']
			]);
		}
	}

	public function helpdesk_config($hd,$date){

		$query = HelpdeskModel::helpdesk_config($hd,$date);

		return view('helpdesk.helpdesk_config', compact('query','hd','date'));
	}

  public function inbox_manja(){
	  $datetime = date('Y');
	  $title = "INBOX HELPDESK MANJA PENDING";
	  $hd = '0';
	  $data = DB::SELECT('
			SELECT
			  dt.Ndem as order_id,
			  dt.id as id_dt,
			  r.uraian as tim,
			  gt.title as sektor,
			  pls.laporan_status as status,
			  pl.catatan as catatan_tek,
			  pl.noPelangganAktif as pic,
			  pl.modified_at as tgl_update,
			  dt.tgl as tgl_dispatch,
			  dt.manja,
			  pl.hd_manja,
			  dt.manjaBy,
			  emp.nama as nama,
			  dt.manja_updated
			FROM dispatch_teknisi dt
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
			LEFT JOIN 1_2_employee emp ON dt.manjaBy = emp.nik
			WHERE
				(gt.ioan_check IS NULL AND gt.title IS NOT NULL) AND
				dt.Ndem NOT LIKE "IN%" AND
				dt.jenis_layanan NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
				pl.status_laporan IN ("17","49","23","45","48","36") AND 
				(DATE(dt.tgl) LIKE "'.$datetime.'%")
			ORDER BY dt.id ASC
	  ');
	  return view('helpdesk.manja',compact('data','datetime','title','hd'));
  }

  public function inbox_manja_berangkat(){
	$datetime = date('Y-m-d');
	$title = "INBOX HELPDESK MANJA ORDER BERANGKAT";
	$hd = '1';
	$data = DB::SELECT('
		  SELECT
			dt.Ndem as order_id,
			dt.id as id_dt,
			r.uraian as tim,
			gt.title as sektor,
			pls.laporan_status as status,
			pl.catatan as catatan_tek,
			pl.noPelangganAktif as pic,
			pl.modified_at as tgl_update,
			dt.tgl as tgl_dispatch,
			dt.manja,
			pl.hd_manja,
			dt.manjaBy,
			emp.nama as nama,
			dt.manja_updated
		  FROM dispatch_teknisi dt
		  LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		  LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		  LEFT JOIN regu r ON dt.id_regu = r.id_regu
		  LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
		  LEFT JOIN 1_2_employee emp ON dt.manjaBy = emp.nik
		  WHERE
			  dt.Ndem NOT LIKE "IN%" AND
			  pl.status_laporan = "28" AND
			  (DATE(dt.tgl) LIKE "'.$datetime.'")
		  ORDER BY dt.id ASC
	');
	return view('helpdesk.manja',compact('data','datetime','title','hd'));
}

  public function editManja($id){
	$data = DB::table('dispatch_teknisi')->where('id',$id)->first();
	return view('helpdesk.editManja',compact('data'));
  }

  public function saveManja(Request $req, $id){
	
	$this->validate($req,
	[
		'manja.required'      => 'Kolom Manja Kosong !'
	]);
	DB::table('dispatch_teknisi')->where('id',$id)->update([
		'manja'       	=> $req->manja,
		'manja_status'	=> 1,
		'manjaBy'     	=> session('auth')->id_user,
		'manja_updated' => DB::raw('NOW()')
	]);
	return back()->with('alerts', [
		['type' => 'success', 'text' => 'Success Update Manja Order ...']
	  ]);
  }

  public function saveExkendala(Request $req)
  {
	DB::table('api_jointer_psb')->where('id_api', $req->id_api)->update([
		'status_manja_hd' => $req->status_manja_hd,
		'note_manja_hd' => $req->note_manja_hd,
		'manja_hd_by' => session('auth')->id_karyawan,
		'dtm_manja_hd' => date('Y-m-d H:i:s')
	]);

	return back()->with('alerts', [
		['type' => 'success', 'text' => 'Success Update Manja Ex-Kendala']
	]);
  }

  public function book($id)
  {
	$cek_order = DB::table('helpdesk_inbox')->where('FUP_BY', session('auth')->id_user)->where('DELETEX', 0)->whereIn('STATUS', [2, 3, 4, 6])->get();
	if (count($cek_order) > 3)
	{
		return back()->with('alerts', [
			['type' => 'danger', 'text' => 'Order yang Dapat Dikerjakan Maksimal 3 !']
		]);
	}
	//cek helpdesk login
	$cek_login = DB::table('1_2_employee')->where('nik', session('auth')->id_user)->first();
	if ($cek_login->chat_id == NULL) {
		return back()->with('alerts', [
			['type' => 'danger', 'text' => 'Login ke Bot Telegram dulu rekan']
		]);
	}

  	DB::table('helpdesk_inbox')->where('ID',$id)->update([
		"FUP_BY" => session('auth')->id_karyawan,
		"FUP_NAME" => session('auth')->nama,
		"STATUS" => 6
  	]);
  	$getData = DB::table('helpdesk_inbox')->where('ID',$id)->first();
	$msg = "ID Order : <b>$getData->ID</b>\nCommand : <b>Fallout</b>\n\nOrder rekan saat ini sedang kami kerjakan, harap menunggu.\n\nSalam <b>$getData->FUP_NAME</b>";

	BotModel::sendMessageReplyTommanGO($getData->CHAT_ID,$msg,$getData->MESSAGE_ID);

	return redirect('/helpdesk_inbox')->with('alerts', [
		['type' => 'success', 'text' => '<strong> BERHASIL </strong>BOOK ID ORDER '.$id]]);
  	
  }
  public function inbox_booked(){
  	  	$getInbox = DB::SELECT('
  		SELECT 
  		a.*,
  		b.STATUS,
  		d.uraian,
  		c.jenis_layanan,
		e.nama as request_by
  		FROM 
  			helpdesk_inbox a
  		LEFT JOIN helpdesk_inbox_status b ON a.STATUS = b.ID
  		LEFT JOIN dispatch_teknisi c ON a.SC = c.Ndem
  		LEFT JOIN regu d ON c.id_regu = d.id_regu
		LEFT JOIN 1_2_employee e ON a.TELEGRAM_ID = e.chat_id
  		WHERE 
  			a.STATUS <> "1" AND 
  			a.FUP_BY = "'.session('auth')->id_karyawan.'"
		  ');
		  
		if (session('auth')->level==10) {
			$layout = 'tech_layout';
		} else {
			$layout = 'layout';
		}
  	// $getInbox = DB::table('helpdesk_inbox')->where('STATUS','<>',1)->where('FUP_BY',session('auth')->id_karyawan)->get();
  	return view('helpdesk.inbox',compact('getInbox','layout'));
  }
  public function fup($id)
  {
  	$message = DB::table('helpdesk_inbox')->leftJoin('1_2_employee','helpdesk_inbox.TELEGRAM_ID','=','1_2_employee.chat_id')->select('helpdesk_inbox.*','1_2_employee.nama')->where('ID',$id)->first();
  	$getStatus = DB::table('helpdesk_inbox_status')->whereNotIn('ID',[0, 1])->orderBy('urut', 'ASC')->get();
  	return view('helpdesk.input',compact('id','message','getStatus'));
  }
  public function save(Request $req)
  {
  	date_default_timezone_set("Asia/Makassar");
	
	//cek helpdesk login
	$cek_login = DB::table('1_2_employee')->where('nik', session('auth')->id_user)->first();
	if ($cek_login->chat_id == NULL) {
		return back()->with('alerts', [
			['type' => 'danger', 'text' => 'Login ke Bot Telegram dulu rekan']
		]);
	}

	$usernameHD = '@'.$cek_login->username_telegram;

  	$messageID = $req->input('MESSAGE_ID');
  	$chatID = $req->input('CHAT_ID');
  	$id = $req->input('ID');
  	$status = $req->input('status_hd');
  	$speedy = $req->input('speedy');
	$reply = $req->input('reply');

	// dd($status, $speedy, $reply);

	$getStatus = DB::table('helpdesk_inbox_status')->where('ID',$status)->first();
  	if ($reply == "")
	{
  		return redirect('/helpdesk_fup/'.$id)->with('alerts', [
		['type' => 'danger', 'text' => '<strong> GAGAL </strong>FUP ORDER '.$id]]);
	} elseif ($status == 7 && ($speedy == null || $speedy == "")) {
		// $ibooster = self::grabIbooster($speedy);

		return redirect('/helpdesk_inbox')->with('alerts', [
		['type' => 'danger', 'text' => '<strong> GAGAL </strong>Harap Masukan No Speedy nya Rekan!']]);
  	} else {
  		DB::table('helpdesk_inbox')->where('ID',$id)->update([
  			"FUP_BY" => session('auth')->id_karyawan,
  			"REPLY" => $reply,
  			"SPEEDY" => $speedy,
  			"TGLUPDATE" => date('Y-m-d H:i:s'),
  			"STATUS" => $status
  		]);
  		DB::table('helpdesk_inbox_log')->insert([
  			"INBOX_ID" => $id,
  			"FUP_BY" => session('auth')->id_karyawan,
  			"STATUS" => $status,
  			"REPLY" => $reply
  		]);

		$getData = DB::table('helpdesk_inbox')->leftJoin('1_2_employee','helpdesk_inbox.TELEGRAM_ID','=','1_2_employee.chat_id')->select('helpdesk_inbox.*','1_2_employee.nama')->where('ID',$id)->first();

		if ($status == '7')
		{
			$text = "ID Order : <b>$getData->ID</b>\nCommand : <b>$getData->Category</b>\nStatus : <b>$getStatus->STATUS</b>\nBalasan :\n\n<b>$reply</b>\n\nHarap konfirmasi order, dengan mengklik tautan dibawah :\n\n<a href='https://biawak.tomman.app/confirmBot?status=done&id=$id'>Layanan Normal</a>\n\n<a href='https://biawak.tomman.app/confirmBot?status=nok&id=$id'>Layanan Belum Normal</a>\n\nSalam <b>$getData->FUP_NAME</b> $usernameHD";
		} else {
			$text = "ID Order : <b>$getData->ID</b>\nCommand : <b>$getData->Category</b>\nStatus : <b>$getStatus->STATUS</b>\nBalasan :\n\n<b>$reply</b>\n\nSalam <b>$getData->FUP_NAME</b> $usernameHD";
		}

        BotModel::sendMessageReplyTommanGO($chatID,$text,$messageID);

		if ($getData->Category == "FALLOUT") {

			$txt = "<b>Reporting Order ID ".$getData->ID." - Fallout</b>\n\nTanggal Order : ".$getData->TGLOPEN."\nSC : ".$getData->SC."\nSN ONT : ".$getData->SNONT."\nNama ODP : ".$getData->ODP."\nValins ID : ".$getData->VALINS_ID."\nCatatan Request : ".$getData->ACTION."\nRequest oleh : ".$getData->nama." / ".$getData->USERNAME."\n\nStatus : ".$getStatus->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";

		} elseif ($getData->Category == "PDA") {

			$txt = "<b>Reporting Order ID ".$getData->ID." - PDA</b>\n\nTanggal Order : ".$getData->TGLOPEN."\nSC : ".$getData->SC."\nSN ONT : ".$getData->SNONT."\nNama ODP : ".$getData->ODP."\nValins ID : ".$getData->VALINS_ID."\nCatatan Request : ".$getData->ACTION."\nRequest oleh : ".$getData->nama." / ".$getData->USERNAME."\n\nStatus : ".$getStatus->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
		} elseif ($getData->Category == "ADDSERVICE") {
			
			$txt = "<b>Reporting Order ID ".$getData->ID." - Add Service</b>\n\nTanggal Order : ".$getData->TGLOPEN."\nSC : ".$getData->SC."\nSN ONT : ".$getData->SNONT."\nCatatan Request : ".$getData->ACTION."\nRequest oleh : ".$getData->nama." / ".$getData->USERNAME."\n\nStatus : ".$getStatus->STATUS."\nHelpdesk : ".session('auth')->nama."\nBalasan : ".$reply."\nTanggal Close : ".date('Y-m-d H:i:s')."\n#TommanGO";
		}

		// send log reporting
		Telegram::sendMessage([
			'chat_id' => '-1001459573787',
			'parse_mode' => 'html',
			'text' => $txt
		]);
		
		return redirect('/helpdesk_inbox/')->with('alerts', [
		['type' => 'success', 'text' => '<strong> FUP ORDER </strong> '.$getStatus->STATUS.' '.$id]]);
  	}
  }

  public function deleteBooked($order,$id)
  {
	

    $auth = session('auth')->level;
    if ( $auth==2 || $auth==15 ){
      DB::table('helpdesk_inbox_delete_log')->insert([
			 'sc'      		=> $order,
			 'inbox_id'		=> $id,
			 'log'     		=> 'Inbox Order '.$order.' Sukses Dihapus ... ',
             'user_by' 		=> session('auth')->id_karyawan,
             'deleted_at'	=> DB::raw('NOW()')
          ]);

		  $update = DB::table('helpdesk_inbox')->where('ID',$id)->update([
			"DELETEX" => 1]);

          return back()->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUCCESS</strong> Deleted Inbox Order . . .']
          ]);
    } else {
      echo "User Not Authorized !!";
    }
  }

  public function monitoring_nte_bot_save(Request $req, $id)
  {
	//   dd($req->input(), $id);
		DB::transaction(
			function () use ($req, $id) {
				DB::table('helpdesk_assurance_inbox')->where('id_hai', $id)->update([
					'ket_so'		=> $req->input('ket_so'),
					'catatan_so'	=> $req->input('catatan_so'),
					'userso_by'		=> session('auth')->id_karyawan,
					'userso_at'		=> date('Y-m-d H:i:s')
				]);
			});

		return back()->with('alerts', [
			['type' => 'success', 'text' => '<strong>SUKSES</strong> Update Data!']
		]);
  }

	public static function remindReportSemesta()
	{
		$chatID = "-1001082577132";
		$text = "Jangan Lupa Report Semesta

@safeja @Fzzzrin @aman_kan @kemasrizkyh @Furqan_Idifasi @nandiazis1 @OsamaAlkatiri @riyanhidayyat @M_Abdul_Rajib";

		Telegram::sendMessage([
			'chat_id' => $chatID,
			'parse_mode' => 'html',
			'text' => $text
		]);

		print_r("success send message to group\n");
	}

	public static function remindActComp()
	{
		$text = "Jangan Lupa Cek Act Comp Guys\n\n";
		$chatID_TL = '';
		$list_ao = $list_pda = [];

		// AO
		$getData_AO = HelpdeskModel::get_actcomp('AO');
		$i_ao = 'none';
		if (count($getData_AO) > 0)
		{
			$i_ao = 'available';
			$text .= "=== AO ===\n\n";
			foreach($getData_AO as $data_ao)
			{
				$list_ao[$data_ao->TL][] = $data_ao;
			}
			if ($list_ao)
			{
				foreach($list_ao as $key => $value)
				{
					$text .= $key . "\n";
					foreach($value as $v)
					{
						if ($v->chat_id)
						{
							$chatID_TL .= $v->chat_id.',';
						}

						$awal  = date_create($v->durasi_jam);
						$akhir = date_create();
						$diff  = date_diff( $awal, $akhir );

						if ($diff->d > 0)
						{
							$waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
						} else {
							$waktu = $diff->h.' Jam '.$diff->i.' Menit';
						}

						$text .= $v->tim . " / " . $v->orderId . " / " . $v->wfm_id . " / " . $waktu . "\n";					
					}
					$text .= "\n";
				}
			}
		}

		// PDA
		$getData_PDA = HelpdeskModel::get_actcomp('PDA');
		$i_pda = 'none';
		if (count($getData_PDA) > 0)
		{
			$i_pda = 'available';
			$text .= "=== PDA ===\n\n";
			foreach($getData_PDA as $data_pda)
			{
				$list_pda[$data_pda->TL][] = $data_pda;
			}
			if($list_pda)
			{
				foreach($list_pda as $key => $value)
				{
					$text .= $key . "\n";
					foreach($value as $v)
					{
						if ($v->chat_id)
						{
							$chatID_TL .= $v->chat_id.',';
						}

						$awal  = date_create($v->durasi_jam);
						$akhir = date_create();
						$diff  = date_diff( $awal, $akhir );

						if ($diff->d > 0)
						{
							$waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
						} else {
							$waktu = $diff->h.' Jam '.$diff->i.' Menit';
						}

						$text .= $v->tim . " / " . $v->orderId . " / " . $v->wfm_id . " / " . $waktu . "\n";					
					}
					$text .= "\n";
				}
			}
		}

		$text .= "\ncc : @mauliddaaputri @AlvianWahyudi @rina_rinamaria @SukaaNgilang @sisigelapdibalikterang @trisbay @Woyykiyomasaa @MrDoNothingggg @Ettynojapri @byaddd @skuuuy @Yanuar11 @safeja @Fzzzrin @aman_kan @kemasrizkyh @Furqan_Idifasi @nandiazis1 @OsamaAlkatiri";

		if ($i_ao != 'none' && $i_pda != 'none')
		{
			Telegram::sendMessage([
				'chat_id'    => '-1001661694038',
				'parse_mode' => 'html',
				'text'       => $text
			]);

			print_r("success send message to group\n");

			$exp_chatID_TL = explode(',', $chatID_TL);
			foreach (array_filter(array_unique($exp_chatID_TL)) as $exc_TL)
			{
				BotModel::sendMessageTommanGO($exc_TL,$text);
			}
		}
	}

	public static function grabIbooster($speedy)
	{
		$ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://10.62.165.58/home.php?page=' . $ibooster->page . '',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => 'nospeedy=' . $speedy . '&analis=ANALISA',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/x-www-form-urlencoded',
			'Cookie: ' . $ibooster->cookies . ''
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$columns = array(
		1 =>
		'ND', 'IP_Embassy', 'Type', 'Calling_Station_Id', 'IP_NE', 'ADSL_Link_Status', 'Upstream_Line_Rate', 'Upstream_SNR', 'Upstream_Attenuation', 'Upstream_Attainable_Rate', 'Downstream_Line_Rate', 'Downstream_SNR', 'Downstream_Attenuation', 'Downstream_Attainable_Rate', 'ONU_Link_Status', 'ONU_Serial_Number', 'Fiber_Length', 'OLT_Tx', 'OLT_Rx', 'ONU_Tx', 'ONU_Rx', 'Gpon_Onu_Type', 'Gpon_Onu_VersionID', 'Gpon_Traffic_Profile_UP', 'Gpon_Traffic_Profile_Down', 'Framed_IP_Address', 'MAC_Address', 'Last_Seen', 'AcctStartTime', 'AccStopTime', 'AccSesionTime', 'Up', 'Down', 'Status_Koneksi', 'Nas_IP_Address'
		);

		$dom = @\DOMDocument::loadHTML(trim($response));
		$table = $dom->getElementsByTagName('table')->item(1);
		if ($table) {
		$rows = $table->getElementsByTagName('tr');
		$result = array();
		for ($i = 3, $count = $rows->length; $i < $count; $i++) {
			$cells = $rows->item($i)->getElementsByTagName('td');
			$data = array();
			for ($j = 1, $jcount = count($columns); $j < $jcount; $j++) {
			//echo $j." ";
			$td = $cells->item($j);
			if (is_object($td)) {
				$node = $td->nodeValue;
			} else {
				$node = "empty";
			}
			$data[$columns[$j]] = $node;
			}
			$result[] = $data;
		}
		return ($result);
		} else {
		dd("Maaf, Ibooster Gangguan!");
		return back()->with('alerts', [['type' => 'danger', 'text' => 'Maaf, Ibooster Gangguan!']]);
		}
	}
}
