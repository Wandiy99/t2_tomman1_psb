<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Telegram;

date_default_timezone_set('Asia/Makassar');

class MonetController extends Controller
{
    public static function autoSyncMonetS($witel)
    {
        ini_set('memory_limit', '-1');
        ini_set("max_execution_time", "-1");

        $get_data = DB::table('monet_sugar_treg6')->where('witel', $witel)->get()->toArray();
        DB::table('ibooster_monet_sugar_treg6')->where('ID_WITEL', $witel)->delete();

        $chunk = array_chunk($get_data, 50);
        foreach ($chunk as $batch => $datachunk) {
            $spd = "'";
            foreach ($datachunk as $no => $data) {
                if ($no) {
                    $spd .= ";" . $data->no_inet;
                } else {
                    $spd .= $data->no_inet;
                }
            }
            $spd .= "'";
            echo ($batch % 150);

            echo "." . ++$no . " php /srv/htdocs/tomman1_psb/artisan monetx ukurSugar " . $witel . " " . $batch . " " . $spd . " > /dev/null &\n\n";
            exec("php /srv/htdocs/tomman1_psb/artisan monetx ukurSugar " . $witel . " " . $batch . " " . $spd . " > /dev/null &");
            if (($batch + 1) % 150 == 0) {
                echo "sleep 4.5 minute\n";
                sleep(270);
                exec("php /srv/htdocs/tomman1_psb/artisan monetinsertx " . $witel . " ukurSugar > /dev/null &");
            }
        }

        sleep(270);
        exec("php /srv/htdocs/tomman1_psb/artisan monetinsertx " . $witel . " ukurSugar > /dev/null &");
    }

    public static function autoSyncMonetX($witel)
    {
        ini_set('memory_limit', '-1');
        ini_set("max_execution_time", "-1");

        $get_data = DB::table('monet_treg6')->where('witel', $witel)->get()->toArray();
        DB::table('ibooster_monet_treg6')->where('ID_WITEL', $witel)->delete();

        $chunk = array_chunk($get_data, 50);
        foreach ($chunk as $batch => $datachunk) {
            $spd = "'";
            foreach ($datachunk as $no => $data) {
                if ($no) {
                    $spd .= ";" . $data->no_inet;
                } else {
                    $spd .= $data->no_inet;
                }
            }
            $spd .= "'";
            echo ($batch % 150);
            echo "." . ++$no . " php /srv/htdocs/tomman1_psb/artisan monetx ukurQ " . $witel . " " . $batch . " " . $spd . " > /dev/null &\n";
            exec("php /srv/htdocs/tomman1_psb/artisan monetx ukurQ " . $witel . " " . $batch . " " . $spd . " > /dev/null &");
            if (($batch + 1) % 150 == 0) {
                echo "sleep 4.5 minute\n";
                sleep(270);
                exec("php /srv/htdocs/tomman1_psb/artisan monetinsertx " . $witel . " ukurQ > /dev/null &");
            }
        }
        sleep(270);
        exec("php /srv/htdocs/tomman1_psb/artisan monetinsertx " . $witel . " ukurQ > /dev/null &");
    }

    public static function grabIboosterMonet($type, $witel, $batch, $spd)
    {
        ini_set('memory_limit', '-1');
        ini_set("max_execution_time", "-1");

        // dd($type, $witel, $batch, urlencode($spd));

        $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://10.62.165.58/home.php?page=' . $ibooster->page . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "nospeedy=" . urlencode($spd) . "&analis=ANALISA");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Origin: http://10.62.165.58';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'Dnt: 1';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Referer: http://10.62.165.58/home.php?page=' . $ibooster->page . '';
        $headers[] = 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6';
        $headers[] = 'Cookie: ' . $ibooster->cookies . '';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        // dd($response);

        $columns = array(
            1 =>
            'ND', 'IP_Embassy', 'Type', 'Calling_Station_Id', 'IP_NE', 'ADSL_Link_Status', 'Upstream_Line_Rate', 'Upstream_SNR', 'Upstream_Attenuation', 'Upstream_Attainable_Rate', 'Downstream_Line_Rate', 'Downstream_SNR', 'Downstream_Attenuation', 'Downstream_Attainable_Rate', 'ONU_Link_Status', 'ONU_Serial_Number', 'Fiber_Length', 'OLT_Tx', 'OLT_Rx', 'ONU_Tx', 'ONU_Rx', 'Gpon_Onu_Type', 'Gpon_Onu_VersionID', 'Gpon_Traffic_Profile_UP', 'Gpon_Traffic_Profile_Down', 'Framed_IP_Address', 'MAC_Address', 'Last_Seen', 'AcctStartTime', 'AccStopTime', 'AccSesionTime', 'Up', 'Down', 'Status_Koneksi', 'Nas_IP_Address'
        );

        $dom = @\DOMDocument::loadHTML(trim($response));
        $table = $dom->getElementsByTagName('table')->item(1);

        // dd($table);

        if (count($table)) {
            $rows = $table->getElementsByTagName('tr');
            $result = $ND = array();
            for ($i = 3, $count = $rows->length; $i < $count; $i++) {
                $cells = $rows->item($i)->getElementsByTagName('td');
                $data = array();
                for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++) {
                    $td = $cells->item($j);
                    if (is_object($td)) {
                        $node = $td->nodeValue;
                    } else {
                        $node = "empty";
                    }
                    $data[$columns[$j]] = trim($node);
                }
                $data['batch'] = $batch;
                $data['ID_WITEL'] = $witel;
                $result[] = $data;
                $ND[] = $data['ND'];
            }

            // dd(json_encode($result));

            if ($type == "ukurQ") {
                file_put_contents(public_path() . "/monetarray/" . $witel . "/" . $type . "/T1TOINSERT" . $batch . "" . date('YmdHis') . ".txt", json_encode($result));
            } elseif ($type == "ukurSugar") {
                file_put_contents(public_path() . "/monetarray/" . $witel . "/" . $type . "/T1TOINSERT" . $batch . "" . date('YmdHis') . ".txt", json_encode($result));
            }

        } else {

            if ($witel == "BALIKPAPAN") {
                $chatID = "-439024559";
            } elseif ($witel == "KALSEL") {
                $chatID = "-512082898";
            }

            Telegram::sendMessage([
                'chat_id' => $chatID,
                'parse_mode' => 'html',
                'text'    => "WORKER-BPP|" . $witel . "|" . $type . "|" . $batch . "|ERR"
            ]);
        }
    }

    public static function monetInsert($witel, $type)
    {
        $path = public_path() . "/monetarray/" . $witel . "/" . $type . "/";
        $result = array();
        if ($handle = opendir($path)) {

            while (false !== ($file = readdir($handle))) {
                if ('.' === $file) continue;
                if ('..' === $file) continue;

                if (filesize($path . $file)) {
                    $result = array_merge($result, json_decode(file_get_contents($path . $file)));
                    unlink($path . $file);
                } else {
                    echo "0 bytes file";
                    unlink($path . $file);
                    dd();
                }
            }
            closedir($handle);
        }
        $result = json_decode(json_encode($result), true);
        $message = null;
        // $no = 0;
        $nd = [];
        foreach ($result as $file) {
            if (in_array($file['ONU_Link_Status'], ['LOS'])) {
                $nd[] = $file['ND'];
            }
        }

        $chunk = array_chunk($result, 500);
        foreach ($chunk as $datachunk) {
            // dd($type, $datachunk);
            if ($type == "ukurQ") {
                DB::table("ibooster_monet_treg6")->insert($datachunk);
            } elseif ($type == "ukurSugar") {
                // dd($datachunk);
                DB::table("ibooster_monet_sugar_treg6")->insert($datachunk);
            }
        }
        // dd("done");

        $message = null;

        if ($witel == "BALIKPAPAN") {
            $chatID = "-439024559";
        } elseif ($witel == "KALSEL") {
            $chatID = "-512082898";
        }

        if ($type == "ukurQ") {
            if ($witel == "BALIKPAPAN") {
                foreach (DB::table('ibooster_monet_treg6')->select('ibooster_monet_treg6.*', 'monet_treg6.*', 'dossier_bpp_rully.STO')->leftjoin('monet_treg6', 'monet_treg6.no_inet', '=', 'ibooster_monet_treg6.ND')->leftjoin('dossier_bpp_rully', 'ibooster_monet_treg6.ND', '=', 'dossier_bpp_rully.NOTEL')->where('monet_treg6.witel', $witel)->whereIn('monet_treg6.no_inet', $nd)->groupBy('monet_treg6.no_inet')->orderBy('dossier_bpp_rully.STO', 'asc')->get() as $no => $val) {
                    $message .= $val->ND . ";" . $val->STO . "\n";
                }
            } elseif ($witel == "KALSEL") {
                foreach (DB::table('ibooster_monet_treg6')->select('ibooster_monet_treg6.*', 'monet_treg6.*')->leftjoin('monet_treg6', 'monet_treg6.no_inet', '=', 'ibooster_monet_treg6.ND')->where('monet_treg6.witel', $witel)->whereIn('monet_treg6.no_inet', $nd)->groupBy('monet_treg6.no_inet')->get() as $no => $val) {
                    $message .= $val->ND . ";\n";
                }
            }

            if ($message) {
                try {
                    Telegram::sendMessage([
                        'chat_id' => $chatID,
                        'parse_mode' => 'html',
                        'text'    => "List Layanan Terpantau LOS (Q) :\n\n" . $message
                    ]);
                } catch (Exception $e) {
                    Telegram::sendMessage([
                        'chat_id' => $chatID,
                        'parse_mode' => 'html',
                        'text'    => 'Pesan gagal terkirim! Silahkan lihat web untuk menunjukan'
                    ]);
                }
            }
        } elseif ($type == "ukurSugar") {
            if ($witel == "BALIKPAPAN") {
                foreach (DB::table('ibooster_monet_sugar_treg6')->select('ibooster_monet_sugar_treg6.*', 'monet_sugar_treg6.*', 'dossier_bpp_rully.STO')->leftjoin('monet_sugar_treg6', 'monet_sugar_treg6.no_inet', '=', 'ibooster_monet_sugar_treg6.ND')->leftjoin('dossier_bpp_rully', 'ibooster_monet_sugar_treg6.ND', '=', 'dossier_bpp_rully.NOTEL')->where('monet_sugar_treg6.witel', $witel)->whereIn('monet_sugar_treg6.no_inet', $nd)->groupBy('monet_sugar_treg6.no_inet')->orderBy('dossier_bpp_rully.STO', 'asc')->get() as $no => $val) {
                    $message .= $val->ND . ";" . $val->STO . "\n";
                }
            } elseif ($witel == "KALSEL") {
                foreach (DB::table('ibooster_monet_sugar_treg6')->select('ibooster_monet_sugar_treg6.*', 'monet_sugar_treg6.*')->leftjoin('monet_sugar_treg6', 'monet_sugar_treg6.no_inet', '=', 'ibooster_monet_sugar_treg6.ND')->where('monet_sugar_treg6.witel', $witel)->whereIn('monet_sugar_treg6.no_inet', $nd)->groupBy('monet_sugar_treg6.no_inet')->get() as $no => $val) {
                    $message .= $val->ND . ";\n";
                }
            }


            if ($message) {
                try {
                    Telegram::sendMessage([
                        'chat_id' => $chatID,
                        'parse_mode' => 'html',
                        'text'    => "List Layanan Terpantau LOS (Sugar) :\n\n" . $message
                    ]);
                } catch (Exception $e) {
                    Telegram::sendMessage([
                        'chat_id' => $chatID,
                        'parse_mode' => 'html',
                        'text'    => 'Pesan gagal terkirim! Silahkan lihat web untuk menunjukan'
                    ]);
                }
            }
        }
    }

    public static function grabIboosterInet($id)
    {
        $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://10.62.165.58/home.php?page=' . $ibooster->page . '',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'nospeedy=' . $id . '&analis=ANALISA',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: ' . $ibooster->cookies . ''
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $columns = array(
            1 =>
            'ND', 'IP_Embassy', 'Type', 'Calling_Station_Id', 'IP_NE', 'ADSL_Link_Status', 'Upstream_Line_Rate', 'Upstream_SNR', 'Upstream_Attenuation', 'Upstream_Attainable_Rate', 'Downstream_Line_Rate', 'Downstream_SNR', 'Downstream_Attenuation', 'Downstream_Attainable_Rate', 'ONU_Link_Status', 'ONU_Serial_Number', 'Fiber_Length', 'OLT_Tx', 'OLT_Rx', 'ONU_Tx', 'ONU_Rx', 'Gpon_Onu_Type', 'Gpon_Onu_VersionID', 'Gpon_Traffic_Profile_UP', 'Gpon_Traffic_Profile_Down', 'Framed_IP_Address', 'MAC_Address', 'Last_Seen', 'AcctStartTime', 'AccStopTime', 'AccSesionTime', 'Up', 'Down', 'Status_Koneksi', 'Nas_IP_Address'
        );

        $dom = @\DOMDocument::loadHTML(trim($response));
        $table = $dom->getElementsByTagName('table')->item(1);
        if ($table) {
            $rows = $table->getElementsByTagName('tr');
            $result = array();
            for ($i = 3, $count = $rows->length; $i < $count; $i++) {
                $cells = $rows->item($i)->getElementsByTagName('td');
                $data = array();
                for ($j = 1, $jcount = count($columns); $j < $jcount; $j++) {
                    //echo $j." ";
                    $td = $cells->item($j);
                    if (is_object($td)) {
                        $node = $td->nodeValue;
                    } else {
                        $node = "empty";
                    }
                    $data[$columns[$j]] = $node;
                }
                $result[] = $data;
            }
            dd($result);
        } else {
            dd("Maaf, Ibooster Gangguan!\n");
        }
    }
}
?>