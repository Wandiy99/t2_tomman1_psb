<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\DA\AssuranceModel;
use App\DA\HomeModel;
use App\DA\Rfc;
use DB;

class rfcController extends Controller
{
    public function input(){	
        $gudang = ["WH SO INV BANJARMASIN 1 (BJM CENTRUM)", "WH SO INV BANJARMASIN 2 (A.YANI)", "Banjarmasin - Area", "WH SO INV BATULICIN", "WH SO INV BARABAI", "WH SO INV TANJUNG TABALONG", "WH SO INV BANJARBARU"];
        
    	return view('rfc.form',compact('gudang'));
    }

    public function search(Request $req){
        $this->validate($req,[
            'q'        => 'required',
        ],[
            'q.required' => 'No. RFC Diisi',
        ]);
       
        $cari = $req->q;
        $existRfc = '';
        $datas = DB::table('alista_material_keluar')->where('no_rfc',$req->q)->get();
        
        if (count($datas)==0){   
            exec('cd ..;php artisan byRfc '.$req->q.'> /dev/null &');    
            sleep(5);
        };
   
        $datas = DB::table('alista_material_keluar')->where('no_rfc',$req->q)->get();
        if (count($datas)==0){
            return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Tidak Ada, Periksa Kembali Inputan RFC !']]);
        };

        $dataRfc = DB::table('rfc_input')->where('no_rfc',$cari)->first();
 
        if (count($dataRfc)<>0){
            $existRfc = 'NO. RFC Sudah Terinput By [ '.$dataRfc->created_by.' ]';
        }

        $gudang = ["WH SO INV BANJARMASIN 1 (BJM CENTRUM)", "WH SO INV BANJARMASIN 2 (A.YANI)", "Banjarmasin - Area", "WH SO INV BATULICIN", "WH SO INV BARABAI", "WH SO INV TANJUNG TABALONG", "WH SO INV BANJARBARU"];
        
        $gudangRfc  = $req->input('gudang'); 
        $table = DB::table('hdd')->get()->first();
        return view('rfc.input',compact('table','datas', 'cari', 'existRfc', 'gudang', 'gudangRfc'));
    }

    public function save(Request $req){
        $noRfc  = $req->noRfc;
        $namaRf = str_replace('/', '-', $noRfc);
        $nik    = session('auth')->id_user;
 
        // cek rfc dengan nama gudang
        // $cekMaterial = DB::table('alista_material_keluar')->where('no_rfc',$noRfc)->where('nama_gudang',$req->gudang)->first();
        // if(empty($cekMaterial)){
        //     return back()->with('alerts',[['type' => 'danger', 'text' => 'NIK Pengambil Tidak Sesuai']]);
        // }

        if ($req->flag==0){
            return back()->with('alerts',[['type' => 'danger', 'text' => 'Foto Material Diupload Bang !']]);
        }
        else{
            if ($req->hasFile('photo')){
                $table = DB::table('hdd')->get()->first();
                $path = public_path().'/'.$table->public.'/rfc/';
                if (!file_exists($path)) {
                    if (!mkdir($path, 0770, true))
                        return 'gagal menyiapkan folder foto evidence';
                }

                try {
                    $image = $req->file('photo');
                    $exe = ".jpg";
                    $image->move("$path", $namaRf.$exe);   

                }
                catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return 'gagal menyimpan foto evidence ';
                }   
            }
        }
       
    	// simpan ke rfc input
    	$exist = DB::table('rfc_input')->where('no_rfc',$noRfc)->first();
    	if (!$exist){

            // get mitra dan regu
            $getMitra = DB::table('alista_material_keluar')->where('no_rfc',$noRfc)->first();
            $getRegu  = DB::select('SELECT * FROM regu where ACTIVE=1 AND (nik1="'.session('auth')->id_user.'" OR nik2="'.session('auth')->id_user.'")');
            $regu   = '';
            $idRegu = '';
            if (count($getRegu)<>0){
                $idRegu = $getRegu[0]->id_regu;
                $regu   = $getRegu[0]->uraian;
            };

    		DB::table('rfc_input')->insert([
    			'tgl'			=> DB::raw('NOW()'),
    			'created_by'	=> session('auth')->id_user,
    			'no_rfc'		=> $noRfc,
                'gudang'        => $getMitra->nama_gudang,
                'mitra'         => $getMitra->mitra,
                'id_regu'       => $idRegu,
                'regu'          => $regu,
                'alista_id'     => $getMitra->alista_id,
                'tgl_keluar'    => $getMitra->tgl,
                'project'       => $getMitra->project
    		]);

            DB::table('rfc_log')->insert([
                'createdBy'  => session('auth')->id_user, 
                'createdAt'  => DB::raw('now()'),
                'rfc'        => $noRfc,
                'gudang'     => $getMitra->nama_gudang
            ]);

    	}
        else{
            return back()->with('alerts',[['type' => 'danger', 'text' => '<strong>Gagal Menyimpan, No. RFC Sudah Pernah Diinput</strong>']]);
        }

    	$tgl = date('Y-m-d');
		$nik = session('auth')->id_user;
		$dataAlista = DB::table('alista_material_keluar')->where('no_rfc',$noRfc)->get();

		if ($dataAlista){
            // dd('a');
            // hapus di rfc
            DB::table('rfc_sal')->where('alista_id',$dataAlista[0]->alista_id)->delete();

            foreach($dataAlista as $alista){
                $item  = $alista->id_barang;
                $num = 0; $num1 = 0;
                if ($item == "Preconnectorized-1C-150-NonAcc"){
                    $num = 150;
                    $num1 = 0;
                }
                elseif ($item == "Preconnectorized-1C-100-NonAcc"){
                    $num = 100;
                    $num1 = 0;
                }
                elseif ($item == "Preconnectorized-1C-75-NonAcc"){
                    $num = 75;
                    $num1= 0;
                }
                elseif ($item == "Preconnectorized-1C-50-NonAcc"){
                    $num = 50;
                    $num1 = 0;
                }
                elseif($item == "Preconnectorized-1C-80-NonAcc"){
                    $num = 80;
                    $num1 = 0;
                };

                if ($item == "PC-SC-SC-10"){
                    $num1 = 10;
                    $num = 0;
                }
                elseif ($item == "PC-SC-SC-20"){
                    $num1 = 20;
                    $num = 0;
                }
                elseif ($item == "PC-SC-SC"){
                    $num1 = 15;
                    $num = 0;
                };

                // cek data sudah ada
                $datax = DB::table('rfc_sal')->where('alista_id',$alista->alista_id)->where('id_item',$alista->id_barang)->first();
                if (count($datax)==0){
                    DB::table('rfc_sal')->insert([
                            'nik'       => $nik,
                            'id_item'   => $alista->id_barang,
                            'id_item_bantu' => $alista->id_barang.'_'.$alista->no_rfc,
                            'nama_item' => $alista->nama_barang,
                            'jumlah'    => $alista->jumlah,
                            'rfc'       => $alista->no_rfc,
                            'num'       => $num,
                            'num1'      => $num1,
                            'alista_id' => $alista->alista_id,
                            'harga'     => $alista->harga,
                            'harga_total' => $alista->harga_total
                    ]);
                }

			}
		}

        // simpan ke maintaince_saldo_rfc
        $dataSaldo = DB::table('rfc_sal')->where('nik',$nik)->where('rfc',$noRfc)->get();
        $dataRegu  = DB::SELECT('SELECT a.*, b.* 
                                 FROM regu a
                                 LEFT JOIN group_telegram b ON a.mainsector=b.chat_id
                                 WHERE
                                    a.ACTIVE=1 AND 
                                    (a.nik1="'.$nik.'" OR a.nik2="'.$nik.'")');
        
        $idRegu = NULL; $uraianRegu = NULL; $nik1   = NULL; $nik2 = NULL; $nikTl = NULL;
        if(count($dataRegu)<>0){
            $idRegu     = $dataRegu[0]->id_regu;
            $uraianRegu = $dataRegu[0]->uraian;
            $nik1       = $dataRegu[0]->nik1;
            $nik2       = $dataRegu[0]->nik2;
            $nikTl      = $dataRegu[0]->TL_NIK;    
        }
        
        if (count($dataSaldo)<>0){
            foreach($dataSaldo as $dtSaldo){
                DB::table('maintenance_saldo_rfc')->where('id_pengeluaran',$dtSaldo->alista_id)->where('id_item',$dtSaldo->id_item)->where('action',1)->delete();
                DB::table('maintenance_saldo_rfc')->insert([
                    'id_pengeluaran'    => $dtSaldo->alista_id,
                    'rfc'               => $dtSaldo->rfc,
                    'regu_id'           => $idRegu,
                    'regu_name'         => $uraianRegu,
                    'nik1'              => $nik1,
                    'nik2'              => $nik2,
                    'niktl'             => $nikTl,
                    'created_at'        => DB::raw('NOW()'),
                    'created_by'        => $nik,
                    'value'             => $dtSaldo->jumlah,
                    'id_item'           => $dtSaldo->id_item,
                    'action'            => '1',
                    'bantu'             => $dtSaldo->rfc.'#'.$dtSaldo->id_item,
                    'transaksi'         => '1'
                ]);
            }
        }
        ////

		return back()->with('alerts', [
        	['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan RFC']
      	]);
    }

    public function tampilLog()
    {
    	$nik   = session('auth')->id_user;
    	$datas = DB::table('rfc_log')->where('createdBy',$nik)->orderBy('createdAt','DESC')->get();

    	return view('rfc.listLog',compact('datas')); 
    }

    public function tampilSisaMaterial()
    {
    	$nik   = session('auth')->id_user;
    	$datas = DB::table('rfc_sal')
    				->where('nik',$nik)
    				->get();
        
    	return view('rfc.listSisa',compact('datas'));	
    }

    public function resetMaterial($rfc){
        $rfcNew = str_replace('-', '/', $rfc);
        DB::table('alista_material_keluar')->where('no_rfc',$rfcNew)->delete();
        $gudang = ["WH SO INV BANJARMASIN 1 (BJM CENTRUM)", "WH SO INV BANJARMASIN 2 (A.YANI)", "Banjarmasin - Area", "WH SO INV BATULICIN", "WH SO INV BARABAI", "WH SO INV TANJUNG TABALONG", "WH SO INV BANJARBARU"];

        return view('rfc.form',compact('gudang'));  
    }

    public function simpanAlistaId(){
        $rfcSal = DB::table('rfc_input')->get();
        foreach($rfcSal as $rfc){
            // $getRegu  = DB::table('regu')->where('uraian',$rfc->regu)->first();
            $getRegu  = DB::select('SELECT * FROM regu WHERE ACTIVE=1 AND (nik1="'.$rfc->created_by.'" OR nik2="'.$rfc->created_by.'" )');
            if (count($getRegu)<>0){
                DB::table('rfc_input')->where('alista_id',$rfc->alista_id)->update([
                        'id_regu'    => $getRegu[0]->id_regu,
                        'regu'       => $getRegu[0]->uraian
                ]);

            };
            
            echo "sukses \n";            
        }

        // $data = DB::table('Data_Pelanggan_Starclick')->get();
        // foreach($data as $dt){
        //     if ($dt->ndemSpeedy<>''){
        //         $internet = explode('~', $dt->ndemSpeedy);
        //         if (count($internet)>1){
        //             $int = $internet[1];
        //         }
        //         else{
        //             $int = $internet[0];                }

        //         DB::table('Data_Pelanggan_Starclick')->where('orderId',$dt->orderId)->update(['internet' => $int]);
        //         echo $int." sukses \n";
        //     }

        // }

        // $data = DB::select('
        //     SELECT rfc_sal.id_item, rfc_sal.nik,
        //         SUM(CASE WHEN regu.mitra<>"TA" THEN rfc_sal.jmlTerpakai ELSE 0 END) as terpakaiMitra,
        //         SUM(CASE WHEN regu.mitra="TA" THEN rfc_sal.jmlTerpakai ELSE 0 END) as terpakaiTA
        //     FROM rfc_sal 
        //         LEFT JOIN regu ON (rfc_sal.nik=regu.nik1 OR rfc_sal.nik=regu.nik2) 
        //         LEFT JOIN rfc_input ON rfc_sal.rfc=rfc_input.no_rfc
        //     WHERE rfc_input.tgl LIKE "%'.$tgl.'%"
        //     GROUP BY id_item');
      
    }

    public function reportMonitoring($bulan){
        $data = DB::SELECT('
            SELECT 
            a.id_barang as id_item, 
            count(*) as jumlah,
                SUM(CASE WHEN regu.mitra<>"TA" THEN psb_laporan_material.qty ELSE 0 END) as terpakaiMitra,
                SUM(CASE WHEN regu.mitra="TA" THEN psb_laporan_material.qty ELSE 0 END) as terpakaiTA
            from dispatch_teknisi 
                LEFT JOIN psb_laporan ON dispatch_teknisi.id=psb_laporan.id_tbl_mj 
                LEFT JOIN psb_laporan_material ON psb_laporan.id=psb_laporan_material.psb_laporan_id 
                LEFT JOIN regu ON dispatch_teknisi.id_regu=regu.id_regu
                LEFT JOIN alista_material_keluar a ON psb_laporan_material.id_item = a.id_barang AND a.tgl LIKE "%'.$bulan.'%"
            WHERE psb_laporan.modified_at LIKE "%'.$bulan.'%" AND psb_laporan_material.id_item<>""
            GROUP BY psb_laporan_material.id_item');

        return view('rfc.monitoringMaterial',compact('data'));
    }
    
    public function reportMonitoringNew($tahun,$bulan){
        $pid = Input::get('pid');
        $mitra = Input::get('mitra');
        $periode = input::get('periode');
        $sektor  = input::get('sektor');

        $fieldPid = '';
        $fieldMitra = '';
        $fieldSektor = '';

        if ($pid==null){
            $pid = 'ALL';
        };

        if ($mitra==null){
            $mitra = 'ALL';
        };

        if ($sektor==null){
            $sektor = 'ALL';
        };

        $where_pid = '';
        if ($pid=="ALL"){
            $where_pid = 'AND project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
        } else {
            $pid = str_replace('/', '_', $pid);
            $where_pid = 'AND project LIKE "'.$pid.'%"';
            $fieldPid = $pid;
        };

        if ($mitra=="ALL"){
            $where_mitra = '';
            $where_mitra2 = '';
        } else {
            $where_mitra = 'AND mitra LIKE "%'.$mitra.'%"';
            $where_mitra2 = 'AND mitra LIKE "%'.$mitra.'%"';
            $fieldMitra = $mitra;
        };

        if ($sektor=="ALL"){
            $where_sektor = '';
        }
        else{
            $where_sektor = 'AND d.chat_id="'.$sektor.'"';
            $fieldSektor  = $sektor;
        };

        if ($periode==null){
            $periode = $tahun.'-'.$bulan;
        }
        else{
            $per = explode('-', $periode);
            $tahun = $per[0];
            $bulan = $per[1];

        };
        
        $where_periode = 'no_rfc LIKE "%'.$bulan.'/'.$tahun.'%"';
        $fieldPeriode = $tahun.'-'.$bulan;


        // $data = DB::SELECT('
        //     SELECT 
        //         a.id_barang as id_item, 
        //         SUM(a.jumlah) as jumlah
        //     FROM 
        //         alista_material_keluar a 
        //     LEFT JOIN regu b ON (a.nik_pemakai = b.nik1 OR a.nik_pemakai = b.nik2) 
        //     WHERE 
        //         a.no_rfc LIKE "%'.$bulan.'/'.$tahun.'" 
        //         '.$where_mitra.'
        //         '.$where_pid.'
        //     GROUP BY a.id_barang
        //     ');
        
        // $dataKembali = array();
        // $dataPSB = array();
        // dd($data);
        // foreach ($data as $result){
        //     $dataKembali[$result->id_item] = AssuranceModel::MaterialKembali($bulan,$tahun,$pid,$mitra,$result->id_item);
        //     $dataPSB[$result->id_item] = AssuranceModel::MaterialPakai($bulan,$tahun,$pid,$mitra,$result->id_item);
        // }
        // return view('rfc.monitoringMaterial',compact('data','dataKembali','dataPSB','pid','mitra'));

        // $sql = '
        //         SELECT 
        //             a.*, b.*, d.*,
        //             SUM(b.jumlah) as jumlahRegister, 
        //             SUM(b.jmlTerpakai) as jumlahPakai,
        //             SUM(b.jmlKembali) as jumlahKembali
        //         FROM 
        //             rfc_input a
        //         LEFT JOIN rfc_sal b on a.alista_id=b.alista_id
        //         LEFT JOIN regu c ON a.id_regu=c.id_regu
        //         LEFT JOIN group_telegram d ON c.mainsector=d.chat_id
        //         where 
        //             b.alista_id is not null AND 
        //             '.$where_periode.'
        //             '.$where_mitra.'
        //             '.$where_pid.'
        //             '.$where_sektor.'
        //         GROUP BY b.id_item';
        
        $material = array();
        $sql = 'SELECT 
                    id_barang, sum(jumlah) as jumlah 
                FROM alista_material_keluar 
                WHERE 
                    '.$where_periode.'
                    '.$where_mitra.'
                    '.$where_pid.'
                GROUP BY id_barang';

        $data = DB::select($sql);
        foreach ($data as $dt){
            $mtPakai = AssuranceModel::MaterialPakai($bulan,$tahun,$pid,$mitra,$dt->id_barang,$sektor);
            $jumlahPakai = 0;
            if(count($mtPakai)<>0){
                $jumlahPakai = $mtPakai[0]->terpakai;
            };  

            $mtKembali = AssuranceModel::MaterialKembali($bulan,$tahun,$pid,$mitra,$dt->id_barang,$sektor);
            $jumlahKembali = 0;
            if (count($mtKembali)<>0){
                $jumlahKembali = $mtKembali[0]->kembali;
            }


            $material[$dt->id_barang]=array(
                'id_item'        => $dt->id_barang,       
                'jumlahRegister' => $dt->jumlah,
                'jumlahTerpakai' => $jumlahPakai,
                'jumlahKembali'  => $jumlahKembali
            );
        }
    
        $dataProject  = DB::select('SELECT project as id, project as text FROM alista_material_keluar WHERE project IN ("R06-8/2019","R06-2/2019","R06-13/2019") GROUP BY project ORDER BY project');
        $dataMitra    = DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
        $dataSektor   = DB::select('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_sektor=1');

        return view('rfc.monitoringMaterial',compact('material','pid','mitra','sektor','dataProject','dataMitra','periode','dataSektor','fieldPid','fieldMitra','fieldPeriode','fieldSektor'));
    }

    public function reportMonitoringNewListPengeluaran($pid,$mitra,$periode,$sektor,$id_item){
        if ($pid=="ALL"){
            $where_pid = 'AND b.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
        } else {
            $pid = str_replace('_', '/', $pid);
            $where_pid = 'AND b.project LIKE "'.$pid.'%"';
        };

        if ($mitra=="ALL"){
            $where_mitra = '';
        } else {
            $where_mitra = 'AND b.mitra LIKE "%'.$mitra.'%"';
        };

        if ($sektor=="ALL"){
            $where_sektor = '';
        }
        else{
            $where_sektor = 'AND d.chat_id="'.$sektor.'"';
        };

        $per    = explode('-', $periode);
        $tahun  = $per[0];
        $bulan  = $per[1];

        $sql = 'SELECT 
                    a.*,
                    b.*,
                    d.*
                FROM 
                    rfc_sal a
                LEFT JOIN rfc_input b ON b.alista_id=a.alista_id
                LEFT JOIN regu c ON b.id_regu = c.id_regu
                LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
                WHERE
                    a.rfc LIKE "%'.$bulan.'/'.$tahun.'%" AND 
                    a.id_item = "'.$id_item.'"
                    '.$where_pid.'
                    '.$where_mitra.'
                    '.$where_sektor.'
               ';
        
        $data = DB::select($sql);
        $list = '';
        if (count($data)<>NULL){
            foreach ($data as $dt){
                $getListWo = AssuranceModel::detailWoMaterial($dt->id_item_bantu);
                if (count($getListWo)<>0){
                    $wo = $getListWo;
                }
                else{
                    $wo = '';
                };

                $list[] = array(
                            'sektor'    => $dt->title,
                            'regu'      => $dt->regu,
                            'id_item'   => $dt->id_item,
                            'item'      => $dt->nama_item,
                            'rfc'       => $dt->rfc,
                            'project'   => $dt->project,
                            'jumlahPakai'   => $dt->jmlTerpakai,
                            'wo'    => $wo        
                        );
            };
        }
        
        return view('rfc.monitoringList',compact('pid','mitra','id_item','list'));
    }

    public function splitSaldoDcForm(){
        $gudang = ["",  "WH SO INV BANJARMASIN 1 (BJM CENTRUM)", "WH SO INV BANJARMASIN 2 (A.YANI)", "Banjarmasin - Area", "WH SO INV BATULICIN", "WH SO INV BARABAI", "WH SO INV TANJUNG TABALONG", "WH SO INV BANJARBARU"];
        
        return view('rfc.SplitSaldoForm',compact('gudang'));
    }

    public function splitSaldoDcProses(Request $req){
        $this->validate($req, [
            'rfc'   => 'required',
            'gudang'=> 'required',
        ],[
            'rfc.required'  => 'No. RFC Diisi',
            'gudang.required' => 'Gudang Diisi',
        ]);

        $inputan = $req->all();
        $getData = DB::table('alista_material_keluar')
                    ->where('no_rfc',$inputan['rfc'])  
                    ->where('nama_gudang',$inputan['gudang'])
                    ->where('id_barang','AC-OF-SM-1B')
                    ->first();
        
        if (count($getData)==0){
            return back()->with('alerts',[['type'=>'danger', 'text'=>'NO. RFC ini Tidak Memiliki Material <strong>AC-OF-SM-1B</strong>']]);
        }

        $getDataExist = DB::table('psb_saldo_dc')->where('alista',$getData->alista_id)->where('ket','2')->get();
        $dataSplit = [];
        foreach($getDataExist as $exist){
            array_push($dataSplit, $exist->jumlah);
        };
        for ($a=count($dataSplit); $a<=10; $a++){
            array_push($dataSplit, 0);
        };


        $gudang = ["",  "WH SO INV BANJARMASIN 1 (BJM CENTRUM)", "WH SO INV BANJARMASIN 2 (A.YANI)", "Banjarmasin - Area", "WH SO INV BATULICIN", "WH SO INV BARABAI", "WH SO INV TANJUNG TABALONG", "WH SO INV BANJARBARU"];

        return view('rfc.SplitSaldoProses',compact('inputan', 'getData', 'gudang', 'dataSplit'));
    }

    public function prosesSplit(Request $req){
        $inputan = $req->all();
        $total = '';
        $ukuran = ['100','150','200','250','300','350'];

        for($a=1;$a<=10;$a++){
            $total += $inputan['split'.$a];
        };

        if ($total <> $inputan['jumlah']){
            return back()->with('alerts',[['type'=>'danger', 'text'=>'Total Split Tidak Sama Dengan Jumlah DC yang Ada']]);
        };
        
        // simpan hasil split
        DB::table('psb_saldo_dc')->where('alista',$inputan['alista_id'])->delete();
        $getData = DB::table('alista_material_keluar')->where('alista_id',$inputan['alista_id'])->where('id_barang','AC-OF-SM-1B')->first();

        DB::table('psb_saldo_dc')->insert([
            'id_item'   => $getData->id_barang,
            'item'      => $getData->nama_barang,
            'jumlah'    => $getData->jumlah,
            'alista'    => $getData->alista_id,
            'rfc_alista'=> $getData->no_rfc,
            'ket'       => '1',
            'created_by'=> Session('auth')->id_user,
        ]);

        for ($a=1; $a<=10; $a++){
            if ($inputan['split'.$a]<>0){
                $data[] = [
                    'rfc_tomman'    => 'Tomman/'.$getData->no_rfc,
                    'id_item'       => $getData->id_barang,
                    'item'          => $getData->nama_barang,
                    'jumlah'        => $inputan['split'.$a],
                    'alista'        => $getData->alista_id,
                    'rfc_alista'    => $getData->no_rfc,
                    'ket'           => '2',
                    'created_by'    => Session('auth')->id_user,
                ];
            };
        };

        DB::table('psb_saldo_dc')->insert($data);

        return back()->with('alerts',[
            [
                'type' => 'success',
                'text' => 'Jumlah Material Berhasil Displit',
            ]
        ]);
    }

    public function dashboardSplitMitra(){
        $ukuran = ['100', '150', '200', '250','300', '350'];
        $data = [];
        foreach ($ukuran as $ukur){
            $data[$ukur] = Rfc::getListSaldoMitra($ukur);
        };

        $dataArea = [];
        foreach ($ukuran as $ukur){
            $dataArea[$ukur] = Rfc::getListSaldoArea($ukur);
        };

        return view('rfc.DashboardDcMitra',compact('data', 'ukuran', 'dataArea'));
    }

    public function sendDcForm(){
        $ukuran = ['100', '150', '200', '250','300', '350'];
        $data = [];
        foreach ($ukuran as $ukur){
            $data[$ukur] = Rfc::getListSaldoMitra($ukur);
        };

        $gudang = DB::select('SELECT chat_id as id, title as text FROM group_telegram WHERE ket_sektor=1');
        return view('rfc.FormSendDcArea',compact('ukuran', 'data', 'gudang'));   
    }

    public function sendDcProses(Request $req){
        $this->validate($req,[
            'gudang'    => 'required',
        ],[
            'gudang.required'   => 'Gudang Jangan Kosong',
        ]);

        $ukuran  = ['100', '150', '200', '250', '300', '350'];
        $inputan = $req->all();
        
        // cek jumlah yg diinput lawan digudang
        foreach($ukuran as $ukur){
            $jml = Rfc::getJumlahDc($ukur)[0];
            if ($inputan['split'.$ukur]>$jml->jumlah){
                return back()->with('alerts',[['type' => 'danger', 'text' => 'Saldo DC Ukuran '.$ukur.' Tidak Mencukupi, Cek Saldo ']]);
            };
        };        

        // update status dc jadi 3 proses kirim
        $dataSearch  = []; 
        foreach($ukuran as $ukur){
            if ($inputan['split'.$ukur]<>null){
                array_push($dataSearch, ['ukuran'=>$ukur, 'jumlah'=>$inputan['split'.$ukur]]);
            };
        };

        foreach($dataSearch as $data){
            $getData = DB::table('psb_saldo_dc')->where('jumlah',$data['ukuran'])->where('ket',2)->take($data['jumlah'])->update([
                'ket'   => 3,
                'gudang_kirim'  => $inputan['gudang'],
            ]);
        };

        return back()->with('alerts',[['type'=>'success', 'text'=>'Data Berhasil Di simpan, Status DC Proses Kirim']]);
    }

    public function konfirmasiDcProses(){
        $auth = session('auth');
        $nik  = $auth->id_user;
        $getChatId = DB::select('SELECT * FROM group_telegram WHERE TL_NIK="'.$nik.'" OR Nik_Atl="'.$nik.'"');

        $chatId = '';
        if ($getChatId){
            $chatId = $getChatId[0]->chat_id;
        };

        $getData = Rfc::getSendProsesArea($chatId);
        $group_telegram = HomeModel::group_telegram($nik);

        $ukuran = ['100', '150', '200', '250','300', '350'];
        $dataSaldo = [];
        foreach ($ukuran as $ukur){
            $dataSaldo[$ukur] = Rfc::getListSaldoSektor($ukur, $chatId);
        };

        $getRegu = DB::table('regu')
                    ->select(DB::raw('id_regu as id, uraian as text'))
                    ->where('mainsector',$chatId)
                    ->where('ACTIVE',1)
                    ->get();
        
        return view('rfc.konfirmasiDcArea',compact('getData', 'getChatId', 'group_telegram', 'dataSaldo', 'ukuran', 'getRegu'));
    }

    public function konfirmasiDc($id){
        Rfc::ubahKet($id, 4);
        return back()->with('alerts',[['type'=>'success', 'text'=>'DC Berhasil Diterima']]);
    }

    public function reportMonitoringNewListSisaSaldo($pid,$mitra,$periode,$id_item){
        if ($pid=="ALL"){
            $where_pid2 = 'AND a.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
        } else {
            $pid = str_replace('_', '/', $pid);
            $where_pid2 = 'AND a.project LIKE "'.$pid.'%"';

        };

        if ($mitra=="ALL"){
            $where_mitra2 = '';
        } else {
            $where_mitra2 = 'AND a.mitra LIKE "%'.$mitra.'%"';
        };

        $per    = explode('-', $periode);
        $tahun  = $per[0];
        $bulan  = $per[1];

        $sql = 'SELECT
                    b.id_item, 
                    b.nama_item,
                    sum(b.jumlah - b.jmlTerpakai) as saldo
                FROM 
                    rfc_input a
                LEFT JOIN rfc_sal b ON b.alista_id = a.alista_id
                WHERE
                    a.no_rfc LIKE "%'.$bulan.'/'.$tahun.'%" AND
                    b.id_item = "'.$id_item.'"
                    '.$where_pid2.'
                    '.$where_mitra2.'
                    GROUP BY b.id_item
               ';
        $listRegu = DB::select($sql);

        $sql2 = 'SELECT 
                    a.id_barang,
                    a.nama_barang,
                    sum(jumlah) as saldo 
                FROM 
                    alista_material_keluar a 
                WHERE 
                    a.no_rfc LIKE "%'.$bulan.'/'.$tahun.'%" AND 
                    a.id_barang = "'.$id_item.'" AND 
                    a.no_rfc NOT IN (
                        SELECT no_rfc FROM rfc_input WHERE no_rfc LIKE "%'.$bulan.'/'.$tahun.'%" GROUP BY no_rfc
                    ) 
                    '.$where_pid2.'
                    '.$where_mitra2.'
                    GROUP BY id_barang
                 ';
        
        $listNotRegu = DB::select($sql2);
   
        return view('rfc.MonitoringSisaSaldo',compact('pid','mitra','id_item','listRegu','listNotRegu'));
    }
}