<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DA\IoanAlkerModel;
use DB;

date_default_timezone_set('Asia/Makassar');

class IoanAlkerController extends Controller
{

  protected $inputPhotos = [
    'Splicer',
    'One_Click_Cleaner',
    'Fiber_Stripper',
    'OTDR',
    'Testphone',
    'LAN_Tester',
    'OPM',
    'Light_Source_Optic',
    'Visual_Fault_Locator',
    'Tangga_Teleskopik',
    'Toolkit_Set',
    'HP_Android',
    'Paket_Internet',
    'Body_Harness',
    'Crimping_Tools',
    'Tone_Checker',
    'LSA_Plus',
    'Seragam_IndiHome',
    'Tas_Punggung',
    'Jas_Hujan',
    'KBM_Roda2',
    'BBM_Roda2',
    'Power_Bank_Valins',
    'ID_Card',
    'Optical_Fiber_Ranger_(MiniOTDR)'
  ];

  public function index()
  {
    $data = IoanAlkerModel::getAll();
    return view('ioanalker.index', compact('data'));
  }

  public function lapor()
  {
    $auth = session('auth');
    $get_status = [
      (object)['id' => 'Sesuai', 'text' => 'Sesuai'],
      (object)['id' => 'Tidak_Sesuai', 'text' => 'Tidak_Sesuai'],
      (object)['id' => 'Tidak_Ada', 'text' => 'Tidak_Ada'],
      (object)['id' => 'Rusak', 'text' => 'Rusak'],
      (object)['id' => 'Hilang', 'text' => 'Hilang']
    ];

    $get_tahun = [
      (object)['id' => '2014', 'text' => '2014'],
      (object)['id' => '2015', 'text' => '2015'],
      (object)['id' => '2016', 'text' => '2016'],
      (object)['id' => '2017', 'text' => '2017'],
      (object)['id' => '2018', 'text' => '2018'],
      (object)['id' => '2019', 'text' => '2019'],
      (object)['id' => '2020', 'text' => '2020'],
      (object)['id' => '2021', 'text' => '2021'],
      (object)['id' => '2022', 'text' => '2022'],
      (object)['id' => '2023', 'text' => '2023'],
    ];
    
    $inputPhotos = $this->inputPhotos;
    $data = IoanAlkerModel::getByPic($auth->id_karyawan);
    return view('ioanalker.lapor', compact('auth', 'data', 'get_status', 'get_tahun', 'inputPhotos'));
  }

  public function laporSave(Request $req)
  {
    IoanAlkerModel::laporSave($req);

    return back()->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Menyimpan Laporan']
    ]);
  }

  public function form($nik, $alker)
  {
    $data = DB::table('ioan_alker_sarker')->where('nik_pic', $nik)->where('alker', $alker)->first();

    return view('ioanalker.form', compact('nik', 'alker', 'data'));
  }

}
?>
