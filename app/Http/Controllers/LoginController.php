<?php

namespace App\Http\Controllers;

use App\DA\ApiModel;
use App\DA\BotModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Telegram;
use Validator;
use DB;

date_default_timezone_set('Asia/Makassar');

class LoginController extends Controller
{
  public function loginPage()
  {
    return view('loginNew');
  }

  public static function bulk_register_sales()
  {
    $data = DB::table('dummy_user_sales')->get();

    foreach ($data as $k => $v)
    {
      $username = str_replace(' ', '', $v->kode_sales);
      DB::table('1_2_employee')->Where('nik', $username )->delete();
      DB::table('user')->Where('id_karyawan', $username )->delete();

		  $token = md5($username . microtime());

      DB::table('1_2_employee')->insert([
        'nik'               => $username,
        'nik_amija'         => $username,
        'nama'              => $v->nama_sales,
        'chat_id'           => $v->id_telegram,
        'sub_directorat'    => "Consumer Service",
        'sub_sub_unit'      => "MULTISKILL",
        'atasan_1'          => 950224,
        'no_telp'			      => null,
        'email'				      => null,
        'status'            => "PKS",
        'status_konfirmasi' => "Valid",
        'CAT_JOB'           => "Consumer Service",
        'ACTIVE'            => 1,
        'Witel_New'         => 'KALSEL',
        'laborcode'         => preg_replace("/[^a-zA-Z0-9-]+/", '', $username ),
        'mitra_amija'       => 'NON TELKOM / MITRA',
        'updateBy'          => 20981020,
        'last_update'       => date('Y-m-d H:i:s'),
        'dateUpdate'        => date('Y-m-d H:i:s')
      ]);


		  DB::table('user')->insert([
        'id_user'				     => $username,
        'password'				   => md5($v->password),
        'level'					     => 61,
        'id_karyawan'			   => $username,
        'reg'					       => 6,
        'witel'					     => 1,
        'psb_reg'				     => 6,
        'psb_remember_token' => $token,
        'updated_user'			 => 20981020,
        'datetime_updated'	 => date('Y-m-d H:i:s')
      ]);

      print_r("$username done!\n");
    }

    print_r("all done\n");
  }

  public function authverification()
  {
    $id = Input::get('id');
    $encrypt = Input::get('encrypt');

    $check = DB::table('user')->where('id_karyawan', $id)->where('otp_encrypt', $encrypt)->first();

    if (!empty($check))
    {
      $data = $check;
    } else {
      $data = new \stdClass();
      $data->id_karyawan = null;
      $data->password = null;
      $data->otp_valid_until = null;
    }

    return view('authverification', compact('encrypt', 'data'));
  }

  public function elitetheme()
  {
    return view('elitetheme');
  }

  public function accountsetting()
  {
    $auth = session('auth')->id_karyawan;
    return view('login.accountsetting', compact('auth'));
  }

  public function accountsettingSave(Request $req)
  {
    $session = $req->session();
    $set = $session->put('theme', $req->input('theme'));

    $update = DB::table('user')->where('id_user',session('auth')->id_user)
                  ->update([
                    "theme" => session('theme')
                  ]);

     return back()->with('alerts', [
          ['type' => 'success', 'text' => 'Theme Set.'.session('theme')]
      ]);
  }

  public function cookiesystem()
  {
    $witel = Input::get('witel');
    $starclick = $kpro = $prabac = $raccoon = $tacticalpro = $ibooster = $wa_broadcast = $wa_provi = $wa_assr =  [];
    if (in_array($witel, ['KALSEL', 'BALIKPAPAN']))
    {
      $starclick = self::get_cookiesystem('starclick', $witel);
      $kpro = self::get_cookiesystem('kpro', $witel);
      $prabac = self::get_cookiesystem('prabac', $witel);
      $raccoon = self::get_cookiesystem('raccoon', $witel);
      $tacticalpro = self::get_cookiesystem('tacticalpro', $witel);
      if ($witel == 'KALSEL')
      {
        $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();
      } else {
        $ibooster_s = DB::table('cookie_systems')->where('application', 'ibooster')->where('opt', 'S')->where('witel', 'BALIKPAPAN')->first();
        $ibooster_x = DB::table('cookie_systems')->where('application', 'ibooster')->where('opt', 'X')->where('witel', 'BALIKPAPAN')->first();
      }
      $utonline = self::get_cookiesystem('utonline', $witel);
      $wa_broadcast = DB::table('whatsapp_api_login')->where('divisi', 'broadcast')->first();
      $wa_provi = DB::table('whatsapp_api_login')->where('divisi', 'provisioning')->first();
      $wa_assr = DB::table('whatsapp_api_login')->where('divisi', 'assurance')->first();

      return view('login.cookiesystem', compact('starclick', 'kpro', 'prabac', 'raccoon', 'tacticalpro', 'ibooster', 'ibooster_s', 'ibooster_x', 'ibooster_for', 'wa_broadcast', 'wa_provi', 'wa_assr', 'witel', 'utonline'));
    } else {
      print_r("Witel Kosong!");
    }
  }

  private function get_cookiesystem($app, $witel)
  {
    return DB::table('cookie_systems')->where('application', $app)->where('witel', $witel)->first();
  }

  public function cookiesystemSave(Request $req)
  {
    $app = $req->input('app');
    $witel = $req->input('witel');

    switch ($app) {
      case 'starclick':
          $username = $req->input('username');
          $password = $req->input('password');
          $page = null;
          $opt = null;
          $cookies = $req->input('cookies');
        break;
      
      case 'kpro':
          $username = null;
          $password = null;
          $page = null;
          $opt = null;
          $cookies = $req->input('cookies');
        break;

      case 'prabac':
          $username = null;
          $password = null;
          $page = null;
          $opt = null;
          $cookies = $req->input('cookies');
        break;

      case 'utonline':
        $username = null;
        $password = null;
        $page = null;
        $opt = null;
        $cookies = $req->input('cookies');
      break;
        
      case 'raccoon':
          $username = null;
          $password = null;
          $page = null;
          $opt = null;
          $cookies = $req->input('cookies');
        break;

      case 'tacticalpro':
          $username = null;
          $password = null;
          $page = null;
          $opt = null;
          $cookies = $req->input('cookies');
        break;

      case 'ibooster':
          $username = null;
          $password = null;          
          $page = $req->input('page');
          $opt = $req->input('opt');
          $cookies = $req->input('cookies');
        break;
    }

    // dd($app, $username, $password, $page, $opt, $witel, $cookies);

    DB::transaction(function () use ($app, $username, $password, $page, $opt, $witel, $cookies) {
      DB::table('cookie_systems')->where('application', $app)->where('opt', $opt)->where('witel', $witel)->update([
        'username'      => $username,
        'password'      => $password,
        'page'          => $page,
        'opt'           => $opt,
        'witel'         => $witel,
        'cookies'       => $cookies,
        'updated_name'  => session('auth')->nama,
        'updated_by'    => session('auth')->id_karyawan
      ]);
    });

    switch ($witel) {
      case 'BALIKPAPAN':
        # code...
        break;
    }

    return redirect('/cookiesystem?witel=' . $witel)->with('alerts', [
      ['type' => 'success', 'text' => 'Cookies ' . $app . ' Witel ' . $witel . ' Berhasil Disimpan.']
    ]);
  }

  public function whatsapp_save(Request $req)
  {
    DB::transaction(function () use ($req) {
      DB::table('whatsapp_api_login')->where('divisi', $req->input('divisi'))->update([
        'sender'       => $req->input('sender'),
        'status'       => $req->input('status'),
        'updated_at'   => date('Y-m-d H:i:s'),
        'updated_by'   => session('auth')->id_karyawan
      ]);
    });

    return redirect('/cookiesystem?witel=KALSEL')->with('alerts', [
      ['type' => 'success', 'text' => 'WhatsApp API untuk ' . strtoupper($req->input('divisi')) . ' Berhasil Disimpan.']
    ]);
  }

  public static function generate_otp($id)
  {
    $key = random_int(0, 999999);
    $key = str_pad($key, 6, 0, STR_PAD_LEFT);
    
    $split = $id.''.$key.''.$id;
    $encrypt = base64_encode($split);

    $check = DB::table('1_2_employee')->where('nik', $id)->first();

    $text = "<i>Don't share this secret code with anyone\n\n</i>Your OTP Code is <b>".$key."</b>\n\n<i>Valid until ".date('d/m/Y H:i:s', strtotime("+1 minutes"))." WITA</i>";

    BotModel::sendMessageTommanGO($check->chat_id, $text);

    DB::table('user')->where('id_karyawan', $id)
    ->update([
      'otp_encrypt' => $encrypt,
      'otp_code' => $key,
      'otp_login' => date('Y-m-d H:i:s'),
      'otp_valid_until' => date('Y-m-d H:i:s', strtotime("+1 minutes"))
    ]);
    
    return $key;
  }

  public static function user_log($id, $status)
  {
    DB::table('user_log')->insert([
      'id_karyawan' => $id,
      'ip' => $_SERVER['REMOTE_ADDR'],
      'status' => $status
    ]);

    print_r("user_log insert!");
  }

  public function reloadCaptcha()
  {
    $result['captcha'] = urldecode(captcha_img('math')); 
    return $result;
  }

  public function validate_login(Request $request)
  {
    $rules = [ 'captcha' => 'required|captcha' ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
      return redirect('/login')->with('alerts', [
        ['type' => 'danger', 'text' => 'Invalid Captcha!']
      ]);
    }
    $username = $request->input('login');
    $password = $request->input('password');

    // cek aktif
    $cek_active = DB::table('1_2_employee')->where('nik', $username)->first();

    if(empty($cek_active))
    {
      return redirect('/login')->with('alerts', [
        ['type' => 'danger', 'text' => 'Unknown Employee, Please Contact Admin']
      ]);
    }

    switch ($cek_active->ACTIVE) {
      case '0':
        return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'User is Not Active!']
        ]);
        break;
      
      case '2':
        return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'User is Banned!']
        ]);
        break;
    }

    // cek for suspend
    $cek_suspend = DB::SELECT('SELECT * FROM regu WHERE (nik1 = "'.$username.'" OR nik2 = "'.$username.'") AND status_team = "SUSPEND_3_DAY"');
    if (count($cek_suspend) > 0)
    {
      return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'Login Gagal, User Anda dalam Status SUSPEND.']
      ]);
    }

    if ($cek_active->chat_id == null)
    {
      return redirect('/login')->with('alerts', [
        ['type' => 'danger', 'text' => 'Harap lakukan login terlebih dahulu ke Bot Telegram']
    ]);
    }

    $result = DB::select('
        SELECT
            u.theme,
            w.nama_witel,
            u.id_user,
            u.password,
            u.id_karyawan,
            k.nama,
            r.id_regu,
            u.level,
            u.psb_remember_token,
            r.mainsector,
            u.maintenance_level,
            u.witel,
            u.psb_reg
        FROM user u
        LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
        LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
        LEFT JOIN witel w ON u.witel = w.id_witel
        WHERE
            id_user = ? AND
            password = MD5(?) AND 
            u.level <> 0',[$username,$password]);

    if (count($result) > 0)
    {
      self::user_log($username, 1);
      self::generate_otp($username);

      $check = DB::table('user')->where('id_karyawan', $username)->first();

      return redirect('/auth-verification?id='.$username.'&encrypt='.$check->otp_encrypt)->with('alerts', [
        ['type' => 'success', 'text' => 'Please input your OTP Code!']
      ]);
    } else {
      self::user_log($username, 2);

      return redirect('/login')->with('alerts', [
          ['type' => 'danger', 'text' => 'Incorrect Username or Password Entered!']
      ]);
    }
      
  }

  public function login(Request $request)
  {
    $username = $request->input('login');
    $password = $request->input('password');
    $otp_code = $request->input('digit')[1].$request->input('digit')[2].$request->input('digit')[3].$request->input('digit')[4].$request->input('digit')[5].$request->input('digit')[6];
    $otp_valid_until = $request->input('otp_valid_until');
    
    // dd($username, $password, $otp_code, $otp_valid_until);

    if (date('Y-m-d H:i:s') > $otp_valid_until)
    {
      return redirect('/login')->with('alerts', [
        ['type' => 'danger', 'text' => 'Your OTP Code is Expired in 1 Minutes!']
      ]);
    }

    // dd($username, $password, $otp_code);

    $result = DB::select('
        SELECT
            u.theme,
            w.nama_witel,
            u.id_user,
            u.password,
            u.id_karyawan,
            k.nama,
            r.id_regu,
            u.level,
            u.psb_remember_token,
            r.mainsector,
            u.maintenance_level,
            u.witel,
            u.psb_reg,
            u.otp_encrypt,
            u.otp_code,
            u.otp_login,
            u.otp_valid_until
        FROM user u
        LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
        LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
        LEFT JOIN witel w ON u.witel = w.id_witel
        WHERE
            u.id_user = ? AND
            u.password = ? AND
            u.otp_code = ? AND 
            u.level <> 0',[$username, $password, $otp_code]);

    // dd($result);

    if (count($result) > 0)
    {
      self::user_log($username, 1);

      $session = $request->session();
      $session->put('auth', $result[0]);
      $session->put('psb_reg',$result[0]->psb_reg);
      $session->put('witel',$result[0]->nama_witel);
      $session->put('theme',$result[0]->theme);

      $this->ensureLocalUserHasRememberToken($result[0]);
      return $this->successResponse($result[0]->psb_remember_token);

    } else {
      
      self::user_log($username, 2);

      return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Invalid OTP Code!']
      ]);
    }
  }

  public function logout()
  {
    Session::forget('auth');
    return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
  }

  public function register()
  {
    $get_mitra = DB::SELECT('SELECT mitra_amija as id, mitra_amija_pt as text, kat FROM mitra_amija WHERE kat <> "" ORDER BY kat ASC');

    return view('register', compact('get_mitra'));
  }

  public function registerSave(Request $req)
  {  
    $rules = array(
      'username' => 'required',
      'password' => 'required',
      'name' => 'required',
      'atasan1' => 'required',
      'level' => 'required',
      'mitra' => 'required'
    );
    $messages = [
      'username.required'  => 'Please Fill in The Fields "Username"',
      'password.required'  => 'Please Fill in The Fields "Password"',
      'name.required'  => 'Please Fill in The Fields "Name"',
      'atasan1.required'  => 'Please Fill in The Fields "NIK Leader Relation"',
      'level.required'  => 'Please Fill in The Fields "Work Position"',
      'mitra.required'  => 'Please Fill in The Fields "Mitra"'
    ];
    $input = $req->input();
    $validator = Validator::make($req->all(), $rules, $messages);
    $validator->sometimes('username', 'required', function ($input) {
       return true;
          }); 
    $validator->sometimes('password', 'required', function ($input) {
       return true;
          });
    $validator->sometimes('name', 'required', function ($input) {
       return true;
          });
    $validator->sometimes('atasan1', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('level', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('mitra', 'required', function ($input) {
      return true;
          });
      if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>FAILED !</strong> Please Fill in the Blanks']
                        ]);
    };

    $token = $this->generateRememberToken($req->input('username'));
    $check = DB::table('1_2_employee')->where('nik',$req->input('username'))->get();
    
    if (count($check) == 0) {
    DB::transaction( function() use($req,$token) {
      DB::table('1_2_employee')->insert([
        'nik'               => $req->input('username'),
        'nik_amija'         => $req->input('username'),
        'nama'              => $req->input('name'),
        'sub_directorat'    => "Consumer Service",
        'sub_sub_unit'      => "MULTISKILL",
        'atasan_1'          => $req->input('atasan1'),
        'status'            => "PKS",
        'status_konfirmasi' => "Valid",
        'CAT_JOB'           => "Consumer Service",
        'ACTIVE'            => 0,
        'Witel_New'         => "KALSEL",
        'laborcode'         => $req->input('username'),
        'mitra_amija'       => $req->input('mitra'),
        'updateBy'          => 20981020,
        'last_update'       => date('Y-m-d H:i:s'),
        'dateUpdate'        => date('Y-m-d H:i:s')
      ]);
  
      DB::table('user')->insert([
        'id_user'             => $req->input('username'),
        'password'            => md5($req->input('password')),
        'level'               => $req->input('level'),
        'id_karyawan'         => $req->input('username'),
        'witel'               => 1,
        'psb_remember_token'  => $token,
        'updated_user'        => 20981020,
        'datetime_updated'    => date('Y-m-d H:i:s')
      ]);
    });

    $nik = $req->input('username');
    $name = $req->input('name');
    $level = $req->input('level');
    $relasi = $req->input('atasan1');
    $datetime = date('Y-m-d H:i:s');
    $IP = $_SERVER['REMOTE_ADDR'];
    $chatID = "-306306083";

    Telegram::sendMessage([
      'chat_id' => $chatID,
      'parse_mode' => 'html',
      'text' => "REG SUCCESS\n\nNIK : $nik\nName : $name\nLevel : $level\nNIK Relasi : $relasi\nIP Address : $IP\n\n<i>$datetime</i>"
    ]);

    return redirect('/login')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>REGISTERED SUCCESS</strong> PLEASE CONTACT ADMIN FOR ACTIVATE THIS USER']
    ]);

  } else {

    return back()->with('alerts', [
      ['type' => 'danger', 'text' => '<strong>FAILED ! NIK ALREADY EXISTS</strong>']
    ]);

  }

  }

  private function ensureLocalUserHasRememberToken($localUser)
  {
    $token = $localUser->psb_remember_token;

    if (!$localUser->psb_remember_token) {
      $token = $this->generateRememberToken($localUser->id_user);
      DB::table('user')
      ->where('id_user', $localUser->id_user)
      ->update([
        'psb_remember_token' => $token
      ]);
      $localUser->psb_remember_token = $token;
    }

    return $token;
  }

  private function generateRememberToken($nik)
  {
    return md5($nik . microtime());
  }

  private function successResponse($rememberToken)
  {
    if (Session::has('auth-originalUrl')) {
      $url = Session::pull('auth-originalUrl');
    } else {
      $url = '/';
    }

    $response = redirect($url);
    if ($rememberToken) {
      $response->withCookie(cookie()->forever('presistent-token', $rememberToken));
    }

    return $response;
  }

  private function failureResponse()
  {
    // flash old input

  }
}
