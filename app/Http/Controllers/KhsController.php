<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\marina\Khs;
use DB;
class KhsController extends Controller
{
    public function setting($id)
    {
      $itemModif = Khs::getByProgram($id, session('witel'));
      $item = Khs::getItemAll(session('witel'));
      $data = DB::table('maintenance_jenis_order')->where('id', $id)->first();
      return view('marina.khs.setting', compact('item', 'itemModif', 'data'));
    }
    public function save(Request $req)
    {
      $data = DB::table('maintenance_mtr_program')->where('id_item', $req->id_item)->where('program_id', $req->hid)->where('witel', session('witel'))->first();
      if(!count($data)){
        DB::table('maintenance_mtr_program')->insert([
          'id_item' => $req->id_item,
          'program_id' => $req->hid,
          'witel' => session('witel')
          ]);
      }
      return redirect()->back();
    }
    public function index()
    {
      $data = DB::table('khs_maintenance')->select('khs_maintenance.*', 'jenis_khs.jenis_khs')->leftJoin('jenis_khs', 'khs_maintenance.jenis', '=', 'jenis_khs.id')->where('witel', session('witel'))->get();
      return view('marina.khs.list', compact('data'));
    }
    public function input($id)
    {
      $data = DB::table('khs_maintenance')->where('id', $id)->first();
      $jeniskhs = DB::select('select id, jenis_khs as text from jenis_khs where 1');
      return view('marina.khs.input', compact('data', 'jeniskhs'));
    }
    public function saveKhs(Request $req)
    {
      $data = DB::table('khs_maintenance')->where('id', $req->hid)->first();
      if(!$data){
        DB::table('khs_maintenance')->insert([
          'id_item' => $req->id_item,
          'uraian' => $req->uraian,
          'satuan' => $req->satuan,
          'material_telkom' => $req->material_telkom,
          'jasa_telkom' => $req->jasa_telkom,
          'material_ta' => $req->material_ta,
          'jasa_ta' => $req->jasa_ta,
          'jenis' => $req->jenis,
          'alista_designator' => $req->alista_designator,
          'witel' => session('witel')
          ]);
      }else{
        DB::table('khs_maintenance')
        ->where('id', $req->hid)
        ->update([
          'id_item' => $req->id_item,
          'uraian' => $req->uraian,
          'satuan' => $req->satuan,
          'material_telkom' => $req->material_telkom,
          'jasa_telkom' => $req->jasa_telkom,
          'material_ta' => $req->material_ta,
          'jasa_ta' => $req->jasa_ta,
          'jenis' => $req->jenis,
          'alista_designator' => $req->alista_designator,
          'witel' => session('witel')
          ]);
      }
      return redirect('/marina/khs')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan']
        ]);
    }
}