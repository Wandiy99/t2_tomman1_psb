<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DA\DatelModel;
use Illuminate\Support\Facades\DB;	

class DatelController extends Controller
{
	public function index(){
		$getAll = DatelModel::getAll();
		$auth   = session('auth');
		return view('datel.index',compact('getAll', 'auth'));
	}
	
	public function input(){
		$auth = session('auth');
		if ($auth->level==2 || $auth->level==46){
			$data = new \stdClass();
	    	$data->id = null;
		    $data->datel = null;
		    $data->sto = null;
		    $data->witel = null;
			$data->grup_sales = null;
			return view('datel.input',compact('data'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}
}
