<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class BonController extends Controller
{
  protected $photoInputs = [
    'Lokasi', 'ODP', 'Hasil_Ukur_OPM', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'LiveTV',
    'TVOD', 'Speedtest', 'Berita_Acara', 'Telephone', 'Tray_Cable', 'K3','KLEM_S', 'LABEL','Patchcore','Stopper','Breket'
  ];
  public function index()
  {
    $auth = session('auth');
    $ids=14;
    $list = DB::select('
      SELECT b.*, k.nama
      FROM bon_ims b left join karyawan k
      ON b.pj=k.id_karyawan
      WHERE pj!=0 order by b.id desc
    ');

    return view('bon.list', compact('list'));
  }

  public function input($id)
  {
    $exists = DB::select('
      SELECT *
      FROM bon_ims
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      // TODO: order by most used
      $materials = DB::select('
        SELECT
          i.id_item, i.nama_item,
          COALESCE(m.qty, 0) AS qty
        FROM item i
        LEFT JOIN
          bon_ims_material m
          ON i.id_item = m.id_item
          AND m.id_bon_ims = ? and m.position_type = 1

        WHERE 1
        ORDER BY id_item
      ', [
        $data->id
      ]);
      $nte = DB::select('
        SELECT
          m.position_key as id, n.sn as text
        FROM
          bon_ims_material m
        LEFT JOIN nte n on m.position_key = n. id
        WHERE m.id_bon_ims =? and m.position_type = 2
        ORDER BY m.position_key
      ', [
        $data->id
      ]);
      $ntes = "";
      $no =1;
      foreach($nte as $n){
        if($no ==1){
          $ntes .= $n->id;$no=2;
        }else{
          $ntes .= ','.$n->id;
        }
      }
      $registeredNte = DB::select('
      SELECT id, sn as text
      FROM nte where position_type = 1 and position_key = ?
      ',[
      $data->gudang_id
      ]);
    }

    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $karyawans = DB::select('
      SELECT id_karyawan as id,nama as text
      FROM karyawan
      WHERE witel="KALSEL" and status_kerja!="RESIGN"
    ');
    //var_dump($karyawans);
    return view('bon.input', compact('data', 'materials', 'gudangs', 'karyawans', 'ntes', 'registeredNte', 'nte'));
  }
  public function create()
  {
    $data = new \StdClass;

    $nte = new \StdClass;
    $ntes = "";
    $nte->id = null;
    $nte->text = null;
    $data->id = null;
    $data->pj = null;
    $data->gudang_id = null;
    // TODO: order by most used
    $materials = DB::select('
      SELECT id_item, nama_item, 0 AS qty
      FROM item
      WHERE 1
      ORDER BY id_item
    ');
    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');
    $karyawans = DB::select('
      SELECT id_karyawan as id,nama as text
      FROM karyawan
      WHERE witel="KALSEL" and status_kerja!="RESIGN"
    ');
    $registeredNte = DB::select('
      SELECT id, sn as text
      FROM nte where position_type = 1 and position_key = ?
    ',[
      $data->gudang_id
    ]);

    return view('bon.input', compact('data', 'materials', 'gudangs', 'karyawans','nte','registeredNte', 'ntes'));
  }
  public function getMaterialExistsGudang($id){
    $materials = DB::select('
      SELECT i.id_item,i.nama_item, 0 as qty
      FROM stok_material_gudang s
      LEFT JOIN item i on s.id_item=i.id_item
      WHERE s.id_gudang = ?
    ',[
      $id
    ]);
    return json_encode($materials);
  }
  public function save(Request $request, $id)
  {
    $ntes = explode(',', $request->input('nte'));
    $auth = session('auth');
    $materials = json_decode($request->input('materials'));
    $pj = $request->input('pj');
    $exists = DB::select('
      SELECT *
      FROM bon_ims
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];
      DB::transaction(function() use($request, $data, $auth, $materials,$pj,$ntes) {
        $this->decrementStokTeknisi($data->id);
        $this->rollbackNte($data->id);
        DB::table('bon_ims')
          ->where('id', $data->id)
          ->update([
            'ts'   => DB::raw('NOW()'),
            'updater'   => $auth->id_karyawan,
            'pj'       => $pj,
            'gudang_id' => $request->input('gudang')
          ]);

        DB::table('bon_ims_material')
          ->where('id_bon_ims', $data->id)
          ->delete();

        foreach($materials as $material) {
          DB::table('bon_ims_material')->insert([
            'id_bon_ims' => $data->id,
            'id_item' => $material->id_item,
            'qty' => $material->qty,
            'position_type' => 1,
            'position_key' =>$material->id_item
          ]);
        }
        foreach($ntes as $nte) {
          DB::table('bon_ims_material')->insert([
            'id_bon_ims' => $data->id,
            'qty' => 1,
            'position_type' => 2,
            'position_key' => $nte
          ]);
        }
        $this->updateNte($ntes, $request);
        $this->incrementStokTeknisi($pj, $materials, $request->input('gudang'));
      });
      return redirect('/bon')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Bon']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth, $materials, $pj,$ntes) {
        $insertId = DB::table('bon_ims')->insertGetId([
          'ts'      => DB::raw('NOW()'),
          'updater' => $auth->id_karyawan,
          'pj'      => $pj,
          'gudang_id' => $request->input('gudang')
        ]);
        foreach($materials as $material) {
          DB::table('bon_ims_material')->insert([
            'id_bon_ims' => $insertId,
            'id_item' => $material->id_item,
            'qty' => $material->qty,
            'position_type' => 1,
            'position_key' => $material->id_item
          ]);
        }
        foreach($ntes as $nte) {
          DB::table('bon_ims_material')->insert([
            'id_bon_ims' => $insertId,
            'qty' => 1,
            'position_type' => 2,
            'position_key' => $nte
          ]);
        }
        $this->updateNte($ntes, $request);
        $this->incrementStokTeknisi($pj, $materials, $request->input('gudang'));
      });
      return redirect('/bon')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan Bon']
      ]);
    }
  }

  public function destroy($id)
  {
    DB::transaction(function() use($id) {
      $this->decrementStokTeknisi($id);
      $this->rollbackNte($id);
      DB::table('bon_ims')
          ->where('id', [$id])->delete();
      DB::table('bon_ims_material')
          ->where('id_bon_ims', [$id])->delete();
    });
    return redirect('/bon')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }

  private function decrementStokTeknisi($id){
    $data = DB::select('
        SELECT *
        FROM bon_ims
        WHERE id = ?
    ',[
      $id
    ]);
    $bon = $data[0];
    $materials = DB::select('
        SELECT *
        FROM bon_ims_material
        WHERE id_bon_ims = ? and position_type = 1
    ',[
      $id
    ]);
    foreach($materials as $material) {
      DB::table('stok_material_teknisi')
        ->where('id_item', $material->id_item)
        ->where('id_karyawan', $bon->pj)
        ->decrement('stok', $material->qty);

      DB::table('stok_material_gudang')
        ->where('id_item', $material->id_item)
        ->where('id_gudang', $bon->gudang_id)
        ->increment('stok', $material->qty);
    }
  }
  private function incrementStokTeknisi($id, $materials, $idgudang){
    foreach($materials as $material) {
      $exists = DB::select('
        SELECT *
        FROM stok_material_teknisi
        WHERE id_item = ? and id_karyawan = ?
      ',[
      $material->id_item, $id
      ]);
      $qty = $material->qty;
      if (count($exists)) {
        $data = $exists[0];
        DB::table('stok_material_teknisi')
          ->where('id', $data->id)
          ->increment('stok', $qty);
      }else{
        DB::table('stok_material_teknisi')->insert([
            'id_karyawan' => $id,
            'id_item' => $material->id_item,
            'stok' => $material->qty
        ]);
      }
      $stokGudangExists = DB::select('
        SELECT *
        FROM stok_material_gudang
        WHERE id_item = ? and id_gudang = ?
      ',[
      $material->id_item, $idgudang
      ]);
      if(count($stokGudangExists)){
        DB::table('stok_material_gudang')
          ->where('id_item', $material->id_item)
          ->where('id_gudang', $idgudang)
          ->decrement('stok', $material->qty);
      }
      else{
        $insertId = DB::table('stok_material_gudang')->insertGetId([
          'id_gudang'      => $idgudang,
          'id_item' => $material->id_item
        ]);
         DB::table('stok_material_gudang')
          ->where('id', $insertId)
          ->decrement('stok', $material->qty);
      }
    }
  }

  private function updateNte($ntes, $req){
    foreach($ntes as $nte){
      DB::table('nte')
        ->where('id', $nte)
        ->update([
        'position_type'   => 2,
        'position_key'   => $req->input('pj')
      ]);
    }
  }

  private function rollbackNte($id){
    $ntes = DB::select('
      SELECT m.*,b.gudang_id
      FROM bon_ims_material m
      LEFT JOIN bon_ims b on m.id_bon_ims = b.id
      WHERE m.position_type = 2 and m.id_bon_ims = ?
    ',[
      $id
    ]);
    foreach($ntes as $nte){
      DB::table('nte')
        ->where('id', $nte->position_key)
        ->update([
        'position_type'   => 1,
        'position_key'   => $nte->gudang_id
      ]);
    }
  }
  public function getMaterialOutstanding($id){
    $materials = DB::select('
      SELECT id_item as id, stok as text
      FROM stok_material_teknisi
      WHERE id_karyawan = ?
    ',[
      $id
    ]);
    $ntes = DB::select('
      SELECT sn as id, jenis_nte as text
      FROM nte
      WHERE position_key = ?
    ',[
      $id
    ]);
    $result = array_merge($materials, $ntes);
    return json_encode($result);
  }

  public function BonInstalled($date){
	  $get_regu = DB::SELECT('
	  	SELECT
	  		a.id_regu,
        b.uraian,
        d.area
	  	FROM
				dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN mdf d ON c.sto = d.mdf
			WHERE
				date(a.tgl) LIKE "'.$date.'%"
      GROUP BY a.id_regu
	  ');

    $get_area = DB::SELECT('
      SELECT
        *
      FROM
        area
    ');

    $lastRegu = '';
    $listTim  = '';
    $lastArea = '';
    $data     = array();
    $get_laporan = DB::SELECT('
      SELECT
        a.id_regu,
        d.area,
        e.uraian,
        b.status_laporan,
        a.Ndem,
        b.id as id_laporan,
        a.id
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN mdf d ON c.sto = d.mdf
      LEFT JOIN regu e ON a.id_regu = e.id_regu
      WHERE
        date(a.tgl) LIKE "'.$date.'%" AND
        b.status_laporan IN (1,5)
      ORDER BY b.status_laporan
    ');

    foreach ($get_laporan as $laporan) :
      $nte = $this->getLaporanNte($laporan->id_laporan);
      if ($laporan->id_regu <> $lastRegu){
        $data[$laporan->area][$laporan->id_regu][] = array('nte'=>$nte, 'id'=>$laporan->id,'SC'=>$laporan->Ndem,'status_laporan' =>$laporan->status_laporan);
      } else {
        $data[$laporan->area][$laporan->id_regu] = array('nte'=>$nte,'id'=>$laporan->id, 'SC'=>$laporan->Ndem, 'status_laporan' =>$laporan->status_laporan);
        $lastRegu = $laporan->id_regu;
      }
    endforeach;
    $photoInputs = $this->photoInputs;
    return view('bon.table',compact('get_regu','data','get_area','photoInputs'));
  }

  public function getLaporanNte($id_laporan){
    $get_nte = DB::SELECT('
      SELECT
        *
      FROM nte a
      LEFT JOIN psb_laporan_material b ON a.id = b.id_item AND b.psb_laporan_id = "'.$id_laporan.'"
      WHERE
        b.type=2
    ');
    $listNte = '';
    foreach ($get_nte as $nte){
      $listNte .= "<span class='label label-default'>".$nte->sn."</span><br />";
    }
    return $listNte;
  }

}
