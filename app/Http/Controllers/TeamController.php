<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\DA\TeamModel;
use App\DA\EmployeeModel;

class TeamController extends Controller
{

	public static function index(){
		$auth = session('auth');
		if (in_array($auth->level, [2, 15, 46]))
		{
			$getregu = TeamModel::getRegu(session('witel'));
			return view('team.index',compact('getregu'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public static function input()
	{
		$auth = session('auth');
		if (in_array($auth->level, [2, 15]))
		{
			$data = new \stdClass();
	    	$data->id_regu = null;
			$data->uraian = null;
			$data->sto = null;
			$data->ket_bantek = null;
		    $data->mitra = null;
		    $data->nama_mitra = null;
		    $data->nik1 = null;
		    $data->nik2 = null;
			$data->mainsector = null;
			$data->area = null;
			$data->fitur = null;
		    $data->crewid = null;
		    $data->kemampuan = null;
		    $data->status_team = NULL;
		    $data->status_alker = null;
		    $data->spesialis_id_regu = null;
			// $karyawan = EmployeeModel::getAll();
			$getStatus = TeamModel::distinctStatus();
			$mitra_Maintenance = TeamModel::mitra_Maintenance();
			$mainsector = TeamModel::mainsector();
			$get_status_team = DB::table('status_team')->get();
			$get_status_alker = DB::table('status_alker')->get();
			$get_sto_regu = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->where('witel', 'KALSEL')->get();
			$get_team_prov_marina = DB::table('regu')->select('id_regu as id', 'uraian as text')->where('spesialis', 'PROV')->where('ACTIVE', '1')->get();
			return view('team.input',compact('data','karyawan','getStatus','mitra_Maintenance','mainsector','get_status_team','get_status_alker','get_sto_regu', 'get_team_prov_marina'));
		}
		else{
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public static function store(Request $r){
		if ($r->input('uraian')!=""){
			//data tidak kosong
			$check = TeamModel::checkRegu($r->input('uraian'));

			if (count($check)>0){
				$type = "danger";
				$message = "FAILED ! GROUP NAME WAS USED";
			} else {
				$save = TeamModel::store($r);
				$type = "success";
				$message = "SUCCESS !";
			}
		} else {
				$type = "danger";
				$message = "FAILED ! GROUP NAME IS EMPTY";
		}
		return redirect('/team')->with('alerts', [
			['type' => $type, 'text' => '<strong>'.$message.'</strong>']
		]);
	}

	public static function edit($id)
	{
		$auth = session('auth');
		if (in_array($auth->level, [2, 15]))
		{
			$data = TeamModel::detailRegu($id);
			// $karyawan = EmployeeModel::getAll();
			$getStatus = TeamModel::distinctStatus();
			$mitra_Maintenance = TeamModel::mitra_Maintenance();
			$mainsector = TeamModel::mainsector();
			$get_status_team = DB::table('status_team')->get();
			$get_status_alker = DB::table('status_alker')->get();
			$get_sto_regu = DB::table('maintenance_datel')->select('sto as id', 'sto as text')->where('witel', 'KALSEL')->get();
			$get_team_prov_marina = DB::table('regu')->select('id_regu as id', 'uraian as text')->where('spesialis', 'PROV')->where('ACTIVE', '1')->orderBy('uraian', 'DESC')->get();
			return view('team.input',compact('data','karyawan','getStatus','mitra_Maintenance','mainsector','get_status_team','get_status_alker','get_sto_regu','get_team_prov_marina'));
		} else {
			echo "Banyaki Bawa Beibadah Ja Parak Kiamat !!!" ;
		}
	}

	public function delete($id){
		$query = TeamModel::deleteTeam($id);
		return redirect('/team')->with('alerts',[['type' => 'success', 'text' => 'Berhasil Menghapus Tim !']]);
	}

	public static function update(Request $r){
		$auth = session('auth');
		if ($r->input('uraian')==""){
			$type = "danger";
			$message = "FAILED ! GROUP NAME IS EMPTY";
		} else {
			if ($auth->level==2) {
				$update = TeamModel::update_2($r);
			} else {
				$update = TeamModel::update($r);
			}
			$type = "success";
			$message = "SUCCESS !";
		}
		return redirect('/team')->with('alerts', [
			['type' => $type, 'text' => '<strong>'.$message.'</strong>']
		]);
	}

	public static function disable($id){
		TeamModel::disable($id);
		return redirect('/team')->with('alerts',[
		 ['type' => 'success','text' => '<strong>SUCCESS ! '.$id.' DISABLED</strong>']
		]);
	}

}
