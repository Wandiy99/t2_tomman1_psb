<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Telegram;
use Validator;
Use App\DA\TicketingModel;

class TicketingController extends Controller
{
  function index(Request $req)
  {
    $this->validate($req,[
        'customer' => 'numeric',
    ]);

    $customer = $req->input('customer');
    $ibooster = '';
    $gamas = '';
    $Calling_Station_Id = '';
    $get_history_order_x = TicketingModel::get_history_order_x($customer);
    if ($req->input('customer')<>""){
      $get_customer_info = TicketingModel::get_customer_info($customer);
      $get_history_order = TicketingModel::get_history_order($customer);
      // dd($get_customer_info, $get_history_order);

      if ($get_customer_info->orderName != null)
      {
        $customer_info = $get_customer_info;
          $check_fitur = DB::table('fitur')->first();
          if ($check_fitur->ibooster == 1){
            #$resultIbooster = $this->grabIboosterbyIN($getData->Incident,'dispatch');
            $ibooster = $this->grabIbooster($customer,0,'customer_info');
          } else {
            $ibooster = "";
            #skip grabIbooster
          }
          // $data =  (object) $ibooster[0];
          // $Calling_Station_Id = substr($data->Calling_Station_Id, 0,29);
          // $query_gamas = DB::SELECT('SELECT * FROM data_nossa_1 a WHERE a.Summary LIKE "%'.$Calling_Station_Id.'%" ');
          // if (count($query_gamas)>0){
          //   $gamas = "IMPACT GAMAS";
          // }
      }
      else{
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Tidak Ditemukan !']]);
      }
    }
    return view('ticketing.home',compact('customer','customer_info','get_history_order','get_history_order_x','ibooster','gamas','Calling_Station_Id'));
  }

  public function newIndexPublic(Request $req)
  {
    $PublicIP = $_SERVER['REMOTE_ADDR'];
    $json     = file_get_contents("http://ip-api.com/json/$PublicIP");
    $json     = json_decode($json, true);
    $country  = $json['country'];
    $region   = $json['regionName'];
    $city     = $json['city'];

    // dd($country, $region, $city);
    return view('ticketing.newIndexPublic', compact('country', 'region', 'city'));
  }

  public function indexPublic(Request $req)
  {
    if ($req->has('inet'))
    {
      $cari = $req->input('inet');

      $sql = 'SELECT * 
          FROM Data_Pelanggan_Starclick
          WHERE ndemSpeedy = "'.$cari.'" OR ndemPots = "'.$cari.'" OR internet = "'.$cari.'" ORDER BY orderDate DESC LIMIT 1';

      $getSql = DB::SELECT($sql);

      if (count($getSql) == 1)
      {
        $data = $getSql[0];
        
        $checkOrder = DB::SELECT('
        SELECT
        dt.id as id_dt,
        dt.Ndem,
        roc.no_speedy,
        dn1l.Service_No,
        r.uraian,
        dn1l.Reported_Date,
        dt.created_at,
        roc.tglOpen,
        pl.status_laporan,
        pls.laporan_status
        FROM dispatch_teknisi dt
        LEFT JOIN roc ON dt.Ndem = roc.no_tiket
        LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        (roc.no_speedy = "'.$data->internet.'" OR dn1l.Service_No = "'.$data->internet.'" OR dn1l.Service_No = "'.$data->ndemPots.'") AND
        (pl.status_laporan NOT IN ("1","88") OR pl.status_laporan IS NULL)
        ORDER BY dt.created_at DESC
        ');

        if (count($checkOrder) == 1)
        {
          $cek = $checkOrder[0];

          if ($cek->laporan_status == null) {
            $status = '<strong>BELUM DIKERJAKAN</strong> OLEH';
          } else {
            $status = '<strong>'.$cek->laporan_status.'</strong> DIKERJAKAN OLEH';
          }
          
          if ($cek->uraian == null) {
            $tim = '<i>NULL</i>';
          } else {
            $tim = $cek->uraian;
          }


          if ($cek->uraian == null && $cek->laporan_status == null) {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>MAAF</strong> PENGADUAN SUDAH DIBUAT DENGAN NO ORDER <strong>'.$cek->Ndem.'</strong> STATUS SEDANG DIKERJAKAN OLEH TIM LOGIC SISTEM DAN AKAN SEGERA DI KIRIM KE PETUGAS LAPANGAN ']
            ]);
          } else {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>MAAF</strong> PENGADUAN SUDAH DIBUAT DENGAN NO ORDER <strong>'.$cek->Ndem.'</strong> STATUS '.$status.' '.$tim.'']
            ]);
          }
        }
      } else {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>MAAF</strong> NO INTERNET ANDA <strong>TIDAK TERDAFTAR</strong> DI DATA KAMI, SILAHKAN CEK KEMBALI. TERIMAKASIH']
        ]);
      }
    }

    $get_keluhan = DB::table('keluhan')->get();

    $get_area_alamat = DB::SELECT('SELECT kode_area as id, area_alamat as text FROM area_alamat');

    return view('ticketing.homePublic',compact('getSql','get_keluhan','data','cari','checkOrder','cek','get_area_alamat'));
  }

  public function indexPublicSave(Request $req)
  {
    date_default_timezone_set('Asia/Makassar');
    $rules = array(
      'Service_No' => 'required',
      'Customer_Name' => 'required',
      'Contact_Name' => 'required',
      'Contact_Phone' => 'required',
      'Workzone' => 'required',
      'alamat' => 'required',
      'JENIS_GGN' => 'required',
      'Service_Type'  =>  'required',
      'gangguan'  =>  'required',
      'Incident_Symptom'  =>  'required'
    );
    $messages = [
      'Service_No.required'  => 'Silahkan isi kolom "No Internet"',
      'Customer_Name.required'  => 'Silahkan isi kolom "Nama Pelanggan"',
      'Contact_Name.required'  => 'Silahkan isi kolom "Nama Kontak"',
      'Contact_Phone.required'  => 'Silahkan isi kolom "No Handphone"',
      'Workzone.required'  => 'Silahkan isi kolom "Area Alamat"',
      'alamat.required'  => 'Silahkan isi kolom "Alamat Lengkap"',
      'JENIS_GGN.required'  => 'Silahkan pilih "Jenis Gangguan"',
      'Service_Type.required'  => 'Silahkan pilih "Kategori Layanan"',
      'gangguan.required'  => 'Silahkan pilih "Kategori Gangguan"',
      'Incident_Symptom.required'  => 'Silahkan isi kolom "Deskripsi Keluhan"'
    ];

    $validator = Validator::make($req->all(), $rules, $messages);
    $validator->sometimes('Service_No', 'required', function ($input) {
       return true;
          }); 
    $validator->sometimes('Customer_Name', 'required', function ($input) {
       return true;
          });
    $validator->sometimes('Contact_Name', 'required', function ($input) {
       return true;
          });
    $validator->sometimes('Contact_Phone', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('Workzone', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('alamat', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('JENIS_GGN', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('Service_Type', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('gangguan', 'required', function ($input) {
      return true;
          });
    $validator->sometimes('Incident_Symptom', 'required', function ($input) {
      return true;
          });

    if ($validator->fails()) {
      return redirect()->back()
        ->withInput($req->input())
            ->withErrors($validator)
            ->with('alerts', [
              ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Membuat Pengaduan :( Silahkan Isi Kolom Wajib']
            ]);
    };

    $inet = $req->input('Service_No');
    $voice = $req->input('ndemPots');

    $get_validasi = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick WHERE ndemSpeedy = "'.$inet.'" OR ndemPots = "'.$voice.'" OR internet = "'.$inet.'" ORDER BY orderDate DESC LIMIT 1');

    if (count($get_validasi) == 0){
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>MAAF</strong> NO INTERNET ANDA <strong>TIDAK TERDAFTAR</strong> DI DATA KAMI, SILAHKAN CEK KEMBALI. TERIMAKASIH']
      ]);
    }

    $checkOrder = DB::SELECT('
      SELECT
      dt.id as id_dt,
      dt.Ndem,
      roc.no_speedy,
      dn1l.Service_No,
      r.uraian,
      dn1l.Reported_Date,
      dt.created_at,
      roc.tglOpen,
      pl.status_laporan,
      pls.laporan_status
      FROM dispatch_teknisi dt
      LEFT JOIN roc ON dt.Ndem = roc.no_tiket
      LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      (roc.no_speedy = "'.$inet.'" OR dn1l.Service_No = "'.$inet.'" OR dn1l.Service_No = "'.$voice.'") AND
      (pl.status_laporan NOT IN ("1","88") OR pl.status_laporan IS NULL) AND
      (dt.tgl = "'.date('Y-m-d').'" OR dt.tgl IS NULL)
      ');

      if (count($checkOrder) == 1){
      
      $cek = $checkOrder[0];

      if ($cek->laporan_status == null) {
        $status = '<strong>BELUM DIKERJAKAN</strong> OLEH';
      } else {
        $status = '<strong>'.$cek->laporan_status.'</strong> DIKERJAKAN OLEH';
      }
      
      if ($cek->uraian == null) {
        $tim = '<i>NULL</i>';
      } else {
        $tim = $cek->uraian;
      }

      if ($cek->uraian == null && $cek->laporan_status == null){
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>MAAF</strong> PENGADUAN SUDAH DIBUAT DENGAN NO ORDER <strong>'.$cek->Ndem.'</strong> STATUS SEDANG DIKERJAKAN OLEH TIM LOGIC SISTEM DAN AKAN SEGERA DI KIRIM KE PETUGAS LAPANGAN ']
        ]);
      } else {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => '<strong>MAAF</strong> PENGADUAN SUDAH DIBUAT DENGAN NO ORDER <strong>'.$cek->Ndem.'</strong> STATUS '.$status.' '.$tim.'']
        ]);
      }
    }

    $INT = 0;
    
    DB::transaction(function() use($req, &$INT) {
      $ID_INC = DB::table('data_nossa_1_log')->insertGetId(['Customer_Name' => $req->input('Customer_Name')]);
      $INT = 'INT'.$ID_INC;
      DB::table('data_nossa_1_log')->where('ID_INCREMENT', $ID_INC)->update([
        'ID'    => $ID_INC,
        'Incident' => $INT,
        'Customer_Name' => $req->input('Customer_Name'),
        'Contact_Name' => $req->input('Contact_Name'),
        'Contact_Phone' => $req->input('Contact_Phone'),
        'Segment_Status' => 'DCS',
        'Summary' => '[OPEN ORDER] '.$req->input('Service_No').' '.$req->input('gangguan').' '.$req->input('Incident_Symptom'),
        'Owner_Group' => 'TA HD WITEL KALSEL',
        'Source' => 'TOMMAN',
        'Service_ID' => $req->input('ndemPots').'_'.$req->input('Service_No'),
        'Service_Type' => $req->input('Service_Type'),
        'Datek' => $req->input('Datek'),
        'Reported_Date' => date('Y-m-d H:i:s'),
        'Reported_Datex' => date('Y-m-d H:i:s'),
        'Workzone' => $req->input('Workzone'),
        'Witel' => 'KALSEL (BANJARMASIN)',
        'Regional' => 'REG-6',
        'Incident_Symptom' => $req->input('gangguan').' '.$req->input('Incident_Symptom'),
        'JENIS_GGN' => $req->input('JENIS_GGN'),
        'ODP' => $req->input('Datek'),
        'user_created' => 'OPEN',
        'no_internet'  => $req->input('Service_No'),
        'Service_No' => $req->input('Service_No'),
        'alamat'  => $req->input('alamat'),
        'ncliOrder' => $req->input('ncliOrder')
      ]);
    });

    $messaggio = "Open Ticket Tomman\n";
    $messaggio .= "===================\n";
    $messaggio .= "[ OPEN ORDER BY CUSTOMER ]\n";
    $messaggio .= "Reported_Date : ".date('Y-m-d H:i:s')."\n";
    $messaggio .= "Kategori :   ".$req->input('JENIS_GGN')."\n";
    $messaggio .= "Incident : ".$INT."\n";
    $messaggio .= "Segment_Status : DCS\n";
    $messaggio .= "Customer_Name : ".$req->input('Customer_Name')."\n";
    $messaggio .= "Contact_Name : ".$req->input('Contact_Name')."\n";
    $messaggio .= "Contact_Phone : ".$req->input('Contact_Phone')."\n";
    $messaggio .= "Service_No : ".$req->input('Service_No')."\n";
    $messaggio .= "Service_ID : ".$req->input('ndemPots').'_'.$req->input('Service_No')."\n";
    $messaggio .= "Order_NCLI : ".$req->input('ncliOrder')."_".$req->input('Service_No')."_INTERNET\n";
    $messaggio .= "Datek : ".$req->input('Workzone')."_".$req->input('Datek')."\n";
    $messaggio .= "Incident_Symptom : ".$req->input('gangguan').' '.$req->input('Incident_Symptom')."\n";
    $messaggio .= "Address : ".$req->input('alamat')."\n";

    // grup sm tl hd assurance
    $chatID = "-1001666568617";

      Telegram::sendMessage([
      'chat_id' => $chatID,
      'text' => $messaggio
    ]);

    // grup plasa
    // $chatID = "-8383667";

    //  Telegram::sendMessage([
    //   'chat_id' => $chatID,
    //   'text' => $messaggio
    // ]);

    $check_fitur = DB::table('fitur')->first();
    $sto = $req->input('Workzone');
    if ($sto == "KDG" || $sto == "PGT" || $sto == "TKI" || $sto == "KPL" || $sto == "RTA" || $sto == "BRI" || $sto == "PLE" || $sto == "BTB") {
      $odp = $sto;
    } else {
      $substr_odp = (substr($req->input('Datek'),4,7));
      $exp_odp = (explode("/",$substr_odp));
      $odp = $exp_odp[0];
    }
    $check_tim = DB::SELECT('SELECT * FROM regu r WHERE r.fitur = "ACTIVE" AND r.area LIKE "%'.$odp.'%" ');
    if ($check_fitur->disp_assurance==1 && $odp<>NULL && (count($check_tim)>0)) {
      return redirect('/assurance/auto/dispatch/'.$odp.'/'.$INT);
    } else {
      return redirect('/online/ticketing')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Membuat Pengaduan Dengan No Order <strong>'.$INT.'</strong>. Estimasi Pengerjaan 3x24 Jam dan Pastikan No Handphone nya Aktif Ya Gan untuk Janji Temu dengan Teknisi, Terimakasih. ']
      ]);
    }
  }

  public function listPersonal(){
    $auth = session('auth');
    $query = TicketingModel::listPersonal($auth);
    return view('ticketing.list',compact('query'));
  }

  public function create($id)
  {
    $check_level = DB::SELECT('select * from level a where a.id_level = '.session('auth')->level);
    $check_level = $check_level[0];
    if ($check_level->createINT==1){
    $get_customer_info = TicketingModel::get_customer_info($id);
    $get_keluhan = DB::table('keluhan')->get();
    $get_area = DB::SELECT('SELECT kode_area as id, area_alamat as text FROM area_alamat');
    $customer_info = $get_customer_info;

    return view('ticketing.input',compact('customer_info','get_keluhan','get_area','id'));
    } else {
      echo "NOT AUTHORIZED";
    }
  }

  public function createSave(Request $req)
  {
    date_default_timezone_set('Asia/Makassar');
    $rules = array(
      'internet' => 'required',
      'orderName' => 'required',
      'alproname' => 'required',
      'sto' => 'required',
      'pelapor' => 'required',
      'kontak' => 'required',
      'catatan' => 'required',
      'alamat'  =>  'required',
      'keterangan'  =>  'required',
    );
    $messages = [
      'internet.required'  => 'Silahkan isi kolom "NDEM SPEEDY" rekan!',
      'orderName.required'  => 'Silahkan isi kolom "Nama Pelanggan" rekan!',
      'alproname.required'  => 'Silahkan isi kolom "ODP" rekan!',
      'sto.required'  => 'Silahkan isi kolom "STO" Bang!',
      'pelapor.required'  => 'Silahkan isi kolom "Pelapor" rekan!',
      'kontak.required'  => 'Silahkan isi kolom "Kontak" rekan!',
      'catatan.required'  => 'Silahkan isi kolom "Catatan" rekan!',
      'alamat.required'  => 'Silahkan isi kolom "Alamat" rekan!',
      'keterangan.required'  => 'Silahkan isi kolom "Keterangan" rekan!',
    ];
    $input = $req->input();
    $validator = Validator::make($req->all(), $rules, $messages);
    $validator->sometimes('ndemSpeedy', 'required', function ($input) {
              return true;
    });
    $validator->sometimes('orderName', 'required', function ($input) {
              return true;
    });
    // $serviceType = '';
    // if($req->input('ndemSpeedy')==''){
    //       $serviceType = 'Voice';
    // };
    if ($req->input('orderName')==""){
      echo "kosong";
    }
      if ($validator->fails()) {
            return redirect()->back()
                ->withInput($req->input())
                        ->withErrors($validator)
                        ->with('alerts', [
                          ['type' => 'danger', 'text' => '<strong>GAGAL</strong> menyimpan laporan']
                        ]);
    };

    $auth = session('auth');
    $getSer = DB::table('data_nossa_1_log')->where('Service_No', $req->input('Service_No'))->whereDate('Reported_Date',date('Y-m-d'))->first();
    if(count($getSer) == 0)
    {
        $Reported_Date = date('Y-m-d H:i:s');
        $INT = 0;

        DB::transaction(function() use($req, $auth, $Reported_Date, &$INT) {

          $ID_INC = DB::table('data_nossa_1_log')->insertGetId(['Customer_Name' => $req->input('orderName')]);
          $INT = 'INT'.$ID_INC;
          
          DB::table('data_nossa_1_log')->where('ID_INCREMENT', $ID_INC)->update([
            'ID'  => $ID_INC,
            'Incident' => $INT,
            'Customer_Name' => $req->input('orderName'),
            'Contact_Name' => $req->input('pelapor'),
            'Contact_Phone' => $req->input('kontak'),
            'Segment_Status' => $req->input('segment_status'),
            'Summary' => $req->input('ndemPots').'_'.$req->input('ndemSpeedy').' '.$req->input('keluhan').' '.$req->input('catatan'),
            'Owner_Group' => 'TA HD WITEL KALSEL',
            'Source' => 'TOMMAN',
            'Service_ID' => $req->input('ndemPots').'_'.$req->input('ndemSpeedy'),
            'Service_Type' => $req->input('kat_layanan'),
            'Datek' => $req->input('alproname'),
            'Reported_Date' => $Reported_Date,
            'Reported_Datex' => $Reported_Date,
            'Workzone' => $req->input('sto'),
            'Witel' => 'KALSEL (BANJARMASIN)',
            'Regional' => 'REG-6',
            'Incident_Symptom' => $req->input('keluhan').' '.$req->input('catatan'),
            'MANJA' => $req->input('tglManja').' '.$req->input('jamManja'),
            'JENIS_GGN' => $req->input('kat_gangguan'),
            'ODP' => $req->input('alproname'),
            'user_created' => $auth->id_karyawan,
            'no_internet'  => $req->input('internet'),
            'Service_No' => $req->input('internet'),
            'alamat'  => $req->input('alamat'),
            'ncliOrder' => $req->input('ncliOrder')
          ]);


          $hvc = $plasa = $sqm = $sales = $helpdesk = $monet = 0;
          if (@$req->keterangan['hvc'] == 'YES')
          {
            $hvc = 'YES';
          }
          if (@$req->keterangan['plasa'] == 1)
          {
            $plasa = 1;
          }
          if (@$req->keterangan['sqm'] == 1)
          {
            $sqm = 1;
          }
          if (@$req->keterangan['sales'] == 1)
          {
            $sales = 1;
          }
          if (@$req->keterangan['helpdesk'] == 1)
          {
            $helpdesk = 1;
          }
          if (@$req->keterangan['monet'] == 1)
          {
            $monet = 1;
          }
          $check_roc = DB::table('roc')->where('no_tiket', $INT)->first();
          if (count($check_roc) > 0)
          {
            DB::table('roc')->where('no_tiket', $INT)
            ->update([
              'pelanggan_hvc' => $hvc,
              'ket_plasa' => $plasa,
              'ket_sqm' => $sqm,
              'ket_sales' => $sales,
              'ket_helpdesk' => $helpdesk,
              'ket_monet' => $monet,
            ]);
          } else {
            DB::table('roc')->insert([
              'no_tiket' => $INT,
              'no_telp' => $req->input('kontak'),
              'no_speedy' => $req->input('internet'),
              'pelanggan_hvc' => $hvc,
              'ket_plasa' => $plasa,
              'ket_sqm' => $sqm,
              'ket_sales' => $sales,
              'ket_helpdesk' => $helpdesk,
              'ket_monet' => $monet,
              'tglOpen' => $Reported_Date
            ]);
          }
        });
      }

        $messaggio = "Open Ticket Tomman\n";
        $messaggio .= "===================\n";
        $messaggio .= "Created_By : ".session('auth')->nama."\n";
        $messaggio .= "Reported_Date : ".$Reported_Date."\n";
        $messaggio .= "Kategori :   ".$req->input('kat_gangguan')."\n";
        $messaggio .= "Incident : ".$INT."\n";
        $messaggio .= "Segment_Status : ".$req->input('segment_status')."\n";
        $messaggio .= "Customer_Name : ".$req->input('orderName')."\n";
        $messaggio .= "Contact_Name : ".$req->input('pelapor')."\n";
        $messaggio .= "Contact_Phone : ".$req->input('kontak')."\n";
        $messaggio .= "Service_No : ".$req->input('internet')."\n";
        $messaggio .= "Service_ID : ".$req->input('ndemPots').'_'.$req->input('ndemSpeedy')."\n";
        $messaggio .= "Order_NCLI : ".$req->input('ncliOrder')."_".$req->input('Service_No')."_INTERNET\n";
        $messaggio .= "Datek : ".$req->input('alproname')."\n";
        $messaggio .= "Incident_Symptom : ".$req->input('keluhan').' '.$req->input('catatan')."\n";
        $messaggio .= "Address : ".$req->input('alamat')."\n";

        // grup sm tl hd assurance
        $chatID = "-1001666568617";

         Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messaggio
        ]);

        // grup plasa
        // $chatID = "-8383667";

        //  Telegram::sendMessage([
        //   'chat_id' => $chatID,
        //   'text' => $messaggio
        // ]);

    $check_fitur = DB::table('fitur')->first();
    $sto = $req->input('sto');
    if($sto == "KDG" || $sto == "PGT" || $sto == "TKI" || $sto == "KPL" || $sto == "RTA" || $sto == "BRI" || $sto == "PLE" || $sto == "BTB"){
      $odp = $sto;
    }else{
      $substr_odp = (substr($req->input('alproname'),4,7));
      $exp_odp = (explode("/",$substr_odp));
      $odp = $exp_odp[0];
    }
    $check_tim = DB::SELECT('SELECT * FROM regu r WHERE r.fitur = "ACTIVE" AND r.area LIKE "%'.$odp.'%" ');
    if($check_fitur->disp_assurance==1 && $odp<>NULL && (count($check_tim)>0)){
      
      return redirect('/assurance/auto/dispatch/'.$odp.'/'.$INT);

    }else{
      
      return redirect('/ticketInfo/'.$INT)->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> membuat pengaduan dg No Tiket '.$INT]
      ]);

    }

  }

  public function dashboard(){
    $auth = session('auth');
    $query = TicketingModel::dashboard();
    $undispatch = TicketingModel::getJumlahUndispatch();
    // dd($undispatch);
    return view('ticketing.dashboard',compact('auth','query','undispatch'));
  }

  public function dashboardClose($date){
    $query = TicketingModel::dashboardClose($date);
    return view('ticketing.dashboardClose',compact('date','query'));
  }

  public function dashboardCloseList($status,$sektor,$periode){
    $getData = TicketingModel::dashboardCloseList($status,$sektor,$periode);
     $auth = session('auth');
    return view('ticketing.dashboardCloseList',compact('auth','date','getData','periode','sektor'));
  }

  public function dashboardList($type,$sektor,$periode){
    $auth = session('auth');
    $getData = TicketingModel::dashboardDetail($type,$sektor,$periode);
    return view('ticketing.dashboardList',compact('auth','type','sektor','periode','getData'));
  }

  public function ticketInfo($id){
    $query = DB::table('data_nossa_1_log')->where('Incident',$id)->get();
    if(count($query)==1){
      $customer_info = $query[0];
    } else {
      return back()->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>SORRY :(</strong> FAILED TO CREATE '.$id.' AND TRY TO SUBMIT AGAIN ']
      ]);
    }
    return view('ticketing.info',compact('id','query','customer_info'));
  }

  public function redispatch($odp,$id){
    date_default_timezone_set('Asia/Makassar');

    // cek tim
    $cek_tim = DB::SELECT('
    SELECT
      dt.id_regu,
      r.uraian as tim,
      SUM(CASE WHEN dt.Ndem <> "" THEN 1 ELSE 0 END) as jumlah
    FROM regu r
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN dispatch_teknisi dt ON r.id_regu = dt.id_regu
    WHERE
      r.area LIKE "%'.$odp.'%" AND
      r.fitur = "ACTIVE" AND
      dt.tgl = "'.date("Y-m-d").'"
    GROUP BY r.uraian
    ORDER BY jumlah ASC
    ');
    $tim = $cek_tim[0];

    // cek data
    $cek_data = DB::SELECT('
    SELECT
    dn1l.ID,
    dn1l.Service_No,
    dn1l.Incident,
    dn1l.user_created,
    dps.orderAddr
    FROM data_nossa_1_log dn1l
    LEFT JOIN Data_Pelanggan_Starclick dps ON dn1l.Service_No = dps.internet AND dps.orderStatusId = 7 AND dps.jenisPsb = "AO|BUNDLING"
    WHERE
    dn1l.Incident = "'.$id.'"
    LIMIT 1
    ');
    $data = $cek_data[0];

    // jenis order
    $jenis = DB::TABLE('data_nossa_1_log')->where('Incident',$id)->first();
    if($jenis->Source=="TOMMAN"){
      $jns = "INT";
    }else{
      $jns = "IN";
    }

    // tanggal dispatch
    $time = date("H:i:s");
    $jam = "17:00:00";
    if($time > $jam){
      $date_manja = date('Y-m-d H:i:s',strtotime("+1 days"));
      $date = date('Y-m-d',strtotime("+1 days"));
    }else{
      $date_manja = date("Y-m-d H:i:s");
      $date = date("Y-m-d");
    }

    // ibooster
    $fitur = DB::table('fitur')->first();
    if($fitur->ibooster_assurance == 1 && $data->Service_No <> "" || $data->Service_No == NULL){
      $resultIbooster = $this->grabIbooster($data->Service_No,$id,'dispatch');
      DB::table('ibooster')->insertIgnore($resultIbooster);
    }

  // insert
  if (count($tim)>0){
  DB::table('psb_laporan_log')->insert([
        'created_at'      => DB::raw('NOW()'),
        'created_by'      => 20981020,
        'status_laporan'  => 6,
        'id_regu'         => $tim->id_regu,
        'jadwal_manja'    => date("Y-m-d h:i:s"),
        'catatan'         => "DISPATCH at ".date('Y-m-d'),
        'Ndem'            => $id
      ]);
  DB::table('dispatch_teknisi_log')->insert([
        'updated_at'      => DB::raw('NOW()'),
        'updated_by'      => 20981020,
        'dispatch_by'     => "TOMMAN",
        'jadwal_manja'    => $date_manja,
        'tgl'             => $date,
        'id_regu'    	    => $tim->id_regu,
        'Ndem'            => $id
      ]);
  DB::table('roc')->insert([
        'no_tiket'        => $id,
        'loker_ta'        => 1,
        'alpro'           => "GPON",
        'streetAddress'   => $data->orderAddr
      ]);
  DB::table('dispatch_teknisi')->insert([
        'created_at'    => DB::raw('NOW()'),
        'updated_at'    => DB::raw('NOW()'),
        'updated_by'    => 20981020,
        'tgl'           => $date,
        'jadwal_manja'  => $date_manja,
        'id_regu'       => $tim->id_regu,
        'Ndem'          => $id,
        'NO_ORDER'      => $data->ID,
        'jenis_order'   => $jns,
        'dispatch_by'   => 2
      ]);

    if ($data->user_created=="OPEN"){
      return redirect('/online/ticketing')->with('alerts', [
        ['type' => 'success', 'text' => 'PENGADUAN SUDAH DIBUAT DENGAN NO ORDER <strong>'.$id.'</strong> STATUS SEDANG DIKERJAKAN OLEH TIM LOGIC SISTEM DAN AKAN SEGERA DI KIRIM KE PETUGAS LAPANGAN']
      ]);
    } else {
      return redirect('/ticketInfo/'.$id)->with('alerts',[['type'=>'success', 'text'=>'SUKSES MEMBUAT ORDER '.$id.' DAN DISPATCH OTOMATIS ']]);
    }
  }
    
  }


  public function grabIbooster($spd, $orderId, $menu)
  {
    $ibooster = DB::table('cookie_systems')->where('application', 'ibooster')->where('witel', 'KALSEL')->first();
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://10.62.165.58/home.php?page=' . $ibooster->page . '',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'nospeedy=' . $spd . '&analis=ANALISA',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ' . $ibooster->cookies . ''
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $columns = array(
      1 =>
      'ND', 'IP_Embassy', 'Type', 'Calling_Station_Id', 'IP_NE', 'ADSL_Link_Status', 'Upstream_Line_Rate', 'Upstream_SNR', 'Upstream_Attenuation', 'Upstream_Attainable_Rate', 'Downstream_Line_Rate', 'Downstream_SNR', 'Downstream_Attenuation', 'Downstream_Attainable_Rate', 'ONU_Link_Status', 'ONU_Serial_Number', 'Fiber_Length', 'OLT_Tx', 'OLT_Rx', 'ONU_Tx', 'ONU_Rx', 'Gpon_Onu_Type', 'Gpon_Onu_VersionID', 'Gpon_Traffic_Profile_UP', 'Gpon_Traffic_Profile_Down', 'Framed_IP_Address', 'MAC_Address', 'Last_Seen', 'AcctStartTime', 'AccStopTime', 'AccSesionTime', 'Up', 'Down', 'Status_Koneksi', 'Nas_IP_Address'
    );

    $dom = @\DOMDocument::loadHTML(trim($response));
    $table = $dom->getElementsByTagName('table')->item(1);
    if ($table) {
      $rows = $table->getElementsByTagName('tr');
      $result = array();
      for ($i = 3, $count = $rows->length; $i < $count; $i++) {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j < $jcount; $j++) {
          //echo $j." ";
          $td = $cells->item($j);
          if (is_object($td)) {
            $node = $td->nodeValue;
          } else {
            $node = "empty";
          }
          $data[$columns[$j]] = $node;
        }
        $data['order_id'] = $orderId;
        $data['user_created'] = @session('auth')->id_user;
        $data['menu'] = $menu;
        $result[] = $data;
      }
      return ($result);
    } else {
      print_r("Maaf, Ibooster Gangguan!\n");
      return back()->with('alerts', [['type' => 'danger', 'text' => 'Maaf, Ibooster Gangguan!']]);
    }
  }

  public static function undispatch(){
    $pesan = 'List Ticketing Undispatch'."\n";
    $pesan .= '================================'."\n";

    $list = TicketingModel::getUndispatchList();
    $listSto = TicketingModel::getStoUndispatchList();
    foreach ($listSto as $sto){
        $pesan .= $sto->Workzone."\n";
        foreach($list as $lst){
            if ($sto->Workzone==$lst->Workzone){
                $pesan .= $lst->Incident.'- '.$lst->Workzone.' - '.$lst->Reported_Date."\n";
            }
        }

        $pesan .= "\n";

    }

    echo $pesan;

    // Telegram::sendMessage([
    //   'chat_id' => '102050936',
    //   'text' => $pesan
    // ]);
  }

  public function hapusTicket($id,$order)
  {
      DB::table('data_nossa_1_log')->where('ID',$id)->delete();

      DB::table('dispatch_delete_log')->insert([
        'id_dispatch_teknisi'     => 0,
        'id_data_nossa_1_log'     => $id,
        'id_order'                => $order,
        'deleted_by'              => session('auth')->id_karyawan,
        'deleted_name'            => session('auth')->nama,
        'deleted_at'              => DB::raw('NOW()')
        ]);

      return redirect('/assurance/search')->with('alerts',[['type' => 'success', 'text' => 'Tiket Berhasil Dihapus']]);
  }

  public function dashboardListUndispacth($status)
  {
    $auth = session('auth');
    $getData = TicketingModel::dashboardDetailUndispatch($status);
    $sektor = 'UNDISPATCH';

    return view('ticketing.dashboardList',compact('auth','sektor','getData'));
  }

  public function createSQM($incident,$serviceno,$ncli)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://ibooster.telkom.co.id/res/get_ukur_indikasi.php?serviceno='.$serviceno.'&assetnum='.$ncli.'_'.$serviceno.'_INTERNET',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'assetNum='.$ncli.'_'.$serviceno.'_INTERNET&ns='.$serviceno.'&externalsystem_ticketid=&idReference=&submit=Create%20Ticket',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: NSC_wt_jcpptufs_iuuqt=ffffffff0936b83545525d5f4f58455e445a4a42378b'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $dom = @\DOMDocument::loadHTML(trim($response));
    $ticketid = $dom->getElementsByTagName('ticketid')[0];
    
    DB::transaction( function() use($ticketid,$incident,$serviceno)
    {

      if ($incident == 'api_t2')
      {

        // sugar kalsel
        DB::connection('bpp')->table('monet_sugar_treg6')->where('witel', 'KALSEL')->where('no_inet', $serviceno)->update([
          'id_ticketid'       => str_replace('IN','',$ticketid->nodeValue),
          'ticketid'          => $ticketid->nodeValue
        ]);

      } elseif ($incident == 'api_t1') {

        // Q kalsel
        DB::connection('bpp')->table('monet_treg6')->where('witel', 'KALSEL')->where('no_inet', $serviceno)->update([
          'id_ticketid'       => str_replace('IN','',$ticketid->nodeValue),
          'ticketid'          => $ticketid->nodeValue
        ]);

      } elseif ($incident == 'api_bppq') {

        // Q balikpapan
        DB::connection('bpp')->table('monet_treg6')->where('witel', 'BALIKPAPAN')->where('no_inet', $serviceno)->update([
          'id_ticketid'       => str_replace('IN','',$ticketid->nodeValue),
          'ticketid'          => $ticketid->nodeValue,
          'upload_by'         => session('auth')->id_karyawan
        ]);

      } elseif ($incident == 'api_bpps') {

        // sugar balikpapan
        DB::connection('bpp')->table('monet_sugar_treg6')->where('witel', 'BALIKPAPAN')->where('no_inet', $serviceno)->update([
          'id_ticketid'       => str_replace('IN','',$ticketid->nodeValue),
          'ticketid'          => $ticketid->nodeValue,
          'upload_by'         => session('auth')->id_karyawan
        ]);

      } else {

        DB::table('data_nossa_1_log')->where('Incident', $incident)->update([
          'id_ticketid'       => str_replace('IN','',$ticketid->nodeValue),
          'ticketid'          => $ticketid->nodeValue,
          'created_ticketid'  => session('auth')->id_karyawan
        ]);

      }
    });

    // return $response;

    return back()->with('alerts',[
      ['type' => 'success', 'text' => 'Berhasil Membuat Tiket SQM <b>'.$ticketid->nodeValue.'</b>']
    ]);
  }

  public function dashboardPlasaAssurance(Request $req)
  {
    $flag = json_encode($req->input('flag') ?? '');
    $startDate = $req->input('startDate') ?? date('Y-m-01');
    $endDate = $req->input('endDate') ?? date('Y-m-d');

    $data = TicketingModel::dashboardPlasaAssurance($flag, $startDate, $endDate);

    return view('ticketing.dashboardPlasaAssurance', compact('flag', 'startDate', 'endDate', 'data'));
  }

  public function dashboardPlasaAssuranceDetail()
  {
    $flag = Input::get('flag');
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');
    $area = Input::get('area');
    $grup = Input::get('grup');

    $data = TicketingModel::dashboardPlasaAssuranceDetail($flag, $startDate, $endDate, $area, $grup);

    return view('ticketing.dashboardPlasaAssuranceDetail', compact('flag', 'startDate', 'endDate', 'area', 'grup', 'data'));
  }

  public function createBulkTicketing(Request $req)
  {
    // $result = null;

    return view('ticketing.createBulkTicketing', compact('result'));
  }

  public function saveBulkTicketing(Request $req)
  {
    $result = null;

    if ($req->has('inets'))
    {
      $inets = str_replace(array("","\r\n"), '', $req->input('inets'));

      foreach (array_filter(explode(';', $inets)) as $value)
      {
        $check = DB::select('SELECT * FROM Data_Pelanggan_Starclick WHERE jenisPsb LIKE "AO%" AND (ndemSpeedy = "'.$value.'" OR ndemPots = "'.$value.'" OR internet = "'.$value.'") ORDER BY orderDate DESC LIMIT 1');

        if (count($check) > 0)
        {
          $data = $check[0];

          $id_inc = DB::table('data_nossa_1_log')->insertGetId(['Customer_Name' => $req->input('Customer_Name')]);
          $int = 'INT'.$id_inc;
          DB::table('data_nossa_1_log')->where('ID_INCREMENT', $id_inc)->update([
            'ID' => $id_inc,
            'Incident' => $int,
            'Customer_Name' => $data->orderName,
            'Contact_Name' => $data->orderName,
            'Contact_Phone' =>  $data->orderNotel,
            'Segment_Status' => 'DCS',
            'Summary' => '[OPEN ORDER HD-FFG] '.$data->internet.' ONT LOSS BY HD-FFG',
            'Owner_Group' => 'TA HD WITEL KALSEL',
            'Source' => 'TOMMAN',
            'Service_ID' => $data->noTelp.'_'.$data->internet,
            'Service_Type' => 'INTERNET',
            'Datek' => $data->alproname,
            'Reported_Date' => date('Y-m-d H:i:s'),
            'Reported_Datex' => date('Y-m-d H:i:s'),
            'Workzone' => $data->sto,
            'Witel' => 'KALSEL (BANJARMASIN)',
            'Regional' => 'REG-6',
            'Incident_Symptom' => 'ONT LOSS BY HD-FFG',
            'JENIS_GGN' => 'FISIK',
            'ODP' => $data->alproname,
            'user_created' => 'OPEN',
            'no_internet'  => $data->internet,
            'Service_No' => $data->internet,
            'alamat'  => $data->orderAddr,
            'ncliOrder' => $data->orderNcli
          ]);

          $messaggio = "Open Ticket Tomman\n";
          $messaggio .= "===================\n";
          $messaggio .= "[ OPEN ORDER BY HD-FFG ]\n";
          $messaggio .= "Reported_Date : ".date('Y-m-d H:i:s')."\n";
          $messaggio .= "Kategori :   FISIK\n";
          $messaggio .= "Incident : ".$int."\n";
          $messaggio .= "Segment_Status : DCS\n";
          $messaggio .= "Customer_Name : ".$data->orderName."\n";
          $messaggio .= "Contact_Name : ".$data->orderName."\n";
          $messaggio .= "Contact_Phone : ".$data->orderNotel."\n";
          $messaggio .= "Service_No : ".$data->internet."\n";
          $messaggio .= "Service_ID : ".$data->noTelp.'_'.$data->internet."\n";
          $messaggio .= "Order_NCLI : ".$data->orderNcli."_".$data->internet."_INTERNET\n";
          $messaggio .= "Datek : ".$data->sto."_".$data->alproname."\n";
          $messaggio .= "Incident_Symptom : ONT LOSS BY HD-FFG\n";
          $messaggio .= "Address : ".$data->orderAddr."\n";

          Telegram::sendMessage([
            'chat_id' => "-1001666568617",
            'text' => $messaggio
          ]);

          $result .= $value."-".$int." ... \r\n";
        }
        sleep(3);
      }

      return view('ticketing.createBulkTicketing', compact('result'));

      return redirect('/createBulkTicketing')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> Membuat Ticketing INT Secara Masal']
      ]);
    }
  }
}
