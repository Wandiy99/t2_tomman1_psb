<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\DA\EmployeeModel;
use Validator;
use Telegram;
use Illuminate\Support\Facades\Input;

date_default_timezone_set('Asia/Makassar');

class EmployeeController extends Controller
{
	public function index()
	{
		$getAll = EmployeeModel::getAll();
		$auth   = session('auth');
		return view('employee.index',compact('getAll', 'auth'));
	}

	public function dashboardKehadiran($posisi_id,$date)
	{
		$get_jml_teknisi = EmployeeModel::jml_teknisi($posisi_id);
		$get_jml_teknisi_hadir = EmployeeModel::jml_teknisi_hadir($posisi_id,$date);
		$get_jml_teknisi_hadir_approved = EmployeeModel::jml_teknisi_hadir_approved($posisi_id,$date);
		$get_jml_teknisi_hadir_not_approved = EmployeeModel::jml_teknisi_hadir_not_approved($posisi_id,$date);
		$get_jml_teknisi_sektor = EmployeeModel::jml_teknisi_sektor($posisi_id,$date);
		$get_jml_teknisi_sektor_1 = EmployeeModel::jml_teknisi_sektor_1($posisi_id,"PT1",$date);
		$getCuaca = EmployeeModel::getCuaca($date);
		return view('employee.dashboardKehadiran',compact('getCuaca','get_jml_teknisi_sektor','get_jml_teknisi_sektor_1','get_jml_teknisi_hadir_approved','get_jml_teknisi_hadir_not_approved','get_jml_teknisi','get_jml_teknisi_hadir','posisi_id','date'));
	}

	public function ListTeknisi($sektor,$posisi_id)
	{
		$list = EmployeeModel::teknisi_sektor($sektor,$posisi_id);
		// dd($list);
		return view('employee.listEmployee_2',compact('posisi','list','sektor'));
	}

	public function inputSchedule($nik, $regu_id, $sto_regu)
	{
		return view('employee.inputSchedule',compact('nik', 'regu_id', 'sto_regu'));
	}

	public function inputScheduleSave(Request $req)
	{
		// dd($req->all());
		$auth = session('auth');
		$date = date('Y-m-d');
		$cek = DB::SELECT('select * from 1_2_employee_schedule where nik = "'.$req->nik.'" AND date="'.$req->date.'"');
		if (count($cek)>0){
			DB::table('1_2_employee_schedule')
									->where('nik', $req->nik)
									->where('date',$req->date)
									->update([
										'status' => $req->status,
										'regu_id' => $req->regu_id,
										'sto_regu' => $req->sto_regu,
										'user_created' => $auth->id_karyawan,
										'checklist_addon' => $req->checklist_addon
									]);
										return redirect('/dashboardKehadiran/1/'.$date)->with('alerts', [
													['type' => 'success', 'text' => '<strong>Jadwal Berhasil di Update</strong>']
											]);
		} else {
			DB::table('1_2_employee_schedule')->insert([
				'nik' => $req->input('nik'),
				'regu_id' => $req->regu_id,
				'sto_regu' => $req->sto_regu,
				'date' => $req->date,
				'status' => $req->status,
				'checklist_addon' => $req->checklist_addon,
				'user_created' => $auth->id_karyawan
			]);
			return redirect('/dashboardKehadiran/1/'.$date)->with('alerts', [
						['type' => 'success', 'text' => '<strong>Jadwal Berhasil di Update</strong>']
				]);
		}
		// dd($req->input());
	}

	public function kehadiranTeknisiList()
	{
		$sektor = Input::get('sektor');
		$status = Input::get('status');
		$date = Input::get('date');

		$list = EmployeeModel::kehadiranList($sektor, $status, $date);

		return view('employee.listEmployee_1',compact('list','sektor','status','date'));
	}

	public function input($id){
		$getDistinctPSA = EmployeeModel::distinctPSA();
		$getDistinctCATJOB = EmployeeModel::distinctCATJOB();
		$getDistinctSubDirectorat = EmployeeModel::distinctSubDirectorat();
		// $getDistinctPosition = EmployeeModel::distinctPosition();
		// $getSQUAD = EmployeeModel::getSQUAD();
		$getMitraAmija = EmployeeModel::getMitraAmija();
		$data = EmployeeModel::getById($id);
		$getWitel = DB::SELECT('SELECT witel FROM maintenance_datel GROUP BY witel');
		$get_sto = DB::SELECT('SELECT kode_area as id, kode_area as text FROM area_alamat ORDER BY kode_area ASC');
		$getStatus = DB::SELECT('SELECT id_active as id, text_active as text FROM 1_2_employee_status');
		$getSektor = DB::SELECT('SELECT chat_id as id, title as text FROM group_telegram ORDER BY title ASC');
		$getRegu = DB::SELECT('SELECT id_regu as id, uraian as text FROM regu WHERE ACTIVE = 1 AND jenis_regu IS NULL ORDER BY uraian ASC');
		$getPosisi = DB::SELECT('SELECT position_id as id, position as text FROM 1_2_empolyee_position');
		$auth   = session('auth');
		return view('employee.input',compact('getStatus','get_sto','getWitel','getMitraAmija','getSQUAD','getDistinctPSA','getDistinctCATJOB','getDistinctSubDirectorat','getDistinctPosition', 'data', 'auth','getSektor','getPosisi','getRegu'));
	}
	public function store(Request $req)
	{
		// dd($req->input(),$req->input('skillset'));
		date_default_timezone_set("Asia/Makassar");
		$rules = array(
		'nik' => 'required',
		'nama' => 'required',
		'no_telp' => 'required',
		// 'laborcode' => 'required',
		'sto' => 'required',
		// 'id_sobi'  =>  'required',
		'atasan_1'  =>  'required',
		'cat_job'  =>  'required',
		'mitra_amija'  =>  'required',
		'nik_amija'  =>  'required',
		'witel'  =>  'required',
		'mainsector' => 'required',
		'id_regu' => 'required',
		'posisi_id' => 'required'
		);
		$messages = [
		'nik.required' => 'Silahkan isi kolom " NIK "',
		'nama.required' => 'Silahkan isi kolom " Nama "',
		'no_telp.required' => 'Silahkan isi kolom " No Handphone "',
		// 'laborcode.required'  => 'Silahkan isi kolom " Laborcode "',
		'sto.required'  => 'Silahkan pilih " STO "',
		// 'id_sobi.required' => 'Silahkan pilih " ID Sobi "',
		'atasan_1.required' => 'Silahkan pilih " Atasan 1 "',
		'cat_job.required' => 'Silahkan pilih " Cat Job "',
		'mitra_amija.required' => 'Silahkan isi kolom " Mitra Amija "',
		'nik_amija.required' => 'Silahkan isi kolom " Nik Amija "',
		'witel.required' => 'Silahkan isi kolom " Witel "',
		'mainsector.required' => 'Silahkan isi kolom " Mainsector "',
		'id_regu.required' => 'Silahkan isi kolom " Regu "',
		'posisi_id.required' => 'Silahkan isi kolom " Posisi "'
		];
		$input = $req->input();
		$validator = Validator::make($req->all(), $rules, $messages);
		$validator->sometimes('nik', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('nama', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('no_telp', 'required', function ($input) {
			return true;
		});
		// $validator->sometimes('chat_id', 'required', function ($input) {
		// 	return true;
		// 	});
		// $validator->sometimes('laborcode', 'required', function ($input) {
		// 	return true;
		// 	});
		// $validator->sometimes('tgl_lahir', 'required', function ($input) {
		// return true;
		// 	});
		// $validator->sometimes('tempat_lahir', 'required', function ($input) {
		// return true;
		// 	});
		// $validator->sometimes('alamat', 'required', function ($input) {
		// return true;
		// 	});
		$validator->sometimes('sto', 'required', function ($input) {
			return true;
		});
		// $validator->sometimes('id_sobi', 'required', function ($input) {
		// 	return true;
		// });
		$validator->sometimes('atasan_1', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('cat_job', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('mitra_amija', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('nik_amija', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('witel', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('mainsector', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('id_regu', 'required', function ($input) {
			return true;
		});
		$validator->sometimes('posisi_id', 'required', function ($input) {
			return true;
		});

		if ($validator->fails())
		{
			return redirect()->back()
				->withInput($req->input())
					->withErrors($validator)
					->with('alerts', [
						['type' => 'danger', 'text' => '<strong>GAGAL</strong> Harap Periksa Lagi']
					]);
		};

		$auth = session('auth');
		if (isset($req->id_people))
		{
		 	EmployeeModel::update($req,$auth->id_karyawan);
		 	return redirect('/employee')->with('alerts', [
				['type' => 'success', 'text' => '<strong>SUCCESS UPDATE!</strong>']
			]);
		}
		else{
			if ($req->input('nik')!="")
			{
			 	$check = DB::table('1_2_employee')->where('nik',$req->input('nik'))->get();
			 	print_r("count($check)");
			 	if (count($check)>0)
				{
				 	return back()->with('alerts', [
			        ['type' => 'danger', 'text' => '<strong>FAILED ! NIK SUDAH DIGUNAKAN</strong>']
			    ]);
			 	} else {
					EmployeeModel::store($req,$auth->id_karyawan);

					// user defaault
				 	$exists = DB::table('user')->where('id_user',$auth->id_karyawan)->get();
				 	if (count($exists) == 0)
					{
				 		DB::table('user')->insert([
				 			'id_user'		=> $req->nik,
				 			'id_karyawan'	=> $req->nik,
				 			'password'		=> md5('telkomakses'),
				 			'level'			=> '10',
				 			'witel'			=> '1',
				 			'status'		=> '0',
				 		]);
				 	};

					return redirect('/employee')->with('alerts', [
				        ['type' => 'success', 'text' => '<strong>SUCCESS INSERT!</strong>']
				    ]);
			  	}
		  	} else {
			 	return back()->with('alerts', [
		        	['type' => 'danger', 'text' => '<strong>FAILED !</strong>']
		    	]);
		  	}
		}
	}

	public static function sendReportKehadiran()
	{
		$date = date('Y-m-d', strtotime("+1 days"));
		// $date = date('Y-m-d');
		// $date_format = date('d F Y', strtotime($date));
		// dd($date, $date_format);

		// inner
		$data_inner = DB::select('
			SELECT
			gt.title as area,
			gt.area_sektor,
			(SELECT COUNT(*) FROM `1_2_employee` emp LEFT JOIN group_telegram gtl ON emp.mainsector = gtl.chat_id WHERE emp.posisi_id = 1 AND gtl.title = gt.title) as total_teknisi,
			SUM(CASE WHEN empe.pt1 = 1 AND empe.saber = 0 THEN 1 ELSE 0 END) as teknisi_pt1,
			SUM(CASE WHEN emps.checklist_addon = "1" THEN 1 ELSE 0 END) as teknisi_mo,
			SUM(CASE WHEN empe.pt1 = 0 AND empe.saber = 1 THEN 1 ELSE 0 END) as teknisi_saber
			FROM group_telegram gt
			LEFT JOIN regu r ON r.mainsector = gt.chat_id AND r.ACTIVE = 1
			LEFT JOIN `1_2_employee_schedule` emps ON r.id_regu = emps.regu_id
			LEFT JOIN `1_2_employee` as empe ON emps.nik = empe.nik
			LEFT JOIN maintenance_datel md ON emps.sto_regu = md.sto
			WHERE
			gt.area_sektor = "INNER" AND
			(DATE(emps.date) = "' . $date. '") AND emps.status = 1 OR emps.date IS NULL AND
			gt.title IS NOT NULL AND gt.ket_posisi = "PROV" AND gt.title NOT IN ("TERRITORY BASE HELPDESK KALSEL", "Tim Dedicated NTE")
			GROUP BY gt.title
		');

		// outer
		$data_outer = DB::select('
			SELECT
			md.sto as area,
			md.sektor_prov,
			(SELECT COUNT(*) FROM `1_2_employee` emp LEFT JOIN maintenance_datel mdd ON emp.sto = mdd.sto WHERE emp.posisi_id = 1 AND mdd.sto = md.sto) as total_teknisi,
			SUM(CASE WHEN empe.pt1 = 1 AND empe.saber = 0 THEN 1 ELSE 0 END) as teknisi_pt1,
			SUM(CASE WHEN emps.checklist_addon = "1" THEN 1 ELSE 0 END) as teknisi_mo,
			SUM(CASE WHEN empe.pt1 = 0 AND empe.saber = 1 THEN 1 ELSE 0 END) as teknisi_saber
			FROM maintenance_datel md
			LEFT JOIN `1_2_employee_schedule` emps ON md.sto = emps.sto_regu
			LEFT JOIN `1_2_employee` as empe ON emps.nik = empe.nik
			WHERE
			(DATE(emps.date) = "' . $date . '") AND
			emps.status = 1 OR emps.date IS NULL AND
            md.witel = "KALSEL" AND
			md.sto NOT IN ("BJM", "ULI", "KYG", "GMB")
			GROUP BY md.sto
		');

		// dd($data_inner, $data_outer);

		$messages = date('d F Y') . "\nJadwal Hadir Teknisi Provisioning\nSektor / Total Teknisi / Teknisi PT1 Hadir / Teknisi Saber Hadir / Teknisi MO Hadir\n\n";
		$all_teknisi = $all_teknisi_pt1 = $all_teknisi_saber = $all_teknisi_mo = 0;
		if (count($data_inner) > 0)
		{
			$messages .= "<b>==== INNER ====</b>\n\n";
			$total_teknisi = $total_teknisi_pt1 = $total_teknisi_saber = $total_teknisi_mo = 0;
			foreach ($data_inner as $value)
			{
				if ($value->area_sektor == 'INNER')
				{
					$total_teknisi += $value->total_teknisi;
					$total_teknisi_pt1 += $value->teknisi_pt1;
					$total_teknisi_saber += $value->teknisi_saber;
					$total_teknisi_mo += $value->teknisi_mo;

					$messages .= $value->area . " / " . $value->total_teknisi . " / " . $value->teknisi_pt1 . " / " . $value->teknisi_saber . " / " . $value->teknisi_mo . "\n";
				}
			}
			$messages .= "\n<b>TOTAL / " . $total_teknisi . " / " . $total_teknisi_pt1 . " / " . $total_teknisi_saber . " / " . $total_teknisi_mo . "</b>";

			$all_teknisi += $total_teknisi;
			$all_teknisi_pt1 += $total_teknisi_pt1;
			$all_teknisi_saber += $total_teknisi_saber;
			$all_teknisi_mo += $total_teknisi_mo;
		}

		if (count($data_outer) > 0)
		{
			$messages .= "\n\n<b>==== OUTER ====</b>\n\n";
			$total_teknisi = $total_teknisi_pt1 = $total_teknisi_saber = $total_teknisi_mo = 0;

			foreach ($data_outer as $value)
			{
				if (!in_array($value->sektor_prov, ["SEKTOR BJM", "SEKTOR KYG", "SEKTOR ULI GMB"]))
				{
					$total_teknisi += $value->total_teknisi;
					$total_teknisi_pt1 += $value->teknisi_pt1;
					$total_teknisi_saber += $value->teknisi_saber;
					$total_teknisi_mo += $value->teknisi_mo;

					$messages .= $value->area . " / " . $value->total_teknisi . " / " . $value->teknisi_pt1 . " / " . $value->teknisi_saber . " / " . $value->teknisi_mo . "\n";
				}
			}
			$messages .= "\n<b>TOTAL / " . $total_teknisi . " / " . $total_teknisi_pt1 . " / " . $total_teknisi_saber . " / " . $total_teknisi_mo . "</b>";

			$all_teknisi += $total_teknisi;
			$all_teknisi_pt1 += $total_teknisi_pt1;
			$all_teknisi_saber += $total_teknisi_saber;
			$all_teknisi_mo += $total_teknisi_mo;
		}

		$messages .= "\n\n<b>==== TOTAL TEKNISI PROVI ====</b>\n";
		$messages .= "\n<b>TOTAL / " . $all_teknisi . " / " . $all_teknisi_pt1 . " / " . $all_teknisi_saber . " / " . $all_teknisi_mo . "</b>";

		// dd($messages);
		$chatID = "-1001390723555"; // HELPDESK
		// $chatID = "-429302864"; // RING 2
		// $chatID = "401791818"; // MYSELF

		Telegram::sendMessage([
			'chat_id' => $chatID,
			'parse_mode' => 'html',
			'text' => $messages
		]);
	}

	public static function reset_token_daily()
	{
		$datetime = date('Y-m-d H:i:s');
		
		DB::table('user')->whereNotIn('id_karyawan', ["915999", "17920262", "18940469", "20981020", "94132168"])->update([
			'psb_remember_token' => null
		]);

		print_r("\nsuccess reset token $datetime\n\n");
	}

	public function update_location(Request $req)
    {
        DB::table('user')->where('id_karyawan', $req->id_karyawan)
        ->update([
            'myLastLatitude'    => $req->lat,
            'myLastLongitude'   => $req->lng,
            'myLastAccuracy'    => $req->accuracy,
            'myLastCoordUpdate' => date('Y-m-d H:i:s')
        ]);
    }
}
