<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Telegram;
use Curl;
use DB;

class FeedbackController extends Controller
{

  public function index()
  {
    $auth = session('auth');

    $list = DB::select('
      SELECT *,
      emp.nik as e_nik,
      emp.nama as e_nama,
      fd.id as f_id,
      fd.status as f_status,
      fat.nik as fat_nik,
      fat.nama as fat_nama
      FROM feedback_t2 fd
      LEFT JOIN 1_2_employee emp ON fd.user_created = emp.nik
      LEFT JOIN feedback_admin_t2 fat ON fd.admin_by = fat.nik
      LEFT JOIN feedback_status_t2 fst ON fd.status = fst.feedback_status_id
    ');

    return view('feedback.list', compact('list'));
  }
  
  public function input($id)
  {
    $exists = DB::select('
      SELECT *,
      emp.nik as e_nik,
      emp.nama as e_nama,
      fd.id as f_id,
      fd.status as f_status,
      fat.nik as fat_nik,
      fat.nama as fat_nama
      FROM feedback_t2 fd
      LEFT JOIN 1_2_employee emp ON fd.user_created = emp.nik
      LEFT JOIN feedback_admin_t2 fat ON fd.admin_by = fat.nik
      LEFT JOIN feedback_status_t2 fst ON fd.status = fst.feedback_status_id
      WHERE fd.id = ?
    ',[
      $id
    ]);
    $data = $exists[0];

    $get_status_user = DB::SELECT('SELECT feedback_status_id as id, feedback_status as text FROM feedback_status_t2 WHERE user ="1" ');
    $get_status_admin = DB::SELECT('SELECT feedback_status_id as id, feedback_status as text FROM feedback_status_t2 WHERE admin ="1" ');
    $get_admin = DB::SELECT('SELECT nik as id, nama as text FROM feedback_admin_t2');
    $get_user = DB::SELECT('SELECT nik as id, nama as text FROM 1_2_employee WHERE Witel_New = "KALSEL"');

    return view('feedback.input', compact('data','get_status_user','get_status_admin','get_admin','get_user'));
  }
  public function create()
  {

    $get_status_user = DB::SELECT('SELECT feedback_status_id as id, feedback_status as text FROM feedback_status_t2 WHERE user ="1" ');
    $get_status_admin = DB::SELECT('SELECT feedback_status_id as id, feedback_status as text FROM feedback_status_t2 WHERE admin ="1" ');
    $get_admin = DB::SELECT('SELECT nik as id, nama as text FROM feedback_admin_t2');
    $get_user = DB::SELECT('SELECT nik as id, nama as text FROM 1_2_employee WHERE Witel_New = "KALSEL"');

    $data = new \stdClass();
    $data->id = null;
    $data->judul = null;
    $data->catatan = null;
    $data->user_created = null;
    $data->admin_by = null;
    $data->status = null;
    $data->balasan = null;
    return view('feedback.input', compact('data','get_status_user','get_status_admin','get_admin','get_user'));
  }
  
  public function save(Request $request, $id)
  {
    $auth = session('auth');
    $exists = DB::select('
      SELECT *
      FROM feedback_t2
      WHERE id = ?
    ',[
      $id
    ]);

    if (count($exists)) {
      $data = $exists[0];

      DB::transaction(function() use($request, $data, $auth) {
        DB::table('feedback_t2')
          ->where('id', $data->id)
          ->update([
            'judul'         => $request->input('judul'),
            'catatan'       => $request->input('catatan'),
            'user_created'  => $request->input('user_created'),
            'status'        => $request->input('status'),
            'admin_by'      => $request->input('admin_by'),
            'balasan'       => $request->input('balasan'),
            'last_updated'  => DB::raw('NOW()'),
            'last_user'     => $auth->id_karyawan
          ]);

        date_default_timezone_set('Asia/Makassar');
        $waktu_updated = date('Y-m-d H:i:s');
        $messaggio = "Order Feedback Tomman\n";
        $messaggio .= "======================\n";
        $messaggio .= "Respon : ".session('auth')->nama."\n";
        $messaggio .= "Tanggal : ".$waktu_updated."\n";
        $messaggio .= "Dibuat oleh : ".$request->input('user_created')."\n";
        $messaggio .= "Judul : ".$request->input('judul')."\n";
        $messaggio .= "Catatan : ".$request->input('catatan')."\n";
        $messaggio .= "\n\n";
        $messaggio .= "Status : ".$request->input('status')."\n";
        $messaggio .= "Admin : ".$request->input('admin_by')."\n";
        $messaggio .= "Balasan : ".$request->input('balasan')."\n";
        
         // Helpdesk Tomman Regional 6
        $chatID = "-344123188";
         Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messaggio
        ]);

      });
      return redirect('/feedback')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>BERHASIL</strong> menyimpan data Feedback']
      ]);
    }
    else {
      DB::transaction(function() use($request, $id, $auth) {
        $insertId = DB::table('feedback_t2')->insertGetId([
            'judul'         => $request->input('judul'),
            'catatan'       => $request->input('catatan'),
            'user_created'  => $request->input('user_created'),
            'status'        => $request->input('status'),
            'admin_by'      => $request->input('admin_by'),
            'balasan'       => $request->input('balasan'),
            'last_updated' => DB::raw('NOW()'),
            'last_user'     => $auth->id_karyawan
        ]);

        date_default_timezone_set('Asia/Makassar');
        $waktu_updated = date('Y-m-d H:i:s');
        $messaggio = "Order Feedback Tomman\n";
        $messaggio .= "======================\n";
        $messaggio .= "Respon : ".session('auth')->nama."\n";
        $messaggio .= "Tanggal : ".$waktu_updated."\n";
        $messaggio .= "Dibuat oleh : ".$request->input('user_created')."\n";
        $messaggio .= "Judul : ".$request->input('judul')."\n";
        $messaggio .= "Catatan : ".$request->input('catatan')."\n";
        $messaggio .= "\n\n";
        $messaggio .= "Status : ".$request->input('status')."\n";
        $messaggio .= "Admin : ".$request->input('admin_by')."\n";
        $messaggio .= "Balasan : ".$request->input('balasan')."\n";
        
         // Helpdesk Tomman Regional 6
        $chatID = "-344123188";
         Telegram::sendMessage([
          'chat_id' => $chatID,
          'text' => $messaggio
        ]);

      });
      return redirect('/feedback')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>TERIMA KASIH</strong> Saran Dan Kritik nya']
      ]);
    }
  }
}