<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use DB;

class NteController extends Controller
{
  
  public function index()
  {
    $get_nte_jenis = DB::SELECT('SELECT * FROM nte_jenis GROUP BY nte_jenis ORDER BY nte_jenis_kat ASC');

    //
    $query_stock_nte = '
      SELECT 
      b.nama_gudang,
      ';
    foreach($get_nte_jenis as $nte_jenis){
    $query_stock_nte .= 'SUM(CASE WHEN a.jenis_nte = "'.$nte_jenis->nte_jenis.'" THEN 1 ELSE 0 END) as "'.$nte_jenis->nte_jenis_code.'",';
    }
    $query_stock_nte .= '
      count(*) as jumlah
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      WHERE
        a.position_key = 1
      GROUP BY b.id_gudang
      ';
    $stock_nte = DB::SELECT($query_stock_nte);

    //
    $query_stock_nte_teknisi = '
      SELECT 
      b.nama_gudang,
      ';
    foreach($get_nte_jenis as $nte_jenis){
    $query_stock_nte_teknisi .= 'SUM(CASE WHEN a.jenis_nte = "'.$nte_jenis->nte_jenis.'" THEN 1 ELSE 0 END) as "'.$nte_jenis->nte_jenis_code.'",';
    }
    $query_stock_nte_teknisi .= '
      count(*) as jumlah
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      LEFT JOIN psb_laporan c ON a.id_dispatch_teknisi = c.id_tbl_mj
      WHERE 
      a.position_key <> 1 AND c.id IS NULL
      GROUP BY b.id_gudang
      ';
    $stock_nte_teknisi = DB::SELECT($query_stock_nte_teknisi);

    //
    $query_stock_nte_used = '
      SELECT 
      b.nama_gudang,
      ';
    foreach($get_nte_jenis as $nte_jenis){
    $query_stock_nte_used .= 'SUM(CASE WHEN a.jenis_nte = "'.$nte_jenis->nte_jenis.'" THEN 1 ELSE 0 END) as "'.$nte_jenis->nte_jenis_code.'",';
    }
    $query_stock_nte_used .= '
      count(*) as jumlah
      FROM nte a 
      LEFT JOIN gudang b ON a.position_type = b.id_gudang  
      LEFT JOIN psb_laporan c ON a.id_dispatch_teknisi = c.id_tbl_mj
      WHERE
      a.position_key <> 1 AND c.id IS NOT NULL
      GROUP BY b.id_gudang
      ';
    $stock_nte_used = DB::SELECT($query_stock_nte_used);
    
    return view('nte.list', compact('stock_nte','stock_nte_teknisi','stock_nte_used'));
  }

  public function listGudang($nte,$gudang){

    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND b.nte_jenis_code LIKE "%'.$nte.'%"';
    }

    $query = DB::SELECT('
      SELECT *, null as jenis_layanan, null as Ndem, null as internet, null as Service_ID, null as orderName, null as Customer_Name, null as orderAddr, null as nama FROM 
        nte a
      LEFT JOIN regu r ON a.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      LEFT JOIN gudang c ON a.position_type = c.id_gudang  
      LEFT JOIN nte_jenis b ON a.jenis_nte = b.nte_jenis
      WHERE
        a.position_key = 1 AND 
        c.nama_gudang = "'.$gudang.'" 
        '.$where_nte.'
      ORDER BY a.date_created DESC
      ');
    return view('nte.listTeknisi',compact('nte','gudang','query'));    
  }

  public function listTeknisi($nte,$gudang){
    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND e.nte_jenis_code LIKE "%'.$nte.'%"';
    }
    $query = DB::SELECT('
      SELECT *,a.sn, null as jenis_layanan, null as Ndem, null as internet, null as Service_ID, null as orderName, null as Customer_Name, null as orderAddr  FROM 
        nte a 
      LEFT JOIN regu r ON a.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      LEFT JOIN 1_2_employee b ON a.petugas = b.nik 
      LEFT JOIN psb_laporan c ON a.id_dispatch_teknisi = c.id_tbl_mj
      LEFT JOIN gudang d ON a.position_type = d.id_gudang  
      LEFT JOIN nte_jenis e ON a.jenis_nte = e.nte_jenis
     WHERE 
        a.position_key <> 1 AND 
        d.nama_gudang = "'.$gudang.'" AND 
        c.id IS NULL 
        '.$where_nte.'
    ORDER BY a.tanggal_keluar DESC
      ');
    return view('nte.listTeknisi',compact('nte','gudang','query'));
  }

public function listUsed($nte,$gudang){
    if ($nte=="ALL") {
      $where_nte = '';
    } else {
      $where_nte = 'AND e.nte_jenis_code LIKE "%'.$nte.'%"';
      
    }
    $query = DB::SELECT('
      SELECT *,a.sn, dd.* FROM 
        nte a 
      LEFT JOIN regu r ON a.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      LEFT JOIN 1_2_employee b ON a.petugas = b.nik 
      LEFT JOIN psb_laporan c ON a.id_dispatch_teknisi = c.id_tbl_mj
      LEFT JOIN dispatch_teknisi dd ON c.id_tbl_mj = dd.id
      LEFT JOIN gudang d ON a.position_type = d.id_gudang  
      LEFT JOIN nte_jenis e ON a.jenis_nte = e.nte_jenis
      LEFT JOIN Data_Pelanggan_Starclick dps ON dd.NO_ORDER = dps.orderIdInteger
      LEFT JOIN data_nossa_1_log dn ON dd.NO_ORDER = dn.ID
      LEFT JOIN roc ON dd.Ndem = roc.no_tiket
     WHERE 
        a.position_key <> 1 AND 
        d.nama_gudang = "'.$gudang.'" AND 
        c.id IS NOT NULL 
        '.$where_nte.'
      ORDER BY a.tanggal_keluar DESC
      ');
    // dd($query);
    return view('nte.listTeknisi',compact('nte','gudang','query'));
  }
  
  public function input($id){
    $exist = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    if (count($exist)) {
      $data=$exist[0];
      
    } else {
      $data = new \StdClass;
    }

    $sn = DB::select('
      SELECT *
      FROM gudang_transaksi_barang g left join nte n on g.key = n.id
       where g.gudang_transaksi_id = ?
      ',[$id]);

    $gudangs = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang where 1
    ');

    return view('nte.nteMasuk', compact('gudangs', 'data', 'sn'));
  }

  public function save(Request $request, $id){
    $auth = session('auth');

    if ($request->input('transaksi') == '1'){
      $key = 1;
      $type = $request->input('gudang');
    } else {
      $key = 0;
      $type = 0;
    }

    $exist = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    if (count($exist)) {
      $data = $exist[0];
      DB::transaction(function() use($request, $data, $auth, $key, $type) {
        DB::table('gudang_transaksi')
          ->where('id', $data->id)
          ->update([
            'modified_at'   => DB::raw('NOW()'),
            'modified_by'   => $auth->id_karyawan,
            'catatan'       => $request->input('catatan'),
            'position_type' => $request->input('transaksi'),
            'gudang_id'     => $request->input('gudang'),
            'status'        => $request->input('status')
          ]);
        $ntes = DB::select('select * from gudang_transaksi_barang where gudang_transaksi_id = ?',[$data->id]);
        foreach($ntes as $nte) {
          DB::table('nte')
            ->where('id', $nte->key)
            ->update([
              'position_type' => $type,
              'position_key'  => $key,
              'status'        => $request->input('status')
            ]);
        }
      });
      
    } else {
        Excel::load($request->file('uploadstock'), function($reader) use($request, $auth , $key , $type) {
          $sns = $reader->toArray();

          $insertId = DB::table('gudang_transaksi')->insertGetId([
            'created_at'      => DB::raw('NOW()'),
            'created_by'      => $auth->id_karyawan,
            'gudang_id'       => $request->input('gudang'),
            'position_type'   => $request->input('transaksi'),
            'catatan'         => $request->input('catatan'),
            'status'          => $request->input('status')
          ]);

          foreach($sns as $sn) {
            if (count($sn) == '2') {
              if ($sn['sn'] <> null || $sn['type'] <> null) {

                $ada = DB::table('nte')->where('sn',$sn['sn'])->first();

                if (count($ada)>0) {
                  $nte = $ada;
                  $idNte = $nte->id;
                  DB::table('nte')
                  ->where('id', $nte->id)
                  ->update([
                    'position_type' => $type,
                    'position_key'  => $key,
                    'status'        => $request->input('status')
                  ]);
                } else {
                  $idNte = DB::table('nte')->insertGetId([
                  'sn'            => $sn['sn'],
                  'jenis_nte'     => $sn['type'], 
                  'position_type' => $type,
                  'position_key'  => $key,
                  'status'        => $request->input('status')
                  ]);
                }

                DB::table('gudang_transaksi_barang')->insert([
                  'gudang_transaksi_id' => $insertId,
                  'type' => 2,
                  'qty' => 1,
                  'key' => $idNte
                ]);

              } else {

              print_r("<b>GAGAL</b> Harap Periksa Kembali Format yg Dimasukan dan Refresh Halaman ini Kembali !");

              }
            } else {

              print_r("<b>GAGAL</b> Harap Periksa Kembali Format yg Dimasukan dan Refresh Halaman ini Kembali !");

            }
          }
        });

        return redirect('/nte')->with('alerts', [
          ['type' => 'success', 'text' => '<strong>SUKSES</strong> menyimpan NTE']
        ]);
    }
  }

  public static function check_nte($sn)
  {
    return DB::SELECT('
      SELECT
          nte.*,
          g.nama_gudang,
          r.uraian,
          dt.Ndem,
          nj.nte_jenis_kat
      FROM nte
      LEFT JOIN nte_jenis nj ON nte.jenis_nte = nj.nte_jenis
      LEFT JOIN gudang g ON nte.position_type = g.id_gudang
      LEFT JOIN dispatch_teknisi dt ON nte.id_dispatch_teknisi = dt.id
      LEFT JOIN psb_laporan pl ON nte.id_dispatch_teknisi = pl.id_tbl_mj
      LEFT JOIN regu r ON nte.id_regu = r.id_regu
      WHERE
          nte.sn = "'.$sn.'"
      ');
  }

  public function kembali(Request $req)
  {
    $data = [];
    if ($req->has('search_sn'))
    {
      $sn = $req->input('search_sn');

      $query = self::check_nte($sn);

      if (count($query)>0)
      {
        $data = $query[0];
      } else {
        $data = [];
      }
      
      $photoBA = ['Berita_Acara', 'BA_Digital'];
    }

    return view('nte.kembali', compact('data'));
  }

  public function kembaliSave(Request $req){
    $this->validate($req,[
      'keterangan'          => 'required'
    ],[
      'keterangan.required' => 'Pilih Keterangan!'
    ]);

    if ($req->input('typente') == NULL) {
      return redirect('/nte/return/teknisi')->with('alerts', [
        ['type' => 'danger', 'text' => '<strong>GAGAL</strong> Cari SN Terlebih Dahulu!']
      ]);
    }

    if ($req->input('keterangan') == "ReturnGudang") {
      DB::table('nte')->where('sn', $req->input('search_sn'))->update([
        'position_key'        => 1,
        'status'              => 2,
        'qty'                 => 0,
        'id_dispatch_teknisi' => 0,
        'tanggal_keluar'      => NULL,
        'petugas'             => NULL,
        'id_regu'             => NULL,
        'nik1'                => NULL,
        'nik2'                => NULL,
        'nik_scmt'            => NULL
      ]);

      if ($req->input('typente') == "typeont") {
        $check_laporan = DB::table('psb_laporan')->where('snont', $req->input('search_sn'))->update([
          'typeont'           => NULL,
          'snont'             => NULL
        ]);
      } elseif ($req->input('typente') == "typestb") {
        $check_laporan = DB::table('psb_laporan')->where('snstb', $req->input('search_sn'))->update([
          'typestb'           => NULL,
          'snstb'             => NULL
        ]);
      }

    } elseif ($req->input('keterangan') == "ReturnTeknisi") {

      DB::table('nte')->where('sn', $req->input('search_sn'))->update([
        'status'              => 2,
        'qty'                 => 0,
        'id_dispatch_teknisi' => 0
      ]);

      if ($req->input('typente') == "typeont") {
        $check_laporan = DB::table('psb_laporan')->where('snont', $req->input('search_sn'))->update([
          'typeont'           => NULL,
          'snont'             => NULL
        ]);
      } elseif ($req->input('typente') == "typestb") {
        $check_laporan = DB::table('psb_laporan')->where('snstb', $req->input('search_sn'))->update([
          'typestb'           => NULL,
          'snstb'             => NULL
        ]);
      }

    } elseif ($req->input('keterangan') == "SalahGudang") {

      DB::table('nte_delete')->insert([
        'sn'                  => $req->input('search_sn'),
        'type'                => $req->input('typente'),
        'created_by'          => session('auth')->id_karyawan
      ]);

      DB::table('nte')->where('sn', $req->input('search_sn'))->delete();
      
      if ($req->input('typente') == "typeont") {
        $check_laporan = DB::table('psb_laporan')->where('snont', $req->input('search_sn'))->update([
          'typeont'           => NULL,
          'snont'             => NULL
        ]);
      } elseif ($req->input('typente') == "typestb") {
        $check_laporan = DB::table('psb_laporan')->where('snstb', $req->input('search_sn'))->update([
          'typestb'           => NULL,
          'snstb'             => NULL
        ]);
      }

    }

    return redirect('/nte/return/teknisi')->with('alerts', [
      ['type' => 'success', 'text' => '<strong>SUKSES</strong> Return NTE']
    ]);
    
  }

  public function destroy($id)
  {
    $exists = DB::select('select * from gudang_transaksi where id = ?',[$id]);
    $data = $exists[0];
    if($data->position_type == 1){
      $key = 0;
      $type = 0;    
    }else{
      $key = 1;
      $type = $data->gudang_id;
    }
    DB::transaction(function() use($id,$key, $type) {
      $ntes = DB::select('select * from gudang_transaksi_barang where gudang_transaksi_id = ?',[$id]);
      foreach($ntes as $nte){
        DB::table('nte')
          ->where('id', $nte->key)
          ->update([
            'position_type'   => $type,
            'position_key'   => $key,
            'status'       => 1
            
          ]);
      }
      DB::table('gudang_transaksi_barang')
          ->where('gudang_transaksi_id', [$id])->delete();
      DB::table('gudang_transaksi')
          ->where('id', [$id])->delete();
    });
    return redirect('/nte')->with('alerts', [
        ['type' => 'success', 'text' => '<strong>SUKSES</strong> menghapus Data']
    ]);
  }
  public function stok(){
    $gudang = DB::select('
      SELECT id_gudang as id, nama_gudang as text
      FROM gudang
    ');
    return view('nte.stok', compact('gudang'));
  }
  public function getAllNte(){
    $nte = DB::select('
      SELECT id, sn as text
      FROM nte
    ');
    return json_encode($nte);
  }
  public function getGudangNte($id){
    $nte = DB::select('
      SELECT id, sn as text, jenis_nte
      FROM nte where position_type = 1 and position_key = ?
    ',[
    $id
    ]);
    return json_encode($nte);
  }
  public function getSumNteType($id){
    $nte = DB::select('
      SELECT jenis_nte as jn,(select count(id) from nte where position_type = 1 and position_key = ? and jenis_nte=jn) as totalType
      FROM nte where 1 group by jenis_nte
    ',[
    $id
    ]);
    return json_encode($nte);
  }

  public function inputTeknisi(){
    $regu = DB::select('
        SELECT a.id_regu as id,a.uraian as text, b.title
        FROM regu a
          LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        WHERE
          ACTIVE = 1
      ');

    $gudang    = DB::select('SELECT id_gudang as id, nama_gudang as text FROM gudang');
    // $materials = DB::select('SELECT *, sn as id, sn as text, null as vueSelected FROM nte WHERE position_key=1 AND position_type=2 ');
    // dd($materials);
    $materials = [];
    return view('nte.inputTeknisi',compact('regu', 'gudang', 'materials'));

  }

  public function saveTeknisi(Request $req){
      
      $this->validate($req,[
          'nama_gudang'   => 'required',
          'nama_regu'     => 'required',
          'kategori_nte'  => 'required'
      ],[
          'nama_gudang.required'    => 'Gudang Dipilih !',
          'nama_regu.required'      => 'Regu Dipilih !',
          'kategori_nte.required'   => 'Pilih Kondisi NTE !'
      ]);

      if($req->sn_nte[0]==null){
          return back()->with('alerts',[['type' => 'danger', 'text' => 'NTE Jangan Dikosongi']]);
      };

      $auth = session('auth');
      $dataRegu = DB::table('regu')->where('id_regu',$req->nama_regu)->first();
      
      $dataNte = explode(',', $req->sn_nte[0]);
      foreach($dataNte as $nte){
          DB::table('nte')->where('sn',$nte)->update([
              'id_regu'         => $dataRegu->id_regu,
              'nik1'            => $dataRegu->nik1,
              'nik2'            => $dataRegu->nik2,
              'position_key'    => $dataRegu->nik1,
              'status'          => 2,
              'nik_scmt'        => $req->nik_scmt,
              'kategori_nte'    => $req->kategori_nte,
              'tanggal_keluar'  => DB::RAW('NOW()'),
              'petugas'         => $auth->id_user
          ]);
      };

      return back()->with('alerts',[['type' => 'success', 'text' => 'Sukses Menyimpan Data']]);
  }

  public function getListGudang($gudang){
      $materials = DB::select('SELECT *, sn as id, sn as text, null as vueSelected FROM nte WHERE status = 2 AND position_type = "'.$gudang.'" ');
      return $materials;
  }
}
