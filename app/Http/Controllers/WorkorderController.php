<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Telegram;
use DB;
use Validator;
use App\DA\WorkorderModel;
use App\DA\PsbModel;
use App\DA\HomeModel;
use App\DA\Tele;

class WorkorderController extends Controller
{

  protected $photoInputs = [
    'Foto_Action','Lokasi', 'ODP', 'Redaman_ODP', 'Hasil_Ukur_OPM', 'Instalasi_Kabel', 'SN_ONT', 'SN_STB', 'LiveTV',
    'TVOD','RFC_Form', 'Speedtest', 'Berita_Acara', 'Telephone_Incoming', 'Telephone_Outgoing', 'Wifi_Analyzer', 'Foto_Pelanggan', 'Pullstrap', 'Tray_Cable', 'K3','KLEM_S', 'LABEL','Excheck_Helpdesk','Patchcore','Stopper','Breket'
  ];

  protected $assuranceInputs = [
    'Foto_Action','Berita_Acara','Rumah_Pelanggan','Test_Layanan_Internet', 'Test_Layanan_TV', 'Test_Layanan_Telepon', 'MODEM', 'SN_ONT_RUSAK'
  ];

  protected $photoCommon = ['Sebelum', 'Progress', 'Sesudah', 'Foto-GRID', 'Denah-Lokasi'];
  protected $photoRemo = ['ODP-dan-Redaman-GRID', 'Sebelum', 'Progress', 'Sesudah', 'Pengukuran', 'Pelanggan_GRID', 'BA-Remo'];
  protected $photoOdpLoss = ['ODP', 'Sebelum', 'Progress', 'Sesudah', 'Foto-GRID'];

  public function list($catstatus,$type){
    $checkONGOING = PsbModel::checkONGOING();
    $query = WorkorderModel::list($catstatus,$type);
    $get_asr_close = WorkorderModel::asr_close();
    return view('workorder.list',compact('get_asr_close','type','catstatus','query','checkONGOING'));
  }

}
