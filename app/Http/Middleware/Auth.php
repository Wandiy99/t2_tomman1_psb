<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rememberToken = $request->cookie('presistent-token');
        if ($rememberToken) {
            $SQL = '
                  SELECT
                    w.nama_witel,
                    u.psb_reg,
                    u.id_user,
                    u.password,
                    u.id_karyawan,
                    k.nama,
                    r.id_regu,
                    r.uraian,
                    r.hold_order,
                    u.level,
                    u.psb_remember_token,
                    r.mainsector,
                    u.witel,
                    k.owner_group,
                    k.ACTIVE
                  FROM user u
                  LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
                  LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
                  LEFT JOIN witel w ON u.witel = w.id_witel
                  LEFT JOIN psb_laporan_status pls ON k.owner_group = pls.jenis_order
                  WHERE
                  u.psb_remember_token = "' . $rememberToken . '"
                  GROUP BY u.id_user
                ';
            $user = DB::select($SQL);

            if (count($user)>0) {
                Session::put('psb_reg',$user[0]->psb_reg);
                Session::put('witel',$user[0]->nama_witel);
                Session::put('auth', $user[0]);
                return $next($request);
            }
        }

        Session::put('auth-originalUrl', $request->fullUrl());
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            return redirect('login');
        }
    }
}
