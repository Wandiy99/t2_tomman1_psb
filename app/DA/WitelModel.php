<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class WitelModel{
  public static function get_datel($witel){
    return DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
  }
}
 
