<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class MapModel
{
  public static function getSektor()
  {
    return DB::select('SELECT title as id, title as text FROM group_telegram WHERE ket_sektor = 1 AND sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND sms_active_prov = 1 ORDER BY urut ASC');
  }

  public static function getLastLocationTech()
  {
    return DB::SELECT('SELECT b.NO_ORDER,a.regu_nama,a.modified_at,a.startLatTechnition as lat,a.startLonTechnition as lon FROM `psb_laporan` a left join dispatch_teknisi b ON b.id = a.id_tbl_mj WHERE a.modified_at LIKE "'.date('Y-m-d').'%" AND a.startLatTechnition IS NOT NULL GROUP BY a.regu_nama ORDER BY a.modified_at DESC');
  }

  public static function getStarclickPI($type)
  {
    switch ($type){
      case "AO" :
      $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick dps LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.NO_ORDER LEFT JOIN regu r ON dt.id_regu = r.id_regu WHERE dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND YEAR(dps.orderDate)="'.date('Y').'"');
      break;
    }
    return $query;
  }

  public static function getStarclickFWFM($type)
  {
    switch ($type){
      case "AO" :
      $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick dps WHERE dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("Fallout (WFM)") AND YEAR(dps.orderDate)="'.date('Y').'"');
      break;
    }
    return $query;
  }

  public static function getStarclickActcomp($type)
  {
    switch ($type){
      case "AO" :
      $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick dps WHERE dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("OSS PONR", "WFM ACTIVATION COMPLETE") AND YEAR(dps.orderDate)="'.date('Y').'"');
      break;
    }
    return $query;
  }

  public static function getStarclickPS($type,$tgl)
  {
    switch ($type){
      case "AO" :
      $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick dps WHERE dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("COMPLETED") AND DATE(dps.orderDatePs)="'.$tgl.'"');
      break;
    }
    return $query;
  }

  public static function jml_teknisi_hadir_approved($posisi_id,$date,$sektor)
  {
    $where_position = '';
    if ($posisi_id<>"ALL"){
      $where_position = 'AND a.posisi_id = '.$posisi_id;
    }
    return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik
    
    WHERE 1 '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR" AND b.approval = 1
    ');
  }

  public static function map_data_teknisi($sektor)
  {
    $data = DB::table('1_2_employee AS emp')
    ->leftJoin('user AS u', 'emp.nik', '=', 'u.id_karyawan')
    ->leftJoin('regu AS r', function($join) {
      $join->on('r.nik1', '=', 'emp.nik')
      ->OrOn('r.nik2', '=', 'emp.nik');
    })
    ->leftJoin('group_telegram AS gt', 'r.mainsector', '=', 'gt.chat_id')
    ->select(DB::raw('
      emp.nik,
      emp.nama,
      r.uraian AS tim,
      gt.title AS sektor,
      u.myLastLatitude,
      u.myLastLongitude,
      u.myLastAccuracy,
      u.myLastCoordUpdate
    '))
    ->where([
      ['emp.ACTIVE', 1],
      ['u.level', 10],
      ['r.ACTIVE', 1],
      ['r.status_team', 'ACTIVE']
    ])
    ->whereNotNull('u.myLastCoordUpdate');

    if ($sektor != 'ALL')
    {
      $data->where('gt.title', $sektor);
    }

    $data = $data->get();

    $combinedData = [];

    foreach ($data as $entry)
    {
      $key = $entry->nik . "-" . $entry->nama;
      
      if (!isset($combinedData[$key]))
      {
        $combinedData[$key] = [
            "nik"               => $entry->nik,
            "nama"              => $entry->nama,
            "tim"               => $entry->tim,
            "sektor"            => $entry->sektor,
            "latitude"          => $entry->myLastLatitude,
            "longitude"         => $entry->myLastLongitude,
            "myLastAccuracy"    => $entry->myLastAccuracy,
            "myLastCoordUpdate" => $entry->myLastCoordUpdate
        ];
      }
      else
      {
        $combinedData[$key]["tim"]    .= " , " . $entry->tim;
        $combinedData[$key]["sektor"] .= " , " . $entry->sektor;
      }
    }

    $result = array_values($combinedData);
    
    return $result;
  }

  public static function map_data_pelanggan($sektor, $date)
  {
    $data = DB::table('dispatch_teknisi AS dt')
    ->leftJoin('Data_Pelanggan_Starclick AS dps', 'dt.NO_ORDER', '=', 'dps.orderIdInteger')
    ->leftJoin('psb_myir_wo AS pmw', 'dt.Ndem', '=', 'pmw.myir')
    ->leftJoin('regu AS r', 'dt.id_regu', '=', 'r.id_regu')
    ->leftJoin('group_telegram AS gt', 'r.mainsector', '=', 'gt.chat_id')
    ->leftJoin('psb_laporan AS pl', 'dt.id', '=', 'pl.id_tbl_mj')
    ->leftJoin('psb_laporan_status AS pls', 'pl.status_laporan', '=', 'pls.laporan_status_id')
    ->leftJoin('dispatch_teknisi_jenis_layanan AS dtjl', 'dt.jenis_layanan_id', '=', 'dtjl.jenis_layanan_id')
    ->select(DB::raw('
      dt.NO_ORDER AS order_id,
      dps.orderName,
      dps.orderDate,
      dps.orderDatePs,
      dps.lat AS latitude,
      dps.lon AS longitude,
      dps.jenisPsb,
      dps.sto,
      r.uraian AS tim,
      gt.title AS sektor,
      pls.laporan_status AS status_teknisi,
      pl.modified_at AS tgl_laporan
    '))
    ->whereRaw('
      r.ACTIVE = 1 AND
      (
        (DATE(dps.orderDatePs) IN ("'.date('Y-m-d').'" , "0000-00-00" , "1900-01-01", "1970-01-01"))
        OR
        dps.orderDatePs IS NULL
      ) AND
      dt.tgl = "'.$date.'" AND
      dtjl.jenis_layanan NOT IN ("QC","ONT_PREMIUM", "CABUT_NTE") AND
      (pls.laporan_status != "PERMINTAAN REVOKE" OR pls.laporan_status IS NULL) AND
      YEAR(dt.tgl) = "'.date('Y').'" AND
      (dps.lat IS NOT NULL OR dps.lon IS NOT NULL)
    ');

    if ($sektor != 'ALL')
    {
      $data->where('gt.title', $sektor);
    }

    return $data->get();
  }
}