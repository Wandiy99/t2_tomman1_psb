<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class TicketingModel {

  public static function dashboard()
  {
    return DB::SELECT('
        SELECT 
        f.title as sektor_asr,
        count(*) as jumlah,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 2 THEN 1 ELSE 0 END) as x0dan2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 4 THEN 1 ELSE 0 END) as x2dan4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 6 THEN 1 ELSE 0 END) as x4dan6,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 8 THEN 1 ELSE 0 END) as x6dan8,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 10 THEN 1 ELSE 0 END) as x8dan10,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 12 THEN 1 ELSE 0 END) as x10dan12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") < 24 THEN 1 ELSE 0 END) as x12dan24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") >=24 THEN 1 ELSE 0 END) as lebih24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . date('Y-m-d H:i:s') . '") <24 THEN 1 ELSE 0 END) as kurang24
        FROM data_nossa_1_log a
        LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN mdf d ON a.Workzone = d.mdf  
        LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
        LEFT JOIN group_telegram f ON c.mainsector = f.chat_id
      WHERE 
        a.Incident <> "" AND
        a.Source = "TOMMAN" AND
        (DATE(a.Reported_Date) = "' . date("Y-m-d") . '") AND 
        (e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
      GROUP BY f.title
    ');
  }

  public static function get_keluhan($id){
  $keluhan = DB::SELECT('select * from keluhan where keluhan = "'.$id.'"');
  return $keluhan;
  }

  public static function dashboardCloseList($status,$sektor,$date){
    $where_sektor = '';
    if ($sektor=="UNDEFINED"){
      $where_sektor = ' AND f.sektor_asr IS NULL';
    } else if ($sektor<>"ALL"){
      $where_sektor = ' AND f.sektor_asr = "'.$sektor.'"';
    }

    $where_status = '';
    if ($status<>"ALL"){
      SWITCH ($status){
        case "UNDISPATCH" : 
          $where_status = 'AND a.id IS NULL';
        break;
        case "SISA" : 
          $where_status = 'AND (c.grup = "SISA" OR (a.id IS NOT NULL AND b.status_laporan IS NULL))';
        break;
        case "UP" : 
          $where_status = 'AND (b.status_laporan = 1 AND a.tgl like "'.$date.'%")';
        break;
        default : 
          $where_status = 'AND c.grup = "'.$status.'"';
        break;
      }
    }
    $query = DB::SELECT('SELECT *,a.id as id_dt, e.* FROM  
       data_nossa_1_log e 
      LEFT JOIN  dispatch_teknisi a ON a.Ndem = e.Incident
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
      LEFT JOIN psb_laporan_action d ON b.action = d.laporan_action_id
      LEFT JOIN psb_laporan_penyebab h ON b.penyebabId = h.idPenyebab
      LEFT JOIN mdf f ON e.Workzone = f.mdf
      LEFT JOIN regu g ON a.id_regu = g.id_regu
      WHERE 
        e.Source = "TOMMAN" 
        '.$where_sektor.'
        '.$where_status.'
      ');
    return $query;
  }

  public static function dashboardClose($date){
    return DB::SELECT('
      SELECT 
      f.sektor_asr,
      SUM(CASE WHEN  a.id IS NULL THEN 1 ELSE 0 END) as UNDISPATCH,
      SUM(CASE WHEN (c.grup = "SISA" OR (a.id IS NOT NULL AND b.status_laporan IS NULL)) THEN 1 ELSE 0 END) as SISA,
      SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
      SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
      SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
      SUM(CASE WHEN c.grup = "UP" THEN 1 ELSE 0 END) as UP 
      FROM 
        data_nossa_1_log e 
      LEFT JOIN dispatch_teknisi a ON a.Ndem = e.Incident
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
      LEFT JOIN psb_laporan_action d ON b.action = d.laporan_action_id
      LEFT JOIN mdf f ON e.Workzone = f.mdf
      LEFT JOIN regu g ON a.id_regu = g.id_regu
      WHERE 
        e.Source = "TOMMAN" AND (
        (b.status_laporan = 1 AND date(a.tgl) LIKE "'.$date.'%") OR 
        (c.grup IN ("KP","KT","OGP")) OR
        (b.status_laporan IS NULL) OR 
        a.id IS NULL
      ) 

      GROUP BY f.sektor_asr
      ');
  }

  public static function get_history_order_x($id)
  {
    return DB::SELECT('
    SELECT 
    *,
    b.id as id_dt
    FROM data_nossa_1_log a 
    LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
    LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
    LEFT JOIN regu g ON b.id_regu = g.id_regu
    WHERE
    a.Service_No = "'.$id.'" OR
    a.no_internet = "'.$id.'"
    ORDER BY a.Reported_Date DESC
    LIMIT 1');
  }

  public static function get_history_order($id)
  {
    return DB::SELECT('
    SELECT 
    *,
    b.id as id_dt
    FROM data_nossa_1_log a 
    LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
    LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
    LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
    LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
    LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
    LEFT JOIN regu g ON b.id_regu = g.id_regu
    WHERE
    a.Service_No = "'.$id.'" OR
    a.no_internet = "'.$id.'"
    ORDER BY a.Reported_Date DESC
    ');
  }

  public static function get_customer_info($id)
  {
      $data = new \StdClass;
      $data->orderName = null;
      $data->ndemPots = null;
      $data->ndemSpeedy = null;
      $data->orderNcli = null;
      $data->orderAddr = null;
      $data->kcontact = null;
      $data->alproname = null;
      $data->sto = null;
      $data->internet = null;
      $data->orderDatePs = null;
      $data->orderId = null;
      $data->id_dt = null;

      $getData = DB::select('
        SELECT 
          a.*,
          b.id as id_dt
        FROM 
          Data_Pelanggan_Starclick a
        LEFT JOIN dispatch_teknisi b ON a.orderIdInteger = b.NO_ORDER 
        WHERE
          (a.internet LIKE "%' . $id . '%" OR a.ndemSpeedy LIKE "%' . $id . '%" OR a.ndemPots LIKE "%' . $id . '%") AND 
          a.orderId IS NOT NULL
        ORDER BY
          a.orderDate DESC
        LIMIT 1 
      ');

      if (count($getData) > 0)
      {
          $result = $getData[0];
          $data->orderName    = $result->orderName;
          $data->ndemPots     = $result->ndemPots;
          $data->ndemSpeedy   = $result->ndemSpeedy;
          $data->orderAddr    = $result->orderAddr;
          $data->kcontact     = $result->kcontact;
          $data->alproname    = $result->alproname;
          $data->sto          = $result->sto;
          $data->internet     = $result->internet;
          $data->orderDatePs  = $result->orderDatePs;
          $data->orderId      = $result->orderId;
          $data->orderNcli    = $result->orderNcli;
      }
      else{
          $getData = DB::select('SELECT * FROM dossier_master WHERE ND LIKE "%'.$id.'%" OR ND_REFERENCE LIKE "%'.$id.'%" LIMIT 1');
          if (count($getData) <> 0)
          {
              $data->orderName  = $getData[0]->NAMA;
              $data->ndemPots   = $getData[0]->ND;
              $data->ndemSpeedy = $getData[0]->ND_REFERENCE;
              $data->orderAddr  = $getData[0]->LCOM;
              $data->kcontact   = $getData[0]->NCLI.'-'.$getData[0]->ND.'-'.$getData[0]->ND_REFERENCE;     
              $data->alproname  = null; 
              $data->sto        = $getData[0]->STO;
              $data->internet   = $getData[0]->ND_REFERENCE;
          }
      }

      return $data;
  }

  public static function listPersonal($auth){
  	if ($auth->id_karyawan=="91153709") {
  		$where_id_karyawan = ' AND a.Source = "TOMMAN"';
  	} else {
  		$where_id_karyawan = ' AND a.user_created = "'.$auth->id_karyawan.'"';
  	}
  	return DB::SELECT('
  		SELECT * FROM data_nossa_1_log a 
  		LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
  		LEFT JOIN regu c ON b.id_regu = c.id_regu 
  		LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
  		LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
  		WHERE d.status_laporan <> 1 
  		'.$where_id_karyawan.'
  		');
  }

  public static function dashboardDetail($type, $sektor, $status)
  {
    
    $date_now = date('Y-m-d H:i:s');

    switch ($status) {
      case 'x0dan2':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 2)';
        break;
      case 'x2dan4':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 4)';
        break;
      case 'x4dan6':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 6)';
        break;
      case 'x6dan8':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 8)';
        break;
      case 'x8dan10':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 10)';
        break;
      case 'x10dan12':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 12)';
        break;
      case 'x12dan24':
        $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 24)';
        break;
      case 'lebih24':
        $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=24';
        break;
      case 'kurang24':
        $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 24';
        break;
      default:
        $where_status = '';
        break;
    }

    switch ($type) {
      case 'sektor':
        switch ($status) {
          case 'ALL':
            $where_sektor = '';
            break;
          default:
            $where_sektor = ' AND d.sektor_asr = "' . $sektor . '"';
            break;
        }
        break;
      case 'hero':
        switch ($status) {
          case 'ALL':
            $where_sektor = '';
            break;
          default:
            $where_sektor = ' AND md.HERO = "' . $sektor . '"';
            break;
        }
        break;
    }

    return DB::select('
    SELECT 
      *,
      b.id as id_dt,
      a.alamat as pel_alamat
    FROM 
      data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN mdf d ON a.Workzone = d.mdf
      LEFT JOIN maintenance_datel md ON a.Workzone = md.sto
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan=f.laporan_status_id
    WHERE 
      a.Source = "TOMMAN" AND 
      (e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
      ' . $where_status . '
      ' . $where_sektor . '
    ');
  }

  public static function getUndispatchList(){
      return DB::select('SELECT * FROM data_nossa_1_log WHERE Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND Incident LIKE "%INT%"');
  }

   public static function getStoUndispatchList(){
      return DB::select('SELECT DISTINCT Workzone FROM data_nossa_1_log WHERE Incident NOT IN (SELECT Ndem FROM dispatch_teknisi) AND Incident LIKE "%INT%"');
  }

  public static function getJumlahUndispatch(){
    
    $date_now = date('Y-m-d H:i:s');
    return DB::SELECT('
      SELECT 
        count(*) as jumlah,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2 THEN 1 ELSE 0 END) as x0dan2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4 THEN 1 ELSE 0 END) as x2dan4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6 THEN 1 ELSE 0 END) as x4dan6,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8 THEN 1 ELSE 0 END) as x6dan8,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10 THEN 1 ELSE 0 END) as x8dan10,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12 THEN 1 ELSE 0 END) as x10dan12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24 THEN 1 ELSE 0 END) as x12dan24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24 THEN 1 ELSE 0 END) as lebih24,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") <24 THEN 1 ELSE 0 END) as kurang24
      FROM 
        data_nossa_1_log a
        LEFT JOIN mdf d ON a.Workzone = d.mdf
        LEFT JOIN dispatch_teknisi dt ON a.ID = dt.NO_ORDER
      WHERE
        dt.id IS NULL AND 
        a.Incident LIKE "%INT%"
      ');
  }

  public static function dashboardDetailUndispatch($status)
  {  
    $date_now = date('Y-m-d H:i:s');

    switch ($status) {
      case 'x0dan2':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 2)';
        break;
      
      case 'x2dan4':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 4)';
        break;

      case 'x4dan6':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 6)';
        break;

      case 'x6dan8':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 8)';
        break;

      case 'x8dan10':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 10)';
        break;

      case 'x10dan12':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 12)';
        break;

      case 'x12dan24':
          $where_status = ' AND (TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24)';
        break;

      case 'lebih24':
          $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") >=24';
        break;

      case 'kurang24':
          $where_status = ' AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "'.$date_now.'") < 24';
        break;

      default:
          $where_status = '';
        break;
    }

    return DB::select('
      SELECT 
        *,
        a.alamat as pel_alamat
      FROM 
        data_nossa_1_log a
        LEFT JOIN mdf d ON a.Workzone = d.mdf
        LEFT JOIN dispatch_teknisi dt ON a.ID = dt.NO_ORDER
        LEFT JOIN maintaince m ON a.Incident = m.no_tiket
      WHERE
        dt.ID IS NULL AND
        m.dispatch_regu_name IS NULL AND
        a.Incident LIKE "%INT%"
        '.$where_status.'
    ');
  }

  public static function dashboardPlasaAssurance($flag, $start, $end)
  {
    if ($flag != '""')
    {
      $all_flag = [];

      foreach (json_decode($flag) as $value)
      {
        $all_flag[] = "r.".$value." = 1";
      }
      $all_flag = 'AND ('. implode(' OR ', $all_flag).')';
    } else {
      $all_flag = '';
    }

    return DB::select('
      SELECT
        aa.area_alamat AS area,
        SUM(CASE WHEN pls.grup IN ("NP", "") THEN 1 ELSE 0 END) AS jml_np,
        SUM(CASE WHEN pls.grup IN ("OGP", "SISA") THEN 1 ELSE 0 END) AS jml_ogp,
        SUM(CASE WHEN pls.grup = "KP" THEN 1 ELSE 0 END) AS jml_kp,
        SUM(CASE WHEN pls.grup = "KT" THEN 1 ELSE 0 END) AS jml_kt,
        SUM(CASE WHEN pls.laporan_status = "UP" THEN 1 ELSE 0 END) AS jml_close
      FROM data_nossa_1_log dn1l
      LEFT JOIN roc r ON dn1l.Incident = r.no_tiket
      LEFT JOIN area_alamat aa ON dn1l.Workzone = aa.kode_area
      LEFT JOIN dispatch_teknisi dt ON dn1l.ID_INCREMENT = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
        dn1l.Incident LIKE "INT%" AND
        (DATE(dn1l.Reported_Datex) BETWEEN "'.$start.'" AND "'.$end.'")
         '.$all_flag.'
      GROUP BY aa.area_alamat
    ');
  }

  public static function dashboardPlasaAssuranceDetail($flag, $start, $end, $area, $grup)
  {
    if ($flag != '""')
    {
      $all_flag = [];

      foreach (json_decode($flag) as $value)
      {
        $all_flag[] = "r.".$value." = 1";
      }
      $all_flag = 'AND ('. implode(' OR ', $all_flag).')';
    } else {
      $all_flag = '';
    }

    switch ($area) {
      case 'ALL':
          $areax = '';
        break;
      
      default:
          $areax = 'AND aa.area_alamat = "'.$area.'"';
        break;
    }

    switch ($grup) {
      case 'NP':
          $grupx = 'AND pls.grup IN ("NP", "")';
        break;
      
      case 'OGP':
          $grupx = 'AND pls.grup IN ("OGP", "SISA")';
        break;

      case 'KP':
          $grupx = 'AND pls.grup = "KP"';
        break;

      case 'KT':
          $grupx = 'AND pls.grup = "KT"';
        break;

      case 'CLOSE':
          $grupx = 'AND pls.laporan_status = "UP"';
        break;
    }

    return DB::select('
      SELECT
          aa.area_alamat AS area,
          rr.uraian as tim,
          gt.title as sektor,
          pls.laporan_status as status_teknisi,
          pl.modified_at as tgl_status_teknisi,
          dn1l.*
        FROM data_nossa_1_log dn1l
        LEFT JOIN roc r ON dn1l.Incident = r.no_tiket
        LEFT JOIN area_alamat aa ON dn1l.Workzone = aa.kode_area
        LEFT JOIN dispatch_teknisi dt ON dn1l.ID_INCREMENT = dt.NO_ORDER
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu rr ON dt.id_regu = rr.id_regu
        LEFT JOIN group_telegram gt ON rr.mainsector = gt.chat_id
        WHERE
          dn1l.Incident LIKE "INT%" AND
          (DATE(dn1l.Reported_Datex) BETWEEN "'.$start.'" AND "'.$end.'")
          '.$all_flag.'
          '.$areax.'
          '.$grupx.'
        ORDER BY dn1l.ID_INCREMENT
    ');
  }

}
