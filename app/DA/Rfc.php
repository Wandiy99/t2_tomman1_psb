<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class Rfc extends Model
{
    public static function getListSaldoMitra($ukuran){
    	$sql = 'SELECT 
    				SUM(CASE WHEN b.mitra IN ("PT. Upaya Tehnik", "PT. UPAYA TEKNIK") THEN 1 ELSE 0 END) as upatek,
    				SUM(CASE WHEN b.mitra IN ("PT ANUGERAH GENERASI BERSAMA", "ANUGERAH GENERASI BERSAMA") THEN 1 ELSE 0 END) as agb,
    				SUM(CASE WHEN b.mitra = "PT. CITRA UTAMA INSANI" THEN 1 ELSE 0 END) as cui,
    				SUM(CASE WHEN b.mitra IN ("PT SANDHY PUTRA MAKMUR", "PT. Sandy Putra Makmur") THEN 1 ELSE 0 END) as spm,
    				count(*) as jumlah
    			FROM 
    				psb_saldo_dc a
    			LEFT JOIN rfc_input b ON a.alista = b.alista_id
    			WHERE 
    				a.ket=2 AND
    				a.jumlah = "'.$ukuran.'"
    			';

    	$data = DB::select($sql);
    	return $data;
    }

    public static function getListSaldoArea($ukuran){
        $sql = 'SELECT 
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV BANJARMASIN 1 (BJM CENTRUM)" THEN 1 ELSE 0 END) as bjm1,
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV BANJARMASIN 2 (A.YANI)" THEN 1 ELSE 0 END) as bjm2,
                    SUM(CASE WHEN a.gudang_kirim = "Banjarmasin - Area" THEN 1 ELSE 0 END) as bjm_area,
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV BATULICIN" THEN 1 ELSE 0 END) as btl,
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV BARABAI" THEN 1 ELSE 0 END) as brb,
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV TANJUNG TABALONG" THEN 1 ELSE 0 END) as tjg,
                    SUM(CASE WHEN a.gudang_kirim = "WH SO INV BANJARBARU" THEN 1 ELSE 0 END) as bjb,
                    count(*) as jumlah
                FROM 
                    psb_saldo_dc a
                LEFT JOIN rfc_input b ON a.alista = b.alista_id
                WHERE 
                    a.ket=4 AND
                    a.jumlah = "'.$ukuran.'"
                ';

        $data = DB::select($sql);
        return $data;
    }

    public static function getJumlahDc($ukuran){
        return DB::select('select count(*) as jumlah from psb_saldo_dc where ket=2 AND jumlah="'.$ukuran.'"');
    }

    public static function getSendProsesArea($chatId){
        return DB::table('psb_saldo_dc')->whereIn('ket',[3,4])->where('gudang_kirim',$chatId)->get();
    }

    public static function ubahKet($id, $ket){
        return DB::table('psb_saldo_dc')->where('id',$id)->update([
            'ket' => $ket
        ]);
    }

    public static function getListSaldoSektor($ukuran, $chatId){
        $sql = 'SELECT 
                    SUM(CASE WHEN ket=4 THEN 1 ELSE 0 END) as stok,
                    SUM(CASE WHEN ket=5 THEN 1 ELSE 0 END) as pakai,
                    count(*) as jumlah
                FROM 
                    psb_saldo_dc
                WHERE 
                    gudang_kirim="'.$chatId.'" AND                    
                    ket IN (4,5) AND 
                    jumlah = "'.$ukuran.'"
                ';

        $data = DB::select($sql);
        return $data;
    }
}
