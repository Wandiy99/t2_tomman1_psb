<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MaterialModel{

  public static function get_material_periode($program,$start,$end)
  {
    $sektor = DB::SELECT('SELECT title FROM group_telegram GROUP BY title');

    $query1 = DB::SELECT('
      SELECT
      c.title,
      COUNT(*) as JML_RFC,
      SUM(CASE WHEN a.material = "AC-OF-SM-1B" THEN a.quantity ELSE 0 END) as DC,
      SUM(CASE WHEN a.material = "PRECON-1C-50-NOAC" THEN a.quantity ELSE 0 END) as PRECON_50,
      SUM(CASE WHEN a.material = "PRECON-1C-80-NOAC" THEN a.quantity ELSE 0 END) as PRECON_80,
      SUM(CASE WHEN a.material IN ("SOC-ILS","SOC-SUM") THEN a.quantity ELSE 0 END) as SOC
      FROM
      inventory_material a
      LEFT JOIN 1_2_employee b ON a.nik_pemakai = b.nik
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      WHERE
      a.program = "'.$program.'" AND
      (DATE(a.posting_datex) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY c.title
    ');

    $query2 = DB::SELECT('
      SELECT
      gt.title,
      SUM(CASE WHEN kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" THEN 1 ELSE 0 END) as order_psb,
      SUM(CASE WHEN kpt.CUSTOMER_NAME LIKE "%WMS%" THEN 1 ELSE 0 END) as order_wms,
      SUM(CASE WHEN kpt.JENIS_PSB LIKE "%PDA%" THEN 1 ELSE 0 END) as order_pda
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
      (DATE(kpt.ORDER_DATE) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY gt.title
    ');

    $query3 = DB::SELECT('
      SELECT
      gt.title,
      SUM(CASE WHEN kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
      SUM(CASE WHEN kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
      SUM(CASE WHEN kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
      SUM(CASE WHEN kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("SOC-ILS","SOC-SUM") THEN plm.qty ELSE 0 END) as socm
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
      (DATE(kpt.ORDER_DATE) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY gt.title
    ');

    foreach($sektor as $val)
    {
      foreach($query1 as $val_c1)
      {
        if (!empty($val_c1->title))
        {
          if ($val_c1->title == $val->title)
          {
            $query[$val->title]['JML_RFC'] = $val_c1->JML_RFC;
            $query[$val->title]['DC'] = $val_c1->DC;
            $query[$val->title]['PRECON_50'] = $val_c1->PRECON_50;
            $query[$val->title]['PRECON_80'] = $val_c1->PRECON_80;
            $query[$val->title]['SOC'] = $val_c1->SOC;
          }
        } else {
            $query['UNMAPPING']['JML_RFC'] = $val_c1->JML_RFC;
            $query['UNMAPPING']['DC'] = $val_c1->DC;
            $query['UNMAPPING']['PRECON_50'] = $val_c1->PRECON_50;
            $query['UNMAPPING']['PRECON_80'] = $val_c1->PRECON_80;
            $query['UNMAPPING']['SOC'] = $val_c1->SOC;
        }
      }
      foreach($query2 as $val_c2)
      {
        if (!empty($val_c2->title))
        {
          if ($val_c2->title == $val->title)
          {
            $query[$val->title]['order_psb'] = $val_c2->order_psb;
            $query[$val->title]['order_wms'] = $val_c2->order_wms;
            $query[$val->title]['order_pda'] = $val_c2->order_pda;
          }
        } else {
            $query['UNMAPPING']['order_psb'] = $val_c2->order_psb;
            $query['UNMAPPING']['order_wms'] = $val_c2->order_wms;
            $query['UNMAPPING']['order_pda'] = $val_c2->order_pda;
        }
      }
      foreach($query3 as $val_c3)
      {
        if (!empty($val_c3->title))
        {
          if ($val_c3->title == $val->title)
          {
            $query[$val->title]['dc_roll'] = $val_c3->dc_roll;
            $query[$val->title]['precon50'] = $val_c3->precon50;
            $query[$val->title]['precon80'] = $val_c3->precon80;
            $query[$val->title]['socm'] = $val_c3->socm;
          }
        } else {
            $query['UNMAPPING']['dc_roll'] = $val_c3->dc_roll;
            $query['UNMAPPING']['precon50'] = $val_c3->precon50;
            $query['UNMAPPING']['precon80'] = $val_c3->precon80;
            $query['UNMAPPING']['socm'] = $val_c3->socm;
        }
      }
    }
    
    return $query;
  }

  public static function dashboardDetail($sektor,$order,$start,$end)
  {

    if ($sektor <> "UNMAPPING")
    {
      $sektorx = 'AND gt.title = "'.$sektor.'"';
    } else {
      $sektorx = 'AND gt.title IS NULL';
    }

    if ($order == "PSB")
    {
      $orderx = 'AND kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES"';
    } elseif ($order == "WMS") {
      $orderx = 'AND kpt.CUSTOMER_NAME LIKE "%WMS%"';
    } elseif ($order == "PDA") {
      $orderx = 'AND kpt.JENIS_PSB LIKE "%PDA%"';
    } elseif ($order == "DC") {
      $orderx = 'AND kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("AC-OF-SM-1B","DC-ROLL")';
    } elseif ($order == "PRECON50") {
      $orderx = 'AND kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M")';
    } elseif ($order == "PRECON80") {
      $orderx = 'AND kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80")';
    } elseif ($order == "SOC") {
      $orderx = 'AND kpt.JENIS_PSB = "AO" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND plm.id_item IN ("SOC-ILS","SOC-SUM")';
    }

    return DB::SELECT('
      SELECT
      kpt.*,
      r.uraian as tim,
      gt.title as sektor,
      ma.mitra_amija_pt as mitra,
      SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
      SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
      SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
      SUM(CASE WHEN plm.id_item IN ("SOC-ILS","SOC-SUM") THEN plm.qty ELSE 0 END) as socm
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      WHERE
      (DATE(kpt.ORDER_DATE) BETWEEN "'.$start.'" AND "'.$end.'")
      '.$sektorx.'
      '.$orderx.'
      GROUP BY kpt.ORDER_ID
    ');
  }

  public static function list_TL($periode,$nik){
    $tglPisah = explode('-', $periode);
    $tgl      = $tglPisah[1].'/'.$tglPisah[0];

    if ($nik == "88159353" || $nik == "850056" || $nik == "94150248"){
        $where = '';
    }
    else if ($nik == "MITRA_UPATEK"){
        $where = 'b.mitra in ("UPATEK","UPATEKDELTA") AND';
    }
    else if ($nik == "MITRA_PTAGB"){
        $where = 'b.mitra = "AGB DELTA" AND';
    }
    else if ($nik == "MITRA_SPM"){
        $where = 'b.mitra = "SPM" AND';
    }
    else if ($nik == "MITRA_CUI"){
        $where = 'b.mitra in ("CUI","CUI DELTA") AND';
    }
    else
    {
        $where = 'c.TL_NIK = "'.$nik.'" AND';
    };

    return DB::SELECT('
      SELECT
        *
      FROM
        rfc_sal a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN 1_2_employee d ON a.nik = d.nik
        WHERE
      '.$where.'
      a.jmlTerpakai < a.jumlah AND
      b.ACTIVE = "1" AND
      c.chat_id <> "-254857170" AND
      a.rfc LIKE "%'.$tgl.'%"
    ');
  }

  public static function listMaterial($periode,$sektor){
    $tglPisah = explode('-', $periode);
    $tgl      = $tglPisah[1].'/'.$tglPisah[0];

    $where_sektor = '';
    if ($sektor<>''){
        $where_sektor = 'c.chat_id="'.$sektor.'" AND ';
    };

    $sql = '
      SELECT
        *
      FROM
        rfc_sal a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN 1_2_employee d ON a.nik = d.nik
        WHERE
      '.$where_sektor.'
      a.jmlTerpakai < a.jumlah AND
      b.ACTIVE = "1" AND
      a.rfc LIKE "%'.$tgl.'%"
    ';

    $data = DB::select($sql);
    return $data;
  }
}
