<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class scbe extends Model
{
    public static function datel($witel){
        $query = DB::SELECT('
            SELECT
            datel
            FROM
            maintenance_datel
            WHERE
            witel = "'.$witel.'"
            GROUP BY datel
        ');
        return $query;
    }

    public static function dashboard_new_list($tgl,$datel,$status){
        $where_tgl = '';
        $where_datel = '';
        $where_status = '';
        if ($tgl<>"ALL"){
            $where_tgl = 'AND DATE(dpsb.ORDER_DATE)="'.$tgl.'"';
        }
        if ($datel<>"ALL"){
            $where_datel = 'AND e.datel = "'.$datel.'"';
        }
        if ($status=="BELUM_DISPATCH") {
            $where_status = 'AND a.id IS NULL';
        }elseif ($status=="NEED PROGRESS NEW"){
            $where_status = 'AND (a.id IS NOT NULL AND b.id IS NULL)';
        }elseif ($status<>"ALL"){
            $where_status = 'AND c.laporan_status = "'.$status.'"';
        }

        $query = DB::SELECT('
            SELECT
            *,
            a.created_at as tgl_dispatch_awal,
            a.updated_at as tgl_dispatch_akhir
            FROM
                Data_Pelanggan_Starclick_Backend dpsb
            LEFT JOIN dispatch_teknisi a ON SUBSTR(dpsb.ORDER_CODE,6,30) = a.Ndem
            LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
            LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
            LEFT JOIN psb_myir_wo d ON SUBSTR(dpsb.ORDER_CODE,6,30) = d.myir
            LEFT JOIN maintenance_datel e ON SUBSTR(d.namaOdp,5,3) = e.sto
            LEFT JOIN regu f ON a.id_regu = f.id_regu
            LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
            WHERE
            dpsb.STATUS_RESUME IN ("booked","open")
            '.$where_tgl.'
            '.$where_datel.'
            '.$where_status.'
        ');
        return $query;
    }

    public static function download_scbe($tgl)
    {
        return DB::select('
        SELECT
            dt.id as id_dt,
            dt.Ndem as ORDER_ID,
            dps.jenisPsb,
            pmw.customer as CUSTOMER,
            dps.orderName as CUSTOMER2,
            pmw.no_ktp as KTP,
            pl.noPelangganAktif as NOHP,
            dps.orderNotel as NOHP2,
            dps.kcontact as KCONTACT,
            dps.orderAddr as ALAMAT_STARCLICK,
            pmw.alamatLengkap as ALAMAT_SALES,
            pkh.paket_harga as PAKET_HARGA,
            pks.paket_sales as PAKET_SALES,
            r.uraian as TIM,
            dt.crewid as DT_CREW,
            r.crewid as R_CREW,
            r.nik1 as NIK1,
            r.nik2 as NIK2,
            r.mitra as MITRA,
            gt.sektor as DATEL,
            md.sto as STO,
            gt.title as SEKTOR,
            gt.TL as TL_SEKTOR,
            pmw.myir as MYIR,
            dps.orderId as SC,
            pl.rfc_number,
            dps.agent_id as AGENT_ID,
            dps.orderNcli as NCLI,
            dps.wfm_id as WFM_ID,
            kpt.WFM_ID as KPT_WFM_ID,
            pl.valins_id,
            dps.ndemSpeedy as ND_INTERNET,
            pmw.no_internet as ND_INTERNET2,
            dps.ndemPots as ND_VOICE,
            pmw.no_telp as ND_VOICE2,
            pl.nama_odp as ODP_TEKNISI,
            pl.kordinat_odp as KOORDINAT_ODP_TEKNISI,
            dps.alproname as ODP_STARCLICK,
            dps.orderStatus as ORDER_STATUS,
            pl.odp_label_code as ODP_LABELCODE,
            pl.dropcore_label_code as DROPCORE_LABELCODE,
            pl.kordinat_pelanggan as KOORDINAT_PELANGGAN_BY_TEKNISI,
            pmw.koorPelanggan as KOORDINAT_PELANGGAN_BY_SALES,
            dps.lat as LAT_PELANGGAN,
            dps.lon as LON_PELANGGAN,
            dt.jenis_layanan as JENIS_LAYANAN,
            pls.laporan_status as STATUS_TICKET,
            pl.status_kendala,
            pl.tgl_status_kendala,
            (SELECT pls2.laporan_status FROM psb_laporan_log pll2 LEFT JOIN psb_laporan_status pls2 ON pll2.status_laporan = pls2.laporan_status_id WHERE pll2.psb_laporan_id = pl.id AND pls2.grup IN ("KP", "KT") ORDER BY pll2.created_at ASC LIMIT 1 OFFSET 1) AS status_order2,
            (SELECT pll2.created_at FROM psb_laporan_log pll2 LEFT JOIN psb_laporan_status pls2 ON pll2.status_laporan = pls2.laporan_status_id WHERE pll2.psb_laporan_id = pl.id AND pls2.grup IN ("KP", "KT") ORDER BY pll2.created_at ASC LIMIT 1 OFFSET 1) AS tgl_status_order2,
            (SELECT pls3.laporan_status FROM psb_laporan_log pll3 LEFT JOIN psb_laporan_status pls3 ON pll3.status_laporan = pls3.laporan_status_id WHERE pll3.psb_laporan_id = pl.id AND pls3.grup IN ("KP", "KT") ORDER BY pll3.created_at ASC LIMIT 1 OFFSET 2) AS status_order3,
            (SELECT pll3.created_at FROM psb_laporan_log pll3 LEFT JOIN psb_laporan_status pls3 ON pll3.status_laporan = pls3.laporan_status_id WHERE pll3.psb_laporan_id = pl.id AND pls3.grup IN ("KP", "KT") ORDER BY pll3.created_at ASC LIMIT 1 OFFSET 1) AS tgl_status_order3,
            pl.catatan as CATATAN_TEKNISI,
            pl.typeont,
            pl.snont,
            dt.updated_by as DISPATCH_BY,
            pl.modified_by as MODIF,
            dt.manja as CATATAN_MANJA,
            dt.manjaBy as MANJA_BY,
            dt.manja_updated as MANJA_UPDATED,
            pl.hd_manja as HD_MANJA,
            pl.hd_fallout as HD_FALLOUT,
            dt.tgl as WAKTU_DISPATCH,
            dt.created_at as WAKTU_AWAL_DISPATCH,
            dps.orderDatePs,
            dps.orderDate,
            pl.modified_at as WAKTU_STATUS,
            pll.created_at as tgl_status_berangkat,
            FORMAT(6371 * acos(cos(radians(pl.gps_latitude)) * cos(radians(dps.lat)) * cos(radians(dps.lon) - radians(pl.gps_longitude)) + sin(radians(pl.gps_latitude)) * sin(radians(dps.lat))),3) as jarak_deviasi,
            FORMAT(6371 * acos(cos(radians(pl.gps_latitude)) * cos(radians(SUBSTRING_INDEX(pl.kordinat_odp, ",", 1))) * cos(radians(SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_odp, ",", -1), ",", 1)) - radians(pl.gps_longitude)) + sin(radians(pl.gps_latitude)) * sin(radians(SUBSTRING_INDEX(pl.kordinat_odp, ",", 1)))),3) as deviasi_pelanggan_odp
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN kpro_tr6 kpt ON dt.NO_ORDER = kpt.ORDER_ID
        LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN psb_paket_harga pkh ON pmw.paket_harga_id = pkh.id
        LEFT JOIN psb_paket_sales pks ON pmw.paket_sales_id = pks.id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN psb_laporan_log pll ON pl.id = pll.psb_laporan_id AND pll.status_laporan = 28
        LEFT JOIN maintenance_datel md ON md.sto = dt.dispatch_sto
        WHERE
            dt.jenis_order IN ("SC","MYIR") AND
            (DATE(dt.tgl) LIKE "'.$tgl.'%")
        GROUP BY dt.NO_ORDER
        ');
    }

    public static function download_qc(){
        $query = DB::SELECT('
        SELECT
        dps.orderId,
        dps.internet,
        dps.orderStatus,
        dps.orderDatePs,
        dps.orderName,
        dps.sto as csto,
        qbt.*,
        pls.laporan_status,
        pl.nama_odp,
        r.uraian,
        gt.title as sektor,
        bo.nikTeknisi,
        bo.mitra as bo_mitra,
        ma.mitra_amija_pt as mitra,
        qqc.prioritas
        FROM qc_borneo_tr6 qbt
        LEFT JOIN draft_qc_2021 qqc ON qbt.sc = qqc.sc
        LEFT JOIN Data_Pelanggan_Starclick dps ON qbt.sc = dps.orderIdInteger
        LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
        LEFT JOIN ba_online bo ON qbt.sc = bo.no_wo_int
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
        dt.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
        qqc.sc IS NOT NULL
        GROUP BY qbt.sc
        ORDER BY qbt.datePs DESC');

        return $query;
    }

    public static function data_progress_scbe($tgl,$datel){
        $SQL = '
        SELECT
        c.laporan_status,
        b.id,
        SUM(CASE WHEN d.myir IS NOT NULL AND a.id IS NULL THEN 1 ELSE 0 END) AS undispatch,
        SUM(CASE WHEN a.id IS NOT NULL AND b.id IS NULL THEN 1 ELSE 0 END) AS progress_new,
        a.id as id_dt,';
        for ($i=0;$i<=2;$i++){
            $datex = date('d',strtotime("-$i days"));
            $date = date('Y-m-d',strtotime("-$i days"));
            $SQL .= '
            SUM(CASE WHEN d.myir IS NOT NULL AND DATE(dpsb.ORDER_DATE)="'.$date.'" THEN 1 ELSE 0 END) as order_'.$datex.',
            SUM(CASE WHEN a.id IS NULL AND DATE(dpsb.ORDER_DATE)="'.$date.'" THEN 1 ELSE 0 END) AS undispatch_'.$datex.',
            SUM(CASE WHEN a.id IS NOT NULL AND b.id IS NULL AND DATE(dpsb.ORDER_DATE)="'.$date.'"  THEN 1 ELSE 0 END) AS progress_new_'.$datex.',';
        }
        $SQL .= '
        count(*) as jumlah
        FROM
            Data_Pelanggan_Starclick_Backend dpsb
        LEFT JOIN dispatch_teknisi a ON SUBSTR(dpsb.ORDER_CODE,6,30) = a.Ndem
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN psb_myir_wo d ON SUBSTR(dpsb.ORDER_CODE,6,30) = d.myir
        LEFT JOIN maintenance_datel e ON SUBSTR(d.namaOdp,5,3) = e.sto
        WHERE
            dpsb.STATUS_RESUME IN ("booked","open") AND

            e.datel = "'.$datel.'"
        GROUP BY
            c.laporan_status
        ';

        $query = DB::SELECT($SQL);
        return $query;
    }
     public static function simpanDispatch($req, $status)
     {
    	if ($status == '0')
        {
            $myirDepan = substr($req->myir, 0, 2);
            $ketMyirGangguan = 0;
            if ($myirDepan == '62')
            {
                $ketMyirGangguan = 1;
            };

	    	return DB::table('psb_myir_wo')->insert([
	    		'myir'		        => $req->myir,
	    		'customer' 	        => $req->nmCustomer,
	    		'namaOdp'	        => $req->nama_odp,
	    		'ket'		        => '0',
	    		'lokerJns'	        => 'myir',
                'sto'               => $req->sto,
	    		'orderDate'         => DB::raw('NOW()'),
                'picPelanggan'      => $req->picPelanggan,
                'dispatch'          => 1,
                'sales_id'          => $req->sales,
                'ketMyirGangguan'   => $ketMyirGangguan
	    	]);
    	} else {
    		return DB::table('psb_myir_wo')->where('myir',$req->myir)->update([
	    		'customer' 	    => $req->nmCustomer,
	    		'namaOdp'	    => $req->nama_odp,
                'sto'           => $req->sto,
                'picPelanggan'  => $req->picPelanggan,
                'sales_id'      => $req->sales
	    	]);
    	}
    }

    public static function cariMyir($cari)
    {
        return DB::select('
            SELECT
                dt.*,
                my.*,
                dt.id as id_dt,
                dt.created_at as tgl_awal_dispatch,
                my.id as idMyir,
                r.uraian,
                pls.laporan_status_id,
                pls.laporan_status,
                pl.nama_odp as odpByTeknisi,
                pl.catatan,
                pl.modified_at,
                pl.id as id_pl,
                pl.valins_id,
                pl.dropcore_label_code,
                pl.odp_label_code,
                dps.kcontact as dps_kcontact,
                dps.orderStatus as dps_orderStatus,
                mc.nama_order as mc_nama_order,
                mc.modified_at as mc_modif_at,
                mc.modified_by as mc_modif_by,
                mc.dispatch_regu_name,
                mc.status as mc_status,
                mc.headline as mc_headline,
                mc.action as mc_action,
                mc.port_used as mc_port_used,
                mc.port_idle as mc_port_idle,
                mc.status_name,
                mc.tgl_selesai
            FROM dispatch_teknisi dt
            LEFT JOIN psb_myir_wo my ON (dt.Ndem = my.myir OR dt.Ndem = my.sc)
            LEFT JOIN maintaince mc ON (my.myir = mc.no_tiket OR my.sc = mc.no_tiket)
            LEFT JOIN Data_Pelanggan_Starclick dps ON my.sc = dps.orderId
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
            WHERE
                my.myir = "' . $cari . '"
        ');
    }

    public static function cariMyirStatusSatu($myir){
        $sql = 'SELECT dt.*, r.uraian, pls.laporan_status_id, pls.laporan_status, dps.*, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, my.picPelanggan, pl.nama_odp as odpByTeknisi
                from
                dispatch_teknisi dt
                    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem=dps.orderId
                    LEFT JOIN regu r ON dt.id_regu=r.id_regu
                    LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
                    LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
                    LEFT JOIN psb_myir_wo my ON dps.orderId=my.sc
                where dps.kcontact like "%'.$myir.'%" ';

        return DB::select($sql);
    }

    public static function getMyirByMyir($myir)
    {
        return DB::table('psb_myir_wo')->select('psb_myir_wo.*', 'psb_myir_wo.id as idMyir')->where('myir',$myir)->first();
    }

    public static function getMyir($id){
         $sql = '
         SELECT dt.*, r.uraian, r.crewid, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, my.picPelanggan, r.mainsector, my.sales_id
         FROM dispatch_teknisi dt 
         LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         WHERE my.id="'.$id.'"';

        return DB::select($sql);
    }

    public static function getDataStarClickByMyir($myir){
        return DB::table('Data_Pelanggan_Starclick')->where('kcontact', 'LIKE', '%'.$myir.'%')->whereNotIN('orderStatus',['UNSC','REVOKE ORDER UNSC','REVOKE ORDER','CANCEL COMPLETED','Cancel Order'])->first();
    }

    public static function getDataDispatchByNdem($ndem){
        return DB::table('dispatch_teknisi')->where('Ndem',$ndem)->first();
    }

    public static function hapusNdemMyir($myir){
        return DB::table('dispatch_teknisi')->where('Ndem',$myir)->delete();
    }

    public static function hapusMyir($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->delete();
    }

    public static function ubahKetScMyir($myir, $sc){
        return DB::table('psb_myir_wo')->where('myir',$myir)->update([
            'ket'   => '1',
            'sc'    => $sc
        ]);
    }

    public static function cekDataMyir($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->first();
    }

    public static function getOdpFull($odp){
        $sql = ' SELECT dt.*, pls.laporan_status_id, pls.laporan_status, my.myir, my.customer, my.id as idMyir, my.namaOdp, my.sto, pl.catatan, pl.nama_odp
          from
            dispatch_teknisi dt
          LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
          LEFT JOIN psb_myir_wo my ON my.myir=dt.Ndem
          where
            (pl.nama_odp="'.$odp.'" or my.namaOdp="'.$odp.'") order by dt.updated_at desc limit 1';

        return DB::select($sql);
        // pls.laporan_status_id=24 and  (pl.nama_odp="'.$odp.'" or my.namaOdp="'.$odp.'") order by dt.updated_at desc limit 1';
    }

    public static function updateDispatch($myir){
        return DB::table('psb_myir_wo')->where('myir',$myir)->update(['dispatch' => 1]);
    }

}
