<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;
use DateTime;

date_default_timezone_set('Asia/Makassar');

class EmployeeModel
{
	public static function getMitraAmija(){
		return DB::table('mitra_amija')->get();
	}
	public static function getAll()
	{
		return DB::SELECT('
			SELECT emp.*, emp.nik as id, emp.nama as text,
			r.uraian as tim
			FROM 1_2_employee emp
			LEFT JOIN regu r ON emp.id_regu = r.id_regu
			WHERE
			emp.nik IS NOT NULL AND
			emp.nama IS NOT NULL AND
			emp.ACTIVE = 1 AND
			emp.Witel_New = "'.session('witel').'"
			ORDER BY emp.last_update DESC
		');
	}

	public static function jml_teknisi($posisi_id){
		return DB::SELECT('select count(*) as jumlah from 1_2_employee where posisi_id = '.$posisi_id);
	}

	public static function jml_teknisi_sektor_1($posisi_id,$type,$date)
	{
		switch ($type) {
			case "PT1" :
				$where_type = 'AND a.pt1 = "1" AND aa.checklist_addon IS NULL ';
			break;
			case "SABER" :
				$where_type = 'AND a.saber = "1" AND aa.checklist_addon IS NULL ';
			break;
			case "ADDON" :
				$where_type = 'AND a.pt1 = "1" AND aa.checklist_addon = 1';
			break;
			default:
				$where_type = '';
			break;
		}
		return DB::SELECT('
			SELECT
				COUNT(*) as jumlah,
				SUM(CASE WHEN b.status = "HADIR" THEN 1 ELSE 0 END) as hadir,
				SUM(CASE WHEN b.status = "HADIR" AND b.approval = 1 THEN 1 ELSE 0 END) as approved,
				SUM(CASE WHEN b.approval = 0 THEN 1 ELSE 0 END) as not_approved,
				SUM(CASE WHEN b.approval IS NULL THEN 1 ELSE 0 END) as belum_absen,
				SUM(CASE WHEN aa.status = 1 THEN 1 ELSE 0 END) as scheduled_besok,
				SUM(CASE WHEN aaa.status = 1 THEN 1 ELSE 0 END) as scheduled_hi,
				0 as absen,
				c.title
			FROM 1_2_employee a
			LEFT JOIN group_telegram c ON a.mainsector = c.chat_id
			LEFT JOIN absen b ON a.nik = b.nik AND date(b.date_created) = "'.$date.'"
			LEFT JOIN 1_2_employee_schedule aa ON a.nik = aa.nik AND date(aa.date) = "'.date('Y-m-d', strtotime("+1 days", strtotime($date))).'"
			LEFT JOIN 1_2_employee_schedule aaa ON a.nik = aaa.nik AND date(aaa.date) = "'.$date.'"
			WHERE
				c.ket_posisi = "PROV" AND c.id NOT IN (97, 103) AND
				a.posisi_id = '.$posisi_id.'
				'.$where_type.'
			GROUP BY c.title
		');
	}

	public static function jml_teknisi_sektor($posisi_id,$date)
	{
		return DB::SELECT('
		SELECT
			c.title,
			COUNT(*) as jumlah,
			SUM(CASE WHEN b.status = "HADIR" THEN 1 ELSE 0 END) as jml_hadir,
			SUM(CASE WHEN b.status = "HADIR" AND b.approval = 1 THEN 1 ELSE 0 END) as jml_approved,
			SUM(CASE WHEN b.approval = 0 THEN 1 ELSE 0 END) as jml_not_approved,
			SUM(CASE WHEN aa.status = 1 AND aa.checklist_addon IS NULL THEN 1 ELSE 0 END) as jml_scheduled_besok,
			SUM(CASE WHEN aa.status = 1 AND aa.checklist_addon = "1" THEN 1 ELSE 0 END) as jml_scheduled_besok_mo,
			SUM(CASE WHEN aa.status = 1 THEN 1 ELSE 0 END) as jml_scheduled_besok,
			SUM(CASE WHEN a.pt1 = 1 AND a.saber = 0 THEN 1 ELSE 0 END) as jml_teknisi_pt1,
			SUM(CASE WHEN a.pt1 = 0 AND a.saber = 1 THEN 1 ELSE 0 END) as jml_teknisi_saber
		FROM 1_2_employee a
		LEFT JOIN group_telegram c ON c.chat_id = a.mainsector
		LEFT JOIN absen b ON a.nik = b.nik AND date(b.date_created) = "'.$date.'"
		LEFT JOIN 1_2_employee_schedule aa ON a.nik = aa.nik AND date(aa.date) = "'.date('Y-m-d', strtotime("+1 days", strtotime($date))).'"
		WHERE
			c.ket_posisi = "PROV" AND c.id NOT IN (97, 103) AND
			a.posisi_id = '.$posisi_id.'
		GROUP BY c.title
		');
	}

	public static function teknisi_sektor($sektor,$posisi_id)
	{
		switch ($sektor) {
			case 'UNMAPPING':
				$where_sektor = 'AND c.title IS NULL';
			break;
			case 'ALL' :
				$where_sektor = '';
			break;
			default:
				$where_sektor = 'AND c.title LIKE "' . $sektor . '%"';
			break;
		}

		return DB::SELECT('
		SELECT
			*,
			a.sto as a_sto,
			a.nik as a_nik
		FROM 1_2_employee a
		LEFT JOIN group_telegram c ON c.chat_id = a.mainsector
		LEFT JOIN 1_2_employee_schedule aa ON a.nik = aa.nik AND DATE(aa.date) = "'.date('Y-m-d', strtotime("+1 days")).'"
		LEFT JOIN 1_2_employee_schedule_status aaa ON aa.status = aaa.schedule_status_id
		LEFT JOIN regu b ON a.id_regu = b.id_regu AND b.ACTIVE = 1 AND b.jenis_regu IS NULL
		WHERE
			c.ket_posisi = "PROV" AND c.id NOT IN (97, 103) AND
			a.posisi_id = '.$posisi_id.'
			'.$where_sektor.'
		GROUP BY a.nik
		');
	}

	public static function jml_teknisi_hadir($posisi_id,$date){
		$where_position = '';
		if ($posisi_id<>"ALL"){
			$where_position = 'AND a.posisi_id = '.$posisi_id;
		}
		return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik WHERE 1 '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR"
		');
	}

	public static function jml_teknisi_hadir_pt1($posisi_id,$date){
		$where_position = '';
		if ($posisi_id<>"ALL"){
			$where_position = 'AND a.posisi_id = '.$posisi_id;
		}
		return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik WHERE a.pt1 = 1 AND a.saber = 0  '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR"
		');
	}
	public static function jml_teknisi_hadir_saber($posisi_id,$date){
		$where_position = '';
		if ($posisi_id<>"ALL"){
			$where_position = 'AND a.posisi_id = '.$posisi_id;
		}
		return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik WHERE a.saber = 1  '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR"
		');
	}

	public static function jml_teknisi_hadir_approved($posisi_id,$date){
		$where_position = '';
		if ($posisi_id<>"ALL"){
			$where_position = 'AND a.posisi_id = '.$posisi_id;
		}
		return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik WHERE 1 '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR" AND b.approval = 1
		');
	}

	public static function jml_teknisi_hadir_not_approved($posisi_id,$date){
		$where_position = '';
		if ($posisi_id<>"ALL"){
			$where_position = 'AND a.posisi_id = '.$posisi_id;
		}
		return DB::SELECT('SELECT count(*) as jumlah FROM 1_2_employee a LEFT JOIN absen b ON a.nik = b.nik WHERE 1 '.$where_position.' AND date(b.date_created) = "'.$date.'" AND b.status = "HADIR" AND b.approval = 0
		');
	}

	public static function kehadiranList($sektor, $status, $date)
	{
		switch ($status) {
			case 'HADIR':
				$statusx = 'AND b.status = "HADIR" AND DATE(b.date_created) = "' . $date . '"';
				break;

			case 'ABSEN':
				$statusx = '';
				break;

			case 'APPROVED':
				$statusx = 'AND b.status = "HADIR" AND b.approval = 1 AND DATE(b.date_created) = "' . $date . '"';
				break;

			case 'NOT_APPROVED':
				$statusx = 'AND b.approval = 0 AND DATE(b.date_created) = "' . $date . '"';
				break;

			case 'PT1':
				$statusx = 'AND a.pt1 = 1 AND a.saber = 0';
				break;

			case 'SABER':
				$statusx = 'AND a.pt1 = 0 AND a.saber = 1';
				break;

			case 'H1_PT1':
				$statusx = 'AND aa.status = 1 AND aa.checklist_addon IS NULL';
				break;

			case 'H1_MO':
				$statusx = 'AND aa.status = 1 AND aa.checklist_addon = "1"';
				break;
		}

		switch ($sektor) {
			case 'ALL':
				$where_sektor = '';
				break;

			default:
				$where_sektor = 'AND c.title = "' . $sektor . '"';
				break;
		}

		return DB::SELECT('
			SELECT
				a.*,
				b.status,
				b.date_created as waktu_absen,
				b.date_approval,
				c.title as nama_sektor,
				d.uraian as nama_tim
			FROM `1_2_employee` a
			LEFT JOIN absen b ON a.nik = b.nik
			LEFT JOIN `1_2_employee_schedule` aa ON a.nik = aa.nik AND date(aa.date)="' . date('Y-m-d', strtotime("+1 days", strtotime($date))) . '"
			LEFT JOIN group_telegram c ON c.chat_id = a.mainsector
			LEFT JOIN regu d ON (a.nik = d.nik1 OR a.nik = d.nik2) AND d.ACTIVE = 1 AND d.jenis_regu IS NULL
			WHERE
				c.ket_posisi = "PROV" AND c.id NOT IN (97, 103) AND
				a.posisi_id = 1
				' . $where_sektor . '
				' . $statusx . '
			GROUP BY a.nik
		');
	}

	public static function getById($id){
		$data =  DB::SELECT('
			SELECT
				a.*,
				b.mitra_amija_pt
			FROM
				1_2_employee a
			LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
			WHERE
				a.id_people = "'.$id.'"
		');

		if(!empty($data)){
			return DB::SELECT('
						SELECT
							a.*,
							b.mitra_amija_pt
						FROM
							1_2_employee a
						LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
						WHERE
							a.id_people = "'.$id.'"
					')[0];
		}
		else{
			return DB::SELECT('
					SELECT
						a.*,
						b.mitra_amija_pt
					FROM
						1_2_employee a
					LEFT JOIN mitra_amija b ON a.mitra_amija = b.mitra_amija
					WHERE
						a.id_people = "'.$id.'"
				');
		}
		// return DB::table('1_2_employee')->where('id_people', $id)->first();
	}
	public static function save($req){
		return DB::table('1_2_employee')->insert([
			'nik' => $req->input('nik')
		]);
	}

	public static function getSQUAD(){
		return DB::table('employee_squad')->get();
	}

	public static function update($data,$nikUpdate)
	{
		date_default_timezone_set('Asia/Makassar');
		$pt1 = $saber = $nte = $mytech = 0;
		if (@$data->skillset['pt1'] == 1)
		{
			$pt1 = 1;
		}
		if (@$data->skillset['saber'] == 1) {
			$saber = 1;
		}
		if (@$data->skillset['nte'] == 1) {
			$nte = 1;
		}
		if (@$data->skillset['mytech'] == 1) {
			$mytech = 1;
		}
		return DB::table('1_2_employee')
			->where('id_people', $data->input('id_people'))
			->update([
				'nik' => $data->input('nik'),
				'nama' => $data->input('nama'),
				'no_telp' => $data->input('no_telp'),
				'chat_id' => $data->input('chat_id'),
				'tgl_lahir' => $data->input('tgl_lahir'),
				'tempat_lahir' => $data->input('tempat_lahir'),
				'alamat' => $data->input('alamat'),
				'sto' => $data->input('sto'),
				'sub_directorat' => "Consumer Service",
				'sub_sub_unit' => "MULTISKILL",
				'position' => $data->input('position'),
				'id_sobi' => $data->input('id_sobi'),
				'atasan_1' => $data->input('atasan_1'),
				'status' => "PKS",
				'status_konfirmasi' => "Valid",
				'CAT_JOB' => $data->input('cat_job'),
				'laborcode' => $data->input('laborcode'),
				'idcrew' => $data->input('idcrew'),
				'ACTIVE' => $data->input('active'),
				'Witel_New' => $data->input('witel'),
				'mitra_amija' => $data->input('mitra_amija'),
				'nik_amija' => $data->input('nik_amija'),
				// 'squad' => $data->input('squad'),
				'updateBy'	=> $nikUpdate,
				'pt1' => $pt1,
				'saber' => $saber,
				'nte' => $nte,
				'mytech' => $mytech,
				'posisi_id' => $data->input('posisi_id'),
				'mainsector' => $data->input('mainsector'),
				'id_regu' => $data->input('id_regu'),
				'last_update'   => date('Y-m-d H:i:s'),
				'dateUpdate'	=> date('Y-m-d H:i:s')
			]);
	}
	public static function store($data,$nikUpdate)
	{
		date_default_timezone_set('Asia/Makassar');
		$pt1 = $saber = $nte = $mytech = 0;
		if (@$data->skillset['pt1'] == 1)
		{
			$pt1 = 1;
		}
		if (@$data->skillset['saber'] == 1) {
			$saber = 1;
		}
		if (@$data->skillset['nte'] == 1) {
			$nte = 1;
		}
		if (@$data->skillset['mytech'] == 1) {
			$mytech = 1;
		}

		return DB::table('1_2_employee')
			->insert([
				'nik' => $data->input('nik'),
				'nama' => $data->input('nama'),
				'no_telp' => $data->input('no_telp'),
				'chat_id' => $data->input('chat_id'),
				'tgl_lahir' => $data->input('tgl_lahir'),
				'tempat_lahir' => $data->input('tempat_lahir'),
				'alamat' => $data->input('alamat'),
				'sto' => $data->input('sto'),
				'sub_directorat' => "Consumer Service",
				'sub_sub_unit' => "MULTISKILL",
				'position' => $data->input('position'),
				'id_sobi' => $data->input('id_sobi'),
				'atasan_1' => $data->input('atasan_1'),
				'status' => "PKS",
				'status_konfirmasi' => "Valid",
				'CAT_JOB' => $data->input('cat_job'),
				'laborcode' => $data->input('laborcode'),
				'idcrew' => $data->input('idcrew'),
				'ACTIVE' => $data->input('active'),
				'Witel_New' => $data->input('witel'),
				'mitra_amija' => $data->input('mitra_amija'),
				'nik_amija' => $data->input('nik_amija'),
				// 'squad' => $data->input('squad'),
				'updateBy'	=> $nikUpdate,
				'pt1' => $pt1,
				'saber' => $saber,
				'nte' => $nte,
				'mytech' => $mytech,
				'posisi_id' => $data->input('posisi_id'),
				'mainsector' => $data->input('mainsector'),
				'id_regu' => $data->input('id_regu'),
				'last_update'   => date('Y-m-d H:i:s'),
				'dateUpdate'	=> date('Y-m-d H:i:s')
			]);
	}
	public static function distinctPSA(){
		return DB::table('1_3_witel')->select('City as id', 'City as text')->get();
	}

	public static function distinctCATJOB(){
		return DB::table('1_2_employee')->select('CAT_JOB as id', 'CAT_JOB as text')->groupBy('CAT_JOB')->orderBy('CAT_JOB')->get();
	}
	public static function distinctSubDirectorat(){
		return DB::table('1_2_employee')->select('sub_directorat as id', 'sub_directorat as text')->groupBy('sub_directorat')->orderBy('sub_directorat')->get();
	}
	public static function distinctSubSubUnit(){
		return DB::table('1_2_employee')->select('sub_sub_unit as id', 'sub_sub_unit as text')->groupBy('sub_sub_unit')->orderBy('sub_sub_unit')->get();
	}
	public static function distinctPosition(){
		return DB::table('1_2_employee')->select('position as id', 'position as text')->groupBy('position')->orderBy('position')->get();
	}
	public static function getCuaca($date)
	{
		return DB::SELECT('
		SELECT a.kota,
		b.cuaca_status as status_jam0,
		b.icon as icon_jam0,
		c.cuaca_status as status_jam6,
		c.icon as icon_jam6,
		d.cuaca_status as status_jam12,
		d.icon as icon_jam12,
		e.cuaca_status as status_jam16,
		e.icon as icon_jam16
		FROM cuaca a
		LEFT JOIN cuaca_status b ON a.jam0 = b.cuaca_status_id
		LEFT JOIN cuaca_status c ON a.jam6 = c.cuaca_status_id
		LEFT JOIN cuaca_status d ON a.jam12 = d.cuaca_status_id
		LEFT JOIN cuaca_status e ON a.jam16 = e.cuaca_status_id
		WHERE a.cuaca_tgl = "'.$date.'"
		');
	}
}
