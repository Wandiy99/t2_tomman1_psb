<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class QcondeskModel{

  public static function checkQCOndesk(){
    $nik = session('auth')->id_user;
    return DB::SELECT('
      SELECT
        *,
        b.id as id_dt,
        0 as rebUmur,
        null as qcNama,
        null as qcPosition, 
        null as jenisPsb,
        null as ND,
        null as datek,
        null as no_tiket,
        null as laporan_status,
        null as orderDate,
        null as sto_dps,
        null as neSTO,
        null as alproname,
        null as neRK,
        null as orderNcli,
        null as noTelp,
        null as neND_TELP,
        null as neND_INT,
        null as internet,
        null as nePRODUK_GGN,
        null as orderName,
        null as neLOKER_DISPATCH,
        null as orderKontak,
        null as neHEADLINE,
        null as orderCity,
        null as neKANDATEL,
        null as duration,
        null as status_gangguan,
        null as KcontactSC
      FROM
        qcondesk a
      LEFT JOIN dispatch_teknisi b On a.qcondesk_order = b.id
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN 1_2_employee d ON a.qcondesk_by = d.nik
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      WHERE
        (c.nik1 = "'.$nik.'" OR
        c.nik2 = "'.$nik.'") AND
        e.status_laporan <> 1
    ');
  }

  public function datediff($start,$end){
    $start = new DateTime(($start));
    $end = new DateTime(($end));
    $interval = $start->diff($end);
    return $interval->h + ($interval->days*24);
  }


  public static function rejectSave($req,$auth){
    date_default_timezone_set('Asia/Makassar');
    $id = $req->input('id');
    $alasan = $req->input('alasan');
    $status = $req->input('status');
    $user = $auth->id_user;
    if ($user==""){
      return false;
    }
    $xcheck = DB::table('qcondesk')->where('qcondesk_order',$id)->get();
    $get_order = DB::SELECT('SELECT *,b.id as idpsb FROM dispatch_teknisi a LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj WHERE a.id = "'.$id.'"');
    DB::table('psb_laporan')->where('id_tbl_mj',$id)
    ->update([
      'modified_at'   => date('Y-m-d H:i:s'),
      'modified_by'   => $user,
      'catatan'       => $alasan." [QCONDESK]",
      'status_laporan'=> 6,
      'action'        => 35,
      'penyebabId'    => 41
    ]);

       DB::table('psb_laporan_log')->insert([
         'created_at'      => date('Y-m-d H:i:s'),
         'created_by'      => $user,
         'status_laporan'  => 6,
         'id_regu'         => $get_order[0]->id_regu,
         'catatan'         => $alasan." [QCONDESK]",
         'action'          => 35,
         'Ndem'            => $get_order[0]->Ndem,
         'psb_laporan_id'  => $get_order[0]->idpsb,
         'penyebabId'      => 41
       ]);


    if (count($xcheck)>0){
      $save = DB::table('qcondesk')->where('qcondesk_order',$id)
                  ->update([
                    'qcondesk_by' => $user,
                    'qcondesk_alasan' => $alasan,
                    'qcondesk_status' => $status,
                    'qcondesk_datetime' => date('Y-m-d H:i:s')
                  ]);
    } else {
      $save = DB::table('qcondesk')->insert([
        'qcondesk_by' => $user,
        'qcondesk_order' => $id,
        'qcondesk_alasan' => $alasan,
        'qcondesk_status' => $status,
        'qcondesk_datetime' => date('Y-m-d H:i:s')
      ]);
    }
    return $save;
  }

  public static function order($id){
    return DB::SELECT('
      SELECT
        a.*
      FROM
        dispatch_teknisi a
      LEFT JOIN data_nossa_1_log b ON a.Ndem = b.Incident
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      WHERE
        a.id = "'.$id.'"
    ');
  }

  public static function list_TL($periode,$TL_NIK){
    if ($TL_NIK=="91153709" || $TL_NIK =="88159353" || $TL_NIK =="user"){
      $where_TL_NIK = '';
    } else {
      $where_TL_NIK = '(c.TL_NIK = "'.$TL_NIK.'" OR c.Nik_Atl="'.$TL_NIK.'" OR c.Nik_Atl2="'.$TL_NIK.'") AND';
    }
    $query = DB::SELECT('
      SELECT
      *,  
      a.id as id_dt
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_action e ON d.action = e.laporan_action_id
      LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
      LEFT JOIN psb_laporan_penyebab g ON d.penyebabId = g.idPenyebab
      LEFT JOIN returnDropcore h ON a.Ndem = h.Incident
      LEFT JOIN roc ON a.Ndem = roc.no_tiket
      LEFT JOIN qcondesk q ON a.id = q.qcondesk_order
      WHERE
        '.$where_TL_NIK.'
        (d.status_laporan = 1 OR (d.status_laporan = 6 AND q.qcondesk_status = "rejected")) AND
        e.statusqc IN (0,1) AND
        a.TGL LIKE "'.$periode.'%"
      ORDER BY
        d.modified_at DESC
    ');
    return $query;
  }

  public static function list_TL_Prov($periode,$TL_NIK){
    if ($TL_NIK=="91153709" || $TL_NIK =="88159353" || $TL_NIK =="user"){
      $where_TL_NIK = '';
    } 
    elseif($TL_NIK=="94150248"){
      $where_TL_NIK = 'c.id in (44, 65, 66, 14) AND ';
    }
    else {
      $where_TL_NIK = '(c.TL_NIK = "'.$TL_NIK.'" OR c.Nik_Atl="'.$TL_NIK.'" OR c.Nik_Atl2="'.$TL_NIK.'") AND';
    }
    $query = DB::SELECT('
      SELECT
      *,
      a.id as id_dt,
      c.title
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick i ON a.Ndem = i.orderId
      LEFT JOIN qcondesk q ON a.id = q.qcondesk_order
      WHERE
        '.$where_TL_NIK.'
        (d.status_laporan = 1 OR (d.status_laporan = 6 AND q.qcondesk_status = "rejected")) AND
        i.orderId<>"" AND
        a.TGL LIKE "'.$periode.'%"
      ORDER BY
        c.title asc, d.modified_at DESC
    ');
    return $query;
  }

}
