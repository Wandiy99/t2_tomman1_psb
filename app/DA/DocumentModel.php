<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class DocumentModel
{

    public static function documentUnit(){
        return DB::table('employee_unit')->get();
    }

    public static function documentType(){
        return DB::table('employee_document_type')->get();
    }

    public static function documentInternal(){
        return DB::SELECT('
          SELECT
            nik as employee_nik,
            user as employee_name,
            jabatan as employee_position
          FROM
            procurement_user
        ');
    }

    public static function documentExternal(){
      return DB::table('procurement_mitra')->get();
    }

    public static function documentJob($NIK){
      return DB::table('procurement_user')->where('nik',$NIK)->get();
    }

    public static function documentGet($NIK){
      return DB::SELECT('
        SELECT
        a.*,
        b.documentType
        FROM
          employee_document a
        LEFT JOIN employee_document_type b ON a.documentType = b.documentTypeID
        WHERE
        a.documentUserCreated = "'.$NIK.'"
      ');
    }

    public static function documentInternalList($NIK){
      return DB::SELECT('
        SELECT
        b.documentUserCreated as documentFrom,
        b.documentTitle,
        b.documentDate,
        c.documentType,
        b.documentID
        FROM
          employee_document_internal a
        LEFT JOIN employee_document b ON a.documentID = b.documentID
        LEFT JOIN employee_document_type c ON b.documentType = c.documentTypeID
        WHERE
          a.documentInternal = "'.$NIK.'" AND
          b.documentUserCreated <> "'.$NIK.'"
      ');
    }

    public static function documentExternalList($dateStart,$dateEnd,$mitra,$type){
      $where_mitra = '';
      if ($mitra<>"ALL") {
        $where_mitra = 'AND a.documentExternal = "'.$mitra.'" ';
      }
      $where_type = '';
      if ($type<>"ALL"){
        $where_type = 'AND b.documentType = "'.$type.'"';
      }

      $query = DB::SELECT('
      SELECT
      *
      FROM
        employee_document_external a
      LEFT JOIN employee_document b ON a.documentID = b.documentID
      LEFT JOIN employee_document_type c ON b.documentType = c.documentTypeID
      LEFT JOIN procurement_mitra d ON a.documentExternal = d.id
      LEFT JOIN procurement_user e ON b.documentUserCreated = e.nik
      WHERE
        (b.documentDate BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'")
        '.$where_mitra.'
        '.$where_type.'
      ');
      return $query;
    }

    public static function documentTypeDetail($ID){
      return DB::table('employee_document_type')->where('documentTypeID',$ID)->first();
    }

    public static function documentMitraDetail($ID){
      return DB::table('procurement_mitra')->where('id',$ID)->first();
    }

    public static function documentDetails($ID){
      return DB::SELECT('
        SELECT
          a.*,
          b.documentType
        FROM
          employee_document a
        LEFT JOIN employee_document_type b ON a.documentType = b.documentTypeID
        WHERE
          a.documentID = "'.$ID.'"
      ');
    }

    public static function documentDetailsInternal($ID){
      return DB::SELECT('
        SELECT
        b.*
        FROM
          employee_document_internal a
        LEFT JOIN procurement_user b ON a.documentInternal = b.nik
        WHERE
          a.documentID = "'.$ID.'"
      ');
    }

    public static function documentDetailsExternal($ID){
      return DB::SELECT('
        SELECT
          b.id,
          b.nama_company
        FROM
          employee_document_external a
        LEFT JOIN procurement_mitra b ON a.documentExternal = b.id
        WHERE
          a.documentID = "'.$ID.'"
      ');
    }

    public static function documentDashboardExternal($dateStart,$dateEnd){
      $q = DB::table('employee_document_type')->get();
      $select = '';
      foreach ($q as $r){
        $select .= 'SUM(CASE WHEN c.documentType = '.$r->documentTypeID.' THEN 1 ELSE 0 END) as jml_'.$r->documentTypeID.',';
      }
      $q_main = DB::SELECT('
        SELECT
          '.$select.'
          b.nama_company,
          b.id
        FROM
          employee_document_external a
        LEFT JOIN procurement_mitra b ON a.documentExternal = b.id
        LEFT JOIN employee_document c ON a.documentID = c.documentID
        WHERE
          c.documentDate BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'"
        GROUP BY b.id
      ');
      return $q_main;
    }

    public static function uploadSave($input){
      $documentID = DB::table('employee_document')->insertGetId([
        'documentTitle' => $input->input('documentTitle'),
        'documentDate' => $input->input('documentDate'),
        'documentType' => $input->input('documentType'),
        'documentUserCreated' => session('auth')->id_karyawan
      ]);

      $documentUnit = $input->input('documentUnit');
      for ($i=0;$i<count($documentUnit);$i++){
        $result = DB::table('employee_document_unit')->insert([
          'documentID' => $documentID,
          'documentUnit' => $documentUnit[$i]
        ]);
      }

      $documentInternal = $input->input('documentInternal');
      for ($i=0;$i<count($documentInternal);$i++){
        $result = DB::table('employee_document_internal')->insert([
          'documentID' => $documentID,
          'documentInternal' => $documentInternal[$i]
        ]);
      }

      $documentExternal = $input->input('documentExternal');
      for ($i=0;$i<count($documentExternal);$i++){
        $result = DB::table('employee_document_external')->insert([
          'documentID' => $documentID,
          'documentExternal' => $documentExternal[$i]
        ]);
      }

      // store file
      $path = '/mnt/hdd4/upload/employee_document/'.$documentID.'/';
      $file = $input->file('documentFile');
      $file->move($path,$documentID.'DOK'.$file->getClientOriginalName());

      return true;
    }

    public static function jsonDocumentInternal(){
        return DB::SELECT('
          SELECT
            nik as employee_nik,
            user as employee_name,
            jabatan as employee_position
          FROM
            procurement_user
          WHERE
            posisi IN ("MGR","SM")
        ');
    }

}
