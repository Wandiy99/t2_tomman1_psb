<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class CcanModel{


public static function get_sektor(){
    return DB::SELECT('SELECT * FROM group_telegram a WHERE (NOT a.title LIKE "TERR%" AND NOT a.title LIKE "INDIBOX%") AND a.sms_active_prov = 1 ORDER BY urut ASC');
  }

public static function matrix_team_prov($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.Ndem = g.orderId
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      WHERE
      b.ACTIVE=1 AND 
      (f.myir IS NOT NULL OR g.orderId IS NOT NULL OR mw.ND IS NOT NULL) AND 
      c.chat_id = "'.$mainsector.'" AND 
      (a.tgl="'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

public static function matrix_team_order_prov($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS,
      0 as GAUL,
      a.created_at as Reported_Date,
      a.updated_by as Assigned_by,
      c.id as id_pl,
      jl.KAT as jenis_layanan
      FROM
        dispatch_teknisi a
      INNER JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND AND YEAR(mw.created_at) = "'.date('Y').'"
      LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
      WHERE
      ((a.tgl="'.$date.'" AND (DATE(mw.created_at) >= "'.date('Y-m-d').'" OR c.id IS NULL))  OR c.status_laporan IN (74,4)) AND
      b.ACTIVE=1 AND
      (f.myir IS NOT NULL OR mw.ND IS NOT NULL) AND
      b.id_regu = "'.$id_regu.'" AND
      YEAR(a.tgl)="'.date('Y').'"
      ORDER BY
      a.created_at
    ');
  }

  public static function sektor_asr_matrix_team($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND c.chat_id = "'.$mainsector.'" AND (a.tgl="'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

    public static function sektor_asr_matrix_team_order($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN data_nossa_1_log e ON a.Ndem = e.Incident
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND b.id_regu = "'.$id_regu.'" AND (a.tgl="'.$date.'" OR DATE(c.modified_at) = "'.$date.'" OR (c.status_laporan = 1 AND DATE(c.modified_at) = "'.$date.'") OR
      ((d.grup IN ("OGP","KP","KT","SISA ") OR c.id IS NULL) AND YEAR(a.tgl)="'.date('Y').'"))
      ORDER BY 
      e.Reported_Date 
    ');
  }

    public static function search($param){
    $query = DB::SELECT('
      SELECT
        *,
        dt.id_regu as id_r,
        dt.id as id_dt,
        mw.ND as mw_nd,
        mw.Nama as mw_nama,
        mw.Alamat as mw_alamat,
        mw.MDF as mw_mdf,
        mw.ODP as mw_odp,
        mw.jenis_layanan as mw_jl,
        mw.layanan as mw_layanan,
        mw.pic as mw_pic,
        mw.loker as mw_loker,
        mw.jenis_tiket as mw_jt,
        pl.id as pl_id
      FROM
        dispatch_teknisi dt
      LEFT JOIN micc_wo mw ON dt.Ndem = mw.ND
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      WHERE
      dt.Ndem LIKE "%'.$param.'%" OR mw.ND LIKE "%'.$param.'%"
         ');
    return $query;
  }

}