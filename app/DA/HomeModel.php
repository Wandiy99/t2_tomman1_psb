<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;
date_default_timezone_set('Asia/Makassar');
class HomeModel
{


  public static function active_team_p($nik,$nik_TL){
    $query = DB::SELECT('
      SELECT
        a.uraian,
        a.id_regu,
        a.crewid,
        a.active
      FROM
        regu a
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      WHERE
        (a.nik1 = "'.$nik.'" OR a.nik2 = "'.$nik.'") AND
        (b.TL_NIK = "'.$nik_TL.'" OR b.Nik_Atl = "'.$nik_TL.'" OR b.Nik_Atl2 = "'.$nik_TL.'")
      ');
    return $query;
  }

  public static function active_team($nik)
  {  
    $query = DB::SELECT('
      SELECT
        d.status as status_kehadiran,
        d.date_created as waktu_absen,
        d.tglAbsen,
        d.kesehatan,
        d.approval,
        d.date_approval,
        d.absen_id,
        c.nik as NIK,
        c.laborcode,
        c.nama,
        c.status as status_emp,
        e.squad,
        a.mitra,
        a.sttsWo
      FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON c.nik = d.nik
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
      WHERE
        (DATE(d.date_created) = "'.date('Y-m-d').'") AND
        (b.TL_NIK = "'.$nik.'" OR b.Nik_Atl = "'.$nik.'" OR b.Nik_Atl2 = "'.$nik.'" ) AND
        d.divisi = "OPS" AND
        a.ACTIVE <> "0"
      GROUP BY c.nik
      ORDER BY d.status ASC
    ');
    return $query;
  }

  public static function active_team_Briefing($nik){
    
    $query = DB::SELECT('
      SELECT
        *,
        d.status as status_kehadiran,
        d.date_created as waktu_absen,
        c.nik as NIK,
        c.status as status_emp,
        e.squad,
        a.mitra,
        a.sttsWo
      FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON (c.nik = d.nik AND date(d.date_created) = "'.date('Y-m-d').'")
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
      WHERE
        (b.TL_NIK = "'.$nik.'" OR b.Nik_Atl = "'.$nik.'" OR b.Nik_Atl2 = "'.$nik.'" ) and a.ACTIVE<>0 
      GROUP BY c.nik
      ORDER BY d.status ASC
    ');
    return $query;
  }

  public static function group_telegram($nik){
    $query = DB::table('group_telegram')->where('TL_NIK',$nik)->orwhere('Nik_Atl',$nik)->orwhere('Nik_Atl2',$nik)->get();
    return $query;
  }

  public static function update_absen_by_id($absen_id,$keterangan,$approval){
    $query = DB::table('absen')->where('absen_id',$absen_id)->update([
      'keterangan' => $keterangan,
      'approval' => $approval,
      'date_approval' => date('Y-m-d h:n:s')
    ]);
    return $query;
  }

  public static function approve_absen($nik,$date){
    $auth = session('auth');
    $query = DB::table('absen')->where('nik',$nik)->where('date_created','LIKE',$date.'%')->update(['approval'=>1,'date_approval'=>date('Y-m-d H:n:s'),'absenEsok' =>2, 'approve_by' => $auth->id_user]);
  }

  public static function absen_approval($nik){
	  

    $query = DB::SELECT('
      SELECT
      a.*,
      b.*,
      a.date_created,
      c.title,
      d.nama
      FROM
        absen a
      LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN 1_2_employee d ON a.nik = d.nik
      WHERE
        (c.TL_NIK = "'.$nik.'" OR c.Nik_Atl = "'.$nik.'" OR c.Nik_Atl2="'.$nik.'" ) AND
        date(a.date_created) = "'.date('Y-m-d').'"
      ORDER BY
        a.approval ASC
    ');
    return $query;
  }

  public static function status_absen_by_id($id){
    $query = DB::SELECT('SELECT * FROM absen a LEFT JOIN 1_2_employee b ON a.nik = b.nik WHERE a.absen_id = "'.$id.'"');
    return $query;
  }

  public static function status_absen($nik)
  {
    return DB::select('SELECT * from absen where nik = "'.$nik.'" AND DATE(date_created) = "'.date('Y-m-d').'" AND divisi = "OPS"');
  }

  public static function regu_teknisi($nik)
  {
    return DB::table('1_2_employee')
    ->leftJoin('user', '1_2_employee.nik', '=', 'user.id_karyawan')
    ->leftJoin('regu', '1_2_employee.id_regu', '=', 'regu.id_regu')
    ->leftJoin('group_telegram', 'regu.mainsector', '=', 'group_telegram.chat_id')
    ->select('1_2_employee.nik', 'user.level', 'regu.uraian', 'group_telegram.title', 'group_telegram.ket_posisi')
    ->where('1_2_employee.nik', $nik)
    ->first();
  }

  public static function status_absenEsok($nik)
  {
    $tgl = mktime(0,0,0,date('m'),date('d')+1,date('Y'));
    $query = DB::SELECT('select * from absen a where a.nik = "'.$nik.'" AND date(a.date_created)="'.date('Y-m-d',$tgl).'"');
    return $query;
  }

  public static function absen($nik, $status)
  {
    $check = DB::table('absen')->where('nik', $nik)->where('status', $status)->whereDate('date_created', date('Y-m-d'))->first();
    $return = array();
    if (count($check) > 0)
    {
      $check_data = $check;
      $return['status'] = "danger";
      $return['message'] = "Anda sudah absen pada ".$check_data->date_created;
    } else {
      
      DB::table('absen')->insert([
        'divisi' => 'OPS',
        'nik' => $nik,
        'status' => $status
      ]);

      $return['status'] = "success";
      $return['message'] = "Anda berhasil absen";
    }
    return $return;
  }

  public static function simpanUpload($req){
    
    $query =  DB::table('absen_upload')->insertGetId([
        'datetime_upload'   => date('Y-m-d H:i:S'),
        'status_absen'		  => $req->status_absen,
        'status_kesehatan'	=> $req->status_kesehatan,
        'nik'               => session('auth')->id_user
    ]);
    return $query;
  }

  public static function inbox_xcheck_material_tl($nik){
    $query = DB::SELECT('
    SELECT
      *,
      dt.id as id_dt,
      DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
      DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
      DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
      SUM( i.num ) AS dc_preconn,
      SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
      SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
      SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
      SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
      SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
      0 as conduit,
      dtjl.mode,
      SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
    FROM
     regu r

     LEFT JOIN  dispatch_teknisi dt ON dt.id_regu = r.id_regu
     LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan

     LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
     LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
     LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
     LEFT JOIN item i ON plm.id_item = i.id_item
     LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
     LEFT JOIN mdf m ON dps.sto = m.mdf

     LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
   WHERE
     dps.orderId <> "" AND
     dt.Tgl like "2017-12%" AND
     pl.status_laporan = 1
     GROUP BY dt.id
    ');
    return $query;
  }

  public static function active_team_allx(){
    
    $query = DB::SELECT('
      SELECT
        *,
        d.status as status_kehadiran,
        d.date_created as waktu_absen,
        c.nik as NIK,
        c.status as status_emp,
        e.squad,
        b.title,
        b.ketTer
      FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON (c.nik = d.nik AND date(d.date_created) = "'.date('Y-m-d').'")
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
      WHERE
        b.TL_NIK in (96157604,87155283,93150264,93154588,95150805,90131189,94131190,96170277,89150015,93170061,99170159,93155665) AND
        b.ketTer=1
      GROUP BY c.nik
      ORDER BY d.status ASC,
      b.TL_NIK ASC
    ');

    return $query;
  }

  public static function simpanAbsenBesok($nik, $tgl){
      $check = DB::SELECT('select * from absen a where a.nik = "'.$nik.'" AND date(a.date_created)="'.$tgl.'"');
      $return = array();
      if (count($check)>0){
        $check_data = $check[0];
        $return['status'] = "danger";
        $return['message'] = "Anda sudah absen pada ".$check_data->date_created;
      } else {
        $insert = DB::table('absen')->insert([
                    'nik'           => $nik,
                    'status'        => "HADIR",
                    'date_created'  => $tgl,
                    'tglAbsen'      => date('Y-m-d H:i:s'),
                    'absenEsok'    => '1'
                  ]);

        $return['status']   = "success";
        $return['message']  = "Anda berhasil absen Untuk Besok";
      }
      return $return;
  }

  public static function cekStatusTekProv($nik){
      $query =  DB::SELECT('
                    SELECT
                      a.*,b.*
                    FROM
                      1_2_employee a
                    LEFT JOIN regu b ON (a.nik = b.nik1 OR a.nik = b.nik2)
                      WHERE
                    b.ACTIVE = "1" AND
                    a.nik="'.$nik.'"
                ');
      return $query;
  }

  public static function active_team_all_atasan(){
    
    $query = DB::SELECT('
      SELECT
        *,
        d.status as status_kehadiran,
        d.date_created as waktu_absen,
        c.nik as NIK,
        c.status as status_emp,
        e.squad
      FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON (c.nik = d.nik AND date(d.date_created) = "'.date('Y-m-d').'")
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
      WHERE
        c.atasan_1 = "96158953"
      GROUP BY c.nik
      ORDER BY d.status ASC,
      b.TL_NIK ASC
    ');
    return $query;
  }

  public static function uploadEvent($event)
  {
    return DB::table('log_uploadFile')->leftJoin('data_vaksin','log_uploadFile.created_by','=','data_vaksin.nik_karyawan')->where('log_uploadFile.created_by', session('auth')->id_karyawan)->where('log_uploadFile.keterangan', $event)->first();
  }

}
