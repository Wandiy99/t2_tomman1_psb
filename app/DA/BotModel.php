<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class BotModel
{
    public static function sendBroadcastTommanGO($chatID,$msg)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot1722635250:AAEUQ-cyQi8QqBg9AsaIfyCNXfNUVpZ4hak/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg \r\n\r\nSalam Hormat <b>".session('auth')->nama."</b>").'&parse_mode=HTML',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function sendMessageTommanGO($chatID,$msg)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot1722635250:AAEUQ-cyQi8QqBg9AsaIfyCNXfNUVpZ4hak/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg").'&parse_mode=HTML',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function sendMessageReplyTommanGO($chatID,$msg,$messageID)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot1722635250:AAEUQ-cyQi8QqBg9AsaIfyCNXfNUVpZ4hak/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg").'&parse_mode=HTML&reply_to_message_id='.$messageID.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function sendMessageLaporODP($chatID,$msg)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg").'&parse_mode=HTML',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function sendMessageReplyLaporODP($chatID,$msg,$messageID)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg").'&parse_mode=HTML&reply_to_message_id='.$messageID.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function sendMessageReplyMarkupLaporODP($chatID,$msg,$keyboard)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot5774572588:AAF4JN_QEtSdCLgPchuzX0FMVgmI2dm1A5w/sendmessage?chat_id='.$chatID.'&text='.urlencode("$msg").'&reply_markup='.$keyboard.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public static function logTommanGO($chatID,$messageID,$username,$message)
    {        
        DB::transaction(function() use($chatID,$messageID,$username,$message) {
            DB::table('log_bot_tomman_go')->insert([
                'chat_id'       => $chatID,
                'message_id'    => $messageID,
                'username'      => $username,
                'message'       => $message,
                'datetime'      => date('Y-m-d H:i:s')
              ]);
        });
    }

    public static function sendMessagePerwiraBot($chatID, $msg)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.telegram.org/bot5339848652:AAG_t2uiBUJ0o6TmExFvYGQIvaDLxA3rZTU/sendmessage?chat_id=' . $chatID . '&text=' . urlencode("$msg") . '&parse_mode=HTML',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public static function sendMessageReplyPerwiraBot($chatID, $msg, $messageID)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.telegram.org/bot5339848652:AAG_t2uiBUJ0o6TmExFvYGQIvaDLxA3rZTU/sendmessage?chat_id=' . $chatID . '&text=' . urlencode("$msg") . '&parse_mode=HTML&reply_to_message_id=' . $messageID . '',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}