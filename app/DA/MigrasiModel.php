<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MigrasiModel
{
  public static function search($param){
    $query = DB::SELECT('
      SELECT
        *,
        b.id_regu as id_r,
        b.id as id_dt
      FROM
        dossier_master a
        LEFT JOIN dispatch_teknisi b ON a.ND = b.Ndem
        LEFT JOIN regu c ON b.id_regu = c.id_regu
        LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
      WHERE ND LIKE "%'.$param.'%" or ND_REFERENCE LIKE "%'.$param.'%"' );
    return $query;
  }

  public function saveCluster($req)
  {
      return DB::table('cluster')->insert([
        'nama_cluster' => $req->nmCluster
      ]);
  }

  public function listCluster()
  {
      // return DB::table('cluster')->get();
      $sql = 'select *, sum(clusterMitra.progress) as total from cluster left join clusterMitra on cluster.id_cluster=clusterMitra.clusterId group by cluster.id_cluster';

      return DB::select($sql); 
  }

  public function listProgress()
  {
      return DB::table('cluster')
            ->leftJoin('clusterMitra','cluster.id_cluster','=','clusterMitra.clusterId')
            ->leftJoin('regu','clusterMitra.reguId','=','regu.id_regu')
            ->get();
  }

  public function getRegu()
  {
      return DB::table('regu')->get();
  }

  public function cekRegu($idRegu)
  {
      return DB::table('clusterMitra')->where('reguId','=',$idRegu)->first();
  }

  public function simpanProgress($req)
  {
      return DB::table('clusterMitra')->insert([
          'reguId'    => $req->regu,
          'progress'  => $req->progress,
          'clusterId' => $req->nmCluster
      ]);
  }

  public function getDataByIdProgress($idProgress)
  {
      return DB::table('cluster')
            ->leftJoin('clusterMitra','cluster.id_cluster','=','clusterMitra.clusterId')
            ->leftJoin('regu','clusterMitra.reguId','=','regu.id_regu')
            ->where('clusterMitra.id','=',$idProgress)
            ->first();
  
  }

  public function ubahProgress($req, $id)
  {
      return DB::table('clusterMitra')
              ->where('id','=',$id)
              ->update([
                  'progress' => $req->progress
              ]);
  }
}
