<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class WorkorderModel
{

  public static function list($catstatus,$jenis){
    date_default_timezone_set('Asia/Makassar');
    $auth = session('auth');
    switch ($jenis){
      case "ASR" :
        $query = DB::SELECT('
          SELECT
            *,
            a.id as id_dt,
            d.Customer_Name as Name,
            d.Contact_Name as Contact,
            d.Contact_Phone as Contact_Phone
          FROM
            dispatch_teknisi a
          LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
          LEFT JOIN regu aa ON a.id_regu = aa.id_regu
          LEFT JOIN roc c ON a.Ndem = c.no_tiket
          LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
          WHERE
    				a.tgl LIKE "'.date('Y-m').'%" AND
    				(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
    				(b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL)
          ORDER BY b.modified_at DESC
        ');
      break;
      default :
      break;
    }
    return $query;
  }

  public static function asr_close(){
    date_default_timezone_set('Asia/Makassar');
    $auth = session('auth');
    $query = DB::SELECT('
      SELECT
        count(*) as jumlah
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN regu aa ON a.id_regu = aa.id_regu
      LEFT JOIN roc c ON a.Ndem = c.no_tiket
      LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
      WHERE
        a.tgl LIKE "'.date('Y-m-d').'" AND
        (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
        (b.status_laporan IN (1)) AND
        c.no_tiket is NOT NULL
      ORDER BY b.modified_at DESC
    ');
    return $query;
  }
}

?>
