<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class GendongModel
{
	public static function simpanGendong($req){
      date_default_timezone_set('Asia/Makassar');
      $simpan =  DB::table('Splitter_Gendong')->insertGetId([
          'updated_at'      => date('Y-m-d H:i:S'),
          'user_renew'		=> session('auth')->id_user,
          'catatan_order'   => $req->catatan_order,
          'nama_odp'   		=> $req->nama_odp,
          'koordinat_odp'   => $req->koordinat_odp,
          'splitter'   		=> $req->splitter
      ]);
      return $simpan;
  }
}
?>