<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class DispatchModel
{

  public static function get_sektor(){
      $query = DB::table('group_telegram')
                      ->groupBy('sektor')
                      ->where('sektor','<>','')
                      ->where('sms_active_prov',1)
                      ->orderBy('urut','ASC')
                      ->get();
      return $query;
  }

  public static function getSektorASR(){
    $query = DB::SELECT('
      SELECT
        a.title,
        count(*) as jumlah
      FROM
        group_telegram a
      LEFT JOIN mdf b ON a.sektor = b.sektor_asr
      LEFT JOIN nonatero_excel c ON b.mdf = c.CMDF
      WHERE
        title LIKE "%ASR%"
      GROUP BY a.title
    ');
    return $query;
  }

  public static function lastupdate($idregu){
    return DB::SELECT('
        SELECT
        *
        FROM
          psb_laporan_log a
        LEFT JOIN psb_laporan_status b ON a.status_laporan = b.laporan_status_id
        WHERE
          a.id_regu = "'.$idregu.'" AND
          a.status_laporan <> 6 AND
          DATE(a.created_at) = "'.date('Y-m-d').'"
        ORDER BY a.created_at DESC
        LIMIT 1
      ');
  }

  public static function produktifitasteklist($dateAwal,$date,$id_regu,$status){
    $where_status = '';
    if ($status=="NP"){
      $where_status = ' AND (b.id IS NULL OR c.grup="SISA") AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)")';

    } elseif($status=="UP_AO"){
      $where_status = ' AND c.grup = "UP" AND dps.jenisPsb LIKE "AO%" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)")';

    } elseif($status=="UP_COMPLETED"){
      $where_status = ' AND dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("COMPLETED","Completed (PS)")';

    } elseif($status=="UP_MO"){
      $where_status = ' AND c.grup ="UP" AND dps.jenisPsb LIKE "MO%" ';

    } elseif($status=="UP_PDA"){
      $where_status = ' AND c.grup = "UP" AND jl.KAT = "PDA" ';

    } elseif($status=="ORDER"){
    $where_status = '';

    } elseif ($status<>"ALL"){
      $where_status = ' AND c.grup="'.$status.'" AND jl.KAT IN ("AO","MO","PDA") AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)")';
    }
    
    if ($id_regu=="ALL"){
      $where_regu = '';
    }else{
      $where_regu = ' AND a.id_regu = "'.$id_regu.'"';
    }
    
    $query = DB::SELECT('
      SELECT
      *,
      e.title as sektorx,
      dps.orderDate as dps_orderDate,
      dps.orderDatePs as dps_orderDatePs,
      pmw.orderDate as pmw_orderDate
      FROM
        dispatch_teknisi a
        LEFT JOIN Data_Pelanggan_Starclick dps ON a.NO_ORDER = dps.orderIdInteger
        LEFT JOIN psb_myir_wo pmw ON a.Ndem = pmw.myir
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        WHERE
        (DATE(a.tgl) BETWEEN "'.$dateAwal.'" AND "'.$date.'") AND
        a.jenis_order IN ("SC","MYIR") AND
        dps.orderStatus NOT IN (" FOLLOW UP BOOKED","REVOKE COMPLETED") AND (c.laporan_status != "PERMINTAAN REVOKE" OR c.laporan_status IS NULL)
          '.$where_regu.'
          '.$where_status.'
      ');
    return $query;
  }

  public static function produktifitastek4($date,$area,$mitra){
    $where_area = "";
    if ($area <> "ALL") {
      $where_area = ' AND e.ket_sektor = '.$area;
    }
    $query = DB::SELECT('
        SELECT
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN c.grup = "UP" AND jl.KAT = "AO" THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN c.grup = "UP" AND jl.KAT = "MO" THEN 1 ELSE 0 END) as UP_MO,
        SUM(CASE WHEN c.grup = "UP" AND jl.KAT = "PDA" THEN 1 ELSE 0 END) as UP_PDA,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM
          dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        WHERE
          jl.KAT IN ("AO","MO","PDA") AND
          a.tgl LIKE "'.$date.'%" AND
          a.jenis_layanan <> "QC"
          '.$where_area.'
        GROUP BY
          e.title
        ORDER BY UP DESC
      ');
    return $query;
  }

  public static function produktifitastek3($dateAwal,$date,$area,$sektor)
  {
    switch ($area) {
      case 'ALL':
        $where_area = '';
        break;
      
      default:
        $where_area = ' AND e.ket_sektor = '.$area.'';
        break;
    }

    switch ($sektor) {
      case 'ALL':
        $where_sektor = '';
        break;
      
      default:
        $where_sektor = ' AND e.chat_id = '.$sektor;
        break;
    }

    $query = DB::SELECT('
        SELECT
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN c.grup = "UP" AND dps.jenisPsb LIKE "AO%" AND dps.orderStatus NOT IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN dps.jenisPsb LIKE "AO%" AND dps.orderStatus IN ("COMPLETED","Completed (PS)") THEN 1 ELSE 0 END) as UP_COMPLETED,
        SUM(CASE WHEN c.grup = "UP" AND dps.jenisPsb LIKE "MO%" THEN 1 ELSE 0 END) as UP_MO,
        SUM(CASE WHEN c.grup = "UP" AND jl.KAT = "PDA" THEN 1 ELSE 0 END) as UP_PDA,
        e.title,
        e.sektor,
        a.id_regu
        FROM
          dispatch_teknisi a
        LEFT JOIN Data_Pelanggan_Starclick dps ON a.NO_ORDER = dps.orderIdInteger
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        WHERE
          (DATE(a.tgl) BETWEEN "'.$dateAwal.'" AND "'.$date.'") AND
          a.jenis_order IN ("SC","MYIR") AND
          dps.orderStatus NOT IN (" FOLLOW UP BOOKED","REVOKE COMPLETED") AND (c.laporan_status != "PERMINTAAN REVOKE" OR c.laporan_status IS NULL)
          AND e.ket_sektor = 1
          '.$where_sektor.'
        GROUP BY
          d.id_regu
        ORDER BY jumlah DESC
      ');
    return $query;
  }

  public static function closingAverage($sektor, $provider)
  {
    switch ($sektor) {
      case 'ALL':
        $sektorx = '';
        break;
      
      default:
        $sektorx = ' AND gt.chat_id = '.$sektor;
        break;
    }

    switch ($provider) {
      case 'DCS':
        $typex = 'AND dps.provider LIKE "DCS%"';
        break;
      
      case 'NON_DCS':
        $typex = 'AND dps.provider NOT LIKE "DCS%"';
        break;
      
      default:
        $typex = '';
        break;
    }

    $days_for = '';
    for ($i = 1; $i < date('t') + 1; $i ++)
    {
      if ($i < 10)
      {
        $keys = '0'.$i;
      } else {
        $keys = $i;
      }

      $day = date('Y-m-').''.$keys;

      $days_for .= ',SUM(CASE WHEN (DATE(dps.orderDatePs) = "'.$day.'") THEN 1 ELSE 0 END) as ps_d'.$keys;
    }
    return DB::select('
      SELECT
          r.uraian as tim,
          gt.title as sektor,
          ma.mitra_amija_pt as mitra
          '.$days_for.'
          ,SUM(CASE WHEN DATE(dps.orderDatePs) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'" THEN 1 ELSE 0 END) as total_ps,
          DATEDIFF("'.date('Y-m-d').'", "'.date('Y-m-01').'") as total_days
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
      WHERE
          dt.jenis_order IN ("SC", "MYIR") AND
          dps.orderStatusId = 1500 AND
          dps.jenisPsb LIKE "AO%" AND
          (DATE(dps.orderDatePS) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")
          '.$sektorx.'
          '.$typex.'
      GROUP BY r.uraian
      ORDER BY total_ps DESC
    ');
  }

  public static function produktifitastekA($date,$mode,$mitra){

    $where_mode = '';
    $where_mode_having = '';
    if ($mode=="INNER"){
      $where_mode = 'AND e.ket_sektor=1';
    }
    if ($mode=="OUTER"){
      $where_mode = 'AND e.ket_sektor=0';
    }
    if ($mode=="UP0"){
      $where_mode_having = 'HAVING UP = 0';
    }
    $query = DB::SELECT('
        SELECT
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN b.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM
          dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        WHERE
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND
          (f.Incident <> "")))
          '.$where_mode.'
        GROUP BY
          d.id_regu
          '.$where_mode_having.'
        ORDER BY UP DESC
      ');
    return $query;
  }

  public static function produktifitassektor($date,$mode){
    $where_mode = '';
    $where_mode_having = '';
    if ($mode=="INNER"){
      $where_mode = 'AND e.ket_sektor=1';
    }
    if ($mode=="OUTER"){
      $where_mode = 'AND e.ket_sektor=0';
    }
    if ($mode=="UP0"){
      $where_mode_having = 'HAVING UP = 0';
    }

    $query = DB::SELECT('
        SELECT
        d.uraian,
        count(*) as jumlah,
        SUM(CASE WHEN (c.grup = "SISA" OR b.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN c.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN c.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN c.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN c.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN b.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM
          dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        WHERE
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND
          (f.Incident <> ""))) AND
          e.title <> "EMPTY GROUP"
          '.$where_mode.'
        GROUP BY
          d.mainsector
          '.$where_mode_having.'
        ORDER BY UP DESC
      ');
    return $query;
  }

  public static function produktifitassektorlist($date,$sektor,$mode){
    $where_sektor = '';
    $where_mode = '';
    if ($sektor<>"ALL"){
      $where_sektor = 'AND e.title = "'.$sektor.'"';
    }
    if ($mode=="NP"){
      $where_mode = 'AND (c.grup = "SISA" OR b.id IS NULL)';
    } else if ($mode<>"ALL"){
      $where_mode = 'AND c.grup = "'.$mode.'"';
    }
    $query = DB::SELECT('
        SELECT
          *,
          h.ONU_Link_Status as ukur_before,
          a.id as id_dt
        FROM
          dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON a.Ndem = f.Incident
        LEFT JOIN psb_laporan_action g ON b.action = g.laporan_action_id
        LEFT JOIN ibooster h ON a.Ndem = h.order_id
        LEFT JOIN psb_laporan_penyebab i ON b.penyebabId = i.idPenyebab
        WHERE
          ((f.Incident <> "" AND b.status_laporan = 4 AND a.tgl LIKE "2019%") OR (
          a.tgl LIKE "'.$date.'%" AND
          (f.Incident <> ""))) AND
          e.title <> "EMPTY GROUP"
          '.$where_mode.'
          '.$where_sektor.'

        ORDER BY f.Reported_Date DESC
      ');
    return $query;
  }

  public static function produktifitastek2($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND b.mitra = '".$area."'";
    }
    $query = DB::SELECT('
      SELECT
       emp.nik,
       emp.nama,
       count(b.id_regu) as jumlah_regu
      FROM
        1_2_employee emp
      LEFT JOIN regu b ON (emp.nik = b.NIK1) OR (emp.nik = b.NIK2)
      LEFT JOIN dispatch_teknisi a ON b.id_regu = a.id_regu
      WHERE
        1
        '.$get_where_area.'
      GROUP BY emp.nik
    ');
    return $query;
  }
  public static function produktifitastek($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else if ($area == "DELTA"){
      $get_where_area = 'AND rm.klasifikasi = "DELTA"';
    } else {
      $get_where_area = "AND b.mitra = '".$area."'";
    }
    $query = DB::SELECT('
    SELECT *,(prov_UP+gangguan_UP_Copper+gangguan_UP_GPON+asis_UP+gangguan_UP_DES+addon_UP)/(target_MH*jumlah_regu) as rank FROM(
      SELECT
        count(*) as jumlah,
        g.TL,
        g.sektor,
        SUM(CASE WHEN a.dispatch_by IN (2) THEN 1 ELSE 0 END) as jlhggn,
        SUM(CASE WHEN a.dispatch_by is null OR a.dispatch_by NOT IN(3,2,1) THEN 1 ELSE 0 END) as jlhpsb,
        SUM(CASE WHEN d.status_laporan IN (5,6,4) THEN 1 ELSE 0 END) as PROGRESS,
        SUM(CASE WHEN d.status_laporan NOT IN (5,6,1,4) THEN 1 ELSE 0 END) as KENDALA,
        SUM(CASE WHEN c.no_tiket<>"" THEN 1 ELSE 0 END) as gangguan,
        SUM(CASE WHEN e.orderId<>"" THEN 1 ELSE 0 END) as prov,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%" )) THEN 2.4 ELSE 0 END) as gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 4.8 ELSE 0 END) as gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 5.3 ELSE 0 END) as asis_UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 1 ELSE 0 END) as jumlah_asis_UP,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_MH,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_MH,
        SUM(CASE WHEN (dm.ND<>"") THEN 5.3 ELSE 0 END) as asis_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 ) THEN 4.8 ELSE 0 END) as gangguan_DES_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_Copper_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 2.4 ELSE 0 END) as gangguan_GPON_MH,
        b.id_regu,
        b.uraian as uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        COUNT(DISTINCT(a.id_regu)) as jumlah_regu,
        a.updated_at,
        b.star,
        b.mitra
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram g ON b.mainsector = g.chat_id
      LEFT JOIN roc c ON a.Ndem = c.no_tiket
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN dossier_master dm ON a.Ndem = dm.ND
      LEFT JOIN regu_mitra rm ON b.mitra = rm.mitra
      WHERE
        ((DATE(d.modified_at) Like "%'.$date.'%" OR DATE(d.created_at) Like "%'.$date.'%") and
        a.tgl LIKE "%'.$date.'%") and
        f.title <> "" AND
        f.title NOT LIKE "%CCAN%" AND
        f.sektor like "%prov%" and
        f.sektor <> "PROVSSC1"
        '.$get_where_area.'
      GROUP BY b.id_regu
    ) x ORDER BY rank DESC
    ');
    return $query;
         // DATE(a.tgl) like "%'.$date.'%" AND
        // ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") and
  }

  public static function group_telegram($area){
    if ($area=="ALL"){
      $sql = 'SELECT * from group_telegram where sms_active_prov = 1 order by urut asc';
    }
    else if ($area=="INNER"){
      $get_where = " WHERE a.ket_sektor=1  AND a.sms_active_prov = 1";
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="OUTER"){
      $get_where = " WHERE a.ket_sektor=0";
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="PROV"){
      $get_where = ' WHERE a.ket_sektor in (0,1) AND b.sttsWo=1  AND a.sms_active_prov = 1';
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else if ($area=="ASR"){
      $get_where = ' WHERE a.ket_sektor in (0,1) AND b.sttsWo=0';
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    else {
      $get_where = ' WHERE sektor LIKE "%'.$area.'%"';
      $sql = 'SELECT a.* FROM group_telegram a LEFT JOIN regu b ON a.chat_id=b.mainsector '.$get_where. ' group by a.title order by a.urut asc';
    }
    return DB::SELECT($sql);
    // return DB::table('group_telegram')->where('type',1)->get();
  }
  public static function listWO($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        c.orderDate,
        a.dispatch_by,
        a.jenis_layanan,
        e.grup,
        g.qcondesk_order,
        i.ORDER_DATE,
        c.orderDatePs,
        c.orderStatus
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIN qcondesk g ON a.id = g.qcondesk_order
      LEFT JOIN psb_myir_wo h ON a.Ndem = h.myir OR a.Ndem = h.sc
      LEFT JOIN Data_Pelanggan_Starclick_Backend i ON SUBSTR(i.ORDER_CODE,6,30) = h.myir
      WHERE
          ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'") OR ((b.status_laporan IN (5,6,7,31,4) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y-m').'%")) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5)) AND a.id_regu = "'.$id_regu.'"
      ORDER BY i.ORDER_DATE
      ');
    return $query;
  }

  public static function getHdMatrix($date){
    return DB::select('SELECT b.nama,a.Ndem,c.status_laporan,a.updated_by FROM dispatch_teknisi a
      LEFT JOIN 1_2_employee b ON a.updated_by = b.nik
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      WHERE a.tgl LIKE "%'.$date.'%" AND dispatch_by IS NULL ORDER BY a.updated_by asc');
  }
  public static function getHdFalloutMatrix($date){
    return DB::select('SELECT a.workerName,a.orderId,c.orderStatus,a.workerId,a.ket FROM dispatch_hd a
      LEFT JOIN Data_Pelanggan_Starclick c ON a.orderId = c.orderId
      WHERE a.updated_at LIKE "%'.$date.'%" ORDER BY a.workerId asc');
  }
  public static function getTeamMatrix1($date,$area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND f.sektor LIKE '%".$area."%'";
    }
    if (strlen($date)<8) {
      if (date('Y-m')==$date) {
        if (date('d')<=22){
          $konstanta = date('d');
        } else {
          $konstanta = 22;
        }
      } else {
        $konstanta = 22;
      }
    } else {
      $konstanta = 1;
    }
    $query = DB::SELECT('
    SELECT *,(prov_UP+gangguan_UP_Copper+gangguan_UP_GPON+asis_UP+gangguan_UP_DES+addon_UP)/(target_MH*jumlah_regu*'.$konstanta.') as rank FROM(
      SELECT
        count(*) as jumlah,
        g.TL,
        SUM(CASE WHEN a.dispatch_by IN (2) THEN 1 ELSE 0 END) as jlhggn,
        SUM(CASE WHEN a.dispatch_by is null OR a.dispatch_by NOT IN(3,2,1) THEN 1 ELSE 0 END) as jlhpsb,
        SUM(CASE WHEN d.status_laporan IN (5,6,4) THEN 1 ELSE 0 END) as PROGRESS,
        SUM(CASE WHEN d.status_laporan NOT IN (5,6,1,4) THEN 1 ELSE 0 END) as KENDALA,
        SUM(CASE WHEN c.no_tiket<>"" THEN 1 ELSE 0 END) as gangguan,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 1 ELSE 0 END) as prov,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%" )) THEN 2.4 ELSE 0 END) as gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 4.8 ELSE 0 END) as gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 5.3 ELSE 0 END) as asis_UP,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_Copper,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2 AND d.status_laporan = 1 AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_GPON,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 AND d.status_laporan = 1 ) THEN 1 ELSE 0 END) as jumlah_gangguan_UP_DES,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_prov_UP,
        SUM(CASE WHEN (e.orderId<>"" AND d.status_laporan = 1 AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END) as jumlah_addon_UP,
        SUM(CASE WHEN (dm.ND<>"" AND d.status_laporan = 1) THEN 1 ELSE 0 END) as jumlah_asis_UP,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%")) THEN 5.3 ELSE 0 END) as prov_MH,
        SUM(CASE WHEN (e.orderId<>"" AND (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%")) THEN 3.3 ELSE 0 END) as addon_MH,
        SUM(CASE WHEN (dm.ND<>"") THEN 5.3 ELSE 0 END) as asis_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta = 2 ) THEN 4.8 ELSE 0 END) as gangguan_DES_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%COPPE%" OR c.alpro LIKE "%COOPPE%" OR c.alpro LIKE "%MSAN%")) THEN 1.2 ELSE 0 END) as gangguan_Copper_MH,
        SUM(CASE WHEN (c.no_tiket<>"" AND c.loker_ta <> 2  AND (c.alpro LIKE "%GPO%" OR c.alpro LIKE "%FIBER%")) THEN 2.4 ELSE 0 END) as gangguan_GPON_MH,
        b.id_regu,
        b.nik1,
        b.nik2,
        f.title as uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        COUNT(DISTINCT(a.id_regu)) as jumlah_regu,
        a.updated_at,
        (SELECT abs1.approval from absen abs1 where abs1.nik = b.nik1 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik1_absen,
        (SELECT abs1.approval from absen abs1 where abs1.nik = b.nik2 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik2_absen
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu

      LEFT JOIN group_telegram g ON b.mainsector = g.chat_id
      LEFT JOIN roc c ON a.Ndem = c.no_tiket
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN dossier_master dm ON a.Ndem = dm.ND

      WHERE
        f.title NOT LIKE "%CCAN%" AND
        DATE(a.tgl) like "%'.$date.'%" AND
        b.mitra IN ("TA","UPATEK","CUI") AND
        f.title <> ""
        '.$get_where_area.'
      GROUP BY f.title
    ) x ORDER BY rank DESC
    ');
    return $query;
  }
  public static function getTeamMatrix($date,$area){
    if ($area=="ALL") {
      $get_where_area = '';
    }
    else if ($area=="INNER"){
        $get_where_area = 'AND b.sttsWo=1 AND f.ket_sektor="1"';
    }
    else if ($area=="OUTER"){
       $get_where_area = 'AND b.sttsWo=1 AND f.ket_sektor="0"';
    }
    else if ($area=="PROV"){
      $get_where_area = ' AND b.sttsWo=1 AND f.ket_sektor in (0,1)';
    }

    else {
      $get_where_area = "AND f.sektor='".$area."' AND b.sttsWo=1";
    }

    $query = DB::SELECT('
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      LEFT JOIN myirSCTgl my ON a.Ndem=my.myir
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5))
        '.$get_where_area.'
      GROUP BY a.id_regu
    ');

    return $query;
  }

  public static function get_sektor_matrix_team($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND c.chat_id = "'.$mainsector.'" AND (a.tgl="'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_order($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      a.jenis_order as dt_jenis_order,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS,
      e.Incident_Symptom as e_is,
      e.Summary as e_summary,
      e.Last_Work_Log_Date as e_ttr,
      e.Reported_Date as e_reported_date,
      e.Reported_Datex as e_reported_datex,
      e.Customer_Type as e_customer_type,
      e.Contact_Name as e_contact_name,
      dn1.Assigned_by as statusManja,
      dn1.Booking_Date as jam_manja,
      roc.customer_assign as roc_customer_assign,
      roc.ca_manja_time
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN data_nossa_1_log e ON a.NO_ORDER = e.ID
      LEFT JOIN data_nossa_1 dn1 ON e.ID = dn1.ID
      LEFT JOIN roc ON a.Ndem = roc.no_tiket
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND b.id_regu = "'.$id_regu.'" AND (a.tgl="'.$date.'" OR DATE(c.modified_at) = "'.$date.'" OR (c.status_laporan = 1 AND DATE(c.modified_at) = "'.$date.'") OR
      ((d.grup IN ("OGP","KP","KT","SISA ") OR c.id IS NULL) AND YEAR(a.tgl)="'.date('Y').'"))
      ORDER BY
      e.Reported_Date
    ');
  }

  public static function getArea($area){
    if ($area=="ALL") {
      $get_where_area = "";
    } else {
      $get_where_area = "AND a.area = '".$area."'";
    }
    $get_area = DB::SELECT('
      SELECT
        a.area,
        b.DESKRIPSI
      FROM
        mdf a
      LEFT JOIN area b ON a.area = b.area
      WHERE
        b.DESKRIPSI is not NULL
        '.$get_where_area.'
      GROUP BY a.area
    ');
    return $get_area;
  }
  public static function listTimDispatch($date){
    $listTim = DB::SELECT('
      SELECT
        a.id_regu,
        b.uraian,
        d.area,
        count(*) as jumlah_wo,
        SUM(CASE WHEN a.manja_status = 1 THEN 1 ELSE 0 END) as jumlah_manja,
        SUM(CASE WHEN a.manja_status = 2 THEN 1 ELSE 0 END) as jumlah_manja_nok
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN mdf d ON c.sto = d.mdf
      WHERE
        a.tgl LIKE "'.$date.'%"
      GROUP BY b.id_regu
    ');
    return $listTim;
  }
  public static function timeslot(){
    $query = DB::table('dispatch_teknisi_timeslot')->get();
    return $query;
  }
  public static function dispatch_timeslot($date){
    $dispatch_timeslot = DB::SELECT('
      SELECT
        a.*,
        b.*,
        h.*,
        c.status_laporan,
        c.catatan,
        c.modified_at,
        c.created_at,
        d.uraian,
        d.id_regu,
        e.orderName,
        g.laporan_status,
        c.id_tbl_mj,
        f.area
      FROM
        dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_timeslot b ON a.updated_at_timeslot = b.dispatch_teknisi_timeslot_id
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN regu d ON a.id_regu = d.id_regu
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN roc h ON a.Ndem = h.no_tiket
      LEFT JOIN mdf f ON e.sto = f.mdf
      LEFT JOIN psb_laporan_status g ON c.status_laporan = g.laporan_status_id
      WHERE
        a.tgl LIKE "'.$date.'%"
      ORDER BY d.id_regu,f.area
    ');
    return $dispatch_timeslot;
  }
  public static function WorkOrderAsr($id)
  {
    $check_data = DB::table('dispatch_teknisi')->where('Ndem', $id)->first();

    if ($check_data != NULL) {
      $get = DB::SELECT('
      SELECT
        a.*,
        c.uraian,
        c.id_regu as id_r,
        b.id as id_dt,
        b.dispatch_by,
        b.tgl as tgl_dt,
        b.jenis_order as b_jenis_order,
        b.created_at as tgl_order_hd,
        a.Incident as no_tiket,
        a.Service_ID as no_telp,
        a.Summary as no_speedy,
        pls.laporan_status as status,
        pl.modified_at as tgl_laporan,
        pl.catatan as catatan_tek,
        pl.noPelangganAktif,
        pl.kordinat_pelanggan,
        pl.nama_odp,
        pl.kordinat_odp,
        pl.valins_id,
        pl.dropcore_label_code,
        pl.odp_label_code,
        pla.action as pla_action,
        plp.penyebab as plp_penyebab,
        ib.ONU_Link_Status,
        ib.ONU_Rx,
        b.Ndem,
        r.no_tiket as tiket_no,
        r.headline as r_headline,
        r.no_speedy as r_no_speedy,
        gt.title as sektor,
        ma.mitra_amija_pt,
        emp.nama as nama_hd
      FROM
      dispatch_teknisi b
      LEFT JOIN data_nossa_1_log a ON b.NO_ORDER = a.ID
      LEFT JOIN roc r ON b.Ndem = r.no_tiket
      LEFT JOIN psb_laporan pl ON b.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN ibooster ib ON a.Incident = ib.order_id
      LEFT JOIN group_telegram gt ON c.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON c.mitra = ma.mitra_amija
      LEFT JOIN `1_2_employee` emp ON b.updated_by = emp.nik
      WHERE
      b.Ndem = "'.$id.'" AND b.jenis_order IN ("IN","INT")
    ');
    } else {
      $get = DB::SELECT('
      SELECT
        a.*,
        c.uraian,
        c.id_regu as id_r,
        b.id as id_dt,
        b.tgl as tgl_dt,
        b.created_at as tgl_order_hd,
        b.jenis_order as b_jenis_order,
        b.dispatch_by,
        a.Incident as no_tiket,
        a.Service_ID as no_telp,
        a.Summary as no_speedy,
        pls.laporan_status as status,
        pl.modified_at as tgl_laporan,
        pl.catatan as catatan_tek,
        pl.noPelangganAktif,
        pl.kordinat_pelanggan,
        pl.nama_odp,
        pl.kordinat_odp,
        pl.valins_id,
        pl.dropcore_label_code,
        pl.odp_label_code,
        pla.action as pla_action,
        plp.penyebab as plp_penyebab,
        ib.ONU_Link_Status,
        ib.ONU_Rx,
        b.Ndem,
        r.no_tiket as tiket_no,
        r.headline as r_headline,
        r.no_speedy as r_no_speedy,
        gt.title as sektor,
        ma.mitra_amija_pt,
        emp.nama as nama_hd
      FROM
      data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
      LEFT JOIN roc r ON b.Ndem = r.no_tiket
      LEFT JOIN psb_laporan pl ON b.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN ibooster ib ON a.Incident = ib.order_id
      LEFT JOIN group_telegram gt ON c.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON c.mitra = ma.mitra_amija
      LEFT JOIN `1_2_employee` emp ON b.updated_by = emp.nik
      WHERE
      a.Incident = "'.$id.'" OR (r.no_speedy LIKE "%'.$id.'%" OR a.Service_No LIKE "%'.$id.'%")
    ');
    }
    return $get;
  }
  public static function CountWO($id){
    $Where  = DispatchModel::GetWhere($id,NULL);
    $result = DispatchModel::QueryCount($Where);
    return $result;
 }
 public static function QueryCount($Where){
   $SQL = '
     SELECT
       count(*) as jumlah
     FROM
       Data_Pelanggan_Starclick a
     LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
     LEFT JOIN dispatch_teknisi c ON b.Ndem=c.Ndem OR a.orderId = c.Ndem
     LEFT JOIN psb_laporan d ON c.id=d.id_tbl_mj
     LEFT JOIN ms2n_sebab e ON b.Sebab = e.ms2n_sebab
     LEFT JOIN ms2n_status_hd f ON b.Ndem=f.Ndem
     LEFT JOIN regu g ON c.id_regu = g.id_regu
  	 LEFT JOIN odp_ftp h ON a.alproname = h.INDEX_NAME
  	 LEFT JOIN olt i ON h.OLT_IP = i.OLT_IP
     LEFT JOIN dshr j ON a.orderId = j.kode_sc
     '.$Where.'
   ';
   $query = DB::select($SQL);
   return $query;
 }

  public static function WorkOrder($id,$search){
    $Where  = DispatchModel::GetWhere($id,$search);
    $result = DispatchModel::QueryBuilder($Where);

    return $result;
  }

  public static function GetWhere($id,$search){
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id = strtoupper($id);
    switch ($id) {
      case 'SURVEY_OK' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            d.status_laporan = "8"
        ';
      break;
      case 'ALL':
        $Where = '
          WHERE
            a.sto NOT IN ("TML") AND
            a.orderStatus NOT IN ("Cancel Order","Process OSS (Cancel)") AND
           (a.orderDate BETWEEN "'.date("Y-m-d",strtotime("first day of previous month")).'" AND "'.date("Y-m-d",$tomorrow).'")
        ';
        break;
      case 'READY':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji","",NULL) AND
            (b.Status <> "PS" OR b.Status is NULL) AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )

          ';
        break;

      case 'UNDISPATCH_MS2N' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            c.id_regu is NULL
        ';
        break;
      case 'NAL' :
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Status <> "PS" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;

      case 'UNDISPATCH_NONMS2N' :
        $Where = '
          WHERE
            a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
            a.orderStatus = "Process OSS (Provision Issued)" AND
            b.Status is NULL
        ';
        break;
        case 'NONNAL' :
          $Where = '
            WHERE
              a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
              a.orderStatus = "Process OSS (Provision Issued)" AND
              b.Status is NULL AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'NONNAL_CLUSTER' :
            $Where = '
              WHERE
                a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
                a.orderStatus = "Process OSS (Provision Issued)" AND
                b.Status is NULL AND
                j.flagging <> "" AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
            break;
            case 'NONNAL_NONCLUSTER' :
              $Where = '
                WHERE
                  a.jenisPsb NOT IN ("", "MO|", "MO|BUNDLING", "MO|INTERNET", "MO|INTERNET + VOICE", "MO|IPTV") AND
                  a.orderStatus = "Process OSS (Provision Issued)" AND
                  b.Status is NULL AND
                  (j.flagging is NULL OR j.flagging = "") AND
                  (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
              ';
              break;


      case 'CFC':
        $Where = '
          WHERE
            a.kcontact LIKE ("%CFC%") AND
            a.orderStatus NOT IN ("Completed (PS)") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'MBSP':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Deskripsi LIKE "%MBSP%" AND
            a.jenisPsb <> "MIGRATE" AND
            b.Status <> "PS" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;
        case 'MBSP_CLUSTER':
          $Where = '
            WHERE
              a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
              b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
              b.Deskripsi LIKE "%MBSP%" AND
              b.Status <> "PS" AND
              a.jenisPsb <> "MIGRATE" AND
              j.flagging is not NULL AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'MBSP_NONCLUSTER':
            $Where = '
              WHERE
                a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
                b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
                b.Deskripsi LIKE "%MBSP%" AND
                b.Status <> "PS" AND
                j.flagging is NULL AND
                a.jenisPsb <> "MIGRATE" AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
          break;

      case 'REGULER':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
            b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
            b.Status <> "PS" AND
            b.Deskripsi NOT LIKE "%MBSP%" AND
            (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
        ';
        break;
        case 'REGULER_CLUSTER':
          $Where = '
            WHERE
              a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
              b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
              b.Deskripsi NOT LIKE "%MBSP%" AND
              j.flagging is not NULL AND
              b.Status <> "PS" AND
              (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
          ';
          break;
          case 'REGULER_NONCLUSTER':
            $Where = '
              WHERE
                a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") AND
                b.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND
                b.Deskripsi NOT LIKE "%MBSP%" AND
                b.Status <> "PS" AND
                j.flagging is NULL AND
                (c.id_regu is NULL OR date(c.updated_at) < "'.date("Y-m-d").'" )
            ';
            break;

      case 'FALLOUTWFM':
        $Where = '
          WHERE
            a.orderStatus IN ("Fallout (WFM)") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'STB':
        $Where = '
          WHERE
            a.Kcontact LIKE "%STB%" AND
            a.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM) ","Fallout (Activation)","Fallout (Data)")
        ';
        break;
      case 'PITOFAILSWA':
        $Where = '
          WHERE
            a.orderStatus IN ("Process OSS (Provision Issued)") AND
            b.Sebab NOT IN ("Belum Ada Janji","Sudah Ada Janji","Follow Up Permintaan","Update","PORT Habis") AND
            b.Status NOT IN ("PS") AND
            a.sto NOT IN ("TML")
        ';
        break;
      case 'FALLOUTACTIVATION':
        $Where = '
          WHERE
            (dps.orderStatus = "Fallout (Activation)" OR a.orderStatus = "Fallout (Activation)")
        ';
        break;
      case 'UPTOPS':
        $Where = '
          WHERE
            a.orderStatus NOT IN ("Completed (PS)") AND
            b.Status <> "PS" AND
            d.status_laporan = 1
        ';
        break;
      case 'MAINTENANCE' :
        $Where = '
          WHERE
            (b.Ket_Sebab LIKE "%ODP LOS%" OR b.Ket_Sebab LIKE "%redaman%" OR b.Sebab = "Follow Up Permintaan") AND
            b.Status NOT IN ("PS")
        ';
        break;
      case 'SEARCH' :
        $Where = '
          WHERE
            (c.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM") OR c.jenis_order IS NULL) AND a.orderIdInteger <> 0 AND
            a.orderIdInteger = "'.$search.'"  OR
            a.internet = "'.$search.'" OR
            a.noTelp = "'.$search.'"
        ';
        break;
      default:
        $Where = '
          WHERE
          a.orderId = 0
        ';
        break;
    }
    return $Where;
  }

   private static function QueryBuilder($id)
   {
     return DB::select('
     	SELECT
   			c.id as id_dt,
        c.created_at as tgl_awal_dispatch,
        c.updated_by as hd,
        c.updated_at as hd_date,
        c.tgl as c_tgl,
        c.jenis_layanan as c_jenis_layanan,
       	a.*,
 				a.orderId as status_wo_by_hd,
        a.orderDate as a_orderDate,
        a.orderDatePs as a_orderDatePs,
        a.provider as a_provider,
     		c.id_regu AS id_r,
     		g.uraian,
        gt.title as sektor_title,
     		d.status_laporan,
     		d.catatan,
        d.modified_at as pl_modif,
        d.id as id_pl,
        d.valins_id,
        d.dropcore_label_code,
        d.odp_label_code,
        d.noPelangganAktif,
        d.detek,
        d.nama_odp as odp_nama,
        d.kordinat_pelanggan,
        d.kordinat_odp,
        d.typeont,
        d.snont,
        d.typestb,
        d.snstb,
        d.material_psb,
        d.rfc_number,
        d.dalapa_checklist,
        d.redaman_iboster as d_redaman_iboster,
 				c.id as jumlah_material,
        c.dispatch_by,
        c.kordinatPel,
        c.alamatSales,
        pls.laporan_status_id,
        pls.laporan_status,
        pls.status_comparin,
        pls.log_action_comparin,
        pls.log_uic_comparin,
        mc.nama_order as mc_nama_order,
        mc.modified_at as mc_modif_at,
        mc.modified_by as mc_modif_by,
        mc.dispatch_regu_name,
        mc.status,
        mc.headline as mc_headline,
        mc.action as mc_action,
        mc.port_used as mc_port_used,
        mc.port_idle as mc_port_idle,
        mc.status_name,
        mc.tgl_selesai,
        ma.mitra_amija_pt,
        SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
        SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-70-NonAcc" THEN plm.qty ELSE 0 END) as precon70,
        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-75-NonAcc" THEN plm.qty ELSE 0 END) as precon75,
        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-80" THEN plm.qty ELSE 0 END) as precon80,
        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-100-NonAcc" THEN plm.qty ELSE 0 END) as precon100,
        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-150-NonAcc" THEN plm.qty ELSE 0 END) as preconnonacc,
        kprot.JENIS_PSB as kprot_jenis_psb,
        kprot.TYPE_TRANSAKSI as kprot_type_transaksi,
        kprot.TYPE_LAYANAN as kprot_type_layanan,
        kprot.PROVIDER as kprot_provider,
        kprot.STATUS_RESUME as kprot_orderStatus,
        kprot.EXTERN_ORDER_ID as kprot_orderStatusId,
        kprot.ORDER_DATE as kprot_orderDate,
        kprot.LAST_UPDATED_DATE as kprot_orderDatePs,
        utt.order_code as utt_order_code,
        utt.statusName as utt_statusName,
        utt.qcStatusName as utt_qcStatusName,
        utt.tglWo as utt_tglWo,
        utt.tglTrx as utt_tglTrx,
        utt.details as utt_details,
        utt.approver as utt_approver,
        utt.getFlowLatest as utt_getFlowLatest,
        utt.last_updated_at as utt_last_updated_at,
        ttp.ps_date as ttp_ps_date,
        ttp.status_wo as ttp_status_wo,
        ttp.layanan as ttp_layanan,
        ttp.id_sto as ttp_id_sto,
        FORMAT(6371 * acos(cos(radians(d.gps_latitude)) * cos(radians(a.lat)) * cos(radians(a.lon) - radians(d.gps_longitude)) + sin(radians(d.gps_latitude)) * sin(radians(a.lat))),3) as jarak_deviasi,
        SUBSTRING_INDEX(d.kordinat_odp, ",", 1) AS lat_odp,
        SUBSTRING_INDEX(SUBSTRING_INDEX(d.kordinat_odp, ",", -1), ",", 1) AS lon_odp,
        FORMAT(6371 * acos(cos(radians(d.gps_latitude)) * cos(radians(SUBSTRING_INDEX(d.kordinat_odp, ",", 1))) * cos(radians(SUBSTRING_INDEX(SUBSTRING_INDEX(d.kordinat_odp, ",", -1), ",", 1)) - radians(d.gps_longitude)) + sin(radians(d.gps_latitude)) * sin(radians(SUBSTRING_INDEX(d.kordinat_odp, ",", 1)))),3) as deviasi_pelanggan_odp,
        dtjl.jenis_transaksi as dtjl_jenis_transaksi
     	FROM
     	Data_Pelanggan_Starclick a
     	LEFT JOIN dispatch_teknisi c ON a.orderIdInteger = c.NO_ORDER
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON c.jenis_layanan_id = dtjl.jenis_layanan_id
      LEFT JOIN utonline_tr6 utt ON a.internet = utt.noInternet AND utt.witel IN ("KALSEL (BANJARMASIN)", "BANJARMASIN")
      LEFT JOIN tacticalpro_tr6 ttp ON a.orderIdInteger = ttp.sc_id
      LEFT JOIN kpro_tr6 kprot ON a.orderIdInteger = kprot.ORDER_ID
      LEFT JOIN maintaince mc ON c.Ndem = mc.no_tiket
     	LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_material plm ON d.id = plm.psb_laporan_id
     	LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram gt ON g.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON g.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      LEFT JOIN psb_laporan_status pls ON d.status_laporan=pls.laporan_status_id
        '.$id.'
        GROUP BY c.id
        ORDER BY a.orderDate DESC
      LIMIT 5');
  }
   private static function Transaksi(){
     $SQL = '
     SELECT
      a.*
     FROM
        dshr a
 		 ORDER BY a.modified_at DESC ';
     $query = DB::select($SQL);
     return $query;
  }

  public static function cekWoTeknisi($tgl, $idRegu)
  {
    return DB::table('dispatch_teknisi')
            ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
            ->select('dispatch_teknisi.id','dispatch_teknisi.id_regu','dispatch_teknisi.created_at','psb_laporan.id_tbl_mj','psb_laporan.status_laporan')
            ->where('dispatch_teknisi.id_regu','=',$idRegu)
            ->where('psb_laporan.status_laporan','=','6')
            ->where(function ($query) use ($tgl){
                  $query->where('dispatch_teknisi.created_at','LIKE','%'.$tgl.'%')
                        ->orwhere('dispatch_teknisi.updated_at','LIKE','%'.$tgl.'%');
              })
            ->get();
  }

  public static function getData($ndem)
  {
      return DB::table('dispatch_teknisi')->where('Ndem',$ndem)->first();
  }

  public static function updateManja($req, $Ndem, $manjaBy)
  {
      return DB::table('dispatch_teknisi')->where('id',$Ndem)->update([
          'manja_status'  => $req->manja_status,
          'manja'         => $req->manja,
          'manjaBy'       => $manjaBy
      ]);
  }

  public static function simpanManja($req, $manjaBy, $id)
  {
      return DB::table('dispatch_teknisi')->insert([
          'Ndem'                => $id,
          'updated_by'          => DB::raw('NOW()'),
          'manja'               => $req->manja,
          'created_at'          => DB::raw('NOW()'),
          'manja_status'        => $req->manja_status,
          'manjaBy'             => $manjaBy
      ]);
  }

  public static function getJmlWoBySektor($tgl)
  {
      $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.JAM as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        c.sto,
        r.id_regu,
        r.uraian,
        gt.chat_id,
        SUM(CASE WHEN c.sto = "BBR" THEN 1 ELSE 0 END) as BBR,
        SUM(CASE WHEN c.sto = "MTP" THEN 1 ELSE 0 END) as MTP,
        SUM(CASE WHEN c.sto = "LUL" THEN 1 ELSE 0 END) as LUL,
        SUM(CASE WHEN c.sto is null or c.sto = "" THEN 1 ELSE 0 END) as lain,
        count(r.id_regu) as jumlahWo
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN nonatero_excel_bank d ON a.Ndem = d.TROUBLE_NO
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIn regu r on a.id_regu=r.id_regu
      LEFT JOIN group_telegram gt on r.mainsector=gt.chat_id
      WHERE
        ((DATE(b.modified_at) = "'.$tgl.'" OR DATE(b.created_at) = "'.$tgl.'" OR DATE(a.updated_at)="'.$tgl.'") OR ((b.status_laporan IN (5,6,7,31)
        OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) and gt.chat_id="-1001108596633" GROUP By r.id_regu
    ');
    return $query;
  }

  public static function getJmlWoBySektorIdRegu($tgl, $sto, $idRegu)
  {
      if ($sto == 'ALL'){
          $where = ' and r.id_regu = "'.$idRegu.'"';
      }
      elseif ($sto=='lain'){
          $where = ' and (c.sto is null or c.sto="") and r.id_regu = "'.$idRegu.'"';
      }
      else {
          $where = ' and c.sto="'.$sto.'" and r.id_regu="'.$idRegu.'"';
      };

      $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.JAM as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        c.sto,
        r.id_regu,
        r.uraian,
        gt.chat_id
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN nonatero_excel_bank d ON a.Ndem = d.TROUBLE_NO
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      LEFT JOIn regu r on a.id_regu=r.id_regu
      LEFT JOIN group_telegram gt on r.mainsector=gt.chat_id
      WHERE
        ((DATE(b.modified_at) = "'.$tgl.'" OR DATE(b.created_at) = "'.$tgl.'" OR DATE(a.updated_at)="'.$tgl.'") OR ((b.status_laporan IN (5,6,7,31)
        OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) and gt.chat_id="-1001108596633"'.$where);

    return $query;
  }

  public static function QueryBuilderTambahan($orderKontak){
     $SQL = '
      SELECT
        c.id as id_dt,
        c.updated_at,
        c.updated_by as hd,
        c.updated_at as hd_date,
        a.*,
        b.*,
        h.LOCN_X,
        h.LOCN_Y,
        h.OLT_IP,
        i.OLT,
        c.id_regu AS id_r,
        g.uraian,
        c.updated_at,
        d.status_laporan,
        d.catatan,
        f.created_by,
        a.orderId as status_wo_by_hd,
        c.id as jumlah_material,
        j.*,
        j.id as id_dshr,
        a.Kcontact as Kcontact,
        mn.manja_status_id,
        mn.manja_status,
        c.dispatch_by,
        c.dispatch_by,
        c.kordinatPel,
        c.alamatSales
      FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN ms2n b ON a.orderNcli = b.Ncli
      LEFT JOIN dispatch_teknisi c ON b.Ndem=c.Ndem OR a.orderId = c.Ndem
      LEFT JOIN psb_laporan d ON c.id=d.id_tbl_mj
      LEFT JOIN ms2n_sebab e ON b.Sebab = e.ms2n_sebab
      LEFT JOIN ms2n_status_hd f ON b.Ndem=f.Ndem
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN odp_ftp h ON a.alproname = h.INDEX_NAME
      LEFT JOIN olt i ON h.OLT_IP = i.OLT_IP
      LEFT JOIN dshr j ON a.orderId = j.kode_sc
      LEFT JOIN manja_status mn on c.manja_status=mn.manja_status_id
      Where a.orderKontak = "'.$orderKontak.'"
      GROUP BY a.orderId DESC ORDER BY a.orderDate DESC LIMIT 5';
      $query = DB::select($SQL);
      return $query;
  }

  public function listManja($tglAwal, $tglAkhir)
  {
      return DB::table('listmanja')
        ->where('orderStatusId',16)
        ->whereBetween('orderDate',[$tglAwal,$tglAkhir])
        ->orderBy('orderDate','DESC')
        ->paginate(20);
        // ->get();
  }

  public function manjaSearchBySc($ndem)
  {
      return DB::table('listmanja')
        ->where('orderId',$ndem)
        ->get();
  }

  public static function cariManja($myir){
      return DB::table('psb_myir_wo')->where('myir',$myir)->first();
  }

  public static function manjaSearchByMyir($ndem)
  {
      return DB::table('psb_myir_wo')
        ->leftJoin('dispatch_teknisi','psb_myir_wo.myir','=','dispatch_teknisi.Ndem')
        ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
        ->select('psb_myir_wo.*','dispatch_teknisi.*','regu.uraian','regu.mainsector')
        ->where('psb_myir_wo.ket','0')
        ->where('psb_myir_wo.myir',$ndem)
        ->get();
  }

  public static function cariMyir($myir){
      return DB::table('psb_myir_wo')->where('myir',$myir)->first();
  }

  public function jumlahManjaByUser($bulan)
  {
      $sql = 'SELECT a.manjaBy, 1_2_employee.nama, sum(case WHEN a.manja_status=1 THEN 1 ELSE 0 END) as manjaOk, sum(case WHEN a.manja_status=2 THEN 1 else 0 END) as manjaNOK FROM dispatch_teknisi a LEFT JOIN 1_2_employee on 1_2_employee.nik=a.manjaBy where a.updated_at like "%'.$bulan.'%" and a.manjaBy is not null and a.manjaBy<>0 GROUP by a.manjaBy';

      return DB::select($sql);
  }

  public static function getGroupTelegramProv(){
      return DB::table('group_telegram')
              ->where('sektor', 'LIKE', 'prov%')
              ->where('TL','!=','')
              ->get();
  }

  public static function getUnscByUnsc($unsc){
      return DB::table('unsc_tinjut')->where('unsc',$unsc)->first();
  }

  public static function getTeamMatrixNot($date){
    $get_where_area = 'AND f.title LIKE "%TERR%"';
    $sql = '
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan,
        f.chat_id,
        b.sttsWo
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      LEFT JOIN myirSCTgl my ON a.Ndem=my.myir
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND (a.dispatch_by IS NULL OR a.dispatch_by IN (1,2,3,4,5))
        '.$get_where_area.'
      GROUP BY a.id_regu
    ';

    $query = DB::SELECT($sql);
    return $query;
  }

  public static function listWOSek($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        d.TROUBLE_NO as close_status,
        d.hari as umur,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        TIMESTAMPDIFF(MINUTE,b.modified_at,"'.date("Y-m-d H:i:s").'") as interval1,
        TIMESTAMPDIFF(MINUTE,b.created_at,"'.date("Y-m-d H:i:s").'") as interval2,
        TIMESTAMPDIFF(HOUR,d.TROUBLE_OPENTIME,"'.date('Y-m-d H:i:s').'") as umur_gangguan,
        TIMESTAMPDIFF(HOUR,c.orderDate,"'.date('Y-m-d H:i:s').'") as umur_prov,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        d.TROUBLE_OPENTIME,
        c.orderDate,
        d.channel,
        a.dispatch_by
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN rock_excel_bank d ON a.Ndem = d.trouble_no
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      WHERE
        ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'" OR DATE(a.updated_at)="'.$date.'") OR ((b.status_laporan IN (6,7) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y').'%")) AND
        a.id_regu = "'.$id_regu.'"
    ');
    return $query;
  }

  public static function getReguNotWo($date){
      $query = DB::select('select t1.uraian, t1.title from (select r.id_regu, r.uraian, d.Ndem, g.title, (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik1 AND date(abs1.date_created)="'.$date.'") as nik1_absen, (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik2 AND date(abs1.date_created)="'.$date.'") as nik2_absen from regu r left join dispatch_teknisi d on d.id_regu=r.id_regu and date(d.updated_at)= "'.$date.'" left join group_telegram g on r.mainsector=g.chat_id where r.ACTIVE=1 and g.sektor like "%prov%") as t1 where t1.nik1_absen=1 and t1.nik2_absen=1 and t1.Ndem is NULL ORDER BY t1.title ASC');

      return $query;
  }

  public static function statusAbsen($nik, $date){
      return DB::table('absen')->where('date_created','LIKE','%'.$date.'%')->where('approval',1)->where('nik',$nik)->first();
  }

  public static function getTeamMatrixReboundary($date){
    $get_where_area = "AND b.sttsWo=1";
    $query = DB::SELECT('
     SELECT
        b.id_regu,
        b.nik1,
        b.nik2,
        b.uraian,
        b.mainsector,
        b.kemampuan as target_MH,
        b.kemampuan,
        a.updated_at,
        b.mitra,
        f.chatTl,
        f.title,
        f.sektor,
        f.ket_sektor,
        d.modified_at,
        d.created_at,
        my.*,
        d.status_laporan
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON a.Ndem = e.orderId
      LEFT JOIN group_telegram f ON b.mainsector = f.chat_id
      LEFT JOIN reboundary_survey my ON e.orderId=my.scid
      WHERE
        ((DATE(d.modified_at) = "'.$date.'" OR DATE(d.created_at) = "'.$date.'") OR ((d.status_laporan IN (5,6,7,4) OR d.id_tbl_mj is NULL ) AND a.tgl LIKE "'.date('Y-m').'%" )) AND a.dispatch_by = 6
        '.$get_where_area.'
      GROUP BY a.id_regu
    ');

    return $query;
  }

  public static function listWOReboundary($date,$id_regu){
    $query = DB::SELECT('
      SELECT
        a.Ndem,
        b.status_laporan,
        c.jenisPsb as jenisPsb,
        e.laporan_status,
        b.action,
        f.action as actionText,
        b.modified_at,
        b.created_at,
        a.created_at as tglDispatch,
        c.sto,
        a.updated_at,
        a.id as id_dt,
        b.catatan,
        c.orderDate,
        a.dispatch_by,
        a.jenis_layanan,
        e.grup
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_laporan_status e ON b.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_action f ON f.laporan_action_id = b.action
      WHERE
          ((DATE(b.modified_at) = "'.$date.'" OR DATE(b.created_at) = "'.$date.'") OR ((b.status_laporan IN (5,6,7,31,4) OR b.id_tbl_mj is NULL) AND a.tgl LIKE "'.date('Y-m').'%")) AND a.dispatch_by = 6 AND a.id_regu = "'.$id_regu.'"
    ');
    return $query;
  }

  public static function deleteDispatch($id)
  {
    return DB::table('dispatch_teknisi')->where('id',$id)->delete();
  }

  public static function deleteIbooster($id)
  {
    return DB::table('ibooster')->where('order_id',$id)->delete();
  }

  public static function get_sektor_matrix_team_prov($mainsector,$date)
  {
      return DB::SELECT('
      SELECT
        b.uraian,
        b.ket_bantek,
        a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.NO_ORDER = g.orderIdInteger
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      WHERE
        b.ACTIVE = 1 AND
        c.chat_id = "'.$mainsector.'" AND
        ((DATE(g.orderDatePs) IN ("'.date('Y-m-d').'" , "0000-00-00" , "1900-01-01", "1970-01-01")) OR g.orderDatePs IS NULL) AND
        a.tgl = "'.$date.'" AND
        a.jenis_layanan NOT IN ("QC","ONT_PREMIUM", "CABUT_NTE") AND (e.laporan_status != "PERMINTAAN REVOKE" OR e.laporan_status IS NULL) AND
        YEAR(a.tgl) = "'.date('Y').'"
      GROUP BY b.id_regu
      ');
  }

  public static function get_sektor_prov()
  {
    return DB::SELECT('SELECT * FROM group_telegram a WHERE a.ket_posisi = "PROV" AND a.sms_active_prov = 1 ORDER BY urut ASC');
  }

  public static function get_sektor_matrix_team_prov_old($mainsector,$date)
  {
    return DB::SELECT('
      SELECT
        b.uraian,
        b.ket_bantek,
        a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.NO_ORDER = g.orderIdInteger
      WHERE
      b.ACTIVE=1 AND
      YEAR(a.tgl)>="'.date('Y').'" AND
      c.chat_id = "'.$mainsector.'" AND
      a.jenis_layanan NOT IN ("QC","CABUT_NTE","ONT_PREMIUM") AND
      (a.tgl="'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl) = "' . date('Y') .'" AND MONTH(a.tgl) = "' . date('m') . '"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_order_prov($id_regu,$date)
  {
    return DB::SELECT('
      SELECT
        *,
        a.Ndem,
        d.grup,
        d.laporan_status,
        a.updated_at,
        a.id as id_dt,
        c.is_3hs as is_3HS,
        c.is_12hs as is_12HS,
        0 as GAUL,
        a.created_at as Reported_Date,
        a.updated_by as Assigned_by,
        dtjl.jenis_transaksi as dtjl_jenis_transaksi,
        ff.approve_date as approve_date_sc,
        g.orderDatePs as orderDate_Ps,
        (SELECT dps.kcontact FROM Data_Pelanggan_Starclick dps WHERE dps.kcontact LIKE "%TREGVI%" AND a.NO_ORDER = dps.orderIdInteger) as bundling_addon,
        (SELECT dps.kcontact FROM Data_Pelanggan_Starclick dps WHERE dps.kcontact LIKE "%PLS%" AND dps.jenisPsb LIKE "MO%" AND a.NO_ORDER = dps.orderIdInteger) as addon_plasa
      FROM dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON a.jenis_layanan_id = dtjl.jenis_layanan_id
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.NO_ORDER = g.orderIdInteger
      LEFT JOIN psb_myir_wo ff ON g.myir = ff.myir
      LEFT JOIN comparin_data_api cda ON g.orderIdInteger = cda.ORDER_ID AND (cda.type_grab = "ADDON" AND cda.STATUS_RESUME IN ("OSS PROVISIONING ISSUED", "Fallout (WFM)", "OSS PONR", "Fallout (Activation)") AND cda.JENIS_ADDON LIKE "%2P-3P%")
      WHERE
      (a.tgl="'.$date.'" OR c.id IS NULL  OR c.status_laporan IN (74,4,6)) AND
      b.ACTIVE=1 AND
      a.jenis_layanan NOT IN ("QC","CABUT_NTE","ONT_PREMIUM") AND
      b.id_regu = "'.$id_regu.'" AND
      YEAR(a.tgl)="'.date('Y').'" AND
      a.jenis_order IN ("SC","MYIR") AND
      a.NO_ORDER <> "0"
      ORDER BY a.created_at ASC
    ');
  }

  public static function get_sektor_matrix_team_order_prov_go($id_regu,$date)
  {  
    return DB::SELECT('
      SELECT
        *,
        a.Ndem,
        d.grup,
        d.laporan_status,
        a.updated_at,
        a.id as id_dt,
        c.is_3hs as is_3HS,
        c.is_12hs as is_12HS,
        0 as GAUL,
        a.created_at as Reported_Date,
        a.updated_by as Assigned_by,
        dtjl.jenis_transaksi as dtjl_jenis_transaksi,
        f.approve_date,
        ff.approve_date as approve_date_sc,
        g.orderDatePs as orderDate_Ps,
        (SELECT dps.kcontact FROM Data_Pelanggan_Starclick dps WHERE dps.kcontact LIKE "%TREGVI%" AND a.NO_ORDER = dps.orderIdInteger) as bundling_addon,
        (SELECT dps.kcontact FROM Data_Pelanggan_Starclick dps WHERE dps.kcontact LIKE "%PLS%" AND dps.jenisPsb LIKE "MO%" AND a.NO_ORDER = dps.orderIdInteger) as addon_plasa,
        cda.JENIS_ADDON
      FROM dispatch_teknisi a
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON a.jenis_layanan_id = dtjl.jenis_layanan_id
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.NO_ORDER = g.orderIdInteger AND YEAR(g.orderDate) = "'.date('Y').'"
      LEFT JOIN psb_myir_wo ff On g.myir = ff.myir
      LEFT JOIN comparin_data_api cda ON g.orderIdInteger = cda.ORDER_ID AND (cda.type_grab = "ADDON" AND cda.STATUS_RESUME IN ("OSS PROVISIONING ISSUED", "Fallout (WFM)", "OSS PONR", "Fallout (Activation)") AND cda.JENIS_ADDON LIKE "%2P-3P%")
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      WHERE
        b.id_regu = "'.$id_regu.'" AND
        ((DATE(g.orderDatePs) IN ("'.date('Y-m-d').'" , "0000-00-00" , "1900-01-01", "1970-01-01")) OR g.orderDatePs IS NULL) AND
        a.tgl = "'.$date.'" AND
        dtjl.jenis_layanan NOT IN ("QC","ONT_PREMIUM", "CABUT_NTE") AND (d.laporan_status != "PERMINTAAN REVOKE" OR d.laporan_status IS NULL) AND
        YEAR(a.tgl) = "'.date('Y').'"
      ORDER BY a.created_at ASC
    ');
  }

  public static function get_sektor_matrix_team_prov_khusus($mainsector,$date)
  {
    return DB::SELECT('
      SELECT
      b.uraian,
      b.ket_bantek,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE = 1 AND
      a.jenis_layanan IN ("CABUT_NTE","ONT_PREMIUM") AND
      c.chat_id = "'.$mainsector.'" AND
      (DATE(a.tgl) = "'.$date.'" AND (d.id IS NULL OR e.grup IN ("NP", "OGP", "UP")) OR
      DATE(a.tgl) BETWEEN "'.date('Y-m-d', strtotime("-1 days", strtotime($date))).'" AND "'.$date.'" AND e.grup IN ("KP", "KT", "SISA"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_order_khusus($id_regu,$date)
  {
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS,
      0 as GAUL,
      a.created_at as Reported_Date,
      a.updated_by as Assigned_by,
      jl.KAT as jenis_layanan
      FROM
        dispatch_teknisi a
      LEFT JOIN cabut_nte_order cno ON a.NO_ORDER = cno.wfm_id
      LEFT JOIN ont_premium_order opo ON a.NO_ORDER = opo.no_inet
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
      WHERE
      b.ACTIVE = 1 AND
      a.jenis_layanan IN ("CABUT_NTE","ONT_PREMIUM") AND
      b.id_regu = "'.$id_regu.'" AND
      (DATE(a.tgl) = "'.$date.'" AND (c.id IS NULL OR d.grup IN ("NP", "OGP", "UP")) OR
      DATE(a.tgl) BETWEEN "'.date('Y-m-d', strtotime("-1 days", strtotime($date))).'" AND "'.$date.'" AND d.grup IN ("KP", "KT", "SISA"))
      ORDER BY
      a.created_at
    ');
  }

  public static function getReguProv()
  {
    return DB::select('
      SELECT
        a.id_regu as id,
        a.uraian as text
      FROM regu a
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      WHERE
        a.ACTIVE = 1 AND 
        b.sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND 
        b.sms_active_prov = 1
    ');
  }

  public static function getReguTerdispatchCabutNte()
  {
    return DB::select('
      SELECT
        a.id_regu as id,
        a.uraian as text,
        SUM(CASE WHEN d.status_laporan = 6 OR d.status_laporan IS NULL THEN 1 ELSE 0 END) as jumlah
      FROM regu a
        LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
        LEFT JOIN dispatch_teknisi c ON a.id_regu = c.id_regu
        LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      WHERE
        a.ACTIVE = 1 AND 
        b.sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND 
        b.sms_active_prov = 1 AND 
        c.jenis_order = "CABUT_NTE" AND 
        (d.status_laporan = 6 OR d.status_laporan IS NULL)
      GROUP BY a.id_regu
      ORDER BY jumlah DESC
    ');
  }

  public static function getOrdersDll($type)
  {
    switch ($type) {
      case 'MIGRASI_STB_PREMIUM':
        return DB::select('
          SELECT
            msp.no_internet as nopel
          FROM migrasi_stb_premium msp
          LEFT JOIN dispatch_teknisi dt ON msp.no_internet = dt.NO_ORDER AND dt.jenis_order = "MIGRASI_STB_PREMIUM"
          WHERE
            msp.kode = "valid" AND
            dt.id IS NULL
        ');
        break;
      
      case 'CABUT_NTE':
        return DB::select('
        ');
        break;

      case 'ONT_PREMIUM':
        return DB::select('
        ');
        break;
    }
  }

  public static function alert_umur_pi($type)
  {
    switch ($type)
    {
      case 'Saldo_PI_Lebih_4_Hours':
          return DB::select('
            SELECT
              TIMESTAMPDIFF(HOUR, dps.orderDatePI, NOW()) AS umur,
              ma.mitra_amija_pt AS nama_mitra,
              gt.title AS nama_sektor,
              gt.chat_id AS chatid_sektor,
              r.uraian AS nama_tim,
              pls.laporan_status AS nama_status,
              dtjl.jenis_layanan,
              dps.orderId,
              dps.orderStatus,
              dps.orderDate,
              dps.orderDatePI,
              dps.orderDateActComp,
              dps.orderDateRegist
            FROM Data_Pelanggan_Starclick dps
            LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
            LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
            LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            WHERE
              dps.orderDatePI IS NOT NULL and
              SUBSTRING_INDEX(dps.jenisPsb, "|", 1) IN ("AO", "PDA", "PDA LOCAL") AND
              dps.orderStatus = "OSS PROVISIONING ISSUED" AND
              (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-2 days')).'" AND "'.date('Y-m-d').'") AND
              gt.title IS NOT NULL
          ');
        break;
      
      case 'TTR_PI_4_Hours':
          return DB::select('
            SELECT
              TIMESTAMPDIFF(HOUR, dps.orderDatePI, dps.orderDateActComp) AS umur,
              ma.mitra_amija_pt AS nama_mitra,
              gt.title AS nama_sektor,
              gt.chat_id AS chatid_sektor,
              r.uraian AS nama_tim,
              pls.laporan_status AS nama_status,
              dtjl.jenis_layanan,
              dps.orderId,
              dps.orderStatus,
              dps.orderDate,
              dps.orderDatePI,
              dps.orderDateActComp,
              dps.orderDateRegist
            FROM Data_Pelanggan_Starclick dps
            LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
            LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
            LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            WHERE
              (dps.orderDatePI IS NOT NULL AND dps.orderDateActComp IS NOT NULL) AND
              SUBSTRING_INDEX(dps.jenisPsb, "|", 1) IN ("AO", "PDA", "PDA LOCAL") AND
              dps.orderStatus != "COMPLETED" AND
              (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-2 days')).'" AND "'.date('Y-m-d').'") AND
              gt.title IS NOT NULL
          ');
        break;

        case 'TTI_3_Days_TSEL':
          return DB::select('
            SELECT
              TIMESTAMPDIFF(HOUR, dps.orderDateRegist, NOW()) AS umur,
              ma.mitra_amija_pt AS nama_mitra,
              gt.title AS nama_sektor,
              gt.chat_id AS chatid_sektor,
              r.uraian AS nama_tim,
              pls.laporan_status AS nama_status,
              dtjl.jenis_layanan,
              dps.orderId,
              dps.orderStatus,
              dps.orderDate,
              dps.orderDatePI,
              dps.orderDateActComp,
              dps.orderDateRegist
            FROM Data_Pelanggan_Starclick dps
            LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
            LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
            LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            WHERE
              dps.orderDateRegist IS NOT NULL AND
              dps.provider = "PL-TSEL" AND
              SUBSTRING_INDEX(dps.jenisPsb, "|", 1) IN ("AO", "PDA", "PDA LOCAL") AND
              dps.orderStatus NOT IN ("COMPLETED", "OSS CANCEL COMPLETE", "CANCEL COMPLETED", "REVOKE COMPLETED", "FOLLOW UP BOOKED", "FOLLOW UP COMPLETED", "OSS - TESTING SERVICE", "WFM ACTIVATION COMPLETE", "WFM UN-SCS", "TIBS FULFILL BILLING START", "TIBS FULFILL BILLING COMPLETED", "TIBS SYNC CUSTOMER COMPLETED", "OSS PROVISIONING COMPLETE", "WAIT FOR PAYMENT") AND
              (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-2 days')).'" AND "'.date('Y-m-d').'") AND
              gt.title IS NOT NULL
          ');
        break;
    }
  }

}
