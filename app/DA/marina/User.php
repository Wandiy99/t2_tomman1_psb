<?php

namespace App\DA\marina;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
class User
{
    const TABLE = 'user';
    const KARYAWAN = 'karyawan';
    const PROFILE = 'manci_profile';

    public static function userExists($id)
    {
        return DB::table(self::TABLE)->where('id_user', $id)->first();
    }
    // public static function absen($id)
    // {
    //     DB::table(self::KARYAWAN)
    //     ->where('id_karyawan', $id)
    //     ->update([
    //         'mt_absen' => DB::raw('now()')
    //     ]);
    //     DB::table('maintenance_absen_log')->insert([
    //         'id_user' => $id,
    //         'status' => 1,
    //         'created_by' => $id,
    //         'created_at' => DB::raw('now()')
    //         ]);
    // }
    // public static function verifAbsen($id, $verif_by)
    // {
    //     DB::table(self::KARYAWAN)->where('id_karyawan', $id)->update([
    //         'mt_verif_absen' => DB::raw('now()')
    //         ]);
    //     DB::table('maintenance_absen_log')->insert([
    //         'id_user' => $id,
    //         'status' => 2,
    //         'created_by' => $verif_by,
    //         'created_at' => DB::raw('now()')
    //         ]);
    // }
    public static function getUser()
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::KARYAWAN.".nama", self::KARYAWAN.".nama_instansi", '1_2_employee.nama as nama2')
        ->leftJoin(self::KARYAWAN, self::TABLE.'.id_karyawan', '=', self::KARYAWAN.'.id_karyawan')
        ->leftJoin('1_2_employee', self::TABLE.'.id_karyawan', '=', '1_2_employee.nik')
        ->whereNotNull('id_user')->get();
    }
    
    // public static function getRecordedUser()
    // {
    //     return DB::table(self::PROFILE)
    //     ->select(self::PROFILE.'.*', self::KARYAWAN.".nama")
    //     ->leftJoin(self::KARYAWAN, self::KARYAWAN.'.id_karyawan', '=', self::PROFILE.'.nik')
    //     ->get();
    // }
    // public static function getProfile($id)
    // {
    //     return DB::table('manci_profile')->where('nik', $id)->first();
    // }
    // public static function updateProfile($req, array $input)
    // {
    //     return DB::table('manci_profile')->where('nik', $req->nik)
    //     ->update($input);
    // }
    // public static function updateProfileByNik($nik, array $input)
    // {
    //     return DB::table('manci_profile')->where('nik', $nik)
    //     ->update($input);
    // }
    // public static function getByRememberToken($token)
    // {
    //     return DB::table(self::KARYAWAN)
    //     ->select(self::TABLE.'.*', self::KARYAWAN.'.nama', self::KARYAWAN.'.status_kerja as loginMethod', self::KARYAWAN.'.nama_instansi', 'manci_profile.*')
    //     ->leftJoin(self::TABLE, self::KARYAWAN.'.id_karyawan', '=', self::TABLE.'.id_karyawan')
    //     ->leftJoin('manci_profile', self::TABLE.'.id_user', '=', 'manci_profile.nik')
    //     ->where('manci_profile.remember_token', $token)->first();
    // }
    // public static function storeProfile(array $input)
    // {
    //     return DB::table('manci_profile')->insert($input);
    // }
    public static function getUserById($id)
    {
        return DB::table(self::TABLE)
        ->select(self::TABLE.'.*', self::KARYAWAN.".nama", self::KARYAWAN.".nama_instansi", '1_2_employee.nama as nama2')
        ->leftJoin(self::KARYAWAN, self::TABLE.'.id_karyawan', '=', self::KARYAWAN.'.id_karyawan')
        ->leftJoin('1_2_employee', self::TABLE.'.id_karyawan', '=', '1_2_employee.nik')
        ->where('id_user', $id)->first();
    }
    
    // public static function userCreate($req)
    // {
    //     DB::table(self::TABLE)->insert([
    //         "id_user" => $req->id_user,
    //         "password" => MD5($req->pwd),
    //         "id_karyawan" => $req->id_user,
    //         "status" => $req->status,
    //         "maintenance_level" => $req->maintenance_level,
    //         "jenis_verif" => $req->jenis_verif
    //     ]);
    // }
    // public static function userUpdate($req)
    // {
    //     if($req->pwd){
    //         DB::table(self::TABLE)
    //         ->where("id_user" , $req->hid)
    //         ->update([
    //             "password" => MD5($req->pwd)
    //         ]);
    //     }
    //     DB::table(self::TABLE)
    //     ->where("id_user" , $req->hid)
    //     ->update([
    //         "id_user" => $req->id_user,
    //         "id_karyawan" => $req->id_user,
    //         "status" => $req->status,
    //         "maintenance_level" => $req->maintenance_level,
    //         "jenis_verif" => $req->jenis_verif
    //     ]);
    // }
    public static function karyawanExists($id)
    {
        return DB::table(self::KARYAWAN)->where('id_karyawan', $id)->first();
    }
    // public static function karyawanCreate($req)
    // {
    //     DB::table(self::KARYAWAN)->insert([
    //         "id_karyawan" => $req->id_user,
    //         "nama" => $req->nama,
    //         "nama_instansi" => $req->nama_instansi,
    //         "status_kerja" => "MT"
    //     ]);
    // }
    // public static function karyawanUpdate($req)
    // {
    //     DB::table(self::KARYAWAN)
    //     ->where("id_karyawan", $req->hid)
    //     ->update([
    //         "id_karyawan" => $req->id_user,
    //         "nama" => $req->nama,
    //         "nama_instansi" => $req->nama_instansi
    //     ]);
    // }

    //regu
    public static function getRegu()
    {
        return DB::table('regu')->get();
    }
    public static function getReguById($id)
    {
        return DB::table('regu')
        ->where('id_regu', $id)->first();
    }
    // public static function reguCreate($req)
    // {
    //     DB::table('maintenance_regu')->insert([
    //         "nama_regu" => $req->nama_regu,
    //         "mitra" => $req->mitra,
    //         "spesialis" => $req->spesialis,
    //         "nik1" => $req->nik1,
    //         "nik2" => $req->nik2,
    //         "nik3" => $req->nik3,
    //         "nik4" => $req->nik4,
    //         "niktl" => $req->niktl,
    //         "chat_id" => $req->chat_id,
    //         "status_regu" => $req->status_regu
    //     ]);
    // }
    // public static function reguUpdate($req)
    // {
        
    //     DB::table('maintenance_regu')
    //     ->where("id" , $req->hid)
    //     ->update([
    //         "nama_regu" => $req->nama_regu,
    //         "mitra" => $req->mitra,
    //         "spesialis" => $req->spesialis,
    //         "nik1" => $req->nik1,
    //         "nik2" => $req->nik2,
    //         "nik3" => $req->nik3,
    //         "nik4" => $req->nik4,
    //         "niktl" => $req->niktl,
    //         "chat_id" => $req->chat_id,
    //         "status_regu" => $req->status_regu
    //     ]);
    // }
}