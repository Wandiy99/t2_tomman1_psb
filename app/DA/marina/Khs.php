<?php

namespace App\DA\marina;

use Illuminate\Support\Facades\DB;
class Khs
{
    public static function getByProgram($id, $witel)
    {
        return DB::table('khs_maintenance')
            ->leftJoin('maintenance_mtr_program', 'khs_maintenance.id_item', '=', 'maintenance_mtr_program.id_item')
            ->where('program_id', $id)->where('maintenance_mtr_program.witel', $witel)->get();
    }
    public static function getItemAll($witel)
    {
        return DB::table('khs_maintenance')->select('id_item as id', 'id_item as text', 'uraian')->where('witel', $witel)->get();
    }
    // public static function countUnverif($id, $lvl)
    // {
    //     $data = 0;
    //     $query = " ";
    //     if($lvl == 2){
    //       $query = ' and mt.verify is null and mr.niktl = "'.$id.'" ';
    //     }else if($lvl == 3){
    //       $query = ' and mt.verify=1';
    //     }else if($lvl == 4){
    //       $query = ' and mt.verify=2';
    //     }
    //     $data = DB::select('select count(*) as count
    //                   from maintaince mt 
    //                   left join maintenance_regu mr on mt.dispatch_regu_id = mr.id 
    //                   where refer is null and mt.status = "close" 
    //           '.$query.' and mt.mtrcount>0 order by tgl_selesai desc')[0];
    //     if($lvl == 2 || $lvl == 3 || $lvl == 4)
    //         return $data->count;
    //     else
    //         return 0;
    // }
}