<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class KproModel
{
    public static function dashboard($jenis,$play,$filter){
        $where_play = '';
        switch ($play){
            case "2p3p" :
                $where_play = 'AND a.type_layanan IN ("2P","3P")';
            break;
        }
        if ($jenis<>"ALL"){
            $where_jenis = 'AND a.jenispsb = "'.$jenis.'"';
        }
        switch ($filter){
          case "potensi_ps" :
            $where_status_resume = 'AND a.status_resume IN ("8 | OSS - PONR","Completed (PS)")';
            $select = 'b.status_resume_group as status_resume_group_x,';
            $group_by = 'group by a.status_resume';
          break;
          case "sisa_pi" :
            $where_status_resume = 'AND a.status_resume IN ("7 | OSS - PROVISIONING ISSUED")';
            $select = 'd.laporan_status as status_resume_group_x,';
            $group_by = 'group by d.laporan_status';
          break;
          case "sisa_fwfm" :
          $where_status_resume = 'AND a.status_resume IN ("Fallout (WFM)")';
          $select = 'd.laporan_status as status_resume_group_x,';
          $group_by = 'group by d.laporan_status';

          break;
        }
        $query = DB::SELECT('
        SELECT
        '.$select. '
        a.status_message,
        a.status_resume,
        count(*) as jumlah,
        sum(case when a.datel = "BJRMASIN" THEN 1 ELSE 0 END) as BANJARMASIN,
        sum(case when a.datel = "BANJARBARU" THEN 1 ELSE 0 END) as BANJARBARU,
        sum(case when a.datel = "BATULICIN" THEN 1 ELSE 0 END) as BATULICIN,
        sum(case when a.datel = "TANJUNG" THEN 1 ELSE 0 END) as TANJUNG
        FROM provi_kpro_tr6 a
        LEFT JOIN starclick_status_group b ON a.status_resume = b.status_resume_kpro
        LEFT JOIN dispatch_teknisi bb ON a.order_id = bb.NO_ORDER
        LEFT JOIN psb_laporan c ON bb.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
        WHERE
        a.witel = "BANJARMASIN"
        '.$where_status_resume.'
        '.$where_jenis.'
        '.$where_play.'
        '.$group_by.'
        ');
        return $query;
    }
}
