<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class IoanAlkerModel
{

  public static function getAll()
  {
    return DB::table('ioan_alker_sarker')->get();
  }

  public static function getByPic($pic)
  {
    return DB::table('ioan_alker_sarker')->where('nik_pic', $pic)->get();
  }

  public static function laporSave($req)
  {
    $get_alker = [
      (object)['id' => 0, 'text' => 'Splicer'],
      (object)['id' => 1, 'text' => 'One_Click_Cleaner'],
      (object)['id' => 2, 'text' => 'Fiber_Stripper'],
      (object)['id' => 3, 'text' => 'OTDR'],
      (object)['id' => 4, 'text' => 'Testphone'],
      (object)['id' => 5, 'text' => 'LAN_Tester'],
      (object)['id' => 6, 'text' => 'OPM'],
      (object)['id' => 7, 'text' => 'Light_Source_Optic'],
      (object)['id' => 8, 'text' => 'Visual_Fault_Locator'],
      (object)['id' => 9, 'text' => 'Tangga_Teleskopik'],
      (object)['id' => 10, 'text' => 'Toolkit_Set'],
      (object)['id' => 11, 'text' => 'HP_Android'],
      (object)['id' => 12, 'text' => 'Paket_Internet'],
      (object)['id' => 13, 'text' => 'Body_Harness'],
      (object)['id' => 14, 'text' => 'Crimping_Tools'],
      (object)['id' => 15, 'text' => 'Tone_Checker'],
      (object)['id' => 16, 'text' => 'LSA_Plus'],
      (object)['id' => 17, 'text' => 'Seragam_IndiHome'],
      (object)['id' => 18, 'text' => 'Tas_Punggung'],
      (object)['id' => 19, 'text' => 'Jas_Hujan'],
      (object)['id' => 20, 'text' => 'KBM_Roda2'],
      (object)['id' => 21, 'text' => 'BBM_Roda2'],
      (object)['id' => 22, 'text' => 'Power_Bank_Valins'],
      (object)['id' => 23, 'text' => 'ID_Card'],
      (object)['id' => 24, 'text' => 'Optical_Fiber_Ranger_(MiniOTDR)']
    ];
    
    foreach ($get_alker as $alker)
    {
      $check =  DB::table('ioan_alker_sarker')
      ->where('nik_pic', session('auth')->id_karyawan)
      ->where('alker', $alker->text)
      ->first();

      if ($check)
      {
        DB::table('ioan_alker_sarker')
        ->where('nik_pic', session('auth')->id_karyawan)
        ->where('alker', $alker->text)
        ->update([
          'sn' => $req->input('sn')[$alker->id],
          'alker' => $req->input('alker')[$alker->id],
          'nik_pic' => session('auth')->id_karyawan,
          'nama_pic' => session('auth')->nama,
          'merk' => $req->input('merk')[$alker->id],
          'tipe' => $req->input('tipe')[$alker->id],
          'tahun_pengadaan' => $req->input('tahun_pengadaan')[$alker->id],
          'plan_nomer' => $req->input('plan_nomer')[$alker->id],
          'status_alker' => $req->input('status_alker')[$alker->id]
        ]);
      } else {
        DB::table('ioan_alker_sarker')
        ->insert([
          'sn' => $req->input('sn')[$alker->id],
          'alker' => $req->input('alker')[$alker->id],
          'nik_pic' => session('auth')->id_karyawan,
          'nama_pic' => session('auth')->nama,
          'merk' => $req->input('merk')[$alker->id],
          'tipe' => $req->input('tipe')[$alker->id],
          'tahun_pengadaan' => $req->input('tahun_pengadaan')[$alker->id],
          'plan_nomer' => $req->input('plan_nomer')[$alker->id],
          'status_alker' => $req->input('status_alker')[$alker->id]
        ]);
      }
    }

    $inputPhotos = [
      'Splicer',
      'One_Click_Cleaner',
      'Fiber_Stripper',
      'OTDR',
      'Testphone',
      'LAN_Tester',
      'OPM',
      'Light_Source_Optic',
      'Visual_Fault_Locator',
      'Tangga_Teleskopik',
      'Toolkit_Set',
      'HP_Android',
      'Paket_Internet',
      'Body_Harness',
      'Crimping_Tools',
      'Tone_Checker',
      'LSA_Plus',
      'Seragam_IndiHome',
      'Tas_Punggung',
      'Jas_Hujan',
      'KBM_Roda2',
      'BBM_Roda2',
      'Power_Bank_Valins',
      'ID_Card',
      'Optical_Fiber_Ranger_(MiniOTDR)'
    ];

    foreach($inputPhotos as $name) {
      $input = 'photo-'.$name;
          if ($req->hasFile($input))
          {
            $path = public_path().'/upload4/alker_sarker/'.session('auth')->id_karyawan.'/';
            if (!file_exists($path))
            {
              if (!mkdir($path, 0770, true))
                return 'gagal menyiapkan folder foto alker_sarker';
            }
            $file = $req->file($input);
            $ext = 'jpg';
            try {
              $moved = $file->move("$path", "$name.$ext");

              $img = new \Imagick($moved->getRealPath());

              $img->scaleImage(100, 150, true);
              $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
              return 'gagal menyimpan foto alker_sarker '.$name;
            }
          }
    }
  }

}

?>
