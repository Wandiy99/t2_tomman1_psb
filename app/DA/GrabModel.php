<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class GrabModel
{

  public static function user_nossa(){
    date_default_timezone_set('Asia/Makassar');
    return DB::SELECT('
      SELECT
      *,
      TIMESTAMPDIFF( HOUR , a.updatetime,  "'.date('Y-m-d H:i:s').'" ) as umurlogin
      FROM
       user_nossa a
    ');
  }

  public static function barcodePsb($order){
        $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://emas.telkom.co.id/DAVA/Auth/submitLogin');
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=Yahyaarif66@gmail.com&password=SK9aOFzdQU");
    $login = curl_exec($ch);
    curl_setopt($ch, CURLOPT_URL, 'https://emas.telkom.co.id/DAVA/dataValidation/labelmgt/LabelCodeInfo_ctrl/findCodeDetails');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "csrf_test_name=&listinfo2=&labelCode=".$order);
    $result = curl_exec($ch);

    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(0);
    //$table = $dom->getElementsByTagName('fieldset')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = 'FALSE';

    if ( $rows->length>=2) {
      $result = 'TRUE';
      $user = ''; 
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $td = $cells->item(0);
        $td1 = $cells->item(1);
    
        if($rows->item(2)<>null){
            $user = $rows->item(2)->getElementsByTagName('td')->item(1)->nodeValue;
        }
      }
      // $order_id = $rows->item(2)->getElementsByTagName('td')->item(1)->nodeValue;

      if ($user<>''){
            $order_id = $user;
            $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a WHERE a.kcontact LIKE "%'.$order_id.'%" OR a.ndemSpeedy LIKE "%'.$order_id.'%" OR a.ndemPots LIKE "%'.$order_id.'%"');
            if (count($query)>0){
            foreach ($query as $result){
                $result = $result->orderId;
            }
          }
      }

    }
    return $result;
    curl_close($ch);
  }

  public static function barcode($order){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://emas.telkom.co.id/DAVA/Auth/submitLogin');
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=Yahyaarif66@gmail.com&password=SK9aOFzdQU");
    $login = curl_exec($ch);
    curl_setopt($ch, CURLOPT_URL, 'https://emas.telkom.co.id/DAVA/dataValidation/labelmgt/LabelCodeInfo_ctrl/findCodeDetails');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "csrf_test_name=&listinfo2=&labelCode=".$order);
    $result = curl_exec($ch);

    $header = curl_getinfo($ch);
    $header_content = substr($result, 0, $header['header_size']);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(0);

    $rows = $table->getElementsByTagName('tr');

    $result = 'Hasil Pencarian DAVA : <br />';

    if ( $rows->length>2) {
      // echo "Informasi DAVA : <br />";
      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $td = $cells->item(0);
        $td1 = $cells->item(1);
        $result .= $td->nodeValue." : ".$td1->nodeValue."<br />";
      }
      $order_id = $rows->item(2)->getElementsByTagName('td')->item(1)->nodeValue;
      if ($order_id<>"") {
        $result .= "<br />";
        $result .= "Informasi STARCLICK : <br />";
        $query = DB::SELECT('SELECT * FROM Data_Pelanggan_Starclick a WHERE a.kcontact LIKE "%'.$order_id.'%" OR a.ndemSpeedy LIKE "%'.$order_id.'%" OR a.ndemPots LIKE "%'.$order_id.'%"');

        foreach ($query as $resultQ) {
          $result .= "OrderId : SC".$resultQ->orderId."<br />";
          $result .= "ndemSpeedy : ".$resultQ->ndemSpeedy."<br />";
          $result .= "ndemPots : ".$resultQ->ndemPots."<br />";
          $result .= "OrderName : ".$resultQ->orderName."<br />";
          $result .= "OrderAddr : ".$resultQ->orderAddr."<br />";
          $result .= "alproname : ".$resultQ->alproname."<br />";
        }
      }
    }
    return $result;
    curl_close($ch);
  }
}
