<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class TeamModel
{
	public static function mainsector(){
		return DB::table('group_telegram')->get();
	}
	public static function getRegu($id)
	{
		return DB::SELECT('
		SELECT
			a.*,
			b.title as mainsector,
			ma.mitra_amija_pt,
			(SELECT aa.nama FROM  1_2_employee aa WHERE aa.nik = a.nik1) as nama_nik1,
			(SELECT bb.nama FROM  1_2_employee bb WHERE bb.nik = a.nik2) as nama_nik2
		FROM
			regu a
			LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
			LEFT JOIN mitra_amija ma ON a.mitra = ma.mitra_amija
		WHERE
			b.Witel_New = "'.$id.'" AND
			a.ACTIVE = 1
		GROUP BY a.id_regu
		ORDER BY a.date_created DESC
	');
	}
	public static function checkRegu($id){
		return DB::table('regu')->where('uraian',$id)->get();
	}
	public static function detailRegu($id){
		return DB::table('regu')->where('id_regu',$id)->first();
	}
	public static function distinctStatus(){
		return DB::SELECT('SELECT mitra_amija as text,mitra_amija as id FROM mitra_amija WHERE kat <> "" GROUP BY mitra_amija');
	}
	public static function distinctStatusOld(){
		return DB::SELECT('SELECT mitra as text,mitra as id FROM regu GROUP BY mitra');
	}
	public static function mitra_Maintenance(){
		return DB::SELECT('SELECT mitra as text, mitra as id FROM maintenance_mitra GROUP BY mitra');
	}
	public static function store($r)
	{	
		return DB::table('regu')
								->insert([
									'uraian' => $r->input('uraian'),
									'sto' => $r->input('sto'),
									'ket_bantek' => $r->input('ket_bantek'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									// 'nama_mitra' => $r->input('nama_mitra'),
									'mainsector' => $r->input('mainsector'),
									'area' => $r->input('area'),
									'fitur' => $r->input('fitur'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'spesialis_id_regu' => $r->input('spesialis_id_regu'),
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'date_created' => date('Y-m-d H:i:s'),
									'sttsWo' => 1
								]);
	}
	public static function update($r)
	{	
		$stts = 0;
		if ($r->input('mitra')=="SPM" || $r->input('mitra')=="AGB DELTA" || $r->input('mitra')=="CUI DELTA" || $r->input('mitra')=="UPATEK DELTA" || $r->input('mitra')=="MAP DELTA" || $r->input('mitra')=="IBS DELTA" || $r->input('mitra')=="TKM DELTA" || $r->input('mitra')=="TELKOM AKSES DELTA"){
			$stts = 1;
		};
		return DB::table('regu')
								->where('id_regu',$r->input('id_regu'))
								->update([
									'uraian' => $r->input('uraian'),
									'sto' => $r->input('sto'),
									'ket_bantek' => $r->input('ket_bantek'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									// 'nama_mitra' => $r->input('nama_mitra'),
									'mainsector' => $r->input('mainsector'),
									'area' => $r->input('area'),
									'fitur' => $r->input('fitur'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'spesialis_id_regu' => $r->input('spesialis_id_regu'),
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'status_alker' => $r->input('status_alker'),
									'date_created' => date('Y-m-d H:i:s'),
									'sttsWo' => $stts
								]);
	}

	public static function deleteTeam($id)
	{
		return DB::table('regu')->where('id_regu',$id)->delete();
	}
	public static function update_2($r)
	{
		$stts = 0;
		if ($r->input('mitra')=="SPM" || $r->input('mitra')=="AGB DELTA" || $r->input('mitra')=="CUI DELTA" || $r->input('mitra')=="UPATEK DELTA" || $r->input('mitra')=="MAP DELTA" || $r->input('mitra')=="IBS DELTA" || $r->input('mitra')=="TKM DELTA" || $r->input('mitra')=="TELKOM AKSES DELTA"){
			$stts = 1;
		};
		return DB::table('regu')
								->where('id_regu',$r->input('id_regu'))
								->update([
									'uraian' => $r->input('uraian'),
									'sto' => $r->input('sto'),
									'ket_bantek' => $r->input('ket_bantek'),
									'ACTIVE' => 1,
									'nik1' => $r->input('nik1'),
									'nik2' => $r->input('nik2'),
									'mitra' => $r->input('mitra'),
									// 'nama_mitra' => $r->input('nama_mitra'),
									'mainsector' => $r->input('mainsector'),
									'area' => $r->input('area'),
									'fitur' => $r->input('fitur'),
									'witel' => "KALSEL",
									'job' => "MULTISKILL",
									'spesialis' => "MULTISKILL",
									'spesialis_id_regu' => $r->input('spesialis_id_regu'),
									'kemampuan' => $r->input('manhours'),
									'crewid' => $r->input('crewid'),
									'date_created' => date('Y-m-d H:i:s'),
									'sttsWo' => $stts,
									'status_team' => $r->input('status_team'),
									'status_alker' => $r->input('status_alker')
								]);
	}

	public static function disable($id){
		return DB::table('regu')
								->where('id_regu',$id)
								->update([
									'ACTIVE' => 0
								]);
	}
}
