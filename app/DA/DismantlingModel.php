<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

class DismantlingModel
{
    public static function dashboard_1()
    {
        return DB::SELECT('
            SELECT 
            a.sto,
            COUNT(*) as jumlah,
            SUM(CASE WHEN b.Ndem IS NULL AND (b.jenis_order IS NULL OR b.jenis_order = "CABUT_NTE") THEN 1 ELSE 0 END) as undispatch,
            SUM(CASE WHEN (c.status_laporan = 6 OR c.id IS NULL) AND b.jenis_order = "CABUT_NTE" THEN 1 ELSE 0 END) as np,
            SUM(CASE WHEN c.status_laporan = 5 AND b.jenis_order = "CABUT_NTE" THEN 1 ELSE 0 END) as ogp
            FROM 
            racoonDismantling a
            LEFT JOIN dispatch_teknisi b ON a.nopel = b.NO_ORDER
            LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
            LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
            WHERE
            a.witel = "KALSEL"
            GROUP BY a.sto
        ');
    }
}