<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class DashboardModel{

	public static function pi_pagi($tgl){
		$query = DB::SELECT('
		SELECT
		b.sektor_prov,
		count(*) as jumlah_pi_pagi,
		SUM(CASE WHEN a.STATUS_RESUME NOT IN ("OSS PROVISIONING ISSUED","COMPLETED") THEN 1 ELSE 0 END) as pi_pagi_progress,
		SUM(CASE WHEN a.STATUS_RESUME IN ("COMPLETED") THEN 1 ELSE 0 END) as pi_pagi_completed
		FROM comparin_data_api a
		LEFT JOIN maintenance_datel b On a.sto = b.sto
		WHERE
			a.type_grab = "PI_PAGI"
		GROUP BY b.sektor_prov
		ORDER BY jumlah_pi_pagi DESC
		');
		return $query;
	}

	public static function pi_pagi_sisa(){
		$query = DB::SELECT('
			SELECT
			d.laporan_status,
			c.status_kendala,
			count(*) as jumlah
			FROM
			comparin_data_api a
			LEFT JOIN dispatch_teknisi b ON a.ORDER_ID = b.NO_ORDER
			LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
			LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
			WHERE
			a.STATUS_RESUME IN ("OSS PROVISIONING ISSUED") AND
			a.type_grab = "PI_PAGI"
			GROUP BY c.status_laporan
			ORDER BY jumlah DESC
		');
		return $query;
	}

	public static function comparinListNew($type,$status,$sektor,$tgl){
		$where_type = '';
		if ($type<>"ALL"){
			$where_type = 'AND a.type_grab = "'.$type.'"';
		}
		$where_status = '';
		if ($status<>"ALL"){
			switch ($status) {
				case "COMPLETED" :
					$where_status = 'AND a.STATUS_RESUME IN ("COMPLETED")';
				break;
				case "LAINNYA" :
					$where_status = 'AND a.STATUS_RESUME NOT IN ("COMPLETED","OSS PROVISIONING ISSUED")';
				break;
				case "SISA" :
					$where_status = 'AND a.STATUS_RESUME IN ("OSS PROVISIONING ISSUED")';
				break;
			}
		}
		$where_sektor = '';
		if ($sektor<>"ALL"){
			$where_sektor = 'AND e.sektor_prov = "'.$sektor.'"';
		}
		$query = DB::SELECT('
			SELECT
			*
			FROM
			comparin_data_api a
			LEFT JOIN dispatch_teknisi b ON a.ORDER_ID = b.NO_ORDER
			LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
			LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
			LEFT JOIN maintenance_datel e ON a.sto =e.sto
			LEFT JOIN regu f ON b.id_regu = f.id_regu
			LEFT JOIN Data_Pelanggan_Starclick g ON b.NO_ORDER = g.orderIdInteger
			WHERE
				1
				'.$where_type.'
				'.$where_status.'
				'.$where_sektor.'
		');
		return $query;
	}


	public static function jumlah_teknisi($tgl){
		$query = DB::SELECT('
    SELECT
		a.id_regu,
		b.uraian,
		COUNT(*) as jumlah
		FROM dispatch_teknisi a
		LEFT JOIN regu b ON a.id_regu = b.id_regu
		LEFT JOIN Data_Pelanggan_Starclick dps on a.NO_ORDER = dps.orderIdInteger
		WHERE
    dps.jenisPsb LIKE "AO%" AND
		a.jenis_layanan <> "QC" AND
		(date(dps.orderDatePs) = "'.$tgl.'" OR date(dps.orderDatePs) = "0000-00-00 00:00:00" OR dps.orderDatePs is null) AND
		a.tgl = "'.$tgl.'" AND a.jenis_order = "SC" group by a.id_regu order by jumlah DESC');
		return $query;
	}

	public static function jumlah_ps($tgl)
  {
		return DB::select(' SELECT * FROM Data_Pelanggan_Starclick WHERE (DATE(orderDatePs) = "' . $tgl . '") AND orderStatus = "COMPLETED" AND jenisPsb LIKE "AO%" ');
	}

	public static function dashboardSales($date,$spv){
		$where_spv = '';
		if ($spv<>"all"){
			$where_spv = ' AND f.ID_SPV = "'.$spv.'"';
		}
		$get_psb_laporan_status = DB::SELECT('SELECT * FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV")  AND stts_dash <> "progress" order by stts_dash');
		$SQL = '
        SELECT
        aa.sales_id as SALESnya,
        d.uraian,
        count(*) as jumlah,';
				foreach ($get_psb_laporan_status as $psb_laporan_status) {
		$SQL .= '
			SUM(CASE WHEN c.laporan_status_id = "'.$psb_laporan_status->laporan_status_id.'" THEN 1 ELSE 0 END) as jumlah_'.$psb_laporan_status->laporan_status_id.',
        ';
			}
		$SQL .='
				count(*) as KENDALA,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM
        	psb_myir_wo aa
				LEFT JOIN Data_Pelanggan_Starclick bb ON aa.myir = bb.myir
        LEFT JOIN dispatch_teknisi a ON aa.myir = a.Ndem
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
				LEFT JOIN sales_agency f ON aa.sales_id = f.KODE_SALES
        WHERE
          aa.orderDate LIKE "'.$date.'%" AND
					c.stts_dash NOT IN ("progress") AND
          aa.sales_id <> ""
					'.$where_spv.'
        GROUP BY
          aa.sales_id
        ORDER BY jumlah DESC
      ';
    $query = DB::SELECT($SQL);
		return $query;
	}

	public static function dashboardSalesBySPV($date){
		$get_psb_laporan_status = DB::SELECT('SELECT * FROM psb_laporan_status WHERE jenis_order IN ("ALL","PROV")  AND stts_dash <> "progress" order by stts_dash');
		$SQL = '
        SELECT
				f.ID_SPV,
				f.SPV_ULTIMATE as NAMA_SPV,
        aa.sales_id as SALESnya,
        d.uraian,
        count(*) as jumlah,';
				foreach ($get_psb_laporan_status as $psb_laporan_status) {
		$SQL .= '
			SUM(CASE WHEN c.laporan_status_id = "'.$psb_laporan_status->laporan_status_id.'" THEN 1 ELSE 0 END) as jumlah_'.$psb_laporan_status->laporan_status_id.',
        ';
			}
		$SQL .='
        count(*) as TOTAL_ORDER,
				SUM(CASE WHEN c.grup IN ("KP","KT") THEN 1 ELSE 0 END) as KENDALA,
        SUM(CASE WHEN ccc.grup IN ("UP") THEN 1 ELSE 0 END) as UP,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem
        FROM
        	psb_myir_wo aa
				LEFT JOIN Data_Pelanggan_Starclick bb ON aa.myir = bb.myir
        LEFT JOIN dispatch_teknisi a ON aa.myir = a.Ndem
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
				LEFT JOIN sales_agency f ON aa.sales_id = f.KODE_SALES
        LEFT JOIN dispatch_teknisi aaa On aa.sc = aaa.Ndem
        LEFT JOIN psb_laporan bbb ON aaa.id = bbb.id_tbl_mj
        LEFT JOIN psb_laporan_status ccc ON bbb.status_laporan = ccc.laporan_status_id
        WHERE
          aa.orderDate LIKE "'.$date.'%" AND
          aa.sales_id <> ""AND
					f.ID_SPV <> ""
        GROUP BY
          f.ID_SPV
        ORDER BY jumlah DESC
      ';
    $query = DB::SELECT($SQL);
		return $query;
	}

	public static function dashboardSalesList($status,$date,$spv,$sales){
		$where_spv = '';
		if ($spv<>"all"){
			$where_spv = ' AND f.ID_SPV = "'.$spv.'"';
		}
		$where_status = '';
		if ($status<>"all"){
			$where_status = ' AND c.laporan_status = "'.$status.'"';
		}
		$where_sales = '';
		if ($sales <> "all"){
			$where_sales = ' AND aa.sales_id = "'.$sales.'"';
		}
		$SQL = '
        SELECT
				*,
        aa.sales_id as SALESnya,
        d.uraian,
        e.title,
        e.sektor,
        a.id_regu,
        a.Ndem,
				bb.kcontact as akcontack,
				jl.jenis_layanan as jenisLayanan,
				b.modified_at as ls,
				aa.alamatLengkap as alamat,
				bb.orderAddr as alamat1
        FROM
        	psb_myir_wo aa
				LEFT JOIN Data_Pelanggan_Starclick bb ON aa.myir = bb.myir
        LEFT JOIN dispatch_teknisi a ON aa.myir = a.Ndem
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.laporan_status_id
        LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
        LEFT JOIN regu d ON a.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
				LEFT JOIN sales_agency f ON aa.sales_id = f.KODE_SALES
				LEFT JOIN maintenance_datel g ON SUBSTR(aa.namaOdp,5,3) = g.sto
        WHERE
          aa.orderDate LIKE "'.$date.'%" AND
					c.stts_dash NOT IN ("progress")
          '.$where_sales.'
					'.$where_spv.'
					'.$where_status.'
        ORDER BY aa.orderDate DESC
      ';
    $query = DB::SELECT($SQL);
		return $query;
	}

  public static function unspec(){
    $query = DB::SELECT('
        SELECT a.Workzone,count(*) as Jumlah FROM data_nossa_1 a WHERE a.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" group by a.Workzone order by Jumlah DESC
      ');
    return $query;
  }

  public static function mon_nossa($date){
    $query = DB::SELECT('
    SELECT
    gt.title as sektor,
    COUNT(dt.Ndem) as jumlah
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN maintenance_datel md ON SUBSTR(dn1l.Datek,13,3) = md.sto
    WHERE
    pl.status_laporan <> "1" AND
    (DATE(dt.tgl) LIKE "'.$date.'%") AND
    dn1l.Hasil_Ukur IN ("CLOSED","MEDIACARE","SALAMSIMPATIK","FINALCHECK")
    GROUP BY sektor
    ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function unspec_prov($date){
    $query = DB::SELECT('
SELECT
gt.title as SEKTOR,
gt.sektorx as STO,
COUNT(dt.Ndem) as JUMLAH
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
WHERE
dt.Ndem NOT LIKE "IN%" AND
(pl.redaman_iboster BETWEEN "-25" AND "-50") AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
GROUP BY SEKTOR
ORDER BY JUMLAH DESC
      ');
    return $query;
  }

  public static function unspec_prov_null($date){
    $query = DB::SELECT('
SELECT
gt.title as SEKTOR,
gt.sektorx as STO,
COUNT(dt.Ndem) as JUMLAH
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
WHERE
dt.Ndem NOT LIKE "IN%" AND
pl.redaman_iboster IS NULL AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
GROUP BY SEKTOR
ORDER BY JUMLAH DESC
      ');
    return $query;
  }

  public static function unspec_asr($date){
    $query = DB::SELECT('
SELECT
gt.title as SEKTOR,
gt.ioan as STO,
COUNT(dt.Ndem) as JUMLAH
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
WHERE
dt.Ndem LIKE "IN%" AND
(pl.redaman_iboster BETWEEN "-25" AND "-50") AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
GROUP BY SEKTOR
ORDER BY JUMLAH DESC
      ');
    return $query;
  }

  public static function unspec_asr_null($date){
    $query = DB::SELECT('
SELECT
gt.title as SEKTOR,
gt.ioan as STO,
COUNT(dt.Ndem) as JUMLAH
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
WHERE
dt.Ndem LIKE "IN%" AND
pl.redaman_iboster IS NULL AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
GROUP BY SEKTOR
ORDER BY JUMLAH DESC
      ');
    return $query;
  }

  public static function unspeclist_prov($sektor,$date){
    $where_sektor = '';
    if ($sektor<>"ALL"){
      $where_sektor = ' AND gt.title = "'.$sektor.'" ';
    }
    $query = DB::SELECT('
SELECT
*,
dt.id as dt_id,
pmw.layanan as myir_layanan
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
WHERE
dt.Ndem NOT LIKE "IN%" AND
(pl.redaman_iboster BETWEEN "-25" AND "-50") AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
'.$where_sektor.'
ORDER BY pl.modified_at
      ');
    return $query;
  }

  public static function unspeclist_prov_null($sektor,$date){
    $where_sektor = '';
    if ($sektor<>"ALL"){
      $where_sektor = ' AND gt.title = "'.$sektor.'" ';
    }
    $query = DB::SELECT('
SELECT
*,
dt.id as dt_id,
pmw.layanan as myir_layanan
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
WHERE
dt.Ndem NOT LIKE "IN%" AND
pl.redaman_iboster IS NULL AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
'.$where_sektor.'
ORDER BY pl.modified_at
      ');
    return $query;
  }

  public static function unspeclist_asr($sektor,$date){
    $where_sektor = '';
    if ($sektor<>"ALL"){
      $where_sektor = ' AND gt.title = "'.$sektor.'"';
    }
    $query = DB::SELECT('
SELECT
*,
dt.id as dt_id,
pla.action as Action
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
WHERE
dt.Ndem LIKE "IN%" AND
(pl.redaman_iboster BETWEEN "-25" AND "-50") AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
'.$where_sektor.'
ORDER BY pl.modified_at
      ');
    return $query;
  }

  public static function unspeclist_asr_null($sektor,$date){
    $where_sektor = '';
    if ($sektor<>"ALL"){
      $where_sektor = ' AND gt.title = "'.$sektor.'"';
    }
    $query = DB::SELECT('
SELECT
*,
dt.id as dt_id,
pla.action as Action
FROM dispatch_teknisi dt
LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
LEFT JOIN regu r ON dt.id_regu = r.id_regu
LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
WHERE
dt.Ndem LIKE "IN%" AND
pl.redaman_iboster IS NULL AND
pl.status_laporan = "1" AND
pl.modified_at LIKE "'.$date.'%"
'.$where_sektor.'
ORDER BY pl.modified_at
      ');
    return $query;
  }

  public static function kehadiran($date){
    $query = DB::SELECT('
        SELECT
        *,
        SUM(CASE WHEN (e.squad="ALPHA.ASR" OR e.squad="ALPHA.WILSUS") THEN 1 ELSE 0 END) as alphaasr,
        count(*) as jumlah
        FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON c.nik = d.nik
      LEFT JOIN employee_squad e ON c.squad = e.squad_id
        WHERE
          a.ACTIVE <> 0 AND
          DATE(d.tglAbsen) = "'.$date.'"
        GROUP BY b.title
      ');
    return $query;
  }

  public static function kehadiran2($date){
    $query = DB::SELECT('
        SELECT
        *,
        count(*) as jumlah
        FROM
        1_2_employee c
      LEFT JOIN regu a ON (a.nik1 = c.nik OR a.nik2 = c.nik)
      LEFT JOIN group_telegram b ON a.mainsector = b.chat_id
      LEFT JOIN absen d ON c.nik = d.nik
        WHERE
          a.ACTIVE <> 0 AND
          DATE(d.tglAbsen) = "'.$date.'" AND
          b.ket_sektor = 1
        GROUP BY b.title
      ');
    return $query;
  }

  public static function detailJumlahAbsen($sektor, $date){
      $sql = 'SELECT
                SUM(CASE WHEN (a.approval=1 and b.sttsWo=0) THEN 1 ELSE 0 END) as approveAsr,
                SUM(CASE WHEN (a.approval=3 and b.sttsWo=0) THEN 1 ELSE 0 END) as declineAsr,
                SUM(CASE WHEN (a.approval=1 and b.sttsWo=1) THEN 1 ELSE 0 END) as approveProv,
                SUM(CASE WHEN (a.approval=3 and b.sttsWo=1) THEN 1 ELSE 0 END) as declineProv,
                SUM(CASE WHEN b.sttsWo=0 THEN 1 ELSE 0 END) as requestAsr,
                SUM(CASE WHEN b.sttsWo=1 THEN 1 ELSE 0 END) as requestProv
              FROM
                absen a
                LEFT JOIN regu b ON (b.nik1 = a.nik OR b.nik2 = a.nik)
              WHERE
                b.ACTIVE <> 0 AND
                b.mainsector="'.$sektor.'" AND
                DATE(a.date_created)="'.$date.'"';

      return DB::select($sql);
  }

  public static function unspeclist($sto){
    $where_sto = '';
    if ($sto<>"ALL"){
      $where_sto = ' AND a.Workzone = "'.$sto.'"';
    }
    $query = DB::SELECT('
        SELECT
        a.*
        FROM
          data_nossa_1 a
        WHERE
          a.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC"
        '.$where_sto.'
      ');
    return $query;
  }

  public static function panjangtarikan($tgl){

    $query = DB::SELECT('
      SELECT
        *,
        SUM( i.num ) AS dc,
        dt.id as id_dt
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN roc ON dt.Ndem = roc.no_tiket
       LEFT JOIN Data_Pelanggan_Starclick psb ON dt.Ndem = psb.orderId
       LEFT JOIN mdf m ON roc.sto_trim = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = 1
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function get_assuranceStatusOpen($witel){
    if ($witel=="ALL"){
      $where_witel = "";
    } else {
      $where_witel = ' AND a.Witel = "'.$witel.'"';
    }
    $query = DB::SELECT('
      SELECT
        d.laporan_status,
        count(*) as jumlah
      FROM
        data_nossa_1 a
        LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      WHERE
      a.Source <> "PROACTIVE_TICKET" AND
      a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
      a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" AND
      a.Induk_Gamas = ""
      '.$where_witel.'
      GROUP BY d.laporan_status
    ');
    return $query;
  }

  public static function by_actual_solution($field,$periode,$mitrax){
     if ($mitrax=="ALL"){
       $where_mitra = '';
     } else {
       $where_mitra = ' AND c.mitra="'.$mitrax.'"';
     }
     $query = DB::SELECT('
     SELECT
       a.'.$field.' as ACTUAL_SOLUTION,
       count(*) as JUMLAH
     FROM
       nonatero_detil a
     LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
     LEFT JOIN regu c ON b.id_regu = c.id_regu
     WHERE a.THNBLN="'.$periode.'" '.$where_mitra.' GROUP BY a.'.$field);
     return $query;
   }


    public static function get_last_sync_nossa(){
      return DB::SELECT('select UpdatedDatetime from data_nossa_1 limit 1');
    }

        public static function nonatero_group_by($par,$periode,$mitrax){
          if ($mitrax == "ALL"){
            $q_mitra = '';
          } else {
            $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
          }

          $query = DB::SELECT('
            SELECT
              '.$par.' as ACTUAL_SOLUTION,
              count(*) as JUMLAH
            FROM
              nonatero_detil a
            LEFT JOIN nonatero_detil_actual_solution b ON a.ACTUAL_SOLUTION = b.ACTUAL_SOLUTION
            LEFT JOIN dispatch_teknisi dt ON a.TROUBLE_NO = dt.Ndem
            LEFT JOIN regu d ON dt.id_regu = d.id_regu
            WHERE
              a.THNBLN = "'.$periode.'" AND
              a.IS_GAMAS = 0
              '.$q_mitra.'
            GROUP BY '.$par.'
            ORDER BY JUMLAH ASC
          ');
          return $query;
        }

        public static function getNossa($channel,$witel,$org){
        $q_channel = '';
        if ($channel<>"ALL"){
          $q_channel = ' AND a.Channel = "'.$channel.'"';
        }
        $q_witel = '';
        if ($witel<>"ALL"){
          $q_witel = ' AND a.Witel = "'.$witel.'"';
        }

        $query = DB::SELECT('
          SELECT
            '.$org.' as ORG,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,"'.date('Y-m-d H:i:s').'")<0   THEN 1 ELSE 0 END) as HOLD,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<1 THEN 1 ELSE 0 END) as x1jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=1 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x2jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<3 THEN 1 ELSE 0 END) as x3jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x3dan4jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x0dan2jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x2dan4jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<6 THEN 1 ELSE 0 END) as x4dan6jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x6dan8jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<10 THEN 1 ELSE 0 END) as x8dan10jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as x10dan12jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12 THEN 1 ELSE 0 END) as x12jam,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as xkurang12jam,
            count(*) as Jumlah
          FROM
            data_nossa_1 a
            LEFT JOIN mdf b ON a.Workzone = b.mdf
          WHERE
            a.Witel <> "" AND
            a.Source <> "PROACTIVE_TICKET" AND
            a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
            a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" AND
            a.Induk_Gamas = ""
            '.$q_channel.'
            '.$q_witel.'
          GROUP BY
            '.$org.'
        ');
        return $query;
      }

      public static function getNossaList($channel,$periode,$witel){


        $q_channel = '';
        if ($channel<>"ALL"){
          $q_channel = ' AND a.Channel = "'.$channel.'"';
        }

        if ($witel == "ALL"){
          $q_witel = '';
        } else {
          $q_witel = 'AND (a.Witel = "'.$witel.'" OR h.sektor_asr = "'.$witel.'")';
        }

        switch ($periode) {
          case 'x0dan2jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2';
          break;

          case 'x1jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<1';
          break;

          case 'x2jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=1 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<2';
          break;

          case 'x3jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<3';
          break;
          case 'x3dan4jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4';
          break;

          case 'x2dan4jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<4';
          break;
          case 'x4dan6jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<6';
          break;
          case 'x6dan8jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<8';
          break;
          case 'x8dan10jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR ,a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<10';
          break;
          case 'x10dan12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12';
          break;
          case 'x12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12';
          break;
          case 'xkurang12jam':
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12';
          break;
          case 'HOLD':
            //$q_periode = 'AND (UmurDatetime BETWEEN "'.date('Y-m-d 00:00:00').'" AND "'.date('Y-m-d 08:00:05').'") OR a.UmurDatetime>="'.date('Y-m-d 17:00:00').'"';
            $q_periode = 'AND TIMESTAMPDIFF( HOUR , a.UmurDatetime,"'.date('Y-m-d H:i:s').'")<0';
          break;

          default:
              $q_periode = '';
            break;
        }

        $query = DB::SELECT('
          SELECT
            a.*,
            TIMESTAMPDIFF(HOUR,a.UmurDatetime,"'.date('Y-m-d H:i:s').'") as Umur,
            c.uraian as Team,
            g.laporan_status as Status,
            e.action as Action,
            f.penyebab as Sebab
          FROM
            data_nossa_1 a
            LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
            LEFT JOIN regu c ON b.id_regu = c.id_regu
            LEFT JOIN psb_laporan d ON b.id = d.id_tbl_mj
            LEFT JOIN psb_laporan_action e ON d.action = e.laporan_action_id
            LEFT JOIN psb_laporan_penyebab f ON f.idPenyebab = d.penyebabId
            LEFT JOIN psb_laporan_status g ON d.status_laporan = g.laporan_status_id
            LEFT JOIN mdf h ON a.Workzone = h.mdf
          WHERE
            a.Source <> "PROACTIVE_TICKET" AND
            a.Incident_Symptom NOT LIKE "%NON NUMBERING%" AND
            a.Customer_Name <> "ASSET DUMMY SEGMENT DCS" and
            a.Induk_Gamas = ""
            '.$q_witel.'
            '.$q_periode.'
            '.$q_channel.'
          ');
        return $query;
      }

      public static function PI($transaksi,$dateStart,$dateEnd){
        switch ($transaksi){
          case "NEWSALES" :
            $q_transaksi = ' AND a.JENISPSB LIKE ("%AO%") ';
          break;
          case "2ND_STB" :
            $q_transaksi = ' AND (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%")';
          break;
          case "MO" :
            $q_transaksi = ' AND (a.PACKAGE_NAME NOT LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME NOT LIKE "%2nd STB%" OR a.PACKAGE_NAME NOT LIKE "%Second%") AND a.JENISPSB LIKE "MO%"';
          break;
          default :
            $q_transaksi = '';
          break;
        }
        $query = 'SELECT count(*) as jumlah,';
        for ($i=0;$i<24;$i++) :
          $query .= 'SUM(CASE WHEN HOUR(a.ORDER_DATE)='.$i.' THEN 1 ELSE 0 END) as h'.$i.',';
        endfor;
        $query .= 'a.WITEL FROM 4_0_master_order_1_log_pi a
        WHERE
          date(a.ORDER_DATE) BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'"
          '.$q_transaksi.'
        GROUP BY a.WITEL';
        $result = DB::connection('mysql4')->SELECT($query);
        return $result;
      }

      public static function PIlist($witel,$jam,$dateStart,$dateEnd){
        if ($witel=="ALL"){
          $q_witel = '';
        } else {
          $q_witel = 'AND WITEL = "'.$witel.'"';
        }
        if ($jam=="ALL"){
          $q_jam = '';
        } else {
          $q_jam = 'AND HOUR(ORDER_DATE)='.$jam;
        }
        $query = DB::connection('mysql4')->SELECT('SELECT * FROM 4_0_master_order_1_log_pi WHERE date(ORDER_DATE) BETWEEN "'.$dateStart.'" AND "'.$dateEnd.'" '.$q_witel.' '.$q_jam.' ');
        return $query;
      }

      public static function kpi_assurance_mitra($periode){
        $query = DB::SELECT('SELECT c.mitra_main as mitra FROM nonatero_detil a LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.NDEM LEFT JOIN regu c ON b.id_regu = c.id_regu WHERE a.THNBLN="'.$periode.'" GROUP BY c.mitra_main');
        return $query;
      }

      public static function kpi_assurance_mitrax($periode,$mitrax){
        if ($mitrax == "ALL"){
          $q_mitra = '';
        } else {
          $q_mitra = ' AND d.mitra_main = "'.$mitrax.'"';
        }

        $query = DB::SELECT('
          SELECT
            count(*) as JUMLAH,
            SUM(a.IS_12HS) as R1DS,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
            (SELECT COUNT(*) FROM nonatero_detil_gaul aa LEFT JOIN dispatch_teknisi bb ON aa.TROUBLE_NO = bb.Ndem LEFT JOIN regu cc ON bb.id_regu = cc.id_regu LEFT JOIN psb_laporan dd ON bb.id = dd.id_tbl_mj WHERE dd.status_laporan = 1 AND cc.mitra_main = d.mitra_main AND aa.THNBLN = "'.$periode.'") as GAUL,
            d.mitra_main as sektor_asr
          FROM
            mdf b
          LEFT JOIN nonatero_detil a ON b.mdf = a.STO
          LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
          LEFT JOIN regu d ON c.id_regu = d.id_regu
          LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
          WHERE
            a.THNBLN LIKE "'.$periode.'%" AND
            a.IS_GAMAS = 0 AND
            e.status_laporan = 1
            '.$q_mitra.'
          GROUP BY
            d.mitra_main
        ');
        return $query;
      }
      public static function kpi_assurance_mitra_tech($periode,$mitrax){
        if ($mitrax == "ALL"){
          $q_mitra = '';
        } else {
          $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
        }

        $query = DB::SELECT('
          SELECT
            count(*) as JUMLAH,
            SUM(a.IS_12HS) as R1DS,
            SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
            (SELECT COUNT(*) FROM nonatero_detil_gaul aa LEFT JOIN dispatch_teknisi bb ON aa.TROUBLE_NO = bb.Ndem LEFT JOIN regu cc ON bb.id_regu = cc.id_regu WHERE cc.crewid = d.crewid AND aa.THNBLN LIKE "'.$periode.'%") as GAUL,
            d.crewid as sektor_asr
          FROM
            mdf b
          LEFT JOIN nonatero_detil a ON b.mdf = a.STO
          LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
          LEFT JOIN regu d ON c.id_regu = d.id_regu
          WHERE
            a.THNBLN LIKE "'.$periode.'%"
            '.$q_mitra.'
          GROUP BY
            d.crewid
        ');
        return $query;
      }
    public static function kpi_assurance_periode(){
      $query = DB::SELECT('SELECT THNBLN FROM nonatero_detil GROUP BY THNBLN');
      return $query;
    }

    // public static function kpi_assurance_mitra($periode){
    //   $query = DB::SELECT('SELECT c.mitra FROM nonatero_detil a LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.NDEM LEFT JOIN regu c ON b.id_regu = c.id_regu WHERE a.THNBLN="'.$periode.'" GROUP BY c.mitra');
    //   return $query;
    // }

    public static function get_last_update_kpi_assurance(){
      $query = DB::SELECT('select UPDATE_TIME from nonatero_detil ORDER BY UPDATE_TIME DESC limit 1 ');
      return $query;
    }

    public static function kpi_assuranceListGaul($sektor,$periode){
      if ($sektor=="ALL"){
        $q_sektor = '';
      } else {
        $q_sektor = 'AND b.sektor_asr = "'.$sektor.'"';
      }
      $query = DB::SELECT('
        SELECT
          a.*,
          d.uraian,
          f.action,
          g.penyebab as sebab,
          d.mitra,
          c.id
        FROM
          nonatero_detil_gaul a
        LEFT JOIN mdf b ON a.CMDF = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
        LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
        LEFT JOIN psb_laporan_penyebab g ON g.idPenyebab = e.penyebabId
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_sektor.'
      ');
      return $query;
    }

    public static function kpi_assuranceList($tipe,$sektor,$periode){
      if ($sektor=="ALL"){
        $q_sektor = '';
      } else {
        $q_sektor = 'AND b.sektor_asr = "'.$sektor.'"';
      }

      switch ($tipe) {
        case 'NC48':
            $q_tipe = 'AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)>=48';
          break;
        case 'NC1DS':
            $q_tipe = 'AND a.IS_12HS = 0';
          break;
        case 'REAL48':
            $q_tipe = 'AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48';
          break;
        case 'REAL1DS':
            $q_tipe = 'AND a.IS_12HS = 1';
          break;
        default:
            $q_tipe = '';
          break;
      }

      $query = DB::SELECT('
        SELECT
        a.*,
        d.uraian,
        f.action,
        g.penyebab as sebab,
        d.mitra,
        c.id
        FROM
          nonatero_detil a
        LEFT JOIN mdf b ON a.STO = b.mdf
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        LEFT JOIN psb_laporan e ON c.id = e.id_tbl_mj
        LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
        LEFT JOIN psb_laporan_penyebab g ON g.idPenyebab = e.penyebabId
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_sektor.'
          '.$q_tipe.'
      ');
      return $query;
    }

    public static function kpi_assurance($periode,$mitrax){

      if ($mitrax == "ALL"){
        $q_mitra = '';
      } else {
        $q_mitra = ' AND d.mitra = "'.$mitrax.'"';
      }

      $query = DB::SELECT('
        SELECT
          count(*) as JUMLAH,
          SUM(a.IS_12HS) as R1DS,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  a.TGL_TECH_CLOSE)<48 THEN 1 ELSE 0 END) as R48,
          (SELECT COUNT(*) FROM nonatero_detil_gaul aa WHERE aa.CMDF = b.mdf AND aa.THNBLN = "'.$periode.'") as GAUL,
          b.sektor_asr
        FROM
          mdf b
        LEFT JOIN nonatero_detil a ON b.mdf = a.STO
        LEFT JOIN dispatch_teknisi c ON a.TROUBLE_NO = c.Ndem
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        WHERE
          a.THNBLN = "'.$periode.'"
          '.$q_mitra.'
        GROUP BY
          b.sektor_asr
      ');
      return $query;
    }



  public static function provisioning_wallboard_byjam(){

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<2 THEN 1 ELSE 0 END) as x0dan2jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as x2dan4jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<6 THEN 1 ELSE 0 END) as x4dan6jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x6dan8jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<10 THEN 1 ELSE 0 END) as x8dan10jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as x10dan12jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=12 THEN 1 ELSE 0 END) as x12jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<8 THEN 1 ELSE 0 END) as x8jam,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 THEN 1 ELSE 0 END) as xlebih8jam
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND a.JENISPSB LIKE ("%AO%") GROUP BY a.WITEL');
    return $query;
  }
  public static function provisioning_wallboard_byhari(){

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as hari1,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as hari2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72 THEN 1 ELSE 0 END) as hari3,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96 THEN 1 ELSE 0 END) as hari4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as hari7,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360 THEN 1 ELSE 0 END) as hari15,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720 THEN 1 ELSE 0 END) as hari30,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720 THEN 1 ELSE 0 END) as lebih30hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as kurang1hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as lebih1hari
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND a.JENISPSB LIKE ("%AO%") GROUP BY a.WITEL');
    return $query;
  }
  public static function provisioning2_wallboard_byhari(){

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        count(*) as jumlah,
        a.WITEL,
        TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" ) as umur,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as hari1,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as hari2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72 THEN 1 ELSE 0 END) as hari3,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96 THEN 1 ELSE 0 END) as hari4,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as hari7,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360 THEN 1 ELSE 0 END) as hari15,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720 THEN 1 ELSE 0 END) as hari30,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720 THEN 1 ELSE 0 END) as lebih30hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as kurang1hari,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as lebih1hari
      FROM 4_0_master_order_1 a WHERE a.WITEL IN ("KALSEL","KALBAR","KALTENG") AND a.ORDER_STATUS = 16 AND (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%") GROUP BY a.WITEL');
    return $query;
  }

  public static function provisioning_wallboard_list($id,$witel){

    switch ($id) {
      case '0_2jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<2';
      break;
      case '2_4jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=2 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<4';
      break;
      case '4_6jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<6';
      break;
      case '6_8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=6 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<8';
      break;
      case '8_10jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=8 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<10';
      break;
      case '10_12jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=10 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<12';
      break;
      case '12jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")>=12';
      break;
      case '8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")<8';
      break;
      case 'lebih8jam':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'")>=8';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        a.JENISPSB LIKE ("%AO%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }


  public static function provisioning_wallboard_list_by_hari($id,$witel){

    switch ($id) {
      case 'hari1':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'hari2':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48';
      break;
      case 'hari3':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72';
      break;
      case 'hari4':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96';
      break;
      case 'hari7':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168';
      break;
      case 'hari15':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360';
      break;
      case 'hari30':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720';
      break;
      case 'lebih30hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720';
      break;
      case 'kurang1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'lebih1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        a.JENISPSB LIKE ("%AO%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }

  public static function provisioning2_wallboard_list_by_hari($id,$witel){

    switch ($id) {
      case 'hari1':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'hari2':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<48';
      break;
      case 'hari3':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<72';
      break;
      case 'hari4':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=72 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<96';
      break;
      case 'hari7':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=96 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<168';
      break;
      case 'hari15':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=168 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<360';
      break;
      case 'hari30':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=360 AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<720';
      break;
      case 'lebih30hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=720';
      break;
      case 'kurang1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )<24';
      break;
      case 'lebih1hari':
          $whereID = 'AND TIMESTAMPDIFF( HOUR , a.ORDER_DATE,  "'.date('Y-m-d H:i:s').'" )>=24';
      break;
      default:
          $whereID = '';
        break;
    }

    if ($witel=="ALL"){
      $whereWITEL = 'AND a.WITEL IN ("KALSEL","KALBAR","KALTENG")';
    } else {
      $whereWITEL = 'AND a.WITEL = "'.$witel.'"';
    }

    $query = DB::connection('mysql4')->SELECT('
      SELECT
        *
      FROM
        4_0_master_order_1 a
      WHERE
        (a.PACKAGE_NAME LIKE "%SWSTBHYBR2%" OR a.PACKAGE_NAME LIKE "%2nd STB%" OR a.PACKAGE_NAME LIKE "%Second%") AND
        a.ORDER_STATUS = 16
        '.$whereID.'
        '.$whereWITEL.'
    ');
    return $query;
  }

  public static function listbyumur($type,$sektor,$durasi,$channel)
  {

    $date_now = date('Y-m-d H:i:s');

    switch ($type) {
      case 'sektor':
        switch ($sektor) {
          case 'ALL':
            $sektor_q = '';
            break;
          case 'UNDEFINED':
            $sektor_q = 'AND f.title is NULL ';
            break;
          default:
            $sektor_q = 'AND f.title = "' . $sektor . '"';
            break;
        }
        break;
      case 'hero':
        switch ($sektor) {
          case 'ALL':
            $sektor_q = '';
            break;
          case 'UNDEFINED':
            $sektor_q = 'AND md.HERO is NULL ';
            break;
          default:
            $sektor_q = 'AND md.HERO = "' . $sektor . '"';
            break;
        }
        break;
    }

    switch ($channel) {
      case 'ALL':
        $channel_q = '';
        break;
      default:
        $channel_q = 'AND a.channel = "' . $channel . '"';
        break;
    }

    switch ($durasi) {
      case "lebih24" :
        $durasi_q = 'AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=24';
      break;
      case "a0dan2" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=0 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 2)';
      break;
      case "a2dan4" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=2 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 4)';
      break;
      case "a4dan6" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 6)';
      break;
      case "a6dan8" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dnll.Reported_Datex,  "'.$date_now.'") >=4 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 6)';
      break;
      case "a8dan10" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=8 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 10)';
      break;
      case "a10dan12" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=10 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 12)';
      break;
      case "a12dan24" :
        $durasi_q = 'AND (TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") >=12 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 24)';
      break;
      case "kurang24" :
        $durasi_q = 'AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "'.$date_now.'") < 24';
      break;
      case "ALL" :
        $durasi_q = '';
      break;
      default :
        $durasi_q = '';
      break;
    }

    $query = DB::SELECT('
      SELECT
      dn1l.*,
      dn1l.Incident as no_tiket,
      dn1l.Service_No as no_internet,
      dn1l.Service_ID,
      dn1l.Reported_Date as tgl_open,
      dn1l.Workzone as sto_trim,
      dn1l.Service_Type as channel,
      e.uraian as tim,
      c.id,
      c.updated_at,
      e.uraian,
      f.title as sektor,
      g.laporan_status,
      g.grup,
      h.action,
      d.catatan,
      i.penyebab
      FROM
        data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi c ON dn1l.ID = c.NO_ORDER
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN regu e ON c.id_regu = e.id_regu
      LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
      LEFT JOIN maintenance_datel md ON dn1l.Workzone = md.sto
      LEFT JOIN psb_laporan_status g ON d.status_laporan = g.laporan_status_id
      LEFT JOIN psb_laporan_action h ON d.action = h.laporan_action_id
      LEFT JOIN psb_laporan_penyebab i ON d.penyebabId = i.idPenyebab
      WHERE
      (DATE(dn1l.Reported_Datex) = "'.date('Y-m-d').'") AND
      dn1l.Incident IS NOT NULL AND
      dn1l.Source <> "TOMMAN" AND
      dn1l.Owner_Group = "TA HD WITEL KALSEL"
      '.$sektor_q.'
      '.$durasi_q.'
      '.$channel_q.'
    ');
    return $query;
  }

  public static function rock_active_by_umur(){

    $query = DB::SELECT('
      SELECT
      b.sektor_asr,
      count(*) as jumlah,
      SUM(CASE WHEN (a.hari) >=0.5 THEN 1 ELSE 0 END) as lebih12,
      SUM(CASE WHEN (a.hari) <0.5 THEN 1 ELSE 0 END) as kurang12,
      SUM(CASE WHEN (a.hari) BETWEEN 0 AND 0.083333 THEN 1 ELSE 0 END) as a0dan2,
      SUM(CASE WHEN (a.hari) BETWEEN 0.083333 AND 0.16667 THEN 1 ELSE 0 END) as a2dan4,
      SUM(CASE WHEN (a.hari) BETWEEN 0.16667 AND 0.25 THEN 1 ELSE 0 END) as a4dan6,
      SUM(CASE WHEN (a.hari)BETWEEN 0.25 AND 0.3334 THEN 1 ELSE 0 END) as a6dan8,
      SUM(CASE WHEN (a.hari) BETWEEN 0.3334 AND 0.416 THEN 1 ELSE 0 END) as a8dan10,
      SUM(CASE WHEN (a.hari) BETWEEN 0.416 AND 0.5 THEN 1 ELSE 0 END) as a10dan12,
      SUM(CASE WHEN (a.hari) BETWEEN 0.5 AND 1 THEN 1 ELSE 0 END) as a12dan24,
      SUM(CASE WHEN (a.hari) >= 1 THEN 1 ELSE 0 END) as lebih24,
      SUM(CASE WHEN (a.hari) < 1 THEN 1 ELSE 0 END) as kurang24
  FROM
        rock_excel_bank a
      LEFT JOIN mdf b ON a.sto = b.mdf
      WHERE a.is_gamas = 0
      GROUP BY b.sektor_asr
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function assuranceOpen($type)
  {
    $date_now = date('Y-m-d H:i:s');
    switch ($type) {
      case 'IN_SEKTOR':
        return DB::select('
          SELECT
          gt.title as sektor_asr,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=0 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 2 THEN 1 ELSE 0 END) as a0dan2,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=2 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 4 THEN 1 ELSE 0 END) as a2dan4,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=4 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 6 THEN 1 ELSE 0 END) as a4dan6,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=6 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 8 THEN 1 ELSE 0 END) as a6dan8,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=8 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 10 THEN 1 ELSE 0 END) as a8dan10,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=10 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 12 THEN 1 ELSE 0 END) as a10dan12,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=12 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 24 THEN 1 ELSE 0 END) as a12dan24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=24 THEN 1 ELSE 0 END) as lebih24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") <24 THEN 1 ELSE 0 END) as kurang24,
          count(*) as jumlah
          FROM data_nossa_1_log dn1l
          LEFT JOIN dispatch_teknisi dt ON dn1l.ID = dt.NO_ORDER
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          (DATE(dn1l.Reported_Datex) = "' . date('Y-m-d') . '") AND
          dn1l.Incident IS NOT NULL AND
          dn1l.Source <> "TOMMAN" AND
          dn1l.Owner_Group = "TA HD WITEL KALSEL"
          GROUP BY gt.title
        ');
        break;
      case 'INT_SEKTOR':
        return DB::select('
          SELECT 
          f.title as sektor_asr,
          count(*) as jumlah,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 2 THEN 1 ELSE 0 END) as x0dan2,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 4 THEN 1 ELSE 0 END) as x2dan4,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 6 THEN 1 ELSE 0 END) as x4dan6,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 8 THEN 1 ELSE 0 END) as x6dan8,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 10 THEN 1 ELSE 0 END) as x8dan10,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 12 THEN 1 ELSE 0 END) as x10dan12,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 24 THEN 1 ELSE 0 END) as x12dan24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=24 THEN 1 ELSE 0 END) as lebih24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") <24 THEN 1 ELSE 0 END) as kurang24
          FROM data_nossa_1_log a
          LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
          LEFT JOIN regu c ON b.id_regu = c.id_regu
          LEFT JOIN mdf d ON a.Workzone = d.mdf  
          LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
          LEFT JOIN group_telegram f ON c.mainsector = f.chat_id
          WHERE 
          a.Incident <> "" AND
          a.Source = "TOMMAN" AND
          (DATE(a.Reported_Date) = "' . date("Y-m-d") . '") AND 
          (e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
          GROUP BY f.title
        ');
        break;
      case 'IN_HERO':
        return DB::select('
          SELECT
          md.HERO as sektor_asr,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=0 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 2 THEN 1 ELSE 0 END) as a0dan2,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=2 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 4 THEN 1 ELSE 0 END) as a2dan4,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=4 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 6 THEN 1 ELSE 0 END) as a4dan6,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=6 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 8 THEN 1 ELSE 0 END) as a6dan8,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=8 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 10 THEN 1 ELSE 0 END) as a8dan10,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=10 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 12 THEN 1 ELSE 0 END) as a10dan12,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=12 AND TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") < 24 THEN 1 ELSE 0 END) as a12dan24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") >=24 THEN 1 ELSE 0 END) as lebih24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dn1l.Reported_Datex,  "' . $date_now . '") <24 THEN 1 ELSE 0 END) as kurang24,
          count(*) as jumlah
          FROM data_nossa_1_log dn1l
          LEFT JOIN dispatch_teknisi dt ON dn1l.ID = dt.NO_ORDER
          LEFT JOIN maintenance_datel md ON dn1l.Workzone = md.sto
          WHERE
          (DATE(dn1l.Reported_Datex) = "'.date('Y-m-d').'") AND
          dn1l.Incident IS NOT NULL AND
          dn1l.Source <> "TOMMAN" AND
          dn1l.Owner_Group = "TA HD WITEL KALSEL"
          GROUP BY md.HERO
        ');
        break;
      case 'INT_HERO':
        return DB::SELECT('
          SELECT 
          md.HERO as sektor_asr,
          count(*) as jumlah,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=0 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 2 THEN 1 ELSE 0 END) as x0dan2,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=2 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 4 THEN 1 ELSE 0 END) as x2dan4,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=4 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 6 THEN 1 ELSE 0 END) as x4dan6,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=6 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 8 THEN 1 ELSE 0 END) as x6dan8,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=8 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 10 THEN 1 ELSE 0 END) as x8dan10,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=10 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 12 THEN 1 ELSE 0 END) as x10dan12,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=12 AND TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") < 24 THEN 1 ELSE 0 END) as x12dan24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") >=24 THEN 1 ELSE 0 END) as lebih24,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.Reported_Date,  "' . $date_now . '") <24 THEN 1 ELSE 0 END) as kurang24
          FROM data_nossa_1_log a
          LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
          LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
          LEFT JOIN maintenance_datel md ON a.Workzone = md.sto
          WHERE 
            a.Incident <> "" AND
            a.Source = "TOMMAN" AND
            (DATE(a.Reported_Date) = "'.date("Y-m-d").'") AND 
            (e.status_laporan <> 1 OR e.id_tbl_mj is NULL)
          GROUP BY md.HERO
        ');
        break;
      default:
        # code...
        break;
    }
  }

  public static function rock_active_by_umur_myi(){

    $query = DB::SELECT('
      SELECT
      b.sektor_asr,
      count(*) as jumlah,
      SUM(CASE WHEN a.hari>=0.5 THEN 1 ELSE 0 END) as lebih12,
      SUM(CASE WHEN a.hari<0.5 THEN 1 ELSE 0 END) as kurang12,
      SUM(CASE WHEN a.hari BETWEEN 0 AND 0.083333 THEN 1 ELSE 0 END) as a0dan2,
      SUM(CASE WHEN a.hari BETWEEN 0.083333 AND 0.16667 THEN 1 ELSE 0 END) as a2dan4,
      SUM(CASE WHEN a.hari BETWEEN 0.16667 AND 0.25 THEN 1 ELSE 0 END) as a4dan6,
      SUM(CASE WHEN a.hari BETWEEN 0.25 AND 0.3334 THEN 1 ELSE 0 END) as a6dan8,
      SUM(CASE WHEN a.hari BETWEEN 0.3334 AND 0.416 THEN 1 ELSE 0 END) as a8dan10,
      SUM(CASE WHEN a.hari BETWEEN 0.416 AND 0.5 THEN 1 ELSE 0 END) as a10dan12,
      SUM(CASE WHEN a.hari BETWEEN 0.5 AND 1 THEN 1 ELSE 0 END) as a12dan24,
      SUM(CASE WHEN a.hari >= 1 THEN 1 ELSE 0 END) as lebih24,
      SUM(CASE WHEN a.hari < 1 THEN 1 ELSE 0 END) as kurang24
      FROM
        rock_excel_bank a
      LEFT JOIN mdf b ON a.sto = b.mdf
      Where
        a.channel = "MY INDIHOME" AND
        a.is_gamas = 0
      GROUP BY b.sektor_asr
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function umurTiket(){


    $query = DB::SELECT('
      SELECT
        a.*,
        TIMESTAMPDIFF( DAY , aaa.Reported_Date,  "'.date('Y-m-d H:i:s').'" ) as lamaHari,
        TIMESTAMPDIFF( HOUR , aaa.Reported_Date,  "'.date('Y-m-d H:i:s').'" ) as lamaJam,
        d.laporan_status,
        e.uraian,
        f.title,
        a.trouble_no as TROUBLE_NO,
        aaa.Summary as HEADLINE,
        aaa.Reported_Date as TROUBLE_OPENTIME,
        c.modified_at as tgl_update,
        c.created_at as tgl_created,
        aaa.Reported_Date as tglOpen
      FROM
        rock_excel a
      LEFT JOIN roc aa ON a.trouble_no = aa.no_tiket
      LEFT JOIN data_nossa_1_log aaa ON a.trouble_no = aaa.Incident
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN regu e ON b.id_regu = e.id_regu
      LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
      ORDER BY aaa.Reported_Date
    ');
    return $query;
  }

  public static function listTimMitra($jenis,$tglAwal,$tglAkhir){
    if ($jenis=="NONE"){
      $getWhere = 'AND ma.mitra_amija_pt IS NULL ';
    } else {
      $getWhere = ' AND ma.mitra_amija_pt = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        a.*,
        ma.mitra_amija_pt
      FROM
        regu a
        LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN Data_Pelanggan_Starclick d ON b.NO_ORDER = d.orderIdInteger
        LEFT JOIN mitra_amija ma ON a.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      b.Ndem NOT LIKE "IN%" AND
      (DATE(d.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'") AND
      b.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM")
      '.$getWhere.'
      GROUP BY a.uraian
    ');
    return $query;
  }

  public static function nonatero_active_by_pic(){
    $query = DB::SELECT('
      SELECT
        d.PIC,
        SUM(CASE WHEN b.troubleno_parent is not NULL AND d.PIC = "TA" THEN 1 ELSE 0 END) as isGamas_ta,
        SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_ndt_before,
        SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_dt_before,
        SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_before,
        SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isGamas_before,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as jumlah_before,
        SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_ndt_after,
        SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_dt_after,
        SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_after,
        SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isGamas_after,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as jumlah_after,
        count(*) as jumlah
      FROM  `nonatero_excel` a
      LEFT JOIN rock_gamas_opn b ON a.trouble_no = b.trouble_no
      LEFT JOIN dispatch_teknisi c ON a.trouble_no = c.Ndem
      LEFT JOIN loker_pic d ON a.LOKER_DISPATCH = d.LOKER
      WHERE
        a.LOKER_DISPATCH <> ""
      GROUP BY d.PIC
    ');
    return $query;
  }

  public static function nonatero_active(){
    $query = DB::SELECT('
    SELECT
      a.LOKER_DISPATCH,
      d.PIC,
      SUM(CASE WHEN b.troubleno_parent is not NULL AND d.PIC = "TA" THEN 1 ELSE 0 END) as isGamas_ta,
      SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_ndt_before,
      SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_dt_before,
      SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isReguler_before,
      SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as isGamas_before,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0 THEN 1 ELSE 0 END) as jumlah_before,
      SUM(CASE WHEN c.id is null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_ndt_after,
      SUM(CASE WHEN c.id is not null AND b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_dt_after,
      SUM(CASE WHEN b.troubleno_parent is null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isReguler_after,
      SUM(CASE WHEN b.troubleno_parent is not null AND TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as isGamas_after,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , a.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0 THEN 1 ELSE 0 END) as jumlah_after,
      count(*) as jumlah
    FROM  `nonatero_excel` a
    LEFT JOIN rock_gamas_opn b ON a.trouble_no = b.trouble_no
    LEFT JOIN dispatch_teknisi c ON a.trouble_no = c.Ndem
    LEFT JOIN loker_pic d ON a.LOKER_DISPATCH = d.LOKER
    Where
      a.LOKER_DISPATCH <> ""
    GROUP BY a.LOKER_DISPATCH
    ORDER BY d.PIC,jumlah DESC
    ');
    return $query;
  }

  public static function ms2nmigrasi($date){
    $query = DB::SELECT('
      SELECT
        a.datel,
        SUM(CASE WHEN dt.id IS NOT NULL THEN 1 ELSE 0 END) as jumlah_asg,
        SUM(CASE WHEN dt.id IS NULL THEN 1 ELSE 0 END) as jumlah_notasg,
        count(*) as jumlah
      FROM
        ms2n_migrasi a
      LEFT JOIN Data_Pelanggan_Starclick b ON a.orderId = b.orderId
      LEFT JOIN dispatch_teknisi dt ON a.orderId = dt.Ndem
      WHERE
        a.last_update LIKE "'.$date.'%"
      GROUP BY a.datel
    ');
    return $query;
  }

  public static function ms2n($date){
    $query = DB::SELECT('
      SELECT
        a.Kandatel,
        SUM(CASE WHEN a.JENIS = "3P" THEN 1 ELSE 0 END) as Jumlah_3P,
        SUM(CASE WHEN a.JENIS = "3P" AND dt.id is null THEN 1 ELSE 0 END) as Jumlah_3P_NOTASG,
        SUM(CASE WHEN a.JENIS = "3P" AND dt.id is not null THEN 1 ELSE 0 END) as Jumlah_3P_ASG,
        SUM(CASE WHEN a.JENIS = "2P" THEN 1 ELSE 0 END) as Jumlah_2P,
        SUM(CASE WHEN a.JENIS = "2P" AND dt.id is null THEN 1 ELSE 0 END) as Jumlah_2P_NOTASG,
        SUM(CASE WHEN a.JENIS = "2P" AND dt.id is not null THEN 1 ELSE 0 END) as Jumlah_2P_ASG,
        count(*) as Jumlah
      FROM
        ms2n a
        LEFT JOIN Data_Pelanggan_Starclick dps ON MID(a.kode_sc,3,10) = dps.orderId
        LEFT JOIN dispatch_teknisi dt ON dt.Ndem = MID(a.kode_sc,3,10)
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS like "'.$date.'%"
      GROUP BY a.Kandatel
    ');
    return $query;
  }

  public static function listms2n($tgl,$so,$jenis,$status){

    if ($so == "ALL"){
      $where_so = '';
    } else {
      $where_so = ' AND a.Kandatel = "'.$so.'"';
    }

    if ($jenis == "ALL"){
      $where_jenis = '';
    } else {
      $where_jenis = ' AND a.JENIS = "'.$jenis.'"';
    }

    if ($status == "ALL"){
      $where_status = '';
    } else if ($status == "ASG"){
      $where_status = ' AND c.id is NOT NULL';
    } else {
      $where_status = ' AND c.id is NULL';
    }

    $query = DB::SELECT('
      SELECT
        a.*,
        b.*,
        pl.*,
        c.id as id_dt,

        DATE_FORMAT(a.Tgl_PS,"%d %M %Y") as Tgl_PS,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140") then plm.qty else 0 end) as tiang_7,
        SUM(case when plm.id_item IN ("PU-S9.0-140") then plm.qty else 0 end) as tiang_9,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
        ms2n a
        LEFT JOIN Data_Pelanggan_Starclick b ON MID(a.kode_sc,3,10) = b.orderId
        LEFT JOIN dispatch_teknisi c ON c.Ndem = MID(a.kode_sc,3,10)
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON c.jenis_layanan = dtjl.jenis_layanan
        LEFT JOIN psb_laporan pl ON c.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
        LEFT JOIN item i ON plm.id_item = i.id_item
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS LIKE "%'.$tgl.'%"
        '.$where_status.'
        '.$where_jenis.'
        '.$where_so.'
      GROUP BY a.Ndem
    ');
    return $query;
  }

  public static function rekonMitrProv($tgl){
      $query = DB::SELECT('
      SELECT
        a.mitra,
        SUM(CASE WHEN d.sc<>"" THEN 1 ELSE 0 END) as jumlah_prov
      FROM
      regu a
      LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_myir_wo d ON b.Ndem = d.sc
      LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
      LEFT JOIN roc r ON b.Ndem = r.no_tiket
      WHERE
        c.status_laporan in ("1","37","38") AND
        b.tgl LIKE "'.$tgl.'%"
      GROUP BY a.mitra
    ');
    return $query;
  }

  public static function rekonMitraList($tgl){
	  $query = DB::SELECT('
      SELECT
      	*
      FROM
        regu a
        LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN Data_Pelanggan_Starclick d ON b.Ndem = d.orderId
        LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
        LEFT JOIN roc r ON b.Ndem = r.no_tiket
      WHERE
        c.status_laporan in ("1","37","38") AND
        b.tgl LIKE "'.$tgl.'%" AND
        d.orderId <> ""

    ');
    return $query;
  }

  public static function qc_borneo($date,$dateend){
	  $query = DB::SELECT('
	  	SELECT
	  	c.mitra,
	  	SUM(CASE WHEN b.foto_berita = "OK" THEN 1 ELSE 0 END) as foto_berita_ok,
	  	SUM(CASE WHEN b.foto_rumah = "OK" THEN 1 ELSE 0 END) as foto_rumah_ok,
	  	SUM(CASE WHEN b.foto_teknisi = "OK" THEN 1 ELSE 0 END) as foto_teknisi_ok,
	  	SUM(CASE WHEN b.foto_odp = "OK" THEN 1 ELSE 0 END) as foto_odp_ok,
	  	SUM(CASE WHEN b.foto_redaman = "OK" THEN 1 ELSE 0 END) as foto_redaman_ok,
	  	SUM(CASE WHEN b.foto_ont = "OK" THEN 1 ELSE 0 END) as foto_ont_ok,
	  	SUM(CASE WHEN b.tagging_lokasi = "OK" THEN 1 ELSE 0 END) as tagging_pelanggan_ok,
      count(*) as jumlah
	  	FROM
	  		dispatch_teknisi a
	  	LEFT JOIN qc_borneo b ON a.Ndem = b.sc
	  	LEFT JOIN regu c ON a.id_regu = c.id_regu
	  	WHERE
	  		(DATE(a.tgl) BETWEEN "'.$date.'" AND "'.$dateend.'") AND
        b.unit LIKE "%ASO%" AND
	  		b.sc <> ""
	  	GROUP BY c.mitra
	  ');
	  return $query;
  }

  public static function dashboardQC($date,$datex)
  {
    return DB::SELECT('
    SELECT
    ma.mitra_amija_pt,
    SUM(CASE WHEN qbt.tag_unit IS NULL THEN 1 ELSE 0 END) as dapros,
    SUM(CASE WHEN qbt.tag_unit LIKE "%ASO%" THEN 1 ELSE 0 END) as valid1_aso,
    SUM(CASE WHEN qbt.tag_unit LIKE "%DAMAN%" THEN 1 ELSE 0 END) as valid1_daman,
    SUM(CASE WHEN qbt.tag_unit LIKE "%CC%" THEN 1 ELSE 0 END) as valid1_cc,
    SUM(CASE WHEN qbt.tag_unit LIKE "%HS%" THEN 1 ELSE 0 END) as valid1_hs,
    SUM(CASE WHEN qbt.tag_unit LIKE "%AMO%" THEN 1 ELSE 0 END) as valid1_amo,
    SUM(CASE WHEN qbt.tag_unit = "Validasi2;" THEN 1 ELSE 0 END) as valid2,
    SUM(CASE WHEN qbt.tag_unit IN ("Valid;","Valid;;Not Integrity") THEN 1 ELSE 0 END) as lolos,
    COUNT(*) as grand_total
    FROM qc_borneo_tr6 qbt
    LEFT JOIN dispatch_teknisi dt ON qbt.sc = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$datex.'")
    GROUP BY ma.mitra_amija_pt
    ');
  }

  public static function dashboardQCNok($date,$datex)
  {
    return DB::SELECT('
    SELECT
    ma.mitra_amija_pt,
    SUM(CASE WHEN qbt.nok = 9 THEN 1 ELSE 0 END) as nok9,
    SUM(CASE WHEN qbt.nok = 8 THEN 1 ELSE 0 END) as nok8,
    SUM(CASE WHEN qbt.nok = 7 THEN 1 ELSE 0 END) as nok7,
    SUM(CASE WHEN qbt.nok = 6 THEN 1 ELSE 0 END) as nok6,
    SUM(CASE WHEN qbt.nok = 5 THEN 1 ELSE 0 END) as nok5,
    SUM(CASE WHEN qbt.nok = 4 THEN 1 ELSE 0 END) as nok4,
    SUM(CASE WHEN qbt.nok = 3 THEN 1 ELSE 0 END) as nok3,
    SUM(CASE WHEN qbt.nok = 2 THEN 1 ELSE 0 END) as nok2,
    SUM(CASE WHEN qbt.nok = 1 THEN 1 ELSE 0 END) as nok1,
    COUNT(*) as grand_total
    FROM qc_borneo_tr6 qbt
    LEFT JOIN dispatch_teknisi dt ON qbt.sc = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$datex.'") AND
    qbt.tag_unit LIKE "%ASO%" AND qbt.nok <> 0
    GROUP BY ma.mitra_amija_pt
    ');
  }

  public static function dashboardQCFoto($date,$datex)
  {
    return DB::SELECT('
    SELECT
    ma.mitra_amija_pt,
    SUM(CASE WHEN qbt.foto_rumah LIKE "Nok%" THEN 1 ELSE 0 END) as ft_rumah,
    SUM(CASE WHEN qbt.foto_teknisi LIKE "Nok%" THEN 1 ELSE 0 END) as ft_teknisi,
    SUM(CASE WHEN qbt.foto_odp LIKE "Nok%" THEN 1 ELSE 0 END) as ft_odp,
    SUM(CASE WHEN qbt.foto_redaman LIKE "Nok%" THEN 1 ELSE 0 END) as ft_redaman,
    SUM(CASE WHEN qbt.foto_cpe_layanan LIKE "Nok%" THEN 1 ELSE 0 END) as ft_cpe_layanan,
    SUM(CASE WHEN qbt.foto_berita_acara LIKE "Nok%" THEN 1 ELSE 0 END) as ft_berita_acara,
    SUM(CASE WHEN qbt.tag_lokasi LIKE "Nok%" THEN 1 ELSE 0 END) as tg_lokasi,
    SUM(CASE WHEN qbt.foto_surat LIKE "Nok%" THEN 1 ELSE 0 END) as ft_surat,
    SUM(CASE WHEN qbt.foto_profile_myih LIKE "Nok%" THEN 1 ELSE 0 END) as ft_profile_myih
    FROM qc_borneo_tr6 qbt
    LEFT JOIN dispatch_teknisi dt ON qbt.sc = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$datex.'")
    GROUP BY ma.mitra_amija_pt
    ');
  }

  public static function dashboardQCdetail($mitra,$date,$datex,$tag_unit)
  {

    if ($mitra == "NA") {
      $where_mitra = 'AND ma.mitra_amija_pt IS NULL';
    } elseif ($mitra == "ALL") {
      $where_mitra = '';
    } else {
      $where_mitra = 'AND ma.mitra_amija_pt = "'.$mitra.'"';
    }

    if ($tag_unit == "ALL") {
      $where_unit = '';
    } elseif ($tag_unit == "ALLNOK") {
      $where_unit = 'AND qbt.nok <> 0 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK9") {
      $where_unit = 'AND qbt.nok = 9 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK8") {
      $where_unit = 'AND qbt.nok = 8 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK7") {
      $where_unit = 'AND qbt.nok = 7 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK6") {
      $where_unit = 'AND qbt.nok = 6 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK5") {
      $where_unit = 'AND qbt.nok = 5 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK4") {
      $where_unit = 'AND qbt.nok = 4 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK3") {
      $where_unit = 'AND qbt.nok = 3 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK2") {
      $where_unit = 'AND qbt.nok = 2 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "NOK1") {
      $where_unit = 'AND qbt.nok = 1 AND qbt.tag_unit LIKE "%ASO%"';
    } elseif ($tag_unit == "DAPROS") {
      $where_unit = 'AND qbt.tag_unit IS NULL';
    }elseif ($tag_unit == "LOLOS") {
      $where_unit = 'AND qbt.tag_unit IN ("Valid;","Valid;;Not Integrity")';
    } elseif ($tag_unit == "VALID2") {
      $where_unit = 'AND qbt.tag_unit = "Validasi2;"';
    } elseif ($tag_unit == "FTRUMAH") {
      $where_unit = 'AND qbt.foto_rumah LIKE "Nok%"';
    } elseif ($tag_unit == "FTTEKNISI") {
      $where_unit = 'AND qbt.foto_teknisi LIKE "Nok%"';
    } elseif ($tag_unit == "FTODP") {
      $where_unit = 'AND qbt.foto_odp LIKE "Nok%"';
    } elseif ($tag_unit == "FTREDAMAN") {
      $where_unit = 'AND qbt.foto_redaman LIKE "Nok%"';
    } elseif ($tag_unit == "FTCPELAYANAN") {
      $where_unit = 'AND qbt.foto_cpe_layanan LIKE "Nok%"';
    } elseif ($tag_unit == "FTBERITAACARA") {
      $where_unit = 'AND qbt.foto_berita_acara LIKE "Nok%"';
    } elseif ($tag_unit == "TGLOKASI") {
      $where_unit = 'AND qbt.tag_lokasi LIKE "Nok%"';
    } elseif ($tag_unit == "FTSURAT") {
      $where_unit = 'AND qbt.foto_surat LIKE "Nok%"';
    } elseif ($tag_unit == "FTPROFILEMYIH") {
      $where_unit = 'AND qbt.foto_profile_myih LIKE "Nok%"';
    } else {
      $where_unit = 'AND qbt.tag_unit LIKE "%'.$tag_unit.'%"';
    }

    return DB::SELECT('
    SELECT
    qbt.*,
    pl.nama_odp,
    pl.kordinat_odp,
    dps.alproname,
    kt.PACKAGE_NAME as kt_package_name,
    dps.orderStatus,
    dps.jenisPsb,
    r.uraian,
    ma.mitra_amija_pt as mitra,
    bo.nikTeknisi,
    bo.mitra as bo_mitra,
    gt.title as sektor,
    gt.TL,
    dt.id as id_dt,
    pl.id as id_pl
    FROM qc_borneo_tr6 qbt
    LEFT JOIN dispatch_teknisi dt ON qbt.sc = dt.NO_ORDER
    LEFT JOIN Data_Pelanggan_Starclick dps ON qbt.sc = dps.orderIdInteger
    LEFT JOIN kpro_tr6 kt ON qbt.sc = kt.ORDER_ID
    LEFT JOIN ba_online bo ON qbt.sc = bo.no_wo_int
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    WHERE
    (DATE(qbt.dtPs) BETWEEN "'.$date.'" AND "'.$datex.'")
    '.$where_unit.'
    '.$where_mitra.'
    ORDER BY qbt.datePs DESC
    ');
  }

  public static function logTeknisiQC($id){
    return DB::SELECT('
    SELECT
      pll.psb_laporan_id,
      pll.created_by,
      pll.created_name,
      emp.nama,
      emp.mitra_amija
    FROM psb_laporan_log pll
    LEFT JOIN user u ON pll.created_by = u.id_karyawan
    LEFT JOIN 1_2_employee emp ON pll.created_by = emp.nik
    WHERE
    pll.psb_laporan_id = "'.$id.'" AND
    u.level = "10"
    ORDER BY pll.created_at DESC
    LIMIT 1
    ')[0];
  }


  public static function rekonMitra($date,$dateend){
    $query = DB::SELECT('
      SELECT
        ma.mitra_amija_pt as mitra,
        SUM(CASE WHEN dm.ND<>"" THEN 1 ELSE 0 END) as jumlah_migrasi,
        SUM(CASE WHEN b.jenis_order IN ("IN","INT") THEN 1 ELSE 0 END) as jumlah_asr
      FROM
      regu a
      LEFT JOIN dispatch_teknisi b ON a.id_regu = b.id_regu
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON b.jenis_layanan = dtjl.jenis_layanan
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN dossier_master dm ON b.Ndem = dm.ND
      LEFT JOIN data_nossa_1_log r ON b.NO_ORDER = r.ID
      LEFT JOIN roc rc ON b.Ndem = rc.no_tiket
      LEFT JOIN mitra_amija ma ON a.mitra = ma.mitra_amija
      WHERE
        c.status_laporan = "1" AND
        (DATE(c.modified_at) BETWEEN "'.$date.'" AND "'.$dateend.'")
      GROUP BY ma.mitra_amija_pt
    ');
    return $query;
  }

  public static function rekonProvMitra($date,$dateend){
    $query = DB::SELECT('
      SELECT
      ma.mitra_amija_pt as mitra,
      SUM(CASE WHEN pl.status_laporan NOT IN (1,4) THEN 1 ELSE 0 END) as jumlah_non,
      SUM(CASE WHEN pl.status_laporan = "1" THEN 1 ELSE 0 END) as jumlah_up,
      SUM(CASE WHEN pl.status_laporan = "4" THEN 1 ELSE 0 END) as jumlah_hr
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      dt.jenis_order IN ("SC","MYIR") AND
      (DATE(dps.orderDate) BETWEEN "'.$date.'" AND "'.$dateend.'")
      GROUP BY ma.mitra_amija_pt
    ');
    return $query;
  }

  public static function warehouse($date) {
    return DB::SELECT('
    SELECT
    gt.so_inv_active,
    SUM(CASE WHEN pl.typeont = "ZTEF660" THEN 1 ELSE 0 END) as ZTEF660,
    SUM(CASE WHEN pl.typeont = "ZTEF609" THEN 1 ELSE 0 END) as ZTEF609,
    SUM(CASE WHEN pl.typeont = "ZTEF670L" THEN 1 ELSE 0 END) as ZTEF670L,
    SUM(CASE WHEN pl.typeont = "ZTEF821" THEN 1 ELSE 0 END) as ZTEF821,
    SUM(CASE WHEN pl.typeont = "ZTEF829" THEN 1 ELSE 0 END) as ZTEF829,
    SUM(CASE WHEN pl.typeont = "HG82455A" THEN 1 ELSE 0 END) as HG82455A,
    SUM(CASE WHEN pl.typeont = "HG8245H5" THEN 1 ELSE 0 END) as HG8245H5,
    SUM(CASE WHEN pl.typeont = "H87Z5675M21" THEN 1 ELSE 0 END) as H87Z5675M21,
    SUM(CASE WHEN pl.typeont = "HG6243C" THEN 1 ELSE 0 END) as HG6243C,
    SUM(CASE WHEN pl.typeont = "ALUG240WA" THEN 1 ELSE 0 END) as ALUG240WA,
    SUM(CASE WHEN pl.typeont = "ALUI240WA" THEN 1 ELSE 0 END) as ALUI240WA,
    SUM(CASE WHEN pl.typestb = "ZTEB700V5" THEN 1 ELSE 0 END) as ZTEB700V5,
    SUM(CASE WHEN pl.typestb = "ZTEB760H" THEN 1 ELSE 0 END) as ZTEB760H,
    SUM(CASE WHEN pl.typestb = "ZTEB660H" THEN 1 ELSE 0 END) as ZTEB660H,
    SUM(CASE WHEN pl.typestb = "INDIBOX" THEN 1 ELSE 0 END) as INDIBOX,
    SUM(CASE WHEN pl.typestb = "IPCAM" THEN 1 ELSE 0 END) as IPCAM
    FROM dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(pl.modified_at) LIKE "'.$date.'%")
    GROUP BY gt.so_inv_active
    ');
  }

  public static function warehouseMaterial($date) {
    return DB::SELECT('
    SELECT
    gt.so_inv_active,
    SUM(CASE WHEN plm.id_item = "BREKET-A" THEN plm.qty ELSE 0 END) as breket,
    SUM(CASE WHEN plm.id_item = "Pulstrap" THEN plm.qty ELSE 0 END) as pulstrap,
    SUM(CASE WHEN plm.id_item = "RJ45-5" THEN plm.qty ELSE 0 END) as RJ45,
    SUM(CASE WHEN plm.id_item IN ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty ELSE 0 END) as roset,
    SUM(CASE WHEN plm.id_item IN ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty ELSE 0 END) as sclamp,
    SUM(CASE WHEN plm.id_item = "SOC-ILS" THEN plm.qty ELSE 0 END) as socils,
    SUM(CASE WHEN plm.id_item = "SOC-SUM" THEN plm.qty ELSE 0 END) as socsum,
    SUM(CASE WHEN plm.id_item = "TC-SM-OTP-1" THEN plm.qty ELSE 0 END) as tcsmotp,
    SUM(CASE WHEN plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") THEN plm.qty ELSE 0 END) as utp,
    SUM(CASE WHEN plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
    SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
    SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
    SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-75-NonAcc","preconnectorized 75 M") THEN plm.qty ELSE 0 END) as precon75,
    SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
    SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-100-NonAcc","preconnectorized 100 M") THEN plm.qty ELSE 0 END) as precon100,
    SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-150-NonAcc" THEN plm.qty ELSE 0 END) as precon150,
    SUM(CASE WHEN plm.id_item = "PU-S7.0-140" THEN plm.qty ELSE 0 END) as tiang7,
    SUM(CASE WHEN plm.id_item = "PU-S9.0-140" THEN plm.qty ELSE 0 END) as tiang9
    FROM dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(pl.modified_at) LIKE "'.$date.'%")
    GROUP BY gt.so_inv_active
    ');
  }

  public static function warehouseRecap($date,$type,$nte,$area){
    if ($type == "ONT") {
      $type_nte = 'AND pl.typeont = "'.$nte.'"';
    } elseif ($type == "STB") {
      $type_nte = 'AND pl.typestb = "'.$nte.'"';
    }
    if ($area == "NA") {
      $areax = 'AND gt.so_inv_active IS NULL';
    } elseif ($area == "ALL") {
      $areax = '';
    } else {
      $areax = 'AND gt.so_inv_active = "'.$area.'"';
    }
    return DB::SELECT('
    SELECT
    gt.so_inv_active,
    dt.Ndem,
    dps.orderName,
    pmw.customer,
    dn1l.Customer_Name,
    pls.laporan_status,
    pl.catatan,
    pl.kordinat_pelanggan,
    pl.typeont,
    pl.snont,
    pl.typestb,
    pl.snstb,
    r.uraian,
    ma.mitra_amija_pt,
    gt.title,
    pl.modified_at
    FROM dispatch_teknisi dt
    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
    LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN roc rc ON dt.Ndem = rc.no_tiket
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    WHERE
    (DATE(pl.modified_at) LIKE "'.$date.'%")
    '.$type_nte.'
    '.$areax.'
    GROUP BY dt.Ndem
    ORDER BY pl.modified_at DESC
    ');
  }

  public static function warehouseData($date,$material,$area){
    if ($material == "breket") {
      $materialx = 'AND plm.id_item = "BREKET-A"';
    } elseif ($material == "pulstrap") {
      $materialx = 'AND plm.id_item = "Pulstrap"';
    } elseif ($material == "RJ45") {
      $materialx = 'AND plm.id_item = "RJ45-5"';
    } elseif ($material == "roset") {
      $materialx = 'AND plm.id_item IN ("RS-IN-SC-1","RS-IN-SC-11")';
    } elseif ($material == "sclamp") {
      $materialx = 'AND plm.id_item IN ("RS-IN-SC-1","RS-IN-SC-11")';
    } elseif ($material == "socils") {
      $materialx = 'AND plm.id_item = "SOC-ILS"';
    } elseif ($material == "socsum") {
      $materialx = 'AND plm.id_item = "SOC-SUM"';
    } elseif ($material == "tcsmotp") {
      $materialx = 'AND plm.id_item = "TC-SM-OTP-1"';
    } elseif ($material == "utp") {
      $materialx = 'AND plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6")';
    } elseif ($material == "tray_cable") {
      $materialx = 'AND plm.id_item = "TC-OF-CR-200"';
    } elseif ($material == "precon50") {
      $materialx = 'AND plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M")';
    } elseif ($material == "precon75") {
      $materialx = 'AND plm.id_item IN ("Preconnectorized-1C-75-NonAcc","preconnectorized 75 M")';
    } elseif ($material == "precon80") {
      $materialx = 'AND plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80")';
    } elseif ($material == "precon100") {
      $materialx = 'AND plm.id_item IN ("Preconnectorized-1C-100-NonAcc","preconnectorized 100 M")';
    } elseif ($material == "precon150") {
      $materialx = 'AND plm.id_item = "Preconnectorized-1C-150-NonAcc"';
    } elseif ($material == "tiang7") {
      $materialx = 'AND plm.id_item = "PU-S7.0-140"';
    } elseif ($material == "tiang9") {
      $materialx = 'AND plm.id_item = "PU-S9.0-140"';
    } else {
      $materialx = '';
    }
    if ($area == "NA") {
      $areax = 'AND gt.so_inv_active IS NULL';
    } elseif ($area == "ALL") {
      $areax = '';
    } else {
      $areax = 'AND gt.so_inv_active = "'.$area.'"';
    }
    return DB::SELECT('
    SELECT
    gt.so_inv_active,
    dt.Ndem,
    dps.orderName,
    pmw.customer,
    dn1l.Customer_Name,
    pls.laporan_status,
    pl.catatan,
    pl.kordinat_pelanggan,
    plm.id_item,
    plm.qty,
    pl.rfc_number,
    r.uraian,
    ma.mitra_amija_pt,
    gt.title,
    pl.modified_at
    FROM dispatch_teknisi dt
    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
    LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN roc rc ON dt.Ndem = rc.no_tiket
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    WHERE
    (DATE(pl.modified_at) LIKE "'.$date.'%")
    '.$materialx.'
    '.$areax.'
    GROUP BY dt.Ndem
    ORDER BY pl.modified_at DESC
    ');
  }

  public static function rekonMitraDc($tgl){
      $sql = 'SELECT
                a.mitra,
                SUM(CASE WHEN IF(d.id_item = "Preconnectorized-1C-100-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-75-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-50-NonAcc",e.num * d.qty,0) + IF(d.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),e.num * d.qty,0) + IF(d.id_item = "AC-OF-SM-1B",d.qty,0) > 75 THEN 1 ELSE 0 END) as dc_preconn
              FROM
                regu a
              LEFT JOIN dispatch_teknisi b ON b.id_regu = a.id_regu
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = b.id
              LEFT JOIN psb_laporan_material d ON d.psb_laporan_id = c.id
              LEFT JOIN rfc_sal e ON d.id_item_bantu = e.id_item_bantu
              Where
                c.status_laporan IN ("1","37","38") AND
                b.tgl LIKE "%'.$tgl.'%"
              GROUP BY a.mitra
             ';

      $data = DB::select($sql);

      return $data;
  }

  public static function rekonMitraDcList($mitra, $tgl){
      $where_mitra = '';
      if($mitra<>"ALL"){
          $where_mitra = 'AND a.mitra="'.$mitra.'"';
      }

      $sql = 'SELECT
                *,
                f.laporan_status as statusTeknisi,
                SUM(IF(d.id_item = "Preconnectorized-1C-100-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-75-NonAcc",e.num * d.qty,0) + IF(d.id_item = "Preconnectorized-1C-50-NonAcc",e.num * d.qty,0) + IF(d.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),e.num * d.qty,0) + IF(d.id_item = "AC-OF-SM-1B",d.qty,0)) as dc_preconn
              FROM
                regu a
              LEFT JOIN dispatch_teknisi b ON b.id_regu = a.id_regu
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = b.id
              LEFT JOIN psb_laporan_material d ON d.psb_laporan_id = c.id
              LEFT JOIN rfc_sal e ON d.id_item_bantu = e.id_item_bantu
              LEFT JOIN psb_laporan_status f ON f.laporan_status_id = c.status_laporan
              Where
                c.status_laporan IN ("1","37","38") AND
                b.tgl LIKE "%'.$tgl.'%"
                '.$where_mitra.'
              GROUP BY b.Ndem HAVING dc_preconn > 75
             ';

      $data = DB::select($sql);
      return $data;
  }

  public static function migrasiListMitra($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
         SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-80-NonAcc",i.num * plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C6", "UTP-C5") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "Preconnectorized-1C-50-NonAcc" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc") then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dm.LQUARTIER as orderAddr,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan in ("1","37","38")
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function migrasiListMitraMs2n($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        dm.LQUARTIER as orderAddr,
        dtjl.mode
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.Ndem IN (
         5262021738,
5114705289,
5113353464,
5262022104,
5113356183,
5114774783,
5114415997,
5114220211,
5113269530,
5113271530,
5113352009,
5113362779,
5113361848,
5113263733,
5262023738,
5113265059,
5113267576,
5114721399,
5114707167,
5113300179,
5113304241,
5114314553,
5114412959,
5114707934,
5114774200,
5114412256,
5114777760,
5114786622,
5113364289,
5114720179,
5114780928,
51874366,
5262021275,
5113250118,
5113353032,
5262021322,
5262025217,
5114722468,
51870600,
51823041,
5113352387,
5113269632,
5276064117,
5114281873,
5114786237,
5276064118,
5276064052,
5113357952,
5114365841,
5113354180,
5113257651,
5113250273,
5114364316,
5113256540,
5113353537,
5113357071,
5113301701,
5113303702,
5114283624,
5114422365,
5114225000,
5114721457,
51742271,
5113352730,
5113352722,
5113263375,
5114314679,
5114413112,
5113256537,
5113256553,
5262022816,
5262022620,
51732170,
5262025257,
5114221617,
5114420333,
5113267984,
5113267855,
5113364565,
5113256219,
5114788091,
5113352783,
5113352720,
5114773635,
51821658,
5114780623,
5113262898,
5113268054,
5113300277,
5113253508,
5113266748,
5113266478,
5113251010,
5114411742,
5262024999,
5113304241,
5113354224,
5113254661,
5114316836,
5114721012,
5114220908,
5113307152,
5113261301,
5113271928,
5114369688,
5114780092,
5113350973,
5114777133,
51743132,
51741437,
5114411771,
5113361129,
5114221037,
5113258809,
5114413185,
51744228,
5114778180,
51261546,
51744397,
5114788108,
5114783288,
51261805,
51743034,
5113301561,
5113274896,
51741826,
51742710,
51741371,
5122062028,
5114784420,
5114363152,
5113360410,
5114282368,
5113252462,
5114365324,
5113300065,
5122062050,
5113250948,
5114784720,
5113306332,
5114414655,
5114415257,
51261106,
5113305508,
5114363118,
5113355273,
5113276516,
5114774457,
5114787857,
5114705097,
5113351379,
5113364839,
51261533,
5113364859,
5113258863,
51261480,
5113364809,
5114369670,
5262025474,
5114787822,
5114413122,
5113364669,
5114782327,
5113307384,
5114774392,
51861102,
5113354823,
5113302338,
5113367024,
5113353323,
5114411710,
5114705167,
5113360353,
5113305186,
5113367080,
5114777335,
51741987,
5114791387,
5114781127,
5114316630,
51731339,
5113364889,
51261582,
5114229328,
5113305183,
5113360273,
5113274503,
5113274502,
5114366201,
5113305014,
5113358003,
5113304487,
5114417105,
5114424676,
5113304650,
5113367377,
5114369200,
51261401,
5114777549,
51741268,
5114777455,
5114774516,
5114773622,
5113305467,
5113304203,
5114783285,
5113359663,
5114772585,
5113264881,
51742266,
51741034,
5113270665,
51743353,
5262023369,
51824457,
5113300154,
51741708,
51743559,
5114721267,
51724906,
51741638,
5114720640,
5114774220,
5113361842,
5114424217,
5113300733,
5113300727,
51721610,
51741306,
5262025107,
5114705353,
5114705354,
5114364100,
5114721420,
5114773269,
5113361001,
5113250293,
5113251986,
5113362842,
5114775027,
5113366853,
5114775028,
5113306318,
5114775029,
5114775030,
5113267373,
5113366502,
5114411141,
5113275029,
51821471,
5114422868,
5113354137,
5113353833,
5113357528,
5113259918,
5113271074,
5113272789,
5113262702,
5113268835,
5113271321,
5113270746,
5113354530,
5114424202,
5114366721,
5113354895,
5113253247,
5114412957,
5113360789,
5113300618,
5113357939,
5113364791,
51744340,
5114721463,
5114781693,
51741223,
5114721641,
51261034,
5114720243,
51742260,
5114721188,
5114722850,
51721188,
51741181,
51741240,
51722512,
51721021,
51721279,
51743362,
5113255267,
5113268325,
5113305978,
5113307525,
5113268496,
5114723014,
5113272221,
51742162,
5113304842,
5113252982,
5114774135,
5114773673,
5114783308,
51743740,
5113268118,
5113268030,
5113268119,
51743430,
5122062814,
5114720855,
51261038,
5114221015,
5113253810,
51221457,
5276064120,
5114721618,
5113262121,
5114777346,
5262025612,
5114787794,
51742877,
5113253687,
5113277062,
5113252472,
5114781790,
5114787739,
51261755,
5114721593,
5114281269,
5113306004,
5113308220,
5113256419,
5113250158,
5114705033,
5114365450,
5113254870,
5114410515,
5113257601,
5113256038,
5113257641,
5113256315,
5113265965,
5113255966,
5113251715,
5113267248,
5113261843,
5113250333,
5113257721,
5113257879,
5113274292,
5113257661,
5113276067,
5113269272,
5113265019,
5113276887,
5122062879,
5113304829,
5114410551,
5113350931,
5114365969,
5113352348,
5113367848,
5114221226,
5113256440,
5114413406,
5113255597,
5113364670,
5113250265,
5114364594,
5122062494,
5113250313,
5113355216,
5113353353,
51221470,
5113366157,
5113302702,
5114423358,
51221841,
5113352000,
5113362600,
5113277084,
5113306451,
5113355512,
5114782634,
51741788,
5113260417,
51743849,
5114723083,
5113360268,
5113364869,
5113255075,
5114221234,
5113275010,
5114363032,
5113352135,
5113306793,
5114413419,
51741182,
5113363883,
5114787949,
5113367666,
5262025463,
5114722864,
5113302515,
5262023136,
5113257066,
5114707235,
5113260122,
5113270007,
5113267257,
51723400,
5114368561,
5114420904,
5113304059,
5114784575,
5113360973,
5113356934,
5114420036,
5114368129,
5114783503,
5262025240,
5114413425,
5113267381,
5113269346,
5113267081,
5113303233,
5114221677,
5114364666,
5114722469,
5113261227,
5114424104,
5114770732,
51874416,
5113275492,
5114369853,
5114722926,
5114780034,
5114773021,
5114422317,
5113265578,
51741239,
51742854,
5114368866,
5113256839,
51742224,
5114721465,
5113259789,
5113275371,
5113271369,
5113250805,
5113277084,
51861357,
5113360620,
5113255734,
5114770058,
5114424619,
5113354567,
5113363625,
5114368319,
5114369697,
5262023710,
5113272736,
51742913,
51722324,
5113271993,
5113252864,
51263166,
51263008,
51263288,
5114722277,
5114364195,
5113301121,
5113353191,
5113267642,
51743046,
5114721038,
5113267103,
51221871,
5114316562,
51742276,
5113305131,
5113357658,
5114721326,
5262021715,
5113274664,
5113251896,
5262030881,
5113263643,
5114721334,
5113275720,
5113353550,
5113263433,
5113358900,
5113351956,
5114363065,
5262025642,
5262021686,
5262025284,
5262025246,
5262025647,
5262024254,
5262025589,
5262025254,
5113262603,
51723307,
5113353608,
51721626,
5113252023,
5113353926,
5113306482,
5113257141,
5114784962,
5114413398,
51723529,
5113257180,
5113307982,
5113351951,
5114720678,
5113250668,
51742664,
5113268223,
5113367588,
5114774490,
5113363135,
5113366515,
5113257880,
5113357483,
5114420042,
5262025301,
5276064035,
5113361912,
5113354285,
5276064045,
5262027154,
51824967,
5114413468,
5114220323,
5113256058,
5113262093,
51741412,
5113354149,
5113252218,
5113263243,
51724450,
51741873,
5114721756,
5114770760,
5113253890,
5113270027,
5113268715,
5114770724,
5113262626,
5113308026,
5113252958,
5276064111,
5113269404,
5114367108,
5113271870,
5114780136,
5262025249,
5113250911,
51742694,
5114720961,
5114790372,
5113358093,
5113301590,
51741321,
51741213,
5114770741,
5114770705,
5113250121,
5113272114,
51742842,
5113270000,
5113272349,
5113262376,
5113272344,
5113306505,
51731695,
5114721373,
5276064068,
5113254406,
5113267987,
5113305556,
5262030948,
5113262964,
5113352153,
5113267194,
5113259719,
5114423145,
5114420388,
5113264175,
5114788082,
5114721091,
51723625,
5114790306,
5114785351,
5113358685,
5113262806,
5113263441,
5114365628,
5113353788,
5113365833,
51721375,
5114311002,
5122062043,
5262022592,
5276064069,
5262025473,
5114782964,
5114315915,
5113259065,
5113357229,
5113366417,
5113353706,
5113260852,
5113271555,
5113266782,
5276064105,
5122062502,
5113350110,
5113304104,
5113267493,
5113268359,
5113277508,
51724930,
51723225,
51724663,
51742634,
5262022778,
51743813,
5114720424,
5114774424,
5113358631,
51724286,
5113262959,
5114420805,
5113255459,
5113302378,
5114777045,
5114787979,
5114785127,
5113306680,
1,
5113357566,
5113302926,
5113350278,
5114722634,
5113365893,
5114785355,
5113259570,
5113363634,
5113352329,
51721577,
5114365196,
52762164,
5113361832,
5114423195,
5113353818,
5113260195,
5262030935,
51875107,
5113364290,
52761013,
5114420401,
5113303537,
5262025214,
5114420684,
5114420225,
5114420439,
5114420769,
5113302919,
5113302927,
5113306895,
5114420519,
5113304333,
5113261217,
5113255983,
51871798,
51874404,
5113276026,
5113251644,
5114420545,
51871613,
5113351138,
5262025259,
5113302660,
5113261782,
5116741673,
5113363899,
5113271458,
5114420102,
5113356251,
5113265551,
5262025322,
5113367040,
5113264988,
52761695,
5113363701,
5114421111,
5114721530,
51874454,
5114777254,
5113359474,
5114364040,
5113358046,
5114723233,
5262021515,
5113362405,
5114368443,
5113357290,
5113357291,
5114785353,
5114785354,
51741649,
5113358098,
51742833,
5114415992,
5113253866,
5113352324,
5114368440,
5114777673,
5262025579,
51724824,
5262025499,
5262025588,
5113366516,
51743320,
5113267883,
5113353575,
5113305092,
5113262209,
5113201126,
5113358449,
5113258715,
5113250174,
5114705508,
51724814,
1,
51723479,
5113351726,
5113263786,
5114787895,
51724861,
51743503
         ) AND
       pl.status_laporan = "1"
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function provisioningListMitra($jenis,$status,$tglAwal,$tglAkhir){
    if ($jenis=="NONE"){
      $getWhere = 'AND ma.mitra_amija_pt IS NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND ma.mitra_amija_pt = "'.$jenis.'" ';
    }

    if ($status=="UP"){
      $getStatus = ' AND pl.status_laporan = "1" ';
    } else if ($status == "HR"){
      $getStatus = ' AND pl.status_laporan = "4" ';
    } else if ($status == "NON"){
      $getStatus = ' AND pl.status_laporan NOT IN (1,4) ';
    } else if ($status == "ALL"){
      $getStatus = '';
    }

    $query = DB::SELECT('
    SELECT
    *,
    dt.id as id_dt,
    DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
    DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
    DATE_FORMAT(dps.orderDate,"%Y-%m-%d") as orderDate,
    DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
    SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80"),i.num * plm.qty,0)) as dc_preconn,
    SUM(case when plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") then plm.qty else 0 end) as dc_roll,
    SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
    SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
    SUM(case when plm.id_item = "PC-SC-SC-5" then plm.qty else 0 end) as patchcore_5,
    SUM(case when plm.id_item = "PC-SC-SC-10" then plm.qty else 0 end) as patchcore_10,
    SUM(case when plm.id_item = "PC-SC-SC-15" then plm.qty else 0 end) as patchcore_15,
    SUM(case when plm.id_item = "PC-SC-SC-20" then plm.qty else 0 end) as patchcore_20,
    SUM(case when plm.id_item = "PC-SC-SC-30" then plm.qty else 0 end) as patchcore_30,
    SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
    SUM(case when plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") then plm.qty else 0 end) as precon80,
    SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
    SUM(case when plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") then plm.qty else 0 end) as precon50,
    SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
    SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
    SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
    SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
    SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
    SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
    SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
    SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
    SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
    0 as conduit,
    dtjl.mode,
    SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
    pl.jml_tiang,
    pl.myir,
    gt.SM,
    m.datel as area_migrasi,
    dt.jenis_layanan as dt_lyn,
    dps.provider as dps_provider,
    kprot.PROVIDER as kprot_provider,
    kprot.ORDER_DATE as kprot_orderDate,
    kprot.LAST_UPDATED_DATE as kprot_orderDatePs,
    kprot.STATUS_RESUME as kprot_orderStatus,
    kprot.DEVICE_ID as kprot_myir
  FROM
  regu r
  LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
  LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
  INNER JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
  LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
  LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
  LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
  LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
  LEFT JOIN kpro_tr6 kprot ON dps.orderIdInteger = kprot.ORDER_ID
  LEFT JOIN maintenance_datel m ON dps.sto = m.sto
  LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
  LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
  WHERE
  dt.Ndem NOT LIKE "IN%" AND
  (DATE(dps.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'") AND
  dt.jenis_order NOT IN ("CABUT_NTE","ONT_PREMIUM")
  '.$getWhere.'
  '.$getStatus.'

  GROUP BY pl.id
    ');

    return $query;
  }

  public static function provisioningListMitraX($jenis,$tglAwal,$tglAkhir){
    if ($jenis=="NONE"){
      $getWhere = 'AND ma.mitra_amija_pt IS NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND ma.mitra_amija_pt = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDate,"%Y-%m-%d") as orderDate,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80"),i.num * plm.qty,0)) as dc_preconn,
        SUM(case when plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item = "PC-SC-SC-5" then plm.qty else 0 end) as patchcore_5,
        SUM(case when plm.id_item = "PC-SC-SC-10" then plm.qty else 0 end) as patchcore_10,
        SUM(case when plm.id_item = "PC-SC-SC-15" then plm.qty else 0 end) as patchcore_15,
        SUM(case when plm.id_item = "PC-SC-SC-20" then plm.qty else 0 end) as patchcore_20,
        SUM(case when plm.id_item = "PC-SC-SC-30" then plm.qty else 0 end) as patchcore_30,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item in ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") then plm.qty else 0 end) as precon80,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        pl.jml_tiang,
        pl.myir,
        gt.SM,
        m.datel as area_migrasi,
        dps.provider as dps_provider,
        kprot.PROVIDER as kprot_provider,
        kprot.PROVIDER as kprot_provider,
        kprot.ORDER_DATE as kprot_orderDate,
        kprot.LAST_UPDATED_DATE as kprot_orderDatePs,
        kprot.STATUS_RESUME as kprot_orderStatus,
        kprot.DEVICE_ID as kprot_myir
      FROM
      regu r
      LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
      LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
      INNER JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
      LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN kpro_tr6 kprot ON dps.orderIdInteger = kprot.ORDER_ID
      LEFT JOIN maintenance_datel m ON dps.sto = m.sto
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (dt.dispatch_by IS NULL OR dt.dispatch_by = 5) AND
      (DATE(dps.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'") AND
      pl.status_laporan <> ""
      '.$getWhere.'
      GROUP BY pl.id
    ');

    return $query;
  }

  public static function new_rekon($tglAwal,$tglAkhir){

    $query = DB::SELECT('
    SELECT
    dps.orderId as sc,
    r.uraian as tim,
    ma.mitra_amija_pt,
    DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
    DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
    DATE_FORMAT(dps.orderDate,"%Y-%m-%d") as orderDate,
    DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
    SUM(case when plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") then plm.qty else 0 end) as dc_roll,
    SUM(case when plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") then plm.qty else 0 end) as precon50,
    SUM(case when plm.id_item IN ("Preconnectorized-1C-75-NonAcc","preconnectorized 75 M") then plm.qty else 0 end) as precon75,
    SUM(case when plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") then plm.qty else 0 end) as precon80,
    SUM(case when plm.id_item IN ("Preconnectorized-1C-100-NonAcc","preconnectorized 100 M") then plm.qty else 0 end) as precon100,
    SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as precon150,
    SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
    SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
    FROM Data_Pelanggan_Starclick dps
    LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
    LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
    LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
    WHERE
    (dt.dispatch_by IS NULL OR dt.dispatch_by = 5) AND
    (DATE(dps.orderDate) BETWEEN "'.$tglAwal.'" AND "'.$tglAkhir.'") AND
    r.id_regu IS NOT NULL AND pl.id IS NOT NULL
    GROUP BY pl.id
    ');

    return $query;
  }


 public static function provisioningListMitra_API($jenis,$tgl,$witel,$datel,$sto){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $getWhere_Witel = '';
    if ($witel<>"ALL"){
      $getWhere_Witel = 'AND m.witel = "'.$witel.'"';
    }
    $getWhere_Datel = '';
    if ($datel<>"ALL"){
      $getWhere_Datel = 'AND m.datel = "'.$datel.'"';
    }
    $getWhere_Sto = '';
    if ($sto<>"ALL"){
      $getWhere_Sto = 'AND m.sto = "'.$sto.'"';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "Preconnectorized-1C-100-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-75-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-50-NonAcc",i.num * plm.qty,0) + IF(plm.id_item in ("Preconnectorized-1C-150-NonAcc","Preconnectorized-1C-150-NonAccc"),i.num * plm.qty,0) + IF(plm.id_item = "AC-OF-SM-1B",plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-80-NonAcc",i.num * plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "Preconnectorized-1C-100-NonAcc" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "Preconnectorized-1C-75-NonAcc" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "Preconnectorized-1C-50-NonAcc" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(CASE when plm.id_item in ("BREKET","BREKET-A") THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item in ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "RJ45-5" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item in ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-ILS" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-SUM" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = "AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        pl.jml_tiang,
        pl.myir,
        gt.SM,
        m.datel as area_migrasi
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       -- LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON dps.sto = m.sto
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id

     WHERE
       dt.dispatch_by = "" AND
       dps.orderId <> "" AND
       dps.orderDatePs like "%'.$tgl.'%" AND
       pl.status_laporan in ("1","37","38")
       '.$getWhere.'
       '.$getWhere_Witel.'
       '.$getWhere_Datel.'
       '.$getWhere_Sto.'
       GROUP BY pl.id
    ');

    return $query;
  }
  public static function assuranceListMitra($jenis,$tglNow){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = 'AND r.mitra <> ""';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.updated_at as tanggal_dispatch,
        roc.STO as sto,
        dn1log.Workzone,
        roc.no_tiket as orderId,
        roc.no_tiket as orderNcli,
        dps.ndemPots as ndemPots,
        dps.internet as ndemSpeedy,
        dps.orderName as orderName,
        "ASSURANCE" as jenisPsb,
        "~" as orderStatus,
        dps.orderAddr as orderAddr,
        dt.id as id_dt,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(roc.tgl_open,"%Y-%m-%d") as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "AD-SC" then plm.qty else 0 end) as AD_SC,
        SUM(case when plm.id_item = "ODP-CA-16" then plm.qty else 0 end) as ODP_CA_16,
        SUM(case when plm.id_item = "ODP-CA-8" then plm.qty else 0 end) as ODP_CA_8,
        SUM(case when plm.id_item = "ODP-PB-16" then plm.qty else 0 end) as ODP_PB_16,
        SUM(case when plm.id_item = "ODP-PB-8" then plm.qty else 0 end) as ODP_PB_8,
        SUM(case when plm.id_item = "PS-1-4" then plm.qty else 0 end) as PS_1_4,
        SUM(case when plm.id_item = "PS-1-8" then plm.qty else 0 end) as PS_1_8,
        SUM(case when plm.id_item = "PS-1-16" then plm.qty else 0 end) as PS_1_16,
        SUM(case when plm.id_item = "SOC-SUM" then plm.qty else 0 end) as SOC_SUM,
        SUM(case when plm.id_item = "SOC-ILS" then plm.qty else 0 end) as SOC_ILS,
        0 as conduit,
        dtjl.mode,
        pla.action,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       INNER JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       INNER JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       INNER JOIN roc ON dt.Ndem = roc.no_tiket
       INNER JOIN data_nossa_1_log dn1log ON dt.Ndem = dn1log.Incident
       INNER JOIN Data_Pelanggan_Starclick dps ON dn1log.Service_No = dps.internet
       LEFT JOIN mdf m ON roc.sto_trim = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       roc.no_tiket <> "" AND
       dt.tgl LIKE "'.$tglNow.'%" AND
       pl.status_laporan = 1
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function provisioningList($tgl,$jenis,$status,$so,$ketTw=null){
     $whereTgl = ' pl.modified_at like "%'.$tgl.'%"';
    if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (pl.modified_at "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.datel = "'.$so.'"';
    }
    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = '';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECTcx
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.created_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        DATE_FORMAT(dps.orderDate,"%Y-%m-%d") as tanggal_wo,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        tt.ketTiang,
        dps.sto,
        pl.redaman_odp,
        pl.status_kendala,
        pl.tgl_status_kendala,
        gt.title as sektorNama,
        m.datel as area_migrasi,
        gt.SM
      FROM
       dispatch_teknisi dt
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON dps.sto = m.sto
       LEFT JOIN regu r ON dt.id_regu = r.id_regu
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
       LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
     WHERE
       dt.dispatch_by = "" AND
       dps.orderId <> "" AND
       '.$whereTgl.'
       '.$where_status.'
       '.$where_area.'
       GROUP BY dt.id
       ORDER BY gt.urut
    ');
    return $query;
  }

  public static function reportPotensi(){
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');
      $data = DB::SELECT('
      SELECT
        m.area_migrasi,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL THEN 1 ELSE 0 END) as jumlah_undispatch
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'") AND
        dps.orderStatusId = 16 AND m.mdf<>""
        GROUP BY m.area_migrasi
        ORDER BY jumlah_undispatch DESC
      ');
      return $data;

        // count(*) as jumlah
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is not NULL THEN 1 ELSE 0 END) as jumlah_dispatch,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND pl.status_laporan not in (1,5,6,8,28,29,30,31) AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL AND TIMESTAMPDIFF(HOUR, dps.orderDate, now()) <= 2 THEN 1 ELSE 0 END) as jumlah_undispatchKurang2,
        // -- SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL AND TIMESTAMPDIFF(HOUR, dps.orderDate, now()) > 2 THEN 1 ELSE 0 END) as jumlah_undispatchLebih2,
        // -- SUM(CASE WHEN dps.orderStatus LIKE "%fallout%" THEN 1 ELSE 0 END) as fo,
        // (dps.orderStatusId = 16 OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
        // SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 3 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        // SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 1 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_up,
        // SUM(CASE WHEN dps.orderStatusId = 52 AND dt.id is not NULL THEN 1 ELSE 0 END) as act_com,
        // SUM(CASE WHEN dps.orderStatusId = 7 AND dt.id is not NULL AND dps.orderDatePs LIKE "%'.$enddate.'%" THEN 1 ELSE 0 END) as ps,

       // (dps.orderStatusId = 16 OR dps.orderStatus like "%fallout%")
        // (dps.orderStatusId in (16,7,52,49,50,51) OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
  }

  public static function listpotensi($jenis,$area){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
    $enddate = date('Y-m-d');

    if ($area == "ALL"){
      $where_area = '';
    } else {
      $where_area = 'AND m.area_migrasi="'.$area.'"';
    }
    switch ($jenis){
      case "undispatch" :
        $where = '
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
          AND dps.orderStatusId in (16,40,14,5,4,2,0,49) AND m.mdf<>""
          AND (dt.id is NULL or dt.id_regu is NULL)
        ';
      break;

      // case "dispatched" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatusId = 16
      //     AND dt.id is not NULL
      //   ';
      // break;
      // case "pi" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatusId = 16
      //   ';
      // break;
      // case "fo" :
      //   $where = '
      //     (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //     AND dps.orderStatus LIKE "%fallout%"
      //   ';
      // break;

      case "kendala" :
        $where = '
            (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
            AND dps.orderStatusId = 16
            AND pl.status_laporan not in (1,5,6,8,28,29,30,31,32,37,38,4,7)
            AND dt.id is not NULL
        ';
      break;

      // case "foup" :
      //   $where = '
      //       (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //       AND dps.orderStatusId in (49,50,51)
      //       AND pl.status_laporan=1
      //       AND dt.id is not NULL
      //   ';
      // break;

      // case "actcom" :
      //   $where = '
      //       (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
      //       AND dps.orderStatusId=52
      //       AND dt.id is not NULL
      //   ';
      // break;

      // case "ps" :
      //   $where = '
      //       (DATE(dps.orderDate) LIKE "%'.$enddate.'%")
      //       AND dps.orderStatusId=7
      //       AND dt.id is not NULL
      //   ';
      // break;

      default :
        $where = '';
      break;
    }
    // $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
    // $enddate = date('Y-m-d');
    $data = DB::SELECT('
      SELECT
        *
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN mdf m ON dps.sto = m.mdf
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
      WHERE
        '.$where.'
        '.$where_area.'
      ');

    return $data;
    // (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
  }

  public static function provisioning($jenis,$tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        dt.dispatch_by = "" AND
        dps.orderId <> "" AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.urutan asc, pls.laporan_status asc
    ');
    return $query;
  }

  public static function absenkanbosku(){
    $query = DB::SELECT('select * from a2s_kehadiran where tgl = "'.date('Y-m-d').'" AND hadir = "TIDAK ABSEN" AND jadwal = "MASUK"');
    return $query;
  }

  public static function getRekon($date){
    $query = DB::SELECT('
      SELECT
        a.JENIS,
        count(*) as jumlah_order
      FROM
       ms2n a
      WHERE
       a.Status = "PS" AND
       a.Tgl_PS LIKE "%'.$date.'%"
      GROUP BY a.jenis
    ');
    return $query;
  }

  public static function getRekonList($jenis,$date){
    $query = DB::SELECT('
      SELECT
        *
      FROM
        ms2n a
        LEFT JOIN mdf b ON a.mdf = b.mdf
      WHERE
        a.Status = "PS" AND
        a.Tgl_PS LIKE "%'.$date.'%" AND
        a.JENIS = "'.$jenis.'"
    ');
    return $query;
  }

  public static function getMigrasiStatusList($tgl,$jenis,$status,$so,$order){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }

    if ($status=="ANTRIAN"){
        $whereStatus = 'AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL)';
    }
    else if ($status=="ALL"){
        $whereStatus = '';
    }
    else{
        $whereStatus = "AND pls.laporan_status = '".$status."'";
    };

    if ($order=="ALL"){
        $where_order = '';
    }
    else if ($order=='EMPTY'){
        $where_order = "AND dt.ketOrder is NULL";
    }
    else{
      $where_order = "AND dt.ketOrder='".$order."'";
    };

    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM( i.num ) AS dc_preconn,
        SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(CASE when plm.id_item = "PS-TIANG" THEN plm.qty else 0 end) as pullstrap,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
        dm.LQUARTIER as orderAddr,
        tt.ketTiang,
        dt.ketOrder
      FROM
       dispatch_teknisi dt
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN rfc_sal i on plm.id_item_bantu=i.id_item_bantu
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN regu r ON dt.id_regu = r.id_regu
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
       LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%"
       '.$whereStatus.'
       '.$where_area.'
       '.$where_order.'
       GROUP BY dm.ND
    ');

   // -- LEFT JOIN item i ON plm.id_item = i.id_item
    // -- SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
    // -- SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
    // -- SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
    // -- SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
    // -- SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,

    return $query;
  }

  public static function getMigrasiStatus($tgl,$jenis){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah,
       dt.ketOrder
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN dataStartClickMigrasi dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON (dm.STO = m.mdf oR dps.Sto = m.mdf)
      WHERE
        (dm.ND <> "" OR dps.orderId<>"")AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status, dt.ketOrder
      ORDER BY pls.laporan_status_id
    ');
      // GROUP BY dt.ketOrder
      // GROUP BY dt.ketOrder
    return $query;
  }

  public static function getMigrasi($tgl){
    $query = DB::SELECT('
      SELECT
        *,
        1 as UP2,
        (SELECT
          count(*)
        FROM
          dossier_master dm
        LEFT JOIN mdf m ON dm.STO = m.mdf
        LEFT JOIN dispatch_teknisi dt ON dm.ND = dt.Ndem
          WHERE m.area_migrasi = a.area_migrasi AND dt.Ndem <> "" AND dt.tgl = "'.$tgl.'"
        ) +
        (SELECT
          count(*)
        FROM
          dossier_master dm
        LEFT JOIN mdf m ON dm.STO = m.mdf
        LEFT JOIN dispatch_teknisi dt ON dm.ND_REFERENCE = dt.Ndem
          WHERE m.area_migrasi = a.area_migrasi AND dt.Ndem <> "" AND dt.tgl = "'.$tgl.'"
        ) as WO_HI,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'")
          as UP,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 1 AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          WHERE m.area_migrasi = a.area_migrasi AND pl.status_laporan = 5 AND dt.tgl = "'.$tgl.'")
          as OGP,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KT" AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KT" AND dt.tgl = "'.$tgl.'")
          as KT,
        (SELECT
          count(*)
         FROM dossier_master dm
         LEFT JOIN mdf m ON dm.STO = m.mdf
         LEFT JOIN dispatch_teknisi dt ON (dm.ND = dt.Ndem)
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KP" AND dt.tgl = "'.$tgl.'") +
         (SELECT
           count(*)
          FROM dossier_master dm
          LEFT JOIN mdf m ON dm.STO = m.mdf
          LEFT JOIN dispatch_teknisi dt ON (dm.ND_REFERENCE = dt.Ndem)
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE m.area_migrasi = a.area_migrasi AND pls.grup = "KP" AND dt.tgl = "'.$tgl.'")
          as KP,

         (SELECT
            count(*)
          FROM Data_Pelanggan_Starclick dps
            LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE m.area_migrasi = a.area_migrasi AND dps.orderStatusId = "7" AND jenisPsb LIKE "%MIGRATE%" AND date(orderDatePs) = "'.$tgl.'"
         ) as PS,
         (SELECT
            count(*)
          FROM Data_Pelanggan_Starclick dps
            LEFT JOIN mdf m ON dps.sto = m.mdf
          WHERE m.area_migrasi = a.area_migrasi AND dps.orderStatusId = "7" AND jenisPsb = "Migrate" AND date(orderDatePs) = "'.$tgl.'"
         ) as PS_asis
      FROM
        mdf a
      LEFT JOIN area_migrasi am ON a.area_migrasi = am.area_migrasi
      WHERE am.area_migrasi <> ""
      GROUP BY a.area_migrasi
      ORDER BY am.id_area_migrasi
    ');
    return $query;
  }

  public static function CountWO($id){
    // $Where  = DashboardModel::GetWhere($id,NULL);
    // $result = DashboardModel::QueryCount($Where);
    // return $result;
    echo "test";
  }

  public static function getAssuranceListx($date,$status)
  {
    if ($status == "ALL"){
      $getStatus = '';
    } else if ($status == "ANTRIAN") {
      $getStatus = 'AND pl.id_tbl_mj is NULL';
    } else {
      $getStatus = 'AND pls.laporan_status = "'.$status.'"';
    }

    $query = DB::SELECT('
    SELECT
      dn1l.Incident,
      dn1l.Contact_Phone,
      pl.noPelangganAktif,
      dt.id as id_dt,
      dt.tgl,
      dt.Ndem,
      0 as umur,
      ib.ONU_Rx,
      pl.redaman_iboster,
      ib.ONU_Link_Status as status_ukur,
      dn1l.Service_No,
      dn1l.Customer_Name,
      pl.kordinat_pelanggan,
      dn1l.no_internet,
      dn1l.Service_ID,
      dn1l.ncliOrder,
      dn1l.Segment_Status,
      dn1l.Customer_Segment,
      dn1ll.Incident as aaIncident,
      dn1l.Summary,
      rc.headline as rc_headline,
      dn1l.alamat,
      rc.streetAddress,
      rc.no_speedy as rc_no_speedy,
      rc.ca_manja_time,
      dn1l.Workzone,
      dn1l.Datek,
      pl.nama_odp,
      r.uraian,
      gt.TL,
      gt.ioan,
      r.mitra,
      gt.title as sektor,
      dn1l.Reported_Date,
      dn1l.Reported_Datex,
      dn1l.Source,
      dn1l.Booking_Date,
      pls.laporan_status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti,
      pl.kondisi_odp,
      sp.splitter,
      pl.catatan,
      pl.typeont,
      pl.snont,
      pl.typestb,
      pl.snstb,
      dn1l.user_created,
      pl.modified_by as close_by_user,
      pl.modified_at,
      pl.created_at,
      dt.tgl,
      dn1l.Status_Date,
      ad.datel as ad_datel,
      ma.mitra_amija_pt
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN data_nossa_1 dn1ll ON dn1l.ID = dn1ll.ID
    LEFT JOIN roc rc ON dt.Ndem = rc.no_tiket
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN assurance_datel ad ON gt.ioan = ad.sto
    LEFT JOIN ibooster ib ON dt.Ndem = ib.order_id
    LEFT JOIN jenis_splitter sp ON pl.splitter = sp.splitter_id
    WHERE
    dt.jenis_order IN ("IN","INT") AND
    (DATE(dt.tgl) LIKE "'.$date.'%" AND YEAR(dt.created_at) = "'.date('Y').'")
    '.$getStatus.'
    ORDER BY dn1l.Reported_Date DESC
    ');
    return $query;
  }
  public static function getAssuranceList2($date,$status,$area){
    if ($area == "ALL") {
      $where_status = "";
    } else {
      $where_status = '  gt.title = "'.$area.'" AND ';
    }
    if ($status == "UNDISPATCH"){
      $where_grup = ' c.id is NULL AND TIMESTAMPDIFF( HOUR , ne.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) <> 0';
    } else if ($status == "UNDISPATCH_after") {
      $where_grup = ' c.id is NULL AND TIMESTAMPDIFF( HOUR , ne.TROUBLE_OPENTIME,  "'.date('Y-m-d').' 15:00" ) = 0';
    } else if ($status == "SISA"){
      $where_grup = ' (e.grup = "'.$status.'" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl)="'.date('Y-m-d').'"';

    } else if ($status == "SISA_H"){
      $where_grup = ' (e.grup = "SISA" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl) > "'.date('Y-m-d').'"';
    } else if ($status == "SISA_EXP"){
      $where_grup = ' (e.grup = "'.$status.'" OR (c.id is not NULL AND e.grup is NULL)) AND date(c.tgl) < "'.date('Y-m-d').'"';
    } else {
      $where_grup = ' e.grup = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECT
        *,
        d.action,
        ne.TROUBLE_NO as no_tiket,
        ne.ND_TELP as no_telp,
        ne.ND_INT as no_speedy,
        ne.HEADLINE as headline,
        ne.TROUBLE_OPENTIME as tgl_open,
        ne.CMDF as sto_trim,
        (ne.hari*24) as umur,
        c.id as id_dt,
        h.action as action
      FROM
        rock_excel ne
      LEFT JOIN mdf a ON a.mdf = ne.CMDF
      LEFT JOIN group_telegram gt ON a.sektor_asr = gt.sektor
      LEFT JOIN dispatch_teknisi c ON ne.TROUBLE_NO = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_penyebab g ON d.penyebabId = g.idPenyebab
      LEFT JOIN psb_laporan_action h ON d.action = h.laporan_action_id
      LEFT JOIN regu f ON c.id_regu = f.id_regu
      WHERE
        '.$where_status.'
        '.$where_grup.'
      ORDER BY
      ne.hari DESC
    ');
    return $query;
  }

  public static function getAssuranceListbyAction($date,$status,$source,$sektor){
    if ($source=="TOMMAN"){
      $where_source = 'AND dd.source="'.$source.'"';
    } else {
      $where_source = 'AND dd.source<>"TOMMAN"';
    }
    if ($sektor=="ALL"){
      $where_sektor = '';
    } else {
      $where_sektor = 'AND gt.ioan = "'.$sektor.'"';
    }
    $status = urldecode($status);
    if ($status == "ALL"){
      $getWhere = "";
    } else {
      $getWhere = 'AND b.action LIKE  "%'.$status.'%"';
    }
    $query = DB::SELECT('
    select
      *,
      c.id as id_dt,
      dd.Workzone as sto,
      a.modified_at as tglClose,
      a.created_at as tglCloseCreate,
      aa.status as aaIncident,
      ib.ONU_Link_Status as status_ukur,
      ib.ONU_Rx as ib_onurx,
      a.modified_by as close_by_user,
      emp.nama as close_by_name
    from
      psb_laporan a
      LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
      LEFT JOIN psb_laporan_penyebab bb ON a.penyebabId = bb.idPenyebab
      LEFT JOIN psb_laporan_penyebab_reti plpr ON a.penyebab_reti_id = plpr.id_penyebab_reti
      LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
      LEFT JOIN roc d ON c.Ndem = d.no_tiket
      LEFT JOIN data_nossa_1_log dd ON c.NO_ORDER = dd.ID
      LEFT JOIN data_nossa_1 aa ON dd.ID = aa.ID
      LEFT JOIN regu e ON c.id_regu = e.id_regu
      LEFT JOIN psb_laporan_status f ON a.status_laporan = f.laporan_status_id
      LEFT JOIN group_telegram gt ON e.mainsector = gt.chat_id
      LEFT JOIN ibooster ib ON c.Ndem = ib.order_id
      LEFT JOIN 1_2_employee emp ON a.modified_by = emp.nik
      where c.tgl LIKE "'.$date.'%" '.$getWhere.'
      '.$where_source.'
      '.$where_sektor.'
 order by d.umur DESC');
    return $query;
  }


  public static function Tiket2(){
    $query = DB::SELECT('
      SELECT
        f.title,
        f.chat_id,
        count(*) as jumlah,
        SUM(CASE WHEN (e.grup = "SISA" OR d.id IS NULL) THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN e.grup = "HR" THEN 1 ELSE 0 END) as HR,
        SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as UP
      FROM
        data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi c ON dn1l.ID = c.NO_ORDER
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      WHERE
        dn1l.Incident <> "" AND
        dn1l.Incident_Symptom <> "PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik" AND
        dn1l.Source <> "TOMMAN" AND
        dn1l.Owner_Group = "TA HD WITEL KALSEL" AND
        c.tgl LIKE "'.date("Y-m-d").'"
      GROUP BY f.title
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function Tiket_INT($date){
    $query = DB::SELECT('
      SELECT
        f.title,
        f.chat_id,
        count(*) as jumlah,
        SUM(CASE WHEN (e.grup IS NULL OR e.grup = "NP") THEN 1 ELSE 0 END) as NP,
        SUM(CASE WHEN e.grup = "KP" THEN 1 ELSE 0 END) as KP,
        SUM(CASE WHEN e.grup = "KT" THEN 1 ELSE 0 END) as KT,
        SUM(CASE WHEN e.grup = "OGP" THEN 1 ELSE 0 END) as OGP,
        SUM(CASE WHEN d.status_laporan = "1" THEN 1 ELSE 0 END) as UP
      FROM
        data_nossa_1_log ne
      LEFT JOIN mdf a  ON a.mdf = ne.Workzone
      LEFT JOIN dispatch_teknisi c ON ne.ID = c.NO_ORDER
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      WHERE
        ne.Source = "TOMMAN" AND
        (DATE(c.tgl) LIKE "'.$date.'%") AND
        f.ioan <> "" AND f.ioan_check = 1 AND f.sms_active = 1
      GROUP BY f.title
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function Tiket2ListINT($sektor, $status, $date){
    $where_sektor  = 'AND f.chat_id="'.$sektor.'"';
    if ($sektor=='UNDISPATCH'){
        $where_sektor = 'AND f.chat_id IS NULL';
    }
    elseif ($sektor=='ALL'){
        $where_sektor = '';
    }

    $where_status  = 'AND e.grup="'.$status.'"';
    if ($status=='np'){
        $where_status = 'AND (e.grup = "NP" OR e.grup = "")';
    }
    elseif($status=='up'){
        $where_status = 'AND d.status_laporan=1';
    }
    elseif ($status=='open'){
        $where_status = '';
    }
    elseif($status=='sisa'){
        $where_status='AND e.grup IN ("SISA","KP","KT","OGP") OR d.id IS NULL';
    };

    $sql = '
      SELECT
        c.Ndem,
        i.IP_NE,
        dn1l.Incident,
        dn1l.Service_No,
        i.ONU_Rx,
        i.IP_NE,
        i.Calling_Station_id,
        i.ONU_Link_Status,
        g.uraian,
        f.title,
        dn1l.Reported_Date,
        e.laporan_status,
        e.grup,
        dn1l.Workzone,
        dn1l.Service_Type,
        plp.penyebab,
        pla.action,
        d.catatan
      FROM
        data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi c ON dn1l.ID = c.NO_ORDER
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_penyebab plp ON plp.idPenyebab = d.penyebabId
      LEFT JOIN psb_laporan_action pla on pla.laporan_action_id = d.action
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      LEFT JOIN ibooster i ON i.order_id = dn1l.Incident
      WHERE
        dn1l.Source = "TOMMAN" AND
        f.ioan <> "" AND f.ioan_check = 1 AND f.sms_active = 1 AND
        c.tgl LIKE "'.$date.'%"
        '.$where_sektor.'
        '.$where_status.'
        ORDER BY dn1l.Reported_Date DESC
    ';

    $query = DB::SELECT($sql);
    return $query;
  }

  public static function Tiket2List($sektor, $status){
    $where_sektor  = 'AND f.chat_id="'.$sektor.'"';
    if ($sektor=='UNDISPATCH'){
        $where_sektor = 'AND f.chat_id IS NULL';
    }
    elseif ($sektor=='ALL'){
        $where_sektor = '';
    }

    $where_status  = 'AND e.grup="'.$status.'"';
    if ($status=='np'){
        $where_status = 'AND (e.grup="SISA" OR d.id IS NULL)';
    }
    elseif($status=='up'){
        $where_status = 'AND d.status_laporan=1';
    }
    elseif ($status=='open'){
        $where_status = '';
    }
    elseif($status=='sisa'){
        $where_status='AND (e.grup IN ("SISA","KP","KT","OGP") OR d.id IS NULL)';
    };

    $sql = '
      SELECT
        c.Ndem,
        i.IP_NE,
        dn1l.Incident,
        dn1l.Service_No,
        i.ONU_Rx,
        i.IP_NE,
        i.Calling_Station_id,
        i.ONU_Link_Status,
        g.uraian,
        f.title,
        dn1l.Reported_Date,
        e.laporan_status,
        dn1l.Workzone,
        dn1l.Service_Type,
        plp.penyebab,
        pla.action,
        d.catatan
      FROM
        data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi c ON dn1l.ID = c.NO_ORDER
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_laporan_penyebab plp ON plp.idPenyebab = d.penyebabId
      LEFT JOIN psb_laporan_action pla on pla.laporan_action_id = d.action
      LEFT JOIN regu g ON c.id_regu = g.id_regu
      LEFT JOIN group_telegram f ON f.chat_id = g.mainsector
      LEFT JOIN ibooster i ON i.ND = dn1l.Service_No
      WHERE
        dn1l.Incident <> "" AND
        dn1l.Source <> "TOMMAN" AND
        dn1l.Incident_Symptom <> "PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik" AND
        dn1l.Owner_Group = "TA HD WITEL KALSEL" AND
        c.tgl = "'.date("Y-m-d").'"
        '.$where_sektor.'
        '.$where_status.'
      GROUP BY dn1l.Incident
      ORDER BY dn1l.Reported_Date
    ';

    $query = DB::SELECT($sql);
    return $query;
  }

  public static function Tiket(){
    $dateNow = date('Y-m-d');
    //$lastWeek = mktime(date("H"), date("i"), date("s"), date("m"), date("d")-6, date("Y"));
    $lastWeek = date('Y-m-01');
    $query = DB::SELECT('
      SELECT
      date_format(a.ts,"%d-%m-%Y") as day,
      count(*) as jumlah,
      SUM(CASE WHEN d.status_laporan = 1 THEN 1 ELSE 0 END) as jumlah_open
      FROM roc a
      LEFT JOIN dispatch_teknisi c ON a.no_tiket = c.Ndem
      LEFT JOIN psb_laporan d ON c.id = d.id_tbl_mj
      WHERE DATE(a.ts) BETWEEN "'.$lastWeek.'" AND "'.$dateNow.'" GROUP BY day ORDER BY a.ts
    ');
    return $query;
  }

  public static function Action($date){
    $get_sektor = DB::SELECT('
    SELECT
    e.chat_id,
    e.ioan,
    SUM(case when f.source = "TOMMAN" then 1 else 0 end) as jumlah_INT,
    SUM(case when f.source <> "TOMMAN" then 1 else 0 end) as jumlah_IN
    FROM psb_laporan a
    LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
    LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
    LEFT JOIN regu d ON c.id_regu = d.id_regu
    LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
    LEFT JOIN data_nossa_1_log f ON c.Ndem = f.Incident
    WHERE
    (DATE(c.tgl) LIKE "'.$date.'%") AND b.action IS NOT NULL and c.jenis_order IN ("IN","INT") AND e.ioan <> ""
    GROUP BY e.ioan
    ORDER BY e.urut');
    $SQL = '
      select
        count(*) as jumlah,
        sum(case when f.source = "TOMMAN" then 1 else 0 end) as jumlah_INT,
        sum(case when f.source <> "TOMMAN" then 1 else 0 end) as jumlah_IN,
        ';
    foreach ($get_sektor as $sektor){
    $SQL .= '
        sum(CASE WHEN e.chat_id="'.$sektor->chat_id.'" AND f.source<>"TOMMAN" THEN 1 ELSE 0 END) as '.$sektor->ioan.',
        sum(CASE WHEN e.chat_id="'.$sektor->chat_id.'" AND f.source="TOMMAN" THEN 1 ELSE 0 END) as INT_'.$sektor->ioan.',';
    }

    $SQL .= '
      b.action
      from
        psb_laporan a
        LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
        LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
        LEFT JOIN regu d ON c.id_regu = d.id_regu
        LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
        LEFT JOIN data_nossa_1_log f ON c.Ndem = f.Incident
        where (DATE(c.tgl) LIKE "'.$date.'%") AND c.dispatch_by<>4 and b.action IS NOT NULL group by a.action order by jumlah_IN DESC';
    $query = DB::SELECT($SQL);
    return $query;
  }

  public static function Status($date){
    $query = DB::SELECT('
      SELECT
        d.laporan_status,
        count(*) as jumlah
      FROM
      dispatch_teknisi b
      LEfT JOIN data_nossa_1_log a ON b.NO_ORDER = a.ID
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
        WHERE
        b.Ndem LIKE "IN%" AND
        (DATE(b.tgl) LIKE "'.$date.'%" AND YEAR(b.created_at) = "'.date('Y').'")
      GROUP BY
        d.laporan_status
    ');
    return $query;
  }

  public static function RocActive(){
    $query = DB::SELECT('
      SELECT
        a.sto as sto,
        count(*) as jumlah,
        SUM(CASE WHEN b.Ndem <> "" THEN 1 ELSE 0 END) as dispatched,
        SUM(CASE WHEN b.Ndem is NULL THEN 1 ELSE 0 END) as undispatched
      FROM
        roc_active a
        LEFT JOIN
        dispatch_teknisi b ON a.no_tiket = b.Ndem
      WHERE
        a.sto NOT IN ("KS. TUBUN")
      GROUP BY
        a.sto
      ORDER BY jumlah DESC
    ');
    return $query;
  }

  public static function GetCount($jenis,$status){
    $tgl_ps   = date('Y-m-d');
    $lastWeek = mktime(date("H"), date("i"), date("s"), date("m"), date("d")-6, date("Y"));
    $jenis = strtoupper($jenis);
    switch ($jenis) {
      case "REKON" :
        switch ($status){
          case "INDIHOME" :
            $SQL = '
              SELECT
                count(*) as Jumlah_Indihome,
                SUM(CASE WHEN a.Status_Indihome="SALES_3P_BUNDLED" THEN 1 ELSE 0 END) as Jumlah_0p_3p,
                SUM(CASE WHEN a.Status_Indihome IN ("MIGRASI_2P_3P_BUNDLED","MIGRASI_2P_3P_UNBUNDLED") THEN 1 ELSE 0 END) as Jumlah_2p_3p,
                SUM(CASE WHEN a.Status_Indihome IN ("MIGRASI_1P_3P_BUNDLED","MIGRASI_1P_3P_UNBUNDLED") THEN 1 ELSE 0 END) as Jumlah_1p_3p

              FROM
                ms2n a
              WHERE
                (a.Tgl_PS BETWEEN "2016-10-26" AND "2016-11-25") AND
                a.Status = "PS"
            ';
          break;
        }
      break;
      case "NAL" :
        switch ($status) {
          case "PS" :
            $SQL = '
              SELECT
                Tgl_PS,
                count(*) as jumlah
              FROM
                ms2n
              WHERE
                (Tgl_PS between "'.date('Y-m-d',$lastWeek).'" AND "'.$tgl_ps.'") AND
                Status = "PS"
              GROUP BY
                Tgl_PS
            ';
          break;
          case "Transaksi" :
            $SQL = '
              SELECT
                Tgl_Reg,
                SUM(CASE WHEN Status = "PI"
                THEN 1
                ELSE 0
                END ) AS PI,
                SUM(CASE WHEN Status = "FO"
                THEN 1
                ELSE 0
                END ) AS FO,
                SUM(CASE WHEN Status = "VA"
                THEN 1
                ELSE 0
                END ) AS VA

              FROM
                ms2n
              WHERE
                (Tgl_Reg between "'.date('Y-m-d',$lastWeek).'" AND "'.$tgl_ps.'")
              GROUP BY
                Tgl_Reg
            ';
          break;
        }
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQueryList($jenis,$status,$area){
    $tgl_ps   = date('Y-m-d');
    $jenis = strtoupper($jenis);

    if ($area=="ALL"){
      $Where = '';
    } else {
      $Where = 'AND cc.area = "'.$area.'"';
    }

    switch ($jenis){
      case "MBSP" :
        $SQL = '
          select
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
            ee.id as id_dt,
            PGMAX as orderId
          from prospek a
            LEFT JOIN mdf b on a.CMDF = b.mdf
            LEFT JOIN ms2n c on a.ND_POTS = c.ND
            LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON a.NCLI_POTS = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
            LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
            LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
        ';
        switch ($status){
          case "F2F" :
            $SQL .= '
              WHERE
              b.area = "'.$area.'" AND
              dd.jenisPsb = "Migrate" AND dd.orderStatus="Completed (PS)" AND c.Status is NULL
            ';
          break;
          case "C2F" :
            $SQL .= '
              WHERE
              b.area = "'.$area.'" AND
              dd.orderId is NULL AND
              c.Status is NULL
            ';
          break;

        }
      break;
      case "NAL" :
        $status = strtoupper($status);
        switch ($status) {
          case "PS" :
          $SQL = '
          SELECT
          *,
          gg.uraian,
          dd.jenisPsb,
          dd.orderStatus,
          dd.Kcontact,
          ee.id as id_dt,
          PGMAX as orderId
          FROM
            ms2n bb
          LEFT JOIN mdf cc on bb.mdf = cc.mdf
          LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
          LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
          LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
          LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
          LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
          LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
            where bb.Status="PS" AND bb.Tgl_PS = "'.$tgl_ps.'"
            '.$Where.'
            GROUP BY bb.Ncli
          ';
          break;
          case "RWOS" :
          $SQL = '
          SELECT
          *,
          gg.uraian,
          dd.jenisPsb,
          dd.orderStatus,
          dd.Kcontact,
            ee.id as id_dt,
          PGMAX as orderId
          FROM
            ms2n bb
          LEFT JOIN mdf cc on bb.mdf = cc.mdf
          LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
          LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
          LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
          LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
          LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
          LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
            where dd.orderStatus="Process ISISKA (RWOS)" AND bb.Status<>"PS"
            '.$Where.'
            GROUP BY bb.Ncli
          ';
          break;
          case "COMPLETED" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            LEFT JOIN mdf cc on bb.mdf = cc.mdf
            LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
            LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
            LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
            LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Process OSS (Activation Completed)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "FALLOUTACTIVATION" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            LEFT JOIN mdf cc on bb.mdf = cc.mdf
            LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
            LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
            LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
            LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Fallout (Activation)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "FALLOUTDATA" :
            $SQL = '
            SELECT
            *,
            gg.uraian,
            dd.jenisPsb,
            dd.orderStatus,
            dd.Kcontact,
              ee.id as id_dt,
            PGMAX as orderId
            FROM
              ms2n bb
            LEFT JOIN mdf cc on bb.mdf = cc.mdf
            LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
            LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
            LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
            LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
            LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
            LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
              where dd.orderStatus="Fallout (Data)" AND bb.Status<>"PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
          case "OGP" :
            $SQL = '
              SELECT
              *,
              gg.uraian,
              dd.jenisPsb,
              dd.orderStatus,
              dd.Kcontact,
                ee.id as id_dt,
              PGMAX as orderId
              FROM
                ms2n bb
              LEFT JOIN mdf cc on bb.mdf = cc.mdf
              LEFT JOIN (SELECT d.*,MAX(d.orderId) as PGMAX from Data_Pelanggan_Starclick d GROUP BY d.orderNcli) as pg ON bb.Ncli = pg.orderNcli
              LeFT JOIN Data_Pelanggan_Starclick dd ON PGMAX = dd.orderId
              LEFT JOIN dispatch_teknisi ee ON dd.orderId = ee.Ndem
              LEFT JOIN psb_laporan ff ON ee.id = ff.id_tbl_mj
              LEFT JOIN regu gg ON ee.id_regu = gg.id_regu
              LEFT JOIN prospek hh ON bb.Ncli = hh.NCLI
              where ff.status_laporan = 5 AND bb.Status <> "PS"
              '.$Where.'
              GROUP BY bb.Ncli
            ';
          break;
        }
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQuery($id,$search){
    $tgl_ps   = date('Y-m-d');
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id       = strtoupper($id);
    switch ($id){
      case "NAL" :
        $SQL = '
        SELECT
						c.area,
						count(*) as jumlah,
						sum(case when (b.orderStatus = "Fallout (Activation)" ) then 1 else 0 end) as fallout_activation,
						sum(case when (b.orderStatus = "Fallout (Data)" ) then 1 else 0 end) as fallout_data,
						sum(case when (e.status_laporan = "5" AND b.orderStatus IN ("Process OSS (Provision Issued)","Fallout (WFM)") ) then 1 else 0 end) as OGP,
						(select count(*) from ms2n bb LEFT JOIN mdf cc on bb.mdf = cc.mdf LEFT JOIN Data_Pelanggan_Starclick dd ON bb.Ncli = dd.orderNcli WHERE cc.area = c.area AND bb.Status<>"PS" AND dd.orderStatus = "Process ISISKA (RWOS)") as RWOS,
						(select count(*) from ms2n bb LEFT JOIN mdf cc on bb.mdf = cc.mdf LEFT JOIN Data_Pelanggan_Starclick dd ON bb.Ncli = dd.orderNcli WHERE cc.area = c.area AND bb.Status<>"PS" AND dd.orderStatus = "Process OSS (Activation Completed)") as Completed,
						(select count(*) from ms2n bb LEFT JOIN mdf cc on bb.mdf = cc.mdf where cc.area=c.area AND bb.Status="PS" AND bb.Tgl_PS = "'.$tgl_ps.'") as PS
					FROM `dispatch_teknisi` a
					LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
					LEFT JOIN mdf c ON b.sto = c.mdf
					LEFT JOIN ms2n d ON b.orderNcli = d.Ncli
					LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
					WHERE
						date(a.updated_at) = "'.$tgl_ps.'" AND
						d.Status <> "PS"
					GROUP BY c.area
          ORDER BY PS desc
        ';
      break;
      case "MBSP" :
        $SQL = '
          select
            b.area,
            count(*) as jumlah,
            (SUM(CASE WHEN c.Status = "PS" THEN 1 ELSE 0 END)) as PS_REGULER,
            (SUM(CASE WHEN c.Status = "PS" AND c.Deskripsi IN ("Upgrade MBSP dari 2P ke Indihome","Upgrade MBSP dari 1P ke Indihome") THEN 1 ELSE 0 END)) as PS_MBSP,
            (SUM(CASE WHEN d.jenisPsb = "Migrate" AND d.orderStatus="Completed (PS)" AND c.Status is NULL THEN 1 ELSE 0 END)) as F2F,
            (SUM(CASE WHEN d.orderId is NULL AND c.Status is NULL THEN 1 ELSE 0 END)) as C2F
          from prospek a
            LEFT JOIN mdf b on a.CMDF = b.mdf
            LEFT JOIN ms2n c on a.ND_POTS = c.ND
            LEFT JOIN Data_Pelanggan_Starclick d ON a.NCLI_POTS = d.orderNcli
          group by b.area
        ';
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public static function GetQueryReport($id,$search){
    $tgl_ps   = date('Y-m-d');
    $tomorrow = mktime(date("H"), date("i"), date("s"), date("m"), date("d")+1, date("Y"));
    $id       = strtoupper($id);
    switch ($id){
      case "NAL" :
        $SQL = '
        SELECT
    					c.area,
    					count(*) as JUMLAH,
    					sum(case when (b.orderStatus = "Fallout (Activation)" ) then 1 else 0 end) as fallout_activation,
    					sum(case when (b.orderStatus = "Fallout (Data)" ) then 1 else 0 end) as fallout_data,
    					sum(case when (b.orderStatus = "Process ISISKA (RWOS)"  ) then 1 else 0 end) as RWOS,
    					sum(case when (b.orderStatus = "Process OSS (Activation Completed)" ) then 1 else 0 end) as Completed,
    					sum(case when (e.status_laporan = "5" ) then 1 else 0 end) as OGP,
    					sum(case when (e.status_laporan is NULL) then 1 else 0 end) as BELUM_SURVEY,
    					sum(case when (e.status_laporan = "2") then 1 else 0 end) as KENDALA_TEKNIS,
    					sum(case when (e.status_laporan = "3") then 1 else 0 end) as KENDALA_PELANGGAN,
              sum(case when (e.status_laporan = "1") then 1 else 0 end) as UP,
              sum(case when (e.status_laporan = "7") then 1 else 0 end) as RESCHEDULE,
              sum(case when (e.status_laporan = "8") then 1 else 0 end) as SURVEY_OK,
              sum(case when (e.status_laporan = "9") then 1 else 0 end) as FOLLOW_UP_SALES,
              (select count(*) from ms2n bb LEFT JOIN Data_Pelanggan_Starclick cc on bb.Ncli = cc.orderNcli LEFT JOIN dispatch_teknisi dd on cc.orderId = dd.Ndem LEFT JOIN mdf ee on bb.mdf = ee.mdf where ee.area = c.area AND bb.Sebab IN ("Update","Sudah Ada Janji","Belum Ada Janji") AND cc.orderStatus = "Process OSS (Provision Issued)" AND dd.id_regu is NULL) as UNDISPATCH,
              sum(case when (e.status_laporan = "4") then 1 else 0 end) as HR
    				FROM `dispatch_teknisi` a
    				LEFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
    				LEFT JOIN mdf c ON b.sto = c.mdf
    				LEFT JOIN ms2n d ON b.orderId = SUBSTR(d.kode_sc,3,10)
    				LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            WHERE
  					date(a.updated_at) = "'.$tgl_ps.'" AND
  					c.area is not null AND
  					d.Status <> ""
    				GROUP BY c.area
        ';
      break;
    }
    $query = DB::select($SQL);
    return $query;
  }

  public function reportPotensiDua()
  {
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")));
      $enddate = date('Y-m-d');
      $data = DB::SELECT('
      SELECT
        m.area_migrasi,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is not NULL THEN 1 ELSE 0 END) as jumlah_dispatch,
        SUM(CASE WHEN dps.orderStatusId = 16 AND dt.id is NULL THEN 1 ELSE 0 END) jumlah_undispatch,
        SUM(CASE WHEN dps.orderStatus LIKE "%fallout%" THEN 1 ELSE 0 END) as fo,
        SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 3 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_kendala,
        SUM(CASE WHEN dps.orderStatusId in (49,50,51) AND pl.status_laporan = 1 AND dt.id is not NULL THEN 1 ELSE 0 END) as fo_up,
        SUM(CASE WHEN dps.orderStatusId = 52 AND dt.id is not NULL THEN 1 ELSE 0 END) as act_com,
        SUM(CASE WHEN dps.orderStatusId = 7 AND dt.id is not NULL AND dps.orderDatePs LIKE "%'.$enddate.'%" THEN 1 ELSE 0 END) as ps,
        count(*) as jumlah
      FROM
        Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderId = dt.Ndem
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'") AND
        (dps.orderStatusId in (16,7,52,49,50,51) OR dps.orderStatus LIKE "%fallout%" ) AND m.mdf<>""
        GROUP BY m.area_migrasi
        ORDER BY jumlah DESC
      ');
      return $data;
  }

  public static function provisioningCari($tgl,$where){
    $query = DB::SELECT('
      SELECT t1.* from
        (SELECT
         pls.laporan_status_id,
         pls.laporan_status,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.laporan_status_id)
      as t1
      where t1.laporan_status_id in '.$where.'

    ');
    // dd($query);
    return $query;
  }

  public static function ActionGuarante($date){
    $query = DB::SELECT('
      select
        b.action,
        count(*) as jumlah
      from
        psb_laporan a
        LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
        LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
        where c.tgl LIKE "%'.$date.'%" AND b.action is not NULL and c.dispatch_by = 4 group by a.action order by jumlah DESC');
    return $query;
  }

  public static function getGuaranteListbyAction($date,$status){
    $status = urldecode($status);
    if ($status == "ALL"){
      $getWhere = "";
    } else {
      $getWhere = 'AND b.action LIKE  "%'.$status.'%"';
    }
    $query = DB::SELECT('
    select
      *
    from
      psb_laporan a
      LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp on a.penyebabId=plp.idPenyebab
      LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
      LEFT JOIN roc d ON c.Ndem = d.no_tiket
      LEFT JOIN regu e ON c.id_regu = e.id_regu
      LEFT JOIN psb_laporan_status f ON a.status_laporan = f.laporan_status_id
      LEFT JOIN group_telegram gt ON e.mainsector = gt.chat_id
      where b.action is not NULL AND c.dispatch_by = 4 AND c.tgl LIKE "%'.$date.'%" '.$getWhere.'
 order by d.umur DESC');
    return $query;
  }

   public static function provisioningListFilter($tgl,$jenis,$status,$so,$where){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }
    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' AND pls.laporan_status_id in '.$where;
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }
    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          SUM( i.num ) AS dc_preconn,
          SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
          SUM(case when plm.id_item IN ("UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
          SUM(case when plm.id_item = "WWL-PULLSTRAP" then plm.qty else 0 end) as pullstrap,
          SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
          SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
          0 as conduit,
          dtjl.mode,
          SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable,
          tt.ketTiang,
          dps.sto,
          pls.laporan_status_id
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = "" AND
         dps.orderId <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function provisioningListMitraSebelumRfc($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
        SUM(IF(plm.id_item = "preconnectorized 100 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 75 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 50 M",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-150-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "DC-ROLL",plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "preconnectorized 100 M" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "preconnectorized 75 M" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "preconnectorized 50 M" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(CASE when plm.id_item = "Breket" THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item = "Clamp-s" THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "Pulstrap" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "Rj-45" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item = "RS-IN-SC" THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-Ilsintentech" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-Sumitomo" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = " AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN  dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dt.dispatch_by = "" AND
       dps.orderId <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = 1
       '.$getWhere.'
       GROUP BY dt.id
    ');

    return $query;
  }

  public static function migrasiListMitraSebelumRfc($jenis,$tgl){
    if ($jenis=="NONE"){
      $getWhere = 'AND r.mitra is NULL ';
    } else if ($jenis == "ALL"){
      $getWhere = '';
    } else {
      $getWhere = ' AND r.mitra = "'.$jenis.'" ';
    }
    $query = DB::SELECT('
      SELECT
        *,
        dt.id as id_dt,
        dt.updated_at as tanggal_dispatch,
        dm.STO as sto,
        dm.ND as orderId,
        dm.NCLI as orderNcli,
        dm.ND as ndemPots,
        dm.ND_REFERENCE as ndemSpeedy,
        dm.NAMA as orderName,
        "MIGRASI AS IS" as jenisPsb,
        "~" as orderStatus,
        DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
        DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
        0 as orderDatePs,
        SUM(IF(plm.id_item = "preconnectorized 100 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 75 M",i.num * plm.qty,0) + IF(plm.id_item = "preconnectorized 50 M",i.num * plm.qty,0) + IF(plm.id_item = "Preconnectorized-1C-150-NonAcc",i.num * plm.qty,0) + IF(plm.id_item = "DC-ROLL",plm.qty,0)) as dc_preconn,
        -- SUM( i.num * plm.qty) AS dc_preconn,
        SUM(case when plm.id_item = "DC-ROLL" then plm.qty else 0 end) as dc_roll,
        SUM(case when plm.id_item IN ("UTP-CAT5", "UTP-CAT6", "UTP-C5", "UTP-C6") then plm.qty else 0 end) as utp,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(case when plm.id_item = "preconnectorized 100 M" then plm.qty else 0 end) as precon100,
        SUM(case when plm.id_item = "preconnectorized 75 M" then plm.qty else 0 end) as precon75,
        SUM(case when plm.id_item = "preconnectorized 50 M" then plm.qty else 0 end) as precon50,
        SUM(case when plm.id_item = "Preconnectorized-1C-150-NonAcc" then plm.qty else 0 end) as preconnonacc,
        SUM(case when plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") then plm.qty else 0 end) as tiang,
        SUM(case when plm.id_item LIKE "PC-SC-SC%" then i.num1 else 0 end) as patchcore,
        SUM(CASE when plm.id_item = "Breket" THEN plm.qty else 0 end) as breket,
        SUM(CASE when plm.id_item = "Clamp-s" THEN plm.qty else 0 end) as clamps,
        SUM(CASE when plm.id_item = "Pulstrap" THEN plm.qty else 0 end) as pulstrap,
        SUM(CASE when plm.id_item = "Rj-45" THEN plm.qty else 0 end) as rj45,
        SUM(CASE when plm.id_item = "RS-IN-SC" THEN plm.qty else 0 end) as roset,
        SUM(CASE when plm.id_item = "SOC-Ilsintentech" THEN plm.qty else 0 end) as SOCIlsintentech,
        SUM(CASE when plm.id_item = "SOC-Sumitomo" THEN plm.qty else 0 end) as SOCSumitomo,
        SUM(CASE when plm.id_item = " AD-SC" THEN plm.qty else 0 end) as adsc,
        0 as conduit,
        dm.LQUARTIER as orderAddr,
        dtjl.mode,
        SUM(case when plm.id_item = "TC-OF-CR-200" then plm.qty else 0 end) as tray_cable
      FROM
       regu r
       LEFT JOIN dispatch_teknisi dt ON dt.id_regu = r.id_regu
       LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
       LEFT JOIN item i ON plm.id_item = i.id_item
       LEFT JOIN dossier_master dm ON dt.Ndem = dm.ND
       LEFT JOIN mdf m ON dm.STO = m.mdf
       LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
     WHERE
       dm.ND <> "" AND
       dt.tgl like "%'.$tgl.'%" AND
       pl.status_laporan = "1"
       '.$getWhere.'
       GROUP BY dt.id
    ');
    return $query;
  }

  public static function scbeSektor($tgl, $ket=null){
    $whereTgl = ' (dt.tgl like "'.$tgl.'%" OR pl.modified_at like "'.$tgl.'%")';
      if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('group_telegram')->where('sms_active_prov',1)->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
       $SQL .= 'SUM(CASE WHEN gt.sektor = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->sektor.',';
    }
    $SQL .= '
       count(*) as jumlah
       FROM
         dispatch_teknisi a
       INNER JOIN regu b ON a.id_regu = b.id_regu
       LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
       LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
       LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
       LEFT JOIN Data_Pelanggan_Starclick g ON a.Ndem = g.orderId AND YEAR(g.orderDate) = "'.date('Y').'"
       LEFT JOIN psb_myir_wo ff ON g.myir = ff.myir
       LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
       LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
       WHERE
       ((a.tgl="'.$tgl.'" AND (DATE(g.orderDatePs) >= "'.date('Y-m-d').'" OR c.id IS NULL))  OR c.status_laporan IN (74,4)) AND
       b.ACTIVE=1 AND
       a.jenis_layanan <> "QC" AND
       (f.myir IS NOT NULL OR g.orderId IS NOT NULL OR mw.ND IS NOT NULL) AND
       YEAR(a.tgl)="'.date('Y').'"


      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbe($tgl){
    // $whereTgl = ' (DATE(a.tgl) LIKE "'.$tgl.'%" OR DATE(d.modified_at) LIKE "'.$tgl.'%" OR a.id IS NULL)';
    //   if ($ket<>null){
    //     $ketTgl = explode('_',$ket);

    //     $whereTgl = ' (DATE(a.tgl) BETWEEN "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    // };
    $get_datel = DB::SELECT('SELECT * FROM maintenance_datel WHERE witel = "'.session('witel').'" GROUP BY datel');
    $SQL = '
      SELECT
       d.laporan_status_id,
       d.laporan_status,
       d.statusGrup,';
    foreach ($get_datel as $datel){
       $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
    }
    $SQL .= '
       count(*) as jumlah
       FROM
         dispatch_teknisi a
       LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
       LEFT JOIN regu b ON a.id_regu = b.id_regu
       LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
       LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
       LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
       LEFT JOIN maintenance_datel m ON m.sto = SUBSTR(f.namaOdp,5,3)
       WHERE
       (DATE(a.tgl) LIKE "'.$tgl.'%") AND
       f.myir IS NOT NULL
      GROUP BY d.laporan_status
      ORDER BY d.udash asc, d.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbe_after($tgl){
    // $whereTgl = ' (a.tgl like "'.$tgl.'%" OR pl.modified_at like "'.$tgl.'%")';
    //   if ($ket<>null){
    //     $ketTgl = explode('_',$ket);
    //
    //     $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    // };
    // $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->get();
    $get_datel = DB::SELECT('SELECT * FROM maintenance_datel WHERE witel = "'.session('witel').'" GROUP BY datel');
    $SQL = '
      SELECT
       d.laporan_status_id,
       d.laporan_status,
       d.statusGrup,';
    foreach ($get_datel as $datel){
       $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
    }
    $SQL .= '
       count(*) as jumlah
       FROM
         dispatch_teknisi a
       LEFT JOIN Data_Pelanggan_Starclick g ON a.NO_ORDER = g.orderIdInteger
       LEFT JOIN psb_myir_wo f ON g.myir = f.myir
       LEFT JOIN regu b ON a.id_regu = b.id_regu
       LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
       LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
       LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
       LEFT JOIN maintenance_datel m ON m.sto = SUBSTR(f.namaOdp,5,3)
       WHERE
       YEAR(g.orderDate) = "'.date('Y').'" AND
       (DATE(a.tgl) LIKE "'.$tgl.'%") AND
       g.orderId IS NOT NULL
      GROUP BY d.laporan_status
      ORDER BY d.udash asc, d.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function averageClose($date){
    $query = DB::SELECT('
    SELECT
    gt.title as sektor,
    gt.TL,
    SUM(CASE WHEN dn1l.Source = "RIGHTNOW" AND pl.status_laporan = "1" THEN 1 ELSE 0 END) as close_IN,
    SUM(CASE WHEN dn1l.Source = "RIGHTNOW" THEN 1 ELSE 0 END) as order_IN,
    SUM(CASE WHEN dn1l.Source = "TOMMAN" AND pl.status_laporan = "1" THEN 1 ELSE 0 END) as close_INT,
    SUM(CASE WHEN dn1l.Source = "TOMMAN" THEN 1 ELSE 0 END) as order_INT,
    SUM(CASE WHEN dn1l.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dn1l.Owner_Group = "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" AND pl.status_laporan = "1" THEN 1 ELSE 0 END) as close_UNSPEC,
    SUM(CASE WHEN dn1l.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dn1l.Owner_Group = "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" THEN 1 ELSE 0 END) as order_UNSPEC,
    SUM(CASE WHEN dn1l.Incident_Symptom = "PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik" AND pl.status_laporan = "1" THEN 1 ELSE 0 END) as close_SQM,
    SUM(CASE WHEN dn1l.Solution_Segment = "PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik" THEN 1 ELSE 0 END) as order_SQM
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
    dn1l.Incident <> "" AND
    (DATE(dt.tgl) LIKE "'.$date.'%") AND
    gt.ket_posisi = "IOAN"
    GROUP BY gt.title
    ORDER BY gt.urut
    ');
    return $query;
  }

  public static function averageCloseDetail($date, $title, $order, $status){
    if ($title == "ALL") {
      $where_title = '';
    } else {
      $where_title = ' AND gt.title = "'.$title.'" ';
    }

    if ($order == "CLOSE") {
      $where_order = 'AND pl.status_laporan = "1"';
    } else if ($order == "ORDER"){
      $where_order = '';
    }

    if ($status == "ALL"){
      $where_status = '';
    } else if ($status == "IN"){
      $where_status = ' AND dn1l.Source = "RIGHTNOW" ';
    } else if ($status == "INT"){
      $where_status = ' AND dn1l.Source = "TOMMAN" ';
    } else if ($status == "UNSPEC"){
      $where_status = ' AND dn1l.Owner_Group = "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" ';
    } else if ($status == "SQM"){
      $where_status = ' AND dn1l.Incident_Symptom = "PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik" ';
    }

    $query = DB::SELECT('
      SELECT
        dt.id as id_dt,
        dn1l.Incident,
        dn1l.Service_No,
        dn1l.no_internet,
        ib.ONU_Rx as onurx_ib,
        pl.redaman_iboster,
        dn1l.Customer_Name,
        dn1l.Contact_Phone,
        dn1l.Summary,
        dn1l.Customer_Segment,
        dn1l.Segment_Status,
        dn1l.Reported_Date,
        r.uraian as tim,
        gt.title as sektor,
        gt.ioan,
        gt.chat_id,
        pl.catatan,
        pls.laporan_status as status,
        pla.action,
        pl.nama_odp,
        pl.modified_at as tgl_laporan,
        dt.tgl as tgl_dispatch
      FROM dispatch_teknisi dt
      LEFT JOIN data_nossa_1_log dn1l ON dt.Ndem = dn1l.Incident
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN ibooster ib ON dn1l.Incident = ib.order_id
      WHERE
      dn1l.Incident <> "" AND
      (DATE(dt.tgl) LIKE "'.$date.'%")
      '.$where_title.'
      '.$where_order.'
      '.$where_status.'
    ');

    return $query;
  }

  public static function sisaTiketNossa()
  {
    return DB::SELECT('
    SELECT
      gt.ioan,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >= 2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Assigned_by IN ("AUTOASSIGNED","") AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as WNOK_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 1 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as DIBAWAH1_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=1 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 2 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as DIBAWAH2_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as DIBAWAH3_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=3 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 12 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as DIBAWAH12_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=12 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 24 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as DIATAS12_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=24 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as LEBIH1HARI_REGULER,
      SUM(CASE WHEN dna.Induk_Gamas <> "" AND dna.Customer_Type = "REGULER" THEN 1 ELSE 0 END) as GAMAS_REGULER,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >= 2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Assigned_by IN ("AUTOASSIGNED","") AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as WNOK_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 1 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as DIBAWAH1_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=1 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 2 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as DIBAWAH2_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as DIBAWAH3_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=3 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 12 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as DIBAWAH12_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=12 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 24 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as DIATAS12_HVC,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=24 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as LEBIH1HARI_HVC,
      SUM(CASE WHEN dna.Induk_Gamas <> "" AND dna.Customer_Type LIKE "HVC%" THEN 1 ELSE 0 END) as GAMAS_HVC
    FROM data_nossa_active dna
    LEFT JOIN dispatch_teknisi dt ON dna.ID = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
      dna.Incident IS NOT NULL AND dna.Service_Type <> "NON-NUMBERING"
    GROUP BY gt.ioan
    ');
  }

  public static function sisaTiketNossaDetail($ioan,$durasi)
  {
    switch ($ioan) {
      case 'UNDISPATCH':
        $where_ioan = 'AND dt.Ndem IS NULL';
        break;
      
      case 'ALL':
        $where_ioan = '';
        break;

      default:
        $where_ioan = 'AND gt.ioan = "'.$ioan.'"';
        break;
    }

    if ($durasi == "WNOK_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >= 2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Assigned_by IN ("AUTOASSIGNED","") AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "DIBAWAH1_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 1 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "DIBAWAH2_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=1 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 2 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "DIBAWAH3_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "DIBAWAH12_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=3 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 12 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "DIATAS12_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=12 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 24 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "LEBIH1HARI_REGULER") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=24 AND dna.Induk_Gamas = "" AND dna.Customer_Type = "REGULER"';
    } elseif ($durasi == "GAMAS_REGULER") {
      $where_durasi = 'AND dna.Induk_Gamas <> "" AND dna.Customer_Type = "REGULER"';

    }elseif ($durasi == "WNOK_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >= 2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Assigned_by IN ("AUTOASSIGNED","") AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "DIBAWAH1_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 1 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "DIBAWAH2_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=1 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 2 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "DIBAWAH3_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "DIBAWAH12_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=3 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 12 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "DIATAS12_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=12 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 24 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "LEBIH1HARI_HVC") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=24 AND dna.Induk_Gamas = "" AND dna.Customer_Type LIKE "HVC%"';
    } elseif ($durasi == "GAMAS_HVC") {
      $where_durasi = 'AND dna.Induk_Gamas <> "" AND dna.Customer_Type LIKE "HVC%"';

    } elseif ($durasi == "ALL" && $ioan <> "ALL") {
      $where_durasi = '';
    } else {
      $where_durasi = 'AND dna.Incident IS NOT NULL';
    }

    return DB::SELECT('
    SELECT
        TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") as durasi,
        dt.id as id_dt,
        dt.tgl as tgl_dt,
        pl.modified_at as tgl_laporan,
        r.uraian as tim,
        gt.ioan,
        pl.catatan,
        pls.laporan_status,
        pla.action,
        plp.penyebab,
        plpr.status_penyebab_reti,
        ib.ONU_Rx as ib_onu_rx,
        pl.redaman_iboster,
        ibs.ONU_Rx as ibs_onu_rx,
        dna.*
    FROM data_nossa_active dna
    LEFT JOIN dispatch_teknisi dt ON dna.ID = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    LEFT JOIN ibooster ib ON dna.Incident = ib.order_id
    LEFT JOIN ibooster_assurance ibs ON dna.Incident = ibs.order_id
    WHERE
        dna.Service_Type <> "NON-NUMBERING"
        '.$where_ioan.'
        '.$where_durasi.'
    ORDER BY dna.Reported_Datex DESC
    ');
  }

  public static function manjaManual()
  {

    return DB::SELECT('
    SELECT
      gt.title as sektor,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 3 THEN 1 ELSE 0 END) as DIBAWAH3,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 3 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 12 THEN 1 ELSE 0 END) as DIBAWAH12,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 12 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 24 THEN 1 ELSE 0 END) as DIATAS12,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 24 THEN 1 ELSE 0 END) as LEBIH1HARI,
      COUNT(*) as JUMLAH
    FROM roc
    LEFT JOIN dispatch_teknisi dt ON roc.no_tiket = dt.Ndem
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
    roc.customer_assign = "CA_YES" AND roc.ca_manja_time IS NOT NULL AND (DATE(dt.tgl) = "'.date('Y-m-d').'") AND ( pl.status_laporan <> 1 OR pl.id IS NULL )
    GROUP BY gt.title
    ');
  }

  public static function ttrManualManja($sektor,$durasi)
  {


    if ($sektor == "UNDISPATCH") {
      $where_sektor = 'AND roc.no_tiket IS NULL';
    } elseif ($sektor == "ALL") {
      $where_sektor = '';
    } else {
      $where_sektor = 'AND gt.title = "'.$sektor.'"';
    }

    if ($durasi == "DIBAWAH3") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 3';
    } elseif ($durasi == "DIBAWAH12") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 3 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 12';
    } elseif ($durasi == "DIATAS12") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 12 AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") < 24';
    } elseif ($durasi == "LEBIH1HARI") {
      $where_durasi = 'AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 24';
    } elseif ($durasi =="ALL" && $sektor <> "ALL") {
      $where_durasi = '';
    } else {
      $where_durasi = 'AND roc.no_tiket IS NOT NULL AND TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") >= 0';
    }

    return DB::SELECT('
    SELECT
      dt.Ndem,
      TIMESTAMPDIFF( HOUR , roc.ca_manja_time, "'.date('Y-m-d H:i:s').'") as durasi,
      dt.id as id_dt,
      dt.tgl as tgl_dt,
      pl.modified_at as tgl_laporan,
      r.uraian as tim,
      gt.title as sektor,
      pl.catatan,
      pls.laporan_status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti,
      roc.*
    FROM roc
    LEFT JOIN dispatch_teknisi dt ON roc.no_tiket = dt.Ndem
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    WHERE
    roc.customer_assign = "CA_YES" AND roc.ca_manja_time IS NOT NULL AND (DATE(dt.tgl) = "'.date('Y-m-d').'") AND ( pl.status_laporan <> 1 OR pl.id IS NULL )
      '.$where_sektor.'
      '.$where_durasi.'
    ORDER BY roc.ca_manja_time DESC
    ');
  }

  public static function get_gaul($date){
    $query = DB::SELECT('
    SELECT
      gt.title as sektor,
      gt.ioan,
      count(*) as jumlah
    FROM data_nossa_1_log dn1l
    LEFT JOIN dispatch_teknisi dt ON dn1l.ID = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    WHERE
    dn1l.Incident <> "" AND
    dn1l.GAUL <> "0" AND
    dn1l.Reported_Date LIKE "'.$date.'%"
    GROUP BY gt.title
    ORDER BY jumlah DESC
    ');

    return $query;
  }

  public static function getSaldoUnspec($date){
    $query = DB::SELECT('
      SELECT
      uss.SEKTOR,
      SUM(CASE WHEN dt.id IS NULL THEN 1 ELSE 0 END) as undispatch,
      SUM(CASE WHEN dt.id <> "" THEN 1 ELSE 0 END) as dispatch,
      count(*) as jumlah
      FROM unspec_saldo_semesta uss
      LEFT JOIN data_nossa_1_log dn1l ON uss.ID_ORDER = dn1l.ID
      LEFT JOIN dispatch_teknisi dt ON uss.ID_ORDER = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
      WHERE
      uss.ID_ORDER <> "0" AND
      (DATE(uss.TGL_GRAB) LIKE "'.$date.'%")
      GROUP BY uss.SEKTOR
      ORDER BY jumlah DESC
    ');

    return $query;
  }

  public static function SaldoUnspec($sektor,$order,$date){

    if($sektor=="ALL"){
      $whereSektor = '';
    }else{
      $whereSektor = 'AND uss.SEKTOR = "'.$sektor.'"';
    }

    if($order=="ALL"){
      $whereOrder = '';
    }elseif($order=="UNDISPATCH"){
      $whereOrder = 'AND dt.id IS NULL';
    }elseif($order=="DISPATCH"){
      $whereOrder = 'AND dt.id <> ""';
    }

    $query = DB::SELECT('
      SELECT
      uss.SEKTOR,
      r.uraian as tim,
      uss.NODE_IP,
      uss.CMDF,
      uss.DP,
      uss.NAMA,
      uss.TYPE_PELANGGAN,
      uss.ALAMAT,
      uss.STATUS_INET,
      uss.ONU_RX_POWER,
      uss.TANGGAL_UKUR,
      uss.NOMOR_TIKET,
      dn1l.Service_No,
      uss.STATUS_TIKET,
      pls.laporan_status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti,
      pl.catatan,
      ib.ONU_Rx,
      pl.redaman_iboster,
      dt.tgl as tgl_dispatch,
      pl.modified_at as tgl_laporan
      FROM unspec_saldo_semesta uss
      LEFT JOIN data_nossa_1_log dn1l ON uss.ID_ORDER = dn1l.ID
      LEFT JOIN dispatch_teknisi dt ON uss.ID_ORDER = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
      LEFT JOIN ibooster ib ON dn1l.Incident = ib.order_id
      WHERE
      uss.ID_ORDER <> "0" AND
      (DATE(uss.TGL_GRAB) LIKE "'.$date.'%")
      '.$whereSektor.'
      '.$whereOrder.'
      ORDER BY uss.TANGGAL_UKUR DESC
    ');

    return $query;
  }

  public static function gaul_detil($sektor, $date){

    if ($sektor == "UNDISPATCH") {
      $where_sektor = 'AND dt.id IS NULL';
    } else if ($sektor == "ALL") {
      $where_sektor = '';
    } else {
      $where_sektor = 'AND gt.title = "'.$sektor.'" ';
    }

    $query = DB::SELECT('
    SELECT
      dn1l.Incident,
      dn1l.Service_No,
      dn1l.GAUL,
      gt.title as sektor,
      r.uraian as tim,
      pls.laporan_status as status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti as penyebab_reti,
      pl.catatan,
      pl.modified_at as tgl_update,
      dt.tgl as tgl_dispatch,
      dn1l.Reported_Date
    FROM data_nossa_1_log dn1l
    LEFT JOIN dispatch_teknisi dt ON dn1l.ID = dt.NO_ORDER
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    WHERE
    dn1l.Incident <> "" AND
    dn1l.GAUL <> "0" AND
    dn1l.Reported_Date LIKE "'.$date.'%"
    '.$where_sektor.'
    ORDER BY dn1l.Reported_Date DESC
    ');

    return $query;
  }

  public static function gaul_detil_count($count, $inet){
    if ($count == "1") {
      $where_count = '2';
    } else {
      $where_count = ''.$count.'';
    }

    $query = DB::SELECT('
    SELECT
      dn1.Incident,
      dn1.Service_No,
      dn1.GAUL,
      gt.title as sektor,
      r.uraian as tim,
      pls.laporan_status as status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti as penyebab_reti,
      pl.catatan,
      pl.modified_at as tgl_update,
      dt.tgl as tgl_dispatch,
      dn1.Reported_Date
    FROM data_nossa_1 dn1
    LEFT JOIN dispatch_teknisi dt ON dn1.Incident = dt.Ndem
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    WHERE
    dn1.Service_No = "'.$inet.'"
    ORDER BY dn1.Reported_Date DESC
    LIMIT '.$where_count.'
    ');

    return $query;
    dd($query);
  }

  public static function scbeUP2($tgl, $ket=null){
    $whereTgl = ' (dt.tgl like "'.$tgl.'%")';
      if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
       $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
    }
    $SQL .= '
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
       LEFT JOIN jenis_layanan jl ON my.layanan = jl.jenis_layanan
      WHERE
        m.witel = "'.session('witel').'" AND
        (jl.kategori = "TARIK" OR
        (dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET"))) AND
        pl.status_laporan = 1 AND
        '.$whereTgl.'
      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeWitel($tgl, $ket=null, $witel){
    $whereTgl = '(pl.modified_at like "'.$tgl.'%")';
    // $whereTgl = '(dt.tgl like "'.$tgl.'%")';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
      pls.laporan_status,
      count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
       LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        dt.jenis_order IN ("SC","MYIR") AND
        (my.myir <> "" OR dps.orderId <> "")  AND
        pls.aktif_dash=1 AND
        pls.grup <> "SISA" AND
        m.witel="'.session('witel').'" AND
        '.$whereTgl.'

      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeWitel_no_update($tgl, $ket=null, $witel){
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.statusGrup,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
      pls.laporan_status,
      count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto)
      WHERE
        m.witel="'.session('witel').'" AND
        dt.jenis_order IN ("SC","MYIR") AND
        (my.myir <> "")  AND
        (pls.grup = "SISA" OR pl.id IS NULL)

      GROUP BY pls.laporan_status
      ORDER BY pls.udash asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);

    return $query;
  }

  public static function scbeNew($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN mdf m ON my.sto = m.mdf
      WHERE
        dt.dispatch_by="5" AND
        my.myir <> "" AND
        dt.tgl like "%'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.statusGrup asc, pls.urutan asc, pls.laporan_status asc
    ');
    return $query;
  }

  public static function scbeUp($tgl, $ket=null){
    $whereTgl = ' (dt.tgl like "%'.$tgl.'%" OR pl.modified_at like "'.$tgl.'%")';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->get();
    $SQL = '
    SELECT
     pls.laporan_status_id,
     pls.laporan_status,
     pls.statusGrup,';
     foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
      }
     $SQL .= '
     count(*) as jumlah
    FROM
    dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
    LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
    LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
    LEFT JOIN jenis_layanan jl ON my.layanan = jl.jenis_layanan
    WHERE
      (dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND (date(dps.orderDatePs) = "'.$tgl.'" OR dps.orderDatePs IS NULL)) AND
      pls.laporan_status_id ="1"
    GROUP BY pls.laporan_status
    ORDER BY pls.statusGrup asc, pls.urutan asc, pls.laporan_status asc
  ';
    $query = DB::SELECT($SQL);
    return $query;
  }

  public static function scbeList_old($tgl,$jenis,$status,$so,$ket,$ketTw=null){
    // if ($status=="UP"){
    //   $whereTgl = '(dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND (date(dps.orderDatePs) >= "'.$tgl.'" OR dps.orderDatePS = "" ) AND date(dps.orderDate)>="2020-03-01")  ';
    // } else {
    //   $whereTgl = ' (dps.orderDate like "'.$tgl.'%")';
    // }
      if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (dt.tgl between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = '';
    if ($so<>"ALL"){
      $where_area = ' (m.datel = "'.$so.'") AND ';
    }

    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $where_ket = '';

    if ($ket=="KENDALA"){
      $where_ket = ' AND pls.stts_dash in ("hs","daman","amo","aso","PT1")';
    };

    $query = DB::SELECT('
      SELECT
          *,
          my.orderDate as tanggal_wo,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          dtjl.mode,
          tt.ketTiang,
          dps.sto as stoMy,
          pls.laporan_status_id,
          dps.*,
          pl.redaman_odp,
          pl.status_kendala,
          pl.tgl_status_kendala,
          gt.title as sektorNama,
          pls.stts_dash,
          psl.nama_sales,
          pl.dropcore_label_code,
          m.datel as area_migrasi,
          dps.myir
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN psb_myir_wo my ON dps.myir = my.myir
         LEFT JOIN maintenance_datel m ON dps.sto = m.sto
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
         LEFT JOIN psb_sales psl ON psl.id = my.sales_id
         LEFT JOIN jenis_layanan jl ON my.layanan = jl.jenis_layanan
       WHERE
       '.$where_area.'
       '.$whereTgl.'
         '.$where_ket.'
         '.$where_status.'
         Order by gt.urut asc
    ');

    return $query;
  }

  public static function scbeList_after($tgl,$jenis,$status,$so,$ket,$ketTw=null){
    // if ($status=="UP"){
    //   $whereTgl = '(dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND (date(dps.orderDatePs) >= "'.$tgl.'" OR dps.orderDatePS = "" ) AND date(dps.orderDate)>="2020-03-01")  ';
    // } else {
    //   $whereTgl = ' (my.orderDate like "'.$tgl.'%")';
    // }
      if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (DATE(my.orderDate) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = '';
    if ($so<>"ALL"){
      $where_area = 'AND (m.datel = "'.$so.'") ';
    }

    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' AND pls.laporan_status_id <> "" ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $where_ket = '';

    if ($ket=="KENDALA"){
      $where_ket = ' AND pls.stts_dash in ("hs","daman","amo","aso","PT1")';
    };

    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          my.orderDate as orderDatePs,
          my.orderDate as tanggal_wo,
          dtjl.mode,
          tt.ketTiang,
          my.sto as stoMy,
          pls.laporan_status_id,
          my.customer,
          my.myir,
          my.sto,
          my.created_by as kode_sales,
          dps.*,
          pl.redaman_odp,
          pl.status_kendala,
          pl.tgl_status_kendala,
          gt.title as sektorNama,
          pls.stts_dash,
          psl.nama_sales,
          pl.dropcore_label_code,
          m.datel as area_migrasi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dpss ON dpss.myir = dt.Ndem OR dpss.orderId = dt.Ndem
         LEFT JOIN psb_myir_wo my ON my.myir = dpss.myir
         LEFT JOIN Data_Pelanggan_Starclick_Backend dps ON my.myir = SUBSTR(dps.ORDER_CODE,6,30)
         LEFT JOIN maintenance_datel m ON (SUBSTR(my. namaOdp,5,3) = m.sto)
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
         LEFT JOIN psb_sales psl ON psl.id = my.sales_id
         LEFT JOIN jenis_layanan jl ON my.layanan = jl.jenis_layanan
       WHERE
       dpss.orderId is not null AND
       pl.modified_at LIKE "%'.$tgl.'%"
       '.$where_area.'
         '.$where_ket.'
         '.$where_status.'
         Order by gt.urut asc
    ');

    return $query;
  }

  public static function scbeList_myir($tgl, $jenis, $status, $so, $ket, $ketTw = null)
  {
		if ($tgl == "kurang3hari")
    {
			$where_tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<3)';
		}
    else if ($tgl == "x3sd7hari")
    {
			$where_tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<7)';
		}
    else if ($tgl == "lebih7hari")
    {
			$where_tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3';
		}
    else
    {
			$where_tgl = '(DATE(pl.modified_at) LIKE "'.$tgl.'%")';
		}
    
    if ($status=="UP")
    {
      $whereTgl = '(dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND (date(dps.orderDatePs) >= "'.$tgl.'" OR dps.orderDatePS = "" ) AND date(dps.orderDate)>="2020-03-01")  ';
    }
    else
    {
      $whereTgl = ' (pmw.orderDate like "'.$tgl.'%")';
    }

    if ($ketTw<>null)
    {
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (DATE(pmw.orderDate) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    }

    $where_area = '';
    if ($so <> "ALL")
    {
      $where_area = 'AND md.datel = "'.$so.'"';
    }

    if ($status == "ANTRIAN")
    {
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    }
    else if ($status == "ALL")
    {
      $where_status = ' AND NOT pls.laporan_status = "PERMINTAAN REVOKE"';
    }
    else if ($status == "XLANJUTPT1")
    {
      $where_status = ' AND pls.laporan_status IN ("LANJUT PT1", "PT2 OK") AND dps.orderStatus NOT IN ("FOLLOW UP COMPLETED")';
    }
    else if ($status == "SOLUSI_PSB")
    {
      $where_status = ' AND pls.laporan_status IN ("ODP FULL","INSERT TIANG","ORDER MAINTENANCE")';
    }
    else
    {
      $where_status = '  AND pls.laporan_status LIKE "%'.$status.'%"';
    }

    $where_ket = '';
    if ($ket=="KENDALA")
    {
      $where_ket = ' AND pls.stts_dash IN ("hs","daman","amo","aso","kendala")';
    }
    else if ($ket=="LIMITED")
    {
      $where_ket = 'AND pls.stts_dash = "limit"';
    }

    $query = DB::SELECT('
    SELECT
      gt.SM,
      gt.TL,
      r.uraian as TIM,
      pls.stts_dash as PIC,
      gt.title as SEKTOR,
      dt.Ndem as ORDER_ID,
      dps.orderId as SC,
      pmw.myir as MYIR,
      dps.agent_id as AGENT_ID,
      pl.valins_id as VALINS_ID,
      dt.tgl as TGL_DISPATCH,
      pl.modified_at as TGL_STATUS,
      dps.orderDatePs,
      dps.sto as STO,
      pmw.sto as STO2,
      dps.ndemPots as VOICE,
      pmw.no_telp as VOICE2,
      dps.ndemSpeedy as INET,
      pmw.no_internet as INET2,
      pmw.sales_id as KODE_SALES,
      pmw.psb,
      pl.sc_unsc,
      pl.kcontact_unsc,
      dps.orderNcli as NCLI,
      dps.orderName as NAMA,
      pmw.customer as NAMA2,
      dps.orderAddr as ALAMAT,
      pmw.alamatLengkap as ALAMAT2,
      pmw.picPelanggan as NO_HP,
      dps.jenisPsb,
      dt.jenis_layanan as LAYANAN,
      pmw.layanan as LAYANAN2,
      dps.kcontact as KCONTACT,
      pmw.kcontack as KCONTACT2,
      pl.kordinat_pelanggan as KORPEL,
      pmw.koorPelanggan as KORSEL,
      pl.nama_odp as ODP,
      dps.alproname as ODP2,
      pmw.namaOdp as ODP3,
      pl.redaman_odp as REDAMAN_ODP,
      md.datel as AREA,
      r.mitra as MITRA,
      dps.orderStatus as STATUS_SC,
      pls.laporan_status as STATUS_TEK,
      tt.ketTiang,
      pl.status_kendala as STATUS_KEN,
      pl.tgl_status_kendala as TGL_STATUS_KEN,
      pl.dropcore_label_code as QRCODE,
      pl.catatan as CATATAN,
      r.uraian,
      pls.stts_dash,
      gt.title as sektorNama,
      dt.id as id_dt,
      dt.Ndem,
      dps.orderId,
      dps.kcontact,
      pmw.orderDate as tanggal_wo,
      pl.valins_id,
      dt.tgl as tanggal_dispatch,
      pl.modified_at,
      SUBSTR(pmw.namaOdp,5,3) as stoMy,
      dps.ndemPots as noTelp,
      dps.ndemSpeedy as internet,
      pmw.sales_id as kode_sales,
      dps.orderNcli,
      dps.orderName,
      dps.orderAddr,
      pmw.picPelanggan as noPelangganAktif,
      dt.jenis_layanan,
      pmw.koorPelanggan as kordinat_pelanggan,
      pmw.koorPelanggan,
      pmw.namaOdp as nama_odp,
      pl.redaman_odp,
      md.datel as area_migrasi,
      r.mitra,
      dps.orderStatus,
      pls.laporan_status,
      pl.status_kendala,
      pl.tgl_status_kendala,
      pl.dropcore_label_code,
      pl.catatan,
      pmw.koorPelanggan as kordinatPel,
      pmw.namaOdp,
      dps.alproname,
      md.datel,
      md.sto
    FROM dispatch_teknisi dt
    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN maintenance_datel md ON dt.dispatch_sto = md.sto
    LEFT JOIN tambahTiang tt ON pl.ttId = tt.id
    WHERE
    dt.jenis_order IN ("SC","MYIR") AND
    pls.aktif_dash = 1 AND
    dt.jenis_layanan NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
    '.$where_tgl.'
    '.$where_area.'
    '.$where_ket.'
    '.$where_status.'
    ORDER BY gt.urut ASC
    ');

    return $query;
  }

  public static function scbeList($tgl,$jenis,$status,$so,$ket,$ketTw=null){
		if ($tgl == "kurang3hari"){
			$where_tgl = 'TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<3 ';
		} else if ($tgl == "x3sd7hari") {
			$where_tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<7)';
		} else if ($tgl == "lebih7hari") {
			$where_tgl = 'TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3 ';
		} else {
			$where_tgl = 'dt.tgl = "'.$tgl.'"';
		}
    if ($status=="UP"){
      $whereTgl = '(dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND (date(dps.orderDatePs) >= "'.$tgl.'" OR dps.orderDatePS = "" ) AND date(dps.orderDate)>="2020-03-01")  ';
    } else {
      $whereTgl = ' (my.orderDate like "'.$tgl.'%")';
    }
      if ($ketTw<>null){
        $ketTgl = explode('_',$ketTw);

        $whereTgl = ' (DATE(my.orderDate) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $where_area = '';
    if ($so<>"ALL"){
      $where_area = 'AND (m.datel = "'.$so.'") ';
    }

    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $where_ket = '';

    if ($ket=="KENDALA"){
      $where_ket = ' AND pls.stts_dash in ("hs","daman","amo","aso","PT1")';
    };

    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          my.orderDate as orderDatePs,
          my.orderDate as tanggal_wo,
          dtjl.mode,
          tt.ketTiang,
          my.sto as stoMy,
          pls.laporan_status_id,
          my.customer,
          my.myir,
          my.sto,
          my.created_by as kode_sales,
          pl.redaman_odp,
          pl.status_kendala,
          pl.tgl_status_kendala,
          gt.title as sektorNama,
          pls.stts_dash,
          psl.nama_sales,
          pl.dropcore_label_code,
          m.datel as area_migrasi
        FROM
         dispatch_teknisi dt
				 LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		     LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN tambahTiang tt ON pl.ttId = tt.id
         LEFT JOIN psb_sales psl ON psl.id = my.sales_id
         LEFT JOIN jenis_layanan jl ON my.layanan = jl.jenis_layanan
		     LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
		     LEFT JOIN maintenance_datel m ON SUBSTR(my. namaOdp,5,3) = m.sto
		     LEFT JOIN regu r ON dt.id_regu = r.id_regu
		     LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
       WHERE
       dpss.orderId IS NOT NULL AND
       '.$where_tgl.'
       '.$where_area.'
         '.$where_ket.'
         '.$where_status.'
         Order by gt.urut asc
    ');

    return $query;
  }

  public static function get_potensi_ps($tgl, $ket=null,$witel){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
    $enddate = date('Y-m-d',strtotime($tgl));

    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $startdate = $ketTgl[0];
        $enddate   = $ketTgl[1];
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
        SELECT
         dps.orderStatusId,
         dps.orderStatus,';
         foreach ($get_datel as $datel){
           $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
         }
         $SQL .='
         count(*) as jumlah
        FROM
            dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN maintenance_datel m ON dps.sto = m.sto
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          (dps.orderStatus in (
            "OSS PONR",
            "WFM ACTIVATION COMPLETE",
            "OSS PROVISIONING COMPLETE",
            "TIBS FULFILL BILLING START",
            "TIBS FULFILL BILLING COMPLETED",
            "EAI COMPLETED",
            "Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Process OSS (Activation Completed)") OR
          (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))) AND
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ';
    $query = DB::select($SQL);

    return $query;
  }

  public static function reportPotensiPsStar($tgl, $ket=null){
    $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
    $enddate = date('Y-m-d',strtotime($tgl));

    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $startdate = $ketTgl[0];
        $enddate   = $ketTgl[1];
    };

    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
            dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          (dps.orderStatus in (
            "OSS PONR",
            "WFM ACTIVATION COMPLETE",
            "OSS PROVISIONING COMPLETE",
            "TIBS FULFILL BILLING START",
            "TIBS FULFILL BILLING COMPLETED",
            "EAI COMPLETED",
            "Process OSS (Provision Completed)",
            "Process ISISKA (RWOS)",
            "Activation Complete",
            "Fallout (Data)",
            "Fallout (Activation)",
            "Process OSS (Activation Completed)") OR
          (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))) AND
          (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');

    return $query;
  }

	public static function new_potensi_ps_all($group)
  {
    switch ($group) {
      case 'SEKTOR':
          $area = DB::select('SELECT title as area FROM group_telegram WHERE ket_posisi = "PROV" AND title != "TERRITORY BASE HELPDESK KALSEL" GROUP BY title');
          $alias = 'gt.title';
        break;

      case 'PROV':
        $area = DB::select('SELECT sektor_prov as area FROM maintenance_datel GROUP BY sektor_prov');
        $alias = 'md.sektor_prov';
        break;
      
      case 'DATEL':
        $area = DB::select('SELECT datel as area FROM maintenance_datel GROUP BY datel');
        $alias = 'md.datel';
        break;
      
      case 'HERO':
          $area = DB::select('SELECT HERO as area FROM maintenance_datel GROUP BY HERO');
          $alias = 'md.HERO';
        break;
    }

    $query1 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN cda.type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as PI_PAGI,
          SUM(CASE WHEN cda.type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as FO_PAGI,
          SUM(CASE WHEN cda.type_grab = "PI_BARU" THEN 1 ELSE 0 END) as PI_BARU,
          SUM(CASE WHEN cda.STATUS_RESUME IN ("CANCEL COMPLETED","REVOKE COMPLETED") THEN 1 ELSE 0 END) as COMP_CANCEL
      FROM comparin_data_api cda
      LEFT JOIN maintenance_datel md ON cda.STO = md.sto
      LEFT JOIN dispatch_teknisi dt ON cda.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          md.witel = "KALSEL" AND cda.JENISPSB = "AO"
      GROUP BY ' . $alias . '
    ');

    $query2 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as antrian_und
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dt.Ndem IS NULL AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '") AND (dps.jenisPsb LIKE "AO%" OR dps.jenisPsb LIKE "PDA LOCAL%") AND dps.orderStatusId IN (1202, 51, 300)
      GROUP BY ' . $alias . '
    ');

    $query3 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as antrian_np
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND (pl.status_laporan = 6 OR pl.status_laporan IS NULL)
      GROUP BY ' . $alias . '
    ');

    $query4 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as potensi_kendala_kp
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1202, 51) AND pl.status_laporan IN (11,13,16,23,48,49,55,56,71,72,73,106,107) AND DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '"
      GROUP BY ' . $alias . '
    ');

    $query5 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as potensi_kendala_kt
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1202, 51) AND pl.status_laporan IN (2,10,15,24,25,26,27,34,35,36,42,46,47,51,57,64,65,75,76,77,81,84,87,108,110,112,113) AND DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '"
      GROUP BY ' . $alias . '
    ');

    $query6 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN dps.jenisPsb LIKE "AO%" THEN 1 ELSE 0 END) as potensi_ps,
          SUM(CASE WHEN dps.jenisPsb LIKE "PDA%" THEN 1 ELSE 0 END) as potensi_ps_pda
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          ((
            (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d' ,strtotime("-1 month")).'" AND "'.date('Y-m-d').'") AND
            dps.orderStatusId IN ("-1400", "50", "1205", "1206", "1400", "1401", "1804")
          )
          OR
          (
            dps.orderStatusId = "1500" AND DATE(dps.orderDatePs) = "'.date('Y-m-d').'"
          ))
      GROUP BY ' . $alias . '
    ');

    $query7 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN dps.jenisPsb LIKE "AO%" THEN 1 ELSE 0 END) as potensi_progress,
          SUM(CASE WHEN dps.jenisPsb LIKE "PDA%" THEN 1 ELSE 0 END) as potensi_progress_pda
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.orderStatusId NOT IN (1205, 1206, 1500, 50) AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)
      GROUP BY ' . $alias . '
    ');

    $query8 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as actcomp
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND YEAR(dps.orderDate) = "' . date('Y') . '" AND dps.orderStatusId IN (1300, 1205)
      GROUP BY ' . $alias . '
    ');

    $query9 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as ps
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1500, 1203, 7) AND ((DATE(dps.orderDatePs) = "' . date('Y-m-d') . '") OR (DATE(dps.orderDate) = "' . date('Y-m-d') . '") AND dps.orderDatePs = "0000-00-00 00:00:00")
      GROUP BY ' . $alias . '
    ');

    $query10 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as start_cancel
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND DATE(dps.orderDate) = "' . date('Y-m-d') . '" AND dps.orderStatusId IN (1600, 1700)
      GROUP BY ' . $alias . '
    ');

    $query11 = DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as potensi_progress_risma
      FROM risma_prov_witel rpw
      LEFT JOIN dispatch_teknisi dt ON rpw.trackID_int = dt.NO_ORDER AND dt.jenis_order = "MYIR"
      LEFT JOIN psb_myir_wo pmw ON rpw.trackID_int = pmw.myir
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          rpw.status_data = "Fresh" AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)
    ');

    $query12 =   DB::select('
      SELECT
          ' . $alias .' as area,
          SUM(CASE WHEN (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d', strtotime("-1 days")).'") THEN 1 ELSE 0 END) as prabac_ps_h1,
          SUM(CASE WHEN (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as prabac_ps_hi
      FROM prabac_psharian_indihome pshi
      LEFT JOIN dispatch_teknisi dt ON pshi.order_id_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pshi.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL"
      GROUP BY ' . $alias . '
    ');

    $query13 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as bypass_pda_hd
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "PDA%" AND
          (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-30 days")) . '" AND "' . date('Y-m-d') . '") AND
          dps.orderStatusId IN ("1202", "-1201") AND
          pl.status_laporan IN (1, 4)
      GROUP BY ' . $alias . '
    ');

    foreach ($area as $val)
    {
      foreach ($query1 as $val_c1)
      {
        if (!empty($val_c1->area))
        {
          if ($val_c1->area == $val->area)
          {
            $query[$val->area]['PI_PAGI'] = $val_c1->PI_PAGI;
            $query[$val->area]['FO_PAGI'] = $val_c1->FO_PAGI;
            $query[$val->area]['PI_BARU'] = $val_c1->PI_BARU;
            $query[$val->area]['COMP_CANCEL'] = $val_c1->COMP_CANCEL;
          }
        } else {
          $query['NON AREA']['PI_PAGI'] = $val_c1->PI_PAGI;
          $query['NON AREA']['FO_PAGI'] = $val_c1->FO_PAGI;
          $query['NON AREA']['PI_BARU'] = $val_c1->PI_BARU;
          $query['NON AREA']['COMP_CANCEL'] = $val_c1->COMP_CANCEL;
        }
      }

      foreach ($query2 as $val_c2) {
        if (!empty($val_c2->area)) {
          if ($val_c2->area == $val->area) {
            $query[$val->area]['antrian_und'] = $val_c2->antrian_und;
          }
        } else {
          $query['NON AREA']['antrian_und'] = $val_c2->antrian_und;
        }
      }

      foreach ($query3 as $val_c3) {
        if (!empty($val_c3->area)) {
          if ($val_c3->area == $val->area) {
            $query[$val->area]['antrian_np'] = $val_c3->antrian_np;
          }
        } else {
          $query['NON AREA']['antrian_np'] = $val_c3->antrian_np;
        }
      }

      foreach ($query4 as $val_c4) {
        if (!empty($val_c4->area)) {
          if ($val_c4->area == $val->area) {
            $query[$val->area]['potensi_kendala_kp'] = $val_c4->potensi_kendala_kp;
          }
        } else {
          $query['NON AREA']['potensi_kendala_kp'] = $val_c4->potensi_kendala_kp;
        }
      }

      foreach ($query5 as $val_c5) {
        if (!empty($val_c5->area)) {
          if ($val_c5->area == $val->area) {
            $query[$val->area]['potensi_kendala_kt'] = $val_c5->potensi_kendala_kt;
          }
        } else {
          $query['NON AREA']['potensi_kendala_kt'] = $val_c5->potensi_kendala_kt;
        }
      }

      foreach ($query6 as $val_c6) {
        if (!empty($val_c6->area)) {
          if ($val_c6->area == $val->area) {
            $query[$val->area]['potensi_ps'] = $val_c6->potensi_ps;
            $query[$val->area]['potensi_ps_pda'] = $val_c6->potensi_ps_pda;
          }
        } else {
          $query['NON AREA']['potensi_ps'] = $val_c6->potensi_ps;
          $query['NON AREA']['potensi_ps_pda'] = $val_c6->potensi_ps_pda;
        }
      }

      foreach ($query7 as $val_c7) {
        if (!empty($val_c7->area)) {
          if ($val_c7->area == $val->area) {
            $query[$val->area]['potensi_progress'] = $val_c7->potensi_progress;
            $query[$val->area]['potensi_progress_pda'] = $val_c7->potensi_progress_pda;
          }
        } else {
          $query['NON AREA']['potensi_progress'] = $val_c7->potensi_progress;
          $query['NON AREA']['potensi_progress_pda'] = $val_c7->potensi_progress_pda;
        }
      }

      foreach ($query8 as $val_c8) {
        if (!empty($val_c8->area)) {
          if ($val_c8->area == $val->area) {
            $query[$val->area]['actcomp'] = $val_c8->actcomp;
          }
        } else {
          $query['NON AREA']['actcomp'] = $val_c8->actcomp;
        }
      }

      foreach ($query9 as $val_c9) {
        if (!empty($val_c9->area)) {
          if ($val_c9->area == $val->area) {
            $query[$val->area]['ps'] = $val_c9->ps;
          }
        } else {
          $query['NON AREA']['ps'] = $val_c9->ps;
        }
      }

      foreach ($query10 as $val_c10) {
        if (!empty($val_c10->area)) {
          if ($val_c10->area == $val->area) {
            $query[$val->area]['start_cancel'] = $val_c10->start_cancel;
          }
        } else {
          $query['NON AREA']['start_cancel'] = $val_c10->start_cancel;
        }
      }

      foreach ($query11 as $val_c11) {
        if (!empty($val_c11->area)) {
          if ($val_c11->area == $val->area) {
            $query[$val->area]['potensi_progress_risma'] = $val_c11->potensi_progress_risma;
          }
        } else {
          $query['NON AREA']['potensi_progress_risma'] = $val_c11->potensi_progress_risma;
        }
      }

      foreach ($query12 as $val_c12) {
        if (!empty($val_c12->area)) {
          if ($val_c12->area == $val->area) {
            $query[$val->area]['prabac_ps_h1'] = $val_c12->prabac_ps_h1;
            $query[$val->area]['prabac_ps_hi'] = $val_c12->prabac_ps_hi;
          }
        } else {
          $query['NON AREA']['prabac_ps_h1'] = $val_c12->prabac_ps_h1;
          $query['NON AREA']['prabac_ps_hi'] = $val_c12->prabac_ps_hi;
        }
      }

      foreach ($query13 as $val_c13) {
        if (!empty($val_c13->area)) {
          if ($val_c13->area == $val->area) {
            $query[$val->area]['bypass_pda_hd'] = $val_c13->bypass_pda_hd;
          }
        } else {
          $query['NON AREA']['bypass_pda_hd'] = $val_c13->bypass_pda_hd;
        }
      }

    }

    return $query;

	}

  public static function new_potensi_ps_egbis($group)
  {
    switch ($group) {
      case 'SEKTOR':
          $area = DB::select('SELECT title as area FROM group_telegram WHERE ket_posisi = "PROV" AND title != "TERRITORY BASE HELPDESK KALSEL" GROUP BY title');
          $alias = 'gt.title';
        break;

      case 'PROV':
        $area = DB::select('SELECT sektor_prov as area FROM maintenance_datel GROUP BY sektor_prov');
        $alias = 'md.sektor_prov';
        break;
      
      case 'DATEL':
        $area = DB::select('SELECT datel as area FROM maintenance_datel GROUP BY datel');
        $alias = 'md.datel';
        break;
      
      case 'HERO':
          $area = DB::select('SELECT HERO as area FROM maintenance_datel GROUP BY HERO');
          $alias = 'md.HERO';
        break;
    }

    $query1 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN cda.type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as PI_PAGI,
          SUM(CASE WHEN cda.type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as FO_PAGI,
          SUM(CASE WHEN cda.type_grab = "PI_BARU" THEN 1 ELSE 0 END) as PI_BARU,
          SUM(CASE WHEN cda.STATUS_RESUME IN ("CANCEL COMPLETED","REVOKE COMPLETED") THEN 1 ELSE 0 END) as COMP_CANCEL
      FROM comparin_data_api cda
      LEFT JOIN maintenance_datel md ON cda.STO = md.sto
      LEFT JOIN dispatch_teknisi dt ON cda.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
          md.witel = "KALSEL"
      GROUP BY ' . $alias . '
    ');

    $query2 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as antrian_und
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dt.Ndem IS NULL AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '") AND
          (dps.jenisPsb LIKE "AO%" OR dps.jenisPsb LIKE "PDA LOCAL%") AND
          dps.orderStatusId IN (1202, 51, 300) AND
          dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query3 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as antrian_np
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND (pl.status_laporan = 6 OR pl.status_laporan IS NULL) AND
          dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query4 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as potensi_kendala_kp
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1202, 51) AND
          pl.status_laporan IN (11,13,16,23,48,49,55,56,71,72,73,106,107) AND
          DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '" AND
          dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query5 = DB::select('
      SELECT
          ' . $alias . ' as area,
          COUNT(*) as potensi_kendala_kt
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1202, 51) AND pl.status_laporan IN (2,10,15,24,25,26,27,34,35,36,42,46,47,51,57,64,65,75,76,77,81,84,87,108,110,112,113) AND DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '" AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query6 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN dps.jenisPsb LIKE "AO%" THEN 1 ELSE 0 END) as potensi_ps,
          SUM(CASE WHEN dps.jenisPsb LIKE "PDA%" THEN 1 ELSE 0 END) as potensi_ps_pda
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.provider NOT LIKE "DCS%" AND
          ((
            (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d' ,strtotime("-1 month")).'" AND "'.date('Y-m-d').'") AND
            dps.orderStatusId IN ("-1400", "50", "1205", "1206", "1400", "1401", "1804")
          )
          OR
          (
            dps.orderStatusId = "1500" AND DATE(dps.orderDatePs) = "'.date('Y-m-d').'"
          ))
      GROUP BY ' . $alias . '
    ');

    $query7 = DB::select('
      SELECT
          ' . $alias . ' as area,
          SUM(CASE WHEN dps.jenisPsb LIKE "AO%" THEN 1 ELSE 0 END) as potensi_progress,
          SUM(CASE WHEN dps.jenisPsb LIKE "PDA%" THEN 1 ELSE 0 END) as potensi_progress_pda
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.orderStatusId NOT IN (1205, 1206, 1500, 50) AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29) AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query8 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as actcomp
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND YEAR(dps.orderDate) = "' . date('Y') . '" AND dps.orderStatusId IN (1300, 1205) AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query9 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as ps
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN (1500, 1203, 7) AND ((DATE(dps.orderDatePs) = "' . date('Y-m-d') . '") OR (DATE(dps.orderDate) = "' . date('Y-m-d') . '") AND dps.orderDatePs = "0000-00-00 00:00:00") AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query10 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as start_cancel
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "AO%" AND DATE(dps.orderDate) = "' . date('Y-m-d') . '" AND dps.orderStatusId IN (1600, 1700) AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    $query11 = DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as potensi_progress_risma
      FROM risma_prov_witel rpw
      LEFT JOIN dispatch_teknisi dt ON rpw.trackID_int = dt.NO_ORDER AND dt.jenis_order = "MYIR"
      LEFT JOIN psb_myir_wo pmw ON rpw.trackID_int = pmw.myir
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          rpw.status_data = "Fresh" AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)
    ');

    $query12 =   DB::select('
      SELECT
          ' . $alias .' as area,
          SUM(CASE WHEN (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d', strtotime("-1 days")).'") THEN 1 ELSE 0 END) as prabac_ps_h1,
          SUM(CASE WHEN (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as prabac_ps_hi
      FROM prabac_psharian_indihome pshi
      LEFT JOIN dispatch_teknisi dt ON pshi.order_id_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pshi.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL"
      GROUP BY ' . $alias . '
    ');

    $query13 =   DB::select('
      SELECT
          ' . $alias .' as area,
          COUNT(*) as bypass_pda_hd
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      WHERE
          md.witel = "KALSEL" AND
          dps.jenisPsb LIKE "PDA%" AND
          (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-30 days")) . '" AND "' . date('Y-m-d') . '") AND
          dps.orderStatusId IN ("1202", "-1201") AND
          pl.status_laporan IN (1, 4) AND dps.provider NOT LIKE "DCS%"
      GROUP BY ' . $alias . '
    ');

    foreach ($area as $val)
    {
      foreach ($query1 as $val_c1)
      {
        if (!empty($val_c1->area))
        {
          if ($val_c1->area == $val->area)
          {
            $query[$val->area]['PI_PAGI'] = $val_c1->PI_PAGI;
            $query[$val->area]['FO_PAGI'] = $val_c1->FO_PAGI;
            $query[$val->area]['PI_BARU'] = $val_c1->PI_BARU;
            $query[$val->area]['COMP_CANCEL'] = $val_c1->COMP_CANCEL;
          }
        } else {
          $query['NON AREA']['PI_PAGI'] = $val_c1->PI_PAGI;
          $query['NON AREA']['FO_PAGI'] = $val_c1->FO_PAGI;
          $query['NON AREA']['PI_BARU'] = $val_c1->PI_BARU;
          $query['NON AREA']['COMP_CANCEL'] = $val_c1->COMP_CANCEL;
        }
      }

      foreach ($query2 as $val_c2) {
        if (!empty($val_c2->area)) {
          if ($val_c2->area == $val->area) {
            $query[$val->area]['antrian_und'] = $val_c2->antrian_und;
          }
        } else {
          $query['NON AREA']['antrian_und'] = $val_c2->antrian_und;
        }
      }

      foreach ($query3 as $val_c3) {
        if (!empty($val_c3->area)) {
          if ($val_c3->area == $val->area) {
            $query[$val->area]['antrian_np'] = $val_c3->antrian_np;
          }
        } else {
          $query['NON AREA']['antrian_np'] = $val_c3->antrian_np;
        }
      }

      foreach ($query4 as $val_c4) {
        if (!empty($val_c4->area)) {
          if ($val_c4->area == $val->area) {
            $query[$val->area]['potensi_kendala_kp'] = $val_c4->potensi_kendala_kp;
          }
        } else {
          $query['NON AREA']['potensi_kendala_kp'] = $val_c4->potensi_kendala_kp;
        }
      }

      foreach ($query5 as $val_c5) {
        if (!empty($val_c5->area)) {
          if ($val_c5->area == $val->area) {
            $query[$val->area]['potensi_kendala_kt'] = $val_c5->potensi_kendala_kt;
          }
        } else {
          $query['NON AREA']['potensi_kendala_kt'] = $val_c5->potensi_kendala_kt;
        }
      }

      foreach ($query6 as $val_c6) {
        if (!empty($val_c6->area)) {
          if ($val_c6->area == $val->area) {
            $query[$val->area]['potensi_ps'] = $val_c6->potensi_ps;
            $query[$val->area]['potensi_ps_pda'] = $val_c6->potensi_ps_pda;
          }
        } else {
          $query['NON AREA']['potensi_ps'] = $val_c6->potensi_ps;
          $query['NON AREA']['potensi_ps_pda'] = $val_c6->potensi_ps_pda;
        }
      }

      foreach ($query7 as $val_c7) {
        if (!empty($val_c7->area)) {
          if ($val_c7->area == $val->area) {
            $query[$val->area]['potensi_progress'] = $val_c7->potensi_progress;
            $query[$val->area]['potensi_progress_pda'] = $val_c7->potensi_progress_pda;
          }
        } else {
          $query['NON AREA']['potensi_progress'] = $val_c7->potensi_progress;
          $query['NON AREA']['potensi_progress_pda'] = $val_c7->potensi_progress_pda;
        }
      }

      foreach ($query8 as $val_c8) {
        if (!empty($val_c8->area)) {
          if ($val_c8->area == $val->area) {
            $query[$val->area]['actcomp'] = $val_c8->actcomp;
          }
        } else {
          $query['NON AREA']['actcomp'] = $val_c8->actcomp;
        }
      }

      foreach ($query9 as $val_c9) {
        if (!empty($val_c9->area)) {
          if ($val_c9->area == $val->area) {
            $query[$val->area]['ps'] = $val_c9->ps;
          }
        } else {
          $query['NON AREA']['ps'] = $val_c9->ps;
        }
      }

      foreach ($query10 as $val_c10) {
        if (!empty($val_c10->area)) {
          if ($val_c10->area == $val->area) {
            $query[$val->area]['start_cancel'] = $val_c10->start_cancel;
          }
        } else {
          $query['NON AREA']['start_cancel'] = $val_c10->start_cancel;
        }
      }

      foreach ($query11 as $val_c11) {
        if (!empty($val_c11->area)) {
          if ($val_c11->area == $val->area) {
            $query[$val->area]['potensi_progress_risma'] = $val_c11->potensi_progress_risma;
          }
        } else {
          $query['NON AREA']['potensi_progress_risma'] = $val_c11->potensi_progress_risma;
        }
      }

      foreach ($query12 as $val_c12) {
        if (!empty($val_c12->area)) {
          if ($val_c12->area == $val->area) {
            $query[$val->area]['prabac_ps_h1'] = $val_c12->prabac_ps_h1;
            $query[$val->area]['prabac_ps_hi'] = $val_c12->prabac_ps_hi;
          }
        } else {
          $query['NON AREA']['prabac_ps_h1'] = $val_c12->prabac_ps_h1;
          $query['NON AREA']['prabac_ps_hi'] = $val_c12->prabac_ps_hi;
        }
      }

      foreach ($query13 as $val_c13) {
        if (!empty($val_c13->area)) {
          if ($val_c13->area == $val->area) {
            $query[$val->area]['bypass_pda_hd'] = $val_c13->bypass_pda_hd;
          }
        } else {
          $query['NON AREA']['bypass_pda_hd'] = $val_c13->bypass_pda_hd;
        }
      }

    }

    return $query;

	}



  public static function new_potensi_ps($date){
    $query = DB::SELECT('
      SELECT
      a.datel,
      (select count(*) from psb_myir_wo aa
      LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
      LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
      where
      date(aa.orderDate) >= "2020-04-01" AND
      ff.orderId IS NULL AND
      aa.status_approval <> "2" AND
      aa.sc IS NULL AND
      aa.layanan <> "PDA" AND
      (cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND
      ee.datel = a.datel) as SISA_WO_PRA,
      (select count(*) from psb_myir_wo aa
      LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
      LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
      where
      date(aa.orderDate) >= "2020-04-01" AND
      ff.orderId IS NULL AND
      aa.sc IS NULL AND
      aa.status_approval <> "2" AND
      aa.layanan = "PDA" AND
      (cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND
      ee.datel = a.datel) as SISA_WO_PDA,
            (select count(*) from Data_Pelanggan_Starclick aa
      LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
      LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
      LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
      LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
      LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
      where
      ee.datel = a.datel AND
      aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
      aa.jenisPsb LIKE "AO%" AND
      date(aa.orderDate) >= "2020-04-01" AND
      ((cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND (ccc.id is null OR ddd.grup IN ("SISA","OGP")))) as SISA_WO,
      (select count(*) from Data_Pelanggan_Starclick aa
      LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
      LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
      LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
      LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
      where
      ee.datel = a.datel AND
      aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
      aa.jenisPsb LIKE "MO%" AND
      date(aa.orderDate) >= "2020-04-01" AND
      (cc.id is NULL OR dd.grup IN ("SISA","OGP"))) as SISA_WO_MO,
      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      c.`laporan_status`="HR" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.datel=a.datel)+(SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      d.orderDatePs = "" AND
      c.`laporan_status`="HR" AND
      d.orderid IS NOT NULL AND
      d.orderStatusId NOT IN ("1500","7","1300","1205") AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.datel=a.datel) as HR,
      (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.datel = a.datel AND ((dpsa.orderStatusId in ("1500","7")  AND DATE(dpsa.orderDatePs) = "'.$date.'" ) OR (DATE(dpsa.orderDate) >="2020-03-01" AND dpsa.orderStatusId IN ("1203","1401"))) ) as PS,
      (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE DATE(dpsa.orderDate) >="2020-03-01"  AND dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.datel = a.datel AND dpsa.orderStatusId IN ("1300","1205") ) as ACTCOMP
      FROM
        maintenance_datel a
      WHERE
        a.witel = "KALSEL"
      GROUP BY a.datel
    ');
    return $query;
  }

	public static function dashboardProgressSCBE(){
		$result = DB::SELECT('
			SELECT
				a.sektor_prov as datel,
				(
					SELECT count(*) FROM Data_Pelanggan_Starclick_Backend aa
					LEFT JOIN psb_myir_wo cc ON cc.myir = SUBSTR(aa.ORDER_CODE,6,30)
					LEFT JOIN maintenance_datel bb ON SUBSTR(aa.LOC_ID,5,3) = bb.sto
					LEFT JOIN dispatch_teknisi dd ON cc.myir = dd.Ndem
					WHERE
					aa.STATUS_RESUME NOT IN ("Provisioning SC","Cancel","Cancel Pre Order") AND
					bb.sektor_prov = a.sektor_prov AND
					date(aa.ORDER_DATE) >= "2020-11-30" AND
					cc.myir is NOT NULL AND
					dd.id is NULL
				) as scbe_undispatch,
				(
					SELECT count(*) FROM Data_Pelanggan_Starclick_Backend aa
					LEFT JOIN psb_myir_wo cc ON cc.myir = SUBSTR(aa.ORDER_CODE,6,30)
					LEFT JOIN maintenance_datel bb ON SUBSTR(aa.LOC_ID,5,3) = bb.sto
					LEFT JOIN dispatch_teknisi dd ON cc.myir = dd.Ndem
					WHERE
					aa.STATUS_RESUME NOT IN ("Provisioning SC","Cancel","Cancel Pre Order") AND
					bb.sektor_prov = a.sektor_prov AND
					date(aa.ORDER_DATE) >= "2020-11-30" AND
					cc.myir is NOT NULL AND
					dd.id is NOT NULL
				) as scbe_dispatched
			FROM
				maintenance_datel a
			WHERE
				a.witel = "KALSEL"
			GROUP BY a.sektor_prov
		');
		return $result;
	}

	public static function scbe_undispatch($sektor){
		if ($sektor=="ALL") {
			$where_sektor = '';
		} else {
			$where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
		}
		$query = DB::SELECT('
		SELECT
		aa.ORDER_CODE as orderId,
		aa.CUSTOMER_NAME as orderName,
		aa.LOC_ID as alproname,
		aa.ORDER_DATE as dps_orderDate,
		aa.STATUS_RESUME as orderStatus,
		b.sektor_prov,
		SUBSTR(aa.ORDER_CODE,6,30) as sto,
		cc.alamatLengkap as orderAddr,
		aa.GPS_LATITUDE as lat,
		aa.GPS_LONGITUDE as lon,
		pl.kordinat_pelanggan,
		pl.catatan,
		pl.valins_id,
		cc.alamatLengkap,
		aa.ORDER_DATE as orderDate,
		cc.id as my_id,
		cc.myir
		FROM Data_Pelanggan_Starclick_Backend aa
    LEFT JOIN psb_myir_wo cc ON cc.myir = SUBSTR(aa.ORDER_CODE,6,30)
		LEFT JOIN dispatch_teknisi dt ON dt.Ndem = cc.myir
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON SUBSTR(cc.namaOdp,5,3) = b.sto
		WHERE
		'.$where_sektor.'
		aa.STATUS_RESUME NOT IN ("Provisioning SC","Cancel","Cancel Pre Order") AND
		date(aa.ORDER_DATE) >= "2020-11-30" AND
		cc.myir is NOT NULL AND
		dt.id is NULL
		ORDER BY aa.ORDER_DATE
		');
		return $query;
	}

	public static function mappingScAO()
  {

    $week = date('Y-m-d', strtotime(date('Y-m-d')." -7 days"));
    $yesterday = date('Y-m-d', strtotime(date('Y-m-d')." -1 days"));
		$result = DB::SELECT('
		SELECT
			a.sektor_prov as datel,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			WHERE
			dt.id is NULL AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'"
			) as PI_UNDISPATCH,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			((pl.id is NULL AND dt.id IS NOT NULL) OR (pls.grup = "NP")) AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as PI_NEEDPROGRESS,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
      NOT pls.laporan_status IN ("PERMINTAAN REVOKE","LANJUT PT1") AND
			pls.grup IN ("SISA","OGP")  AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as PI_PROGRESS,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.laporan_status_id IN (2,4,10,11,15,23,26,27,35,36,45,46,47,48,49,50,51,52,55,64,65,72,73,84,87) AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as PI_KENDALA_FUP,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.laporan_status_id IN (56,85,71,13,60,66,77,81,9,25,24,83,34,16) AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as PI_KENDALA_CANCEL,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
						LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.grup IN ("HR")  AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as HR,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			WHERE
			dt.id is NULL AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'"
			) as FWFM_UNDISPATCH,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			((pl.id is NULL AND dt.id IS NOT NULL) OR (pls.grup = "NP"))  AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as FWFM_NEEDPROGRESS,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
      NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
			pls.grup IN ("SISA","OGP")  AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as FWFM_PROGRESS,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.laporan_status_id IN (2,4,10,11,15,23,26,27,35,36,45,46,47,48,49,50,51,52,55,64,65,72,73,84,87) AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as FWFM_KENDALA_FUP,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.laporan_status_id IN (56,85,71,13,60,66,77,81,9,25,24,83,34,16) AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as FWFM_KENDALA_CANCEL,
			(SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			pls.grup IN ("HR")  AND
			b.sektor_prov = a.sektor_prov AND
			dps.orderStatus IN ("Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			YEAR(dps.orderDate) = "'.date('Y').'") as FWFM_HR,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
			WHERE
			dt.Ndem IS NOT NULL  AND
			b.sektor_prov = a.sektor_prov AND
			pls.laporan_status IN ("KENDALA ALPRO","KORDINAT PELANGGAN SALAH JARAK ISTIQOMAH TIDAK SPEK","CANCEL","ODP FULL","PERMINTAAN BATAL PELANGGAN","DOUBLE INPUT","INPUT ULANG","KENDALA IZIN HS","ORDER MAINTENANCE","ODP BELUM GO LIVE","INSERT TIANG","FOLLOW UP TL","INDIKASI CABUT PASANG","PENDING H+") AND
      dps.orderStatus IN ("OSS CANCEL COMPLETE","NCX CANCEL COMPLETE") AND
			dps.jenisPsb LIKE "AO%" AND
			(DATE(pl.modified_at) = "'.date('Y-m-d').'")) as CANCEL_ORDER,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			WHERE
			dt.id IS NULL AND
			b.sektor_prov = a.sektor_prov AND
      dps.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			(DATE(dps.orderDate) BETWEEN "'.$week.'" AND "'.$yesterday.'")
			) as UNDISPATCH_H1,
      (SELECT COUNT(*) FROM Data_Pelanggan_Starclick dps
			LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
			LEFT JOIN regu r ON dt.id_regu = r.id_regu
			LEFT JOIN maintenance_datel b ON dps.sto = b.sto
			WHERE
			dt.id IS NULL AND
			b.sektor_prov = a.sektor_prov AND
      dps.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
			dps.jenisPsb LIKE "AO%" AND
			(DATE(dps.orderDate) = "'.date('Y-m-d').'")
			) as UNDISPATCH_HI
			FROM
			maintenance_datel a
			WHERE
			a.witel = "KALSEL"
			GROUP BY a.sektor_prov

		');
		return $result;
	}

	public static function umur_fwfm($grup,$umur,$sektor){
		$where_umur = '';
		switch ($umur) {
			case "kurang1hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<24 AND ';
			break;
			case "1sd2hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<48 AND ';
			break;
			case "2sd3hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<60 AND ';
			break;
			case "3sd7hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=60 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<168 AND ';
			break;
			case "lebih7hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=168 AND ';
			break;

		}

		if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'a.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT
			*,
			dt.id as id_dt,
			a.sto
		FROM
		Data_Pelanggan_Starclick b
		LEFT JOIN maintenance_datel a ON b.sto = a.sto
		LEFT JOIN dispatch_teknisi dt ON b.orderId = dt.Ndem
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		WHERE
		'.$where_umur.'
		'.$where_sektor.'
		b.orderStatus IN ("Fallout (WFM)") AND
		b.jenisPsb LIKE "AO%" AND
		date(b.orderDate) >= "2020-11-01"
		');
		return $query;
	}

	public static function umur_pi($grup,$umur,$sektor){
		$where_umur = '';
		switch ($umur) {
			case "kurang1hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<24 AND ';
			break;
			case "1sd2hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<48 AND ';
			break;
			case "2sd3hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<60 AND ';
			break;
			case "3sd7hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=60 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<168 AND ';
			break;
			case "lebih7hari" :
				$where_umur = 'TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=168 AND ';
			break;

		}

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'a.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT
			*,
			dt.id as id_dt,
			a.sto
		FROM
		Data_Pelanggan_Starclick b
		LEFT JOIN maintenance_datel a ON b.sto = a.sto
		LEFT JOIN dispatch_teknisi dt ON b.orderId = dt.Ndem
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		WHERE
		'.$where_umur.'
		'.$where_sektor.'
		b.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		b.jenisPsb LIKE "AO%" AND
		date(b.orderDate) >= "2020-11-01"
		');
		return $query;
	}

	public static function SCAO_byumur(){
		$query = DB::SELECT('
		SELECT
		SUM(CASE WHEN  b.orderStatus IN ("OSS PROVISIONING ISSUED") THEN 1 ELSE 0 END) as jumlah_PI,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<4 THEN 1 ELSE 0 END) as pi_comply,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=4 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as pi_kurang1hari,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as pi_1sd2hari,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<60 THEN 1 ELSE 0 END) as pi_2sd3hari,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=60 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as pi_3sd7hari,
		SUM(CASE WHEN b.orderStatus IN ("OSS PROVISIONING ISSUED") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=168 THEN 1 ELSE 0 END) as pi_lebih7hari,
		SUM(CASE WHEN  b.orderStatus IN ("OSS PROVISIONING ISSUED") THEN 1 ELSE 0 END) as jumlah_pi,
		SUM(CASE WHEN  b.orderStatus IN ("Fallout (WFM)") THEN 1 ELSE 0 END) as jumlah_fwfm,
		SUM(CASE WHEN b.orderStatus IN ("Fallout (WFM)") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as fwfm_kurang1hari,
		SUM(CASE WHEN b.orderStatus IN ("Fallout (WFM)") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=24 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<48 THEN 1 ELSE 0 END) as fwfm_1sd2hari,
		SUM(CASE WHEN b.orderStatus IN ("Fallout (WFM)") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=48 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<60 THEN 1 ELSE 0 END) as fwfm_2sd3hari,
		SUM(CASE WHEN b.orderStatus IN ("Fallout (WFM)") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=60 AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )<168 THEN 1 ELSE 0 END) as fwfm_3sd7hari,
		SUM(CASE WHEN b.orderStatus IN ("Fallout (WFM)") AND TIMESTAMPDIFF( HOUR , b.orderDate,  "'.date('Y-m-d H:i:s').'" )>=168 THEN 1 ELSE 0 END) as fwfm_lebih7hari,
		a.sektor_prov as datel
		FROM
		maintenance_datel a
		LEFT JOIN Data_Pelanggan_Starclick b ON a.sto = b.sto
		WHERE
		YEAR(b.orderDate) = "'.date('Y').'" AND
		b.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
		b.jenisPsb LIKE "AO%" AND
		a.witel = "KALSEL"
		GROUP BY a.sektor_prov
		');
		return $query;
	}

	public static function undispatch_sc($kat,$sektor)
  {

    $week = date('Y-m-d', strtotime(date('Y-m-d')." -7 days"));
    $yesterday = date('Y-m-d', strtotime(date('Y-m-d')." -1 days"));

		if($kat == "H1")
    {
      $where_kat = 'AND (DATE(dps.orderDate) BETWEEN "'.$week.'" AND "'.$yesterday.'")';
    } elseif ($kat == "HI")
    {
      $where_kat = 'AND (DATE(dps.orderDate) = "'.date('Y-m-d').'")';
    }
    if ($sektor == "ALL") {
      $where_sektor = 'AND b.sektor_prov IN ("SEKTOR BBR MTP","SEKTOR BJM KYG","SEKTOR BLC","SEKTOR KDG","SEKTOR LUL","SEKTOR PLE","SEKTOR TJL","SEKTOR ULI GMB")';
    } else {
      $where_sektor = 'AND b.sektor_prov = "'.$sektor.'"';
    }

		return DB::SELECT('
		SELECT *, dps.orderDate as dps_orderDate, DATE(dps.orderDate) as tgl_orderDate, TIME(dps.orderDate) as time_orderDate, dps.lat as dps_lat, dps.lon as dps_lon
    FROM Data_Pelanggan_Starclick dps
    LEFT JOIN comparin_data_api cda ON dps.orderIdInteger = cda.ORDER_ID
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		WHERE
		dt.id IS NULL AND
		dps.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%"
    '.$where_kat.'
    '.$where_sektor.'
		ORDER BY dps.orderDate
		');
	}

  public static function undispatch_kpro($kat,$sektor)
  {

    $week = date('Y-m-d', strtotime(date('Y-m-d')." -7 days"));
    $yesterday = date('Y-m-d', strtotime(date('Y-m-d')." -1 days"));

		if($kat == "H1")
    {
      $where_kat = 'AND (DATE(pkt.order_date) BETWEEN "'.$week.'" AND "'.$yesterday.'")';
    } elseif ($kat == "HI")
    {
      $where_kat = 'AND (DATE(pkt.order_date) = "'.date('Y-m-d').'")';
    }
    if ($sektor == "ALL") {
      $where_sektor = 'AND md.sektor_prov IN ("SEKTOR BBR MTP","SEKTOR BJM KYG","SEKTOR BLC","SEKTOR KDG","SEKTOR LUL","SEKTOR PLE","SEKTOR TJL","SEKTOR ULI GMB")';
    } else {
      $where_sektor = 'AND md.sektor_prov = "'.$sektor.'"';
    }

		return DB::SELECT('
		SELECT
    pkt.*,
    md.sektor_prov
    FROM provi_kpro_tr6 pkt
    LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER
    LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
		WHERE
    dt.id IS NULL AND
		pkt.status_resume IN ("7 | OSS - PROVISIONING ISSUED","Fallout (WFM)") AND
		pkt.jenispsb = "AO"
    '.$where_kat.'
    '.$where_sektor.'
		ORDER BY pkt.order_date DESC
		');
	}

	public static function fwfm_needprogress($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		((pl.id is NULL AND dt.id IS NOT NULL) OR (pls.grup = "NP"))  AND
		'.$where_sektor.'
		dps.orderStatus IN ("Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function fwfm_progress($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
    NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
		pls.grup IN ("SISA","OGP")  AND
		'.$where_sektor.'
		dps.orderStatus IN ("Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function fwfm_kendala($grup,$sektor,$status){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

    if ($status=="FUP") {
      $where_status = 'pls.laporan_status_id IN (2,4,10,11,15,23,26,27,35,36,45,46,47,48,49,50,51,52,55,64,65,72,73,84,87) AND';
    } elseif($status=="CANCEL") {
      $where_status = 'pls.laporan_status_id IN (56,85,71,13,60,66,77,81,9,25,24,83,34,16) AND';
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		'.$where_status.'
		'.$where_sektor.'
		dps.orderStatus IN ("Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function fwfm_hr($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		pls.grup IN ("HR")  AND
		'.$where_sektor.'
		dps.orderStatus IN ("Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function pi_needprogress($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		((pl.id is NULL AND dt.id IS NOT NULL) OR (pls.grup = "NP"))  AND
		'.$where_sektor.'
		dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function pi_all($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		'.$where_sektor.'
    NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
		dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function fwfm_all($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		'.$where_sektor.'
    NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
		dps.orderStatus IN ("Fallout (WFM)") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function pi_progress($grup,$sektor){

		if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
    NOT pls.laporan_status IN ("PERMINTAAN REVOKE","LANJUT PT1") AND
		pls.grup IN ("SISA","OGP")  AND
		'.$where_sektor.'
		dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function pi_kendala($grup,$sektor,$status){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

    if ($status=="FUP") {
      $where_status = 'pls.laporan_status_id IN (2,4,10,11,15,23,26,27,35,36,45,46,47,48,49,50,51,52,55,64,65,72,73,84,87) AND';
    } elseif ($status=="CANCEL") {
      $where_status = 'pls.laporan_status_id IN (56,85,71,13,60,66,77,81,9,25,24,83,34,16) AND';
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		'.$where_status.'
		'.$where_sektor.'
		dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

	public static function pi_hr($grup,$sektor){

    if ($grup=="DATEL") {
      if ($sektor=="ALL") {
        $where_sektor = '';
      } else {
        $where_sektor = 'b.sektor_prov = "'.$sektor.'" AND';
      }
    } elseif ($grup=="MITRA") {
      if ($sektor=="ALL") {
        $where_sektor = 'ma.kat = "DELTA" AND';
      } else {
        $where_sektor = 'ma.mitra_amija_pt = "'.$sektor.'" AND ma.kat = "DELTA" AND';
      }
    }

		$query = DB::SELECT('
		SELECT *,dt.id as id_dt, pl.kordinat_pelanggan, pl.catatan, pl.valins_id, pmw.alamatLengkap, dps.orderDate as dps_orderDate  FROM Data_Pelanggan_Starclick dps
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
		LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dps.orderIdInteger
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN maintenance_datel b ON dps.sto = b.sto
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		pls.grup IN ("HR")  AND
		'.$where_sektor.'
		dps.orderStatus IN ("OSS PROVISIONING ISSUED") AND
		dps.jenisPsb LIKE "AO%" AND
		YEAR(dps.orderDate) = "'.date('Y').'"
		ORDER BY dps.orderDate
		');
		return $query;
	}

  public static function cancelOrder_all($grup, $sektor)
  {
    switch ($grup) {
      case 'SEKTOR':

        $select = 'gt.title as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = '';
            break;

          case 'NON AREA':
            $where_sektor = 'gt.title IS NULL AND';
            break;
          
          default:
            $where_sektor = 'gt.title = "' . $sektor . '" AND';
            break;
        }
        break;

      case 'PROV':

        $select = 'b.sektor_prov as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = '';
            break;

          case 'NON AREA':
            $where_sektor = 'b.sektor_prov IS NULL AND';
            break;

          default:
            $where_sektor = 'b.sektor_prov = "' . $sektor . '" AND';
            break;
        }
        break;

      case 'EGBIS':

        $select = 'b.sektor_prov as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = 'AND dps.provider NOT LIKE "DCS%"';
            break;

          case 'NON AREA':
            $where_sektor = 'AND dps.provider NOT LIKE "DCS%" AND b.sektor_prov IS NULL AND';
            break;

          default:
            $where_sektor = 'AND dps.provider NOT LIKE "DCS%" AND b.sektor_prov = "' . $sektor . '" AND';
            break;
        }
        break;

      case 'DATEL':

        $select = 'b.datel as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = '';
            break;

          case 'NON AREA':
            $where_sektor = 'b.datel IS NULL AND';
            break;

          default:
            $where_sektor = 'b.datel = "' . $sektor . '" AND';
            break;
        }
        break;
      
      case 'MITRA':

        $select = 'ma.mitra_amija_pt as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = 'ma.kat = "DELTA" AND';
            break;

          case 'NON AREA':
            $where_sektor = 'ma.mitra_amija_pt IS NULL AND ma.kat = "DELTA" AND';
            break;

          default:
            $where_sektor = 'ma.mitra_amija_pt = "' . $sektor . '" AND ma.kat = "DELTA" AND';
            break;
        }
        break;
      
      case 'SCNCX':

        $select = 'b.sektor_prov as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = 'b.sektor_prov IS NOT NULL';
            break;

          case 'NON AREA':
            $where_sektor = 'b.sektor_prov IS NULL';
            break;

          default:
            $where_sektor = 'b.sektor_prov = "' . $sektor . '"';
            break;
        }
        break;
      
      case 'HERO':

        $select = 'b.HERO as area,';

        switch ($sektor) {
          case 'ALL':
            $where_sektor = 'b.HERO IS NOT NULL';
            break;

          case 'NON AREA':
            $where_sektor = 'b.HERO IS NULL';
            break;

          default:
            $where_sektor = 'b.HERO = "' . $sektor . '"';
            break;
        }
        break;
    }

    if (in_array($grup,['SEKTOR', 'DATEL', 'MITRA', 'PROV'])) {
      $where_query = '
        dt.Ndem IS NOT NULL AND
        '.$where_sektor. '
        pls.laporan_status IN ("KENDALA ALPRO","KORDINAT PELANGGAN SALAH JARAK ISTIQOMAH TIDAK SPEK","CANCEL","ODP FULL","PERMINTAAN BATAL PELANGGAN","DOUBLE INPUT","INPUT ULANG","KENDALA IZIN HS","ORDER MAINTENANCE","ODP BELUM GO LIVE","INSERT TIANG","FOLLOW UP TL","INDIKASI CABUT PASANG","PENDING H+") AND
        dps.orderStatus IN ("OSS CANCEL COMPLETE","NCX CANCEL COMPLETE") AND
        dps.jenisPsb LIKE "AO%" AND
        (DATE(pl.modified_at) = "' . date('Y-m-d') . '")
      ';
    } elseif (in_array($grup, ['SCNCX', 'HERO'])) {
      $where_query = '
        ' . $where_sektor . ' AND
        DATE(dps.orderDate) = "' . date('Y-m-d') . '"  AND dps.jenisPsb LIKE "AO%" AND dps.orderStatusId IN ("1600","1700")
      ';
    }

		return DB::SELECT('
      SELECT
      *,
      ' . $select . '
      dt.id as id_dt,
      pl.kordinat_pelanggan,
      pl.catatan,
      pl.valins_id,
      pmw.alamatLengkap,
      dps.orderDate as dps_orderDate
      FROM Data_Pelanggan_Starclick dps
      LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
      LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel b ON dps.sto = b.sto
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      ' . $where_query . '
      ORDER BY dps.orderDate ASC
		');
	}




    public static function new_potensi_ps3($date){
      $query = DB::SELECT('
        SELECT
        a.sektor_prov as datel,
        (select count(*) from psb_myir_wo aa
  LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
  LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
  LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
  LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
  LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
  where
  date(aa.orderDate) >= "2020-04-01" AND
  ff.orderId IS NULL AND
  aa.status_approval <> "2" AND
  aa.sc IS NULL AND
  aa.layanan <> "PDA" AND
  (cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND
  ee.sektor_prov = a.sektor_prov) as SISA_WO_PRA,
  (select count(*) from psb_myir_wo aa
  LEFT JOIN dispatch_teknisi bb ON aa.myir = bb.Ndem
  LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
  LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
  LEFT JOIN maintenance_datel ee ON SUBSTR(aa.namaOdp,5,3) = ee.sto
  LEFT JOIN Data_Pelanggan_Starclick ff ON aa.myir = ff.myir
  where
  date(aa.orderDate) >= "2020-04-01" AND
  ff.orderId IS NULL AND
  aa.sc IS NULL AND
  aa.status_approval <> "2" AND
  aa.layanan = "PDA" AND
  (cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND
  ee.sektor_prov = a.sektor_prov) as SISA_WO_PDA,
        (select count(*) from Data_Pelanggan_Starclick aa
  LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
  LEFT JOIN psb_myir_wo ff ON aa.myir = ff.myir
  LEFT JOIN dispatch_teknisi bbb ON ff.myir = bbb.Ndem
  LEFT JOIN psb_laporan ccc ON bbb.id = ccc.id_tbl_mj
  LEFT JOIN psb_laporan_status ddd ON ccc.status_laporan = ddd.laporan_status_id
  LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
  LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
  LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
  where
  ee.sektor_prov = a.sektor_prov AND
  aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
  aa.jenisPsb LIKE "AO%" AND
  date(aa.orderDate) >= "2020-04-01" AND
  ((cc.id is NULL OR dd.grup IN ("SISA","OGP")) AND (ccc.id is null OR ddd.grup IN ("SISA","OGP")))) as SISA_WO,
  (select count(*) from Data_Pelanggan_Starclick aa
  LEFT JOIN dispatch_teknisi bb ON aa.orderId = bb.Ndem
  LEFT JOIN psb_laporan cc ON bb.id = cc.id_tbl_mj
  LEFT JOIN psb_laporan_status dd ON cc.status_laporan = dd.laporan_status_id
  LEFT JOIN maintenance_datel ee ON aa.sto = ee.sto
  where
  ee.sektor_prov = a.sektor_prov AND
  aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
  aa.jenisPsb LIKE "MO%" AND
  date(aa.orderDate) >= "2020-04-01" AND
  (cc.id is NULL OR dd.grup IN ("SISA","OGP"))) as SISA_WO_MO,
        (SELECT COUNT(*)  FROM dispatch_teknisi aa
        LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        c.`laporan_status`="HR" AND
        d.sc IS NULL AND
        d.myir IS NOT NULL AND g.sektor_prov=a.sektor_prov)+(SELECT COUNT(*)   FROM dispatch_teknisi aa
        LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON d.sto = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        d.orderDatePs = "" AND
        c.`laporan_status`="HR" AND
        d.orderid IS NOT NULL AND
        d.orderStatusId NOT IN ("1500","7","1300","1205") AND
        d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.sektor_prov=a.sektor_prov) as HR,
        (SELECT COUNT(*)  FROM dispatch_teknisi aa
        LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        c.`laporan_status_id`="24" AND
        d.sc IS NULL AND
        d.myir IS NOT NULL AND g.sektor_prov=a.sektor_prov)+(SELECT COUNT(*)   FROM dispatch_teknisi aa
        LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON d.sto = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        d.orderDatePs = "" AND
        c.`laporan_status_id`="24" AND
        d.orderid IS NOT NULL AND
        d.orderStatusId NOT IN ("1500","7","1300","1205") AND
        d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.sektor_prov=a.sektor_prov) as ODP_FULL,
        (SELECT COUNT(*)  FROM dispatch_teknisi aa
        LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        c.`laporan_status_id`="42" AND
        d.sc IS NULL AND
        d.myir IS NOT NULL AND g.sektor_prov=a.sektor_prov)+(SELECT COUNT(*)   FROM dispatch_teknisi aa
        LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
        LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
        LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
        LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
        LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
        LEFT JOIN maintenance_datel g ON d.sto = g.sto
        WHERE
        aa.tgl >= "2020-04-01" AND
        d.orderDatePs = "" AND
        c.`laporan_status_id`="42" AND
        d.orderid IS NOT NULL AND
        d.orderStatusId NOT IN ("1500","7","1300","1205") AND
        d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.sektor_prov=a.sektor_prov) as KENDALA_JALUR,
        (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.sektor_prov = a.sektor_prov AND ((dpsa.orderStatusId in ("1500","7")  AND DATE(dpsa.orderDatePs) = "'.$date.'" ) OR (DATE(dpsa.orderDate) >="2020-03-01" AND dpsa.orderStatusId IN ("1203","1401"))) ) as PS,
        (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE DATE(dpsa.orderDate) >="2020-03-01"  AND dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.sektor_prov = a.sektor_prov AND dpsa.orderStatusId IN ("1300","1205") ) as ACTCOMP
        FROM
          maintenance_datel a
        WHERE
          a.witel = "KALSEL"
        GROUP BY a.sektor_prov
      ');
      return $query;
    }

  public static function new_potensi_ps_old($date){
    $query = DB::SELECT('
      SELECT
      a.datel,
      (SELECT COUNT(*) AS jumlah
      FROM Data_Pelanggan_Starclick aa
      LEFT JOIN psb_myir_wo bb ON aa.myir = bb.myir AND YEAR(aa.orderDate) >= "2020"
      LEFT JOIN maintenance_datel cc ON aa.sto = cc.sto
      LEFT JOIN dispatch_teknisi dd ON (aa.orderId = dd.Ndem OR dd.Ndem = aa.myir) AND YEAR(aa.orderDate) >= "2020"
      LEFT JOIN psb_laporan ee ON dd.id = ee.id_tbl_mj
      LEFT JOIN psb_laporan_status ff ON ee.status_laporan = ff.laporan_status_id
      LEFT JOIN regu gg ON dd.id_regu = gg.id_regu
      LEFT JOIN dispatch_teknisi ddd ON bb.sc = ddd.Ndem
      LEFT JOIN psb_laporan eee ON ddd.id = eee.id_tbl_mj
      LEFT JOIN psb_laporan_status fff ON eee.status_laporan = fff.laporan_status_id
      WHERE
      aa.orderStatus IN ("OSS PROVISIONING ISSUED","Fallout (WFM)") AND
      (ff.grup IN ("SISA","OGP") OR dd.id IS NULL) AND
      (fff.grup IN ("SISA","OGP") OR ddd.id IS NULL) AND
      DATE(aa.orderDate) >= "'.date('Y-m').'-01" AND
      cc.datel = a.datel AND
      aa.jenisPsb LIKE "AO%") +
      (SELECT COUNT(*) AS jumlah FROM psb_myir_wo aa
      LEFT JOIN Data_Pelanggan_Starclick_Backend bb ON aa.myir = SUBSTR(bb.ORDER_CODE,6,30)
      LEFT JOIN maintenance_datel cc ON  SUBSTR(aa.namaOdp,5,3) = cc.sto
      LEFT JOIN dispatch_teknisi dd ON aa.myir = dd.Ndem
      LEFT JOIN psb_laporan ee ON dd.id = ee.id_tbl_mj
      LEFT JOIN psb_laporan_status ff ON ee.status_laporan = ff.laporan_status_id
      LEFT JOIN regu gg ON dd.id_regu = gg.id_regu
      WHERE
      (bb.STATUS_RESUME IN ("open","booked") OR aa.ket_input = 0) AND
      cc.datel = a.datel AND
      (ff.grup IN ("SISA","OGP") OR dd.id IS NULL)  AND
      aa.sc IS NULL) as SISA_WO,
      (SELECT COUNT(*)  FROM dispatch_teknisi aa
      LEFT JOIN psb_myir_wo d ON aa.`Ndem` = d.myir
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON SUBSTR(d. namaOdp,5,3) = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      c.`laporan_status`="HR" AND
      d.sc IS NULL AND
      d.myir IS NOT NULL AND g.datel=a.datel)+(SELECT COUNT(*)   FROM dispatch_teknisi aa
      LEFT JOIN Data_Pelanggan_Starclick d ON aa.`Ndem` = d.orderId
      LEFT JOIN psb_laporan b ON aa.id = b.`id_tbl_mj`
      LEFT JOIN psb_laporan_status c ON b.status_laporan = c.`laporan_status_id`
      LEFT JOIN regu e ON aa.`id_regu` = e.`id_regu`
      LEFT JOIN group_telegram f ON e.`mainsector` = f.`chat_id`
      LEFT JOIN maintenance_datel g ON d.sto = g.sto
      WHERE
      aa.tgl >= "2020-04-01" AND
      d.orderDatePs = "" AND
      c.`laporan_status`="HR" AND
      d.orderid IS NOT NULL AND
      d.orderStatusId NOT IN ("1500","7","1300","1205") AND
      d.`jenisPsb` IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND  g.datel=a.datel) as HR,
      (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.datel = a.datel AND ((dpsa.orderStatusId in ("1500","7")  AND DATE(dpsa.orderDatePs) = "'.$date.'" ) OR (DATE(dpsa.orderDate) >="2020-03-01" AND dpsa.orderStatusId IN ("1203","1401"))) ) as PS,
      (SELECT count(*) FROM Data_Pelanggan_Starclick dpsa LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto WHERE DATE(dpsa.orderDate) >="2020-03-01"  AND dpsa.jenisPsb IN ("AO| ","AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET") AND aa.datel = a.datel AND dpsa.orderStatusId IN ("1300","1205") ) as ACTCOMP
      FROM
        maintenance_datel a
      WHERE
        a.witel = "KALSEL"
      GROUP BY a.datel
    ');
    return $query;
  }

  public static function get_potensi_ps_up($tgl, $ket=null,$witel){
    $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };
    $get_datel = DB::table('maintenance_datel')->where('witel',$witel)->groupBy('datel')->get();
    $SQL = '
        SELECT
         dps.orderStatusId,
         dps.orderStatus,';
    foreach($get_datel as $datel){
         $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }
    $SQL .= '
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN maintenance_datel m ON dps.sto = m.sto
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          dps.orderStatusId in ("1500","7")  AND
          '.$whereTgl.'
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ';
    $query = DB::select($SQL);
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsStarUp($tgl, $ket=null){
    $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
    if ($ket<>null){
        $ketTgl = explode('_',$ket);

        $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
    };

    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dps.witel = "'.session('witel').'" AND
          dps.jenisPsb IN ("AO|TLP+INET+IPTV","AO|INTERNET+IPTV","AO|TLP+INTERNET","AO|") AND
          dps.orderStatusId in ("1500","7")  AND
          '.$whereTgl.'
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsStarFalloutWfm($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
           dispatch_teknisi dt
            LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
            LEFT JOIN mdf m ON dps.sto = m.mdf
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
        WHERE
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dps.jenisPsb LIKE "%AO%" AND
          dps.orderStatus in ("Fallout (WFM)")  AND
          DATE(dps.orderDatePs) LIKE "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
          // dps.orderDate BETWEEN "'.$startdate.'" AND "'.$enddate.'"
    return $query;
  }

  public static function reportPotensiPsScbe($tgl){
      $query = DB::SELECT('
        SELECT
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" || m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
         LEFT JOIN mdf m ON my.sto = m.mdf
        WHERE
          dt.dispatch_by="5" AND
          my.myir <> "" AND
          pls.laporan_status_id in ("31","5","28","29","4") AND
          dt.tgl like "%'.$tgl.'%"
    ');
    return $query;
  }

  public static function potensiPs($tgl,$jenis,$status,$so,$ketTw=null){
      $startdate = date('Y-m-d', mktime(0, 0, 0, date("m",strtotime($tgl)), date("d",strtotime($tgl))-5, date("Y",strtotime($tgl))));
      $enddate = date('Y-m-d',strtotime($tgl));

      if ($ketTw<>null){
          $ketTgl = explode('_',$ketTw);

          $startdate = $ketTgl[0];
          $enddate   = $ketTgl[1];
      };

      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($so=="KDG"){
          $where_area = ' AND m.area_migrasi in ("KDG","TJL")';
      };

      if ($status == "ALL"){
        $where_status = ' AND (dps.orderStatus in ("Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Process OSS (Activation Completed)") OR (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38")))';
      }
      else if ($status=="Fallout (WFM)"){
          $where_status = ' AND (dps.orderStatus="Fallout (WFM)" AND pls.laporan_status_id in ("1","37","38"))';
      }
      else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dps.orderDate as tanggal_order,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          pl.redaman_iboster,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = "" AND
         dps.orderId <> "" AND
         dps.jenisPsb LIKE "%AO%" AND
         (DATE(dps.orderDate) BETWEEN "'.$startdate.'" AND "'.$enddate.'")
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
         // dt.tgl like "%'.$tgl.'%"

      return $query;
  }

  public static function potensiPsUp($tgl,$jenis,$status,$so,$ketTw=null){
      $whereTgl = ' DATE(dps.orderDatePs) LIKE "%'.$tgl.'%" ';
      if ($ketTw<>null){
          $ketTgl = explode('_',$ketTw);

          $whereTgl = ' (DATE(dps.orderDatePs) between "'.$ketTgl[0].'" AND "'.$ketTgl[1].'")';
      };

      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.datel = "'.$so.'"';
      };

      if ($so=="TANJUNG"){
          $where_area = ' AND m.datel in ("KANDANGAN","TANJUNG")';
      };

      $where_status = '';

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dps.orderDate as tanggal_order,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          pl.redaman_iboster,
          dps.*,
          m.datel as area_migrasi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN data_starclick_ao dps ON dt.Ndem = dps.orderId
         LEFT JOIN maintenance_datel m ON dps.sto = m.sto
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
        dps.orderStatusId in ("1500","52","7") AND
         '.$whereTgl.'
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

  public static function potensiPsScbe($tgl,$jenis,$status,$so){
    $where_area = "";
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    };

    if ($so=="KDG"){
        $where_area = ' AND m.area_migrasi in ("KDG","TJL")';
    };

    $where_status = ' AND pls.laporan_status_id in ("31","5","28","29","4")';
    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.updated_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          DATE_FORMAT(my.orderDate,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          my.sto,
          pls.laporan_status_id,
          my.customer,
          my.myir,
          my.sto,
          pls.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
         LEFT JOIN mdf m ON my.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by="5" AND
         my.myir <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function reportPotensiPsStarMo($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         Data_Pelanggan_Starclick dps
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dps.orderId <> "" AND
          dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
          dps.orderStatus in ("Completed (PS)","Process OSS (Provision Issued)","Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Fallout (WFM)","Process OSS (Activation Completed)") AND
          dps.orderDate like "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
    return $query;
  }

  public static function reportPotensiPsStarMoAsli($tgl){
    $query = DB::select('
        SELECT
         dps.orderStatusId,
         dps.orderStatus,
         SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
         SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
         SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
         SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
         SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by = "" AND
          dps.orderId <> "" AND
          dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
          dps.orderStatus in ("Completed (PS)","Process OSS (Provision Issued)","Process OSS (Provision Completed)","Process ISISKA (RWOS)","Fallout (Data)","Fallout (Activation)","Fallout (WFM)","Process OSS (Activation Completed)") AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY dps.orderStatusId
        ORDER BY dps.orderStatusId asc
      ');
    return $query;
  }

  public static function potensiPsMo($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($status == "ALL"){
        $where_status = '';
      } else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          dps.*
        FROM
         Data_Pelanggan_Starclick dps
         LEFT JOIN mdf m ON dps.sto = m.mdf
       WHERE
         dps.orderId <> "" AND
         dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
         dps.orderDate like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dps.orderId
      ');

      return $query;
  }

  public static function potensiPsMoAsli($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND m.area_migrasi = "'.$so.'"';
      };

      if ($status == "ALL"){
        $where_status = '';
      } else {
        $where_status = '  AND dps.orderStatus = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = "" AND
         dps.orderId <> "" AND
         dps.jenisPsb in ("MO|VOICE","MO|IPTV","MO|INTERNET + VOICE","MO|INTERNET + IPTV","MO|INTERNET","MO|BUNDLING","MO (INTERNET)","MO
MIGRATE|MO|INTERNET + IPTV","MIGRATE|MO|BUNDLING","MIGRATE|AS + MO|INTERNET + VOICE","MIGRATE|AS + MO|BUNDLING","MIGRATE","AS|VOICE","AS + MO|INTERNET + VOICE","AS + MO|BUNDLING") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

  public static function dashboardTransaksi($tgl){
    $query = DB::select('
        SELECT
        pls.laporan_status_id,
        pls.laporan_status,
         SUM(CASE WHEN dt.jenis_layanan = "ADDONSTB" THEN 1 ELSE 0 END) as wo_ADDONSTB,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTB" THEN 1 ELSE 0 END) as wo_CHANGESTB,
         SUM(CASE WHEN dt.jenis_layanan = "2ndSTB" THEN 1 ELSE 0 end) as wo_2ndSTB,
         SUM(CASE WHEN dt.jenis_layanan = "3ndSTB" THEN 1 ELSE 0 end) as wo_3ndSTB,
         SUM(CASE WHEN dt.jenis_layanan = "WifiExtender" THEN 1 else 0 END) as wo_WifiExtender,
         SUM(CASE WHEN dt.jenis_layanan = "plc" THEN 1 else 0 END) as wo_plc,
         SUM(CASE WHEN dt.jenis_layanan = "PDA" THEN 1 else 0 END) as wo_pda,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTB2NDSTB" THEN 1 else 0 END) as wo_CHANGESTB2NDSTB,
         SUM(CASE WHEN dt.jenis_layanan = "CHANGESTBWIFIEXTENDER" THEN 1 else 0 END) as wo_CHANGESTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "ADDSTBPLC" THEN 1 else 0 END) as wo_ADDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "ADDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_ADDSTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "2NDSTBPLC" THEN 1 else 0 END) as wo_2NDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "2NDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_2NDSTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "3RDSTBPLC" THEN 1 else 0 END) as wo_3RDSTBPLC,
         SUM(CASE WHEN dt.jenis_layanan = "3RDSTBWIFIEXTENDER" THEN 1 else 0 END) as wo_3RDSTBWIFIEXTENDER,
         SUM(CASE WHEN dt.jenis_layanan = "INDIBOX" THEN 1 else 0 END) as wo_INDIBOX,
         SUM(CASE WHEN dt.jenis_layanan = "ADDSERVICEINET" THEN 1 else 0 END) as wo_ADDSERVICEINET,
         SUM(CASE WHEN dt.jenis_layanan = "SMARTINDIHOME" THEN 1 else 0 END) as wo_SMARTINDIHOME,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         INNER JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         INNER JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by IS NULL AND
          dps.orderId <> "" AND
          dt.jenis_layanan in ("ADDONSTB","CHANGESTB","2ndSTB","WifiExtender","plc","PDA","CHANGESTB2NDSTB","CHANGESTBWIFIEXTENDER","ADDSTBPLC","ADDSTBWIFIEXTENDER","2NDSTBPLC","2NDSTBWIFIEXTENDER","3RDSTBPLC","3RDSTBWIFIEXTENDER","INDIBOX","ADDSERVICEINET","SMARTINDIHOME") AND
          (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","53","64","65","4","31","35","74")) AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.urutan ASC
      ');
    return $query;
  }

  public static function reportDashboardTransaksi($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND dt.jenis_layanan = "'.$so.'"';
      };

      if ($status == "ANTRIAN"){
        $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      } else if ($status == "ALL"){
        $where_status = ' AND (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","64","65")) ';
      } else {
        $where_status = '  AND pls.laporan_status = "'.$status.'"';
      }

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tgl_awal,
          dt.jenis_layanan as dtJenisLayanan,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*,
          gt.title
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by IS NULL AND
         dps.orderId <> "" AND
         dt.jenis_layanan in ("ADDONSTB","CHANGESTB","2ndSTB","WifiExtender","plc","PDA","CHANGESTB2NDSTB","CHANGESTBWIFIEXTENDER","ADDSTBPLC","ADDSTBWIFIEXTENDER","2NDSTBPLC","2NDSTBWIFIEXTENDER","3RDSTBPLC","3RDSTBWIFIEXTENDER","INDIBOX","ADDSERVICEINET","SMARTINDIHOME") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
      // dd($query);
      return $query;
  }

  public static function dashboardTransaksiSmartIndihome($tgl){
    $query = DB::select('
        SELECT
        pls.laporan_status_id,
        pls.laporan_status,
         SUM(CASE WHEN dt.jenis_layanan = "SMARTINDIHOME" THEN 1 else 0 END) as wo_smartindihome,
         count(*) as jumlah
        FROM
         dispatch_teknisi dt
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_myir_wo dps ON dt.Ndem = dps.myir
         LEFT JOIN mdf m ON dps.sto = m.mdf
        WHERE
          dt.dispatch_by = 7 AND
          dps.myir <> "" AND
          dt.jenis_layanan in ("SMARTINDIHOME") AND
          (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","53","31")) AND
          dt.tgl like "%'.$tgl.'%"
        GROUP BY pls.laporan_status
        ORDER BY pls.urutan ASC
      ');
    return $query;
  }

  public static function reportDashboardTransaksiSmartIndhome($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so<>"ALL"){
        $where_area = ' AND dt.jenis_layanan = "'.$so.'"';
      };

      if ($status == "ANTRIAN"){
        $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      } else if ($status == "ALL"){
        $where_status = ' AND (pl.status_laporan is NULL OR
          pls.laporan_status_id in ("1","16","49","50","6","48","45","12","5","29","28","51","46","31")) ';
      } else {
        $where_status = '  AND pls.laporan_status = "'.$status.'"';
      }

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          dt.jenis_layanan as dtJenisLayanan,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          0 as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN item i ON plm.id_item = i.id_item
         LEFT JOIN psb_myir_wo dps ON dt.Ndem = dps.myir
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = 7 AND
         dps.myir <> "" AND
         dt.jenis_layanan in ("SMARTINDIHOME") AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');
      // dd($query);
      return $query;
  }

  public static function getKetDashboard($nik){
      return DB::table('group_telegram')->where('TL_NIK',$nik)->where('title','LIKE','%TERRITORY%')->first();
  }

  public static function dashboardTeritory($tgl,$ketDashboard){
    if ($ketDashboard==1){
        $getWO = 'SUM(CASE WHEN m.chat_id = "-253578253" THEN 1 ELSE 0 END) as woTlWahyu,
                  SUM(CASE WHEN m.chat_id = "-1001405220793" THEN 1 ELSE 0 END) as woTlRokhman,
                  SUM(CASE WHEN m.chat_id = "-1001188642735" THEN 1 ELSE 0 END) as woTlArie,';

        $ket = ' m.ket_dashboard=1 AND';
    }
    elseif($ketDashboard==2){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-283767249" THEN 1 ELSE 0 END) as woTlJunai,
                SUM(CASE WHEN m.chat_id = "-348628322" THEN 1 ELSE 0 END) as woTlHasan,
                SUM(CASE WHEN m.chat_id = "-1001155521550" THEN 1 ELSE 0 END) as woTlTamami,
                SUM(CASE WHEN m.chat_id = "-1001318576095" THEN 1 ELSE 0 END) as woTlIqbal,';

      $ket = ' m.ket_dashboard=2 AND';
    }
    elseif($ketDashboard==3){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-114836111" THEN 1 ELSE 0 END) as woTlSarpani,
                SUM(CASE WHEN m.chat_id = "-1001077578882" THEN 1 ELSE 0 END) as woTlHifni,';
      $ket = ' m.ket_dashboard=3 AND';
    }
    elseif($ketDashboard==4){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-1001186288135" THEN 1 ELSE 0 END) as woTlSamsudin,';
      $ket = ' m.ket_dashboard=4 AND';
    }
    elseif ($ketDashboard==5){
      $getWO = 'SUM(CASE WHEN m.chat_id = "-1001499053925" THEN 1 ELSE 0 END) as woTlArif,
                SUM(CASE WHEN m.chat_id = "-1001439986938" THEN 1 ELSE 0 END) as woTlVherda,
                SUM(CASE WHEN m.chat_id = "-307695867" THEN 1 ELSE 0 END) as woTlRestu,
                SUM(CASE WHEN m.chat_id = "-1001194255648" THEN 1 ELSE 0 END) as woTlVherda2,
                SUM(CASE WHEN m.chat_id = "-316682210" THEN 1 ELSE 0 END) as woTlHasbullah,
                SUM(CASE WHEN m.chat_id = "-242478590" THEN 1 ELSE 0 END) as woTlIrwan,';
      $ket = ' m.ket_dashboard=5 AND';
    };

    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       '.$getWO.'
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
       LEFT JOIN regu r ON dt.id_regu=r.id_regu
       LEFT JOIN group_telegram m ON r.mainsector = m.chat_id
      WHERE
        dt.dispatch_by = "" AND
        dps.orderId <> "" AND
        r.ACTIVE=1 AND
        '.$ket.'
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.urutan asc, pls.laporan_status asc
    ');

    return $query;
  }

  public static function listDashboardTeritori($tgl,$jenis,$status,$so){
      $where_area = "";
      if ($so==1){
        $where_area = ' AND gt.chat_id in ("-253578253","-1001405220793","-1001188642735") ';
      }
      elseif ($so==2){
        $where_area = ' AND gt.chat_id in ("-283767249","-348628322","-1001155521550","-1001318576095") ';
      }
      elseif ($so==3){
        $where_area = ' AND gt.chat_id in ("-114836111","-1001077578882") ';
      }
      elseif ($so==4){
        $where_area = ' AND gt.chat_id in ("-1001186288135") ';
      }
      elseif ($so==5){
        $where_area = ' AND gt.chat_id in ("-242478590","-307695867","-316682210","-1001194255648","-1001439986938","-1001499053925") ';
      }
      elseif ($so=="ALL"){
        $where_area = '';
      }
      else {
        $where_area = ' AND gt.chat_id = "'.$so.'"';
      };

      if ($status=="NOUPDATE"){
        $where_status = ' AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
      }
      else if ($status == "ALL"){
        $where_status = '';
      } else {
       $where_status = '  AND pls.laporan_status = "'.$status.'"';
      };

      $query = DB::SELECT('
        SELECT
          *,
          dt.id as id_dt,
          DATE_FORMAT(dt.updated_at,"%Y-%m-%d") as tanggal_dispatch,
          DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
          DATE_FORMAT(dps.orderDatePs,"%Y-%m-%d") as orderDatePs,
          tt.ketTiang,
          dps.*
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by = "" AND
         dps.orderId <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
      ');

      return $query;
  }

	public static function scbeKendala_pra_range($range){

		$tgl = '';
		if ($range == "kurang3hari"){
			$tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<3)';
		} else if ($range == "x3sd7hari") {
			$tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3 AND TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )<7)';
		} else if ($range == "lebih7hari") {
			$tgl = '(TIMESTAMPDIFF( DAY , pl.modified_at,  "'.date('Y-m-d H:i:s').'" )>=3)';
		}
		$get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->groupBy('datel')->get();
		$SQL = '
			SELECT
			 pls.laporan_status_id,
			 pls.laporan_status,
			 pls.statusGrup,
			 pls.stts_dash,';
			 foreach ($get_datel as $datel){
				$SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
			}

		$SQL .= '
		 count(*) as jumlah
		FROM
		 dispatch_teknisi dt
     LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
		 LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
		 LEFT JOIN regu r ON dt.id_regu = r.id_regu
		 LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		 LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		 LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
		 LEFT JOIN maintenance_datel m ON dt.dispatch_sto = m.sto
		WHERE
      dt.jenis_order IN ("SC","MYIR") AND
      NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
      pls.aktif_dash = 1 AND
      dt.jenis_layanan NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
			pls.stts_dash IN ("hs","daman","aso","amo") AND
		 	'.$tgl.'
		GROUP BY pls.laporan_status
		ORDER BY pls.urut_stts asc
	';
		$query = DB::SELECT($SQL);
		return $query;
	}

  public static function scbeKendala_pra($tgl){
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->groupBy('datel')->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,';
       foreach ($get_datel as $datel){
        $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
      }

    $SQL .= '
     count(*) as jumlah
    FROM
     dispatch_teknisi dt
     LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
     LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
     LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
     LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
     LEFT JOIN maintenance_datel m ON dt.dispatch_sto = m.sto
     LEFT JOIN regu r ON dt.id_regu = r.id_regu
     LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
      dt.jenis_order IN ("SC","MYIR") AND
      pls.aktif_dash = 1 AND
      dt.jenis_layanan NOT IN ("CABUT_NTE","ONT_PREMIUM") AND
      NOT pls.laporan_status = "PERMINTAAN REVOKE" AND
      pls.stts_dash IN ("hs","daman","amo","aso","kendala") AND
      (DATE(pl.modified_at) LIKE "'.$tgl.'%")

    GROUP BY pls.laporan_status
    ORDER BY pls.urut_stts asc
  ';
    $query = DB::SELECT($SQL);
    return $query;
  }

  public static function pt2_master($tgl){
    $query = DB::SELECT('
    SELECT
      md.datel as DATEL,
      SUM(CASE WHEN pm.lt_status IS NULL THEN 1 ELSE 0 END) as NO_UPDATE,
      SUM(CASE WHEN pm.lt_status = "Berangkat" THEN 1 ELSE 0 END) as BERANGKAT,
      SUM(CASE WHEN pm.lt_status = "Ogp" THEN 1 ELSE 0 END) as OGP,
      SUM(CASE WHEN pm.lt_status = "Kendala" THEN 1 ELSE 0 END) as KENDALA,
      SUM(CASE WHEN pm.lt_status = "Selesai" THEN 1 ELSE 0 END) as SELESAI,
      COUNT(*) AS JUMLAH
    FROM dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN pt2_master pm ON dt.id = pm.scid
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
    LEFT JOIN maintenance_datel md ON SUBSTR(pmw. namaOdp,5,3) = md.sto
    WHERE
    dt.jenis_order IN ("SC","MYIR") AND
    (DATE(pl.modified_at) LIKE "'.$tgl.'%") AND
    pls.laporan_status = "PT2"
    GROUP BY md.datel
    ');

    return $query;
  }

  public static function pt2($datel, $status, $tgl){
    if ($datel == "ALL"){
      $where_datel = ' ';
    } else {
      $where_datel = ' AND md.datel = "'.$datel.'" ';
    }

    if ($status == "NO_UPDATE"){
      $where_status = ' AND pm.lt_status IS NULL ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = ' AND pm.lt_status = "'.$status.'" ';
    }

    $query = DB::SELECT('
    SELECT
      r.TL as tl,
      r.uraian as teknisi,
      dt.Ndem as order_id,
      pm.odp_nama,
      pm.lt_koordinat_odp,
      pm.lt_action,
      pm.tgl_pengerjaan as tgl_dispatch,
      pm.tgl_selesai as tgl_update,
      pm.lt_status as status,
      pm.sto,
      md.datel,
      pl.status_kendala,
      pl.tgl_status_kendala,
      pl.catatan
    FROM dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN pt2_master pm ON dt.id = pm.scid
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
    LEFT JOIN maintenance_datel md ON SUBSTR(pmw. namaOdp,5,3) = md.sto
    WHERE
    dt.jenis_order IN ("SC","MYIR") AND
    (DATE(pl.modified_at) LIKE "'.$tgl.'%") AND
    pls.laporan_status = "PT2"
    '.$where_datel.'
    '.$where_status.'
    ORDER BY tgl_dispatch DESC
    ');

    return $query;
  }

  public static function scbeKendala_after($tgl){
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->groupBy('datel')->get();
    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,';
       foreach ($get_datel as $datel){
        $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as WO_'.$datel->datel.',';
      }

    $SQL .= '
     count(*) as jumlah
    FROM
     dispatch_teknisi dt
     LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
     LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
     LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem=dps.orderId
     LEFT JOIN psb_myir_wo my ON dps.myir=my.myir
     LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
     LEFT JOIN regu r ON dt.id_regu = r.id_regu
     LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
      dt.jenis_order IN ("SC","MYIR") AND
      pls.aktif_dash=1 AND
      pls.stts_dash IN ("hs","daman","aso","amo") AND
      pl.modified_at like "%'.$tgl.'%"

    GROUP BY pls.laporan_status
    ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
  ';
    $query = DB::SELECT($SQL);
    return $query;
  }


  public static function scbeKendalaWitel($tgl){
    $get_datel = DB::table('maintenance_datel')->where('witel',session('witel'))->groupBy('datel')->get();

    $SQL = '
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,';
    foreach ($get_datel as $datel){
      $SQL .= 'SUM(CASE WHEN m.datel = "'.$datel->datel.'" THEN 1 ELSE 0 END) as '.$datel->datel.',';
    }

       $SQL .= '
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem=dps.orderId
       LEFT JOIN maintenance_datel m ON (my.sto = m.sto OR dps.sto = m.sto)
      WHERE
        dt.jenis_order IN ("SC","MYIR") AND
        (my.myir <> "" OR dps.orderId<>"") AND
        pls.aktif_dash=1 AND
        pls.stts_dash IN ("hs","daman","aso","amo") AND
        dt.tgl like "'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
    ';
    $query = DB::SELECT($SQL);
    return $query;
  }



  public static function scbeKendalaPt1($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       pls.stts_dash,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
       LEFT JOIN data_starclick_ao dps ON dt.Ndem=dps.orderId
       LEFT JOIN mdf m ON (my.sto = m.mdf OR dps.sto = m.mdf)
      WHERE
        dt.jenis_order IN ("SC","MYIR") AND
        (my.myir <> "" OR dps.orderId<>"") AND
        pls.aktif_dash=1 AND
        pls.stts_dash IN ("PT1") AND
        dt.tgl like "%'.$tgl.'%"

      GROUP BY pls.laporan_status
      ORDER BY pls.urut_stts asc, pls.laporan_status_id asc
    ');
    return $query;
  }

  public static function dashReboundaryDispatch($tgl){
    $query = DB::SELECT('
      SELECT
       SUM(CASE WHEN m.area_migrasi = "INNER" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_INNER_dispatch,
       SUM(CASE WHEN m.area_migrasi = "INNER" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_INNER_undispatch,
       SUM(CASE WHEN m.area_migrasi = "BBR" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_BBR_dispatch,
       SUM(CASE WHEN m.area_migrasi = "BBR" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_BBR_undispatch,
       SUM(CASE WHEN m.area_migrasi = "TJL" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_TJL_dispatch,
       SUM(CASE WHEN m.area_migrasi = "TJL" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_TJL_undispatch,
       SUM(CASE WHEN m.area_migrasi = "BLC" AND rs.dispatch = "1" THEN 1 ELSE 0 END) as WO_BLC_dispatch,
       SUM(CASE WHEN m.area_migrasi = "BLC" AND rs.dispatch = "0" THEN 1 ELSE 0 END) as WO_BLC_undispatch,
       count(*) as jumlah
      FROM
       reboundary_survey rs
       LEFT JOIN Data_Pelanggan_Starclick dps ON rs.scid=dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        rs.updated_at like "%'.$tgl.'%"
    ');
    return $query;
  }

  public static function listDispatchReboundary($area, $tgl, $status){
    if ($area=='ALL'){
        return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->leftJoin('mdf','Data_Pelanggan_Starclick.sto','=','mdf.mdf')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status', 'mdf.area_migrasi', 'dispatch_teknisi.id as idDt', 'reboundary_survey.idTimLama', 'reboundary_survey.uraianTimLama')
                ->where('reboundary_survey.updated_at','LIKE', '%'.$tgl.'%')
                ->where('reboundary_survey.dispatch',$status)
                ->get();
    }
    else{
        return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->leftJoin('mdf','Data_Pelanggan_Starclick.sto','=','mdf.mdf')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status', 'mdf.area_migrasi', 'dispatch_teknisi.id as idDt', 'reboundary_survey.idTimLama', 'reboundary_survey.uraianTimLama')
                ->where('reboundary_survey.updated_at','LIKE', '%'.$tgl.'%')
                ->where('mdf.area_migrasi',$area)
                ->where('reboundary_survey.dispatch',$status)
                ->get();
    };
  }

  public static function progressReboundary($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       pls.statusGrup,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN reboundary_survey my ON dt.Ndem=my.no_tiket_reboundary
       LEFT JOIN Data_Pelanggan_Starclick dps ON my.scid=dps.orderId
       LEFT JOIN mdf m ON dps.sto = m.mdf
      WHERE
        dt.dispatch_by="6" AND
        my.no_tiket_reboundary <> ""  AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function reboundaryList($tgl,$jenis,$status,$so){
    $where_area = '';
    if ($so<>"ALL"){
      $where_area = ' AND m.area_migrasi = "'.$so.'"';
    }

    if ($status == "ANTRIAN"){
      $where_status = '  AND (pls.laporan_status_id = "6" OR pls.laporan_status is NULL) ';
    } else if ($status == "ALL"){
      $where_status = ' ';
    } else {
      $where_status = '  AND pls.laporan_status = "'.$status.'"';
    }

    $query = DB::SELECT('
      SELECT
          *,
          dt.id as id_dt,
          dt.created_at as tanggal_dispatch,
          pl.modified_at as modified_at,
          tt.ketTiang,
          pls.laporan_status_id,
          dps.*,
          my.catatan as catatan_reboundary,
          pl.catatan as catatan_teknisi
        FROM
         dispatch_teknisi dt
         LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
         LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
         LEFT JOIN reboundary_survey my ON dt.Ndem = my.no_tiket_reboundary
         LEFT JOIN Data_Pelanggan_Starclick dps ON my.scid = dps.orderId
         LEFT JOIN mdf m ON dps.sto = m.mdf
         LEFT JOIN regu r ON dt.id_regu = r.id_regu
         LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
         LEFT JOIN tambahTiang tt ON pl.ttId=tt.id
       WHERE
         dt.dispatch_by="6" AND
         my.scid <> "" AND
         dt.tgl like "%'.$tgl.'%"
         '.$where_status.'
         '.$where_area.'
         GROUP BY dt.id
    ');

    return $query;
  }

  public static function dashboadWifiId($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN micc_wo mw ON dt.Ndem=mw.ND
       LEFT JOIN mdf m ON mw.MDF = m.mdf
      WHERE
        mw.ND <> ""  AND
        mw.jenis_layanan = "WIFI.ID" AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function dashboadDatin($tgl){
    $query = DB::SELECT('
      SELECT
       pls.laporan_status_id,
       pls.laporan_status,
       SUM(CASE WHEN m.area_migrasi = "INNER" THEN 1 ELSE 0 END) as WO_INNER,
       SUM(CASE WHEN m.area_migrasi = "BBR" THEN 1 ELSE 0 END) as WO_BBR,
       SUM(CASE WHEN m.area_migrasi = "KDG" THEN 1 ELSE 0 END) as WO_KDG,
       SUM(CASE WHEN m.area_migrasi = "TJL" THEN 1 ELSE 0 END) as WO_TJL,
       SUM(CASE WHEN m.area_migrasi = "BLC" THEN 1 ELSE 0 END) as WO_BLC,
       count(*) as jumlah
      FROM
       dispatch_teknisi dt
       LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
       LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
       LEFT JOIN micc_wo mw ON dt.Ndem=mw.ND
       LEFT JOIN mdf m ON mw.MDF = m.mdf
      WHERE
        mw.ND <> ""  AND
        (mw.jenis_layanan LIKE "%ASTINET%" OR mw.jenis_layanan LIKE "%VPNIP%" OR mw.jenis_layanan LIKE "%METRO%" ) AND
        dt.tgl like "%'.$tgl.'%"
      GROUP BY pls.laporan_status
      ORDER BY pls.stts_grup_order asc, pls.urutan asc
    ');
    return $query;
  }

  public static function listWifiId($area, $tgl, $status){
    if ($area=="ALL"){
        $where_area = '';
    }
    else{
        $where_area = 'AND m.area_migrasi = "'.$area.'"';
    };

    if ($status=="ALL"){
        $where_status = '';
    }
    else if ($status=="ANTRIAN"){
        $where_status = ' AND (pl.status_laporan=6 OR pl.status_laporan=NULL OR pl.status_laporan="")';
    }
    else{
        $where_status = ' AND pls.laporan_status="'.$status.'"';
    }

    $sql = 'SELECT
              *,
            mw.jenis_layanan as layananMicc
            FROM
              micc_wo mw
            LEFT JOIN dispatch_teknisi dt ON mw.ND = dt.Ndem
            LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
            LEFT JOIN mdf m ON mw.MDF=m.mdf
            LEFT JOIN regu r ON dt.id_regu=r.id_regu
            LEFT JOIN group_telegram gr ON r.mainsector=gr.chat_id
            WHERE
              dt.tgl LIKE "%'.$tgl.'%" AND
              mw.jenis_layanan="WIFI.ID"'.$where_area.$where_status;

    return DB::select($sql);
  }

  public static function listDatin($area, $tgl, $status){
    if ($area=="ALL"){
        $where_area = '';
    }
    else{
        $where_area = ' AND m.area_migrasi = "'.$area.'"';
    };

    if ($status=="ALL"){
        $where_status = '';
    }
    else if ($status=="ANTRIAN"){
        $where_status = ' AND (pl.status_laporan=6 OR pl.status_laporan=NULL OR pl.status_laporan="")';
    }
    else{
        $where_status = ' AND pls.laporan_status="'.$status.'"';
    }

    $sql = 'SELECT
              *,
            mw.jenis_layanan as layananMicc
            FROM
              micc_wo mw
            LEFT JOIN dispatch_teknisi dt ON mw.ND = dt.Ndem
            LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
            LEFT JOIN mdf m ON mw.MDF=m.mdf
            LEFT JOIN regu r ON dt.id_regu=r.id_regu
            LEFT JOIN group_telegram gr ON r.mainsector=gr.chat_id
            WHERE
              dt.tgl LIKE "%'.$tgl.'%" AND
              (mw.jenis_layanan LIKE "%ASTINET%" OR mw.jenis_layanan LIKE "%VPNIP%" OR mw.jenis_layanan LIKE "%METRO%" )'.$where_area.$where_status;

    return DB::select($sql);
  }

  public static function getListUndispatchCcan($tgl, $area, $ket){
      $where_ket = '';
      if ($ket<>"ALL"){
        $where_ket = 'AND dps.layanan LIKE "%'.$ket.'%"';
      };

      $where_area = 'AND m.area_migrasi="'.$area.'"';
      if ($area=="ALL"){
        $where_area = '';
      };

      $sql = '
            SELECT
                *
            FROM
                micc_wo dps
                LEFT JOIN dispatch_teknisi dt ON dps.ND = dt.Ndem
                LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                LEFT JOIN mdf m ON dps.MDF = m.mdf
            WHERE
                m.mdf<>""
                AND dt.id is NULL
                '.$where_area.'
                '.$where_ket.'
            ';

      return  DB::select($sql);
  }

  public static function getHrPs($bulan){
      $sql = 'SELECT
                a.*, c.modified_at as tglStatus, e.*, d.uraian
              FROM
                dispatch_teknisi a
              LEFT JOIN Data_Pelanggan_Starclick b ON b.orderId = a.Ndem
              LEFT JOIN psb_laporan c ON c.id_tbl_mj = a.id
              LEFT JOIN regu d ON d.id_regu = a.id_regu
              LEFT JOIN group_telegram e ON e.chat_id = d.mainsector
              WHERE
                b.orderStatus="Completed (PS)" AND
                c.status_laporan = "4" AND
                c.modified_at LIKE "%'.$bulan.'%"
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getMatrixCcan($tgl, $user){
      $sql = 'SELECT
                a.Ndem, a.tgl, a.updated_by, c.nama
              FROM
                dispatch_teknisi a
              LEFT JOIN micc_wo b ON b.ND = a.Ndem
              LEFT JOIN roc d ON d.no_tiket = a.Ndem
              LEFT JOIN 1_2_employee c ON c.nik = a.updated_by
              WHERE
                (b.ND IS NOT NULL OR d.loker_ta="2") AND
                a.updated_by = "'.$user.'" AND
                a.tgl LIKE "%'.$tgl.'%"
              GROUP BY a.Ndem
              ';

      return DB::select($sql);
  }

  public static function getListUmurByNossa($tgl){
      $sql = 'SELECT
                *
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN regu c ON b.id_regu = c.id_regu
              WHERE
                a.Reported_Date LIKE "%'.$tgl.'%" AND
                (a.Customer_Segment IS NOT NULL OR a.Customer_Segment NOT IN ("DCS"))
              ORDER BY
                a.TTR_Witel DESC
             ';
      $data = DB::select($sql);
      return $data;
  }

  public static function dashboardUmurByNossa($tgl){
        $sql = 'SELECT
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("MM_ASTINET","MM_IPVPN") AND TTR_Witel < 3) THEN 1 ELSE 0 END ) as datin_C ,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("MM_ASTINET","MM_IPVPN") AND TTR_Witel > 3) THEN 1 ELSE 0 END ) as datin_NC ,
                SUM(CASE WHEN (Customer_Segment IN ("DGS") AND Service_Type IN ("VOICE") AND TTR_Witel < 5) THEN 1 ELSE 0 END ) as voice_dgs_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS") AND Service_Type IN ("VOICE") AND TTR_Witel > 5) THEN 1 ELSE 0 END ) as voice_dgs_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DBS", "DES") AND Service_Type IN ("VOICE") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as voice_dbs_des_c,
                SUM(CASE WHEN (Customer_Segment IN ("DBS", "DES") AND Service_Type IN ("VOICE") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as voice_dbs_des_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("INTERNET","IPTV") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as inetIptv_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("INTERNET","IPTV") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as inetIptv_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DWS") AND Service_Type IN ("MM_METRO_ETHERENT","SITE") AND TTR_Witel < 3) THEN 1 ELSE 0 END ) as squadOlo_c,
                SUM(CASE WHEN (Customer_Segment IN ("DWS") AND Service_Type IN ("MM_METRO_ETHERENT","SITE") AND TTR_Witel > 3) THEN 1 ELSE 0 END ) as squadOlo_nc,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("LOC_AP","MM_WIFI_ID") AND TTR_Witel < 8.5) THEN 1 ELSE 0 END ) as wifiId_c,
                SUM(CASE WHEN (Customer_Segment IN ("DGS", "DBS", "DES") AND Service_Type IN ("LOC_AP","MM_WIFI_ID") AND TTR_Witel > 8.5) THEN 1 ELSE 0 END ) as wifiId_nc
              FROM
                data_nossa_1_log
              WHERE
                Reported_Date LIKE "%'.$tgl.'%"
             ';

        $data = DB::select($sql);
        return $data;
  }

  public static function getWOPs($tgl){
     $sql = 'SELECT
                a.id as id_dt,
                d.title,
                d.chat_id,
                c.id_regu,
                c.uraian,
                SUM(CASE WHEN e.status_laporan="1" THEN 1 ELSE 0 END) as jumlahPs,
                SUM(CASE WHEN (e.status_laporan="1" AND e.dropcore_label_code IS NOT NULL) THEN 1 ELSE 0 END) as jumlahPSQr

            FROM
              dispatch_teknisi a
            LeFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON d.chat_id = c.mainsector
            LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
            LEFT JOIN jenis_layanan f ON a.jenis_layanan = f.jenis_layanan
            WHERE
              e.modified_at LIKE "'.$tgl.'%" AND
              e.status_laporan <> "" AND
              b.orderId <> "" AND
              d.ketTer = 1
            GROUP BY
              d.title
            ';

    return DB::select($sql);
  }

  public static function getWoPsDetail($tgl, $chatId, $status){
      if ($status==0){
          $where_status = 'AND e.status_laporan <> ""';
      }
      elseif($status==1){
          $where_status = 'AND (e.status_laporan <> "" AND e.dropcore_label_code IS NOT NULL)';
      };

      if($chatId=="ALL"){
          $where_chatid = '';
      }
      else{
          $where_chatid = 'AND d.chat_id="'.$chatId.'"';
      };

      $sql = 'SELECT
                d.title,
                d.chat_id,
                c.id_regu,
                c.uraian,
                b.orderStatus,
                e.dropcore_label_code,
                g.laporan_status,
                b.orderDatePs,
                b.*,
                a.Ndem as orderId,
                e.modified_at as tglUp,
                b.jenisPsb,
                a.id as id_dt,
                e.nama_odp,
                e.snont,
                e.port_number,
                h.*,
                i.namaOdp
                FROM
                  dispatch_teknisi a
                LeFT JOIN Data_Pelanggan_Starclick b ON a.Ndem = b.orderId
                LEFT JOIN regu c ON a.id_regu = c.id_regu
                LEFT JOIN group_telegram d ON d.chat_id = c.mainsector
                LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
                LEFT JOIN jenis_layanan f ON a.jenis_layanan = f.jenis_layanan
                LEFT JOIN psb_laporan_status g ON e.status_laporan = g.laporan_status_id
                LEFT JOIN maintenance_datel h ON h.sto = b.sto
                LEFT JOIN psb_myir_wo i ON b.orderId = i.sc
                WHERE
                  e.modified_at LIKE "'.$tgl.'%" AND
                  e.status_laporan <> "" AND
                  b.orderId <> "" AND
                  d.ketTer = 1
              '.$where_status.'
              '.$where_chatid.'
            ';

      return DB::select($sql);
  }

  public static function dashboardDismanlteArea($type,$date)
  {


    if ($type == "SEKTOR")
    {
      $get_area = DB::SELECT('SELECT title as area FROM group_telegram GROUP BY title');
      $select = 'gt.title as area,';
      $group_by = 'GROUP BY gt.title';
    } elseif ($type == "MITRA") {
      $get_area = DB::SELECT('SELECT mitra_amija_pt as area FROM mitra_amija GROUP BY mitra_amija_pt');
      $select = 'ma.mitra_amija_pt as area,';
      $group_by = 'GROUP BY ma.mitra_amija_pt';
    } elseif ($type == "WITEL") {
      $get_area = DB::SELECT('SELECT Witel_New as area FROM group_telegram GROUP BY Witel_New');
      $select = 'gt.Witel_New as area,';
      $group_by = 'GROUP BY gt.Witel_New';
    }

    $query1 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN cno.jenis_dismantling IS NULL THEN 1 ELSE 0 END) as und_null,
      SUM(CASE WHEN cno.jenis_dismantling IN ("NEW CT0","CT0","Dismantling CT0") THEN 1 ELSE 0 END) as und_ct0,
      SUM(CASE WHEN cno.jenis_dismantling NOT IN ("NEW CT0","CT0","Dismantling CT0","147","MIGRASI 147","NEW LOSS") THEN 1 ELSE 0 END) as und_massal,
      SUM(CASE WHEN cno.jenis_dismantling IN ("147","MIGRASI 147") THEN 1 ELSE 0 END) as und_147,
      SUM(CASE WHEN cno.jenis_dismantling = "NEW LOSS" THEN 1 ELSE 0 END) as und_newloss
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query2 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN crd.jenis_nte = "ont" THEN 1 ELSE 0 END) as jml_ont_h1,
      SUM(CASE WHEN crd.jenis_nte = "stb" THEN 1 ELSE 0 END) as jml_stb_h1,
      SUM(CASE WHEN crd.jenis_nte NOT IN ("ont","stb") THEN 1 ELSE 0 END) as jml_others_h1,
      SUM(CASE WHEN crd.jenis_nte IN ("ont","stb") THEN 1 ELSE 0 END) as total_h1
      FROM collectedRacoonDismantling crd
      LEFT JOIN dispatch_teknisi dt ON crd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(crd.tanggalcreate) = "'.date('Y-m-d',strtotime("-1 days")). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query3 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN crd.jenis_nte = "ont" THEN 1 ELSE 0 END) as jml_ont_hi,
      SUM(CASE WHEN crd.jenis_nte = "stb" THEN 1 ELSE 0 END) as jml_stb_hi,
      SUM(CASE WHEN crd.jenis_nte NOT IN ("ont","stb") THEN 1 ELSE 0 END) as jml_others_hi,
      SUM(CASE WHEN crd.jenis_nte <> "" THEN 1 ELSE 0 END) as total_hi
      FROM collectedRacoonDismantling crd
      LEFT JOIN dispatch_teknisi dt ON crd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(crd.tanggalcreate) = "'.date('Y-m-d'). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query4 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN crd.jenis_nte = "ont" THEN 1 ELSE 0 END) as jml_ont_bi,
      SUM(CASE WHEN crd.jenis_nte = "stb" THEN 1 ELSE 0 END) as jml_stb_bi,
      SUM(CASE WHEN crd.jenis_nte NOT IN ("ont","stb") THEN 1 ELSE 0 END) as jml_others_bi,
      SUM(CASE WHEN crd.jenis_nte <> "" THEN 1 ELSE 0 END) as total_bi
      FROM collectedRacoonDismantling crd
      LEFT JOIN dispatch_teknisi dt ON crd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(crd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d'). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query5 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN vrd.tipe_dapros IN ("CT0","NEW CT0") THEN 1 ELSE 0 END) as jml_visit_ct0_h1,
      SUM(CASE WHEN vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") THEN 1 ELSE 0 END) as jml_visit_massal_h1,
      SUM(CASE WHEN vrd.tipe_dapros IN ("147","MIGRASI 147") THEN 1 ELSE 0 END) as jml_visit_147_h1,
      SUM(CASE WHEN vrd.tipe_dapros = "NEW LOSS" THEN 1 ELSE 0 END) as jml_visit_newloss_h1,
      SUM(CASE WHEN vrd.tipe_dapros <> "" THEN 1 ELSE 0 END) as total_visit_h1
      FROM visitRacoonDismantling vrd
      LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(vrd.tanggalcreate) = "'.date('Y-m-d',strtotime("-1 days")). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query6 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN vrd.tipe_dapros IN ("CT0","NEW CT0") THEN 1 ELSE 0 END) as jml_visit_ct0_hi,
      SUM(CASE WHEN vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") THEN 1 ELSE 0 END) as jml_visit_massal_hi,
      SUM(CASE WHEN vrd.tipe_dapros IN ("147","MIGRASI 147") THEN 1 ELSE 0 END) as jml_visit_147_hi,
      SUM(CASE WHEN vrd.tipe_dapros = "NEW LOSS" THEN 1 ELSE 0 END) as jml_visit_newloss_hi,
      SUM(CASE WHEN vrd.tipe_dapros <> "" THEN 1 ELSE 0 END) as total_visit_hi
      FROM visitRacoonDismantling vrd
      LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(vrd.tanggalcreate) = "'.date('Y-m-d'). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    $query7 = DB::SELECT('
      SELECT
      '.$select.'
      SUM(CASE WHEN vrd.tipe_dapros IN ("CT0","NEW CT0") THEN 1 ELSE 0 END) as jml_visit_ct0_bi,
      SUM(CASE WHEN vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") THEN 1 ELSE 0 END) as jml_visit_massal_bi,
      SUM(CASE WHEN vrd.tipe_dapros IN ("147","MIGRASI 147") THEN 1 ELSE 0 END) as jml_visit_147_bi,
      SUM(CASE WHEN vrd.tipe_dapros = "NEW LOSS" THEN 1 ELSE 0 END) as jml_visit_newloss_bi,
      SUM(CASE WHEN vrd.tipe_dapros <> "" THEN 1 ELSE 0 END) as total_visit_bi
      FROM visitRacoonDismantling vrd
      LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d'). '")
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      '.$group_by.'
    ');

    foreach($get_area as $val)
    {
      foreach($query1 as $val_c1)
      {
        if (!empty($val_c1->area))
        {
          if ($val_c1->area == $val->area)
          {
            $query[$val->area]['und_null'] = $val_c1->und_null;
            $query[$val->area]['und_ct0'] = $val_c1->und_ct0;
            $query[$val->area]['und_massal'] = $val_c1->und_massal;
            $query[$val->area]['und_147'] = $val_c1->und_147;
            $query[$val->area]['und_newloss'] = $val_c1->und_newloss;
          }
        } else {
          $query['NON AREA']['und_null'] = $val_c1->und_null;
          $query['NON AREA']['und_ct0'] = $val_c1->und_ct0;
          $query['NON AREA']['und_massal'] = $val_c1->und_massal;
          $query['NON AREA']['und_147'] = $val_c1->und_147;
          $query['NON AREA']['und_newloss'] = $val_c1->und_newloss;
        }
      }
      foreach($query2 as $val_c2)
      {
        if (!empty($val_c2->area))
        {
          if ($val_c2->area == $val->area)
          {
            $query[$val->area]['jml_ont_h1'] = $val_c2->jml_ont_h1;
            $query[$val->area]['jml_stb_h1'] = $val_c2->jml_stb_h1;
            $query[$val->area]['jml_others_h1'] = $val_c2->jml_others_h1;
            $query[$val->area]['total_h1'] = $val_c2->total_h1;
          }
        } else {
            $query['NON AREA']['jml_ont_h1'] = $val_c2->jml_ont_h1;
            $query['NON AREA']['jml_stb_h1'] = $val_c2->jml_stb_h1;
            $query['NON AREA']['jml_others_h1'] = $val_c2->jml_others_h1;
            $query['NON AREA']['total_h1'] = $val_c2->total_h1;
        }
      }
      foreach($query3 as $val_c3)
      {
        if (!empty($val_c3->area))
        {
          if ($val_c3->area == $val->area)
          {
            $query[$val->area]['jml_ont_hi'] = $val_c3->jml_ont_hi;
            $query[$val->area]['jml_stb_hi'] = $val_c3->jml_stb_hi;
            $query[$val->area]['jml_others_hi'] = $val_c3->jml_others_hi;
            $query[$val->area]['total_hi'] = $val_c3->total_hi;
          }
        } else {
            $query['NON AREA']['jml_ont_hi'] = $val_c3->jml_ont_hi;
            $query['NON AREA']['jml_stb_hi'] = $val_c3->jml_stb_hi;
            $query['NON AREA']['jml_others_hi'] = $val_c3->jml_others_hi;
            $query['NON AREA']['total_hi'] = $val_c3->total_hi;
        }
      }
      foreach($query4 as $val_c4)
      {
        if (!empty($val_c4->area))
        {
          if ($val_c4->area == $val->area)
          {
            $query[$val->area]['jml_ont_bi'] = $val_c4->jml_ont_bi;
            $query[$val->area]['jml_stb_bi'] = $val_c4->jml_stb_bi;
            $query[$val->area]['jml_others_bi'] = $val_c4->jml_others_bi;
            $query[$val->area]['total_bi'] = $val_c4->total_bi;
          }
        } else {
            $query['NON AREA']['jml_ont_bi'] = $val_c4->jml_ont_bi;
            $query['NON AREA']['jml_stb_bi'] = $val_c4->jml_stb_bi;
            $query['NON AREA']['jml_others_bi'] = $val_c4->jml_others_bi;
            $query['NON AREA']['total_bi'] = $val_c4->total_bi;
        }
      }
      foreach($query5 as $val_c5)
      {
        if (!empty($val_c5->area))
        {
          if ($val_c5->area == $val->area)
          {
            $query[$val->area]['jml_visit_ct0_h1'] = $val_c5->jml_visit_ct0_h1;
            $query[$val->area]['jml_visit_massal_h1'] = $val_c5->jml_visit_massal_h1;
            $query[$val->area]['jml_visit_147_h1'] = $val_c5->jml_visit_147_h1;
            $query[$val->area]['jml_visit_newloss_h1'] = $val_c5->jml_visit_newloss_h1;
            $query[$val->area]['total_visit_h1'] = $val_c5->total_visit_h1;
          }
        } else {
          $query['NON AREA']['jml_visit_ct0_h1'] = $val_c5->jml_visit_ct0_h1;
          $query['NON AREA']['jml_visit_massal_h1'] = $val_c5->jml_visit_massal_h1;
          $query['NON AREA']['jml_visit_147_h1'] = $val_c5->jml_visit_147_h1;
          $query['NON AREA']['jml_visit_newloss_h1'] = $val_c5->jml_visit_newloss_h1;
          $query['NON AREA']['total_visit_h1'] = $val_c5->total_visit_h1;
        }
      }
      foreach($query6 as $val_c6)
      {
        if (!empty($val_c6->area))
        {
          if ($val_c6->area == $val->area)
          {
            $query[$val->area]['jml_visit_ct0_hi'] = $val_c6->jml_visit_ct0_hi;
            $query[$val->area]['jml_visit_massal_hi'] = $val_c6->jml_visit_massal_hi;
            $query[$val->area]['jml_visit_147_hi'] = $val_c6->jml_visit_147_hi;
            $query[$val->area]['jml_visit_newloss_hi'] = $val_c6->jml_visit_newloss_hi;
            $query[$val->area]['total_visit_hi'] = $val_c6->total_visit_hi;
          }
        } else {
          $query['NON AREA']['jml_visit_ct0_hi'] = $val_c6->jml_visit_ct0_hi;
          $query['NON AREA']['jml_visit_massal_hi'] = $val_c6->jml_visit_massal_hi;
          $query['NON AREA']['jml_visit_147_hi'] = $val_c6->jml_visit_147_hi;
          $query['NON AREA']['jml_visit_newloss_hi'] = $val_c6->jml_visit_newloss_hi;
          $query['NON AREA']['total_visit_hi'] = $val_c6->total_visit_hi;
        }
      }
      foreach($query7 as $val_c7)
      {
        if (!empty($val_c7->area))
        {
          if ($val_c7->area == $val->area)
          {
            $query[$val->area]['jml_visit_ct0_bi'] = $val_c7->jml_visit_ct0_bi;
            $query[$val->area]['jml_visit_massal_bi'] = $val_c7->jml_visit_massal_bi;
            $query[$val->area]['jml_visit_147_bi'] = $val_c7->jml_visit_147_bi;
            $query[$val->area]['jml_visit_newloss_bi'] = $val_c7->jml_visit_newloss_bi;
            $query[$val->area]['total_visit_bi'] = $val_c7->total_visit_bi;
          }
        } else {
          $query['NON AREA']['jml_visit_ct0_bi'] = $val_c7->jml_visit_ct0_bi;
          $query['NON AREA']['jml_visit_massal_bi'] = $val_c7->jml_visit_massal_bi;
          $query['NON AREA']['jml_visit_147_bi'] = $val_c7->jml_visit_147_bi;
          $query['NON AREA']['jml_visit_newloss_bi'] = $val_c7->jml_visit_newloss_bi;
          $query['NON AREA']['total_visit_bi'] = $val_c7->total_visit_bi;
        }
      }
    }

    return $query;
  }

  public static function dismantlingBerhasil($date)
  {


    return DB::SELECT('
      SELECT
      gt.title as area,
      SUM(CASE WHEN plds.id_status = 1 THEN 1 ELSE 0 END) as tidak_bisa_bayar,
      SUM(CASE WHEN plds.id_status = 2 THEN 1 ELSE 0 END) as sering_gangguan,
      SUM(CASE WHEN plds.id_status = 3 THEN 1 ELSE 0 END) as pindah_rumah,
      SUM(CASE WHEN plds.id_status = 4 THEN 1 ELSE 0 END) as salah_paket,
      SUM(CASE WHEN plds.id_status = 5 THEN 1 ELSE 0 END) as sudah_tidak_dipakai,
      SUM(CASE WHEN plds.id_status = 6 THEN 1 ELSE 0 END) as rumah_tidak_ditempati,
      SUM(CASE WHEN plds.id_status = 7 THEN 1 ELSE 0 END) as internet_lambat,
      COUNT(*) as jumlah
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_dismatling_status plds ON pl.dismantling_status = plds.id_status
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
      dt.jenis_order = "CABUT_NTE" AND
      pl.status_laporan = 94 AND
      plds.id_status IN (1,2,3,4,5,6,7) AND
      (DATE(pl.modified_at) LIKE "'.$date.'%")
      GROUP BY gt.title
    ');
  }

  public static function dashboardOntPremium($date)
  {


    return DB::SELECT('
      SELECT
      gt.title as sektor,
      SUM(CASE WHEN opo.dispatch_by LIKE "UNDISPATCH%" AND (dt.jenis_order IS NULL AND dt.Ndem IS NULL) THEN 1 ELSE 0 END) as undispatch,
      SUM(CASE WHEN dt.Ndem <> "" AND pl.id_tbl_mj IS NULL AND dt.jenis_order = "ONT_PREMIUM" THEN 1 ELSE 0 END) as no_update,
      SUM(CASE WHEN pl.status_laporan IN (5,6,28,29,101) AND dt.jenis_order = "ONT_PREMIUM" THEN 1 ELSE 0 END) as progress,
      SUM(CASE WHEN pl.status_laporan IN (97,99,100,102,103) AND dt.jenis_order = "ONT_PREMIUM" THEN 1 ELSE 0 END) as kendala,
      SUM(CASE WHEN pl.status_laporan = 98 AND dt.jenis_order = "ONT_PREMIUM" THEN 1 ELSE 0 END) as closed,
      COUNT(*) as jumlah
      FROM ont_premium_order opo
      LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      (dt.tgl LIKE "'.$date.'%" AND dt.jenis_order = "ONT_PREMIUM") OR ((DATE(opo.tanggal_order) LIKE "'.$date.'%") AND dt.jenis_order IS NULL)
      GROUP BY gt.title
      ORDER BY jumlah DESC
    ');
  }

  public static function detailDismanlte($group, $area, $type, $status, $date)
  {
    switch ($group) {
      case 'WITEL':

        $select = 'gt.Witel_New as area,';

        switch ($area) {
          case 'ALL':
            $where_area = '';
            break;
          case 'NON AREA':
            $where_area = 'gt.Witel_New IS NULL AND';
            break;
          default:
            $where_area = 'gt.Witel_New = "' . $area . '" AND';
            break;
        }

        break;
      case 'SEKTOR':

        $select = 'gt.title as area,';

        switch ($area) {
          case 'ALL':
            $where_area = '';
            break;
          case 'NON AREA':
            $where_area = 'gt.title IS NULL AND';
            break;
          default:
            $where_area = 'gt.title = "' . $area . '" AND';
            break;
        }

        break;
      
      case 'MITRA':

        $select = 'ma.mitra_amija_pt as area,';

        switch ($area) {
          case 'ALL':
            $where_area = '';
            break;
          case 'NON AREA':
            $where_area = 'ma.mitra_amija_pt IS NULL AND';
            break;
          default:
            $where_area = 'ma.mitra_amija_pt = "' . $area . '" AND';
            break;
        }

        break;
    }

    if ($status == "und_null")
    {
      $where_status = 'cno.jenis_dismantling IS NULL AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")';
    } elseif ($status == "und_ct0") {
      $where_status = 'cno.jenis_dismantling IN ("NEW CT0","CT0","Dismantling CT0") AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")';
    } elseif ($status == "und_massal") {
      $where_status = 'cno.jenis_dismantling NOT IN ("NEW CT0","CT0","Dismantling CT0","147","MIGRASI 147","NEW LOSS") AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")';
    } elseif ($status== "und_147") {
      $where_status = 'cno.jenis_dismantling IN ("147","MIGRASI 147") AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")';
    } elseif ($status== "und_newloss") {
      $where_status = 'cno.jenis_dismantling = "NEW LOSS" AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")';
    } elseif ($status== "jml_ont_h1") {
      $where_status = 'crd.jenis_nte = "ont" AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status== "jml_stb_h1") {
      $where_status = 'crd.jenis_nte = "stb" AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status== "jml_others_h1") {
      $where_status = 'crd.jenis_nte NOT IN ("ont","stb") AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status== "total_h1") {
      $where_status = 'crd.jenis_nte IN ("ont","stb") AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status== "jml_ont_hi") {
      $where_status = 'crd.jenis_nte = "ont" AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status== "jml_stb_hi") {
      $where_status = 'crd.jenis_nte = "stb" AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status== "jml_others_hi") {
      $where_status = 'crd.jenis_nte NOT IN ("ont","stb") AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status== "total_hi") {
      $where_status = 'crd.jenis_nte <> "" AND (DATE(crd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status== "jml_ont_bi") {
      $where_status = 'crd.jenis_nte = "ont" AND (DATE(crd.tanggalcreate) BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-d') . '")';
    } elseif ($status== "jml_stb_bi") {
      $where_status = 'crd.jenis_nte = "stb" AND (DATE(crd.tanggalcreate) BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_others_bi") {
      $where_status = 'crd.jenis_nte NOT IN ("ont","stb") AND (DATE(crd.tanggalcreate) BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-d') . '")';
    } elseif ($status == "total_bi") {
      $where_status = 'crd.jenis_nte <> "" AND (DATE(crd.tanggalcreate) BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_visit_ct0_h1") {
      $where_status = 'vrd.tipe_dapros IN ("CT0","NEW CT0") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status == "jml_visit_massal_h1") {
      $where_status = 'vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status == "jml_visit_147_h1") {
      $where_status = 'vrd.tipe_dapros IN ("147","MIGRASI 147") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status == "jml_visit_newloss_h1") {
      $where_status = 'vrd.tipe_dapros = "NEW LOSS" AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status == "total_visit_h1") {
      $where_status = 'vrd.tipe_dapros <> "" AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d', strtotime("-1 days")) . '")';
    } elseif ($status == "jml_visit_ct0_hi") {
      $where_status = 'vrd.tipe_dapros IN ("CT0","NEW CT0") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_visit_massal_hi") {
      $where_status = 'vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_visit_147_hi") {
      $where_status = 'vrd.tipe_dapros IN ("147","MIGRASI 147") AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_visit_newloss_hi") {
      $where_status = 'vrd.tipe_dapros = "NEW LOSS" AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status == "total_visit_hi") {
      $where_status = 'vrd.tipe_dapros <> "" AND (DATE(vrd.tanggalcreate) = "' . date('Y-m-d') . '")';
    } elseif ($status == "jml_visit_ct0_bi") {
      $where_status = 'vrd.tipe_dapros IN ("CT0","NEW CT0") AND (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")';
    } elseif ($status == "jml_visit_massal_bi") {
      $where_status = 'vrd.tipe_dapros NOT IN ("CT0","NEW CT0","147","MIGRASI 147","NEW LOSS","") AND (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")';
    } elseif ($status == "jml_visit_147_bi") {
      $where_status = 'vrd.tipe_dapros IN ("147","MIGRASI 147") AND (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")';
    } elseif ($status == "jml_visit_newloss_bi") {
      $where_status = 'vrd.tipe_dapros = "NEW LOSS" AND (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")';
    } elseif ($status == "total_visit_bi") {
      $where_status = 'vrd.tipe_dapros <> "" AND (DATE(vrd.tanggalcreate) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'")';
    } elseif ($status == "TIDAK_BISA_BAYAR") {
      $where_status = 'plds.id_status = 1 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "SERING_GANGGUAN") {
      $where_status = 'plds.id_status = 2 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "PINDAH_RUMAH") {
      $where_status = 'plds.id_status = 3 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "SALAH_PAKET") {
      $where_status = 'plds.id_status = 4 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "SUDAH_TIDAK_DIPAKAI") {
      $where_status = 'plds.id_status = 5 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "RUMAH_TIDAK_DITEMPATI") {
      $where_status = 'plds.id_status = 6 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "INTERNET_LAMBAT") {
      $where_status = 'plds.id_status = 7 AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "ALL_DONE") {
      $where_status = 'plds.id_status IN (1,2,3,4,5,6,7) AND dt.jenis_order = "CABUT_NTE" AND pls.laporan_status_id = 94 AND (DATE(pl.modified_at) LIKE "' . $date . '%")';
    } elseif ($status == "ALL_ORDER_DISPATCH") {
      $where_status = 'dt.jenis_order = "CABUT_NTE" AND (DATE(dt.tgl) LIKE "'.$date.'%")';
    }

    if (in_array($type, ["SISA_ORDER","DONE","DISPATCH"]))
    {
      return DB::SELECT('
        SELECT
        '.$select. '
        dt.id as id_dt,
        dt.tgl as tgl_dt,
        pls.laporan_status,
        dt.jenis_order,
        plds.status_dismantling,
        r.uraian as tim,
        pl.snstb,
        crd.sn as crd_sn,
        cno.wfm_id as nopel,
        crd.jenis_nte,
        pl.snont,
        pl.snstb,
        cno.*
        FROM cabut_nte_order cno
        LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
        LEFT JOIN collectedRacoonDismantling crd ON dt.NO_ORDER = crd.nopel
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN psb_laporan_dismatling_status plds ON pl.dismantling_status = plds.id_status
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
        ' . $where_area . '
        ' . $where_status . '
        ORDER BY cno.tgl_order DESC
      ');
    } elseif ($type == "COLLECTED") {

      return DB::SELECT('
      SELECT
      ' . $select . '
      dt.id as id_dt,
      dt.jenis_order,
      r.uraian as tim,
      pl.snont,
      pl.snstb,
      crd.*
      FROM collectedRacoonDismantling crd
      LEFT JOIN dispatch_teknisi dt ON crd.nopel = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      ' . $where_area . '
      ' . $where_status . '
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      GROUP BY crd.nopel
      ORDER BY crd.tanggalcreate DESC
      ');

    } elseif ($type == "VISIT") {

      return DB::SELECT('
      SELECT
      ' . $select . '
      dt.id as id_dt,
      dt.jenis_order,
      r.uraian as tim,
      pl.snont,
      pl.snstb,
      vrd.*
      FROM visitRacoonDismantling vrd
      LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
      ' . $where_area . '
      ' . $where_status . '
      AND (dt.jenis_order IS NULL OR dt.jenis_order = "CABUT_NTE")
      GROUP BY vrd.nopel
      ORDER BY vrd.tanggalcreate DESC
      ');

    }

  }

  public static function detailOntPremium($group, $sektor, $type, $status, $date){

    if ($sektor=="ALL")
    {
      $where_sektor = '';
    } elseif ($sektor=="NA") {
      $where_sektor = 'gt.title IS NULL AND';
    } else {
      $where_sektor = 'gt.title = "'.$sektor.'" AND';
    }

    if ($status=="UNDISPATCH")
    {
      $where_status = 'opo.dispatch_by LIKE "UNDISPATCH%" AND (dt.jenis_order IS NULL AND dt.Ndem IS NULL) AND (DATE(opo.tanggal_order) LIKE "'.$date.'%")';
    } elseif ($status=="NO_UPDATE") {
      $where_status = 'dt.Ndem <> "" AND pl.id_tbl_mj IS NULL AND dt.jenis_order = "ONT_PREMIUM" AND dt.tgl LIKE "'.$date.'%"';
    } elseif ($status=="PROGRESS") {
      $where_status = 'pl.status_laporan IN (5,6,28,29,101) AND dt.jenis_order = "ONT_PREMIUM" AND dt.tgl LIKE "'.$date.'%"';
    } elseif ($status=="KENDALA") {
      $where_status = 'pl.status_laporan IN (97,99,100,102,103) AND dt.jenis_order = "ONT_PREMIUM" AND dt.tgl LIKE "'.$date.'%"';
    } elseif ($status=="CLOSED") {
      $where_status = 'pl.status_laporan = 98 AND dt.jenis_order = "ONT_PREMIUM" AND dt.tgl LIKE "'.$date.'%"';
    } elseif ($status=="ALL" && $sektor=="NA" || $sektor=="ALL") {
      $where_status = '(dt.tgl LIKE "'.$date.'%" AND dt.jenis_order = "ONT_PREMIUM") OR ((DATE(opo.tanggal_order) LIKE "'.$date.'%") AND dt.jenis_order IS NULL)';
    } elseif ($status=="ALL" && $sektor <> "NA") {
      $where_status = '(dt.tgl LIKE "'.$date.'%" AND dt.jenis_order = "ONT_PREMIUM")';
    }
    return DB::SELECT('
    SELECT
    opo.*,
    dt.id as id_dt,
    dt.tgl as tgl_dt,
    pl.kordinat_pelanggan,
    pl.catatan,
    pl.modified_at,
    pl.noPelangganAktif,
    pls.laporan_status,
    r.uraian as tim,
    gt.title as sektor
    FROM ont_premium_order opo
    LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    '.$where_sektor.'
    '.$where_status.'
    ORDER BY opo.tanggal_order DESC
    ');
  }

  public static function comparinData($type, $group, $area)
  {
    switch ($type) {
      case 'CANCEL':
        $types = 'AND cda.STATUS_RESUME IN ("CANCEL COMPLETED","REVOKE COMPLETED")';
        break;
      
      default:
        $types = 'AND cda.type_grab = "' . $type . '"';
        break;
    }

    switch ($group) {
      case 'SEKTOR':

        $select = 'gt.title as area,';

        switch ($area) {
          case 'ALL':
            $areas = '';
            break;

          case 'NON AREA':
            $areas = 'AND gt.title IS NULL';
            break;

          default:
            $areas = 'AND gt.title = "' . $area . '"';
            break;
        }
        break;

      case 'PROV':

        $select = 'md.sektor_prov as area,';

        switch ($area) {
          case 'ALL':
            $areas = '';
            break;

          case 'NON AREA':
            $areas = 'AND md.sektor_prov IS NULL';
            break;

          default:
            $areas = 'AND md.sektor_prov = "' . $area . '"';
            break;
        }
        break;

      case 'DATEL':

        $select = 'md.datel as area,';

        switch ($area) {
          case 'ALL':
            $areas = '';
            break;

          case 'NON AREA':
            $areas = 'AND md.datel IS NULL';
            break;

          default:
            $areas = 'AND md.datel = "' . $area . '"';
            break;
        }
        break;
      
      case 'HERO':

        $select = 'md.HERO as area,';

        switch ($area) {
          case 'ALL':
            $areas = '';
            break;

          case 'NON AREA':
            $areas = 'AND md.HERO IS NULL';
            break;
          
          default:
            $areas = 'AND md.HERO = "' . $area . '"';
            break;
        }
        break;
      
      case 'EGBIS':

        $select = 'md.sektor_prov as area,';

        switch ($area) {
          case 'ALL':
            $areas = 'AND cda.provider NOT LIKE "DCS%"';
            break;

          case 'NON AREA':
            $areas = 'AND cda.provider NOT LIKE "DCS%" AND md.sektor_prov IS NULL';
            break;

          default:
            $areas = 'AND cda.provider NOT LIKE "DCS%" AND md.sektor_prov = "' . $area . '"';
            break;
        }
        break;
    }
    

    return DB::SELECT('
    SELECT
    ' . $select . '
    r.uraian as tim,
    pl.modified_at,
    pls.laporan_status,
    cda.*
    FROM comparin_data_api cda
    LEFT JOIN dispatch_teknisi dt ON cda.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN maintenance_datel md ON cda.STO = md.sto
    WHERE
    cda.JENISPSB = "AO"
    '.$types.'
    '.$areas.'
    ORDER BY cda.ORDER_DATE ASC
    ');
  }

  public static function allOpenTiket($witel,$date)
  {

    if ($witel == "KALSEL")
    {
      return DB::SELECT('
        SELECT
        date_format(dn1l.Reported_Datex, "%Y-%m-%d") as tgl,
        SUM(CASE WHEN dn1l.Source = "TOMMAN" THEN 1 ELSE 0 END) as jml_open_tomman,
        SUM(CASE WHEN dn1l.Source = "RIGHTNOW" THEN 1 ELSE 0 END) as jml_reguler,
        SUM(CASE WHEN dn1l.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dn1l.Owner_Group = "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" THEN 1 ELSE 0 END) as jml_unspec,
        SUM(CASE WHEN dn1l.Summary LIKE "%SQM%" THEN 1 ELSE 0 END) as jml_sqm
        FROM data_nossa_1_log dn1l
        WHERE
        date_format(dn1l.Reported_Datex, "%Y-%m") = "'.date('Y-m', strtotime($date)).'"
        GROUP BY tgl ORDER BY tgl
      ');
    } elseif ($witel == "BALIKPAPAN") {
      return DB::connection('bpp')->SELECT('
        SELECT
        date_format(dnt.Reported_Datex, "%Y-%m-%d") as tgl,
        SUM(CASE WHEN dnt.Source = "RIGHTNOW" THEN 1 ELSE 0 END) as jml_reguler,
        SUM(CASE WHEN dnt.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dnt.Owner_Group = "ACCESS MAINTENANCE WITEL KALTIMSEL (BALIKPAPAN)" THEN 1 ELSE 0 END) as jml_unspec,
        SUM(CASE WHEN dnt.Summary LIKE "%SQM%" THEN 1 ELSE 0 END) as jml_sqm
        FROM data_nossa_today dnt
        WHERE
        date_format(dnt.Reported_Datex, "%Y-%m") = "'.date('Y-m', strtotime($date)).'"
        GROUP BY tgl ORDER BY tgl
      ');
    }
  }

  public static function get_sektor($date)
  {
    return DB::SELECT('
      SELECT
      e.chat_id,
      e.ioan,
      SUM(case when f.source = "TOMMAN" then 1 else 0 end) as jumlah_INT,
      SUM(case when f.source <> "TOMMAN" then 1 else 0 end) as jumlah_IN
      FROM psb_laporan a
      LEFT JOIN psb_laporan_action b ON a.action = b.laporan_action_id
      LEFT JOIN dispatch_teknisi c ON a.id_tbl_mj = c.id
      LEFT JOIN regu d ON c.id_regu = d.id_regu
      LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
      LEFT JOIN data_nossa_1_log f ON c.Ndem = f.Incident
      WHERE
      (DATE(c.tgl) LIKE "'.$date.'%") AND b.action IS NOT NULL and c.jenis_order IN ("IN","INT") AND e.ioan <> ""
      GROUP BY e.ioan
      ORDER BY e.urut
    ');
  }

  public static function assuranceBPP($date)
  {


    $workzone = DB::connection('bpp')->SELECT('SELECT Workzone FROM data_nossa_today GROUP BY Workzone ORDER BY Workzone ASC');

    // jumlah order
    $query1 = DB::connection('bpp')->SELECT('
      SELECT
        dnt.Workzone,
        SUM(CASE WHEN dnt.Customer_Segment = "DCS" THEN 1 ELSE 0 END) as jml_dcs,
        SUM(CASE WHEN dnt.Customer_Segment = "DBS" THEN 1 ELSE 0 END) as jml_dbs,
        SUM(CASE WHEN dnt.Customer_Segment = "DES" THEN 1 ELSE 0 END) as jml_des,
        SUM(CASE WHEN dnt.Customer_Segment = "DGS" THEN 1 ELSE 0 END) as jml_dgs,
        SUM(CASE WHEN dnt.Customer_Segment = "DS0" THEN 1 ELSE 0 END) as jml_dso,
        COUNT(*) as jml_order
      FROM data_nossa_today dnt
      WHERE
        (DATE(dnt.Reported_Datex) = "'.$date.'")
      GROUP BY dnt.Workzone
    ');

    // ttr sisa saldo tiket
    $query2 = DB::connection('bpp')->SELECT('
      SELECT
        dna.Workzone,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >= 2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" AND dna.Assigned_by IN ("AUTOASSIGNED","") THEN 1 ELSE 0 END) as WNOK,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=0 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 1 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as DIBAWAH1,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=1 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 2 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as DIBAWAH2,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=2 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 3 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as DIBAWAH3,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=3 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 12 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as DIBAWAH12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=12 AND TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") < 24 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as DIATAS12,
        SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dna.Reported_Datex, "'.date('Y-m-d H:i:s').'") >=24 AND dna.Induk_Gamas = "" THEN 1 ELSE 0 END) as LEBIH1HARI,
        SUM(CASE WHEN dna.Induk_Gamas <> "" THEN 1 ELSE 0 END) as GAMAS,
        COUNT(*) as JUMLAH
      FROM data_nossa_active dna
      WHERE
        dna.Incident IS NOT NULL AND dna.Service_Type <> "NON-NUMBERING"
      GROUP BY dna.Workzone
      ORDER BY JUMLAH DESC
    ');

    // rata rata close order
    $query3 = DB::connection('bpp')->SELECT('
      SELECT
        dnt.Workzone,
        SUM(CASE WHEN dnt.Source = "RIGHTNOW" THEN 1 ELSE 0 END) as order_RIGHTNOW,
        SUM(CASE WHEN dnt.Source = "RIGHTNOW" AND dnt.Status = "CLOSED" THEN 1 ELSE 0 END) as close_RIGHTNOW,
        SUM(CASE WHEN dnt.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dnt.Owner_Group = "ACCESS MAINTENANCE WITEL KALTIMSEL (BALIKPAPAN)" THEN 1 ELSE 0 END) as order_UNSPEC,
        SUM(CASE WHEN dnt.Incident_Symptom = "PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" OR dnt.Owner_Group = "ACCESS MAINTENANCE WITEL KALTIMSEL (BALIKPAPAN)" AND dnt.Status = "CLOSED" THEN 1 ELSE 0 END) as close_UNSPEC,
        SUM(CASE WHEN dnt.Summary LIKE "%SQM%" THEN 1 ELSE 0 END) as order_SQM,
        SUM(CASE WHEN dnt.Summary LIKE "%SQM%" AND dnt.Status = "CLOSED" THEN 1 ELSE 0 END) as close_SQM
      FROM data_nossa_today dnt
      WHERE
        (DATE(dnt.Reported_Datex) = "'.$date.'")
      GROUP BY dnt.Workzone
    ');

    foreach($workzone as $val)
    {
      foreach($query1 as $val_c1)
      {
        if (!empty($val_c1->Workzone))
        {
          if ($val_c1->Workzone == $val->Workzone)
          {
            $query[$val->Workzone]['jml_dcs'] = $val_c1->jml_dcs;
            $query[$val->Workzone]['jml_dbs'] = $val_c1->jml_dbs;
            $query[$val->Workzone]['jml_des'] = $val_c1->jml_des;
            $query[$val->Workzone]['jml_dgs'] = $val_c1->jml_dgs;
            $query[$val->Workzone]['jml_dso'] = $val_c1->jml_dso;
          }
        } else {
            $query['NON WORKZONE']['jml_dcs'] = $val_c1->jml_dcs;
            $query['NON WORKZONE']['jml_dbs'] = $val_c1->jml_dbs;
            $query['NON WORKZONE']['jml_des'] = $val_c1->jml_des;
            $query['NON WORKZONE']['jml_dgs'] = $val_c1->jml_dgs;
            $query['NON WORKZONE']['jml_dso'] = $val_c1->jml_dso;
        }
      }
      foreach($query2 as $val_c2)
      {
        if (!empty($val_c2->Workzone))
        {
          if ($val_c2->Workzone == $val->Workzone)
          {
            $query[$val->Workzone]['WNOK'] = $val_c2->WNOK;
            $query[$val->Workzone]['DIBAWAH3'] = $val_c2->DIBAWAH3;
            $query[$val->Workzone]['DIBAWAH12'] = $val_c2->DIBAWAH12;
            $query[$val->Workzone]['DIATAS12'] = $val_c2->DIATAS12;
            $query[$val->Workzone]['LEBIH1HARI'] = $val_c2->LEBIH1HARI;
            $query[$val->Workzone]['GAMAS'] = $val_c2->GAMAS;
            $query[$val->Workzone]['JUMLAH'] = $val_c2->JUMLAH;
          }
        } else {
            $query['NON WORKZONE']['WNOK'] = $val_c2->WNOK;
            $query['NON WORKZONE']['DIBAWAH3'] = $val_c2->DIBAWAH3;
            $query['NON WORKZONE']['DIBAWAH12'] = $val_c2->DIBAWAH12;
            $query['NON WORKZONE']['DIATAS12'] = $val_c2->DIATAS12;
            $query['NON WORKZONE']['LEBIH1HARI'] = $val_c2->LEBIH1HARI;
            $query['NON WORKZONE']['GAMAS'] = $val_c2->GAMAS;
            $query['NON WORKZONE']['JUMLAH'] = $val_c2->JUMLAH;
        }
      }
      foreach($query3 as $val_c3)
      {
        if (!empty($val_c3->Workzone))
        {
          if ($val_c3->Workzone == $val->Workzone)
          {
            $query[$val->Workzone]['order_RIGHTNOW'] = $val_c3->order_RIGHTNOW;
            $query[$val->Workzone]['close_RIGHTNOW'] = $val_c3->close_RIGHTNOW;
            $query[$val->Workzone]['order_UNSPEC'] = $val_c3->order_UNSPEC;
            $query[$val->Workzone]['close_UNSPEC'] = $val_c3->close_UNSPEC;
            $query[$val->Workzone]['order_SQM'] = $val_c3->order_SQM;
            $query[$val->Workzone]['close_SQM'] = $val_c3->close_SQM;
          }
        } else {
            $query['NON WORKZONE']['order_RIGHTNOW'] = $val_c3->order_RIGHTNOW;
            $query['NON WORKZONE']['close_RIGHTNOW'] = $val_c3->close_RIGHTNOW;
            $query['NON WORKZONE']['order_UNSPEC'] = $val_c3->order_UNSPEC;
            $query['NON WORKZONE']['close_UNSPEC'] = $val_c3->close_UNSPEC;
            $query['NON WORKZONE']['order_SQM'] = $val_c3->order_SQM;
            $query['NON WORKZONE']['close_SQM'] = $val_c3->close_SQM;
        }
      }
    }

    return $query;

  }

  public static function racingPS($start, $end)
  {
    $employee = DB::SELECT('SELECT nik, nama FROM 1_2_employee WHERE Witel_New = "KALSEL" GROUP BY nik ORDER BY last_update DESC');

    $query1 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      r.uraian as nama_tim,
      SUM(CASE WHEN qbt.sc THEN 1 ELSE 0 END) as jml_order_ao_nik1
      FROM dispatch_teknisi dt
      LEFT JOIN qc_borneo_tr6 qbt ON dt.NO_ORDER = qbt.sc
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN 1_2_employee emp ON r.nik1 = emp.nik
      WHERE
      dps.jenisPsb LIKE "AO%" AND
      qbt.tag_unit = "Valid;" AND
      (DATE(qbt.dtPs) BETWEEN "'.$start.'" AND "'.$end. '") AND
      emp.nik IS NOT NULL
      GROUP BY emp.nik
      ORDER BY jml_order_ao_nik1 DESC
    ');

    $query2 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      r.uraian as nama_tim,
      SUM(CASE WHEN qbt.sc THEN 1 ELSE 0 END) as jml_order_ao_nik2
      FROM dispatch_teknisi dt
      LEFT JOIN qc_borneo_tr6 qbt ON dt.NO_ORDER = qbt.sc
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN 1_2_employee emp ON r.nik2 = emp.nik
      WHERE
      dps.jenisPsb LIKE "AO%" AND
      qbt.tag_unit = "Valid;" AND
      (DATE(qbt.dtPs) BETWEEN "'.$start.'" AND "'.$end.'") AND
      emp.nik IS NOT NULL
      GROUP BY emp.nik
      ORDER BY jml_order_ao_nik2 DESC
    ');

    $query3 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN m.no_tiket THEN 1 ELSE 0 END) as jml_order_sk_nik1
      FROM maintaince m
      LEFT JOIN dispatch_teknisi dt ON m.no_tiket = dt.Ndem
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik1 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      m.headline LIKE "SABER%" AND
      m.status = "close" AND
      (DATE(m.modified_at) BETWEEN "'.$start.'" AND "'.$end. '")
      GROUP BY emp.nik
      ORDER BY jml_order_sk_nik1 DESC
    ');

    $query4 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN m.no_tiket THEN 1 ELSE 0 END) as jml_order_sk_nik2
      FROM maintaince m
      LEFT JOIN dispatch_teknisi dt ON m.no_tiket = dt.Ndem
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik2 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      m.headline LIKE "SABER%" AND
      m.status = "close" AND
      (DATE(m.modified_at) BETWEEN "'.$start.'" AND "'.$end. '")
      GROUP BY emp.nik
      ORDER BY jml_order_sk_nik2 DESC
    ');

    $query5 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN kpt.ORDER_ID THEN 1 ELSE 0 END) as jml_order_mo_nik1
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik1 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order IN ("SC","MYIR") AND
      kpt.JENIS_PSB = "MO" AND kpt.EXTERN_ORDER_ID = "1500" AND
      (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$start.'" AND "'.$end. '")
      GROUP BY emp.nik
      ORDER BY jml_order_mo_nik1 DESC
    ');

    $query6 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN kpt.ORDER_ID THEN 1 ELSE 0 END) as jml_order_mo_nik2
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik2 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order IN ("SC","MYIR") AND
      kpt.JENIS_PSB = "MO" AND kpt.EXTERN_ORDER_ID = "1500" AND
      (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY emp.nik
      ORDER BY jml_order_mo_nik2 DESC
    ');

    $query7 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN cno.wfm_id THEN 1 ELSE 0 END) as jml_order_dism_nik1
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik1 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order = "CABUT_NTE" AND
      pl.status_laporan = 94 AND
      (DATE(pl.modified_at) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY emp.nik
      ORDER BY jml_order_dism_nik1 DESC
    ');

    $query8 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN cno.wfm_id THEN 1 ELSE 0 END) as jml_order_dism_nik2
      FROM cabut_nte_order cno
      LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik2 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order = "CABUT_NTE" AND
      pl.status_laporan = 94 AND
      (DATE(pl.modified_at) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY emp.nik
      ORDER BY jml_order_dism_nik2 DESC
    ');

    $query9 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN prt.SC_ID_INT THEN 1 ELSE 0 END) as jml_order_ffg_nik1
      FROM prabac_tr6 prt
      LEFT JOIN dispatch_teknisi dt ON prt.SC_ID_INT = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik1 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order IN ("SC","MYIR") AND
      (DATE(prt.TGL_PS) BETWEEN "'.$start.'" AND "'.$end.'")
      GROUP BY emp.nik
      ORDER BY jml_order_ffg_nik1 DESC
    ');

    $query10 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      SUM(CASE WHEN prt.SC_ID_INT THEN 1 ELSE 0 END) as jml_order_ffg_nik2
      FROM prabac_tr6 prt
      LEFT JOIN dispatch_teknisi dt ON prt.SC_ID_INT = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN 1_2_employee emp ON r.nik2 = emp.nik
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      emp.nik IS NOT NULL AND
      dt.jenis_order IN ("SC","MYIR") AND
      (DATE(prt.TGL_PS) BETWEEN "' . $start . '" AND "' . $end . '")
      GROUP BY emp.nik
      ORDER BY jml_order_ffg_nik2 DESC
    ');

    $query11 = DB::SELECT('
      SELECT
      emp.nik,
      emp.nama,
      ppt.point_now,
      ppt.point_old,
      ppt.coupon_vip,
      ppt.coupon_reguler
      FROM prov_point_tech ppt
      LEFT JOIN 1_2_employee emp ON ppt.tech_nik = emp.nik
      WHERE
      emp.nik IS NOT NULL
      GROUP BY emp.nik
    ');

    foreach ($employee as $emp) {
      foreach ($query1 as $val_c1) {
        if (!empty($val_c1->nik)) {
          if ($val_c1->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_ao_nik1'] = $val_c1->jml_order_ao_nik1;
          }
        } else {
          $query['NONE']['jml_order_ao_nik1'] = $val_c1->jml_order_ao_nik1;
        }
      }
      foreach ($query2 as $val_c2) {
        if (!empty($val_c2->nik)) {
          if ($val_c2->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_ao_nik2'] = $val_c2->jml_order_ao_nik2;
          }
        } else {
          $query['NONE']['jml_order_ao_nik2'] = $val_c2->jml_order_ao_nik2;
        }
      }
      foreach ($query3 as $val_c3) {
        if (!empty($val_c3->nik)) {
          if ($val_c3->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_sk_nik1'] = $val_c3->jml_order_sk_nik1;
          }
        } else {
          $query['NONE']['jml_order_sk_nik1'] = $val_c3->jml_order_sk_nik1;
        }
      }
      foreach ($query4 as $val_c4) {
        if (!empty($val_c4->nik)) {
          if ($val_c4->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_sk_nik2'] = $val_c4->jml_order_sk_nik2;
          }
        } else {
          $query['NONE']['jml_order_sk_nik2'] = $val_c4->jml_order_sk_nik2;
        }
      }
      foreach ($query5 as $val_c5) {
        if (!empty($val_c5->nik)) {
          if ($val_c5->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_mo_nik1'] = $val_c5->jml_order_mo_nik1;
          }
        } else {
          $query['NONE']['jml_order_mo_nik1'] = $val_c5->jml_order_mo_nik1;
        }
      }
      foreach ($query6 as $val_c6) {
        if (!empty($val_c6->nik)) {
          if ($val_c6->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_mo_nik2'] = $val_c6->jml_order_mo_nik2;
          }
        } else {
          $query['NONE']['jml_order_mo_nik2'] = $val_c5->jml_order_mo_nik2;
        }
      }
      foreach ($query7 as $val_c7) {
        if (!empty($val_c7->nik)) {
          if ($val_c7->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_dism_nik1'] = $val_c7->jml_order_dism_nik1;
          }
        } else {
          $query['NONE']['jml_order_dism_nik1'] = $val_c7->jml_order_dism_nik1;
        }
      }
      foreach ($query8 as $val_c8) {
        if (!empty($val_c8->nik)) {
          if ($val_c8->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_dism_nik2'] = $val_c8->jml_order_dism_nik2;
          }
        } else {
          $query['NONE']['jml_order_dism_nik2'] = $val_c8->jml_order_dism_nik2;
        }
      }
      foreach ($query9 as $val_c9) {
        if (!empty($val_c9->nik)) {
          if ($val_c9->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_ffg_nik1'] = $val_c9->jml_order_ffg_nik1;
          }
        } else {
          $query['NONE']['jml_order_ffg_nik1'] = $val_c9->jml_order_ffg_nik1;
        }
      }
      foreach ($query10 as $val_c10) {
        if (!empty($val_c10->nik)) {
          if ($val_c10->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['jml_order_ffg_nik2'] = $val_c10->jml_order_ffg_nik2;
          }
        } else {
          $query['NONE']['jml_order_ffg_nik2'] = $val_c10->jml_order_ffg_nik2;
        }
      }
      foreach ($query11 as $val_c11) {
        if (!empty($val_c11->nik)) {
          if ($val_c11->nik == $emp->nik) {
            $query[$emp->nik]['nama'] = $emp->nama;
            $query[$emp->nik]['point_now'] = $val_c11->point_now;
            $query[$emp->nik]['point_old'] = $val_c11->point_old;
            $query[$emp->nik]['coupon_vip'] = $val_c11->coupon_vip;
            $query[$emp->nik]['coupon_reguler'] = $val_c11->coupon_reguler;

          }
        } else {
          $query['NONE']['point_now'] = $val_c11->point_now;
          $query['NONE']['point_old'] = $val_c11->point_old;
          $query['NONE']['coupon_vip'] = $val_c11->coupon_vip;
          $query['NONE']['coupon_reguler'] = $val_c11->coupon_reguler;

        }
      }
    }

    return $query;
  }

  public static function psreKpro($area, $segmen)
  {
    switch ($segmen) {
      case 'REGULER':
        $segmen = 'AND skt.PROVIDER LIKE "DCS%"';
        break;
      
      case 'CORPORATE':
        $segmen = 'AND skt.PROVIDER NOT LIKE "DCS%"';
        break;

      default:
        $segmen = '';
        break;
    }

    $periode = date('Ym');

    switch ($area) {
      case 'ALL':
        return DB::SELECT('
          SELECT
            skt.WITEL as area,
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
            COUNT(*) as jumlah,
            ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
              SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
            ) ) * 100 , 2) AS round_psre
          FROM selfie_kpro_tr6 skt
          WHERE
            skt.PERIODE = '.$periode.'
            '.$segmen.'
          GROUP BY skt.WITEL
          ORDER BY round_psre DESC
        ');
        break;
      
      case 'BANJARMASIN':
        return DB::SELECT('
          SELECT
            md.sektor_prov as area,
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
            COUNT(*) as jumlah,
            ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
              SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
            ) ) * 100 , 2) AS round_psre
          FROM selfie_kpro_tr6 skt
          LEFT JOIN maintenance_datel md ON skt.STO = md.sto
          WHERE
            skt.PERIODE = '.$periode.' AND skt.WITEL = "BANJARMASIN"
            '.$segmen.'
          GROUP BY md.sektor_prov
          ORDER BY round_psre DESC
        ');
        break;

      case 'BALIKPAPAN_SEKTOR':
        return DB::SELECT('
          SELECT
            md.sektor_prov as area,
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
            COUNT(*) as jumlah,
            ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
              SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
            ) ) * 100 , 2) AS round_psre
          FROM selfie_kpro_tr6 skt
          LEFT JOIN maintenance_datel md ON skt.STO = md.sto
          WHERE
            skt.PERIODE = '.$periode.' AND skt.WITEL = "BALIKPAPAN"
            '.$segmen.'
          GROUP BY md.sektor_prov
          ORDER BY round_psre DESC
        ');
        break;

      case 'BALIKPAPAN_STO':
        return DB::SELECT('
          SELECT
            md.sto as area,
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
            COUNT(*) as jumlah,
            ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
              SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
              SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
              SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
            ) ) * 100 , 2) AS round_psre
          FROM selfie_kpro_tr6 skt
          LEFT JOIN maintenance_datel md ON skt.STO = md.sto
          WHERE
            skt.PERIODE = '.$periode.' AND skt.WITEL = "BALIKPAPAN"
            '.$segmen.'
          GROUP BY md.sto
          ORDER BY round_psre DESC
        ');
        break;

      case 'SEKTOR':
        return DB::select('
        SELECT
          gt.title as area,
          SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
          SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
          SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
          SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
          SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
          SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
          COUNT(*) as jumlah,
          ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
          ) ) * 100 , 2) AS round_psre
        FROM selfie_kpro_tr6 skt
        LEFT JOIN maintenance_datel md ON skt.STO = md.sto
        LEFT JOIN dispatch_teknisi dt oN skt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
        WHERE
          skt.PERIODE = '.$periode.' AND skt.WITEL = "BANJARMASIN"
          '.$segmen.'
        GROUP BY gt.title
        ORDER BY round_psre DESC
        ');
        break;

      case 'MITRA':
        return DB::select('
        SELECT
          ma.mitra_amija_pt as area,
          SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
          SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
          SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
          SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
          SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
          SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
          COUNT(*) as jumlah,
          ROUND( (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) / (
            SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) + 
            SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) + 
            SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) +
            SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)
          ) ) * 100 , 2) AS round_psre
        FROM selfie_kpro_tr6 skt
        LEFT JOIN maintenance_datel md ON skt.STO = md.sto
        LEFT JOIN dispatch_teknisi dt oN skt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
          skt.PERIODE = '.$periode.' AND skt.WITEL = "BANJARMASIN"
          '.$segmen.'
        GROUP BY ma.mitra_amija_pt
        ORDER BY round_psre DESC
        ');
        break;
    }
  }

  public static function psreKpro_1($periode)
  {
    return DB::SELECT('
    SELECT
      md.sektor_prov,
      SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END) as cancel,
      SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END) as prapi,
      SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END) as ogp,
      SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END) as ps,
      SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END) as revoke_comp,
      SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END) as unsc,
      (SUM(CASE WHEN skt.STATUS_MESSAGE = "Completed" THEN 1 ELSE 0 END)/(SUM(CASE WHEN skt.STATUS_RESUME IN ("OSS - FALLOUT","OSS - PROVISIONING DESAIN","OSS - PROVISIONING START") THEN 1 ELSE 0 END)+
      SUM(CASE WHEN skt.STATUS_RESUME = "9 | WFM - UN-SCS" THEN 1 ELSE 0 END)+
      SUM(CASE WHEN skt.STATUS_RESUME IN ("Fallout (WFM)","7 | OSS - PROVISIONING ISSUED") THEN 1 ELSE 0 END)+
      SUM(CASE WHEN skt.STATUS_RESUME = "100 | REVOKE COMPLETED" THEN 1 ELSE 0 END)+
      SUM(CASE WHEN skt.STATUS_MESSAGE LIKE "%Cancel%" THEN 1 ELSE 0 END))) as psre_1,
      COUNT(*) as jumlah
    FROM selfie_kpro_tr6 skt
    LEFT JOIN maintenance_datel md ON skt.STO = md.sto
    WHERE
    skt.PERIODE = '.$periode.'
    GROUP BY md.sektor_prov
    ORDER BY psre_1 DESC
    ');
  }

  public static function dashboardFFG()
  {

    return DB::SELECT('
      SELECT
        md.sektor_prov,
        (SELECT COUNT(*) FROM kpro_tr6 kt WHERE kt.STATUS_MESSAGE = "Completed" AND kt.JENIS_PSB = "AO" AND TIMESTAMPDIFF(MONTH, kt.TANGGAL_PROSES, NOW()) = 2 AND kt.STO = md.sto) as ps_2bln,
        (SELECT COUNT(*) FROM prabac_wecare_ffg fkt WHERE fkt.sto = md.sto) as jml_ffg,
        (SELECT COUNT(*) FROM prabac_wecare_ffg fkt LEFT JOIN data_nossa_1 dn1 ON fkt.trouble_no_int = dn1.ID WHERE dn1.Status = "CLOSED" AND TIMESTAMPDIFF(HOUR , dn1.Resolved_Datex, NOW()) <= 3 AND fkt.sto = md.sto) as tti_3jam,
        (SELECT COUNT(*) FROM prabac_wecare_ffg fkt LEFT JOIN data_nossa_1 dn1 ON fkt.trouble_no_int = dn1.ID WHERE dn1.Status = "CLOSED" AND TIMESTAMPDIFF(HOUR , dn1.Resolved_Datex, NOW()) > 3 AND fkt.sto = md.sto) as tti_max3jam
      FROM maintenance_datel md
      WHERE
      md.witel = "KALSEL"
      GROUP BY md.sektor_prov
      ORDER BY jml_ffg DESC
    ');
  }

  public static function dashboardPSRE($type, $sektor, $status, $segmen)
  {
    $periode = date('Ym');

    switch (session('auth')->nama_witel) {
      case 'KALSEL':
          switch ($type) {
            case 'WITEL':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = '';
                  break;
                
                case 'NON AREA':
                  $where_sektor = 'skt.WITEL IS NULL AND';
                  break;
                
                default:
                  $where_sektor = 'skt.WITEL = "' . $sektor . '" AND';
                  break;
              }
              break;
            
            case 'SEKTOR':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = 'skt.WITEL = "BANJARMASIN" AND';
                  break;
      
                case 'NON AREA':
                  $where_sektor = 'md.sektor_prov IS NULL AND skt.WITEL = "BANJARMASIN" AND';
                  break;
      
                default:
                  $where_sektor = 'md.sektor_prov = "' . $sektor . '" AND skt.WITEL = "BANJARMASIN" AND';
                  break;
              }
              break;
      
            case 'SEKTORX':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = 'skt.WITEL = "BANJARMASIN" AND';
                  break;
      
                case 'NON AREA':
                  $where_sektor = 'gt.title IS NULL AND skt.WITEL = "BANJARMASIN" AND';
                  break;
      
                default:
                  $where_sektor = 'gt.title = "' . $sektor . '" AND skt.WITEL = "BANJARMASIN" AND';
                  break;
              }
              break;
      
            case 'MITRA':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = '';
                  break;
      
                case 'NON AREA':
                  $where_sektor = 'ma.mitra_amija_pt IS NULL AND skt.WITEL = "BANJARMASIN" AND';
                  break;
      
                default:
                  $where_sektor = 'ma.mitra_amija_pt = "' . $sektor . '" AND skt.WITEL = "BANJARMASIN" AND';
                  break;
              }
              break;
          }
        break;
      
      case 'BALIKPAPAN':
          switch ($type) {
            case 'WITEL':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = '';
                  break;
                
                case 'NON AREA':
                  $where_sektor = 'skt.WITEL IS NULL AND';
                  break;
                
                default:
                  $where_sektor = 'skt.WITEL = "' . $sektor . '" AND';
                  break;
              }
              break;
            
            case 'SEKTOR':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = 'skt.WITEL = "BALIKPAPAN" AND';
                  break;
      
                case 'NON AREA':
                  $where_sektor = 'md.sektor_prov IS NULL AND skt.WITEL = "BALIKPAPAN" AND';
                  break;
      
                default:
                  $where_sektor = 'md.sektor_prov = "' . $sektor . '" AND skt.WITEL = "BALIKPAPAN" AND';
                  break;
              }
              break;
      
            case 'STO':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = 'skt.WITEL = "BALIKPAPAN" AND';
                  break;
      
                case 'NON AREA':
                  $where_sektor = 'md.sto IS NULL AND skt.WITEL = "BALIKPAPAN" AND';
                  break;
      
                default:
                  $where_sektor = 'md.sto = "' . $sektor . '" AND skt.WITEL = "BALIKPAPAN" AND';
                  break;
              }
              break;
          }
        break;
      default:
          switch ($type) {
            case 'WITEL':
              switch ($sektor) {
                case 'ALL':
                  $where_sektor = '';
                  break;
                
                case 'NON AREA':
                  $where_sektor = 'skt.WITEL IS NULL AND';
                  break;
                
                default:
                  $where_sektor = 'skt.WITEL = "' . $sektor . '" AND';
                  break;
              }
              break;
          }
        break;
    }

    

    switch ($status) {
      case 'CANCEL':
        $where_status = 'AND skt.STATUS_MESSAGE LIKE "%Cancel%"';
        break;
      
      case 'PRA_PI':
        $where_status = 'AND skt.STATUS_RESUME IN ("OSS - PROVISIONING DESAIN","OSS - PROVISIONING START")';
        break;
      
      case 'OGP':
        $where_status = 'AND skt.STATUS_RESUME IN ("OSS - FALLOUT","Fallout (WFM)","7 | OSS - PROVISIONING ISSUED")';
        break;

      case 'PS':
        $where_status = 'AND skt.STATUS_MESSAGE = "Completed"';
        break;

      case 'REVOKE':
        $where_status = 'AND skt.STATUS_RESUME = "100 | REVOKE COMPLETED"';
        break;
      
      case 'UNSC':
        $where_status = 'AND skt.STATUS_RESUME = "9 | WFM - UN-SCS"';
        break;
    }

    switch ($segmen) {
      case 'REGULER':
        $where_segmen = 'AND skt.PROVIDER LIKE "DCS%"';
        break;
      
      case 'CORPORATE':
        $where_segmen = 'AND skt.PROVIDER NOT LIKE "DCS%"';
        break;

      default:
        $where_segmen = '';
        break;
    }

    return DB::SELECT('
      SELECT
        md.sektor_prov as area,
        gt.title as sektor,
        ma.mitra_amija_pt as mitra,
        skt.*,
        pls.laporan_status as status_laporan,
        pl.kordinat_pelanggan,
        pl.deviasi_order,
        dt.id as dt_id,
        pkt.status_prj,
        ptm.odp_nama as odp_pt2,
        ptm.lt_status as status_pt2,
        ptm.regu_name as tim_pt2,
        rr.mitra as mitra_pt2,
        dps.wfm_id,
        scso.status_survey,
        scso.updated_name as survey_updated_name,
        scso.updated_at as survey_updated_at
      FROM selfie_kpro_tr6 skt
      LEFT JOIN maintenance_datel md ON skt.STO = md.sto
      LEFT JOIN dispatch_teknisi dt ON skt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN Data_Pelanggan_Starclick dps ON skt.ORDER_ID = dps.orderIdInteger
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      LEFT JOIN pt2simple_kpro_tr6 pkt ON skt.ORDER_ID = pkt.order_id
      LEFT JOIN pt2_master ptm ON pkt.order_id = ptm.nomor_sc
      LEFT JOIN regu rr ON ptm.regu_id = rr.id_regu
      LEFT JOIN sc_survey_ondesk scso ON skt.ORDER_ID = scso.sc_order
      WHERE
        ' . $where_sektor . '
        skt.PERIODE = "' . $periode . '"
        '.$where_status.'
        '.$where_segmen.'
      ORDER BY skt.PERIODE ASC
    ');
  }

  public static function dashboardFFGTTI($sektor, $status)
  {
    if ($sektor == 'NONE SEKTOR')
    {
      $where_sektor = 'AND md.sektor_prov IS NULL';
    } elseif ($sektor == 'ALL') {
      $where_sektor = '';
    } else {
      $where_sektor = 'AND md.sektor_prov = "'.$sektor.'"';
    }

    if ($status == 'PS_KPRO')
    {
      $query = '
        SELECT
        md.sektor_prov,
        kt.*
        FROM kpro_tr6 kt
        LEFT JOIN maintenance_datel md ON kt.sto = md.sto
        WHERE
        kt.STATUS_MESSAGE = "Completed" AND
        kt.JENIS_PSB = "AO" AND
        (TIMESTAMPDIFF(MONTH, kt.TANGGAL_PROSES, NOW()) = 2)
        '.$where_sektor.'
      ';
    } elseif ($status == 'FFG') {
      $query = '
        SELECT
        md.sektor_prov,
        fkt.*
        FROM prabac_wecare_ffg fkt
        LEFT JOIN data_nossa_1 dn1 ON fkt.trouble_no_int = dn1.ID
        LEFT JOIN maintenance_datel md ON fkt.sto = md.sto
        WHERE
        fkt.trouble_no IS NOT NULL
        '.$where_sektor.'
      ';
    }

    return DB::SELECT($query);
  }

  public static function TTRcomparin()
  {
    return DB::SELECT('
      SELECT
      md.sektor_prov,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) = 1 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_1hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) > 1 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 3 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_1_2hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 2 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 4 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_2_3hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 3 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 5 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_3_4hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 4 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 7 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_4_6hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) > 7 AND type_grab = "PI_PAGI" THEN 1 ELSE 0 END) as pipagi_7hari,

      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) = 1 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_1hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) > 1 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 3 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_1_2hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 2 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 4 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_2_3hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 3 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 5 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_3_4hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) >= 4 AND TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) < 7 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_4_6hari,
      SUM(CASE WHEN TIMESTAMPDIFF(DAY , cda.ORDER_DATE, NOW()) > 7 AND type_grab = "FO_PAGI" THEN 1 ELSE 0 END) as fopagi_7hari
      FROM comparin_data_api cda
      LEFT JOIN maintenance_datel md ON cda.STO = md.sto
      GROUP BY md.sektor_prov
    ');
  }

  public static function dashboardUTOnline($type, $start, $end)
  {
    switch ($type) {
      case 'MITRA':
        $area = DB::select('SELECT mitra_amija_pt as area FROM mitra_amija GROUP BY mitra_amija_pt');
        $query1 = DB::select('
          SELECT
          ma.mitra_amija_pt as area,
          SUM(CASE WHEN utt.jenis_status = "need_validate" THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN utt.jenis_status = "need_approve" THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN utt.jenis_status = "rejected" THEN 1 ELSE 0 END) as rejected,
          SUM(CASE WHEN utt.jenis_status = "return_mytech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN utt.jenis_status = "valid_qc2" THEN 1 ELSE 0 END) as valid_qc2,
          COUNT(*) as jumlah
          FROM utonline_tr6 utt
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(utt.tglTrx) BETWEEN "' . $start . '" AND "' . $end . '") AND
          utt.witel = "KALSEL (BANJARMASIN)" AND
          utt.scId_int <> 0
          GROUP BY ma.mitra_amija_pt
        ');

        $query2 = DB::select('
          SELECT
          ma.mitra_amija_pt as area,
          SUM(CASE WHEN kpt.JENIS_PSB = "AO" THEN 1 ELSE 0 END) as ps_ao,
          SUM(CASE WHEN kpt.JENIS_PSB = "MO" THEN 1 ELSE 0 END) as ps_mo
          FROM kpro_tr6 kpt
          LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(kpt.TANGGAL_PROSES) BETWEEN "' . $start . '" AND "' . $end . '") AND
          kpt.STATUS_RESUME = "Completed (PS)" AND
          gt.ket_posisi = "PROV"
          GROUP BY ma.mitra_amija_pt
        ');
        break;
      case 'SEKTOR':
        $area = DB::select('SELECT title as area FROM group_telegram GROUP BY title');
        $query1 = DB::select('
          SELECT
          gt.title as area,
          SUM(CASE WHEN utt.jenis_status = "need_validate" THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN utt.jenis_status = "need_approve" THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN utt.jenis_status = "rejected" THEN 1 ELSE 0 END) as rejected,
          SUM(CASE WHEN utt.jenis_status = "return_mytech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN utt.jenis_status = "valid_qc2" THEN 1 ELSE 0 END) as valid_qc2,
          COUNT(*) as jumlah
          FROM utonline_tr6 utt
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(utt.tglTrx) BETWEEN "' . $start . '" AND "' . $end . '") AND
          utt.witel = "KALSEL (BANJARMASIN)" AND
          utt.scId_int <> 0
          GROUP BY gt.title
        ');
        
        $query2 = DB::select('
          SELECT
          gt.title as area,
          SUM(CASE WHEN kpt.JENIS_PSB = "AO" THEN 1 ELSE 0 END) as ps_ao,
          SUM(CASE WHEN kpt.JENIS_PSB = "MO" THEN 1 ELSE 0 END) as ps_mo
          FROM kpro_tr6 kpt
          LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(kpt.TANGGAL_PROSES) BETWEEN "' . $start . '" AND "' . $end . '") AND
          kpt.STATUS_RESUME = "Completed (PS)" AND
          gt.ket_posisi = "PROV"
          GROUP BY gt.title
        ');
        break;
    }

    foreach ($area as $val)
    {
      foreach ($query1 as $val_c1)
      {
        if (!empty($val_c1->area))
        {
          if ($val_c1->area == $val->area)
          {
            $query[$val->area]['need_validate'] = $val_c1->need_validate;
            $query[$val->area]['need_approve'] = $val_c1->need_approve;
            $query[$val->area]['rejected'] = $val_c1->rejected;
            $query[$val->area]['return_mytech'] = $val_c1->return_mytech;
            $query[$val->area]['valid_qc2'] = $val_c1->valid_qc2;
            $query[$val->area]['jumlah'] = $val_c1->jumlah;
          }
        } else {
          $query['NON AREA']['need_validate'] = $val_c1->need_validate;
          $query['NON AREA']['need_approve'] = $val_c1->need_approve;
          $query['NON AREA']['rejected'] = $val_c1->rejected;
          $query['NON AREA']['return_mytech'] = $val_c1->return_mytech;
          $query['NON AREA']['valid_qc2'] = $val_c1->valid_qc2;
          $query['NON AREA']['jumlah'] = $val_c1->jumlah;
        }
      }
      foreach ($query2 as $val_c2)
      {
        if (!empty($val_c2->area))
        {
          if ($val_c2->area == $val->area)
          {
            $query[$val->area]['ps_ao'] = $val_c2->ps_ao;
            $query[$val->area]['ps_mo'] = $val_c2->ps_mo;
          }
        } else {
          $query['NON AREA']['ps_ao'] = $val_c2->ps_ao;
          $query['NON AREA']['ps_mo'] = $val_c2->ps_mo;
        }
      }
    }

    return $query;
    
  }

  public static function dashboardUTOnlineDetail($type, $area, $date, $datex, $status)
  {
    switch ($type) {
      case 'MITRA':
        switch ($area) {
          case 'NON AREA':
            $areax = 'AND ma.mitra_amija_pt IS NULL';
            break;

          case 'ALL':
            $areax = '';
            break;

          default:
            $areax = 'AND ma.mitra_amija_pt = "' . $area . '"';
            break;
        }
        break;
      case 'SEKTOR':
        switch ($area) {
          case 'NON AREA':
            $areax = 'AND gt.title IS NULL';
            break;

          case 'ALL':
            $areax = '';
            break;

          default:
            $areax = 'AND gt.title = "' . $area . '"';
            break;
        }
        break;
    }
    

    switch ($status) {
      case 'all':
        $statusx = '';
        break;

      case 'ps_ao':
        $statusx = 'AND kpt.JENIS_PSB = "AO"';
        break;

      case 'ps_mo':
        $statusx = 'AND kpt.JENIS_PSB = "MO"';
        break;

      default:
        $statusx = 'AND utt.jenis_status = "'.$status.'"';
        break;
    }

    if (!in_array($status, ['ps_ao','ps_mo']))
    {
      return DB::select('
        SELECT
        gt.TL,
        gt.title as sektor,
        r.uraian as tim,
        ma.mitra_amija_pt as mitra,
        dps.jenisPsb,
        utt.*
        FROM utonline_tr6 utt
        LEFT JOIN Data_Pelanggan_Starclick dps ON utt.scId_int = dps.orderIdInteger
        LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
        (DATE(utt.tglTrx) BETWEEN "' . $date . '" AND "' . $datex . '") AND
        utt.witel = "KALSEL (BANJARMASIN)" AND
        utt.scId_int <> 0
        ' . $areax . '
        ' . $statusx . '
        ORDER BY utt.tglWo DESC
      ');
    } else {
      return DB::select('
        SELECT
        gt.TL,
        gt.title as sektor,
        r.uraian as tim,
        ma.mitra_amija_pt as mitra,
        kpt.*
        FROM kpro_tr6 kpt
        LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
        (DATE(kpt.TANGGAL_PROSES) BETWEEN "' . $date . '" AND "' . $datex . '") AND
        kpt.STATUS_RESUME = "Completed (PS)" AND
        gt.ket_posisi = "PROV"
        ' . $areax . '
        ' . $statusx . '
        GROUP BY kpt.ORDER_ID
        ORDER BY kpt.TANGGAL_PROSES DESC
      ');
    }
  }

  public static function monitoring_nte_lama($status, $date)
  {
    switch ($status) {
      case 'APPROVED':
        $statusx = 'AND hai.ket_so = "1"';
        break;
      case 'UNAPPROVED':
        $statusx = 'AND hai.ket_so = "0"';
        default:
        $statusx = '';
        break;
    }
    return DB::select('
      SELECT
      hai.*,
      emp.nama as nama_teknisi,
      empp.nama as nama_petugas_so
      FROM helpdesk_assurance_inbox hai
      LEFT JOIN 1_2_employee emp ON hai.telegram_id = emp.chat_id
      LEFT JOIN 1_2_employee empp ON hai.userso_by = empp.nik
      WHERE
      (DATE(hai.tgl_close) LIKE "'.$date.'%") AND
      hai.status_order = 1 AND
      hai.command = "ganti_ont"
      '.$statusx.'
      ORDER BY hai.tgl_close DESC
    ');
  }

  public static function reportPickupOnline($type, $witel_ms2n, $month)
  {
    $for_day = date('t', strtotime($month));
    $days_for = '';
    for ($i = 1; $i < $for_day + 1; $i ++) {
      if ($i < 10)
      {
        $keys = '0'.$i;
      } else {
        $keys = $i;
      }

      $day = date('Y-m-', strtotime($month)).''.$keys;
      $yesterday = date('Y-m-d', strtotime($day. "-1 days"));

      // dd($month, $for_day, $day, $yesterday);

      $days_for .= ',SUM(CASE WHEN (DATE(kpt.LAST_UPDATED_DATE) = "'.$day.'") THEN 1 ELSE 0 END) as ps_kpro_d'.$keys.',
      SUM(CASE WHEN (DATE(kpt.LAST_UPDATED_DATE) = "'.$day.'") AND ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) BETWEEN "'.$yesterday.'" AND "'.$day.'") THEN 1 ELSE 0 END) as ps_pickup_d'.$keys.'';
    }

    switch ($type) {
      case 'witel':
        $area = 'kpt.WITEL';
        break;
      
      case 'sektor':
        $area = 'gt.title';
        break;
    }

    return DB::select('
      SELECT
        '.$area.' as area
        '.$days_for.'
      FROM kpro_tr6 kpt
      LEFT JOIN tacticalpro_tr6 ttp ON kpt.ORDER_ID = ttp.sc_id
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
      WHERE
        kpt.TYPE_TRANSAKSI = "NEW SALES" AND
        kpt.JENIS_PSB = "AO" AND
        kpt.STATUS_RESUME = "Completed (PS)" AND
        kpt.PRODUCT = "INDIHOME" AND
        kpt.WITEL = "'.$witel_ms2n.'"
      GROUP BY '.$area.'
    ');
  }

  public static function pickupOnline($type, $source, $witel)
  {
    $query = null;

    switch ($source) {
      case 'kpro':
        $query1x = '
          SUM(CASE WHEN pkt.status_resume = "Completed (PS)" THEN 1 ELSE 0 END) as jml_ps,
          SUM(CASE WHEN pkt.status_resume IN ("8 | OSS - PONR", "WFM - ACTIVATION COMPLETE") THEN 1 ELSE 0 END) as jml_ponactm,
          SUM(CASE WHEN ttp.status_wo = "OPEN" AND pkt.status_resume != "Completed (PS)" THEN 1 ELSE 0 END) as jml_open_uncomp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_assigned_hd_uncomp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_progress_uncomp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_kendala_uncomp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_pending_uncomp
          FROM provi_kpro_tr6 pkt
          LEFT JOIN tacticalpro_tr6 ttp ON pkt.order_id = ttp.sc_id
          LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          WHERE
          pkt.jenispsb = "AO" AND pkt.product = "INDIHOME" AND pkt.type_trans = "NEW SALES" AND pkt.witel = "' . $witel . '"
        ';

        $query2x = '
          SUM(CASE WHEN ttp.status_wo = "OPEN" THEN 1 ELSE 0 END) as jml_open_comp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" THEN 1 ELSE 0 END) as jml_assigned_hd_comp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") THEN 1 ELSE 0 END) as jml_progress_comp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" THEN 1 ELSE 0 END) as jml_kendala_comp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" THEN 1 ELSE 0 END) as jml_pending_comp
          FROM provi_kpro_tr6 pkt
          LEFT JOIN tacticalpro_tr6 ttp ON pkt.order_id = ttp.sc_id
          LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          WHERE
          pkt.jenispsb = "AO" AND pkt.product = "INDIHOME" AND pkt.type_trans = "NEW SALES" AND pkt.status_resume = "Completed (PS)" AND pkt.witel = "' . $witel . '"
        ';

        $query1z = '
          SUM(CASE WHEN pkt.status_resume = "Completed (PS)" THEN 1 ELSE 0 END) as jml_ps,
          SUM(CASE WHEN pkt.status_resume IN ("8 | OSS - PONR", "WFM - ACTIVATION COMPLETE") THEN 1 ELSE 0 END) as jml_ponactm,
          SUM(CASE WHEN ttp.status_wo = "OPEN" AND pkt.status_resume != "Completed (PS)" THEN 1 ELSE 0 END) as jml_open_uncomp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_assigned_hd_uncomp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_progress_uncomp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_kendala_uncomp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_pending_uncomp
          FROM provi_kpro_tr6 pkt
          LEFT JOIN tacticalpro_tr6 ttp ON pkt.order_id = ttp.sc_id
          LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          pkt.jenispsb = "AO" AND pkt.product = "INDIHOME" AND pkt.type_trans = "NEW SALES" AND pkt.witel = "' . $witel . '"
        ';

        $query2z = '
          SUM(CASE WHEN ttp.status_wo = "OPEN" THEN 1 ELSE 0 END) as jml_open_comp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" THEN 1 ELSE 0 END) as jml_assigned_hd_comp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") THEN 1 ELSE 0 END) as jml_progress_comp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" THEN 1 ELSE 0 END) as jml_kendala_comp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" THEN 1 ELSE 0 END) as jml_pending_comp
          FROM provi_kpro_tr6 pkt
          LEFT JOIN tacticalpro_tr6 ttp ON pkt.order_id = ttp.sc_id
          LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          pkt.jenispsb = "AO" AND pkt.product = "INDIHOME" AND pkt.type_trans = "NEW SALES" AND pkt.status_resume = "Completed (PS)" AND pkt.witel = "' . $witel . '"
        ';
        break;

      case 'starclick':
        $query1x = '
          SUM(CASE WHEN dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_ps,
          SUM(CASE WHEN dps.orderStatus IN ("OSS PONR", "WFM ACTIVATION COMPLETE") AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_ponactm,
          SUM(CASE WHEN ttp.status_wo = "OPEN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_open_uncomp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_assigned_hd_uncomp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_progress_uncomp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_kendala_uncomp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_pending_uncomp
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN tacticalpro_tr6 ttp ON dps.orderIdInteger = ttp.sc_id
          LEFT JOIN maintenance_datel md ON dps.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          WHERE
          dps.jenisPsb LIKE "AO%" AND dps.kcontact NOT LIKE "%WMS%" AND dps.witel = "' . $witel . '"
        ';

        $query2x = '
          SUM(CASE WHEN ttp.status_wo = "OPEN" THEN 1 ELSE 0 END) as jml_open_comp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" THEN 1 ELSE 0 END) as jml_assigned_hd_comp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") THEN 1 ELSE 0 END) as jml_progress_comp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" THEN 1 ELSE 0 END) as jml_kendala_comp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" THEN 1 ELSE 0 END) as jml_pending_comp
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN tacticalpro_tr6 ttp ON dps.orderIdInteger = ttp.sc_id
          LEFT JOIN maintenance_datel md ON dps.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          WHERE
          dps.jenisPsb LIKE "AO%" AND dps.orderStatus = "COMPLETED" AND dps.kcontact NOT LIKE "%WMS%" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '") AND dps.witel = "' . $witel . '"
        ';

        $query1z = '
          SUM(CASE WHEN dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_ps,
          SUM(CASE WHEN dps.orderStatus IN ("OSS PONR", "WFM ACTIVATION COMPLETE") AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_ponactm,
          SUM(CASE WHEN ttp.status_wo = "OPEN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_open_uncomp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_assigned_hd_uncomp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_progress_uncomp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_kendala_uncomp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'") THEN 1 ELSE 0 END) as jml_pending_uncomp
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN tacticalpro_tr6 ttp ON dps.orderIdInteger = ttp.sc_id
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          dps.jenisPsb LIKE "AO%" AND dps.kcontact NOT LIKE "%WMS%" AND dps.witel = "' . $witel . '"
        ';

        $query2z = '
          SUM(CASE WHEN ttp.status_wo = "OPEN" THEN 1 ELSE 0 END) as jml_open_comp,
          SUM(CASE WHEN ttp.status_wo = "ASSIGNED_HD" THEN 1 ELSE 0 END) as jml_assigned_hd_comp,
          SUM(CASE WHEN ttp.status_wo IN ("PICKED", "GO", "WORK") THEN 1 ELSE 0 END) as jml_progress_comp,
          SUM(CASE WHEN ttp.status_wo LIKE "KENDALA%" THEN 1 ELSE 0 END) as jml_kendala_comp,
          SUM(CASE WHEN ttp.status_wo = "PENDING PELANGGAN" THEN 1 ELSE 0 END) as jml_pending_comp
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN tacticalpro_tr6 ttp ON dps.orderIdInteger = ttp.sc_id
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          dps.jenisPsb LIKE "AO%" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '") AND dps.kcontact NOT LIKE "%WMS%" AND dps.witel = "' . $witel . '"
        ';
        break;
    }

    switch ($type) {
      case 'STO':
          $area = DB::select('SELECT sektor_prov as area FROM maintenance_datel WHERE witel = "' . $witel . '" GROUP BY sektor_prov');

          $query1 = DB::select('
            SELECT
            md.sektor_prov as area,
            ' . $query1x . '
            GROUP BY md.sektor_prov
          ');

          $query2 = DB::select('
            SELECT
            md.sektor_prov as area,
            SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) != "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_pstactical_xhi,
            SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_pstactical_hi,
            SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS) KPRO" THEN 1 ELSE 0 END) as jml_ps_kpro,
            SUM(CASE WHEN ttp.status_wo = "REVOKE" THEN 1 ELSE 0 END) as jml_revoke,
            SUM(CASE WHEN ttp.sc_id IS NULL THEN 1 ELSE 0 END) as jml_belum_upload,
            ' . $query2x . '
            GROUP BY md.sektor_prov
          ');
        break;
      case 'SEKTOR':
          $area = DB::select('SELECT title as area FROM group_telegram WHERE ket_posisi = "PROV" AND title != "TERRITORY BASE HELPDESK KALSEL" GROUP BY title');

          $query1 = DB::select('
              SELECT
              gt.title as area,
              ' . $query1z . '
              GROUP BY gt.title
          ');

          $query2 = DB::select('
              SELECT
              gt.title as area,
              SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) != "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_pstactical_xhi,
              SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as jml_pstactical_hi,
              SUM(CASE WHEN ttp.status_wo = "COMPLETED (PS) KPRO" THEN 1 ELSE 0 END) as jml_ps_kpro,
              SUM(CASE WHEN ttp.status_wo = "REVOKE" THEN 1 ELSE 0 END) as jml_revoke,
              SUM(CASE WHEN ttp.sc_id IS NULL THEN 1 ELSE 0 END) as jml_belum_upload,
              ' . $query2z . '
              GROUP BY gt.title
          ');
        break;
    }


    // dd($query1, $query2);

    foreach ($area as $val)
    {
      foreach ($query1 as $val_c1)
      {
        if (!empty($val_c1->area))
        {
          if ($val_c1->area == $val->area)
          {
            $query[$val->area]['jml_ps'] = $val_c1->jml_ps;
            $query[$val->area]['jml_ponactm'] = $val_c1->jml_ponactm;
            $query[$val->area]['jml_open_uncomp'] = $val_c1->jml_open_uncomp;
            $query[$val->area]['jml_assigned_hd_uncomp'] = $val_c1->jml_assigned_hd_uncomp;
            $query[$val->area]['jml_progress_uncomp'] = $val_c1->jml_progress_uncomp;
            $query[$val->area]['jml_kendala_uncomp'] = $val_c1->jml_kendala_uncomp;
            $query[$val->area]['jml_pending_uncomp'] = $val_c1->jml_pending_uncomp;
          }
        } else {
          $query['NON AREA']['jml_ps'] = $val_c1->jml_ps;
          $query['NON AREA']['jml_ponactm'] = $val_c1->jml_ponactm;
          $query['NON AREA']['jml_open_uncomp'] = $val_c1->jml_open_uncomp;
          $query['NON AREA']['jml_assigned_hd_uncomp'] = $val_c1->jml_assigned_hd_uncomp;
          $query['NON AREA']['jml_progress_uncomp'] = $val_c1->jml_progress_uncomp;
          $query['NON AREA']['jml_kendala_uncomp'] = $val_c1->jml_kendala_uncomp;
          $query['NON AREA']['jml_pending_uncomp'] = $val_c1->jml_pending_uncomp;
        }
      }

      foreach ($query2 as $val_c2)
      {
        if (!empty($val_c2->area))
        {
          if ($val_c2->area == $val->area)
          {
            $query[$val->area]['jml_pstactical_xhi'] = $val_c2->jml_pstactical_xhi;
            $query[$val->area]['jml_pstactical_hi'] = $val_c2->jml_pstactical_hi;
            $query[$val->area]['jml_ps_kpro'] = $val_c2->jml_ps_kpro;
            $query[$val->area]['jml_open_comp'] = $val_c2->jml_open_comp;
            $query[$val->area]['jml_assigned_hd_comp'] = $val_c2->jml_assigned_hd_comp;
            $query[$val->area]['jml_progress_comp'] = $val_c2->jml_progress_comp;
            $query[$val->area]['jml_kendala_comp'] = $val_c2->jml_kendala_comp;
            $query[$val->area]['jml_pending_comp'] = $val_c2->jml_pending_comp;
            $query[$val->area]['jml_revoke'] = $val_c2->jml_revoke;
            $query[$val->area]['jml_belum_upload'] = $val_c2->jml_belum_upload;
          }
        } else {
          $query['NON AREA']['jml_pstactical_xhi'] = $val_c2->jml_pstactical_xhi;
          $query['NON AREA']['jml_pstactical_hi'] = $val_c2->jml_pstactical_hi;
          $query['NON AREA']['jml_ps_kpro'] = $val_c2->jml_ps_kpro;
          $query['NON AREA']['jml_open_comp'] = $val_c2->jml_open_comp;
          $query['NON AREA']['jml_assigned_hd_comp'] = $val_c2->jml_assigned_hd_comp;
          $query['NON AREA']['jml_progress_comp'] = $val_c2->jml_progress_comp;
          $query['NON AREA']['jml_kendala_comp'] = $val_c2->jml_kendala_comp;
          $query['NON AREA']['jml_pending_comp'] = $val_c2->jml_pending_comp;
          $query['NON AREA']['jml_revoke'] = $val_c2->jml_revoke;
          $query['NON AREA']['jml_belum_upload'] = $val_c2->jml_belum_upload;
        }
      }
    }

    return $query;
  }

  public static function pickupOnlineDetail($source, $witel, $type, $area, $status)
  {
    switch ($type) {
      case 'STO':
        switch ($area) {
          case 'NON AREA':
            $areax = 'AND md.sektor_prov IS NULL';
            break;
          case 'ALL':
            $areax = '';
            break;
          default:
            $areax = 'AND md.sektor_prov = "' . $area . '"';
            break;
        }

        $select = 'md.sektor_prov as area,';
        break;
      case 'SEKTOR':
        switch ($area) {
          case 'NON AREA':
            $areax = 'AND gt.title IS NULL';
            break;
          case 'ALL':
            $areax = '';
            break;
          default:
            $areax = 'AND gt.title = "' . $area . '"';
            break;
        }

        $select = 'gt.title as area,';
        break;
    }

    switch ($source) {
      case 'kpro':

        switch ($status) {
          case 'PS':
            $statusx = 'AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'PONACTM':
            $statusx = 'AND pkt.status_resume IN ("8 | OSS - PONR", "WFM - ACTIVATION COMPLETE")';
            break;
          case 'PS_TACTICAL_XHI':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.tgl_selesai_kerja) != "' . date('Y-m-d') . '") AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'PS_TACTICAL_HI':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.tgl_selesai_kerja) = "' . date('Y-m-d') . '") AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'PS_KPRO':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS) KPRO" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'OPEN_UNCOMP':
            $statusx = 'AND ttp.status_wo = "OPEN" AND pkt.status_resume != "Completed (PS)"';
            break;
          case 'OPEN_COMP':
            $statusx = 'AND ttp.status_wo = "OPEN" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'ASSIGNED_HD_UNCOMP':
            $statusx = 'AND ttp.status_wo = "ASSIGNED_HD" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'")';
            break;
          case 'ASSIGNED_HD_COMP':
            $statusx = 'AND ttp.status_wo = "ASSIGNED_HD" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'PROGRESS_UNCOMP':
            $statusx = 'AND ttp.status_wo IN ("PICKED", "GO", "WORK") AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'")';
            break;
            case 'PROGRESS_COMP':
            $statusx = 'AND ttp.status_wo IN ("PICKED", "GO", "WORK") AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'KENDALA_UNCOMP':
            $statusx = 'AND ttp.status_wo LIKE "KENDALA%" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'")';
            break;
          case 'KENDALA_COMP':
            $statusx = 'AND ttp.status_wo LIKE "KENDALA%" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'PENDING_UNCOMP':
            $statusx = 'AND ttp.status_wo = "PENDING PELANGGAN" AND pkt.status_resume != "Completed (PS)" AND (DATE(pkt.order_date) BETWEEN "'.date('Y-m-d', strtotime('-3 days')).'" AND "'.date('Y-m-d').'")';
            break;
          case 'PENDING_COMP':
            $statusx = 'AND ttp.status_wo = "PENDING PELANGGAN" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'REVOKE':
            $statusx = 'AND ttp.status_wo = "REVOKE" AND pkt.status_resume = "Completed (PS)"';
            break;
          case 'BELUM_UPLOAD':
            $statusx = 'AND ttp.sc_id IS NULL AND pkt.status_resume = "Completed (PS)"';
            break;
        }

        $query = '
          pkt.order_id,
          pkt.order_date,
          pkt.last_updated_date as order_date_ps,
          pkt.type_layanan,
          pkt.status_resume
          FROM provi_kpro_tr6 pkt
          LEFT JOIN tacticalpro_tr6 ttp ON pkt.order_id = ttp.sc_id
          LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          pkt.jenispsb = "AO" AND pkt.product = "INDIHOME" AND pkt.type_trans = "NEW SALES" AND pkt.witel = "' . $witel . '"
        ';
        break;
      case 'starclick':

        switch ($status) {
          case 'PS':
            $statusx = 'AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'PONACTM':
            $statusx = 'AND dps.orderStatus IN ("OSS PONR", "WFM ACTIVATION COMPLETE") AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '")';
            break;
          case 'PS_TACTICAL_XHI':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) != "' . date('Y-m-d') . '") AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'PS_TACTICAL_HI':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS)" AND (DATE(ttp.ps_date) = "' . date('Y-m-d') . '") AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'PS_KPRO':
            $statusx = 'AND ttp.status_wo = "COMPLETED (PS) KPRO" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'OPEN_UNCOMP':
            $statusx = 'AND ttp.status_wo = "OPEN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) = "' . date('Y-m-d') . '")';
            break;
          case 'OPEN_COMP':
            $statusx = 'AND ttp.status_wo = "OPEN" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'ASSIGNED_HD_UNCOMP':
            $statusx = 'AND ttp.status_wo = "ASSIGNED_HD" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '")';
            break;
          case 'ASSIGNED_HD_COMP':
          $statusx = 'AND ttp.status_wo = "ASSIGNED_HD" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
          break;
          case 'PROGRESS_UNCOMP':
            $statusx = 'AND ttp.status_wo IN ("PICKED", "GO", "WORK") AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '")';
            break;
          case 'PROGRESS_COMP':
            $statusx = 'AND ttp.status_wo IN ("PICKED", "GO", "WORK") AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'KENDALA_UNCOMP':
            $statusx = 'AND ttp.status_wo LIKE "KENDALA%" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '")';
            break;
          case 'KENDALA_COMP':
            $statusx = 'AND ttp.status_wo LIKE "KENDALA%" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'PENDING_UNCOMP':
            $statusx = 'AND ttp.status_wo = "PENDING PELANGGAN" AND dps.orderStatus != "COMPLETED" AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-3 days")) . '"  AND "' . date('Y-m-d') . '")';
            break;
          case 'PENDING_COMP':
            $statusx = 'AND ttp.status_wo = "PENDING PELANGGAN" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'REVOKE':
            $statusx = 'AND ttp.status_wo = "REVOKE" AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
          case 'BELUM_UPLOAD':
            $statusx = 'AND ttp.sc_id IS NULL AND dps.orderStatus = "COMPLETED" AND (DATE(dps.orderDatePS) = "' . date('Y-m-d') . '")';
            break;
        }

        $query = '
          dps.orderIdInteger as order_id,
          dps.orderDate as order_date,
          dps.orderDatePs as order_date_ps,
          dps.jenisPsb as type_layanan,
          dps.orderStatus as status_resume
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN tacticalpro_tr6 ttp ON dps.orderIdInteger = ttp.sc_id
          LEFT JOIN maintenance_datel md ON dps.sto = md.sto
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.title != "TERRITORY BASE HELPDESK KALSEL"
          WHERE
          dps.jenisPsb LIKE "AO%" AND dps.kcontact NOT LIKE "%WMS%" AND dps.witel = "' . $witel . '"
        ';
        break;
    }

    return DB::select('
      SELECT
      '.$select. '
      dt.id as id_dt,
      r.uraian as tim,
      gt.TL,
      ttp.status_wo,
      ttp.tgl_selesai_kerja,
      ttp.ps_date,
      pls.laporan_status,
      md.datel,
      md.teamleader_name,
      ttp.teknisi,
      ttp.uc_by,
      ttp.uc_at,
      ' . $query . '
      ' . $areax .'
      ' . $statusx . '
    ');
  }

  public static function detailPotensiPS($group, $area, $status)
  {
    switch ($group) {
      case 'SEKTOR':

        $select = 'gt.title as area,';

        switch ($area) {
          case 'ALL':
            $areax = '';
            break;
          
          case 'NON AREA':
            $areax = 'AND gt.title IS NULL';
            break;
          
          default:
            $areax = 'AND gt.title = "'.$area.'"';
            break;
        }
        break;

      case 'PROV':

        $select = 'md.sektor_prov as area,';

        switch ($area) {
          case 'ALL':
            $areax = '';
            break;

          case 'NON AREA':
            $areax = 'AND md.sektor_prov IS NULL';
            break;

          default:
            $areax = 'AND md.sektor_prov = "' . $area . '"';
            break;
        }
        break;

      case 'DATEL':

        $select = 'md.datel as area,';

        switch ($area) {
          case 'ALL':
            $areax = '';
            break;

          case 'NON AREA':
            $areax = 'AND md.datel IS NULL';
            break;

          default:
            $areax = 'AND md.datel = "' . $area . '"';
            break;
        }
        break;
      
      case 'HERO':

        $select = 'md.HERO as area,';

        switch ($area) {
          case 'ALL':
            $areax = '';
            break;

          case 'NON AREA':
            $areax = 'AND md.HERO IS NULL';
            break;
          
          default:
            $areax = 'AND md.HERO = "' . $area . '"';
            break;
        }
        break;
    }

    switch ($status) {
      case 'antrian_und':
        $wherex = '';
        $statusx = 'dt.Ndem IS NULL AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '") AND (dps.jenisPsb LIKE "AO%" OR dps.jenisPsb LIKE "PDA LOCAL%") AND dps.orderStatusId IN (1202, 51, 300)';
        break;
      
      case 'antrian_np':
        $wherex = 'dps.jenisPsb LIKE "AO%"';
        $statusx = 'AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND (pl.status_laporan = 6 OR pl.status_laporan IS NULL)';
        break;

      case 'potensi_kendala_kp':
        $wherex = 'dps.jenisPsb LIKE "AO%"';
        $statusx = 'AND dps.orderStatusId IN (1202, 51) AND pl.status_laporan IN (11,13,16,23,48,49,55,56,71,72,73,106,107) AND DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '"';
        break;

      case 'potensi_kendala_kt':
        $wherex = 'dps.jenisPsb LIKE "AO%"';
        $statusx = 'AND dps.orderStatusId IN (1202, 51) AND pl.status_laporan IN (2,10,15,24,25,26,27,34,35,36,42,46,47,51,57,64,65,75,76,77,81,84,87,108,110,112,113) AND DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-7 days")) . '" AND "' . date('Y-m-d') . '"';
        break;

      case 'potensi_ps':
        $wherex = 'dps.jenisPsb LIKE "AO%"';
        $statusx = '
        AND
        ((
          (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d' ,strtotime("-1 month")).'" AND "'.date('Y-m-d').'") AND
          dps.orderStatusId IN ("-1400", "50", "1205", "1206", "1400", "1401", "1804")
        )
        OR
        (
          dps.orderStatusId = "1500" AND DATE(dps.orderDatePs) = "'.date('Y-m-d').'"
        ))
        ';
        break;

      case 'potensi_ps_pda':
        $wherex = 'dps.jenisPsb LIKE "PDA%"';
        $statusx = '
        AND
        ((
          (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d' ,strtotime("-1 month")).'" AND "'.date('Y-m-d').'") AND
          dps.orderStatusId IN ("-1400", "50", "1205", "1206", "1400", "1401", "1804")
        )
        OR
        (
          dps.orderStatusId = "1500" AND DATE(dps.orderDatePs) = "'.date('Y-m-d').'"
        ))
        ';
        break;

      case 'potensi_progress':
        $wherex = 'dps.jenisPsb LIKE "AO%"';
        $statusx =  'AND dps.orderStatusId NOT IN (1205, 1206, 1500, 50) AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)';
        break;
      
      case 'potensi_progress_pda':
        $wherex = 'dps.jenisPsb LIKE "PDA%"';
        $statusx =  'AND dps.orderStatusId NOT IN (1205, 1206, 1500, 50) AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)';
        break;

      case 'potensi_progress_risma':
        $wherex = 'rpw.status_data = "Fresh"';
        $statusx =  'AND (DATE(pl.modified_at) = "' . date('Y-m-d') . '") AND pl.status_laporan IN (5, 28, 29)';
        break;

      case 'bypass_pda_hd':
        $wherex = 'dps.jenisPsb LIKE "PDA%"';
        $statusx =  'AND (DATE(dps.orderDate) BETWEEN "' . date('Y-m-d', strtotime("-30 days")) . '" AND "' . date('Y-m-d') . '") AND dps.orderStatusId IN ("1202", "-1201") AND pl.status_laporan IN (1, 4)';
        break;

      case 'prabac_ps_h1':
        $wherex = 'md.witel = "KALSEL"';
        $statusx =  'AND (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d', strtotime("-1 days")).'")';
        break;

      case 'prabac_ps_hi':
        $wherex = 'md.witel = "KALSEL"';
        $statusx =  'AND (DATE(pshi.tgl_etat_dt) = "'.date('Y-m-d').'")';
        break;
    }

    if ($status == 'potensi_progress_risma')
    {
      return DB::select('
          SELECT
              ' . $select . '
              pmw.sto,
              r.uraian,
              rpw.trackID_int as orderIdInteger,
              rpw.nama_pelanggan as orderName,
              pmw.kcontack as kcontact,
              rpw.status_data as orderStatus,
              pls.laporan_status,
              pmw.orderDate,
              "0000-00-00 00:00:00" as orderDatePs,
              pl.modified_at,
              "000000000" as wfm_id
          FROM risma_prov_witel rpw
          LEFT JOIN dispatch_teknisi dt ON rpw.trackID_int = dt.NO_ORDER AND dt.jenis_order = "MYIR"
          LEFT JOIN psb_myir_wo pmw ON rpw.trackID_int = pmw.myir
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE
              ' . $wherex . '
              ' . $statusx .'
              ' . $areax . '
        ');
    } elseif (in_array($status, ['prabac_ps_h1', 'prabac_ps_hi'])) {
      return DB::select('
          SELECT
              ' . $select . '
              pshi.sto,
              r.uraian,
              pshi.order_id_int as orderIdInteger,
              pshi.nama as orderName,
              pshi.kcontact,
              pshi.status_order as orderStatus,
              pls.laporan_status,
              dps.orderDate,
              pshi.tgl_etat_dt as orderDatePs,
              pl.modified_at,
              dps.wfm_id
          FROM prabac_psharian_indihome pshi
          LEFT JOIN Data_Pelanggan_Starclick dps ON pshi.order_id_int = dps.orderIdInteger
          LEFT JOIN dispatch_teknisi dt ON pshi.order_id_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN maintenance_datel md ON pshi.sto = md.sto
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE
              ' . $wherex . '
              ' . $statusx .'
              ' . $areax . '
        ');
    } else {
      return DB::select('
          SELECT
              ' . $select . '
              dps.sto,
              r.uraian,
              dps.orderIdInteger,
              dps.orderName,
              dps.kcontact,
              dps.orderStatus,
              pls.laporan_status,
              dps.orderDate,
              dps.orderDatePs,
              pl.modified_at,
              dps.wfm_id
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN maintenance_datel md ON dps.sto = md.sto
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          WHERE
              ' . $wherex . '
              ' . $statusx .'
              ' . $areax . '
        ');
    }

  }

  public static function reportQC2($area, $source, $start, $end)
  {
    switch ($area) {
      case 'SEKTOR':
        $areax = 'gt.title';
        break;
      
      case 'MITRA':
        $areax = 'ma.mitra_amija_pt';
        break;

      case 'WITEL':
        $areax = 'rqt.witel';
        break;
    }

    if (in_array($area, ['SEKTOR', 'MITRA']))
    {
      switch ($source) {
        case 'QC2':
          return DB::select('
          SELECT
          ' . $areax . ' as area,
          COUNT(*) as jml_ps,
          SUM(CASE WHEN rqt.status_ut IS NULL AND rqt.status_qc IS NULL THEN 1 ELSE 0 END) as create_failed,
          SUM(CASE WHEN rqt.status_ut IS NOT NULL AND rqt.status_qc IS NOT NULL THEN 1 ELSE 0 END) as create_success,
          SUM(CASE WHEN rqt.status_ut = "Reopen MyTech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN rqt.status_ut IN ("Need validate TL", "Need validate TL TA") THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN rqt.status_ut IN ("Not approved", "Not Validate ASO") THEN 1 ELSE 0 END) as not_approved,
          SUM(CASE WHEN rqt.status_ut = "Not Valid QC2" THEN 1 ELSE 0 END) as not_valid_qc2,
          SUM(CASE WHEN rqt.status_ut IN ("Need approve", "Need Validate ASO") THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN rqt.status_ut IN ("Approved", "Need Validate QC2") AND rqt.status_qc = "Need Validate QC" THEN 1 ELSE 0 END) as need_validate_qc2,
          SUM(CASE WHEN (rqt.status_ut LIKE "Valid QC2%" OR rqt.status_ut IN ("Close", "Closed")) AND rqt.status_qc = "Valid" THEN 1 ELSE 0 END) as validate_qc2,
          SUM(CASE WHEN rqt.status_ut IN ("Close", "Closed") THEN 1 ELSE 0 END) as closed
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '") AND
          rqt.witel = "BANJARMASIN"
          GROUP BY ' . $areax . '
        ');
          break;

        case 'UTONLINE':
          return DB::select('
          SELECT
          ' . $areax . ' as area,
          COUNT(*) as jml_ps,
          SUM(CASE WHEN utt.statusName IS NULL THEN 1 ELSE 0 END) as create_failed,
          SUM(CASE WHEN utt.statusName IS NOT NULL THEN 1 ELSE 0 END) as create_success,
          SUM(CASE WHEN utt.statusName = "Reopen MyTech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN utt.statusName IN ("Need validate TL", "Need validate TL TA") THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN utt.statusName IN ("Not approved", "Not Validate ASO") THEN 1 ELSE 0 END) as not_approved,
          SUM(CASE WHEN utt.statusName = "Not Valid QC2" THEN 1 ELSE 0 END) as not_valid_qc2,
          SUM(CASE WHEN utt.statusName IN ("Need approve", "Need Validate ASO") THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN utt.statusName = "Need Validate QC2" THEN 1 ELSE 0 END) as need_validate_qc2,
          SUM(CASE WHEN utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid" THEN 1 ELSE 0 END) as validate_qc2,
          SUM(CASE WHEN utt.statusName IN ("Close", "Closed") THEN 1 ELSE 0 END) as closed
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '") AND
          rqt.witel = "BANJARMASIN"
          GROUP BY ' . $areax . '
        ');
          break;

        case 'UTONLINEACTCOMP':
          return DB::select('
          SELECT
          ' . $areax . ' as area,
          COUNT(*) as jml_ps,
          SUM(CASE WHEN utt.scId_int IS NULL THEN 1 ELSE 0 END) as create_failed,
          SUM(CASE WHEN utt.scId IS NOT NULL THEN 1 ELSE 0 END) as create_success,
          SUM(CASE WHEN utt.statusName = "Reopen MyTech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN utt.statusName IN ("Need validate TL", "Need validate TL TA") THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN utt.statusName IN ("Not approved", "Not Validate ASO") THEN 1 ELSE 0 END) as not_approved,
          SUM(CASE WHEN utt.statusName = "Not Valid QC2" THEN 1 ELSE 0 END) as not_valid_qc2,
          SUM(CASE WHEN utt.statusName IN ("Need approve", "Need Validate ASO") THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN utt.statusName = "Need Validate QC2" THEN 1 ELSE 0 END) as need_validate_qc2,
          SUM(CASE WHEN utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid" THEN 1 ELSE 0 END) as validate_qc2,
          SUM(CASE WHEN utt.statusName IN ("Close", "Closed") THEN 1 ELSE 0 END) as closed
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN utonline_tr6 utt ON dps.internet = utt.noInternet
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(dps.orderDate) BETWEEN "' . $start . '" AND "' . $end . '") AND
          dps.jenisPsb LIKE "AO%" AND
          dps.orderStatusId IN ("1300","1205")
          GROUP BY ' . $areax . '
        ');
          break;
      }
    } elseif ($area == 'WITEL') {
      switch ($source) {
        case 'QC2':
          return DB::select('
          SELECT
          ' . $areax . ' as area,
          COUNT(*) as jml_ps,
          SUM(CASE WHEN rqt.status_ut IS NULL AND rqt.status_qc IS NULL THEN 1 ELSE 0 END) as create_failed,
          SUM(CASE WHEN rqt.status_ut IS NOT NULL AND rqt.status_qc IS NOT NULL THEN 1 ELSE 0 END) as create_success,
          SUM(CASE WHEN rqt.status_ut = "Reopen MyTech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN rqt.status_ut IN ("Need validate TL", "Need validate TL TA") THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN rqt.status_ut IN ("Not approved", "Not Validate ASO") THEN 1 ELSE 0 END) as not_approved,
          SUM(CASE WHEN rqt.status_ut = "Not Valid QC2" THEN 1 ELSE 0 END) as not_valid_qc2,
          SUM(CASE WHEN rqt.status_ut IN ("Need approve", "Need Validate ASO") THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN rqt.status_ut IN ("Approved", "Need Validate QC2") AND rqt.status_qc = "Need Validate QC" THEN 1 ELSE 0 END) as need_validate_qc2,
          SUM(CASE WHEN (rqt.status_ut LIKE "Valid QC2%" OR rqt.status_ut IN ("Close", "Closed")) AND rqt.status_qc = "Valid" THEN 1 ELSE 0 END) as validate_qc2,
          SUM(CASE WHEN rqt.status_ut IN ("Close", "Closed") THEN 1 ELSE 0 END) as closed
          FROM report_qc2_tr6 rqt
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '")
          GROUP BY ' . $areax . '
        ');
          break;

        case 'UTONLINE':
          return DB::select('
          SELECT
          ' . $areax . ' as area,
          COUNT(*) as jml_ps,
          SUM(CASE WHEN utt.statusName IS NULL THEN 1 ELSE 0 END) as create_failed,
          SUM(CASE WHEN utt.statusName IS NOT NULL THEN 1 ELSE 0 END) as create_success,
          SUM(CASE WHEN utt.statusName = "Reopen MyTech" THEN 1 ELSE 0 END) as return_mytech,
          SUM(CASE WHEN utt.statusName IN ("Need validate TL", "Need validate TL TA") THEN 1 ELSE 0 END) as need_validate,
          SUM(CASE WHEN utt.statusName IN ("Not approved", "Not Validate ASO") THEN 1 ELSE 0 END) as not_approved,
          SUM(CASE WHEN utt.statusName = "Not Valid QC2" THEN 1 ELSE 0 END) as not_valid_qc2,
          SUM(CASE WHEN utt.statusName IN ("Need approve", "Need Validate ASO") THEN 1 ELSE 0 END) as need_approve,
          SUM(CASE WHEN utt.statusName = "Need Validate QC2" THEN 1 ELSE 0 END) as need_validate_qc2,
          SUM(CASE WHEN utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid" THEN 1 ELSE 0 END) as validate_qc2,
          SUM(CASE WHEN utt.statusName IN ("Close", "Closed") THEN 1 ELSE 0 END) as closed
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '")
          GROUP BY ' . $areax . '
        ');
          break;

        case 'UTONLINEACTCOMP':
          # code...
          break;
      }
    }
    
  }

  public static function reportQC2Detail($area, $source, $group, $start, $end, $status)
  {
    switch ($area) {
      case 'SEKTOR':
        $areax = 'gt.title';
        switch ($group) {
          case 'NON AREA':
            $where_area = 'AND gt.title IS NULL';
            break;
          
          case 'ALL':
            $where_area = '';
            break;
          
          default:
            $where_area = 'AND gt.title = "' . $group . '"';
            break;
        }
        break;
      
      case 'MITRA':
        $areax = 'ma.mitra_amija_pt';
        switch ($group) {
          case 'NON AREA':
            $where_area = 'AND ma.mitra_amija_pt IS NULL';
            break;

          case 'ALL':
            $where_area = '';
            break;

          default:
            $where_area = 'AND ma.mitra_amija_pt = "' . $group . '"';
            break;
        }
        break;

      case 'WITEL':
        $areax = 'rqt.witel';
        switch ($group) {
          case 'NON AREA':
            $where_area = 'AND rqt.witel IS NULL';
            break;

          case 'ALL':
            $where_area = '';
            break;

          default:
            $where_area = 'AND rqt.witel = "' . $group . '"';
            break;
        }
        break;
    }

    if (in_array($area, ['SEKTOR', 'MITRA']))
    {
      switch ($source) {
        case 'QC2':
          switch ($status) {
            case 'JML_PS':
              $where_status = '';
              break;

            case 'CREATE_FAILED':
              $where_status = 'AND rqt.status_ut IS NULL AND rqt.status_qc IS NULL';
              break;

            case 'CREATE_SUCCESS':
              $where_status = 'AND rqt.status_ut IS NOT NULL AND rqt.status_qc IS NOT NULL';
              break;

            case 'RETURN_MYTECH':
              $where_status = 'AND rqt.status_ut = "Reopen MyTech"';
              break;

            case 'NEED_VALIDATE':
              $where_status = 'AND rqt.status_ut IN ("Need validate TL", "Need validate TL TA")';
              break;

            case 'NOT_APPROVED':
              $where_status = 'AND rqt.status_ut IN ("Not approved", "Not Validate ASO")';
              break;

            case 'NOT_VALID_QC2':
              $where_status = 'AND rqt.status_ut = "Not Valid QC2"';
              break;

            case 'NEED_APPROVE':
              $where_status = 'AND rqt.status_ut IN ("Need approve", "Need Validate ASO")';
              break;

            case 'NEED_VALIDATE_QC2':
              $where_status = 'AND rqt.status_ut IN ("Approved", "Need Validate QC2") AND rqt.status_qc = "Need Validate QC"';
              break;

            case 'VALIDATE_QC2':
              $where_status = 'AND (rqt.status_ut LIKE "Valid QC2%" OR rqt.status_ut IN ("Close", "Closed")) AND rqt.status_qc = "Valid"';
              break;
          }

          return DB::select('
          SELECT
          ' . $areax . ' as area,
          pls.laporan_status,
          pl.modified_at as tanggal_laporan,
          r.uraian as tim,
          utt.statusName as utt_statusName,
          utt.qcStatusName as utt_qcStatusName,
          utt.tglTrx as utt_tglTrx,
          utt.noInternet,
          utt.details as utt_evidence,
          utt.customer_desc as utt_customer_desc,
          rqt.*
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '") AND
          rqt.witel = "BANJARMASIN"
          ' . $where_area . '
          ' . $where_status . '
          ORDER BY rqt.tanggal_ps_format DESC
          ');
          break;

        case 'UTONLINE':
          switch ($status) {
            case 'JML_PS':
              $where_status = '';
              break;

            case 'CREATE_FAILED':
              $where_status = 'AND utt.statusName IS NULL';
              break;

            case 'CREATE_SUCCESS':
              $where_status = 'AND utt.statusName IS NOT NULL';
              break;

            case 'RETURN_MYTECH':
              $where_status = 'AND utt.statusName = "Reopen MyTech"';
              break;

            case 'NEED_VALIDATE':
              $where_status = 'AND utt.statusName IN ("Need validate TL", "Need validate TL TA")';
              break;

            case 'NOT_APPROVED':
              $where_status = 'AND utt.statusName IN ("Not approved", "Not Validate ASO")';
              break;

            case 'NOT_VALID_QC2':
              $where_status = 'AND utt.statusName = "Not Valid QC2"';
              break;

            case 'NEED_APPROVE':
              $where_status = 'AND utt.statusName IN ("Need approve", "Need Validate ASO")';
              break;

            case 'NEED_VALIDATE_QC2':
              $where_status = 'AND utt.statusName = "Need Validate QC2"';
              break;

            case 'VALIDATE_QC2':
              $where_status = 'AND utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid"';
              break;
          }

          return DB::select('
          SELECT
          ' . $areax . ' as area,
          pls.laporan_status,
          pl.modified_at as tanggal_laporan,
          r.uraian as tim,
          utt.laborCode as teknisi,
          utt.qcApproveBy as agent,
          utt.sto,
          utt.statusName as utt_statusName,
          utt.qcStatusName as utt_qcStatusName,
          utt.tglTrx,
          utt.noInternet,
          utt.details as utt_evidence,
          utt.customer_desc as utt_customer_desc,
          rqt.*
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '") AND
          rqt.witel = "BANJARMASIN"
          ' . $where_area . '
          ' . $where_status . '
          GROUP BY rqt.id
          ORDER BY rqt.tanggal_ps_format DESC
          ');
          break;

        case 'UTONLINEACTCOMP':
          switch ($status) {
            case 'JML_PS':
              $where_status = '';
              break;

            case 'CREATE_FAILED':
              $where_status = 'AND utt.statusName IS NULL';
              break;

            case 'CREATE_SUCCESS':
              $where_status = 'AND utt.statusName IS NOT NULL';
              break;

            case 'RETURN_MYTECH':
              $where_status = 'AND utt.statusName = "Reopen MyTech"';
              break;

            case 'NEED_VALIDATE':
              $where_status = 'AND utt.statusName IN ("Need validate TL", "Need validate TL TA")';
              break;

            case 'NOT_APPROVED':
              $where_status = 'AND utt.statusName IN ("Not approved", "Not Validate ASO")';
              break;

            case 'NOT_VALID_QC2':
              $where_status = 'AND utt.statusName = "Not Valid QC2"';
              break;

            case 'NEED_APPROVE':
              $where_status = 'AND utt.statusName IN ("Need approve", "Need Validate ASO")';
              break;

            case 'NEED_VALIDATE_QC2':
              $where_status = 'AND utt.statusName = "Need Validate QC2"';
              break;

            case 'VALIDATE_QC2':
              $where_status = 'AND utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid"';
              break;
          }

          return DB::select('
          SELECT
          ' . $areax . ' as area,
          pls.laporan_status,
          pl.modified_at as tanggal_laporan,
          r.uraian as tim,
          dps.orderIdInteger as order_id,
          dps.jenisPsb as transaksi,
          dps.provider as unit,
          utt.laborCode as teknisi,
          utt.qcApproveBy as agent,
          utt.sto,
          dps.orderDatePs as tanggal_ps,
          utt.statusName as utt_statusName,
          utt.qcStatusName as utt_qcStatusName,
          utt.tglTrx,
          utt.noInternet,
          utt.details as evidence,
          utt.customer_desc as utt_customer_desc,
          FROM Data_Pelanggan_Starclick dps
          LEFT JOIN utonline_tr6 utt ON dps.internet = utt.noInternet
          LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
          WHERE
          (DATE(dps.orderDate) BETWEEN "' . $start . '" AND "' . $end . '") AND
          dps.jenisPsb LIKE "AO%" AND
          dps.orderStatusId IN ("1300","1205")
          ' . $where_area . '
          ' . $where_status . '
          ORDER BY dps.orderDatePs DESC
          ');
          break;
      }
    } elseif ($area == 'WITEL') {
      switch ($source) {
        case 'QC2':
          switch ($status) {
            case 'JML_PS':
              $where_status = '';
              break;

            case 'CREATE_FAILED':
              $where_status = 'AND rqt.status_ut IS NULL AND rqt.status_qc IS NULL';
              break;

            case 'CREATE_SUCCESS':
              $where_status = 'AND rqt.status_ut IS NOT NULL AND rqt.status_qc IS NOT NULL';
              break;

            case 'RETURN_MYTECH':
              $where_status = 'AND rqt.status_ut = "Reopen MyTech"';
              break;

            case 'NEED_VALIDATE':
              $where_status = 'AND rqt.status_ut IN ("Need validate TL", "Need validate TL TA")';
              break;

            case 'NOT_APPROVED':
              $where_status = 'AND rqt.status_ut IN ("Not approved", "Not Validate ASO")';
              break;

            case 'NOT_VALID_QC2':
              $where_status = 'AND rqt.status_ut = "Not Valid QC2"';
              break;

            case 'NEED_APPROVE':
              $where_status = 'AND rqt.status_ut IN ("Need approve", "Need Validate ASO")';
              break;

            case 'NEED_VALIDATE_QC2':
              $where_status = 'AND rqt.status_ut IN ("Approved", "Need Validate QC2") AND rqt.status_qc = "Need Validate QC"';
              break;

            case 'VALIDATE_QC2':
              $where_status = 'AND (rqt.status_ut LIKE "Valid QC2%" OR rqt.status_ut IN ("Close", "Closed")) AND rqt.status_qc = "Valid"';
              break;
          }

          return DB::select('
          SELECT
          ' . $areax . ' as area,
          "#N/A" as laporan_status,
          "0000-00-00 00:00:00" as tanggal_laporan,
          "#N/A" as tim,
          utt.statusName as utt_statusName,
          utt.qcStatusName as utt_qcStatusName,
          utt.tglTrx as utt_tglTrx,
          utt.noInternet,
          utt.customer_desc as utt_customer_desc,
          utt.details as utt_evidence,
          rqt.*
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '")
          ' . $where_area . '
          ' . $where_status . '
          ORDER BY rqt.tanggal_ps_format DESC
          ');
          break;

        case 'UTONLINE':
          switch ($status) {
            case 'JML_PS':
              $where_status = '';
              break;

            case 'CREATE_FAILED':
              $where_status = 'AND utt.statusName IS NULL';
              break;

            case 'CREATE_SUCCESS':
              $where_status = 'AND utt.statusName IS NOT NULL';
              break;

            case 'RETURN_MYTECH':
              $where_status = 'AND utt.statusName = "Reopen MyTech"';
              break;

            case 'NEED_VALIDATE':
              $where_status = 'AND utt.statusName IN ("Need validate TL", "Need validate TL TA")';
              break;

            case 'NOT_APPROVED':
              $where_status = 'AND utt.statusName IN ("Not approved", "Not Validate ASO")';
              break;

            case 'NOT_VALID_QC2':
              $where_status = 'AND utt.statusName = "Not Valid QC2"';
              break;

            case 'NEED_APPROVE':
              $where_status = 'AND utt.statusName IN ("Need approve", "Need Validate ASO")';
              break;

            case 'NEED_VALIDATE_QC2':
              $where_status = 'AND utt.statusName = "Need Validate QC2"';
              break;

            case 'VALIDATE_QC2':
              $where_status = 'AND utt.statusName LIKE "Valid QC2%" AND utt.qcStatusName = "Valid"';
              break;
          }

          return DB::select('
          SELECT
          ' . $areax . ' as area,
          "#N/A" as laporan_status,
          "0000-00-00 00:00:00" as tanggal_laporan,
          "#N/A" as tim,
          utt.laborCode as teknisi,
          utt.qcApproveBy as agent,
          utt.sto,
          utt.statusName as utt_statusName,
          utt.qcStatusName as utt_qcStatusName,
          utt.tglTrx,
          utt.noInternet,
          utt.details as utt_evidence,
          utt.customer_desc as utt_customer_desc,
          rqt.*
          FROM report_qc2_tr6 rqt
          LEFT JOIN utonline_tr6 utt ON rqt.order_code = utt.order_code
          WHERE
          (DATE(rqt.tanggal_ps_format) BETWEEN "' . $start . '" AND "' . $end . '")
          ' . $where_area . '
          ' . $where_status . '
          GROUP BY rqt.id
          ORDER BY rqt.tanggal_ps_format DESC
          ');
          break;
      }
    }
    
  }

  public static function dashboardAddonComparin($start, $end, $status)
  {
    switch ($status) {
      case '2p3p':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%2P-3P%"';
        break;
      
      case 'upgrade':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%UPGRADE%"';
        break;

      case 'second_stb':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%SECOND STB%"';
        break;

      case 'wifi_extender':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%WIFI EXTENDER%"';
        break;
    }

    return DB::select('
        SELECT
          md.datel as area,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) <= 24 THEN 1 ELSE 0 END) as range_und1hari,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) > 24 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) <= 48 THEN 1 ELSE 0 END) as range1_2hari,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) > 48 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) <= 72 THEN 1 ELSE 0 END) as range2_3hari,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) > 72 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) <= 144 THEN 1 ELSE 0 END) as range4_6hari,
          SUM(CASE WHEN TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, NOW()) > 144 THEN 1 ELSE 0 END) as range_1minggu
        FROM comparin_data_api cda
        LEFT JOIN maintenance_datel md ON cda.STO = md.sto
        WHERE
          (DATE(cda.ORDER_DATE) BETWEEN "'.$start.'" AND "'.$end.'") AND
          cda.type_grab = "ADDON" AND
          cda.STATUS_RESUME IN ("OSS PROVISIONING ISSUED", "Fallout (WFM)", "OSS PONR", "Fallout (Activation)")
          '.$statusx.'
        GROUP BY md.datel
    ');
  }

  public static function dashboardAddonComparinDetail($datel, $status, $start, $end, $waktu)
  {
    switch ($datel) {
      case 'NON AREA':
        $datelx = 'AND md.datel IS NULL';
        break;
      
      case 'ALL':
        $datelx = '';
        break;

      default:
        $datelx = 'AND md.datel = "' . $datel . '"';
        break;
    }

    switch ($status) {
      case '2p3p':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%2P-3P%"';
        break;
      
      case 'upgrade':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%UPGRADE%"';
        break;

      case 'second_stb':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%SECOND STB%"';
        break;

      case 'wifi_extender':
        $statusx = 'AND cda.JENIS_ADDON LIKE "%WIFI EXTENDER%"';
        break;
    }

    switch ($waktu) {
      case 'ALL':
        $waktux = '';
        break;

      case 'range_und1hari':
        $waktux = 'AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") <= 24';
        break;
      
      case 'range1_2hari':
        $waktux = 'AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") <= 48';
        break;

      case 'range2_3hari':
        $waktux = 'AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") <= 72';
        break;

      case 'range4_6hari':
        $waktux = 'AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") <= 144';
        break;

      case 'range_1minggu':
        $waktux = 'AND TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") > 144';
        break;
    }

    return DB::select('
        SELECT
            md.datel as area,
            gt.title as sektor,
            r.uraian as tim,
            pls.laporan_status,
            pl.modified_at as tgl_laporan,
            TIMESTAMPDIFF( HOUR , cda.ORDER_DATE, "' . date('Y-m-d H:i:s') . '") as umur_jam,
            cda.*
        FROM comparin_data_api cda
        LEFT JOIN maintenance_datel md ON cda.STO = md.sto
        LEFT JOIN dispatch_teknisi dt ON cda.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
            (DATE(cda.ORDER_DATE) BETWEEN "'.$start.'" AND "'.$end.'") AND
            cda.type_grab = "ADDON" AND
            cda.STATUS_RESUME IN ("OSS PROVISIONING ISSUED", "Fallout (WFM)", "OSS PONR", "Fallout (Activation)")
            ' . $statusx .'
            ' . $waktux . '
            ' . $datelx . '
    ');
  }

  public static function dashboardAddon($source, $start, $end, $status)
  {
    switch ($source) {
      case 'STARCLICKNCX':
        switch ($status) {
          case '1p2p':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') .'") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') .'") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') .'") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') .'") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') .'") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN maintenance_datel md ON dps.sto = md.sto
              WHERE
              (DATE(dps.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dps.jenisPsb LIKE "MO%" AND
              dps.kcontact LIKE "%PTGSDTG%" AND
              dps.orderStatus = "OSS PROVISIONING ISSUED" AND dps.kcontact LIKE "%1P-2P%"
              GROUP BY md.datel
            ');
            break;

          case '2p3p':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN maintenance_datel md ON dps.sto = md.sto
              WHERE
              (DATE(dps.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dps.jenisPsb LIKE "MO%" AND
              dps.kcontact LIKE "%PTGSDTG%" AND
              dps.orderStatus = "OSS PROVISIONING ISSUED" AND dps.kcontact LIKE "%2P-3P%"
              GROUP BY md.datel
            ');
            break;

          case 'upgrade':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN maintenance_datel md ON dps.sto = md.sto
              WHERE
              (DATE(dps.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dps.jenisPsb LIKE "MO%" AND
              dps.kcontact LIKE "%PTGSDTG%" AND
              dps.kcontact LIKE "%UG%" AND
              dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")
              GROUP BY md.datel
            ');
            break;

          case 'minipack':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN maintenance_datel md ON dps.sto = md.sto
              WHERE
              (DATE(dps.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dps.jenisPsb LIKE "MO%" AND
              dps.kcontact LIKE "%PTGSDTG%" AND
              dps.kcontact LIKE "%MINIPACK%" AND
              dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")
              GROUP BY md.datel
            ');
            break;

          case 'ott':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN maintenance_datel md ON dps.sto = md.sto
              WHERE
              (DATE(dps.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dps.jenisPsb LIKE "MO%" AND
              dps.kcontact LIKE "%PTGSDTG%" AND
              dps.kcontact LIKE "%OTT%" AND
              dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")
              GROUP BY md.datel
            ');
            break;
        }
        break;
      
      case 'MYIRMANUAL':
        switch ($status) {
          case '1p2p':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dja.kategori = "1P - 2P" AND
              pmw.layanan NOT LIKE "PDA%" AND
              (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;

          case '2p3p':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dja.kategori = "2P - 3P" AND
              pmw.layanan NOT LIKE "PDA%" AND
              (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;

          case 'upgrade':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dja.kategori = "UPGRADE" AND
              pmw.layanan NOT LIKE "PDA%" AND
              (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;

          case 'pda':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start . '" AND "' . $end . '") AND
              pmw.layanan LIKE "PDA%" AND
              pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+") AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;

          case 'minipack':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dja.kategori = "MINIPACK" AND
              pmw.layanan NOT LIKE "PDA%" AND
              (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;

          case 'ott':
            return DB::select('
              SELECT
              md.datel as area,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24 THEN 1 ELSE 0 END) as range_und1hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48 THEN 1 ELSE 0 END) as range1_2hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72 THEN 1 ELSE 0 END) as range2_3hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144 THEN 1 ELSE 0 END) as range4_6hari,
              SUM(CASE WHEN TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144 THEN 1 ELSE 0 END) as range_1minggu
              FROM psb_myir_wo pmw
              LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
              LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
              LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
              LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
              LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
              LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
              WHERE
              (DATE(pmw.orderDate) BETWEEN "' . $start .'" AND "' . $end . '") AND
              dja.kategori = "OTT" AND
              pmw.layanan NOT LIKE "PDA%" AND
              (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND
              (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
              GROUP BY md.datel
            ');
            break;
        }
        break;
    }
  }

  public static function dashboardAddonDetail($source, $datel, $status, $start, $end, $waktu)
  {
    switch ($datel) {
      case 'NON AREA':
        $datelx = 'AND md.datel IS NULL';
        break;
      
      case 'ALL':
        $datelx = '';
        break;

      default:
        $datelx = 'AND md.datel = "' . $datel . '"';
        break;
    }

    switch ($source) {
      case 'STARCLICKNCX':
        switch ($status) {
          case '1p2p':
            $statusx = 'AND dps.orderStatus = "OSS PROVISIONING ISSUED" AND dps.kcontact LIKE "%1P-2P%"';
            break;
          
          case '2p3p':
            $statusx = 'AND dps.orderStatus = "OSS PROVISIONING ISSUED" AND dps.kcontact LIKE "%2P-3P%"';
            break;
          
          case 'upgrade':
            $statusx = 'AND dps.kcontact LIKE "%UG%" AND dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")';
            break;

          case 'minipack':
            $statusx = 'AND dps.kcontact LIKE "%MINIPACK%" AND dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")';
            break;

          case 'ott':
            $statusx = 'AND dps.kcontact LIKE "%OTT%" AND dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED")';
            break;
        }

        switch ($waktu) {
          case 'ALL':
            $waktux = '';
            break;

          case 'range_und1hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24';
            break;
          
          case 'range1_2hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48';
            break;

          case 'range2_3hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72';
            break;

          case 'range4_6hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144';
            break;

          case 'range_1minggu':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") > 144';
            break;
        }

        return DB::select('
            SELECT
                md.datel as area,
                gt.title as sektor,
                r.uraian as tim,
                pls.laporan_status,
                pl.modified_at as tgl_laporan,
                TIMESTAMPDIFF( HOUR , dps.orderDate, "' . date('Y-m-d H:i:s') . '") as umur_jam,
                dps.orderId,
                dps.orderName,
                dps.orderDate,
                dps.orderStatus,
                dps.jenisPsb as layanan,
                dps.kcontact
            FROM Data_Pelanggan_Starclick dps
            LEFT JOIN maintenance_datel md ON dps.sto = md.sto
            LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
            WHERE
                (DATE(dps.orderDate) BETWEEN "' . $start . '" AND "' . $end .'") AND
                dps.jenisPsb LIKE "MO%" AND
                dps.kcontact LIKE "%PTGSDTG%"
                ' . $statusx .'
                ' . $waktux . '
                ' . $datelx . '
        ');

        break;
      
      case 'MYIRMANUAL':

        switch ($status) {
          case '1p2p':
            $statusx = 'AND dja.kategori = "1P - 2P" AND (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+"))';
            break;

          case '2p3p':
            $statusx = 'AND dja.kategori = "2P - 3P" AND (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+"))';
            break;

          case 'upgrade':
            $statusx = 'AND dja.kategori = "UPGRADE" AND (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND pmw.layanan NOT LIKE "PDA%"';
            break;

          case 'pda':
            $statusx = 'AND pmw.layanan LIKE "PDA%" AND pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")';
            break;

          case 'minipack':
            $statusx = 'AND dja.kategori = "MINIPACK" AND (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND pmw.layanan NOT LIKE "PDA%"';
            break;

          case 'ott':
            $statusx = 'AND dja.kategori = "OTT" AND (pls.laporan_status IS NULL OR pls.laporan_status IN ("NEED PROGRESS", "BERANGKAT", "TIBA", "OGP", "PENDING H+")) AND pmw.layanan NOT LIKE "PDA%"';
            break;
        }

        switch ($waktu) {
          case 'ALL':
            $waktux = '';
            break;

          case 'range_und1hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 24';
            break;

          case 'range1_2hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 24 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 48';
            break;

          case 'range2_3hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 48 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 72';
            break;

          case 'range4_6hari':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 72 AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") <= 144';
            break;

          case 'range_1minggu':
            $waktux = 'AND TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") > 144';
            break;
        }

        return DB::select('
            SELECT
                md.datel as area,
                gt.title as sektor,
                gtt.title as sektorr,
                r.uraian as tim,
                rr.uraian as timm,
                pls.laporan_status,
                plss.laporan_status as laporan_statuss,
                pl.modified_at as tgl_laporan,
                pll.modified_at as tgl_laporann,
                TIMESTAMPDIFF( HOUR , pmw.orderDate, "' . date('Y-m-d H:i:s') . '") as umur_jam,
                pmw.myir as orderId,
                dtt.Ndem as orderIdd,
                pmw.customer as orderName,
                pmw.orderDate,
                dps.orderStatus,
                pmw.layanan,
                pmw.kcontack as kcontact
            FROM psb_myir_wo pmw
            LEFT JOIN dshrplasa_jenis_ayanan dja ON pmw.layanan = dja.id_layanan
            LEFT JOIN maintenance_datel md ON pmw.sto = md.sto
            LEFT JOIN dispatch_teknisi dt ON pmw.myir = dt.Ndem
            LEFT JOIN dispatch_teknisi dtt ON pmw.sc = dtt.Ndem AND dtt.jenis_order = "SC"
            LEFT JOIN Data_Pelanggan_Starclick dps ON pmw.sc = dps.orderId
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN regu rr ON dtt.id_regu = rr.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN group_telegram gtt ON rr.mainsector = gtt.chat_id
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN psb_laporan pll ON dtt.id = pll.id_tbl_mj
            LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
            LEFT JOIN psb_laporan_status plss ON pll.status_laporan = plss.laporan_status_id
            WHERE
                (DATE(pmw.orderDate) BETWEEN "' . $start . '" AND "' . $end . '") AND
                (dps.orderStatus IS NULL OR dps.orderStatus NOT IN ("COMPLETED", "CANCEL COMPLETED"))
                ' . $statusx . '
                ' . $waktux . '
                ' . $datelx . '
        ');

        break;
    }
  }

  public static function newDashboardDismantling($start, $end)
  {
    $tim = DB::table('regu')->where('ACTIVE', 1)->select('id_regu as id','uraian as tim')->get();

    $query1 = DB::select('
        SELECT
        r.uraian as tim,
        SUM(CASE WHEN vrd.tipe_dapros IN ("CT0", "NEW CT0") THEN 1 ELSE 0 END) as und_ct0,
        SUM(CASE WHEN vrd.tipe_dapros = "NEW LOSS" THEN 1 ELSE 0 END) as und_newloss,
        SUM(CASE WHEN vrd.tipe_dapros = "CAPS" THEN 1 ELSE 0 END) as und_caps,
        SUM(CASE WHEN vrd.tipe_dapros NOT IN ("CT0", "NEW CT0", "NEW LOSS", "CAPS") THEN 1 ELSE 0 END) as und_massal
        FROM visitRacoonDismantling vrd
        LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER AND dt.jenis_order = "CABUT_NTE"
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        dt.jenis_order IS NULL AND
        vrd.nik IN ("15897115", "15897117")
        GROUP BY r.uraian
    ');

    $query2 = DB::select('
        SELECT
        r.uraian as tim,
        SUM(CASE WHEN cno.jenis_dismantling IN ("CT0", "NEW CT0", "Dismantling CT0") AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as collect_ct0,
        SUM(CASE WHEN cno.jenis_dismantling = "NEW LOSS" AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as collect_newloss,
        SUM(CASE WHEN cno.jenis_dismantling IN ("CAPS", "147", "MIGRASI 147") AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as collect_caps,
        SUM(CASE WHEN (cno.jenis_dismantling NOT IN ("CT0", "NEW CT0", "Dismantling CT0", "NEW LOSS", "CAPS", "147", "MIGRASI 147") OR cno.jenis_dismantling IS NULL) AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as collect_massal,
        SUM(CASE WHEN pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as berhasil_diambil,
        SUM(CASE WHEN rrt.type_rtwh = "rtwh_total" AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as rtwh_berhasil,
        SUM(CASE WHEN( rrt.type_rtwh = "uncollected_total" OR rrt.type_rtwh IS NULL) AND pls.laporan_status = "BERHASIL DIAMBIL" THEN 1 ELSE 0 END) as rtwh_belum
        FROM cabut_nte_order cno
        LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER AND (dt.jenis_order = "CABUT_NTE" OR dt.jenis_layanan = "CABUT_NTE")
        LEFT JOIN collected_nte_dismantling cnd ON cno.wfm_id = cnd.no_order
        LEFT JOIN tacticalpro_rtwh rrt ON cnd.sn_nte = rrt.sn AND cnd.no_order = rrt.no_inet
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu AND r.uraian LIKE "%DEDICATED NTE%"
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        (DATE(dt.tgl) BETWEEN "' . $start .'" AND "' . $end . '")
        GROUP BY r.uraian
    ');

    foreach ($tim as $val)
    {

      foreach ($query1 as $val_c1) {
        if (!empty($val_c1->tim)) {
          if ($val_c1->tim == $val->tim) {
            $query[$val->tim]['und_ct0'] = $val_c1->und_ct0;
            $query[$val->tim]['und_newloss'] = $val_c1->und_newloss;
            $query[$val->tim]['und_caps'] = $val_c1->und_caps;
            $query[$val->tim]['und_massal'] = $val_c1->und_massal;
          }
        } else {
          $query['NON TIM']['und_ct0'] = $val_c1->und_ct0;
          $query['NON TIM']['und_newloss'] = $val_c1->und_newloss;
          $query['NON TIM']['und_caps'] = $val_c1->und_caps;
          $query['NON TIM']['und_massal'] = $val_c1->und_massal;
        }
      }

      foreach ($query2 as $val_c2) {
        if (!empty($val_c2->tim)) {
          if ($val_c2->tim == $val->tim) {
            $query[$val->tim]['collect_ct0'] = $val_c2->collect_ct0;
            $query[$val->tim]['collect_newloss'] = $val_c2->collect_newloss;
            $query[$val->tim]['collect_caps'] = $val_c2->collect_caps;
            $query[$val->tim]['collect_massal'] = $val_c2->collect_massal;
            $query[$val->tim]['berhasil_diambil'] = $val_c2->berhasil_diambil;
            $query[$val->tim]['rtwh_berhasil'] = $val_c2->rtwh_berhasil;
            $query[$val->tim]['rtwh_belum'] = $val_c2->rtwh_belum;
          }
        } else {
          $query['NON TIM']['collect_ct0'] = $val_c2->collect_ct0;
          $query['NON TIM']['collect_newloss'] = $val_c2->collect_newloss;
          $query['NON TIM']['collect_caps'] = $val_c2->collect_caps;
          $query['NON TIM']['collect_massal'] = $val_c2->collect_massal;
          $query['NON TIM']['berhasil_diambil'] = $val_c2->berhasil_diambil;
          $query['NON TIM']['rtwh_berhasil'] = $val_c2->rtwh_berhasil;
          $query['NON TIM']['rtwh_belum'] = $val_c2->rtwh_belum;
        }
      }

    }

    return $query;

  }

  public static function newDashboardDismantlingDetail($tim, $start, $end, $status, $type)
  {
    switch ($tim) {
      case 'ALL':
        $timx = '';
        break;
      case 'NON TIM':
        $timx = 'AND r.uraian IS NULL';
        break;
      default:
        $timx = 'AND r.uraian = "' . $tim . '"';
        break;
    }

    switch ($type) {
      case 'SISA_ORDER':
        switch ($status) {
          case 'UND_CT0':
            $statusx = 'AND vrd.tipe_dapros IN ("CT0", "NEW CT0")';
            break;
          case 'UND_NEWLOSS':
            $statusx = 'AND vrd.tipe_dapros = "NEW LOSS"';
            break;
          case 'UND_CAPS':
            $statusx = 'AND vrd.tipe_dapros = "CAPS"';
            break;
          case 'UND_MASSAL':
            $statusx = 'AND vrd.tipe_dapros NOT IN ("CT0", "NEW CT0", "NEW LOSS", "CAPS")';
            break;
          case 'UND_ALL':
            $statusx = '';
            break;
        }

        return DB::select('
          SELECT
          r.uraian as tim,
          dt.tgl as tgl_dispatch,
          gt.title as sektor,
          vrd.namapelanggan,
          vrd.*
          FROM visitRacoonDismantling vrd
          LEFT JOIN dispatch_teknisi dt ON vrd.nopel = dt.NO_ORDER AND dt.jenis_order = "CABUT_NTE"
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
          dt.jenis_order IS NULL AND
          vrd.nik IN ("15897115", "15897117")
          ' . $timx .'
          ' . $statusx . '
        ');

        break;
      
      case 'COLLECTING':
        switch ($status) {
          case 'COLLECT_CT0':
            $statusx = 'AND cno.jenis_dismantling IN ("CT0", "NEW CT0", "Dismantling CT0") AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'COLLECT_NEWLOSS':
            $statusx = 'AND cno.jenis_dismantling = "NEW LOSS" AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'COLLECT_CAPS':
            $statusx = 'AND cno.jenis_dismantling IN ("CAPS", "147", "MIGRASI 147") AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'COLLECT_MASSAL':
            $statusx = 'AND (cno.jenis_dismantling NOT IN ("CT0", "NEW CT0", "Dismantling CT0", "NEW LOSS", "CAPS", "147", "MIGRASI 147") OR cno.jenis_dismantling IS NULL) AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'BERHASIL_DIAMBIL':
            $statusx = 'AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'RTWH_BERHASIL':
            $statusx = 'AND rrt.type_rtwh = "rtwh_total" AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'RTWH_BELUM':
            $statusx = 'AND (rrt.type_rtwh = "uncollected_total" OR rrt.type_rtwh IS NULL) AND pls.laporan_status = "BERHASIL DIAMBIL"';
            break;
          case 'COLLECT_ALL':
            $statusx = '';
            break;
          case 'RTWH_ALL':
            $statusx = '';
            break;
        }

        return DB::select('
          SELECT
              dt.id as id_dt,
              r.uraian as tim,
              dt.tgl as tgl_dispatch,
              gt.title as sektor,
              pls.laporan_status,
              pl.catatan as catatan_tek,
              pl.typeont,
              pl.snont,
              pl.typestb,
              pl.snstb,
              pl.type_plc,
              pl.sn_plc,
              pl.type_wifiext,
              pl.sn_wifiext,
              pl.type_indibox,
              pl.sn_indibox,
              pl.type_indihomesmart,
              pl.sn_indihomesmart,
              pl.modified_at as tgl_laporan,
              cno.wfm_id as nopel,
              rrt.generated_at as rrt_generated_at,
              rrt.flag as rrt_flag,
              rrt.vendor as rrt_vendor,
              rrt.sn as rrt_sn,
              rrt.source as rrt_source,
              rrt.warehouse as rrt_warehouse,
              rrt.collected_source as rrt_collected_source,
              rrt.type_rtwh,
              cnd.type_nte as cnd_flag,
              cnd.merk_nte as cnd_vendor,
              cnd.sn_nte as cnd_sn,
              cno.*
          FROM cabut_nte_order cno
          LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER AND (dt.jenis_order = "CABUT_NTE" OR dt.jenis_layanan = "CABUT_NTE")
          LEFT JOIN collected_nte_dismantling cnd ON cno.wfm_id = cnd.no_order
          LEFT JOIN tacticalpro_rtwh rrt ON cnd.sn_nte = rrt.sn AND cnd.no_order = rrt.no_inet
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
              (DATE(dt.tgl) BETWEEN "' . $start . '" AND "' . $end .'")
              ' . $timx . '
              ' . $statusx . '
        ');
        break;
    }
  }

  public static function progress_totalpi()
  {
    return DB::select('
      SELECT
        md.sektorx as area,
        SUM(CASE WHEN (pls.pivot_status_pi = "BELUM PROGRESS" OR pls.laporan_status IS NULL) AND (DATE(pkt.order_date) != "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as belum_progress_pipagi,
        SUM(CASE WHEN pls.pivot_status_pi = "PROGRESS" AND (DATE(pkt.order_date) != "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as sudah_progress_pipagi,
        SUM(CASE WHEN dps.orderStatus = "COMPLETED" AND (DATE(pkt.order_date) != "' . date('Y-m-d') .'") THEN 1 ELSE 0 END) as ps_pipagi,
        SUM(CASE WHEN (pls.pivot_status_pi = "BELUM PROGRESS" OR pls.laporan_status IS NULL) AND (DATE(pkt.order_date) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as belum_progress_pibaru,
        SUM(CASE WHEN pls.pivot_status_pi = "PROGRESS" AND (DATE(pkt.order_date) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as sudah_progress_pibaru,
        SUM(CASE WHEN dps.orderStatus = "COMPLETED" AND (DATE(pkt.order_date) = "' . date('Y-m-d') . '") THEN 1 ELSE 0 END) as ps_pibaru
      FROM pi_kpro_tr6 pkt
      LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN Data_Pelanggan_Starclick dps ON pkt.order_id = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
      WHERE
        pkt.witel = "BANJARMASIN"
      GROUP BY md.sektorx
    ');
  }

  public static function progress_totalpi_detail($sektor, $type, $status)
  {
    switch ($sektor) {
      case 'NON AREA':
        $where_sektor = 'AND md.sektorx IS NULL';
        break;
      
      case 'ALL':
        $where_sektor = '';
        break;
      
      default:
        $where_sektor = 'AND md.sektorx = "' . $sektor . '"';
        break;
    }

    switch ($type) {
      case 'pi_pagi':
        $where_type = 'AND (DATE(pkt.order_date) != "' . date('Y-m-d') . '")';
        break;
      
      case 'pi_baru':
        $where_type = 'AND (DATE(pkt.order_date) = "' . date('Y-m-d') . '")';
        break;
      
      case 'pi':
        $where_type = '';
        break;
    }

    switch ($status) {
      case 'belum_progress':
        $where_status = 'AND (pls.pivot_status_pi = "BELUM PROGRESS" OR pls.laporan_status IS NULL)';
        break;
      
      case 'sudah_progress':
        $where_status = 'AND pls.pivot_status_pi = "PROGRESS"';
        break;

      case 'ps':
        $where_status = 'AND dps.orderStatus = "COMPLETED"';
        break;

      case 'total':
        $where_status = '';
        break;
    }

    return DB::select('
      SELECT
        md.sektorx as area,
        gt.title as sektor,
        r.uraian as tim,
        pl.modified_at as tgl_status_tek,
        pls.laporan_status as status_tek,
        dps.orderStatus as status_starclick,
        DATE(pkt.order_date) as pkt_order_date,
        pkt.*
      FROM pi_kpro_tr6 pkt
      LEFT JOIN dispatch_teknisi dt ON pkt.order_id = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN Data_Pelanggan_Starclick dps ON pkt.order_id = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON pkt.sto = md.sto
      WHERE
        pkt.witel = "BANJARMASIN"
        ' . $where_sektor .'
        ' . $where_type .'
        ' . $where_status . '
    ');
  }

  public static function reportDailyClosingOrder()
  {
    return DB::select('
      SELECT
        gt.title as sektor,
        SUM(CASE WHEN ib.ONU_Rx < "-23" THEN 1 ELSE 0 END) as jml_spec,
        SUM(CASE WHEN (ib.ONU_Rx > "-23" OR ib.ONU_Rx IS NULL) THEN 1 ELSE 0 END) as jml_unspec,
        SUM(CASE WHEN ib.ONU_Link_Status = "DYING GASP" THEN 1 ELSE 0 END) as jml_dyinggasp,
        SUM(CASE WHEN ib.ONU_Link_Status = "LOS" THEN 1 ELSE 0 END) as jml_los,
        SUM(CASE WHEN ib.ONU_Link_Status = "OFFLINE" THEN 1 ELSE 0 END) as jml_offline,
        SUM(CASE WHEN ib.ONU_Link_Status IN ("-", "") THEN 1 ELSE 0 END) as jml_na,
        COUNT(*) as total
      FROM data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi dt ON dn1l.ID_INCREMENT = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN ibooster_dailyAssurance ib ON dn1l.Incident = ib.order_id
      WHERE
        gt.ket_posisi = "IOAN" AND
        (DATE(dt.tgl) = "'.date('Y-m-d', strtotime('-1 days')).'") AND
        pls.laporan_status = "UP"
      GROUP BY gt.title
      ORDER BY total DESC
    ');
  }

  public static function reportDailyClosingOrderDetail($sektor, $status)
  {
    switch ($sektor) {
      case 'ALL':
          $sektorx = '';
        break;
      
      default:
          $sektorx = 'AND gt.title = "'.$sektor.'"';
        break;
    }

    switch ($status) {
      case 'SPEC':
        $statusx = 'AND ib.ONU_Rx < "-23" ';
        break;
      
      case 'UNSPEC':
        $statusx = 'AND (ib.ONU_Rx > "-23" OR ib.ONU_Rx IS NULL)';
        break;

      case 'DYINGGASP':
        $statusx = 'AND ib.ONU_Link_Status = "DYING GASP"';
        break;

      case 'LOS':
        $statusx = 'AND ib.ONU_Link_Status = "LOS"';
        break;

      case 'OFFLINE':
        $statusx = 'AND ib.ONU_Link_Status = "OFFLINE"';
        break;

      case 'NA':
        $statusx = 'AND ib.ONU_Link_Status IN ("-", "")';
        break;

      case 'ALL':
        $statusx = '';
        break;
    }
    return DB::select('
      SELECT
        gt.title as sektor,
        r.uraian as tim,
        dn1l.Incident,
        dn1l.Service_No,
        ib.ONU_Rx,
        pls.laporan_status as status_tek,
        pla.action as action_tek,
        plp.penyebab as penyebab_tek,
        dt.created_at as tgl_dispatch
      FROM data_nossa_1_log dn1l
      LEFT JOIN dispatch_teknisi dt ON dn1l.ID_INCREMENT = dt.NO_ORDER
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN ibooster_dailyAssurance ib ON dn1l.Incident = ib.order_id
      WHERE
        gt.ket_posisi = "IOAN" AND
        (DATE(dt.tgl) = "'.date('Y-m-d', strtotime('-1 days')).'") AND
        pls.laporan_status = "UP"
        '.$sektorx.'
        '.$statusx.'
      ORDER BY dt.created_at ASC
    ');
  }

  public static function kendala_alpro($date)
  {
    return DB::select('
      SELECT
        gt.title as sektor,
        r.uraian as tim,
        dt.id as id_dt,
        dt.Ndem as order_id,
        dt.jenis_order,
        dps.orderName as dps_nama,
        pmw.customer as pmw_nama,
        dps.orderDate as dps_orderDate,
        pmw.orderDate as pmw_orderDate,
        pl.kordinat_pelanggan,
        pl.nama_odp,
        pl.kordinat_odp,
        pls.laporan_status,
        pl.modified_at
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
      pls.laporan_status = "KENDALA ALPRO" AND
      (DATE(pl.modified_at) LIKE "'.$date.'%")
      ORDER BY pl.modified_at DESC
    ');
  }

  public static function raportPickupOnline($nper)
  {
    return DB::select('
      SELECT
          gt.title as sektor,
          r.uraian as team,
          kpt.ORDER_ID as order_id,
          kpt.CUSTOMER_NAME as customer_name,
          kpt.JENIS_PSB as jenis_psb,
          SUBSTR(kpt.PROVIDER, 1, 3) as provider,
          kpt.STATUS_MESSAGE as status_kpro,
          kpt.LAST_UPDATED_DATE as tgl_ps_kpro,
          ttp.status_wo as status_pickup,
          ttp.last_status as last_status_pickup,
          ttp.ps_date as tgl_ps_pickup    
      FROM kpro_tr6 kpt
      LEFT JOIN tacticalpro_tr6 ttp ON kpt.ORDER_ID = ttp.sc_id
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
      WHERE
          kpt.TYPE_TRANSAKSI = "NEW SALES" AND
          kpt.STATUS_RESUME = "Completed (PS)" AND
          kpt.JENIS_PSB = "AO" AND
          SUBSTR(kpt.PROVIDER, 1, 3) = "DCS" AND
          kpt.PRODUCT = "INDIHOME" AND
          kpt.NPER = "'.$nper.'" AND
          kpt.WITEL = "BANJARMASIN"
      ORDER BY kpt.LAST_UPDATED_DATE DESC
    ');
  }

  public static function kendalaSaber($startDate, $endDate)
  {
    $query1 = DB::select('
        SELECT
            gt.title as sektor,
            r.uraian as tim,
            ma.mitra_amija_pt,
            pls.laporan_status as status_teknisi,
            dt.Ndem as order_id,
            dps.orderName,
            dps.orderDate,
            dps.orderStatus,
            pl.nama_odp,
            dps.sto,
            pl.modified_at as tgl_laporan
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.kat = "DELTA" AND ma.witel = "KALSEL"
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
            dt.jenis_order = "SC" AND
            dps.orderStatusId IN ("1202", "51", "-1201", "1204", "1600", "2300") AND
            pl.status_laporan IN (11, 2, 24, 112, 113) AND
            (DATE(pl.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
    ');

    $query2 = DB::select('
        SELECT
            gt.title as sektor,
            r.uraian as tim,
            ma.mitra_amija_pt,
            pls.laporan_status as status_teknisi,
            dt.Ndem as order_id,
            pmw.customer as orderName,
            pmw.orderDate,
            "NULL" as orderStatus,
            pl.nama_odp,
            pmw.sto,
            pl.modified_at as tgl_laporan
        FROM dispatch_teknisi dt
        LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.kat = "DELTA" AND ma.witel = "KALSEL"
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        WHERE
            dt.jenis_order = "MYIR" AND
            pl.status_laporan IN (11, 2, 24, 112, 113) AND
            (DATE(pl.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
    ');

    $query_merge = array_merge($query1, $query2);

    return $query_merge;
  }

  public static function giveawayPS($date)
  {
    $regu = DB::select('SELECT uraian as tim FROM regu WHERE ACTIVE = 1');

    $query = [];

    $query1 = DB::select('
        SELECT
            r.uraian as tim,
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            SUM(CASE WHEN psi.tgl_etat_dt THEN 1 ELSE 0 END) as jml_ps_prabac
        FROM prabac_psharian_indihome psi
        LEFT JOIN dispatch_teknisi dt ON psi.order_id_int = dt.NO_ORDER AND dt.jenis_order = "SC"
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id 
        LEFT JOIN regu r ON dt.id_regu = r.id_regu AND r.id_regu NOT IN (779, 2198)
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            (DATE(psi.tgl_etat_dt) = "'.$date.'") AND
            r.uraian IS NOT NULL
        GROUP BY r.uraian
    ');

    $query2 = DB::select('
      SELECT
            r.uraian as tim,
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            SUM(CASE WHEN dps.orderStatusId = 1500 THEN 1 ELSE 0 END) as jml_ps_pda
        FROM Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order = "SC"
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id 
        LEFT JOIN regu r ON dt.id_regu = r.id_regu AND r.id_regu NOT IN (779, 2198)
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            (DATE(dps.orderDatePs) = "'.$date.'") AND
            dps.jenisPsb LIKE "PDA%" AND
            r.uraian IS NOT NULL
        GROUP BY r.uraian
    ');

    foreach ($regu as $val)
    {
      foreach ($query1 as $val_c1)
      {
        if (!empty($val_c1->tim))
        {
          if ($val_c1->tim == $val->tim)
          {
            $query[$val->tim]['sektor'] = $val_c1->sektor;
            $query[$val->tim]['mitra'] = $val_c1->mitra;
            $query[$val->tim]['list']['jml_ps_prabac'] = $val_c1->jml_ps_prabac;
          }
        } else {
          $query['NON TIM']['sektor'] = $val_c1->sektor;
          $query['NON TIM']['mitra'] = $val_c1->mitra;
          $query['NON TIM']['list']['jml_ps_prabac'] = $val_c1->jml_ps_prabac;
        }
      }

      foreach ($query2 as $val_c2)
      {
        if (!empty($val_c2->tim))
        {
          if ($val_c2->tim == $val->tim)
          {
            $query[$val->tim]['sektor'] = $val_c2->sektor;
            $query[$val->tim]['mitra'] = $val_c2->mitra;
            $query[$val->tim]['list']['jml_ps_pda'] = $val_c2->jml_ps_pda;
          }
        } else {
          $query['NON TIM']['sektor'] = $val_c2->sektor;
          $query['NON TIM']['mitra'] = $val_c2->mitra;
          $query['NON TIM']['list']['jml_ps_pda'] = $val_c2->jml_ps_pda;
        }
      }
    }
    
    $fd = [];

    foreach($query as $k => &$v)
    {
      if(in_array(array_sum($v['list']), [3, 4, 5]) )
      {
        $fd['PS_'.array_sum($v['list'])][$k]['sektor'] = $v['sektor'];
        $fd['PS_'.array_sum($v['list'])][$k]['mitra'] = $v['mitra'];
        $fd['PS_'.array_sum($v['list'])][$k]['qty'] = array_sum($v['list']);
        unset($query[$k]['list']); 
      }
    }
    unset($v);

    ksort($fd);
    return $fd;
  }

  public static function giveawayPSdetail($tim, $date)
  {
    switch ($tim) {
      case 'ALL':
        $timx = '';
        break;
      
      default:
        $timx = 'AND r.uraian = "'.$tim.'"';
        break;
    }
    
    $query1 = DB::select('
        SELECT
            r.uraian as tim,
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            psi.order_id_int as order_id,
            psi.nama as nama_pelanggan,
            psi.tgl_etat_dtm as order_datePs,
            pls.laporan_status,
            pl.modified_at as tgl_laporan
        FROM prabac_psharian_indihome psi
        LEFT JOIN dispatch_teknisi dt ON psi.order_id_int = dt.NO_ORDER AND dt.jenis_order = "SC"
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id 
        LEFT JOIN regu r ON dt.id_regu = r.id_regu AND r.id_regu NOT IN (779, 2198)
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            (DATE(psi.tgl_etat_dt) = "'.$date.'") AND
            psi.status_order = "PS"
            '.$timx.'
        ORDER BY psi.tgl_etat_dtm DESC
    ');

    $query2 = DB::select('
        SELECT
            r.uraian as tim,
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            dps.orderIdInteger as order_id,
            dps.orderName as nama_pelanggan,
            dps.orderDatePs as order_datePs,
            pls.laporan_status,
            pl.modified_at as tgl_laporan
        FROM Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER AND dt.jenis_order = "SC"
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id 
        LEFT JOIN regu r ON dt.id_regu = r.id_regu AND r.id_regu NOT IN (779, 2198)
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            (DATE(dps.orderDatePs) = "'.$date.'") AND
            dps.jenisPsb LIKE "PDA%" AND
            dps.orderStatusId = 1500
            '.$timx.'
        ORDER BY dps.orderDatePs DESC
    ');

    return (array_merge($query1, $query2));
  }

  public static function produktifTeam($show_data, $area, $start, $end)
  {
    switch ($show_data) {
      case 'SEKTOR':
          switch ($area) {
            case 'ALL':
                $areax = '';
              break;
            
            default:
                $areax = 'AND gt.title = "'.$area.'"';
              break;
          }
        break;
      
      case 'MITRA':
          switch ($area) {
            case 'ALL':
                $areax = '';
              break;
            
            default:
                $areax = 'AND ma.mitra_amija_pt = "'.$area.'"';
              break;
          }
        break;

        default:
            $areax = '';
        break;
    }

    return DB::select('
        SELECT
            r.uraian as tim,
            ma.mitra_amija_pt as mitra,
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            SUM(CASE WHEN (pl.status_laporan = 6 OR pl.id IS NULL) THEN 1 ELSE 0 END) as jml_antrian,
            SUM(CASE WHEN pl.status_laporan = 5 THEN 1 ELSE 0 END) as jml_ogp,
            SUM(CASE WHEN pl.status_laporan = 4 THEN 1 ELSE 0 END) as jml_hr,
            SUM(CASE WHEN pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "KT" THEN 1 ELSE 0 END) as jml_kt,
            SUM(CASE WHEN pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "KP" THEN 1 ELSE 0 END) as jml_kp,
            SUM(CASE WHEN pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "UP" THEN 1 ELSE 0 END) as jml_up
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            dt.jenis_order IN ("SC", "MYIR") AND
            DATE(dt.tgl) BETWEEN "'.$start.'" AND "'.$end.'"
            '.$areax.'
        GROUP BY r.uraian
        ORDER BY jml_up DESC
    ');
  }

  public static function produktifTeamDetail($show_data, $area, $team, $start, $end, $status)
  {
    switch ($show_data) {
      case 'SEKTOR':
          switch ($area) {
            case 'ALL':
                $areax = '';
              break;
            
            default:
                $areax = 'AND gt.title = "'.$area.'"';
              break;
          }
        break;
      
      case 'MITRA':
          switch ($area) {
            case 'ALL':
                $areax = '';
              break;
            
            default:
                $areax = 'AND ma.mitra_amija_pt = "'.$area.'"';
              break;
          }
        break;

        default:
            $areax = '';
        break;
    }

    switch ($team) {
      case 'ALL':
          $teamx = '';
        break;
      
      default:
          $teamx = 'AND r.uraian = "'.$team.'"';
        break;
    }

    switch ($status) {
      case 'JML_ORDER':
          $statusx = 'AND (pls.laporan_status_id IN (1, 2, 4, 5, 6, 10, 11, 13, 15, 16, 23, 24, 25, 26, 27, 30, 34, 35, 36, 37, 38, 42, 46, 47, 48, 49, 51, 55, 56, 64, 65, 71, 72, 73, 77, 81, 87, 106, 107, 108, 109, 110, 112, 113, 118, 121) OR pl.id IS NULL)';
        break;

      case 'ANTRIAN':
          $statusx = 'AND (pls.laporan_status_id = 6 OR pl.id IS NULL)';
        break;

      case 'OGP':
          $statusx = 'AND pls.laporan_status_id = 5';
        break;

      case 'HR':
        $statusx = 'AND pls.laporan_status_id = 4';
      break;

      case 'KT':
        $statusx = 'AND pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "KT"';
      break;

      case 'KP':
        $statusx = 'AND pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "KP"';
      break;

      case 'UP':
        $statusx = 'AND pls.jenis_order IN ("ALL", "PROV", "PROVA", "PROVV") AND pls.grup = "UP"';
      break;
    }

    return DB::select('
        SELECT
            gt.title as sektor,
            ma.mitra_amija_pt as mitra,
            r.uraian as tim,
            dt.Ndem as order_id,
            dps.orderDate as dps_orderDate,
            pmw.orderDate as pmw_orderDate,
            pls.laporan_status as status_tek,
            pl.modified_at as tgl_laporan
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            dt.jenis_order IN ("SC", "MYIR") AND
            DATE(dt.tgl) BETWEEN "'.$start.'" AND "'.$end.'"
            '.$areax.'
            '.$teamx.'
            '.$statusx.'
    ');
  }

  public static function kpi7_psb($witel, $type, $area_show, $periode)
  {
    $nper = date('Ym', strtotime($periode));
    $periode1 = date('Ym', strtotime(date($periode)."first day of last month"));
    $periode2 = date('Ym', strtotime($periode));
    $periode1ps = 'ps_'.strtolower(date('M', strtotime(date($periode)."first day of last month")));
    $periode2ps = 'ps_'.strtolower(date('M', strtotime(date($periode))));

    switch ($witel) {
      case 'KALSEL':
        switch ($type) {
          case 'SEKTOR':

            $area = DB::table('group_telegram')->where('ket_posisi', 'PROV')->whereNotIn('id', [97, 103])->whereNotNull('title')->select('title as area')->get();

            switch ($area_show) {
              case 'ALL':
                $areas = 'gt.title IS NOT NULL';
                break;
              
              default:
                $areas = 'gt.title = "'.$area_show.'"';
                break;
            }

            $query1 = DB::select('
              SELECT
                gt.title as area,
                SUM(CASE WHEN pkp.witel = "KALSEL" AND pkp.header = "KPIPS" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_ps,
                SUM(CASE WHEN pkp.witel = "KALSEL" AND pkp.header = "KPIPI" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_pi
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
              LEFT JOIN prabac_kpiprov_pspi pkp ON dt.NO_ORDER = pkp.no_sc_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY gt.title
            ');

            $query2 = DB::select('
              SELECT
                gt.title as area,
                SUM(CASE WHEN pkt.witel = "KALSEL" AND pkt.header = "COMPTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as comptti,
                SUM(CASE WHEN pkt.witel = "KALSEL" AND pkt.header = "PSTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as pstti
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
              LEFT JOIN prabac_kpiprov_tti pkt ON dt.NO_ORDER = pkt.no_sc_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY gt.title
            ');

            $query3 = DB::select('
              SELECT
                gt.title as area,
                SUM(CASE WHEN pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%" THEN 1 ELSE 0 END) as jml_ffg
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
              LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY gt.title
            ');

            $query4 = DB::select('
              SELECT
                gt.title as area,
                SUM(CASE WHEN (pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
                SUM(CASE WHEN (pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
              LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
              LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
              WHERE
                '.$areas.'
              GROUP BY gt.title
            ');

            $query5 = DB::select('
              SELECT
                gt.title as area,
                SUM(CASE WHEN kpt.WITEL = "BANJARMASIN" AND kpt.NPER = "'.$periode1.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode1ps.',
                SUM(CASE WHEN kpt.WITEL = "BANJARMASIN" AND kpt.NPER = "'.$periode2.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode2ps.'
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV" AND gt.id NOT IN (97,103)
              LEFT JOIN kpro_tr6 kpt ON dt.NO_ORDER = kpt.ORDER_ID
              WHERE
                '.$areas.'
              GROUP BY gt.title
            ');

            foreach ($area as $val)
            {
              foreach ($query1 as $val_c1)
              {
                if (!empty($val_c1->area))
                {
                  if ($val_c1->area == $val->area)
                  {
                    $query[$val->area]['kpi_ps'] = $val_c1->kpi_ps;
                    $query[$val->area]['kpi_pi'] = $val_c1->kpi_pi;
                  }
                } else {
                  $query['NON AREA']['kpi_ps'] = $val_c1->kpi_ps;
                  $query['NON AREA']['kpi_pi'] = $val_c1->kpi_pi;
                }
              }
              foreach ($query2 as $val_c2)
              {
                if (!empty($val_c2->area))
                {
                  if ($val_c2->area == $val->area)
                  {
                    $query[$val->area]['comptti'] = $val_c2->comptti;
                    $query[$val->area]['pstti'] = $val_c2->pstti;
                  }
                } else {
                  $query['NON AREA']['comptti'] = $val_c2->comptti;
                  $query['NON AREA']['pstti'] = $val_c2->pstti;
                }
              }
              foreach ($query3 as $val_c3)
              {
                if (!empty($val_c3->area))
                {
                  if ($val_c3->area == $val->area)
                  {
                    $query[$val->area]['jml_ffg'] = $val_c3->jml_ffg;
                  }
                } else {
                  $query['NON AREA']['jml_ffg'] = $val_c3->jml_ffg;
                }
              }
              foreach ($query4 as $val_c4)
              {
                if (!empty($val_c4->area))
                {
                  if ($val_c4->area == $val->area)
                  {
                    $query[$val->area]['ffg_notcomply'] = $val_c4->ffg_notcomply;
                    $query[$val->area]['ffg_comply'] = $val_c4->ffg_comply;
                  }
                } else {
                  $query['NON AREA']['ffg_notcomply'] = $val_c4->ffg_notcomply;
                  $query['NON AREA']['ffg_comply'] = $val_c4->ffg_comply;
                }
              }
              foreach ($query5 as $val_c5)
              {
                if (!empty($val_c5->area))
                {
                  if ($val_c5->area == $val->area)
                  {
                    $query[$val->area][$periode1ps] = $val_c5->$periode1ps;
                    $query[$val->area][$periode2ps] = $val_c5->$periode2ps;
                  }
                } else {
                  $query['NON AREA'][$periode1ps] = $val_c5->$periode1ps;
                  $query['NON AREA'][$periode2ps] = $val_c5->$periode2ps;
                }
              }
            }
            return $query;
          break;
          
          case 'MITRA':

            $area = DB::table('mitra_amija')->where('mitra_status', 'ACTIVE')->where('witel', 'KALSEL')->select('mitra_amija_pt as area')->groupBy('mitra_amija_pt')->get();

            switch ($area_show) {
              case 'ALL':
                $areas = 'ma.mitra_amija_pt IS NOT NULL';
                break;
              
              default:
                $areas = 'ma.mitra_amija_pt = "'.$area_show.'"';
                break;
            }

            $query1 = DB::select('
              SELECT
                ma.mitra_amija_pt as area,
                SUM(CASE WHEN pkp.witel = "KALSEL" AND pkp.header = "KPIPS" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_ps,
                SUM(CASE WHEN pkp.witel = "KALSEL" AND pkp.header = "KPIPI" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_pi
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
              LEFT JOIN prabac_kpiprov_pspi pkp ON dt.NO_ORDER = pkp.no_sc_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY ma.mitra_amija_pt
            ');

            $query2 = DB::select('
              SELECT
                ma.mitra_amija_pt as area,
                SUM(CASE WHEN pkt.witel = "KALSEL" AND pkt.header = "COMPTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as comptti,
                SUM(CASE WHEN pkt.witel = "KALSEL" AND pkt.header = "PSTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as pstti
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
              LEFT JOIN prabac_kpiprov_tti pkt ON dt.NO_ORDER = pkt.no_sc_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY ma.mitra_amija_pt
            ');

            $query3 = DB::select('
              SELECT
                ma.mitra_amija_pt as area,
                SUM(CASE WHEN pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%" THEN 1 ELSE 0 END) as jml_ffg
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
              LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
              WHERE
                '.$areas.'
              GROUP BY ma.mitra_amija_pt
            ');

            $query4 = DB::select('
              SELECT
                ma.mitra_amija_pt as area,
                SUM(CASE WHEN (pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
                SUM(CASE WHEN (pwf.witel = "KALSEL" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
              LEFT JOIN prabac_wecare2022_ffg pwf ON dt.NO_ORDER = pwf.sc_id_int AND dt.jenis_order IN ("SC", "MYIR")
              LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
              WHERE
                '.$areas.'
              GROUP BY ma.mitra_amija_pt
            ');

            $query5 = DB::select('
              SELECT
                ma.mitra_amija_pt as area,
                SUM(CASE WHEN kpt.WITEL = "BANJARMASIN" AND kpt.NPER = "'.$periode1.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode1ps.',
                SUM(CASE WHEN kpt.WITEL = "BANJARMASIN" AND kpt.NPER = "'.$periode2.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode2ps.'
              FROM dispatch_teknisi dt
              LEFT JOIN regu r ON dt.id_regu = r.id_regu
              LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
              LEFT JOIN kpro_tr6 kpt ON dt.NO_ORDER = kpt.ORDER_ID
              WHERE
                '.$areas.'
              GROUP BY ma.mitra_amija_pt
            ');

            foreach ($area as $val)
            {
              foreach ($query1 as $val_c1)
              {
                if (!empty($val_c1->area))
                {
                  if ($val_c1->area == $val->area)
                  {
                    $query[$val->area]['kpi_ps'] = $val_c1->kpi_ps;
                    $query[$val->area]['kpi_pi'] = $val_c1->kpi_pi;
                  }
                } else {
                  $query['NON AREA']['kpi_ps'] = $val_c1->kpi_ps;
                  $query['NON AREA']['kpi_pi'] = $val_c1->kpi_pi;
                }
              }
              foreach ($query2 as $val_c2)
              {
                if (!empty($val_c2->area))
                {
                  if ($val_c2->area == $val->area)
                  {
                    $query[$val->area]['comptti'] = $val_c2->comptti;
                    $query[$val->area]['pstti'] = $val_c2->pstti;
                  }
                } else {
                  $query['NON AREA']['comptti'] = $val_c2->comptti;
                  $query['NON AREA']['pstti'] = $val_c2->pstti;
                }
              }
              foreach ($query3 as $val_c3)
              {
                if (!empty($val_c3->area))
                {
                  if ($val_c3->area == $val->area)
                  {
                    $query[$val->area]['jml_ffg'] = $val_c3->jml_ffg;
                  }
                } else {
                  $query['NON AREA']['jml_ffg'] = $val_c3->jml_ffg;
                }
              }
              foreach ($query4 as $val_c4)
              {
                if (!empty($val_c4->area))
                {
                  if ($val_c4->area == $val->area)
                  {
                    $query[$val->area]['ffg_notcomply'] = $val_c4->ffg_notcomply;
                    $query[$val->area]['ffg_comply'] = $val_c4->ffg_comply;
                  }
                } else {
                  $query['NON AREA']['ffg_notcomply'] = $val_c4->ffg_notcomply;
                  $query['NON AREA']['ffg_comply'] = $val_c4->ffg_comply;
                }
              }
              foreach ($query5 as $val_c5)
              {
                if (!empty($val_c5->area))
                {
                  if ($val_c5->area == $val->area)
                  {
                    $query[$val->area][$periode1ps] = $val_c5->$periode1ps;
                    $query[$val->area][$periode2ps] = $val_c5->$periode2ps;
                  }
                } else {
                  $query['NON AREA'][$periode1ps] = $val_c5->$periode1ps;
                  $query['NON AREA'][$periode2ps] = $val_c5->$periode2ps;
                }
              }
            }
            return $query;
        break;
        }

      case 'BALIKPAPAN':
        switch ($type) {
          case 'SEKTOR':

            $area = DB::table('maintenance_datel')->where('witel', 'BALIKPAPAN')->select('sektor_prov as area')->groupBy('sektor_prov')->get();

            switch ($area_show) {
              case 'ALL':
                $areas = 'AND md.sektor_prov IS NOT NULL';
                break;
              
              default:
                $areas = 'AND md.sektor_prov = "'.$area_show.'"';
                break;
            }

            $query1 = DB::select('
              SELECT
                md.sektor_prov as area,
                SUM(CASE WHEN pkp.witel = "BALIKPAPAN" AND pkp.header = "KPIPS" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_ps,
                SUM(CASE WHEN pkp.witel = "BALIKPAPAN" AND pkp.header = "KPIPI" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_pi
              FROM maintenance_datel md
              LEFT JOIN prabac_kpiprov_pspi pkp ON md.sto = pkp.sto
              WHERE
                md.witel = "BALIKPAPAN"
                '.$areas.'
              GROUP BY md.sektor_prov
            ');

            $query2 = DB::select('
              SELECT
                md.sektor_prov as area,
                SUM(CASE WHEN pkt.witel = "BALIKPAPAN" AND pkt.header = "COMPTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as comptti,
                SUM(CASE WHEN pkt.witel = "BALIKPAPAN" AND pkt.header = "PSTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as pstti
              FROM maintenance_datel md
              LEFT JOIN prabac_kpiprov_tti pkt ON md.sto = pkt.sto
              WHERE
                md.witel = "BALIKPAPAN"
                '.$areas.'
              GROUP BY md.sektor_prov
            ');

            $query3 = DB::select('
              SELECT
                md.sektor_prov as area,
                SUM(CASE WHEN pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%" THEN 1 ELSE 0 END) as jml_ffg
              FROM maintenance_datel md
              LEFT JOIN prabac_wecare2022_ffg pwf ON md.sto = pwf.sto
              WHERE
                md.witel = "BALIKPAPAN"
                '.$areas.'
              GROUP BY md.sektor_prov
            ');

            $query4 = DB::select('
              SELECT
                md.sektor_prov as area,
                SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
                SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
              FROM maintenance_datel md
              LEFT JOIN prabac_wecare2022_ffg pwf ON md.sto = pwf.sto
              LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
              WHERE
                md.witel = "BALIKPAPAN"
                '.$areas.'
              GROUP BY md.sektor_prov
            ');

            $query5 = DB::select('
              SELECT
                md.sektor_prov as area,
                SUM(CASE WHEN kpt.WITEL = "BALIKPAPAN" AND kpt.NPER = "'.$periode1.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode1ps.',
                SUM(CASE WHEN kpt.WITEL = "BALIKPAPAN" AND kpt.NPER = "'.$periode2.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode2ps.'
              FROM maintenance_datel md
              LEFT JOIN kpro_tr6 kpt ON md.sto = kpt.STO
              WHERE
                md.witel = "BALIKPAPAN"
                '.$areas.'
              GROUP BY md.sektor_prov
            ');

            foreach ($area as $val)
            {
              foreach ($query1 as $val_c1)
              {
                if (!empty($val_c1->area))
                {
                  if ($val_c1->area == $val->area)
                  {
                    $query[$val->area]['kpi_ps'] = $val_c1->kpi_ps;
                    $query[$val->area]['kpi_pi'] = $val_c1->kpi_pi;
                  }
                } else {
                  $query['NON AREA']['kpi_ps'] = $val_c1->kpi_ps;
                  $query['NON AREA']['kpi_pi'] = $val_c1->kpi_pi;
                }
              }
              foreach ($query2 as $val_c2)
              {
                if (!empty($val_c2->area))
                {
                  if ($val_c2->area == $val->area)
                  {
                    $query[$val->area]['comptti'] = $val_c2->comptti;
                    $query[$val->area]['pstti'] = $val_c2->pstti;
                  }
                } else {
                  $query['NON AREA']['comptti'] = $val_c2->comptti;
                  $query['NON AREA']['pstti'] = $val_c2->pstti;
                }
              }
              foreach ($query3 as $val_c3)
              {
                if (!empty($val_c3->area))
                {
                  if ($val_c3->area == $val->area)
                  {
                    $query[$val->area]['jml_ffg'] = $val_c3->jml_ffg;
                  }
                } else {
                  $query['NON AREA']['jml_ffg'] = $val_c3->jml_ffg;
                }
              }
              foreach ($query4 as $val_c4)
              {
                if (!empty($val_c4->area))
                {
                  if ($val_c4->area == $val->area)
                  {
                    $query[$val->area]['ffg_notcomply'] = $val_c4->ffg_notcomply;
                    $query[$val->area]['ffg_comply'] = $val_c4->ffg_comply;
                  }
                } else {
                  $query['NON AREA']['ffg_notcomply'] = $val_c4->ffg_notcomply;
                  $query['NON AREA']['ffg_comply'] = $val_c4->ffg_comply;
                }
              }
              foreach ($query5 as $val_c5)
              {
                if (!empty($val_c5->area))
                {
                  if ($val_c5->area == $val->area)
                  {
                    $query[$val->area][$periode1ps] = $val_c5->$periode1ps;
                    $query[$val->area][$periode2ps] = $val_c5->$periode2ps;
                  }
                } else {
                  $query['NON AREA'][$periode1ps] = $val_c5->$periode1ps;
                  $query['NON AREA'][$periode2ps] = $val_c5->$periode2ps;
                }
              }
            }
            return $query;
            break;

          case 'MITRA':

              $area = DB::table('maintenance_datel')->where('witel', 'BALIKPAPAN')->select('mitra_prov as area')->groupBy('mitra_prov')->get();

              switch ($area_show) {
                case 'ALL':
                  $areas = 'AND md.mitra_prov IS NOT NULL';
                  break;
                
                default:
                  $areas = 'AND md.mitra_prov = "'.$area_show.'"';
                  break;
              }

              $query1 = DB::select('
                SELECT
                  md.mitra_prov as area,
                  SUM(CASE WHEN pkp.witel = "BALIKPAPAN" AND pkp.header = "KPIPS" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_ps,
                  SUM(CASE WHEN pkp.witel = "BALIKPAPAN" AND pkp.header = "KPIPI" AND pkp.periode = "'.$nper.'" THEN 1 ELSE 0 END) as kpi_pi
                FROM maintenance_datel md
                LEFT JOIN prabac_kpiprov_pspi pkp ON md.sto = pkp.sto
                WHERE
                  md.witel = "BALIKPAPAN"
                  '.$areas.'
                GROUP BY md.mitra_prov
              ');

              $query2 = DB::select('
                SELECT
                  md.mitra_prov as area,
                  SUM(CASE WHEN pkt.witel = "BALIKPAPAN" AND pkt.header = "COMPTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as comptti,
                  SUM(CASE WHEN pkt.witel = "BALIKPAPAN" AND pkt.header = "PSTTI" AND pkt.periode = "'.$nper.'" THEN 1 ELSE 0 END) as pstti
                FROM maintenance_datel md
                LEFT JOIN prabac_kpiprov_tti pkt ON md.sto = pkt.sto
                WHERE
                  md.witel = "BALIKPAPAN"
                  '.$areas.'
                GROUP BY md.mitra_prov
              ');

              $query3 = DB::select('
                SELECT
                  md.mitra_prov as area,
                  SUM(CASE WHEN pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%" THEN 1 ELSE 0 END) as jml_ffg
                FROM maintenance_datel md
                LEFT JOIN prabac_wecare2022_ffg pwf ON md.sto = pwf.sto
                WHERE
                  md.witel = "BALIKPAPAN"
                  '.$areas.'
                GROUP BY md.mitra_prov
              ');

              $query4 = DB::select('
                SELECT
                  md.mitra_prov as area,
                  SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1) > "3" THEN 1 ELSE 0 END) as ffg_notcomply,
                  SUM(CASE WHEN (pwf.witel = "BALIKPAPAN" AND DATE(pwf.trouble_opentime) LIKE "'.$periode.'%") AND SUBSTRING_INDEX(dnf.TTR_Customer, ".", 1)  < "3" THEN 1 ELSE 0 END) as ffg_comply
                FROM maintenance_datel md
                LEFT JOIN prabac_wecare2022_ffg pwf ON md.sto = pwf.sto
                LEFT JOIN data_nossa_ffg dnf ON pwf.trouble_no = dnf.Incident
                WHERE
                  md.witel = "BALIKPAPAN"
                  '.$areas.'
                GROUP BY md.mitra_prov
              ');

              $query5 = DB::select('
                SELECT
                  md.mitra_prov as area,
                  SUM(CASE WHEN kpt.WITEL = "BALIKPAPAN" AND kpt.NPER = "'.$periode1.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode1ps.',
                  SUM(CASE WHEN kpt.WITEL = "BALIKPAPAN" AND kpt.NPER = "'.$periode2.'" AND kpt.TYPE_TRANSAKSI = "NEW SALES" AND kpt.STATUS_RESUME = "Completed (PS)" THEN 1 ELSE 0 END) as '.$periode2ps.'
                FROM maintenance_datel md
                LEFT JOIN kpro_tr6 kpt ON md.sto = kpt.STO
                WHERE
                  md.witel = "BALIKPAPAN"
                  '.$areas.'
                GROUP BY md.mitra_prov
              ');

              foreach ($area as $val)
              {
                foreach ($query1 as $val_c1)
                {
                  if (!empty($val_c1->area))
                  {
                    if ($val_c1->area == $val->area)
                    {
                      $query[$val->area]['kpi_ps'] = $val_c1->kpi_ps;
                      $query[$val->area]['kpi_pi'] = $val_c1->kpi_pi;
                    }
                  } else {
                    $query['NON AREA']['kpi_ps'] = $val_c1->kpi_ps;
                    $query['NON AREA']['kpi_pi'] = $val_c1->kpi_pi;
                  }
                }
                foreach ($query2 as $val_c2)
                {
                  if (!empty($val_c2->area))
                  {
                    if ($val_c2->area == $val->area)
                    {
                      $query[$val->area]['comptti'] = $val_c2->comptti;
                      $query[$val->area]['pstti'] = $val_c2->pstti;
                    }
                  } else {
                    $query['NON AREA']['comptti'] = $val_c2->comptti;
                    $query['NON AREA']['pstti'] = $val_c2->pstti;
                  }
                }
                foreach ($query3 as $val_c3)
                {
                  if (!empty($val_c3->area))
                  {
                    if ($val_c3->area == $val->area)
                    {
                      $query[$val->area]['jml_ffg'] = $val_c3->jml_ffg;
                    }
                  } else {
                    $query['NON AREA']['jml_ffg'] = $val_c3->jml_ffg;
                  }
                }
                foreach ($query4 as $val_c4)
                {
                  if (!empty($val_c4->area))
                  {
                    if ($val_c4->area == $val->area)
                    {
                      $query[$val->area]['ffg_notcomply'] = $val_c4->ffg_notcomply;
                      $query[$val->area]['ffg_comply'] = $val_c4->ffg_comply;
                    }
                  } else {
                    $query['NON AREA']['ffg_notcomply'] = $val_c4->ffg_notcomply;
                    $query['NON AREA']['ffg_comply'] = $val_c4->ffg_comply;
                  }
                }
                foreach ($query5 as $val_c5)
                {
                  if (!empty($val_c5->area))
                  {
                    if ($val_c5->area == $val->area)
                    {
                      $query[$val->area][$periode1ps] = $val_c5->$periode1ps;
                      $query[$val->area][$periode2ps] = $val_c5->$periode2ps;
                    }
                  } else {
                    $query['NON AREA'][$periode1ps] = $val_c5->$periode1ps;
                    $query['NON AREA'][$periode2ps] = $val_c5->$periode2ps;
                  }
                }
              }
              return $query;
              break;
        }
        break;
      }
    }

    public static function newDashboardKendala($load, $view, $startDate, $endDate)
    {
      switch ($load) {
        case '1':
          $select = 'SUM(CASE WHEN pl.status_laporan = 77 THEN 1 ELSE 0 END) AS kendala_alpro,
          SUM(CASE WHEN pl.status_laporan = 42 THEN 1 ELSE 0 END) AS kendala_jalur,
          SUM(CASE WHEN pl.status_laporan = 66 THEN 1 ELSE 0 END) AS odp_jauh,
          SUM(CASE WHEN pl.status_laporan = 34 THEN 1 ELSE 0 END) AS no_odp,
          SUM(CASE WHEN pl.status_laporan = 24 THEN 1 ELSE 0 END) AS odp_full,
          SUM(CASE WHEN pl.status_laporan IN (2, 112) THEN 1 ELSE 0 END) AS odp_loss_reti,
          SUM(CASE WHEN pl.status_laporan = 11 THEN 1 ELSE 0 END) AS insert_tiang,
          SUM(CASE WHEN pl.status_laporan = 113 THEN 1 ELSE 0 END) AS onu_32';
          break;
        
        case '2':
          $select = 'SUM(CASE WHEN pl.status_laporan = 118 THEN 1 ELSE 0 END) AS segmen_tdk_sesuai,
          SUM(CASE WHEN pl.status_laporan = 81 THEN 1 ELSE 0 END) AS kendala_izin_hs,
          SUM(CASE WHEN pl.status_laporan = 56 THEN 1 ELSE 0 END) AS double_input,
          SUM(CASE WHEN pl.status_laporan = 106 THEN 1 ELSE 0 END) AS follow_up_hs,
          SUM(CASE WHEN pl.status_laporan = 102 THEN 1 ELSE 0 END) AS alamat_tdk_ditemukan,
          SUM(CASE WHEN pl.status_laporan = 23 THEN 1 ELSE 0 END) AS rukos,
          SUM(CASE WHEN pl.status_laporan = 48 THEN 1 ELSE 0 END) AS pending_hplus,
          SUM(CASE WHEN pl.status_laporan = 16 THEN 1 ELSE 0 END) AS permintaan_batal';
          break;
      }
      switch ($view) {
        case 'DATEL':
          $area = 'md.datel_HERO';
          $where = 'dt.dispatch_sto IS NOT NULL AND';
          break;
        
        case 'SEKTOR':
          $area = 'gt.title';
          $where = 'gt.title IS NOT NULL AND';
          break;
      }
      return DB::select('
        SELECT
            '.$area.' AS area,
            '.$select.'
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
        LEFT JOIN maintenance_datel md ON dt.dispatch_sto = md.sto
        WHERE
            '.$where.'
            dt.jenis_order IN ("SC","MYIR") AND
            (DATE(pl.modified_at) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
        GROUP BY '.$area.'
      ');
    }

    public static function undispatchSC($type, $orderDate)
    {
      switch ($type) {
        case 'reguler':
            return DB::select('
              SELECT
                aa.area_alamat,
                dps.*
              FROM Data_Pelanggan_Starclick dps
              LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
              LEFT JOIN area_alamat aa ON dps.sto = aa.kode_area
              WHERE
                dt.Ndem IS NULL AND
                (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime("-7 days")).'" AND "'.date('Y-m-d'). '") AND
                SUBSTRING_INDEX(dps.jenisPsb, "|", 1) IN ("AO", "PDA", "PDA LOCAL") AND dps.orderStatusId IN (1202, 51, 300)
              ORDER BY dps.orderDate DESC
            ');
          break;
        case 'backend_sc':
            return DB::select('
              SELECT
                dpsb.*
              FROM Data_Pelanggan_Starclick_Backend dpsb
              LEFT JOIN dispatch_teknisi dt ON dpsb.ORDER_CODE_INT = dt.NO_ORDER
              WHERE
                dt.Ndem IS NULL AND
                SUBSTRING_INDEX(dpsb.JENISPSB, "|", 1) IN ("AO") AND
                (DATE(dpsb.ORDER_DATE) LIKE "'.$orderDate.'%")
              ORDER BY dpsb.ORDER_DATE DESC
            ');
          break;
        case 'risma':
            return DB::select('
              SELECT
                rpw.*
              FROM risma_prov_witel rpw
              LEFT JOIN dispatch_teknisi dt ON rpw.trackID_int = dt.NO_ORDER
              LEFT JOIN psb_myir_wo pmw ON rpw.trackID_int = pmw.myir
              WHERE
                dt.Ndem IS NULL AND
                rpw.status_data = "Fresh" AND
                (pmw.sc IS NULL OR pmw.sc = 0)
              ORDER BY rpw.apply_date DESC
            ');
          break;
        case 'ex_kendala':
            return DB::select('
              SELECT
                ajp.id_api,
                ajp.response_at,
                aa.area_alamat,
                dps.sto,
                dps.orderId,
                dps.orderName,
                ajp.no_hp_pelanggan,
                ajp.status_manja_hd,
                ajp.note_manja_hd,
                dps.kcontact,
                dps.jenisPsb
              FROM api_jointer_psb ajp
              LEFT JOIN Data_Pelanggan_Starclick dps ON ajp.order = dps.orderIdInteger
              LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
              LEFT JOIN area_alamat aa ON dps.sto = aa.kode_area
              WHERE
                dt.Ndem IS NULL AND
                ajp.jenis_order = "SC" AND
                (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime("-130 days")).'" AND "'.date('Y-m-d').'")
              GROUP BY dps.orderId
              ORDER BY dps.orderDate DESC
            ');
          break;
      }
    }

    public static function needCancelEvaluasi()
    {
      return DB::select('
      SELECT
        dt.id as id_dt,
        dps.orderId,
        gt.title as sektor,
        dps.sto,
        md.datel,
        dps.jenisPsb,
        dps.orderStatus,
        dps.orderDate,
        pls.laporan_status,
        pl.kordinat_pelanggan,
        pl.modified_at
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN maintenance_datel md ON dps.sto = md.sto AND md.witel = "KALSEL"
      WHERE
        (DATE(pl.modified_at) BETWEEN "2023-01-11" AND "'.date('Y-m-d').'") AND
        SUBSTRING_INDEX(dps.jenisPsb, "|", 1) = "AO" AND
        pls.laporan_status IN ("KENDALA ALPRO") AND
        dps.orderStatus IN ("OSS PROVISIONING ISSUED", "Fallout (WFM)", "OSS FALLOUT") AND
        dt.jenis_order IN ("SC", "MYIR")
      GROUP BY dps.orderId
      ORDER BY dps.orderDate DESC
      ');
    }

    public static function psreDbsDgs($month)
    {
      $for_day = date('t', strtotime($month));
      $days_for = '';
      for ($i = 1; $i < $for_day + 1; $i ++) {
        if ($i < 10)
        {
          $keys = '0'.$i;
        } else {
          $keys = $i;
        }

        $day = date('Y-m-', strtotime($month)).''.$keys;

        $days_for .= ',SUM(CASE WHEN (DATE(skt.ORDER_DATE) = "'.$day.'") THEN 1 ELSE 0 END) as psre_d'.$keys;
      }

      return DB::select('
        SELECT
          gt.title AS area
          '.$days_for.'
          ,COUNT(*) AS jumlah
        FROM selfie_kpro_tr6 skt
        LEFT JOIN maintenance_datel md ON skt.STO = md.sto
        LEFT JOIN dispatch_teknisi dt oN skt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
        WHERE
        skt.STATUS_RESUME IN ("100 | REVOKE COMPLETED", "7 | OSS FALLOUT", "7 | PROVISIONING ISSUED", "8 | OSS - PONR", "97 | SIBEL - CANCEL COMPLETE", "98 | OSS - CANCEL COMPLETE", "CANCEL COMPLETED", "Cancel Order", "Completed (PS)", "INPROGRESS") AND
        skt.PROVIDER NOT LIKE "DCS%" AND
        (DATE(skt.ORDER_DATE) LIKE "'.$month.'%") AND
        skt.WITEL = "BANJARMASIN"
        GROUP BY gt.title
        ORDER BY jumlah DESC
      ');
    }

    public static function psreDbsDgsDetail($sektor, $month, $day)
    {
      switch ($day) {
        case 'JML':
          $periode = $month;
          break;
        
        default:
          $periode = $month.'-'.$day;
          break;
      }

      switch ($sektor) {
        case 'NON AREA':
          $sektor = 'AND gt.title IS NULL';
          break;
        case 'ALL':
          $sektor = '';
          break;
        default:
          $sektor = 'AND gt.title = "'.$sektor.'"';
          break;
      }

      return DB::select('
      SELECT
        gt.title AS sektor,
        r.uraian AS tim,
        pls.laporan_status AS status_tek,
        pl.modified_at AS tgl_laporan_tek,
        skt.*
      FROM selfie_kpro_tr6 skt
      LEFT JOIN dispatch_teknisi dt oN skt.ORDER_ID = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id AND gt.ket_posisi = "PROV"
      WHERE
        skt.STATUS_RESUME IN ("100 | REVOKE COMPLETED", "7 | OSS FALLOUT", "7 | PROVISIONING ISSUED", "8 | OSS - PONR", "97 | SIBEL - CANCEL COMPLETE", "98 | OSS - CANCEL COMPLETE", "CANCEL COMPLETED", "Cancel Order", "Completed (PS)", "INPROGRESS") AND
        skt.PROVIDER NOT LIKE "DCS%" AND
        (DATE(skt.ORDER_DATE) LIKE "'.$periode.'%") AND
        skt.WITEL = "BANJARMASIN"
        '.$sektor.'
      ORDER BY skt.ORDER_DATE DESC
      ');
    }

    public static function reportProvTodaySave($req)
    {
      $date   = $req->input('date');
      $report = $req->input('report');
      $time = date('H:i:s');

      $dtm = date('Y-m-d', strtotime($date)).' '.$time;

      $list_sto = ['bjm', 'kyg', 'bjmkyg', 'uli', 'gmb', 'uligmb', 'bbr', 'ple', 'btb', 'tki', 'plebtbtki', 'blc', 'ser', 'trj', 'blcsertrj', 'kpl', 'pgt', 'sti', 'kip', 'pgtstikip', 'tjl', 'pgn', 'amt', 'tjlpgnamt', 'mtp', 'mrb', 'mtpmrb', 'lul', 'bri', 'kdg', 'neg', 'kdgneg', 'rta', 'kdgnegrta'];

      if ($report == 'progress')
      {
        foreach ($list_sto as $sto)
        {
          $check = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->first();

          if (count($check) > 0)
          {
            DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->update([
              'tim_pt1'       => $req->input($sto.'_tim_pt1'),
              'tim_saber'     => $req->input($sto.'_tim_saber'),
              'tim_bantek'    => $req->input($sto.'_tim_bantek'),
              'tim_mo'        => $req->input($sto.'_tim_mo'),
              'order_pi'      => $req->input($sto.'_order_pi'),
              'order_risma'   => $req->input($sto.'_order_risma'),
              'order_mo'      => $req->input($sto.'_order_mo'),
              'order_pda'     => $req->input($sto.'_order_pda'),
              'order_hsi'     => $req->input($sto.'_order_hsi'),
              'order_wms'     => $req->input($sto.'_order_wms'),
              'order_prog'    => $req->input($sto.'_order_prog'),
              'order_kendala' => $req->input($sto.'_order_kendala'),
              'real_ps'       => $req->input($sto.'_real_ps'),
              'komitmen_ps'   => $req->input($sto.'_komitmen_ps'),
              'reported_date' => $date,
              'reported_time' => $time,
              'reported_dtm'  => $dtm,
              'reported_by'   => session('auth')->id_karyawan,
              'reported_name' => session('auth')->nama
            ]);
          }
          else
          {
            DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->insert([
              'sto'           => strtoupper($sto),
              'tim_pt1'       => $req->input($sto.'_tim_pt1'),
              'tim_saber'     => $req->input($sto.'_tim_saber'),
              'tim_bantek'    => $req->input($sto.'_tim_bantek'),
              'tim_mo'        => $req->input($sto.'_tim_mo'),
              'order_pi'      => $req->input($sto.'_order_pi'),
              'order_risma'   => $req->input($sto.'_order_risma'),
              'order_mo'      => $req->input($sto.'_order_mo'),
              'order_pda'     => $req->input($sto.'_order_pda'),
              'order_hsi'     => $req->input($sto.'_order_hsi'),
              'order_wms'     => $req->input($sto.'_order_wms'),
              'order_prog'    => $req->input($sto.'_order_prog'),
              'order_kendala' => $req->input($sto.'_order_kendala'),
              'real_ps'       => $req->input($sto.'_real_ps'),
              'komitmen_ps'   => $req->input($sto.'_komitmen_ps'),
              'reported_date' => $date,
              'reported_time' => $time,
              'reported_dtm'  => $dtm,
              'reported_by'   => session('auth')->id_karyawan,
              'reported_name' => session('auth')->nama
            ]);
          }
        }
      }
      elseif ($report == 'cuaca')
      {
        foreach ($list_sto as $sto)
        {
          $check = DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->first();

          if (count($check) > 0)
          {
            DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->update([
              'cuaca_pagi'    => $req->input($sto.'_cuaca_pagi'),
              'cuaca_siang'   => $req->input($sto.'_cuaca_siang'),
              'cuaca_sore'    => $req->input($sto.'_cuaca_sore'),
              'nte_material'  => $req->input($sto.'_nte_material'),
              'kendala_1'     => $req->input($sto.'_kendala_1'),
              'action_1'      => $req->input($sto.'_action_1'),
              'action_2'      => $req->input($sto.'_action_2'),
              'reported_date' => $date,
              'reported_time' => $time,
              'reported_dtm'  => $dtm,
              'reported_by'   => session('auth')->id_karyawan,
              'reported_name' => session('auth')->nama
            ]);
          }
          else
          {
            DB::table('daily_report_progress_cuaca_kalsel')->where('sto', strtoupper($sto))->whereDate('reported_date', $date)->insert([
              'sto'           => strtoupper($sto),
              'cuaca_pagi'    => $req->input($sto.'_cuaca_pagi'),
              'cuaca_siang'   => $req->input($sto.'_cuaca_siang'),
              'cuaca_sore'    => $req->input($sto.'_cuaca_sore'),
              'nte_material'  => $req->input($sto.'_nte_material'),
              'kendala_1'     => $req->input($sto.'_kendala_1'),
              'action_1'      => $req->input($sto.'_action_1'),
              'action_2'      => $req->input($sto.'_action_2'),
              'reported_date' => $date,
              'reported_time' => $time,
              'reported_dtm'  => $dtm,
              'reported_by'   => session('auth')->id_karyawan,
              'reported_name' => session('auth')->nama
            ]);
          }
        }
      }
    }

}

?>
