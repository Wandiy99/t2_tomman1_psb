<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

class leaderboardsModel
{
  public static function get_squads($squads,$date,$dateend,$mitra){
    if ($mitra == "ALL"){
      $where_mitra = '';
    } else {
      $where_mitra = ' b.mitra like "%'.$mitra.'%" AND ';
    }
    $query = DB::select('
      SELECT *,(jumlah_asis_UP+jumlah_prov_UP+jumlah_addon_UP+jumlah_gangguan_UP_GPON+jumlah_gangguan_UP_Copper+jumlah_gangguan_UP_DES + maintenance) as rank from (SELECT
      a.*,
      b.mitra,
      SUM(CASE WHEN g.no_tiket <> "" THEN 1 ELSE 0 END) as JUMLAH_GGN,
      SUM(CASE WHEN (g.no_tiket <> "" AND  f.status_laporan = 1) THEN 1 ELSE 0 END) as JUMLAH_GGN_UP,
      SUM(CASE WHEN (dm.ND<>"" AND f.status_laporan = 1) THEN 1 ELSE 0 END)*5.33 as jumlah_asis_UP,
      SUM(CASE WHEN (e.orderId<>"" AND f.status_laporan = 1 AND (e.jenisPsb LIKE "AO|%" OR e.jenisPsb LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END)*5.3 as jumlah_prov_UP,
      SUM(CASE WHEN (e.orderId<>"" AND f.status_laporan = 1 AND  (e.jenisPsb NOT LIKE "AO|%" AND e.jenisPsb NOT LIKE "%MIGRATE%") ) THEN 1 ELSE 0 END)*2.4 as jumlah_addon_UP,
      SUM(CASE WHEN (g.no_tiket<>"" AND g.loker_ta <> 2 AND f.status_laporan = 1 AND (g.alpro LIKE "%GPO%" OR g.alpro LIKE "%FIBER%")) THEN 1 ELSE 0 END)*2.4 as jumlah_gangguan_UP_GPON,
      SUM(CASE WHEN (g.no_tiket<>"" AND g.loker_ta <> 2 AND f.status_laporan = 1 AND (g.alpro LIKE "%COOPPE%" OR g.alpro LIKE "%MSAN%")) THEN 1 ELSE 0 END)*1.2 as jumlah_gangguan_UP_Copper,
      SUM(CASE WHEN (g.no_tiket<>"" AND g.loker_ta = 2 AND f.status_laporan = 1 ) THEN 1 ELSE 0 END)*4.2 as jumlah_gangguan_UP_DES,
      (select count(*) FROM
          maintenance_regu mr
          LEFT JOIN maintaince m on mr.id=m.dispatch_regu_id
          LEFT JOIN psb_laporan pl on m.psb_laporan_id = pl.id
        WHERE
          (mr.nik1 = a.nik OR mr.nik2=a.nik) and
          m.jenis_order= 3
          and pl.status_laporan=1
          and (date(m.created_at) between  "'.$date.'" AND "'.$dateend.'")) * 4 as maintenance
      FROM
        1_2_employee a
        LEFT JOIN regu b ON b.nik1 = a.nik OR b.nik2 = a.nik
        LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
        LEFT JOIN dispatch_teknisi d ON b.id_regu = d.id_regu
        LEFT JOIN Data_Pelanggan_Starclick e ON d.Ndem = e.orderId
        LEFT JOIN psb_laporan f ON d.id = f.id_tbl_mj
        LEFT JOIN roc g ON d.Ndem = g.no_tiket
        LEFT JOIN dossier_master dm ON d.Ndem = dm.ND
        LEFT JOIN employee_squad es ON a.squad = es.squad_id
      WHERE
        c.unit = 1 AND
        es.squad = "'.$squads.'" AND
        '.$where_mitra.'
        (date(d.tgl) BETWEEN "'.$date.'" AND "'.$dateend.'")
      GROUP BY a.nik)
       x ORDER BY rank DESC
    ');
    return $query;
  }

  public static function getData($squad){
    return DB::table('leaderboards')->where('squad',$squad)->first();
  }

  public static function hapusData($squad){
    return DB::table('leaderboards')->where('squad',$squad)->delete();
  }

  public static function simpanData($squads, $squadTim){
      return DB::table('leaderboards')->insert([
           'nik'            => $squads->nik,
           'laborcode'      => $squads->laborcode,
           'warrior'        => $squads->nama,
           'mitra'          => $squads->mitra,
           'jumlah_ggn'     => $squads->JUMLAH_GGN,
           'jumlah_ggn_up'  => $squads->JUMLAH_GGN_UP,
           'a_d'            => $squads->jumlah_gangguan_UP_DES,
           'a_c'            => $squads->jumlah_gangguan_UP_Copper,
           'a_f'            => $squads->jumlah_gangguan_UP_GPON,
           'p_a'            => $squads->jumlah_addon_UP, 
           'p_1'            => $squads->jumlah_prov_UP,
           'm'              => $squads->maintenance,
           'mig'            => $squads->jumlah_asis_UP,
           'total'          => $squads->rank,
           'squad'          => $squadTim
         ]);
  }

  public static function getAllData($squad){
    return DB::table('leaderboards')->where('squad',$squad)->get();
  }

}
