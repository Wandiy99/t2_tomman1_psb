<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class BAModel
{

  const TABLE = 'ba_controller_tr6';

  public static function getAll(){
    return DB::table(self::TABLE)->orderBy('id', 'desc')->get();
  }
  public static function getAllRequest(){
    return DB::table(self::TABLE)->whereIn('request', [2,3])->orderBy('id', 'desc')->get();
  }
  public static function getById($id){
    return DB::table(self::TABLE)->where('id', $id)->first();
  }
  public static function getByOrderNo($order){
    return DB::table(self::TABLE)->where('sc_id', $order)->first();
  }
  public static function insert($query){
    DB::table(self::TABLE)->insert($query);
  }
  public static function update($id, $query){
    DB::table(self::TABLE)->where('id', $id)->update($query);
  }

}
