<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tool extends Model
{
    public function getProject()
    {
    	return DB::select('SELECT project as id, project as text FROM alista_material_keluar GROUP BY project ORDER BY project');
    }

    public function getGudang()
    {
    	return DB::select('SELECT nama_gudang as id, nama_gudang as text FROM alista_gudang GROUP BY nama_gudang ORDER BY nama_gudang');
    }

    public function getMitra()
    {
    	return DB::select('SELECT mitra as id, mitra as text FROM alista_material_keluar GROUP BY mitra ORDER BY mitra');
    }

    public function getMaterialKeluar($project, $gudang, $mitra, $tglKeluar)
    {
    	return DB::table('alista_material_keluar')
    		->where('project', $project)
    		->where('nama_gudang', $gudang)
    		->where('mitra', $mitra)
    		->where('tgl', 'LIKE', '%'.$tglKeluar.'%')
    		->get();
    }

    public function getListOrderTarikan($bulan)
    {
        $query = DB::SELECT('
            SELECT
            *,
            dt.id as id_dt,
            SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll
          FROM
           regu r
           LEFT JOIN  dispatch_teknisi dt ON dt.id_regu = r.id_regu
           LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
           LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
           LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
           LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
           LEFT JOIN rfc_sal i ON plm.id_item_bantu = i.id_item_bantu
           LEFT JOIN Data_Pelanggan_Starclick dps ON dt.Ndem = dps.orderId
           LEFT JOIN mdf m ON dps.sto = m.mdf
         WHERE
           dps.orderId <> "" AND
           dt.tgl like "%'.$bulan.'%" AND
           pl.status_laporan = 1
           GROUP BY dt.id
           HAVING dc_roll > 150
        ');
         return $query;
    }

    public static function getListOrderTarikanGangguan($bulan)
    {
        $query = DB::SELECT('
            SELECT
            *,
            dt.id as id_dt,
            DATE_FORMAT(dt.tgl,"%Y-%m-%d") as tanggal_dispatch,
            DATE_FORMAT(pl.modified_at,"%Y-%m-%d") as modified_at,
            DATE_FORMAT(roc.tgl_open,"%Y-%m-%d") as orderDatePs,    
            SUM( i.num * plm.qty ) AS dc_preconn,
            SUM(case when plm.id_item = "AC-OF-SM-1B" then plm.qty else 0 end) as dc_roll
          FROM
           regu r
           LEFT JOIN  dispatch_teknisi dt ON dt.id_regu = r.id_regu
           LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
           LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
           LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
           LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
           LEFT JOIN rfc_sal i ON plm.id_item_bantu = i.id_item_bantu
          LEFT JOIN roc ON dt.Ndem = roc.no_tiket
           LEFT JOIN mdf m ON roc.sto = m.mdf
         WHERE
           roc.no_tiket <> "" AND
           dt.tgl like "%'.$bulan.'%" AND
           pl.status_laporan = 1
           GROUP BY dt.id
           HAVING dc_roll > 150
        ');
         return $query;
    }

  public static function getAbsenTekProv($date){
    $auth = session('auth');
    if ($auth->level==2){
        $sektor = 'g.sektor LIKE "%PROV%"';
    }
    else if($auth->level==46){
        $sektor = 'g.TL_NIK = "'.$auth->id_user.'" AND g.sektor LIKE "%PROV%"';
    };

    $sql = '
            select 
              r.*,
              g.title, 
              g.sektor, 
              g.TL_NIK,
              (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik1 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik1_absen,
              (SELECT abs1.absenEsok from absen abs1 where abs1.nik = r.nik1 AND date(abs1.tglAbsen)="'.$date.'%" AND abs1.absenEsok=1 group by date(abs1.tglAbsen)) as nik1_absenEsok,
              (SELECT abs1.approval from absen abs1 where abs1.nik = r.nik2 AND date(abs1.date_created)="'.$date.'%" group by date(abs1.date_created)) as nik2_absen,
              (SELECT abs1.absenEsok from absen abs1 where abs1.nik = r.nik2 AND date(abs1.tglAbsen)="'.$date.'%" AND abs1.absenEsok=1 group by date(abs1.tglAbsen)) as nik2_absenEsok
            FROM  
              regu r
              left join group_telegram g ON r.mainsector=g.chat_id
            where
              r.active=1 AND
              '.$sektor;

    return DB::select($sql);
  }

  public static function getListStokMaterial(){
      return DB::table('alista_stok')->get();
  }

   public static function getRekapRfc($bulan){
      $sql = 'SELECT 
                a.rfc, 
                SUM(a.jumlah) as outGudang, 
                SUM(a.jmlTerpakai) as terpakai, 
                SUM(a.jmlKembali) as kembali,
                b.regu, 
                c.nik1, 
                c.nik2
              FROM 
                rfc_input b
                LEFT JOIN rfc_sal a ON b.no_rfc=a.rfc
                LEFT JOIN regu c ON (b.created_by=c.nik1 OR b.created_by=c.nik2)
              where 
                a.rfc LIKE "%'.$bulan.'%" AND 
                c.ACTIVE = 1
              GROUP BY a.rfc';

      // $sql = 'SELECT *,  
      //           SUM(CASE WHEN action=1 THEN abs(value) ELSE 0 END) as outGudang, 
      //           SUM(CASE WHEN action=2 THEN abs(value) ELSE 0 END) as terpakai, 
      //           SUM(CASE WHEN action=3 THEN abs(value) ELSE 0 END) as kembali
      //         FROM 
      //           maintenance_saldo_rfc
      //         where 
      //           rfc LIKE "%'.$bulan.'%" AND 
      //           transaksi=1
      //         GROUP BY rfc';


      $data = DB::select($sql);

      return $data;
   }

   public static function getByRfc($rfc){
      return DB::table('rfc_sal')->where('rfc',$rfc)->get();
   }

   public static function getRekapRfcDetail($bulan){
      $sql = 'SELECT 
                a.rfc, 
                a.id_item,
                a.nama_item,
                a.jumlah,
                a.jmlTerpakai,
                a.jmlKembali,
                b.regu, 
                c.mainsector,
                c.nik1, 
                c.nik2,
                d.title
              FROM 
                rfc_input b
                LEFT JOIN rfc_sal a ON b.no_rfc=a.rfc
                LEFT JOIN regu c ON (b.created_by=c.nik1 OR b.created_by=c.nik2)
                LEFT JOIN group_telegram d ON c.mainsector=d.chat_id
              where 
                a.rfc LIKE "%'.$bulan.'%" AND 
                c.ACTIVE = 1
              ORDER BY d.title, a.rfc';

      $data = DB::select($sql);

      return $data;
   }

   public static function getBaDigitalBySc($sc){
      return DB::table('ba_online')->where('no_wo','LIKE','%'.$sc.'%')->get();
   }

   public static function getWoInRfc($Rfc){
      $sql = 'SELECT 
              a.no_rfc, e.Ndem
              FROM 
              rfc_input a
              LEFT JOIN rfc_sal b ON a.alista_id = b.alista_id
              LEFT JOIN psb_laporan_material c ON b.id_item_bantu = c.id_item_bantu
              LEFT JOIN psb_laporan d ON c.psb_laporan_id = d.id
              LEFT JOIN dispatch_teknisi e ON d.id_tbl_mj = e.id
              WHERE
              a.no_rfc = "'.$rfc.'" AND c.type=1
              GROUP BY e.Ndem';

      return DB::select($sql);
   }

  public static function getWoInRfcDetail($rfc, $id_item){
      $sql = 'SELECT 
              a.no_rfc, e.Ndem, c.id_item, c.qty
              FROM 
              rfc_input a
              LEFT JOIN rfc_sal b ON a.alista_id = b.alista_id
              LEFT JOIN psb_laporan_material c ON b.id_item_bantu = c.id_item_bantu
              LEFT JOIN psb_laporan d ON c.psb_laporan_id = d.id
              LEFT JOIN dispatch_teknisi e ON d.id_tbl_mj = e.id
              WHERE
              c.type=1 AND
              a.no_rfc = "'.$rfc.'" AND
              c.id_item = "'.$id_item.'"';

      return DB::select($sql);
  }

}
