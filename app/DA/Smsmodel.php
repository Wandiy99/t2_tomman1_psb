<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class Smsmodel
{
	public function savecontact($req, $nomor){
		
		return DB::table('contact_sms')->insert([
			'nama' => $req->input('nama'),
			'telp_2' => $req->input('telp'),
			'telp' => $nomor,
			'A_S' => $req->input('alamatS')
		]);
	}

	public function savegroup($req){
		return DB::table('group_sms')->insert([
			'nama_G' => $req->input('namag'),
			'Ket' => $req->input('ketg')
		]);
	}
	public function listcontact_n(){
		return DB::table('contact_sms')->get();
	}

	public function listcontact(){
		return DB::table('contact_sms')->paginate(10);
	}

	public function simpanGroupC($req, $kontak)
	{
		return DB::table('group_contact_sms')->insert([
			'id_C' => $kontak,
			'Grup' => $req->namag
		]);
	}

	public function listgroup(){
		return DB::table('group_sms')->get();
	}

	public function countcont(){
		return DB::table('group_sms')
		->leftjoin('group_contact_sms', 'group_sms.nama_G', '=', 'group_contact_sms.Grup')
		->leftjoin('contact_sms', 'contact_sms.id', '=', 'group_contact_sms.id_C')->get();
	}

// public function livesearch_nama($to){
// 	return DB::table('contact_sms')->select('nama')->where('nama', 'like', '%'. $to .'%')->orderBy('nama', 'desc')->distinct->get();
// }

	public function send3006single($req, $pengirim){
		return DB::connection('mysql2')->table('outbox')->insertGetId([
			'DestinationNumber' => $pengirim,
			'TextDecoded' => $req->pesan,
			'MultiPart' => 'true',
			'CreatorID' => '3006'
		]);
	}

	public function send3006multi($req, $udh, $AI, $i){
		return DB::connection('mysql2')->table('outbox_multipart')->insertGetId([
			'UDH' => $udh,
			'TextDecoded' => $req->pesan,
			'ID' => $AI,
			'SequencePosition' => $i,
		]);
	}

	public function AI(){
		return DB::connection('mysql2')->select('SHOW TABLE STATUS LIKE "outbox"');
	}

	public function inboxlist(){
		return DB::table('t1_sms.inbox')->LeftJoin('t1.contact_sms', 't1_sms.inbox.SenderNumber', '=', 't1.contact_sms.telp')->orderBy('inbox.UpdatedInDB', 'DESC')->paginate(10);

// $database = ('SELECT * FROM t1_sms.inbox om left join t1.contact_sms cs on t1.cs.telp=t1_sms.om.SenderNumber ORDER BY UpdatedInDB DESC');
// return DB::select($database);
	}

	public function outboxlist(){
		return DB::table('t1_sms.outbox')->LeftJoin('t1.contact_sms', 't1_sms.outbox.DestinationNumber', '=', 't1.contact_sms.telp_2')->orderBy('outbox.UpdatedInDB', 'DESC')->paginate(10);
	}

	public function sentitemslist(){
		return DB::table('t1_sms.sentitems')->LeftJoin('t1.contact_sms', 't1_sms.sentitems.DestinationNumber', '=', 't1.contact_sms.telp_2')->orderBy('sentitems.UpdatedInDB', 'DESC')->paginate(10);
	}

	public function detail_I($ID){
		return DB::table('t1_sms.inbox')->LeftJoin('t1.contact_sms', 't1_sms.inbox.SenderNumber', '=', 't1.contact_sms.telp_2')->where('t1_sms.inbox.ID', '=', $ID)->first();
	}

	public function detail_O($ID){
		return DB::table('t1_sms.outbox')->LeftJoin('t1.contact_sms', 't1_sms.outbox.DestinationNumber', '=', 't1.contact_sms.telp_2')->where('t1_sms.outbox.ID', '=', $ID)->first();
	}

	public function detail_S($ID){
		return DB::table('t1_sms.sentitems')->LeftJoin('t1.contact_sms', 't1_sms.sentitems.DestinationNumber', '=', 't1.contact_sms.telp_2')->where('t1_sms.sentitems.ID', '=', $ID)->first();
	}

	public function delete($id)
	{
		return DB::table('contact_sms')->where('id','=',$id)->delete();
	}

	public function edit($id){
		return DB::table('contact_sms')->where('id', '=', $id)->first();
	}

	public function update_62($req, $id, $nomor){
		return DB::table('contact_sms')->where('id', '=', $id)->update([
			'nama' => $req->input('nama'),
			'telp_2' => $req->input('telp'),
			'telp' => $nomor,
			'A_S' => $req->input('alamatS')
		]);
	}

	public function update_0($req, $id, $nomor){
		return DB::table('contact_sms')->where('id', '=', $id)->update([
			'nama' => $req->input('nama'),
			'telp' => $req->input('telp'),
			'telp_2' => $nomor,
			'A_S' => $req->input('alamatS')
		]);
	}

	public function listPesanMasuk($tgl)
	{
		return DB::table('t1_sms.inbox')
			->leftJoin('t1.Data_Pelanggan_Starclick','t1_sms.inbox.SenderNumber','=','t1.Data_Pelanggan_Starclick.noTelp')
			->select('t1_sms.inbox.ReceivingDateTime', 't1.Data_Pelanggan_Starclick.orderName', 't1.Data_Pelanggan_Starclick.orderId', 't1_sms.inbox.TextDecoded', 't1.Data_Pelanggan_Starclick.noTelp', 't1_sms.inbox.SenderNumber')
			->where('t1_sms.inbox.ReceivingDateTime','LIKE','%'.$tgl.'%')
			->orderBy('t1_sms.inbox.ReceivingDateTime','DESC')
			->get();
	}

	public function getNoTelpBySc($sc)
	{
		return DB::table('Data_Pelanggan_Starclick')
			->whereIn('orderId',$sc)
			->get();
	}

	public function emptysearch(){
		return DB::table('contact_sms')->get();
	}

	public function value_search($search){
		 return DB::table('contact_sms')->where('nama', 'like', '%' .$search. '%')->limit(5)->get();
	}
}
