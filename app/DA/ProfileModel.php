<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class ProfileModel
{
    public static function index()
    {
        return DB::select('
        SELECT
            emp.nama,
            emp.nik,
            emp.jk,
            ma.mitra_amija,
            ma.mitra_amija_pt,
            emp.id_regu,
            emp.sto,
            u.password,
            emp.noktp,
            emp.email,
            emp.tgl_lahir,
            emp.tempat_lahir,
            emp.tinggi_badan,
            emp.berat_badan,
            emp.alamat,
            emp.no_telp,
            emp.no_wa,
            emp.laborcode,
            emp.pt1,
            emp.saber,
            emp.nte,
            emp.mytech
        FROM 1_2_employee emp
        LEFT JOIN user u ON emp.nik = u.id_karyawan
        LEFT JOIN mitra_amija ma ON emp.mitra_amija = ma.mitra_amija AND ma.witel = "'.session('auth')->nama_witel.'"
        WHERE
            emp.nik = "'.session('auth')->id_karyawan.'"
        ');
    }

    public static function profileSave($req)
    {
        if (!empty($req->input('id_regu')))
        {
            $get_sektor = DB::table('regu')->where('id_regu', $req->input('id_regu'))->select('mainsector')->first();
            $id_regu = $req->input('id_regu');
            $mainsector = $get_sektor->mainsector;
        } else {
            $id_regu = 0;
            $mainsector = null;
        }

        $pt1 = $saber = $nte = $mytech = 0;
		if (@$req->input('skillset')['pt1'] == 1)
		{
			$pt1 = 1;
		}
		if (@$req->input('skillset')['saber'] == 1)
        {
			$saber = 1;
		}
		if (@$req->input('skillset')['nte'] == 1)
        {
			$nte = 1;
		}
		if (@$req->input('skillset')['mytech'] == 1)
        {
			$mytech = 1;
		}

        DB::table('1_2_employee')
        ->where('nik', $req->input('nik'))
        ->update([
            'jk' => $req->input('jk'),
            'mitra_amija' => $req->input('mitra_amija'),
            'id_regu' => $id_regu,
            'mainsector' => $mainsector,
            'sto' => $req->input('sto'),
            'noktp' => $req->input('noktp'),
            'email' => $req->input('email'),
            'tgl_lahir' => $req->input('tgl_lahir'),
            'tempat_lahir' => $req->input('tempat_lahir'),
            'tinggi_badan' => $req->input('tinggi_badan'),
            'berat_badan' => $req->input('berat_badan'),
            'alamat' => $req->input('alamat'),
            'no_telp' => $req->input('no_telp'),
            'no_wa' => $req->input('no_wa'),
            'laborcode' => $req->input('laborcode'),
            'pt1' => $pt1,
            'saber' => $saber,
            'nte' => $nte,
            'mytech' => $mytech
        ]);

        if ($req->input('password') != null || $req->input('password') != '')
        DB::table('user')
        ->where('id_karyawan', $req->input('nik'))
        ->update([
            'password' => MD5($req->input('password'))
        ]);

        if ($req->hasFile('img_profile'))
        {
            $name = 'profile';
            $path = public_path().'/upload4/profile/'.$req->input('nik').'/';
            if (!file_exists($path))
            {
                if (!mkdir($path, 0775, true))
                return 'gagal menyiapkan folder foto profile';
            }
            $file = $req->file('img_profile');
            $ext = 'jpg';
            try {
                $moved = $file->move("$path", "$name.$ext");

                $img = new \Imagick($moved->getRealPath());

                $img->scaleImage(100, 150, true);
                $img->writeImage("$path/$name-th.$ext");
            }
            catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return 'gagal menyimpan foto profile '.$name;
            }
        }
    }
}