<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class ApiModel
{
    public static function detailMessageWhatsapp($response, $date, $id_sender)
    {
      switch ($response) {
        case 'SUCCESS':
          $where_response = 'response = "Successfully Sent" AND';
          break;
        
        case 'FAILED':
          $where_response = 'response = "The Number in Not Registered WhatsApp" AND';
          break;
      }

        return DB::SELECT('
          SELECT
            *
          FROM whatsapp_api_log
          WHERE
            '.$where_response.'
            (DATE(send_at) LIKE "'.$date.'%") AND
            divisi = "'.$id_sender.'"
        ');
    }

    public static function raportMessageWhatsapp()
    {
      $days_for = '';
      for ($i = 1; $i < date('t') + 1; $i ++) {
        if ($i < 10)
        {
          $keys = '0'.$i;
        } else {
          $keys = $i;
        }

        $day = date('Y-m-').''.$keys;

        $days_for .= ',SUM(CASE WHEN (DATE(send_at) = "'.$day.'") AND response = "Successfully Sent" THEN 1 ELSE 0 END) as send_s_d'.$keys.',
        SUM(CASE WHEN (DATE(send_at) = "'.$day.'") AND response = "The Number in Not Registered WhatsApp" THEN 1 ELSE 0 END) as send_f_d'.$keys.'';
      }

      return DB::select('
        SELECT
          divisi as id_sender
          '.$days_for.'
          ,SUM(CASE WHEN (DATE(send_at) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'") AND response = "Successfully Sent" THEN 1 ELSE 0 END) as total_send_s,
          SUM(CASE WHEN (DATE(send_at) BETWEEN "'.date('Y-m-01').'" AND "'.date('Y-m-d').'") AND response = "The Number in Not Registered WhatsApp" THEN 1 ELSE 0 END) as total_send_f
        FROM whatsapp_api_log
        GROUP BY divisi
      ');
    }

    public static function sendWhatsapp($sender, $number, $msg, $file, $kat)
    {

    // dd($sender, $number, $msg, $file, $kat);
    
        if ($kat == "message") {
    
          $curl = curl_init();
    
          curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://10.131.40.214:6969/send-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'sender='.$sender.'&number='.$number.'&message='.$msg.'',
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
    
          $result = curl_exec($curl);
          
        } elseif ($kat == "media") {
    
          $curl = curl_init();
    
          curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://10.131.40.214:6969/send-media',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'sender='.$sender.'&number='.$number.'&caption='.$msg.'&file='.$file.'',
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
    
          $result = curl_exec($curl);
    
        }
    
        if(curl_errno($curl))
        {
            print_r('Curl error: ' . curl_error($curl));
        }
        else
        {
            print_r('Successfully Sent '.$number.'');
        }
    
        curl_close($curl);
    
        $hasil = json_decode($result);

        if ($hasil != null)
        {
          // input log
          if ($hasil->status == false) {
            $response = "The Number in Not Registered WhatsApp";
          } elseif ($hasil->status == true) {
            $response = "Successfully Sent";
          } else {
            $response = "Undefined";
          }
          // dd($response);

          if (session('auth') != null) {
            $send_by = session('auth')->id_karyawan;
            $send_by_name = session('auth')->nama;
          } else {
            $send_by = '20981020';
            $send_by_name = 'BOT - MAHDIAN';
          }

          DB::table('whatsapp_api_log')->insert([
            'phone_numbers'   => $number,
            'type'            => $kat,
            'message'         => $msg,
            'url_file'        => $file,
            'response'        => $response,
            'divisi'          => $sender,
            'send_by'         => $send_by,
            'send_by_name'    => $send_by_name,
            'send_at'         => DB::raw('NOW()')
          ]);

          return $hasil;
        }
    }

    public static function profileTeknisi($ket,$posisi)
    {  

      if ($ket == 'data')
      {
        if ($posisi == 'PROV')
        {
          $select = '
          u.id_user as id_teknisi,
          emp.nama,
          emp.id_sobi,
          u.id_user as id_mytech,
          u.id_user as id_scmt,
          r.id_regu,
          r.uraian as nama_regu,
          gt.title as sektor,
          gt.ket_posisi as ket_sektor,
          emp.tgl_lahir,
          emp.tempat_lahir,
          emp.alamat,
          emp.no_telp,
          emp.chat_id,
          UPPER(ma.mitra_amija_pt) as nama_mitra,
          r.mitra as ket_mitra,
          ma.kat as kat_mitra,
          emp.Witel_New as witel,
          r.sto
          ';
          $ket_posisi = 'emp.ACTIVE IN (1,2) AND emp.pt1 = 1';
          $left_join = '';
        } elseif ($posisi == 'IOAN') {
          $select = '
          u.id_user as id_teknisi,
          emp.nama,
          emp.id_sobi,
          u.id_user as id_mytech,
          u.id_user as id_scmt,
          r.id_regu,
          r.uraian as nama_regu,
          gt.title as sektor,
          gt.ket_posisi as ket_sektor,
          emp.tgl_lahir,
          emp.tempat_lahir,
          emp.alamat,
          emp.no_telp,
          emp.chat_id,
          ma.mitra_amija_pt as nama_mitra,
          r.mitra as ket_mitra,
          ma.kat as kat_mitra,
          emp.Witel_New as witel,
          r.sto
          ';
          $ket_posisi = 'gt.ket_posisi = "IOAN" AND r.ACTIVE = 1';
          $left_join = '';
        }
      } elseif ($ket == 'absensi') {
        if ($posisi == 'PROV')
        {
          $select = '
          u.id_user as id_teknisi,
          emp.nama,
          emp.id_sobi,
          u.id_user as id_mytech,
          u.id_user as id_scmt,
          r.id_regu,
          r.uraian as nama_regu,
          gt.title as sektor,
          gt.ket_posisi as ket_sektor,
          emp.tgl_lahir,
          emp.tempat_lahir,
          emp.alamat,
          emp.no_telp,
          emp.chat_id,
          UPPER(ma.mitra_amija_pt) as nama_mitra,
          r.mitra as ket_mitra,
          ma.kat as kat_mitra,
          emp.Witel_New as witel,
          r.sto,
          ab.tglAbsen,
          ab.date_approval as tglApproveAbsen
          ';
          $ket_posisi = 'emp.ACTIVE IN (1,2) AND emp.pt1 = 1 AND DATE(ab.tglAbsen) = "'.date('Y-m-d').'"';
          $left_join = '';
        } elseif ($posisi == 'IOAN') {
          $select = '
          u.id_user as id_teknisi,
          emp.nama,
          emp.id_sobi,
          u.id_user as id_mytech,
          u.id_user as id_scmt,
          r.id_regu,
          r.uraian as nama_regu,
          gt.title as sektor,
          gt.ket_posisi as ket_sektor,
          emp.tgl_lahir,
          emp.tempat_lahir,
          emp.alamat,
          emp.no_telp,
          emp.chat_id,
          ma.mitra_amija_pt as nama_mitra,
          r.mitra as ket_mitra,
          ma.kat as kat_mitra,
          emp.Witel_New as witel,
          r.sto,
          ab.tglAbsen,
          ab.date_approval as tglApproveAbsen
          ';
          $ket_posisi = 'gt.ket_posisi = "IOAN" AND r.ACTIVE = 1 AND DATE(ab.tglAbsen) = "'.date('Y-m-d').'"';
          $left_join = '';
        }

      }

      return DB::SELECT('
      SELECT
        '.$select.'
      FROM user u
      LEFT JOIN absen ab ON u.id_user = ab.nik
      LEFT JOIN 1_2_employee emp ON u.id_user = emp.nik
      LEFT JOIN regu r ON emp.id_regu = r.id_regu
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      '.$left_join.'
      WHERE
        u.level = 10 AND
        emp.Witel_New = "KALSEL" AND
        '.$ket_posisi.'
      GROUP BY u.id_user
      ORDER BY r.date_created DESC
      ');
    }

    public static function fotoQCBorneo($id)
    {
      return DB::table('Data_Pelanggan_Starclick')->leftJoin('dispatch_teknisi','Data_Pelanggan_Starclick.orderIdInteger','=','dispatch_teknisi.NO_ORDER')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.NO_ORDER',$id)->select('psb_laporan.kordinat_pelanggan','Data_Pelanggan_Starclick.witel')->first();

      // return DB::table('dispatch_teknisi')->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')->where('dispatch_teknisi.NO_ORDER',$id)->select('psb_laporan.kordinat_pelanggan')->first();
    }

    public static function dataReportDalapa($order_id)
    {
      return DB::SELECT('
        SELECT
        dt.id as id_dt,
        dt.Ndem,
        dps.orderStatus,
        pls.laporan_status,
        r.uraian,
        r.nik1,
        r.nik2,
        gt.title as sektor,
        gt.TL,
        gt.TL_Username,
        dct.*
        FROM dispatch_teknisi dt
        LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
        LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.myir
        LEFT JOIN dalapa_comparin_tr6 dct ON dt.NO_ORDER = dct.dalapa_order_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        dt.Ndem = "'.$order_id.'"
      ');
    }

    public static function orderPickupOnline($type, $id)
    {
      switch ($type) {
        case 'ORDER':
            $where = 'dps.orderStatusId <> 1500 AND';
          break;
        
        case 'DORONG':
            $where = '';
          break;
      }
      return DB::select('
        SELECT
          dps.orderId AS sc_id,
          dps.orderDate AS order_date,
          dps.orderDatePs AS ps_date,
          dps.orderNo AS no_order,
          dps.orderIdNcx AS id_ncx,
          dps.orderNcli AS ncli,
          dps.orderName AS nama_pelanggan,
          dps.orderAddr AS alamat_pelanggan,
          dps.internet AS no_inet,
          dps.noTelp AS no_telp,
          dps.alproname AS alpro,
          dps.witel,
          dps.kcontact,
          "OSS PROVISIONING ISSUED" AS status_sc,
          dps.username AS user_id,
          dps.jenisPsb AS layanan,
          SUBSTRING_INDEX(dps.jenisPsb, "|", 1) AS jenis_transaksi,
          dps.provider AS unit,
          dps.sto AS id_sto,
          dps.lat AS latitude,
          dps.lon AS longitude
        FROM Data_Pelanggan_Starclick dps
        WHERE
          '.$where.'
          SUBSTRING_INDEX(dps.jenisPsb, "|", 1) IN ("AO", "MO", "PDA", "PDA LOCAL") AND
          dps.orderIdInteger = "'.$id.'"
      ');
    }

    public static function uploadPickupOnline($id)
    {
      $data = self::orderPickupOnline('ORDER', $id);
      
      if (count($data) > 0)
      {
        $postfields['source'] = 'nonbackend';
        $postfields['data'] = $data;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://tacticalpro.co.id/api/workorder/creates',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($postfields),
          CURLOPT_HTTPHEADER => array(
            'apisecret: bnB3WWhYSmE4ZmQvcnh6dnd6QnV1dz09',
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if (json_decode($response) != null)
        {
          $result = json_decode($response);

          DB::table('log_tacticalpro_tr6')->insert([
            'order_id'      => $id,
            'code'          => $result->code,
            'message'       => $result->message,
            'created_by'    => session('auth')->id_karyawan ?? 20981020
          ]);
  
          print_r($result->message);
  
          return $response;
        }
      }
      else
      {
        print_r("Order Not Found :(");
      }
    }

    public static function checkServiceValins($id)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://10.62.165.36/valins/home.php?page=PG'.date('ymd').'00179',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'value=' . $id . '&search=true',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/x-www-form-urlencoded',
        'Cookie: PHPSESSID=CFBD00878F57B744825E86553C946143'
        ),
      ));

      $response = curl_exec($curl);
      curl_close($curl);

      $dom = @\DOMDocument::loadHTML(trim($response));
      $table = $dom->getElementsByTagName('table')->item(1);
      
      if ($table <> null)
      {
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
          0 => 'service', 'valins_id', 'type_valins', 'time', 'summary_odp', 'panel', 'port_odp', 'ip_olt', 'slot', 'port'
        );
        $result = array();

        for ($i = 1, $count = $rows->length; $i < $count; $i++) {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++) {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
        }

        return $result;
      } else {
        dd("Valins Gangguan!");
      }
    }

    public static function orderPs3Month()
    {
      return DB::select('
        SELECT
          kpt.SPEEDY as no_inet,
          kpt.CUSTOMER_NAME as customer_name,
          kpt.NO_HP as numb_hp
        FROM kpro_tr6 kpt
        WHERE
          kpt.STATUS_RESUME = "Completed (PS)" AND
          kpt.JENIS_PSB = "AO" AND
          kpt.TYPE_TRANSAKSI = "NEW SALES" AND
          kpt.TYPE_LAYANAN IN ("2P", "3P") AND
          kpt.PRODUCT = "INDIHOME" AND
          kpt.NPER IN ("'.date('Ym', strtotime("-2 month")).'", "'.date('Ym', strtotime("-1 month")).'", "'.date('Ym').'")
      ');
    }

    public static function get8pelanggan($type, $sto, $lat, $long)
    {
      switch ($type) {
        case 'order':
          $select = 'FORMAT(6371 * acos(cos(radians('.$lat.')) * cos(radians(dps.lat)) * cos(radians(dps.lon) - radians('.$long.')) + sin(radians('.$lat.')) * sin(radians(dps.lat))), 3) as jarak,';
          $where = 'HAVING jarak < 0.25';
          $limit = 'LIMIT 8';
          break;
        
        case 'sto':
          $select = '';
          $where = 'AND dps.sto = "'.$sto.'"';
          $limit = '';
          break;
      }
      return DB::select('
      SELECT
        '.$select.'
        r.uraian as tim,
        gt.title as sektor,
        pls.laporan_status as status_teknisi,
        pl.modified_at as tgl_laporan_teknisi,
        dps.orderId,
        dps.orderName,
        dps.orderDate,
        dps.orderDatePs,
        dps.sto,
        dps.orderAddr,
        dps.lat,
        dps.lon,
        dps.jenisPsb as jenis_layanan,
        SUBSTRING_INDEX(dps.jenisPsb, "|", 1) as jenis_transaksi,
        dps.orderStatus
      FROM dispatch_teknisi dt
      LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      WHERE
        (DATE(dps.orderDate) BETWEEN "'.date('Y-m-d', strtotime("-120 days")).'" AND "'.date('Y-m-d').'") AND
        pls.laporan_status IN ("KENDALA ALPRO", "KENDALA JALUR/NYEBRANG JALAN", "NO ODP", "ODP FULL", "ODP JAUH", "UNSC") AND
        dt.jenis_order IN ("SC", "MYIR")
        '.$where.'
      '.$limit.'
      ');
    }

}