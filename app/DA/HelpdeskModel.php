<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');
class HelpdeskModel
{
    public static function get_manja($date)
    {
        return DB::SELECT('
        SELECT
        pl.hd_manja as nama,
        count(*) as jumlah
        FROM dispatch_teknisi dt
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        WHERE
        pl.hd_manja NOT IN ("TINA","MAULIDA") AND
        (DATE(pl.modified_at) LIKE "'.$date.'%")
        GROUP BY pl.hd_manja
        ORDER BY jumlah DESC
        ');
    }

    public static function get_config($date)
    {
        return DB::SELECT('
        SELECT
        *,
        COUNT(*) as jumlah
        FROM helpdesk_assurance_inbox hai
        WHERE
        (DATE(hai.tgl_close) LIKE "'.$date.'%") AND
        hai.status_order = 1
        GROUP BY hai.helpdesk_name
        ORDER BY jumlah DESC
        ');
    }

    public static function get_fallout($date)
    {
        return DB::SELECT('
        SELECT
        hi.FUP_BY as id_hd,
        hi.FUP_NAME as nama,
        count(*) as jumlah
        FROM helpdesk_inbox hi
        WHERE
        hi.FUP_BY <> "" AND
        hi.STATUS = "1" AND
        (DATE(hi.TGLUPDATE) LIKE "'.$date.'%")
        GROUP BY hi.FUP_BY
        ORDER BY jumlah DESC
        ');
    }

    public static function helpdesk_config($hd, $date)
    {
        if ($hd <> "ALL") {
            $where_hd = 'AND hai.helpdesk_name = "'.$hd.'"';
        } else {
            $where_hd = '';
        }

        return DB::SELECT('
        SELECT
		hai.*,
		his.STATUS,
		emp.nama,
		empp.nama as request_by
		FROM helpdesk_assurance_inbox hai
		LEFT JOIN 1_2_employee emp ON hai.helpdesk_by = emp.nik
		LEFT JOIN 1_2_employee empp ON hai.telegram_id = empp.chat_id
		LEFT JOIN helpdesk_inbox_status his ON hai.status_order = his.ID
		WHERE
		(DATE(hai.tgl_order) LIKE "%'.$date.'%") AND
        hai.status_order = 1
        '.$where_hd.'
        ORDER BY hai.tgl_close DESC
        ');
    }

    public static function get_mediacarring($date)
    {
        return DB::select('
        SELECT
        mcl.created_by,
        emp.nama,
        COUNT(*) as jumlah
        FROM media_carring_log mcl
        LEFT JOIN `1_2_employee` emp ON mcl.created_by = emp.nik
        WHERE
        (DATE(mcl.created_at) LIKE "' . $date . '%") AND
        mcl.status_carring = "OK"
        GROUP BY mcl.created_by
        ORDER BY jumlah DESC
        ');
    }

    public static function get_actcomp($type)
    {
        return DB::select('
            SELECT
                aa.datel,
                gt.title as sektor,
                gt.TL,
                emp.username_telegram,
                emp.chat_id,
                r.uraian as tim,
                dpsa.sto,
                dpsa.orderId,
                dpsa.wfm_id,
                hin.TGLOPEN as durasi_jam
            FROM Data_Pelanggan_Starclick dpsa
            LEFT JOIN dispatch_teknisi dt ON dpsa.orderIdInteger = dt.NO_ORDER AND dt.jenis_order IN ("SC", "MYIR")
            LEFT JOIN helpdesk_inbox hin ON dpsa.orderId = hin.SC
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
            LEFT JOIN 1_2_employee emp ON gt.TL_NIK = emp.nik
            LEFT JOIN maintenance_datel aa ON dpsa.sto = aa.sto
            WHERE
                YEAR(dpsa.orderDate) = "'.date('Y').'"
                AND dpsa.jenisPsb LIKE "'.$type.'%"
                AND dpsa.orderStatusId IN ("1300","1205")
            GROUP BY dpsa.orderIdInteger
            ORDER BY dpsa.orderDate ASC
        ');
    }
}
?>