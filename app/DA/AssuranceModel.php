<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class AssuranceModel{

  public static function ttr3notcomply($query){
    return DB::SELECT($query);
  }

  public static function download($date){
    $query = DB::SELECT('
    SELECT
      dn1l.Incident,
      dt.Ndem as dt_ndem,
      ib.ONU_Rx,
      pl.redaman_iboster,
      dn1l.Service_No,
      dn1l.Customer_Name,
      pl.kordinat_pelanggan,
      dn1l.no_internet,
      dn1l.Service_ID,
      dn1l.ncliOrder,
      dn1l.Segment_Status,
      dn1l.Customer_Segment,
      dn1ll.Incident as aaIncident,
      dn1l.Summary,
      rc.headline as rc_headline,
      dn1l.alamat,
      rc.streetAddress,
      dn1l.Workzone,
      dn1l.Datek,
      pl.nama_odp,
      r.uraian,
      gt.TL,
      gt.ioan,
      r.mitra,
      gt.title,
      dn1l.Reported_Date,
      dn1l.Source,
      dn1l.Reported_Datex,
      dn1l.Booking_Date,
      pls.laporan_status,
      pla.action,
      plp.penyebab,
      plpr.status_penyebab_reti,
      pl.kondisi_odp,
      sp.splitter,
      pl.catatan,
      pl.typeont,
      pl.snont,
      pl.typestb,
      pl.snstb,
      dn1l.user_created,
      pl.modified_by,
      pl.modified_at,
      dt.tgl,
      dn1l.Status_Date
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN data_nossa_1 dn1ll ON dn1l.ID = dn1ll.ID
    LEFT JOIN roc rc ON dn1l.Incident = rc.no_tiket
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN ibooster ib ON dt.Ndem = ib.order_id
    LEFT JOIN 1_2_employee emp ON pl.modified_by = emp.nik
    LEFT JOIN 1_2_employee empp ON dn1l.user_created = empp.nik
    LEFT JOIN jenis_splitter sp ON pl.splitter = sp.splitter_id
    WHERE
    dt.jenis_order IN ("IN","INT") AND
    (DATE(dt.tgl) LIKE "'.$date.'%")
    ORDER BY dt.created_at DESC
    ');
  return $query;
  }

  public static function monitoringkpi_ttr3jam($sektor,$tgl){
    return DB::SELECT('
        SELECT
        b.*,
        a.*,
        e.*,
        f.*,
        h.*,
        c.Status as statusopen
        FROM
          data_nossa_1_log a
          LEFT JOIN mdf b ON a.Workzone = b.mdf
          LEFT JOIN data_nossa_1 c ON a.Incident = c.Incident
          LEFT JOIN dispatch_teknisi d ON a.Incident = d.Ndem
          LEFT JOIN regu e ON d.id_regu = e.id_regu
          LEFT JOIN group_telegram f ON e.mainsector = f.chat_id
          LEFT JOIN psb_laporan g ON d.id = g.id_tbl_mj
          LEFT JOIN psb_laporan_status h ON g.status_laporan = h.laporan_status_id
        WHERE
          b.sektor_asr = "'.$sektor.'" AND
          date(a.Booking_Date) = "'.$tgl.'"
      ');
  }

  public static function materialKembali($bulan, $tahun, $pid, $mitra, $id_item, $sektor){
    if ($pid=="ALL"){
        $where_pid = 'AND a.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
    } else {
        $where_pid = 'AND a.project LIKE "'.$pid.'%"';
    }

    if ($mitra=="ALL"){
        $where_mitra2 = '';
    } else {
        $where_mitra2 = 'AND a.mitra LIKE "%'.$mitra.'%"';
    }

    if ($sektor=='ALL'){
        $where_sektor = '';
    }
    else{
        $where_sektor = 'AND d.chat_id="'.$sektor.'"';
    }

    $sql = 'SELECT
              b.id_item,
              sum(b.jmlKembali) as kembali
            FROM
              rfc_input a
            LEFT JOIN rfc_sal b ON b.alista_id = a.alista_id
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
            WHERE
              rfc LIKE "%'.$bulan.'/'.$tahun.'" AND
              id_item ="'.$id_item.'"
              '.$where_pid.'
              '.$where_mitra2.'
              '.$where_sektor.'
           ';

    $data = DB::select($sql);
    return $data;
  }

  public static function materialPakai($bulan, $tahun, $pid, $mitra, $id_item, $sektor){
    if ($pid=="ALL"){
        $where_pid = 'AND a.project IN ("R06-8/2019","R06-2/2019","R06-13/2019")';
    } else {
        $where_pid = 'AND a.project LIKE "'.$pid.'%"';
    }

    if ($mitra=="ALL"){
        $where_mitra2 = '';
    } else {
        $where_mitra2 = 'AND a.mitra LIKE "%'.$mitra.'%"';
    }

    if ($sektor=='ALL'){
        $where_sektor = '';
    }
    else{
        $where_sektor = 'AND d.chat_id="'.$sektor.'"';
    }

    $sql = 'SELECT
              b.id_item,
              sum(b.jmlTerpakai) as terpakai
            FROM
              rfc_input a
            LEFT JOIN rfc_sal b ON b.alista_id = a.alista_id
            LEFT JOIN regu c ON a.id_regu = c.id_regu
            LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
            WHERE
              rfc LIKE "%'.$bulan.'/'.$tahun.'" AND
              id_item ="'.$id_item.'"
              '.$where_pid.'
              '.$where_mitra2.'
              '.$where_sektor.'
           ';

    $data = DB::select($sql);
    return $data;
  }


  public static function kendala_teknik_by_umur(){
    return DB::SELECT('
      SELECT
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=0 AND TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<12 THEN 1 ELSE 0 END) as kurang12jam,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=12 AND TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )<24 THEN 1 ELSE 0 END) as x12jam,
      SUM(CASE WHEN TIMESTAMPDIFF( HOUR , d.UmurDatetime,  "'.date('Y-m-d H:i:s').'" )>=24 THEN 1 ELSE 0 END) as x24jam,
      c.title
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN data_nossa_1_log d ON a.Ndem = d.Incident
      LEFT JOIN psb_laporan e ON a.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
        WHERE
          f.grup = "KT" AND YEAR(a.tgl)="'.date('Y').'" AND
          c.title LIKE "TERR%"
      GROUP BY
        c.title
      ');
  }

  public static function ttr3notcomply_sektor(){
    $start = date('Y-m-01');
    $end = date('Y-m-d');
    return DB::SELECT('
      SELECT
      d.title,
      count(*) as jumlah
      FROM
        detail_gangguan_close_3on3 a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        (a.tgl_close BETWEEN "'.$start.'" AND "'.$end.'") AND
        a.ttr3_jam = 0
      GROUP BY d.title
      ORDER BY jumlah DESC
    ');
  }

  public static function trendgaullist($sektor,$date){
    $when_sektor = '';
    $when_date = '';
    if ($sektor <> "ALL"){
      $when_sektor = 'AND d.title = "'.$sektor.'"';
    }
    if ($date <> "ALL"){
      $when_date = 'AND date(a.tgl_close)="'.$date.'"';
    }
    return DB::SELECT('
      SELECT
      *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE 1
         '.$when_sektor.'
         '.$when_date.'
    ');
  }
  public static function ttr3notcomplylist($sektor,$date){
    $when_sektor = '';
    $when_date = '';
    if ($sektor <> "ALL"){
      $when_sektor = 'AND d.title = "'.$sektor.'"';
    }
    if ($date <> "ALL"){
      $when_date = 'AND date(a.tgl_close)="'.$date.'"';
    }
    return DB::SELECT('
      SELECT
      *,
      "#NA" as is_gaul
      FROM
        detail_gangguan_close_3on3 a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE
          a.ttr3_jam = 0
         '.$when_sektor.'
         '.$when_date.'
    ');
  }

  public static function trendgaul_date(){
    return DB::SELECT('
      SELECT
      DATE(tgl_close) as date_close,
      DAY(tgl_close) as tgl_close
      FROM
        detail_gangguan_close
      GROUP BY date(tgl_close)
    ');
  }

  public function trendgaul(){
    return DB::SELECT('
      SELECT
        *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b on a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      GROUP BY
        d.title
    ');
  }
  public static function trendgaul_sektor($query){
    return DB::SELECT($query);
  }

  public static function get_sektor()
  {
    return DB::SELECT('SELECT * FROM group_telegram a WHERE a.sektorx NOT IN ("CCAN1","CCAN2","CCAN3","CCAN4","CCAN5","CCAN6","LOGICCCAN","SQUADBJB","SQUADBJM","SQUADTTG","SQUADBLN") AND a.sms_active_prov = 1 ORDER BY urut ASC');
  }

  public static function get_sektor_ioan(){
    return DB::SELECT('SELECT * FROM group_telegram a WHERE a.ioan <> "" AND a.ioan_check = 1 AND a.sms_active = 1 ORDER BY urut ASC');
  }

  public static function get_sektor_ioanx(){
    return DB::SELECT('SELECT a.title as id, a.title as text FROM group_telegram a WHERE a.ioan <> "" AND a.ioan_check = 1 AND a.sms_active = 1 ORDER BY urut ASC');
  }

  public static function get_sektor_matrix_team($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE = 1 AND a.Ndem LIKE "IN%" AND c.chat_id = "'.$mainsector.'" AND (a.tgl = "'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT","SISA") AND YEAR(a.tgl) = "'.date('Y').'" AND MONTH(a.tgl) = "'.date('m').'"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_old($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      WHERE
      b.ACTIVE=1 AND a.Ndem LIKE "IN%" AND c.chat_id = "'.$mainsector.'" AND (a.tgl="'.$date.'" OR DATE(d.modified_at) = "'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_prov_qc($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      b.ket_bantek,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.Ndem = g.orderId
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      WHERE
      b.ACTIVE=1 AND
      YEAR(a.tgl)>="'.date('Y').'" AND
      c.chat_id = "'.$mainsector.'" AND
      a.jenis_layanan = "QC" AND
      (a.tgl="'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      GROUP BY b.id_regu');
  }

  public static function get_sektor_matrix_team_prov_go($mainsector,$date){
    return DB::SELECT('
      SELECT
      b.uraian,
      a.id_regu
      FROM dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN group_telegram c ON b.mainsector = c.chat_id
      LEFT JOIN psb_laporan d ON a.id = d.id_tbl_mj
      LEFT JOIN psb_laporan_status e ON d.status_laporan = e.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.Ndem = g.orderId
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      WHERE
      b.ACTIVE=1 AND
      c.chat_id = "'.$mainsector.'" AND
      (date(g.orderDatePs) = "'.$date.'" OR date(g.orderDatePs) = "0000-00-00 00:00:00" OR g.orderDatePs is null) AND
      (a.tgl="'.$date.'" OR
      (e.grup IN ("OGP","KP","KT") AND YEAR(a.tgl)="'.date('Y').'" AND MONTH(a.tgl)="'.date('m').'"))
      AND
      YEAR(a.tgl)>="'.date('Y').'"
      GROUP BY b.id_regu');
  }


  public static function get_sektor_matrix_team_order($id_regu,$date)
  {
    return DB::SELECT('
        SELECT
          *,
          a.Ndem,
          a.jenis_order as dt_jenis_order,
          d.grup,
          d.laporan_status,
          a.updated_at,
          a.id as id_dt,
          c.is_3hs as is_3HS,
          c.is_12hs as is_12HS,
          e.Incident_Symptom as e_is,
          e.Summary as e_summary,
          e.Last_Work_Log_Date as e_ttr,
          e.Reported_Date as e_reported_date,
          e.Reported_Datex as e_reported_datex,
          e.Customer_Type as e_customer_type,
          e.Contact_Name as e_contact_name,
          dn1.Assigned_by as statusManja,
          dn1.Booking_Date as jam_manja,
          roc.customer_assign as roc_customer_assign,
          roc.ca_manja_time
        FROM dispatch_teknisi a
        LEFT JOIN regu b ON a.id_regu = b.id_regu
        LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
        LEFT JOIN data_nossa_1_log e ON a.NO_ORDER = e.ID
        LEFT JOIN data_nossa_1 dn1 ON e.ID = dn1.ID
        LEFT JOIN roc ON a.Ndem = roc.no_tiket
        WHERE
          b.ACTIVE = 1 AND a.Ndem LIKE "IN%" AND 
          b.id_regu = "'.$id_regu.'" AND 
          (a.tgl = "'.$date.'" OR DATE(c.modified_at) = "'.$date.'" OR (c.status_laporan = 1 AND DATE(c.modified_at) = "'.$date.'") OR
          ((d.grup IN ("OGP","KP","KT","SISA") OR c.id IS NULL) AND 
          YEAR(a.tgl) = "'.date('Y').'"))
        GROUP BY a.NO_ORDER
        ORDER BY e.Reported_Date ASC
    ');
  }

  public static function get_sektor_matrix_team_order_prov_qc($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS,
      0 as GAUL,
      a.created_at as Reported_Date,
      a.updated_by as Assigned_by,
      jl.KAT as jenis_layanan,
      ff.approve_date as approve_date_sc
      FROM
        dispatch_teknisi a
      INNER JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      LEFT JOIN Data_Pelanggan_Starclick g ON a.Ndem = g.orderId AND YEAR(g.orderDate) = "'.date('Y').'"
      LEFT JOIN psb_myir_wo ff ON g.myir = ff.myir
      LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
      LEFT JOIN jenis_layanan jl ON a.jenis_layanan = jl.jenis_layanan
      WHERE
      ((a.tgl="'.$date.'" AND (DATE(g.orderDatePs) >= "'.date('Y-m-d').'" OR c.id IS NULL))  OR c.status_laporan IN (74,4,6,1)) AND
      b.ACTIVE=1 AND
      a.jenis_layanan = "QC" AND
      b.id_regu = "'.$id_regu.'" AND
      YEAR(a.tgl)="'.date('Y').'"
      ORDER BY
      a.created_at
    ');
  }

  public static function get_sektor_matrix_team_order_go($id_regu,$date){
    return DB::SELECT('
      SELECT
      *,
      a.Ndem,
      d.grup,
      d.laporan_status,
      a.updated_at,
      a.id as id_dt,
      c.is_3hs as is_3HS,
      c.is_12hs as is_12HS
      FROM
        dispatch_teknisi a
      LEFT JOIN regu b ON a.id_regu = b.id_regu
      LEFT JOIN psb_laporan c ON a.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN data_nossa_1_log e ON a.Ndem = e.Incident
      WHERE
      b.id_regu = "'.$id_regu.'" AND
      a.tgl="'.$date.'" AND
      a.jenis_layanan <> "QC" AND
      c.status_laporan <> 74 AND
      (f.myir IS NOT NULL OR g.orderId IS NOT NULL OR mw.ND IS NOT NULL) AND
      YEAR(a.tgl)="'.date('Y').'"
      ORDER BY
      e.Reported_Date
    ');
  }

  public static function nonatero_gaul($periode,$rayon){
    if ($rayon=="ALL"){
      $where_rayon = '';
    } else {
      $where_rayon = 'AND h.sektor_asr = "'.$rayon.'"';
    }
    $query = DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil_gaul a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "%CCAN%" AND
            a.THNBLN LIKE "'.$periode.'%"
            '.$where_rayon.'
          GROUP BY a.TROUBLE_NO
      ');
    return $query;
  }
   public static function nonatero_gaul_detil($number){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title,
          c.modified_at
        FROM
          nonatero_detil_gaul a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            a.TROUBLE_NUMBER = "'.$number.'"
        ORDER BY a.TGL_UPDATE DESC
        LIMIT 4
      ');
  }
   public static function nonatero_ttr3jam($periode,$rayon){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "CCAN%" AND
            a.IS_GAMAS = 0 AND
            a.IS_CLOSE_3JAM = 1 AND
            a.IS_3HS = 0 AND
            h.sektor_asr = "'.$rayon.'" AND
            a.THNBLNTGL_CLOSE LIKE "'.$periode.'%"
      ');
  }
  public static function nonatero_ttr12jam($periode,$rayon){
    return DB::SELECT('
        SELECT
          a.*,
          c.*,
          c.catatan as keterangan,
          f.uraian,
          d.action,
          e.penyebab,
          g.title
        FROM
          nonatero_detil a
        LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
        LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
        LEFT JOIN psb_laporan_action d ON c.action = d.laporan_action_id
        LEFT JOIN psb_laporan_penyebab e ON c.penyebabId = e.idPenyebab
        LEFT JOIN regu f ON b.id_regu = f.id_regu
        LEFT JOIN group_telegram g ON f.mainsector = g.chat_id
        LEfT JOIN mdf h ON a.CMDF = h.mdf
          WHERE
            g.title NOT LIKE "CCAN%" AND
            a.IS_GAMAS = 0 AND
            a.IS_12HS = 0 AND
            h.sektor_asr = "'.$rayon.'" AND
            a.THNBLNTGL_CLOSE LIKE "'.$periode.'%"
      ');
  }

  public static function manja($date){
    
    return DB::SELECT('
      SELECT
        SUM(CASE WHEN HOUR(a.Booking_Datex)=8 THEN 1 ELSE 0 END) as jam8,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=9 THEN 1 ELSE 0 END) as jam9,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=10 THEN 1 ELSE 0 END) as jam10,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=11 THEN 1 ELSE 0 END) as jam11,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=12 THEN 1 ELSE 0 END) as jam12,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=13 THEN 1 ELSE 0 END) as jam13,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=14 THEN 1 ELSE 0 END) as jam14,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=15 THEN 1 ELSE 0 END) as jam15,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=16 THEN 1 ELSE 0 END) as jam16,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=17 THEN 1 ELSE 0 END) as jam17,
        d.title
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        DATE(a.Booking_Datex) = "'.date('Y-m-d', strtotime($date)).'" AND
        a.Assigned_by = "CUSTOMERASSIGNED" AND d.ket_posisi = "IOAN"
      GROUP BY d.chat_id
    ');
  }

  public static function get_tiketmanja($title,$date,$jam){
    
    if ($title=="") $title="-";
    if ($jam=="expired"){
      $where_jam = 'AND TIMESTAMPDIFF( HOUR , a.Booking_Datex,  "'.date('Y-m-d H:i:s').'" )>3';
    } else {
      $where_jam = 'AND
        HOUR(a.Booking_Datex) = "'.$jam.'"';
    }
    return DB::SELECT('
      SELECT
        a.*,
        f.laporan_status,
        f.grup
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b On a.ID = b.NO_ORDER
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        d.title = "'.$title.'" AND
        DATE(a.Booking_Datex) = "'.date('Y-m-d', strtotime($date)).'"
        '.$where_jam.'
    ');
  }

  public static function get_tiketmanja2($title,$date,$jam){
    if ($title=="") $title="-";
    return DB::SELECT('
      SELECT
        a.Incident,
        f.laporan_status,
        f.grup
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b On a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        d.title = "'.$title.'" AND
        date(a.Booking_Date) = "'.$date.'" AND
        HOUR(a.Booking_Date) = "'.$jam.'" AND
        e.status_laporan <> 1

    ');
  }

  public static function manja_hvc(){
    
    return DB::SELECT('
      SELECT
        SUM(CASE WHEN HOUR(a.Booking_Datex)=8 THEN 1 ELSE 0 END) as jam8,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=9 THEN 1 ELSE 0 END) as jam9,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=10 THEN 1 ELSE 0 END) as jam10,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=11 THEN 1 ELSE 0 END) as jam11,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=12 THEN 1 ELSE 0 END) as jam12,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=13 THEN 1 ELSE 0 END) as jam13,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=14 THEN 1 ELSE 0 END) as jam14,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=15 THEN 1 ELSE 0 END) as jam15,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=16 THEN 1 ELSE 0 END) as jam16,
        SUM(CASE WHEN HOUR(a.Booking_Datex)=17 THEN 1 ELSE 0 END) as jam17,
        d.title
      FROM
        data_nossa_active a
      LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      WHERE
        (DATE(a.Booking_Datex) = "'.date('Y-m-d').'") AND
        a.Assigned_by = "CUSTOMERASSIGNED" AND a.Customer_Type IN ("HVC_GOLD","HVC_PLATINUM")
      GROUP BY d.title
    ');
  }

  public static function get_tiketmanja_hvc($title,$jam){
    
    if ($title=="") $title="-";
    if ($jam=="expired"){
      $where_jam = 'AND TIMESTAMPDIFF( HOUR , a.Booking_Datex,  "'.date('Y-m-d H:i:s').'" )>3';
    } else {
      $where_jam = 'AND
        HOUR(a.Booking_Datex) = "'.$jam.'"';
    }
    return DB::SELECT('
      SELECT
        a.*,
        f.laporan_status,
        f.grup
      FROM
        data_nossa_active a
      LEFT JOIN dispatch_teknisi b ON a.ID = b.NO_ORDER
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
      d.title = "'.$title.'" AND (DATE(a.Booking_Datex) = "'.date('Y-m-d').'") AND a.Assigned_by = "CUSTOMERASSIGNED" AND a.Customer_Type IN ("HVC_GOLD","HVC_PLATINUM")
        '.$where_jam.'
    ');
  }

  public static function manja2($date){
    return DB::SELECT('
      SELECT
        SUM(CASE WHEN HOUR(a.Booking_Date)=8 THEN 1 ELSE 0 END) as jam8,
        SUM(CASE WHEN HOUR(a.Booking_Date)=9 THEN 1 ELSE 0 END) as jam9,
        SUM(CASE WHEN HOUR(a.Booking_Date)=10 THEN 1 ELSE 0 END) as jam10,
        SUM(CASE WHEN HOUR(a.Booking_Date)=11 THEN 1 ELSE 0 END) as jam11,
        SUM(CASE WHEN HOUR(a.Booking_Date)=12 THEN 1 ELSE 0 END) as jam12,
        SUM(CASE WHEN HOUR(a.Booking_Date)=13 THEN 1 ELSE 0 END) as jam13,
        SUM(CASE WHEN HOUR(a.Booking_Date)=14 THEN 1 ELSE 0 END) as jam14,
        SUM(CASE WHEN HOUR(a.Booking_Date)=15 THEN 1 ELSE 0 END) as jam15,
        SUM(CASE WHEN HOUR(a.Booking_Date)=16 THEN 1 ELSE 0 END) as jam16,
        SUM(CASE WHEN HOUR(a.Booking_Date)=17 THEN 1 ELSE 0 END) as jam17,
        d.title
      FROM
        data_nossa_1_log a
      LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      WHERE
        date(a.Booking_Date) = "'.$date.'" AND
        e.status_laporan <> 1 AND
        a.Assigned_by = "CUSTOMERASSIGNED"
        GROUP BY d.chat_id
    ');
  }

  public static function public_ttr(){
    
    return DB::SELECT('
    SELECT
      gt.title,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=5 THEN 1 ELSE 0 END) as jam5,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=6 THEN 1 ELSE 0 END) as jam6,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=7 THEN 1 ELSE 0 END) as jam7,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=8 THEN 1 ELSE 0 END) as jam8,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=9 THEN 1 ELSE 0 END) as jam9,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=10 THEN 1 ELSE 0 END) as jam10,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=11 THEN 1 ELSE 0 END) as jam11,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=12 THEN 1 ELSE 0 END) as jam12,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=13 THEN 1 ELSE 0 END) as jam13,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=14 THEN 1 ELSE 0 END) as jam14,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=15 THEN 1 ELSE 0 END) as jam15,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=16 THEN 1 ELSE 0 END) as jam16,
      SUM(CASE WHEN HOUR(dn1l.Last_Work_Log_Datetime)=17 THEN 1 ELSE 0 END) as jam17
    FROM data_nossa_1_log dn1l
    LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    WHERE	
    (DATE(dt.tgl) = "'.date('Y-m-d').'") AND
    dn1l.Source <> "TOMMAN" AND
    pl.status_laporan <> "1" AND
    dn1l.Incident_Symptom NOT IN ("PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik","PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC") AND
    dn1l.Owner_Group <> "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" AND
    dn1l.Last_Work_Log_Datetime <> "0000-00-00 00:00:00" AND
    gt.ioan_check = 1 AND gt.sms_active = 1
    GROUP BY gt.title
    ORDER BY gt.urut ASC');
  }

  public static function get_ttr($title,$jam){
    
    return DB::SELECT('
    SELECT
    dn1l.Incident,
    pls.grup,
    pls.laporan_status,
    dn1l.Status
    FROM data_nossa_1_log dn1l
    LEFT JOIN dispatch_teknisi dt ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
    HOUR(dn1l.Last_Work_Log_Datetime) = "'.$jam.'" AND
    gt.title = "'.$title.'" AND
    (DATE(dt.tgl) = "'.date('Y-m-d').'") AND
    dn1l.Source <> "TOMMAN" AND
    pl.status_laporan <> "1" AND
    dn1l.Incident_Symptom NOT IN ("PROACTIVE TICKET | SQM - PREDICTIVE | Predictive Internet Fisik","PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC") AND
    dn1l.Owner_Group <> "ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)" AND
    dn1l.Last_Work_Log_Datetime <> "0000-00-00 00:00:00" AND
    gt.ioan_check = 1 AND gt.sms_active = 1
    ');
  }

  public static function gaulsektor($periode){
    $data = DB::SELECT('
      SELECT
        d.title,
        a.TROUBLE_NO as ticket_id,
        SUM(CASE WHEN aa.TROUBLE_NO<>"" THEN 1 ELSE 0 END) as gaulv2,
        count(*) as JML
      FROM
        nonatero_detil a
      LEFT JOIN nonatero_detil_gaul aa ON (a.TROUBLE_NO = aa.TROUBLE_NO AND aa.THNBLN = "'.$periode.'")
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
        WHERE
        a.THNBLN = "'.$periode.'" AND
        a.IS_GAMAS = 0 AND
        d.title <> ""
      GROUP BY d.title
      ORDER BY JML DESC
    ');
    return $data;
  }

  public static function gaulsektorlist($sektor){
    $data = DB::SELECT('
      SELECT
        *
      FROM
        detail_gangguan_close a
      LEFT JOIN dispatch_teknisi b ON a.ticket_id = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_action f ON e.action = f.laporan_action_id
      LEFT JOIN psb_laporan_penyebab g ON e.penyebabId = g.idPenyebab
        WHERE
        d.title = "'.$sektor.'"
      ORDER BY
        a.is_gaul DESC
    ');
    return $data;
  }

  public static function gaulgenerator($periode){
    $data = DB::SELECT('
    SELECT
      *,
      e.action as actionText
    FROM nonatero_detil_gaul a
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
      LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
      LEFT JOIN regu g ON b.id_regu = g.id_regu
      LEFT JOIN group_telegram h ON g.mainsector = h.chat_id
    WHERE a.THNBLN = "'.$periode.'"
    ORDER BY a.TROUBLE_NUMBER
    ');
    return $data;
  }

  public static function dashboard_gaul_by_seq_list($action,$periode){
    $data = DB::SELECT('
    SELECT
      *,
      e.action as actionText
    FROM nonatero_detil_gaul a
      LEFT JOIN dispatch_teknisi b ON a.TROUBLE_NO = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
      LEFT JOIN psb_laporan_status d ON c.status_laporan = d.laporan_status_id
      LEFT JOIN psb_laporan_action e ON c.action = e.laporan_action_id
      LEFT JOIN psb_laporan_penyebab f ON c.penyebabId = f.idPenyebab
      LEFT JOIN regu g ON b.id_regu = g.id_regu
      LEFT JOIN group_telegram h ON g.mainsector = h.chat_id
    WHERE
      a.THNBLN = "'.$periode.'" AND
      a.SEQ = "'.$action.'"
    ORDER BY a.TROUBLE_NUMBER
    ');
    return $data;
  }

  public static function dashboard_gaul_by_seq($periode,$sort){
    $data = DB::SELECT('
      SELECT
        *,
        count(*) as jumlah
      FROM
        nonatero_detil_gaul a
      WHERE
        a.THNBLN LIKE "'.$periode.'%" AND
        a.SEQ NOT IN ("LOGIC»","FISIK»","NA»","NONFISIK»")
      GROUP BY
        a.SEQ
      ORDER BY
        jumlah '.$sort.'
    ');
    return $data;
  }

  public static function getDataIboosterByIn($inOrder){
      return DB::table('ibooster')->where('order_id',$inOrder)->first();
  }

  public static function detailWoMaterial($id_item){
      $sql = 'SELECT
                a.*,
                c.Ndem,
                c.dispatch_by,
                c.id as id_dt
              FROM
                psb_laporan_material a
              LEFT JOIN psb_laporan b ON a.psb_laporan_id=b.id
              LEFT JOIN dispatch_teknisi c ON b.id_tbl_mj=c.id
              WHERE
                a.id_item_bantu="'.$id_item.'"
              ';

      $data = DB::select($sql);
      return $data;
  }

  public static function getStatusOrderMaintenance($bulan){
      $sql = 'SELECT
                a.*,
                b.*,
                c.*,
                d.uraian,
                e.chat_id,
                e.title
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
              LEFT JOIN regu d ON b.id_regu = d.id_regu
              LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
              WHERE
                c.status_laporan = 2 AND
                b.tgl LIKE "%'.$bulan.'%"
              ';
      $data = DB::select($sql);
      return $data;
  }

  public static function getStatusKirimTeknisi($bulan){
      $sql = 'SELECT
                a.*,
                b.*,
                c.*,
                d.uraian,
                e.chat_id,
                e.title
              FROM
                data_nossa_1_log a
              LEFT JOIN dispatch_teknisi b ON a.Incident = b.Ndem
              LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
              LEFT JOIN regu d ON b.id_regu = d.id_regu
              LEFT JOIN group_telegram e ON d.mainsector = e.chat_id
              WHERE
                c.status_laporan = 67 AND
                b.tgl LIKE "%'.$bulan.'%"
              ';
      $data = DB::select($sql);
      return $data;
  }

  public static function getManjaForBot($date,$jam){
    $jamCari = '(';
    for($a=8; $a<=$jam; $a++){
        $jamCari .= '"'.$a.'"'.',';
    };

    $jamCari = substr($jamCari, 0, -1);
    $jamCari .=')';

    $where_jam = 'AND HOUR(a.Booking_Date) IN '.$jamCari;

    return DB::SELECT('
      SELECT
        a.Incident,
        d.title,
        c.uraian,
        HOUR(a.Booking_Date) as jamBooking,
        f.laporan_status,
        a.Service_ID
      FROM
        data_nossa_1 a
      LEFT JOIN dispatch_teknisi b On a.Incident = b.Ndem
      LEFT JOIN regu c ON b.id_regu = c.id_regu
      LEFT JOIN group_telegram d ON c.mainsector = d.chat_id
      LEFT JOIN psb_laporan e ON b.id = e.id_tbl_mj
      LEFT JOIN psb_laporan_status f ON e.status_laporan = f.laporan_status_id
      WHERE
        b.Ndem <>"" AND
        a.Assigned_by="CUSTOMERASSIGNED" AND
        (e.status_laporan NOT IN ("1","37","38") OR e.status_laporan IS NULL) AND
        date(a.Booking_Date) = "'.$date.'"
        '.$where_jam.'
    ');
  }

  public static function detailMatrix($sektor,$date){
    
    return DB::SELECT('
    SELECT
    dn1l.*,
    dt.tgl,
    r.uraian as tim,
    gt.title as sektor,
    pls.laporan_status,
    pls.grup,
    pl.catatan,
    pl.redaman_iboster,
    pla.action,
    plp.penyebab,
    plpr.status_penyebab_reti,
    ib.ONU_Rx as ib_onu_rx
    FROM data_nossa_1_log dn1l
    LEFT JOIN dispatch_teknisi dt ON dn1l.ID = dt.NO_ORDER
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN ibooster ib ON dn1l.Incident = ib.order_id
    WHERE
    gt.title = "'.$sektor.'" AND
    r.ACTIVE = 1 AND
    (dt.tgl = "'.$date.'" OR DATE(pl.modified_at) = "'.$date.'" OR (pl.status_laporan = 1 AND DATE(pl.modified_at) = "'.$date.'") OR
      ((pls.grup IN ("OGP","KP","KT","SISA") OR pl.id IS NULL) AND YEAR(dt.tgl) = "'.date('Y').'"))
    ORDER BY dn1l.Reported_Date DESC
    ');
  }

  public static function dashboardTekSQMList($tim,$kat,$date){
    
    
    if($tim <> "ALL"){
      $where_tim = 'dt.id_regu = "'.$tim.'" AND';
    }else{
      $where_tim = '';
    }

    if($kat=="SISA"){
      $where_kat = 'AND pls.grup = "SISA"';
    }elseif($kat=="OGP"){
      $where_kat = 'AND pls.grup = "OGP"';
    }elseif($kat=="UP"){
      $where_kat = 'AND pls.grup = "UP"';
    }elseif($kat=="KENDALA"){
      $where_kat = 'AND pls.grup IN ("KT","KP")';
    }elseif($kat=="ORDER"){
      $where_kat = '';
    }

    return DB::SELECT('
    SELECT
    dn1l.*,
    rc.no_tiket as rc_no_tiket,
    dt.tgl,
    r.uraian as tim,
    gt.title as sektor,
    ma.mitra_amija_pt as mitra,
    pls.laporan_status,
    pls.grup,
    pl.catatan,
    pl.redaman_iboster,
    pla.action,
    plp.penyebab,
    plpr.status_penyebab_reti,
    ib.ONU_Rx as ib_onu_rx
    FROM dispatch_teknisi dt
    LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
    LEFT JOIN roc rc ON dt.Ndem = rc.no_tiket
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND (ma.kat = "IOAN" AND ma.witel = "KALSEL")
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
    LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
    LEFT JOIN psb_laporan_penyebab_reti plpr ON pl.penyebab_reti_id = plpr.id_penyebab_reti
    LEFT JOIN ibooster ib ON dt.Ndem = ib.order_id
    WHERE
    '.$where_tim.'
    (dt.tgl LIKE "'.$date.'%" OR DATE(pl.modified_at) LIKE "'.$date.'%" OR (pl.status_laporan = 1 AND DATE(pl.modified_at) LIKE "'.$date.'%") OR
      ((pls.grup IN ("OGP","KP","KT","SISA") OR pl.id IS NULL) AND YEAR(dt.tgl) = "'.date('Y').'"))
    '.$where_kat.'
    ORDER BY dn1l.Reported_Date DESC
    ');
  }

  public static function cek_ongoing(){
    
    return DB::SELECT('
    SELECT
      a.id as id_dt,
      a.Ndem,
      a.jenis_order,
      pls.laporan_status
      FROM
        dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON b.status_laporan = pls.laporan_status_id
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        LEFT JOIN data_nossa_1_log d ON a.NO_ORDER = d.ID
      WHERE
        (c.nik1 = "'.session('auth')->id_karyawan.'" OR c.nik2 = "'.session('auth')->id_karyawan.'") AND
        b.status_laporan IN (28,29,5,31) AND
        a.tgl = "'.date('Y-m-d').'"
      GROUP BY a.id
    ');
  }

  public static function monitoringOrder($status)
  {
    

    switch ($status) {
      case 'berangkat':
        $data = DB::SELECT('
        SELECT
        dt.Ndem,
        pl.modified_at,
        pls.laporan_status,
        r.uraian as tim,
        gt.TL,
        gt.TL_Username,
        gt.SM,
        gt.title as sektor
        FROM dispatch_teknisi dt
        LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
        LEFT JOIN roc ON dt.Ndem = roc.no_tiket
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        dt.jenis_order IN ("IN","INT") AND
        DATE(dt.tgl) = "'.date('Y-m-d').'" AND
        TIMESTAMPDIFF( MINUTE , pl.modified_at, "'.date('Y-m-d H:i:s').'") >= 30 AND
        pl.status_laporan IN (28)
        GROUP BY dt.Ndem
        ORDER BY pl.modified_at ASC
        ');
      break;
      case 'progress':
        $data = DB::SELECT('
        SELECT
        dt.Ndem,
        pl.modified_at,
        pls.laporan_status,
        r.uraian as tim,
        gt.TL,
        gt.TL_Username,
        gt.SM,
        gt.title as sektor
        FROM dispatch_teknisi dt
        LEFT JOIN data_nossa_1_log dn1l ON dt.NO_ORDER = dn1l.ID
        LEFT JOIN roc ON dt.Ndem = roc.no_tiket
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
        dt.jenis_order IN ("IN","INT") AND
        DATE(dt.tgl) = "'.date('Y-m-d').'" AND
        TIMESTAMPDIFF( HOUR , pl.modified_at, "'.date('Y-m-d H:i:s').'") >= 1 AND
        pl.status_laporan IN (5)
        GROUP BY dt.Ndem
        ORDER BY pl.modified_at ASC
        ');
      break;
    }

    return $data;
  }

  public static function checkDoubleOrder($type ,$inet)
  {
    switch ($type) {
      case 'ROC':
        return DB::SELECT('
          SELECT
            roc.no_tiket,
            roc.tglOpen,
            dt.tgl as tgl_dispatch,
            pls.laporan_status,
            r.uraian as tim,
            gt.title as sektor
          FROM roc
          LEFT JOIN dispatch_teknisi dt ON roc.no_tiket = dt.Ndem
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
            roc.no_speedy = "' . $inet . '" AND
            DATE(dt.tgl) = "' . date('Y-m-d') . '" AND
            (pl.status_laporan IN (6, 28, 29, 5) OR pl.status_laporan IS NULL)
          ORDER BY roc.tglOpen DESC
        ');
        break;
      
      case 'NOSSA':
        return DB::SELECT('
          SELECT
            dn1l.Incident as no_tiket,
            dn1l.Reported_Datex as tglOpen,
            dt.tgl as tgl_dispatch,
            pls.laporan_status,
            r.uraian as tim,
            gt.title as sektor
          FROM data_nossa_1_log dn1l
          LEFT JOIN dispatch_teknisi dt ON dn1l.ID_INCREMENT = dt.NO_ORDER AND dt.jenis_order IN ("IN", "INT")
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN regu r ON dt.id_regu = r.id_regu
          LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
          WHERE
            dn1l.no_internet = "' . $inet . '" AND
            DATE(dt.tgl) = "' . date('Y-m-d') . '" AND
            (pl.status_laporan IN (6, 28, 29, 5) OR pl.status_laporan IS NULL)
          ORDER BY dn1l.Reported_Datex DESC
        ');
        break;
    }
  }

  public static function monetIbooster($witel)
  {
    if ($witel == "KALSEL")
    {
      return DB::connection('bpp')->select('
        SELECT
        DATEDIFF(NOW(), im.date_created) as umur,
        m.ticketid,
        m.no_inet,
        m.upload_by,
        m.ncli,
        im.*
        FROM monet_sugar_treg6 m
        LEFT JOIN ibooster_monet_sugar im ON m.no_inet = im.ND
        WHERE
        im.ID_WITEL = "'.$witel.'" AND
        DATEDIFF(NOW(), im.date_created) < 60 AND
        im.ONU_Link_Status = "LOS"
        GROUP BY m.no_inet
        ORDER BY m.ts DESC
      '); 
    } elseif ($witel == "BALIKPAPAN") {
      return DB::connection('bpp')->select('
        SELECT
        DATEDIFF(NOW(), im.date_created) as umur,
        m.*,
        im.*,
        dbr.STO
        FROM monet_sugar_treg6 m
        LEFT JOIN ibooster_monet_sugar im ON m.no_inet = im.ND
        LEFT JOIN dossier_bpp_rully dbr ON m.no_inet = dbr.NOTEL
        WHERE
        im.ID_WITEL = "'.$witel.'" AND
        DATEDIFF(NOW(), im.date_created) < 60 AND
        im.ONU_Link_Status = "LOS"
        GROUP BY m.no_inet
        ORDER BY m.ts DESC
      ');
    }
  }

  public static function QmonetIbooster($witel)
  {
    if ($witel == "KALSEL")
    {
      return DB::connection('bpp')->select('
        SELECT
        im.*,
        DATEDIFF(NOW(), im.date_created) as umur,
        m.ncli as NCLI,
        m.id_ticketid,
        m.ticketid,
        m.upload_by
        FROM monet_treg6 m 
        LEFT JOIN ibooster_monet im ON m.no_inet = im.ND
        WHERE
        m.witel = "'.$witel.'" AND
        im.ONU_Link_Status = "LOS"
      ');
    } elseif ($witel == "BALIKPAPAN") {
      return DB::connection('bpp')->select('
        SELECT
        im.*,
        DATEDIFF(NOW(), im.date_created) as umur,
        dbr.NCLI,
        dbr.STO,
        m.id_ticketid,
        m.ticketid,
        m.upload_by
        FROM monet_treg6 m
        LEFT JOIN dossier_bpp_rully dbr ON m.no_inet = dbr.NOTEL
        LEFT JOIN ibooster_monet im ON m.no_inet = im.ND
        WHERE
        m.witel = "'.$witel.'" AND
        im.ONU_Link_Status = "LOS"
      ');
    }
  }

  public static function save_inet($bulk,$witel)
  {
    if ($witel == "KALSEL")
    {
      $inet = str_replace(array('.0','@telkom.net','@apps.telkom'),'',$bulk['inet']);

      $check = DB::table('monet')->where('no_inet',$inet)->first();
      if (count($check)>0)
      {
        DB::transaction(function() use($inet) {
          DB::table('monet')->where('no_inet',$inet)->update([
            'no_inet' => $inet,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      } else {
        DB::transaction(function() use($inet) {
          DB::table('monet')->insert([
            'no_inet' => $inet,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      }
    } elseif ($witel == "BALIKPAPANS") {
      $inet = str_replace(array('.0','@telkom.net','@apps.telkom'),'',$bulk['inet']);
      $ncli = @$bulk['ncli'];

      $check = DB::connection('bpp')->table('monet_sugar')->where('no_inet',$inet)->first();
      if (count($check)>0)
      {
        DB::transaction(function() use($inet,$ncli) {
          DB::connection('bpp')->table('monet_sugar')->where('no_inet',$inet)->update([
            'no_inet' => $inet,
            'ncli' => $ncli,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      } else {
        DB::transaction(function() use($inet,$ncli) {
          DB::connection('bpp')->table('monet_sugar')->insert([
            'no_inet' => $inet,
            'ncli' => $ncli,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      }
    } elseif ($witel == "BALIKPAPANQ") {
      $inet = str_replace(array('.0','@telkom.net','@apps.telkom'),'',$bulk['inet']);
      $ncli = @$bulk['ncli'];
      
      $check = DB::connection('bpp')->table('monet')->where('no_inet',$inet)->first();
      if (count($check)>0)
      {
        DB::transaction(function() use($inet,$ncli) {
          DB::connection('bpp')->table('monet')->where('no_inet',$inet)->update([
            'no_inet' => $inet,
            'ncli' => $ncli,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      } else {
        DB::transaction(function() use($inet,$ncli) {
          DB::connection('bpp')->table('monet')->insert([
            'no_inet' => $inet,
            'ncli' => $ncli,
            'upload_by' => session('auth')->id_karyawan
          ]);
        }); 
      }
    }
  }

  public static function inboxMediaCarring($status, $tanggal)
  {
    switch ($status) {
      case 'OK':
        $statusx = 'AND mdc.status_carring = "OK"';
        break;
      
      case 'NOK':
        $statusx = 'AND mdc.status_carring = "NOK"';
        break;

      case 'UNDEFINED':
        $statusx = 'AND mdc.status_carring IS NULL';
        break;
      
      default:
        $statusx = '';
        break;
    }
    return DB::select('
      SELECT
      dn1.*,
      pl.noPelangganAktif,
      pls.laporan_status,
      pla.action,
      plp.penyebab,
      r.uraian as tim,
      gt.title as sektor,
      ib.ONU_Link_Status,
      ib.ONU_Rx,
      ib.date_created as ib_date_created,
      mdc.status_carring,
      mdc.no_whatsapp,
      mdc.catatan_carring
      FROM data_nossa_1 dn1
      LEFT JOIN dispatch_teknisi dt ON dn1.ID = dt.NO_ORDER AND dt.jenis_order = "IN"
      LEFT JOIN media_carring_log mdc ON dn1.ID = mdc.id_nossa
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      LEFT JOIN psb_laporan_action pla ON pl.action = pla.laporan_action_id
      LEFT JOIN psb_laporan_penyebab plp ON pl.penyebabId = plp.idPenyebab
      LEFT JOIN ibooster ib ON dn1.Incident = ib.order_id AND ib.menu = "media_carring"
      WHERE
      (DATE(dn1.Reported_Datex) LIKE "' . $tanggal .'%") AND
      dn1.Source = "RIGHTNOW" AND 
      dn1.Customer_Segment = "DCS" AND
      dn1.Status = "CLOSED" AND
      dn1.Witel = "KALSEL" AND
      dn1.Workzone IN ("BBR", "MTP", "LUL", "MRB", "BJM", "GMB", "ULI") AND
      pl.id_tbl_mj IS NOT NULL
      ' . $statusx . '
      GROUP BY dn1.ID
      ORDER BY dn1.Reported_Datex DESC, ib.date_created DESC
    ');
  }

  public static function inboxMediaCarringSave($req)
  {
    DB::transaction(function () use ($req) {
      DB::table('media_carring_log')->insert([
        'id_nossa'          => $req->input('id_nossa'),
        'incident'          => $req->input('incident'),
        'status_carring'    => $req->input('status_carring'),
        'no_whatsapp'       => $req->input('no_whatsapp'),
        'catatan_carring'   => $req->input('catatan_carring'),
        'created_by'        => session('auth')->id_karyawan
      ]);
    });

    switch ($req->input('status_carring')) {
      case 'NOK':
        $check = DB::table('dispatch_teknisi')->where('NO_ORDER', $req->input('id_nossa'))->first();

        if (count($check) > 0)
        {
          DB::transaction(function () use ($req, $check) {
            DB::table('psb_laporan_log')->insert([
              'created_at'      => DB::raw('NOW()'),
              'created_by'      => session('auth')->id_karyawan,
              'created_name'    => session('auth')->nama,
              'status_laporan'  => 6,
              'id_regu'         => $check->id_regu,
              'jadwal_manja'    => date('Y-m-d H:i:s'),
              'catatan'         => "DISPATCH at " . date('Y-m-d'),
              'Ndem'            => $req->input('incident')
            ]);

            DB::table('dispatch_teknisi_log')->insert([
              'updated_at'          => DB::raw('NOW()'),
              'updated_by'          => session('auth')->id_karyawan,
              'jadwal_manja'        => date('Y-m-d H:i:s'),
              'tgl'                 => date('Y-m-d'),
              'id_regu'             => $check->id_regu,
              'manja'               => null,
              'manja_status'        => null,
              'updated_at_timeslot' => null,
              'Ndem'                => $req->input('incident'),
              'NO_ORDER'            => $req->input('id_nossa')
            ]);

            DB::table('psb_laporan')->where('id_tbl_mj', $check->id)->update([
              'status_laporan' => 6,
              'action' => null
            ]);

            DB::table('dispatch_teknisi')->where('id', $check->id)->update([
              'NO_ORDER'        => $req->input('id_nossa'),
              'jenis_order'     => 'IN',
              'step_id'         => '1.0',
              'updated_at'      => DB::raw('NOW()'),
              'updated_by'      => session('auth')->id_karyawan,
              'tgl'             => date('Y-m-d'),
              'jadwal_manja'    => date('Y-m-d H:i:s'),
              'id_regu'         => $check->id_regu,
              'media_carring'   => 'NOK_MEDIA_CARRING'
            ]);
          });
        }
        break;
    }    
  }
}
?>
