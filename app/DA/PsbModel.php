<?PHP

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Telegram;

date_default_timezone_set('Asia/Makassar');

class PsbModel
{
  public static function psb_laporan_status(){
    return DB::SELECT('SELECT laporan_status_id as id, laporan_status as text FROM psb_laporan_status ORDER BY laporan_status');
  }
  public static function batch_get_data_for_dalapa($id){
    $get_sc = DB::SELECT('
      SELECT
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0" as Authorization,
      b.nama_odp as ODP_LOCATION,
      b.port_number as PORT_NUMBER,
      b.kordinat_pelanggan as KORDINAT,
      b.snont as SN_ONT,
      c.orderName as NAMA_PELANGGAN,
      c.noTelp as VOICE_NUMBER,
      c.internet as INTERNET_NUMBER,
      DATE(b.modified_at) as DATE_VALIDATION,
      b.odp_label_code as QR_PORT_ODP,
      b.dropcore_label_code as QR_DROPCORE,
      d.alamatLengkap as NAMA_JALAN,
      d.picPelanggan as CONTACT,
      1 as TYPE,
      c.orderId,
      a.Ndem,
      a.id as id_dt
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_myir_wo d ON a.Ndem = d.sc
      WHERE
        a.tgl LIKE "'.$id.'%" AND
        b.status_laporan = 1 AND
        b.dalapa_status IS NULL AND
        c.jenisPsb NOT LIKE "MO%" AND 
        c.orderId IS NOT NULL
    ');
    return $get_sc;
  }

  public static function get_data_for_dalapa($id){
    $get_sc = DB::SELECT('
      SELECT
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ3aXRlbCI6IkJKTSIsInZhbHVlIjp7IjEiOjIwMzcsIjIiOjIwMzgsIjMiOjIwMzksIjQiOjIwNDAsIjUiOjIwNDEsIjYiOjIwNDIsIjciOjIwNDN9fQ.GF0HcedknFaFQ5h-Dll93F6MLRmVpj2sBYrpgoytDC0" as Authorization,
      b.nama_odp as ODP_LOCATION,
      b.port_number as PORT_NUMBER,
      b.kordinat_pelanggan as KORDINAT,
      b.snont as SN_ONT,
      c.orderName as NAMA_PELANGGAN,
      c.noTelp as VOICE_NUMBER,
      c.internet as INTERNET_NUMBER,
      DATE(b.modified_at) as DATE_VALIDATION,
      b.odp_label_code as QR_PORT_ODP,
      b.dropcore_label_code as QR_DROPCORE,
      d.alamatLengkap as NAMA_JALAN,
      d.picPelanggan as CONTACT,
      1 as TYPE
      FROM
        dispatch_teknisi a
      LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
      LEFT JOIN psb_myir_wo d ON a.Ndem = d.sc
      WHERE
        a.id = "'.$id.'" AND
        c.orderId IS NOT NULL
    ');
    return $get_sc;
  }

  public static function updatenote(){
    return DB::table('updatenote')->get()->first();
  }

  public static function checkONGOING(){
    
    $auth = session('auth');
    $query = DB::SELECT('
      SELECT
      *,
      pls.laporan_status,
      a.id as id_dt,
      a.jenis_order
      FROM
        dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON b.status_laporan = pls.laporan_status_id
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        LEFT JOIN data_nossa_1_log d ON a.NO_ORDER = d.ID
        LEFT JOIN roc ON a.Ndem = roc.no_tiket
        LEFT JOIN cabut_nte_order cno ON a.NO_ORDER = cno.wfm_id
        LEFT JOIN ont_premium_order opo ON a.NO_ORDER = opo.no_inet
        LEFT JOIN Data_Pelanggan_Starclick e ON a.NO_ORDER = e.orderIdInteger OR cno.order_id = e.orderIdInteger
        LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      WHERE
        (c.nik1 = "'.$auth->id_karyawan.'" OR c.nik2 = "'.$auth->id_karyawan.'") AND
        b.status_laporan IN (28,29,5,31) AND
        a.tgl = "'.date('Y-m-d').'"
      GROUP BY a.id
    ');
    return $query;
  }

  public static function checkSTATUS($status){
    
    if($status=="OGP"){
      $where_status = 'AND b.status_laporan IN (28,29,5,31,101)';
    }else if($status=="UP"){
      $where_status = 'AND b.status_laporan IN (1,88,94,98)';
    }else if($status=="KENDALA"){
      $where_status = 'AND b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }else if($status=="HR"){
      $where_status = 'AND b.status_laporan = 4';
    }
    $auth = session('auth');
    $query = DB::SELECT('
      SELECT
      *,
      a.id as id_dt
      FROM
        dispatch_teknisi a
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN regu c ON a.id_regu = c.id_regu
        LEFT JOIN data_nossa_1_log d ON a.NO_ORDER = d.ID
        LEFT JOIN Data_Pelanggan_Starclick e ON a.NO_ORDER = e.orderIdInteger
        LEFT JOIN psb_myir_wo f ON a.Ndem = f.myir
      WHERE
        (c.nik1 = "'.$auth->id_karyawan.'" OR c.nik2 = "'.$auth->id_karyawan.'")
        '.$where_status.'
        AND a.tgl = "'.date('Y-m-d').'"
      GROUP BY a.id
    ');
    return $query;
  }

  public static function checkQC($bulan)
  {
    if($bulan == "ALL")
    {
      $where_bulan = '';
    } else {
      $where_bulan = 'AND DATE(utt.tglTrx) LIKE "'.$bulan.'%"';
    }
    $auth = session('auth');

    return DB::SELECT('
      SELECT
      dt.id as id_dt,
      utt.*,
      pls.laporan_status,
      dps.orderKontak,
      dps.orderStatus,
      dps.orderAddr,
      pl.modified_at,
      pl.noPelangganAktif,
      r.uraian,
      r.id_regu
      FROM utonline_tr6 utt
      LEFT JOIN dispatch_teknisi dt ON utt.scId_int = dt.NO_ORDER
      LEFT JOIN Data_Pelanggan_Starclick dps ON utt.scId_int = dps.orderIdInteger
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      WHERE
      utt.statusName IN ("Reopen MyTech") AND
      utt.witel = "KALSEL (BANJARMASIN)" AND
      dt.jenis_order IN ("SC", "MYIR") AND
      (r.nik1 = "'.$auth->id_karyawan.'" OR r.nik2 = "'.$auth->id_karyawan.'")
      '.$where_bulan.'
      ORDER BY utt.tglTrx DESC
    ');
  }
  
  public static function checkCabutanNte()
  {
    $auth = session('auth');
    return DB::SELECT('
    SELECT
      cno.*,
      dt.jenis_layanan,
      dt.id as id_dt,
      pls.laporan_status,
      dps.orderDatePs,
      dps.orderKontak,
      dps.orderStatus,
      dps.orderAddr,
      dps.internet,
      pl.modified_at,
      pl.noPelangganAktif,
      r.uraian,
      r.id_regu
    FROM cabut_nte_order cno
    LEFT JOIN dispatch_teknisi dt ON cno.wfm_id = dt.NO_ORDER
    LEFT JOIN Data_Pelanggan_Starclick dps ON cno.order_id = dps.orderIdInteger
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
      dt.jenis_order = "CABUT_NTE" AND
      (r.nik1 = "'.$auth->id_karyawan.'" OR r.nik2 = "'.$auth->id_karyawan.'") AND
      (DATE(dt.tgl) = "'.date('Y-m-d').'")
    ORDER BY updated_at DESC
    ');
  }

  public static function checkOntPremium()
  {  
    $auth = session('auth');
    return DB::SELECT('
    SELECT
      opo.*,
      dt.jenis_layanan,
      dt.id as id_dt,
      pls.laporan_status,
      dps.orderDatePs,
      dps.orderKontak,
      dps.orderStatus,
      dps.orderAddr,
      dps.jenisPsb,
      pl.modified_at,
      pl.noPelangganAktif,
      r.uraian,
      r.id_regu
    FROM ont_premium_order opo
    LEFT JOIN dispatch_teknisi dt ON opo.no_inet = dt.NO_ORDER
    LEFT JOIN Data_Pelanggan_Starclick dps ON opo.no_inet = dps.internet AND (dps.orderStatus IN ("COMPLETED", "Completed (PS)") AND dps.jenisPsb LIKE "AO%")
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    WHERE
      dt.jenis_order = "ONT_PREMIUM" AND
      (r.nik1 = "'.$auth->id_karyawan.'" OR r.nik2 = "'.$auth->id_karyawan.'") AND
      (DATE(dt.tgl) = "'.date('Y-m-d').'")
    GROUP BY opo.no_inet
    ORDER BY updated_at DESC
    ');
  }

  public static function WorkOrder($id, $bulan)
  {  
    $datetime = date('Y-m-d H:i:s');
    $auth = session('auth');
    
    $limit = $orderBy = '';
      switch ($id) {
        case 'NEED_PROGRESS':
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
    				(b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL))';
          break;
        case 'NEED_PROGRESS_MANJA':
          $limit = 'LIMIT 2';
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Assigned_by = "CUSTOMERASSIGNED" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
    				(b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_IN_HVC_MIN_3JAM':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Source = "RIGHTNOW" AND dn1log.Customer_Type LIKE "HVC%" AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") >=0 AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") < 3 AND dn1log.Induk_Gamas = "" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_IN_REGULER_MIN_3JAM':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Source = "RIGHTNOW" AND (dn1log.Customer_Type NOT LIKE "HVC%" OR dn1log.Customer_Type = "") AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") >=0 AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") < 3 AND dn1log.Induk_Gamas = "" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_IN_HVC_MAX_3JAM':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Source = "RIGHTNOW" AND dn1log.Customer_Type LIKE "HVC%" AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") > 3 AND dn1log.Induk_Gamas = "" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_IN_REGULER_MAX_3JAM':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Source = "RIGHTNOW" AND (dn1log.Customer_Type NOT LIKE "HVC%" OR dn1log.Customer_Type = "") AND TIMESTAMPDIFF( HOUR , dn1log.Reported_Datex, "'.$datetime.'") > 3 AND dn1log.Induk_Gamas = "" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_INT':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Source = "TOMMAN" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'NEED_PROGRESS_IN_SQM':
          $Where = '
          WHERE
            (
            a.tgl = "'.date('Y-m-d').'" AND dn1log.Summary LIKE "%SQM%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan = 6 OR b.status_laporan IS NULL)) ';
          break;
        case 'PROV_UMUR_LAMA':
          $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY a.created_at ASC';
          $Where = '
          WHERE
            (
            (DATE(a.tgl) BETWEEN "'.date('Y-m-d' ,strtotime('-7 days')).'" AND "'.date('Y-m-d').'") AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR")';
          break;
        case 'NEED_PROGRESS_PROV':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
    				(b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_PROV_WEEKEND':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            dtjl.jenis_transaksi NOT IN ("PDA", "MO") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_PROV_PI':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND c.orderStatusId = "1202" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_PROV_FWFM':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND c.orderStatus = "Fallout (WFM)" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_PROV_MO_2P3P':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND (c.kcontact LIKE "%2p-3p%" OR c.kcontact LIKE "%add usee tv%") AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_PROV_MO_ADDON':
          // $limit = 'LIMIT 1';
          $orderBy = 'ORDER BY c.orderDate ASC';
          $Where = '
          WHERE
            (
            a.tgl LIKE "'.date('Y-m-d').'%" AND (c.kcontact LIKE "%2ndSTB%" OR c.kcontact LIKE "%Second set top box%" OR c.kcontact LIKE "%2NDSTBPLC%") AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (6) OR b.status_laporan IS NULL)) AND a.jenis_order IN ("SC","MYIR","IN","INT","MIGRASI_CCAN")';
          break;
        case 'NEED_PROGRESS_QC':
          $Where = '
          WHERE
            ((aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND a.jenis_layanan = "QC" AND b.status_laporan <> 1  ) OR (
            a.tgl LIKE "'.$bulan.'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL))';
          break;
        case 'NEED_PROGRESS_FFG':
          $Where = '
          WHERE
    				a.tgl LIKE "'.$bulan.'%" AND
            dn1log.Owner_Group = "WOC FULFILLMENT KALSEL (BANJARMASIN)" AND
    				(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
    				(b.id IS NULL OR b.status_laporan IN (5,6,4,7) OR b.status_laporan IS NULL)';
          break;
        case 'KENDALA':
          $Where = '
          WHERE
            a.tgl LIKE "%'.$bulan.'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
          break;
        case 'ONGOING':
          $Where = '
          WHERE
            a.tgl LIKE "%'.$bulan.'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            b.status_laporan IN (28,29)';
          break;
        case 'OKTARIK':
          $Where = '
          WHERE
            a.tgl LIKE "%'.$bulan.'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            bb.laporan_status = "OK TARIK"';
          break;
        case 'UP':
          $Where = '
          WHERE
            a.tgl LIKE "'.$bulan.'%" AND
            (aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND
            b.status_laporan IN (1,37,38)';
          break;
        default:
          $Where = '
          WHERE
            a.tgl LIKE "'.$bulan.'%" AND

            (b.id IS NULL OR b.status_laporan > 7 OR b.status_laporan = "")';
          break;
      }

      $result = PsbModel::QueryBuilder($Where, $orderBy, $limit);
      return $result;
  }

  private static function QueryBuilder($id, $orderBy, $limit)
  {
    return DB::select('
    SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        0 as jumlah_material,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        0 as ND,
        0 as duration,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        dn1log.Workzone as neSTO,
        dn1log.Datek as neRK,
        dn1log.Service_ID as neND_TELP,
        dn1log.Service_No as neND_INT,
        dn1log.Service_Type as nePRODUK_GGN,
        dn1log.Summary as neHEADLINE,
        dn1log.Owner_Group as neLOKER_DISPATCH,
        dn1log.Customer_Name as neKANDATEL,
        dn1log.Incident,
        dn1log.Assigned_by as by_Assigned,
        dn1log.Booking_Date as jam_manja,
        dn1log.Reported_Date as TROUBLE_OPENTIME,
        dn1log.Incident as status_gangguan,
        dn1log.Reported_Datex,
        dn1log.Induk_Gamas,
        0 as rebUmur,
        0 as alamatGGN,
        0 as historyPSB,
        dn1log.Service_No as ND_TELP,
        dn1log.Contact_Phone,
        dn1log.Customer_Name,
        dn1log.Reported_Date,
        dn1log.Contact_Name,
        dn1log.Customer_Type,
        0 as datek,
        dn1log.Incident as no_tiket,
        roc.no_tiket as roc_no_tiket,
        roc.headline as roc_headline,
        roc.customer_assign as roc_customer_assign,
        roc.ca_manja_time,
        roc.pelanggan_hvc,
        roc.ket_plasa,
        roc.ket_sqm,
        roc.ket_sales,
        roc.ket_helpdesk,
        roc.ket_monet,
        c.noTelp,
        c.internet,
        ps.kode_sales,
        ps.nama_sales,
        my.myir as myir,
        my.picPelanggan,
        my.customer,
        my.namaOdp,
        my.sto as stoMy,
        my.no_internet,
        my.approve_date,
        my.sto,
        my.no_telp,
        my.lat as latSales,
        my.lon as lonSales,
        my.koorPelanggan,
        mw.ND as mwnd,
        mw.created_at as mwcreated_at,
        mw.jenis_layanan as mwjl,
        mw.nama as mwnama,
        mw.MDF as mwmdf,
        mw.ODP as mwodp,
        mw.Alamat as mwalamat,
        mw.pic as mwpic,
        mw.layanan as mwlayanan,
        (SELECT dps.kcontact FROM Data_Pelanggan_Starclick dps WHERE a.NO_ORDER = dps.orderIdInteger AND dps.kcontact LIKE "%TREGVI%") as bundling_addon,
        kpt.group_channel
      FROM
        dispatch_teknisi a
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON a.jenis_layanan = dtjl.jenis_layanan
        LEFT JOIN data_nossa_1_log dn1log ON dn1log.ID = a.NO_ORDER AND a.jenis_order IN ("IN", "INT")
        LEFT JOIN data_nossa_1 dn1 ON dn1log.ID = dn1.ID AND a.jenis_order IN ("IN", "INT")
        LEFT JOIN psb_myir_wo my on (a.Ndem = my.myir or a.Ndem = my.sc)
        LEFT JOIN Data_Pelanggan_Starclick c ON a.NO_ORDER = c.orderIdInteger AND a.jenis_order = "SC"
        LEFT JOIN roc ON a.Ndem = roc.no_tiket AND a.jenis_order IN ("IN", "INT")
        LEFT JOIN micc_wo mw ON a.Ndem = mw.ND
        LEFT JOIN provi_kpro_tr6 kpt ON c.orderIdInteger = kpt.order_id AND a.jenis_order = "SC"
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN psb_sales ps ON ps.kode_sales = my.sales_id
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        '.$id.'
      GROUP BY a.Ndem
      '.$orderBy.'
      '.$limit.'
    ');
 }

 public static function woProv($ket){
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.session('auth')->id_karyawan.'" OR aa.nik2 = "'.session('auth')->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        my.customer,
        my.myir,
        my.picPelanggan,
        c.noTelp,
        c.internet
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        LEFT JOIN psb_myir_wo my on a.Ndem=my.myir
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function woReboundary($ket){
    $auth = session('auth');
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        c.orderName,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        c.ndemPots,
        c.ndemSpeedy,
        c.Kcontact as KcontactSC,
        c.jenisPsb,
        c.orderId,
        c.orderCity,
        c.sto as sto_dps,
        c.alproname,
        c.orderNcli,
        c.orderKontak,
        c.orderDate,
        c.lat,
        c.lon,
        c.email,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        my.*,
        c.noTelp,
        c.internet
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN Data_Pelanggan_Starclick c ON a.Ndem = c.orderId
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        LEFT JOIN reboundary_survey my on a.Ndem=my.myir
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function woMigrasi($ket){
    $auth = session('auth');
    $now  = date('Y-m-d H:n:s');
    $id_regu = '(aa.nik1 = "'.$auth->id_karyawan.'" OR aa.nik2 = "'.$auth->id_karyawan.'") AND';

    // 1 = need progress, 2 = kendala, 3 = up
    if ($ket==1){
        $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    (b.id IS NULL OR b.status_laporan IN (5,6,4,3,7) OR b.status_laporan IS NULL) ';
    }
    elseif ($ket==2){
      $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan NOT IN (5,6,1,4,29,28,37,38)';
    }
    elseif($ket==3){
       $Where   = 'WHERE a.tgl LIKE "'.date('Y-m').'%" AND
                    '.$id_regu.'
                    b.status_laporan IN (1,37,38)';
    }

    $sql = ' SELECT
        a.*,
        a.id as id_dt,
        b.id as id_pl,
        b.status_laporan as status_laporan,
        mg3.mdf_grup as grup_starclick,
        mg3.mdf_grup_id as grup_id_starclick,
        bb.laporan_status_id,
        bb.laporan_status,
        bb.laporan_status as status_wo_by_teknisi,
        mw.nd as ndmw,
        mw.pic,
        mw.nama as namamw,
        mw.alamat as alamatmw,
        mw.jenis_layanan as jnslayananmw,
      FROM
        dispatch_teknisi a
        LEFT JOIN regu aa ON a.id_regu = aa.id_regu
        LEFT JOIN micc_wo mw on a.Ndem = mw.ND
        LEFT JOIN mdf m3 ON c.sto = m3.mdf
        LEFT JOIN mdf_grup mg3 ON m3.mdf_grup_id = mg3.mdf_grup_id
        LEFT JOIN psb_laporan b ON a.id = b.id_tbl_mj
        LEFT JOIN psb_laporan_status bb ON b.status_laporan = bb.laporan_status_id
        '.$Where.'
      ';

    $query = DB::select($sql);
    dd($query);
 }

 public static function manualStarclickSave($req, $orderId){
  $auth = session('auth');
  $data = DB::table('Data_Pelanggan_Starclick')->where('orderId',$orderId)->first();

    if(count($data)==0){
        return DB::table('Data_Pelanggan_Starclick')->insert([
          'orderId'     => $req->orderId,
          'orderName'   => $req->orderName,
          'orderAddr'   => $req->orderAddr,
          'orderDate'   => $req->orderDate,
          'orderDatePs' => $req->orderDatePs,
          'orderCity'   => $req->orderCity,
          'orderStatus' => $req->orderStatus,
          'orderNcli'   => $req->orderNcli,
          'ndemSpeedy'  => $req->ndemSpeedy,
          'ndemPots'    => $req->ndemPots,
          'kcontact'    => $req->kcontact,
          'alproname'   => $req->alproname,
          'jenisPsb'    => $req->jenisPsb,
          'sto'         => $req->sto,
          'internet'    => $req->internet,
          'noTelp'      => $req->noTelp,
          'myir'        => $req->myir,
          'manual_by'   => $auth->id_karyawan
      ]);
      }
      else{
        return back()->with('alerts',[['type' => 'danger', 'text' => 'Data Sudah Ada !']]);
      }
  }

  public static function checkSCNew($sc){
    $check = DB::SELECT(' SELECT * FROM Data_Pelanggan_Starclick WHERE orderId = "'.$sc.'" ');

    return $check;
  }

 public static function orderMaintenance($order){
    // nik uji coba 98170106

    $auth = session('auth');
    if ($order == 'maintenance'){
        $status = " and (m.status is NULL OR m.status != 'close')";
        $condition = "(m.username1 = '".$auth->id_karyawan."' OR m.username2 = '".$auth->id_karyawan."' OR m.username3 = '".$auth->id_karyawan."' OR m.username4 = '".$auth->id_karyawan."')";

        $result = DB::select('
          SELECT m.*, pl.nama_odp, pl.kordinat_odp, m.nama_odp as nama_odp_m
          FROM maintaince m left join psb_laporan pl on m.psb_laporan_id = pl.id
          WHERE '.$condition.' '.$status.' order by m.id desc
        ');
    }
   return $result;
  }

  public static function getTambahTiang()
  {
      return DB::SELECT('SELECT id, ketTiang as text FROM tambahTiang ORDER BY id');
  }

  public static function value_search($search){
     return DB::table('Data_Pelanggan_Starclick')->where('orderId', 'like', '%' .$search. '%')->limit(5)->get();
  }

  public static function cekDataMaterial($id){
      return DB::table('psb_laporan_material')
          ->where('psb_laporan_id',$id)
          ->where('type','1')
          ->get();
  }

  public static function getIdWoFrormPsbid($psbId){
      return DB::table('dispatch_teknisi')
            ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
            ->select('dispatch_teknisi.id')
            ->where('psb_laporan.id',$psbId)
            ->first();
  }

  public static function cekDataMyirIfSc($id){
      return DB::table('dispatch_teknisi')
                ->leftJoin('psb_myir_wo','dispatch_teknisi.Ndem','=','psb_myir_wo.myir')
                ->select('psb_myir_wo.*', 'dispatch_teknisi.jenis_order')
                ->where('dispatch_teknisi.id',$id)
                ->first();
  }

  public static function listReboundary($tgl){
      return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status')
                ->whereDate('reboundary_survey.updated_at',$tgl)
                ->get();
  }

  public static function listReboundaryCari($scid){
      return DB::table('reboundary_survey')
                ->leftJoin('dispatch_teknisi','reboundary_survey.no_tiket_reboundary','=','dispatch_teknisi.Ndem')
                ->leftJoin('Data_Pelanggan_Starclick','reboundary_survey.scid','=','Data_Pelanggan_Starclick.orderId')
                ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                ->leftJoin('group_telegram','reboundary_survey.chat_sektor','=','group_telegram.chat_id')
                ->leftJoin('psb_laporan','dispatch_teknisi.id','=','psb_laporan.id_tbl_mj')
                ->leftJoin('psb_laporan_status','psb_laporan.status_laporan','=','psb_laporan_status.laporan_status_id')
                ->select('reboundary_survey.*','Data_Pelanggan_Starclick.*', 'regu.id_regu', 'regu.uraian', 'group_telegram.chat_id', 'group_telegram.title', 'psb_laporan.status_laporan','psb_laporan_status.laporan_status')
                ->where('reboundary_survey.scid',$scid)
                ->get();
  }

  public static function simpanReboundary($req, $nik, $status, $id=null){
      $timLama = DB::table('dispatch_teknisi')
                  ->leftJoin('regu','dispatch_teknisi.id_regu','=','regu.id_regu')
                  ->select('dispatch_teknisi.Ndem','regu.id_regu','regu.uraian')
                  ->where('dispatch_teknisi.Ndem',$req->scid)
                  ->first();

      $idTimLama = NULL;
      $uraianLama = NULL;
      if (count($timLama)<>0){
          $idTimLama  = $timLama->id_regu;
          $uraianLama = $timLama->uraian;
      };

      if ($status==1){
          return DB::table('reboundary_survey')->insert([
              'scid'            => $req->scid,
              'tarikan_lama'    => $req->tarikanLama,
              'koor_pelanggan'  => $req->koorPel,
              'koor_odp_lama'   => $req->koorOdpLama,
              'koor_odp_baru'   => $req->koorOdpBaru,
              'odp_lama'        => $req->odpLama,
              'odp_baru'        => $req->odpBaru,
              'estimasi_tarikan'=> $req->tarikanEstimasi,
              'created_by'      => $nik,
              'catatan'         => $req->catatan,
              'chat_sektor'     => $req->sektor,
              'idTimLama'       => $idTimLama,
              'uraianTimLama'   => $uraianLama
          ]);
      }
      else{
          return DB::table('reboundary_survey')->where('id',$id)->update([
              'scid'            => $req->scid,
              'tarikan_lama'    => $req->tarikanLama,
              'koor_pelanggan'  => $req->koorPel,
              'koor_odp_lama'   => $req->koorOdpLama,
              'koor_odp_baru'   => $req->koorOdpBaru,
              'odp_lama'        => $req->odpLama,
              'odp_baru'        => $req->odpBaru,
              'estimasi_tarikan'=> $req->tarikanEstimasi,
              'created_by'      => $nik,
              'catatan'         => $req->catatan,
              'chat_sektor'     => $req->sektor,
              'idTimLama'       => $idTimLama,
              'uraianTimLama'   => $uraianLama
          ]);
      }
  }

  public static function cekSCInDataStarclick($scid){
      return DB::table('Data_Pelanggan_Starclick')->where('orderId',$scid)->first();
  }

  public static function getDataReboundaryById($id){
      return DB::table('reboundary_survey')->where('id',$id)->first();
  }

  public static function getDataReboundaryByScid($scid){
      return DB::table('reboundary_survey')->where('scid',$scid)->first();
  }

  public static function updateNoTiket($scid, $notiket){
      return DB::table('reboundary_survey')->where('scid',$scid)->update(['no_tiket_reboundary' => $notiket]);
  }

  public static function getAkses($nik, $akses){
      return DB::table('psb_nik_scbe')->where('nik',$nik)->where('akses',$akses)->first();
  }

  public static function checktiket($id){
    $query = DB::table('
    SELECT
      dps.orderId,
      dps.orderNcli,
      dps.internet,
      dps.noTelp,
      dps.orderName,
      dps.orderStatus,
      dps.kcontact,
      dps.alproname,
      dps.jenisPsb,
      dps.sto,
      pmw.sc,
      pmw.no_internet,
      pmw.no_telp,
      pmw.customer,
      pmw.kcontack,
      dt.id as id_dt,
      r.uraian as tim,
      pl.kordinat_pelanggan,
      pl.catatan,
      pl.nama_odp,
      pl.kordinat_odp
    FROM dispatch_teknisi dt
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN Data_Pelanggan_Starclick dps ON dt.NO_ORDER = dps.orderIdInteger
    LEFT JOIN psb_myir_wo pmw ON dps.orderId = pmw.sc
    WHERE
      (dps.internet = "'.$id.'" OR
      dps.noTelp = "'.$id.'" OR
      pmw.no_internet = "'.$id.'" OR
      pmw.no_telp = "'.$id.'") AND
      dps.orderStatus IN ("Completed (PS)","COMPLETED")
      ORDER BY dps.orderDatePs DESC
      LIMIT 1
    ');

    return $query;
  }

  public static function multiWorkorder($type,$params)
  {
    if ($type == 'inet')
    {
      $where = 'AND dps.internet IN ('.$params.')
      GROUP BY dps.internet';
    } elseif ($type == 'voice') {
      $where = 'AND dps.noTelp IN ('.$params.')
      GROUP BY dps.noTelp';
    }

    return DB::SELECT('
    SELECT
      dps.orderId,
      dps.jenisPsb,
      dps.orderDate,
      dps.orderDatePs,
      dps.internet,
      dps.noTelp,
      pls.laporan_status,
      pl.noPelangganAktif,
      pmw.picPelanggan,
      dps.orderAddr,
      pmw.alamatLengkap,
      pl.kordinat_pelanggan as korpel_teknisi,
      dps.lat as korpel_lat_starclick,
      dps.lon as korpel_lon_starclick,
      pl.nama_odp as odp_teknisi,
      dps.alproname as odp_starclick,
      pl.modified_at,
      r.uraian as regu,
      ma.mitra_amija_pt as mitra,
      gt.title as sektor
    FROM Data_Pelanggan_Starclick dps
    LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
    LEFT JOIN psb_myir_wo pmw ON dt.Ndem = pmw.sc
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
    WHERE
    dps.jenisPsb LIKE "AO%"
    '.$where.'
    ORDER BY dps.orderDate DESC
    ');
  }

  public static function checkListMaterials($id)
  {
    return DB::SELECT('
    SELECT
    dt.Ndem as order_id,
    plm.id as id_plm,
    plm.id_item,
    plm.qty
    FROM dispatch_teknisi dt
    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
    WHERE
    plm.id_item_bantu IS NOT NULL AND
    dt.id = "'.$id.'"
    ORDER BY plm.psb_laporan_id DESC
    ');
  }

  public static function checkPickupOnline($id)
  {
    return DB::table('Data_Pelanggan_Starclick')
    ->leftJoin('dispatch_teknisi', 'Data_Pelanggan_Starclick.orderIdInteger', '=', 'dispatch_teknisi.NO_ORDER')
    ->select('Data_Pelanggan_Starclick.orderIdInteger', 'Data_Pelanggan_Starclick.orderStatus')
    ->where('Data_Pelanggan_Starclick.jenisPsb', 'like', 'AO%')
    ->where('Data_Pelanggan_Starclick.kcontact', 'not like', '%WMS%')
    ->where('Data_Pelanggan_Starclick.orderIdInteger', $id)
    ->orderBy('Data_Pelanggan_Starclick.orderDate', 'desc')
    ->first();
  }

  public static function validationPickupOnline($id)
  {
    return DB::table('Data_Pelanggan_Starclick')
    ->leftJoin('tacticalpro_tr6', 'Data_Pelanggan_Starclick.orderIdInteger', '=', 'tacticalpro_tr6.sc_id')
    ->leftJoin('dispatch_teknisi', 'Data_Pelanggan_Starclick.orderIdInteger', '=', 'dispatch_teknisi.NO_ORDER')
    ->select('Data_Pelanggan_Starclick.orderIdInteger', 'Data_Pelanggan_Starclick.orderStatus', 'tacticalpro_tr6.status_wo')
    ->where('Data_Pelanggan_Starclick.jenisPsb', 'like', 'AO%')
    ->where('Data_Pelanggan_Starclick.kcontact', 'not like', '%WMS%')
    ->whereIn('Data_Pelanggan_Starclick.orderStatusId', [1205, 1300, 1500])
    ->whereIn('tacticalpro_tr6.status_wo', ['COMPLETED (PS)', 'COMPLETED (PS) KPRO'])
    ->where('Data_Pelanggan_Starclick.orderIdInteger', $id)
    ->orderBy('Data_Pelanggan_Starclick.orderDate', 'desc')
    ->first();
  }

  public static function ba_controller_inbox($start, $end)
  {
    return DB::select('
    SELECT
    r.uraian as tim,
    gt.title as sektor,
    emp.nama as nama_req,
    bct.*,
    utt.*
    FROM ba_controller_tr6 bct
    LEFT JOIN 1_2_employee emp ON bct.request_by = emp.nik
    LEFT JOIN utonline_tr6 utt ON bct.sc_id = utt.scId_int AND utt.witel = "KALSEL (BANJARMASIN)"
    LEFT JOIN dispatch_teknisi dt ON bct.sc_id = dt.NO_ORDER AND dt.jenis_order = "SC"
    LEFT JOIN regu r ON dt.id_regu = r.id_regu
    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
    WHERE
    (DATE(bct.request_at) BETWEEN "' . $start .'" AND "' . $end . '")
    ');
  }

  public static function updateCollectNTE($id)
  {
    $data = DB::table('dispatch_teknisi')
    ->leftJoin('psb_laporan', 'dispatch_teknisi.id', '=', 'psb_laporan.id_tbl_mj')
    ->where('dispatch_teknisi.id', $id)
    ->select('dispatch_teknisi.id', 'dispatch_teknisi.NO_ORDER', 'psb_laporan.typeont', 'psb_laporan.snont', 'psb_laporan.typestb', 'psb_laporan.snstb', 'psb_laporan.type_plc', 'psb_laporan.sn_plc', 'psb_laporan.type_wifiext', 'psb_laporan.sn_wifiext', 'psb_laporan.type_indibox', 'psb_laporan.sn_indibox', 'psb_laporan.type_indihomesmart', 'psb_laporan.sn_indihomesmart')
    ->first();

    if (count($data) > 0)
    {

      print_r("\nstart insert nte ...\n\n");

      // delete previous data
      DB::table('collected_nte_dismantling')->where('id_dispatch', $data->id)->where('no_order', $data->NO_ORDER)->delete();

      if (!empty($data->snont))
      {
        // ONT
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'ONT',
          'merk_nte' => $data->typeont,
          'sn_nte' => $data->snont
        ]);
      }
      if (!empty($data->snstb))
      {
        // STB
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'STB',
          'merk_nte' => $data->typestb,
          'sn_nte' => $data->snstb
        ]);
      }
      if (!empty($data->sn_plc))
      {
        // PLC
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'PLC',
          'merk_nte' => $data->type_plc,
          'sn_nte' => $data->sn_plc
        ]);
      }
      if (!empty($data->sn_wifiext))
      {
        // WIFIEXT
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'WIFIEXT',
          'merk_nte' => $data->type_wifiext,
          'sn_nte' => $data->sn_wifiext
        ]);
      }
      if (!empty($data->sn_indibox))
      {
        // INDIBOX
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'INDIBOX',
          'merk_nte' => $data->type_indibox,
          'sn_nte' => $data->sn_indibox
        ]);
      }
      if (!empty($data->sn_indihomesmart))
      {
        // INDIHOMESMART
        DB::table('collected_nte_dismantling')->insert([
          'id_dispatch' => $data->id,
          'no_order' => $data->NO_ORDER,
          'type_nte' => 'INDIHOMESMART',
          'merk_nte' => $data->type_indihomesmart,
          'sn_nte' => $data->sn_indihomesmart
        ]);
      }
      print_r("done insert nte \n");
    } else {
      print_r("failed insert nte! \n");
    }
  }

  public static function cutOffRekon($startDate, $endDate)
  {
    return DB::select('
        SELECT
            dt.id as id_dispatch,
            dt.NO_ORDER as order_id,
            dps.noTelp as no_voice,
            dps.internet as no_inet,
            dps.orderName as nama_pelanggan,
            pl.nama_odp as odp,
            dps.alproname as odp2,
            dt.jenis_layanan_id as id_layanan,
            dtjl.alias as alias_layanan,
            dt.jenis_layanan as layanan,
            pls.laporan_status as status_teknisi,
            DATE(dps.orderDate) as tgl_wo,
            DATE(dps.orderDatePs) as tgl_ps,
            dps.orderAddr as address,
            r.id_regu as regu_id,
            r.uraian as regu_name,
            ma.mitra_amija_pt as mitra,
            dps.sto,
            gt.title as sektor,
            SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
            SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
            SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-75-NonAcc" THEN plm.qty ELSE 0 END) as precon75,
            SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
            SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-100-NonAcc" THEN plm.qty ELSE 0 END) as precon100,
            SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-150-NonAcc" THEN plm.qty ELSE 0 END) as precon150
        FROM Data_Pelanggan_Starclick dps
        LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
        WHERE
            (DATE(dps.orderDatePs) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND
            dps.orderStatusId = 1500 AND
            pl.status_laporan IN (1, 4) AND
            dt.jenis_order = "SC"
        GROUP BY dps.orderIdInteger
    ');
  }

  public static function checkAllOrder($nik)
  {
    return DB::select('
        SELECT
            dt.Ndem,
            dtjl.jenis_transaksi,
            dtjl.jenis_layanan,
            pls.laporan_status
        FROM dispatch_teknisi dt
        LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan_id = dtjl.jenis_layanan_id
        LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
        LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
        LEFT JOIN regu r ON dt.id_regu = r.id_regu
        LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
        WHERE
            (r.nik1 = "'.$nik.'" OR r.nik2 = "'.$nik.'") AND
            (pl.id IS NULL OR pl.status_laporan = 6) AND
            (DATE(dt.tgl) = "'.date('Y-m-d').'")
        ORDER BY dt.created_at ASC
    ');
  }

  public static function download_ps_ao($start, $end)
  {
    return DB::select('
      SELECT
          kpt.SPEEDY,
          gt.title as sektor,
          r.uraian as tim,
          ma.mitra_amija_pt as mitra,
          kpt.ORDER_ID,
          kpt.LAST_UPDATED_DATE,
          kpt.status_resume
      FROM kpro_tr6 kpt
      LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
      LEFT JOIN regu r ON dt.id_regu = r.id_regu
      LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
      LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
      WHERE
          (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$start.'" AND "'.$end.'") AND
          kpt.JENIS_PSB = "AO" AND
          kpt.status_resume = "Completed (PS)"
    ');
  }

  public static function redirectShowTiang($id)
  {
		return DB::table('dispatch_teknisi AS dt')
		->leftJoin('Data_Pelanggan_Starclick AS dps', 'dt.NO_ORDER', '=', 'dps.orderIdInteger')
		->leftJoin('psb_laporan AS pl', 'dt.id', '=', 'pl.id_tbl_mj')
		->leftJoin('psb_laporan_status AS pls', 'pl.status_laporan', '=', 'pls.laporan_status_id')
		->leftJoin('regu AS r', 'dt.id_regu', '=', 'r.id_regu')
		->leftJoin('group_telegram AS gt', 'r.mainsector', '=', 'gt.chat_id')
		->whereIn('dt.jenis_order', ['SC', 'MYIR'])
		->where('dt.id', $id)
		->select('pl.kordinat_pelanggan', 'pl.koordinat_tiang', 'r.uraian AS tim', 'gt.title AS sektor', 'dps.*')
		->first();
	}

	public static function ajx_tiang_all()
	{
		// return DB::select('
		// 	SELECT
		// 		id,
		// 		SUBSTRING_INDEX(SUBSTRING_INDEX(koordinat, ",", -2), ",", 1) as latitude,
		// 		SUBSTRING_INDEX(koordinat, ",", 1) as longitude,
		// 		odc,
		// 		dist
		// 	FROM master_tiang
		// ');

    return DB::table('pole_uim_ihld')->select('data_id', 'latitude', 'longitude')->get();

	}

  public static function ajx_odp_all()
  {
    return DB::table('odp_uim_ihld')->select('odp_name', 'latitude', 'longitude', 'occ_2')->get();
  }

  public static function saveLogSearchId($id, $nik, $name)
  {
    DB::table('log_search_inet')->insert([
      'search_id'       => $id,
      'created_by'      => $nik,
      'created_by_name' => $name
    ]);
  }

}