<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createtmpodp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmpodp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('divre');
            $table->string('witel');
            $table->string('cmdf');
            $table->string('rk');
            $table->string('dp');
            $table->string('no_speedy');
            $table->string('node_id');
            $table->string('node_ip');
            $table->string('slot');
            $table->string('port');
            $table->string('onu');
            $table->string('onu_desc');
            $table->string('onu_type');
            $table->string('onu_sn');
            $table->string('fiber_length');
            $table->string('olt_rx_power');
            $table->string('olt_rx_power_akhir');
            $table->string('onu_rx_power');
            $table->string('onu_rx_power_akhir');
            $table->string('tgl_ukur_akhir');
            $table->string('status');
            $table->string('alamat');
            $table->string('status_warranty');
            $table->string('tgl_pelaksanaan');
            $table->string('is_cabut');
            $table->string('is_kw1');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmpodp');
    }
}
