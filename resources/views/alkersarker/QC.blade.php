@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
.verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    transform: rotate(270deg);

}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:'';
    width:0;
    padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
    display:inline-block;
    vertical-align:middle;
    text-align : right;
}

</style>

<div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <?php
                    $jumlah = array();
                    ?>
                    @foreach ($get_alkersarkerx as $alkersarker)
                    <?php
                      $jumlah[$alkersarker->alkersarker_id] = 0;
                    ?>
                    <th><p>{{ $alkersarker->alkersarker }}</p></th>
                    @endforeach
                </tr>
                @foreach ($get_alkersarker_employee as $num => $alkersarker_employee)
                <tr>
                  <td>{{ ++$num }}</td>
                  <td>{{ $alkersarker_employee->nik }}</td>
                  <td>{{ $alkersarker_employee->nama }}</td>
                  @foreach ($get_alkersarkerx as $alkersarker)
                  <td>
                    <?php
                      if (array_key_exists($alkersarker->alkersarker_id,$result[$alkersarker_employee->nik])) $qty = $result[$alkersarker_employee->nik][$alkersarker->alkersarker_id]; else $qty = 0;
                      $jumlah[$alkersarker->alkersarker_id] += $qty;
                    ?>
                  {{ $qty }}</td>
                  @endforeach
                </tr>
                @endforeach
                <tr>
                  <th colspan="3">Total</th>
                  @foreach ($get_alkersarkerx as $alkersarker)
                  <th>{{ $jumlah[$alkersarker->alkersarker_id] }}</th>
                  @endforeach
                </tr>
            </table>
            </div>
        </div>
    </div>

@endsection
