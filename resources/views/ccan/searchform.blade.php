@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlistccan')

  <!-- dialog !-->
  <div>
    <form class="form-group" method="post" id="submit-form" >
      <label for="Search">Search</label>
      <input type="text" name="Search" class="form-control" id="Search" value="">
      <button class="btn form-control btn-primary">Search</button>
    </form>
  </div>


  @if ($data <> NULL)
  <br />
  <div >
  <div class="form-control">
    {{ $data }}. <br />
    Result ({{ $jumlah }}) Found.
    <br />
    <br />
    @if ($jumlah > 0)
    <div class="table-responsive">
    <table class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
  		    <th width="75" colspan=2>Work Order</th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $no => $data)
        <tr>
  		@if(session('auth')->level <> 19)
          <td colspan=2><span>
  		@if(!empty($data->id_r))
            <a href="/migrasi/dispatch/{{ $data->mw_nd }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
          	<span class="label label-warning">{{ $data->uraian }}</span>
            <span class="label label-default">{{ $data->modified_at }}</span>
          @else
            <a href="/migrasi/dispatch/{{ $data->mw_nd }}" class="label label-info">Dispatch</a>
          @endif
  	     	</span>
          <span><label class="label label-success">{{ $data->laporan_status }}</label></span>
          <span><a href="/sendsms/{{ $data->mw_nd }}" class="label label-info">Send SMS</a></span>
          @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 )
          <a href="/dispatch/delete/{{ $data->Ndem }}/{{ $data->id_dt }}" class="label label-danger" id="delete_sc" data-toggle="tooltip" >DELETE DISPATCH</a>
          <span class="label label-default">{{ $data->tgl }}</span>
          @if ($data->status_laporan == 74)
          <a href="/approveQC/{{ $data->pl_id }}/{{ $data->Ndem }}/{{ $data->id_r }}" class="label label-info">APPROVE QC</a>
          @endif
          @endif
          </td>
  		@endif
  		</tr>
  		<tr>
          <td>
    			@if ($data->id_dt<>'')
    			<a href="/{{ $data->id_dt }}">{{ $data->Ndem }}</a>
    			@else
    			{{ $data->Ndem ? : 'Tidak Ada SC' }}
    			@endif
          <br />
    	        {{ $data->mw_nd ? : $data->ND ? : '~' }}<br />
    	        {{ $data->mw_nama ? : '~' }}<br />
    	        {{ $data->mw_mdf ? : '~' }} /
    	        {{ $data->mw_alamat ? : '~' }} <br />
    	        {{ $data->mw_pic ? : '~' }} <br />
    	        {{ $data->mw_odp ? : $data->nama_odp ? : 'Tidak Ada Informasi ODP' }} <br />
              JENIS LAYANAN : {{ $data->mw_jl ? : '~' }} / {{ $data->mw_layanan ? : '~' }}<br />
              {{ $data->loker }} / {{ $data->mw_jt }}
          </td>
  	    </tr>
  			<tr>
      		<td valign="top">
      	        Catatan Teknisi : <br />
      	        {{ $data->catatan ? : 'Tidak ada' }}

      		</td>
        </tr>
        @endforeach
      </tbody>
    </table>
   <br />
   </div>
    @endif

  </div>
  </div>

  @endif

</div>

<br />
<br />
  <script>
  $(document).ready(function(){
    $('#submit-form').submit(function() {

    });
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection