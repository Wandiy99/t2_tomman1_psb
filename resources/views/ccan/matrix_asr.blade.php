@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>MATRIX ASSURANCE CCAN KALSEL</h3>
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .label-KP {
    background-color: #ff6b6b;
  }
  .label-KT {
    background-color: #0abde3;
  }
  .label-UP {
    background-color: #1dd1a1;
  }
  .label-OGP {
    background-color: #feca57;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-NP {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }
  .underline {
    text-decoration: underline;
  }
  .gaul {
    border: 2px solid #606060;
  }
  .label-bluepas {
      background-color : #fd79a8;
    }
  </style>
  <div class="row">
    <div class="col-sm-12">
      @foreach ($getSektor as $sektor)
      <div class="panel panel-primary">
          <div class="panel-heading">{{ $sektor->title }}</div>
          <div class="panel-body">
            <table width="100%" class="table table-striped table-bordered">
              <tr>
                <th width="300">TIM</th>
                <th>LIST ORDER</th>
                <th width="50">NP</th>
                <th width="50">OGP</th>
                <th width="50">KT</th>
                <th width="50">KP</th>
                <th width="50">UP</th>
              </tr>
              <?php
                $total_UP = 0;
                $total_KT = 0;
                $total_KP = 0;
                $total_OGP = 0;
                $total_NP = 0;
              ?>
              @foreach ($matrix[$sektor->chat_id] as $team)
              @if (@$team->uraian<>"")
              <tr>
                <td>{{ $team->uraian }}</td>
                <td>
                  <?php 
                    $UP = 0;
                    $KT = 0;
                    $KP = 0;
                    $OGP = 0;
                    $NP = 0;
                  ?>
                  @foreach (@$matrix[@$sektor->chat_id][@$team->id_regu] as $num => $team_order)
                  <?php 
                    if (@$team_order->laporan_status=="UP") $UP += 1;
                    if (@$team_order->grup=="KT") $KT += 1;
                    if (@$team_order->grup=="KP") $KP += 1;
                    if (@$team_order->grup=="OGP") $OGP += 1;
                    if (@$team_order->grup=="SISA") $NP += 1;
                    if (@$team_order->grup=="") $NP += 1;
                  ?>

                  <?php
                  if (@$team_order->Assigned_by=="CUSTOMERASSIGNED") {
                    $underLine = 'underline';
                  } else {
                    $underLine = '';
                  }
                  if (@$team_order->GAUL>0) {
                    $gaul = 'gaul';
                  } else {
                    $gaul = '';
                  }


                  $labelWarna = $team_order->grup;
                  if ($team_order->laporan_status=="KIRIM TEKNISI"){
                      $labelWarna = 'bluepas';
                  };

                  date_default_timezone_set('Asia/Makassar');
                  if ($team_order->modified_at=='' || $team_order->modified_at==NULL){
                    $jamWo = $team_order->updated_at;
                  }
                  else{
                    $jamWo = $team_order->modified_at;
                  };

                  if ($jamWo<>''){
                      $awal  = date_create($jamWo);
                      $akhir = date_create(); // waktu sekarang
                      $diff  = date_diff( $awal, $akhir );
                                          
                      if($diff->d <> 0){
                            $waktu = ' // '.$diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                      }
                      else {
                            $waktu = ' // '.$diff->h.' Jam | '.$diff->i.' Menit'; 
                      }
                  }
                  else{
                    $waktu = ' //';
                  }

                  ?>
                  <a class="label label-{{ $labelWarna }} {{ $underLine }} {{ $gaul }}" data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ @$team_order->Reported_Date}}<br /><b>Dispatch at</b> :<br />{{ @$team_order->updated_at }}<br /><b>Status</b> : <br />{{ @$team_order->laporan_status }} / <small></small><br /><b>Catatan Teknisi</b> : <br />{{@$team_order->catatan }}<br /><a href='/tiket/{{ @$team_order->id_dt }}'>Detil</a>">
                  #{{ ++$num }} {{ @$team_order->Ndem }} // {{ @$team_order->laporan_status ? : 'NEED PROGRESS' }} // {{ $waktu }} 
                   
                    <?php
                    if ($team_order->laporan_status_id == 1 && $team_order->Assigned_by=="CUSTOMERASSIGNED") {
                      echo "(".$team_order->is_3HS.")";
                    } else {
                      if ($team_order->is_12HS>0) {
                        echo "(".$team_order->is_12HS.")";
                      } 
                    }
                    ?>
                    <?php
                    if ($team_order->GAUL>0) { echo " // GAUL"; } else { echo ""; }
                    ?> // {{ $team_order->Reported_Date }}</a>
                    @if ($team_order->Source=="PROACTIVE_TICKET")
                     <span class="label label-danger">UNSPEC</span>
                    @endif
                    <br />
                  @endforeach
                </td>
                <td>{{ $NP }}</td>
                <td>{{ $OGP }}</td>
                <td>{{ $KT }}</td>
                <td>{{ $KP }}</td>
                <td>{{ $UP }}</td>
                <?php
                  $total_NP += $NP;
                  $total_OGP += $OGP;
                  $total_KT += $KT;
                  $total_KP += $KP;
                  $total_UP += $UP;
                ?>
              </tr>
              @endif
              @endforeach
              <tr>
                <th colspan="2">TOTAL</th>
                <th>{{ $total_NP }}</th>
                <th>{{ $total_OGP }}</th>
                <th>{{ $total_KT }}</th>
                <th>{{ $total_KP }}</th>
                <th>{{ $total_UP }}</th>
              </tr>
            </table>
          </div>
      </div>
      @endforeach
    </div>
  </div>
  <!-- <script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

        })


        var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script> -->
@endsection