@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
    }
    td {
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
      background-color: #FFF;
    }
    .warna1 {
      background-color: #2874A6;
    }
    .warna2 {
      background-color: #2E86C1;
    }
    .warna3 {
      background-color: #D6EAF8;
    }
    .warna4 {
      background-color: #E67E22;
      #F0B27A
    }
    .warna4x {
      background-color: #F0B27A;
      #FAE5D3
    }
    .warna4z {
      background-color: #FAE5D3;
    }
    .colorArea {
      background-color: #F4D03F;
    }
    .colorArea2 {
      background-color: #FCF3CF;
    }
    .colorArea3 {
      background-color: #F7DC6F;
    }
    .colorComparin {
      background-color: #48C9B0;
    }
    .colorComparin2 {
      background-color: #D1F2EB;
    }
    .colorComparin3 {
      background-color: #76D7C4;
    }
    .text2 {
      color: black !important;
    }
    .text1 {
      color: white !important;
    }
    .text1:link {
      color: white !important;
    }
    .text1:visited {
      color: white !important;
    }
    th a:link {
      color: white;
    }
    th a:visited {
      color: white;
    }
    .overload {
      background-color: red;
      color: white;
    }
    .merahx {
      background-color: #C0392B;
      color: white !important;
    }
    .hijaux {
      background-color:#1ABC9C;
      color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 4px 0px !important;
    }
    .color-green {
      background-color: #1ABC9C;
    }
    .color-black {
      background-color: #2C3E50;
    }
    .color-red {
      background-color: #E74C3C;
    }
    .color-yellow {
      background-color: #F39C12;
    }
    .color-blues {
      background-color: #154360;
    }
    .txt-white {
        color: white !important;
    }
    .smallertd td,th {
      font-size: 10px;
    }
  </style>
  <h3>Dashboard Provisioning {{ $month }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="white-box">
        <h3 class="box-title">RE HARIAN KALSEL</h3>
        <ul class="list-inline text-right">
          <li><h5><i class="fa fa-circle m-r-5" style="color: #48C9B0;"></i>Banjarbaru</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>Banjarmasin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>Batulicin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Tanjung</h5></li>
        </ul>
      <div class="" id="chart_re" style="height:200">
      </div>
      </div>
      <table class="table">
        <tr>
          <th width="80">DATEL</th>
          <?php
            $jml = $maxDay;
            for ($x=1;$x<=$jml;$x++)
            {
              ?>
              <th>{{ $x }}</th>
              <?php
            }
            $total = 0;
          ?>
        </tr>
        @foreach ($get_datel as $result)
        <tr>
          <td>{{ $result->DATEL }}</td>
          <?php
          for ($x=1;$x<=$maxDay;$x++){
          ?>
           <td>{{ @$get_re[$result->DATEL][$x] }}</td>
          <?php
          }
          ?>
        </tr>
        @endforeach
        <tr>
          <th>TOTAL</th>
          <?php
          for ($x=1;$x<=$maxDay;$x++) {
          ?>
          <th>{{ @$get_re_all[$x] }}</th>
          <?php
          }
          ?>
        </tr>
      </table>
    </div>
    <div class="col-sm-12">
      <div class="white-box">
        <h3 class="box-title">PS HARIAN KALSEL</h3>
        <ul class="list-inline text-right">
          <li><h5><i class="fa fa-circle m-r-5" style="color: #48C9B0;"></i>Banjarbaru</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>Banjarmasin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>Batulicin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Tanjung</h5></li>
        </ul>
      <div class="" id="chart_ps" style="height:200">
      </div>
      </div>
      <table class="table">

          <tr>
            <th width="80">DATEL</th>
            <?php
              $jml = $maxDay;
              for ($x=1;$x<=$jml;$x++)
              {
                ?>
                <th>{{ $x }}</th>
                <?php
              }
              $total = 0;
            ?>
          </tr>

        @foreach ($get_datel as $result)
        <tr>
          <td>{{ $result->DATEL }}</td>
          <?php
          for ($x=1;$x<=$maxDay;$x++) {
          ?>
          <td><?php
            if ($x==3 AND $result->DATEL=="BATULICIN" AND $month=="2022-10") {
              echo "0";
            } else {
          ?>
            {{ @$get_ps[$result->DATEL][$x] }}
          <?php } ?>
          </td>
          <?php
          }
          ?>
        </tr>
        @endforeach
        <tr>
          <th>TOTAL</th>
          <?php
          for ($x=1;$x<=$maxDay;$x++) {
          ?>
          <th>{{ @$get_ps_all[$x] }}</th>
          <?php
          }
          ?>
        </tr>
      </table>
    </div>
    <div class="col-sm-12">
      <div class="white-box">
        <h3 class="box-title">PRODUKTIFITAS TEKNISI KALSEL</h3>
        <ul class="list-inline text-right">
          <li><h5><i class="fa fa-circle m-r-5" style="color: #48C9B0;"></i>Banjarbaru</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>Banjarmasin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>Batulicin</h5></li>
          <li><h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Tanjung</h5></li>
        </ul>
      <div class="" id="chart_prod_teknisi" style="height:200">
      </div>
      </div>
      <table class="table">
          <tr>
            <th  width="80">DATEL</th>
            <?php
              $jml = $maxDay;
              for ($x=1;$x<=$jml;$x++)
              {
                ?>
                <th>{{ $x }}</th>
                <?php
              }
              $total = 0;
            ?>
          </tr>
          <tr>
            @foreach ($get_datel as $result)
            <tr>
              <td>{{ $result->DATEL }}</td>
              <?php
              for ($x=1;$x<=$maxDay;$x++) {
                $xx = @($get_ps[$result->DATEL][$x]/$teknisi[$result->DATEL][$x])/2;
              ?>
              <td>
              <?php
                if ($x==3 AND $result->DATEL=="BATULICIN" AND $month=="2022-10") {
                  echo "0";
                } else {
              ?>

              {{ round($xx,2) }}
              <?php
            }
              }
              ?>
              </td>
            </tr>
            @endforeach
            <tr>
              <th>TOTAL</th>
              <?php
              for ($x=1;$x<=$maxDay;$x++){
                ?>
                <th>{{ @str_replace(".",",",(round((@$get_ps_all[$x]/@$teknisi_all[$x]/2),2))) }}</th>
                <?php
              }
              ?>
            </tr>
          </table>
          </div>
          <div class="col-sm-12">
            <div class="white-box">
              <h3 class="box-title">TREND PS/RE HARIAN KALSEL</h3>

              <ul class="list-inline text-right">
                <li>AVG : </li>
                <li>MIN : </li>
                <li>MAX : </li>
              </ul>
              <ul class="list-inline text-right">
                <li><h5><i class="fa fa-circle m-r-5" style="color: #48C9B0;"></i>Banjarbaru</h5></li>
                <li><h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>Banjarmasin</h5></li>
                <li><h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>Batulicin</h5></li>
                <li><h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Tanjung</h5></li>
              </ul>
            <div class="" id="chart_psre" style="height:200">
            </div>
            </div>
            <table class="table smallertd">
                <tr>
                  <th  width="80">DATEL</th>
                  <?php
                    $jml = $maxDay;
                    for ($x=1;$x<=$jml;$x++)
                    {
                      ?>
                      <th>{{ $x }}</th>
                      <?php
                    }
                    $total = 0;
                  ?>
                  <th>AVG</th>
                </tr>
                <?php $average = array(); ?>
                <?php foreach ($get_datel as $datel) {
                $average[$datel->DATEL] = 0;
                } ?>
                @foreach ($get_datel as $datel)
                <tr>
                  <?php $dayCount = 0;
                   ?>
                  <td>{{ $datel->DATEL }}</td>
                  @for ($x=1;$x<=$maxDay;$x++)
                  <td>
                  @if (@is_array(@$get_psre_completed[$datel->DATEL][$x])==true)
                  @else
                  <?php
                    if (is_nan(@((@$get_psre_completed[$datel->DATEL][$x]/@$get_re[$datel->DATEL][$x])*100))) {
                      $jml = 0;
                    } else {
                      $jml = @((@$get_psre_completed[$datel->DATEL][$x]/@$get_re[$datel->DATEL][$x])*100);
                      $dayCount += 1;
                    }
                    $average[$datel->DATEL] += $jml;
                  ?>
                  {{ @round(@$jml,1) }}%
                  @endif
                  </td>

                  @endfor
                  <td>{{ round(($average[$datel->DATEL]/$dayCount),2) }} - {{ $dayCount }}</td>
                </tr>
                @endforeach
                <tr>
                  <th>TOTAL</th>
                  <?php
                    $total = 0;
                  ?>
                  @for ($x=1;$x<=$maxDay;$x++)
                  <?php
                    if (is_nan(@((@$get_psre_completed_all[$x]/@$get_re_all[$x])*100))){
                      $jml = 0;
                    } else {
                      $jml = ((@$get_psre_completed_all[$x]/@$get_re_all[$x])*100);
                    }
                    $total += $jml;
                  ?>
                  <th>{{ @round($jml,1) }}%</th>
                  @endfor
                  <th>{{ round(($total/$dayCount),2) }}</th>
                </tr>
              </table>
            </div>
  </div>
  <script>
  $(function() {
    new Morris.Line({
      element: 'chart_re',
      data: [
        <?php for ($x=1;$x<=$maxDay;$x++) {

        ?>
        { Tanggal: '{{ $month }}-{{ $x }}', Banjarbaru: {{ @$get_re['BANJARBARU'][$x] ? : 0 }}, Banjarmasin: {{ @$get_re['BJRMASIN'][$x] ? : 0 }}, Batulicin: {{ @$get_re['BATULICIN'][$x] ? : 0 }}, Tanjung: {{ @$get_re['TANJUNG'][$x] ? : 0 }} },
        <?php } ?>
      ],
      behaveLikeLine: true,
      gridLineColo8r: '#e0e0e0',
      lineWidth: 2,
      hideHover: 'false',
      fillOpacity: 0,
      pointStrokeColors:['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      lineColors: ['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      resize: true,
      xkey: 'Tanggal',
      ykeys: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung'],
      labels: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung']
    });
    new Morris.Line({
      element: 'chart_ps',
      data: [
        <?php for ($x=1;$x<=$maxDay;$x++) { ?>
        { Tanggal: '{{ $month }}-{{ $x }}', Banjarbaru: {{ @$get_ps['BANJARBARU'][$x] ? : 0 }}, Banjarmasin: {{ @$get_ps['BJRMASIN'][$x] ? : 0 }}, Batulicin: {{ @$get_ps['BATULICIN'][$x] ? : 0 }}, Tanjung: {{ @$get_ps['TANJUNG'][$x] ? : 0 }} },
        <?php } ?>
      ],
      behaveLikeLine: true,
      gridLineColo8r: '#e0e0e0',
      lineWidth: 2,
      hideHover: 'false',
      fillOpacity: 0,
      pointStrokeColors:['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      lineColors: ['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      resize: true,
      xkey: 'Tanggal',
      ykeys: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung'],
      labels: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung']
    });

    new Morris.Bar({
      element: 'chart_prod_teknisi',
      data: [
        <?php for ($x=1;$x<=$maxDay;$x++) {
          $banjarbaru = @($get_ps['BANJARBARU'][$x]/@$teknisi['BANJARBARU'][$x])/2;
          $banjarmasin = @($get_ps['BJRMASIN'][$x]/@$teknisi['BJRMASIN'][$x])/2;
          $batulicin = @($get_ps['BATULICIN'][$x]/@$teknisi['BATULICIN'][$x])/2;
          $tanjung = @($get_ps['TANJUNG'][$x]/@$teknisi['TANJUNG'][$x])/2;
          if (is_nan($banjarbaru)) { $banjarbaru = 0; }
          if (is_nan($banjarmasin)) { $banjarmasin = 0; }
          if (is_nan($batulicin)) { $batulicin = 0; }
          if (is_nan($tanjung)) { $tanjung = 0; }
          ?>
        { Tanggal: '{{ $month }}-{{ $x }}', Banjarbaru: {{ @$banjarbaru ? : 0 }}, Banjarmasin: {{ @$banjarmasin ? : 0 }}, Batulicin: {{ @$batulicin ? : 0 }}, Tanjung: {{ @$tanjung ? : 0 }} },
        <?php } ?>
      ],
      behaveLikeLine: true,
      gridLineColo8r: '#e0e0e0',
      lineWidth: 2,
      hideHover: 'false',
      fillOpacity: 0,
      pointStrokeColors:['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      barColors: ['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      resize: true,
      xkey: 'Tanggal',
      ykeys: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung'],
      labels: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung']
    });
    new Morris.Bar({
      element: 'chart_psre',
      data: [
        <?php for ($x=1;$x<=$maxDay;$x++) {
          $banjarbaru = @(($get_psre_completed['BANJARBARU'][$x]/@$get_re['BANJARBARU'][$x]))*100;
          $banjarmasin = @(($get_psre_completed['BJRMASIN'][$x]/@$get_re['BJRMASIN'][$x]))*100;
          $batulicin = @(($get_psre_completed['BATULICIN'][$x]/@$get_re['BATULICIN'][$x]))*100;
          $tanjung = @(($get_psre_completed['TANJUNG'][$x]/@$get_re['TANJUNG'][$x]))*100;
          if (is_nan($banjarbaru)) { $banjarbaru = 0; }
          if (is_nan($banjarmasin)) { $banjarmasin = 0; }
          if (is_nan($batulicin)) { $batulicin = 0; }
          if (is_nan($tanjung)) { $tanjung = 0; }
          ?>
        { Tanggal: '{{ $month }}-{{ $x }}', Banjarbaru: {{ @$banjarbaru ? : 0 }}, Banjarmasin: {{ @$banjarmasin ? : 0 }}, Batulicin: {{ @$batulicin ? : 0 }}, Tanjung: {{ @$tanjung ? : 0 }} },
        <?php } ?>
      ],
      behaveLikeLine: true,
      gridLineColo8r: '#e0e0e0',
      lineWidth: 2,
      hideHover: 'false',
      fillOpacity: 0,
      pointStrokeColors:['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      barColors: ['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
      resize: true,
      xkey: 'Tanggal',
      ykeys: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung'],
      labels: ['Banjarbaru','Banjarmasin','Batulicin','Tanjung']
    });

  });
  </script>
@endsection
