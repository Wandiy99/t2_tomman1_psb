@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    #table td,th {
      padding : 5px;
    }
  </style>
    <script src="/js/dropzone.js"></script>
  <link rel="stylesheet" href="/css/dropzone.min.css">
  <center>
    <h3>FORM Penilaian Praktek</h3>
    <h3>Brevet Provisioning Type-1</h3>
  </center>

    <form method="post">
      <input type="hidden" name="NIK" value="{{ $nik }}">
  <div class="row">

    <div class="col-sm-12">
      <table>
        <tr>
          <td>Nama / NIK</td>
          <td width="20" align="center">:</td>
          <td>{{ $data->nama }} / {{ $data->nik }}</td>
        </tr>
        <tr>
          <td>Regional</td>
          <td  align="center">:</td>
          <td>Kalimantan</td>
        </tr>
        <tr>
          <td>Tgl Tes</td>
          <td  align="center">:</td>
          <td>
            <input type="date" name="tgl" value="">
          </td>
        </tr>
        <tr>
          <td>Waktu Mulai & Selesai</td>
          <td  align="center">:</td>
          <td>
            <input type="time" name="waktuMulai" value="">
            <input type="time" name="waktuSelesai" value="">
          </td>
        </tr>
        <tr>
          <td>Assessor</td>
          <td>:</td>
          <td><input type="text" name="assessor" value="{{ session('auth')->id_karyawan }}"></td>
        </tr>
      </table>
      <br />
    </div>
    <div class="col-sm-12">
      <table border=1 width="100%" id="table">
        <tr>
          <th width=10>NO</th>
          <th>URAIAN PEKERJAAN</th>
          <th width="20">BOBOT</th>
          <th width="20">RANGE NILAI</th>
          <th>NILAI</th>
          <th>KETERANGAN CATATAN</th>
        </tr>
        <tr>
          <td><b>1.</b></td>
          <td><b>Tes Kelengkapan</b></td>
          <td><b>15</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><b>Material</b></td>
          <td></td>
          <td>5</td>
          <td width="40">
            <input type="text" name="nilaiMaterial" placeholder data-mask="9" value="">
          </td>
          <td width="40">
            Lengkap 5, tidak lengkap 0
          </td>
        </tr>

        <tr>
          <td></td>
          <td>a. Dropcore Cable</td>
          <td></td>
          <td></td>
          <td></td>
          <td width="40">
            <input type="text" name="ketMaterialDropcore" value="">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>b. Splice On Connector (SOC)</td>
          <td></td>
          <td></td>
          <td></td>
          <td width="40">
            <input type="text" name="ketMaterialSOC" value="">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>c. S-Clamp</td>
          <td></td>
          <td></td>
          <td></td>
          <td width="40">
            <input type="text" name="ketMaterialSClamp" value="">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>d. One Click Cleaner</td>
          <td></td>
          <td></td>
          <td></td>
          <td width="40">
            <input type="text" name="ketOneClickCleaner" value="">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>e. Kabel Ties / Tie Wrap</td>
          <td></td>
          <td></td>
          <td></td>
          <td width="40">
            <input type="text" name="ketKabelTies" value="">
          </td>
        </tr>
        </tr>
        <tr>
          <td></td>
          <td><b>Perlengkapan & Peralatan</b></td>
          <td></td>
          <td>10</td>
          <td width="40">
            <input type="text" placeholder data-mask="99" name="nilaiPerlengkapan" value="">
          </td>
          <td width="40">
            Lengkap 10, tidak lengkap 0
          </td>
        </tr>
        <tr>
          <td></td>
          <td>a. Lakban</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketLakban" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>b. Tang Potong</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketTangPotong" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>c. Gunting / Cutter</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketGuntingCutter" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>c. Tangga Telescopic</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketTanggaTelescopic" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>d. OPM</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketOPM" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>d. OLS</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketOLS" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>e. VFL</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketVFL" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>f. Safety Helmet</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketSafetyHelmet" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>g. Safety Glove</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketSafetyGlove" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>h. Body Harness / Safety Belt</td>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <input type="text" name="ketSafetyBelt" value="">  
          </td>
        </tr>
        <tr>
          <td><b>2.</b></td>
          <td><b>Tes Standar Pemasangan</b></td>
          <td><b>30</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>a. Pemasangan S-Klem dilengkapi dengan stopper dan Breket</td>
          <td></td>
          <td>3/6/10</td>
          <td>
            <input type="text" name="nilaiSklem" placeholder data-mask="99" value="">  
          </td>
          <td>
            <input type="text" name="ketSklem" value="">  
          </td>
        </tr>
        <tr>
          <td></td>
          <td>b. Pemasangan Dropcore ke OTP</td>
          <td></td>
          <td>1-10</td>
          <td>
            <input type="text" name="nilaiOTP" placeholder data-mask="99" value="">  
          </td>
          <td>
            <input type="text" name="ketOTP" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>c. Pemasangan Dropcore ke ODP</td>
          <td></td>
          <td>1-10</td>
          <td>
            <input type="text" name="nilaiODP" placeholder data-mask="99" value="">  
          </td>
          <td>
            <input type="text" name="ketODP" value="">  
          </td>          
        </tr>
        <tr>
          <td><b>3.</b></td>
          <td><b>Tes Redaman</b></td>
          <td><b>15</b></td>
          <td></td>
          <td>
            <input type="text" name="nilaiODP" placeholder data-mask="9" value="">  
          </td>
          <td>
            <input type="text" name="ketODP" value="">  
          </td>          
         </tr>
        <tr>
          <td></td>
          <td>a. Apabila redaman SOC dibawah 0,7dB mendapat 15 poin</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>b. Apabila redaman SOC antara 0,7dB - 0,9dB mendapat 10 poin</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>c. Apabila redaman SOC di atas 0,9dB mendapat 0 poin</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><b>4.</b></td>
          <td><b>Tes Kebersihan dan K3</b></td>
          <td><b>15</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>a. Kerapian sisa potongan dropcore dan sampah plastik</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiPotonganDropcore" value="">  
          </td>
          <td>
            <input type="text" name="ketPotonganDropcre" value="">  
          </td> 
        </tr>
        <tr>
          <td></td>
          <td>b. Pemakaian APD</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiPemakaianAPD" value="">  
          </td>
          <td>
            <input type="text" name="ketPemakaianAPD" value="">  
          </td> 
        </tr>
        <tr>
          <td></td>
          <td>c. Kerapihan dan kebersihan peralatan setelah praktek</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiKebersihanPeralatan" value="">  
          </td>
          <td>
            <input type="text" name="ketKebersihanPeralatan" value="">  
          </td> 
        </tr>
        <tr>
          <td><b>5.</b></td>
          <td><b>Tes Code of Conduct</b></td>
          <td><b>30</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>a. Mengucapkan Salam</td>
          <td></td>
          <td>1</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiSalam" value="">  
          </td>
          <td>
            <input type="text" name="ketSalam" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>b. Memperkenalkan Diri</td>
          <td></td>
          <td>1</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiMemperkenalkanDiri" value="">  
          </td>
          <td>
            <input type="text" name="ketMemperkenalkanDiri" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>c. Menjelaskan maksud dan tujuan</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiMaksuddanTujuan" value="">  
          </td>
          <td>
            <input type="text" name="ketMaksuddanTujuan" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>d. Menggunakan ID Card</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiSeragamIndihome" value="">  
          </td>
          <td>
            <input type="text" name="ketSeragamIndihome" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>e. Menggunakan Seragam Indihome</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiSeragamIndihome" value="">  
          </td>
          <td>
            <input type="text" name="ketSeragamIndihome" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>f. Rambut Rapi (3 2 1)</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiRambut" value="">  
          </td>
          <td>
            <input type="text" name="ketRambut" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>g. Pengetahuan Produk</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiPengetahuanProduk" value="">  
          </td>
          <td>
            <input type="text" name="ketPengetahuanProduk" value="">  
          </td>          
        </tr>
        <tr>
          <td></td>
          <td>h. Tidak menerima tips dari pelanggan</td>
          <td></td>
          <td>2</td>
          <td>
            <input type="text" placeholder data-mask="9" name="nilaiTips" value="">  
          </td>
          <td>
            <input type="text" name="ketTips" value="">  
          </td>          
        </tr>
      </table>
    </div>
   
    
  </div>

   <div class="col-sm-12">
      <input type="submit" class="btn btn-success form-control" name="submit" value="Simpan">
    </div>
  </form>
    <div class="col-sm-12">
    <div class="panel panel-default">
                <div class="panel-heading">Input Foto</div>
                <div class="panel-body">

                    <form method="post" action="/rider/{{ $nik }}/upload" enctype="multipart/form-data" class="dropzone" id="dropzone">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    </form>   

                </div>
            </div>
  </div>
  <script type="text/javascript">
        Dropzone.options.dropzone =
     {
        maxFilesize: 12,
        renameFile: function(file) {
            var dt = new Date();
            var time = dt.getTime();
           return time+file.name;
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 50000,
        removedfile: function(file) 
        {
            var name = file.upload.filename;
            var id   = document.getElementById('idLaporan').value;

            console.log(id);
            $.ajax({
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                type: 'POST',
                url: '/uploadFoto/'+id+'/removed',
                data: {filename: name},
                success: function (data){
                    console.log("File has been successfully removed!!");
                },
                error: function(e) {
                    console.log(e);
                }});
                var fileRef;
                return (fileRef = file.previewElement) != null ? 
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
   
        success: function(file, response) 
        {
            console.log(response);
        },
        error: function(file, response)
        {
           return false;
        }
    };
  </script>
@endsection
