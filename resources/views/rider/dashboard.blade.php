@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }

    .text1:visited{
      color: white !important;
    }


    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>
<center>
  <h3>Dashboard Rider {{ session('witel') }}</h3>
</center>
<div class="row">
  <div class="col-lg-6 col-sm-12 col-xs-12">
    <div class="row">
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="white-box">
          <h3 class="box-title">TOTAL TEKNISI</h3>
            <ul class="list-inline two-part">
              <li><i class="icon-people text-info"></i></li>
              <li class="text-right"><span class="counter">280</span></li>
              
            </ul>
          </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="white-box">
          <h3 class="box-title">PRA-ASSESSMENT</h3>
            <ul class="list-inline two-part">
              <li><i class="icon-people text-info"></i></li>
              <li class="text-right"><span class="counter">23</span></li>
            </ul>
          </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="white-box">
          <h3 class="box-title">LULUS</h3>
            <ul class="list-inline two-part">
              <li><i class="icon-people text-info"></i></li>
              <li class="text-right"><span class="counter">280</span></li>
            </ul>
          </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="white-box">
                                            <h3 class="box-title">VISIT QC</h3>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6  m-t-30">
                                                    <h1 class="text-warning">$678</h1></div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div id="sales1" class="text-center"><canvas width="100" height="100" style="display: inline-block; width: 100px; height: 100px; vertical-align: top;"></canvas></div>
                                                </div>
                                            </div>
                                        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {

   var sparklineLogin = function() {
        $('#sales1').sparkline([20, 40, 30], {
            type: 'pie',
            height: '100',
            resize: true,
            sliceColors: ['#808f8f', '#fecd36', '#f1f2f7']
        });
        $('#sales2').sparkline([6, 10, 9, 11, 9, 10, 12], {
            type: 'bar',
            height: '154',
            barWidth: '4',
            resize: true,
            barSpacing: '10',
            barColor: '#0270d6'
        });

   }
    var sparkResize;

        $(window).resize(function(e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineLogin, 500);
        });
        sparklineLogin();

});
</script>
@endsection
