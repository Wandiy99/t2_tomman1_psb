@extends('layout')
@section('content')
  @include('partial.alerts')
  <a href="/coc/input" class="btn btn-success"> <i class="fa fa-plus"></i> INPUT</a>
  <h3>Report Role Play Video</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">INBOX</div>
  			<div class="panel-body table-responsive">
  				<table class="table">
  					<tr>
  						<th>#</th>
  						<th>UPLOADER</th>
  						<th>NIK</th>
  						<th>TGL</th>
  						<th>NOTE</th> 
  						<th>UPDATE</th>
  						<th>LINK</th>
  					</tr>
  					@foreach($query as $num => $result)
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>{{ $result->USER_CREATED }}</td>
  						<td>{{ $result->NIK1 }}</td>
  						<td>{{ $result->TGL }}</td>
  						<td><?php echo nl2br($result->NOTE); ?></td>
  						<td>{{ $result->TGLUPDATE }}</td>
  						<td><a href="{{ $result->LINK }}">VIDEO</a></td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>

@endsection
