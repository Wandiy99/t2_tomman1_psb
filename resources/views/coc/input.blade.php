@extends('layout')
@section('content')
  @include('partial.alerts')
  <h3>Report Role Play Video</h3>
  
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-body table-responsive">
<form method="post" data-toggle="validator">
                                        <div class="form-body">
                                            <h3 class="box-title">Person Info</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">NIK1</label>
                                                        <input type="text" id="nik1" name="nik1" class="form-control" data-error="Data NIK1 diperlukan" required>
                                                        <span class="help-block  with-errors"> NIK Peserta 1 </span> 
                                                      </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">TGL</label>
                                                        <input type="date" id="tgl" name="tgl" class="form-control" data-error="Data Tanggal Simulasi diperlukan" required>
                                                        <span class="help-block with-errors"> Tanggal Simulasi </span> </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">NOTE</label>
                                                        <textarea class="form-control" name="note" rows="10" cols="20"  data-error="Data Catatan sebagai masukkan kepada teknisi" required></textarea>
                                                        <span class="help-block  with-errors"> Catatan yang diberikan kepada teknisi </span> </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">LINK YOUTUBE</label>
                                                        <textarea class="form-control" name="linkyoutube" rows="10" cols="20" data-error="Link Youtube diperlukan" required></textarea>
                                                        <span class="help-block  with-errors"> Silahkan paste link youtube dari Video CoC peserta </span> </div>
                                                </div>
                                                
                                                <!--/span-->
                                            </div>
                                          </div>
                                          <div class="form-actions">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                        </form>
  			</div>
  		</div>
  	</div>
  </div>

@endsection
