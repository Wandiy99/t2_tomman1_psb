@extends('layout')
@section('content')
@include('partial.alerts')
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-default">
		<div class="panel-heading">MANJA ORDER ID-{{ $data->Ndem }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>CATATAN MANJA</label>
						<input type="text" name="manja" class="form-control" value="{{ $data->manja ?: old('manja') }}">
						{!! $errors->first('manja','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection