@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">PRODUKTIFITAS FALLOUT {{ $hd }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>SC</th>
                    <th>SNONT</th>
                    <th>ACTION</th>
                    <th>ODP</th>
                    <th>FUP BY</th>
                    <th>REPLY</th>
                    <th>TGL UPDATE</th>
                    <th>TGL OPEN</th>
                    <th>REQUEST BY</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $result)
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->SC }}</td>
                    <td>{{ $result->SNONT }}</td>
                    <td>{{ $result->ACTION }}</td>
                    <td>{{ $result->ODP }}</td>
                    <td>{{ $result->FUP_NAME }}</td>
                    <td>{{ $result->REPLY }}</td>
                    <td>{{ $result->TGLUPDATE }}</td>
                    <td>{{ $result->TGLOPEN }}</td>
                    <td>{{ $result->USERNAME }}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'PRODUKTIFITAS HELPDESK FALLOUT PROV TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'PRODUKTIFITAS HELPDESK FALLOUT PROV TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'PRODUKTIFITAS HELPDESK FALLOUT PROV TOMMAN'
                }
            ]
        });
    });
</script>
@endsection