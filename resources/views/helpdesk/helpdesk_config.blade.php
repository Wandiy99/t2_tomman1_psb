@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">PRODUKTIFITAS HELPDESK CONFIG {{ $hd }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID ORDER</th>
                    <th>NO TIKET</th>
                    <th>INET</th>
                    <th>SN LAMA</th>
                    <th>SN BARU</th>
                    <th>MAC STB</th>
                    <th>LABELCODE</th>
                    <th>NAMA ODP</th>
                    <th>VALINS ID</th>
                    <th>CATATAN REQ</th>
                    <th>COMMAND</th>
                    <th>STATUS ORDER</th>
                    <th>REQUEST BY</th>
                    <th>TGL ORDER</th>
                    <th>BALASAN</th>
                    <th>TGL CLOSE</th>
                    <th>HELPDESK</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $result)
                  <tr>
                      <td>{{ ++$num }}</td>
                      <td>{{ $result->id_hai }}</td>
                      <td>{{ $result->no_tiket }}</td>
                      <td>{{ $result->inet }}</td>
                      <td>{{ $result->sn_lama }}</td>
                      <td>{{ $result->sn_baru }}</td>
                      <td>{{ $result->mac_stb }}</td>
                      <td>{{ $result->labelcode }}</td>
                      <td>{{ $result->nama_odp }}</td>
                      <td>{{ $result->valins_id }}</td>
                      <td>{{ $result->catatan_request }}</td>
                      <td>{{ $result->command }}</td>
                      <td>{{ $result->STATUS }}</td>
                      <td>{{ $result->request_by }} / {{ $result->username }}</td>
                      <td>{{ $result->tgl_order }}</td>
                      <td>{{ $result->helpdesk_reply }}</td>
                      <td>{{ $result->tgl_close }}</td>
                      <td>{{ $result->helpdesk_name }}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
          </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'PRODUKTIFITAS HELPDESK MULTISKILL ASSURANCE TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'PRODUKTIFITAS HELPDESK MULTISKILL ASSURANCE TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'PRODUKTIFITAS HELPDESK MULTISKILL ASSURANCE TOMMAN'
                }
            ]
        });
    });
</script>
@endsection