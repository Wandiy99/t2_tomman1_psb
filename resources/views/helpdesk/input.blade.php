@extends('layout')

@section('content')
  @include('partial.alerts')
<h3>INBOX HELPDESK</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				INBOX
			</div>
			<div class="panel-body">
				<form method="post">
					<input type="hidden" name="ID" value="{{ $message->ID }}">
					<input type="hidden" name="MESSAGE_ID" value="{{ $message->MESSAGE_ID }}">
					<input type="hidden" name="CHAT_ID" value="{{ $message->CHAT_ID }}">
					<table class="table">
						<tr>
							<td>FROM</td>
							<td>:</td>
							<td>{{ $message->nama }} / {{ $message->USERNAME }}</td>
						</tr>
						<tr>
							<td>TEXT</td>
							<td>:</td>
							<td>{{ $message->TEXTBOT }}</td>
						</tr>
						<tr>
							<td>STATUS HD</td>
							<td>:</td>
							<td>
								<select name="status_hd">
									<option value="" disabled>Pilih Status</option>
									@foreach ($getStatus as $status)
                                    <?php if ($status->ID == $message->STATUS) $selected = "selected='selected'"; else $selected = ""; ?>
					                <option value="{{ $status->ID }}" {{ $selected }}>{{ $status->STATUS }}</option>
									@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<td>SPEEDY</td>
							<td>:</td>
							<td>
								<input type="number" name="speedy" minlength="12">
							</td>
						</tr>
						<tr>
							<td>TEXT</td>
							<td>:</td>
							<td>
								<textarea name="reply" cols="50" rows="10"></textarea>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<input type="submit" name="submit" value="Submit">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection