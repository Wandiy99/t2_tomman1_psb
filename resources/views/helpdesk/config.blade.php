@extends($layout)
@section('content')
@include('partial.alerts')
<style>
    th, td {
        text-align: center;
        vertical-align: middle;
    }
    h3 {
        text-align: center;
        font-weight: bold;
    }
    option {
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">NEED TAKE ORDER</h3>
            <div class="table-responsive">
                <table id="table_order" class="table_inbox display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            @if(in_array(session('auth')->level,[2,15]) && count($check_to)<2)
                            <th>TO</th>
                            @endif
                            <th>ID ORDER</th>
                            <th>NO TIKET</th>
                            <th>INET</th>
                            <th>SN LAMA</th>
                            <th>SN BARU</th>
                            <th>MAC STB</th>
                            <th>LABELCODE</th>
                            <th>NAMA ODP</th>
                            <th>VALINS ID</th>
                            <th>CATATAN REQ</th>
                            <th>COMMAND</th>
                            <th>STATUS ORDER</th>
                            <th>REQUEST BY</th>
                            <th>TTR</th>
                            <th>TGL ORDER</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $num => $d)
                        @php
                            date_default_timezone_set("Asia/Makassar");
                            $waktu = date('Y-m-d H:i:s');
                            $tglAwal = new DateTime($d->tgl_order);
                            $tglAkhir = new DateTime($waktu);
                            $TTR = $tglAkhir->diff($tglAwal)->days * 24 * 60;
                            $TTR += $tglAkhir->diff($tglAwal)->h * 60;
                            $TTR += $tglAkhir->diff($tglAwal)->i;
                        @endphp
                        <tr>
                            <td>{{ ++$num }}</td>
                            @if(in_array(session('auth')->level,[2,15]) && count($check_to)<2)
                                <td>
                                    <span type="button" class="label label-info">
                                        <a style="color: white" target="_blank" href="/helpdesk/assurance/config/{{ $d->id_hai }}">
                                        @if ($d->nama <> NULL)
                                        Take Order by {{ $d->nama }}
                                        @else
                                        Take Order
                                        @endif
                                        </a>
                                    </span>
                                </td>
                            @endif
                            <td>{{ $d->id_hai }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{ $d->STATUS }}</td>
                            <td></td>
                            <td>
                            @if ($TTR >= 10)
                            <span class="label label-danger">{{ $TTR }} Menit</span>
                            @else
                            <span class="label label-success">{{ $TTR }} Menit</span>
                            @endif
                            </td>
                            <td>{{ $d->tgl_order }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">HISTORY TAKE ORDER</h3>
            <div class="table-responsive">
                <table id="table_history" class="table_inbox display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>TO</th>
                            <th>ID ORDER</th>
                            <th>NO TIKET</th>
                            <th>INET</th>
                            <th>SN LAMA</th>
                            <th>SN BARU</th>
                            <th>MAC STB</th>
                            <th>LABELCODE</th>
                            <th>NAMA ODP</th>
                            <th>VALINS ID</th>
                            <th>CATATAN REQ</th>
                            <th>COMMAND</th>
                            <th>STATUS ORDER</th>
                            <th>REQUEST BY</th>
                            <th>TGL ORDER</th>
                            <th>BALASAN</th>
                            <th>TGL CLOSE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_history as $num => $d)
                        <tr>
                            <td>{{ ++$num }}</td>
                            @if(in_array(session('auth')->level,[2,15]))
                            <td>
                                <div id="responsive-modal-{{ $d->id_hai }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $d->id_hai }}" aria-hidden="true" style="display: none;">
                                    <form class="form-group" action="/helpdesk/assurance/config/{{ $d->id_hai }}" method="POST">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Take ID Order {{ $d->id_hai }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="col-sm-12">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label" for="input-transfer" >Transfer ?</label>
                                                                <input name="input_transfer" type="hidden" id="input-transfer" class="form-control input-transfer" />
                                                            </div>
                                                            <div class="form-group col-md-6 transfer_to" id="transfer_to">
                                                                <label for="transfer_to" class="col-form-label">Transfer To :</label>
                                                                <select class="form-control select2" name="transfer_to" id="transfer_to" required>
                                                                    <option value="" selected disabled>Pilih Pemain</option>
                                                                    @foreach ($get_hd as $hd)
                                                                    <option data-subtext="description 1" value="{{ $hd->id }}"
                                                                        <?php if (@$hd->id == @$d->helpdesk_by) { echo "Selected"; } else { echo ""; } ?>>{{ @$hd->text }}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6 hiddens">
                                                                <label for="input-command" class="col-form-label">Command :</label>
                                                                <select class="form-control select2" name="command" id="input-command" required>
                                                                    <option value="" selected disabled>Pilih Status</option>
                                                                    @foreach ($get_command as $command)
                                                                    <option data-subtext="description 1" value="{{ $command->id }}"
                                                                        <?php if (@$command->id == @$d->command) { echo "Selected"; } else { echo ""; } ?>>{{ @$command->text }}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6 hiddens">
                                                                <label for="input-status" class="col-form-label">Status :</label>
                                                                <select class="form-control select2" name="status_order" id="input-status" required>
                                                                    <option value="" selected disabled>Pilih Status</option>
                                                                    @foreach ($get_status as $status)
                                                                    <option data-subtext="description 1" value="{{ $status->id }}"
                                                                        <?php if (@$status->id == @$d->status_order) { echo "Selected"; } else { echo ""; } ?>>{{ @$status->text }}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-12 hiddens">
                                                                <label for="message-text" class="control-label">Balasan :</label>
                                                                <textarea class="form-control" name="helpdesk_reply" id="message-text" rows="4">{{ $d->helpdesk_reply ? : '' }}</textarea>
                                                            </div>
                                                            <div class="form-group col-md-12 table-responsive hiddens">
                                                                <table class="table table-hover table-bordered table-sm" style="text-align: center;">
                                                                    <thead style="background-color: yellow;">
                                                                    <tr>
                                                                        <th style="color: black;">NO TIKET</th>
                                                                        <th style="color: black;">INET</th>
                                                                        <th style="color: black;">SN LAMA</th>
                                                                        <th style="color: black;">SN BARU</th>
                                                                        <th style="color: black;">MAC STB</th>
                                                                        <th style="color: black;">LABELCODE</th>
                                                                        <th style="color: black;">NAMA ODP</th>
                                                                        <th style="color: black;">CATATAN REQUEST</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td><input class="text-center" type="text" name="no_tiket" value="{{ $d->no_tiket }}"></td>
                                                                        <td><input class="text-center" type="text" name="inet" value="{{ $d->inet }}"></td>
                                                                        <td>
                                                                            <input class="text-center" type="text" name="sn_lama" value="{{ $d->sn_lama }}" maxlength="16">
                                                                            {!! $errors->first('sn_lama', '<span class="label label-danger">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            <input class="text-center" type="text" name="sn_baru" value="{{ $d->sn_baru }}" maxlength="16">
                                                                            {!! $errors->first('sn_baru', '<span class="label label-danger">:message</span>') !!}
                                                                        </td>
                                                                        <td><input class="text-center" type="text" name="mac_stb" value="{{ $d->mac_stb }}"></td>
                                                                        <td><input class="text-center" type="text" name="labelcode" value="{{ $d->labelcode }}"></td>
                                                                        <td><input class="text-center" type="text" name="nama_odp" value="{{ $d->nama_odp }}"></td>
                                                                        <td><input class="text-center" type="text" name="catatan_request" value="{{ $d->catatan_request }}"></td>

                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <span type="button" class="label label-info" data-toggle="modal" data-target="#responsive-modal-{{ $d->id_hai }}">
                                    {{ $d->nama }}
                                </span>
                            </td>
                            @else
                            <td>Take Order by {{ $d->nama }}</td>
                            @endif
                            <td>{{ $d->id_hai }}</td>
                            <td>{{ $d->no_tiket }}</td>
                            <td>{{ $d->inet }}</td>
                            <td>{{ $d->sn_lama }}</td>
                            <td>{{ $d->sn_baru }}</td>
                            <td>{{ $d->mac_stb }}</td>
                            <td>{{ $d->labelcode }}</td>
                            <td>{{ $d->nama_odp }}</td>
                            <td>{{ $d->valins_id }}</td>
                            <td>{{ $d->catatan_request }}</td>
                            <td>{{ $d->command }}</td>
                            <td>{{ $d->STATUS }}</td>
                            <td>{{ $d->request_by }} / {{ $d->username }}</td>
                            <td>{{ $d->tgl_order }}</td>
                            <td>{{ $d->helpdesk_reply }}</td>
                            <td>{{ $d->tgl_close }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.transfer_to').hide();
        var dataTF = [
            {"id":"YES", "text":"YES"},
            {"id":"NO", "text":"NO"}
        ];

        var transfer = function() {
        return {
                data: dataTF,
                placeholder: '----- Keterangan Transfer -----',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                    return  data.text;
                }
            }
        }
        $('.input-transfer').select2(transfer());

        $('.input-transfer').change(function(){
            console.log(this.value);
            if(this.value=='YES'){
                $('.transfer_to').show();
                $('.hiddens').hide();
            }
            else{
                $('.transfer_to').hide();
            };
        });

        $('.table_inbox').DataTable({
            order: [],
            lengthMenu: [[-1, 10, 25, 50], ["All" , 10, 25, 50]],
            ordering: false
        });
    });
</script>
@endsection