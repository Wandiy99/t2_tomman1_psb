@extends($layout)
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style>
	tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    /* th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    table.dataTable tbody tr{
        background-color: transparent;
    } */
</style>
@if (session('auth')->level != 10)
<h3>{{ session('auth')->nama }}</h3>
@endif
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default"><div class="panel-heading"><center>INBOX</center></div>
			<div class="table-responsive">
				<table class="table table-bordered tableData" id="table_inbox">
					<thead>
						<tr>
							<th class="align-middle">#</th>
							<th class="align-middle">ID ORDER</th>
							<th class="align-middle">COMMAND</th>
							<th class="align-middle">SC</th>
							<th class="align-middle">SN ONT</th>
							<th class="align-middle">NAMA ODP</th>
							<th class="align-middle">VALINS ID</th>
							<th class="align-middle">CATATAN</th>
							<th class="align-middle">STATUS</th>
							<th class="align-middle">REQUEST BY</th>
							<th class="align-middle">TIM</th>
							<th class="align-middle">JENIS LAYANAN</th>
							<th class="align-middle">TGL OPEN</th>
							<th class="align-middle">TTR</th>
							<th class="align-middle">BALASAN</th>
							<th class="align-middle">TGL UPDATE</th>
							<th class="align-middle">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($getInbox as $num => $inbox)
						<?php
							$tglAwal = new DateTime($inbox->TGLOPEN);
							$tglAkhir = new DateTime(date('Y-m-d H:i:s'));
							$TTR = $tglAkhir->diff($tglAwal)->days * 24 * 60;
							$TTR += $tglAkhir->diff($tglAwal)->h * 60;
							$TTR += $tglAkhir->diff($tglAwal)->i;
						?>
						<tr>
							<td class="align-middle">{{ ++$num }}</td>
							@if(session('auth')->id_karyawan == "20981020" || session('auth')->id_karyawan == $inbox->FUP_BY)
								<td class="align-middle"><a href="/helpdesk_log/{{ $inbox->ID }}">{{ $inbox->ID }}</a></td>
								<td class="align-middle">{{ $inbox->Category }}</td>
								<td class="align-middle">{{ $inbox->SC }}</td>
								<td class="align-middle">{{ $inbox->SNONT }}</td>
								<td class="align-middle">{{ $inbox->ODP }}</td>
								<td class="align-middle">{{ $inbox->VALINS_ID }}</td>
								<td class="align-middle">{{ $inbox->ACTION }}</td>
							@else
								<td class="align-middle">{{ $inbox->ID }}</td>
								<td class="align-middle"></td>
								<td class="align-middle"></td>
								<td class="align-middle"></td>
								<td class="align-middle"></td>
								<td class="align-middle"></td>
								<td class="align-middle"></td>
							@endif
							<td class="align-middle">{{ $inbox->STATUS }}</td>
							@if(session('auth')->id_karyawan == "20981020" || session('auth')->id_karyawan == $inbox->FUP_BY)
								<td class="align-middle">{{ $inbox->request_by }} / {{ $inbox->USERNAME }}</td>
								<td class="align-middle">{{ $inbox->uraian }}</td>
								<td class="align-middle">{{ $inbox->jenis_layanan }}</td>
							@else
								<td class="align-middle"></td>
								<td class="align-middle"></td>
								<td class="align-middle">{{ $inbox->jenis_layanan }}</td>
							@endif
							<td class="align-middle">{{ $inbox->TGLOPEN }}</td>
							<td class="align-middle">{{ $TTR }} menit</td>
							<td class="align-middle">{{ $inbox->REPLY }}</td>
							<td class="align-middle">{{ $inbox->TGLUPDATE }}</td>
							<td class="align-middle">
							@if (session('auth')->level == 10)
								{{ $inbox->FUP_BY }}
							@else
								@if ($inbox->FUP_BY=="") 
									<a href="/helpdesk_book/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-warning">BOOK</a>
								@elseif (session('auth')->id_karyawan == $inbox->FUP_BY)
									<a href="/helpdesk_fup/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-success">FUP</a>

									@if ($inbox->STATUS<>"DONE")
									<br /><br />
									<a href="/helpdesk_inbox/delete/{{ $inbox->SC }}/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-danger">DELETE</a>
									@endif
								@else
									{{ $inbox->FUP_BY }}
								@endif
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="panel panel-default"><div class="panel-heading"><center>BOOKED</center></div>
			<div class="table-responsive">
				<table class="table table-bordered tableData" id="table_booked">
					<thead>
						<tr>
							<th class="align-middle">#</th>
							<th class="align-middle">ID ORDER</th>
							<th class="align-middle">COMMAND</th>
							<th class="align-middle">SC</th>
							<th class="align-middle">SN ONT</th>
							<th class="align-middle">NAMA ODP</th>
							<th class="align-middle">VALINS ID</th>
							<th class="align-middle">CATATAN</th>
							<th class="align-middle">STATUS</th>
							<th class="align-middle">REQUEST BY</th>
							<th class="align-middle">TIM</th>
							<th class="align-middle">JENIS LAYANAN</th>
							<th class="align-middle">TGL OPEN</th>
							<th class="align-middle">TTR</th>
							<th class="align-middle">BALASAN</th>
							<th class="align-middle">TGL UPDATE</th>
							<th class="align-middle">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($getBooked as $num => $inbox)
						<?php
							$tglAwal = new DateTime($inbox->TGLOPEN);
							$tglAkhir = new DateTime(date('Y-m-d H:i:s'));
							$TTR = $tglAkhir->diff($tglAwal)->days * 24 * 60;
							$TTR += $tglAkhir->diff($tglAwal)->h * 60;
							$TTR += $tglAkhir->diff($tglAwal)->i;
						?>
						<tr>
							<td class="align-middle">{{ ++$num }}</td>
							<td class="align-middle"><a href="/helpdesk_log/{{ $inbox->ID }}">{{ $inbox->ID }}</a></td>
							<td class="align-middle">{{ $inbox->Category }}</td>
							<td class="align-middle">{{ $inbox->SC }}</td>
							<td class="align-middle">{{ $inbox->SNONT }}</td>
							<td class="align-middle">{{ $inbox->ODP }}</td>
							<td class="align-middle">{{ $inbox->VALINS_ID }}</td>
							<td class="align-middle">{{ $inbox->ACTION }}</td>
							<td class="align-middle">{{ $inbox->STATUS }}</td>
							<td class="align-middle">{{ $inbox->request_by }} / {{ $inbox->USERNAME }}</td>
							<td class="align-middle">{{ $inbox->uraian }}</td>
							<td class="align-middle">{{ $inbox->jenis_layanan }}</td>
							<td class="align-middle">{{ $inbox->TGLOPEN }}</td>
							<td class="align-middle">{{ $TTR }} menit</td>
							<td class="align-middle">{{ $inbox->REPLY }}</td>
							<td class="align-middle">{{ $inbox->TGLUPDATE }}</td>
							<td class="align-middle">
							@if ($inbox->FUP_BY == "") 
								<a href="/helpdesk_book/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-warning">BOOK</a>
							@elseif (session('auth')->id_karyawan == $inbox->FUP_BY)
								<a href="/helpdesk_fup/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-success">FUP</a>
								@if ($inbox->STATUS<>"DONE")
								<br /><br />
								<a href="/helpdesk_inbox/delete/{{ $inbox->SC }}/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-danger">DELETE</a>
								@endif
							@else
								{{ $inbox->FUP_NAME }}
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@if (session('auth')->level != 10)
	<div class="col-sm-12">
		<div class="panel panel-default"><div class="panel-heading"><center>CLOOSED</center></div>
			<div class="table-responsive">
				<table class="table table-bordered tableData" id="table_cloosed">
					<thead>
						<tr>
							<th class="align-middle">#</th>
							<th class="align-middle">ID ORDER</th>
							<th class="align-middle">COMMAND</th>
							<th class="align-middle">SC</th>
							<th class="align-middle">SN ONT</th>
							<th class="align-middle">NAMA ODP</th>
							<th class="align-middle">VALINS ID</th>
							<th class="align-middle">CATATAN</th>
							<th class="align-middle">STATUS</th>
							<th class="align-middle">REQUEST BY</th>
							<th class="align-middle">TIM</th>
							<th class="align-middle">JENIS LAYANAN</th>
							<th class="align-middle">TGL OPEN</th>
							<th class="align-middle">TTR</th>
							<th class="align-middle">BALASAN</th>
							<th class="align-middle">TGL UPDATE</th>
							<th class="align-middle">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($getCloosed as $num => $inbox)
						<?php
							$tglAwal = new DateTime($inbox->TGLOPEN);
							$tglAkhir = new DateTime(date('Y-m-d H:i:s'));
							$TTR = $tglAkhir->diff($tglAwal)->days * 24 * 60;
							$TTR += $tglAkhir->diff($tglAwal)->h * 60;
							$TTR += $tglAkhir->diff($tglAwal)->i;
						?>
						<tr>
							<td class="align-middle">{{ ++$num }}</td>
							<td class="align-middle"><a href="/helpdesk_log/{{ $inbox->ID }}">{{ $inbox->ID }}</a></td>
							<td class="align-middle">{{ $inbox->Category }}</td>
							<td class="align-middle">{{ $inbox->SC }}</td>
							<td class="align-middle">{{ $inbox->SNONT }}</td>
							<td class="align-middle">{{ $inbox->ODP }}</td>
							<td class="align-middle">{{ $inbox->VALINS_ID }}</td>
							<td class="align-middle">{{ $inbox->ACTION }}</td>
							<td class="align-middle">{{ $inbox->STATUS }}</td>
							<td class="align-middle">{{ $inbox->request_by }} / {{ $inbox->USERNAME }}</td>
							<td class="align-middle">{{ $inbox->uraian }}</td>
							<td class="align-middle">{{ $inbox->jenis_layanan }}</td>
							<td class="align-middle">{{ $inbox->TGLOPEN }}</td>
							<td class="align-middle">{{ $TTR }} menit</td>
							<td class="align-middle">{{ $inbox->REPLY }}</td>
							<td class="align-middle">{{ $inbox->TGLUPDATE }}</td>
							<td class="align-middle">
							@if ($inbox->FUP_BY == "") 
								<a href="/helpdesk_book/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-warning">BOOK</a>
							@elseif (session('auth')->id_karyawan == $inbox->FUP_BY)
								<a href="/helpdesk_fup/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-success">FUP</a>

								@if ($inbox->STATUS<>"DONE")
								<br /><br />
								<a href="/helpdesk_inbox/delete/{{ $inbox->SC }}/{{ $inbox->ID }}" class="btn btn-sm btn-block btn-rounded btn-danger">DELETE</a>
								@endif
							@else
								{{ $inbox->FUP_NAME }}
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function() {
        $('.tableData').DataTable({
            "ordering": false
        });
    });
</script>
@endsection