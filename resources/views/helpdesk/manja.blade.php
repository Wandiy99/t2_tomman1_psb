@extends('layout')

@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style>
    th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    table.dataTable tbody tr{
        background-color: transparent;
    }
</style>
<h3>{{ $title }}</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				INBOX {{ $datetime }}
			</div>
			<div class="table-responsive">
				<table class="table" id="table_data">
                    <thead>
                    <tr>
                        <th>NO</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>ORDER ID</th>
                        <th>STATUS</th>
                        <th>CATATAN TEKNISI</th>
                        <th>PIC PELANGGAN</th>
                        <th>TANGGAL UPDATE</th>
                        <th>TANGGAL DISPATCH</th>
                        @if ($hd==1)
                            <th>HD MANJA</th>
                        @endif
                        <th>CATATAN MANJA</th>
                        <th>TERAKHIR UPDATE</th>
                        <th>MANJA OLEH</th>
                    </tr>
                    <thead>
                    <tbody>
					@foreach ($data as $num => $d)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $d->tim }}</td>
						<td>{{ $d->sektor }}</td>
						<td><a href="/{{ $d->id_dt }}">{{ $d->order_id }}</a></td>
						<td>{{ $d->status }}</td>
						<td>{{ $d->catatan_tek }}</td>
						<td>{{ $d->pic }}</td>
						<td>{{ $d->tgl_update }}</td>
						<td>{{ $d->tgl_dispatch }}</td>
                        @if ($hd==1)
                            <td>{{ $d->hd_manja }}
                        @endif
						<td>{{ $d->manja }}</td>
                        <td>{{ $d->manja_updated }}</td>
						<td>
                        @if($d->manjaBy=="")
                            <a href="/helpdesk/manja/{{ $d->id_dt }}" class="btn btn-warning">TAKE ORDER</a>
                        @else
                            <a href="/helpdesk/manja/{{ $d->id_dt }}" class="btn btn-success">TAKE BY {{ $d->nama }} / {{ $d->manjaBy }}</a>
                        @endif
                        </td>
					</tr>
					@endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>ORDER ID</th>
                        <th>STATUS</th>
                        <th>CATATAN TEKNISI</th>
                        <th>PIC PELANGGAN</th>
                        <th>TANGGAL UPDATE</th>
                        <th>TANGGAL DISPATCH</th>
                        @if ($hd==1)
                            <th>HD MANJA</th>
                        @endif
                        <th>CATATAN MANJA</th>
                        <th>TERAKHIR UPDATE</th>
                        <th>MANJA OLEH</th>
                    </tr>
                    </tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('plugins')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function() {
        $('#table_data').DataTable();
    });
</script>
@endsection