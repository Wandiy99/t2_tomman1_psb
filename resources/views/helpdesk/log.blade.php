@extends($layout)

@section('content')
  @include('partial.alerts')
<h3>LOG HELPDESK</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				LOG
			</div>
			<div class="table-responsive">
				<table class="table dataTable">
					<tr>
						<th>#</th>
						<th>STATUS</th>
						<th>FUP_BY</th>
						<th>SC</th>
						<th>MESSAGE</th>
						<th>TGLUPDATE</th>
					</tr>
					@foreach ($query as $num => $result)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $result->STATUS }}</td>
						<td>{{ $result->FUP_BY ? : 'START' }}</td>
						<td>{{ $result->SC }}</td>
						<td>{{ $result->REPLY ? : 'INIT 0' }}</td>
						<td>{{ $result->TGLUPDATE }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>
@endsection