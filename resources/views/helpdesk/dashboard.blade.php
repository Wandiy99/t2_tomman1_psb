@extends('layout')
@section('content')
<style>
th{
    text-align : center;
}
td{
    text-align : center;
}
.color{
    background-color : #01aa9b;
    color : white;
}
.paleturquoise{
    background-color : #AFEEEE;
    color : black;
}
</style>
<center>
<b>
    <h3>DASHBOARD PRODUKTIFITAS HELPDESK KALSEL</h3>
    <h3>PERIODE {{ $date }}</h3>
</b>
</center>
<br /><br />
<div class="row">

    <div class="col-sm-6 table-responsive">
        <div class="panel panel-default">
            <div class="panel-heading"><center>HELPDESK FALLOUT PROVISIONING</center></div>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="color">NAMA</th>
                        <th class="color">JUMLAH</th>
                        <th class="color">TARGET</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total_fallout = 0;
                    $tf = 0;
                    $target = 0;
                    $total_target = 0;
                @endphp
                @foreach ($get_fallout as $fallout2)
                @php
                    $tf += $fallout2->jumlah;
                @endphp
                @endforeach
                @foreach ($get_fallout as $fallout)
                @php
                    $total_fallout += $fallout->jumlah;
                    $target = (@round(@$fallout->jumlah / @$tf * 100,2));
                    $total_target += $target;
                @endphp
                    <tr>
                        <td>{{ $fallout->nama }}</td>
                        <td><a href="/helpdesk_prod/fallout/{{ $fallout->id_hd }}/{{ $date }}">{{ $fallout->jumlah }}</a></td>
                        <td>{{ $target }} %</td>
                    </tr>
                @endforeach
                    <tr>
                        <td class="paleturquoise">TOTAL</th>
                        <td class="paleturquoise"><a href="/helpdesk_prod/fallout/ALL/{{ $date }}">{{ $total_fallout }}</a></td>
                        <td class="paleturquoise">{{ $total_target }} %</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-6 table-responsive">
        <div class="panel panel-default">
            <div class="panel-heading"><center>HELPDESK CONFIG ASSURANCE</center></div>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="color">NAMA</th>
                        <th class="color">JUMLAH</th>
                        <th class="color">TARGET</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total_config = 0;
                    $tc = 0;
                    $target = 0;
                    $total_target = 0;
                @endphp
                @foreach ($get_config as $config2)
                    @php
                        $tc += $config2->jumlah;
                    @endphp
                @endforeach
                @foreach ($get_config as $config)
                @php
                    $total_config += $config->jumlah;
                    $target = (@round(@$config->jumlah / @$tc * 100,2));
                    $total_target += $target;
                @endphp
                    <tr>
                        <td>{{ $config->helpdesk_name }}</td>
                        <td><a href="/helpdesk_prod/config/{{ $config->helpdesk_name }}/{{ $date }}">{{ $config->jumlah }}</td>
                        <td>{{ $target }} %</td>
                    </tr>
                @endforeach
                    <tr>
                        <td class="paleturquoise">TOTAL</th>
                        <td class="paleturquoise"><a href="/helpdesk_prod/config/ALL/{{ $date }}">{{ $total_config }}</td>
                        <td class="paleturquoise">{{ $total_target }} %</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-6 table-responsive">
        <div class="panel panel-default">
            <div class="panel-heading"><center>HELPDESK MANJA PROVISIONING</center></div>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="color">NAMA</th>
                        <th class="color">JUMLAH</th>
                        <th class="color">TARGET</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total = 0;
                    $target = 0;
                    $sum = 0;
                    $total_target = 0;
                @endphp
                @foreach ($get_manja as $manja2)
                    @php
                        $sum += $manja2->jumlah;
                    @endphp
                @endforeach
                @foreach ($get_manja as $manja)
                @php
                    $total += $manja->jumlah;
                    $target = (@round(@$manja->jumlah / @$sum * 100,2));
                    $total_target += $target;
                @endphp
                    <tr>
                        <td>{{ $manja->nama }}</td>
                        <td>{{ $manja->jumlah }}</td>
                        <td>{{ $target }} %</td>
                    </tr>
                @endforeach
                    <tr>
                        <td class="paleturquoise">TOTAL</th>
                        <td class="paleturquoise">{{ $total }}</td>
                        <td class="paleturquoise">{{ $total_target }} %</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-6 table-responsive">
        <div class="panel panel-default">
            <div class="panel-heading"><center>HELPDESK MEDIA CARRING</center></div>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="color">NAMA</th>
                        <th class="color">JUMLAH</th>
                        <th class="color">TARGET</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total = 0;
                    $target = 0;
                    $sum = 0;
                    $total_target = 0;
                @endphp
                @foreach ($get_mediacarring as $carr2)
                    @php
                        $sum += $carr2->jumlah;
                    @endphp
                @endforeach
                @foreach ($get_mediacarring as $carr)
                @php
                    $total += $carr->jumlah;
                    $target = (@round(@$carr->jumlah / @$sum * 100,2));
                    $total_target += $target;
                @endphp
                    <tr>
                        <td>{{ $carr->nama }}</td>
                        <td>{{ $carr->jumlah }}</td>
                        <td>{{ $target }} %</td>
                    </tr>
                @endforeach
                    <tr>
                        <td class="paleturquoise">TOTAL</th>
                        <td class="paleturquoise">{{ $total }}</td>
                        <td class="paleturquoise">{{ $total_target }} %</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection