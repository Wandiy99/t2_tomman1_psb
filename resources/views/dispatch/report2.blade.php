@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlist')

  <div style="padding-bottom: 10px;">
    <strong>Mapping Order SHVA</strong><br/>
    <strong></strong>
<!--     <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong> -->
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading">Mapping</div>
    <div class="panel-body">
      <div class="list-group">
	      MAPPING WO BANJARMASIN HARI INI | KORLAP : WAHYU & JUNAIDI <br />
		  <div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tr>
						<td>No</td>
						<td>STO</td>
						<td>Area</td>
						<td>Nama Tim</td>
						<td>HD</td>
						<td>Tanggal Dispatch</td>
						<td>SC</td>
						<td>Timeslot</td>
						<td>NCLI</td>
						<td>Ndem Speedy</td>
						<td>Nama Pelanggan</td>
						<td>Tipe WO</td>
						<td>Status Teknisi</td>
						<td>Status SC</td>
            <td>Kordinat Pelanggan</td>
						<td>Status MS2N</td>
						<td>Catatan Teknisi</td>
						<td>Keterangan</td>
						<td>Alamat</td>

					</tr>
				{{-- */
					$number = 1;

					$UP_BJM = 0;
					$HR_BJM = 0;
					$OGP_BJM = 0;
					$ODP_LOS_BJM = 0;
					$INSERT_TIANG_BJM = 0;
					$PROGRESS_PT2_PT3_BJM = 0;
					$PROGRESS_PT2_PT3_BJM = 0;
					$KENDALA_PELANGGAN_BJM = 0;
					$BELUM_SURVEY_BJM = 0;
					$JUMLAH_WO_BJM = 0;

				/* --}}


				@foreach ($get_list as $no => $list)
				@if ($list->area=="BJM")

					<tr>
						<td>{{ ++$no }}</td>
						<td>{{ $list->sto ? : $list->mdf }}</td>
						<td>{{ $list->area}}</td>
						<td>{{ $list->uraian }}</td>
						<td>{{ $list->HD }}</td>
						<td>{{ $list->updated_at }}</td>
						<td><a href="/{{ $list->id_dt }}">SC{{ $list->orderId ? : $list->orderIdMS2N }}</a></td>
            @if ($list->dispatch_teknisi_timeslot<>'')
						<td><a href="#">{{ $list->dispatch_teknisi_timeslot }}</a></td>
            @else
            <td><a href="#">Manja NOK</a></td>
            @endif
						<td>{{ $list->Ncli ? : $list->orderNcli }}</td>
						<td>{{ $list->ND_Speedy ? : $list->ndemSpeedy }}</td>
						<td>{{ $list->Nama ? : $list->orderName }}</td>
						<td>{{ $list->jenisPsb ? : $list->jenisPsbMS2N }}</td>
						<td>{{ $list->status_wo_by_hd ? : 'ANTRIAN' }}</td>
						<td>{{ $list->orderStatus ? : $list->orderStatusMS2N }}</td>
            <td>{{ $list->kordinat_pelanggan ? : '' }}</td>
						<td>{{ $list->Sebab ? : 'Tidak Ada di MS2N' }}</td>
						<td>{{ $list->catatan ? : 'Tidak Ada Catatan' }}</td>
						<td>{{ $list->Ket_Sebab ? : 'Tidak Ada di MS2N' }}</td>
						<td>{{ $list->orderAddr ? : 'Tidak Ada di MS2N' }}</td>
					</tr>
				@endif
				@endforeach
				</table>



      </div>
	  <br />
      <div class="list-group">
	      MAPPING WO SO BANJARBARU HARI INI | KORLAP : YULIANTO ARI PERMADI <br />
		  <div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tr>
						<td>No</td>
						<td>STO</td>
						<td>Area</td>
						<td>Nama Tim</td>
						<td>HD</td>
						<td>Tanggal Dispatch</td>
						<td>SC</td>
						<td>NCLI</td>
						<td>Ndem Speedy</td>
						<td>Nama Pelanggan</td>
						<td>Tipe WO</td>
						<td>Status Teknisi</td>
						<td>Status SC</td>
						<td>Status MS2N</td>
						<td>Catatan Teknisi</td>
						<td>Keterangan</td>

					</tr>
				{{-- */
					$number = 1;
					$UP_BJB = 0;
					$HR_BJB = 0;
					$OGP_BJB = 0;
					$KENDALA_TEKNIS_BJB = 0;
					$KENDALA_PELANGGAN_BJB = 0;
					$BELUM_SURVEY_BJB = 0;
					$JUMLAH_WO_BJB = 0;
				/* --}}


				@foreach ($get_list as $no => $list)
				@if ($list->area=="BBR")
				{{-- */
					if ($list->status_laporan==1)
						$UP_BJB++;
					if ($list->status_laporan==2)
						$KENDALA_TEKNIS_BJB++;
					if ($list->status_laporan==3)
						$KENDALA_PELANGGAN_BJB++;
					if ($list->status_laporan==4)
						$HR_BJB++;
					if ($list->status_laporan==5)
						$OGP_BJB++;
					if ($list->status_wo_by_hd=="BELUM SURVEY")
						$BELUM_SURVEY_BJB++;
					$JUMLAH_WO_BJB++;

				/* --}}

					<tr>
						<td>{{ ++$no }}</td>
						<td>{{ $list->sto ? : $list->mdf }}</td>
						<td>{{ $list->area }}</td>
						<td>{{ $list->uraian }}</td>
						<td>{{ $list->HD }}</td>
						<td>{{ $list->updated_at }}</td>
						<td><a href="/{{ $list->id_dt }}">SC{{ $list->orderId ? : $list->orderIdMS2N }}</a></td>
						<td>{{ $list->Ncli ? : $list->orderNcli }}</td>
						<td>{{ $list->ND_Speedy ? : $list->ndemSpeedy }}</td>
						<td>{{ $list->Nama ? : $list->orderName }}</td>
						<td>{{ $list->jenisPsb ? : $list->jenisPsbMS2N }}</td>
						<td>{{ $list->status_wo_by_hd ? : 'ANTRIAN' }}</td>
						<td>{{ $list->orderStatus ? : $list->orderStatusMS2N }}</td>
						<td>{{ $list->Sebab ? : 'Tidak Ada di MS2N' }}</td>
						<td>{{ $list->catatan ? : 'Tidak Ada Catatan' }}</td>
						<td>{{ $list->Ket_Sebab ? : 'Tidak Ada di MS2N' }}</td>
					</tr>
				@endif
				@endforeach
				</table>
      </div>
	  </div>
      <div class="list-group">
	      MAPPING WO SO TANJUNG HARI INI | KORLAP : SARPANI <br />
		  <div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tr>
						<td>No</td>
						<td>STO</td>
						<td>Area</td>
						<td>Nama Tim</td>
						<td>HD</td>
						<td>Tanggal Dispatch</td>
						<td>SC</td>
						<td>NCLI</td>
						<td>Ndem Speedy</td>
						<td>Nama Pelanggan</td>
						<td>Tipe WO</td>
						<td>Status Teknisi</td>
						<td>Status SC</td>
						<td>Status MS2N</td>
						<td>Catatan Teknisi</td>
						<td>Keterangan</td>

					</tr>
				{{-- */
					$number = 1;
					$UP_TTG = 0;
					$HR_TTG = 0;
					$OGP_TTG = 0;
					$KENDALA_TEKNIS_TTG = 0;
					$KENDALA_PELANGGAN_TTG = 0;
					$BELUM_SURVEY_TTG = 0;
					$JUMLAH_WO_TTG = 0;
				/* --}}


				@foreach ($get_list as $no => $list)
				@if ($list->area=="TTG")
				{{-- */
					if ($list->status_laporan==1)
						$UP_TTG++;
					if ($list->status_laporan==2)
						$KENDALA_TEKNIS_TTG++;
					if ($list->status_laporan==3)
						$KENDALA_PELANGGAN_TTG++;
					if ($list->status_laporan==4)
						$HR_TTG++;
					if ($list->status_laporan==5)
						$OGP_TTG++;
					if ($list->status_wo_by_hd=="BELUM SURVEY")
						$BELUM_SURVEY_TTG++;
					$JUMLAH_WO_TTG++;
				/* --}}
					<tr>
						<td>{{ ++$no }}</td>
						<td>{{ $list->sto ? : $list->mdf }}</td>
						<td>{{ $list->area }}</td>
						<td>{{ $list->uraian }}</td>
						<td>{{ $list->HD }}</td>
						<td>{{ $list->updated_at }}</td>
						<td><a href="/{{ $list->id_dt }}">SC{{ $list->orderId ? : $list->orderIdMS2N }}</a></td>
						<td>{{ $list->Ncli ? : $list->orderNcli }}</td>
						<td>{{ $list->ND_Speedy ? : $list->ndemSpeedy }}</td>
						<td>{{ $list->Nama ? : $list->orderName }}</td>

						<td>{{ $list->jenisPsb ? : $list->jenisPsbMS2N }}</td>
						<td>{{ $list->status_wo_by_hd ? : 'ANTRIAN' }}</td>
						<td>{{ $list->orderStatus ? : $list->orderStatusMS2N }}</td>
						<td>{{ $list->Sebab ? : 'Tidak Ada di MS2N' }}</td>
						<td>{{ $list->catatan ? : 'Tidak Ada Catatan' }}</td>
						<td>{{ $list->Ket_Sebab ? : 'Tidak Ada di MS2N' }}</td>
					</tr>
				@endif
				@endforeach
				</table>
      </div>
	  </div>
            <div class="list-group">
	      MAPPING WO BATULICIN HARI INI | KORLAP : GUSTI AHMAD SAUFI <br />
		  <div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tr>
						<td>No</td>
						<td>STO</td>
						<td>Area</td>
						<td>Nama Tim</td>
						<td>HD</td>
						<td>Tanggal Dispatch</td>
						<td>SC</td>
						<td>NCLI</td>
						<td>Ndem Speedy</td>
						<td>Nama Pelanggan</td>
						<td>Tipe WO</td>
						<td>Status Teknisi</td>
						<td>Status SC</td>
						<td>Status MS2N</td>
						<td>Catatan Teknisi</td>
						<td>Keterangan</td>

					</tr>
				{{-- */
					$number = 1;
					$UP_BLN = 0;
					$HR_BLN = 0;
					$OGP_BLN = 0;
					$KENDALA_TEKNIS_BLN = 0;
					$KENDALA_PELANGGAN_BLN = 0;
					$BELUM_SURVEY_BLN = 0;
					$JUMLAH_WO_BLN = 0;

				/* --}}


				@foreach ($get_list as $no => $list)
				@if ($list->area=="BLN")
				{{-- */
					if ($list->status_laporan==1)
						$UP_BLN++;
					if ($list->status_laporan==2)
						$KENDALA_TEKNIS_BLN++;
					if ($list->status_laporan==3)
						$KENDALA_PELANGGAN_BLN++;
					if ($list->status_laporan==4)
						$HR_BLN++;
					if ($list->status_laporan==5)
						$OGP_BLN++;
					if ($list->status_wo_by_hd=="BELUM SURVEY")
						$BELUM_SURVEY_BLN++;
					$JUMLAH_WO_BLN++;
				/* --}}
					<tr>
						<td>{{ ++$no }}</td>
						<td>{{ $list->sto ? : $list->mdf }}</td>
						<td>{{ $list->area }}</td>
						<td>{{ $list->uraian }}</td>
						<td>{{ $list->HD }}</td>
						<td>{{ $list->updated_at}}</td>
						<td><a href="/{{ $list->id_dt }}">SC{{ $list->orderId ? : $list->orderIdMS2N }}</a></td>
						<td>{{ $list->Ncli ? : $list->orderNcli }}</td>
						<td>{{ $list->ND_Speedy ? : $list->ndemSpeedy }}</td>
						<td>{{ $list->Nama ? : $list->orderName }}</td>
						<td>{{ $list->jenisPsb ? : $list->jenisPsbMS2N }}</td>
						<td>{{ $list->status_wo_by_hd ? : 'ANTRIAN' }}</td>
						<td>{{ $list->orderStatus ? : $list->orderStatusMS2N }}</td>
						<td>{{ $list->Sebab ? : 'Tidak Ada di MS2N' }}</td>
						<td>{{ $list->catatan ? : 'Tidak Ada Catatan' }}</td>
						<td>{{ $list->Ket_Sebab ? : 'Tidak Ada di MS2N' }}</td>
					</tr>
				@endif
				@endforeach
				</table>
				</div>
      </div>
	  </div>

    </div>
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading">REKAP</div>
    <div class="panel-body">
  	  <div class="list-group">
	  <table border=0 style="padding:5px;" class="table">
		<tr>
			<td width="50">No</td>
			<td width="100">Area</td>
			<td width="100">UP</td>
			<td width="100">HR</td>
			<td width="100">OGP</td>
			<td width="100">ODP LOS</td>
			<td width="100">INSERT TIANG</td>
			<td width="100">PROGRESS PT2/PT3</td>
			<td width="100">RESCHEDULE</td>
			<td width="100">SURVEY OK</td>
			<td width="100">FOLLOW UP SALES</td>
			<td width="100">UP DG SC LAIN</td>
			<td width="100">INPUT ULANG</td>
			<td width="100">ANOMALI</td>
			<td width="100">KEND. SISTEM</td>
			<td width="120">KEND. PELANGGAN</td>
			<td width="120">PERMINTAAN BATAL</td>
			<td width="100">BELUM SURVEY</td>
			<td width="100">JUMLAH WO</td>
		</tr>
		<?php
			$up = 0;
			$hr = 0;
			$ogp = 0;
			$odp_los = 0;
			$insert_tiang = 0;
			$progress_pt23 = 0;
			$reschedule = 0;
			$survey_ok = 0;
			$follow_up_sales = 0;
			$up_dg_sc_lain = 0;
			$input_ulang = 0;
			$anomali = 0;
			$kend_sistem = 0;
			$kend_pelanggan = 0;
			$permintaan_batal = 0;
			$belum_survey = 0;
			$jumlah_wo = 0;
			$jumlah = 0;
			$total = 0;
		?>

		@foreach ($get_report as $num => $report)
		<?php
			$up = $up + $report->UP;
			$hr = $hr +$report->HR;
			$ogp = $ogp + $report->OGP;
			$odp_los = $odp_los + $report->ODP_LOS;
			$insert_tiang = $insert_tiang + $report->INSERT_TIANG;
			$progress_pt23 = $progress_pt23 + $report->PROGRESS_PT23;
			$reschedule = $reschedule + $report->RESCHEDULE;
			$survey_ok = $survey_ok + $report->SURVEY_OK;
			$follow_up_sales = $follow_up_sales + $report->FOLLOW_UP_SALES;
			$up_dg_sc_lain = $up_dg_sc_lain + $report->UP_DG_SC_LAIN;;
			$input_ulang = $input_ulang + $report->INPUT_ULANG;
			$anomali = $anomali + $report->ANOMALI;
			$kend_sistem = $kend_sistem + $report->KEND_SISTEM;
			$kend_pelanggan = $kend_pelanggan + $report->KEND_PELANGGAN;
			$permintaan_batal = $permintaan_batal + $report->PERMINTAAN_BATAL;
			$belum_survey = $belum_survey + $report->BELUM_SURVEY;
			$jumlah_wo = 0;
			$jumlah_wo = $report->UP +
										$report->HR +
										$report->OGP +
										$report->ODP_LOS +
										$report->INSERT_TIANG +
										$report->PROGRESS_PT23 +
										$report->RESCHEDULE +
										$report->SURVEY_OK +
										$report->FOLLOW_UP_SALES +
										$report->UP_DG_SC_LAIN +
										$report->INPUT_ULANG +
										$report->ANOMALI +
										$report->KEND_SISTEM +
										$report->KEND_PELANGGAN +
										$report->PERMINTAAN_BATAL +
										$report->BELUM_SURVEY;
			$total = $total + $jumlah_wo;

		?>

		<tr>
			<td>{{ ++$num }}</td>
			<td>{{ $report->area }}</td>
			<td>{{ $report->UP }}</td>
			<td>{{ $report->HR }}</td>
			<td>{{ $report->OGP }}</td>
			<td>{{ $report->ODP_LOS }}</td>
			<td>{{ $report->INSERT_TIANG }}</td>
			<td>{{ $report->PROGRESS_PT23 }}</td>
			<td>{{ $report->RESCHEDULE }}</td>
			<td>{{ $report->SURVEY_OK }}</td>
			<td>{{ $report->FOLLOW_UP_SALES }}</td>
			<td>{{ $report->UP_DG_SC_LAIN }}</td>
			<td>{{ $report->INPUT_ULANG }}</td>
			<td>{{ $report->ANOMALI }}</td>
			<td>{{ $report->KEND_SISTEM }}</td>
			<td>{{ $report->KEND_PELANGGAN }}</td>
			<td>{{ $report->PERMINTAAN_BATAL }}</td>
			<td>{{ $report->BELUM_SURVEY }}</td>
			<td>{{ $jumlah_wo }}</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="2">TOTAL</td>
			<td>{{ $up }}</td>
			<td>{{ $hr }}</td>
			<td>{{ $ogp }}</td>
			<td>{{ $odp_los }}</td>
			<td>{{ $insert_tiang }}</td>
			<td>{{ $progress_pt23 }}</td>
			<td>{{ $reschedule }}</td>
			<td>{{ $survey_ok }}</td>
			<td>{{ $follow_up_sales }}</td>
			<td>{{ $up_dg_sc_lain }}</td>
			<td>{{ $input_ulang }}</td>
			<td>{{ $anomali }}</td>
			<td>{{ $kend_sistem }}</td>
			<td>{{ $kend_pelanggan }}</td>
			<td>{{ $permintaan_batal }}</td>
			<td>{{ $belum_survey }}</td>
			<td>{{ $total }}</td>
		</tr>


	  </table>
	  </div>
	  </div>
	  </div>

		<div class="panel-body">
		<script>
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
			</script>

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
