@extends('layout')

@section('content')
@include('partial.alerts')
<div class="panel panel-default">
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>SC</th>
                <th width="10%">MYIR</th>
                <th>Nama Pelanggan</th>
                <th>No. INET</th>
                <th>No. Telpon</th>
                <th>Regu</th>
                <th>Sektor</th>
                <th>STO</th>
                <th>Area</th>
                <th>Tgl Dispatch</th>
                <th>Status Kendala</th>
                <th>Tgl Status Kendala</th>
                <th>Bulan</th>
                <th>Status SC Kendala</th>
                <th>Tgl Re-dispatch</th>
                <th>Status Laporan </th>
                <th>Tgl Up </th>
                <th>Status SC UP</th>
                <th>Ket</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no=>$dt)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td><a href="/{{ $dt->id_dt }}" >{{ $dt->orderId }}</a></td>

                    <?php
                        $data = explode(';', $dt->kcontact);
                        if (count($data)>1){
                            $myir = $data[1];
                        }
                        else{
                            $myir = '~';
                        }
                     ?>

                    <td>{{ $myir }}</td>
                    <td>{{ $dt->orderName }}</td>
                    <td>{{ $dt->internet }}</td>
                    <td>{{ $dt->noTelp }}</td>
                    <td>{{ $dt->uraian }}</td>
                    <td>{{ $dt->title }}</td>
                    <td>{{ $dt->area }}</td>
                    <td>{{ $dt->area_migrasi }}</td>
                    <td>{{ date('Y-m-d',strtotime($dt->tglDispatch)) }}</td>
                    <td>{{ $dt->status_kendala }}</td>
                    <td>{{ date('Y-m-d',strtotime($dt->tgl_status_kendala)) }}</td>
                    <td>{{ date('M',strtotime($dt->tgl_status_kendala)) }}</td>
                    <td>{{ $dt->status_sc ? : '~'}}</td>
                    <td>{{ $dt->tglReDispatch }}</td>
                    <td>{{ $dt->laporan_status }}</td>
                    <td>{{ $dt->tgl_up ?: date('Y-m-d',strtotime($dt->modified_at)) }}</td>
                    <td>{{ $dt->orderStatus }}</td>
                    @if ($dt->orderStatus=="Completed (PS)" || $dt->orderStatus=="Process OSS (Activation Completed)" )
                        <td>PS</td>
                    @else
                        <td>Belum PS</td>
                    @endif
                </tr>
            @endforeach()

            @foreach($dataMyir as $dt)
                <tr>
                    <td>{{ $noMyir++ }}</td>
                    <td><a href="/{{ $dt->id_dt }}" >{{ $dt->Ndem }}</a></td>
                    <td>~</td>
                    <td>{{ $dt->customer }}</td>
                    <td>~</td>
                    <td>{{ $dt->picPelanggan }}</td>
                    <td>{{ $dt->uraian }}</td>
                    <td>{{ $dt->title }}</td>
                    <td>{{ $dt->area }}</td>
                    <td>{{ $dt->area_migrasi }}</td>
                    <td>{{ date('Y-m-d',strtotime($dt->tglDispatch)) }}</td>
                    <td>{{ $dt->status_kendala }}</td>
                    <td>{{ date('Y-m-d',strtotime($dt->tgl_status_kendala)) }}</td>
                    <td>{{ date('M',strtotime($dt->tgl_status_kendala)) }}</td>
                    <td>{{ $dt->status_sc ? : '~'}}</td>
                    <td>{{ $dt->tglReDispatch }}</td>
                    <td>{{ $dt->laporan_status }}</td>
                    <td>{{ $dt->tgl_up ?: date('Y-m-d',strtotime($dt->modified_at)) }}</td>
                    <td>~</td>
                    <td>Belum PS</td>
                </tr>
            @endforeach()


           
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
