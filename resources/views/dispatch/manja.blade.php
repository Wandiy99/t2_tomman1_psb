@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off" action="/dispatch/manja/{{ $id }}/save">
    {{-- <input name="_method" type="hidden" value="PUT"> --}}
    {{-- <input type="hidden" name="sto" value="{{ $data->sto }}"> --}}
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="/manja/list/{{ date('Y-m-d')}}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>

      @if($ketSc=='myir')
          Manja MYIR {{ $data->myir == '' ? '' : $data->myir }} [ {{ $data->customer }} ]
      @else
          Manja WO {{ $data->Ndem == '' ? $data->orderId : $data->Ndem}} [ {{ $data->Nama ? : $data->orderName }} ]
      @endif

    </h3>

    <div class="form-group {{ $errors->has('manja_status') ? 'has-error' : '' }}">
      <label class="control-label" for="input-manja-status">Status Manja</label>
		  <input name="manja_status" type="hidden" id="input-manja-status" class="form-control" value="{{ $data->manja_status }}"  />
      {!! $errors->first('manja_status','<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group {{ $errors->has('manja') ? 'has-error' : '' }}">
      <label class="control-label" for="manja">Manja</label>
		  <textarea name="manja" class="form-control">{{   $data->manja }}</textarea>
      {!! $errors->first('manja','<span class="label label-danger">:message</span>') !!}
    </div>
  
    <div>
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script>
      $(function() {
          var manja_data = <?= json_encode($get_manja_status) ?>;
          var manja = function() {
            return {
              data: manja_data,
              placeholder: 'Status Manja',
              formatResult: function(data) {
              return  '<span class="label label-default">'+data.id+'</span>'+
                    '<strong style="margin-left:5px">'+data.text+'</strong>';
              }
            }
          }
          $('#input-manja-status').select2(manja());
      })
  </script>
@endsection
