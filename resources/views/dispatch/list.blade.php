@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlist')

  <div class="table-responsive">
  <table class="table table-striped table-bordered dataTable">
    <thead>
      <tr>
        <th>#</th>
		    <th width="75" colspan=3>Work Order</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)
      <tr>
        <td rowspan=3>{{ ++$no }}.</td>
		@if(session('auth')->level <> 19)
        <td colspan=3><span>
		@if(!empty($data->id_r))
        <?php
          echo substr($data->hd_date,0,10);
        ?>
        	<span class="label label-warning">{{ $data->uraian }}</span>
          @if(session('auth')->id_karyawan == $data->hd || substr($data->hd_date,0,10) != date('Y-m-d'))
            <a href="/dispatch/{{ $data->orderId }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
          @endif
          <span class="label label-warning">{{ $data->hd }}</span>
					<span class="label label-warning">{{ $data->updated_at }}</span>
        @else
          <a href="/dispatch/{{ $data->orderId }}" class="label label-info">Dispatch</a>
        @endif
		</span>

        <span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem={{ $data->Ndem }}&etat=VA" class="label label-info">Update MS2N</a></span>
        <span><a href="/sendsms/{{ $data->orderId }}" class="label label-warning">Send SMS</a></span>
        <span><a href="/sendtotelegram/{{ $data->id_dt }}" class="label label-warning">Send to Telegram</a></span>
        </td>
		@endif
		</tr>
		<tr>
        <td colspan=1>

	        {{ $data->orderStatus }}
          @if ($data->flagging <> "")
          WO Cluster {{ $data->flagging }}
          @endif
          <br />

			@if ($data->id_dt<>'')
			<a href="/{{ $data->id_dt }}">SC{{ $data->orderId ? : 'Tidak Ada SC' }}</a>
			@else
			SC{{ $data->orderId ? : 'Tidak Ada SC' }}
			@endif

			/ {{ $data->orderDate ? : 'Tidak Ada SC' }}<br />
	        {{ $data->ND ? : $data->ndemPots }}~{{ $data->ND_Speedy ? : $data->ndemSpeedy }}<br />
	        {{ $data->Nama  ? : $data->orderName }}<br />
	        {{ $data->sto }} /
	        {{ $data->jenisPsb }} <br />
	        {{ $data->Deskripsi }} <br />
	        {{ $data->alproname ? : 'Tidak Ada Informasi ODP' }} <br />
			@if ($data->LOCN_X <> '')
			({{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }}, {{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }}) / {{ $data->OLT_IP }} / {{ $data->OLT }} <br />
			Lokasi ODP : http://maps.google.com/?q={{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }},{{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }} <br />
			@endif
	        {{ $data->Kcontact ? : $data->kcontact }}<br />
	        {{ $data->Jalan ? : '' }} {{ $data->No_Jalan ? : '' }} {{ $data->Distrik ? : '' }}<br />
	        {{ $data->Kota ? : '' }}
          {{ $data->orderAddr ? : '' }} </td>

          <td>
            @if ($data->nik_sales <> '')
            {{ $data->nik_sales }}<br />
            Korlap : {{ $data->korlap }}<br />
            {{ $data->nama_pelanggan }}<br />
            {{ $data->pic }}<br />
            {{ $data->alamat }}<br />
            {{ $data->nama_odp }}<br />
            Kordinat ODP : {{ $data->koor_odp }}<br />
            Kordinat Pelanggan : {{ $data->koor_pel }}<br />
            {{ $data->keterangan }}<br />
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan-th.jpg" /></a>
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP-th.jpg" /></a>
          @endif
        </td>
	     </tr>
			<tr>
        <td>
	        <span>Sebab : <br />{{ $data->Sebab }}</span><br/>
	        <span>{{ $data->Ket_Sebab }}</span><br />
	        <span>Solusi : <br />{{ $data->Ket_Solusi }}</span><br />
		</td>
		<td valign="top">
	        Catatan Teknisi : <br />
	        {{ $data->catatan ? : 'Tidak ada' }}
		<br />{{ $data->modified_at }}
		</td>

      </tr>
      @endforeach
    </tbody>
  </table>
  </div>



  <div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Verifikasi</h4>
				</div>

				<div class="modal-body">
					Anda akan mengirimkan SMS Notifikasi kepada Pelanggan
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-info">Lanjutkan</button>
				</div>
			</div>
		</di

  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
