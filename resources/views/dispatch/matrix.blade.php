@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    .red {
      background-color: #FF0000;
      color : #FFF;
    }
    .label-greenx{
      background-color: #1abc9c;
    }
    .myindihome {
      border : 1px solid #000;
    }
    .label-darkgrey {
      background-color: #6b6b6b;

    }
    .label-bluepas {
      background-color : #fd79a8;
    }
    .label-purple {
      background-color : #800080;
    }
    .label-black {
      background-color: #000000;
    }

    .label-coklat {
      background-color: #A0522D;
    }
    .label-golden{
       background-color: #008B8B;
    }

    a {
      cursor: pointer;
    }
    .geser_kiri_dikit {
      position: relative;
      left : -12px;
    }
  </style>

  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <div style="padding-bottom: 10px;">
    <h3>Matrix Workorder </h3>
    <form method="get">
      SEKTOR :
      <select name="sektor">
        <option value="">--</option>
        <option value="ALL" <?php if ($area=="ALL") echo 'selected = "selected"'; else ''; ?>>ALL</option>
        <option value="ASR" <?php if ($area=="ASR") echo 'selected = "selected"'; else ''; ?>>ASR ONLY</option>
        <option value="PROV" <?php if ($area=="PROV") echo 'selected = "selected"'; else ''; ?>>PROV ONLY</option>
        <option value="INNER" <?php if ($area=="INNER") echo 'selected = "selected"'; else ''; ?>>INNER ONLY</option>
        <option value="OUTER" <?php if ($area=="OUTER") echo 'selected = "selected"'; else ''; ?>>OUTER ONLY</option>

        @foreach ($get_sektor as $sektor)
        <?php if ($sektor->sektor==$area) { $selected = 'selected="selected"'; } else { $selected = ''; }  ?>
        <option value="{{ $sektor->sektor }}" {{ $selected }}>{{ $sektor->sektor }}</option>
        @endforeach
      </select>

      DATE :
      <input type="text" name="date" id="input-tgl" value="{{ $date }}" />
      <input type="submit" />
    </form>
  </div>

  @if (count($get_area)>0)
  @foreach ($get_area as $result_area)
  @if (array_key_exists($result_area->chat_id, $data))
  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX ORDER {{ $result_area->title }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
        <?php
          if (strpos($result_area->title,'ASR') !== false){
            //echo "SISA TIKET : ";
          }
        ?>

        <table class="table table-striped table-bordered">
          <tr>
            <th width="10" rowspan="2">No</th>
            <th width="200" rowspan="2">Nama Tim</th>
            <th width="60" rowspan="2">Target MH</th>
            <th width="60" colspan=2 align="center"><center>WO</center></th>
            <th width="60" rowspan="2" align="center"><center>ANTRIAN</center></th>
            <th width="60" rowspan="2" align="center"><center>PROGRESS</center></th>
            <th width="60" rowspan="2" align="center"><center>KENDALA</center></th>
            <th width="60" rowspan="2" align="center"><center>UP</center></th>
            <th width="60" rowspan="2" align="center"><center>MH ACH</center></th>
            <th width="60" rowspan="2" align="center"><center>PROD</center></th>
            <th rowspan="2">LIST</th>
          </tr>
          <tr>
            <th width=30>Jumlah</th>
            <th width=30>MH</th>
          </tr>
          <!-- <tr>
            <th width="30">ALL</th>
            <th width="30">A</th>
            <th width="30">F</th>
            <th width="30">ALL</th>
            <th width="30">A</th>
            <th width="30">F</th>
          </tr> -->

          <?php
            $total_target_MH = 0;
            $total_jumlah = 0;
            $total_MH = 0;
            $total_wo_no_update = 0;
            $total_progress = 0;
            $total_kendala = 0;
            $total_up = 0;
            $total_MH_UP = 0;
            $total_PROD = 0;

            $number = 1;
            $list = $data[$result_area->chat_id];

            for ($i=0;$i<count($list);$i++){
              ?>
              @if( count($list[$i]['listWO'])<>0 )

              <tr>
                <td>{{ $number }}</td>

                <td>
                  <small>{{ @$list[$i]['nik1'] ? : '0' }} // {{ @$list[$i]['nik2'] ? : '0' }}</small>
                {{ $list[$i]['uraian'] }}  <span class="label label-primary">{{ @$list[$i]['mitra'] }}</span>
                @if ($list[$i]['nik1_absen']==1 || $list[$i]['nik2_absen']==1)
                      <span class="label label-success">HADIR</span>
                @else
                      <span class="label label-info">BELUM HADIR</span>
                @endif
                </td>

                </td>
                <td><center>{{ $list[$i]['target_MH'] }}</center></td>
                <?php
                  $MH = $list[$i]['gangguan_Copper_MH'] +
                        $list[$i]['gangguan_GPON_MH'] +
                        $list[$i]['gangguan_DES_MH'] +
                        $list[$i]['prov_MH']+
                        $list[$i]['ccan_MH']+
                        $list[$i]['addon_MH']+
                        $list[$i]['asis_MH'];
                ?>
                <td><center>{{ $list[$i]['jumlah'] }}</center></td>
                <td>{{ $MH }}</td>
                <!-- <td><center>{{ $list[$i]['gangguan'] }}</center></td> -->
                <!-- <td><center>{{ $list[$i]['prov'] }}</center></td> -->
                <?php
                  $MH_UP = $list[$i]['gangguan_UP_Copper'] +
                        $list[$i]['gangguan_UP_GPON'] +
                        $list[$i]['gangguan_UP_DES'] +
                        $list[$i]['addon_UP']+
                        $list[$i]['prov_UP']+
                        $list[$i]['ccan_UP']+
                        $list[$i]['asis_UP'];
                  $PROD = @($MH_UP / $list[$i]['kemampuan'])*100;
                  $wo_no_update = $list[$i]['jumlah'] - ($list[$i]['UP'] +
                                  $list[$i]['KENDALA'] +
                                  $list[$i]['PROGRESS']);

                  $total_target_MH = $total_target_MH + $list[$i]['target_MH'];
                  $total_jumlah = $total_jumlah + $list[$i]['jumlah'];
                  $total_MH = $total_MH + $MH;
                  $total_wo_no_update = $total_wo_no_update + $wo_no_update;
                  $total_progress = $total_progress + $list[$i]['PROGRESS'];
                  $total_kendala = $total_kendala + $list[$i]['KENDALA'];
                  $total_up = $total_up + $list[$i]['UP'];
                  $total_MH_UP = $total_MH_UP + $MH_UP;
                  $total_PROD = $total_PROD + $PROD;
                ?>
                @if ($wo_no_update>0)
                <td class="red"><center>{{ $wo_no_update }}</center></td>
                @else
                <td><center>{{ $wo_no_update }}</center></td>
                @endif
                <td><center>{{ $list[$i]['PROGRESS'] }}</center></td>
                <td><center>{{ $list[$i]['KENDALA'] }}</center></td>
                <td><center>{{ $list[$i]['UP'] }}</center></td>
                <td><center>{{ $MH_UP }}</center></td>

                @if ($list[$i]['jns']=='prov')
                    @if ($list[$i]['nik1_absen']==1 || $list[$i]['nik2_absen']==1)
                        <td><center>{{ round($PROD,2) }}%</center></td>
                    @else
                        <td class="red"><center>{{ round($PROD,2) }}%</center></td>
                    @endif
                @else
                    <td><center>{{ round($PROD,2) }}%</center></td>
                @endif
                <!-- <td><center>{{ $list[$i]['prov_UP'] }}</center></td> -->

                <td>
                  @foreach ($list[$i]['listWO'] as $num => $wo)
                    <?php
                      if ($wo->status_laporan==6 || $wo->status_laporan==NULL){
                        $label = 'default';
                      } else if ($wo->status_laporan <> 28 && $wo->status_laporan <> 29 && $wo->status_laporan <> 30 && $wo->status_laporan <> 7 && $wo->status_laporan<>5 && $wo->status_laporan<>6 && $wo->status_laporan<>1 && $wo->status_laporan<>4 && $wo->status_laporan<>31 && $wo->status_laporan<>37 && $wo->status_laporan<>38 && $wo->status_laporan<>43 && $wo->status_laporan<>44 && $wo->status_laporan<>45 && $wo->status_laporan<>48 && $wo->status_laporan<>50 && $wo->status_laporan<>46) {
                        $label = 'danger';
                      } else if ( $wo->status_laporan==5){
                        $label = 'warning';
                      } else if ( $wo->status_laporan==4){
                        $label = 'info';
                      } else if ($wo->status_laporan==1 || $wo->status_laporan==37 || $wo->status_laporan==38){
                        $label = 'success';
                      } else if ($wo->status_laporan==7){
                        $label = 'darkgrey';
                      } else if ($wo->status_laporan==28 || $wo->status_laporan==29) {
                        $label = 'bluepas';
                      } else if ($wo->status_laporan==31){
                        $label = 'purple';
                      } else if ($wo->status_laporan==43 || $wo->status_laporan==44 ){
                        $label = 'black';
                      } else if ($wo->status_laporan==45 || $wo->status_laporan==48 || $wo->status_laporan==46){
                        $label = 'coklat';
                      } else if ($wo->status_laporan==50){
                        $label = 'golden';
                      }
                        else {
                        $label = 'default';
                      };

                      if ($wo->status_laporan==1){
                        $close_status = "";
                      } else {
                        //$close_status = " | ".$wo->TROUBLE_OPENTIME ? : $wo->orderDate;
                          $close_status = " | ".round($wo->umur_gangguan/24)." Day";
                      }

                      if (($wo->status_laporan==1) && ($wo->close_status != '')){
                        //$close_status = " |x ".$wo->umur_gangguan." Day";
                        $label = 'greenx';
                      }

                      if ($wo->status_laporan==5){
                        if ($wo->modified_at<>""){
                          $hitung = $wo->interval1/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        } else {
                          $hitung = $wo->interval2/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        }
                        $timeinterval = ' | '.$timex;
                      } else {
                        $timeinterval = '';
                      }

                      if ($wo->status_laporan==28 || $wo->status_laporan==29){
                        if ($wo->modified_at<>""){
                          $hitung = $wo->interval1/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        } else {
                          $hitung = $wo->interval2/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        }
                        $timeinter = ' | '.$timex;
                      } else {
                        $timeinter = '';
                      };

                      if ($wo->status_laporan==31){
                        if ($wo->modified_at<>""){
                          $hitung = $wo->interval1/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        } else {
                          $hitung = $wo->interval2/60;
                          if ($hitung<1){
                            $timex = round($hitung*60,2)." Min";
                          } else {
                            $timex = round($hitung,2)." Hour";
                          }
                        }
                        $timeinterset = ' | '.$timex;
                      } else {
                        $timeinterset = '';
                      };

                      if ($wo->status_laporan==1){
                        if ($wo->uptime>0) {
                          $timex = $wo->uptime." Hour";
                        } else {
                          $timex = $wo->uptimeM2." Min";
                        }

                        $timeinterset = ' | '.$timex;
                      }

                      if ($wo->dispatch_by=='4'){
                          $noTiket = substr($wo->Ndem, 0, 2);
                          if ($noTiket=='IN'){
                              $tiket = $wo->Ndem;
                          }
                          else{
                              $tiket = 'FG '.$wo->Ndem;
                          };
                      }
                      else{
                          $tiket = $wo->Ndem;
                      };

                      if ($wo->action==1){
                        $wook = "• ".$tiket;
                      } else {
                        $wook = $tiket;
                      };

                      if ($wo->channel=="6"){
                        $t_border = 'myindihome';
                      } else {
                        $t_border = '';
                      }

                      $jnsPsb = substr($wo->jenisPsb, 0, 2);
                    ?>

                      @if ($wo->dispatch_by=='4')
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->TROUBLE_OPENTIME ? : $wo->orderDate }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'ANTRIAN' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : </b><br />{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/guarantee/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}"> {{ $tiket }} ({{ $wo->umur_gangguan }}) {{ $close_status }}{{ $timeinterval }}{{ $timeinter }}{{ $timeinterset }}</a>
                      @elseif ($wo->dispatch_by=='5')
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->TROUBLE_OPENTIME ? : $wo->orderDate ? : $wo->created_at }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'ANTRIAN' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : <br /></b>{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/{{ $wo->id_dt }}'>Detil</a>" class="label label-{{ $label }}">{{'MYIR-'}} {{ $wook }} ({{ $wo->umur_gangguan ? : 0 }} Hour){{ $close_status }}{{ $timeinterval }}{{ $timeinter }}{{ $timeinterset }}</a>
                      @else
                          <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ $wo->TROUBLE_OPENTIME ? : $wo->orderDate }}<br /><b>Dispatch at</b> :<br />{{ $wo->updated_at }}<br /><b>Status</b> : <br />{{ $wo->laporan_status ? : 'ANTRIAN' }} / <small>{{ $wo->modified_at ? : $wo->created_at ? : '~' }}</small><br /><b>Jenis Psb : </b><br />{{ $wo->jenisPsb ?: '-'}}<br /><b>Jenis Layanan : </b><br />{{ $wo->jenis_layanan ?: '-'}}<br /><b>Catatan Teknisi</b> : <br />{{ $wo->catatan }}<br /><a href='/{{ $wo->id_dt }}'>Detil</a><br /><a href='/qcview/{{ $wook }}'>Approve</a>" class="label label-{{ $label }}">{{ $jnsPsb=='MO' ? 'SC' : '' }} {{ $wook }} ({{ $wo->umur_gangguan ? : 0 }} Hour){{ $close_status }}{{ $timeinterval }}{{ $timeinter }}{{ $timeinterset }}</a>
                      @endif


                    <?php
                    if ($wo->umur_gangguan>72 || $wo->umur_prov>72){
                    ?>
                    <img src="/image/hot.png" class="geser_kiri_dikit" />
                    <?php
                  } else if ($wo->umur_gangguan>48 || $wo->umur_prov>48){
                    ?>
                    <img src="/image/hot2.png" class="geser_kiri_dikit" />
                    <?php
                     }
                    ?>
                    <?php
                      ++$num;

                      if ($num%4) {} else {
                        echo "<br />";
                      }
                    ?>
                  @endforeach
                </td>
              </tr>

              @endif

              <?php
              $number++;
            }
          ?>
          <tr>
            <td colspan="2"><center>Total</center></td>
            <td><center>{{ $total_target_MH }}</center></td>
            <td><center>{{ $total_jumlah }}</center></td>
            <td><center>{{ $total_MH }}</center></td>
            <td><center>{{ $total_wo_no_update }}</center></td>
            <td><center>{{ $total_progress }}</center></td>
            <td><center>{{ $total_kendala }}</center></td>
            <td><center>{{ $total_up }}</center></td>
            <td><center>{{ $total_MH_UP }}</center></td>
            <td><center>{{ round($total_PROD/$number,2) }}%</center></td>
            <td><center></center></td>
          </tr>
        </table>

      </div>
    </div>
  </div>
</div>
@endif
@endforeach
@else
<div id="matrix"></div>
@endif

@if($area=="PROV")
    <div class="panel panel-primary">
        <div class="panel-heading">Tim Hadir Belum Dapat WO}</div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <th width="5%">No</th>
                <th>Nama TIM</th>
                <th>Sektor</th>

                @foreach($reguNotWo as $no => $list)
                    <tr>
                      <td>{{++$no}}</td>
                      <td>{{ $list->uraian }}</td>
                      <td>{{ $list->title }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endif



		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

        })


				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

						</div>
					</div>
  </div>
@endsection
