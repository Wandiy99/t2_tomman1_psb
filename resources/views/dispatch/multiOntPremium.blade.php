@extends('layout')
@section('content')
  @include('partial.alerts')
  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Harap Perhatikan Format yang Digunakan !!! Jangan Ada Field yang Kosong atau Memakai Garis Baru (Enter)</strong>
  </div>

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <h3>
      Multi Dispatch Order ONT Premium
    </h3>

    <div class="form-group">
        <label for="input-regu" class="col-form-label">REGU</label>
        <input type="hidden" name="id_regu" class="form-control" id="input-regu"/>
        {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group">
        <label for="tgl-dispatch" class="col-form-label">TANGGAL DISPATCH</label>
        <input type="date" name="tgl_dispatch" class="form-control" id="tgl-dispatch">
        {!! $errors->first('tgl_dispatch','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="multi">Format yang di Gunakan <b>( INET, VOICE, NAMA PELANGGAN )</b> Pisah dengan simbol Titik Koma dan Jangan Gunakan Baris Baru (Enter)</label>
		<textarea name="multi" id="multi" class="form-control" rows="5" required /></textarea>
        {!! $errors->first('multi','<p class="label label-danger">:message</p>') !!}
	</div>
    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Sync</button>
    </div>
  </form>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Pilih Regu',
          formatResult: function(data) {
          return '<p>'+data.text+'</p>';
          }
        }
      }
      $('#input-regu').select2(regu());
    });
</script>
@endsection