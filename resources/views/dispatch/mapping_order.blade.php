@extends('layout')

@section('content')
  @include('partial.alerts')
    <style>
    /* .marker {
     background-size: cover;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    cursor: pointer;
    }
    */
    .mapboxgl-popup {
      font-size: 12px;
    }
    </style>
    <script src="/js/mapbox-gl.js"></script>
    <link rel="stylesheet" href="/css/mapbox-gl.css" />
  <div id="mapMarkerModal_mapView">x</div>

<script>
    (function (){
        var mapEl = document.getElementById('mapMarkerModal_mapView');
        mapEl.style.height = (window.innerHeight * .65) + 'px';
        var map;
        var marker;
        var $in;
        var $modal = $('#mapMarkerModal');
        var $title = $('#mapMarkerModal_title');
        var $latText = $('#mapMarkerModal_latText');
        var $lngText = $('#mapMarkerModal_lngText');

        var $okBtn = $('#mapMarkerModal_okBtn');

        var setLngLat = function(ll) {
            marker.setLngLat(ll);
            map.panTo(ll);
            $latText.html(ll.lat);
            $lngText.html(ll.lng);
        }

        var getLngLat = function(text) {
            var tokens = text.split(',');
            if (tokens.length != 2) return center;

            return {
                lat: tokens[0].trim(),
                lng: tokens[1].trim()
            }
        }

        var initMap = function() {
            if (map) return;
            var center = { lng: 114.590097, lat: -3.319583 };
            map = new mapboxgl.Map({
                container: mapEl,
                center: center,
                zoom: 12,
                style: 'https://map.tomman.app/styles/osm-liberty/style.json'
            });

            <?php
            foreach ($query as $num => $data) {
            ?>
            var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
            'ORDER ID : {{ $data->order_id }}<br />TIM : {{ $data->uraian ? : "UNDISPATCH" }}<br />STATUS : {{ $data->laporan_status }}<br /> '
            );

            new mapboxgl.Marker({ draggable: false, icon:'default', color:'red', cursor:'pointer' })
                .setLngLat({lat : '{{ $data->lat ? : 0 }}',lon : '{{ $data->lon ? : 0 }}'})
                .setPopup(popup)
                .addTo(map);
            <?
            }
            ?>


        }

        initMap();
    })()
</script>
</div>
@endsection
