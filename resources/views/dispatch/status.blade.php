@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3>
      <a href="/dispatch/reportPsb" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Dispatch WO {{ $data->Nama }}
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="<?= $data->status_wo_by_hd ?>" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

  <script>
    $(function() {
      var status = $('#input-status').select2({
        data: [{"id":1, "text":"SELESAI"},{"id":2, "text":"KENDALA"},{"id":3, "text":"RESCHEDULE"},{"id":4, "text":"HR"},{"id":5, "text":"OGP"},{"id":6, "text":"BELUM SURVEY"}],
        allowClear: true,
        placeholder: 'Input status',
        formatResult: function(data) {
          return  '<span class="label label-default">'+data.id+'</span>'+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
      });
    })
  </script>
@endsection
