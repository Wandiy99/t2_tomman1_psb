@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>MATRIX PROVISIONING KALSEL</h3>
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .label-KP {
    background-color: #c0392b;
  }
  .label-KT {
    background-color: #8e44ad;
  }
  .label-HR {
    background-color: #2980b9;
  }

  .label-UP {
    background-color: #27ae60;
  }
  .label-OGP {
    background-color: #ff9800;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-NP {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }
  .underline {
    text-decoration: underline;
  }
  .gaul {
    border: 2px solid #606060;
  }
  .label-bluepas {
      background-color : #fd79a8;
    }
  .label-deep-pink {
      background-color : #FF1493;
    }
    .label-black {
      background-color : #000000;
      border-color : #FFFFFF;
      color : $FFFFFF;
    }
  .table_matrix{
    text-align: center !important;
    font-weight: bold;
  }
  .bg-danger {
    background-color: #e74c3c;
    color : #FFF;
  }
  </style>
  <div class="row">
    <div class="col-sm-12">
      @foreach ($get_sektor as $sektor)
      <div class="panel panel-primary">
          <div class="panel-heading">{{ $sektor->title }}</div>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th width="10">#</th>
                <th width="300">TIM</th>
                <th width="50">ORDER</th>
                <th width="50">NP</th>
                <th width="50">OGP</th>
                <th width="50">KT</th>
                <th width="50">KP</th>
                <th width="50">HR</th>
                <th width="50">UP</th>
                <th>LIST ORDER</th>

              </tr>
              <?php
                $total_ORDER = 0;
                $total_UP = 0;
                $total_KT = 0;
                $total_KP = 0;
                $total_OGP = 0;
                $total_HR = 0;
                $total_NP = 0;
              ?>
              @foreach ($matrix[$sektor->chat_id] as $num => $team)
              @if (@$team->uraian<>"")
              <?php
                $total_ORDER += count(@$matrix[@$sektor->chat_id][@$team->id_regu]);
              ?>
              <tr>
                <td>{{ ++$num }}</td>
                <td>
                  {{ $team->uraian }}
                  @if($team->ket_bantek <> null)
                  <br /><span class="label label-success">{{ $team->ket_bantek }}</span>
                  @endif
                </td>
                <td class="table_matrix">{{ count(@$matrix[@$sektor->chat_id][@$team->id_regu]) }}</td>
                  <?php
                    $UP = 0;
                    $HR = 0;
                    $KT = 0;
                    $KP = 0;
                    $OGP = 0;
                    $NP = 0;
                  ?>
                  @foreach (@$matrix[@$sektor->chat_id][@$team->id_regu] as $num => $team_order)
                  <?php
                    if (@$team_order->grup=="UP") $UP += 1;
                    if (@$team_order->grup=="HR") $HR += 1;
                    if (@$team_order->grup=="KT") $KT += 1;
                    if (@$team_order->grup=="KP") $KP += 1;
                    if (@$team_order->grup=="OGP") $OGP += 1;
                    if (@$team_order->grup=="SISA") $NP += 1;
                    if (@$team_order->grup=="NP" || @$team_order->grup=="") $NP += 1;

                    $colorOGP = "";
                    $color = "";
                    $colorUP = "";
                    if ($NP==0){
                      $color = "bg-danger";
                    }
                    if ($OGP==0 && $NP>0){
                      $colorOGP = "bg-danger";
                    }
                    if ($UP==0){
                      $colorUP = "bg-danger";
                    }
                  ?>
                  @endforeach
                <td class="table_matrix {{ @$color }}">{{ $NP }}</td>
                <td class="table_matrix {{ @$colorOGP }}">{{ $OGP }}</td>
                <td class="table_matrix">{{ $KT }}</td>
                <td class="table_matrix">{{ $KP }}</td>
                <td class="table_matrix">{{ $HR }}</td>
                <td class="table_matrix {{ @$colorUP }}">{{ $UP }}</td>
                <td>
                  <?php
                    $UP = 0;
                    $HR = 0;
                    $KT = 0;
                    $KP = 0;
                    $OGP = 0;
                    $NP = 0;
                  ?>
                  @foreach (@$matrix[@$sektor->chat_id][@$team->id_regu] as $num => $team_order)
                  <?php
                    if (@$team_order->laporan_status=="UP") $UP += 1;
                    if (@$team_order->grup=="HR") $HR += 1;
                    if (@$team_order->grup=="KT") $KT += 1;
                    if (@$team_order->grup=="KP") $KP += 1;
                    if (@$team_order->grup=="OGP") $OGP += 1;
                    if (@$team_order->grup=="SISA") $NP += 1;
                    if (@$team_order->grup=="NP" || @$team_order->grup=="") $NP += 1;
                  ?>

                  <?php
                  if (@$team_order->Assigned_by=="CUSTOMERASSIGNED") {
                    $underLine = 'underline';
                  } else {
                    $underLine = '';
                  }
                  if (@$team_order->GAUL>0) {
                    $gaul = 'gaul';
                  } else {
                    $gaul = '';
                  }


                  $labelWarna = $team_order->grup;
                  if ($team_order->laporan_status=="KIRIM TEKNISI"){
                      $labelWarna = 'bluepas';
                  }elseif ($team_order->laporan_status=="PENDING H+") {
                      $labelWarna = 'deep-pink';
                  }elseif ($team_order->laporan_status=="PENDING HI") {
                      $labelWarna = 'deep-pink';
                  }elseif ($team_order->laporan_status=="CANCEL") {
                      $labelWarna = 'black';
                  }elseif ($team_order->laporan_status=="HR" && $team_order->orderStatus=="COMPLETED") {
                      $labelWarna = 'UP';
                  };

                  date_default_timezone_set('Asia/Makassar');
                  if ($team_order->modified_at=='' || $team_order->modified_at==NULL){
                    $jamWo = $team_order->updated_at;
                  }
                  else{
                    $jamWo = $team_order->modified_at;
                  };

                  if ($jamWo<>''){
                      $awal  = date_create($jamWo);
                      $akhir = date_create(); // waktu sekarang
                      $diff  = date_diff( $awal, $akhir );

                      if($diff->d <> 0){
                            $waktu = $diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                      }
                      else {
                            $waktu = $diff->h.' Jam | '.$diff->i.' Menit';
                      }
                  }
                  else{
                    $waktu = ' UNDEFINED ';
                  }

                  ?>
                  <a class="label label-{{ $labelWarna }} {{ $underLine }} {{ $gaul }}" data-html="true" data-original-title data-toggle="popover" data-placement="bottom" title="" data-content="<b>Open Order</b> :<br />{{ @$team_order->Reported_Date}}<br /><b>TGL ORDER</b> :<br />{{ @$team_order->tgl }}<br /><b>Status</b> : <br />{{ @$team_order->laporan_status }} / <small></small><br /><b>Catatan Teknisi</b> : <br />{{@$team_order->catatan }}<br /><a href='/{{ @$team_order->id_dt }}'>Detil</a><br />">
                  #{{ ++$num }} // {{ @$team_order->jenis_layanan }} // {{ @$team_order->Ndem }} // {{ @$team_order->laporan_status ? : 'ANTRIAN' }} // {{ $waktu }}</a>
                  
                  @if($team_order->jenis_layanan=="CABUT_NTE" && $team_order->jenis_dismantling <> "")
                  &nbsp;<span class="label label-danger">{{ $team_order->jenis_dismantling }}</span>
                  @endif

                  <br />
                  @endforeach
                </td>
                <?php
                  $total_NP += $NP;
                  $total_OGP += $OGP;
                  $total_KT += $KT;
                  $total_KP += $KP;
                  $total_HR += $HR;
                  $total_UP += $UP;
                ?>
              </tr>
              @endif
              @endforeach
              <tr>
                <th colspan="2">TOTAL</th>
                <th class="table_matrix">{{ $total_ORDER }}</th>
                <th class="table_matrix">{{ $total_NP }}</th>
                <th class="table_matrix">{{ $total_OGP }}</th>
                <th class="table_matrix">{{ $total_KT }}</th>
                <th class="table_matrix">{{ $total_KP }}</th>
                <th class="table_matrix">{{ $total_HR }}</th>
                <th class="table_matrix">{{ $total_UP }}</th>
                <th></th>
              </tr>
            </table>
          </div>
      </div>
      @endforeach
    </div>
  </div>
  <!-- <script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

        })


        var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script> -->
@endsection
