@extends('layout')

@section('content')
  @include('partial.alerts')
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Sebelum Dispatch Manual, Cek Dulu di Menu <i style="font-weight: bold;">Pencarian Order Assurance</i> pada halaman <i>&nbsp;<i class="ti-new-window" class="linea-icon linea-basic fa-fw"></i><a href="/assurance/search" style="color: white !important; font-weight: bold;">&nbsp;SEARCH</a></i>
  </div>

  <div class="panel panel-body">
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="col-md-12">
        <h3>
          <a href="../" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          Dispatch WO [Manual Assurance]
        </h3>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('no_tiket') ? 'has-error' : '' }}">
      <label for="no_tiket">No Tiket</label>
      <input type="text" class="form-control" name="no_tiket">
      {!! $errors->first('no_tiket','<p class=help-block>:message</p>') !!}
    </div>

    <div class="form-group col-md-6 {{ $errors->has('inet_no') ? 'has-error' : '' }}">
      <label for="inet_no">No Internet</label>
      <input type="number" class="form-control" name="inet_no">
      {!! $errors->first('inet_no','<p class=help-block>:message</p>') !!}
    </div>

    <div class="form-group col-md-12">
      <label for="headline">Headline</label>
      <textarea rows="3" class="form-control" name="headline"></textarea>
    </div>

    {{-- <div class="form-group col-md-6 {{ $errors->has('pelanggan_hvc') ? 'has-error' : '' }}">
      <label class="control-label" for="input-hvc" >Pelanggan HVC</label>
      <input name="pelanggan_hvc" type="hidden" id="input-hvc" class="form-control" />
      {!! $errors->first('pelanggan_hvc','<p class=help-block>:message</p>') !!}
    </div> --}}

    <div class="form-group col-md-6">
      <label>Cek Keterangan</label>
      <div class="input-group">
        <input type="checkbox" class="check" id="hvc" name="keterangan[hvc]" value="YES" data-checkbox="icheckbox_flat-red">
        <label for="hvc">&nbsp;HVC</label>&nbsp;&nbsp;
        <input type="checkbox" class="check" id="plasa" name="keterangan[plasa]" value="1" data-checkbox="icheckbox_flat-red">
        <label for="plasa">&nbsp;Plasa</label>&nbsp;&nbsp;
        <input type="checkbox" class="check" id="sqm" name="keterangan[sqm]" value="1" data-checkbox="icheckbox_flat-red">
        <label for="sqm">&nbsp;SQM</label>&nbsp;&nbsp;
        <input type="checkbox" class="check" id="sales" name="keterangan[sales]" value="1" data-checkbox="icheckbox_flat-red">
        <label for="sales">&nbsp;Sales</label>&nbsp;&nbsp;
        <input type="checkbox" class="check" id="helpdesk" name="keterangan[helpdesk]" value="1" data-checkbox="icheckbox_flat-red">
        <label for="helpdesk">&nbsp;Helpdesk</label>&nbsp;&nbsp;
        <input type="checkbox" class="check" id="sqm" name="keterangan[sqm]" value="1" data-checkbox="icheckbox_flat-red">
        <label for="sqm">&nbsp;SQM</label>&nbsp;&nbsp;
      </div>
    </div>

    <div class="form-group col-md-6 {{ $errors->has('customer_assign') ? 'has-error' : '' }}">
      <label class="control-label" for="input-ca" >Customer Assign</label>
      <input name="customer_assign" type="hidden" id="input-ca" class="form-control" />
      {!! $errors->first('customer_assign','<p class=help-block>:message</p>') !!}
    </div>

    <div class="form-group col-md-6" id="ca_manja_time">
      <label for="ca_manja_time">Waktu Manja</label>
      <input type="datetime-local" class="form-control" name="ca_manja_time">
    </div>

    <div class="form-group col-md-6 {{ $errors->has('refSc') ? 'has-error' : '' }}" id="hilang2">
      <label for="refSc">References SC</label>
      <input type="text" class="form-control" name="refSc">
      {!! $errors->first('refSc','<p class=help-block>:message</p>') !!}
    </div>

    <!-- <div class="form-group col-md-12">
      <label for="no_tiket">No WO</label>
      <input class="form-control" name="noSC" type="hidden" id="noSC" />
      <input class="form-control" name="noSC" type="text" />
    </div> -->

    <div class="form-group col-md-6">
      <label class="control-label" for="input-regu" >Loker</label>
      <input name="loker" type="hidden" id="input-loker" class="form-control" />
    </div>

    <div class="form-group col-md-6">
      <label class="control-label" for="input-regu">Alpro</label>
      <input name="alpro" type="hidden" id="input-alpro" class="form-control" />
    </div>

    <div class="form-group col-md-6">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control" />
    </div>

    <div class="form-group col-md-6">
      <label class="control-label" for="jenis_ont" >Jenis ONT</label>
      <input name="jenis_ont" type="hidden" id="jenis_ont" class="form-control" />
    </div>
    
    <div class="form-group col-md-6 {{ $errors->has('tgl') ? 'has-error' : '' }}">
      <label for="date">Tanggal</label>
      <div class="input-group">
        <input type="text" name="tgl" class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd" value="{{ date('Y-m-d') }}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
      </div>
      {!! $errors->first('tgl','<p class="label label-danger">:message</p>') !!}
    </div>

    {{-- <div class="form-group col-md-4 {{ $errors->has('jamOpen') ? 'has-error' : '' }}">
            <label class="label-control">Jam Open</label><br />
            <input id="jamOpen" name="jamOpen" type="time">
            {!! $errors->first('jamOpen','<p class="label label-danger">:message</p>') !!}
    </div> --}}

    {{-- <div class="form-group col-md-4 {{ $errors->has('tglOpen') ? 'has-error' : '' }}">
        <label class="control-label" for="tglOpen">Tanggal Open</label>
        <input name="tglOpen" type="date" id="tglOpen" class="form-control" value="{{ date('Y-m-d') }}" />  
        {!! $errors->first('tglOpen','<p class="label label-danger">:message</p>') !!}
    </div> --}}

     <div class="form-group col-md-12" id="hilang">
          <label class="control-label" for="ket"><b>Keterangan Fullfillment Guarante</b></label>
          <textarea name="ket" class="form-control" rows="5"></textarea>
    </div>

    <div class="form-group col-md-12">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <br/><br/>
  </form>
  </div>
@endsection
@section('plugins')
  {{-- <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script> --}}
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
    $(function() {

    $('#datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
    });

    var dataloker = [
      {"id":"1", "text":"CONSUMER"},
      {"id":"2", "text":"CORPORATE"},
      {"id":"3", "text":"Fulfillment Guarantee"},
      {"id":"4", "text":"REBOUNDARY"}
      ];

    $('#hilang').hide();
    $('#hilang2').hide();
    $('#input-loker').change(function(){
        console.log(this.value);
        if(this.value==4){
          $('#hilang2').show();
          $('#hilang').hide();
        }
        else if (this.value==3){
            $('#hilang').show();
            $('#hilang2').show();
        }
        else{
          $('#hilang').hide();
          $('#hilang2').hide();
        };

    });

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }

    $('#input-loker').select2(loker());

    var dataloker = [
      {"id":"1", "text":"CONSUMER"},
      {"id":"2", "text":"CORPORATE"},
      {"id":"3", "text":"FULFILLMENT GUARANTEE"},
      {"id":"4", "text":"REBOUNDARY"}
      ];

    var loker = function() {
      return {
        data: dataloker,
        placeholder: 'Input Loker',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-loker').select2(loker());

    var dataalpro = [
      {"id":"COPPER", "text":"COPPER"},
      {"id":"GPON", "text":"GPON"}
      ];

    var alpro = function() {
      return {
        data: dataalpro,
        placeholder: 'Input Alpro',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-alpro').select2(alpro());

    var dataHVC = [
      {"id":"YES", "text":"YES"},
      {"id":"NO", "text":"NO"}
      ];

    var pelangganHVC = function() {
      return {
        data: dataHVC,
        placeholder: 'Apakah Pelanggan HVC ?',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-hvc').select2(pelangganHVC());

    $('#ca_manja_time').hide();
    var dataCA = [
      {"id":"CA_YES", "text":"YES"},
      {"id":"CA_NO", "text":"NO"}
      ];

    var customerassign = function() {
      return {
        data: dataCA,
        placeholder: 'Pilih Customer Assign',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#input-ca').select2(customerassign());

    $('#input-ca').change(function(){
        console.log(this.value);
        if(this.value=='CA_YES'){
          $('#ca_manja_time').show();
        }
        else{
          $('#ca_manja_time').hide();
        };
    });

    var dataOnt = [
        {"id":"ZTE", "text":"ZTE"},
        {"id":"HUAWEI", "text":"HUAWEI"},
        {"id":"ALU", "text":"ALU"}
    ];

    var ont = function() {
      return {
        data: dataOnt,
        placeholder: 'Input Jenis ONT',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        }
      }
    }
    $('#jenis_ont').select2(ont());

      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.title+'</span> '+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());
      
      // var day = {
      //   format : 'yyyy-mm-dd',
      //   viewMode: 0,
      //   minViewMode: 0
      // };
 
      // $('#input-tgl').datepicker(day).on('changeDate', function(e){
      //   $(this).datepicker('hide');
      // });

      // $('#tglOpen').datepicker(day).on('changeDate', function(e){
      //   $(this).datepicker('hide');
      // });

      // $('#jamOpen').timepicker({
      //     showMeridian : false,
      //     disableMousewheel : false,
      //     minuteStep : 1
      // });

      $('#noSC').select2({
        placeholder: "Masukkan No. SC",
        minimumInputLength: 2,
        ajax: { 
          url: "{{route('live_SearchSC')}}",
          dataType: 'json',
          data: function (params) {
            return {
              q: $.trim(params.term)
              // q:'11314277'
            };
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

    });

  </script>
@endsection