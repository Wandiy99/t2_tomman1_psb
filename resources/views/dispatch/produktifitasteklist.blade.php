@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">{{ $title }}</h3>
        <div class="table-responsive">
			<table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
					<tr>
	        			<th>NO</th>
	        			<th>TIM</th>
	        			<th>SEKTOR</th>
						<th>DATEL</th>
	        			<th>ORDER ID</th>
						<th>ORDER STATUS</th>
						<th>ORDER DATE</th>
						<th>ORDER DATE PS</th>
	        			<th>TGL DISPATCH</th>
	        			<th>STATUS</th>
	        			<th>CATATAN</th>
	        		</tr>
				</thead>
				<tbody>
				@foreach ($get_produktifitasteklist as $num => $produktifitasteklist)
	        		<tr>
	        			<td>{{ ++$num }}</td>
	        			<td>{{ $produktifitasteklist->uraian }}</td>
	        			<td>{{ $produktifitasteklist->sektorx }}</td>
						<td>{{ $produktifitasteklist->sektor }}</td>
	        			<td>{{ $produktifitasteklist->Ndem }}</td>
						<td>{{ $produktifitasteklist->orderStatus }}</td>
						<td>{{ $produktifitasteklist->dps_orderDate ? : $produktifitasteklist->pmw_orderDate }}</td>
						<td>{{ $produktifitasteklist->dps_orderDatePs }}</td>
	        			<td>{{ $produktifitasteklist->tgl }}</td>
	        			<td>{{ $produktifitasteklist->laporan_status ? : 'NEED PROGRESS' }}</td>
	        			<td>{{ $produktifitasteklist->catatan }}</td>
	        		</tr>
				@endforeach
				</tbody>
				<tfoot>
					<tr>
	        			<th>NO</th>
	        			<th>TIM</th>
	        			<th>SEKTOR</th>
						<th>DATEL</th>
	        			<th>ORDER ID</th>
						<th>ORDER STATUS</th>
						<th>ORDER DATE</th>
						<th>ORDER DATE PS</th>
	        			<th>TGL DISPATCH</th>
	        			<th>STATUS</th>
	        			<th>CATATAN</th>
	        		</tr>
				</tfoot>
			</table>
		</div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL PRODUKTIFITASTEK LIST TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL PRODUKTIFITASTEK LIST TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL PRODUKTIFITASTEK LIST TOMMAN'
                }
            ]
        });
    });
</script>
@endsection