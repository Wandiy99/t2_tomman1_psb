@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    .red {
      background-color: #FF0000;
      color : #FFF;
    }
    .label-greenx{
      background-color: #1abc9c;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <h3>Matrix Workorder </h3>
    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>
  
  <div class="panel panel-primary" id="matrix">
     <div class="panel-heading">MATRIX HD Fallout</div>
      <div class="panel-body">
        <div class="list-group">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th width="10" rowspan="2">No</th>
                <th width="300" rowspan="2">Nama</th>
                <th width="60" rowspan="2">Target PBS</th>
                <th width="60" rowspan="2">Target Perbaikan</th>
                <th width="60" rowspan="2">Take Order</th>
                
                <th width="60" colspan="2" align="center"><center>UP</center></th>
                <th width="60" rowspan="2" align="center"><center>Resolves</center></th>
                <th width="60" rowspan="2" align="center"><center>PBS</center></th>
                <th rowspan="2">LIST</th>
              </tr>
              <tr>
                <th width=30>FO</th>
                <th width=30>PDA</th>
              
              </tr>

              <?php
                $targetMH     = 8;
                $jmlWo        = 0;
                $jmlProgress  = 0;
                $jmlUpFo      = 0;
                $jmlUpPda     = 0;

                $targetPsb = 100;
                $targetPerbaikan = 12;
              ?>

              @foreach($fout as $no=>$list)
                <tr>
                    <?php
                        $jmlWo += $list['jumlahWO'];
                        $jmlUpFo += $list['UP'];
                        $jmlProgress += $list['PROGRESS']; 
                        $jmlUpPda += $list['UP_PDA'];  
                     ?>

                    <td>{{ ++$no }}</td>
                    <td>{{ $list['nama'] }}</td>
                    <td><center>{{ $targetPsb }} %</center></td>
                    <td><center>{{ $targetPerbaikan }}</center></td>
                    <td><center>{{ $list['jumlahWO'] }}</center></td>                  
                    <td><center>{{ $list['UP'] }}</center></td>
                    <td><center>{{ $list['UP_PDA'] }}</center></td>
                    <td><center>{{ $list['UP'] + $list['UP_PDA'] }}</center></td>
                    <td><center>{{ round((($list['UP'] + $list['UP_PDA']) / $targetPerbaikan) * (100)) }} % </center></td>
                    <td>
                      @foreach ($list['WO'] as $wo)
                        <?php
                          if ($wo['status_laporan']=="Process OSS (Activation Completed)" || $wo['status_laporan']=="Completed (PS)" || $wo['status_laporan']=="Process ISISKA (RWOS)"){
                            $label = 'success';
                          } else{
                            $label = 'default';
                          }
                        ?>
                        @if($wo['keteranganWo']==0)
                          <span class="label label-{{ $label }}">{{ $wo['sc'] }}</span>
                        @else
                          <span class="label label-{{ $label }}">{{ $wo['sc'] }} [PDA]</span>
                        @endif 
                      @endforeach
                    </td>
                  </tr>
                @endforeach
              <tr>
                <td colspan="2"><center>Total</center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center>{{ $jmlWo }}</center></td>
                <td><center>{{ $jmlUpFo }}</center></td>
                <td><center>{{ $jmlUpPda }}</center></td>
                <td><center>{{ $jmlUpFo + $jmlUpPda  }}</center></td>
                <td><center></center></td>
                <td><center></center></td>
              </tr>
            </table>
          </div>
      </div>
    </div>
  </div>

@endsection