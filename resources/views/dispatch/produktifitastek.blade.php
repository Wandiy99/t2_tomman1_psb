@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    td {
      padding : 0px;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <h3>Produktifitas </h3>
    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>

  <div class="panel panel-primary" id="produktifitas">
    <div class="panel-heading">PRODUKTIFITAS TIM</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tr>
            <th width="10" rowspan="2">No</th>
            <th width="200" rowspan="2">Nama Tim</th>
            <th width="60" rowspan="2">Target MH</th>
            <th width="60" colspan=4 align="center"><center>WO</center></th>
            <th width="60" rowspan="2" align="center"><center>ANTRIAN</center></th>
            <th width="60" rowspan="2" align="center"><center>PROGRESS</center></th>
            <th width="60" rowspan="2" align="center"><center>KENDALA</center></th>
            <th width="60" colspan="4" align="center"><center>UP</center></th>
            <th width="60" rowspan="2" align="center"><center>MH ACH</center></th>
            <th width="60" rowspan="2" align="center"><center>PROD<br />MH</center></th>
            <th width="60" rowspan="2" align="center"><center>PROD<br />WO</center></th>
          </tr>
          <tr>
            <th width=15>Jumlah</th>
            <th width=15>PSB</th>
            <th width=15>ASR</th>
            <th width=30>MH</th>
            <th width=30>Asr (C)</th>
            <th width=30>Asr (F)</th>
            <th width=30>Prov</th>
            <th width=30>Jumlah</th>
          </tr>
          <?php
            $number = 1;
            $total_target_MH = 0;
            $total_WO = 0;
            $total_MH = 0;
            $total_Progress = 0;
            $total_NO_UPDATE = 0;
            $total_kendala = 0;
            $total_UP_A = 0;
            $total_UP_C = 0;
            $total_UP_Prov = 0;
            $total_MH_ACH = 0;
            $total_prod = 0;
            $totalpsb = 0;
            $totalggn = 0;
            $total_WOnya = $total_WO - $total_NO_UPDATE;
            $total_prod = ($total_WOnya / $total_WO) * 100;

          ?>
          @foreach ($getProduktifitasTek as $num => $ProduktifitasTek)
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $ProduktifitasTek->uraian }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>

<div id="timexcount">0 ms</div>
		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
        })
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
