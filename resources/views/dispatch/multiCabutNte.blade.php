@extends('layout')
@section('content')
@include('partial.alerts')

{{-- <div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Harap Perhatikan Format yang Digunakan !!! Jangan Ada Field yang Kosong atau Memakai Garis Baru (Enter)</strong>
</div> --}}

<div class="row">

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">Multi Dispatch Order Cabut NTE</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/multi/cabutNteSave" enctype="multipart/form-data" autocomplete="off">

        <div class="form-group">
            <label for="input-regu1" class="col-form-label">REGU</label>
            <input type="text" name="id_regu" class="form-control" id="input-regu1"/>
            {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="tgl-dispatch" class="col-form-label">TANGGAL DISPATCH</label>
            <input type="text" class="form-control" id="cabutNteSave" name="tgl_dispatch" placeholder="yyyy-mm-dd">
            {{-- <input type="date" name="tgl_dispatch" class="form-control" id="tgl-dispatch"> --}}
            {!! $errors->first('tgl_dispatch','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
          <label class="control-label" for="multi">Import File (.xlsx) *Format Disesuaikan*</label>
          <input type="file" name="multi" id="multi" class="dropify" />
          {!! $errors->first('multi','<p class="label label-danger">:message</p>') !!}
        </div>
        
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-primary">Dispatch</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">Pindah Order dari Regu ke Regu</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/multi/cabutNtePindah" enctype="multipart/form-data" autocomplete="off">

        <div class="form-group">
            <label for="input-regu-terdispatch" class="col-form-label">REGU A</label>
            <input type="hidden" name="id_regu_a" class="form-control" id="input-regu-terdispatch"/>
            {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="tgl-dispatch" class="col-form-label">TANGGAL DISPATCH</label>
            <input type="text" class="form-control" id="cabutNtePindah" name="tgl_dispatch" placeholder="yyyy-mm-dd">
            {{-- <input type="date" name="tgl_dispatch" class="form-control" id="tgl-dispatch"> --}}
            {!! $errors->first('tgl_dispatch','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-regu2" class="col-form-label">REGU B</label>
            <input type="hidden" name="id_regu_b" class="form-control" id="input-regu2"/>
            {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
        </div>
        
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-primary">Dispatch</button>
        </div>
      </form>
    </div>
  </div>
</div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {

      $('.dropify').dropify();

      $('#cabutNteSave').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });

      $('#cabutNtePindah').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });

      var state= <?= json_encode($regu1) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Pilih Regu',
          formatResult: function(data) {
          return '<p>'+data.text+'</p>';
          }
        }
      }
      $('#input-regu1').select2(regu());

      var state= <?= json_encode($regu2) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Pilih Regu',
          formatResult: function(data) {
          return '<p>'+data.text+'</p>';
          }
        }
      }
      $('#input-regu2').select2(regu());

      var state= <?= json_encode($regu_terdispatch) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Pilih Regu',
          formatResult: function(data) {
          return '<span>'+data.text+'</span>'+'&nbsp<span class="label label-success">'+data.jumlah+'</span>';
          }
        }
      }
      $('#input-regu-terdispatch').select2(regu());
    });
</script>
@endsection