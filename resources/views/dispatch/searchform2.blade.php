@extends('layout')

@section('content')
@include('partial.alerts')
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 10px 6px;
    }
    th{
        text-align: center;
        color: black;
        width: 1px;
        white-space: nowrap;
    }
    td{
        color: black;
    }
</style>


<div class="col-sm-12">
    <form class="form-group" method="post" >
        <div class="row">
            <div class="col-md-10">
                <div class="form-group {{ $errors->has('Search') ? 'has-error' : '' }}">
                    <label for="Search">Search</label>
                    <input type="number" name="Search" class="form-control" id="Search" value="">
                    {!! $errors->first('Search', '<p class=help-block>:message</p>') !!}
                </div>
            </div>
            <div class="col-md-2">
                <label for="btn">&nbsp;</label>
                <button class="form-control btn btn-primary btn-rounded">Search</button>
            </div>
        </div>
    </form>
</div>


@if ($data <> NULL)
<br />
<div class="panel-body" style="background-color: white !important">
<div class="table-responsive">
    {{ $data }}. <br />
    Result ({{ $jumlah }}) Found.
    <br />
    <br />
    @if ($jumlah > 0)
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>#</th>
            <th style="width: 40%">Starclick NCX & K-PRO</th>
            <th style="width: 40%">Informasi Order</th>
            <th>Modals</th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $no => $data)
        @if($data->orderId <> NULL)
        <tr>
            <td style="text-align: center; vertical-align: middle;">{{ ++$no }}</td>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <td colspan="3" class="text-center">
                                @if ($data->id_dt<>'')
                                <a href="/{{ $data->id_dt }}" target="_blank"><b>SC {{ $data->orderId ? : 'Tidak Ada SC' }}</b></a>
                                @else
                                <b>SC {{ $data->orderId ? : 'Tidak Ada SC' }}</b>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>NCLI / WFM ID / MYIR</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->orderNcli ? : '-' }} / {{ $data->wfm_id ? : '-' }} / {{ $data->myir ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Nama Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->orderName ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>ND Inet / ND Voice</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->internet ? : $data->ndemSpeedy ? : '-' }} / {{ $data->noTelp ? : $data->ndemPots ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Order / Tanggal Order PS</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->a_orderDate ? : $data->kprot_orderDate ? : '-' }} / {{ $data->a_orderDatePs ? : $data->kprot_orderDatePs ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Status Order / Status Order ID</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;"><label class="label label-success">{{ $data->orderStatus ? : $data->kprot_orderStatus ? : '-' }} / {{ $data->orderStatusId ? : $data->kprot_orderStatusId ? : '-' }}</label></td>
                        </tr>
                        <tr>
                            <td>Jenis Layanan / Provider / Tipe Transaksi / Package Name</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->jenisPsb ? : '-' }} / {{ $data->provider ? : $data->kprot_provider ? : '-' }} / <label class="label label-info">{{ $data->kprot_jenis_psb ? : '-' }} | {{ $data->kprot_type_transaksi ? : '-' }}</label> <label class="label label-danger">{{ $data->kprot_type_layanan ? : '-' }}</label> / {{ $data->orderPackageName ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>WITEL / STO / ODP</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->witel ? : '-' }} / {{ $data->sto ? : '-' }} / {{ $data->alproname ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>K-Contact</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kcontact ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Appointment Desc</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->appointment_desc ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Alamat Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->orderAddr }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat Pelanggan ( Lat / Lon )</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->lat ? : '-' }},{{ $data->lon ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Alamat dari Sales</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kordinatPel ? : ' - ' }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat dari Sales</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->alamatSales ? : ' - ' }}</td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Status / Tanggal Laporan / Umur</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">
                                @if ($data->laporan_status != '')
                                <label class="label label-success">{{ $data->laporan_status }}</label>
                                @elseif ($data->id_dt != null && $data->laporan_status == null)
                                <label class="label label-default">ANTRIAN</label>
                                @else
                                -
                                @endif
                                 / {{ $data->pl_modif ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Umur Order</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">
                                @php
                                    if ($data->id_dt == null)
                                    {
                                        $tglOrder = $data->a_orderDate;
                                    } else {
                                        $tglOrder = $data->tgl_awal_dispatch;
                                    }
                                    $awal  = date_create($tglOrder);
                                    $akhir = date_create();
                                    $diff  = date_diff( $awal, $akhir );

                                    if ($diff->d > 0)
                                    {
                                        $waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
                                    } else {
                                        $waktu = $diff->h.' Jam '.$diff->i.' Menit';
                                    }
                                @endphp

                                {{ $waktu ? : '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Redaman / ONU Rx</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">
                                @if($data->d_redaman_iboster == "-" || $data->d_redaman_iboster < -23)
                                    <label class="label label-danger">UNSPEC</label>
                                @else
                                    <label class="label label-success">SPEC</label>
                                @endif
                                / {{ $data->d_redaman_iboster ? : '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Tim</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->uraian ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Mitra</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->mitra_amija_pt ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Sektor</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->sektor_title ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Catatan Teknisi</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->catatan ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Kontak Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->noPelangganAktif ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kordinat_pelanggan ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>ODP</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->odp_nama ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat ODP</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kordinat_odp ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Valins ID</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->valins_id ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Datek</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->detek ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Dropcore Labelcode</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->dropcore_label_code ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>ODP Labelcode</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->odp_label_code ? : '-' }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <!-- DISPATCH ORDER -->
                <div id="responsive-modal-dispatch-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-dispatch-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>DISPATCH SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                @if(!empty($data->id_r))
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Tim</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->uraian ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Dispatch</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->c_tgl ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Order</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->hd_date ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Dispatch Oleh</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->hd ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Jenis Layanan</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->dtjl_jenis_transaksi ? : '-' }} | {{ $data->c_jenis_layanan ? : '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br /><br />
                                <a href="/dispatch/{{ $data->orderId }}" class="btn btn-rounded btn-block btn-info">Re-Dispatch</a>
                                @else
                                    @if($data->orderId <> NULL)
                                        {{-- @if (str_contains($data->kcontact, 'PTGSTDKDTG') == false) --}}
                                            <a href="/dispatch/{{ $data->orderId }}" class="btn btn-rounded btn-block btn-info">Dispatch</a>
                                        {{-- @endif --}}
                                    @endif
                                @endif
                                
                                <br/><br/>
                                @if($data->orderId <> NULL && in_array(session('auth')->level, [2]))
                                <div class="col-md-6">
                                    <a href="/ibooster/delete/{{ $data->orderId }}" class="btn btn-sm btn-rounded btn-block btn-danger">Delete Ibooster</a>
                                </div>
                                @endif

                                @if((session('psb_reg') <> 1) && in_array(session('auth')->level, [2]))
                                <div class="col-md-6">
                                    <a href="/dispatch/delete/{{ $data->orderId }}/{{ $data->id_dt }}/{{ $data->id_pl ? : '0' }}" class="btn btn-sm btn-rounded btn-block btn-danger">Delete Dispatch</a>
                                </div>
                                @endif
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-info btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-dispatch-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Dispatch Order</b>
                </button>

                &nbsp;

                <!-- LOG DISPATCH ORDER -->
                <div id="responsive-modal-logdispatch-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-logdispatch-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>LOG DISPATCH<br />SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body" style="overflow-y:auto">
                                <ul class="list-group">
                                    @foreach ($log_dispatch[$data->orderId] as $log_disp)
                                    <li class="list-group-item">
                                        {{ @$log_disp->created_by }} - {{ @$log_disp->created_name }}<br />
                                        {{ @$log_disp->laporan_status ? : 'ANTRIAN' }} - {{ @$log_disp->created_at }}<br />
                                        {{ @$log_disp->catatan }}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-info btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-logdispatch-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log Dispatch Order</b>
                </button>

                &nbsp;

                <!-- MATERIAL ORDER -->
                <div id="responsive-modal-material-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-material-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>MATERIAL SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Jenis ONT</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->typeont ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">SN ONT</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->snont ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Jenis STB</td>
                                                <td>:</td>
                                                <td style="text-align: right;">{{ $data->typestb ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">SN STB</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->snstb ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Number RFC</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->rfc_number ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Jenis Material</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->material_psb ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">AC-OF-SM-1B / DC Roll</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->dc_roll ? : '0' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Preconnectorized-1C-50-NonAcc</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->precon50 ? : '0' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Preconnectorized-1C-75-NonAcc</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->precon75 ? : '0' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Preconnectorized-1C-80</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->precon80 ? : '0' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Preconnectorized-1C-100-NonAcc</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->precon100 ? : '0' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Preconnectorized-1C-150-NonAcc</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->preconnonacc ? : '0' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-default btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-material-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Material Order</b>
                </button>

                &nbsp;

                <!-- DEVIASI ORDER -->
                <div id="responsive-modal-deviasi-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-deviasi-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>DEVIASI SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body row">
                                <div class="col-md-12 text-center">
                                    <h4><b>KOORDINAT PELANGGAN
                                        @if($data->jarak_deviasi > '0.250')
                                        <h3 style="color: red !important"><b>{{ $data->jarak_deviasi ? : '-' }} Kilometer [NOK]</b></h3>
                                        @else
                                        <h3 style="color: green !important"><b>{{ $data->jarak_deviasi ? : '-' }} Kilometer [OK]</b></h3>
                                        @endif
                                        Perbedaan Jarak
                                    </b></h4>
                                    <p class="text-muted">1 Kilometer (KM) = 1000 Meter (M)</p>
                                    <div class="col-md-6">
                                        <h5><b>Starclick NCX</b></h5>
                                        <br />
                                        <div class="mapouter">
                                            <div class="gmap_canvas"><iframe width="384" height="335" id="gmap_canvas_starc"
                                                    src="https://maps.google.com/maps?q={{ $data->lat ? : '-' }},{{ $data->lon ? : '-' }}&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        text-align: right;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        background: none !important;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                        <b>{{ $data->lat ? : '-' }},{{ $data->lon ? : '-' }}</b>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Teknisi</b></h5>
                                        <br />
                                        <div class="mapouter">
                                            <div class="gmap_canvas"><iframe width="384" height="335" id="gmap_canvas_tek"
                                                    src="https://maps.google.com/maps?q={{ $data->kordinat_pelanggan ? : '-' }}&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        text-align: right;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        background: none !important;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                        <b>{{ $data->kordinat_pelanggan ? : '-' }}</b>
                                    </div>
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4><b>KOORDINAT PELANGGAN KE ODP (TEKNISI)
                                        @if($data->deviasi_pelanggan_odp > '0.250')
                                        <h3 style="color: red !important"><b>{{ $data->deviasi_pelanggan_odp ? : '-' }} Kilometer [NOK]</b></h3>
                                        @else
                                        <h3 style="color: green !important"><b>{{ $data->deviasi_pelanggan_odp ? : '-' }} Kilometer [OK]</b></h3>
                                        @endif
                                        Perbedaan Jarak
                                    </b></h4>
                                    <p class="text-muted">1 Kilometer (KM) = 1000 Meter (M)</p>
                                    <div class="col-md-6">
                                        <h5><b>ODP</b></h5>
                                        <br />
                                        <div class="mapouter">
                                            <div class="gmap_canvas"><iframe width="384" height="335" id="gmap_canvas_starc"
                                                    src="https://maps.google.com/maps?q={{ $data->lat_odp ? : '-' }},{{ $data->lon_odp ? : '-' }}&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        text-align: right;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        background: none !important;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                        <b>{{ $data->lat_odp ? : '-' }},{{ $data->lon_odp ? : '-' }}</b>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Teknisi</b></h5>
                                        <br />
                                        <div class="mapouter">
                                            <div class="gmap_canvas"><iframe width="384" height="335" id="gmap_canvas_tek"
                                                    src="https://maps.google.com/maps?q={{ $data->kordinat_pelanggan ? : '-' }}&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        text-align: right;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        background: none !important;
                                                        height: 384px;
                                                        width: 335px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                        <b>{{ $data->kordinat_pelanggan ? : '-' }}</b>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-warning btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-deviasi-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Deviasi Order</b>
                </button>

                &nbsp;

                <!-- UTONLINE -->
                <div id="responsive-modal-utonline-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-utonline-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>BA UT ONLINE SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Order Code</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->utt_order_code }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Status (WITEL)</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->utt_statusName ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Status (QC2)</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->utt_qcStatusName ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal WO / Tanggal Transaksi</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->utt_tglWo }} / {{ $data->utt_tglTrx }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Log Activity </td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->utt_last_updated_at }} WITA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @if($data->utt_statusName <> null)
                                        @if($data->utt_statusName != 'Approved')
                                        <span class="label label-warning pull-right">Bisa Return Mytech</span>
                                        @else
                                        <a class="label label-success pull-right" href="/utonline/request-reset-ba/{{ $data->orderId }}" target="_blank">Request Reset BA</a>
                                        @endif
                                        <br />
                                    @endif
                                    {{-- <p class="text-muted pull-left">Materials Alista:</p>
                                    <table class="table table-sm table-hover table-bordered table-striped text-center">
                                        <thead style="background-color: yellow;">
                                            <tr>
                                                <th class="align-middle" style="color: black;">ID Order</th>
                                                <th class="align-middle" style="color: black;">ID Barang</th>
                                                <th class="align-middle" style="color: black;">Stok</th>
                                                <th class="align-middle" style="color: black;">Satuan</th>
                                                <th class="align-middle" style="color: black;">Volume</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($material_alista[$id])
                                                @foreach($material_alista[$id] as $value)
                                                <tr>
                                                    <td class="align-middle">{{ $value['id_order'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['id_barang'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['stok'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['satuan'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['volume'] ? : '0' }}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="align-middle" colspan="8">Data is not available rightnow :(</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    <p class="text-muted pull-left">Materials Tambahan:</p>
                                    <table class="table table-sm table-hover table-bordered table-striped text-center">
                                        <thead style="background-color: yellow;">
                                            <tr>
                                                <th class="align-middle" style="color: black;">ID Order</th>
                                                <th class="align-middle" style="color: black;">Nama</th>
                                                <th class="align-middle" style="color: black;">Volume</th>
                                                <th class="align-middle" style="color: black;">Latitude</th>
                                                <th class="align-middle" style="color: black;">Longitude</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($material_tambahan[$id])
                                                @foreach($material_tambahan[$id] as $value)
                                                <tr>
                                                    <td class="align-middle">{{ $value['id_order'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['id_barang'] ? : '0' }}</td>
                                                    <td class="align-middle">{{ $value['stok'] ? : '0' }}</td>
                                                    <td class="align-middle">{{ $value['satuan'] ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value['volume'] ? : '-' }}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="align-middle" colspan="8">Data is not available rightnow :(</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table> --}}
                                    @if($data->utt_details <> null)
                                    <p class="text-muted pull-left">Log Details :</p>
                                    <table class="table table-sm table-hover table-bordered table-striped text-center">
                                        <thead style="background-color: yellow;">
                                            <tr>
                                                <th style="color: black;">Log</th>
                                                <th style="color: black;">Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $utt_details = json_decode($data->utt_details);
                                            @endphp
                                                {{-- datek --}}
                                                <tr>
                                                    <td class="align-middle">Info<br />Return</td>
                                                    <td class="align-middle">
                                                        <table class="table table-sm table-hover table-bordered table-striped text-center">
                                                            <tr>
                                                                <th class="align-middle">Foto<br />Kurang<br />Lengkap</th>
                                                                <th class="align-middle">BA<br />Tidak Sesuai</th>
                                                                <th class="align-middle">Hasil Ukur<br />Under Spec<br />/<br />Kosong</th>
                                                                <th class="align-middle">Data Material<br />Tidak Sesuai</th>
                                                                <th class="align-middle">Tanggal<br />Tidak Sesuai</th>
                                                                <th class="align-middle">Alamat<br />/<br />Koordinat<br />Tidak Sesuai</th>
                                                                <th class="align-middle">Catatan</th>
                                                            </tr>
                                                            <tr>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifPhoto == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifPhoto ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifBA == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifBA ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifUkur == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifUkur ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifMaterial == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifMaterial ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifTanggal == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifTanggal ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">
                                                                    @if(@$utt_details->datek->verifKoordinat == '1')
                                                                    <p style="color: red; font-weight: bold;">X</p>
                                                                    @else
                                                                    {{ @$utt_details->datek->verifKoordinat ? : '-' }}
                                                                    @endif
                                                                </td>
                                                                <td class="align-middle">{{ @$utt_details->datek->verifNote ? : '-' }}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                {{-- qcValidate --}}
                                                <tr>
                                                    <td class="align-middle">QC Validate</td>
                                                    <td class="align-middle">
                                                        <table class="table table-sm table-hover table-bordered table-striped text-center">
                                                            <tr>
                                                                <th class="align-middle">Status<br />Valid</th>
                                                                <th class="align-middle">Keterangan</th>
                                                                <th class="align-middle">Reject by<br />Role</th>
                                                                <th class="align-middle">Reject by<br />Username</th>
                                                                <th class="align-middle">Reject by<br />User Code</th>
                                                                <th class="align-middle">Reject by<br />Date</th>
                                                            </tr>
                                                            <tr>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->statusValid ? : '-' }}</td>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->keterangan ? : '-' }}</td>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->rejectByRole ? : '-' }}</td>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->rejectByUserName ? : '-' }}</td>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->rejectByUserCode ? : '-' }}</td>
                                                                <td class="align-middle">{{ @$utt_details->qcValidate->rejectDate ? : '-' }}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                    @if($data->utt_getFlowLatest <> null)
                                    <p class="text-muted pull-left">Log Get Flow Latest :</p>
                                    <table class="table table-sm table-hover table-bordered table-striped text-center">
                                        <thead style="background-color: yellow;">
                                            <tr>
                                                <th class="align-middle" style="color: black;">Order<br />Remark IN</th>
                                                <th class="align-middle" style="color: black;">Order<br />Remark OUT</th>
                                                <th class="align-middle" style="color: black;">Update<br />Datetime</th>
                                                <th class="align-middle" style="color: black;">Assigner Name</th>
                                                <th class="align-middle" style="color: black;">Assignee Name</th>
                                                <th class="align-middle" style="color: black;">Status Name</th>
                                                <th class="align-middle" style="color: black;">Processed<br />Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $utt_getFlowLatest = json_decode($data->utt_getFlowLatest);
                                            @endphp
                                            <tr>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->remark_in ? : '-' }}</td>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->remark_out ? : '-' }}</td>
                                                <td class="align-middle">{{ substr(@$utt_getFlowLatest->update_dtm, 0, 19) ? : '-' }}</td>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->assigner_name ? : '-' }}</td>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->assignee_name ? : '-' }}</td>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->status_name ? : '-' }}</td>
                                                <td class="align-middle">{{ @$utt_getFlowLatest->processed_name ? : '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                    <p class="text-muted pull-left">Log Approver :</p>
                                    <table class="table table-sm table-hover table-bordered table-striped text-center">
                                        <thead style="background-color: yellow;">
                                            <tr>
                                                <th class="align-middle" style="color: black;">Order<br />Flow ID</th>
                                                <th class="align-middle" style="color: black;">Order Flow<br />Task Code</th>
                                                <th class="align-middle" style="color: black;">Order<br />Remark IN</th>
                                                <th class="align-middle" style="color: black;">Order<br />Remark OUT</th>
                                                <th class="align-middle" style="color: black;">Status Name</th>
                                                <th class="align-middle" style="color: black;">Processed<br />Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($data->utt_approver <> null)
                                                @foreach(json_decode($data->utt_approver) as $value)
                                                <tr>
                                                    <td class="align-middle">{{ $value->order_flow_id ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value->order_flow_task_code ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value->remark_in ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value->remark_out ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value->status_name ? : '-' }}</td>
                                                    <td class="align-middle">{{ $value->processed_name ? : '-' }}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="align-middle" colspan="8">Data is not available rightnow :(</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-danger btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-utonline-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log BA UT Online</b>
                </button>
                
                &nbsp;

                <!-- TACTICALPRO PICKUP -->
                <div id="responsive-modal-pickuponline-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-pickuponline-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>PICKUP ONLINE SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td class="text-center align-middle">Completed Date</td>
                                                <td class="text-center align-middle">:</td>
                                                <td style="text-align: right;" class="align-middle">{{ $data->ttp_ps_date }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center align-middle">STO</td>
                                                <td class="text-center align-middle">:</td>
                                                <td style="text-align: right;" class="align-middle">{{ $data->ttp_id_sto }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center align-middle">Status WO</td>
                                                <td class="text-center align-middle">:</td>
                                                <td style="text-align: right;" class="align-middle">{{ $data->ttp_layanan }} / {{ $data->ttp_status_wo }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center align-middle">Status KPRO</td>
                                                <td class="text-center align-middle">:</td>
                                                <td style="text-align: right;" class="align-middle">{{ $data->kprot_jenis_psb }}<br />{{ $data->kprot_provider }}<br />{{ $data->kprot_orderStatus }}<br />{{ $data->kprot_orderDatePs }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center align-middle">Status Starclick NCX</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;" class="align-middle">{{ $data->jenisPsb }}<br />{{ $data->a_provider }}<br />{{ $data->orderStatus }}<br />{{ $data->orderDatePs }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-success btn-rounded waves-effect" href="/api/dorongPickupOnline?id={{ $data->orderId }}">Dorong Order</a>
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-danger btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-pickuponline-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log Pickup Online</b>
                </button>
                
                &nbsp;
                
                <!-- COMPARIN -->
                <div id="responsive-modal-comparin-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-comparin-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>LOG DALAPA x COMPARIN SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Tanggal</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->pl_modif ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Status</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">@if(in_array($data->laporan_status_id, [4, 6, 5, 28, 29, 1, 37, 38])) OK @else NOK @endif</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Log Cat</td>
                                                <td>:</td>
                                                <td style="text-align: right;">{{ $data->status_comparin ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Log Action</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->log_action_comparin ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Log UIC</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->log_uic_comparin ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Sub Kategori</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->laporan_status ? : 'ANTRIAN' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Log Detail</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->catatan ? : '-' }} [ VALINS: {{ $data->valins_id ? : '-' }} ] [{{ $data->laporan_status ? : 'ANTRIAN' }}]</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">API Status</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">@if ($data->dalapa_checklist == null) UNORDERED @elseif ($data->dalapa_checklist == 1) CREATED @elseif ($data->dalapa_checklist == 2) UPDATED @endif</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-success btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-comparin-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log Comparin</b>
                </button>

                &nbsp;

                <!-- MARINA -->
                <div id="responsive-modal-marina-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-marina-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>LOG MARINA SC-{{ $data->orderId }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Jenis Order</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_nama_order ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Status</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->status ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Headline</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_headline ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Action</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_action ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Port Used</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_port_used ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Port Idle</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_port_idle ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Selesai</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->tgl_selesai ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Nama Regu</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->dispatch_regu_name ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">User Update</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_modif_by ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Terakhir Update</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->mc_modif_at ? : '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-primary btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-marina-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log Marina</b>
                </button>
            </td>
        </tr>
        @endif
        @endforeach
      </tbody>
    </table>
    </div>
    @endif
</div>
</div>
@endif
</div>
@endsection