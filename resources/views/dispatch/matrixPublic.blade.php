@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <h3>MATRIX ASSURANCE KALSEL</h3>
  <br />
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .label-KP {
    background-color: #ff6b6b;
  }
  .label-KT {
    background-color: #0abde3;
  }
  .label-UP {
    background-color: #1dd1a1;
  }
  .label-OGP {
    background-color: #feca57;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-NP {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }
  .underline {
    text-decoration: underline;
  }
  .gaul {
    border: 2px solid #DC143C;
  }
  .label-bluepas {
      background-color : #fd79a8;
  }
  .label-purple {
      background-color : #800080;
  }
  </style>
  <div class="row">
    <div class="col-sm-12">
      @foreach ($get_sektor as $sektor)
      <div class="panel panel-primary">
          <div class="panel-heading">{{ $sektor->title }}</div>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th width="300">TIM</th>
                <th>LIST ORDER</th>
                <th width="50">NP</th>
                <th width="50">OGP</th>
                <th width="50">KT</th>
                <th width="50">KP</th>
                <th width="50">UP</th>
              </tr>
              <?php
                $total_UP = 0;
                $total_KT = 0;
                $total_KP = 0;
                $total_OGP = 0;
                $total_NP = 0;
              ?>
              @foreach ($matrix[$sektor->chat_id] as $team)
              @if (@$team->uraian<>"")
              <tr>
                <td>{{ $team->uraian }}</td>
                <td>
                  <?php 
                    $UP = 0;
                    $KT = 0;
                    $KP = 0;
                    $OGP = 0;
                    $NP = 0;
                  ?>
                  @foreach (@$matrix[@$sektor->chat_id][@$team->id_regu] as $num => $team_order)
                  <?php 
                    if (@$team_order->laporan_status=="UP") $UP += 1;
                    if (@$team_order->grup=="KT") $KT += 1;
                    if (@$team_order->grup=="KP") $KP += 1;
                    if (@$team_order->grup=="OGP") $OGP += 1;
                    if (@$team_order->grup=="SISA") $NP += 1;
                    if (@$team_order->grup=="NP" || @$team_order->grup=="") $NP += 1;
                  ?>

                  <?php
                  if (@$team_order->GAUL>0) {
                    $gaul = 'gaul';
                  } else {
                    $gaul = '';
                  }


                  $labelWarna = $team_order->grup;
                  if ($team_order->laporan_status=="KIRIM TEKNISI"){
                      $labelWarna = 'bluepas';
                  }elseif ($team_order->laporan_status=="RESCHEDULE"){
                      $labelWarna = 'KP';
                  }elseif ($team_order->laporan_status=="RNA"){
                      $labelWarna = 'KP';
                  }elseif ($team_order->laporan_status=="KENDALA HUJAN"){
                      $labelWarna = 'KT';
                  }elseif ($team_order->laporan_status=="FOLLOW UP TL"){
                    $labelWarna = 'purple';
                  };

                  date_default_timezone_set('Asia/Makassar');
                  if ($team_order->modified_at=='' || $team_order->modified_at==NULL){
                    $jamWo = $team_order->updated_at;
                  }
                  else{
                    $jamWo = $team_order->modified_at;
                  };

                  if ($jamWo<>''){
                      $awal  = date_create($jamWo);
                      $akhir = date_create(); // waktu sekarang
                      $diff  = date_diff( $awal, $akhir );
                                          
                      if($diff->d <> 0){
                            $waktu = ' // '.$diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                      }
                      else {
                            $waktu = ' // '.$diff->h.' Jam | '.$diff->i.' Menit'; 
                      }
                  }
                  else{
                    $waktu = ' //';
                  }

                  ?>
                  <a class="label label-{{ $labelWarna }} {{ $gaul }}" data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ @$team_order->Reported_Date}}<br /><b>Dispatch at</b> :<br />{{ @$team_order->updated_at }}<br /><b>Status</b> : <br />{{ @$team_order->laporan_status }} / <small></small><br /><b>Catatan Teknisi</b> : <br />{{@$team_order->catatan }}<br /><a href='/tiket/{{ @$team_order->id_dt }}'>Detil</a>">
                  #{{ ++$num }} {{ @$team_order->Ndem }} // {{ @$team_order->laporan_status ? : 'ANTRIAN' }} // {{ $waktu }} 
                   
                    <?php
                    // if ($team_order->laporan_status_id == 1 && $team_order->Assigned_by=="CUSTOMERASSIGNED") {
                    //   echo "(".$team_order->is_3HS.")";
                    // } else {
                    //   if ($team_order->is_12HS>0) {
                    //     echo "(".$team_order->is_12HS.")";
                    //   } 
                    // }
                    ?>
                    <?php
                    if ($team_order->GAUL>0) { echo " // GAUL"; } else { echo ""; }
                    ?> // {{ $team_order->e_reported_date }}</a>
                    @if ($team_order->assurance_level<>"")
                    &nbsp;<span class="label label-danger">{{ $team_order->assurance_level }}</span>
                    @endif
                    <?php $pattern = "/HVC/i"; if (preg_match($pattern, $team_order->e_customer_type)){ ?>
                      &nbsp;<span class="label label-danger">{{ $team_order->e_customer_type }}</span>
                    <?php }; ?>
                    @if ($team_order->e_is=="PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" || $team_order->Owner_Group=="ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)")
                    &nbsp;<span class="label label-danger">UNSPEC</span>
                    @endif
                    <?php $pattern = "/SQM/i"; if (preg_match($pattern, $team_order->e_summary)){ ?>
                      &nbsp;<span class="label label-danger">SQM</span>
                    <?php }; ?>
                    @if($team_order->e_contact_name=="MANUAL_USER")
                    &nbsp;<span class="label label-danger">SQM MANUAL</span>
                    @endif
                    @if ($team_order->Status=="CLOSED" || $team_order->Status=="MEDIACARE" || $team_order->Status=="SALAMSIMPATIK" || $team_order->Status=="FINALCHECK")
                    &nbsp;<span class="label label-danger">CLOSE</span>
                    @endif
                    @php
                      $under = date_create($team_order->jam_manja);
                      $jam = date_format($under, "H");
                      $under_date = date_create();
                      $diff = date_diff($under, $under_date);
                    @endphp
                    @if ($diff->d > 0 && $team_order->statusManja=="CUSTOMERASSIGNED" && $team_order->jam_manja <> "")
                      &nbsp;<span class="label label-danger">MANJA H-{{ $diff->d }} JAM {{ $jam }}</span>
                    @endif
                    @if ($under==$under_date && $team_order->statusManja=="CUSTOMERASSIGNED" && $team_order->jam_manja <> "")
                      &nbsp;<span class="label label-danger">MANJA JAM {{ $jam }}</span>
                    @endif
                    @if($team_order->roc_customer_assign=="CA_YES")
                    &nbsp;<span class="label label-danger">MANJA {{ $team_order->ca_manja_time }}</span>
                    @endif
                    @if($team_order->pelanggan_hvc=="YES")
                    &nbsp;<span class="label label-danger">HVC</span>
                    @endif
                    <!-- @php
                      $under = date_create($team_order->Reported_Date);
                      $under_time = date_format($under, "H:i:s");
                      $under_date = date_format($under, "Y-m-d");
                      $date = date('Y-m-d');
                      $jam5 = "17:00:00";
                    @endphp
                    @if ( $under_date == $date && $under_time < $jam5 )
                      &nbsp;<span class="label label-danger">< JAM 5 </span>
                    @endif
                    @php
                      $under = date_create($team_order->Reported_Date);
                      $under_date = date_create();
                      $diff = date_diff($under, $under_date);
                    @endphp
                    @if ($diff->d > 0)
                      &nbsp;<span class="label label-danger"> H-{{ $diff->d }} </span>
                    @endif -->
                    @if ($team_order->dt_jenis_order == "IN")
                      @php
                        date_default_timezone_set('Asia/Makassar');
                        $date_conv = date('Y-m-d H:i:s', strtotime($team_order->e_reported_date));
                        $datecreate = date_create($date_conv);
                        $jam_log = date_format($datecreate, "H");
                        $ttr_jam = ($jam_now - $jam_log);
                        $ttr_balik = (24 - $jam_log + $jam_now);
                      @endphp
                      @if ($ttr_jam > 0 && !in_array($team_order->laporan_status, ["UP","UP UNSPEC"]))
                        @if ($ttr_jam <= 3)
                          &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">< 3 JAM</span>
                        @elseif ($ttr_jam <= 12)
                          &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">< 12 JAM</span>
                        @elseif ($ttr_jam > 12)
                          &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">> 12 JAM</span>
                        @endif
                      @elseif ($ttr_jam < 0 && !in_array($team_order->laporan_status, ["UP","UP UNSPEC"]))
                        &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">> 12 JAM</span>
                      @endif
                    @endif
                    @if ($team_order->Owner_Group=="REMO")
                    &nbsp;<span class="label label-danger">REMO</span>
                    @endif
                    @if ($team_order->Owner_Group=="WOC FULFILLMENT KALSEL (BANJARMASIN)")
                    &nbsp;<span class="label label-danger">FFG</span>
                    @endif
                    <br />
                  @endforeach
                </td>
                <td>{{ $NP }}</td>
                <td>{{ $OGP }}</td>
                <td>{{ $KT }}</td>
                <td>{{ $KP }}</td>
                <td>{{ $UP }}</td>
                <?php
                  $total_NP += $NP;
                  $total_OGP += $OGP;
                  $total_KT += $KT;
                  $total_KP += $KP;
                  $total_UP += $UP;
                ?>
              </tr>
              @endif
              @endforeach
              <tr>
                <th colspan="2">TOTAL</th>
                <th>{{ $total_NP }}</th>
                <th>{{ $total_OGP }}</th>
                <th>{{ $total_KT }}</th>
                <th>{{ $total_KP }}</th>
                <th>{{ $total_UP }}</th>
              </tr>
            </table>
          </div>
      </div>
      @endforeach
    </div>
  </div>
@endsection
