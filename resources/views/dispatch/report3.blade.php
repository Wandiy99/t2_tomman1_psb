@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
   MONITORING WORK ORDER <a href="/ms2n/sync/VA" class="btn btn-default">
      <span class="glyphicon glyphicon-refresh"> SyncNow</span>
    </a>
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'dispatch') ? 'active' : '' }}"><a href="/dispatch">ALL WORK ORDER</a></li>
    <li class="{{ (Request::path() == 'dispatchPI') ? 'active' : '' }}"><a href="/dispatchPI">WORK ORDER READY</a></li>
    <li class="{{ (Request::path() == 'woSTB') ? 'active' : '' }}"><a href="/woSTB">WORK ORDER STB</a></li>
    <li class="{{ (Request::path() == 'woPI') ? 'active' : '' }}"><a href="/woPI">PI to FAILSWA</a></li>
    <li class="{{ (Request::path() == 'falloutWFM') ? 'active' : '' }}"><a href="/falloutWFM">FALLOUT WFM</a></li>
    <li class="{{ (Request::path() == 'falloutActivation') ? 'active' : '' }}"><a href="/falloutActivation">FALLOUT ACTIVATION</a></li>
<!--     <li class="{{ (Request::segment(2) == 'dispatchedPsb') ? 'active' : '' }}"><a href="/dispatch/dispatchedPsb">DISPATCHED SHVA</a></li> -->
    <li class="{{ (Request::segment(2) == 'reportPsb2') ? 'active' : '' }}"><a href="/dispatch/reportPsb2">REPORT WO SHVA</a></li>
  </ul>
  <div style="padding-bottom: 10px;">
    <strong>Mapping Order SHVA</strong><br/>
    <strong></strong>
<!--     <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong> -->
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading">Mapping</div>
    <div class="panel-body">
      <div class="list-group">
	      <br />
				<table class="table">
					<tr>
						<td>No</td>
						<td>id</td>
					</tr>
				{{-- */
					$number = 1;
				
				/* --}} 
				   
				   
				@foreach ($get_list as $no => $list)
				<?php
            $path = "/upload4/evidence/{$list->id}/K3";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
          ?>
          @if (file_exists(public_path().$th))
					<tr>
						<td>{{ $number++ }}</td>
						<td>{{ $list->id }}</td>
						<td>
							
		            <a href="{{ $img }}">
		              <img src="{{ $th }}" alt="K3" />
		            </a>
		         
						</td>
					</tr>
					@endif
				@endforeach
				</table>
				
      </div>
     
    </div>
  </div>

		<div class="panel-body">	
		<script>
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
			</script>
							
							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
