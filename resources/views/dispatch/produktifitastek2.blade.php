@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    .UP4 {
      background-color : #2ecc71;
    }
    .okay {
      background-color : #636e72;
    }
    .red {
      background-color : #ff7675;
    }
  </style>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <div style="padding-bottom: 10px; text-align: center;">
      <h3><b>PRODUKTIFITAS<br />PERIODE {{ $dateAwal }} s/d {{ $date }}</b></h3>
  </div>

  <div class="panel panel-primary" id="produktifitas" style="text-align: center;">
    <?php
      if ($area == 1) { $area = "INNER"; } elseif ($area == 0) { $area = "OUTER"; }
    ?>
    <div class="panel-body">
      <form method="GET">
            <div class="col-md-2">
	  						<label>Tanggal Awal</label>
	  						<input type="date" name="dateAwal" id="dateAwal" class="form-control" value="{{ $dateAwal }}">
  					</div>

  					<div class="col-md-2">
	  						<label>Tanggal Akhir</label>
	  						<input type="date" name="date" id="date" class="form-control" value="{{ $date }}">
  					</div>

            {{-- <div class="col-md-2">
              <label class="control-label" for="input-area">Area</label>
              <select class="form-control select2" id = "area" name="area">
              <option value="" selected disabled>Pilih Area</option>
              <option value="ALL"> ALL</option>
              <option value="1">1</option>
              </select>
            </div> --}}

            <div class="col-md-3">
              <label class="control-label" for="input-type-sektor">Sektor</label>
              <select class="form-control select2" id = "input-type-sektor" name="sektor">
                <option disabled>Pilih Sektor</option>
                <option value="ALL">ALL</option>
              </select>
            </div>

            <div class="col-md-3">
              <label class="control-label" for="input-provider">Provider</label>
              <select class="form-control select2" id = "input-provider" name="provider">
                <option disabled>Pilih Provider</option>
              </select>
            </div>

            <br />
  					<div class="col-md-2">
  						<input type="submit" id="bt_direct" value="Proses" class="btn btn-primary btn-rounded btn-block">
  					</div>
      </form>
      </div>
    </div>
	    <div class="table-responsive">

        <table class="table table-striped table-bordered" id="closingAverage">
            <tr>
                <th class="align-middle" rowspan="2">TIM</th>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="{{ date('t') }}">PS ({{ strtoupper(date('F Y')) }})</th>
                <th class="align-middle" rowspan="2">TOTAL<br/>PS</th>
                <th class="align-middle" rowspan="2">AVG</th>
            </tr>
            <tr>
                    @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                                $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                                $keys = $i;
                            @endphp
                        @endif
                    <th class="align-middle">{{ $keys }}</th>
                    @endfor
            </tr>
            @php
                $ps = 0;
                $txt_ps = '';
            @endphp
            @foreach ($get_closing as $result)
            <tr>
                <td class="align-middle">{{ $result->tim ? : 'NON TIM' }}</td>
                <td class="align-middle">{{ $result->sektor ? : 'NON SEKTOR' }}</td>
                @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                                $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                                $keys = $i;
                            @endphp
                        @endif
    
                        @php
                            $txt_ps = 'ps_d'.$keys;
                            $ps = $result->$txt_ps;

                            if ($ps > 0)
                            {
                              $bg = 'background-color: #1ABC9C; color: white !important;';
                            } else {
                              $bg = '';
                            };
                        @endphp
                        <td class="align-middle" style="{{ $bg }}">{{ $ps }}</td>
                @endfor
                <td class="align-middle">{{ $result->total_ps ? : '0' }}</td>
                <td class="align-middle">{{ $result->total_ps / ($result->total_days + 1) }}</td>
            </tr>
            @endforeach
        </table>

        <table class="table table-striped table-bordered" id="produktifitas" style="text-align: center;">
          <tr>
            <th>RANK</th>
            <th>TIM</th>
            <th>SEKTOR</th>
            <th>ORDER</th>
            <th>NP</th>
            <th>OGP</th>
            <th>KP</th>
            <th>KT</th>
            <th>HR</th>
            <th>MO</th>
            <th>PDA</th>
            <th>AO</th>
            <th>AO COMPLETED</th>
          </tr>
          @php
            $total = $NP = $OGP = $KP = $KT = $HR = $UP_MO = $UP_PDA = $UP = $UP_COMPLETED = 0;
          @endphp
          @foreach ($getTeamMatrix as $num => $team)
          @php
            $total += $team->jumlah;
            $NP += $team->NP;
            $OGP += $team->OGP;
            $KP += $team->KP;
            $KT += $team->KT;
            $HR += $team->HR;
            $UP += $team->UP;
            $UP_COMPLETED += $team->UP_COMPLETED;
            $UP_MO += $team->UP_MO;
            $UP_PDA += $team->UP_PDA;
            $colorUP = "okay";
            $colorUPCOM = "okay";
            $colorUPMO = "okay";
            $colorUPPDA = "okay";
            $colorNP = "okay";

            if ($team->UP>=3) { $colorUP = "UP4"; };
            if ($team->UP_COMPLETED>=3) { $colorUPCOM = "UP4"; };
            if ($team->UP_MO>=3) { $colorUPMO = "UP4"; };
            if ($team->UP_PDA>=3) { $colorUPPDA = "UP4"; };
            if ($team->NP==0) { $colorNP = "red"; };
            if ($team->UP==0) { $colorUP = "red"; };
            if ($team->UP_COMPLETED==0) { $colorUPCOM = "red"; };
          @endphp
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $team->uraian }}</td>
            <td>{{ $team->title }}</td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/ORDER">{{ $team->jumlah }}</a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/NP"><span class="badge {{ $colorNP }}">{{ $team->NP }}</span></a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/OGP">{{ $team->OGP }}</a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/KP">{{ $team->KP }}</a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/KT">{{ $team->KT }}</a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/HR">{{ $team->HR }}</a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/UP_MO"><span class="badge {{ $colorUPMO }}">{{ $team->UP_MO }}</span></a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/UP_PDA"><span class="badge {{ $colorUPPDA }}">{{ $team->UP_PDA }}</span></a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/UP_AO"><span class="badge {{ $colorUP }}">{{ $team->UP }}</span></a></td>
            <td><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/{{ $team->id_regu }}/UP_COMPLETED"><span class="badge {{ $colorUPCOM }}">{{ $team->UP_COMPLETED }}</span></a></td>
          </tr>
          @endforeach
          <tr>
            <th colspan="3">Total</th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/ORDER">{{ $total }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/NP">{{ $NP }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/OGP">{{ $OGP }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/KP">{{ $KP }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/KT">{{ $KT }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/HR">{{ $HR }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/UP_MO">{{ $UP_MO }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/UP_PDA">{{ $UP_PDA }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/UP_AO">{{ $UP }}</a></th>
            <th><a href="/produktifitasteklist/{{ $dateAwal }}/{{ $date }}/ALL/UP_COMPLETED">{{ $UP_COMPLETED }}</a></th>
          </tr>
        </table>

      </div>
    </div>
  </div>


  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
      $(function(){
			// $('#bt_direct').click(function(){
      //  			 window.location.href = "/produktifitastek"+$('#dateAwal').val()+"/"+$('#date').val()+"/1/"+$('#sektor').val()+";
      // 			});
			var day = {
		      	  format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

		    $('#dateAwal').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		    $('#date').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

      var data = <?= json_encode($get_listsektor) ?>;
        var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Sektor',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-type-sektor').select2(select2Options());

      var dataProvider = <?= json_encode($get_provider) ?>;
        var select2Options = function() {
        return {
          data: dataProvider,
          placeholder: 'Input Sektor',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-provider').select2(select2Options());

			})
	</script>
@endsection