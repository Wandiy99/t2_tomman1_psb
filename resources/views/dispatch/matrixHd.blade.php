@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      text-align: center;
      vertical-align: middle;
    }
    .red {
      background-color: #FF0000;
      color : #FFF;
    }
    .label-greenx{
      background-color: #1abc9c;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <h3>Matrix Workorder </h3>
    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>
  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX HD</div>
      <div class="panel-body">
        <div class="list-group">
    	    <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th width="10" rowspan="2">No</th>
                <th width="200" rowspan="2">Nama Tim</th>
                <th width="60" rowspan="2">Target MH</th>
                <th width="60" colspan=2 align="center"><center>WO</center></th>
                <th width="60" rowspan="2" align="center"><center>ANTRIAN</center></th>
                <th width="60" rowspan="2" align="center"><center>PROGRESS</center></th>
                <th width="60" rowspan="2" align="center"><center>KENDALA</center></th>
                <th width="60" rowspan="2" align="center"><center>UP</center></th>
                <th width="60" rowspan="2" align="center"><center>MH ACH</center></th>
                <th width="60" rowspan="2" align="center"><center>PROD</center></th>
                <th rowspan="2">LIST</th>
              </tr>
              <tr>
                <th width=30>Jumlah</th>
                <th width=30>MH</th>
              </tr>
              <?php
                $targetMH = 8;
              ?>
              @foreach($data as $no=>$list)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $list['nama'] }}</td>
                    <td><center>{{ $targetMH }}</center></td>
                    <td><center>{{ $list['jumlahWO'] }}</center></td>
                    <td><center></center></td>
                    <td><center>{{ $list['NO_UPDATE'] }}</center></td>
                    <td><center>{{ $list['PROGRESS'] }}</center></td>
                    <td><center>{{ $list['KENDALA'] }}</center></td>
                    <td><center>{{ $list['UP'] }}</center></td>
                    <td><center></center></td>
                    <td><center></center></td>
                    <td>
                      @foreach ($list['WO'] as $wo)
                        <?php
                          if ($wo['status_laporan']==6 || $wo['status_laporan']==NULL){
                            $label = 'default';
                          } else if ($wo['status_laporan']<>5 && $wo['status_laporan']<>6 && $wo['status_laporan']<>1 && $wo['status_laporan']<>4) {
                            $label = 'danger';
                          } else if ( $wo['status_laporan']==5){
                            $label = 'warning';
                          } else if ( $wo['status_laporan']==4){
                            $label = 'info';
                          } else if ($wo['status_laporan']==1){
                            $label = 'success';
                          } else {
                            $label = 'default';
                          }
                        ?>
                        <span class="label label-{{ $label }}">{{ $wo['sc'] }}</span> 
                      @endforeach
                    </td>
                  </tr>
                @endforeach
              <tr>
                <td colspan="2"><center>Total</center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center></center></td>
              </tr>
            </table>
          </div>
      </div>
    </div>
  </div>
  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX HD Fallout</div>
      <div class="panel-body">
        <div class="list-group">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th width="10" rowspan="2">No</th>
                <th width="300" rowspan="2">Nama</th>
                <th width="60" rowspan="2">Target PBS</th>
                <th width="60" rowspan="2">Target Perbaikan</th>
                <th width="60" rowspan="2">Take Order</th>
                
                <th width="60" colspan="2" align="center"><center>UP</center></th>
                <th width="60" rowspan="2" align="center"><center>Resolves</center></th>
                <th width="60" rowspan="2" align="center"><center>PBS</center></th>
                <th rowspan="2">LIST</th>
              </tr>
              <tr>
                <th width=30>FO</th>
                <th width=30>PDA</th>
              
              </tr>

              <?php
                $targetMH     = 8;
                $jmlWo        = 0;
                $jmlProgress  = 0;
                $jmlUpFo      = 0;
                $jmlUpPda     = 0;

                $targetPsb = 100;
                $targetPerbaikan = 12;
              ?>

              @foreach($fout as $no=>$list)
                <tr>
                    <?php
                        $jmlWo += $list['jumlahWO'];
                        $jmlUpFo += $list['UP'];
                        $jmlProgress += $list['PROGRESS']; 
                        $jmlUpPda += $list['UP_PDA'];  
                     ?>

                    <td>{{ ++$no }}</td>
                    <td>{{ $list['nama'] }}</td>
                    <td><center>{{ $targetPsb }} %</center></td>
                    <td><center>{{ $targetPerbaikan }}</center></td>
                    <td><center>{{ $list['jumlahWO'] }}</center></td>                  
                    <td><center>{{ $list['UP'] }}</center></td>
                    <td><center>{{ $list['UP_PDA'] }}</center></td>
                    <td><center>{{ $list['UP'] + $list['UP_PDA'] }}</center></td>
                    <td><center>{{ round((($list['UP'] + $list['UP_PDA']) / $targetPerbaikan) * (100)) }} % </center></td>
                    <td>
                      @foreach ($list['WO'] as $wo)
                        <?php
                          if ($wo['status_laporan']=="Process OSS (Activation Completed)" || $wo['status_laporan']=="Completed (PS)" || $wo['status_laporan']=="Process ISISKA (RWOS)"){
                            $label = 'success';
                          } else{
                            $label = 'default';
                          }
                        ?>
                        @if($wo['keteranganWo']==0)
                          <span class="label label-{{ $label }}">{{ $wo['sc'] }}</span>
                        @else
                          <span class="label label-{{ $label }}">{{ $wo['sc'] }} [PDA]</span>
                        @endif 
                      @endforeach
                    </td>
                  </tr>
                @endforeach
              <tr>
                <td colspan="2"><center>Total</center></td>
                <td><center></center></td>
                <td><center></center></td>
                <td><center>{{ $jmlWo }}</center></td>
                <td><center>{{ $jmlUpFo }}</center></td>
                <td><center>{{ $jmlUpPda }}</center></td>
                <td><center>{{ $jmlUpFo + $jmlUpPda  }}</center></td>
                <td><center></center></td>
                <td><center></center></td>
              </tr>
            </table>
          </div>
      </div>
    </div>
  </div>

  <div class="panel panel-primary" id="matrix">
    <div class="panel-heading">MATRIX PRODUCTIFITAS MANJA</div>
      <div class="panel-body">
        <div class="list-group">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                  <td>NIK</td>
                  <td>Nama</td>
                  <td>Manja OK</td>
                  <td>Manja NOK</td>    
              </tr>
              
              @foreach($manjas as $manja)
                  <tr>
                      <td>{{ $manja->manjaBy }}</td>
                      <td>{{ $manja->nama }}</td>
                      <td>{{ $manja->manjaOk }}</td>
                      <td>{{ $manja->manjaNOK }}</td>
                  </tr>
              @endforeach
            </table>
          </div>
      </div>
    </div>
  </div>


<div id="timexcount">0 ms</div>
		<div class="panel-body">
		<script>
        $(document).ready(function(){
          $("[data-toggle='popover']").popover({html:true});
        })
				var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
        var c=0;
        var minutes= 0;
        var t;
        var timer_is_on=0;

        function timedCount(element)
        {
        document.getElementById(element).innerHTML = minutes+' min '+c+' sec';
        c=c+1;
        if (c%60==0){
          minutes+=1;
          c=0;
        }
        t=setTimeout("timedCount()",1000);
        }

        function showElement(element){
          document.getElementById(element).innerHTML = "Detected";
        }


    </script>

							<div id="bar-10" style="height: 450px; width: 100%;"></div>
						</div>
					</div>
  </div>
@endsection
