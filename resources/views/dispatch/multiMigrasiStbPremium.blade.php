@extends('layout')
@section('content')
@include('partial.alerts')

{{-- <div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Harap Perhatikan Format yang Digunakan !!! Jangan Ada Field yang Kosong atau Memakai Garis Baru (Enter)</strong>
</div> --}}

<div class="row">

<div class="col-sm-12">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">Dispatch Order Migrasi STB Premium</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/multi/migrasiStbPremiumSave" enctype="multipart/form-data" autocomplete="off">

        <div class="form-group">
            <label for="input-regu" class="col-form-label">REGU</label>
            <input type="text" name="id_regu" class="form-control" id="input-regu"/>
            {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="tgl-dispatch" class="col-form-label">TANGGAL DISPATCH</label>
            <input type="text" class="form-control" id="tgl_dispatch" name="tgl_dispatch" placeholder="yyyy-mm-dd">
            {!! $errors->first('tgl_dispatch','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
          <label class="control-label" for="nopel">No Internet</label>
          <input type="text" name="nopel[]" id="nopel" class="form-control" />
          {!! $errors->first('nopel','<p class="label label-danger">:message</p>') !!}
        </div>
        
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-primary">Dispatch</button>
        </div>
      </form>
    </div>
  </div>
</div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {

      $('#tgl_dispatch').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });

      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Pilih Regu',
          formatResult: function(data) {
          return '<p>'+data.text+'</p>';
          }
        }
      }
      $('#input-regu').select2(regu());

      $.ajax({
            url : '/getOrdersDll/MIGRASI_STB_PREMIUM',
            dataType : 'json',
            success : function(data){
                var material = data;
                $('#nopel').select2({
                    data: material,
                    placeholder: 'Pilih No Internet',
                    allowClear: true,
                    multiple: true,
                });
            }
        })
    });
</script>
@endsection