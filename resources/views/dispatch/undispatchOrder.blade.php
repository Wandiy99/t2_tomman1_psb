@extends('layout')
@section('content')
@include('partial.alerts')
<div class="row">

<div class="col-sm-6">
    <div class="panel panel-primary">

        @if(in_array(session('auth')->id_karyawan, ['20981020', '21010605', '16930880']))
            <a href="/dismantle/tarikOrderDismanlte" class="btn btn-warning waves-effect waves-light" type="button" style="color: white !important"><span class="btn-label"><i class="fa fa-recycle"></i></span>Tarik Order</a>
        @endif
        
        <div class="panel-heading text-center" style="background-color: white; border-color: white">
            <h4 style="font-weight: bolder !important;color: black !important;">DISMANTLING / CABUT NTE</h4>
        </div>
        <div class="panel-body">
            <form id="submit-form" method="post" action="/multi/undispatchOrder/undispatch_cabut_nte" enctype="multipart/form-data" autocomplete="off">
                <div class="form-group">
                    <label class="control-label" for="format">Format</label>
                        <textarea name="format" id="message" class="form-control" rows="5" placeholder="Masukan Format: WFM ID/NCLI/INET , SC , NAMA_PELANGGAN Lalu Pisah Dengan Titik Koma" required /></textarea>
                        {!! $errors->first('format','<p class="label label-danger">:message</p>') !!}
                </div>
                <div style="text-align: center">
                    <button class="btn btn-primary">Upload</button>
                </div>
            </form>
            <br />
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="panel panel-primary">

        @if(in_array(session('auth')->id_karyawan, ['20981020', '21010605', '16930880']))
            <a href="/ontPremium/tarikOrderontPremium" class="btn btn-warning waves-effect waves-light" type="button" style="color: white !important"><span class="btn-label"><i class="fa fa-recycle"></i></span>Tarik Order</a>
        @endif

        <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">ONT PREMIUM</h4>
        </div>
        <div class="panel-body">
            <form id="submit-form" method="post" action="/multi/undispatchOrder/undispatch_ont_premium" enctype="multipart/form-data" autocomplete="off">

                <div class="form-group">
                <label class="control-label" for="format">Format</label>
                    <textarea name="format" id="format" class="form-control" rows="5" placeholder="Masukan Format: NO_INET , NO_VOICE , ALAMAT , NAMA_PELANGGAN Lalu Pisah Dengan Titik Koma" required /></textarea>
                    {!! $errors->first('format','<p class="label label-danger">:message</p>') !!}
                </div>
                
                <div style="text-align: center">
                    <button class="btn btn-primary">Upload</button>
                </div>
            </form>
            <br />
        </div>
    </div>
</div>

</div>
@endsection