@extends('layout')

@section('content')
  @include('partial.alerts')
    <style>
    /* .marker {
     background-size: cover;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    cursor: pointer;
    }
    */
    .mapboxgl-popup {
      font-size: 12px;
    }
    </style>
  <div id="mapMarkerModal_mapView"></div>
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />

<script>
    (function (){
        var mapEl = document.getElementById('mapMarkerModal_mapView');
        mapEl.style.height = (window.innerHeight * .65) + 'px';



        var map;
        var marker;
        var $in;


        var initMap = function() {
            if (map) return;



            <?php
            foreach ($list as $num => $data) {
              if ($num==0){
                ?>
                var center = { lng: {{ $data->lon }}, lat: {{ $data->lat }} };
                map = new mapboxgl.Map({
                    container: mapEl,
                    center: center,
                    zoom: {{ @$zoom }},
                    style: 'https://map.tomman.app/styles/osm-liberty/style.json'
                });
                <?php
              }

              if ($data->umurApprove>=7){
                $color = "red";
              } elseif ($data->umur>7){
                $color = "red";
              } else {
                $color = "green";
              }
              if ($data->uraian<>""){
                $color = "grey";
              }
            ?>
            var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
            'ORDER ID : {{ $data->orderId }}<br />TIM : {{ $data->uraian ? : "UNDISPATCH" }}<br />STATUS : {{ $data->laporan_status }}<br /><a href="/dispatch/{{ $data->orderId }}">DISPATCH</a>  '
            );

            new mapboxgl.Marker({ draggable: false, icon:'default', color:'{{ $color }}', cursor:'pointer' })
                .setLngLat({lat : '{{ $data->lat ? : 0 }}',lon : '{{ $data->lon ? : 0 }}'})
                .setPopup(popup)
                .addTo(map);
            <?php
            }
            ?>
            <?php
            foreach ($list_scbe as $num => $data) {

              if (@$data->umurApprove>=7){
                $color = "red";
              } elseif (@$data->umur>7){
                $color = "red";
              } else {
                $color = "green";
              }
              if ($data->uraian<>""){
                $color = "grey";
              }
            ?>
            var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
            'ORDER ID : {{ $data->ORDER_CODE }}<br />TIM : {{ $data->uraian ? : "UNDISPATCH" }}<br />STATUS : {{ $data->laporan_status }}<br /><a href="/dispatch/{{ $data->ORDER_CODE }}">DISPATCH</a>  '
            );

            new mapboxgl.Marker({ draggable: false, icon:'default', color:'{{ $color }}', cursor:'pointer' })
                .setLngLat({lat : '{{ $data->lat ? : 0 }}',lon : '{{ $data->lon ? : 0 }}'})
                .setPopup(popup)
                .addTo(map);
            <?php
            }
            ?>

        }
        initMap();

    })()
</script>
<div class="row">
    <div class="col-sm-16">
        <div class="panel panel-default">
            <div class="panel-heading">
                Data Order Starclick Terdekat
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>SC</th>
                        <th>MYIR</th>
                        <th>SC-MYIR</th>
                        <th>JARAK</th>
                        <th>UMUR SC</th>
                        <th>UMUR APPR</th>
                        <th>STO</th>
                        <th>PELANGGAN</th>
                        <th>ALAMAT</th>
                        <th>ORDER DATE</th>
                        <th>Status</th>
                        <th>Status Tek</th>
                        <th>Dispatch</th>
                    </tr>
                    @foreach ($list as $num => $result)
                    <?php
                    if ($result->umurApprove<>""){
                      $tglAwal = new DateTime($result->umurApprove);
                      $tglAkhir = new DateTime(date('Y-m-d'));
                      $umurApprove = $tglAkhir->diff($tglAwal)->days + 1;
                      $umurApprove .= " hari";
                    } else {
                      $umurApprove = "~";
                    }
                    if ($result->umur<>""){
                      $tglAwal = new DateTime($result->umur);
                      $tglAkhir = new DateTime(date('Y-m-d'));
                      $umurSC = $tglAkhir->diff($tglAwal)->days + 1;
                      $umurSC .= " hari";
                    } else {
                      $umurSC = "~";
                    }
                    ?>
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->orderId }}</td>
                        <td>{{ $result->myir }}</td>
                        <td>{{ $result->sc }}</td>
                        <td>{{ $result->jarak*1000 }}m</td>
                        <td>{{ @$umurSC }}</td>
                        <td>{{ @$umurApprove }}</td>
                        <td>{{ $result->STO }}</td>
                        <td>{{ $result->orderName }}</td>
                        <td>{{ $result->orderAddr }} <br />{{ $result->alamatLengkap }}<br />{{ $result->kcontact }}</td>
                        <td>{{ $result->orderDate }}</td>
                        <td>{{ $result->orderStatus }}</td>
                        <td>{{ @$result->laporan_status }}</td>
                        <td>{{ @$result->uraian }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>
    <div class="col-sm-16">
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Order SCBE Terdekat
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>MYIR</th>
                    <th>JARAK</th>
                    <th>UMUR</th>
                    <th>STO</th>
                    <th>PELANGGAN</th>
                    <th>ALAMAT</th>
                    <th>ORDER DATE</th>
                    <th>Status</th>
                    <th>Status Tek</th>
                    <th>Dispatch</th>
                </tr>
                @foreach ($list_scbe as $num => $result)
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->ORDER_CODE }}</td>
                    <td>{{ $result->jarak*1000 }}m</td>
                    <td>{{ $result->umur }} hari</td>
                    <td>{{ $result->STO }}</td>
                    <td>{{ $result->CUSTOMER_NAME }}</td>
                    <td>{{ $result->INS_ADDRESS }}</td>
                    <td>{{ $result->ORDER_DATE }}</td>
                    <td>{{ $result->STATUS_RESUME }}</td>
                    <td>{{ @$result->laporan_status }}</td>
                    <td>{{ @$result->uraian }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

</div>
@endsection
