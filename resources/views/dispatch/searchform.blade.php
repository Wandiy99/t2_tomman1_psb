@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlist')

  <!-- dialog !-->
 
    <form class="form-group" method="post" id="submit-form" >
      <div class="form-group {{ $errors->has('Search') ? 'has-error' : '' }}">
          <label for="Search">Search</label>
          <input type="number" name="Search" class="form-control" id="Search" value="">
          {!! $errors->first('Search', '<p class=help-block>:message</p>') !!}
          <button class="btn form-control btn-primary">Search</button>
      </div>
    </form>
 


  @if ($data <> NULL)
  <br />
  <div class="table-responsive">
    <div class="col-ls-12">
    {{ $data }}. <br />
    Result ({{ $jumlah }}) Found.
    <br />
    <br />
    @if ($jumlah > 0)
    <table class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
          <th>#</th>
  		    <th width="75" colspan=2>Work Order</th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $no => $data)
        <tr>
          <td rowspan=3>{{ ++$no }}.</td>
  		@if(session('auth')->level <> 19)
          <td colspan=2>
  		@if(!empty($data->id_r))
          <span class="label label-default">{{ $data->uraian }}</span>
                @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 )
                  <a href="/dispatch/{{ $data->orderId }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
      				  @endif
                <span class="label label-warning">{{ $data->hd }}</span>
                <span class="label label-warning">{{ $data->hd_date }}</span>
                <span class="label label-info">{{ $data->orderDate }}</span>
                <?php
                  $dateStart = strtotime($data->orderDate);
                  $dateEnd = strtotime(date('Y-m-d H:i:s'));
                  $diff = $dateEnd - $dateStart;
                  $jam = floor($diff / (60 * 60));
                ?>
                <span class="label label-primary">{{ $jam }} Jam</span>
              @else
                <a href="/dispatch/{{ $data->orderId }}" class="label label-info">Dispatch</a>
              @endif
  		    </span>

          <!-- <span><a href="http://mydashboard.telkom.co.id/ms2/update_demand_useetv2.php?ndem=&etat=VA" class="label label-info">Update MS2N</a></span> -->
          <!-- <span><a href="/sendsms/{{ $data->orderId }}" class="label label-warning">Send SMS</a></span> -->
          <span><label class="label label-success">{{ $data->laporan_status ? : 'ANTRIAN' }}</label></span>

          <!-- @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 )
                <a href="/sendkpro/manual/{{ $data->id_dt }}" class="label label-primary"> Send To KPRO</a>
                <a class="label label-default">{{ $data->pl_modif }}</a>
          @endif -->

          @if ($data->dispatch_by==4)
              <span><label class="label label-success">Fulfillment Guarante</label></span>
          @endif

         {{--  @if(session('auth')->level == 2 || session('auth')->level == 30)
              <a href="/dispatch/manja/{{$data->orderId }}" class="label label-info" data-toggle="tooltip" title="{{ $data->manja_status }}" >M a n j a</a>
          @endif --}}

          @if(session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 && $data->orderId <> "")
          <a href="/ibooster/delete/{{ $data->orderId }}" class="label label-danger">DELETE IBOOSTER</a>
          @endif
          @if((session('psb_reg')<>1) && session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46 && $data->id_pl <> "")
          <a href="/dispatch/delete/{{ $data->orderId }}/{{ $data->id_dt }}/{{ $data->id_pl ? : '0' }}" class="label label-danger" id="delete_sc" data-toggle="tooltip" >DELETE DISPATCH</a>
          @endif
          @endif
        </td>
      
  		</tr>
  		<tr>
          <td>

  	        {{ $data->orderStatus }}
            @if ($data->flagging<>'')
            WO Cluster {{ $data->flagging }}
            @endif
            <br />

  			@if ($data->id_dt<>'')
  			<a href="/{{ $data->id_dt }}">SC{{ $data->orderId ? : 'Tidak Ada SC' }}</a>
  			@else
  			SC{{ $data->orderId ? : 'Tidak Ada SC' }}
  			@endif

  			/ {{ $data->orderDate ? : 'Tidak Ada SC' }}<br />
  	        {{ $data->ndemPots }}~{{ $data->ndemSpeedy }}<br />
  	        {{ $data->orderName }}<br />
            {{ $data->ndemPots ?: '-' }}<br />
  	        {{ $data->sto }} /
  	        {{ $data->jenisPsb }} <br />
            NCLI : {{ $data->orderNcli ? : '' }} <br />
            {{ $data->alproname ? : 'Tidak Ada Informasi ODP' }} <br />
        @if ($data->LOCN_X <> '')
        ({{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }}, {{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }}) / {{ $data->OLT_IP }} / {{ $data->OLT }} <br />
        Lokasi ODP : http://maps.google.com/?q={{ substr($data->LOCN_X,0,2) }}.{{ substr($data->LOCN_X,2,10) }},{{ substr($data->LOCN_Y,0,3) }}.{{ substr($data->LOCN_Y,3,10) }} <br />
        @endif
  	        {{ $data->Kcontact ? : $data->kcontact }}<br />
            {{ $data->orderAddr ? : '' }} <br />

             <!-- catatan kordinat pelanggan dan alamat sales -->
             Koordinat Pelanggan : <b>{{ $data->kordinatPel ? : ' - ' }}</b><br>
             Alamat Sales        : <b>{{ $data->alamatSales ? : ' - ' }}</b><br><br>



             {{-- catatan unsc sdi --}}
             @if (!empty($dataUnsc))
                CATATAN UNSC SDI<br />
                {{ $dataUnsc->catatan ?: $dataUnsc->catatan ?: '-'}}
             @endif



          </td>
       <!-- <td>
            @if ($data->nik_sales <> '')
            {{ $data->nik_sales }}<br />
            Korlap : {{ $data->korlap }}<br />
            {{ $data->nama_pelanggan }}<br />
            {{ $data->pic }}<br />
            {{ $data->alamat }}<br />
            {{ $data->nama_odp }}<br />
            Kordinat ODP : {{ $data->koor_odp }}<br />
            Kordinat Pelanggan : {{ $data->koor_pel }}<br />
            {{ $data->keterangan }}<br />
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_Rumah_Pelanggan-th.jpg" /></a>
            <a href="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP.jpg" ><img src="/upload/dshr/{{ $data->id_dshr }}/Foto_ODP-th.jpg" /></a>
          @endif
        </td> -->
  	     </tr>
  			<tr>
          <td>
  	        <b>Catatan Teknisi :</b><br />
            {{ $data->catatan ? : 'Tidak ada' }}<br />
  		    </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <b>Valins ID :</b><br />
            {{ $data->valins_id ? : '-' }}
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <b>Progress Marina :</b><br />
            Jenis Order: {{ $data->mc_nama_order ? : '-' }} <br /> <br />
            Terakhir Update: {{ $data->mc_modif_at ? : '-' }} <br />
            User Yang Update: {{ $data->mc_modif_by ? : '-' }} <br />
            Tanggal Selesai: {{ $data->tgl_selesai ? : '-' }}<br />
            Nama Regu: {{ $data->dispatch_regu_name ? : '-' }} <br />
            Status: {{ $data->status ? : '-' }} <br />
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            Dropcore Labelcode : {{ $data->dropcore_label_code ? : '-' }}<br />
            ODP Labelcode : {{ $data->odp_label_code ? : '-' }}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    </div>
    @endif

  </div>
  </div>

  @endif
</div>
@endsection