@extends('layout')

@section('content')
  @include('partial.alerts')

      <div class="panel panel-default">
          <div class="panel-heading">
              Order Migrasi STB Premium
          </div>
          <div class="panel-body">
              <form method="post" class="form-vertical">
                  <div class="form-group">
                      <div class="col-md-6">
                        <label for="tgl_dispatch" class="control-label">Tanggal Dispatch</label>
                        <input name="tgl_dispatch" type="date" id="tgl_dispatch" class="form-control" />
                        {!! $errors->first('tgl_dispatch','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-6">
                        <label for="nama_regu" class="control-label">Regu</label>
                        <input name="nama_regu" type="text" id="nama_regu" class="form-control" />
                        {!! $errors->first('nama_regu','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                   <div class="form-group">
                      <div class="col-md-12">
                        <label for="no_internet" class="control-label">No Internet</label>
                        <input name="no_internet[]" type="text" id="no_internet" class="form-control" />
                        {!! $errors->first('no_internet','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>
                  
                  <div class="form-group">
                      <div class="col-md-12">
                        <br/>
                          <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-refresh"></span>
                              <span>Proses</span>
                          </button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {

            var regu = <?= json_encode($regu) ?>;
            $('#nama_regu').select2({
                data: regu,
                placeholder: 'Pilih Regu',
                allowClear: true,
            });

        });
    </script>
@endsection