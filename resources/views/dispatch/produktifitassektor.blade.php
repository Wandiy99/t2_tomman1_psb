@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      padding: 5px;
      text-align: center;
      vertical-align: middle;
    }
    td {
      padding :5px;
      border-color:black;
    }
    .UP4 {
      background-color : #2ecc71;
    }
    .okay {
      background-color : #636e72;  
    }
    .red {
      background-color : #ff7675;  
    }

  </style>
  <div style="padding-bottom: 10px;">

    <h3>Produktifitas </h3>

    <strong>{{ date('d F Y') }} {{ date("H:i", mktime(date("H")+8, date("i"), date("m"), date("d"), date("Y"))) }} WITA</strong>
  </div>

  <div class="panel panel-primary" id="produktifitas">
    <div class="panel-heading">PRODUKTIFITAS SEKTOR - {{ $date }}</div>
    <div class="panel-body">
      <div class="list-group">
	    <div class="table-responsive" id="produktifitas">
        <table class="table table-striped table-bordered" >
          <tr>
            <th>Rank</th>
            <th>Sektor</th>
            <th>ORDER</th>
            <th>NP</th>
            <th>OGP</th>
            <th>KP</th>
            <th>KT</th>
            <th>HR</th>
            <th>MO</th>
            <th>PDA</th>
            <th>AO</th>
          </tr>
          <?php
            $total = 0; 
            $NP = 0; 
            $OGP = 0; 
            $KP = 0; 
            $KT = 0; 
            $HR = 0; 
            $UP_MO = 0;
            $UP_PDA = 0;
            $UP = 0;
          ?>
          @foreach ($getTeamMatrix as $num => $team)
          <?php
            $total += $team->jumlah;
            $NP += $team->NP;
            $KP += $team->KP;
            $OGP += $team->OGP;
            $KT += $team->KT;
            $HR += $team->HR;
            $UP += $team->UP;
            $UP_MO += $team->UP_MO;
            $UP_PDA += $team->UP_PDA;
            $colorUP = "okay";
            $colorUPMO = "okay";
            $colorUPPDA = "okay";
            $colorNP = "okay";
            if ($team->UP>=3) { $colorUP = "UP4"; }
            if ($team->UP_MO>=3) { $colorUPMO = "UP4"; }
            if ($team->UP_PDA>=3) { $colorUPPDA = "UP4"; }
            if ($team->NP==0) { $colorNP = "red"; }
            if ($team->UP==0) { $colorUP = "red"; }
          ?>
          <tr>
            <td align="center">{{ ++$num }}</td>
            <td>{{ $team->title }}</td>
            <td align="center">{{ $team->jumlah }}</td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->title }}/NP"><span class="badge {{ $colorNP }}">{{ $team->NP }}</span></a></td> 
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->title }}/OGP">{{ $team->OGP }}</a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->title }}/KP">{{ $team->KP }}</a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->title }}/KT">{{ $team->KT }}</a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->title }}/HR">{{ $team->HR }}</a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->id_regu }}/UP_MO"><span class="badge {{ $colorUPMO }}">{{ $team->UP_MO }}</span></a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->id_regu }}/UP_PDA"><span class="badge {{ $colorUPPDA }}">{{ $team->UP_PDA }}</span></a></td>
            <td align="center"><a href="/produktifitassektorlist/{{ $date }}/{{ $team->id_regu }}/UP_AO"><span class="badge {{ $colorUP }}">{{ $team->UP }}</span></a></td>
         </tr>
          @endforeach
          <tr> 
            <th colspan="2">Total</th>
            <th>{{ $total }}</th>
            <th>{{ $NP }}</th>
            <th>{{ $OGP }}</th>
            <th>{{ $KP }}</th>
            <th>{{ $KT }}</th>
            <th>{{ $HR }}</th>
            <th>{{ $UP_MO }}</th>
            <th>{{ $UP_PDA }}</th>
            <th>{{ $UP }}</th>
          </tr>
        </table>


      </div>
    </div>
  </div>
</div>
@endsection
