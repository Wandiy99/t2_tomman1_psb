@extends('layout')

@section('content')
  @include('partial.alerts')  
  <div class="panel panel-primary" id="info">
      <div class="panel-heading">Detail Penggunaan Material</div>
      <div class="panel-body table-responsive">
        <table class="table table-bordered table-hover table-fixed ">
           <tr>
              <th>No</th>
              <th>Sektor</th>
              <td>Tim</td>
              <th>ID Item</th>
              <th>Item</th>
              <th>RFC</th>
              <th>Project</th>
              <th>Jumlah Terpakai</th>
              <th>Wo Terpakai</th>
           </tr>

           @if (($list)<>'')
             @foreach($list as $no=>$dt)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $dt['sektor'] }}</td>
                    <td>{{ $dt['regu'] }}</td>
                    <td>{{ $dt['id_item'] }}</td>
                    <td>{{ $dt['item'] }}</td>
                    <td>{{ $dt['rfc'] }}</td>
                    <td>{{ $dt['project'] }}</td>
                    <td>{{ $dt['jumlahPakai'] }}</td>
                    @if ($dt['wo']=='')
                      <td>-</td>
                    @else
                      <td>
                        @foreach($dt['wo'] as $listWo)
                            <a data-toggle="popover" data-placement="bottom" title="WO : {{ $listWo->Ndem }}" data-content="{{ $listWo->id_item }} : {{ $listWo->qty }}" class="label label-primary">{{ $listWo->Ndem }}</a>
                        @endforeach
                      </td>
                    @endif
                </tr>
             @endforeach
          @endif
        </table>
      </div>
  </div>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover(); 
});
</script>
@endsection
