@extends('layout')

@section('content')
@include('partial.alerts')  
<h3>Dashboard Pemakaian Material</h3>

<div class="panel panel-default">
	<div class="panel-body"> 
		<form method="GET" class="form-horizontal">
			<div class="form-group" class="form-horizontal">
              	<div class="col-md-12">
                  	<label for="mitra" class="control-label">Mitra</label>
                    <input name="mitra" type="text" id="mitra" class="form-control" value="{{ $fieldMitra }}" />
              	</div>
          	</div>

          	<div class="form-group">
                <div class="col-md-12">
                    <label for="pid" class="control-label">Project ID</label>
                    <input name="pid" type="text" id="pid" class="form-control" value="{{ $fieldPid }}"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label for="sektor" class="control-label">Sektor</label>
                    <input name="sektor" type="text" id="sektor" class="form-control" value="{{ $fieldSektor }}"/>
                </div>
            </div>

           	<div class="form-group">
                <div class="col-md-12">
                    <label for="periode" class="control-label">Periode</label>
                    <input name="periode" type="text" id="periode" class="form-control" value="{{ $fieldPeriode }}"/>
                </div>
            </div>

            <div class="form-group">
              	<div class="col-md-12">
                  	<button class="btn btn-primary" type="submit">
                      	<span class="glyphicon glyphicon-refresh"></span>
                     	<span>Proses</span>
                 	</button>
              	</div>
            </div>
		</form>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-4"> 
				<table border="1" class="table">
				<th>NO</th>
				<th>ID ITEM </th>
				<th>PENGELUARAN</th>
				<th>PEMAKAIAN</th>
				<th>PENGEMBALIAN</th>
				<th>SALDO</th>

				<?php
					$totalRegister = 0;
					$totalPakai	   = 0;
					$totalKembali  = 0;
					$totalSaldo    = 0;
				  $no = 1;
         ?>

				@foreach($material as $dt)
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $dt['id_item'] }}</td>
						<td>{{ number_format($dt['jumlahRegister']) }}</td>
						<td><a href="/listPengeluaran/{{ $pid }}/{{ $mitra }}/{{ $periode }}/{{ $sektor }}/{{ $dt['id_item'] }}">{{ number_format($dt['jumlahTerpakai']) }}</a></td>
						<td>{{ number_format($dt['jumlahKembali']) }}</td>
						<td><a href="/listSisaSaldo/{{ $pid }}/{{ $mitra }}/{{ $periode }}/{{ $dt['id_item'] }}">{{ number_format($dt['jumlahRegister'] - $dt['jumlahTerpakai'] - $dt['jumlahKembali']) }}</td>
					
						<?php
							$totalRegister = $totalRegister + $dt['jumlahRegister'];
							$totalPakai	   = $totalPakai + $dt['jumlahTerpakai'];
							$totalKembali  = $totalKembali + $dt['jumlahKembali'];
							$totalSaldo    = $totalSaldo + ($dt['jumlahRegister'] - $dt['jumlahTerpakai'] - $dt['jumlahKembali']);
						?>
					</tr>
				@endforeach
					<tr>
						<td colspan="2">Total</td>
						<td>{{ number_format($totalRegister) }}</td>
						<td>{{ number_format($totalPakai) }}</td>
						<td>{{ number_format($totalKembali) }}</td>
						<td>{{ number_format($totalSaldo) }}</td>

					</tr>
				</table>	
			</div>			
	</div>
</div>

@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {
            var project = <?= json_encode($dataProject) ?>;
            console.log(project)
            $('#pid').select2({
                data: project,
                allowClear : true,
                placeholder: 'Pilih project',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
           
            var mitras = <?= json_encode($dataMitra) ?>;
            $('#mitra').select2({
                data: mitras,
                placeholder: 'Pilih Mitra',
                allowClear: true,
                // multiple: true
                maximumSelectionSize: 0,
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });

            var sektors = <?= json_encode($dataSektor) ?>;
            $('#sektor').select2({
                data: sektors,
                placeholder: 'Pilih Sektor',
                allowClear: true,
                // multiple: true
                maximumSelectionSize: 0,
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });

            var day = {
              format : 'yyyy-mm',
              viewMode: "months",
              minViewMode: "months",
              autoClose: true
            };
       
            $('#periode').datepicker(day).on('changeDate', function(e){
              $(this).datepicker('hide');
            });
        });
    </script>
@endsection