@extends('tech_layout')

@section('content')
@include('partial.alerts')  
  <div class="panel panel-default">
      <div class="panel-heading">INPUT NO. RFC</div>
      <div class="panel-body">
          <form class="row" style="margin-bottom: 20px" method="POST" action="/rfc/input">
              <div class="col-md-12">
                <div class="col-md-12">
                      <div class="form-group">
                          <label class="control-label">No. RFC</label>
                          <input type="text" class="form-control" name="q" id="q" placeholder="Cari No. RFC" value="{{ old('q') }}">
                          {!! $errors->first('q', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div>

               <!--  <div class="col-md-3">
                      <div class="form-group">
                          <label class="control-label">Tanggal Permintaan RFC</label>
                          <input type="text" class="form-control" name="tgl" id="tgl" placeholder="Tanggal Permintaan RFC" value="{{ date('Y-m-d') }}">
                          {!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}
                      </div>
                </div> -->

                <div class="col-md-3">              
                    <button class="btn btn-primary" type="submit">
                          <span class="glyphicon glyphicon-search"></span>
                    </button>
                </div>
            </div>
          </form>
      </div>
  </div>
@endsection
