@extends('layout')
@section('content')	
	@include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">Send DC Area</div>
		<div class="panel-body">
			<form method="POST">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="gudang">Gudang</label>
							<input type="text" name="gudang" id="gudang" value="-1001194255648">
	                      	<input type="submit" value="Kirim" class="btn btn-primary">
	                      	{!! $errors->first('gudang','<br><div class="label label-danger">:message</div>') !!}
						</div>
					</div>
				</div><hr>

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-2">
							<div class="form-group">
								<label>100 | Stok : {{ $data[100][0]->jumlah ? : '0' }}</label>
								<?php
									$disable = '';
									if ($data[100][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split100" class="form-control" {{ $disable }}>
							</div>	
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>150 | Stok : {{ $data[150][0]->jumlah ? : '0' }} </label>
								<?php
									$disable = '';
									if ($data[150][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split150" class="form-control" {{ $disable }} >
							</div>	
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>200 | Stok : {{ $data[200][0]->jumlah ? : '0' }} </label>
								<?php
									$disable = '';
									if ($data[200][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split200" class="form-control" {{ $disable }} >
							</div>	
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>250 | Stok : {{ $data[250][0]->jumlah ? : '0' }}</label>
								<?php
									$disable = '';
									if ($data[250][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split250" class="form-control" {{ $disable }} >
							</div>	
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>300 | Stok : {{ $data[300][0]->jumlah ? : '0' }}</label>
								<?php
									$disable = '';
									if ($data[300][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split300" class="form-control" {{ $disable }} >
							</div>	
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>350 | Stok : {{ $data[350][0]->jumlah ? : '0' }}</label>
								<?php
									$disable = '';
									if ($data[350][0]->jumlah==NULL){
										$disable = 'readonly';
									};
								 ?>
								<input type="text" name="split350" class="form-control" {{ $disable }} >
							</div>	
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection	

@section('plugins')
  <script type="text/javascript">
      $(function() {
      		var gudang = <?=json_encode($gudang) ?>;
         	$("#gudang").select2({data:gudang});
      });
  </script>
@endsection