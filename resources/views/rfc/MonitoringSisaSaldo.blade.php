@extends('layout')

@section('content')
  @include('partial.alerts')  
  <div class="panel panel-primary" id="info">
      <div class="panel-heading">Detail Saldo Material</div>
      <div class="panel-body table-responsive">
        <table class="table table-bordered table-hover table-fixed ">
           <tr>
              <th>No</th>
              <th>ID Item</th>
              <th>Regu</th>
              <th>Sisa Saldo</th>
           </tr>

           @foreach($listNotRegu as $no=>$list)
              <tr>
                  <td>1</td>
                  <td>{{ $list->id_barang }}</td>
                  <td>NOT INPUT TEKNISI</td>
                  <td>{{$list->saldo}}</td>
              </tr>
           @endforeach

           @foreach($listRegu as $no=>$list)
              <tr>
                  <td>2</td>
                  <td>{{ $list->id_item }}</td>
                  <td>TERINPUT TEKNISI</td>
                  <td>{{$list->saldo}}</td>
              </tr>
           @endforeach
        

        </table>
      </div>
  </div>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover(); 
});
</script>
@endsection
