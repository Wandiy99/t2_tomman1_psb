@extends('layout')
@section('content')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Stok STO 2
					<a href="{{ route('send.dc.area') }}" class="btn btn-primary pull-right" style="margin-top: -3px;">Kirim Stok</a>
				</div>
				<div class="panel-body table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>DC</th>
							<th>UPATEK</th>
							<th>AGB</th>
							<th>CUI</th>
							<th>SPM</th>
							<th>Jumlah</th>
						</tr>

						@foreach($ukuran as $ukur)
						<tr>	
							<td>{{ $ukur }}</td>
							@foreach($data[$ukur] as $dt)
								<td>{{ $dt->upatek ? : '0' }}</td>
								<td>{{ $dt->agb ? : '0' }}</td>
								<td>{{ $dt->cui ? : '0' }}</td>
								<td>{{ $dt->spm ? : '0' }}</td>
								<td>{{ $dt->jumlah ? : '0' }}</td>
							@endforeach
						</tr>
						@endforeach
					</table>
				</div>
				
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Stok Area</div>
				<div class="panel-body table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>DC</th>
							<th>BJM 1</th>
							<th>BJM 2</th>
							<th>BJM - Area</th>
							<th>BTL</th>
							<th>BRB</th>
							<th>TJG</th>
							<th>BJB</th>
							<th>Jumlah</th>
						</tr>

						@foreach($ukuran as $ukur)
						<tr>	
							<td>{{ $ukur }}</td>
							@foreach($dataArea[$ukur] as $dt)
								<td>{{ $dt->bjm1 ? : '0' }}</td>
								<td>{{ $dt->bjm2 ? : '0' }}</td>
								<td>{{ $dt->bjm_area ? : '0' }}</td>
								<td>{{ $dt->btl ? : '0' }}</td>
								<td>{{ $dt->brb ? : '0' }}</td>
								<td>{{ $dt->tjg ? : '0' }}</td>
								<td>{{ $dt->bjb ? : '0' }}</td>
								<td>{{ $dt->jumlah ? : '0' }}</td>	
							@endforeach
						</tr>
						@endforeach
					</table>
				</div>
				
			</div>
		</div>
	</div>
@endsection