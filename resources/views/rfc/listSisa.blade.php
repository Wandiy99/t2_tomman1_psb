@extends('tech_layout')

@section('content')
@include('partial.alerts')
 <h4></h4>

  @if(count($datas) <> 0)
    <div class="panel panel-default">
        <div class="panel-heading">List Sisa Material Teknisi</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>RFC</th>
              		    <th>ID Item</th>
                      <th>Nama Item</th>
                      <th>Jumlah Terpakai</th>
                      <th>Sisa Material </th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($datas as $no=>$data)
                        <tr>
                          <?php
                              $sisa = $data->jumlah - $data->jmlTerpakai - $data->jmlKembali;
                           ?>

                           @if ($sisa > 0 )
                              <td>{{ $data->rfc }}</td>
                              <td>{{ $data->id_item }}</td>
                              <td>{{ $data->nama_item }}</td>
                              <td>{{ $data->jmlTerpakai }}</td>
                              <td>{{ $sisa }}</td>
                            @endif 
                        </tr>
                      @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
  @endif
@endsection
