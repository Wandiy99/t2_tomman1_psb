@extends('layout')

@section('content')
  @include('partial.alerts')
  
  <div class="row">
      <h3>
        <div class="col-sm-6">    
          <a href="/reboundary/proses/input" class="btn btn-info">
            <span class="glyphicon glyphicon-plus"></span>
          </a>
        </div>
      </h3>      
  </div>  
  <br>

  <div class="panel panel-primary">
      <div class="panel-heading">List Reboundary</div>
      <div class="panel-body">
          
          <form method="GET">
            <div class="input-group col-sm-12">
              
                <input type="text" class="form-control inputSearch" name="q" placeholder="Pencarian No. SC" />
                <span class="input-group-btn">
                    <button class="btn btn-success search">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div><br>
          </form>

          <table class="table table-bordered">  
            @foreach ($data as $no=>$dt)
              <tr>
                  <td width="1%">{{ ++$no }}</td>
                  <td>
                       @if(session('auth')->level==2 || session('auth')->level==15 )
                        @if ($dt->dispatch==1)
                          <a href="/reboundary/dispatch/{{ $dt->scid }}/1" class="btn btn-sm btn-primary">Re-Dispatch</a>
                          <?php
                              if ($dt->status_laporan==6 || $dt->status_laporan==null ){
                                  $ket = 'NEED PROGRESS';
                              }
                              else{
                                  $ket = $dt->laporan_status;
                              };
                           ?>
                          <label class="btn btn-danger btn-sm">{{ $dt->uraian }} // {{ $ket }}</label>
                        @else
                          <a href="/reboundary/dispatch/{{ $dt->scid }}/2" class="btn btn-sm btn-primary">Dispatch</a>
                        @endif
                      @endif
                         
                      <a href="/reboundary/proses/edit/{{ $dt->id }}" class="btn btn-sm btn-primary">Edit</a>
                      <br><br>
                      <b>Tgl Order : </b>{{ $dt->updated_at }}<br>
                      <b>{{ $dt->orderName }} ( {{ $dt->orderKontak }} )</b><br>
                      {{ $dt->scid }} <br>
                      {{ $dt->jenisPsb }} <br>
                      {{ $dt->noTelp }} <br>
                      {{ $dt->ndemPots }} <br>
                      {{ $dt->ndemSpeedy }} <br>
                      <b>Tarikan Lama : </b>{{ $dt->tarikan_lama}} M<br>
                      <b>Koor. Pelannggan : </b>{{ $dt->koor_pelanggan }}<br>
                      <b>ODP Lama : </b>{{ $dt->odp_lama }} // {{ $dt->koor_odp_lama }}<br>
                      <b>ODP Baru : </b>{{ $dt->odp_baru }} // {{ $dt->koor_odp_baru }}<br>
                      <b>Estimasi Tarikan : </b>{{ $dt->estimasi_tarikan }} M<br>
                      <b>Catatan : </b>{{ $dt->catatan }}<br>
                      <b>Sektor : </b>{{ $dt->title ?: '-' }}
                  </td>
              </tr>
            @endforeach
          </table>
      </div>
  </div>


@endsection