@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .text1{
      color: black;
    }

    .link:link{
      color: black;
    }

    .link:visited{
      color: black;
    }
</style>
  <div id="screnshoot">  
    <!-- <h3>Periode {{ $tgl }}</h3> -->
    
    
    <div class="row">
      <div class="col-sm-6">
        <h3>Dashboard REBOUNDARY TA KALSEL [Periode {{$tgl}}]</h3>
        <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th>STATUS</th>
            <th>INNER</th>
            <th>BBR</th>
            <th>KDG</th>
            <th>TJL</th>
            <th>BLC</th>
            <th>JUMLAH</th>
          </tr>
          <?php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $total_inner = 0;                                                                                                                                                                                                                                         
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          ?>
       
            @foreach ($progressReboundary as $result)
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                <tr>
                    <td>{{ $result->laporan_status }}</td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/INNER">{{ $result->WO_INNER }}</a></td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BBR">{{ $result->WO_BBR }}</a></td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/KDG">{{ $result->WO_KDG }}</a></td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/TJL">{{ $result->WO_TJL }}</a></td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BLC">{{ $result->WO_BLC }}</a></td>
                    <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                </tr>
                <?php
                  $total_inner += $result->WO_INNER;
                  $total_bbr += $result->WO_BBR;
                  $total_kdg += $result->WO_KDG;
                  $total_tjl += $result->WO_TJL;
                  $total_blc += $result->WO_BLC;
                ?>
                @else
                <?php
                  $no_update_jumlah += $result->jumlah;
                  $no_update_inner += $result->WO_INNER;
                  $no_update_bbr += $result->WO_BBR;
                  $no_update_kdg += $result->WO_KDG;
                  $no_update_tjl += $result->WO_TJL;
                  $no_update_blc += $result->WO_BLC;
                ?>

                @endif
            @endforeach
    
          <tr>
            <td>ANTRIAN</td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/INNER">{{ $no_update_inner }}</a></td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/BBR">{{ $no_update_bbr }}</a></td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/KDG">{{ $no_update_kdg }}</a></td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/TJL">{{ $no_update_tjl }}</a></td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/BLC">{{ $no_update_blc }}</a></td>
            <td><a href="/dashboard/reboundary/{{ $tgl }}/ALL/ANTRIAN/ALL">{{ $no_update_jumlah }}</a></td>
          </tr>
          <?php
          $total_inner = $total_inner + $no_update_inner;
          $total_bbr = $total_bbr + $no_update_bbr;
          $total_kdg = $total_kdg + $no_update_kdg;
          $total_tjl = $total_tjl + $no_update_tjl;
          $total_blc = $total_blc + $no_update_blc;
          $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          ?>
          <tr>
            <td class="warna7 text1">TOTAL</td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/INNER">{{ $total_inner }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/BBR">{{ $total_bbr }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/KDG">{{ $total_kdg }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/TJL">{{ $total_tjl }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/BLC">{{ $total_blc }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/reboundary/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td>
          </tr>
        </table>
      </div> 

       <div class="col-sm-6">
        <h3>Dashboard Dispatch [Periode {{$tgl}}]</h3>
        <table class="table table-striped table-bordered dataTable">
            <tr>
                <th>Sales</th>
                <th>Sudah Dispatch</th>
                <th>Belum Dispatch</th>
            </tr>

            @foreach($dataDispatch as $data)
                <tr>
                    <td>BJM</td>
                    <td><a href="/dashboard/dispatch-reboundary/INNER/{{ $tgl }}/1">{{ $data->WO_INNER_dispatch ?: 0}}</a></td>
                    <td><a href="/dashboard/dispatch-reboundary/INNER/{{ $tgl }}/0">{{ $data->WO_INNER_undispatch ?: 0}}</a></td>
                </tr>

                <tr>
                    <td>BJB</td>
                    <td><a href="/dashboard/dispatch-reboundary/BBR/{{ $tgl }}/1">{{ $data->WO_BBR_dispatch ?: 0}}</a></td>
                    <td><a href="/dashboard/dispatch-reboundary/BBR/{{ $tgl }}/0">{{ $data->WO_BBR_undispatch ?: 0}}</a></td>
                </tr>

                <tr>
                    <td>TJL</td>
                    <td><a href="/dashboard/dispatch-reboundary/TJL/{{ $tgl }}/1">{{ $data->WO_TJL_dispatch ?: 0}}</a></td>
                    <td><a href="/dashboard/dispatch-reboundary/TJL/{{ $tgl }}/0">{{ $data->WO_TJL_undispatch ?: 0}}</a></td>
                </tr>

                <tr>
                    <td>BLC</td>
                    <td><a href="/dashboard/dispatch-reboundary/BLC/{{ $tgl }}/1">{{ $data->WO_BLC_dispatch ?: 0}}</a></td>
                    <td><a href="/dashboard/dispatch-reboundary/BLC/{{ $tgl }}/0">{{ $data->WO_BLC_undispatch ?: 0}}</a></td>
                </tr>

                <tr>
                    <td>TOTAL</td>
                    <?php
                        $totalDispatch = $data->WO_INNER_dispatch + $data->WO_BBR_dispatch + $data->WO_TJL_dispatch + $data->WO_BLC_dispatch;
                        $totalUndispatch = $data->WO_INNER_undispatch + $data->WO_BBR_undispatch + $data->WO_TJL_undispatch + $data->WO_BLC_undispatch;  
                     ?>
                    <td><a href="/dashboard/dispatch-reboundary/ALL/{{ $tgl }}/1">{{ $totalDispatch }}</a></td>
                    <td><a href="/dashboard/dispatch-reboundary/ALL/{{ $tgl }}/0">{{ $totalUndispatch }}</a></td>
                </tr>
            @endforeach
        </table>
      </div> 

     
    </div>
  </div>
@endsection