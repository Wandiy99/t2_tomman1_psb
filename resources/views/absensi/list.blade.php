@extends('tech_layout')

@section('content')
  @include('partial.alerts')
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        REKAP ABSENSI
      </div>
      <div class="panel-body table-responsive">
        <table class="table">
          <tr>
            <th>No.</th>
            <th>TGL</th>
            <th>JADWAL</th>
            <th>KEHADIRAN</th>
          </tr>
          @foreach ($query as $num => $result)
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $result->tgl }}</td>
            <td>{{ $result->jadwal }}</td>
            <td>{{ $result->hadir }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
