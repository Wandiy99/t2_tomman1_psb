@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <a href="/alpro/tambahkan" class="btn btn-warning">+</a>
            <table class="table">
              <tr>
                <td>#</td>
                <td>ALPRONAME</td>
                <td>LAT</td>
                <td>LON</td>
                <td>AVAI</td>
                <td>USED</td>
                <td>RSV</td>
                <td>RSK</td>
                <td>IS_TOTAL</td>
              </tr>
              @foreach ($get_alpro as $num => $alpro)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $alpro->ODP_NAME }}</td>
                <td>{{ $alpro->LATITUDE }}</td>
                <td>{{ $alpro->LONGITUDE }}</td>
                <td>{{ $alpro->AVAI }}</td>
                <td>{{ $alpro->USED }}</td>
                <td>{{ $alpro->RSV }}</td>
                <td>{{ $alpro->RSK }}</td>
                <td>{{ $alpro->IS_TOTAL }}</td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection
