@extends('layout')

@section('content')
@include('partial.alerts')
    <form method="POST" autocomplete="off">
        <div class="col-md-12">
            <a href="/dshr/splitter/list" class="btn btn-default">
                <span class="glyphicon glyphicon-arrow-left"></span>
            </a>

            <button type="submit" class="btn btn-primary">Simpan</button>
        </div><br><br>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ubah Data </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>No Tiket</label>
                                <input type="text" class="form-control" name="noTiket" value="{{ $data->noTiket }}">
                                {!! $errors->first('noTiket', '<span class="label label-danger">:message</span>') !!}
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Pelanggan</label>
                                <input type="text" class="form-control" name="nmPelanggan" value="{{ $data->nm_pelanggan }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                              <div class="form-group">
                                  <label>No. Telpon</label>
                                  <input type="text" class="form-control" name="noTelp" value="{{ $data->no_telp }}">
                                  {!! $errors->first('noTelp', '<span class="label label-danger">:message</span>') !!}
                              </div>
                        </div>

                        <div class="col-md-6">
                             <div class="form-group">
                              <label class="control-label" for="input-regu">Regu</label>
                              <input name="idRegu" type="hidden" id="input-regu" class="form-control" value="{{ $data->id_regu }}" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ODP</label>
                                <input type="text" class="form-control" name="nmOdp" id="input-odp" value="{{ $data->nm_odp }}">
                            </div>                          
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Koordinat ODP</label>
                                <input type="text" class="form-control" name="koordinatOdp" value="{{ $data->koor_odp }}">
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                              <div class="form-group">
                                  <label>Redamana Before</label>
                                  <input type="text" class="form-control" name="redamanBefore" value="{{ $data->redaman_before }}">
                              </div>                    
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Redamana After</label>
                                <input type="text" class="form-control" name="redamanAfter" value="{{ $data->redaman_after }}">
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Splitter</label>
                                <!-- <input type="text" class="form-control" name="splitter"> -->
                                <select class="form-control" name="splitter" id="splitter">
                                    <option value="1:2" {{ $data->splitter=='1:2' ? 'selected' : '' }}>1:2</option>
                                    <option value="1:4" {{ $data->splitter=='1:4' ? 'selected' : '' }}>1:4</option>
                                    <option value="1:8" {{ $data->splitter=='1:8' ? 'selected' : '' }}>1:8</option>
                                </select>
                            </div>             
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estimasi Tarikan</label>
                                <input type="text" class="form-control" name="tarikan" value="{{ $data->estimasi_tarikan }}">
                            </div>
                        </div>
                    </div>

                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Source Data</label>
                                <select class="form-control" name="source" id="source">
                                    <option value="PSB" {{ $data->source=='PSB' ? 'selected' : '' }}>PSB</option>
                                    <option value="migrasi" {{ $data->source=='migrasi' ? 'selected' : '' }}>Migrasi</option>
                                    <option value="Assurance" {{ $data->source=='Assurance' ? 'selected' : '' }}>Assurance</option>
                                    <option value="Maintenaince" {{ $data->source=='Maintenaince' ? 'selected' : '' }}>Maintenaince</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </form>
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script>
        $(document).ready(function () {
            $("#input-odp").inputmask("AAA-AAA-A{2,3}/999");

            var state= <?= json_encode($regu) ?>;
            var regu = function() {
              return {
                data: state,
                placeholder: 'Input Regu',
                formatResult: function(data) {
                return  '<span class="label label-default">'+data.id+'</span>'+
                      '<strong style="margin-left:5px">'+data.text+'</strong>';
                }
              }
            };

            $('#input-regu').select2(regu());
            $('#splitter').select2();
            $('#source').select2();
        });
  </script>
@endsection
