@extends('layout')
@section('content')
@include('partial.alerts')

<div class="row">

<div class="col-sm-3">
</div>

<div class="col-sm-6" style="text-align: center;">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">REPORT DOOR TO DOOR SALES - {{ $id }}</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/dshr/door-to-door/assign/update/{{ $id }}" enctype="multipart/form-data" autocomplete="off">

      <div class="form-group">
          <label for="input-odp" class="col-form-label">ODP</label>
          <p>
            @foreach (json_decode($data->list_odp) as $numb => $list_odp)
            {{ ++$numb }}. {{ $list_odp }}<br />
            @endforeach
          </p>
      </div>

      <div class="form-group">
        <label for="input-catatan" class="col-form-label">Catatan oleh HERO</label>
        <p>
          {{ $data->catatan }}
        </p>
    </div>

        <div class="form-group">
            <label for="input-brosur" class="col-form-label">Brosur</label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="brosur" class="form-control text-center" id="input-brosur" value="{{ old('brosur') ? : $data->brosur }}" required/>
            {!! $errors->first('brosur','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-prospek" class="col-form-label">Prospek</label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="prospek" class="form-control text-center" id="input-prospek" value="{{ old('prospek') ? : $data->prospek }}" required/>
            {!! $errors->first('prospek','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-inputan" class="col-form-label">Inputan</label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="inputan" class="form-control text-center" id="input-inputan" value="{{ old('inputan') ? : $data->inputan }}" required/>
            {!! $errors->first('inputan','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
          <label class="control-label" for="evidence">*Upload Evidence*</label>
            @if (count(@$check) > 0)
              @php
                $file = "data-default-file=/upload4/doortodoorassign/$id.jpg";
              @endphp
            @else
              @php
                $file = '';
              @endphp
            @endif
          <input type="file" accept="image/*" name="evidence" id="evidence" class="dropify" {{ $file }} data-height="500" required/>
          {!! $errors->first('evidence','<p class="label label-danger">:message</p>') !!}
        </div>
        
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-lg btn-primary btn-rounded">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-sm-3">
</div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {

      $('.dropify').dropify();

    });
</script>
@endsection