@extends('public_layout')

<?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=MYIR NOT SC ".$tgl.".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
?>
<!DOCTYPE html>
<html>
<body>
<table table border="1" width="100%">
    <tr align="center">
        <th>ID</th>
        <th>MYIR</th>
        <th>CUSTOMER</th>
        <th>NO_KTP</th>
        <th>PIC</th>
        <th>ND_INET</th>
        <th>ND_VOICE</th>
        <th>VALINS_ID</th>
        <th>ODP_MYIR</th>
        <th>ALAMAT_MYIR</th>
        <th>PAKET_HARGA</th>
        <th>PAKET_SALES</th>
        <th>TIM</th>
        <th>NIK1</th>
        <th>NIK2</th>
        <th>MITRA</th>
        <th>DATEL</th>
        <th>STO</th>
        <th>SEKTOR</th>
        <th>TL_SEKTOR</th>
        <th>ODP_LABELCODE</th>
        <th>DROPCORE_LABELCODE</th>
        <th>KOORDINAT_PELANGGAN</th>
        <th>JENIS_LAYANAN</th>
        <th>STATUS_TICKET</th>
        <th>ODP_TEKNISI</th>
        <th>KOORDINAT_ODP_TEKNISI</th>
        <th>CATATAN_TEKNISI</th>
        <th>UPDATED BY</th>
        <th>CATATAN_MANJA</th>
        <th>MANJA_BY</th>
        <th>MANJA_UPDATED</th>
        <th>HD_MANJA</th>
        <th>WAKTU_DISPATCH</th>
        <th>WAKTU_STATUS</th>
        <th>STATUS_KENDALA</th>
        <th>TGL_STATUS_KENDALA</th>
    </tr>
    @foreach($query as $no => $list)
    <tr>
        <td>{{ $list->ID }}</td>
        <td>{{ $list->MYIR }}</td>
        <td>{{ $list->CUSTOMER }}</td>
        <td>{{ $list->KTP }}</td>
        <td>{{ $list->PIC }}</td>
        <td>{{ $list->ND_INET ? : 'null' }}</td>
        <td>{{ $list->ND_VOICE ? : 'null' }}</td>
        <td>{{ $list->valins_id ? : 'null' }}</td>
        <td>{{ $list->ODP_MYIR ? : 'null' }}</td>
        <td>{{ $list->ALAMAT_MYIR ? : 'null' }}</td>
        <td>{{ $list->PAKET_HARGA ? : 'null' }}</td>
        <td>{{ $list->PAKET_SALES ? : 'null' }}</td>
        <td>{{ $list->TIM  ? : 'null' }}</td>
        <td>{{ $list->NIK1 }}</td>
        <td>{{ $list->NIK2 }}</td>
        <td>{{ $list->MITRA  ? : 'null' }}</td>
        <td>{{ $list->DATEL ? : 'null' }}</td>
        <td>{{ $list->STO ? : 'null' }}</td>
        <td>{{ $list->SEKTOR ? : 'null' }}</td>
        <td>{{ $list->TL_SEKTOR ? : 'null' }}</td>
        <td>{{ $list->ODP_LABELCODE ? : 'null' }}</td>
        <td>{{ $list->DROPCORE_LABELCODE ? : 'null' }}</td>
        <td>{{ $list->KOORDINAT_PELANGGAN ? : 'null' }}</td>
        <td>{{ $list->JENIS_LAYANAN ? : 'null' }}</td>
        <td>{{ $list->STATUS_TICKET ? : 'ANTRIAN' }}</td>
        <td>{{ $list->ODP_TEKNISI }}</td>
        <td>{{ $list->KOORDINAT_ODP_TEKNISI }}</td>
        <td>{{ $list->CATATAN_TEKNISI ? : 'null' }}</td>
        <td>{{ $list->MODIF }}</td>
        <td>{{ $list->CATATAN_MANJA }}</td>
        <td>{{ $list->MANJA_BY }}</td>
        <td>{{ $list->MANJA_UPDATED }}</td>
        <td>{{ $list->HD_MANJA }}</td>
        <td>{{ $list->WAKTU_DISPATCH }}</td>
        <td>{{$list->WAKTU_STATUS }}</td>
        <td>{{$list->status_kendala ? : '-' }}</td>
        <td>{{$list->tgl_status_kendala ? : '-' }}</td>
    </tr>
    @endforeach
</table>
</body>
</html>