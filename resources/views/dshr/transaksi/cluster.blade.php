
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>

    Monitoring DSHR Cluster


  </h3>

  <ul class="nav nav-tabs ajax">

    <li class="active"><a href="#" id="SUTOYO">SUTOYO S ({{ $data->SUTOYO }})</a></li>
    <li><a href="#" id="BUMI MAS">BUMI MAS ({{ $data->BUMI_MAS }})</a></li>
  





  </ul>
  <br/>
  <div id="txtHint"></div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
  $(document).ready(function(){
    $(".loading").css({"visibility":"visible"});
    $.get("/dshr/cluster/SUTOYO", function(text) {
      $("#txtHint").html(text);
    }).done(function(){
      $(".loading").css({"visibility":"hidden"});
    });
    $(".ajax a").on("click", function(){
      $(".loading").css({"visibility":"visible"});
      $(".nav").find(".active").removeClass("active");
      $(this).parent().addClass("active");
      $.get("/dshr/cluster/"+$(this).attr('id'), function(text) {
        $("#txtHint").html(text);
      }).done(function(){
        $(".loading").css({"visibility":"hidden"});
      });
    });
  });
  </script>
@endsection
