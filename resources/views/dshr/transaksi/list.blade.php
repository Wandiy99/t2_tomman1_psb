
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>
    <div class="col-sm-6">
  
      
    <a href="/dshr-transaksi/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
    </a>

    </div>
    <div class="input-group col-sm-6">
        <input type="text" class="form-control inputSearch" placeholder="Search.." name="q" />
        <span class="input-group-btn">
            <button class="btn btn-default search">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
  </h3>
  <ul class="nav nav-tabs ajax">
	@if(session('auth')->level <> 51)
    <li class="active"><a href="#" id="today">Today</a></li>
    <li><a href="#dua" id="tm">This Month</a></li>
    <!--<li><a href="#tiga" id="all">All</a></li>-->
	@endif

	@if(session('auth')->level <> 49)
    <li><a href="#empat" id="uim">Progress UIM</a></li>
    <li><a href="#lima" id="rfsoke">RFS OK (Siap Input)</a></li>
    <li><a href="#enam" id="odpbelummuncul">ODP Belum Muncul</a></li>
    <li><a href="#enam" id="odpsudahmuncul">ODP Sudah Muncul</a></li>
	@endif

	@if(session('auth')->level <> 51)
    <!-- <li><a href="#tujuh" id="followupsales">Follow UP Sales</a></li> -->
    <!-- {{-- <li><a href="#tujuh" id="followupsales">Siap Input</a></li> --}} -->
    
    <!-- <li><a href="#tujuh" id="followupok">Follow UP OK</a></li> -->
    <li><a href="#delapan" id="neworder">NO Update</a></li>
	@endif

    <!-- <li><a href="#sembilan" id="unscdeployer">UNSC Deployer</a></li> -->
    <!-- <li><a href="#sepuluh" id="unscnr2g">UNSC NR2G</a></li> -->
  
    <!-- <li><a href="#sebelas" id="kendalawebcare">KENDALA WEBCARE</a></li> -->
    <li><a href="#sebelas" id="kendalawebcare">Kendala Input</a></li> 
  
    <!-- <li><a href="#duabelas" id="belumwebcare">BELUM WEBCARE</a></li>
         <li><a href="#duabelas" id="belumwebcare">Ont Mati</a></li> -->
  
    <!-- <li><a href="#tigabelas" id="sudahwebcare">SUDAH WEBCARE</a></li> -->
     <!-- <li><a href="#tigabelas" id="sudahwebcare">Inet Bermasalah</a></li> -->

    <li><a href="#empatbelas" id="berhasilinput">Berhasil Input</a></li>
    <!-- <li><a href="#empatbelas" id="mappingok">Mapping MBSP</a></li> -->
    <!-- <li><a href="#limabelas" id="followupopen">Follow UP MBSP Open</a></li> -->



  </ul>

  <br/>
  <div id="txtHint"></div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
  $(document).ready(function(){
    $(".loading").css({"visibility":"visible"});
    $.get("/dshr/transaksi/today", function(text) {
      $("#txtHint").html(text);
    }).done(function(){
      $(".loading").css({"visibility":"hidden"});
    });
    $(".ajax a").on("click", function(){
      $(".loading").css({"visibility":"visible"});
      $(".nav").find(".active").removeClass("active");
      $(this).parent().addClass("active");
      $.get("/dshr/transaksi/"+$(this).attr('id'), function(text) {
        $("#txtHint").html(text);
      }).done(function(){
        $(".loading").css({"visibility":"hidden"});
      });
    });

    //button search
    $(".search").on("click", function(){
      $(".loading").css({"visibility":"visible"});
      $(".nav").find(".active").removeClass("active");
      $(this).parent().addClass("active");
      $.get("/dshr/search/"+$(".inputSearch").val(), function(text) {
        $("#txtHint").html(text);
      }).done(function(){
        $(".loading").css({"visibility":"hidden"});
      });
    });
    //input pasted
    $('.inputSearch').on('paste', function() {
      setTimeout(function () {
        $(".loading").css({"visibility":"visible"});
        $(".nav").find(".active").removeClass("active");
        $(this).parent().addClass("active");
        $.get("/dshr/search/"+$(".inputSearch").val(), function(text) {
          $("#txtHint").html(text);
        }).done(function(){
          $(".loading").css({"visibility":"hidden"});
        });
      }, 100);
    });
  });
  </script>
@endsection
