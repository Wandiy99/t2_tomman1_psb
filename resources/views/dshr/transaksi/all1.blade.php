<div class="table-responsive">
        <table class="table table-striped table-bordered ">
          <thead>
            <tr>
            <th width="5%">#</th>
            <th width="20%">Action / Status </th>
            <th width="10%">Flagging / SC Status</th>
            <th width="10%">Tanggal Deal</th>
            <th width="10%">Korlap / Nama Sales</th>
            <th width="10%">Data Pelanggan</th>
            <th width="10%">Data Kordinat</th>
            <th width="20%">Alamat / PIC </th>
            <th width="10%">Keterangan Paket</th>
            </tr>
          </thead>
          <tbody>
            {{-- */
                $no = 1;
                $id = 0;
            /* --}}
            @foreach($list as $no=>$data)

                <tr>
                <td>{{ ++$no }}</td>
                <td>
					<a href="/dshr-transaksi-telegram/{{ $data->id }}" class="label label-info">Send To Telegram</a>
				      <br />
          <a  href="/dshr-transaksi/{{ $data->id }}" class="label label-info">{{ $data->status_hdesk ? : 'EDIT' }}</a>
          <br />
          <b>Keterangan :</b><br /> {{ $data->keterangan_hdesk }}
        </td>
				<td>
          {{ $data->flagging }}<br />
          {{ $data->kode_sc }}<br />
          {{ $data->orderStatus }}<br />
          {{ $data->created_at }}</td>
                <td>
                  {{ $data->tanggal_deal ? : $data->Tanggal_Call }}<br />
                  {{ $data->tipe_transaksi ? : $data->Status_Inbox }}<br />
                  {{ $data->jenis_layanan ? : $data->Status_Webcare }}
                </td>
                <td>
                  <span class="label label-warning">{{ $data->uraian ? : 'TIM MBSP' }}</span><br />
                  {{ $data->sales }}</td>
                <td>
                  <b>{{ strtoupper($data->nama_pelanggan ? : $data->Nama) }}</b><br />
                  {{ $data->no_internet ? : $data->NO_INTERNET }}<br />
                  {{ $data->no_telp ? : $data->ND }}</td>
				        <td>
                  <b>Kord. Pel : </b>{{ $data->koor_pel ? : 'BELUM ADA' }}<br />
                  <b>Kord. ODP : </b>{{ $data->koor_odp ? : 'BELUM ADA' }}</td>
                <td>
                  {{ $data->alamat ? : $data->Alamat_Instalasi }}<br />
                  <b>PIC : </b>{{ $data->pic ? : $data->ND }}
                </td>
                <td>
                  <b>Paket : </b>{{ $data->paket_deal ? : 'MBSP' }}<br />
                  <b>Harga : </b>{{ $data->harga_deal ? : 'MBSP' }}<br />
                  <b>KContact : </b><br />
                  DSHR;PTTA;{{ $data->nik_sales }};{{ $data->pic }};{{ strtoupper($data->nama_pelanggan) }};{{ $data->harga_deal }};{{ $data->paket_deal }};{{ $data->kodefikasi }}</td>

            @endforeach
          </tbody>
        </table>
      </div>
