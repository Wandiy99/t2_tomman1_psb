@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
   UPLOAD TRANSAKSI DSHR
    </a>
  </h3>
  
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    <input type="hidden" name="dshr" value="[]" /> 
    <div class="form-group">
      <input type="file" id="tes" class="form-control"/>
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <div id="jumlah"></div>
    <div class="list-group"></div>
    
  </form>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

  <script src="/bower_components/excel/zip/WebContent/zip.js"></script>
  <script src="/bower_components/excel/async-master/dist/async.js"></script>
  <script src="/bower_components/excel/underscore.js"></script>
  <script src="/bower_components/excel/xlsxParser.js"></script>
  <script>
    $(function() {
      var dshr = [];
      $("#tes").change(function(e){
        dshr = [];
        $(".loading").css({"visibility":"visible"});
        $(".removable").remove();
        var file = document.getElementById('tes').files[0];
        zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
        xlsxParser.parse(file).then(function(data) {
          var count = 0;
          for(var i=1;i<data.length;i++){
            var e_tgl_inp = data[i][1],e_korlap = data[i][2], e_nik = data[i][3], e_sales = data[i][4], e_nama = data[i][5], e_speedy = data[i][6]
            , e_telp = data[i][7], e_jenis = data[i][8]
            , e_email = data[i][9]
            , e_kcontact = data[i][10]
            , e_pic = data[i][11]
            , e_alamat = data[i][12]
            , e_tgl_status = data[i][13]
            , e_status = data[i][14]
            , e_odp = data[i][15]
            , e_sc = data[i][16]
            , e_harga = data[i][17]
            , e_paket = data[i][18]
            , e_keterangan = data[i][19]
            , e_kodefikasi = data[i][20]
            , e_koor_pel = data[i][21]
            , e_koor_odp = data[i][22]
            , e_kendala = data[i][23]
            , e_history = data[i][24];
            console.log(e_tgl_inp);            
            dshr.push({tgl_inp: e_tgl_inp, korlap : e_korlap, nik: e_nik, sales : e_sales, nama : e_nama, speedy : e_speedy, email : e_email, pic : e_pic, tgl_status : e_tgl_status, odp : e_odp, sc : e_sc, harga : e_harga, paket : e_paket, keterangan : e_keterangan, kodefikasi : e_kodefikasi, koor_pel : e_koor_pel, koor_odp : e_koor_odp, kendala : e_kendala, history : e_history, alamat : e_alamat, status : e_status, jenis : e_jenis, telp : e_telp});
            $(".list-group").append("<div class='list-group-item removable'><span>" + e_tgl_inp + "<span><br/><span>" + e_korlap + "<span><br/><span>" + e_nik + "<span><br/><span>" + e_sales + "<span><br/><span>" + e_nama + "<span><br/><span>" + e_speedy + "<span><br/><span>" + e_telp + "<span><br/><span>" + e_jenis + "<span><br/><span>" + e_email + "<span><br/><span>" + e_kcontact + "<span><br/><span>" + e_pic + "<span><br/><span>" + e_tgl_status + "<span><br/><span>" + e_status + "<span><br/><span>" + e_odp + "<span><br/><span>" + e_sc + "<span><br/><span>" + e_harga + "<span><br/><span>" + e_paket + "<span><br/><span>" + e_keterangan + "<span><br/><span>" + e_kodefikasi + "<span><br/><span>" + e_koor_pel + "<span><br/><span>" + e_koor_odp + "<span><br/><span>" + e_kendala + "<span><br/><span>" + e_history + "<span><br/>");
            count++;
          }
          
          $("#jumlah").append("<span class=removable>Transaksi Found : " + count + "</span>");
          $(".loading").css({"visibility":"hidden"});
        }, function(err) {
          alert.log('error', err);
        });
      })
      $('#submit-form').submit(function() {
        $('input[name=dshr]').val(JSON.stringify(dshr));
      });    
	  })
  </script>
@endsection
