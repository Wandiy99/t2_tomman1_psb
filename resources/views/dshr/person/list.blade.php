@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
   List Personel DSHR
  </h3>
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>NIK</th>
        <th>NAMA</th>
        <th>CATATAN</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)
      <tr>
        <td>{{ ++$no }}</td>
        <td><span>{{ $data->id_karyawan }}</span><br/>
        <span>{{ $data->nama }}</span><br/>
        <span>{{ $data->catatan }}</span><br/>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
@endsection
