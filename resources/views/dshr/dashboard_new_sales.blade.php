@extends('layout')

@section('header')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection

@section('content')
@include('partial.alerts')

<div class="white-box">
    <h3 class="box-title">
        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target=".btn_input_modal">
            <span class="glyphicon glyphicon-plus"></span>
        </button>
        &nbsp;
        New Sales {{ date('Y') }}
    </h3>

    <div class="modal fade btn_input_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Data Transaksi</h4>
                </div>
                <form action="/dshr/new-sales/save-input" method="POST" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="new">
                    <input type="hidden" name="unix_id" value="new">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nomor KTP</label>
                                    <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" placeholder="Nomor Identitas KTP" name="no_ktp" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nomor NPWP</label>
                                    <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" placeholder="Nomor NPWP" name="no_npwp" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nama Pemohon</label>
                                    <input type="text" class="form-control" placeholder="Nama Pemohon" name="nama_pemohon" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nama Tagihan</label>
                                    <input type="text" class="form-control" placeholder="Nama Tagihan" name="nama_tagihan" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" class="form-control" placeholder="Email" name="email"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">PIC Pelanggan</label>
                                    <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" placeholder="PIC Pelanggan" name="pic_pelanggan" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tanggal Terbit KTP</label>
                                    <input type="text" class="form-control input-tgl" placeholder="Tanggal Terbit KTP" name="tgl_terbit_ktp" readonly required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Ibu Kandung</label>
                                    <input type="text" class="form-control" placeholder="Ibu Kandung" name="ibu_kandung" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tanggal Lahir</label>
                                    <input type="text" class="form-control input-tgl" placeholder="Tanggal Lahir" name="tgl_lahir" readonly required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Alamat Lengkap</label>
                                    <textarea class="form-control" rows="4" name="alamat_lengkap" placeholder="Alamat Lengkap" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">STO</label>
                                    <input type="text" class="form-control" placeholder="STO" id="input-sto" name="sto" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Kelurahan</label>
                                    <input type="text" class="form-control" placeholder="Kelurahan" name="kelurahan" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Koordinat Pelanggan</label>
                                    <input type="text" class="form-control" placeholder="Koordinat Pelanggan" name="koordinat_pelanggan" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">ODP</label>
                                    <input type="text" class="form-control" placeholder="Nama ODP" name="nama_odp" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Koordinat ODP</label>
                                    <input type="text" class="form-control" placeholder="Koordinat ODP" name="koordinat_odp" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Jenis Layanan</label>
                                    <input type="text" class="form-control" placeholder="Jenis Layanan" id="input-jenis-layanan" name="jenis_layanan" required/>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="form-group row text-center">
                                    <label class="control-label">Foto Lokasi Pelanggan</label>
                                    <input type="file" class="dropify" id="input-file-now" name="lokasi_pelanggan" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-rounded btn-success waves-effect text-left">Submit</button>
                        <button type="button" class="btn btn-rounded btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <ul class="nav customtab nav-tabs" role="tablist">
        <li role="presentation" class="nav-item">
            <a href="#dispatch_today" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                <span class="visible-xs">
                    <i class="ti-dashboard"></i>
                    &nbsp; Today
                </span>
                <span class="hidden-xs"> Today</span>
            </a>
        </li>
        <li role="presentation" class="nav-item">
            <a href="#belum_terdispatch" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">
                    <i class="ti-archive"></i>
                    &nbsp; Belum Terdispatch
                </span>
                <span class="hidden-xs">Belum Terdispatch</span>
            </a>
        </li>
        <li role="presentation" class="nav-item">
            <a href="#terdispatch" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs">
                    <i class="ti-direction-alt"></i>
                    &nbsp; Terdispatch
                </span>
                <span class="hidden-xs">Terdispatch</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="dispatch_today">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable align-middle text-center">
                        <thead>
                            <tr>
                                <th class="align-middle text-center" rowspan="2">Datel</th>
                                <th class="align-middle text-center" colspan="5">Status Laporan</th>
                                <th class="align-middle text-center" rowspan="2">Total</th>
                            </tr>
                            <tr>
                                <th class="align-middle text-center">NP</th>
                                <th class="align-middle text-center">OGP</th>
                                <th class="align-middle text-center">OK<br>TARIK</th>
                                <th class="align-middle text-center">KP</th>
                                <th class="align-middle text-center">KT</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dashboard_laporan as $dl_k => $dl_v)
                            <tr>
                                <td>{{ $dl_v->datel }}</td>
                                <td>{{ $dl_v->status_np }}</td>
                                <td>{{ $dl_v->status_ogp }}</td>
                                <td>{{ $dl_v->status_hr }}</td>
                                <td>{{ $dl_v->status_kp }}</td>
                                <td>{{ $dl_v->status_kt }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-5 pull-right">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable align-middle text-center">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">Datel</th>
                                <th class="align-middle text-center">Belum Terdispatch</th>
                                <th class="align-middle text-center">Sudah Terdispatch</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dashboard_dispatch as $dd_k => $dd_v)
                            <tr>
                                <td>{{ $dd_v->datel }}</td>
                                <td>{{ $dd_v->undispatch }}</td>
                                <td>{{ $dd_v->ondispatch }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="belum_terdispatch">
            <div class="table-responsive">
                <table class="display nowrap table_data align-middle text-center" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Unix ID</th>
                            <th>MYIR</th>
                            <th>SC</th>
                            <th>Nama Pemohon</th>
                            <th>Nama Tagihan</th>
                            <th>Email</th>
                            <th>Pic Pelanggan</th>
                            <th>Tgl Terbit KTP</th>
                            <th>Ibu Kandung</th>
                            <th>Tgl Akhir</th>
                            <th>NPWP</th>
                            <th>Alamat Detail</th>
                            <th>Datel</th>
                            <th>STO</th>
                            <th>Kelurahan</th>
                            <th>Koordinat Pelanggan</th>
                            <th>ODP</th>
                            <th>Koordinat ODP</th>
                            <th>Jenis Layanan</th>
                            <th>Sales ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_belum_terdispatch as $dbt_k => $dbt_v)
                        <tr>
                            <td>{{ ++$dbt_k }}</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-rounded btn-info">Dispatch</button>
                            </td>
                            <td>MYIX-{{ $dbt_v->new_sales_unix }}</td>
                            <td>{{ $dbt_v->myir }}</td>
                            <td>{{ $dbt_v->sc }}</td>
                            <td>{{ $dbt_v->nama_pemohon }}</td>
                            <td>{{ $dbt_v->nama_tagihan }}</td>
                            <td>{{ $dbt_v->email }}</td>
                            <td>{{ $dbt_v->pic_pelanggan }}</td>
                            <td>{{ $dbt_v->tgl_terbit_ktp }}</td>
                            <td>{{ $dbt_v->ibu_kandung }}</td>
                            <td>{{ $dbt_v->tgl_lahir }}</td>
                            <td>{{ $dbt_v->no_npwp }}</td>
                            <td>{{ $dbt_v->alamat_lengkap }}</td>
                            <td>{{ $dbt_v->datel }}</td>
                            <td>{{ $dbt_v->sto }}</td>
                            <td>{{ $dbt_v->kelurahan }}</td>
                            <td>{{ $dbt_v->koordinat_pelanggan }}</td>
                            <td>{{ $dbt_v->odp_nama }}</td>
                            <td>{{ $dbt_v->koordinat_odp }}</td>
                            <td>{{ $dbt_v->nama_jenis_layanan }}</td>
                            <td>{{ $dbt_v->created_by }} - {{ $dbt_v->nama_sales }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="terdispatch">
            <div class="table-responsive">
                <table class="display nowrap table_data" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Unix ID</th>
                            <th>MYIR</th>
                            <th>SC</th>
                            <th>Nama Pemohon</th>
                            <th>Nama Tagihan</th>
                            <th>Email</th>
                            <th>Pic Pelanggan</th>
                            <th>Tgl Terbit KTP</th>
                            <th>Ibu Kandung</th>
                            <th>Tgl Akhir</th>
                            <th>NPWP</th>
                            <th>Alamat Detail</th>
                            <th>Datel</th>
                            <th>STO</th>
                            <th>Kelurahan</th>
                            <th>Koordinat Pelanggan</th>
                            <th>ODP</th>
                            <th>Koordinat ODP</th>
                            <th>Jenis Layanan</th>
                            <th>Sales ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_sudah_terdispatch as $dst_k => $dst_v)
                        <tr>
                            <td>{{ ++$dst_k }}</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-rounded btn-info">Re-Dispatch</button>
                            </td>
                            <td>MYIX-{{ $dst_v->new_sales_unix }}</td>
                            <td>{{ $dst_v->myir }}</td>
                            <td>{{ $dst_v->sc }}</td>
                            <td>{{ $dst_v->nama_pemohon }}</td>
                            <td>{{ $dst_v->nama_tagihan }}</td>
                            <td>{{ $dst_v->email }}</td>
                            <td>{{ $dst_v->pic_pelanggan }}</td>
                            <td>{{ $dst_v->tgl_terbit_ktp }}</td>
                            <td>{{ $dst_v->ibu_kandung }}</td>
                            <td>{{ $dst_v->tgl_lahir }}</td>
                            <td>{{ $dst_v->no_npwp }}</td>
                            <td>{{ $dst_v->alamat_lengkap }}</td>
                            <td>{{ $dst_v->datel }}</td>
                            <td>{{ $dst_v->sto }}</td>
                            <td>{{ $dst_v->kelurahan }}</td>
                            <td>{{ $dst_v->koordinat_pelanggan }}</td>
                            <td>{{ $dst_v->odp_nama }}</td>
                            <td>{{ $dst_v->koordinat_odp }}</td>
                            <td>{{ $dst_v->nama_jenis_layanan }}</td>
                            <td>{{ $dst_v->created_by }} - {{ $dst_v->nama_sales }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

@endsection

@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.table_data').DataTable();
        $('.dropify').dropify();

        $('.input-tgl').datepicker({
            format: 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
        }).on('changeDate', function(e) {
            $(this).datepicker('hide');
        });

        var sto =  {!! json_encode($get_sto) !!};
        $('#input-sto').select2({
            data: sto,
            placeholder: 'Pilih STO'
        });

        var jenis_layanan =  {!! json_encode($get_jenis_layanan) !!};
        $('#input-jenis-layanan').select2({
            data: jenis_layanan,
            placeholder: 'Pilih Jenis Layanan'
        });
        
        $("input[type=file]").change(function () {
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                $(inputEl).parent().find("img").attr("src", e.target.result);
                };
                reader.readAsDataURL(inputEl.files[0]);
            }
        });
    
        $(".input-photos").on("click", "button", function () {
            $(this).parent().find("input[type=file]").click();
        });
    });
</script>
@endsection