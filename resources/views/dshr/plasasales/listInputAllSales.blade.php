@extends('layout')

@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Plasa - Sales</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">	
						@if ($level==2 || $level==45 || $level==59)
							<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
						@elseif ($level==2 || $level==45 || $level==61)
							<a href="/dshr/plasa-sales/sales/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
						@endif
					</div>
					
					<input type="hidden" name="tglFilter" id="tglFilter" value="{{ $tglAll }}">
					
					<ul class="nav nav-tabs" style="width: 100%;">
					  <li class="active"><a href="/dshr/plasa-sales/list-wo-by-sales">Transaksi</a></li>	
					  <li><a href="/dshr/plasa-sales/pencarian">Pencarian</a></li>							
					</ul>

					<br>
					
					<div id="isi">
						<form method="get">
							<input type="text" name="tglAll" id="tglAll" value="{{ $tglAll }}">
							<input type="submit" value="Filter" class="btn btn-primary">
						</form><br>

						<div class="row">
							<div class="col-md-2">	
								<table class="table table-bordered">
									<tr>
										<th rowspan="2" style="text-align: center; text">Today</th>
										<th colspan="5" style="text-align: center">This Month</th>
									</tr>

									<tr>
										<th>NEED PROGRESS</th>
										<th>KP</th>
										<th>KT</th>
										<th>UP</th>
										<th>TOTAL</th>
									</tr>
				
									<tr>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-today/{{ $tglAll }}" id="woToday">{{ $woHi }}</a></td>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-progress/{{ $tglAll }}/NEED" class="woProgress">{{ $jumlahProgress[0]->jmlNeed ?: '0' }}</a></td>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-progress/{{ $tglAll }}/KP" class="woProgress">{{ $jumlahProgress[0]->jmlKp ?: '0' }}</a></td>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-progress/{{ $tglAll }}/KT" class="woProgress">{{ $jumlahProgress[0]->jmlKt ?: '0' }}</a></td>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-progress/{{ $tglAll }}/UP" class="woProgress">{{ $jumlahProgress[0]->jmlUp ?: '0' }}</a></td>
										<td><a href="/dshr/plasa-sales/list-wo-by-sales/detail-progress/{{ $tglAll }}/TOTAL" class="woProgress">{{ $jumlahProgress[0]->jmlNeed + $jumlahProgress[0]->jmlKp + $jumlahProgress[0]->jmlKt + $jumlahProgress[0]->jmlUp }}</a></td>
									</tr>
									
								</table>
							</div>							
						</div>
									
					</div>
						
					<div id="isiKlik">
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  	<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
	<script>
		$(function(){
			var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};
		    
		    $('#tglAll').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		    $('#woToday').on('click', function(e){
		    	e.preventDefault();
		    	var me  = $(this),
		    		url = me.attr('href');

		    	$.ajax({
		    		url: url,
		    		dataType: 'html',
		    		success: function(data){
		    			console.log(data);
		    			$('#isiKlik').html(data);
		    		}
		    	})
		    });

		    $('.woProgress').on('click', function(e){
		    	e.preventDefault();
		    	var me  = $(this),
		    		url = me.attr('href');
		    		
		    	$.ajax({
		    		url: url,
		    		dataType: 'html',
		    		success: function(data){
		    			console.log(data);
		    			$('#isiKlik').html(data);
		    		}
		    	})
		    	
		    })
		})
	</script>
@endsection