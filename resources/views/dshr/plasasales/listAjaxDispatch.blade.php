<table class="table table-bordered">
	<tr>	
		<th>No</th>
		<th>Tgl Order</th>
		<th>Tgl Dispatch</th>
		<th>Myir / SC</th>
		<th>SC Sinkron</th>v
		<th>Pelanggaan</th>
		<th>Tanggal Lahir</th>
		<th>Alamat</th>
		<th>STO</th>
		<th>ODP</th>
		<th>Layanan</th>
		<th>PSB</th>
		<th>Paket Indihome</th>
		<th>Paket Sales</th>
		<th>Email</th>
		<th>Kcontack</th>
		<th>Ket Dispatch</th>
		<th>Regu</th>
		<th>Sales ID / Kode Plasa</th>		
		<th>Status Laporan</th>
		<th>Ket</th>
	</tr>

	@foreach($getData as $no=>$data)
		<tr>	
			<td>{{ ++$no }}</td>
			<td>{{ $data->orderDate }}</td>
			<td>{{ $data->tglDispatch }}</td>
			<td>{{ $data->myirInput }}</td>
			<td>
				@if($data->dispatch==1 && $data->ket==0 )
					<a href="/dshr/plasa-sales/input-sc/{{ $data->myirInput }}" class="btn btn-primary btn-sm" id="add_sc">Add SC</a><br>
				@endif
				{{ $data->Ndem ?: '-'}}
			</td>
			<td>{{ $data->customer }}</td>
			<td>{{ $data->tgl_lahir ?: ' - ' }}</td>
			<td>{{ $data->alamatLengkap }}</td>
			<td>{{ $data->sto_wo }}</td>
			<td>{{ $data->namaOdp }}</td>
			<td>{{ $data->layanan }}</td>
			<td>{{ $data->psb ?: '-'}}</td>
			<td>{{ $data->paket_harga ?: '-' }}</td>
			<td>{{ $data->paket_sales ?: '-' }}</td>
			<td>{{ $data->email ?: '-' }}</td>
			<td>{{ $data->kcontack ?: '-' }}</td>

			<td>
				@if($data->ket_input==0)	
					<a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myirInput }}" class="btn btn-primary btn-sm">Re-Dispatch</a>
				@else
					<a href="/dshr/plasa-sales/dispatch/sales/{{ $data->myirInput }}" class="btn btn-primary btn-sm">Re-Dispatch</a>
				@endif
			</td>

			<td>{{ $data->uraian }} - {{ $data->title }}</td>
			<td>{{ $data->sales_id ?: $data->created_by}}</td>
			<td>{{ $data->laporan_status ?: 'NEED PROGRESS' }}</td>

			@if($data->ket_input==0)
				<td>
					PLASA
					<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_psbWo }}" target="_blank" class="btn btn-info btn-sm">
						<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
					</a>
				</td>
			@else
				<td>SALES</td>
			@endif
		</tr>
	@endforeach
</table>