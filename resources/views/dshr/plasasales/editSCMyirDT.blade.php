@extends('layout')
@section('content')

<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Edit No. SC-{{ $data->Ndem }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>NO. SC BARU</label>
						<input type="text" name="Ndem" class="form-control" value="{{ $data->Ndem ?: old('Ndem') }}" required>
						{!! $errors->first('Ndem','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>ORDER ID</label>
						<input type="text" name="id" class="form-control" value="{{ $data->id ?: old('id') }}" readonly>
						{!! $errors->first('id','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>ID TIM</label>
						<input type="text" name="id_regu" class="form-control" value="{{ $data->id_regu ?: old('id_regu') }}" readonly>
						{!! $errors->first('id_regu','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>				

			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection