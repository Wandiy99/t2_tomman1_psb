@extends('layout')
@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Decline Order</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
					</div>

					<ul class="nav nav-tabs" style="width: 100%;">
					  <li><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Today</a></li>
            		  <li><a href="/qc1_list/ALL">QC 1</a></li>
					  <li><a href="/belum_dispatch/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Belum Terdispatch</a></li>
					  <li><a href="/dshr/plasa-sales/list-dispatch/{{ date('Y-m') }}">Terdispatch</a></li>
					  <li @if ($active == "list_decline") class="active" @endif><a href="/list_decline/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Decline</a></li>
					  <li><a href="/list_non_onecall/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}"> List SC Non-Onecall & SC belum sinkron (BETA)</a></li>
					  <li><a href="/belum_dispatch/plasa/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Order Plasa</a></li>
					</ul>

					<br>

					<div id="isi">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>ID Myir</th>
								<th>Tgl Onecall</th>
								<th>Tgl SCBE</th>
								<th>Tgl Appr</th>
								<th>Tgl Descline</th>
								<th>Myir</th>
								<th>SC</th>
								<th>Pelanggaan</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>STO</th>
								<th>ODP</th>
								<th>Layanan</th>
								<th>PSB</th>
								<th>Paket Indihome</th>
								<th>Ket Dispatch</th>
								<th>Decline Oleh</th>
								<th>Sales ID / Kode Plasa</th>
								<th>Email</th>
								<th>Kcontack</th>
								<th>Ket</th>
								<th>Return</th>
							</tr>

							@foreach($getData as $no=>$data)
								<tr>
									<td>{{ ++$no }}</td>
									<td>
										<a href="/dshr/plasa-sales/delete/{{ $data->myir }}/{{ $data->id_wo }}" class="label label-danger">D E L E T E</a>
									</td>
									</td></td>
									<td>{{ $data->orderDate }}</td>
									<td>{{ $data->ORDER_DATE }}</td>
									<td>{{ $data->approve_date }}</td>
									<td>{{ $data->decline_date }}</td>
									<td>{{ $data->myir }}</td>
									<td>{{ $data->sc ? : '-' }}</td>
									<td>{{ $data->customer }}</td>
									<td>{{ $data->tgl_lahir ?: ' - ' }}</td>
									<td>{{ $data->alamatLengkap }}</td>
									<td>{{ $data->sto }}</td>
									<td>{{ $data->namaOdp }}</td>
									<td>{{ $data->layanan }}</td>
									<td>{{ $data->psb ?: '-'}}</td>
									<td>{{ $data->paket_harga ?: '-' }}</td>
									@if ($active == "dispatch")
									@if($data->ket_input==0)
										@if ($data->sc<>"")
										<td><a href="/dispatch/{{ $data->sc }}" class="btn btn-primary btn-sm">Dispatch</a><br />Approved by<br />{{ $data->approve_by }}</td>
										@else
										<td><a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch</a><br />Approved by<br />{{ $data->approve_by }}</td>
										@endif
									@elseif ($data->ket_input==1)
									@if ($data->sc<>"")
										<td><a href="/dispatch/{{ $data->sc }}" class="btn btn-primary btn-sm">Dispatch</a><br />Approved by<br />{{ $data->approve_by }}</td>
										@else
										<td><a href="/dshr/plasa-sales/dispatch/plasa/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch</a><br />Approved by<br />{{ $data->approve_by }}</td>
										@endif
									@else
										@if($data->myir<>'')
											<td><a href="/dshr/plasa-sales/dispatch/sales-onecall/{{ $data->myir }}" class="btn btn-primary btn-sm">Dispatch </a><br />Approved by <br />{{ $data->approve_by }}</td>
										@else
											<td></td>
										@endif
									@endif
									@else
										<td>
											<a href="/dshr/plasa-sales/approve/{{ $data->myir }}" class="btn btn-primary btn-sm">Approve</a>
											<br/><br/>
											<a href="/dshr/plasa-sales/decline/{{ $data->myir }}" class="btn btn-danger btn-sm">Decline</a>
										</td>
									@endif
									<td>{{ $data->decline_by }}</td>
									<td>{{ $data->sales_id ?: $data->created_by}}</td>
									<td>{{ $data->email ?: '-' }}</td>
									<td>{{ $data->kcontack ?: '-' }}</td>

									@if($data->ket_input==0)
										<td>
											PLASA
											<a href="/dshr/plasa-sales/cetak-plasa/{{ $data->id_wo }}" target="_blank" class="btn btn-info btn-sm">
												<i class="glyphicon glyphicon-print" aria-hidden="true"></i>
											</a>
										</td>
									@elseif($data->ket_input==1)
										<td>SALES</td>
									@else
										<td>SALES ONECALL</td>
									@endif
									<td><a href="/dshr/plasa-sales/return/{{ $data->myir }}" class="btn btn-danger btn-sm">RETURN</a></td>
								</tr>
							@endforeach
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection