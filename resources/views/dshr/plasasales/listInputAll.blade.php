@extends('layout')

@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Plasa - Sales</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
					</div>
<!--
					<h4>Filter :</h4>
					<input type="text" name="filter" id="filter" value="ALL" disabled><br><br>
					<input type="hidden" name="tglFilter" id="tglFilter" value="{{ $tglAll }}"> -->

					<ul class="nav nav-tabs" style="width: 100%;">
            <li ><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Today</a></li>
            <li @if ($active == "validasi") class="active" @endif ><a href="/qc1_list/ALL">QC 1</a></li>
					  <li @if ($active == "dispatch") class="active" @endif ><a href="/belum_dispatch/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Belum Terdispatch</a></li>
					  <li><a href="/dshr/plasa-sales/list-dispatch/{{ date('Y-m') }}">Terdispatch</a></li>
					  <li><a href="/list_decline/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Decline</a></li>
					  <li><a href="/list_non_onecall/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}"> List SC Non-Onecall & SC belum sinkron (BETA)</a></li>
					  <li @if ($active == "undispatch") class="active" @endif ><a href="/belum_dispatch/plasa/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Order Plasa</a></li>
					</ul>

					<br>

					<div id="isi">
						<form method="get">
							<input type="date" name="tglAwal" id="tglAwal" value="{{ $tglAwal }}">
							<input type="date" name="tglAkhir" id="tglAkhir" value="{{ $tglAkhir }}">
							<input type="button" id="bt_direct" value="Filter" class="btn btn-primary">
						</form><br>

						<div class="row">
							<div class="col-md-6">
                <table class="table">
                  <tr>
                    <td>STO</td>
                    <td align=center>QC1</td>
                    <td align=center>BLM DISPATCH</td>
                    <td align=center>DISPATCH</td>
                    <td align=center>CLOSE</td>
                  </tr>
                  <?php
                    $total_pivot = 0;
                    $total_belum_dispatch = 0;
                    $total_sudah_dispatch = 0;
                    $total_close_dispatch = 0;
                  ?>
                  @foreach ($getSto as $result)
                  <?php
                    $total_pivot += $pivot[$result->sto];
                    $total_belum_dispatch += $belum_dispatch[$result->sto];
                    $total_sudah_dispatch += $sudah_dispatch[$result->sto];
                    $total_close_dispatch += $close_dispatch[$result->sto];
                  ?>
                  <tr>
                    <td>{{ $result->sto }}</td>
                    <td align=center><a href="/qc1_list/{{ $result->sto }}/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">{{ $pivot[$result->sto] }}</a></td>
                    <td align=center><a href="/belum_dispatch/{{ $result->sto }}/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">{{ $belum_dispatch[$result->sto] }}</a></td>
                    <td align=center>{{ $sudah_dispatch[$result->sto] }}</td>
                    <td align=center>{{ $close_dispatch[$result->sto] }}</td>
                  </tr>
                  @endforeach
                  <tr>
                    <td>TOTAL</td>
                    <td align=center><a href="/qc1_list/ALL">{{ $total_pivot }}</a></td>
                    <td align=center><a href="/belum_dispatch/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">{{ $total_belum_dispatch }}</a></td>
                    <td align=center>{{ $total_sudah_dispatch }}</td>
                    <td align=center>{{ $total_close_dispatch }}</td>
                  </tr>
							  </table>
              </div>
						</div>


					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('plugins')
	<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  	<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
	<!-- <script src="/js/jquery.min.js"></script>
  	<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script> -->
	<script>
		$(function(){
      $('#bt_direct').click(function(){
        window.location.href = "/dshr/plasa-sales/list-wo-by-sales/ALL/"+$('#tglAwal').val()+"/"+$('#tglAkhir').val();
      });
			var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

		    $('#tglAwal').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		    $('#tglAkhir').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });
		})
	</script>
@endsection
