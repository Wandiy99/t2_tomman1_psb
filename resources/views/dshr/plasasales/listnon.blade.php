@extends('layout')
@section('content')
  @include('partial.alerts')
	<div class="panel panel-primary">
		<div class="panel-heading">List Plasa - Sales</div>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<a href="/dshr/plasa-sales/plasa/form" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span></a>
					</div>

					<!-- <h4>Filter :</h4>
					<input type="text" name="filter" id="filter" value="ALL">
					<input type="hidden" name="tglFilter" id="tglFilter" value="{{ $tgl }}">
					<form method="get">
							<input type="text" name="tglAwal" id="tglAwal" value="{{ $tglAwal }}">
							<input type="text" name="tglAkhirr" id="tglAkhirr" value="{{ $tglAkhirr }}">
							<input type="button" id="bt_direct" value="Filter" class="btn btn-primary">
					</form><br> -->


					<ul class="nav nav-tabs" style="width: 100%;">
					  <li ><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Today</a></li>
            		  <li @if ($active == "validasi") class="active" @endif ><a href="/qc1_list/ALL">QC 1</a></li>
					  <li @if ($active == "dispatch") class="active" @endif ><a href="/belum_dispatch/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Belum Terdispatch</a></li>
					  <li><a href="/dshr/plasa-sales/list-dispatch/{{ date('Y-m') }}">Terdispatch</a></li>
					  <li><a href="/list_decline/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Decline</a></li>
                      <li><a href="/list_non_onecall/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}"> List SC Non-Onecall & SC belum sinkron (BETA)</a></li>
					  <li @if ($active == "undispatch") class="active" @endif ><a href="/belum_dispatch/plasa/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">Order Plasa</a></li>
					</ul>

					<br>

					<div id="isi">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Tgl ORDER</th>
								<th>Tgl SCBE</th>
								<th>SC</th>
								<th>Pelanggaan</th>
								<th>Alamat</th>
								<th>STO</th>
								<th>Alpro</th>
								<th>Jenis Psb</th>
								<th>Order Status</th>
								<th>Ket Dispatch</th>
								<th>Kcontact</th>

							</tr>

							@foreach($getData as $no=>$data)
								<tr>
									<td>{{ ++$no }}</td>
									<td>{{ $data->orderDate }}</td>
									<td>{{ $data->tglscbe }}</td>
									<td>{{$data->orderId ? :  $data->myir }}</td>
									<td>{{ $data->orderName }}</td>
									<td>{{ $data->orderAddr }}</td>
									<td>{{ $data->sto }}</td>
									<td>{{ $data->alproname }}</td>
									<td>{{ $data->jenisPsb }}</td>
									<td>{{ $data->orderStatus }}</td>
									@if ($active == "non")
										<td><a href="/dispatch/{{ $data->orderId }}" class="btn btn-primary btn-sm">Dispatch</a></td>
									@endif
									<td>{{ $data->kcontact ?: '-' }}</td>

								</tr>
							@endforeach
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection
@section('plugins')
	
	<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  	<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
	  <script>
		$(function(){
			var dataFilter = [
				{'id':'ALL', 'text' : 'ALL'},
				{'id':'plasa', 'text' : 'Plasa'},
				{'id':'sales', 'text' : 'Sales'},
			];

			$('#filter').select2({
				data: dataFilter,
				placeholder: 'Pilih Filter'
			});

			$('#bt_direct').click(function(){
       			 window.location.href = "/belum_dispatch/ALL/"+$('#tglAwal').val()+"/"+$('#tglAkhirr').val();
      			});
			var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

		    $('#tglAwal').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

		    $('#tglAkhirr').datepicker(day).on('changeDate', function(e){
		        $(this).datepicker('hide');
		    });

			$('#filter').on('click', function(){
				var nilai = $('#filter').val(),
					tgl   = $('#tglFilter').val(),
					url   = "/dshr/plasa-sales/list-belum-dispatch/ajax/"+nilai+'/'+tgl;

				$.ajax({
					url: url,
					dataType: 'HTML',
					success : function(data){
						$('#isi').html(data)
					}
				})

			})
		})
	</script>
@endsection
