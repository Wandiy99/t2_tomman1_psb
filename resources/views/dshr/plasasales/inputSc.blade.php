@extends('layout')
@section('content')

<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Add No. SC,  Myir - {{ $data->myir }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>No. SC</label>
						@if($data->ket==1)
							<input type="text" name="sc" class="form-control" value="{{ $data->sc }}" readonly>
						@else
							<input type="text" name="sc" class="form-control" value="{{ $data->sc ?: old('sc') }}">
						@endif
						{!! $errors->first('sc','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>No. Internet</label>
						<input type="text" name="no_inet" class="form-control" value="{{ $data->no_internet ?: old('no_inet') }}">
						{!! $errors->first('no_inet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>No. Telpon</label>
						<input type="text" name="no_telp" class="form-control" value="{{ $data->no_telp ?: old('no_telp') }}">
						{!! $errors->first('no_telp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
