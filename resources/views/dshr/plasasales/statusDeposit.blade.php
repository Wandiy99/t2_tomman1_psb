@extends('layout')
@section('content')
@include('partial.alerts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-default">
		<div class="panel-heading">DEPOSIT MYIR-{{ $data->myir }}</div>
		<div class="panel-body">
			<div class="row">
                <div class="col-md-4">
					<div class="form-group">
						<label>ID</label>
						<input type="text" name="id" class="form-control" value="{{ $data->id ?: old('id') }}" readonly/>
						{!! $errors->first('id','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>MYIR</label>
						<input type="text" name="myir" class="form-control" value="{{ $data->myir ?: old('myir') }}" readonly/>
						{!! $errors->first('myir','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>SC</label>
						<input type="text" name="sc" class="form-control" value="{{ $data->sc ?: old('sc') }}" readonly/>
						{!! $errors->first('sc','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>CUSTOMER</label>
						<input type="text" name="customer" class="form-control" value="{{ $data->customer ?: old('customer') }}" readonly/>
						{!! $errors->first('customer','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>PIC PELANGGAN</label>
						<input type="text" name="picPelanggan" class="form-control" value="{{ $data->picPelanggan ?: old('picPelanggan') }}" readonly/>
						{!! $errors->first('picPelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>KCONTACT</label>
						<input type="text" name="kcontack" class="form-control" value="{{ $data->kcontack ?: old('kcontack') }}" readonly/>
						{!! $errors->first('kcontack','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>ALAMAT</label>
						<input type="text" name="alamatLengkap" class="form-control" value="{{ $data->alamatLengkap ?: old('alamatLengkap') }}" readonly/>
						{!! $errors->first('alamatLengkap','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>ND INET</label>
						<input type="text" name="no_internet" class="form-control" value="{{ $data->no_internet ?: old('no_internet') }}" readonly/>
						{!! $errors->first('no_internet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-4">
					<div class="form-group">
						<label>ORDER DATE</label>
						<input type="text" name="orderDate" class="form-control" value="{{ $data->orderDate ?: old('orderDate') }}" readonly/>
						{!! $errors->first('orderDate','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-6">
					<div class="form-group">
                        <label class="control-label" for="status_deposit">STATUS DEPOSIT</label>
                        <select class="input-status-deposit form-control" id="status_deposit" name="status_deposit">
                        <option value="{{ $data->status_deposit }}">{{ $data->status_deposit }}</option>
                        <option value="DONE"> DONE </option>
                        <option value="NOK"> NOK </option>
                        </select>
						{!! $errors->first('status_deposit','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

                <div class="col-md-6">
					<div class="form-group">
						<label>UPDATE BY</label>
						<input type="text" name="updated_deposit_by" class="form-control" value="{{ $data->updated_deposit_by ?: old('updated_deposit_by') }}" readonly/>
						{!! $errors->first('updated_deposit_by','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(function() {
$(".input-status-deposit").select2({
      placeholder: "Pilih Status Deposit",
      allowClear: true
      });
})
</script>
@endsection