@extends('layout')
@section('content')

<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Edit No. SC-{{ $data->sc }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>NO. SC BARU</label>
						<input type="text" name="sc" class="form-control" value="{{ $data->sc ?: old('sc') }}" required/>
						{!! $errors->first('sc','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NO. SC LAMA</label>
						<input type="text" name="sc_lama" class="form-control" value="{{ $data->sc_lama ?: old('sc_lama') }}" required/>
						{!! $errors->first('sc_lama','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NO. INTERNET</label>
						<input type="text" name="no_inet" class="form-control" value="{{ $data->no_internet ?: old('no_inet') }}">
						{!! $errors->first('no_inet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NO. TELEPON</label>
						<input type="text" name="no_telp" class="form-control" value="{{ $data->no_telp ?: old('no_telp') }}">
						{!! $errors->first('no_telp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection