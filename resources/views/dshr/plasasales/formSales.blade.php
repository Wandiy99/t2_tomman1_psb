@extends('layout')
@section('content')
@include('partial.alerts')

<ul class="nav nav-tabs">
  @if($level==2 || $level==45 || $level==15)
  	<li><a href="/dshr/plasa-sales/plasa/form">Plasa</a></li>
  @endif
  <li class="active"><a href="/dshr/plasa-sales/sales/form">Sales</a></li>
</ul>
<br>
<form method="GET">
    <div class="row">
      <div class="input-group col-sm-6">
          <input align="right" type="text" class="form-control inputSearch" placeholder="Search ODP with ND INET . . ." name="OdpByInet" id="OdpByInet" />
          <span class="input-group-btn">
              <button class="btn btn-default search">
                  <span class="glyphicon glyphicon-search"></span>
              </button>
          </span>
      </div>
    </div>
</form>
<br />
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Form Input Sales</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>No. MYIR / No. KTP</label>
						<input type="text" name="myir" class="form-control" maxlength="16" value="{{ old('myir') }}">
						{!! $errors->first('myir','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Nama Pelanggan</label>
						<input type="text" name="nama_pelanggan" class="form-control" value="{{ old('nama_pelanggan') }}">
						{!! $errors->first('nama_pelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>PIC Pelanggan</label>
						<input type="text" name="pic_pelanggan" class="form-control" value="{{ old('pic_pelanggan') }}">
						{!! $errors->first('pic_pelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" class="form-control" value="{{ old('email') }}" >
						{!! $errors->first('email','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Koordinat Pelanggan</label>
						<input type="text" name="koor_pelanggan" class="form-control" value="{{ old('koor_pelanggan') }}">
						{!! $errors->first('koor_pelanggan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>STO</label>
						<input type="text" name="sto" class="form-control" id="sto" value="{{ old('sto') }}">
						{!! $errors->first('sto','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Jenis Layanan</label>
						<input type="text" name="layanan" class="form-control" id="layanan" value="{{ old('layanan') }}">
						{!! $errors->first('layanan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Source PSB</label>
						<input type="text" name="psb" class="form-control" id="psb" value="{{ old('psb') }}">
						{!! $errors->first('psb','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Paket</label>
						<input type="text" name="paket" class="form-control" id="paket" value="{{ old('paket') }}">
						{!! $errors->first('paket','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				
				<div class="col-md-3">
					<div class="form-group">
						<label>ODP</label>
						<input type="text" name="namaOdp" class="form-control" id="odp" value="{{ $nama_odp }}">
						{!! $errors->first('namaOdp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Koordinat ODP</label>
						<input type="text" name="koordinatOdp" class="form-control" value="{{ $kor_odp }}">
						{!! $errors->first('koordinatOdp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Alamat Detail</label>
						<input type="text" name="alamat_detail" class="form-control" value="{{ old('alamat_detail') }}">
						{!! $errors->first('alamat_detail','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
			</div>

			<div class="row">	
				<div class="col-md-6">	
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">	
					</div>
				</div>
			</div>		
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Dokumentasi Foto</div>
		<div class="panel-body">
			<div class="row">	
					<div class="col-xs-2 col-md-1 text-center input-photos">
		                <img src="/image/placeholder.gif" alt="belum ada foto"><br>KTP + Pelanggan<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_ktp" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_ktp','<span class="label label-danger">:message</span>') !!}
					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="belum ada foto"><br>ODP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_odp" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_odp','<span class="label label-danger">:message</span>') !!}
					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
		                <img src="/image/placeholder.gif" alt="belum ada foto"><br>Rumah Pelanggan<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_rumah" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_rumah','<span class="label label-danger">:message</span>') !!}

					</div>
				</div>
			</div>
		</div>	
	</div>
</form>
@endsection
@section('plugins')
	<!-- <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" /> -->
	<!-- <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script> -->
	<script src="/js/jquery.min.js"></script>
  	<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
	<script src="/bower_components/select2/select2.min.js"></script>

	<script>
		$(function(){
			$('input[type=file]').change(function() {
		        console.log(this.name);
		        var inputEl = this;
		        if (inputEl.files && inputEl.files[0]) {
		         	$(inputEl).parent().find('input[type=text]').val(1);
		          	var reader = new FileReader();
		          	reader.onload = function(e) {
		            	$(inputEl).parent().find('img').attr('src', e.target.result);
		          	}
		        	reader.readAsDataURL(inputEl.files[0]);
			    }
		    });

		    $('.input-photos').on('click', 'button', function() {
		    	$(this).parent().find('input[type=file]').click();
		    });


		    // var day = {
		    //   	  	format: 'yyyy-mm-dd',
		    //     	viewMode: 0,
		    //    	 	minViewMode: 0
		    //   	};
		    // $('#tglOrder').datepicker(day).on('changeDate', function(e){
		    //     $(this).datepicker('hide');
		    // });

		    // $('#tglLahir').datepicker(day).on('changeDate', function(e){
		    //     $(this).datepicker('hide');
		    // });

		    var sto = <?=json_encode($getSto); ?>;
			$('#sto').select2({
				data: sto,
				placeholder: 'Pilih Sto'
			});

      		// $("#odp").inputmask("AAA-AAA-A{2,3}/999");

      		var datajenislayanan = [
		        {"id":"1PVOICE", "text":"<span class='label label-success'>1P</span> VOICE"},
		        {"id":"1PINET", "text":"<span class='label label-success'>1P</span> INET"},
		        {"id":"1PTV", "text":"<span class='label label-success'>1P</span> TV"},
		        {"id":"2PINETVOICE", "text":"<span class='label label-success'>2P</span> INET VOICE"},
		        {"id":"2PINETUSEETV", "text":"<span class='label label-success'>2P</span> INET USEETV"},
		        {"id":"3P", "text":"<span class='label label-success'>3P</span> VOICE INET USEETV"},
		        {"id":"AddServiceModify", "text":"AddService + Modify Paket"},
		      ];

		    var jenislayanan = function() {
			        return {
			          data: datajenislayanan,
			          placeholder: 'Input Jenis Layanan',
			          formatSelection: function(data) { return data.text },
			          formatResult: function(data) {
			            return  data.text;
			          }
			        }
			    }

		    $('#layanan').select2(jenislayanan());

		    var dataPsb = [
		    	{"id":"DTD","text":"DTD"},
		    	{"id":"DTDSTORE","text":"DTD STORE"},
		    	{"id":"OPENTABEL","text":"OPEN TABEL"},
		    	{"id":"DIHUBUNGI","text":"DIHUBUNGI"},
		    	{"id":"PLASA","text":"PLASA"},
		    	{"id":"SOSMED","text":"SOSMED"},
		    	{"id":"DOWNLINE","text":"DOWNLINE"},
		    ];

		    $('#psb').select2({
		    	data: dataPsb,
		    	placeholder: 'Pilh Source PSB'
		    });

		    var paket = <?=json_encode($getPaket); ?>;
		    $('#paket').select2({
		    	data: paket,
		    	placeholder: 'Pilih Paket Sales'
		    });
		})
	</script>
@endsection
