@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
.filterable {
    margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}
</style>
<h3>INBOX ORDER PLASA</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default filterable">
			<div class="panel-heading">
				INBOX {{ $datetime }}
			</div>
            <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div>
			<div class="table-responsive">
				<table class="table">
                    <thead>
					<tr class="filters">
						<th><input type="text" class="form-control" placeholder="#" disabled></th>
						<th><input type="text" class="form-control" placeholder="TIM"></th>
						<th><input type="text" class="form-control" placeholder="SEKTOR"></th>
						<th><input type="text" class="form-control" placeholder="MYIR"></th>
						<th><input type="text" class="form-control" placeholder="SC"></th>
						<th><input type="text" class="form-control" placeholder="STATUS"></th>
						<th><input type="text" class="form-control" placeholder="CATATAN TEKNISI"></th>
						<th><input type="text" class="form-control" placeholder="STATUS DEPOSIT"></th>
                        <th><input type="text" class="form-control" placeholder="KODE PLASA"></th>
						<th><input type="text" class="form-control" placeholder="TANGGAL DISPATCH"></th>
						<th><input type="text" class="form-control" placeholder="TERAKHIR UPDATE"></th>
					</tr>
                    <tr>
                        <th>NO</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>MYIR</th>
                        <th>SC</th>
                        <th>LAYANAN</th>
                        <th>STATUS</th>
                        <th>CATATAN TEKNISI</th>
                        <th>STATUS DEPOSIT</th>
                        <th>KODE PLASA</th>
                        <th>TANGGAL DISPATCH</th>
                        <th>TERAKHIR UPDATE</th>
                    </tr>
                    <thead>
                    <tbody>
					@foreach ($data as $num => $d)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $d->tim }}</td>
						<td>{{ $d->sektor }}</td>
						<td>{{ $d->myir ? : $d->myir1 }}</td>
						<td>{{ $d->sc ? : $d->sc1 }}</td>
                        <td>{{ $d->layanan }}</td>
						<td>{{ $d->status ? : 'ANTRIAN' }}</td>
						<td>{{ $d->catatan }}</td>
						<td>
                        @if($d->sts_depo=="")
                            <a href="/dshr/plasa-sales/order/inbox/{{ $d->id_pmw }}" class="btn btn-warning">NO STATUS</a>
                        @else
                            <a href="/dshr/plasa-sales/order/inbox/{{ $d->id_pmw }}" class="btn btn-success">{{ $d->sts_depo }} / UPDATE BY {{ $d->udb }}</a>
                        @endif
                        </td>
						<td>{{ $d->udb ? : '-' }}</td>
						<td>{{ $d->tgl_disp }}</td>
                        <td>{{ $d->modified_at }}</td>
					</tr>
					@endforeach
                    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
</script>
@endsection