@extends('layout')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

@section('content')

@include('partial.alerts')

<form method="get">
    <div class="row">
        <div class="col-sm-10">
            <div class="form-group row">
                <label for="input-kelurahan" class="col-2 col-form-label">Kelurahan</label>
                <div class="col-10">
                    <select class="form-control select2" name="kelurahan" id="input-kelurahan" required>
                        <option value="" selected disabled>Pilih Kelurahan</option>
                        @foreach ($list_kelurahan as $kel)
                        <option value="{{ $kel->validasi_kelurahan }}">{{ $kel->validasi_kelurahan }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <input type="submit" value="Cari!" id="btn_get" class="btn btn-sm btn-info btn-rounded btn-block">
        </div>
    </div>
</form>

@if ($kelurahan != null)
<form method="post">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title m-b-0">Assign Order</h3>
                <div class="form-group row">
                    <label for="input-sales" class="col-2 col-form-label">Sales</label>
                    <div class="col-10">
                        <select class="form-control select2" name="id_sales" id="input-sales" required>
                            <option value="" selected disabled>Pilih Sales</option>
                            @foreach ($list_sales as $sales)
                            <option value="{{ $sales->nik }}">{{ $sales->nama }} ({{ $sales->nik }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-odp" class="col-2 col-form-label">ODP</label>
                    <div class="col-10">
                        <select class="form-control select2-multiple" multiple="multiple" name="list_odp[]" id="input-odp" required>
                            @foreach ($list_odp as $odp)
                            <option value="{{ $odp->odp_name }}">{{ $odp->odp_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="catatan" class="col-2 col-form-label">Catatan</label>
                    <div class="col-10">
                        <textarea class="form-control" name="catatan" rows="4">{{ old('catatan') }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date_assign" class="col-2 col-form-label">Date</label>
                    <div class="col-10">
                        <input type="text" class="form-control" id="date_assign" name="date_assign" value="{{ old('date_assign') ? : date('Y-m-d')}}" placeholder="yyyy-mm-dd" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title m-b-0">Map</h3>
            </div>
        </div>

        <div class="col-sm-12">
            <input type="submit" value="Kirim!" id="btn_post" class="btn btn-sm btn-success btn-rounded btn-block">
        </div>
    </div>
</form>
@endif

@endsection

@section('plugins')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#input-kelurahan').select2({
            placeholder: "Pilih Kelurahan",
            allowClear: true
        });

        $('#input-sales').select2({
            placeholder: "Pilih Sales",
            allowClear: true
        });

        $('#input-odp').select2();

        $('#date_assign').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
    });
</script>
@endsection