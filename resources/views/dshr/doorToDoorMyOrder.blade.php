@extends('layout')

@section('content')

@include('partial.alerts')

<div class="col-md-12">
    <div class="white-box">
        <h3 class="box-title">DOOR TO DOOR - MY ORDER</h3>
        <div class="row">
            @foreach ($data as $k => $v)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-block text-center">
                        <p class="card-text">
                            @foreach (json_decode($v->list_odp) as $numb => $odp)
                            ODP {{ ++$numb }} : {{ $odp }}<br />
                            @endforeach
                            <br />
                            {{ $v->catatan }}
                        </p>
                        <a href="/dshr/door-to-door/assign/update/{{ $v->id_assign }}" class="btn btn-rounded btn-primary btn-block">Update</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection