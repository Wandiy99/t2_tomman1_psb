@extends('layout')
@section('content')
<style>
    th, td {
    text-align: center;
    vertical-align: middle;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@include('partial.alerts')

<div class="row">

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD<br />DOOR TO DOOR SALES<br />PERIODE {{ $start_date }} S/D {{ $end_date }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    
                    <div class="col-md-5">
                        <label>Tanggal Awal</label>
                        <input type="date" name="startDate" id="startDate" class="form-control" value="{{ $start_date }}">
                    </div>

                    <div class="col-md-5">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="endDate" id="endDate" class="form-control" value="{{ $end_date }}">
                    </div>

                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 table-responsive white-box">
    <table class="display nowrap" cellspacing="0" width="100%" id="table_data">
        <thead>
              <tr>
                  <th class="text-center align-middle">#</th>
                  <th class="text-center align-middle">ODP</th>
                  <th class="text-center align-middle">JML</th>
              </tr>
        </thead>
        <tbody>
            @php
                $total = 0;
            @endphp
            @foreach ($data as $key => $value)
            @php
                $total += $value->jml;
            @endphp
              <tr>
                <td class="text-center align-middle">{{ ++$key }}</td>
                <td class="text-center align-middle">{{ $value->odp_name }}</td>
                <td class="text-center align-middle">{{ $value->jml }}</td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td class="text-center align-middle" colspan="2">TOTAL</td>
                <td class="text-center align-middle">{{ $total }}</td>
            </tr>
        </tfoot>
    </table>
</div>

</div>

<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {

        $('.select2').select2();
    
        $('#table_data').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });

        var day = {
                format: 'yyyy-mm-dd',
                viewMode: 0,
                minViewMode: 0
            };

        $('#startDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        $('#endDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });
    });
</script>
@endsection