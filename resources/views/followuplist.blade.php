<h3>
 FOLLOWING UP WORK ORDER <a href="/ms2n/sync/VA" class="btn btn-info">
    <span class="glyphicon glyphicon-refresh"> SyncNow</span>
  </a>
</h3>
<ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::path() == 'dispatch/followup/PITOFAILSWA') ? 'active' : '' }}"><a href="/dispatch/workorder/PITOFAILSWA">PI to FAILSWA</a></li>
  <li class="{{ (Request::path() == 'dispatch/followup/UPTOPS') ? 'active' : '' }}"><a href="/dispatch/workorder/UPTOPS">UP to PS</a></li>
  <li class="{{ (Request::path() == 'dispatch/followup/FALLOUTWFM') ? 'active' : '' }}"><a href="/dispatch/workorder/falloutWFM">FALLOUT WFM</a></li>
  <li class="{{ (Request::path() == 'dispatch/followup/FALLOUTACTIVATION') ? 'active' : '' }}"><a href="/dispatch/workorder/FALLOUTACTIVATION">FALLOUT ACTIVATION</a></li>
  <li class="{{ (Request::path() == 'dispatch/followup/MAINTENANCE') ? 'active' : '' }}"><a href="/dispatch/workorder/MAINTENANCE">MAINTENANCE</a></li>
</ul>
