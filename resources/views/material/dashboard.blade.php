@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th,td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="row">
  <div class="col-sm-12">
    <div class="white-box">
      <h3 class="box-title" style="text-align: center;">DASHBOARD INVENTORY OUT MATERIAL PROGRAM {{ $program }} PERIODE {{ $start }} / {{ $end }}</h3>
      <div class="table-responsive">
        <table class="table table-bordered" id="table_data">
          <thead>
            <tr>
              <th rowspan="2" class="align-middle">NO</th>
              <th rowspan="2" class="align-middle">SEKTOR</th>
              <th rowspan="2" class="align-middle">RFC</th>
              <th colspan="4">PENGELUARAN MATERIAL</th>
              <th colspan="4">ORDER</th>
              <th colspan="4">PEMAKAIAN MATERIAL</th>
              <th colspan="4">DEVIASI</th>
            </tr>
            <tr>
              <th>DC</th>
              <th>PRECON 50</th>
              <th>PRECON 80</th>
              <th>SOC</th>
              <th>PSB</th>
              <th>WMS</th>
              <th>PDA</th>
              <th>TOTAL</th>
              <th>DC</th>
              <th>PRECON 50</th>
              <th>PRECON 80</th>
              <th>SOC</th>
              <th>DC</th>
              <th>PRECON 50</th>
              <th>PRECON 80</th>
              <th>SOC</th>
            </tr>
          </thead>
          <tbody>
              @php
                $no = $total_rfc = $total_dc = $total_precon_50 = $total_precon_80 = $total_soc = $total_order_psb = $total_order_wms = $total_order_pda = $total_order_all = $total_dc_roll = $total_precon50 = $total_precon80 = $total_socm = 0;
              @endphp
            @foreach ($query as $num => $result)
            <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $num }}</td>
              <td>{{ @$result['JML_RFC'] }}</td>
              <td>{{ number_format(@$result['DC']) }}</td>
              <td>{{ number_format(@$result['PRECON_50']) }}</td>
              <td>{{ number_format(@$result['PRECON_80']) }}</td>
              <td>{{ number_format(@$result['SOC']) }}</td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=PSB&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ $result['order_psb'] }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=WMS&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ $result['order_wms'] }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=PDA&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ $result['order_pda'] }}</a></td>
              <td>{{ (($result['order_psb']) + ($result['order_wms']) + ($result['order_pda'])) }}</td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=DC&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($result['dc_roll']) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=PRECON50&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($result['precon50']) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=PRECON80&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($result['precon80']) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor={{ $num }}&order=SOC&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($result['socm']) }}</a></td>
              <td>{{ number_format((@$result['DC']) - ($result['dc_roll'])) }}</td>
              <td>{{ number_format((@$result['PRECON_50']) - ($result['precon50'])) }}</td>
              <td>{{ number_format((@$result['PRECON_80']) - ($result['precon80'])) }}</td>
              <td>{{ number_format((@$result['SOC']) - ($result['socm'])) }}</td>
            </tr>
              @php
                $total_rfc += @$result['JML_RFC'];
                $total_dc += @$result['DC'];
                $total_precon_50 += @$result['PRECON_50'];
                $total_precon_80 += @$result['PRECON_80'];
                $total_soc += @$result['SOC'];
                $total_order_psb += $result['order_psb'];
                $total_order_wms += $result['order_wms'];
                $total_order_pda += $result['order_pda'];
                $total_dc_roll += $result['dc_roll'];
                $total_precon50 += $result['precon50'];
                $total_precon80 += $result['precon80'];
                $total_socm += $result['socm'];
                $total_order_all = $total_order_psb + $total_order_wms + $total_order_pda;
              @endphp
            @endforeach
            <tr>
              <td colspan="2">TOTAL</td>
              <td>{{ $total_rfc }}</td>
              <td>{{ number_format($total_dc) }}</td>
              <td>{{ number_format($total_precon_50) }}</td>
              <td>{{ number_format($total_precon_80) }}</td>
              <td>{{ number_format($total_soc) }}</td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=PSB&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_order_psb) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=WMS&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_order_wms) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=PDA&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_order_pda) }}</a></td>
              <td>{{ number_format($total_order_all) }}</td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=DC&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_dc_roll) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=PRECON50&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_precon50) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=PRECON80&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_precon80) }}</a></td>
              <td><a href="/dashboardMaterialDetail?sektor=ALL&order=SOC&startDate={{ $start }}&endDate={{ $end }}" target="_blank" style="text-decoration: underline; color: black;">{{ number_format($total_socm) }}</a></td>
              <td>{{ number_format(($total_dc) - ($total_dc_roll)) }}</td>
              <td>{{ number_format(($total_precon_50) - ($total_precon50)) }}</td>
              <td>{{ number_format(($total_precon_80) - ($total_precon80)) }}</td>
              <td>{{ number_format(($total_soc) - ($total_socm)) }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
