@extends('layout')
@section('content')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL MATERIAL SEKTOR {{ $sektor }} ORDER {{ $order }} PERIODE {{ $start }} S/D {{ $end }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                    @if(!in_array($order,["PSB","WMS","PDA"]))
                        @if($order == "DC")
                        <th>DC</th>
                        @elseif($order == "PRECON50")
                        <th>PRECON 50</th>
                        @elseif($order == "PRECON50")
                        <th>PRECON 80</th>
                        @elseif($order == "SOC")
                        <th>SOC</th>
                        @endif
                    @else
                        <th>DC</th>
                        <th>PRECON 50</th>
                        <th>PRECON 80</th>
                        <th>TOTAL PRECON</th>
                        <th>SOC</th>
                    @endif
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total_dc = $total_precon50 = $total_precon80 = $total_socm = 0;
                    @endphp
                    @foreach ($query as $num => $data)
                    @php
                        $total_dc += $data->dc_roll;
                        $total_precon50 += $data->precon50;
                        $total_precon80 += $data->precon80;
                        $total_socm += $data->socm;
                    @endphp 
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->ORDER_ID }}</td>
                        <td>{{ $data->tim ? : '-' }}</td>
                        <td>{{ $data->mitra ? : '-' }}</td>
                    @if(!in_array($order,["PSB","WMS","PDA"]))
                        @if($order == "DC")
                        <td>{{ number_format($data->dc_roll) }}</td>
                        @elseif($order == "PRECON50")
                        <td>{{ $data->precon50 }}</td>
                        @elseif($order == "PRECON50")
                        <td>{{ $data->precon80 }}</td>
                        @elseif($order == "SOC")
                        <td>{{ $data->socm }}</td>
                        @endif
                    @else
                        <td>{{ number_format($data->dc_roll) }}</td>
                        <td>{{ $data->precon50 }}</td>
                        <td>{{ $data->precon80 }}</td>
                        <td>{{ number_format($data->precon50 + $data->precon80) }}</td>
                        <td>{{ $data->socm }}</td>
                    @endif
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        @if(!in_array($order,["PSB","WMS","PDA"]))
                            @if($order == "DC")
                            <th>{{ number_format($total_dc) }}</th>
                            @elseif($order == "PRECON50")
                            <th>{{ number_format($total_precon50) }}</th>
                            @elseif($order == "PRECON50")
                            <th>{{ number_format($total_precon80) }}</th>
                            @elseif($order == "SOC")
                            <th>{{ number_format($total_socm) }}</th>
                            @endif
                        @else
                            <th>{{ number_format($total_dc) }}</th>
                            <th>{{ number_format($total_precon50) }}</th>
                            <th>{{ number_format($total_precon80) }}</th>
                            <th>{{ number_format($total_precon50 + $total_precon80) }}</th>
                            <th>{{ number_format($total_socm) }}</th>
                        @endif
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL MATERIAL ORDER SEKTOR BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection