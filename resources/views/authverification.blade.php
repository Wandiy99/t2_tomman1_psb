<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BIAWAK</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Login and Start Out Experience Work is Easy With Us | TOMMAN" />
        <meta
            name="keywords"
            content="ops tomman app,tomman app,telkomakses tomman,tomman telkom,tomman banjarmasin,tomman kalsel,telkomakses,warrior telkom akses,login tomman app,login tomman,tomman portal,tomman telkomakses,tomman telkom akses,telkomakses tomman,kalsel hibat,tomman kalsel hibat,banjarmasin,kalimatan selatan,indihome,tomman indihome"
        />
        <meta name="copyright" content="biawak.tomman.app" />
        <meta name="author" content="Login - TOMMAN" />
        <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/fonts/iconic/css/material-design-iconic-font.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/animate.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/hamburgers.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/animsition.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/util.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/main.css" />
        <style>
            .digit-group input {
                width: 30px;
                height: 50px;
                background-color: #525252!important;
                border: none;
                line-height: 50px;
                text-align: center;
                font-size: 24px;
                font-family: 'Raleway', sans-serif;
                font-weight: 200;
                color: white;
                margin: 0 2px;
            }
            .digit-group .splitter {
                padding: 0 5px;
                color: white;
                font-size: 24px;
            }
            .prompt {
                margin-bottom: 20px;
                font-size: 20px;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    @include('partial.alerts')
                    <form method="post" class="login100-form validate-form digit-group" data-group-name="digits">
                        <span class="login100-form-title p-b-26"> TOM<font style="color: red;">MAN</font> OPERATIONAL </span><br /><br />

                        <input type="hidden" name="login" value="{{ $data->id_karyawan }}">
                        <input type="hidden" name="password" value="{{ $data->password }}">
                        <input type="hidden" name="otp_valid_until" value="{{ $data->otp_valid_until }}">
                        
                        {{-- <div class="wrap-input100 validate-input">
                            <input class="input100" type="number" name="otp_code" minlength="6" maxlength="6" id="input-otp-code" required/>
                            <span class="focus-input100" data-placeholder="Code OTP"></span>
                        </div> --}}

                        <div class="text-center">
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-1" name="digit[1]" data-next="digit-2" required/>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-2" name="digit[2]" data-next="digit-3" data-previous="digit-1" required/>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-3" name="digit[3]" data-next="digit-4" data-previous="digit-2" required/>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-4" name="digit[4]" data-next="digit-5" data-previous="digit-3" required/>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-5" name="digit[5]" data-next="digit-6" data-previous="digit-4" required/>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="digit-6" name="digit[6]" data-next="digit-7" data-previous="digit-5" required/>
                        </div>

                        <br />
                        
                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Login
                                </button>
                            </div>
                        </div>
                        <br />
                        <p class="text-muted" style="text-align: center;">BIAWAK</p>
                        <br />
                        <p style="text-align: center; color: #E74C3C">Pastikan Telegram Rekan Sudah Login ke <a href="https://t.me/TommanGo_Bot">Bot TommanGO</a></p>
                    </form>
                </div>
            </div>
        </div>
        <div id="dropDownSelect1"></div>

        <script src="/bower_components/vendor/jquery-3.2.1.min.js"></script>
        <script src="/bower_components/vendor/animsition.min.js"></script>
        <script src="/bower_components/vendor/popper.js"></script>
        <script src="/bower_components/vendor/bootstrap.min.js"></script>
        <script src="/bower_components/vendor/select2.min.js"></script>
        <script src="/bower_components/vendor/moment.min.js"></script>
        <script src="/bower_components/vendor/daterangepicker.js"></script>
        <script src="/bower_components/vendor/countdowntime.js"></script>
        <script src="/bower_components/vendor/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
            $('.digit-group').find('input').each(function() {
                $(this).attr('maxlength', 1);
                $(this).on('keyup', function(e) {
                var parent = $($(this).parent());
                if(e.keyCode === 8 || e.keyCode === 37) {
                var prev = parent.find('input#' + $(this).data('previous'));
                if(prev.length) {
                $(prev).select();
                }
                } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                var next = parent.find('input#' + $(this).data('next'));
                if(next.length) {
                $(next).select();
                } else {
                if(parent.data('autosubmit')) {
                parent.submit();
                }
                }
                }
                });
            });
        </script>
    </body>
</html>
