@extends('layout')

@section('content')
  @include('partial.alerts')

  <div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <div class="table-responsive">
            <table class="table">
              <tr>
                <th>No</th>
                <th>Sektor</th>
                <th>ORDER</th>
                <th>TIKET</th>
                <th>Status</th>
                <th>TGL REPORT</th>
                <th>ALPRO</th>
                <th>TIM</th>
                <th>PEKERJAAN</th>
                <th>FOTO</th>
              </tr>
              @foreach ($get_list_TL as $num => $list_TL)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $list_TL->title }}</td>
                <td>{{ $order }}</td>
                <td>
                  {{ $list_TL->Ndem }}<br />
                </td>
                <td>
                  @if ($list_TL->qcondesk_status=="rejected")
                  <span class="label label-danger">REJECTED</span><br />
                  <p>{{ $list_TL->qcondesk_alasan }}</p>
                  @else
                  <a href="/QCOndeskApprove/{{ $list_TL->id_dt }}" class="label label-success">Approve</a><br />
                  <a href="/QCOndeskReject/{{ $list_TL->id_dt }}" class="label label-danger">Reject</a><br />
                  @endif
                </td>
                <td>{{ $list_TL->modified_at }}</td>
                @if($order=="ASR")
                  <td>{{ $list_TL->alpro }}</td>
                @else
                  <td></td>
                @endif
                <td>{{ $list_TL->uraian }} ({{ $list_TL->title }})</td>
                <td>{{ $list_TL->action ? : $list_TL->jenisPsb }}</td>
                <td>
                <?php
                  $result = '';
                  foreach($photoInputs as $input) {

                      $path   = "/upload4/".$folder."/".$list_TL->id_dt."/$input";
                      $path2  = "/upload3/".$folder."/".$list_TL->id_dt."/$input";
                      $th     = "$path-th.jpg";
                      $th2    = "$path2-th.jpg";
                      $img    = "$path.jpg";
                      $img2   = "$path2-th.jpg";


                      $result .='
                      <td style="padding:5px;" valign=top>';
                      if (file_exists(public_path().$th)){
                        $filemime = @$data[$list_TL->id_dt][$input];
                        if (@array_key_exists("DateTimeOriginal",$filemime)){
                          $DateTimeOriginal = @$filemime['DateTimeOriginal'].' / '.@$filemime['Software'] ? : 'default';
                        } else {
                          $DateTimeOriginal = 0;
                        }
                        //print_r();
                        $result .='
                        <a href="/upload4/'.$folder.'/'.$list_TL->id_dt.'/'.$input.'.jpg">
                        <img src="/upload4/'.$folder.'/'.$list_TL->id_dt.'/'.$input.'-th.jpg" alt="'.$input.'">
                        <br />'.$input.' ('.$DateTimeOriginal.')
                        </a>';
                      } 
                      elseif (file_exists(public_path().$th2)){
                        $filemime = @$data[$list_TL->id_dt][$input];
                        if (@array_key_exists("DateTimeOriginal",$filemime)){
                          $DateTimeOriginal = @$filemime['DateTimeOriginal'].' / '.@$filemime['Software'] ? : 'default';
                        } else {
                          $DateTimeOriginal = 0;
                        }
                        //print_r();
                        $result .='
                        <a href="/upload4/'.$folder.'/'.$list_TL->id_dt.'/'.$input.'.jpg">
                        <img src="/upload4/'.$folder.'/'.$list_TL->id_dt.'/'.$input.'-th.jpg" alt="'.$input.'">
                        <br />'.$input.' ('.$DateTimeOriginal.')
                        </a>';
                      }
                      else {
                        $result .='<img src="/image/placeholder.gif" alt="" /><br />  <a href="/upload4/'.$folder.'/'.$list_TL->id_dt.'/'.$input.'.jpg">
                        '.$input.'</a>';
                      }

                  }
                  echo $result;
                  ?>
                </td>
              </tr>
              @endforeach
            </table>
          </div>

          </div>
      </div>
    </div>
    </div>

@endsection
