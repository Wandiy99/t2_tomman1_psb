@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      <a href="/QCOndesk/{{ date('Y-m') }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      <br />
      <br />
      <div class="panel panel-default">
        <div class="panel-heading">
          QC Ondesk Reject
        </div>
        <div class="panel-body">
          <form method="post">
            <input type="hidden" name="status" value="rejected">
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="form-group">
              <label for="alasan">Alasan</label>
              <textarea id="alasan" class="form-control" name="alasan"></textarea>
              <br />
            </div>
            <input type="submit" name="submit" value="submit" class="btn btn-danger" />
          </form>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          Detail Report
        </div>
        <div class="panel-body">
        <div class="row text-center input-photos" style="margin: 20px 0">
          <?php
            $result = '  ';
            foreach($photo as $input) {
                $result .= '<div class="col-xs-6 col-sm-2">';
                $path = "/upload/".$folder."/".$id."/$input";
                $th   = "$path-th.jpg";
                $img  = "$path.jpg";

                if (file_exists(public_path().$th)){
                  $filemime = @$data[$list_TL->id_dt][$input];
                  if (@array_key_exists("DateTimeOriginal",$filemime)){
                    $DateTimeOriginal = @$filemime['DateTimeOriginal'].' / '.@$filemime['Software'] ? : 'default';
                  } else {
                    $DateTimeOriginal = 0;
                  }
                  //print_r();
                  $result .='
                  <a href="/upload/'.$folder.'/'.$id.'/'.$input.'.jpg">
                  <img src="/upload/'.$folder.'/'.$id.'/'.$input.'-th.jpg" alt="'.$input.'">
                  <br />'.$input.'
                  </a>';
                } else {
                  $result .='<img src="/image/placeholder.gif" alt="" /><br />  <a href="/upload/'.$folder.'/'.$id.'/'.$input.'.jpg">
                  '.$input.'</a>';
                }
                $result .= '</div>';
            }
            echo $result;
            ?>
        </div>
      </div>
    </div>
  </div>
@endsection
