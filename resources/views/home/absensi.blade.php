@extends('tech_layout')
@section('content')
@include('partial.alerts')

<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-default">
		<div class="panel-heading">Absensi</div>
			<div class="panel-body">
				<div class="row">

                <div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Status Absen</label>
						<select class="input-status-absen form-control" name="status_absen">
                        <option value="HADIR"> HADIR </option>
                        <option value="PULANG"> PULANG </option>
                        </select>
						{!! $errors->first('status_absen','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Status Kesehatan</label>
						<select class="input-status-kesehatan form-control" name="status_kesehatan">
                        <option value="SEHAT"> SEHAT </option>
                        <option value="SAKIT"> SAKIT </option>
                        <option value="IZIN"> IZIN </option>
                        <option value="CUTI"> CUTI </option>
                        </select>
						{!! $errors->first('status_kesehatan','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
		
				<div class="col-md-6">	
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">	
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">Foto Absensi</div>
		<div class="panel-body">
			<div class="row">	
					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="Belum Ada Foto" width="80"><br>Hadir<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_absen_Hadir" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_absen_Hadir','<span class="label label-danger">:message</span>') !!}
					</div>
	
					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="Belum Ada Foto" width="80"><br>Pulang<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_absen_Pulang" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_absen_Pulang','<span class="label label-danger">:message</span>') !!}
					</div>

				</div>
			</div>
		</div>	
	</div>
</form>
@endsection

@section('plugins')
<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.4-beta.33/jquery.inputmask.min.js"></script>
	<script>
		$(function(){

			$('input[type=file]').change(function() {
		        console.log(this.name);
		        var inputEl = this;
		        if (inputEl.files && inputEl.files[0]) {
		         	$(inputEl).parent().find('input[type=text]').val(1);
		          	var reader = new FileReader();
		          	reader.onload = function(e) {
		            	$(inputEl).parent().find('img').attr('src', e.target.result);
		          	}
		        	reader.readAsDataURL(inputEl.files[0]);
			    }
		    });

		    $('.input-photos').on('click', 'button', function() {
		    	$(this).parent().find('input[type=file]').click();
		    });

            $(".input-status-kesehatan").select2({
            placeholder: "Pilih Status Kesehatan",
            allowClear: true
            });

            $(".input-status-absen").select2({
            placeholder: "Pilih Status Absen",
            allowClear: true
            });
		})
	</script>
@endsection