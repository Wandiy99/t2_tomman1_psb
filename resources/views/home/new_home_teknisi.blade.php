@extends('new_tech_layout')
@section('content')
@include('partial.alerts')
<center>
    <img src="/image/people.png" style="width: 25%;" />
    <br />
    <br />
    <h1 class="display-7">Hi, {{ $session->nama }}</h1>

    @if (count($status_absen) == 0)
    <br />
    <br />
    <br />
    <br />
    <form method="post" action="/home" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="status" value="HADIR" />
        <input type="hidden" name="nik" value="{{ $session->id_user }}" />
        <input type="hidden" name="nama" value="{{ $session->nama }}" />
        <div class="container col-md-6">
            <div class="mb-4">
                <label for="Image" class="form-label">Absen</label>
                <input class="form-control" type="file" accept="image/*" id="evidence_absen" onchange="preview_evidence_absen()" name="evidence_absen" required />
            </div>

            <div class="mb-2">
                <button onclick="clearImage_evidence_absen()" class="btn btn-warning mt-3">Clear ? Absen</button>
            </div>
            <img id="frame_evidence_absen" src="" class="img-fluid" />
        </div>

        <script>
            function preview_evidence_absen() {
                frame_evidence_absen.src = URL.createObjectURL(event.target.files[0]);
            }
            function clearImage_evidence_absen() {
                document.getElementById("evidence_absen").value = null;
                frame_evidence_absen.src = "";
            }
        </script>
        @if (date('l') == 'Friday')
        <br />
        <br />
        <div class="container col-md-6">
            <div class="mb-4">
                <label for="Image" class="form-label">Peduli Infra</label>
                <input class="form-control" type="file" accept="image/*" id="peduli_infra" onchange="preview_peduli_infra()" name="PeduliInfra" required />
            </div>
            <div class="mb-2">
                <button onclick="clearImage_peduli_infra()" class="btn btn-warning mt-3">Clear ? Peduli Infra</button>
            </div>
            <img id="frame_peduli_infra" src="" class="img-fluid" />
        </div>

        <script>
            function preview_peduli_infra() {
                frame_peduli_infra.src = URL.createObjectURL(event.target.files[0]);
            }
            function clearImage_peduli_infra() {
                document.getElementById("peduli_infra").value = null;
                frame_peduli_infra.src = "";
            }
        </script>
        @endif
        <button class="btn btn-primary mt-3" type="submit">Upload !</button>
    </form>
    @elseif ((count($status_absen) > 0) && $status_absen[0]->approval == 0)
    <br />
    <br />
    <br />
    <br />
    <img src="/image/green-checklists.png" style="width: 25%;" />
    <br />
    <br />
    {{ $datex }}
    <br />
    <br />
    <span>Menunggu Persetujuan Absen dari TL</span>
    @elseif ((count($status_absen) > 0) && $status_absen[0]->approval == 3)
    <br />
    <br />
    <br />
    <br />
    <form method="post" action="/home" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="status" value="HADIR" />
        <div class="container col-md-6">
            <div class="mb-4">
                <label for="Image" class="form-label">Absen</label>
                <input class="form-control" type="file" accept="image/*" id="evidence_absen" onchange="preview_evidence_absen()" name="evidence_absen" required />
            </div>

            <div class="mb-2">
                <button onclick="clearImage_evidence_absen()" class="btn btn-warning mt-3">Clear ? Absen</button>
            </div>
            <img id="frame_evidence_absen" src="" class="img-fluid" />
        </div>

        <script>
            function preview_evidence_absen() {
                frame_evidence_absen.src = URL.createObjectURL(event.target.files[0]);
            }
            function clearImage_evidence_absen() {
                document.getElementById("evidence_absen").value = null;
                frame_evidence_absen.src = "";
            }
        </script>
        @if (date('l') == 'Friday')
        <br />
        <br />
        <div class="container col-md-6">
            <div class="mb-4">
                <label for="Image" class="form-label">Peduli Infra</label>
                <input class="form-control" type="file" accept="image/*" id="peduli_infra" onchange="preview_peduli_infra()" name="PeduliInfra" required />
            </div>
            <div class="mb-2">
                <button onclick="clearImage_peduli_infra()" class="btn btn-warning mt-3">Clear ? Peduli Infra</button>
            </div>
            <img id="frame_peduli_infra" src="" class="img-fluid" />
        </div>

        <script>
            function preview_peduli_infra() {
                frame_peduli_infra.src = URL.createObjectURL(event.target.files[0]);
            }
            function clearImage_peduli_infra() {
                document.getElementById("peduli_infra").value = null;
                frame_peduli_infra.src = "";
            }
        </script>
        @endif
        <button class="btn btn-primary mt-3" type="submit">Upload !</button>
    </form>
    @else
    <br />
    <br />
    <br />
    <br />
    <figure class="text-center">
        <blockquote class="blockquote">
            <p>Selamat Bertugas Rekan Tetap Jaga Kesehatan dan Kebersihan, Utamakan K3 Dalam Bekerja dan Terutama Jangan Lupa Ibadah. Semangat!!!</p>
        </blockquote>
        <figcaption class="blockquote-footer">
            Tomman Inovation ©2016 - {{ date('Y') }}
        </figcaption>
    </figure>
    @endif
</center>
@endsection