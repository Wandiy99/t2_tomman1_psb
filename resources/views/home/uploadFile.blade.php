@extends('tech_layout')
@section('content')
@include('partial.alerts')
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Upload File</div>
			<div class="panel-body">
				<div class="row" style="text-align: center;">

				<div class="col-md-12">
					<div class="form-group">
						<label>Keterangan</label>
						<input type="text" style="text-align: center;" name="keterangan" class="form-control" value="Sertifikat_Vaksin" readonly>
					</div>
				</div>

                <div class="col-md-12 input-photos">
                    <div class="form-group">
                        <img src="/image/placeholder.gif" alt="File Foto" width="80"><br>Hasil Screenshot<br>
                        <input type="text" class="hidden" name="" value=""/>
                        <input type="file" class="hidden" name="photo_hasil_screenshot" accept="image/jpeg" />
                        <button type="button" class="btn btn-sm btn-info ">
                            <i class="glyphicon glyphicon-camera"></i>
                        </button><br>
                    </div>
                    {!! $errors->first('photo_hasil_screenshot','<span class="label label-danger">:message</span>') !!}
                </div>

                <br><br>
		
				<div class="col-md-12">	
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">	
					</div>
				</div>

			</div>
		</div>
	</div>
</form>
@endsection

@section('plugins')
<script>
    $(function(){

        $('input[type=file]').change(function() {
            console.log(this.name);
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(inputEl.files[0]);
            }
        });

        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
        });

    })
</script>
@endsection