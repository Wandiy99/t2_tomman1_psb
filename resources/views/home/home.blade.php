@extends('layout')

@section('content')
  @include('partial.alerts')

  <form method="POST" action="{{ route('approve.massal') }}">
    <div class="row">
      <div class="col-md-12" id="bio_tl">
        <div class="panel panel-default">
            <div class="panel-heading">
            Profile
            </div>
            <div class="panel-body table-responsive">
              <table style="padding:10px;">
                <tr>
                  <td style="padding:15px">
                    <img src="/image/people.png" width="100" />
                  </td>
                  <td style="padding:10px">

                    {{ session('auth')->nama }}<br />
                    @foreach ($group_telegram as $gt)
                    <span class="label label-info">{{ $gt->title }}</span>
                    @endforeach
                    <br />
                    <small>
                      Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                    </small>
                  </td>
                  <td style="padding : 10px" valign=top>

                  </td>
                </tr>

              </table>
              {{-- @include ('partial.home') --}}

                    <?php
                      $nama_tim = '';
                      $jumlah_tim = 0;
                      $jumlah_teknisi = 0;
                    ?>
                    <table class="table">
                      <tr>
                        <td class="text-center">No</td>
                        @if(in_array(session('auth')->level, [2, 15, 46]))
                          <td>
                            <input type="submit" class="btn btn-primary btn-xs btn-rounded" value="Approve Cheklist">
                          </td>
                        @endif
                        <td class="text-center">Status Approval</td>
                        <td class="text-center">Evidence Absen</td>
                        @if (date('l') == 'Friday')
                        <td class="text-center">Evidence Peduli Infra</td>
                        @endif
                        <td class="text-center">Sertifikat Vaksin</td>
                        <td class="text-center">NIK</td>
                        <td class="text-center">Laborcode</td>
                        <td class="text-center">Name</td>
                        <td class="text-center">Squad</td>
                        <td class="text-center">Team / Crew ID</td>
                        <td class="text-center">Kesehatan</td>
                        <td class="text-center">Kehadiran</td>
                        <td class="text-center">Waktu Absen</td>
                        <td class="text-center">Waktu Approval</td>
                        @if (session('auth')->id_user=='88159353' || session('auth')->id_user=='720428')
                          <td class="text-center">TL</td>
                          <td class="text-center">Sektor</td>
                        @endif
                      </tr>
                      <!-- <?php print_r($data_team); ?> -->
                      @if (count($active_team)>0)

                    @foreach ($active_team as $num => $at)
                      <tr>
                        <td class="align-middle">{{ ++$num }}.</td>

                        @if(in_array(session('auth')->level, [2, 15, 46]))
                        <td class="align-middle">
                            @if ($at->status_kehadiran=="HADIR")
                              <?php
                                  $disable = 'disabled';
                                  if ($at->approval == 0)
                                  {
                                      $disable = '';
                                  }
                               ?>
                              <center>
                                <input type="checkbox" name="approveMassal[]" value="{{ $at->NIK }}" {{ $disable }}>
                              </center>
                            @endif
                        </td>
                        @endif

                        <td class="align-middle">
                          <center>
                          @if ($at->status_kehadiran == "HADIR")
                            @if ($at->approval == 0)
                              <a href="/home/absen/approve/{{ $at->NIK }}/{{ date('Y-m-d') }}" class="label label-default">APPROVE</a>
                              <br /><br />
                              <a href="/home/absen/decline/{{ $at->absen_id }}" class="label label-danger">DECLINE</a>
                            @elseif ($at->approval == 3)
                              <span class="label label-warning">DECLINED</span>
                            @else
                              <span class="label label-success">APPROVED</span>
                            @endif
                          @endif
                          </center>
                        </td>

                        <td class="align-middle">
                          <center>
                          @php
                            $name = date('Ymd');
                            $path4  = "/upload4/absensi/".$at->NIK;
                            $th4    = "$path4/$name-th.jpg";
                            $path   = null;

                            if (file_exists(public_path().$th4))
                            {
                              $path = "$path4/$name";
                            }

                            if($path)
                            {
                                $img    = "$path.jpg";
                                $th     = "$path-th.jpg";
                            } else {
                                $img    = null;
                                $th     = null;
                            }
                          @endphp
                          @if ($img != null)
                          <a href="{{ $img }}">
                            <img src="{{ $th }}" class="image" alt="{{ $name }}">
                          </a>
                          @else
                          <a href="javascript:void(0)">
                            <img src="/image/placeholder.gif" class="image" alt="{{ $name }}">
                          </a>
                          @endif
                          </center>
                        </td>
                        @if (date('l') == 'Friday')
                        <td class="align-middle">
                          <center>
                          @php
                            $name = 'PeduliInfra_'.date('Ymd');
                            $path4  = "/upload4/peduli_infra/".$at->NIK;
                            $th4    = "$path4/$name-th.jpg";
                            $path   = null;

                            if (file_exists(public_path().$th4))
                            {
                              $path = "$path4/$name";
                            }

                            if($path)
                            {
                                $img    = "$path.jpg";
                                $th     = "$path-th.jpg";
                            } else {
                                $img    = null;
                                $th     = null;
                            }
                          @endphp
                          @if ($img != null)
                          <a href="{{ $img }}">
                            <img src="{{ $th }}" class="image" alt="{{ $name }}">
                          </a>
                          @else
                          <a href="javascript:void(0)">
                            <img src="/image/placeholder.gif" class="image" alt="{{ $name }}">
                          </a>
                          @endif
                          </center>
                        </td>
                        @endif
                        <td class="align-middle">
                          <center>
                          @php
                            $name = 'photo_hasil_screenshot';
                            $path4  = "/upload4/profile/".$at->NIK;
                            $th4    = "$path4/$name-th.jpg";
                            $path   = null;

                            if (file_exists(public_path().$th4))
                            {
                              $path = "$path4/$name";
                            }

                            if($path)
                            {
                                $img    = "$path.jpg";
                                $th     = "$path-th.jpg";
                            } else {
                                $img    = null;
                                $th     = null;
                            }
                          @endphp
                          @if ($img != null)
                          <a href="{{ $img }}">
                            <img src="{{ $th }}" class="image" alt="{{ $name }}">
                          </a>
                          @else
                          <a href="javascript:void(0)">
                            <img src="/image/placeholder.gif" class="image" alt="{{ $name }}">
                          </a>
                          @endif
                          </center>
                        </td>
                        <td class="align-middle"><center>{{ $at->NIK  }}</center></td>
                        <td class="align-middle"><center>{{ $at->laborcode  }}</center></td>
                        <td class="align-middle"><center><a class="label label-info">{{ $at->nama  }}</a></center></td>
                        <td class="align-middle"><center><a class="label label-info" href="/updatesquad/{{ $at->NIK }}">{{ $at->squad ? : 'NOTSET' }}</a></center></td>
                        <td class="align-middle">
                          <center>
                          @if (!empty($data_team[$at->NIK]))
                            @foreach ($data_team[$at->NIK] as $team)
                                   <a class="label label-info">{{ $team->uraian }} // {{ $team->crewid ? : 'EMPTY' }}</a>
                            @endforeach
                          @endif
                          </center>
                        </td>
                        <td class="align-middle">
                          <center>
                          <select name="kesehatan[{{ $at->NIK }}]">
                            <option value="" selected disabled>Pilih Kesehatan</option>
                            @foreach ($get_kesehatan as $kesehatan)
                            <option data-subtext="description 1" value="{{ $kesehatan->id }}" <?php if (@$kesehatan->id == @$at->kesehatan) { echo "Selected"; } else { echo ""; } ?>>{{ @$kesehatan->text }}</option>
                            @endforeach
                          </select>
                          </center>
                        </td>
                        <td class="align-middle">
                          <center>
                          @if ($at->status_kehadiran == "HADIR")
                          <span class="label label-success">HADIR</span>
                          <br />
                            @for ($i=0;$i<@count(@$data_alker[$at->NIK]);$i++)
                            <span class="label label-info">{{ @$data_alker[$at->NIK][$i] }}</span>
                            @endfor
                          @else
                          <span class="label label-danger">TIDAK HADIR</span>
                          @endif
                          </center>
                        </td>
                        <td class="align-middle">
                          <center>
                            <span class="label label-default align-middle">{{ $at->tglAbsen }}</span>
                          </center>
                        </td>
                        <td class="align-middle">
                          <center>
                            <span class="label label-default">{{ $at->date_approval }}</span>
                          </center>
                        </td>

                        @if (in_array(session('auth')->id_user, ['88159353', '720428']))
                          <center>
                          <td class="align-middle"><span class="label label-primary">{{ $at->TL }}</span></td>
                          <td class="align-middle"><span class="label label-primary">{{ $at->title }}</span></td>
                          </center>
                        @endif
                      </tr>
                    @endforeach
                    @endif
                  </table>

            </div>
        </div>
      </div>
      </div>
  </form>
@endsection
