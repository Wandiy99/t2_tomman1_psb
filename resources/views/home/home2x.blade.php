@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">

    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>TREND ORDER</h3>
            <div class="stats-row">
                <div class="stat-item">
                    <h6>Overall Growth</h6> <b>80.40%</b></div>
                <div class="stat-item">
                    <h6>Montly</h6> <b>15.40%</b></div>
                <div class="stat-item">
                    <h6>Day</h6> <b>5.50%</b></div>
            </div>
            <div id="sparkline9"><canvas width="773" height="50" style="display: inline-block; width: 773px; height: 50px; vertical-align: top;"></canvas></div>
        </div>
    </div>

    <div class="col-lg-4 col-md-12">
        <div class="white-box">
            <h3 class="box-title">PI PAGI <small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then yesterday</small></h3>  
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12">
        <div class="white-box">
            <h3 class="box-title">PI BARU <small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then yesterday</small></h3>  
        </div>
    </div>
    <div class="col-lg-4 col-md-12">
        <div class="white-box">
            <h3 class="box-title">FWFM <small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then yesterday</small></h3>  
        </div>
    </div>

    <div class="col-md-12 col-lg-4">
        <div class="white-box">
            <h3 class="box-title">PI PAGI SEKTOR</h3>
            <ul class="basic-list">
                <li>Google Chrome <span class="pull-right label-danger label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
                <li>Apple Safari <span class="pull-right label-success label">21.8%</span></li>
                <li>Internet Explorer <span class="pull-right label-info label">21.8%</span></li>
                <li>Opera mini <span class="pull-right label-warning label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
            </ul>
        </div>
    </div>

    <div class="col-md-12 col-lg-4">
        <div class="white-box">
            <h3 class="box-title">PI BARU SEKTOR</h3>
            <ul class="basic-list">
                <li>Google Chrome <span class="pull-right label-danger label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
                <li>Apple Safari <span class="pull-right label-success label">21.8%</span></li>
                <li>Internet Explorer <span class="pull-right label-info label">21.8%</span></li>
                <li>Opera mini <span class="pull-right label-warning label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
            </ul>
        </div>
    </div>    

    <div class="col-md-12 col-lg-4">
        <div class="white-box">
            <h3 class="box-title">FWFM SEKTOR</h3>
            <ul class="basic-list">
                <li>Google Chrome <span class="pull-right label-danger label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
                <li>Apple Safari <span class="pull-right label-success label">21.8%</span></li>
                <li>Internet Explorer <span class="pull-right label-info label">21.8%</span></li>
                <li>Opera mini <span class="pull-right label-warning label">21.8%</span></li>
                <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
            </ul>
        </div>
    </div>  
  </div>
@endsection