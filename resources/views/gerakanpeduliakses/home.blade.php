@extends('public_layout')

@section('content')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin=""/>

  @include('partial.alerts')
    <style>
    /* .marker {
     background-size: cover;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    cursor: pointer;
    }
    */
    .mapboxgl-popup {
      font-size: 12px;
    }
    </style>
    <center><h3>Gerakan Peduli Akses</h3></center>
  <div id="map" style="height: 750px; margin-top: 10px;"></div>
<script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>
<script type="text/javascript">
$(function(){
    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    odp_open = L.layerGroup(),
    teknisi_where = L.layerGroup(),
    zoom_me = 18,
    googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    }),
    googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    }),
    googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    }),
    googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    }),
    googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    }),
    googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
    }),
    map = L.map('map', {
        center: ['-3.319000', '114.584100'],
        zoom: zoom_me,
        maxZoom: 20,
        layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, odp_open, teknisi_where]
    }),
    overlays = {
        'ODP Terbuka': odp_open,
        'Teknisi': teknisi_where,
    },
    baseLayers = {
        'Jalan': googleStreets,
        'Satelit': googleSat,
        'Satelit Dan Jalan': googleHybrid,
        'Jalur': googleTerrain,
        'Jalur 2': googleAlteredRoad,
        'Lalu Lintas': googleTraffic,
    },
    list = {!! json_encode($list) !!},
    whereisteknisi = {!! json_encode($whereisteknisi) !!},
    layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map);

    $.each(list, function(k, v){
        console.log(v)
        if(v.status_odp == 1){
            var color = new L.Icon({
                iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });
        }else{
            var color = new L.Icon({
                iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });
        }

        var explode = v.summary.split('/odp_terbuka'),
        popup = `ORDER ID : ${v.pedulialpro_id} <br/>SUMMARY : ${explode[1].trim()}<br/>PELAPOR : ${v.username}<br/>TGL : ${v.timestamp}`;

        L.marker([v.lat, v.lon], {icon: color})
        .bindPopup(popup)
        .addTo(odp_open)
    });

    var color_teknisi = new L.Icon({
        iconUrl: '/image/technician.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    })

    $.each(whereisteknisi, function(k, v){
        var lat = v.lat;
        var lon = v.lon;

        var popup = `Nama : ${v.nama}<br />Tim : ${v.tim}<br />Sektor : ${v.sektor}<br /><br />Order Terakhir : ${v.Ndem}`;

        L.marker([v.lat, v.lon], {icon: color_teknisi})
        .bindPopup(popup)
        .addTo(teknisi_where)
    });
})
</script>
@endsection
