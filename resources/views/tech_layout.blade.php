<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>BIAWAK</title>
  @yield('header')
  <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png">
  <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
  <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
  <!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/css/new_template.css" />
  <link rel="stylesheet" href="/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
  <script src="/js/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>
  {{-- <script type="text/javascript" src="/bower_components/dx/globalize.min.js"></script> --}}
  <script src="/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <!-- <script type="text/javascript" src="/bower_components/select2/select2.min.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script> -->
  <!-- jQuery Custom Scroller CDN -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <style>

</style>
</head>
<body>
  <div class="wrapper">
      <!-- Sidebar -->
      <nav id="sidebar">

          <div id="dismiss">
              <i class="fas fa-arrow-left"></i>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </div>

          <div class="sidebar-header">
              <span class="tomman">TOMMAN</span>
          </div>

          <ul class="list-unstyled components">
              <p><small><span class="gold">{{ @session('auth')->id_user }}</span> {{ @session('auth')->nama }}</small></p>
              <!-- <li class="active">
                  <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
                  <ul class="collapse list-unstyled" id="homeSubmenu">
                      <li>
                          <a href="#">Home 1</a>
                      </li>
                      <li>
                          <a href="#">Home 2</a>
                      </li>
                      <li>
                          <a href="#">Home 3</a>
                      </li>
                  </ul>
              </li> -->
              <li>
                  <a href="/">Work Order </a>
              </li>
              <li><a href="/marina/tech">Work Order Marina </a></li>
             <!-- <li><a href="/check_history">History Check </a></li> -->
             <li><a href="/checktiket">History PSB </a></li>
             <li><a href="/qcborneo/check">QC Borneo Check </a></li>
             <li><a href="/helpdesk_inbox">Inbox Helpdesk </a></li>

             <!--  <li>
                  <a href="#WOsubmenu" data-toggle="collapse" aria-expanded="false">Work Order</a>
                  <ul class="collapse list-unstyled" id="WOsubmenu">
                      <li>
                          <a href="/rfc/input">Need Progress</a>
                      </li>
                      <li>
                          <a href="/rfc/sisamaterial">Kendala</a>
                      </li>
                      <li>
                          <a href="/rfc/sisamaterial">UP</a>
                      </li>
                      <li>
                          <a href="/rfc/sisamaterial">IBooster</a>
                      </li>
                      <li>
                          <a href="/rfc/sisamaterial">Barcode</a>
                      </li>
                  </ul>
              </li> -->

              <!-- <li>
                  <a href="#RFCsubmenu" data-toggle="collapse" aria-expanded="false">RFC</a>
                  <ul class="collapse list-unstyled" id="RFCsubmenu">
                      <li>
                          <a href="/rfc/input">Input</a>
                      </li>
                      <li>
                          <a href="/rfc/sisamaterial">Saldo Material</a>
                      </li>
                  </ul>
              </li>
              <li>
                  <a href="#Produktifitas" data-toggle="collapse" aria-expanded="false">Produktifitas</a>
                  <ul class="collapse list-unstyled" id="Produktifitas">
                      <li>
                          <a href="/leaderboards/ALPHA.ASR/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/UPATEK">UPATEK</a>
                      </li>
                      <li>
                          <a href="/leaderboards/ALPHA.ASR/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/CUI">CUI</a>
                      </li>
                  </ul>
              </li>

              <li>
                  <a href="/rekap_mtd_teknisi">Absensi</a>
              </li> -->

              <li>
                  <a href="/tool/list-ba-digital">Search BA Digital</a>
              </li>

              <!-- <li>
                  <a href="/teknisi/listwo">List WO</a>
              </li> -->

              <li>
                  <a href="/logout">Logout</a>
              </li>
          </ul>
      </nav>

      <!-- Page Content -->
      <div id="content">

      <!-- <span class="mar alert alert-danger"><marquee>WASPADA CORONA VIRUS (COVID-19) # 1. Gunakan Masker dan Bawa Hand Sanitizer ketika ke Rumah Pelanggan # 2. Tuntaskan Order dengan Cepat dan tidak berlama-lama di Rumah Pelanggan # 3. Mintalah izin ke Pelanggan untuk tetap menggunakan masker selama melakukan pekerjaan # 4. Tuntaskan pekerjaan dg Berkualitas sesuai SOP # 5. Semoga kita selalu diberikan lindungan dan kesehatan dari Allah SWT. # Tomman</marquee></span> -->


              <div class="container-fluid">
                <!-- <div class="img-container"></div> -->
                <!-- <div class="blur"></div> -->
                <div class="row" style="padding-top:10px;">
                  <div class="col-sm-12">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <span class="glyphicons glyphicon-arrow-left"></span>
                </button>

              </div>
              </div>
              <br />
                  @yield('content')
              </div>
              <div id="footer" class="footer">
             <center><small>Tomman Creative Teams 2018</small></center>
             </div>
      </div>
      <!-- Dark Overlay element -->
      <div class="overlay"></div>
  </div>

</div>

      @yield('plugins')
      <script>
      

        $(document).ready(function(){
          $('.dropdown-submenu a.test').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
          });

        // $("#sidebar").mCustomScrollbar({
        //     theme: "minimal"
        // });

        $('#dismiss, .overlay').on('click', function () {
            // hide sidebar
            $('#sidebar').removeClass('active');
            // hide overlay
            $('.overlay').removeClass('active');
        });

        $('#sidebarCollapse').on('click', function () {
            // open sidebar
            $('#sidebar').addClass('active');
            // fade in the overlay
            $('.overlay').addClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });

        });

      </script>
>
      <!-- <script src="/bower_components/select2/select2.min.js"></script> -->

    </body>
    </html>
