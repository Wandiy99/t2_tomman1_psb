@extends('layout')

@section('content')
  @include('partial.alerts')
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Input Jadwal Teknisi</h3>
            <p class="text-muted m-b-30 font-13"> agar di update realtime </p>
            <form class="form" method="POST">
                <input type="hidden" name="regu_id" value="{{ $regu_id }}">
                <input type="hidden" name="sto_regu" value="{{ $sto_regu }}">

                <div class="form-group row">
                    <label for="example-text-input" class="col-2 col-form-label">NIK</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="{{ $nik }}" id="nik" name="nik">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tanggalJadwal" class="col-2 col-form-label">Date</label>
                    <div class="col-10">
                        <input class="form-control" type="date" name="date" value="{{ date('Y-m-d', strtotime("+1 days")) }}" id="tanggalJadwal">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-2 col-form-label">Libur</label>
                    <div class="col-10">
                    <select class="custom-select col-12" name="status" id="status">
                        <option selected="" disabled>Pilih Status ...</option>
                        <option value="1">MASUK</option>
                        <option value="2">LIBUR</option>
                        <option value="3">IZIN</option>
                        <option value="4">CUTI</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="checklist" class="col-2 col-form-label">Checklist</label>
                    <div class="col-10">
                        <input type="checkbox" id="checklist_addon" style="width: 20px; height: 20px;" name="checklist_addon" value="1">
                        <label for="checklist_addon" style="font-size: 20px"> Add On</label>
                    </div>
                </div>

                <div class="form-group m-b-0">
                <div class="col-12">
                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">SAVE</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection