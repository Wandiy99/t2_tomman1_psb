@extends('layout')

@section('content')
@include('partial.alerts')
<style>
th, td {
text-align: center;
vertical-align: middle;
}
</style>
<div class="row">
<div class="col-md-12 col-lg-12 col-sm-12">
<div class="white-box">
<div class="row row-in">
<div class="col-lg-3 col-sm-6 row-in-br">
<div class="col-in row">
<?php
$target_pt1 = "230";
$real_pt1 = $get_jml_teknisi[0]->jumlah;
$percentage = round($real_pt1/$target_pt1*100);
?>
<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="V"></i>
<h5 class="text-muted vb">TEKNISI PROV ({{ $percentage }}%)</h5> </div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h3 class="counter text-right m-t-15 text-primary">{{ $get_jml_teknisi[0]->jumlah }}</h3> </div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="progress">

<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%"> <span class="sr-only">{{ $percentage }}% Complete (success)</span> </div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 row-in-br">
<div class="col-in row">
<?php
$real_pt1 = $get_jml_teknisi[0]->jumlah;
$hadir_pt1 = $get_jml_teknisi_hadir[0]->jumlah;
$percentage = round($hadir_pt1/$real_pt1*100);
?>
<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
<h5 class="text-muted vb">HADIR ({{ $percentage }}%)</h5> </div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h3 class="counter text-right m-t-15 text-info">{{ $get_jml_teknisi_hadir[0]->jumlah }}</h3> </div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="progress">

<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%"> <span class="sr-only">{{ $percentage }}% Complete (success)</span> </div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 row-in-br">
<div class="col-in row">
<?php
$approved = $get_jml_teknisi_hadir_approved[0]->jumlah;
$percentage = round($approved/$real_pt1*100);
$percentage_ready = $percentage;
?>
<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
<h5 class="text-muted vb">APPROVED ({{ $percentage }}%)</h5> </div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h3 class="counter text-right m-t-15 text-success">{{ $approved }}</h3> </div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="progress">
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%"> <span class="sr-only">{{ $percentage }}% Complete (success)</span> </div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 row-in-br">
<div class="col-in row">
<?php
$absen = $real_pt1-$hadir_pt1;
$percentage = round($absen/$real_pt1*100);
?>
<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
<h5 class="text-muted vb">ABSEN ({{ $percentage }}%)</h5> </div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h3 class="counter text-right m-t-15 text-danger">{{ $absen }}</h3> </div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="progress">
<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%"> <span class="sr-only">{{ $percentage }}% Complete (success)</span> </div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-5  b-0">
<div class="col-in row">
<?php
$not_approved = $get_jml_teknisi_hadir_not_approved[0]->jumlah;
$percentage = round($not_approved/$hadir_pt1*100);
?>
<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
<h5 class="text-muted vb">NOT APPROVED ({{ $percentage }}%)</h5> </div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h3 class="counter text-right m-t-15 text-danger">{{ $not_approved }}</h3> </div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="progress">
<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%"> <span class="sr-only">{{ $percentage }}% Complete (success)</span> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-12 col-lg-9 col-sm-12">
<div class="white-box">
<h3 class="box-title">KEHADIRAN TEKNISI
<div class="col-md-3 col-sm-4 col-xs-6 pull-right">
<select class="form-control pull-right row b-none">
<option>{{ date('l , d F Y') }}</option>
</select>
</div>
</h3>
<div class="row sales-report">
<div class="col-md-6 col-sm-6 col-xs-6">
<h2>{{ $date }}</h2>
<p>REPORT KEHADIRAN TEKNISI PT1</p>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 ">
<h1 class="text-right text-success m-t-20">{{ $percentage_ready }}%</h1> </div>
</div>
<div class="table-responsive">
<table class="table">
<thead>
<tr>
<th>#</th>
<th>SEKTOR</th>
<th>TEKNISI PROV</th>
<th>PT 1</th>
<th>SABER</th>
<th>HADIR</th>
<th>ABSEN</th>
<th>APPROVED</th>
<th>NOT APPROVED</th>
<th>% APPROVED</th>
<th>SCHEDULE H+1</th>
<th>%</th>
</tr>
</thead>
<tbody>
<?php
$total = 0;
$total_hadir = 0;
$total_approved = 0;
$total_not_approved = 0;
$total_absen = 0;
$total_h_1 = 0;
$total_pt1 = 0;
$total_saber = 0;
?>
@foreach ($get_jml_teknisi_sektor as $num => $result)
<?php
$percent_approved = @round($result->jml_approved/$result->jumlah*100);
$absen = $result->jumlah - $result->jml_hadir;
$total_absen += $absen;
$total_h_1 += $result->jml_scheduled_besok;
?>
<tr>
<td>{{ ++$num }}</td>
<td class="txt-oflo">{{ $result->title ? : 'UNMAPPING' }}</td>
<td class="txt-oflo"><a href="/ListTeknisi/{{ $result->title ? : 'UNMAPPING' }}/1">{{ $result->jumlah }}</a></td>
<td class="txt-oflo"><a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=PT1&date={{ $date }}">{{ $result->jml_teknisi_pt1 }}</a></td>
<td class="txt-oflo"><a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=SABER&date={{ $date }}">{{ $result->jml_teknisi_saber }}</a></td>
<td class="txt-oflo"><a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=HADIR&date={{ $date }}">{{ $result->jml_hadir }}</a></td>
<td class="txt-oflo">{{ $absen }}</td>
<td class="txt-oflo"><a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=APPROVED&date={{ $date }}">{{ $result->jml_approved }}</a></td>
<td class="txt-oflo"><a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=NOT_APPROVED&date={{ $date }}">{{ $result->jml_not_approved }}</a></td>
<td class="txt-oflo"><span class="label label-success label-rounded">{{ $percent_approved }}%</span></td>
<td class="txt-oflo">{{ $result->jml_scheduled_besok }}</td>
<?php
$percent_h_1 = round($result->jml_scheduled_besok/$result->jumlah*100);
?>
<td class="txt-oflo"><span class="label label-success label-rounded">{{ $percent_h_1 }}%</span></td>
</tr>
<?php
$total += $result->jumlah;
$total_hadir += $result->jml_hadir;
$total_approved += $result->jml_approved;
$total_not_approved += $result->jml_not_approved;
$total_pt1 += $result->jml_teknisi_pt1;
$total_saber += $result->jml_teknisi_saber;
?>
@endforeach
<?php
$total_percent_approved = round($total_approved/$total*100);
$total_percent_h_1 = round($total_h_1/$total*100);
?>
<tr>
<td colspan="2">TOTAL</td>
<td><a href="/ListTeknisi/ALL/1">{{ $total }}</a></td>
<td><a href="/dashboardKehadiranList?sektor=ALL&status=PT1&date={{ $date }}">{{ $total_pt1 }}</a></td>
<td><a href="/dashboardKehadiranList?sektor=ALL&status=SABER&date={{ $date }}">{{ $total_saber }}</a></td>
<td><a href="/dashboardKehadiranList?sektor=ALL&status=HADIR&date={{ $date }}">{{ $total_hadir }}</a></td>
<td>{{ $total_absen }}</td>
<td><a href="/dashboardKehadiranList?sektor=ALL&status=APPROVED&date={{ $date }}">{{ $total_approved }}</a></td>
<td><a href="/dashboardKehadiranList?sektor=ALL&status=NOT_APPROVED&date={{ $date }}">{{ $total_not_approved }}</a></td>
<td><span class="label label-success label-rounded">{{ $total_percent_approved }}%</span></td>
<td>{{ $total_h_1 }}</td>
<td><span class="label label-success label-rounded">{{ $total_percent_h_1 }}%</span></td>
</tr>
</tbody>
</table>Check all the tech</div>
</div>
</div>
<div class="col-md-5 col-lg-3 col-sm-6 col-xs-12">
<div class="row">
<div class="col-md-12">
<div class="white-box slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 380px;"><ul class="country-state slimscrollcountry" style="overflow: hidden; width: auto; height: 380px;">
<li>
<h2>6350</h2> <small>Hadir</small>
<div class="pull-right">48% <i class="fa fa-level-up text-success"></i></div>
<div class="progress">
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:48%;"> <span class="sr-only">48% Complete</span></div>
</div>
</li>
<li>
<h2>6350</h2> <small>Tidak Hadir</small>
<div class="pull-right">48% <i class="fa fa-level-up text-success"></i></div>
<div class="progress">
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:48%;"> <span class="sr-only">48% Complete</span></div>
</div>
</li>
<li>
<h2>3250</h2> <small>Approved</small>
<div class="pull-right">98% <i class="fa fa-level-up text-success"></i></div>
<div class="progress">
<div class="progress-bar progress-bar-inverse" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:98%;"> <span class="sr-only">98% Complete</span></div>
</div>
</li>
<li>
<h2>1250</h2> <small>Not Approved</small>
<div class="pull-right">75% <i class="fa fa-level-down text-danger"></i></div>
<div class="progress">
<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:75%;"> <span class="sr-only">75% Complete</span></div>
</div>
</li>

</ul><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 145.349px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="bg-theme-dark m-b-15">
<div class="row weather p-20">
<div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 m-t-40">
<h3>&nbsp;</h3>
<h1>30<sup>°C</sup></h1>
<p class="text-white">BANJARMASIN, KALSEL</p>
</div>
<div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 text-right"> <i class="wi wi-day-cloudy-high"></i>
<br>
<br>
<b class="text-white">SUNNEY DAY</b>
<p class="w-title-sub">{{ date('d M Y') }}</p>
</div>
</div>
</div>
</div>

</div>

<div class="row">
<div class="col-md-12">
<div class="white-box">
<table class="table">
<tr>
<th>Kota</th>
<th>Pagi</th>
<th>Siang</th>
<th>Malam</th>
<th>Dini Hari</th>
</tr>
@foreach ($getCuaca as $cuaca)
<tr>
<td>{{ $cuaca->kota }}</td>
<td><i class="{{ $cuaca->icon_jam0 }}"></i></td>
<td><i class="{{ $cuaca->icon_jam6 }}"></i></td>
<td><i class="{{ $cuaca->icon_jam12 }}"></i></td>
<td><i class="{{ $cuaca->icon_jam16 }}"></i></td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
</div>
</div>
@endsection
