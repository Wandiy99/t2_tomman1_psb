@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">
    <div class="white-box">
      <div class="row row-in">
        <div class="col-lg-3 col-sm-6 row-in-br">
          <div class="col-in row">
                @php
                    $target_pt1 = "230";
                    $real_pt1 = $get_jml_teknisi[0]->jumlah;
                    $percentage = round($real_pt1/$target_pt1*100);
                @endphp
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i class="linea-icon linea-basic" data-icon="V"></i>
              <h5 class="text-muted vb">TEKNISI PROV ({{ $percentage }}%)</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-primary">{{ $get_jml_teknisi[0]->jumlah }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                  <span class="sr-only">{{ $percentage }}% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 row-in-br">
          <div class="col-in row">
                @php
                    $real_pt1 = $get_jml_teknisi[0]->jumlah;
                    $hadir_pt1 = $get_jml_teknisi_hadir[0]->jumlah;
                    $percentage = round($hadir_pt1/$real_pt1*100);
                @endphp
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i class="linea-icon linea-basic" data-icon=""></i>
              <h5 class="text-muted vb">HADIR ({{ $percentage }}%)</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-info">{{ $get_jml_teknisi_hadir[0]->jumlah }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                  <span class="sr-only">{{ $percentage }}% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 row-in-br">
          <div class="col-in row">
                @php
                    $approved = $get_jml_teknisi_hadir_approved[0]->jumlah;
                    $percentage = round($approved/$real_pt1*100);
                    $percentage_ready = $percentage;
                @endphp
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i class="linea-icon linea-basic" data-icon=""></i>
              <h5 class="text-muted vb">APPROVED ({{ $percentage }}%)</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-success">{{ $approved }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                  <span class="sr-only">{{ $percentage }}% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 row-in-br">
          <div class="col-in row">
                @php
                    $absen = $real_pt1-$hadir_pt1;
                    $percentage = round($absen/$real_pt1*100);
                @endphp
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i class="linea-icon linea-basic" data-icon=""></i>
              <h5 class="text-muted vb">ABSEN ({{ $percentage }}%)</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">{{ $absen }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                  <span class="sr-only">{{ $percentage }}% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-5  b-0">
          <div class="col-in row">
                @php
                    $not_approved = $get_jml_teknisi_hadir_not_approved[0]->jumlah;
                    $percentage = round($not_approved/$hadir_pt1*100);
                @endphp
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i class="linea-icon linea-basic" data-icon=""></i>
              <h5 class="text-muted vb">NOT APPROVED ({{ $percentage }}%)</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">{{ $not_approved }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                  <span class="sr-only">{{ $percentage }}% Complete (success)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-12 col-sm-12">
    <div class="white-box">
      <h3 class="box-title">KEHADIRAN TEKNISI <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
          <select class="form-control pull-right row b-none">
            <option>{{ date('l , d F Y') }}</option>
          </select>
        </div>
      </h3>
      <div class="row sales-report">
        <div class="col-md-6 col-sm-6 col-xs-6">
          <h2>{{ $date }}</h2>
          <p>REPORT KEHADIRAN PT1 </p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 ">
          <h1 class="text-right text-success m-t-20">{{ $percentage_ready }}%</h1>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th rowspan="2" class="align-middle">#</th>
              <th rowspan="2" class="align-middle">SEKTOR</th>
              <th rowspan="2" class="align-middle">JML</th>
              <th rowspan="2" class="align-middle">SCHEDULED HI</th>
              <th rowspan="2" class="align-middle">%</th>
              <th rowspan="2" class="align-middle">HADIR</th>
              <th rowspan="2" class="align-middle">%  <br />to Schedule</th>
              <th rowspan="2" class="align-middle">%  <br />to JML</th>
              <th rowspan="2" class="align-middle">ABSEN</th>
              <th rowspan="2" class="align-middle">%</th>
              <th rowspan="2" class="align-middle">NOT APPROVED</th>
              <th rowspan="2" class="align-middle">%</th>
              <th rowspan="2" class="align-middle">BELUM<br />ABSEN</th>
              <th rowspan="2" class="align-middle">%</th>
              <th rowspan="2" class="align-middle">APPROVED</th>
              <th rowspan="2" class="align-middle">%</th>
              <th rowspan="2" class="align-middle">SCHEDULED H+1</th>
              <th rowspan="2" class="align-middle">%</th>
            </tr>
          </thead>
          <tbody>
          <?php
            $total = $jml_scheduled = $jml_hadir = $jml_absen = $jml_not_approved = $jml_belum_absen = $jml_approved = $jml_scheduled_besok = 0;
          ?>
          @foreach ($get_jml_teknisi_sektor_1 as $num => $result)
          <?php
            $total += $result->jumlah;
            $jml_scheduled += $result->scheduled_hi;
            $jml_hadir += $result->hadir;
            $jml_absen += $result->absen;
            $jml_not_approved += $result->not_approved;
            $jml_approved += $result->approved;
            $jml_scheduled_besok += $result->scheduled_besok;

            $percent_scheduled = @($result->scheduled_hi/$result->jumlah)*100;
            $percent_hadir = @($result->hadir/$result->scheduled_hi)*100;
            $percent_hadir_1 = @($result->hadir/$result->jumlah)*100;
            $percent_absen = @($result->absen/$result->jumlah)*100;
            $percent_not_approved = @($result->not_approved/$result->jumlah)*100;
            $percent_belum_absen = @($result->belum_absen/$result->jumlah)*100;
            $percent_approved = @($result->approved/$result->jumlah)*100;
            $percent_scheduled_besok = @($result->not_approved/$result->jumlah);
            $absen = $result->jumlah-$result->scheduled_hi;
            $percent_absen = @($absen/$result->jumlah)*100;
            $jml_absen += $absen;
          ?>
          <tr>
            <td class="txt-oflo">{{ ++$num }}</td>
            <td class="txt-oflo">{{ $result->title }}</td>
            <td class="txt-oflo">{{ $result->jumlah }}</td>
            <td class="txt-oflo">{{ $result->scheduled_hi }}</td>
            <td class="txt-oflo">{{ round($percent_scheduled) }}%</td>
            <td class="txt-oflo">{{ $result->hadir }}</td>
            <td class="txt-oflo">{{ round($percent_hadir) }}%</td>
            <td class="txt-oflo">{{ round($percent_hadir_1) }}%</td>
            <td class="txt-oflo">{{ $absen }}</td>
            <td class="txt-oflo">{{ round($percent_absen) }}%</td>
            <td class="txt-oflo">{{ $result->not_approved }}</td>
            <td class="txt-oflo">{{ round($percent_not_approved) }}%</td>
            <td class="txt-oflo">{{ $result->belum_absen }}</td>
            <td class="txt-oflo">{{ round($percent_belum_absen) }}%</td>
            <td class="txt-oflo">{{ $result->approved }}</td>
            <td class="txt-oflo">{{ round($percent_approved) }}%</td>
            <td class="txt-oflo">{{ $result->scheduled_besok }}</td>
            <td class="txt-oflo">{{ round($percent_scheduled_besok) }}%</td>
          </tr>
          @endforeach
          <?php
            $percent_jml_scheduled = @($jml_scheduled/$total)*100;
            $percent_jml_hadir = @($jml_hadir/$jml_scheduled)*100;
            $percent_jml_hadir_1 = @($jml_hadir/$total)*100;
            $percent_jml_absen = @($jml_absen/$total)*100;
            $percent_approved = @($jml_approved/$jml_scheduled)*100;
            $percent_not_approved = @($jml_not_approved/$jml_scheduled)*100;
            $percent_jml_scheduled_besok = @($jml_scheduled_besok/$total)*100;
          ?>
          </tbody>
          <tfooter>
            <tr>
              <th colspan="2" class="align-middle">TOTAL</th>
              <th class="align-middle">{{ $total }}</th>
              <th class="align-middle">{{ $jml_scheduled }}</th>
              <th class="align-middle">{{ round($percent_jml_scheduled) }}%</th>
              <th class="align-middle">{{ $jml_hadir }}</th>
              <th class="align-middle">{{ round($percent_jml_hadir) }}%</th>
              <th class="align-middle">{{ round($percent_jml_hadir_1) }}%</th>
              <th class="align-middle">{{ $jml_absen }}</th>
              <th class="align-middle">{{ round($percent_jml_absen) }}%</th>
              <th class="align-middle">{{ $jml_not_approved }}</th>
              <th class="align-middle">{{ round($percent_not_approved,1) }}%</th>
              <th class="align-middle">{{ $jml_belum_absen }}</th>
              <th class="align-middle">{{ round($percent_belum_absen,1) }}%</th>
              <th class="align-middle">{{ $jml_approved }}</th>
              <th class="align-middle">{{ round($percent_approved,1) }}%</th>
              <th class="align-middle">{{ $jml_scheduled_besok }}</th>
              <th class="align-middle">{{ round($percent_jml_scheduled_besok,1) }}%</th>
            </tr>
          </tfooter>
        </table>Check all the tech
      </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-9 col-sm-12">
    <div class="white-box">
      <h3 class="box-title">KEHADIRAN TEKNISI <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
          <select class="form-control pull-right row b-none">
            <option>{{ date('l , d F Y') }}</option>
          </select>
        </div>
      </h3>
      <div class="row sales-report">
        <div class="col-md-6 col-sm-6 col-xs-6">
          <h2>{{ $date }}</h2>
          <p>REPORT KEHADIRAN SEKTOR</p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 ">
          <h1 class="text-right text-success m-t-20">{{ $percentage_ready }}%</h1>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th rowspan="2" class="align-middle">#</th>
              <th rowspan="2" class="align-middle">SEKTOR</th>
              <th rowspan="2" class="align-middle">TEKNISI <br />PROV </th>
              <th rowspan="2" class="align-middle">PT 1</th>
              <th rowspan="2" class="align-middle">SABER</th>
              <th rowspan="2" class="align-middle">HADIR</th>
              <th rowspan="2" class="align-middle">ABSEN</th>
              <th rowspan="2" class="align-middle">APPROVED</th>
              <th rowspan="2" class="align-middle">NOT <br />APPROVED </th>
              <th rowspan="2" class="align-middle">%<br />APPROVED</th>
              <th colspan="2" class="align-middle">SCHEDULE</th>
              <th rowspan="2" class="align-middle">ACH %</th>
            </tr>
            <tr>
              <th class="align-middle">H+1 PT1 </th>
              <th class="align-middle">H+1 MO </th>
            </tr>
          </thead>
          <tbody>
            @php
                $total = $total_hadir = $total_approved = $total_not_approved = $total_absen = $total_h_1 = $total_h1_mo = $total_pt1 = $total_saber = 0;
            @endphp
            @foreach ($get_jml_teknisi_sektor as $num => $result)
            @php
                $percent_approved = @round($result->jml_approved/$result->jumlah*100);
                $absen = $result->jumlah - $result->jml_hadir;
                $total_absen += $absen;
                $total_h_1 += $result->jml_scheduled_besok;
                $total_h1_mo += $result->jml_scheduled_besok_mo;
            @endphp
            <tr>
                <td>{{ ++$num }}</td>
                <td class="txt-oflo">{{ $result->title ? : 'UNMAPPING' }}</td>
                <td class="txt-oflo">
                    <a href="/ListTeknisi/{{ $result->title ? : 'UNMAPPING' }}/1">{{ $result->jumlah }}</a>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=PT1&date={{ $date }}">{{ $result->jml_teknisi_pt1 }}</a>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=SABER&date={{ $date }}">{{ $result->jml_teknisi_saber }}</a>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=HADIR&date={{ $date }}">{{ $result->jml_hadir }}</a>
                </td>
                <td class="txt-oflo">{{ $absen }}</td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=APPROVED&date={{ $date }}">{{ $result->jml_approved }}</a>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=NOT_APPROVED&date={{ $date }}">{{ $result->jml_not_approved }}</a>
                </td>
                <td class="txt-oflo">
                    <span class="label label-success label-rounded">{{ $percent_approved }}%</span>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=H1_PT1&date={{ $date }}">{{ $result->jml_scheduled_besok }}</a>
                </td>
                <td class="txt-oflo">
                    <a href="/dashboardKehadiranList?sektor={{ $result->title ? : 'UNMAPPING' }}&status=H1_MO&date={{ $date }}">{{ $result->jml_scheduled_besok_mo }}</a>
                </td>
              @php
                $percent_h_1 = round(($result->jml_scheduled_besok + $result->jml_scheduled_besok_mo)/$result->jumlah*100);
              @endphp
              <td class="txt-oflo">
                <span class="label label-success label-rounded">{{ $percent_h_1 }}%</span>
              </td>
            </tr>
            @php
                $total += $result->jumlah;
                $total_hadir += $result->jml_hadir;
                $total_approved += $result->jml_approved;
                $total_not_approved += $result->jml_not_approved;
                $total_pt1 += $result->jml_teknisi_pt1;
                $total_saber += $result->jml_teknisi_saber;
            @endphp
            @endforeach
            @php
                $total_percent_approved = round($total_approved/$total*100);
                $total_percent_h_1 = round(($total_h_1 + $total_h1_mo) / $total_hadir * 100);
            @endphp

            <tr>
                <td colspan="2">TOTAL</td>
                <td>
                    <a href="/ListTeknisi/ALL/1">{{ $total }}</a>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=PT1&date={{ $date }}">{{ $total_pt1 }}</a>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=SABER&date={{ $date }}">{{ $total_saber }}</a>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=HADIR&date={{ $date }}">{{ $total_hadir }}</a>
                </td>
                <td>{{ $total_absen }}</td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=APPROVED&date={{ $date }}">{{ $total_approved }}</a>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=NOT_APPROVED&date={{ $date }}">{{ $total_not_approved }}</a>
                </td>
                <td>
                    <span class="label label-success label-rounded">{{ $total_percent_approved }}%</span>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=H1_PT1&date={{ $date }}">{{ $total_h_1 }}</a>
                </td>
                <td>
                    <a href="/dashboardKehadiranList?sektor=ALL&status=H1_MO&date={{ $date }}">{{ $total_h1_mo }}</a>
                </td>
                <td>
                    <span class="label label-success label-rounded">{{ $total_percent_h_1 }}%</span>
                </td>
            </tr>
          </tbody>
        </table>Check all the tech
      </div>
    </div>
  </div>
  <div class="col-md-5 col-lg-3 col-sm-6 col-xs-12">
    <div class="row">
      <div class="col-md-12">
        <div class="bg-theme-dark m-b-15">
          <div class="row weather p-20">
            <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 m-t-40">
              <h3>&nbsp;</h3>
              <h1>30 <sup>°C</sup>
              </h1>
              <p class="text-white">BANJARMASIN, KALSEL</p>
            </div>
            <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 text-right">
              <i class="wi wi-day-cloudy-high"></i>
              <br>
              <br>
              <b class="text-white">SUNNEY DAY</b>
              <p class="w-title-sub">{{ date('d M Y') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="white-box">
          <table class="table">
            <tr>
              <th>Kota</th>
              <th>Pagi</th>
              <th>Siang</th>
              <th>Malam</th>
              <th>Dini Hari</th>
            </tr> @foreach ($getCuaca as $cuaca) <tr>
              <td>{{ $cuaca->kota }}</td>
              <td>
                <i class="{{ $cuaca->icon_jam0 }}"></i>
              </td>
              <td>
                <i class="{{ $cuaca->icon_jam6 }}"></i>
              </td>
              <td>
                <i class="{{ $cuaca->icon_jam12 }}"></i>
              </td>
              <td>
                <i class="{{ $cuaca->icon_jam16 }}"></i>
              </td>
            </tr> @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div> @endsection
