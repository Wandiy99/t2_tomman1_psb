@extends('layout')

@section('content')
@include('partial.alerts')

@if (session('auth')->level==2 || session('auth')->level==15 || session('auth')->level == 46)
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">Data User Karyawan</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
					<tr>
    	  				<th>NO</th>	
      					<th>NIK</th>	
      					<th>NAMA</th>	
      					<th>LABORCODE</th>	
      					<th>CREWID</th>
						<th>ID SOBI</th>
						<th>REGU</th>
						<th>MITRA</th>
						<th>ATASAN 1</th>	
      					<th>CAT JOB</th>	
      					<th>STATUS</th>	
      					<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($getAll as $num => $data)
      				<tr>
    	  				<td>{{ ++$num }}</td>
    	  				<td>{{ $data->nik }}</td>
    	  				<td>{{ $data->nama }}</td>
    	  				<td>{{ $data->laborcode }}</td>
						<td>{{ $data->idcrew }}</td>
						<td>{{ $data->id_sobi }}</td>
						<td>{{ $data->tim }}</td>
						<td>{{ $data->mitra_amija }}</td>
    	  				<td>{{ $data->atasan_1 }}</td>
    	  				<td>{{ $data->CAT_JOB }}</td>
    	  				<td>
							@if($data->ACTIVE == 1)
							Active
							@elseif($data->ACTIVE == 2)
							Banned
							@else
							Non Active / Resign
							@endif
						</td>
						<td>
							<a href="/employee/{{ $data->id_people }}" data-original-title="Edit">
							<i class="fa fa-edit text-inverse m-r-10"></i></a>
						</td>
      				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable();
    });
</script>
@else
	<h1>Banyaki Bawa Beibadah Ja Parak Kiamat !!!</h1>
@endif
@endsection