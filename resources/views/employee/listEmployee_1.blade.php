@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DAFTAR TEKNISI {{ $sektor }} STATUS {{ $status }} TANGGAL {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td>NO</td>
                    <td>NIK</td>
                    <td>NAMA</td>
                    <td>SEKTOR</td>
                    <td>TIM</td>
                    <td>WAKTU ABSEN</td>
                    <td>WAKTU APPROVED</td>
                  </tr>
                </thead>
                <tbody>
                @foreach($list as $num => $result)
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->nik }}</td>
                    <td>{{ $result->nama }}</td>
                    <td>{{ $result->nama_sektor }}</td>
                    <td>{{ $result->nama_tim }}</td>
                    <td>{{ $result->waktu_absen }}</td>
                    <td>{{ $result->date_approval }}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
          lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
@endsection