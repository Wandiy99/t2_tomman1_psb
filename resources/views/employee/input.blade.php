@extends('layout')

@section('content')
@if ($auth->level==2 || $auth->level==15 || session('auth')->level == 46)
  @include('partial.alerts')
  <link href="/elitetheme/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
  <link href="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<style>
	.panel-content {
		padding : 10px;
	}
</style>
	<h3>
		<a href="/employee" class="btn btn-sm btn-default">
			<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
	Employee
	</h3>
	<div class="panel panel-primary">
		<div class="panel-heading">Input</div>
		<div class="panel panel-body">
			<form method="post" action="/employee/input">

				<input type="hidden" name="id_people" id="id_people" class="form-control" value="{{ @$data->id_people }}" />

				<div class="form-group col-md-4">
					<label for="nik">NIK</label> <a style="color: red !important">* Wajib Diisi</a>
						<input type="text" name="nik" id="nik" class="form-control" value="{{ @$data->nik }}" />
						{!! $errors->first('nik','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="nama">NAMA</label> <a style="color: red !important">* Wajib Diisi</a>
						<input type="text" name="nama" class="form-control" value="{{ @$data->nama }}" />
						{!! $errors->first('nama','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="nama">NO HANDPHONE</label>
						<input type="number" name="no_telp" class="form-control" value="{{ @$data->no_telp }}" />
						{!! $errors->first('no_telp','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="nama">CHAT ID TELEGRAM</label>
						<input type="text" name="chat_id" class="form-control" value="{{ @$data->chat_id }}" />
						{!! $errors->first('chat_id','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="laborcode">LABORCODE</label>
						<input type="text" name="laborcode" class="form-control" value="{{ @$data->laborcode }}" />
						{!! $errors->first('laborcode','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="idcrew">CREW ID</label>
						<input type="text" name="idcrew" class="form-control" value="{{ @$data->idcrew }}" />
						{!! $errors->first('idcrew','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="id_sobi">ID SOBI</label>
						<input type="text" name="id_sobi" class="form-control" value="{{ @$data->id_sobi }}" />
						{!! $errors->first('id_sobi','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="laborcode">TANGGAL LAHIR</label>
						<input type="text" id="datepicker-autoclose" placeholder="yyyy-mm-dd" name="tgl_lahir" class="form-control" value="{{ @$data->tgl_lahir }}" />
						{!! $errors->first('tgl_lahir','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="laborcode">TEMPAT LAHIR</label>
						<input type="text" name="tempat_lahir" class="form-control" value="{{ @$data->tempat_lahir }}" />
						{!! $errors->first('tempat_lahir','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="laborcode">ALAMAT</label>
						<input type="text" name="alamat" class="form-control" value="{{ @$data->alamat }}" />
						{!! $errors->first('alamat','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="cat_job">CAT JOB</label> <a style="color: red !important">* Wajib Diisi</a>
						<select name="cat_job" class="form-control select2">
							<option value=""> -- </option>
							@foreach ($getDistinctCATJOB as $CATJOB)
								<?php
								if ($CATJOB->id == @$data->CAT_JOB){
									$selected = 'selected = "selected"';
								} else {
									$selected = '';
								}
								?>
							<option value="{{ $CATJOB->id }}" {{ $selected }}>{{ $CATJOB->text }}</option>
							@endforeach
						</select>
					{!! $errors->first('cat_job','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
				<label for="laborcode">STO</label> <a style="color: red !important">* Wajib Diisi</a>
					<select class="form-control select2" name="sto" id="sto">
						<option value="" selected disabled>Pilih area STO</option>
						@foreach ($get_sto as $area_sto)
						<option data-subtext="description 1" value="{{ $area_sto->id }}" <?php if (@$area_sto->id==@$data->sto) { echo "Selected"; } else { echo ""; } ?>>{{ @$area_sto->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('sto','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
				<label for="atasan_1">ATASAN 1</label> <a style="color: red !important">* Wajib Diisi</a>
					<input type="text" name="atasan_1" class="form-control" value="{{ @$data->atasan_1 }}" />
					{!! $errors->first('atasan_1','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="cat_job">Sub Directorat</label> <a style="color: red !important">* Wajib Diisi</a>
					<select name="cat_job" class="form-control select2">
						<option value=""> -- </option>
						@foreach ($getDistinctSubDirectorat as $SubDirectorat)
						<?php
						if ($SubDirectorat->id == @$data->sub_directorat){
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $SubDirectorat->id }}" {{ $selected }}>{{ $SubDirectorat->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('cat_job','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
				<label for="mitra_amija">Mitra Amija</label>
					<select class="form-control select2" name="mitra_amija">
						<option value=""> -- </option>
						@foreach ($getMitraAmija as $MitraAmija)
						<?php
						if ($MitraAmija->mitra_amija == @$data->mitra_amija){
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $MitraAmija->mitra_amija }}" {{ $selected }}>{{ $MitraAmija->mitra_amija }}</option>
						@endforeach
					</select>
					{!! $errors->first('mitra_amija','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
				<label for="nik_amija">Witel</label>
					<select class="form-control select2" name="witel">
						<option value="" selected disabled> - Pilih Witel - </option>
						@foreach ($getWitel as $Witel)
						<?php
						if ($Witel->witel == @$data->Witel_New){
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $Witel->witel }}" {{ $selected }}>{{ $Witel->witel }}</option>
						@endforeach
					</select>
					{!! $errors->first('witel','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="nik_amija">NIK Amija</label> <a style="color: red !important">* Wajib Diisi</a>
					<input type="text" name="nik_amija" class="form-control" value="{{ @$data->nik_amija }}">
					{!! $errors->first('nik_amija','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
				<label for="active">Status</label> <a style="color: red !important">* Wajib Diisi</a>
					<select class="form-control select2" name="active" id="active">
						<option value="" selected disabled> - Pilih Status Karyawan - </option>
						@foreach ($getStatus as $status)
						<option data-subtext="description 1" value="{{ $status->id }}" <?php if (@$status->id==@$data->ACTIVE) { echo "Selected"; } else { echo ""; } ?>>{{ @$status->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('active','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="mainsector">Main Sektor</label> <a style="color: red !important">* Wajib Diisi</a>
					<select name="mainsector" class="form-control select2">
						<option value="" selected disabled> - Pilih Sektor - </option>
						@foreach ($getSektor as $sektor)
						<?php
						if ($sektor->id == @$data->mainsector) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $sektor->id }}" {{ $selected }}>{{ $sektor->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('mainsector','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="id_regu">Regu</label> <a style="color: red !important">* Wajib Diisi</a>
					<select name="id_regu" class="form-control select2">
						<option value="" selected disabled> Pilih Regu </option>
						@foreach ($getRegu as $regu)
						<?php
						if ($regu->id == @$data->id_regu) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $regu->id }}" {{ $selected }}>{{ $regu->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('id_regu','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-4">
					<label for="posisi_id">Posisi</label> <a style="color: red !important">* Wajib Diisi</a>
					<select name="posisi_id" class="form-control select2">
						<option value="" selected disabled> - Pilih Posisi - </option>
						@foreach ($getPosisi as $posisi)
						<?php
						if ($posisi->id == @$data->posisi_id){
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
						?>
						<option value="{{ $posisi->id }}" {{ $selected }}>{{ $posisi->text }}</option>
						@endforeach
					</select>
					{!! $errors->first('posisi_id','<p><span class="label label-danger">:message</span></p>') !!}
				</div>

				<div class="form-group col-md-12" style="text-align: center;">
					<br/><br/>
					@if(@$data->pt1 == 1)
					<input type="checkbox" checked id="pt1" style="width: 40px; height: 25px;" name="skillset[pt1]" value="1">
					<label for="PT1" style="font-size: 30px"> PT 1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@else
					<input type="checkbox" id="pt1" style="width: 40px; height: 25px;" name="skillset[pt1]" value="1">
					<label for="PT1" style="font-size: 30px"> PT 1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@endif
					@if(@$data->saber == 1)
					<input type="checkbox" checked id="saber" style="width: 40px; height: 25px;" name="skillset[saber]" value="1">
					<label for="saber" style="font-size: 30px"> Saber</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@else
					<input type="checkbox" id="saber" style="width: 40px; height: 25px;" name="skillset[saber]" value="1">
					<label for="saber" style="font-size: 30px"> Saber</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@endif
					@if(@$data->nte == 1)
					<input type="checkbox" checked id="nte" style="width: 40px; height: 25px;" name="skillset[nte]" value="1">
					<label for="nte" style="font-size: 30px"> NTE</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@else
					<input type="checkbox" id="nte" style="width: 40px; height: 25px;" name="skillset[nte]" value="1">
					<label for="nte" style="font-size: 30px"> NTE</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					@endif 
					@if(@$data->mytech == 1)
					<input type="checkbox" checked id="mytech" style="width: 40px; height: 25px;" name="skillset[mytech]" value="1">
					<label for="mytech" style="font-size: 30px"> MY-Tech</label>
					@else
					<input type="checkbox" id="mytech" style="width: 40px; height: 25px;" name="skillset[mytech]" value="1">
					<label for="mytech" style="font-size: 30px"> MY-Tech</label>
					@endif
				</div>

				<div class="form-group col-md-12">
					<input type="submit" name="submit" class="btn btn-lg btn-success" />
				</div>

			</form>
		</div>
	</div>
@else
    <h1>Banyaki Bawa Beibadah Ja Parak Kiamat !!!</h1>
@endif
@endsection
@section('plugins')
<script src="/elitetheme/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
    $(function() {
        $(".select2").select2();

        $('#datepicker-autoclose').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
    });
</script>
@endsection
