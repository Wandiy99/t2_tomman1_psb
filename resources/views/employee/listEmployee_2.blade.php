@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DAFTAR TEKNISI {{ $sektor }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td>NO</td>
                    <td>NIK</td>
                    <td>NAMA</td>
                    <td>SEKTOR</td>
                    <td>STO</td>
                    <td>TIM</td>
                    <td>PT1</td>
                    <td>SABER</td>
                    <td>ACTION</td>
                    <td>BESOK</td>
                  </tr>
                </thead>
                <tbody>
                @foreach($list as $num => $result)
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->a_nik }}</td>
                    <td>{{ $result->nama }}</td>
                    <td>{{ $result->title }}</td>
                    <td>{{ $result->a_sto }}</td>
                    <td>{{ $result->uraian }}</td>
                    <td>{{ $result->pt1 }}</td>
                    <td>{{ $result->saber }}</td>
                    <td>
                      <a href="/employee/{{ $result->id_people }}" data-toggle="tooltip" data-original-title="Edit Employee">
                        <i class="fa fa-edit text-inverse m-r-10"></i>
                      </a>
                      <a href="/schedule/{{ $result->a_nik }}/{{ $result->id_regu }}/{{ $result->a_sto }}" data-toggle="tooltip" data-original-title="Jadwal">
                        <i class="fa fa-users text-inverse m-r-10"></i>
                      </a>
                    </td>
                    <td>{{ $result->schedule_status }}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
          </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
@endsection
