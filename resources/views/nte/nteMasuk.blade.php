@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="panel panel-default">
    <div class="panel-heading">
      Input NTE Masuk
    </div>
  <div class="panel-body">
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
  <input name="transaksi" type="hidden" value="1">
  <input name="status" type="hidden" value="2">
    {{-- <input name="_method" type="hidden" value="PUT">
    <input type="hidden" name="sns" value="[]" /> --}} 
    <div class="form-group">
      <label class="control-label" for="input-gudang_id">Masuk ke Gudang : </label>
      <input name="gudang" type="hidden" id="input-gudang_id" class="form-control" value="{{ $data->gudang_id or '' }}" />
    </div>
    {{-- <div class="form-group">
      <label class="control-label" for="input-transaksi">Transaksi</label>
      <input name="transaksi" type="hidden" id="input-transaksi" class="form-control" value="{{ $data->position_type or '' }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="{{ $data->status or '' }}" />
    </div> --}}
    <div class="form-group">
      <label class="control-label">Catatan</label>
      <textarea name="catatan" class="form-control" rows="3">{{ $data->catatan or '' }}</textarea>
    </div>
    @if (!isset($data->id))
    <div class="form-group">
      <label class="control-label" for="uploadstock">Import Excel</label>
      <input type="file" id="uploadstock" name="uploadstock" class="form-control"/>
    </div>
    @endif
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    </div>
    </div>
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  <div class="panel panel-primary">
  <div class="panel-heading">Recorded NTE</div>
  <div class="list-group">
    @foreach($sn as $no => $sn_nte)
      <div class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
        <strong>{{ $sn_nte->sn }}</strong>
        <span class="badge">{{ $sn_nte->jenis_nte }}</span>
      </div>
    @endforeach
  </div>
</div>
  @endif
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script src="/bower_components/select2/select2.min.js"></script>  
  <script src="/bower_components/excel/zip/WebContent/zip.js"></script>
  <script src="/bower_components/excel/async-master/dist/async.js"></script>
  <script src="/bower_components/excel/underscore.js"></script>
  <script src="/bower_components/excel/xlsxParser.js"></script>
  <script>
    $(function() {
      // var data_sn = [];
      // $("#tes").change(function(e){
      //   data_sn = [];
      //   $(".loading").css({"visibility":"visible"});
      //   $(".removable").remove();
      //   var file = document.getElementById('tes').files[0];
      //   zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
      //   xlsxParser.parse(file).then(function(data) {
      //     var count = 0;
      //     for(var i=1;i<data.length;i++){
      //       var sn = data[i][2], type = data[i][1];
      //       if(sn){
      //         data_sn.push({type: type, sn: sn});
      //         $(".list-group").append("<div class='list-group-item removable'><span>" + type + "<span> <span>" + sn + "<span></div>");
      //         count++;
      //       } 
      //     }
          
      //     $("#jumlah").append("<span class=removable>SN Found : " + count + "</span>");
      //     $(".loading").css({"visibility":"hidden"});
      //   }, function(err) {
      //     alert.log('error', err);
      //   });
      // })
      // $('#submit-form').submit(function() {
      //   var gudang = document.getElementById('input-gudang_id').value;
      //   var transaksi = document.getElementById('input-transaksi').value;
      //   var status = document.getElementById('input-status').value;
      //   if(gudang == '' || transaksi == '' || status == '' || data_sn == '')
      //     return false;
      //   $('input[name=sns]').val(JSON.stringify(data_sn));
      // });
      var data = <?= json_encode($gudangs) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Gudang',
          allowClear: true,
        }
      }
      $('#input-gudang_id').select2(select2Options());
      $('#input-transaksi').select2({
          data: [{'id':1,'text':'Masuk'}, {'id':2,'text':'Keluar'}],
          allowClear: true,
          placeholder: 'Input Transaksi'
        });
      var status = $('#input-status').select2({
        data: [{'id':1,'text':'Useable'}, {'id':2,'text':'Un-Useable'}],
        allowClear: true,
        placeholder: 'Input status'
      });
      
	  })
  </script>
@endsection
