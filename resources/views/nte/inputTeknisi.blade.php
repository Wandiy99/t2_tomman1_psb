@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="modal fade" id="modal-confirm" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title" id="myModalLabel">Select NTE</h4>
            </div>
            
            <div class="modal-body">
              <ul id="searchlist" class="list-group">
                <li class="list-group-item" v-repeat="item in items">
                  <strong v-text="item.sn"></strong><br>
                  <strong v-text="item.jenis_nte"></strong>

                  <div class="widget-products-footer input-group has-success" style="width:200px;float:right;">
                    <span class="input-group-btn" v-if="item.vueSelected">
                      <button type="button" class="btn btn-success btn-outline" v-on:click="onDelete(item)">Hapus</button>
                    </span>
                    <span class="input-group-btn" v-if="item.vueSelected == null">
                      <button type="button" class="btn btn-success btn-outline" v-on:click="onSelect(item)">Tambah</button>
                    </span>
                  </div>
                </li>
              </ul>
            </div>
            
            <div class="modal-footer">
              <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
          
          </div>
        </div>
      </div>

      <div class="panel panel-default">
          <div class="panel-heading">
              NTE Teknisi
          </div>
          <div class="panel-body">
              <form method="post" class="form-vertical">
                  <div class="form-group">
                      <div class="col-md-6">
                        <label for="nama_gudang" class="control-label">Nama Gudang</label>
                        <input name="nama_gudang" type="text" id="nama_gudang" class="form-control" />
                        {!! $errors->first('nama_gudang','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-6">
                        <label for="nama_regu" class="control-label">Regu</label>
                        <input name="nama_regu" type="text" id="nama_regu" class="form-control" />
                        {!! $errors->first('nama_regu','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-6">
                        <label for="nik_scmt" class="control-label">NIK SCMT (Teknisi)</label>
                        <input name="nik_scmt" type="text" id="nik_scmt" class="form-control" />
                        {!! $errors->first('nik_scmt','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6">
                        <label class="control-label" for="kategori_nte">Kondisi NTE</label>
                        <select class="form-control" name="kategori_nte" id="kategori_nte">
                        <option value="" selected disabled>Pilih Kondisi</option>
                        <option value="Baru"> Baru </option>
                        <option value="Bekas"> Bekas </option>
                        </select>
                        {!! $errors->first('kategori_nte','<span class="label label-danger">:message</span>') !!}
                    </div>
                  </div>

                   <div class="form-group">
                      <div class="col-md-12">
                        <label for="sn_nte" class="control-label">SN NTE</label>
                        <input name="sn_nte[]" type="text" id="sn_nte" class="form-control" />
                        {!! $errors->first('sn_nte','<span class="label label-danger">:message</span>') !!}
                      </div>
                  </div>

                  <!-- <div class="form-group">
                      <div class="col-md-12">
                        <button type="button" class="btn btn-danger btn-outline" data-toggle="modal" data-target="#modal-confirm">
                          <span class="glyphicon glyphicon-list"></span>
                          Tambah NTE
                        </button>
                      </div>
                  </div> -->
                  
                  <div class="form-group">
                      <div class="col-md-12">
                        <br/>
                          <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-refresh"></span>
                              <span>Proses</span>
                          </button>
                      </div>
                  </div>
              </form>

              <!-- <div class="form-group">
                <ul id="Itemlist" class="list-group">
                  <li class="list-group-item" v-for="item in items" v-if="item.vueSelected">
                    <span class="badge" v-text="item.jenis_nte"></span>
                    <strong v-text="item.sn"></strong>
                  </li>
                </ul>
              </div> -->
          </div>
      </div>
@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {
            var gudang = <?= json_encode($gudang) ?>;
            $('#nama_gudang').select2({
                data: gudang,
                placeholder: 'Pilih Gudang',
                allowClear: true,
            });

            var regu = <?= json_encode($regu) ?>;
            $('#nama_regu').select2({
                data: regu,
                placeholder: 'Pilih Regu',
                allowClear: true,
            });

            $('#nama_gudang').change(function(){
                var id_gudang = $('#nama_gudang').val(),
                    url       = '/nte-list/'+id_gudang;

                $.ajax({
                    url : url,
                    dataType : 'json',
                    success : function(data){
                      var material = data;
                      $('#sn_nte').select2({
                          data: material,
                          placeholder: 'Pilih SN NTE',
                          allowClear: true,
                          multiple: true,
                      });
                    }
                })
               
            })
          

            // var materials = <?= json_encode($materials) ?>;
            // var vueData = {
            //   items: []
            // };

            // vueData.items = materials;
            
            // // $.ajax({
            // //   url: "/ajaxEmployeeMitra/"+idMitra,
            // //   method: 'GET',
            // //   success: function (data) {
            // //     vueData.items = data;
            // //   },
            // //   error: function (error) {
            // //     console.log(error);
            // //   }
            // // });

            // new Vue({
            //   el: '#modal-confirm',
            //   data: vueData,
            //   methods: {
            //     onSelect: function(item) {
            //       item.vueSelected = 1;
            //     },
            //     onDelete: function(item) {
            //       item.vueSelected = null;
            //     }
            //   }
            // });

            // new Vue({
            //   el: '#Itemlist',
            //   data: vueData
            // });

            // console.log(vueData.items[0].sn)

        });
    </script>
@endsection