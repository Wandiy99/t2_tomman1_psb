@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="panel panel-default">
          <div class="panel-heading">
              RETURN NTE
          </div>

          <div class="panel-body">

          <form method="GET">
            <div class="row">
            <div class="input-group col-sm-6">
                <input align="right" type="text" class="form-control inputSearch" placeholder="Search Serial Number . . ." name="search_sn" id="search_sn" />
                <span class="input-group-btn">
                    <button class="btn btn-default search">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
            </div>
            </form>

            <form method="POST" enctype="multipart/form-data" class="form-horizontal">

            <div class="form-group">
            <div class="col-md-12">
                <label class="control-label" for="keterangan">Keterangan</label>
                <select class="form-control" name="keterangan" id="keterangan">
                <option value="" selected disabled>Pilih Keterangan</option>
                <option value="SalahGudang"> Bukan Stok Gudang / Salah Gudang </option>
                <option value="ReturnGudang"> Return NTE Teknisi ke Gudang </option>
                <option value="ReturnTeknisi"> Revisi Laporan Teknisi </option>
                </select>
                {!! $errors->first('keterangan','<span class="label label-danger">:message</span>') !!}
            </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                <label for="nama_gudang" class="control-label">Gudang</label>
                <input name="nama_gudang" type="text" id="nama_gudang" class="form-control" value="{{ @$data->nama_gudang ? : '' }}" disabled/>
                {!! $errors->first('uraian','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                <label for="uraian" class="control-label">Regu</label>
                <input name="uraian" type="text" id="uraian" class="form-control" value="{{ @$data->uraian ? : '' }}" disabled/>
                {!! $errors->first('uraian','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                <label for="ndem" class="control-label">No Order</label>
                <input name="ndem" type="text" id="ndem" class="form-control" value="{{ @$data->Ndem ? : '' }}" disabled/>
                {!! $errors->first('ndem','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                <label for="jenis_nte" class="control-label">Type NTE</label>
                <input name="jenis_nte" type="text" id="jenis_nte" class="form-control" value="{{ @$data->jenis_nte ? : '' }}" disabled/>
                {!! $errors->first('jenis_nte','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-12">
                    <label for="sn" class="control-label">SN NTE</label>
                    <input name="sn" type="text" id="sn" class="form-control" value="{{ @$data->sn ? : '' }}" disabled/>
                    {!! $errors->first('sn','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                <label for="typente" class="control-label">NTE ID</label>
                <input name="typente" type="text" id="typente" class="form-control" value="{{ @$data->nte_jenis_kat ? : '' }}" readonly/>
                {!! $errors->first('nte_jenis_kat','<span class="label label-danger">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-refresh"></span>
                        <span>Proses</span>
                    </button>
                </div>
            </div>
            </form>

          </div>
      </div>
@endsection

@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
@endsection