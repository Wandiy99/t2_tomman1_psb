@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    STOK NTE
  </h3>
  <div class="input-group">
    <div>
      <input type="hidden" class="form-control" id="gudang">
    </div>
  </div>
  <div id="txtHint">
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  
  <script>
  $(function() {
    var data = <?= json_encode($gudang) ?>;
    var status = $('#gudang').select2({
      data: data,
      placeholder: 'Pilih Gudang'
    });
    $('#gudang').change(function(e) {
      $(".loading").css({"visibility":"visible"});
      var url = '../nte/getSumNteType/' + e.val;
      $('.removeable').remove();
      $.getJSON(url, function(data) {
        data.forEach(function(item) {
          $("#txtHint").append("<div class='list-group-item removeable'><span>" + item.jn + "</span> <span class='badge'>" + item.totalType + "</span></div>");
        });
        $(".loading").css({"visibility":"hidden"});
      })
    });
  });
  </script>
@endsection