@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">LIST NTE TYPE {{ $nte }} WAREHOUSE {{ $gudang }}</h3>
        <div class="table-responsive">
			<table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
					<tr>
  						<th>No</th>
  						<th>Tgl Keluar</th>
						<th>Tim</th>
						<th>Mitra</th>
						<th>Jenis Layanan</th>
  						<th>Tipe NTE</th>
  						<th>SN</th>
						<th>Kondisi NTE</th>
						<th>Work Order</th>
						<th>Inet</th>
						<th>Pelanggan</th>
						<th>Alamat Pelanggan</th>
  						<th>Petugas</th>
						<th>NIK SCMT</th>
						<th>Teknisi</th>
  					</tr>
				</thead>
				<tbody>
				@foreach ($query as $num => $result)
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>{{ $result->tanggal_keluar ?: "#N/A" }}</td>
						<td>{{ $result->uraian ?: "#N/A" }}</td>
						<td>{{ $result->mitra_amija_pt ?: "#N/A" }}</td>
              			<td>{{ $result->jenis_layanan ?: "#N/A" }}</td>
  						<td>{{ $result->jenis_nte ?: "#N/A" }}</td>
  						<td>{{ $result->sn ?: "#N/A" }}</td>
						<td>{{ $result->kategori_nte ?: "#N/A" }}</td>
						<td>{{ $result->Ndem ?: "#N/A" }}</td>
						<td>{{ $result->internet ?: $result->Service_ID ?: "#N/A" }}</td>
						<td>{{ $result->orderName ?: $result->Customer_Name ?: "#N/A" }}</td>
						<td>{{ $result->orderAddr ?: "#N/A" }}</td>
  						<td>{{ $result->nama ?: "#N/A" }}</td>
						<td>{{ $result->nik_scmt ?: "#N/A" }}</td>
						<td>{{ $result->position_key ?: "#N/A" }}</td>
  					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                }
            ]
        });
    });
</script>
@endsection