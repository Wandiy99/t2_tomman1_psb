@extends('layout')

@section('content')
  @include('partial.alerts')
  <style type="text/css">
    th {
      font-size: 11px;
      font-weight: bold;
      text-align: center;
      vertical-align: middle;
    }
    td {
      vertical-align: middle;
      text-align: center;
    }
  </style>
  <h3>
    <a href="/nte/input" class="btn btn-info">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
    <a href="/nte/return/teknisi" class="btn btn-success">
      <span class="glyphicon glyphicon-retweet"></span>
    </a>
    NTE ( <i>Network Terminal Equipment</i> )
  </h3>

  <div class="panel panel-default">
    <div class="panel-heading">STOCK NTE</div>
    <div class="table-responsive">
      <table class="table table-bordered">
        <tr>
          <th class="align-middle" rowspan="2">AREA</th>
          <th class="align-middle" rowspan="2">TOTAL</th>
          <th class="align-middle" colspan="19">ONT</th>
          <th class="align-middle" colspan="8">STB</th>
          <th class="align-middle" colspan="1">INDIBOX</th>
          <th class="align-middle" colspan="2">PLC</th>
          <th class="align-middle" colspan="1">IP CAM</th>
          <th class="align-middle" colspan="2">WIFI EXTENDER</th>
          <th class="align-middle" colspan="3">ADSL</th>
        </tr>
        <tr>
          <th class="align-middle">ZTE F609</th>
          <th class="align-middle">ZTE F609 V5.3</th>
          <th class="align-middle">ZTE F660</th>
          <th class="align-middle">ZTE F670L</th>
          <th class="align-middle">ZTE F670 V.1 PREMIUM</th>
          <th class="align-middle">ZTE F670 V.2 PREMIUM</th>
          <th class="align-middle">ZTE F670 WMS</th>
          <th class="align-middle">HUAWEI HG8245</th>
          <th class="align-middle">HUAWEI HG8245A</th>
          <th class="align-middle">HUAWEI HG8245H</th>
          <th class="align-middle">HUAWEI HG8245H5</th>
          <th class="align-middle">HUAWEI HG8245U PREMIUM</th>
          <th class="align-middle">ALU G240W</th>
          <th class="align-middle">ALU I240W</th>
          <th class="align-middle">ALU I240WA</th>
          <th class="align-middle">NOKIA G-240W-F</th>
          <th class="align-middle">NOKIA G-240W-L</th>
          <th class="align-middle">FIBERHOME HG6243C</th>
          <th class="align-middle">FIBERHOME HG6145F</th>
          <th class="align-middle">ZTE B860V5</th>
          <th class="align-middle">ZTE B600</th>
          <th class="align-middle">ZTE B700</th>
          <th class="align-middle">ZTE B760H</th>
          <th class="align-middle">ZTE B860H</th>
          <th class="align-middle">ZTE B860H V2.1</th>
          <th class="align-middle">HUAWEI EC6108V9</th>
          <th class="align-middle">FIBERHOME HG680H</th>
          <th class="align-middle">OTT BOX AKARI AX 117A TV</th>
          <th class="align-middle">D-LINK DHP-W310AV</th>
          <th class="align-middle">TPLINK PA4010 KIT</th>
          <th class="align-middle">IPCAM AZUSTAR</th>
          <th class="align-middle">SMART N300</th>
          <th class="align-middle">EW-7438RPN</th>
          <th class="align-middle">ZYXEL P660-HN</th>
          <th class="align-middle">ZTE H108NV40</th>
          <th class="align-middle">TPLINK TDW8961N</th>
        </tr>
        @foreach ($stock_nte as $data)
        <tr>
          <td>{{ $data->nama_gudang ? : '#N/A' }}</td>
          <td><a href="/nte/list/gudang/ALL/{{ $data->nama_gudang }}">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/gudang/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/gudang/ztef609v53/{{ $data->nama_gudang }}">{{ $data->ztef609v53 }}</a></td>
          <td><a href="/nte/list/gudang/ztef660/{{ $data->nama_gudang }}">{{ $data->ztef660 }}</a></td>
          <td><a href="/nte/list/gudang/ztef670l/{{ $data->nama_gudang }}">{{ $data->ztef670l }}</a></td>
          <td><a href="/nte/list/gudang/ztef670v1/{{ $data->nama_gudang }}">{{ $data->ztef670v1 }}</a></td>
          <td><a href="/nte/list/gudang/ztef670v2/{{ $data->nama_gudang }}">{{ $data->ztef670v2 }}</a></td>
          <td><a href="/nte/list/gudang/ztef670wms/{{ $data->nama_gudang }}">{{ $data->ztef670wms }}</a></td>
          <td><a href="/nte/list/gudang/hwihg8245/{{ $data->nama_gudang }}">{{ $data->hwihg8245 }}</a></td>
          <td><a href="/nte/list/gudang/hwhg8245a/{{ $data->nama_gudang }}">{{ $data->hwhg8245a }}</a></td>
          <td><a href="/nte/list/gudang/hwhg8245h/{{ $data->nama_gudang }}">{{ $data->hwhg8245h }}</a></td>
          <td><a href="/nte/list/gudang/hwhg8245h5/{{ $data->nama_gudang }}">{{ $data->hwhg8245h5 }}</a></td>
          <td><a href="/nte/list/gudang/hg8245u/{{ $data->nama_gudang }}">{{ $data->hg8245u }}</a></td>
          <td><a href="/nte/list/gudang/alug240w/{{ $data->nama_gudang }}">{{ $data->alug240w }}</a></td>
          <td><a href="/nte/list/gudang/alui240w/{{ $data->nama_gudang }}">{{ $data->alui240w }}</a></td>
          <td><a href="/nte/list/gudang/alui240wa/{{ $data->nama_gudang }}">{{ $data->alui240wa }}</a></td>
          <td><a href="/nte/list/gudang/nokiag240wf/{{ $data->nama_gudang }}">{{ $data->nokiag240wf }}</a></td>
          <td><a href="/nte/list/gudang/nokiag240wl/{{ $data->nama_gudang }}">{{ $data->nokiag240wl }}</a></td>
          <td><a href="/nte/list/gudang/fhhg6243c/{{ $data->nama_gudang }}">{{ $data->fhhg6243c }}</a></td>
          <td><a href="/nte/list/gudang/fhhg6145f/{{ $data->nama_gudang }}">{{ $data->fhhg6145f }}</a></td>
          <td><a href="/nte/list/gudang/zteb860v5/{{ $data->nama_gudang }}">{{ $data->zteb860v5 }}</a></td>
          <td><a href="/nte/list/gudang/zteb600/{{ $data->nama_gudang }}">{{ $data->zteb600 }}</a></td>
          <td><a href="/nte/list/gudang/zteb700/{{ $data->nama_gudang }}">{{ $data->zteb700 }}</a></td>
          <td><a href="/nte/list/gudang/zteb760h/{{ $data->nama_gudang }}">{{ $data->zteb760h }}</a></td>
          <td><a href="/nte/list/gudang/zteb860h/{{ $data->nama_gudang }}">{{ $data->zteb860h }}</a></td>
          <td><a href="/nte/list/gudang/zteb860hv21/{{ $data->nama_gudang }}">{{ $data->zteb860hv21 }}</a></td>
          <td><a href="/nte/list/gudang/hwiec6108v9/{{ $data->nama_gudang }}">{{ $data->hwiec6108v9 }}</a></td>
          <td><a href="/nte/list/gudang/fhhg680h/{{ $data->nama_gudang }}">{{ $data->fhhg680h }}</a></td>
          <td><a href="/nte/list/gudang/indibox/{{ $data->nama_gudang }}">{{ $data->indibox }}</a></td>
          <td><a href="/nte/list/gudang/dhpw310av/{{ $data->nama_gudang }}">{{ $data->dhpw310av }}</a></td>
          <td><a href="/nte/list/gudang/pa4010kit/{{ $data->nama_gudang }}">{{ $data->pa4010kit }}</a></td>
          <td><a href="/nte/list/gudang/ipcam/{{ $data->nama_gudang }}">{{ $data->ipcam }}</a></td>
          <td><a href="/nte/list/gudang/smartn300/{{ $data->nama_gudang }}">{{ $data->smartn300 }}</a></td>
          <td><a href="/nte/list/gudang/ew743rpn/{{ $data->nama_gudang }}">{{ $data->ew743rpn }}</a></td>
          <td><a href="/nte/list/gudang/tdw8961n/{{ $data->nama_gudang }}">{{ $data->tdw8961n }}</a></td>
          <td><a href="/nte/list/gudang/zteh108nv40/{{ $data->nama_gudang }}">{{ $data->zteh108nv40 }}</a></td>
          <td><a href="/nte/list/gudang/p660hn/{{ $data->nama_gudang }}">{{ $data->p660hn }}</a></td>
       </tr>
        @endforeach
      </table>
    </div>
  </div> 

  <div class="panel panel-default">
    <div class="panel-heading">STOCK NTE TEKNISI</div>
    <div class="table-responsive">
      <table class="table table-bordered">
        <tr>
          <th class="align-middle" rowspan="2">AREA</th>
          <th class="align-middle" rowspan="2">TOTAL</th>
          <th class="align-middle" colspan="19">ONT</th>
          <th class="align-middle" colspan="8">STB</th>
          <th class="align-middle" colspan="1">INDIBOX</th>
          <th class="align-middle" colspan="2">PLC</th>
          <th class="align-middle" colspan="1">IP CAM</th>
          <th class="align-middle" colspan="2">WIFI EXTENDER</th>
          <th class="align-middle" colspan="3">ADSL</th>
        </tr>
        <tr>
          <th class="align-middle">ZTE F609</th>
          <th class="align-middle">ZTE F609 V5.3</th>
          <th class="align-middle">ZTE F660</th>
          <th class="align-middle">ZTE F670L</th>
          <th class="align-middle">ZTE F670 V.1 PREMIUM</th>
          <th class="align-middle">ZTE F670 V.2 PREMIUM</th>
          <th class="align-middle">ZTE F670 WMS</th>
          <th class="align-middle">HUAWEI HG8245</th>
          <th class="align-middle">HUAWEI HG8245A</th>
          <th class="align-middle">HUAWEI HG8245H</th>
          <th class="align-middle">HUAWEI HG8245H5</th>
          <th class="align-middle">HUAWEI HG8245U PREMIUM</th>
          <th class="align-middle">ALU G240W</th>
          <th class="align-middle">ALU I240W</th>
          <th class="align-middle">ALU I240WA</th>
          <th class="align-middle">NOKIA G-240W-F</th>
          <th class="align-middle">NOKIA G-240W-L</th>
          <th class="align-middle">FIBERHOME HG6243C</th>
          <th class="align-middle">FIBERHOME HG6145F</th>
          <th class="align-middle">ZTE B860V5</th>
          <th class="align-middle">ZTE B600</th>
          <th class="align-middle">ZTE B700</th>
          <th class="align-middle">ZTE B760H</th>
          <th class="align-middle">ZTE B860H</th>
          <th class="align-middle">ZTE B860H V2.1</th>
          <th class="align-middle">HUAWEI EC6108V9</th>
          <th class="align-middle">FIBERHOME HG680H</th>
          <th class="align-middle">OTT BOX AKARI AX 117A TV</th>
          <th class="align-middle">D-LINK DHP-W310AV</th>
          <th class="align-middle">TPLINK PA4010 KIT</th>
          <th class="align-middle">IPCAM AZUSTAR</th>
          <th class="align-middle">SMART N300</th>
          <th class="align-middle">EW-7438RPN</th>
          <th class="align-middle">ZYXEL P660-HN</th>
          <th class="align-middle">ZTE H108NV40</th>
          <th class="align-middle">TPLINK TDW8961N</th>
        </tr>
        @foreach ($stock_nte_teknisi as $data)
        <tr>
          <td>{{ $data->nama_gudang }}</td>
          <td><a href="/nte/list/teknisi/ALL/{{ $data->nama_gudang }}">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/teknisi/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef609v53/{{ $data->nama_gudang }}">{{ $data->ztef609v53 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef660/{{ $data->nama_gudang }}">{{ $data->ztef660 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef670l/{{ $data->nama_gudang }}">{{ $data->ztef670l }}</a></td>
          <td><a href="/nte/list/teknisi/ztef670v1/{{ $data->nama_gudang }}">{{ $data->ztef670v1 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef670v2/{{ $data->nama_gudang }}">{{ $data->ztef670v2 }}</a></td>
          <td><a href="/nte/list/teknisi/ztef670wms/{{ $data->nama_gudang }}">{{ $data->ztef670wms }}</a></td>
          <td><a href="/nte/list/teknisi/hwihg8245/{{ $data->nama_gudang }}">{{ $data->hwihg8245 }}</a></td>
          <td><a href="/nte/list/teknisi/hwhg8245a/{{ $data->nama_gudang }}">{{ $data->hwhg8245a }}</a></td>
          <td><a href="/nte/list/teknisi/hwhg8245h/{{ $data->nama_gudang }}">{{ $data->hwhg8245h }}</a></td>
          <td><a href="/nte/list/teknisi/hwhg8245h5/{{ $data->nama_gudang }}">{{ $data->hwhg8245h5 }}</a></td>
          <td><a href="/nte/list/teknisi/hg8245u/{{ $data->nama_gudang }}">{{ $data->hg8245u }}</a></td>
          <td><a href="/nte/list/teknisi/alug240w/{{ $data->nama_gudang }}">{{ $data->alug240w }}</a></td>
          <td><a href="/nte/list/teknisi/alui240w/{{ $data->nama_gudang }}">{{ $data->alui240w }}</a></td>
          <td><a href="/nte/list/teknisi/alui240wa/{{ $data->nama_gudang }}">{{ $data->alui240wa }}</a></td>
          <td><a href="/nte/list/teknisi/nokiag240wf/{{ $data->nama_gudang }}">{{ $data->nokiag240wf }}</a></td>
          <td><a href="/nte/list/teknisi/nokiag240wl/{{ $data->nama_gudang }}">{{ $data->nokiag240wl }}</a></td>
          <td><a href="/nte/list/teknisi/fhhg6243c/{{ $data->nama_gudang }}">{{ $data->fhhg6243c }}</a></td>
          <td><a href="/nte/list/teknisi/fhhg6145f/{{ $data->nama_gudang }}">{{ $data->fhhg6145f }}</a></td>
          <td><a href="/nte/list/teknisi/zteb860v5/{{ $data->nama_gudang }}">{{ $data->zteb860v5 }}</a></td>
          <td><a href="/nte/list/teknisi/zteb600/{{ $data->nama_gudang }}">{{ $data->zteb600 }}</a></td>
          <td><a href="/nte/list/teknisi/zteb700/{{ $data->nama_gudang }}">{{ $data->zteb700 }}</a></td>
          <td><a href="/nte/list/teknisi/zteb760h/{{ $data->nama_gudang }}">{{ $data->zteb760h }}</a></td>
          <td><a href="/nte/list/teknisi/zteb860h/{{ $data->nama_gudang }}">{{ $data->zteb860h }}</a></td>
          <td><a href="/nte/list/teknisi/zteb860hv21/{{ $data->nama_gudang }}">{{ $data->zteb860hv21 }}</a></td>
          <td><a href="/nte/list/teknisi/hwiec6108v9/{{ $data->nama_gudang }}">{{ $data->hwiec6108v9 }}</a></td>
          <td><a href="/nte/list/teknisi/fhhg680h/{{ $data->nama_gudang }}">{{ $data->fhhg680h }}</a></td>
          <td><a href="/nte/list/teknisi/indibox/{{ $data->nama_gudang }}">{{ $data->indibox }}</a></td>
          <td><a href="/nte/list/teknisi/dhpw310av/{{ $data->nama_gudang }}">{{ $data->dhpw310av }}</a></td>
          <td><a href="/nte/list/teknisi/pa4010kit/{{ $data->nama_gudang }}">{{ $data->pa4010kit }}</a></td>
          <td><a href="/nte/list/teknisi/ipcam/{{ $data->nama_gudang }}">{{ $data->ipcam }}</a></td>
          <td><a href="/nte/list/teknisi/smartn300/{{ $data->nama_gudang }}">{{ $data->smartn300 }}</a></td>
          <td><a href="/nte/list/teknisi/ew743rpn/{{ $data->nama_gudang }}">{{ $data->ew743rpn }}</a></td>
          <td><a href="/nte/list/teknisi/tdw8961n/{{ $data->nama_gudang }}">{{ $data->tdw8961n }}</a></td>
          <td><a href="/nte/list/teknisi/zteh108nv40/{{ $data->nama_gudang }}">{{ $data->zteh108nv40 }}</a></td>
          <td><a href="/nte/list/teknisi/p660hn/{{ $data->nama_gudang }}">{{ $data->p660hn }}</a></td>
       </tr>
        @endforeach
      </table>
    </div>
  </div> 

  <div class="panel panel-default">
  <div class="panel-heading">NTE USED</div>
    <div class="table-responsive">
      <table class="table table-bordered">
        <tr>
          <th class="align-middle" rowspan="2">AREA</th>
          <th class="align-middle" rowspan="2">TOTAL</th>
          <th class="align-middle" colspan="19">ONT</th>
          <th class="align-middle" colspan="8">STB</th>
          <th class="align-middle" colspan="1">INDIBOX</th>
          <th class="align-middle" colspan="2">PLC</th>
          <th class="align-middle" colspan="1">IP CAM</th>
          <th class="align-middle" colspan="2">WIFI EXTENDER</th>
          <th class="align-middle" colspan="3">ADSL</th>
        </tr>
        <tr>
          <th class="align-middle">ZTE F609</th>
          <th class="align-middle">ZTE F609 V5.3</th>
          <th class="align-middle">ZTE F660</th>
          <th class="align-middle">ZTE F670L</th>
          <th class="align-middle">ZTE F670 V.1 PREMIUM</th>
          <th class="align-middle">ZTE F670 V.2 PREMIUM</th>
          <th class="align-middle">ZTE F670 WMS</th>
          <th class="align-middle">HUAWEI HG8245</th>
          <th class="align-middle">HUAWEI HG8245A</th>
          <th class="align-middle">HUAWEI HG8245H</th>
          <th class="align-middle">HUAWEI HG8245H5</th>
          <th class="align-middle">HUAWEI HG8245U PREMIUM</th>
          <th class="align-middle">ALU G240W</th>
          <th class="align-middle">ALU I240W</th>
          <th class="align-middle">ALU I240WA</th>
          <th class="align-middle">NOKIA G-240W-F</th>
          <th class="align-middle">NOKIA G-240W-L</th>
          <th class="align-middle">FIBERHOME HG6243C</th>
          <th class="align-middle">FIBERHOME HG6145F</th>
          <th class="align-middle">ZTE B860V5</th>
          <th class="align-middle">ZTE B600</th>
          <th class="align-middle">ZTE B700</th>
          <th class="align-middle">ZTE B760H</th>
          <th class="align-middle">ZTE B860H</th>
          <th class="align-middle">ZTE B860H V2.1</th>
          <th class="align-middle">HUAWEI EC6108V9</th>
          <th class="align-middle">FIBERHOME HG680H</th>
          <th class="align-middle">OTT BOX AKARI AX 117A TV</th>
          <th class="align-middle">D-LINK DHP-W310AV</th>
          <th class="align-middle">TPLINK PA4010 KIT</th>
          <th class="align-middle">IPCAM AZUSTAR</th>
          <th class="align-middle">SMART N300</th>
          <th class="align-middle">EW-7438RPN</th>
          <th class="align-middle">ZYXEL P660-HN</th>
          <th class="align-middle">ZTE H108NV40</th>
          <th class="align-middle">TPLINK TDW8961N</th>
        </tr>
        @foreach ($stock_nte_used as $data)
        <tr>
          <td>{{ $data->nama_gudang }}</td>
          <td><a href="/nte/list/used/ALL/{{ $data->nama_gudang }}">{{ $data->jumlah }}</a></td>
          <td><a href="/nte/list/used/ztef609/{{ $data->nama_gudang }}">{{ $data->ztef609 }}</a></td>
          <td><a href="/nte/list/used/ztef609v53/{{ $data->nama_gudang }}">{{ $data->ztef609v53 }}</a></td>
          <td><a href="/nte/list/used/ztef660/{{ $data->nama_gudang }}">{{ $data->ztef660 }}</a></td>
          <td><a href="/nte/list/used/ztef670l/{{ $data->nama_gudang }}">{{ $data->ztef670l }}</a></td>
          <td><a href="/nte/list/used/ztef670v1/{{ $data->nama_gudang }}">{{ $data->ztef670v1 }}</a></td>
          <td><a href="/nte/list/used/ztef670v2/{{ $data->nama_gudang }}">{{ $data->ztef670v2 }}</a></td>
          <td><a href="/nte/list/used/ztef670wms/{{ $data->nama_gudang }}">{{ $data->ztef670wms }}</a></td>
          <td><a href="/nte/list/used/hwihg8245/{{ $data->nama_gudang }}">{{ $data->hwihg8245 }}</a></td>
          <td><a href="/nte/list/used/hwhg8245a/{{ $data->nama_gudang }}">{{ $data->hwhg8245a }}</a></td>
          <td><a href="/nte/list/used/hwhg8245h/{{ $data->nama_gudang }}">{{ $data->hwhg8245h }}</a></td>
          <td><a href="/nte/list/used/hwhg8245h5/{{ $data->nama_gudang }}">{{ $data->hwhg8245h5 }}</a></td>
          <td><a href="/nte/list/used/hg8245u/{{ $data->nama_gudang }}">{{ $data->hg8245u }}</a></td>
          <td><a href="/nte/list/used/alug240w/{{ $data->nama_gudang }}">{{ $data->alug240w }}</a></td>
          <td><a href="/nte/list/used/alui240w/{{ $data->nama_gudang }}">{{ $data->alui240w }}</a></td>
          <td><a href="/nte/list/used/alui240wa/{{ $data->nama_gudang }}">{{ $data->alui240wa }}</a></td>
          <td><a href="/nte/list/used/nokiag240wf/{{ $data->nama_gudang }}">{{ $data->nokiag240wf }}</a></td>
          <td><a href="/nte/list/used/nokiag240wl/{{ $data->nama_gudang }}">{{ $data->nokiag240wl }}</a></td>
          <td><a href="/nte/list/used/fhhg6243c/{{ $data->nama_gudang }}">{{ $data->fhhg6243c }}</a></td>
          <td><a href="/nte/list/used/fhhg6145f/{{ $data->nama_gudang }}">{{ $data->fhhg6145f }}</a></td>
          <td><a href="/nte/list/used/zteb860v5/{{ $data->nama_gudang }}">{{ $data->zteb860v5 }}</a></td>
          <td><a href="/nte/list/used/zteb600/{{ $data->nama_gudang }}">{{ $data->zteb600 }}</a></td>
          <td><a href="/nte/list/used/zteb700/{{ $data->nama_gudang }}">{{ $data->zteb700 }}</a></td>
          <td><a href="/nte/list/used/zteb760h/{{ $data->nama_gudang }}">{{ $data->zteb760h }}</a></td>
          <td><a href="/nte/list/used/zteb860h/{{ $data->nama_gudang }}">{{ $data->zteb860h }}</a></td>
          <td><a href="/nte/list/used/zteb860hv21/{{ $data->nama_gudang }}">{{ $data->zteb860hv21 }}</a></td>
          <td><a href="/nte/list/used/hwiec6108v9/{{ $data->nama_gudang }}">{{ $data->hwiec6108v9 }}</a></td>
          <td><a href="/nte/list/used/fhhg680h/{{ $data->nama_gudang }}">{{ $data->fhhg680h }}</a></td>
          <td><a href="/nte/list/used/indibox/{{ $data->nama_gudang }}">{{ $data->indibox }}</a></td>
          <td><a href="/nte/list/used/dhpw310av/{{ $data->nama_gudang }}">{{ $data->dhpw310av }}</a></td>
          <td><a href="/nte/list/used/pa4010kit/{{ $data->nama_gudang }}">{{ $data->pa4010kit }}</a></td>
          <td><a href="/nte/list/used/ipcam/{{ $data->nama_gudang }}">{{ $data->ipcam }}</a></td>
          <td><a href="/nte/list/used/smartn300/{{ $data->nama_gudang }}">{{ $data->smartn300 }}</a></td>
          <td><a href="/nte/list/used/ew743rpn/{{ $data->nama_gudang }}">{{ $data->ew743rpn }}</a></td>
          <td><a href="/nte/list/used/tdw8961n/{{ $data->nama_gudang }}">{{ $data->tdw8961n }}</a></td>
          <td><a href="/nte/list/used/zteh108nv40/{{ $data->nama_gudang }}">{{ $data->zteh108nv40 }}</a></td>
          <td><a href="/nte/list/used/p660hn/{{ $data->nama_gudang }}">{{ $data->p660hn }}</a></td>
       </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection