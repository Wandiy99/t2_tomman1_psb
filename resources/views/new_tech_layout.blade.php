@if(session('auth')->level <> 88 || session('auth')->level <> 0)
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tomman Tech V.2.0</title>
    @yield('header')

    <!-- Bootstrap core CSS -->
    <link rel="canonical" href="/bootstrap-5.0.0-beta1/site/content/docs/5.0/examples/dashboard/">
    <link href="/bootstrap-5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png">
    <meta name="theme-color" content="#7952b3">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .tomman{
        text-align: center;
        font-family: fantasy;
        color: #f5f5f5d6;
      }
      .uigradient{
        background: #BE93C5;
        background: -webkit-linear-gradient(to right, #7BC6CC, #BE93C5);
        background: linear-gradient(to right, #7BC6CC, #BE93C5);
      }
    </style>
    <link href="/bootstrap-5.0.0-beta1/site/content/docs/5.0/examples/dashboard/dashboard.css" rel="stylesheet">
  </head>
  <body>
    
<header class="navbar navbar-light sticky-top flex-md-nowrap p-0 shadow uigradient">
  <h3 class="col-md-3 col-lg-2 me-0 px-3 tomman">Tomman Tech V.2.0</h3>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <i class="navbar-toggler-icon" style="font-size: 0.4em;"></i>
    </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search . . ." aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
    </li>
  </ul>
</header>

<div class="container-fluid">
  <div class="row justify-content-md-center">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/">
              <span data-feather="home"></span>
              Beranda
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/home">
              <span data-feather="clipboard"></span>
              Home
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/profile">
              <span data-feather="user"></span>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/logout">
              <span data-feather="log-out"></span>
              Keluar
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Provisioning</span>
          <a class="link-secondary" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          {{-- <li class="nav-item">
            <a class="nav-link" href="/qcborneo/check">
              <span data-feather="search"></span>
              Cek Order QC
            </a>
          </li> --}}
          {{-- <li class="nav-item">
            <a class="nav-link" href="/qcborneo/amo">
              <span data-feather="search"></span>
              Cek ODP Pelanggan
            </a>
          </li> --}}
          <li class="nav-item">
            <a class="nav-link" href="/helpdesk_inbox">
              <span data-feather="inbox"></span>
              Inbox Fallout Helpdesk
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Assurance</span>
          <a class="link-secondary" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="/checktiket">
              <span data-feather="search"></span>
              Riwayat Pasang Baru
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/helpdesk/assurance/config">
              <span data-feather="inbox"></span>
              Inbox Config Helpdesk
            </a>
          </li>
        </ul>

        {{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Marina</span>
          <a class="link-secondary" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="/marina/tech">
              <span data-feather="list"></span>
              Order
            </a>
          </li>
        </ul> --}}

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Tambahan</span>
          <a class="link-secondary" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
        <li class="nav-item">
            {{-- <a class="nav-link" href="/ibooster">
              <span data-feather="star"></span>
              Ukur Ibooster
            </a> --}}
            <a class="nav-link" href="https://perwira.tomman.app/input_potensi_b/{{ session('auth')->id_karyawan }}">
              <span data-feather="star"></span>
              Lapor Potensi Bahaya
            </a>
          </li>
        </ul>
    </nav>
  </div>

<!-- Content -->
<br />
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-5">
@yield('content')
</main>
</div>


<!-- End Content -->
@else
<center><h5>User is Banned!</h5></center>
@endif

    <script src="/bootstrap-5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
    {{-- <script src="/bootstrap-5.0.0-beta1/site/content/docs/5.0/examples/dashboard/dashboard.js"></script>  --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"> --}}
    <script src="/bootstrap-5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <script src="/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
		$(document).ready(function () {
			if ("geolocation" in navigator) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					var accuracy = position.coords.accuracy;
					var roundedAccuracy = Math.round(accuracy * 100) / 100;
					
					console.log("Latitude:", lat);
					console.log("Longitude:", lng);
					console.log("Accuracy:", roundedAccuracy, "meters");

					$('#latitude').html(`${lat}`);
					$('#longitude').html(`${lng}`);
					$('#accuracy').html(`${roundedAccuracy}`);
				}, function(error) {
					switch(error.code) {
						case error.PERMISSION_DENIED:
							console.error("User denied the request for Geolocation.");
							break;
						case error.POSITION_UNAVAILABLE:
							console.error("Location information is unavailable.");
							break;
						case error.TIMEOUT:
							console.error("The request to get user location timed out.");
							break;
						case error.UNKNOWN_ERROR:
							console.error("An unknown error occurred.");
							break;
					}
				}, {
					enableHighAccuracy: true,
					maximumAge: 10000,
            		timeout: 5000,
				});
			} else {
				console.error("Geolocation is not available.");
			}

			var session = {!! json_encode(session('auth')) !!};
		
			function updateLocation() {
				navigator.geolocation.getCurrentPosition(function(position) {
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					var accuracy = position.coords.accuracy;
					var roundedAccuracy = Math.round(accuracy * 100) / 100;
			
					$.post('/update-location', {id_karyawan: session.id_karyawan, lat: lat, lng: lng, accuracy: roundedAccuracy, _token: '{{ csrf_token() }}'});
				});
			
				setTimeout(updateLocation, 5000);
			}
			updateLocation();
		});
	  </script>