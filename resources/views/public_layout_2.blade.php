<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Tomman Dashboard v1.0</title>
  @yield('header')
  <script src="/js/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <link rel="stylesheet" href="/bower_components/bootswatch-dist/css/bootstrap.min.css" />
  <script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

  <script type="text/javascript" src="/bower_components/dx/globalize.min.js"></script>

  {{-- time picker --}}
  <link rel="stylesheet" href="/bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
  <script src="/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
 

</head>
<body style="padding:10px;">

    

    @yield('content')

    @yield('plugins')
</div>
</body>
</html>
