@extends('layout')

@section('content')
  @include('partial.alerts')
  @include('workorderlistmigrasi')

  <!-- dialog !-->
  <div>
    <form class="form-group" method="post" id="submit-form" >
      <label for="Search">Search</label>
      <input type="text" name="Search" class="form-control" id="Search" value="">
      <button class="btn form-control btn-primary">Search</button>
    </form>
  </div>


  @if ($data <> NULL)
  <br />
  <div >
  <div class="form-control">
    {{ $data }}. <br />
    Result ({{ $jumlah }}) Found.
    <br />
    <br />
    @if ($jumlah > 0)
    <div class="table-responsive">
    <table class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
  		    <th width="75" colspan=2>Work Order</th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $no => $data)
        <tr>
  		@if(session('auth')->level <> 19)
          <td colspan=2><span>
  		@if(!empty($data->id_r))
          	<span class="label label-warning">{{ $data->uraian }}</span>

            <a href="/dispatch/{{ $data->ND }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
      
            <span class="label label-warning">{{ $data->updated_at }}</span>
          @else
            <a href="/dispatch/{{ $data->ND }}" class="label label-info">Dispatch</a>
          @endif
  		</span>

          <span><a href="/sendsms/{{ $data->ND }}" class="label label-warning">Send SMS</a></span>
          </td>
  		@endif
  		</tr>
  		<tr>
          <td>
    			@if ($data->id_dt<>'')
    			<a href="/{{ $data->id_dt }}">{{ $data->ND ? : 'Tidak Ada SC' }}</a>
    			@else
    			{{ $data->ND ? : 'Tidak Ada SC' }}
    			@endif

    			/ {{ $data->tgl ? : 'Tidak Ada SC' }}<br />
    	        {{ $data->ND ? : '' }} ~ {{ $data->ND_REFERENCE ? : '' }}<br />
              NCLI : {{ $data->NCLI ? : ''}} <br />
    	        {{ $data->NAMA  ? : '' }}<br />
    	        {{ $data->STO }} /
    	        {{ $data->LQUARTIER }} <br />
    	        {{ $data->LVOIE }} <br />
    	        {{ $data->ALPRO ? : 'Tidak Ada Informasi ODP' }} <br />
          </td>
  	    </tr>
  			<tr>
      		<td valign="top">
      	        Catatan Teknisi : <br />
      	        {{ $data->catatan ? : 'Tidak ada' }}

      		</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <br />
    <br />
    <br />
    <br />
    </div>
    @endif

  </div>
  </div>

  @endif

</div>

<br />
<br />
  <script>
  $(document).ready(function(){
    $('#submit-form').submit(function() {

    });
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
