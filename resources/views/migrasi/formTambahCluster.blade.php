@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>Tambah Cluster </h3><hr>

  <form method="post" autocomplete enctype="multipart/form-data">
      <div class="row">
        <div class="form-group">
            <div class="col-md-12 {{ $errors->has('nmCluster') ? 'has-error' : '' }}">
                Cluster
                <input type="text" name="nmCluster" class="form-control">
                {!! $errors->first('nmCluster','<p class=help-block>:message</p>') !!}
            </div>
        </div>
        <br><br><br>

        <div class="form-group">
          @foreach($fileMigration as $file)
            <div class="col-sm-4">
              <label class="control-label" for="{{$file}}">{{$file}}</label>
                  <input name="{{$file}}" type="file" id="{{$file}}" class="form-control" />
              </div>
            @endforeach
          </div>
        <br><br><br>
        <div class="col-md-12">
            <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            <a href="/migrasi/monitoring/loker1" class="btn btn-default btn-sm">Batal</a>
        </div>
      </div>
  </form>
@endsection
