@extends('layout')

@section('content')
  @include('partial.alerts')
	<style>
		.panel-content {
			padding : 10px;
		}
	</style>
	<div class="panel panel-primary">
		<div class="panel-heading">Input Order</div>
		<div class="panel-content">
			<form method="post">
				<input type="hidden" name="id" value="{{ $data->id or '' }}" />
			<div class="row">
				<div class="col-sm-4">
					<label for="uraian">AO</label>
					<input type="text" name="ao" id="ao" class="form-control" value="{{ $data->ND or '' }}" />
				</div>
				<div class="col-sm-6">
					<label for="uraian">NAMA</label>
					<input type="text" name="nama" id="nama" class="form-control" value="{{ $data->Nama or '' }}" />
				</div>
				<div class="col-sm-2">
					<label for="uraian">STO</label>
					<input type="text" name="sto" id="sto" class="form-control" value="{{ $data->MDF or '' }}" />
				</div>
			</div>


			<div class="form-group">
				<label for="uraian">ALAMAT</label>
				<input type="text" name="alamat" id="alamat" class="form-control" value="{{ $data->Alamat or '' }}" />
			</div>

			<div class="row">
				<div class="col-sm-4">
					<label for="uraian">JENIS LAYANAN</label>
					<input type="text" name="jenis_layanan" id="jenis_layanan" class="form-control" value="{{ $data->jenis_layanan or '' }}" />
				</div>

				<div class="col-sm-4">
					<label for="uraian">ODP</label>
					<input type="text" name="odp" id="odp" class="form-control" value="{{ $data->ODP or '' }}" />
				</div>

				<div class="col-sm-4">
					<label for="uraian">VLAN</label>
					<input type="text" name="vlan" id="vlan" class="form-control" value="{{ $data->sid or '' }}" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<label for="uraian">PIC</label>
					<input type="text" name="pic" id="pic" class="form-control" value="{{ $data->pic or '' }}" />
				</div>

				<div class="col-sm-4">
					<label for="uraian">LAYANAN</label>
					<input type="text" name="layanan" id="layanan" class="form-control" value="{{ $data->layanan or '' }}" />
				</div>
				<div class="col-sm-4">
					<label for="uraian">JENIS TIKET</label>
					<input type="text" name="jenis_tiket" id="jenis_tiket" class="form-control" value="{{ $data->jenis_tiket or '' }}" />
				</div>
			</div>
		</br>
			<div class="form-group">
				<input type="submit" name="submit" id="submit" class="btn btn-success" />
			</div>
			</form>
		</div>
	</div>
@endsection
@section('plugins')
	<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
	<script src="/bower_components/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $("#odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");
            var data = [{"id":"WIFI.ID", "text": "WIFI.ID"},{"id":"VPN", "text": "VPN"},{"id":"ASTINET", "text": "ASTINET"}];
            $("#jenis_layanan").select2({data:data});
            var data = [{"id":"PSB", "text": "PSB"},{"id":"GANGGUAN", "text": "GANGGUAN"},{"id":"MIGRASI", "text":"MIGRASI"}];
            $("#jenis_tiket").select2({data:data});
        });
    </script>
@endsection
