@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    MONITORING WORK ORDER MIGRASI <a href="/ms2n/sync/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> SyncNow</span></a>&nbsp
    <a href="migrasi/upload" class="btn btn-info">
      <span class="glyphicon glyphicon-upload"> Upload</span>
    </a>
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'migrasi') ? 'active' : '' }}"><a href="/migrasi/<?php echo date('Y-m-d'); ?>">ALL MIGRASI</a></li>
    <li class="{{ (Request::segment(2) == 'report') ? 'active' : '' }}"><a href="/migrasi/report">REPORT</a></li>
    <li class="{{ (Request::segment(2) == 'upload') ? 'active' : '' }}"><a href="/migrasi/upload">UPLOAD</a></li>
    <li class="{{ (Request::path() == 'ccan/search') ? 'active' : '' }}"><a href="/ccan/search">SEARCH</a></li>
  </ul>
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Order</th>
        <th>Keterangan Sebab</th>
        <th>Due Date</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)
      <tr>
        <td>{{ ++$no }}</td>
        <td><span>{{ $data->ND ? : '' }}</span><br/>
        <span>{{ $data->ND_Speedy ? : '' }}</span><br/>
        <span>{{ $data->MDF ? : '' }}</span><br/>
        <span>{{ $data->Nama ? : '' }} </span><br/>
        <span>{{ $data->Kcontact ? : '' }} </span><br/>
        <span>{{ $data->Alamat ? : ''}} </span></td>
        <td><span>{{ $data->Sebab ? : ''}}</span><br/><span>{{ $data->Ket_Sebab ? : ''}}</span></td>
        <td>{{ $data->Due_Date_Solusi ? : ''}}</td>
        <td>
        @if(!empty($data->id_r))
          <a href="migrasi/dispatch/{{ $data->ND }}" class="label label-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
        @else
          <a href="migrasi/dispatch/{{ $data->ND }}" class="label label-info">Dispatch</a>
        @endif
          
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
