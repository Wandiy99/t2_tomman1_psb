@extends('layout')
@section('content')
<style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
</style>
<!DOCTYPE html>
<html>
<body>
<div class="table-responsive">
<table class="table table-striped table-bordered dataTable">
    <tr align="center">
    <th>NO</th>
    <th>TL</th>
    <th>TEKNISI</th>
    <th>ORDER ID</th>
    <th>ODP</th>
    <th>KOORDINAT ODP</th>
    <th>ACTION</th>
    <th>TGL DISPATCH</th>
    <th>TGL UPDATE</th>
    <th>STATUS</th>
    <th>STO</th>
    <th>DATEL</th>
    <th>STATUS KENDALA</th>
    <th>TGL STATUS KENDALA</th>
    <th>CATATAN TEKNISI PSB</th>
    </tr>
    @foreach($data as $no => $list)
    <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $list->tl ? : '-' }}</td>
        <td>{{ $list->teknisi }}</td>
        <td>{{ $list->order_id }}</td>
        <td>{{ $list->odp_nama ? : '-' }}</td>
        <td>{{ $list->lt_koordinat_odp ? : '-' }}</td>
        <td>{{ $list->lt_action ? : '-' }}</td>
        <td>{{ $list->tgl_dispatch ? : '-' }}</td>
        <td>{{ $list->tgl_update ? : '-' }}</td>
        <td>{{ $list->status ? : 'No Update' }}</td>
        <td>{{ $list->sto }}</td>
        <td>{{ $list->datel }}</td>
        <td>{{ $list->status_kendala }}</td>
        <td>{{ $list->tgl_status_kendala }}</td>
        <td>{{ $list->catatan }}</td>
    </tr>
    @endforeach
</table>
</div>
</body>
</html>
@endsection