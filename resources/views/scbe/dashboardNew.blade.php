@extends('layout')

@section('content')
  @include('partial.alerts')
  <center><h3>Dashboard Provisioning</h3></center>
  <br />
  <div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
            Data Progress SCBE
            </div>
            <div class="panel-body">
            <table class="table">
                <tr>
                    <th rowspan=2>Datel</th>
                    <th colspan=3>Tanggal</th>
                    <th>Total</th>
                </tr>
                <tr>
                <?php
                for ($i=0;$i<=2;$i++){
                    $datex = date('d',strtotime("-$i days"));
                    echo "<th>".$datex."</th>";
                }
                ?>  
                </tr>
                @foreach ($datel as $result_datel)
                    <tr><td colspan=5><u><i>{{ $result_datel->datel }}</u></i></td></tr>
                    @foreach ($data_progress_scbe[$result_datel->datel] as $result)
                    
                        @if ($result->undispatch>0)
                        <tr>
                        <td><li>BELUM DISPATCH</a></li></td>
                        <?php
                            for ($i=0;$i<=2;$i++){
                                $datex = date('d',strtotime("-$i days"));
                                $datey = date('Y-m-d',strtotime("-$i days"));
                                $field = "undispatch_".$datex;
                                echo "<td><a href='/dashboardScbeList/".$datey."/".$result_datel->datel."/BELUM_DISPATCH'>".$result->$field."</a></td>";
                            }
                        ?>
                         <td><a href='/dashboardScbeList/ALL/{{ $result_datel->datel }}/BELUM_DISPATCH'>{{ $result->undispatch }}</a></td>
                         </tr>
                         <tr>
                        <td><li>NEED PROGRESS NEW</a></li></td>
                        <?php
                            for ($i=0;$i<=2;$i++){
                                $datex = date('d',strtotime("-$i days"));
                                $datey = date('Y-m-d',strtotime("-$i days"));
                                $field = "progress_new_".$datex;
                                echo "<td><a href='/dashboardScbeList/".$datey."/".$result_datel->datel."/NEED PROGRESS NEW'>".$result->$field."</a></td>";
                            }
                        ?>
                         <td><a href='/dashboardScbeList/ALL/{{ $result_datel->datel }}/NEED PROGRESS NEW'>{{ $result->progress_new }}</a></td>
                         </tr>
                        @else
                        <tr>
                        <td><li>{{ $result->laporan_status }}</a></li></td>
                        <?php
                            for ($i=0;$i<=2;$i++){
                                $datex = date('d',strtotime("-$i days"));
                                $datey = date('Y-m-d',strtotime("-$i days"));
                                $field = "order_".$datex;
                                echo "<td><a href='/dashboardScbeList/".$datey."/".$result_datel->datel."/".$result->laporan_status."'>".$result->$field."</a></td>";
                            }
                        ?>
                         <td><a href='/dashboardScbeList/ALL/{{ $result_datel->datel }}/{{ $result->laporan_status }}'>{{ $result->jumlah }}</a></td>
                         </tr>
                        @endif
                        
                       
                    
                    @endforeach
                @endforeach

            </table>
            </div>
        </div>
    </div>
  </div>
@endsection