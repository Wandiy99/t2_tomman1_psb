@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .warna8{
      background-color: #483D8B;
    }

    .warna9{
      background-color: #FFE4B5;
    }

    .text2{
      color: black !important;
    }

    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }

    .text1:visited{
      color: white !important;
    }


    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>

<div class="row">
  <div class="col-sm-12">
    <h3>{{ $title }}</h3>
    <table class="table table-striped table-bordered dataTable">
      <tr>
        <th>NO</th>
        <th>SYNC</th>
        <th>ORDER_ID</th>
        <th>ORDER NAME</th>
        <th>ORDER DATE</th>
        <th>STATUS</th>
        <th>SEKTOR</th>
        <th>STO</th>
        <th>REGU</th>
        <th>WFMID</th>
        <th>STATUS TEK</th>
      </tr>
      @foreach ($umur_pi as $num => $result)
      <tr>
        <td>{{ ++$num }}</td>
        <td><a href="/syncSC/{{ $result->orderId }}">SYNC</a></td>
        <td>{{ $result->orderId }}</td>
        <td>{{ $result->orderName }}</td>
        <td>{{ $result->orderDate }}</td>
        <td>{{ $result->orderStatus }}</td>
        <td>{{ $result->sektor_prov }}</td>
        <td>{{ $result->sto }}</td>
        <td>{{ $result->uraian }}</td>
        <td>{{ $result->id_dt }}</td>
        <td>{{ $result->laporan_status ? : "NEED PROGRESS" }}</td>
      </tr>
      @endforeach
    </table>
  </div>
</div>

@endsection
