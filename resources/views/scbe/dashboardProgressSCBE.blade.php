@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .warna8{
      background-color: #483D8B;
    }

    .warna9{
      background-color: #FFE4B5;
    }

    .text2{
      color: black !important;
    }

    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }

    .text1:visited{
      color: white !important;
    }


    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>
<center>
  <h2>Dashboard Progress SCBE Kalsel</h2>
</center>
<div class="row">
  <div class="col-sm-12">
    <h3>PROGRESS SCBE</h3>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna7 text1">AREA</th>
            <th class="warna7 text1">UNDISPATCH</th>
            <th class="warna7 text1">DISPATCH</th>
          </tr>
          <?php
            $total_scbe_undispatch = 0;
            $total_scbe_dispatched = 0;
          ?>
          @foreach ($mappingSCBE as $result)
          <?php
            $total_scbe_undispatch += $result->scbe_undispatch;
            $total_scbe_dispatched += $result->scbe_dispatched;
          ?>
          <tr>
            <td>{{ $result->datel }}</td>
            <td><a href="/scbe_undispatch/{{ $result->datel }}">{{ $result->scbe_undispatch }}</a></td>
            <td><a href="/scbe_dispatched/{{ $result->datel }}">{{ $result->scbe_dispatched }}</a></td>
          </tr>
          @endforeach
          <tr>
            <th>TOTAL</th>
            <th><a href="/scbe_undispatch/ALL">{{ $total_scbe_undispatch }}</a></th>
            <th><a href="/scbe_dispatched/ALL">{{ $total_scbe_dispatched }}</a></th>
          </tr>
        </table>
      </div>
    </div>
@endsection
