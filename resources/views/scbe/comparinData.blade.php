@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL COMPARIN {{ $type }} AREA {{ $area }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>STO</th>
                        <th>TIM</th>
                        <th>ORDER ID</th>
                        <th>WFM ID</th>
                        <th>NO INET</th>
                        <th>CUSTOMER NAME</th>
                        <th>JENIS PSB</th>
                        <th>TYPE LAYANAN</th>
                        <th>PROVIDER</th>
                        <th>STATUS RESUME</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>STATUS TEKNISI</th>
                        <th>TGL LAPORAN</th>
                        <th>LAST GRAB</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area }}</td>
                        <td>{{ $data->STO }}</td>
                        <td>{{ $data->tim }}</td>
                        <td>{{ $data->ORDER_ID }}</td>
                        <td>{{ $data->WFM_ID }}</td>
                        <td>{{ $data->SPEEDY }}</td>
                        <td>{{ $data->CUSTOMER_NAME }}</td>
                        <td>{{ $data->JENISPSB }}</td>
                        <td>{{ $data->TYPE_LAYANAN }}</td>
                        <td>{{ $data->PROVIDER }}</td>
                        <td>{{ $data->STATUS_RESUME }}</td>
                        <td>{{ $data->ORDER_DATE }}</td>
                        <td>{{ $data->ORDER_DATE_PS }}</td>
                        <td>{{ $data->laporan_status }}</td>
                        <td>{{ $data->modified_at }}</td>
                        <td>{{ $data->last_grab_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL COMPARIN PI FO TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL COMPARIN PI FO TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL COMPARIN PI FO TOMMAN'
                }
            ]
        });
    });
</script>
@endsection