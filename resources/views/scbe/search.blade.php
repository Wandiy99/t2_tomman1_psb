@extends('layout')

@section('content')
@include('partial.alerts')
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 10px 6px;
    }
    th{
        text-align: center;
        color: black;
        width: 1px;
        white-space: nowrap;
    }
    td{
        color: black;
    }
</style>
<h4>Cari MYIR</h4>
  <form class="row" style="margin-bottom: 20px">
      <div class="col-md-12 {{ $errors->has('scbeq') ? 'has-error' : '' }}">
          <div class="col-md-11">
            <div class="input-group">
              <input type="number" class="form-control" name="scbeq"/>
            </div>
          </div>
          <div class="col-md-1">
            <div class="input-group-btn">
              <button class="btn btn-sm btn-primary btn-rounded" type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </div>
          </div>
      {!! $errors->first('scbeq','<p class=help-block>:message</p>') !!}
      </div>
  </form>

  <a href="/scbe/input" type="button" class="btn btn-success btn-sm btn-rounded"><i class="ti-plus" class="linea-icon linea-basic fa-fw"></i>&nbsp; Dispatch Manual</a>

  @if($list<>NULL)
  <br /> <br />
  <div class="table-responsive white-box">
    <table class="table table-sm table-bordered">
      <thead>
        <tr>
            <td class="text-center"><b>#</b></td>
            <td class="text-center" style="width: 60%"><b>OneCall</b></td>
            <td class="text-center"><b>Informasi Order</b></td>
            <td class="text-center" style="width: 10%"><b>Dispatch</b></td>
        </tr>
      </thead>
      <tbody>
            <td style="text-align: center; vertical-align: middle;">1</td>
            <td>
              <table class="table table-sm table-bordered">
                <tbody>
                  <tr>
                    <td>MYIR</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">
                      @if($myirAda == 0)
                        <a href="/{{ $list['id'] }}">{{ $list['myir'] }}</a>
                      @else
                        {{ $list['myir'] }}
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td>Order Date</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['orderDate'] }}</td>
                  </tr>
                  <tr>
                    <td>Pelanggan</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['customer'] }}</td>
                  </tr>
                  <tr>
                    <td>Jenis Layanan</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{   $list['jenis_layanan'] }}</td>
                  </tr>
                  <tr>
                    <td>Koordinat Pelanggan</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['kordinatPel'] }}</td>
                  </tr>
                  <tr>
                    <td>Alamat Sales</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['alamatSales'] }}</td>
                  </tr>
                  <tr>
                    <td>ODP By Sales</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['namaOdp'] }}</td>
                  </tr>
                  @if ($ket == '1')
                  <tr>
                    <td>MYIR</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">MYIR-{{ $list['myir'] }}</td>
                  </tr>
                  @endif
                  <tr>
                    <td>PIC Pelangan</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['picPelanggan'] }}</td>
                  </tr>
                  <tr>
                    <td>No Internet</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['no_internet'] }}</td>
                  </tr>
                  <tr>
                    <td>No Telp</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['no_telp'] }}</td>
                  </tr>
                  <tr>
                    <td>K-Contact</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['kcontact'] }}</td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td>
              <table class="table">
                <tbody>
                  <tr>
                    <td>SC ID</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['Ndem'] }}</td>
                  </tr>
                  <tr>
                    <td>Order Status SC</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;"><label class="label label-success">{{ $list['orderStatus'] }}</label></td>
                  </tr>
                  <tr>
                    <td>Tim</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['uraian'] }}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Awal Dispatch</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['tgl_awal_dispatch'] }}</td>
                  </tr>
                  <tr>
                    <td>Tanggal Dispatch</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['tgl'] }}</td>
                  </tr>
                  <tr>
                    <td>Umur</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">
                      @php
                          if ($list['id_dt'] == null)
                          {
                              $tglOrder = $list['orderDate'];
                          } else {
                              $tglOrder = $list['tgl_awal_dispatch'];
                          }
                          $awal  = date_create($tglOrder);
                          $akhir = date_create();
                          $diff  = date_diff( $awal, $akhir );

                          if ($diff->d > 0)
                          {
                              $waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
                          } else {
                              $waktu = $diff->h.' Jam '.$diff->i.' Menit';
                          }
                      @endphp

                      {{ $waktu }}
                    </td>
                  </tr>
                  <tr>
                    <td>Laporan Status</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['laporan_status'] ?: 'ANTRIAN' }}</td>
                  </tr>
                  <tr>
                    <td>ODP By Teknisi</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ $list['odpByTeknisi'] }}</td>
                  </tr>
                  <tr>
                    <td>Catatan Teknisi</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ @$list['catatan'] }}<br />{{ @$list['modified_at'] }}</td>
                  </tr>
                  <tr>
                    <td>Valins ID</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ @$list['valins_id'] }}</td>
                  </tr>
                  <tr>
                    <td>Dropcore Labelcode</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ @$list['dropcore_label_code'] }}</td>
                  </tr>
                  <tr>
                    <td>ODP Labelcode</td>
                    <td style="text-align: center; vertical-align: middle;">:</td>
                    <td style="text-align: right; vertical-align: middle;">{{ @$list['odp_label_code'] }}</td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td class="text-center">
              @if($myirAda == 0)

                @if ($list['ket']=='0')  
                    <a href="/scbe/edit/{{ $list['idMyir'] }}" class="label label-info">Re-Dispatch</a>
                    <br /><br />
                    <a href="/dshr/plasa-sales/input-sc/{{ $list['myir'] }}" class="label label-success">Add-SC</a>
                    <br /><br />
                @else
                    <a href="/dshr/plasa-sales/edit-sc-myir/{{ $list['myir'] }}" class="label label-info">Edit-SC {{ $list['Ndem'] }}</a>
                    <br /><br />
                @endif
                @if($sc == 1 && $ket == 0)
                    <a href="/scbe/sinkronsc/{{ $list['myir'] }}" class="label label-success">Syncron SC</a>
                    <br /><br />
                @endif

                @if(!empty($list['uraian']) && $list['id_pl'] <> "" )  
                  @if(in_array(session('auth')->level, [2, 15]))
                    <a href="/dispatch/delete/{{ $list['myir'] }}/{{ $list['id'] }}/{{ $list['id_pl'] }}" class="label label-danger">Delete Dispatch</a>
                    <br /><br />
                  @endif
                @endif

                <!-- MARINA -->
                <div id="responsive-modal-marina-{{ $list['id'] }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-marina-{{ $list['id'] }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>LOG MARINA MYIR-{{ $list['myir'] }}</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Jenis Order</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_nama_order'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Status</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_status'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Headline</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_headline'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Action</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_action'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Port Used</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_port_used'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Port Idle</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_port_idle'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Selesai</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['tgl_selesai'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Nama Regu</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['dispatch_regu_name'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">User Update</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_modif_by'] ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Terakhir Update</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $list['mc_modif_at'] ? : '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-primary btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-marina-{{ $list['id'] }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Log Marina</b>
                </button>

              @else

                @if($list['ketInput'] == 0)
                  <a href="/dshr/plasa-sales/dispatch/plasa/{{ $list['myir'] }}" class="label label-info">Dispatch</a>
                  <br /><br />
                  {{-- <a href="/dshr/plasa-sales/delete/{{ $list['myir'] }}/{{ $list['id'] }}" class="label label-danger">Delete-MYIR</a>
                  <br /><br /> --}}
                @else
                  <a href="/dshr/plasa-sales/dispatch/sales/{{ $list['myir'] }}" class="label label-info">Dispatch</a>
                  {{-- <br /><br />
                  <a href="/dshr/plasa-sales/delete/{{ $list['myir'] }}/{{ $list['id'] }}" class="label label-danger">Delete-MYIR</a> --}}
                @endif

              @endif
            </td>
      </tbody>
    </table>
  </div>
  @endif



@endsection