@extends('layout')

@section('content')
  <!-- @include('partial.alerts') -->


  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>

  <!-- Modal -->
 <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add SC | K-Contact</h5>
        </div>

        <div class="modal-body">
            <form method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id_dt" id="id_dt">

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                            <label class="label-control" for="sc">SC</label>
                            <input type="text" name="sc" id="sc" class="form-control" autofocus>
                      </div>
                  </div>

                  <div class="col-md-12">
                      <div class="form-group">
                            <label class="label-control" for="kcontact">K-Contact</label>
                            <textarea class="form-control" rows="4" id="kcontact" name="kcontact">

                            </textarea>
                      </div>
                  </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn-save">Save</button>
        </div>
      </div>
    </div>
  </div>

  <a href="/dashboard/scbe/{{ $tgl }}/" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>

  <!-- <a href="/dashboard/sendtobotkpro/{{ $tgl }}/{{ $jenis }}/{{ $status }}/{{ $so }}/{{ $ket }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-refresh"> KPRO </span>
  </a> -->

  <h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable"> 
      <table table border="1">
        <tr>
          <th rowspan="2">No.</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>

          @if ($status=='ALL' || $status=="UP")
            <th colspan="14">WO / TIKET</th>
          @else
            <th colspan="12">WO / TIKET</th>
          @endif

          @if ($status=='UNSC')
                <th colspan="4">UNSC</th>
          @endif

          <th colspan="3">NOMOR LAYANAN</th>
          <th colspan="4">DATA PELANGGAN</th>
          <th colspan="4">TRANSAKSI / MUTASI</th>

          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th colspan="13">LAIN-LAIN</th>
          @else
              <th colspan="14">LAIN-LAIN</th>
          @endif

        </tr>

        <tr>
          <th>PIC</th>
          <th>SEKTOR</th>
          <th>NOMOR SC</th>
          <th>MYIR</th>
          <th>VALINS ID</th>

          <!-- <th>TGL WO</th> -->
          <th>TGL DISPATCH</th>

          @if($status=='ALL' || $status=="UP")
            <th>TGL PS</th>
          @endif

          <th>TGL STATUS</th>

          <th>DATEL</th>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>Kode Sales</th>
          <th>SOURCE</th>
          @if ($status=='UNSC')
            <th>SC UNSC</th>
            <th>K-KONTAK</th>
            <th>Action</th>
          @endif

          <th>NCLI</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>No. HP</th>
          <th>JENIS MUTASI</th>
          <th>LAYANAN</th>
          <th>KCONTACT</th>
          <th>KORD. PEL (TEKNISI)</th>
          <th>KORD. PEL (SALES)</th>
          <th>ODP</th>
          <th>REDAMAN ODP</th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>

          @if ($status=='INSERT TIANG' || $status=='ALL')
              <th>TAMBAH TIANG</th>
          @endif

          <th>STATUS KENDALA</th>
          <th>TGL STATUS KENDALA</th>
          <th>QRCODE</th>

          <th>CATATAN TEK 1</th>
          <th>CATATAN TEK 2</th>
          <th>CATATAN TEK 3</th>
        </tr>


        <!-- data -->

        @foreach ($data as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $result->TL }}</td>
          <td>{{ $result->TIM }}</td>
          <td>{{ $result->PIC }}</td>
          <td>{{ $result->SEKTOR }}</td>
          <td>{{ $result->SC ? : $result->ORDER_ID }}</td>
          <td>{{ $result->MYIR }}</td>
          <td>{{ $result->VALINS_ID }}</td>
          <td>{{ $result->TGL_DISPATCH }}</td>

          @if($status=='ALL' || $status=="UP")
            <td>{{ $result->orderDatePs ?: '~' }}</td>
          @endif

          <td>{{ $result->TGL_STATUS }}</td>

          <td>{{ $result->area_migrasi ? : $result->datel }}</td>
          <td>{{ $result->sto ? : $result->stoMy }}</td>
          <td>{{ $result->VOICE ? : $result->VOICE2 ? : '~' }}</td>
          <td>{{ $result->INET ? : $result->INET2 ? : '~' }}</td>
          <td>{{ $result->KODE_SALES }}</td>
          <td>{{ $result->psb }}</td>

          @if($status=='UNSC')
            <td>{{ $result->sc_unsc}}</td>
            <td>{{ $result->kcontact_unsc }}</td>
            <td>
              @if(session('auth')->level==2 || session('auth')->id_user=="99386" || session('auth')->id_user=="106847" )
                  @if ($result->sc_unsc=='' || $result->kcontact_unsc=='' )
                    <center><!-- <a href="{{ route('show.data.sc',[$result->id_dt]) }}" class="label label-primary" id="btn-modal"> -->ADD SC UNSC<!-- </a> --></center>
                  @else
                    <center><!-- <a href="{{ route('show.data.sc',[$result->id_dt]) }}" class="label label-primary" id="btn-modal"> -->UPDATE SC UNSC<!-- </a> --></center>
                  @endif
              @endif
            </td>
          @endif


          <td>{{ $result->NCLI ? : '~' }}</td>
          <td>{{ $result->NAMA ? : $result->NAMA2 }}</td>
          <td>{{ $result->ALAMAT ? : $result->ALAMAT2 }}</td>
          <td>{{ $result->NO_HP }}</td>
          <td>{{ $result->jenisPsb ? : '~' }}</td>
          <td>{{ $result->LAYANAN ? : $result->LAYANAN2 }}</td>
          <td>{{ $result->KCONTACT ? : $result->KCONTACT2 }}</td>

          <td>{{ $result->KORPEL }}</td>
          <td>{{ $result->KORSEL }}</td>
          <td>{{ $result->ODP ? : $result->ODP2 ? : $result->ODP3 }}</td>
          <td>{{ $result->REDAMAN_ODP ?: '~'}}</td>
          <td>{{ $result->AREA }}</td>
          <td>{{ $result->MITRA }}</td>
          <td>{{ $result->STATUS_SC ? : '~'}}</td>
          <td>{{ $result->STATUS_TEK }}</td>

          @if ($status=='INSERT TIANG' || $status=='ALL')
                <td>{{ $result->ketTiang }}</td>
          @endif

          <td>{{ $result->STATUS_KEN ? : '-' }}</td>
          <td>{{ $result->TGL_STATUS_KEN ? : '-' }}</td>
          <td>{{ $result->QRCODE }}</td>

          @php
            if ($result->CATATAN==NULL){
               echo "<td></td>";
               echo "<td></td>";
               echo "<td></td>";
            }
            else{
                $cat = explode('#',$result->CATATAN);
                $jml = count($cat);

                $jmlCat = $jml;
                $awal = 0;
                if ($jml >= 3){
                    $jmlCat = 3;
                }

                if (!empty($jmlCat)){
                    for ($a=$awal;$a<=($jmlCat-1);$a++){
                        echo "<td>".$cat[$a]."</td>";
                    }

                    $bbb = 3 - $jmlCat;
                    for ($iii=0;$iii<=$bbb-1;$iii++){
                        echo "<td></td>";
                    }
                }
            }
          @endphp
        </tr>
        @endforeach
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection

@section('plugins')
    <script>
      $(function(){
         $('body').on('click', '#btn-modal', function(e){
              e.preventDefault();
              var me  = $(this),
                  url = me.attr('href');

              $.ajax({
                  'url' : url,
                  'dataType' : 'json',
                  success : function(data){
                    console.log(data);
                    $('#sc').val(data['sc_unsc']);
                    $('#kcontact').val(data['kcontact_unsc']);
                    $("#id_dt").val(data['id_tbl_mj']);

                    $('#modal').modal('show');
                  }
              })
        });

        $('body').on('click', '#btn-save', function(){
           var form = $('.modal-body form'),
               id   = $('#id_dt').val(),
               data = form.serialize(),
               url  = '/show-data/'+id+'/save';

               form.find('.help-block').remove();
               form.find('.form-group').removeClass('has-error');

               $.ajax({
                    'url'   : url,
                    'method': 'GET',
                    'data'  : data,
                    success : function(){
                       $('#modal').modal('hide');
                       location.reload();
                    },
                    error : function(xhr){
                        var err = xhr.responseJSON;
                        if ($.isEmptyObject(err)==false){
                            $.each(err, function(key, value){
                                $('#'+key).closest('.form-group').addClass('has-error').append('<span class="help-block">'+value+'<strong></strong></span>')
                            })
                        }
                    }
               })
        })


      })
    </script>
@endsection
