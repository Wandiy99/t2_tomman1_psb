@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #870505;
    }

    .warna2{
      background-color: #1180af;
    }

    .warna3{
      background-color: #89400c;
    }

    .warna4{
      background-color: #15b21a;
    }

  </style>
  <div id="screnshoot">  
    <h3>Periode {{ $tgl }}</h3>
    <div class="row">
      <div class="col-sm-6">
      <h3>Dashboard SCBE TA KALSEL</h3>
       <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th>Status</th>
            <th>INNER</th>
            <th>BBR</th>
            <th>KDG</th>
            <th>TJL</th>
            <th>BLC</th>
            <th>JUMLAH</th>
          </tr>
          <?php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $total_inner = 0;                                                                                                                                                                                                                                         
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          ?>

          @if(count($dataUp)<>0)
            <?php
              $upInner = $dataUp[0]->WO_INNER;
              $upBbr   = $dataUp[0]->WO_BBR;
              $upKdg   = $dataUp[0]->WO_KDG;
              $upTjl   = $dataUp[0]->WO_TJL;
              $upBlc   = $dataUp[0]->WO_BLC;
              $upJumlah= $dataUp[0]->jumlah;
            ?>

            <tr>
              <td>UP</td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/INNER/SCBE">{{ $dataUp[0]->WO_INNER }}</a></td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/BBR/SCBE">{{ $dataUp[0]->WO_BBR }}</a></td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/KDG/SCBE">{{ $dataUp[0]->WO_KDG }}</a></td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/TJL/SCBE">{{ $dataUp[0]->WO_TJL }}</a></td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/BLC/SCBE">{{ $dataUp[0]->WO_BLC }}</a></td>
              <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/UP/ALL/SCBE">{{ $dataUp[0]->jumlah }}</a></td>
            </tr>
          @else
            <?php
              $upInner = 0;
              $upBbr   = 0;
              $upKdg   = 0;
              $upTjl   = 0;
              $upBlc   = 0;
              $upJumlah= 0;
            ?>
          @endif

          @foreach ($data as $result)
          @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
          <tr>
            <td>{{ $result->laporan_status }}</td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/INNER/NEW">{{ $result->WO_INNER }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BBR/NEW">{{ $result->WO_BBR }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/KDG/NEW">{{ $result->WO_KDG }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/TJL/NEW">{{ $result->WO_TJL }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BLC/NEW">{{ $result->WO_BLC }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL/NEW">{{ $result->jumlah }}</a></td>
          </tr>
          <?php
            $total_inner += $result->WO_INNER;
            $total_bbr += $result->WO_BBR;
            $total_kdg += $result->WO_KDG;
            $total_tjl += $result->WO_TJL;
            $total_blc += $result->WO_BLC;
          ?>
          @else
          <?php
            $no_update_jumlah += $result->jumlah;
            $no_update_inner += $result->WO_INNER;
            $no_update_bbr += $result->WO_BBR;
            $no_update_kdg += $result->WO_KDG;
            $no_update_tjl += $result->WO_TJL;
            $no_update_blc += $result->WO_BLC;
          ?>

          @endif
          @endforeach
          <tr>
            <td>ANTRIAN</td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/INNER/NEW">{{ $no_update_inner }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/BBR/NEW">{{ $no_update_bbr }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/KDG/NEW">{{ $no_update_kdg }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/TJL/NEW">{{ $no_update_tjl }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/BLC/NEW">{{ $no_update_blc }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/ALL/NEW">{{ $no_update_jumlah }}</a></td>
          </tr>
          <?php
          $total_inner = $total_inner + $no_update_inner + $upInner;
          $total_bbr = $total_bbr + $no_update_bbr + $upBbr;
          $total_kdg = $total_kdg + $no_update_kdg + $upKdg;
          $total_tjl = $total_tjl + $no_update_tjl + $upTjl;
          $total_blc = $total_blc + $no_update_blc + $upBlc;
          $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          ?>
          <tr>
            <td>TOTAL</td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/INNER/NEW">{{ $total_inner }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BBR/NEW">{{ $total_bbr }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/KDG/NEW">{{ $total_kdg }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/TJL/NEW">{{ $total_tjl }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BLC/NEW">{{ $total_blc }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/ALL/NEW">{{ $total }}</a></td>
          </tr>
        </table>
      </div> 

      <div class="col-sm-6">
           <h3>Report Potensi PS</h3>
           <table class="table table-striped table-bordered dataTable">
              <tr></tr>
                <th>Status (AO)</th>
                <th>INNER</th>
                <th>BBR</th>
                <th>TJL</th>
                <!-- <th>TJL</th> -->
                <th>BLC</th>
                <th>JUMLAH</th>
              </tr>
              <?php
                $no_update_jumlah = 0;
                $no_update_inner = 0;
                $no_update_bbr = 0;
                $no_update_kdg = 0;
                // $no_update_tjl = 0;
                $no_update_blc = 0;
                $total_inner = 0;                                                                                                                                                                                                                                         
                $total_bbr = 0;
                $total_kdg = 0;
                // $total_tjl = 0;
                $total_blc = 0;
                $total = 0;
              ?>

              <tr>
                <td>Completed (PS)</td>
                <?php
                    $completedPsInner = 0;
                    $completedPsBbr   = 0;
                    $completedPsKdg   = 0;
                    $completedPsBlc   = 0;
                    $completedPsJumlah= 0;
                 ?>

                @if (count($dataReportPsStarUp)<>0)
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/INNER">{{ $dataReportPsStarUp[0]->WO_INNER ?: 0 }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BBR">{{ $dataReportPsStarUp[0]->WO_BBR }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/KDG">{{ $dataReportPsStarUp[0]->WO_KDG }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BLC">{{ $dataReportPsStarUp[0]->WO_BLC }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/ALL">{{ $dataReportPsStarUp[0]->jumlah }}</a></td>
                  <?php
                    $completedPsInner = $dataReportPsStarUp[0]->WO_INNER;
                    $completedPsBbr   = $dataReportPsStarUp[0]->WO_BBR;
                    $completedPsKdg   = $dataReportPsStarUp[0]->WO_KDG;
                    $completedPsBlc   = $dataReportPsStarUp[0]->WO_BLC;
                    $completedPsJumlah= $dataReportPsStarUp[0]->jumlah;
                 ?>
                @else
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/INNER">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BBR">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/KDG">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BLC">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/ALL">0</a></td>              
                @endif
              </tr>

              @foreach ($dataReportPsStar as $result)
              <tr>
                <td>{{ $result->orderStatus }}</td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/INNER">{{ $result->WO_INNER }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BBR">{{ $result->WO_BBR }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/KDG">{{ $result->WO_KDG }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BLC">{{ $result->WO_BLC }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/ALL">{{ $result->jumlah }}</a></td>
              </tr>
              <?php
                $total_inner += $result->WO_INNER;
                $total_bbr += $result->WO_BBR;
                $total_kdg += $result->WO_KDG;
                // $total_tjl += $result->WO_TJL;
                $total_blc += $result->WO_BLC;
              ?>
              @endforeach

              <?php
                  if (empty($dataReportPsScbe)){
                      $scbe_inner   = 0;
                      $scbe_bbr     = 0;
                      $scbe_kdg     = 0;
                      $scbe_blc     = 0;
                      $scbe_jumlah  = 0;  
                  } 
                  else {
                      $scbe_inner   = $dataReportPsScbe[0]->WO_INNER;
                      $scbe_bbr     = $dataReportPsScbe[0]->WO_BBR;
                      $scbe_kdg     = $dataReportPsScbe[0]->WO_KDG;
                      $scbe_blc     = $dataReportPsScbe[0]->WO_BLC;
                      $scbe_jumlah  = $dataReportPsScbe[0]->jumlah;
                  }
                ?>
                
              <?php
              $total_inner = $total_inner + $completedPsInner;
              $total_bbr = $total_bbr + $completedPsBbr;
              $total_kdg = $total_kdg + $completedPsKdg;
              $total_blc = $total_blc + $completedPsBlc;
              $total = $total_inner + $total_bbr + $total_kdg + $total_blc; 
              ?>
              <tr>
                <td>TOTAL</td>
                <td><a href="/dashboard/scbeListTotal/{{ $tgl }}/INNER">{{ $total_inner }}</a></td>
                <td><a href="/dashboard/scbeListTotal/{{ $tgl }}/BBR">{{ $total_bbr }}</a></td>
                <td><a href="/dashboard/scbeListTotal/{{ $tgl }}/KDG">{{ $total_kdg }}</a></td>
                <td><a href="/dashboard/scbeListTotal/{{ $tgl }}/BLC">{{ $total_blc }}</a></td>
                <td><a href="/dashboard/scbeListTotal/{{ $tgl }}/ALL">{{ $total }}</a></td>

      <!--           <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/INNER">{{ $total_inner }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BBR">{{ $total_bbr }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/KDG">{{ $total_kdg }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/TJL">{{ $total_tjl }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BLC">{{ $total_blc }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td> -->
              </tr>
            </table>
      </div>
    </div>
    
     <div class="row">
      <div class="col-sm-6">
        <h3>Dashboard Transaksi 2P-3P, CHANGESTB, 2ndSTB</h3>
        <table class="table table-striped table-bordered dataTable">
            <tr></tr>
              <th>Status</th>
              <th>2P-3P</th>
              <th>CHANGESTB</th>
              <th>2ndSTB</th>
              <th>3ndSTB</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $no_update_jumlah = 0;
              $no_update_addonstb = 0;
              $no_update_chengestb = 0;
              $no_update_2ndstb = 0;
              $no_update_3ndstb = 0;

              $total_wo_ADDONSTB = 0;
              $total_wo_CHANGESTB = 0;
              $total_wo_2ndSTB = 0;
              $total_wo_3ndSTB = 0;
              $total = 0;
            ?>
            @foreach ($dataReportTransaksi as $result)
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                    <tr>
                      <td>{{ $result->laporan_status }}</td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/addonstb">{{ $result->wo_ADDONSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/changestb">{{ $result->wo_CHANGESTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/2ndstb">{{ $result->wo_2ndSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/3ndstb">{{ $result->wo_3ndSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                    </tr>
                    <?php
                      $total_wo_ADDONSTB  += $result->wo_ADDONSTB;
                      $total_wo_CHANGESTB += $result->wo_CHANGESTB;
                      $total_wo_2ndSTB    += $result->wo_2ndSTB;
                      $total_wo_3ndSTB    += $result->wo_3ndSTB;
                    ?>
                @else
                    <?php
                      $no_update_jumlah    += $result->jumlah;
                      $no_update_addonstb  += $result->wo_ADDONSTB;
                      $no_update_chengestb += $result->wo_CHANGESTB;
                      $no_update_2ndstb    += $result->wo_2ndSTB;
                      $no_update_3ndstb    += $result->wo_3ndSTB;
                    ?>
                @endif
            @endforeach
            <tr>
              <td>ANTRIAN</td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/addonstb">{{ $no_update_addonstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/changestb">{{ $no_update_chengestb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/2ndstb">{{ $no_update_2ndstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/3ndstb">{{ $no_update_3ndstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/ALL">{{ $no_update_jumlah }}</a></td>
            </tr>

            <?php
              $total_wo_ADDONSTB    = $total_wo_ADDONSTB + $no_update_addonstb;
              $total_wo_CHANGESTB   = $total_wo_CHANGESTB +  $no_update_chengestb;
              $total_wo_2ndSTB      = $total_wo_2ndSTB  +  $no_update_2ndstb;
              $total_wo_3ndSTB      = $total_wo_3ndSTB  +  $no_update_3ndstb;
              $total = $total_wo_ADDONSTB + $total_wo_CHANGESTB + $total_wo_2ndSTB + $total_wo_3ndSTB;
            ?>
            
            <tr>
              <td>TOTAL</td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/addonstb">{{ $total_wo_ADDONSTB }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/changestb">{{ $total_wo_CHANGESTB }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/2ndstb">{{ $total_wo_2ndSTB }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/3ndstb">{{ $total_wo_3ndSTB }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td>
            </tr>
        </table>
      </div> 

      <div class="col-sm-6">
      <h3>Report Potensi MO</h3>
        <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th>Status (MO)</th>
            <th>INNER</th>
            <th>BBR</th>
            <th>KDG</th>
            <th>TJL</th>
            <th>BLC</th>
            <th>JUMLAH</th>
          </tr>
          <?php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $total_inner = 0;                                                                                                                                                                                                                                         
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          ?>

          @foreach ($dataReporPsStarMo as $result)
          <tr>
            <td>{{ $result->orderStatus }}</td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/INNER/Mo">{{ $result->WO_INNER }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BBR/Mo">{{ $result->WO_BBR }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/KDG/Mo">{{ $result->WO_KDG }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/TJL/Mo">{{ $result->WO_TJL }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BLC/Mo">{{ $result->WO_BLC }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/ALL/Mo">{{ $result->jumlah }}</a></td>
          </tr>
          <?php
            $total_inner += $result->WO_INNER;
            $total_bbr += $result->WO_BBR;
            $total_kdg += $result->WO_KDG;
            $total_tjl += $result->WO_TJL;
            $total_blc += $result->WO_BLC;
          ?>
          @endforeach

          <?php
              if (empty($dataReportPsScbe)){
                  $scbe_inner   = 0;
                  $scbe_bbr     = 0;
                  $scbe_kdg     = 0;
                  $scbe_tjl     = 0;
                  $scbe_blc     = 0;
                  $scbe_jumlah  = 0;  
              } 
              else {
                  $scbe_inner   = $dataReportPsScbe[0]->WO_INNER;
                  $scbe_bbr     = $dataReportPsScbe[0]->WO_BBR;
                  $scbe_kdg     = $dataReportPsScbe[0]->WO_KDG;
                  $scbe_tjl     = $dataReportPsScbe[0]->WO_TJL;
                  $scbe_blc     = $dataReportPsScbe[0]->WO_BLC;
                  $scbe_jumlah  = $dataReportPsScbe[0]->jumlah;
              }
            ?>

        
          <?php
            $total_inner = $total_inner + 0;
            $total_bbr = $total_bbr + 0;
            $total_kdg = $total_kdg + 0;
            $total_tjl = $total_tjl + 0;
            $total_blc = $total_blc + 0;
            $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          ?>
          <tr>
            <td>TOTAL</td>
            <td><a>{{ $total_inner }}</a></td>
            <td><a>{{ $total_bbr }}</a></td>
            <td><a>{{ $total_kdg }}</a></td>
            <td><a>{{ $total_tjl }}</a></td>
            <td><a>{{ $total_blc }}</a></td>
            <td><a>{{ $total }}</a></td>
          </tr>
        </table>  
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
      $(document).ready(function() {
          $('.status').select2();
      });
  </script>
@endsection
