@extends('layout')

@section('content')
@include('partial.alerts')
 <h4>Check ODP Full</h4>
  <form class="row" style="margin-bottom: 20px">
      <div class="col-md-12 {{ $errors->has('odpq') ? 'has-error' : '' }}">
          <div class="input-group">
            <input type="text" class="form-control" name="odpq" id="odp" />
              <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
          </div>
          {!! $errors->first('odpq','<p class=help-block>:message</p>') !!}
      </div>
  </form>

  <!-- <a href="/scbe/input" class="btn btn-primary btn-sm">Input ODP Full</a> -->

  @if($datas<>NULL)
      <br /> <br />
      <table class="table table-striped table-bordered dataTable">
          <tr>
              <td width="1%">#</td>
              <td>
                  Keterangan ODP 
              </td>
          </tr>

          <tr>
              <td>1</td>
              <td>
                 ODP : {{ $cari }} <br>
                 Tgl Update Statsu : {{ $datas[0]->updated_at }} <br>
                 Status ODP : {{ $datas[0]->laporan_status=='ODP FULL' ? 'ODP FULL' : ' - ' }} <br>

                <img src="/upload/evidence/{{ $datas[0]->id }}/ODP.jpg" height="400" width="500" alt="Foto Tidak Ada">

              </td>
          </tr>
      </table>
  @endif

  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function() {
      $("#odp").inputmask("AAA-AAA-A{2,3}/999");  
    })
  </script>

@endsection