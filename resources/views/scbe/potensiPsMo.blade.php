@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <a href="/dashboard/scbe/{{ $tgl }}/" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>NO.</th>
          <th>SM</th>
          <th>WO TIKET</th>
          <th>MYIR</th>
          <th>TGL WO</th>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NCLI</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>No. HP</th>    
          <th>ID SALES</th>    
          <th>JENIS PSB</th>
          <th>STATUS</th>
        </tr>

        @foreach ($data as $num => $result)
          <td>{{ ++$num }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          <td>{{ $result->orderId }}</td>

          @php
            if (!empty($result->kcontact)){
                $dataMyir = $result->kcontact;
                $pisah    = explode(';',$dataMyir);
                $myir = $pisah[1];
            }
          @endphp

          <td> {{ $myir }} </td>
          <td>{{ $result->orderDate }}</td>
          <td>{{ $result->sto }}</td> 
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderNcli }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->orderAddr }}</td>
          <td>{{ $result->ndemPots }}</td>
          
           <!-- salas -->
          <?php
              $idSales = '-';
              if (substr($result->jenisPsb,0,2)=="AO"){
                $data = $result->kcontact;
                $dataNew = explode(';', $data);
                if ($dataNew[0] == 'MI'){
                  $idSales = $dataNew[2];       
                }
                else{
                  $idSales = '-';
                }
              }
           ?>

          <td>{{ $idSales }}</td>
          <td>{{ $result->jenisPsb }}</td>
          <td>{{ $result->orderStatus }}</td>          
        </tr>
        @endforeach
      </table>
    </div>
    </div>

  </div>    <br />
      <br />
@endsection     
