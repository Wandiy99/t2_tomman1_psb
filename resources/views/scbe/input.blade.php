@extends('layout')

@section('content')
  @include('partial.alerts')
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script> -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

  <form method="GET">
    <div class="row">
      <div class="col-md-6">
        <h3>
          <a href="/scbe/search" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          Dispatch MYIR
        </h3>
      </div>

      <div class="input-group col-sm-6">
          <input type="text" class="form-control inputSearch" placeholder="Cari ODP . ." name="searchOdp" id="searchOdp" />
          <span class="input-group-btn">
              <button class="btn btn-default search">
                  <span class="glyphicon glyphicon-search"></span>
              </button>
          </span>
      </div>
    </div>
  </form>

  <div class="row">

    <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
      <!-- <div class="panel panel-default">
          <div class="panel-heading">MAP ODP</div>
          <div class="panel-body">
            <div id="Maps" class="Maps" style="height:200px;">
            </div>
            Koordinat ODP : {{ $lat }},{{ $long }}
          </div>
      </div> -->

      @if (count($odpSektor)>1)
        <div class="panel panel-default">
            <div class="panel-body">
            Rekomendasi Sektor Berdasarkan ALPRO ({{$odp}}) :
            @foreach ($odpSektor as $sektor)
                <label class="label label-primary">{{ $sektor->title }}</label>
            @endforeach
            </div>
        </div>
      @endif

      <div class="form-group col-md-4">
        <label class="control-label" for="myir">MYIR</label>
        <input type="text" name="myir" id="myir" value="{{ old('myir') }}" class="form-control">
        {!! $errors->first('myir','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="sales">Sales</label>
        <input type="text" name="sales" id="sales" value="{{ old('sales') }}" class="form-control">
        {!! $errors->first('sales','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="myir">Nama Pelanggan</label>
        <input type="text" name="nmCustomer" id="nmCustomer" value="{{old('nmCustomer')}}" class="form-control">
        {!! $errors->first('nmCustomer','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
          <label class="control-label">Nama ODP</label>
          <input type="text" name="nama_odp" id="odp2" class="form-control" rows="1" value="{{ $odp }}" readonly />
          {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
      </div>

      <div class="form-group {{ $errors->has('source_order') ? 'has-error' : '' }} col-md-4">
        <label class="control-label" for="source_order">Source Order</label>
        <select class="form-control select2" id="source_order" name="source_order">
          <option value="" readonly>- Pilih Source Order -</option>
          <option value="RISMA">RISMA</option>
          <option value="NONRISMA">NONRISMA</option>
        </select>
      </div>

      <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }} col-md-4">
        <label class="control-label" for="myir">STO</label>
        <input type="hidden" name="sto" id="sto" value="{{old('sto')}}" class="form-control">
        {!! $errors->first('sto','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="jenis_layanan">Jenis Layanan</label>
        <input type="hidden" name="jenis_layanan" id="jenis_layanan" value="{{ old('jenis_layanan') }}" class="form-control">
        {!! $errors->first('jenis_layanan', '<span class="label label-danger">:message</span>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-sektor">Sektor</label>
        <input name="input-sektor" type="hidden" id="input-sektor" class="form-control" value="{{ old('input-sektor') }}"  />
        {!! $errors->first('input-sektor', '<span class="label label-danger">:message</span>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-regu">Regu</label>
        <input name="id_regu" type="hidden" id="input-regu" class="form-control" value="{{ old('id_regu') }}" />
        {!! $errors->first('id_regu', '<span class="label label-danger">:message</span>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-timeslot">Crew ID</label>
        <input type="text" name="crewid" id="crewid" value="{{ old('crewid') }}" class="form-control">
        {!! $errors->first('crewid','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-tgl">Tanggal</label>
        <input name="tgl" type="date" id="input-tgl" class="form-control" value="{{ date('Y-m-d') }}" />
        {!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}
        {{-- <div class="col-xs-10">{!! $errors->first('tgl', '<span class="label label-danger">:message</span>') !!}</div> --}}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-timeslot">Koordinat Pelanggan</label>
        <input type="text" name="kordinatPel" id="kordinatPel" value="{{ old('kordinatPel') }}" class="form-control">
        {!! $errors->first('kordinatPel','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-timeslot">Alamat Sales</label>
        <textarea name="alamatSales" id="alamatSales" class="form-control" >{{ old('alamatSales') }}</textarea>
        {!! $errors->first('alamatSales','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-4">
        <label class="control-label" for="input-timeslot">PIC Pelanggan</label>
        <textarea name="picPelanggan" id="picPelanggan" class="form-control" maxlength="14">{{ old('picPelanggan') }}</textarea>
        {!! $errors->first('picPelanggan','<p class="label label-danger">:message</p>') !!}
      </div>

      <div class="form-group col-md-12">
        <button class="btn btn-primary">Simpan</button>
      </div>

    </form>

  </div>

  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  @endif
  <script src="/js/jquery.min.js"></script>
  <!-- <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script> -->
  <!-- <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" /> -->
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
    $(function() {
      var marker;
      function taruhMarker(peta, posisiTitik){
          if( marker ){
            // pindahkan marker
            marker.setPosition(posisiTitik);
          } else {
            // buat marker baru
            marker = new google.maps.Marker({
              position: posisiTitik,
              map: peta
            });
          }

          console.log("Posisi marker: " + posisiTitik);

      }

       function showLocation(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            alert("Latitude : " + latitude + " Longitude: " + longitude);
       }

       function errorHandler(err) {
          if(err.code == 1) {
             alert("Error: Access is denied!");
          } else if( err.code == 2) {
             alert("Error: Position is unavailable!");
          }
       }

      function initialize(lat, long) {

        var propertiPeta = {
          center:new google.maps.LatLng(lat,long),
          zoom:15,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        var peta = new google.maps.Map(document.getElementById("Maps"), propertiPeta);

        if (lat==1){
            var options = {timeout:60000};
            navigator.geolocation.getCurrentPosition(function(position){
              var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(position.coords.latitude,position.coords.longitude),
                  map: peta
              });
             }, errorHandler, options);
        }
        else{
          var marker=new google.maps.Marker({
              position: new google.maps.LatLng(lat,long),
              map: peta
            });
        }

        // even listner ketika peta diklik
        google.maps.event.addListener(peta, 'click', function(event) {
          marker.setMap(null);
          taruhMarker(this, event.latLng);
        });
      }

      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })

      var data_sektor= <?= json_encode($dataSektor) ?>;
      var sektor = function() {
        return {
          data: data_sektor,
          placeholder: 'Pilih Sektor',
          formatResult: function(data) {
          return '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-sektor').select2(sektor());
      var base_url = window.location.origin;
      $("#input-regu").select2({
          placeholder: 'Pilih Regu',
          ajax : {
            url : base_url+"/assurance/inputRegu/",
            dataType : "json",
            data: function() {
              return {
                sektor : $("#input-sektor").val()
              }
            },
            results : function (data){
              var myResults = [];
              $.each(data, function (index, item) {
                myResults.push({
                      'id': item.id,
                      'text': item.text
                  });
              });
              return {
                  results: myResults,
              };
            }
          }
      });

      var dataJenisLayanan = <?= json_encode($jenis_layanan) ?>;
      var JenisLayanan = function() {
        return {
          data: dataJenisLayanan,
          placeholder: 'Pilih Jenis Layanan',
          formatResult: function(data) {
          return '<strong style="margin-left:5px">'+data.id+'</strong> '+data.text;
          }
        }
      }
      $('#jenis_layanan').select2(JenisLayanan());

      var stoData = <?= json_encode($sto) ?>;
      var sto = function() {
        return {
          data: stoData,
          placeholder: 'Pilih STO',
          formatResult: function(data) {
          return '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#sto').select2(sto());

      // $("#searchOdp").inputmask("AAA-AAA-A{2,3}/999");

      // var day = {
      //   format: 'yyyy-mm-dd',
      //   viewMode: 0,
      //   minViewMode: 0
      // };
      // $('#input-tgl').datepicker(day).on('changeDate', function(e){
      //   $(this).datepicker('hide');
      // });;

      let dataSales = <?=json_encode($dataSales) ?>;
      console.log(dataSales);
      let sales = function () {
          return {
                data : dataSales,
                placeholder : 'Search By Kode Sales',
                formatResult : function(data){
                      return '<span class="label label-success">'+data.kode_sales+'</span> '+data.nama_sales;
                }
          }
      };

      $('#sales').select2(sales());

      $('#source_order').select2({
        placeholder: 'Pilih Source Order',
        allowClear: true
      });

      // initMap({{ $lat ? : '-3.332081'}},{{ $long ? : '114.67323'}});
      // google.maps.event.addDomListener(window, 'load', initialize( {{ $lat ? : '1'}},{{ $long ? : '1' }} ));
    })
  </script>
@endsection
