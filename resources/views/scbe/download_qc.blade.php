@extends('public_layout')
<?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=UPDATE LAPORAN QC KALSEL.xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
?>
<!DOCTYPE html>
<html>
<head>
    <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
</style>
</head>
<body>
<table table border="1" width="100%">
    <tr align="center">
        <th>SC</th>
        <th>INET</th>
        <th>CUSTOMER</th>
        <th>ORDER STATUS</th>
        <th>ODP TEK</th>
        <th>STATUS TEK</th>
        <th>DATE PS</th>
        <th>BULAN PS</th>
        <th>TAHUN PS</th>
        <th>REGU</th>
        <th>MITRA</th>
        <th>SEKTOR</th>
        <th>STO</th>
        <th>DATEL</th>
        <th>KATEGORI</th>
        <th>PRIORITAS</th>
        <th>FOTO RUMAH</th>
        <th>FOTO TEKNISI</th>
        <th>FOTO ODP</th>
        <th>FOTO REDAMAN</th>
        <th>FOTO CPE LAYANAN</th>
        <th>FOTO BERITA ACARA</th>
        <th>TAG LOKASI</th>
        <th>FOTO SURAT</th>
        <th>FOTO PROFILE MYIH</th>
        <th>TAG UNIT</th>
    </tr>
    @foreach($query as $no => $list)
    <?php
    date_default_timezone_set('Asia/Makassar');
    $tahun = date('Y', strtotime($list->dtPs));
    $tahunx = date('Y', strtotime($list->orderDatePs));
    $bulan = date('m', strtotime($list->dtPs));
    $bulanx = date('m', strtotime($list->orderDatePs));
    ?>
    <tr>
        <td>{{ $list->orderId ? : $list->sc }}</td>
        <td>{{ $list->no_inet ? : $list->internet }}</td>
        <td>{{ $list->nama ? : $list->orderName }}</td>
        <td>{{ $list->orderStatus }}</td>
        <td>{{ $list->nama_odp }}</td>
        <td>{{ $list->laporan_status }}</td>
        <td>{{ $list->datePs ? : $list->orderDatePs }}</td>
        <td>{{ $bulan ? : $bulanx }}</td>
        <td>{{ $tahun ? : $tahunx }}</td>
        <!-- <td>{{ $list->uraian ? : $list->nikTeknisi }}</td>
        @if($list->mitra=="" && $list->bo_mitra=="CITRA UTAMA INSANI")
            <td>PT CITRA UTAMA INSANI</td>
        @elseif($list->mitra=="" && $list->bo_mitra=="PT. ANUGERAH GENERASI BERSAMA")
            <td>PT ANUGERAH GENERASI BERSAMA</td>
        @elseif($list->mitra=="" && $list->bo_mitra=="SANDHY PUTRAMAKMUR")
            <td>PT SANDHY PUTRAMAKMUR</td>
        @elseif($list->mitra=="" && $list->bo_mitra=="Telkom Akses")
            <td>PT TELKOM AKSES</td>
        @elseif($list->mitra=="" && $list->bo_mitra=="UPAYA TEHNIK")
            <td>PT UPAYA TEHNIK</td>
        @else
            <td>{{ $list->mitra ? : $list->bo_mitra }}</td>
        @endif -->
        <td>{{ $list->uraian }}</td>
        <td>{{ $list->mitra }}</td>
        <td>{{ $list->sektor }}</td>
        <td>{{ $list->sto ? : $list->csto }}</td>
        <td>{{ $list->datel }}</td>
        <td>{{ $list->kategori ? : '#N/A' }}</td>
        <td>{{ $list->prioritas }}</td>
        <td>{{ $list->foto_rumah ? : ';' }}</td>
        <td>{{ $list->foto_teknisi ? : ';' }}</td>
        <td>{{ $list->foto_odp ? : ';' }}</td>
        <td>{{ $list->foto_redaman ? : ';' }}</td>
        <td>{{ $list->foto_cpe_layanan ? : ';' }}</td>
        <td>{{ $list->foto_berita_acara ? : ';' }}</td>
        <td>{{ $list->tag_lokasi ? : ';' }}</td>
        <td>{{ $list->foto_surat ? : ';' }}</td>
        <td>{{ $list->foto_profile_myih ? : ';' }}</td>
        <td>{{ $list->tag_unit }}</td>
    </tr>
    @endforeach
</table>
</body>
</html>
