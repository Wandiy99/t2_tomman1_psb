@extends('public_layout')

<?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=SCBE ".$tgl.".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
?>
<!DOCTYPE html>
<html>
<head>
    <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
</style>
</head>
<body>
<table table border="1" width="100%">
    <tr align="center">
        <th>ND_INTERNET</th>
        <th>ORDER_ID</th>
        <th>CUSTOMER</th>
        <th>JENIS_ORDER</th>
        <th>NO_KTP</th>
        <th>NO HP</th>
        <th>KCONTACT</th>
        <th>ALAMAT_STARCLICK</th>
        <th>ALAMAT_SALES</th>
        <th>PAKET_HARGA</th>
        <th>PAKET_SALES</th>
        <th>TIM</th>
        <th>CREW_ID</th>
        <th>NIK1</th>
        <th>NIK2</th>
        <th>MITRA</th>
        <th>DATEL</th>
        <th>STO</th>
        <th>SEKTOR</th>
        <th>TL_SEKTOR</th>
        <th>MYIR</th>
        <th>SC</th>
        <th>RFC_NUMBER</th>
        <th>AGENT_ID</th>
        <th>NCLI</th>
        <th>WFM_ID</th>
        <th>VALINS_ID</th>
        <th>ND_VOICE</th>
        <th>ODP_STARCLICK</th>
        <th>ORDER_STATUS</th>
        <th>DROPCORE_LABELCODE</th>
        <th>KOORDINAT_PELANGGAN_BY_TEKNISI</th>
        <th>KOORDINAT_PELANGGAN_BY_SALES</th>
        <th>JENIS_LAYANAN</th>
        <th>STATUS_TICKET</th>
        <th>ODP_TEKNISI</th>
        <th>KOORDINAT_ODP_TEKNISI</th>
        <th>DEVIASI_ODER (STATUS)</th>
        <th>DEVIASI_ODER (KM)</th>
        <th>CATATAN_TEKNISI</th>
        <th>TYPE_MODEM</th>
        <th>SN_MODEM</th>
        <th>DISPATCH_BY</th>
        <th>UPDATED_BY</th>
        <th>CATATAN_MANJA</th>
        <th>MANJA_BY</th>
        <th>MANJA_UPDATED</th>
        <th>HD_MANJA</th>
        <th>HD_FALLOUT</th>
        <th>WAKTU_DISPATCH</th>
        <th>WAKTU_AWAL_DISPATCH</th>
        <th>ORDER_DATE_PS</th>
        <th>ORDER_DATE</th>
        <th>UMUR</th>
        <th>WAKTU_STATUS</th>
        <th>STATUS_KENDALA</th>
        <th>TGL_STATUS_KENDALA</th>
        <th>TGL_STATUS_BERANGKAT</th>
        <th>STATUS_ORDER_2</th>
        <th>TGL_STATUS_ORDER_2</th>
        <th>STATUS_ORDER_3</th>
        <th>TGL_STATUS_ORDER_3</th>
    </tr>
    @foreach($query as $no => $list)
    <tr>
        <td>{{ $list->ND_INTERNET ? : $list->ND_INTERNET2 }}</td>
        <td>{{ $list->ORDER_ID }}</td>
        <td>{{ $list->CUSTOMER ? : $list->CUSTOMER2 }}</td>
        <td>{{ $list->jenisPsb }}</td>
        <td>{{ $list->KTP }}</td>
        <td>{{ $list->NOHP ? : $list->NOHP2 }}</td>
        <td>{{ $list->KCONTACT }}</td>
        <td>{{ $list->ALAMAT_STARCLICK }}</td>
        <td>{{ $list->ALAMAT_SALES }}</td>
        <td>{{ $list->PAKET_HARGA }}</td>
        <td>{{ $list->PAKET_SALES }}</td>
        <td>{{ $list->TIM }}</td>
        <td>{{ $list->DT_CREW ? : $list->R_CREW }}</td>
        <td>{{ $list->NIK1 }}</td>
        <td>{{ $list->NIK2 }}</td>
        <td>{{ $list->MITRA }}</td>
        <td>{{ $list->DATEL }}</td>
        <td>{{ $list->STO }}</td>
        <td>{{ $list->SEKTOR }}</td>
        <td>{{ $list->TL_SEKTOR }}</td>
        <td>{{ $list->MYIR }}</td>
        <td>{{ $list->SC }}</td>
        <td>{{ $list->rfc_number }}</td>
        <td>{{ $list->AGENT_ID }}</td>
        <td>{{ $list->NCLI }}</td>
        <td>{{ $list->WFM_ID ? : $list->KPT_WFM_ID }}</td>
        <td>{{ $list->valins_id }}</td>
        <td>{{ $list->ND_VOICE ? : $list->ND_VOICE2 }}</td>
        <td>{{ $list->ODP_STARCLICK }}</td>
        <td>{{ $list->ORDER_STATUS }}</td>
        <td>{{ $list->DROPCORE_LABELCODE }}</td>
        <td>{{ $list->KOORDINAT_PELANGGAN_BY_TEKNISI }}</td>
        <td>
        @if($list->KOORDINAT_PELANGGAN_BY_SALES == NULL || $list->KOORDINAT_PELANGGAN_BY_SALES == "")
            {{ $list->LAT_PELANGGAN ? : '' }},{{ $list->LON_PELANGGAN ? : '' }}
        @else
            {{ $list->KOORDINAT_PELANGGAN_BY_SALES }}
        @endif
        </td>
        <td>{{ $list->JENIS_LAYANAN }}</td>
        <td>{{ $list->STATUS_TICKET ? : 'ANTRIAN' }}</td>
        <td>{{ $list->ODP_TEKNISI }}</td>
        <td>{{ $list->KOORDINAT_ODP_TEKNISI }}</td>
        <td>
            @if($list->deviasi_pelanggan_odp > '0.250')
            NOK
            @else
            OK
            @endif
        </td>
        <td>{{ $list->deviasi_pelanggan_odp }}</td>
        <td>{{ $list->CATATAN_TEKNISI }}</td>
        <td>{{ $list->typeont }}</td>
        <td>{{ $list->snont }}</td>
        <td>{{ $list->DISPATCH_BY }}</td>
        <td>{{ $list->MODIF }}</td>
        <td>{{ $list->CATATAN_MANJA }}</td>
        <td>{{ $list->MANJA_BY }}</td>
        <td>{{ $list->MANJA_UPDATED }}</td>
        <td>{{ $list->HD_MANJA }}</td>
        <td>{{ $list->HD_FALLOUT }}</td>
        <td>{{ $list->WAKTU_DISPATCH }}</td>
        <td>{{ $list->WAKTU_AWAL_DISPATCH }}</td>
        <td>{{ $list->orderDatePs }}</td>
        <td>{{ $list->orderDate }}</td>
        <td>
            @php
                if ($list->id_dt == null)
                {
                    $tglOrder = $list->orderDate;
                } else {
                    $tglOrder = $list->WAKTU_AWAL_DISPATCH;
                }
                $awal  = date_create($tglOrder);
                $akhir = date_create();
                $diff  = date_diff( $awal, $akhir );

                if ($diff->d > 0)
                {
                    $waktu = $diff->d.' Hari '.$diff->h.' Jam '.$diff->i.' Menit';
                } else {
                    $waktu = $diff->h.' Jam '.$diff->i.' Menit';
                }
            @endphp

            {{ $waktu }}
        </td>
        <td>{{ $list->WAKTU_STATUS }}</td>
        <td>{{ $list->status_kendala ? : '-' }}</td>
        <td>{{ $list->tgl_status_kendala ? : '-' }}</td>
        <td>{{ $list->tgl_status_berangkat ? : '-' }}</td>

        <td>{{ $list->status_order2 ? : '-' }}</td>
        <td>{{ $list->tgl_status_order2 ? : '-' }}</td>
        <td>{{ $list->status_order3 ? : '-' }}</td>
        <td>{{ $list->tgl_status_order3 ? : '-' }}</td>
    </tr>
    @endforeach
</table>
</body>
</html>
