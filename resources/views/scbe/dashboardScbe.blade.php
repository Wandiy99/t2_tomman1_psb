@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle!important;
    }
    td {
      text-align: center;
      vertical-align: middle!important;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .warna8{
      background-color: #483D8B;
    }

    .warna9{
      background-color: #FFE4B5;
    }

    .text2{
      color: black !important;
    }

    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }

    .text1:visited{
      color: white !important;
    }


    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>
  <div id="screnshoot">
    <!-- <h3>Periode {{ $tgl }}</h3> -->
    <center>
    <h3 style="font-weight: bold; color:black !important">DASHBOARD PROVISIONING<br />PERIODE {{ $tgl }}</h3>
    </center>
    <br />

    <div class="row">
      <div class="col-sm-12">
           <h3>Report Potensi PS [Periode {{$tgl}}]</h3>
           <table class="table table-striped table-bordered dataTable">
              <tr>
                <th class="warna7 text1">DATEL</th>
                <th class="warna2 text1">PRA SC</th>
                <th class="warna2 text1">WO SC AO</th>
                <th class="warna2 text1">WO PDA</th>
                <th class="warna2 text1">WO SC MO</th>
                <th class="warna2 text1">HR</th>
                <th class="warna7 text1">A.COMP</th>
                <th class="warna7 text1">PS AO</th>
                <th class="warna7 text1">EST</th>
              </tr>
              <?php
                $total_PS = 0;
                $total_ACTCOMP = 0;
                $total_HR = 0;
                $total_SISA_WO = 0;
                $total_SISA_WO_PRA = 0;
                $total_SISA_WO_PDA = 0;
                $total_SISA_WO_MO = 0;
              ?>
              @foreach ($potensiPS as $result)
              <?php
                $total_PS += $result->PS;
                $total_ACTCOMP += $result->ACTCOMP;
                $total_HR += $result->HR;
                $total_SISA_WO += $result->SISA_WO;
                $total_SISA_WO_PRA += $result->SISA_WO_PRA;
                $total_SISA_WO_PDA += $result->SISA_WO_PDA;
                $total_SISA_WO_MO += $result->SISA_WO_MO;
              ?>
              <tr>
              <td>{{ $result->datel }}</td>
              <td><a href="/sisa_wo_pra/{{ $result->datel }}">{{ $result->SISA_WO_PRA }}</a></td>
              <td><a href="/sisa_wo/{{ $result->datel }}">{{ $result->SISA_WO }}</a></td>
              <td><a href="/sisa_wo_pda/{{ $result->datel }}">{{ $result->SISA_WO_PDA }}</a></td>
              <td><a href="/sisa_wo_mo/{{ $result->datel }}">{{ $result->SISA_WO_MO }}</a></td>
              <td><a href="/wo_hr/{{ $result->datel }}">{{ $result->HR }}</td>
              <td><a href="/actcomp/{{ $result->datel }}">{{ $result->ACTCOMP }}</a></td>
              <td><a href="/wo_ps/{{ $result->datel }}/{{ $tgl }}">{{ $result->PS }}</a></td>
              <td>{{ $result->PS+$result->ACTCOMP }}</td>
              </tr>
              @endforeach
              <tr>
                  <td class="warna7 text1">TOTAL</td>
                  <td class="warna7 text1"><a class="text1" href="/sisa_wo_pra/ALL">{{ $total_SISA_WO_PRA  }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/sisa_wo/ALL">{{ $total_SISA_WO }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/sisa_wo_pda/ALL">{{ $total_SISA_WO_PDA }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/sisa_wo_mo/ALL">{{ $total_SISA_WO_MO }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/wo_hr/ALL"> {{ $total_HR }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/actcomp/ALL">{{ $total_ACTCOMP }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/wo_ps/ALL/{{ $tgl }}">{{ $total_PS }}</a></td>
                  <td class="warna7 text1">{{ $total_PS+$total_ACTCOMP }}</td>
              </tr>
              </table>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                            <h5 class="text-muted vb">PRA ORDER SCBE</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-danger">{{ $total_SISA_WO_PRA  }}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                            <h5 class="text-muted vb">ORDER SC</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-warning">{{ $total_SISA_WO }}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                            <h5 class="text-muted vb">PDA</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-info">{{ $total_SISA_WO_PDA }}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                            <h5 class="text-muted vb">ADD ON</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-success">{{ $total_SISA_WO_MO }}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-heading">Data Solusi Kendala < 3 hari</div>
      <div class="panel-body table-responsive" style="padding:0px !important">
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2" class="warna7 text1 align-middle">DATEL</th>

            <?php
                $countHs    = 0;
                $countDaman = 0;
                $countAmo   = 0;
                $countAso   = 0;
                $countClose = 0;
                $totalSum   = 0;

                foreach($dataKendala_kurang3hari as $kendala){
                    if ($kendala->stts_dash=='hs'){
                        $countHs += 1;
                    }
                    else if ($kendala->stts_dash=='daman'){
                        $countDaman += 1;
                    }
                    else if ($kendala->stts_dash=="amo"){
                        $countAmo += 1;
                    }
                    else if ($kendala->stts_dash=="aso"){
                        $countAso += 1;
                    }
                    // else if ($kendala->stts_dash=="close"){
                    //   $countClose += 1;
                    // }
                }

                $witel = ['WO_BANJARMASIN', 'WO_BANJARBARU', 'WO_TANJUNG', 'WO_BATULICIN'];
             ?>

            <th colspan="<?= $countHs; ?>" class="warna1 text1">HS</th>

            @if($countDaman<>0)
              <th colspan="<?= $countDaman; ?>" class="warna2 text1">DAMAN</th>
            @endif

            <th colspan="<?= $countAmo; ?>" class="warna3 text1">AMO</th>
            <th colspan="<?= $countAso; ?>" class="warna4 text1">ASO</th>
            <!-- <th colspan="<?= $countClose; ?>" class="warna8 text1">CLOSE</th> -->
            <th rowspan="2" class="warna7 text1"><b>TOTAL</b></th>
            <!-- <th colspan="2" class="warna9 text2">CLOSE</th> -->
          </tr>

          <tr>
              @foreach($dataKendala_kurang3hari as $result)
                  @php
                      $warna = '';
                      if($result->stts_dash=="hs"){
                          $warna = "warna1 text1";
                      }
                      elseif($result->stts_dash=="daman"){
                          $warna = "warna2 text1";
                      }
                      elseif($result->stts_dash=="amo"){
                          $warna = "warna3 text1";
                      }
                      elseif($result->stts_dash=="aso"){
                          $warna = "warna4 text1";
                      }
                      elseif($result->stts_dash=="close"){
                          $warna = "warna8 text1";
                      };

                  @endphp

              <td class="{{ $warna }}"><b>{{ $result->laporan_status }}</b></td>
              @endforeach
              <!-- <td class="warna9 text2<!-- ">UP</td>
              <td class="warna9 text2">CANCEL</td> -->
          </tr>


          @foreach($witel as $list)
            <tr>
                @php
                    if ($list=='WO_BANJARMASIN'){
                        $wtl = 'BANJARMASIN';
                        $sttaus = 'INNER';
                    }
                    else if($list=='WO_BANJARBARU'){
                        $wtl = 'BANJARBARU';
                        $sttaus = 'BBR';
                    }
                    else if($list=='WO_TANJUNG'){
                        $wtl = 'TANJUNG';
                        $sttaus = 'TJL';
                    }
                    else if($list=='WO_BATULICIN'){
                        $wtl = 'BATULICIN';
                        $sttaus = 'BLC';
                    }
                 @endphp

                  <td>{{ $wtl }}</td>

                <?php
                    $jmlInner = 0;
                 ?>
                @foreach($dataKendala_kurang3hari as $result)


                    <td><a href="/dashboard/scbeList/kurang3hari/ALL/{{ $result->laporan_status }}/{{ $wtl }}/KENDALA"><b>{{ @$result->$list }}</b></a></td>

                    @php
                      if ($list=="WO_BANJARMASIN"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BANJARBARU"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_TANJUNG"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BATULICIN"){
                        $jmlInner += @$result->$list;
                      }


                    @endphp

                @endforeach
                <?php
                $totalSum += $jmlInner;
                ?>

                <td><a href="/dashboard/scbeList/kurang3hari/ALL/ALL/{{ $wtl }}/KENDALA">{{ $jmlInner }}</a></td>
               <!--  <td>0</td>
                <td>0</td> -->
             </tr>
          @endforeach

          <!-- Jumlah  -->
          <tr>
              <td class="warna7 text1">JUMLAH</td>

              <?php
                  $jmlInner = 0;
               ?>
              @foreach($dataKendala_kurang3hari as $result)

              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/kurang3hari/ALL/{{ $result->laporan_status }}/ALL/KENDALA"><b>{{ $result->jumlah }}</b></a></td>

              @endforeach
              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/kurang3hari/ALL/ALL/ALL/KENDALA">{{ $totalSum }}</a></td><!--
              <td class="warna7 text1">0</td>
              <td class="warna7 text1">0</td> -->
          </tr>

          <!-- end jumlah -->

        </table>
        </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-heading">Data Solusi Kendala 3 - 7 hari</div>
      <div class="panel-body table-responsive" style="padding:0px !important">
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2" class="warna7 text1 align-middle">DATEL</th>

            <?php
                $countHs    = 0;
                $countDaman = 0;
                $countAmo   = 0;
                $countAso   = 0;
                $countClose   = 0;
                $totalSum   = 0;

                foreach($dataKendala_x3sd7hari as $kendala){
                    if ($kendala->stts_dash=='hs'){
                        $countHs += 1;
                    }
                    else if ($kendala->stts_dash=='daman'){
                        $countDaman += 1;
                    }
                    else if ($kendala->stts_dash=="amo"){
                        $countAmo += 1;
                    }
                    else if ($kendala->stts_dash=="aso"){
                        $countAso += 1;
                    }
                    else if ($kendala->stts_dash=="close"){
                        $countClose += 1;
                    }
                }

                $witel = ['WO_BANJARMASIN', 'WO_BANJARBARU', 'WO_TANJUNG', 'WO_BATULICIN'];
             ?>

            <th colspan="<?= $countHs; ?>" class="warna1 text1">HS</th>

            @if($countDaman<>0)
              <th colspan="<?= $countDaman; ?>" class="warna2 text1">DAMAN</th>
            @endif

            <th colspan="<?= $countAmo; ?>" class="warna3 text1">AMO</th>
            <th colspan="<?= $countAso; ?>" class="warna4 text1">ASO</th>
            <!-- <th colspan="<?= $countClose; ?>" class="warna8 text1"><b>CLOSE</b></th> -->
            <th rowspan="2" class="warna7 text1"><b>TOTAL</b></th>
          </tr>

          <tr>
              @foreach($dataKendala_x3sd7hari as $result)
                  @php
                      $warna = '';
                      if($result->stts_dash=="hs"){
                          $warna = "warna1 text1";
                      }
                      elseif($result->stts_dash=="daman"){
                          $warna = "warna2 text1";
                      }
                      elseif($result->stts_dash=="amo"){
                          $warna = "warna3 text1";
                      }
                      elseif($result->stts_dash=="aso"){
                          $warna = "warna4 text1";
                      }elseif($result->stts_dash=="close"){
                          $warna = "warna8 text1";
                      };

                  @endphp

                  <td class="{{ $warna }}"><b>{{ $result->laporan_status }}</b></td>
              @endforeach
          </tr>


          @foreach($witel as $list)
            <tr>
                @php
                    if ($list=='WO_BANJARMASIN'){
                        $wtl = 'BANJARMASIN';
                        $sttaus = 'INNER';
                    }
                    else if($list=='WO_BANJARBARU'){
                        $wtl = 'BANJARBARU';
                        $sttaus = 'BBR';
                    }
                    else if($list=='WO_TANJUNG'){
                        $wtl = 'TANJUNG';
                        $sttaus = 'TJL';
                    }
                    else if($list=='WO_BATULICIN'){
                        $wtl = 'BATULICIN';
                        $sttaus = 'BLC';
                    }
                 @endphp

                  <td>{{ $wtl }}</td>

                <?php
                    $jmlInner = 0;
                 ?>
                @foreach($dataKendala_x3sd7hari as $result)

                    <td><a href="/dashboard/scbeList/x3sd7hari/ALL/{{ $result->laporan_status }}/{{ $wtl }}/KENDALA"><b>{{ @$result->$list }}</b></a></td>

                    @php
                      if ($list=="WO_BANJARMASIN"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BANJARBARU"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_TANJUNG"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BATULICIN"){
                        $jmlInner += @$result->$list;
                      }


                    @endphp

                @endforeach
                <?php
                $totalSum += $jmlInner;
                ?>
                <td><a href="/dashboard/scbeList/x3sd7hari/ALL/ALL/{{ $wtl }}/KENDALA">{{ $jmlInner }}</a></td>
             </tr>
          @endforeach

          <!-- Jumlah  -->
          <tr>
              <td class="warna7 text1">JUMLAH</td>

              <?php
                  $jmlInner = 0;
               ?>
              @foreach($dataKendala_x3sd7hari as $result)

                <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/x3sd7hari/ALL/{{ $result->laporan_status }}/ALL/KENDALA"><b>{{ $result->jumlah }}</b></a></td>


              @endforeach
              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/x3sd7hari/ALL/ALL/ALL/KENDALA">{{ $totalSum }}</a></td>
          </tr>

          <!-- end jumlah -->

        </table>
        </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-heading">Data Solusi Kendala > 7 hari</div>
      <div class="panel-body table-responsive" style="padding:0px !important">
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2" class="warna7 text1 align-middle">DATEL</th>

            <?php
                $countHs    = 0;
                $countDaman = 0;
                $countAmo   = 0;
                $countAso   = 0;
                $countClose = 0;
                $totalSum   = 0;

                foreach($dataKendala_lebih7hari as $kendala){
                    if ($kendala->stts_dash=='hs'){
                        $countHs += 1;
                    }
                    else if ($kendala->stts_dash=='daman'){
                        $countDaman += 1;
                    }
                    else if ($kendala->stts_dash=="amo"){
                        $countAmo += 1;
                    }
                    else if ($kendala->stts_dash=="aso"){
                        $countAso += 1;
                    }
                }

                $witel = ['WO_BANJARMASIN', 'WO_BANJARBARU', 'WO_TANJUNG', 'WO_BATULICIN'];
             ?>

            <th colspan="<?= $countHs; ?>" class="warna1 text1">HS</th>
            @if($countDaman<>0)
              <th colspan="<?= $countDaman; ?>" class="warna2 text1">DAMAN</th>
            @endif
            <th colspan="<?= $countAmo; ?>" class="warna3 text1">AMO</th>
            <th colspan="<?= $countAso; ?>" class="warna4 text1">ASO</th>
            <th rowspan="2" class="warna7 text1"><b>TOTAL</b></th>
          </tr>

          <tr>
              @foreach($dataKendala_lebih7hari as $result)
                  @php
                      $warna = '';
                      if($result->stts_dash=="hs"){
                          $warna = "warna1 text1";
                      }
                      elseif($result->stts_dash=="daman"){
                          $warna = "warna2 text1";
                      }
                      elseif($result->stts_dash=="amo"){
                          $warna = "warna3 text1";
                      }
                      elseif($result->stts_dash=="aso"){
                          $warna = "warna4 text1";
                      }
                  @endphp

                  <td class="{{ $warna }}"><b>{{ $result->laporan_status }}</b></td>
              @endforeach
          </tr>


          @foreach($witel as $list)
            <tr>
                @php
                    if ($list=='WO_BANJARMASIN'){
                        $wtl = 'BANJARMASIN';
                        $sttaus = 'INNER';
                    }
                    else if($list=='WO_BANJARBARU'){
                        $wtl = 'BANJARBARU';
                        $sttaus = 'BBR';
                    }
                    else if($list=='WO_TANJUNG'){
                        $wtl = 'TANJUNG';
                        $sttaus = 'TJL';
                    }
                    else if($list=='WO_BATULICIN'){
                        $wtl = 'BATULICIN';
                        $sttaus = 'BLC';
                    }
                 @endphp

                  <td>{{ $wtl }}</td>

                <?php
                    $jmlInner = 0;
                 ?>
                @foreach($dataKendala_lebih7hari as $result)

                    <td><a href="/dashboard/scbeList/lebih7hari/ALL/{{ $result->laporan_status }}/{{ $wtl }}/KENDALA"><b>{{ @$result->$list }}</b></a></td>

                    @php
                      if ($list=="WO_BANJARMASIN"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BANJARBARU"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_TANJUNG"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BATULICIN"){
                        $jmlInner += @$result->$list;
                      }


                    @endphp

                @endforeach
                <?php
                $totalSum += $jmlInner;
                ?>
                <td><a href="/dashboard/scbeList/lebih7hari/ALL/ALL/{{ $wtl }}/KENDALA">{{ $jmlInner }}</a></td>
             </tr>
          @endforeach

          <!-- Jumlah  -->
          <tr>
              <td class="warna7 text1">JUMLAH</td>

              <?php
                  $jmlInner = 0;
               ?>
              @foreach($dataKendala_lebih7hari as $result)
              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/lebih7hari/ALL/{{ $result->laporan_status }}/ALL/KENDALA"><b>{{ $result->jumlah }}</b></a></td>
              @endforeach
              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/lebih7hari/ALL/ALL/ALL/KENDALA">{{ $totalSum }}</a></td>
          </tr>

          <!-- end jumlah -->

        </table>
        </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-default">
      <div class="panel-heading">Data Solusi Kendala {{ $tgl }}</div>
      <div class="panel-body table-responsive" style="padding:0px !important">
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2" class="warna7 text1 align-middle">DATEL</th>

            <?php
                $countHs    = 0;
                $countDaman = 0;
                $countAmo   = 0;
                $countAso   = 0;
                $countClose   = 0;
                $countKendala = 0;
                $totalSum   = 0;

                foreach($dataKendala as $kendala){
                    if ($kendala->stts_dash=='hs'){
                        $countHs += 1;
                    }
                    else if ($kendala->stts_dash=='daman'){
                        $countDaman += 1;
                    }
                    else if ($kendala->stts_dash=="amo"){
                        $countAmo += 1;
                    }
                    else if ($kendala->stts_dash=="aso"){
                        $countAso += 1;
                    }
                    else if ($kendala->stts_dash=="kendala"){
                      $countKendala += 1;
                    }
                    // else if ($kendala->stts_dash=="close"){
                    //   $countClose += 1;
                    // }
                }

                $witel = ['WO_BANJARMASIN', 'WO_BANJARBARU', 'WO_TANJUNG', 'WO_BATULICIN'];
             ?>
            <th colspan="<?= $countHs; ?>" class="warna1 text1">HS</th>
            @if($countDaman<>0)
              <th colspan="<?= $countDaman; ?>" class="warna2 text1">DAMAN</th>
            @endif
            <th colspan="<?= $countAmo; ?>" class="warna3 text1">AMO</th>
            <th colspan="<?= $countAso; ?>" class="warna4 text1">ASO</th>
            @if($countKendala<>0)
            <th colspan="<?= $countKendala; ?>" class="warna8 text1">KENDALA</th>
            @endif
            <th rowspan="2" class="warna7 text1"><b>TOTAL</b></th>
            <!-- <th colspan="2" class="warna9 text2">CLOSE</th> -->
          </tr>

          <tr>
              @foreach($dataKendala as $result)
                  @php
                      $warna = '';
                      if($result->stts_dash=="hs"){
                          $warna = "warna1 text1";
                      }
                      elseif($result->stts_dash=="daman"){
                          $warna = "warna2 text1";
                      }
                      elseif($result->stts_dash=="amo"){
                          $warna = "warna3 text1";
                      }
                      elseif($result->stts_dash=="aso"){
                          $warna = "warna4 text1";
                      }
                      elseif($result->stts_dash=="kendala"){
                          $warna = "warna8 text1";
                      }

                  @endphp

                  <td class="{{ $warna }}"><b>{{ $result->laporan_status }}</b></td>
              @endforeach
              <!--
              <td class="warna9 text2">UP</td>
              <td class="warna9 text2">CANCEL</td> -->
          </tr>


          @foreach($witel as $list)
            <tr>
                @php
                    if ($list=='WO_BANJARMASIN'){
                        $wtl = 'BANJARMASIN';
                        $sttaus = 'INNER';
                    }
                    else if($list=='WO_BANJARBARU'){
                        $wtl = 'BANJARBARU';
                        $sttaus = 'BBR';
                    }
                    else if($list=='WO_TANJUNG'){
                        $wtl = 'TANJUNG';
                        $sttaus = 'TJL';
                    }
                    else if($list=='WO_BATULICIN'){
                        $wtl = 'BATULICIN';
                        $sttaus = 'BLC';
                    }
                 @endphp

                  <td>{{ $wtl }}</td>

                <?php
                    $jmlInner = 0;
                 ?>
                @foreach($dataKendala as $result)

                    <td><a href="/dashboard/scbeList/{{ $tgl }}/{{ $result->laporan_status }}/{{ $wtl }}/KENDALA"><b>{{ @$result->$list }}</b></a></td>

                    @php
                      if ($list=="WO_BANJARMASIN"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BANJARBARU"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_TANJUNG"){
                        $jmlInner += @$result->$list;
                      }
                      else if($list=="WO_BATULICIN"){
                        $jmlInner += @$result->$list;
                      }


                    @endphp

                @endforeach
                <?php
                $totalSum += $jmlInner;
                ?>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $wtl }}/KENDALA">{{ $jmlInner }}</a></td>
                <!-- <td>0</td>
                <td>0</td> -->
             </tr>
          @endforeach

          <!-- Jumlah  -->
          <tr>
              <td class="warna7 text1">JUMLAH</td>

              <?php
                  $jmlInner = 0;
               ?>
              @foreach($dataKendala as $result)

              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/{{ $result->laporan_status }}/ALL/KENDALA"><b>{{ $result->jumlah }}</b></a></td>
              @endforeach
              <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/KENDALA">{{ $totalSum }}</a></td>
              <!-- <td class="warna7 text1">0</td>
              <td class="warna7 text1">0</td> -->
          </tr>

          <!-- end jumlah -->

        </table>
        </div>
        </div>
      </div>

    </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
           <h3>PT2 Kendala [Periode {{$tgl}}]</h3>
           <table class="table table-striped table-bordered dataTable">
              <tr>
                <th class="warna7 text1">DATEL</th>
                <th class="warna2 text1">ANTRIAN</th>
                <th class="warna2 text1">BERANGKAT</th>
                <th class="warna2 text1">OGP</th>
                <th class="warna2 text1">KENDALA</th>
                <th class="warna2 text1">SELESAI</th>
                <th class="warna7 text1">JUMLAH</th>
              </tr>
              <?php
                $total_NO_UPDATE = 0;
                $total_BERANKAT = 0;
                $total_OGP = 0;
                $total_KENDALA = 0;
                $total_SELESAI = 0;
                $total_JUMLAH = 0;
              ?>
              @foreach ($pt2_master as $result)
              <?php
                $total_NO_UPDATE += $result->NO_UPDATE;
                $total_BERANKAT += $result->BERANGKAT;
                $total_OGP += $result->OGP;
                $total_KENDALA += $result->KENDALA;
                $total_SELESAI += $result->SELESAI;
                $total_JUMLAH += $result->JUMLAH;
              ?>
              <tr>
              <td style="text-align:left !important">{{ $result->DATEL }}</td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/NO_UPDATE/{{ $tgl }}">{{ $result->NO_UPDATE }}</a></td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/Berangkat/{{ $tgl }}">{{ $result->BERANGKAT }}</a></td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/Ogp/{{ $tgl }}">{{ $result->OGP }}</a></td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/Kendala/{{ $tgl }}">{{ $result->KENDALA }}</a></td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/Selesai/{{ $tgl }}">{{ $result->SELESAI }}</td>
              <td><a href="/pt2/kendala/{{ $result->DATEL }}/ALL/{{ $tgl }}">{{ $result->JUMLAH }}</td>
              </tr>
              @endforeach
              <tr>
                  <td class="warna7 text1">TOTAL</td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/NO_UPDATE/{{ $tgl }}">{{ $total_NO_UPDATE  }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/Berangkat/{{ $tgl }}">{{ $total_BERANKAT }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/Ogp/{{ $tgl }}">{{$total_OGP }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/Kendala/{{ $tgl }}">{{ $total_KENDALA }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/Selesai/{{ $tgl }}"> {{ $total_SELESAI }}</a></td>
                  <td class="warna7 text1"><a class="text1" href="/pt2/kendala/ALL/ALL/{{ $tgl }}"> {{ $total_JUMLAH }}</a></td>
              </tr>
              </table>
      </div>
    </div>

    <div class=" row">
      <div class="col-sm-6">
      <h3>Progress Pra Provisioning [Periode {{$tgl}}]</h3>
       <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th class="warna7 text1">STATUS</th>
            <th class="warna7 text1">INNER</th>
            <th class="warna7 text1">BBR</th>
            <th class="warna7 text1">TJL</th>
            <th class="warna7 text1">BLC</th>
            <th class="warna7 text1">JUMLAH</th>
          </tr>
          @php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $jumlah = 0;
            $total_inner = 0;
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          @endphp
            @foreach ($data as $result)
            @php
              $jumlah = $result->WO_BANJARMASIN + $result->WO_BANJARBARU + $result->WO_TANJUNG + $result->WO_BATULICIN;
            @endphp
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                <tr>
                    <td>{{ $result->laporan_status }}</td>
                    <!-- <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BANJARMASIN/PROV">{{ $result->WO_BANJARMASIN }}</a></td>
                    <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BANJARBARU/PROV">{{ $result->WO_BANJARBARU }}</a></td>
                    <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/TANJUNG/PROV">{{ $result->WO_TANJUNG }}</a></td>
                    <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BATULICIN/PROV">{{ $result->WO_BATULICIN }}</a></td>
                    <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL/PROV">{{ $jumlah }}</a></td> -->
                    <td>{{ $result->WO_BANJARMASIN }}</td>
                    <td>{{ $result->WO_BANJARBARU }}</td>
                    <td>{{ $result->WO_TANJUNG }}</td>
                    <td>{{ $result->WO_BATULICIN }}</td>
                    <td>{{ $jumlah }}</td>
                </tr>
                @php
                  $total_inner += $result->WO_BANJARMASIN;
                  $total_bbr += $result->WO_BANJARBARU;
                  $total_tjl += $result->WO_TANJUNG;
                  $total_blc += $result->WO_BATULICIN;
                @endphp
                @else
                @php
                  $no_update_jumlah += $result->WO_BANJARMASIN + $result->WO_BANJARBARU + $result->WO_TANJUNG + $result->WO_BATULICIN;
                  $no_update_inner += $result->WO_BANJARMASIN;
                  $no_update_bbr += $result->WO_BANJARBARU;
                  $no_update_tjl += $result->WO_TANJUNG;
                  $no_update_blc += $result->WO_BATULICIN;
                @endphp
                @endif
            @endforeach
          <tr>
            <td>ANTRIAN</td>
            <!-- <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/BANJARMASIN/PROV">{{ $no_update_inner }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/BANJARBARU/PROV">{{ $no_update_bbr }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/TANJUNG/PROV">{{ $no_update_tjl }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/BATULICIN/PROV">{{ $no_update_blc }}</a></td>
            <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ANTRIAN/ALL/PROV">{{ $no_update_jumlah }}</a></td> -->
            <td>{{ $no_update_inner }}</td>
            <td>{{ $no_update_bbr }}</td>
            <td>{{ $no_update_tjl }}</td>
            <td>{{ $no_update_blc }}</td>
            <td>{{ $no_update_jumlah }}</td>
          </tr>
          @php
            $total_inner = $total_inner + $no_update_inner;
            $total_bbr = $total_bbr + $no_update_bbr;
            $total_tjl = $total_tjl + $no_update_tjl;
            $total_blc = $total_blc + $no_update_blc;
            $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          @endphp
          <tr>
            <td class="warna7 text1">TOTAL</td>
            <!-- <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BANJARMASIN/PROV">{{ $total_inner }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BANJARBARU/PROV">{{ $total_bbr }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/TANJUNG/PROV">{{ $total_tjl }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BATULICIN/PROV">{{ $total_blc }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/ALL/PROV">{{ $total }}</a></td> -->
            <td class="warna7 text1">{{ $total_inner }}</td>
            <td class="warna7 text1">{{ $total_bbr }}</td>
            <td class="warna7 text1">{{ $total_tjl }}</td>
            <td class="warna7 text1">{{ $total_blc }}</td>
            <td class="warna7 text1">{{ $total }}</td>
          </tr>
        </table>
      </div>
      <div class="col-sm-6">
      <h3>Progress After Provisioning [Periode {{$tgl}}]</h3>
       <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th class="warna7 text1">STATUS</th>
            <th class="warna7 text1">INNER</th>
            <th class="warna7 text1">BBR</th>
            <th class="warna7 text1">TJL</th>
            <th class="warna7 text1">BLC</th>
            <th class="warna7 text1">JUMLAH</th>
          </tr>
          @php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $jumlah = 0;
            $total_inner = 0;
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          @endphp
            @foreach ($data_after as $result)
            @php
              $jumlah = $result->WO_BANJARMASIN + $result->WO_BANJARBARU + $result->WO_TANJUNG + $result->WO_BATULICIN;
            @endphp
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                <tr>
                    <td>{{ $result->laporan_status }}</td>
                    <!-- <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BANJARMASIN/PROV">{{ $result->WO_BANJARMASIN }}</a></td>
                    <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BANJARBARU/PROV">{{ $result->WO_BANJARBARU }}</a></td>
                    <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/{{ $result->laporan_status }}/TANJUNG/PROV">{{ $result->WO_TANJUNG }}</a></td>
                    <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/{{ $result->laporan_status }}/BATULICIN/PROV">{{ $result->WO_BATULICIN }}</a></td>
                    <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL/PROV">{{ $jumlah }}</a></td> -->
                    <td>{{ $result->WO_BANJARMASIN }}</td>
                    <td>{{ $result->WO_BANJARBARU }}</td>
                    <td>{{ $result->WO_TANJUNG }}</td>
                    <td>{{ $result->WO_BATULICIN }}</td>
                    <td>{{ $jumlah }}</td>
                </tr>
                @php
                  $total_inner += $result->WO_BANJARMASIN;
                  $total_bbr += $result->WO_BANJARBARU;
                  $total_tjl += $result->WO_TANJUNG;
                  $total_blc += $result->WO_BATULICIN;
                @endphp
                @else
                @php
                  $no_update_jumlah += $result->WO_BANJARMASIN + $result->WO_BANJARBARU + $result->WO_TANJUNG + $result->WO_BATULICIN;
                  $no_update_inner += $result->WO_BANJARMASIN;
                  $no_update_bbr += $result->WO_BANJARBARU;
                  $no_update_tjl += $result->WO_TANJUNG;
                  $no_update_blc += $result->WO_BATULICIN;
                @endphp
                @endif
            @endforeach
          <tr>
            <td>ANTRIAN</td>
            <!-- <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ANTRIAN/BANJARMASIN/PROV">{{ $no_update_inner }}</a></td>
            <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ANTRIAN/BANJARBARU/PROV">{{ $no_update_bbr }}</a></td>
            <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ANTRIAN/TANJUNG/PROV">{{ $no_update_tjl }}</a></td>
            <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ANTRIAN/BATULICIN/PROV">{{ $no_update_blc }}</a></td>
            <td><a href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ANTRIAN/ALL/PROV">{{ $no_update_jumlah }}</a></td> -->
            <td>{{ $no_update_inner }}</td>
            <td>{{ $no_update_bbr }}</td>
            <td>{{ $no_update_tjl }}</td>
            <td>{{ $no_update_blc }}</td>
            <td>{{ $no_update_jumlah }}</td>
          </tr>
          @php
            $total_inner = $total_inner + $no_update_inner;
            $total_bbr = $total_bbr + $no_update_bbr;
            $total_tjl = $total_tjl + $no_update_tjl;
            $total_blc = $total_blc + $no_update_blc;
            $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          @endphp
          <tr>
            <td class="warna7 text1">TOTAL</td>
            <!-- <td class="warna7 text1"><a class="link" href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ALL/BANJARMASIN/PROV">{{ $total_inner }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ALL/BANJARBARU/PROV">{{ $total_bbr }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ALL/TANJUNG/PROV">{{ $total_tjl }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ALL/BATULICIN/PROV">{{ $total_blc }}</a></td>
            <td class="warna7 text1"><a class="link" href="/dashboard/scbeListAfter/{{ $tgl }}/ALL/ALL/ALL/PROV">{{ $total }}</a></td> -->
            <td class="warna7 text1">{{ $total_inner }}</td>
            <td class="warna7 text1">{{ $total_bbr }}</td>
            <td class="warna7 text1">{{ $total_tjl }}</td>
            <td class="warna7 text1">{{ $total_blc }}</td>
            <td class="warna7 text1">{{ $total }}</td>
          </tr>
        </table>
      </div>
      <div class="col-sm-6">
           <h3>Report Potensi PS [Periode {{$tgl}}]</h3>
           <table class="table table-striped table-bordered dataTable">
              <tr></tr>
                <th class="warna7 text1">Status (AO)</th>
                <th class="warna7 text1">INNER</th>
                <th class="warna7 text1">BBR</th>
                <th class="warna7 text1">TJL</th>
                <th class="warna7 text1">BLC</th>
                <th class="warna7 text1">JUMLAH</th>
              </tr>
              <?php
                $no_update_jumlah = 0;
                $no_update_inner = 0;
                $no_update_bbr = 0;
                $no_update_kdg = 0;
                // $no_update_tjl = 0;
                $no_update_blc = 0;
                $total_inner = 0;
                $total_bbr = 0;
                $total_kdg = 0;
                // $total_tjl = 0;
                $total_blc = 0;
                $total = 0;
              ?>

              <tr>
                <td>Completed (PS)</td>
                <?php
                    $completedPsInner = 0;
                    $completedPsBbr   = 0;
                    $completedPsKdg   = 0;
                    $completedPsBlc   = 0;
                    $completedPsJumlah= 0;
                 ?>

                @if (count($dataReportPsStarUp)<>0)
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BANJARMASIN">{{ $dataReportPsStarUp[0]->WO_INNER ?: 0 }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BANJARBARU">{{ $dataReportPsStarUp[0]->WO_BBR }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/TANJUNG">{{ $dataReportPsStarUp[0]->WO_KDG }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BATULICIN">{{ $dataReportPsStarUp[0]->WO_BLC }}</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/ALL">{{ $dataReportPsStarUp[0]->jumlah }}</a></td>
                  <?php
                    $completedPsInner = $dataReportPsStarUp[0]->WO_INNER;
                    $completedPsBbr   = $dataReportPsStarUp[0]->WO_BBR;
                    $completedPsKdg   = $dataReportPsStarUp[0]->WO_KDG;
                    $completedPsBlc   = $dataReportPsStarUp[0]->WO_BLC;
                    $completedPsJumlah= $dataReportPsStarUp[0]->jumlah;
                 ?>
                @else
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/INNER">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BBR">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/KDG">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/BLC">0</a></td>
                  <td><a href="/dashboard/potensiPsListPs/{{ $tgl }}/ALL/completedPs/ALL">0</a></td>
                @endif
              </tr>

              @foreach ($dataReportPsStar as $result)
              <tr>
                <td>{{ $result->orderStatus }}</td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/INNER">{{ $result->WO_INNER }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BBR">{{ $result->WO_BBR }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/KDG">{{ $result->WO_KDG }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BLC">{{ $result->WO_BLC }}</a></td>
                <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/ALL">{{ $result->jumlah }}</a></td>
              </tr>
              <?php
                $total_inner += $result->WO_INNER;
                $total_bbr += $result->WO_BBR;
                $total_kdg += $result->WO_KDG;
                // $total_tjl += $result->WO_TJL;
                $total_blc += $result->WO_BLC;
              ?>
              @endforeach

              <?php
                  if (empty($dataReportPsScbe)){
                      $scbe_inner   = 0;
                      $scbe_bbr     = 0;
                      $scbe_kdg     = 0;
                      $scbe_blc     = 0;
                      $scbe_jumlah  = 0;
                  }
                  else {
                      $scbe_inner   = $dataReportPsScbe[0]->WO_INNER;
                      $scbe_bbr     = $dataReportPsScbe[0]->WO_BBR;
                      $scbe_kdg     = $dataReportPsScbe[0]->WO_KDG;
                      $scbe_blc     = $dataReportPsScbe[0]->WO_BLC;
                      $scbe_jumlah  = $dataReportPsScbe[0]->jumlah;
                  }
                ?>
              <?php
              $total_inner = $total_inner + $completedPsInner;
              $total_bbr = $total_bbr + $completedPsBbr;
              $total_kdg = $total_kdg + $completedPsKdg;
              $total_blc = $total_blc + $completedPsBlc;
              $total = $total_inner + $total_bbr + $total_kdg + $total_blc;
              ?>
              <tr>
                <td class="warna7 text1">TOTAL</td>
                <td class="warna7 text1"><a class="link" href="/dashboard/scbeListTotal/{{ $tgl }}/INNER">{{ $total_inner }}</a></td>
                <td class="warna7 text1"><a class="link" href="/dashboard/scbeListTotal/{{ $tgl }}/BBR">{{ $total_bbr }}</a></td>
                <td class="warna7 text1"><a class="link" href="/dashboard/scbeListTotal/{{ $tgl }}/KDG">{{ $total_kdg }}</a></td>
                <td class="warna7 text1"><a class="link" href="/dashboard/scbeListTotal/{{ $tgl }}/BLC">{{ $total_blc }}</a></td>
                <td class="warna7 text1"><a class="link" href="/dashboard/scbeListTotal/{{ $tgl }}/ALL">{{ $total }}</a></td>

      <!--           <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/INNER">{{ $total_inner }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BBR">{{ $total_bbr }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/KDG">{{ $total_kdg }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/TJL">{{ $total_tjl }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/BLC">{{ $total_blc }}</a></td>
                <td><a href="/dashboard/scbeList/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td> -->
              </tr>
            </table>

            <!-- grafik -->
            <canvas id="myChart" width="300px" height="200px"></canvas>
      </div>
      </div>


     <div class="row">
      <div class="col-sm-12 table-responsive">
        <h3>Dashboard Transaksi [Periode {{$tgl}}]</h3>
        <table class="table table-striped table-bordered dataTable">
            <tr></tr>
              <th class="warna7 text1">Status</th>
              <th class="warna7 text1">2P-3P</th>
              <th class="warna7 text1">CHANGESTB</th>
              <th class="warna7 text1">2ndSTB</th>
              <th class="warna7 text1">3ndSTB</th>
              <th class="warna7 text1">PLC</th>
              <th class="warna7 text1">WifiExtender</th>
              <th class="warna7 text1">PDA</th>
              <th class="warna7 text1">CHANGE STB + 2ND STB</th>
              <th class="warna7 text1">CHANGE STB + WIFI EXTENDER</th>
              <th class="warna7 text1">ADD STB + PLC</th>
              <th class="warna7 text1">ADD STB + WIFI EXTENDER</th>
              <th class="warna7 text1">2ND STB + PLC</th>
              <th class="warna7 text1">2ND STB + WIFI EXTENDER</th>
              <th class="warna7 text1">3RD STB + PLC</th>
              <th class="warna7 text1">3RD STB + WIFI EXTENDER</th>
              <th class="warna7 text1">INDIBOX</th>
              <th class="warna7 text1">ADDSERVICEINET</th>
              <th class="warna7 text1">SMARTINDIHOME</th>
              <th class="warna7 text1">JUMLAH</th>
            </tr>
            <?php
              $no_update_jumlah = 0;
              $no_update_addonstb = 0;
              $no_update_chengestb = 0;
              $no_update_2ndstb = 0;
              $no_update_3ndstb = 0;
              $no_WifiExtender = 0;
              $no_update_plc = 0;
              $no_update_pda = 0;
              $no_update_CHANGESTB2NDSTB = 0;
              $no_update_CHANGESTBWIFIEXTENDER = 0;
              $no_update_ADDSTBPLC = 0;
              $no_update_ADDSTBWIFIEXTENDER = 0;
              $no_update_2NDSTBPLC = 0;
              $no_update_2NDSTBWIFIEXTENDER = 0;
              $no_update_3RDSTBPLC = 0;
              $no_update_3RDSTBWIFIEXTENDER = 0;
              $no_update_INDIBOX = 0;
              $no_update_ADDSERVICEINET = 0;
              $no_update_SMARTINDIHOME = 0;

              $total_wo_ADDONSTB = 0;
              $total_wo_CHANGESTB = 0;
              $total_wo_2ndSTB = 0;
              $total_wo_3ndSTB = 0;
              $total_WifiExtender = 0;
              $total_plc = 0;
              $total_pda = 0;
              $total_CHANGESTB2NDSTB = 0;
              $total_CHANGESTBWIFIEXTENDER = 0;
              $total_ADDSTBPLC = 0;
              $total_ADDSTBWIFIEXTENDER = 0;
              $total_2NDSTBPLC = 0;
              $total_2NDSTBWIFIEXTENDER = 0;
              $total_3RDSTBPLC = 0;
              $total_3RDSTBWIFIEXTENDER = 0;
              $total_INDIBOX = 0;
              $total_ADDSERVICEINET = 0;
              $total_SMARTINDIHOME = 0;
              $total = 0;
            ?>
            @foreach ($dataReportTransaksi as $result)
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                    <tr>
                      <td>{{ $result->laporan_status }}</td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/addonstb">{{ $result->wo_ADDONSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/changestb">{{ $result->wo_CHANGESTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/2ndstb">{{ $result->wo_2ndSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/3ndstb">{{ $result->wo_3ndSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/plc">{{ $result->wo_plc }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/WifiExtender">{{ $result->wo_WifiExtender }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/PDA">{{ $result->wo_pda }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/CHANGESTB2NDSTB">{{ $result->wo_CHANGESTB2NDSTB }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/CHANGESTBWIFIEXTENDER">{{ $result->wo_CHANGESTBWIFIEXTENDER }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ADDSTBPLC">{{ $result->wo_ADDSTBPLC }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ADDSTBWIFIEXTENDER">{{ $result->wo_ADDSTBWIFIEXTENDER }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/2NDSTBPLC ">{{ $result->wo_2NDSTBPLC }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/2NDSTBWIFIEXTENDER">{{ $result->wo_2NDSTBWIFIEXTENDER }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/3RDSTBPLC">{{ $result->wo_3RDSTBPLC }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/3RDSTBWIFIEXTENDER">{{ $result->wo_3RDSTBWIFIEXTENDER }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/INDIBOX">{{ $result->wo_INDIBOX }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ADDSERVICEINET">{{ $result->wo_ADDSERVICEINET }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/SMARTINDIHOME">{{ $result->wo_SMARTINDIHOME }}</a></td>
                      <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                    </tr>
                    <?php
                      $total_wo_ADDONSTB  += $result->wo_ADDONSTB;
                      $total_wo_CHANGESTB += $result->wo_CHANGESTB;
                      $total_wo_2ndSTB    += $result->wo_2ndSTB;
                      $total_wo_3ndSTB    += $result->wo_3ndSTB;
                      $total_WifiExtender += $result->wo_WifiExtender;
                      $total_plc          += $result->wo_plc;
                      $total_pda          += $result->wo_pda;
                      $total_CHANGESTB2NDSTB        += $result->wo_CHANGESTB2NDSTB;
                      $total_CHANGESTBWIFIEXTENDER  += $result->wo_CHANGESTBWIFIEXTENDER;
                      $total_ADDSTBPLC              += $result->wo_ADDSTBPLC;
                      $total_ADDSTBWIFIEXTENDER     += $result->wo_ADDSTBWIFIEXTENDER;
                      $total_2NDSTBPLC              += $result->wo_2NDSTBPLC;
                      $total_2NDSTBWIFIEXTENDER     += $result->wo_2NDSTBWIFIEXTENDER;
                      $total_3RDSTBPLC              += $result->wo_3RDSTBPLC;
                      $total_3RDSTBWIFIEXTENDER     += $result->wo_3RDSTBWIFIEXTENDER;
                      $total_INDIBOX     += $result->wo_INDIBOX;
                      $total_ADDSERVICEINET     += $result->wo_ADDSERVICEINET;
                      $total_SMARTINDIHOME     += $result->wo_SMARTINDIHOME;
                    ?>
                @else
                    <?php
                      $no_update_jumlah    += $result->jumlah;
                      $no_update_addonstb  += $result->wo_ADDONSTB;
                      $no_update_chengestb += $result->wo_CHANGESTB;
                      $no_update_2ndstb    += $result->wo_2ndSTB;
                      $no_update_3ndstb    += $result->wo_3ndSTB;
                      $no_WifiExtender     += $result->wo_WifiExtender;
                      $no_update_plc       += $result->wo_plc;
                      $no_update_pda       += $result->wo_pda;
                      $no_update_CHANGESTB2NDSTB        += $result->wo_CHANGESTB2NDSTB;
                      $no_update_CHANGESTBWIFIEXTENDER  += $result->wo_CHANGESTBWIFIEXTENDER;
                      $no_update_ADDSTBPLC              += $result->wo_ADDSTBPLC;
                      $no_update_ADDSTBWIFIEXTENDER     += $result->wo_ADDSTBWIFIEXTENDER;
                      $no_update_2NDSTBPLC              += $result->wo_2NDSTBPLC;
                      $no_update_2NDSTBWIFIEXTENDER     += $result->wo_2NDSTBWIFIEXTENDER;
                      $no_update_3RDSTBPLC              += $result->wo_3RDSTBPLC;
                      $no_update_3RDSTBWIFIEXTENDER     += $result->wo_3RDSTBWIFIEXTENDER;
                      $no_update_INDIBOX     += $result->wo_INDIBOX;
                      $no_update_ADDSERVICEINET     += $result->wo_ADDSERVICEINET;
                      $no_update_SMARTINDIHOME     += $result->wo_SMARTINDIHOME;

                    ?>
                @endif
            @endforeach
            <tr>
              <td>ANTRIAN</td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/addonstb">{{ $no_update_addonstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/changestb">{{ $no_update_chengestb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/2ndstb">{{ $no_update_2ndstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/3ndstb">{{ $no_update_3ndstb }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/plc">{{ $no_update_plc }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/WifiExtender">{{ $no_WifiExtender }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/PDA">{{ $no_update_pda }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/CHANGESTB2NDSTB">{{ $no_update_CHANGESTB2NDSTB }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/CHANGESTBWIFIEXTENDER">{{ $no_update_CHANGESTBWIFIEXTENDER }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/ADDSTBPLC">{{ $no_update_ADDSTBPLC }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/ADDSTBWIFIEXTENDER">{{ $no_update_ADDSTBWIFIEXTENDER }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/2NDSTBPLC ">{{ $no_update_2NDSTBPLC }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/2NDSTBWIFIEXTENDER">{{ $no_update_2NDSTBWIFIEXTENDER }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/3RDSTBPLC">{{ $no_update_3RDSTBPLC }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/3RDSTBWIFIEXTENDER">{{ $no_update_3RDSTBWIFIEXTENDER }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/INDIBOX">{{ $no_update_INDIBOX }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/ADDSERVICEINET">{{ $no_update_ADDSERVICEINET }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/SMARTINDIHOME">{{ $no_update_SMARTINDIHOME }}</a></td>
              <td><a href="/dashboard/transaksi/{{ $tgl }}/ALL/ANTRIAN/ALL">{{ $no_update_jumlah }}</a></td>
            </tr>

            <?php
              $total_wo_ADDONSTB    = $total_wo_ADDONSTB + $no_update_addonstb;
              $total_wo_CHANGESTB   = $total_wo_CHANGESTB +  $no_update_chengestb;
              $total_wo_2ndSTB      = $total_wo_2ndSTB  +  $no_update_2ndstb;
              $total_wo_3ndSTB      = $total_wo_3ndSTB  +  $no_update_3ndstb;
              $total_WifiExtender   = $total_WifiExtender + $no_WifiExtender;
              $total_plc            = $total_plc + $no_update_plc;
              $total_pda            = $total_pda + $no_update_pda;
              $total_CHANGESTB2NDSTB          = $total_CHANGESTB2NDSTB + $no_update_CHANGESTB2NDSTB;
              $total_CHANGESTBWIFIEXTENDER    = $total_CHANGESTBWIFIEXTENDER + $no_update_CHANGESTBWIFIEXTENDER;
              $total_ADDSTBPLC                = $total_ADDSTBPLC + $no_update_ADDSTBPLC;
              $total_ADDSTBWIFIEXTENDER       = $total_ADDSTBWIFIEXTENDER + $no_update_ADDSTBWIFIEXTENDER;
              $total_2NDSTBPLC                = $total_2NDSTBPLC + $no_update_2NDSTBPLC;
              $total_2NDSTBWIFIEXTENDER       = $total_2NDSTBWIFIEXTENDER + $no_update_2NDSTBWIFIEXTENDER;
              $total_3RDSTBPLC                = $total_3RDSTBPLC + $no_update_3RDSTBPLC;
              $total_3RDSTBWIFIEXTENDER       = $total_3RDSTBWIFIEXTENDER + $no_update_3RDSTBWIFIEXTENDER;
              $total_INDIBOX       = $total_INDIBOX + $no_update_INDIBOX;
              $total_ADDSERVICEINET       = $total_ADDSERVICEINET + $no_update_ADDSERVICEINET;
              $total_SMARTINDIHOME       = $total_SMARTINDIHOME + $no_update_SMARTINDIHOME;

              $total = $total_wo_ADDONSTB + $total_wo_CHANGESTB + $total_wo_2ndSTB + $total_wo_3ndSTB + $total_WifiExtender + $total_plc + $total_pda + $total_CHANGESTB2NDSTB + $total_CHANGESTBWIFIEXTENDER + $total_ADDSTBPLC + $total_ADDSTBWIFIEXTENDER + $total_2NDSTBPLC + $total_2NDSTBWIFIEXTENDER + $total_3RDSTBPLC + $total_3RDSTBWIFIEXTENDER + $total_INDIBOX + $total_ADDSERVICEINET + $total_SMARTINDIHOME;
            ?>

            <tr>
              <td class="warna7 text1">TOTAL</td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/addonstb">{{ $total_wo_ADDONSTB }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/changestb">{{ $total_wo_CHANGESTB }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/2ndstb">{{ $total_wo_2ndSTB }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/3ndstb">{{ $total_wo_3ndSTB }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/plc">{{ $total_plc }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/WifiExtender">{{ $total_WifiExtender }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/PDA">{{ $total_pda }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/CHANGESTB2NDSTB">{{ $total_CHANGESTB2NDSTB }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/CHANGESTBWIFIEXTENDER">{{ $total_CHANGESTBWIFIEXTENDER }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/ADDSTBPLC">{{ $total_ADDSTBPLC }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/ADDSTBWIFIEXTENDER">{{ $total_ADDSTBWIFIEXTENDER }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/2NDSTBPLC ">{{ $total_2NDSTBPLC }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/2NDSTBWIFIEXTENDER">{{ $total_2NDSTBWIFIEXTENDER }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/3RDSTBPLC">{{ $total_3RDSTBPLC }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/3RDSTBWIFIEXTENDER">{{ $total_3RDSTBWIFIEXTENDER }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/INDIBOX">{{ $total_INDIBOX }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/ADDSERVICEINET">{{ $total_ADDSERVICEINET }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/SMARTINDIHOME">{{ $total_SMARTINDIHOME }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td>
            </tr>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6">
      <h3>Report Potensi MO [Periode {{$tgl}}]</h3>
        <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th class="warna7 text1">Status (MO)</th>
            <th class="warna7 text1">INNER</th>
            <th class="warna7 text1">BBR</th>
            <th class="warna7 text1">KDG</th>
            <th class="warna7 text1">TJL</th>
            <th class="warna7 text1">BLC</th>
            <th class="warna7 text1">JUMLAH</th>
          </tr>
          <?php
            $no_update_jumlah = 0;
            $no_update_inner = 0;
            $no_update_bbr = 0;
            $no_update_kdg = 0;
            $no_update_tjl = 0;
            $no_update_blc = 0;
            $total_inner = 0;
            $total_bbr = 0;
            $total_kdg = 0;
            $total_tjl = 0;
            $total_blc = 0;
            $total = 0;
          ?>

          @foreach ($dataReporPsStarMo as $result)
          <tr>
            <td>{{ $result->orderStatus }}</td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/INNER/Mo">{{ $result->WO_INNER }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BBR/Mo">{{ $result->WO_BBR }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/KDG/Mo">{{ $result->WO_KDG }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/TJL/Mo">{{ $result->WO_TJL }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/BLC/Mo">{{ $result->WO_BLC }}</a></td>
            <td><a href="/dashboard/potensiPsList/{{ $tgl }}/ALL/{{ $result->orderStatus }}/ALL/Mo">{{ $result->jumlah }}</a></td>
          </tr>
          <?php
            $total_inner += $result->WO_INNER;
            $total_bbr += $result->WO_BBR;
            $total_kdg += $result->WO_KDG;
            $total_tjl += $result->WO_TJL;
            $total_blc += $result->WO_BLC;
          ?>
          @endforeach

          <?php
              if (empty($dataReportPsScbe)){
                  $scbe_inner   = 0;
                  $scbe_bbr     = 0;
                  $scbe_kdg     = 0;
                  $scbe_tjl     = 0;
                  $scbe_blc     = 0;
                  $scbe_jumlah  = 0;
              }
              else {
                  $scbe_inner   = $dataReportPsScbe[0]->WO_INNER;
                  $scbe_bbr     = $dataReportPsScbe[0]->WO_BBR;
                  $scbe_kdg     = $dataReportPsScbe[0]->WO_KDG;
                  $scbe_tjl     = $dataReportPsScbe[0]->WO_TJL;
                  $scbe_blc     = $dataReportPsScbe[0]->WO_BLC;
                  $scbe_jumlah  = $dataReportPsScbe[0]->jumlah;
              }
            ?>


          <?php
            $total_inner = $total_inner + 0;
            $total_bbr = $total_bbr + 0;
            $total_kdg = $total_kdg + 0;
            $total_tjl = $total_tjl + 0;
            $total_blc = $total_blc + 0;
            $total = $total_inner + $total_bbr + $total_kdg + $total_tjl + $total_blc;
          ?>
          <tr>
            <td class="warna7 text1">TOTAL</td>
            <td class="warna7 text1">{{ $total_inner }}</td>
            <td class="warna7 text1">{{ $total_bbr }}</td>
            <td class="warna7 text1">{{ $total_kdg }}</td>
            <td class="warna7 text1">{{ $total_tjl }}</td>
            <td class="warna7 text1">{{ $total_blc }}</td>
            <td class="warna7 text1">{{ $total }}</td>
          </tr>
        </table>
      </div>

      <div class="col-sm-6">
        <h3>Dashboard Transaksi SMARTINDIHOME [Periode {{$tgl}}]</h3>
        <table class="table table-striped table-bordered dataTable">
            <tr></tr>
              <th class="warna7 text1">Status</th>
              <th class="warna7 text1">SMARTINDIHOME</th>
              <th class="warna7 text1">JUMLAH</th>
            </tr>
            <?php
              $no_update_jumlah;
              $no_update_smartindihome = 0;
              $total_smartindihome = 0;
              $total = 0;
            ?>
            @foreach ($dataSmartIndome as $result)
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                    <tr>
                      <td>{{ $result->laporan_status }}</td>
                      <td><a href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/{{ $result->laporan_status }}/SMARTINDIHOME">{{ $result->wo_smartindihome }}</a></td>
                      <td><a href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
                    </tr>
                    <?php
                      $total_smartindihome += $result->wo_smartindihome;
                    ?>
                @else
                    <?php
                      $no_update_jumlah        += $result->jumlah;
                      $no_update_smartindihome += $result->wo_smartindihome;
                    ?>
                @endif
            @endforeach
            <tr>
              <td>ANTRIAN</td>
              <td><a href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/ANTRIAN/SMARTINDIHOME">{{ $no_update_smartindihome }}</a></td>
              <td><a href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/ANTRIAN/ALL">{{ $no_update_jumlah }}</a></td>
            </tr>

            <?php
              $total_smartindihome = $total_smartindihome + $no_update_smartindihome;
              $total = $total_smartindihome;
            ?>

            <tr>
              <td class="warna7 text1">TOTAL</td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/ALL/SMARTINDIHOME">{{ $total_smartindihome }}</a></td>
              <td class="warna7"><a class="link" href="/dashboard/transaksi-smartindihome/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td>
            </tr>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <script src="/bower_components/select2/select2.min.js"></script>
  <script src="/bower_components/chartJs/Chart.min.js"></script>
  <script>
      $(document).ready(function() {
          $('.status').select2();
      });

      var ctx = document.getElementById('myChart');
      var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: {!! json_encode($label) !!},
              datasets: [{
                  label: 'Dashboard SCBE (%)',
                  data: {!! json_encode($nilai) !!},
                  backgroundColor: [
                        "rgba(73, 204, 25, 1)",
                        "rgba(210,105,30,1)",
                        "rgba(251,127,80,1)",
                        "rgba(100,149,237,1)",
                        "rgba(225,248,220,1)",
                        "rgba(220,20,60,1)",
                        "rgba(62,254,255,1)",
                        "rgba(0,0,139,1)",
                        "rgba(29,139,139,1)",
                        "rgba(184,134,11,1)",
                        "rgba(169,169,169,1)",
                        "rgba(19,100,0,1)",
                        "rgba(189,183,107,1)",
                        "rgba(139,0,140,1)",
                        "rgba(85,107,47,1)",
                        "rgba(251,140,1,1)",
                        "rgba(153,50,204,1)",
                        "rgba(139,5,0,1)",
                        "rgba(233,150,122,1)",
                        "rgba(143,188,144,1)",
                        "rgba(72,61,139,1)",
                        "rgba(47,79,79,1)",
                        "rgba(48,206,209,1)",
                        "rgba(148,0,211,1)",
                        "rgba(249,19,147,1)",
                        "rgba(43,191,254,1)",
                        "rgba(105,105,105,1)",
                        "rgba(30,144,255,1)",
                        "rgba(178,34,33,1)",
                        "rgba(102,205,170,1)",
                        "rgba(0,0,205,1)",
                        "rgba(186,85,211,1)",
                        "rgba(147,112,219,1)",
                        "rgba(60,179,113,1)",
                        "rgba(123,103,238,1)",
                        "rgba(62,250,153,1)",
                        "rgba(72,209,204,1)",
                        "rgba(199,21,133,1)",
                        "rgba(25,25,112,1)",
                        "rgba(245,255,250,1)",
                        "rgba(254,228,225,1)",
                        "rgba(254,228,181,1)",
                        "rgba(254,222,173,1)",
                        "rgba(0,0,128,1)",
                        "rgba(253,245,230,1)",
                        "rgba(128,128,1,1)",
                        "rgba(107,142,35,1)",
                        "rgba(252,165,3,1)",
                        "rgba(250,69,1,1)",
                        "rgba(218,112,214,1)",
                  ],
                  borderWidth: 1
              }]
          },
          options: {
            responsive : true,
            legend :{
                position : 'left',
            },
            title:{
              display:false,
              text : 'Dashboard SCBE (%)',
            },
            tooltips: {
              callbacks: {
                label : function(tooltipItem, data){
                      var dt = data['labels'];
                      return dt[tooltipItem['index']];

                },
                afterLabel: function(tooltipItem, data) {
                  var dataset = data['datasets'][0];
                  var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                  return '(' + percent + '%)';
                }
              }
            }
          }
      });
  </script>
@endsection
