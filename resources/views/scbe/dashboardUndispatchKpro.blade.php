@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">LIST {{ $title }} {{ $sektor }}</h3>
        <div class="table-responsive">
              <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>SEKTOR PROV</th>
                    <th>ORDER_ID</th>
                    <th>REGIONAL</th>
                    <th>WITEL</th>
                    <th>DATEL</th>
                    <th>STO</th>
                    <th>UNIT</th>
                    <th>JENISPSB</th>
                    <th>TYPE_TRANS</th>
                    <th>TYPE_LAYANAN</th>
                    <th>TYPE_CHANNEL</th>
                    <th>GROUP_CHANNEL</th>
                    <th>FLAG_DEPOSIT</th>
                    <th>STATUS_RESUME</th>
                    <th>STATUS_MESSAGE</th>
                    <th>PROVIDER</th>
                    <th>order_date</th>
                    <th>last_updated_date</th>
                    <th>DEVICE_ID</th>
                    <th>HIDE</th>
                    <th>PACKAGE_NAME</th>
                    <th>LOC_ID</th>
                    <th>NCLI</th>
                    <th>POTS</th>
                    <th>SPEEDY</th>
                    <th>CUSTOMER_NAME</th>
                    <th>CONTACT_HP</th>
                    <th>INS_ADDRESS</th>
                    <th>GPS_LONGITUDE</th>
                    <th>GPS_LATITUDE</th>
                    <th>KCONTACT</th>
                    <th>UMUR</th>
                    <th>TINDAK_LANJUT</th>
                    <th>ISI_COMMENT</th>
                    <th>tgl_comment</th>
                    <th>USER_ID_TL</th>
                    <th>WONUM</th>
                    <th>DESC_TASK</th>
                    <th>STATUS_TASK</th>
                    <th>SCHEDULE_LABOR</th>
                    <th>ACT_START</th>
                    <th>AMCREW</th>
                    <th>STATUS_REDAMAN</th>
                    <th>STATUS_VOICE</th>
                    <th>STATUS_INET</th>
                    <th>STATUS_ONU</th>
                    <th>OLT_RX</th>
                    <th>ONU_RX</th>
                    <th>SNR_UP</th>
                    <th>SNR_DOWN</th>
                    <th>UPLOAD</th>
                    <th>DOWNLOAD</th>
                    <th>LAST_PROGRAM</th>
                    <th>CLID</th>
                    <th>last_start</th>
                    <th>last_view</th>
                    <th>ukur_time</th>
                    <th>TEKNISI</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($query as $num => $result)
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->sektor_prov }}</td>
                    <td>{{ $result->order_id }}</td>
                    <td>{{ $result->regional }}</td>
                    <td>{{ $result->witel }}</td>
                    <td>{{ $result->datel }}</td>
                    <td>{{ $result->sto }}</td>
                    <td>{{ $result->unit }}</td>
                    <td>{{ $result->jenispsb }}</td>
                    <td>{{ $result->type_trans }}</td>
                    <td>{{ $result->type_layanan }}</td>
                    <td>{{ $result->type_channel }}</td>
                    <td>{{ $result->group_channel }}</td>
                    <td>{{ $result->flag_deposit }}</td>
                    <td>{{ $result->status_resume }}</td>
                    <td>{{ $result->status_message }}</td>
                    <td>{{ $result->provider }}</td>
                    <td>{{ $result->order_date }}</td>
                    <td>{{ $result->last_updated_date }}</td>
                    <td>{{ $result->device_id }}</td>
                    <td>{{ $result->hide }}</td>
                    <td>{{ $result->package_name }}</td>
                    <td>{{ $result->loc_id }}</td>
                    <td>{{ $result->ncli }}</td>
                    <td>{{ $result->pots }}</td>
                    <td>{{ $result->speedy }}</td>
                    <td>{{ $result->customer_name }}</td>
                    <td>{{ $result->contact_hp }}</td>
                    <td>{{ $result->ins_address }}</td>
                    <td>{{ $result->gps_longitude }}</td>
                    <td>{{ $result->gps_latitude }}</td>
                    <td>{{ $result->kcontact }}</td>
                    <td>{{ $result->umur }}</td>
                    <td>{{ $result->tindak_lanjut }}</td>
                    <td>{{ $result->isi_comment }}</td>
                    <td>{{ $result->tgl_comment }}</td>
                    <td>{{ $result->user_id_tl }}</td>
                    <td>{{ $result->wonum }}</td>
                    <td>{{ $result->desc_task }}</td>
                    <td>{{ $result->status_task }}</td>
                    <td>{{ $result->schedule_labor }}</td>
                    <td>{{ $result->act_start }}</td>
                    <td>{{ $result->amcrew }}</td>
                    <td>{{ $result->status_redaman }}</td>
                    <td>{{ $result->status_voice }}</td>
                    <td>{{ $result->status_inet }}</td>
                    <td>{{ $result->status_onu }}</td>
                    <td>{{ $result->olt_rx }}</td>
                    <td>{{ $result->onu_rx }}</td>
                    <td>{{ $result->snr_up }}</td>
                    <td>{{ $result->snr_down }}</td>
                    <td>{{ $result->upload }}</td>
                    <td>{{ $result->download }}</td>
                    <td>{{ $result->last_program }}</td>
                    <td>{{ $result->clid }}</td>
                    <td>{{ $result->last_start }}</td>
                    <td>{{ $result->last_view }}</td>
                    <td>{{ $result->ukur_time }}</td>
                    <td>{{ $result->teknisi }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL UNDISPATCH SC TOMMAN'
                }
            ]
        });
    });
</script>
@endsection