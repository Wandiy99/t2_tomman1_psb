@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #3498db;
      color: white;
      font-weight: bold
    }

    .text1{
      color: white;
      font-weight: bold;
    }

    .text2{
      color: white;
      font-size: 10px;
    }

    .link:link{
      color: white;
    }

    .link:visited{
      color: black;
    }
    .dataTable th,td{
      padding:2px !important;
    }
</style>
  <div id="screnshoot">
    <!-- <h3>Periode {{ $tgl }}</h3> -->
    <center><h3>DASHBOARD PROVISIONING {{ session('witel') }}</h3></center>
    <div class=" row">
      <div class="col-sm-12">
        <small>By Kendala</small>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th rowspan="2" class="warna7 text1">AREA</th>

            <?php
                $countHs    = 0;
                $countDaman = 0;
                $countAmo   = 0;
                $countAso   = 0;
                $totalPt1   = 0;
                $totalSum   = 0;

                foreach($dataKendala as $kendala){
                    if ($kendala->stts_dash=='hs'){
                        $countHs += 1;
                    }
                    else if ($kendala->stts_dash=='daman'){
                        $countDaman += 1;
                    }
                    else if ($kendala->stts_dash=="amo"){
                        $countAmo += 1;
                    }
                    else if ($kendala->stts_dash=="aso"){
                        $countAso += 1;
                    }
                }

                $countAso += 1;
                $witel = ['WO_INNER', 'WO_BBR', 'WO_KDG', 'WO_TJL', 'WO_BLC'];
             ?>

            @if($countHs<>0)
            <th colspan="<?= $countHs; ?>" class="warna1 text1">HS</th>
            @endif

            @if($countDaman<>0)
              <th colspan="<?= $countDaman; ?>" class="warna2 text1">DAMAN</th>
            @endif
            
            @if($countAmo<>0)
            <th colspan="<?= $countAmo; ?>" class="warna3 text1">AMO</th>
            @endif

            <th colspan="<?= $countAso; ?>" class="warna4 text1">ASO</th>

            <!-- <th rowspan="2" class="warna5 text1"><b>LANJUT PT 1</b></th> -->
            <th rowspan="2" class="warna7 text1"><b>TOTAL</b></th>
          </tr>
          <tr>
            @foreach($dataKendala as $result)
                @php
                    $warna = '';
                    if($result->stts_dash=="hs"){
                        $warna = "warna1 text1";
                    }
                    elseif($result->stts_dash=="daman"){
                        $warna = "warna2 text1";
                    }
                    elseif($result->stts_dash=="amo"){
                        $warna = "warna3 text1";
                    }
                    elseif($result->stts_dash=="aso"){
                        $warna = "warna4 text1";
                    };

                @endphp

                @if ($result->laporan_status_id<>52)
                  <td class="{{ $warna }}" align=center><b>{{ $result->laporan_status }}</b></td>
                @endif
            @endforeach
            <td class="warna4 text1" ><b>LANJUT PT 1</b></td>
          </tr>
          <?php
          $jumlah_datel = array();
          foreach ($get_datel as $datel){
            $jumlah_datel[$datel->datel] = 0;
          }
          $total = 0;
          ?>
          @foreach($get_datel as $list)
          <tr>
            <td>{{ $list->datel }}</td>
            <?php
              $datel = $list->datel;
              $pt1 = 0;

            ?>
            @foreach($dataKendala as $result)
            <?php
              $jumlah_datel[$list->datel] += $result->$datel;
              $total += $result->$datel;
            ?>
            @if ($result->laporan_status_id<>52)
                <td align=center><a href="/dashboard/scbeList/{{ date('Y-m',strtotime($tgl)) }}/ALL/{{ $result->laporan_status }}/{{ $list->datel }}/KENDALA"><b>{{ $result->$datel }}</b></a></td>
            @else
                <?php
                    $pt1 = $result->$list;
                    $totalPt1 += $pt1;
                 ?>
            @endif
            @endforeach
            <td align=center><a href="/dashboard/scbeList/{{ date('Y',strtotime($tgl)) }}/ALL/LANJUT PT1/{{ $list->datel }}/KENDALA">{{ $pt1 }}</a></td>
            <td align=center><a href="/dashboard/scbeList/{{ date('Y-m',strtotime($tgl)) }}/ALL/ALL/{{ $list->datel }}/KENDALA">{{ $jumlah_datel[$list->datel] }}</a></td>

          </tr>
          @endforeach
          <tr>
              <td class="warna7 text1">JUMLAH</td>
              @foreach($dataKendala as $result)
                  @if ($result->laporan_status_id<>52)
                      <td align=center class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ date('Y-m',strtotime($tgl)) }}/ALL/{{ $result->laporan_status }}/ALL/KENDALA"><b>{{ $result->jumlah }}</b></a></td>
                  @endif
              @endforeach
              <?php
                $total += $totalPt1;
              ?>
              <td align=center class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ date('Y-m',strtotime($tgl)) }}/ALL/LANJUT PT1/ALL/KENDALA"><b>{{ $totalPt1 }}</b></a></td>
              <td align=center class="warna7 text1"><a class="link" href="/dashboard/scbeList/{{ date('Y-m',strtotime($tgl)) }}/ALL/ALL/ALL/KENDALA"><b>{{ $total }}</b></a></td>
            </tr>

            <tr>
              <td class="warna7 text1">Download excel</td>
              @foreach($dataKendala as $result)
                  @if ($result->laporan_status_id<>52)
                      <td align=center class="warna7 text2"><a class="link" href="/dashboard/scbeListwithexcel/{{ date('Y-m',strtotime($tgl)) }}/ALL/{{ $result->laporan_status }}/ALL/KENDALA">download <p>&#8595;</p></a></td>
                  @endif
              @endforeach
              <td align=center class="warna7 text2"><a class="link" href="/dashboard/scbeListwithexcel/{{ date('Y-m',strtotime($tgl)) }}/ALL/LANJUT PT1/ALL/KENDALA">download <p>&#8595;</p></a></td>
              <td align=center class="warna7 text2"><a class="link" href="/dashboard/scbeListwithexcel/{{ date('Y-m',strtotime($tgl)) }}/ALL/ALL/ALL/KENDALA">download <p>&#8595;</p></a></td>
            </tr>
        </table>
      </div>
      <div class="col-sm-7">
      <small>By Tomman Status {{$tgl}}</small>
       <table class="table table-striped table-bordered dataTable">
          <tr></tr>
            <th class="warna7">STATUS</th>
            @foreach ($get_datel as $datel)
            <th class="warna7">{{ $datel->datel_sortname }}</th>
            @endforeach
            <th class="warna7">JML</th>
          </tr>
            <?php
              $total_datel = array();
              foreach ($get_datel as $datel){
                $total_datel[$datel->datel] = 0;
              }
              $jumlah_total = 0;
            ?>
            @foreach ($data as $result)
                @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
                <tr>
                    <td text-align=left>{{ $result->laporan_status }}</td>
                    <?php
                      $total_status = 0;
                    ?>
                    @foreach ($get_datel as $datel)
                    <?php
                      $datel_result = $datel->datel;
                      $total_datel[$datel->datel] += $result->$datel_result;
                      $total_status += $result->$datel_result;
                    ?>
                    <td align=center><a href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/{{ $datel->datel }}/PROV">{{ $result->$datel_result }}</a></td>
                    @endforeach
                    <th class="warna7" align=center><a class="link" href="/dashboard/scbeList/{{ $tgl }}/ALL/{{ $result->laporan_status }}/ALL/PROV">{{ $total_status }}</a></th>

                </tr>
                <?php
                  $jumlah_total += $total_status;
                ?>
                @endif
            @endforeach



          <tr>
            <td class="warna7" align="center">TOTAL</td>
            @foreach ($get_datel as $datel)
            <td class="warna7" align="center">{{ $total_datel[$datel->datel] }}</td>
            @endforeach
            <td class="warna7" align="center">{{ $jumlah_total }}</td>
            </tr>
        </table>
      </div>
      <div class="col-sm-5">
      <small>Potensi PS Starclick</small>
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna7">STATUS (SC)</th>
            @foreach ($get_datel as $datel)
            <th class="warna7">{{ $datel->datel_sortname }}</th>
            @endforeach
            <th class="warna7">JML</th>

          </tr>
          <?php
          $total_datel = array();
          foreach ($get_datel as $datel){
            $total_datel[$datel->datel] = 0;
          }
          $jumlah_total = 0;
          ?>
          @foreach ($get_potensi_ps_up as $potensi_ps_up)
          <tr>
            <td>{{ $potensi_ps_up->orderStatus }}</td>
            <?php
              $sc_status = 0;
            ?>
            @foreach ($get_datel as $datel)
            <?php
              $datel_result = $datel->datel;
              $total_datel[$datel->datel] += $potensi_ps_up->$datel_result;
              $sc_status += $potensi_ps_up->$datel_result;
            ?>
            <td align="center">{{ $potensi_ps_up->$datel_result }}</td>
            @endforeach
            <?php
              $jumlah_total += $sc_status;
            ?>
            <td align="center" class="warna7">{{ $sc_status }}</td>
          </tr>
          @endforeach
          @foreach ($get_potensi_ps as $potensi_ps)
          <tr>
            <td>{{ $potensi_ps->orderStatus }}</td>
            <?php
              $sc_status = 0;
            ?>
            @foreach ($get_datel as $datel)
            <?php
              $datel_result = $datel->datel;
              $total_datel[$datel->datel] += $potensi_ps->$datel_result;
              $sc_status += $potensi_ps->$datel_result;
            ?>
            <td align="center">{{ $potensi_ps->$datel_result }}</td>
            @endforeach
            <?php
              $jumlah_total += $sc_status;
            ?>
            <td align="center" class="warna7">{{ $sc_status }}</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna7" align="center">TOTAL</th>
            @foreach($get_datel as $datel)
            <th class="warna7" align="center">{{ $total_datel[$datel->datel] }}</th>
            @endforeach
            <th class="warna7" align="center">{{ $jumlah_total }}</th>
          </tr>
        </table>
        <canvas id="myChart" width="200px" height="200px"></canvas>

      </div>
      <div class="col-sm-7">

      </div>
      <div class="col-sm-4">
            <!-- grafik -->
      </div>
    </div>

  </div>
@endsection

@section('plugins')
  <script src="/bower_components/select2/select2.min.js"></script>
  <script src="/bower_components/chartJs/Chart.min.js"></script>
  <script>
      $(document).ready(function() {
          $('.status').select2();
      });

      var ctx = document.getElementById('myChart');
      var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: {!! json_encode($label) !!},
              datasets: [{
                  label: 'Dashboard SCBE (%)',
                  data: {!! json_encode($nilai) !!},
                  backgroundColor: [
                        "rgba(73, 204, 25, 1)",
                        "rgba(210,105,30,1)",
                        "rgba(251,127,80,1)",
                        "rgba(100,149,237,1)",
                        "rgba(225,248,220,1)",
                        "rgba(220,20,60,1)",
                        "rgba(62,254,255,1)",
                        "rgba(0,0,139,1)",
                        "rgba(29,139,139,1)",
                        "rgba(184,134,11,1)",
                        "rgba(169,169,169,1)",
                        "rgba(19,100,0,1)",
                        "rgba(189,183,107,1)",
                        "rgba(139,0,140,1)",
                        "rgba(85,107,47,1)",
                        "rgba(251,140,1,1)",
                        "rgba(153,50,204,1)",
                        "rgba(139,5,0,1)",
                        "rgba(233,150,122,1)",
                        "rgba(143,188,144,1)",
                        "rgba(72,61,139,1)",
                        "rgba(47,79,79,1)",
                        "rgba(48,206,209,1)",
                        "rgba(148,0,211,1)",
                        "rgba(249,19,147,1)",
                        "rgba(43,191,254,1)",
                        "rgba(105,105,105,1)",
                        "rgba(30,144,255,1)",
                        "rgba(178,34,33,1)",
                        "rgba(102,205,170,1)",
                        "rgba(0,0,205,1)",
                        "rgba(186,85,211,1)",
                        "rgba(147,112,219,1)",
                        "rgba(60,179,113,1)",
                        "rgba(123,103,238,1)",
                        "rgba(62,250,153,1)",
                        "rgba(72,209,204,1)",
                        "rgba(199,21,133,1)",
                        "rgba(25,25,112,1)",
                        "rgba(245,255,250,1)",
                        "rgba(254,228,225,1)",
                        "rgba(254,228,181,1)",
                        "rgba(254,222,173,1)",
                        "rgba(0,0,128,1)",
                        "rgba(253,245,230,1)",
                        "rgba(128,128,1,1)",
                        "rgba(107,142,35,1)",
                        "rgba(252,165,3,1)",
                        "rgba(250,69,1,1)",
                        "rgba(218,112,214,1)",
                  ],
                  borderWidth: 1
              }]
          },
          options: {
            responsive : true,
            legend :{
                position : 'left',
            },
            title:{
              display:false,
              text : 'Dashboard SCBE (%)',
            },
            tooltips: {
              callbacks: {
                label : function(tooltipItem, data){
                      var dt = data['labels'];
                      return dt[tooltipItem['index']];

                },
                afterLabel: function(tooltipItem, data) {
                  var dataset = data['datasets'][0];
                  var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                  return '(' + percent + '%)';
                }
              }
            }
          }
      });
  </script>
@endsection
