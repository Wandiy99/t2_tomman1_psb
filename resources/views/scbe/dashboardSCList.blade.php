@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/combine/npm/sweetalert2@10.13.0/dist/sweetalert2.min.css,npm/sweetalert2@10.13.0/dist/sweetalert2.min.css">
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">LIST {{ $title }} {{ $sektor }}</h3>
        <div class="table-responsive">
              <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>ACTION</th>
                    <th>STATUS PI</th>
                    <th>ORDER_ID</th>
                    <th>ORDER NAME</th>
                    <th>KOORDINAT PELANGGAN</th>
                    <th>CATATAN TEKNISI</th>
                    <th>VALINS ID</th>
                    <th>ODP</th>
                    <th>ORDER DATE</th>
                    <th>STATUS</th>
                    <th>SEKTOR</th>
                    <th>MITRA</th>
                    <th>STO</th>
                    <th>ODP</th>
                    <th>ALAMAT</th>
                    <th>KORDINAT SC</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($query as $num => $result)
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>
                      <a class="confirmSync label label-success" data-id="{{ $result->orderId }}">SYNC</a>
                      <br/>
                      <a class="confirmDispatch label label-info" data-id="{{ $result->orderId }}">Dispatch</a>
                    </td>
                    <td>
                      @if($result->type_grab <> '')
                        {{ $result->type_grab ? : '-' }}
                      @elseif($result->tgl_orderDate == date('Y-m-d'))
                        @if($result->time_orderDate > '06:30:00')
                          PI_BARU
                        @endif
                      @else
                        -
                      @endif
                    </td>
                    <td>{{ $result->orderId }}</td>
                    <td>{{ $result->orderName }}</td>
                    <td>{{ $result->kordinat_pelanggan }}</td>
                    <td>{{ $result->catatan }}</td>
                    <td>{{ $result->valins_id }}</td>
                    <td>{{ $result->alproname }}</td>
                    <td>{{ $result->dps_orderDate }}</td>
                    <td>{{ $result->orderStatus }}</td>
                    <td>{{ $result->sektor_prov }}</td>
                    <td>{{ $result->mitra_amija_pt }}</td>
                    <td>{{ $result->sto }}</td>
                    <td>{{ $result->alproname }}</td>
                    <td>{{ $result->orderAddr ? : $result->alamatLengkap }}</td>
                    <td>{{ $result->dps_lat }},{{ $result->dps_lon }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.13.0/dist/sweetalert2.all.min.js"></script>
<script>
  (function (){
    $('.confirmSync').click( function() {
          var data_id = $(this).attr('data-id');
          Swal.fire({
            title: 'Rekan yakin ?',
            text: "Jangan lupa Assign Crew Nossa nya",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "<a href='/syncSC/" + data_id + "' style='color: white;'>Yes, syncron it!</a>"
          });
    }),
    $('.confirmDispatch').click( function() {
          var data_id = $(this).attr('data-id');
          Swal.fire({
            title: 'Rekan yakin ?',
            text: "Jangan Kada Ingat lah Assign Crew Nossa nya Biar Isuk Kada Invalid",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "<a href='/dispatch/" + data_id + "' style='color: white;'>Yes, dispatch it!</a>"
          });
    })
  })()
</script>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL UNDISPATCH SC TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL UNDISPATCH SC TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL UNDISPATCH SC TOMMAN'
                }
            ]
        });
    });
</script>
@endsection