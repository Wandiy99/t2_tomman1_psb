@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
  .label {
    font-size: 12px;
  }
  th {
    border-color: #34495e;
    background-color: #7f8c8d;
    color : #ecf0f1;
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  tr, th, td {
      text-align: center;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 1px 1px;
  }
  .warna1 {
    background-color: #2874A6;
  }
  .warna2 {
    background-color: #2E86C1;
  }
  .warna3 {
    background-color: #D6EAF8;
  }
  .warna4 {
    background-color: #E67E22;
    #F0B27A
  }
  .warna4x {
    background-color: #F0B27A;
    #FAE5D3
  }
  .warna4z {
    background-color: #FAE5D3;
  }
  .colorArea {
    background-color: #F4D03F;
  }
  .colorArea2 {
    background-color: #FCF3CF;
  }
  .colorArea3 {
    background-color: #F7DC6F;
  }
  .colorComparin {
    background-color: #48C9B0;
  }
  .colorComparin2 {
    background-color: #D1F2EB;
  }
  .colorComparin3 {
    background-color: #76D7C4;
  }
  .text2 {
    color: black !important;
  }
  .text1 {
    color: white !important;
  }
  .text1:link {
    color: white !important;
  }
  .text1:visited {
    color: white !important;
  }
  th a:link {
    color: white;
  }
  th a:visited {
    color: white;
  }
  .overload {
    background-color: red;
    color: white;
  }
  .merahx {
    background-color: #C0392B;
    color: white !important;
  }
  .hijaux {
    background-color:#1ABC9C;
    color: white !important;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 4px 0px !important;
  }
  .color-green {
    background-color: #1ABC9C;
  }
  .color-black {
    background-color: #2C3E50;
  }
  .color-red {
    background-color: #E74C3C;
  }
  .color-yellow {
    background-color: #F39C12;
  }
  .color-blues {
    background-color: #154360;
  }
  .txt-white {
      color: white !important;
  }
</style>

<h2 style="font-weight: bold; text-align: center;" class="text-center">PROVISIONING WITEL {{ session('auth')->nama_witel }}</h2>

<div class="row">

  @if(session('auth')->nama_witel == 'KALSEL')
  <div class="col-sm-12">
    <div class="white-box">
        <div class="row row-in">
            <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="&#xe01a;" class="linea-icon linea-basic"></i>
                        <h5 class="text-muted vb">PI PAGI</h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        @php
                        $total_PI_PAGI = 0;
                        @endphp
                        @foreach ($potensiPS_hero as $area => $result)
                        @php
                        $total_PI_PAGI += @$result['PI_PAGI'];
                        @endphp
                        @endforeach
                        <h3 class="counter text-right m-t-15 text-danger">{{ $total_PI_PAGI  }}</h3>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="&#xe01a;" class="linea-icon linea-basic"></i>
                        <h5 class="text-muted vb">PI BARU</h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        @php
                        $total_PI_BARU = 0;
                        @endphp
                        @foreach ($potensiPS_hero as $area => $result)
                        @php
                        $total_PI_BARU += @$result['PI_BARU'];
                        @endphp
                        @endforeach
                        <h3 class="counter text-right m-t-15 text-danger">{{ $total_PI_BARU  }}</h3>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="d" class="linea-icon linea-basic"></i>
                        <h5 class="text-muted vb">PROGRESS</h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        @php
                        $jumlah_progress = 0;
                        @endphp
                        @foreach ($potensiPS_hero as $num => $result)
                        @php
                        $jumlah_progress += @$result['potensi_progress'];
                        @endphp
                        @endforeach
                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_progress  }}</h3>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="col-in row">
                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="b" class="linea-icon linea-basic"></i>
                        <h5 class="text-muted vb">PS AO</h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_ps }}</h3>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
  </div>

  <div class="col-sm-12 table-responsive">
    <h2 style="font-weight: bold; text-align: center;" class="text-center">PROGRESS ORDER AO</h2>

    <table class="table table-bordered dataTable">
      <tr>
        <th class="align-middle colorArea text1" rowspan="2">AREA</th>
        <th class="align-middle colorComparin text1" colspan="4">COMPARIN</th>
        <th class="align-middle warna1 text1" colspan="4">STARCLICK NCX</th>
        <th class="align-middle colorComparin text1" colspan="4">PRABAC</th>
      </tr>
      <tr>
        <th class="align-middle colorComparin3 text1">PI PAGI</th>
        <th class="align-middle colorComparin3 text1">FO PAGI</th>
        <th class="align-middle colorComparin3 text1">PI BARU</th>
        <th class="align-middle colorComparin3 text1">CANCEL<br />ORDER</th>

        <th class="align-middle warna1 text1">A.COMP</th>
        <th class="align-middle warna1 text1">PS AO</th>
        <th class="align-middle warna1 text1">EST</th>
        <th class="align-middle warna1 text1">CANCEL<br />ORDER</th>

        <th class="align-middle colorComparin3 text1">PS H-1</th>
        <th class="align-middle colorComparin3 text1">PS HI</th>
      </tr>
      @php
        $total_PI_PAGI = $total_FO_PAGI = $total_PI_BARU = $total_COMP_CANCEL = $total_prabac_ps_h1 = $total_prabac_ps_hi = $total_acomp = $total_ps = $total_start_cancel = 0;
      @endphp
      @foreach ($potensiPS_prov as $area => $result)
      @php
        $total_PI_PAGI += @$result['PI_PAGI'];
        $total_FO_PAGI += @$result['FO_PAGI'];
        $total_PI_BARU += @$result['PI_BARU'];
        $total_COMP_CANCEL += @$result['COMP_CANCEL'];
        $total_acomp += @$result['actcomp'];
        $total_ps += @$result['ps'];
        $total_start_cancel += @$result['start_cancel'];
        $total_prabac_ps_h1 += @$result['prabac_ps_h1'];
        $total_prabac_ps_hi += @$result['prabac_ps_hi'];
      @endphp
      <tr>
        <td class="colorArea2">{{ $area }}</td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_PAGI/PROV/{{ $area }}" target="_blank">{{ @$result['PI_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/FO_PAGI/PROV/{{ $area }}" target="_blank">{{ @$result['FO_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_BARU/PROV/{{ $area }}" target="_blank">{{ @$result['PI_BARU'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/CANCEL/PROV/{{ $area }}" target="_blank">{{ @$result['COMP_CANCEL'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/actcomp/PROV/{{ $area }}" target="_blank">{{ @$result['actcomp'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/wo_ps/PROV/{{ $area }}/{{ date('Y-m-d') }}" target="_blank">{{ @$result['ps'] ? : '0' }}</a></td>
        <td class="warna3">{{ @$result['actcomp'] + @$result['ps'] }}</td>
        <td class="warna3"><a class="text2" href="/cancelOrder_all/PROV/{{ $area }}" target="_blank">{{ @$result['start_cancel'] ? : '0' }}</a></td>
        <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/PROV/{{ $area }}/prabac_ps_h1" target="_blank">{{ @$result['prabac_ps_h1'] ? : '0' }}</td>
        <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/PROV/{{ $area }}/prabac_ps_hi" target="_blank">{{ @$result['prabac_ps_hi'] ? : '0' }}</td>
      </tr>
      @endforeach
      <tr>
          <td class="colorArea txt-white">TOTAL</td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_PAGI/PROV/ALL" target="_blank">{{ $total_PI_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/FO_PAGI/PROV/ALL" target="_blank">{{ $total_FO_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_BARU/PROV/ALL" target="_blank">{{ $total_PI_BARU }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/CANCEL/PROV/ALL" target="_blank">{{ $total_COMP_CANCEL }}</a></td>
          <td class="warna1"><a class="txt-white" href="/actcomp/PROV/ALL" target="_blank">{{ $total_acomp }}</a></td>
          <td class="warna1"><a class="txt-white" href="/wo_ps/PROV/ALL/{{ date('Y-m-d') }}" target="_blank">{{ $total_ps }}</a></td>
          <td class="warna1 txt-white">{{ $total_ps + $total_acomp }}</td>
          <td class="warna1"><a class="txt-white" href="/cancelOrder_all/PROV/ALL" target="_blank">{{ $total_start_cancel }}</a></td>
          <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/PROV/ALL/prabac_ps_h1" target="_blank">{{ $total_prabac_ps_h1 }}</a></td>
          <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/PROV/ALL/prabac_ps_hi" target="_blank">{{ $total_prabac_ps_hi }}</a></td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12 table-responsive">
    <table class="table table-bordered dataTable">
      <tr>
        <th class="align-middle colorArea text1" rowspan="2">HERO</th>
        <th class="align-middle colorComparin text1" colspan="4">COMPARIN</th>
        <th class="align-middle warna1 text1" colspan="4">STARCLICK NCX</th>
        <th class="align-middle colorComparin text1" colspan="4">PRABAC</th>
      </tr>
      <tr>
        <th class="align-middle colorComparin3 text1">PI PAGI</th>
        <th class="align-middle colorComparin3 text1">FO PAGI</th>
        <th class="align-middle colorComparin3 text1">PI BARU</th>
        <th class="align-middle colorComparin3 text1">CANCEL<br />ORDER</th>

        <th class="align-middle warna1 text1">A.COMP</th>
        <th class="align-middle warna1 text1">PS AO</th>
        <th class="align-middle warna1 text1">EST</th>
        <th class="align-middle warna1 text1">CANCEL<br />ORDER</th>

        <th class="align-middle colorComparin3 text1">PS H-1</th>
        <th class="align-middle colorComparin3 text1">PS HI</th>
      </tr>
      @php
        $total_PI_PAGI = $total_FO_PAGI = $total_PI_BARU = $total_COMP_CANCEL = $total_prabac_ps_h1 = $total_prabac_ps_hi = $total_acomp = $total_ps = $total_start_cancel = 0;
      @endphp
      @foreach ($potensiPS_hero as $area => $result)
      @php
        $total_PI_PAGI += @$result['PI_PAGI'];
        $total_FO_PAGI += @$result['FO_PAGI'];
        $total_PI_BARU += @$result['PI_BARU'];
        $total_COMP_CANCEL += @$result['COMP_CANCEL'];
        $total_acomp += @$result['actcomp'];
        $total_ps += @$result['ps'];
        $total_start_cancel += @$result['start_cancel'];
        $total_prabac_ps_h1 += @$result['prabac_ps_h1'];
        $total_prabac_ps_hi += @$result['prabac_ps_hi'];
      @endphp
      <tr>
        <td class="colorArea2">{{ $area }}</td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_PAGI/HERO/{{ $area }}" target="_blank">{{ @$result['PI_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/FO_PAGI/HERO/{{ $area }}" target="_blank">{{ @$result['FO_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_BARU/HERO/{{ $area }}" target="_blank">{{ @$result['PI_BARU'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/CANCEL/HERO/{{ $area }}" target="_blank">{{ @$result['COMP_CANCEL'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/actcomp/HERO/{{ $area }}" target="_blank">{{ @$result['actcomp'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/wo_ps/HERO/{{ $area }}/{{ date('Y-m-d') }}" target="_blank">{{ @$result['ps'] ? : '0' }}</a></td>
        <td class="warna3">{{ @$result['actcomp'] + @$result['ps'] }}</td>
        <td class="warna3"><a class="text2" href="/cancelOrder_all/HERO/{{ $area }}" target="_blank">{{ @$result['start_cancel'] ? : '0' }}</a></td>
        <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/HERO/{{ $area }}/prabac_ps_h1" target="_blank">{{ @$result['prabac_ps_h1'] ? : '0' }}</td>
        <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/HERO/{{ $area }}/prabac_ps_hi" target="_blank">{{ @$result['prabac_ps_hi'] ? : '0' }}</td>
      </tr>
      @endforeach
      <tr>
          <td class="colorArea txt-white">TOTAL</td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_PAGI/HERO/ALL" target="_blank">{{ $total_PI_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/FO_PAGI/HERO/ALL" target="_blank">{{ $total_FO_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_BARU/HERO/ALL" target="_blank">{{ $total_PI_BARU }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/CANCEL/HERO/ALL" target="_blank">{{ $total_COMP_CANCEL }}</a></td>
          <td class="warna1"><a class="txt-white" href="/actcomp/HERO/ALL" target="_blank">{{ $total_acomp }}</a></td>
          <td class="warna1"><a class="txt-white" href="/wo_ps/HERO/ALL/{{ date('Y-m-d') }}" target="_blank">{{ $total_ps }}</a></td>
          <td class="warna1 txt-white">{{ $total_ps + $total_acomp }}</td>
          <td class="warna1"><a class="txt-white" href="/cancelOrder_all/HERO/ALL" target="_blank">{{ $total_start_cancel }}</a></td>
          <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/HERO/ALL/prabac_ps_h1" target="_blank">{{ $total_prabac_ps_h1 }}</a></td>
          <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/HERO/ALL/prabac_ps_hi" target="_blank">{{ $total_prabac_ps_hi }}</a></td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12 table-responsive">
    <h2 style="font-weight: bold; text-align: center;" class="text-center">PROGRESS ORDER EGBIS</h2>

    <table class="table table-bordered dataTable">
      <tr>
        <th class="align-middle colorArea text1" rowspan="2">AREA</th>
        <th class="align-middle colorComparin text1" colspan="4">COMPARIN</th>
        <th class="align-middle warna1 text1" colspan="4">STARCLICK NCX</th>
      </tr>
      <tr>
        <th class="align-middle colorComparin3 text1">PI PAGI</th>
        <th class="align-middle colorComparin3 text1">FO PAGI</th>
        <th class="align-middle colorComparin3 text1">PI BARU</th>
        <th class="align-middle colorComparin3 text1">CANCEL<br />ORDER</th>

        <th class="align-middle warna1 text1">A.COMP</th>
        <th class="align-middle warna1 text1">PS AO</th>
        <th class="align-middle warna1 text1">EST</th>
        <th class="align-middle warna1 text1">CANCEL<br />ORDER</th>
      </tr>
      @php
        $total_PI_PAGI = $total_FO_PAGI = $total_PI_BARU = $total_COMP_CANCEL = $total_acomp = $total_ps = $total_start_cancel = 0;
      @endphp
      @foreach ($potensiPS_egbis as $area => $result)
      @php
        $total_PI_PAGI += @$result['PI_PAGI'];
        $total_FO_PAGI += @$result['FO_PAGI'];
        $total_PI_BARU += @$result['PI_BARU'];
        $total_COMP_CANCEL += @$result['COMP_CANCEL'];
        $total_acomp += @$result['actcomp'];
        $total_ps += @$result['ps'];
        $total_start_cancel += @$result['start_cancel'];
      @endphp
      <tr>
        <td class="colorArea2">{{ $area }}</td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_PAGI/PROV/{{ $area }}" target="_blank">{{ @$result['PI_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/FO_PAGI/PROV/{{ $area }}" target="_blank">{{ @$result['FO_PAGI'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/PI_BARU/PROV/{{ $area }}" target="_blank">{{ @$result['PI_BARU'] ? : '0' }}</a></td>
        <td class="colorComparin2"><a class="text2" href="/comparinData/CANCEL/PROV/{{ $area }}" target="_blank">{{ @$result['COMP_CANCEL'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/actcomp/EGBIS/{{ $area }}" target="_blank">{{ @$result['actcomp'] ? : '0' }}</a></td>
        <td class="warna3"><a class="text2" href="/wo_ps/EGBIS/{{ $area }}/{{ date('Y-m-d') }}" target="_blank">{{ @$result['ps'] ? : '0' }}</a></td>
        <td class="warna3">{{ @$result['actcomp'] + @$result['ps'] }}</td>
        <td class="warna3"><a class="text2" href="/cancelOrder_all/EGBIS/{{ $area }}" target="_blank">{{ @$result['start_cancel'] ? : '0' }}</a></td>
      </tr>
      @endforeach
      <tr>
          <td class="colorArea txt-white">TOTAL</td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_PAGI/PROV/ALL" target="_blank">{{ $total_PI_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/FO_PAGI/PROV/ALL" target="_blank">{{ $total_FO_PAGI }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/PI_BARU/PROV/ALL" target="_blank">{{ $total_PI_BARU }}</a></td>
          <td class="colorComparin3"><a class="txt-white" href="/comparinData/CANCEL/PROV/ALL" target="_blank">{{ $total_COMP_CANCEL }}</a></td>
          <td class="warna1"><a class="txt-white" href="/actcomp/EGBIS/ALL" target="_blank">{{ $total_acomp }}</a></td>
          <td class="warna1"><a class="txt-white" href="/wo_ps/EGBIS/ALL/{{ date('Y-m-d') }}" target="_blank">{{ $total_ps }}</a></td>
          <td class="warna1 txt-white">{{ $total_ps + $total_acomp }}</td>
          <td class="warna1"><a class="txt-white" href="/cancelOrder_all/EGBIS/ALL" target="_blank">{{ $total_start_cancel }}</a></td>
      </tr>
    </table>
  </div>
  @endif


  <div class="col-sm-12 table-responsive">
    <h3 style="font-weight: bold; text-align: center;">PS / RE<br />SEGMEN DCS<br />PERIODE {{ date('Y-m') }}</h3>
    <p class="text-muted" style="float: left;">Sumber Data : Selfi K-PRO</p>
    <p class="text-muted" style="float: right;">Last Updated {{ $log_psre->LAST_SYNC }} WITA</p>

        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">WITEL</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre_all as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ $result->area ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/CANCEL/ALL" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/OGP/ALL" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/PRA_PI/ALL" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/PS/ALL" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/REVOKE/ALL" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/WITEL/{{ $result->area ? : 'NON AREA' }}/UNSC/ALL" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/CANCEL/ALL" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/OGP/ALL" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/PRA_PI/ALL" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/PS/ALL" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/REVOKE/ALL" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/WITEL/ALL/UNSC/ALL" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>

        @if(session('auth')->nama_witel == 'BALIKPAPAN')
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">AREA</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre_sektor as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ $result->area ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/CANCEL/REGULER" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/OGP/REGULER" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/PRA_PI/REGULER" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/PS/REGULER" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/REVOKE/REGULER" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/UNSC/REGULER" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna2 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/CANCEL/REGULER" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/OGP/REGULER" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/PRA_PI/REGULER" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/PS/REGULER" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/REVOKE/REGULER" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/UNSC/REGULER" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>

        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">AREA</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ $result->area ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/CANCEL/REGULER" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/OGP/REGULER" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/PRA_PI/REGULER" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/PS/REGULER" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/REVOKE/REGULER" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/STO/{{ $result->area ? : 'NON AREA' }}/UNSC/REGULER" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/STO/ALL/CANCEL/REGULER" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/STO/ALL/OGP/REGULER" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/STO/ALL/PRA_PI/REGULER" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/STO/ALL/PS/REGULER" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/STO/ALL/REVOKE/REGULER" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/STO/ALL/UNSC/REGULER" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>
        @endif

        @if(session('auth')->nama_witel == 'KALSEL')
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">AREA</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ str_replace('SEKTOR ', '' , $result->area) ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/CANCEL/REGULER" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/OGP/REGULER" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/PRA_PI/REGULER" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/PS/REGULER" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/REVOKE/REGULER" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTOR/{{ $result->area ? : 'NON AREA' }}/UNSC/REGULER" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna2 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/CANCEL/REGULER" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/OGP/REGULER" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/PRA_PI/REGULER" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/PS/REGULER" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/REVOKE/REGULER" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/SEKTOR/ALL/UNSC/REGULER" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>

        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">SEKTOR</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre_sektor as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ str_replace('TERRITORY BASE', 'SEKTOR ' , $result->area) ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/CANCEL/REGULER" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/OGP/REGULER" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/PRA_PI/REGULER" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/PS/REGULER" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/REVOKE/REGULER" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/SEKTORX/{{ $result->area ? : 'NON AREA' }}/UNSC/REGULER" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/CANCEL/REGULER" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/OGP/REGULER" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/PRA_PI/REGULER" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/PS/REGULER" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/REVOKE/REGULER" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/SEKTORX/ALL/UNSC/REGULER" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>

        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">MITRA</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre_mitra as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
            <td>{{ $result->area ? : 'NON AREA' }}</td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/CANCEL/REGULER" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/OGP/REGULER" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/PRA_PI/REGULER" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/PS/REGULER" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/REVOKE/REGULER" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/MITRA/{{ $result->area ? : 'NON AREA' }}/UNSC/REGULER" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th style="background-color: #C0392B"><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/CANCEL/REGULER" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna1"><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/OGP/REGULER" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/PRA_PI/REGULER" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna1" style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/PS/REGULER" target="_blank">{{ $total_ps }}</a></th>
            <th class="txt-white" style="background-color: #E67E22"><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/REVOKE/REGULER" target="_blank">{{ $total_revoke }}</a></th>
            <th class="txt-white" style="background-color: #8E44AD"><a class="txt-white" href="/dashboardPSRE/MITRA/ALL/UNSC/REGULER" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna1 txt-white">{{ $grand_total }}</th>

            <th class="txt-white" style="background-color: #C0392B">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="txt-white">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white" style="background-color: #1ABC9C">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #E67E22">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="txt-white" style="background-color: #8E44AD">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna1 txt-white">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>
        @endif
  </div>

  <br />

  @if(session('auth')->nama_witel == 'KALSEL')
  <div class="col-sm-12 table-responsive">
    <h3 class="page-title" style="text-align: center; font-weight: bold;">PROGRESS PI K-PRO</h3>
      <p class="text-muted" style="float: left;">Sumber Data : PI PROVI K-PRO</p>
      <p class="text-muted" style="float: right;">Last Updated {{ $log_pi_kpro->last_updated_at }} WITA</p>
        <table class="table table-bordered dataTable">
          <tr>
            <th class="align-middle color-black" rowspan="2">AREA</th>
            <th class="align-middle color-green" colspan="2">PI PAGI<br />(PI Kemarin HI Jam 8 WIB)</th>
            <th class="align-middle color-black" colspan="1">%</th>
            {{-- <th class="align-middle color-black" rowspan="2">JUMLAH<br />PS</th> --}}
            <th class="align-middle color-green" colspan="2">PI BARU<br />(PI HI jam 8-15 WIB)</th>
            <th class="align-middle color-black" colspan="1">%</th>
            {{-- <th class="align-middle color-black" rowspan="2">JUMLAH<br />PS</th> --}}
            <th class="align-middle color-red" rowspan="2">TOTAL PI</th>
            <th class="align-middle color-red" colspan="1">%</th>
            {{-- <th class="align-middle color-red" rowspan="2">TOTAL PS</th> --}}
          </tr>
          <tr>
            <th class="align-middle color-green">BELUM<br />PROGRESS</th>
            <th class="align-middle color-green">SUDAH<br />PROGRESS</th>
            <th class="align-middle color-black">PROGRESS</th>
            <th class="align-middle color-green">BELUM<br />PROGRESS</th>
            <th class="align-middle color-green">SUDAH<br />PROGRESS</th>
            <th class="align-middle color-black">PROGRESS</th>
            <th class="align-middle color-red">PROGRESS</th>
          </tr>
          @php
            $total_belum_progress_pipagi = $total_sudah_progress_pipagi = $total_ps_pipagi = $total_belum_progress_pibaru = $total_sudah_progress_pibaru = $total_ps_pibaru = $jml_pi = $total_pi = $jml_ps = $total_ps = 0;
          @endphp
          @foreach ($pi_kpro as $result)
          @php
            $total_belum_progress_pipagi += $result->belum_progress_pipagi;
            $total_sudah_progress_pipagi += $result->sudah_progress_pipagi;
            $total_ps_pipagi += $result->ps_pipagi;

            $round_pipagi = @round(($result->ps_pipagi + $result->sudah_progress_pipagi) / ($result->ps_pipagi + $result->sudah_progress_pipagi + $result->belum_progress_pipagi) * 100,2);
            if (is_nan($round_pipagi) == true)
            {
              $round_pipagi = 0;
            } else {
              $round_pipagi = $round_pipagi;
            }

            $total_round_pipagi = @round(($total_ps_pipagi + $total_sudah_progress_pipagi) / ($total_ps_pipagi + $total_sudah_progress_pipagi + $total_belum_progress_pipagi) * 100,2);
            if (is_nan($total_round_pipagi) == true)
            {
              $total_round_pipagi = 0;
            } else {
              $total_round_pipagi = $total_round_pipagi;
            }

            $total_belum_progress_pibaru += $result->belum_progress_pibaru;
            $total_sudah_progress_pibaru += $result->sudah_progress_pibaru;
            $total_ps_pibaru += $result->ps_pibaru;

            $round_pibaru = @round(($result->ps_pibaru + $result->sudah_progress_pibaru) / ($result->ps_pibaru + $result->sudah_progress_pibaru + $result->belum_progress_pibaru) * 100,2);
            if (is_nan($round_pibaru) == true)
            {
              $round_pibaru = 0;
            } else {
              $round_pibaru = $round_pibaru;
            }

            $total_round_pibaru = @round(($total_ps_pibaru + $total_sudah_progress_pibaru) / ($total_ps_pibaru + $total_sudah_progress_pibaru + $total_belum_progress_pibaru) * 100,2);
            if (is_nan($total_round_pibaru) == true)
            {
              $total_round_pibaru = 0;
            } else {
              $total_round_pibaru = $total_round_pibaru;
            }

            $jml_pi = $result->belum_progress_pipagi + $result->sudah_progress_pipagi + $result->belum_progress_pibaru + $result->sudah_progress_pibaru;
            $total_pi += $jml_pi;

            $round_progress = @round(($result->ps_pipagi + $result->sudah_progress_pipagi + $result->ps_pibaru + $result->sudah_progress_pibaru) / ($result->ps_pipagi + $result->sudah_progress_pipagi + $result->belum_progress_pipagi + $result->ps_pibaru + $result->sudah_progress_pibaru + $result->belum_progress_pibaru) * 100,2);
            if (is_nan($round_progress) == true)
            {
              $round_progress = 0;
            } else {
              $round_progress = $round_progress;
            }

            $total_round_progress = @round(($total_ps_pipagi + $total_sudah_progress_pipagi + $total_ps_pibaru + $total_sudah_progress_pibaru) / ($total_ps_pipagi + $total_sudah_progress_pipagi + $total_belum_progress_pipagi + $total_ps_pibaru + $total_sudah_progress_pibaru + $total_belum_progress_pibaru) * 100,2);
            if (is_nan($total_round_progress) == true)
            {
              $total_round_progress = 0;
            } else {
              $total_round_progress = $total_round_progress;
            }

            $jml_ps = $result->ps_pipagi + $result->ps_pibaru;
            $total_ps += $jml_ps;
          @endphp
          <tr>
              <td>{{ $result->area ? : 'NON AREA' }}</td>
              <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_pagi&status=belum_progress" target="_blank">{{    $result->belum_progress_pipagi ? : '0' }}</a>
              </td>
              <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_pagi&status=sudah_progress" target="_blank">{{ $result->sudah_progress_pipagi ? : '0' }}</a>
              </td>
              <td>
                  {{ $round_pipagi }} %
              </td>
              {{-- <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_pagi&status=ps" target="_blank">{{ $result->ps_pipagi ? : '0' }}</a>
              </td>  --}}
              <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_baru&status=belum_progress" target="_blank">{{ $result->belum_progress_pibaru ? : '0' }}</a>
              </td>
              <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_baru&status=sudah_progress" target="_blank">{{ $result->sudah_progress_pibaru ? : '0' }}</a>
              </td>
              <td>
                  {{ $round_pibaru }} %
              </td>
              {{-- <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi_baru&status=ps" target="_blank">{{ $result->ps_pibaru ? : '0' }}</a>
              </td>  --}}
              <td>
                <a class="text2" href="/dashboard/progress_totalpi_detail?sektor={{ $result->area ? : 'NON AREA' }}&type=pi&status=total" target="_blank">
                {{ $jml_pi }}</a>
              </td>
              <td>
                {{ $round_progress }} %
              </td>
              {{-- <td>
                {{ $jml_ps }}
              </td> --}}
          </tr>
          @endforeach
          <tr>
              <td class="color-black txt-white">TOTAL</td>
              <td class="color-green txt-white">
                  <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_pagi&status=belum_progress" target="_blank">{{ $total_belum_progress_pipagi }}</a>
              </td>
              <td class="color-green txt-white">
                  <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_pagi&status=sudah_progress" target="_blank">{{ $total_sudah_progress_pipagi }}</a>
              </td>
              <td class="color-black txt-white">
                {{ $total_round_pipagi }} %
              </td>
              {{-- <td class="color-black txt-white">
                  <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_pagi&status=ps" target="_blank">{{ $total_ps_pipagi }}</a>
              </td> --}}
              <td class="color-green txt-white">
                  <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_baru&status=belum_progress" target="_blank">{{ $total_belum_progress_pibaru }}</a>
              </td>
              <td class="color-green txt-white">
                  <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_baru&status=sudah_progress" target="_blank">{{ $total_sudah_progress_pibaru }}</a>
              </td>
              <td class="color-black txt-white">
                {{ $total_round_pibaru }} %
              </td>
              {{-- <td class="color-black txt-white">
                  <a style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi_baru&status=ps" target="_blank">{{ $total_ps_pibaru }}</a>
              </td> --}}
              <td class="color-red txt-white">
                <a class="text2" style="color: white !important" href="/dashboard/progress_totalpi_detail?sektor=ALL&type=pi&status=total" target="_blank">
                {{ $total_pi }}</a>
              </td>
              <td class="color-red txt-white">{{ $total_round_progress }} %</td>
              {{-- <td class="color-red txt-white">{{ $total_ps }}</td> --}}
          </tr>
        </table>
  </div>
  @endif

  @if(session('auth')->nama_witel == 'KALSEL')
  <div class="col-sm-12 table-responsive">
    <br />
    <h3 style="font-weight: bold; text-align: center;">FFG & TTI</h3>
    <p class="text-muted; text-align: center;">Sumber Data : K-PRO & FFG KPI WeCare (Prabac)</p>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">AREA</th>
            <th class="text1 align-middle" rowspan="2" style="background-color: #1ABC9C">PS K-PRO<br />{{ date('d M Y', strtotime("-2 Month")) }} - {{ date('d M Y') }}</th>
            <th class="warna1 text1 align-middle" rowspan="2">TIKET FFG<br />{{ date('01 M Y') }} - {{ date('d M Y') }}</th>
            <th class="warna1 text1" colspan="2">TTI</th>
            <th class="warna1 text1 align-middle" rowspan="2">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #1ABC9C">< 3 JAM</th>
            <th class="text1 align-middle" style="background-color: #C0392B">> 3 JAM</th>
          </tr>
          @php
              $total_ps2bln = $total_ffg = $total_tti_3jam = $total_tti_max3jam = 0;
          @endphp
          @foreach ($ffg as $num => $result)
          @php
            $total_ps2bln += $result->ps_2bln;
            $total_ffg += $result->jml_ffg;
            $total_tti_3jam += $result->tti_3jam;
            $total_tti_max3jam += $result->tti_max3jam;
          @endphp
          <tr>
            <td>{{ $result->sektor_prov ? : 'NONE SEKTOR' }}</td>
            <td><a class="text2" href="/dashboardFFGTTI/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/PS_KPRO" target="_blank">{{ $result->ps_2bln }}</a></td>
            <td><a class="text2" href="/dashboardFFGTTI/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/FFG" target="_blank">{{ $result->jml_ffg }}</a></td>
            <td>{{ $result->tti_3jam }}</td>
            <td>{{ $result->tti_max3jam }}</td>
            <td>{{ @round($result->jml_ffg / $result->ps_2bln * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th style="background-color: #1ABC9C"><a class="txt-white" href="/dashboardFFGTTI/ALL/PS_KPRO" target="_blank">{{ $total_ps2bln }}</th>
            <th class="warna1"><a class="txt-white" href="/dashboardFFGTTI/ALL/FFG" target="_blank">{{ $total_ffg }}</a></th>
            <th class="txt-white" style="background-color: #1ABC9C">{{ $total_tti_3jam }}</th>
            <th class="txt-white" style="background-color: #C0392B">{{ $total_tti_max3jam }}</th>
            <th class="warna1 txt-white">{{ @round($total_ffg / $total_ps2bln * 100,2) }} %</th>
          </tr>
        </table>
        <br />
  </div>
  <div class="col-sm-12 table-responsive">
    <br />
    <h3 style="font-weight: bold; text-align: center;">TTR (PI PAGI & FO PAGI)</h3>
    <p class="text-muted" style="float: left;">Sumber Data : API Comparin</p>
    <p class="text-muted" style="float: right;">Last Updated {{ $log_comparin->last_grab_at }} WITA</p>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna1 text1 align-middle" rowspan="2">AREA</th>
            <th class="warna1 text1" colspan="7">PI PAGI</th>
            <th class="warna1 text1" colspan="7">FO PAGI</th>
          </tr>
          <tr>
            <th class="text1 align-middle">< 1 Hari</th>
            <th class="text1 align-middle">1 - 2 Hari</th>
            <th class="text1 align-middle">2 - 3 Hari</th>
            <th class="text1 align-middle">3 - 4 Hari</th>
            <th class="text1 align-middle">4 - 6 Hari</th>
            <th class="text1 align-middle">> 1 Minggu</th>
            <th class="text1 align-middle warna1">TOTAL</th>
            <th class="text1 align-middle">< 1 Hari</th>
            <th class="text1 align-middle">1 - 2 Hari</th>
            <th class="text1 align-middle">2 - 3 Hari</th>
            <th class="text1 align-middle">3 - 4 Hari</th>
            <th class="text1 align-middle">4 - 6 Hari</th>
            <th class="text1 align-middle">> 1 Minggu</th>
            <th class="text1 align-middle warna1">TOTAL</th>
          </tr>
          @php
              $total_pipagi_1hari = $total_pipagi_1_2hari = $total_pipagi_2_3hari = $total_pipagi_3_4hari = $total_pipagi_4_6hari = $total_pipagi_7hari = $jml_pipagi = $total_fopagi_1hari = $total_fopagi_1_2hari = $total_fopagi_2_3hari = $total_fopagi_3_4hari = $total_fopagi_4_6hari = $total_fopagi_7hari = $jml_pipagi = 0;
          @endphp
          @foreach ($ttr as $num => $result)
          @php
            @$total_pipagi_1hari += @$result->pipagi_1hari;
            @$total_pipagi_1_2hari += @$result->pipagi_1_2hari;
            @$total_pipagi_2_3hari += @$result->pipagi_2_3hari;
            @$total_pipagi_3_4hari += @$result->pipagi_3_4hari;
            @$total_pipagi_4_6hari += @$result->pipagi_4_6hari;
            @$total_pipagi_7hari += @$result->pipagi_7hari;
            $jml_pipagi = @$result->pipagi_1hari + @$result->pipagi_1_2hari + @$result->pipagi_2_3hari + @$result->pipagi_3_4hari + @$result->pipagi_4_6hari + @$result->pipagi_7hari;

            @$total_fopagi_1hari += @$result->fopagi_1hari;
            @$total_fopagi_1_2hari += @$result->fopagi_1_2hari;
            @$total_fopagi_2_3hari += @$result->fopagi_2_3hari;
            @$total_fopagi_3_4hari += @$result->fopagi_3_4hari;
            @$total_fopagi_4_6hari += @$result->fopagi_4_6hari;
            @$total_fopagi_7hari += @$result->fopagi_7hari;
            @$jml_fopagi = @$result->fopagi_1hari + @$result->fopagi_1_2hari + @$result->fopagi_2_3hari + @$result->fopagi_3_4hari + @$result->fopagi_4_6hari + @$result->fopagi_7hari;
          @endphp
          <tr>
            <td>{{ @$result->sektor_prov ? : 'NONE SEKTOR' }}</td>

            <td><b>{{ @$result->pipagi_1hari }}</b> ({{ @round(@$result->pipagi_1hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ @$result->pipagi_1_2hari }}</b> ({{ @round(@$result->pipagi_1_2hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ @$result->pipagi_2_3hari }}</b> ({{ @round(@$result->pipagi_2_3hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ @$result->pipagi_3_4hari }}</b> ({{ @round(@$result->pipagi_3_4hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ @$result->pipagi_4_6hari }}</b> ({{ @round(@$result->pipagi_4_6hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ @$result->pipagi_7hari }}</b> ({{ @round(@$result->pipagi_7hari / $jml_pipagi * 100,2) }} %)</td>
            <td><b>{{ $jml_pipagi }}</b></td>

            <td><b>{{ @$result->fopagi_1hari }}</b> ({{ @round(@$result->fopagi_1hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$result->fopagi_1_2hari }}</b> ({{ @round(@$result->fopagi_1_2hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$result->fopagi_2_3hari }}</b> ({{ @round(@$result->fopagi_2_3hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$result->fopagi_3_4hari }}</b> ({{ @round(@$result->fopagi_3_4hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$result->fopagi_4_6hari }}</b> ({{ @round(@$result->fopagi_4_6hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$result->fopagi_7hari }}</b> ({{ @round(@$result->fopagi_7hari / $jml_fopagi * 100,2) }} %)</td>
            <td><b>{{ @$jml_fopagi }}</b></td>
          </tr>
          @endforeach
          <tr>
            <th class="warna1 txt-white">TOTAL</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_1hari }}</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_1_2hari }}</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_2_3hari }}</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_3_4hari }}</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_4_6hari }}</th>
            <th class="warna1 txt-white">{{ @$total_pipagi_7hari }}</th>
            <th class="warna1 txt-white">{{ $jml_pipagi }}</th>

            <th class="warna1 txt-white">{{ @$total_fopagi_1hari }}</th>
            <th class="warna1 txt-white">{{ @$total_fopagi_1_2hari }}</th>
            <th class="warna1 txt-white">{{ @$total_fopagi_2_3hari }}</th>
            <th class="warna1 txt-white">{{ @$total_fopagi_3_4hari }}</th>
            <th class="warna1 txt-white">{{ @$total_fopagi_4_6hari }}</th>
            <th class="warna1 txt-white">{{ @$total_fopagi_7hari }}</th>
            <th class="warna1 txt-white">{{ @$jml_fopagi }}</th>
          </tr>
        </table>
        <br />
  </div>
  @endif
</div>
@endsection
