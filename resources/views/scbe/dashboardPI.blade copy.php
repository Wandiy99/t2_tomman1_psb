@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .warna8{
      background-color: #483D8B;
    }

    .warna9{
      background-color: #FFE4B5;
    }

    .text2{
      color: black !important;
    }

    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }

    .text1:visited{
      color: white !important;
    }


    th a:link{
      color: white;
    }

    th a:visited{
      color: white;
    }
</style>
<center>
  <h2>Dashboard Progress SC Kalsel</h2>
</center>
<div class="row">
  <div class="col-sm-12">
    <div class="white-box">
                        <div class="row row-in">
                            <div class="col-lg-3 col-sm-6 row-in-br">
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="V" class="linea-icon linea-basic"></i>
                                        <h5 class="text-muted vb">TEKNISI PT1 AO</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_teknisi  }}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 row-in-br">
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="&#xe01a;" class="linea-icon linea-basic"></i>
                                        <h5 class="text-muted vb">SISA PI & FWFM</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                      <?php
                                      $jumlah_wo = 0;
                                      $jumlah_PI = 0;
                                      $jumlah_FWFM = 0;
                                        foreach ($mappingSCAO as $num => $result) {
                                          $jumlah_PI += $result->PI_UNDISPATCH+$result->PI_NEEDPROGRESS+$result->PI_PROGRESS+$result->PI_KENDALA_FUP+$result->PI_KENDALA_CANCEL+$result->HR;
                                          $jumlah_FWFM += $result->FWFM_UNDISPATCH+$result->FWFM_NEEDPROGRESS+$result->FWFM_PROGRESS+$result->FWFM_KENDALA_FUP+$result->FWFM_KENDALA_CANCEL+$result->FWFM_HR;
                                        }
                                        $jumlah_wo = $jumlah_PI + $jumlah_FWFM;

                                      ?>
                                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_wo  }}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 row-in-br">
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="d" class="linea-icon linea-basic"></i>
                                        <h5 class="text-muted vb">PROGRESS</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                      <?php
                                      $jumlah_progress = 0;
                                        foreach ($mappingSCAO as $num => $result) {
                                          $jumlah_progress += $result->PI_PROGRESS+$result->FWFM_PROGRESS;
                                        }
                                      ?>
                                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_progress  }}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="b" class="linea-icon linea-basic"></i>
                                        <h5 class="text-muted vb">PS AO</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_ps  }}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
<div class="row">
  <div class="col-sm-12">
    <br />
    <h3>PROGRESS PI & FWFM</h3>
        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="align-middle warna7 text1" rowspan="2">AREA</th>
            <th class="align-middle warna7 text1" colspan="2">UNDISPATCH</th>
            <th class="align-middle warna7 text1" colspan="6">PI</th>
            <th class="align-middle warna7 text1" colspan="6">FWFM</th>
            <th class="align-middle warna7 text1" rowspan="2">CANCEL ORDER</th>
          </tr>
          <tr>
            <td class="align-middle warna7 text1">H-1</td>
            <td class="align-middle warna7 text1">HI</td>
            <td class="align-middle warna7 text1">NP</td>
            <td class="align-middle warna7 text1">PROGRESS</td>
            <td class="align-middle warna7 text1">KENDALA FUP</td>
            <td class="align-middle warna7 text1">KENDALA CANCEL</td>
            <td class="align-middle warna7 text1">HR</td>
            <td class="align-middle warna7 text1">JML</td>
            <td class="align-middle warna7 text1">NP</td>
            <td class="align-middle warna7 text1">PROGRESS</td>
            <td class="align-middle warna7 text1">KENDALA FUP</td>
            <td class="align-middle warna7 text1">KENDALA CANCEL</td>
            <td class="align-middle warna7 text1">HR</td>
            <td class="align-middle warna7 text1">JML</td>
          </tr>
          <?php
            $total_undispatch_h1 = 0;
            $total_undispatch_hi = 0;

            $total_PI_NEEDPROGRESS = 0;
            $total_PI_PROGRESS = 0;
            $total_PI_KENDALA_FUP = 0;
            $total_PI_KENDALA_CANCEL = 0;
            $total_PI_HR = 0;
            $total_PI = 0;

            $total_FWFM_NEEDPROGRESS = 0;
            $total_FWFM_PROGRESS = 0;
            $total_FWFM_KENDALA_FUP = 0;
            $total_FWFM_KENDALA_CANCEL = 0;
            $total_FWFM_HR = 0;
            $total_FWFM = 0;

            $total_CANCEL_ORDER = 0;
          ?>
          @foreach ($mappingSCAO as $num => $result)
          <?php
            $total_undispatch_h1 += $result->UNDISPATCH_H1;
            $jml_undispatch_h1 = $total_undispatch_h1;
            $total_undispatch_hi += $result->UNDISPATCH_HI;
            $jml_undispatch_hi = $total_undispatch_hi;

            $jumlah_PI = 0;
            $total_PI_NEEDPROGRESS += $result->PI_NEEDPROGRESS;
            $total_PI_PROGRESS += $result->PI_PROGRESS;
            $total_PI_KENDALA_FUP += $result->PI_KENDALA_FUP;
            $total_PI_KENDALA_CANCEL += $result->PI_KENDALA_CANCEL;
            $total_PI_HR += $result->HR;
            $jumlah_PI = $result->PI_NEEDPROGRESS+$result->PI_PROGRESS+$result->PI_KENDALA_FUP+$result->PI_KENDALA_CANCEL+$result->HR;
            $total_PI += $jumlah_PI;

            $jumlah_FWFM = 0;
            $total_FWFM_NEEDPROGRESS += $result->FWFM_NEEDPROGRESS;
            $total_FWFM_PROGRESS += $result->FWFM_PROGRESS;
            $total_FWFM_KENDALA_FUP += $result->FWFM_KENDALA_FUP;
            $total_FWFM_KENDALA_CANCEL += $result->FWFM_KENDALA_CANCEL;
            $total_FWFM_HR += $result->FWFM_HR;
            $jumlah_FWFM = $result->FWFM_NEEDPROGRESS+$result->FWFM_PROGRESS+$result->FWFM_KENDALA_FUP+$result->FWFM_KENDALA_CANCEL+$result->FWFM_HR;
            $total_FWFM += $jumlah_FWFM;

            $total_CANCEL_ORDER += $result->CANCEL_ORDER;
          ?>
          <tr>
            <td>{{ $result->datel }}</td>
            <td><a href="/undispatch_sc/DATEL/H1/{{ $result->datel }}" >{{ $result->UNDISPATCH_H1 }}</a></td>
            <td><a href="/undispatch_sc/DATEL/HI/{{ $result->datel }}" >{{ $result->UNDISPATCH_HI }}</a></td>
            <td><a href="/pi_needprogress/DATEL/{{ $result->datel }}" >{{ $result->PI_NEEDPROGRESS }}</a></td>
            <td><a href="/pi_progress/DATEL/{{ $result->datel }}" >{{ $result->PI_PROGRESS }}</a></td>
            <td><a href="/pi_kendala/DATEL/{{ $result->datel }}/FUP" >{{ $result->PI_KENDALA_FUP }}</a></td>
            <td><a href="/pi_kendala/DATEL/{{ $result->datel }}/CANCEL" >{{ $result->PI_KENDALA_CANCEL }}</a></td>
            <td><a href="/pi_hr/DATEL/{{ $result->datel }}" >{{ $result->HR }}</a></td>
            <td><a href="/pi_all/DATEL/{{ $result->datel }}" >{{ $jumlah_PI }}</a></td>
            <td><a href="/fwfm_progress/DATEL/{{ $result->datel }}" >{{ $result->FWFM_NEEDPROGRESS }}</a></td>
            <td><a href="/fwfm_progress/DATEL/{{ $result->datel }}" >{{ $result->FWFM_PROGRESS }}</a></td>
            <td><a href="/fwfm_kendala/DATEL/{{ $result->datel }}/FUP" >{{ $result->FWFM_KENDALA_FUP }}</a></td>
            <td><a href="/fwfm_kendala/DATEL/{{ $result->datel }}/CANCEL" >{{ $result->FWFM_KENDALA_CANCEL }}</a></td>
            <td><a href="/fwfm_hr/DATEL/{{ $result->datel }}" >{{ $result->FWFM_HR }}</a></td>
            <td><a href="/fwfm_all/DATEL/{{ $result->datel }}" >{{ $jumlah_FWFM }}</a></td>
            <td><a href="/cancelOrder_all/DATEL/{{ $result->datel }}" >{{ $result->CANCEL_ORDER }}</a></td>
          </tr>
          @endforeach
          <tr>
            <th class="align-middle warna7 text1">TOTAL</th>
            <th class="align-middle warna7 text1"><a href="/undispatch_sc/DATEL/H1/ALL" >{{ $total_undispatch_h1 }}</a></th>
            <th class="align-middle warna7 text1"><a href="/undispatch_sc/DATEL/HI/ALL" >{{ $total_undispatch_hi }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_needprogress/DATEL/ALL" >{{ $total_PI_NEEDPROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_progress/DATEL/ALL" >{{ $total_PI_PROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_kendala/DATEL/ALL/FUP" >{{ $total_PI_KENDALA_FUP }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_kendala/DATEL/ALL/CANCEL" >{{ $total_PI_KENDALA_CANCEL }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_hr/DATEL/ALL" >{{ $total_PI_HR }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_all/DATEL/ALL" >{{ $total_PI }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_needprogress/DATEL/ALL" >{{ $total_FWFM_NEEDPROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_progress/DATEL/ALL" >{{ $total_FWFM_PROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_kendala/DATEL/ALL/FUP" >{{ $total_FWFM_KENDALA_FUP }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_kendala/DATEL/ALL/CANCEL" >{{ $total_FWFM_KENDALA_CANCEL }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_hr/DATEL/ALL" >{{ $total_FWFM_HR }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_all/DATEL/ALL" >{{ $total_FWFM }}</a></th>
            <th class="align-middle warna7 text1"><a href="/cancelOrder_all/DATEL/ALL" >{{ $total_CANCEL_ORDER }}</a></th>
          </tr>
        </table>

        <br />

        <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="align-middle warna7 text1" rowspan="2">MITRA</th>
            <th class="align-middle warna7 text1" colspan="6">PI</th>
            <th class="align-middle warna7 text1" colspan="6">FWFM</th>
            <th class="align-middle warna7 text1" rowspan="2">CANCEL ORDER</th>
          </tr>
          <tr>
            <td class="align-middle warna7 text1">NP</td>
            <td class="align-middle warna7 text1">PROGRESS</td>
            <td class="align-middle warna7 text1">KENDALA FUP</td>
            <td class="align-middle warna7 text1">KENDALA CANCEL</td>
            <td class="align-middle warna7 text1">HR</td>
            <td class="align-middle warna7 text1">JML</td>
            <td class="align-middle warna7 text1">NP</td>
            <td class="align-middle warna7 text1">PROGRESS</td>
            <td class="align-middle warna7 text1">KENDALA FUP</td>
            <td class="align-middle warna7 text1">KENDALA CANCEL</td>
            <td class="align-middle warna7 text1">HR</td>
            <td class="align-middle warna7 text1">JML</td>
          </tr>
          <?php
            $total_PI_NEEDPROGRESS = 0;
            $total_PI_PROGRESS = 0;
            $total_PI_KENDALA_FUP = 0;
            $total_PI_KENDALA_CANCEL = 0;
            $total_PI_HR = 0;
            $total_PI = 0;

            $total_FWFM_NEEDPROGRESS = 0;
            $total_FWFM_PROGRESS = 0;
            $total_FWFM_KENDALA_FUP = 0;
            $total_FWFM_KENDALA_CANCEL = 0;
            $total_FWFM_HR = 0;
            $total_FWFM = 0;

            $total_CANCEL_ORDER = 0;
          ?>
          @foreach ($mappingSCAOMitra as $num => $result)
          <?php
            $jumlah_PI = 0;
            $total_PI_NEEDPROGRESS += $result->PI_NEEDPROGRESS;
            $total_PI_PROGRESS += $result->PI_PROGRESS;
            $total_PI_KENDALA_FUP += $result->PI_KENDALA_FUP;
            $total_PI_KENDALA_CANCEL += $result->PI_KENDALA_CANCEL;
            $total_PI_HR += $result->HR;
            $jumlah_PI = $result->PI_NEEDPROGRESS+$result->PI_PROGRESS+$result->PI_KENDALA_FUP+$result->PI_KENDALA_CANCEL+$result->HR;
            $total_PI += $jumlah_PI;

            $jumlah_FWFM = 0;
            $total_FWFM_NEEDPROGRESS += $result->FWFM_NEEDPROGRESS;
            $total_FWFM_PROGRESS += $result->FWFM_PROGRESS;
            $total_FWFM_KENDALA_FUP += $result->FWFM_KENDALA_FUP;
            $total_FWFM_KENDALA_CANCEL += $result->FWFM_KENDALA_CANCEL;
            $total_FWFM_HR += $result->FWFM_HR;
            $jumlah_FWFM = $result->FWFM_NEEDPROGRESS+$result->FWFM_PROGRESS+$result->FWFM_KENDALA_FUP+$result->FWFM_KENDALA_CANCEL+$result->FWFM_HR;
            $total_FWFM += $jumlah_FWFM;

            $total_CANCEL_ORDER += $result->CANCEL_ORDER;
          ?>
          <tr>
            <td>{{ $result->mitra_amija_pt }}</td>
            <td><a href="/pi_needprogress/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->PI_NEEDPROGRESS }}</a></td>
            <td><a href="/pi_progress/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->PI_PROGRESS }}</a></td>
            <td><a href="/pi_kendala/MITRA/{{ $result->mitra_amija_pt }}/FUP" >{{ $result->PI_KENDALA_FUP }}</a></td>
            <td><a href="/pi_kendala/MITRA/{{ $result->mitra_amija_pt }}/CANCEL" >{{ $result->PI_KENDALA_CANCEL }}</a></td>
            <td><a href="/pi_hr/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->HR }}</a></td>
            <td><a href="/pi_all/MITRA/{{ $result->mitra_amija_pt }}" >{{ $jumlah_PI }}</a></td>
            <td><a href="/fwfm_progress/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->FWFM_NEEDPROGRESS }}</a></td>
            <td><a href="/fwfm_progress/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->FWFM_PROGRESS }}</a></td>
            <td><a href="/fwfm_kendala/MITRA/{{ $result->mitra_amija_pt }}/FUP" >{{ $result->FWFM_KENDALA_FUP }}</a></td>
            <td><a href="/fwfm_kendala/MITRA/{{ $result->mitra_amija_pt }}/CANCEL" >{{ $result->FWFM_KENDALA_CANCEL }}</a></td>
            <td><a href="/fwfm_hr/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->FWFM_HR }}</a></td>
            <td><a href="/fwfm_all/MITRA/{{ $result->mitra_amija_pt }}" >{{ $jumlah_FWFM }}</a></td>
            <td><a href="/cancelOrder_all/MITRA/{{ $result->mitra_amija_pt }}" >{{ $result->CANCEL_ORDER }}</a></td>
          </tr>
          @endforeach
          <tr>
            <th class="align-middle warna7 text1">TOTAL</th>
            <th class="align-middle warna7 text1"><a href="/pi_needprogress/MITRA/ALL" >{{ $total_PI_NEEDPROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_progress/MITRA/ALL" >{{ $total_PI_PROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_kendala/MITRA/ALL/FUP" >{{ $total_PI_KENDALA_FUP }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_kendala/MITRA/ALL/CANCEL" >{{ $total_PI_KENDALA_CANCEL }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_hr/MITRA/ALL" >{{ $total_PI_HR }}</a></th>
            <th class="align-middle warna7 text1"><a href="/pi_all/MITRA/ALL" >{{ $total_PI }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_needprogress/MITRA/ALL" >{{ $total_FWFM_NEEDPROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_progress/MITRA/ALL" >{{ $total_FWFM_PROGRESS }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_kendala/MITRA/ALL/FUP" >{{ $total_FWFM_KENDALA_FUP }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_kendala/MITRA/ALL/CANCEL" >{{ $total_FWFM_KENDALA_CANCEL }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_hr/MITRA/ALL" >{{ $total_FWFM_HR }}</a></th>
            <th class="align-middle warna7 text1"><a href="/fwfm_all/MITRA/ALL" >{{ $total_FWFM }}</a></th>
            <th class="align-middle warna7 text1"><a href="/cancelOrder_all/MITRA/ALL" >{{ $total_CANCEL_ORDER }}</a></th>
          </tr>
        </table>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
       <h3>Report Potensi PS [Periode {{$tgl}}]</h3>
       <table class="table table-striped table-bordered dataTable">
          <tr>
            <th class="warna2 text1">DATEL</th>
            <!-- <th class="warna2 text1">HR</th> -->
            <th class="align-middle warna7 text1">A.COMP</th>
            <th class="align-middle warna7 text1">PS AO</th>
            <th class="align-middle warna7 text1">EST</th>
          </tr>
          <?php
            $total_PS = 0;
            $total_ACTCOMP = 0;
            $total_HR = 0;
          ?>
          @foreach ($potensiPS as $result)
          <?php
            $total_PS += $result->PS;
            $total_ACTCOMP += $result->ACTCOMP;
            $total_HR += $result->HR;
          ?>
          <tr>
          <td style="text-align:left !important">{{ $result->datel }}</td>
          <!-- <td><a href="/wo_hr/{{ $result->datel }}">{{ $result->HR }}</td> -->
          <td><a href="/actcomp/{{ $result->datel }}">{{ $result->ACTCOMP }}</a></td>
          <td><a href="/wo_ps/{{ $result->datel }}/{{ $tgl }}">{{ $result->PS }}</a></td>
          <td>{{ $result->PS+$result->ACTCOMP }}</td>
          </tr>
          @endforeach
          <tr>
              <td class="align-middle warna7 text1">TOTAL</td>
              <!-- <td class="align-middle warna7 text1"><a class="text1" href="/wo_hr/ALL"> {{ $total_HR }}</a></td> -->
              <td class="align-middle warna7 text1"><a class="text1" href="/actcomp/ALL">{{ $total_ACTCOMP }}</a></td>
              <td class="align-middle warna7 text1"><a class="text1" href="/wo_ps/ALL/{{ $tgl }}">{{ $total_PS }}</a></td>
              <td class="align-middle warna7 text1">{{ $total_PS+$total_ACTCOMP }}</td>
          </tr>
          </table>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <h3>Umur PI & FWFM</h3>
    <table class="table table-striped table-bordered dataTable">
      <tr>
        <th rowspan="2" class="align-middle warna7 text1">Area</th>
        <th class="align-middle warna7 text1" colspan="7">PI</th>
        <th class="align-middle warna7 text1" colspan="6">FWFM</th>
      </tr>
      <tr>
        <th class="align-middle warna7 text1">COMPLY</th>
        <th class="align-middle warna7 text1">< 1 hari</th>
        <th class="align-middle warna7 text1">1-2 hari</th>
        <th class="align-middle warna7 text1">2-3 hari</th>
        <th class="align-middle warna7 text1">3-7 hari</th>
        <th class="align-middle warna7 text1">lebih 7 hari</th>
        <th class="align-middle warna7 text1">JML</th>
        <th class="align-middle warna7 text1">< 1 hari</th>
        <th class="align-middle warna7 text1">1-2 hari</th>
        <th class="align-middle warna7 text1">2-3 hari</th>
        <th class="align-middle warna7 text1">3-7 hari</th>
        <th class="align-middle warna7 text1">lebih 7 hari</th>
        <th class="align-middle warna7 text1">JML</th>
      </tr>
      <?php
        $pi_comply = 0;
        $pi_kurang1hari = 0;
        $pi_1sd2hari = 0;
        $pi_2sd3hari = 0;
        $pi_3sd7hari = 0;
        $pi_lebih7hari = 0;
        $total_pi = 0;

        $fwfm_kurang1hari = 0;
        $fwfm_1sd2hari = 0;
        $fwfm_2sd3hari = 0;
        $fwfm_3sd7hari = 0;
        $fwfm_lebih7hari = 0;
        $total_fwfm = 0;

      ?>
      @foreach ($SCAO_byumur as $result)
      <?php
        $pi_comply += $result->pi_comply;
        $pi_kurang1hari += $result->pi_kurang1hari;
        $pi_1sd2hari += $result->pi_1sd2hari;
        $pi_2sd3hari += $result->pi_2sd3hari;
        $pi_3sd7hari += $result->pi_3sd7hari;
        $pi_lebih7hari += $result->pi_lebih7hari;
        $total_pi += $result->jumlah_PI;

        $fwfm_kurang1hari += $result->fwfm_kurang1hari;
        $fwfm_1sd2hari += $result->fwfm_1sd2hari;
        $fwfm_2sd3hari += $result->fwfm_2sd3hari;
        $fwfm_3sd7hari += $result->fwfm_3sd7hari;
        $fwfm_lebih7hari += $result->fwfm_lebih7hari;
        $total_fwfm += $result->jumlah_fwfm;

      ?>
      <tr>
        <td>{{ $result->datel }}</td>
        <td><a href="/umur_pi/comply/{{ $result->datel }}">{{ $result->pi_comply }}</a></td>
        <td><a href="/umur_pi/kurang1hari/{{ $result->datel }}">{{ $result->pi_kurang1hari }}</a></td>
        <td><a href="/umur_pi/1sd2hari/{{ $result->datel }}">{{ $result->pi_1sd2hari }}</a></td>
        <td><a href="/umur_pi/2sd3hari/{{ $result->datel }}">{{ $result->pi_2sd3hari }}</a></td>
        <td><a href="/umur_pi/3sd7hari/{{ $result->datel }}">{{ $result->pi_3sd7hari }}</a></td>
        <td><a href="/umur_pi/lebih7hari/{{ $result->datel }}">{{ $result->pi_lebih7hari }}</a></td>
        <td><a href="/umur_pi/all/{{ $result->datel }}">{{ $result->jumlah_pi }}</a></td>
        <td><a href="/umur_fwfm/kurang1hari/{{ $result->datel }}">{{ $result->fwfm_kurang1hari }}</a></td>
        <td><a href="/umur_fwfm/1sd2hari/{{ $result->datel }}">{{ $result->fwfm_1sd2hari }}</a></td>
        <td><a href="/umur_fwfm/2sd3hari/{{ $result->datel }}">{{ $result->fwfm_2sd3hari }}</a></td>
        <td><a href="/umur_fwfm/3sd7hari/{{ $result->datel }}">{{ $result->fwfm_3sd7hari }}</a></td>
        <td><a href="/umur_fwfm/lebih7hari/{{ $result->datel }}">{{ $result->fwfm_lebih7hari }}</a></td>
        <td><a href="/umur_fwfm/all/{{ $result->datel }}">{{ $result->jumlah_fwfm }}</a></td>

      </tr>
      @endforeach
      <tr>
        <th class="align-middle warna7 text1">TOTAL</th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/comply/ALL">{{ $pi_comply }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/kurang1hari/ALL">{{ $pi_kurang1hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/1sd2hari/ALL">{{ $pi_1sd2hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/2sd3hari/ALL">{{ $pi_2sd3hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/3sd7hari/ALL">{{ $pi_3sd7hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/lebih7hari/ALL">{{ $pi_lebih7hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_pi/all/ALL">{{ $total_pi }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/kurang1hari/ALL">{{ $fwfm_kurang1hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/1sd2hari/ALL">{{ $fwfm_1sd2hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/2sd3hari/ALL">{{ $fwfm_2sd3hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/3sd7hari/ALL">{{ $fwfm_3sd7hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/lebih7hari/ALL">{{ $fwfm_lebih7hari }}</a></th>
        <th class="align-middle warna7 text1"><a href="/umur_fwfm/all/ALL">{{ $total_fwfm }}</a></th>
      </tr>
    </table>
  </div>
</div>
@endsection
