@extends('layout')

@section('content')
  @include('partial.alerts')
  <style media="screen">
    .tableProfile td {
      padding : 2px 5px !important;
    }
    .tableProfile th {
      padding : 2px 5px !important;
      font-weight: bold;
    }
    .btnFilter {
      position: absolute !important;
      bottom: 0 !important;
    }
    .panel .panel-body {
      padding: 0px !important;
    }
    .textTocenter {
      text-align: center !important;
    }
    table {
    }
  </style>
  <center>
    <h3>DOCUMENT MANAGEMENT</h3><br /><br />
  </center>
  <div class="row">
    <div class="col-md-12">
      <div class="white-box">
        <form action="" method="get">
          <div class="row">
            <div class="col-sm-3">
              <label for="dateStart">Start</label>
              <input type="text" name="dateStart" id="dateStart" class="form-control" value="{{ $dateStart }}">
            </div>
            <div class="col-sm-3">
              <label for="dateEnd">End</label>
              <input type="text" name="dateEnd" id="dateEnd" class="form-control" value="{{ $dateEnd }}">
            </div>
            <div class="col-sm-3" style="position:relative !important">
              <input type="button" class="btn btn-success btnFilter" name="button" id="btnFilter" value="Filter">
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          External RECIPIENT
        </div>
        <div class="panel-body table-responsive">
          <table class="tableProfile" border=1 width=100%>
            <tr>
              <th rowspan="2">RECIPIENT</th>
              <th class="textTocenter" colspan="{{ $jml_documentType }}" >DOCUMENT TYPE</th>
            </tr>
            <tr>
              @foreach ($get_documentType as $r)
              <th width="15%">{{ $r->documentType }}</th>
              @endforeach
            </tr>
            <?php
              $total = array();
              foreach ($get_documentType as $r){
                $total['jml_'.$r->documentTypeID] = 0;
              }
            ?>
            @foreach ($get_documentDashboardExternal as $num => $r)
            <tr>
              <td>{{ $r->nama_company }}</td>
              @foreach ($get_documentType as $rt)
              <td><a href="/documentDashboardExternalList/{{ $dateStart }}/{{ $dateEnd }}/{{$r->id }}/{{ $rt->documentTypeID }}">
              <?php
                $object_name = 'jml_'.$rt->documentTypeID;
                $total[$object_name] += @$r->$object_name;
                echo @$r->$object_name; ?></a>
              </td>
              @endforeach
            </tr>
            @endforeach
            <tr>
              <th class="textTocenter">TOTAL</th>
              @foreach ($get_documentType as $r)
              <th><a href="/documentDashboardExternalList/{{ $dateStart }}/{{ $dateEnd }}/ALL/{{ $r->documentTypeID }}">
              <?php echo ($total['jml_'.$r->documentTypeID]); ?></a></th>
              @endforeach
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script>
  $(function(){
    $('#dateStart').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
    });

    $('#dateEnd').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
    });
    $('#btnFilter').click(function(){
      var url = '/documentDashboard/'+$('#dateStart').val()+'/'+$('#dateEnd').val();
      location.href = url;
    });
  });
  </script>
@endsection
