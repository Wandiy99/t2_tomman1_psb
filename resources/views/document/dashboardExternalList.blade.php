@extends('layout')

@section('content')
@include('partial.alerts')
<a href="{{ redirect()->getUrlGenerator()->previous() }}" class="btn btn-sm btn-default">
  <span class="glyphicon glyphicon-arrow-left"></span>
</a>
<br />
<br />
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        LIST DOCUMENT {{ @$get_documentTypeDetail->documentType ? : "ALL" }} {{ @$get_documentMitraDetail->nama_company ? : "ALL" }} {{ $dateStart }} s/d {{ $dateEnd }}
      </div>
      <div class="panel-body">
        <table id="tableList">
          <thead>
            <tr>
              <td>#</td>
              <td>RECIPIENT</td>
              <td>TYPE</td>
              <td>TITLE</td>
              <td>DATE</td>
              <td>SENDER</td>
            </tr>
          </thead>
          <tbody>
            @foreach($get_documentExternalList as $n => $r)
            <tr>
              <td>{{ ++$n }}</td>
              <td>{{ $r->nama_company }}</td>
              <td>{{ $r->documentType }}</td>
              <td>{{ $r->documentTitle }}</td>
              <td>{{ $r->documentDate }}</td>
              <td>{{ $r->user }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  $('#tableList').DataTable();
});
</script>
@endsection
