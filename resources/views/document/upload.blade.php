@extends('layout')

@section('content')
@include('partial.alerts')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                UPLOAD DOCUMENT
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form method="post">
                        <div class="form-body">
                            <h3 class="box-title">DOCUMENT INFO</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentTitle" class="control-label">TITLE</label>
                                        <input type="text" id="documentTitle" name="documentTitle" class="form-control" placeholder="MoM Kesepakatan Operasional Provisioning Mitra" value="{{ old('documentTitle') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentDate" class="control-label">DATE</label>
                                        <input type="text" id="documentDate" name="documentDate" class="form-control" placeholder="YYYY-MM-dd" value="{{ old('documentDate') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentType" class="control-label">TYPE</label>
                                        <select id="documentType" name="documentType" class="form-control">
                                            <option value="">--</option>
                                            @foreach ($get_documentType as $result)
                                            <option value="{{ $result->employee_document_type_id }}">{{ $result->employee_document_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentUnit" class="control-label">UNIT</label>
                                        <select id="documentUnit" name="documentUnit" class="form-control">
                                            <option value="">--</option>
                                            @foreach ($get_documentUnit as $result)
                                            <option value="{{ $result->employee_unit_id }}">{{ $result->employee_unit }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentInternal">INTERNAL</label>
                                        <button type="button" data-toggle="modal" data-target="#documentInternalModal" class="btn btn-success">+</button>
                                        <BR />
                                        <textarea name="documentInternal" id="documentInternal" READONLY cols="55" rows="10"></textarea>
                                    </div>
                                    <div class="modal fade" id="documentInternalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4>Employee Search</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="form-group">
                                                                <div class="table-responsive">
                                                                    <table id="documentInternalModalTable" class="table table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>NIK</th>
                                                                                <th>NAME</th>
                                                                                <th>POSITION</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentExternal">EXTERNAL</label>
                                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-success">+</button>
                                        <BR />
                                        <textarea name="documentExternal" id="documentExternal" READONLY cols="55" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="multi">PUT YOUR FILES DOWN HERE</label>
                                        <input type="file" name="documentFile" id="documentFile">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('#documentDate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
        var table = $('#documentInternalModalTable').DataTable({
          processing: true,
          serverSide: true,
          "ajax": {
            "url": "/json/DocumentInternal",
            "dataSrc" : ""
          },
          columns:[
            {data : 'employee_nik'},
            {data : 'employee_name'},
            {data : 'employee_position'}
          ]

        });
        $('#documentInternalModalTable tbody').on('click', 'tr', function () {
            var data = table.row(this).data();
            var oldValueDocumentInternal = $("textarea#documentInternal").val();
            $("textarea#documentInternal").val(oldValueDocumentInternal + data[0] + "|" + data[1] +"\n");
            alert('Added ' + data[0] + "");
            table.row(this).remove();
        });

    });
</script>
@endsection
