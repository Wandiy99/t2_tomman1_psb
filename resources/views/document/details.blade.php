@extends('layout')

@section('content')
  @include('partial.alerts')
  <style media="screen">
    .tableProfile td {
      padding : 0 5px !important;
    }
  </style>
  <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a>
  <br />
  <br />
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-heading">
          DOCUMENT
        </div>
        <div class="panel-body table-responsive">
          <b>INFORMATION</b><br />
          <table class="tableProfile">
            @foreach ($get_documentDetails as $result)
            <tr>
              <td>TITLE</td>
              <td>:</td>
              <td>{{ $result->documentTitle }}</td>
            </tr>
            <tr>
              <td>TYPE</td>
              <td>:</td>
              <td>{{ $result->documentType }}</td>
            </tr>
            <tr>
              <td>PERIOD</td>
              <td>:</td>
              <td>{{ $result->documentDate }}</td>
            </tr>
            <tr>
              <td>CREATED BY</td>
              <td>:</td>
              <td>{{ $result->documentUserCreated }}</td>
            </tr>
            @endforeach
          </table>
          <br />
          <b>INTERNAL DISPOSITION</b><br />
          <table class="tableProfile" border=1>
            <tr>
              <td>#</td>
              <td>NIK</td>
              <td>NAMA</td>
              <td>JABATAN</td>
            </tr>
            @foreach ($get_documentDetailsInternal as $num => $result)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $result->nik }}</td>
              <td>{{ $result->user }}</td>
              <td>{{ $result->jabatan }}</td>
            </tr>
            @endforeach
          </table>
          <br />
          <b>EXTERNAL DISPOSITION</b><br />
          <table class="tableProfile" border="1">
            <tr>
              <td>#</td>
              <td>NAMA PERUSAHAAN</td>
            </tr>
            @foreach ($get_documentDetailsExternal as $num => $result)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $result->nama_company }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          FILES
        </div>
        <div class="panel-body">
        <?php
        foreach ($files as $path) {
          $file = pathinfo($path);
          echo '<img src="/upload4/employee_document/google-docs.png" width=48 /></p><a href="/upload4/employee_document/'.$documentID.'/'.$file['filename'].'.'.$file['extension'].'">Download</a>';
        }
        ?>
        </div>
      </div>
    </div>
  </div>
@endsection
