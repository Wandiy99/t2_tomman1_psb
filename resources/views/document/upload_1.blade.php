@extends('layout')

@section('content')
@include('partial.alerts')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                UPLOAD DOCUMENT
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form method="post" id="documentForm" enctype="multipart/form-data">
                        <div class="form-body">
                            <h3 class="box-title">DOCUMENT INFO</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentTitle" class="control-label">TITLE</label>
                                        <input required type="text" id="documentTitle" name="documentTitle" class="form-control" placeholder="MoM Kesepakatan Operasional Provisioning Mitra" value="{{ old('documentTitle') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentDate" class="control-label">PERIOD</label>
                                        <input required type="text" id="documentDate" name="documentDate" class="form-control" placeholder="YYYY-MM-dd" value="{{ old('documentDate') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentType" class="control-label">TYPE</label>
                                        <select required id="documentType" name="documentType" class="form-control">
                                            <option value="">--</option>
                                            @foreach ($get_documentType as $result)
                                            <option value="{{ $result->documentTypeID }}">{{ $result->documentType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentUnit" class="control-label">UNIT</label>
                                        <select required multiple id="documentUnit" name="documentUnit[]" class="form-control">
                                            <option value="">--</option>
                                            @foreach ($get_documentUnit as $result)
                                            <option value="{{ $result->employee_unit_id }}">{{ $result->employee_unit }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentInternal">INTERNAL</label>
                                        <select required multiple class="form-control" id="documentInternal" name="documentInternal[]">
                                        @foreach ($get_documentInternal as $result)
                                        <option value="{{ $result->employee_nik }}">{{ $result->employee_name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="documentExternal">EXTERNAL</label>
                                        <select required multiple class="form-control" id="documentExternal" name="documentExternal[]">
                                        @foreach ($get_documentExternal as $result)
                                        <option value="{{ $result->id }}">{{ $result->nama_company }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="multi">PUT YOUR FILE DOWN HERE</label>
                                        <input required type="file" name="documentFile" id="documentFile">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('#documentDate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
        $('#documentInternal').select2({
          placeholder:"ADD INTERNAL DISPOSITION"
        });
        $('#documentExternal').select2({
          placeholder:"ADD EXTERNAL DISPOSITION"
        });
        $('#documentUnit').select2({
          placeholder:"ADD UNIT IN-CHARGE"
        });

        var table = $('#documentInternalModalTable').DataTable({
          processing: true,
          serverSide: true,
          "ajax": {
            "url": "/json/DocumentInternal",
            "dataSrc" : ""
          },
          columns:[
            {data : 'employee_nik'},
            {data : 'employee_name'},
            {data : 'employee_position'}
          ]

        });
        $('#documentInternalModalTable tbody').on('click', 'tr', function () {
            var data = table.row(this).data();
            var oldValueDocumentInternal = $("textarea#documentInternal").val();
            $("textarea#documentInternal").val(oldValueDocumentInternal + data[0] + "|" + data[1] +"\n");
            alert('Added ' + data[0] + "");
            table.row(this).remove();
        });

    });
</script>
@endsection
