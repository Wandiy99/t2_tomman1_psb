@extends('layout')

@section('content')
  @include('partial.alerts')
  <style media="screen">
    #tableProfile td {
      padding : 0 5px !important;
    }
  </style>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          PROFILE
        </div>
        <div class="panel-body table-responsive">
          <div class="row">
            <div class="col-md-2">
              <center>
              <img src="/image/people.png" width="100" />
              </center>
            </div>
            <div class="col-md-8">
              <table id="tableProfile">
                <thead>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ session('auth')->nama }}</td>
                  </tr>
                </thead>
                <tbody>
                @foreach ($get_job as $result)
                <tr>
                  <td>Jabatan</td>
                  <td>:</td>
                  <td>{{ $result->jabatan }}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          YOUR DOCUMENTS
        </div>
        <div class="panel-body table-responsive">
          <table class="table" id="yoursDocument">
            <thead>
              <tr>
                <td>#</td>
                <td>TYPE</td>
                <td>TITLE</td>
                <td>PERIOD</td>
              </tr>
            </thead>
            <tbody>
            @foreach ($get_document as $num => $result)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $result->documentType }}</td>
              <td><a href="/documentDetails/{{ $result->documentID }}">{{ $result->documentTitle }}</a></td>
              <td>{{ $result->documentDate }}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          INTERNAL DISPOSITION
        </div>
        <div class="panel-body table-responsive">
          <table class="table" id="tableDocumentInternal">
            <thead>
              <tr>
                <td>#</td>
                <td>FROM</td>
                <td>TYPE</td>
                <td>TITLE</td>
                <td>PERIOD</td>
              </tr>
            </thead>
            <tbody>
            @foreach ($get_documentInternalList as $num => $result)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $result->documentFrom }}</td>
              <td>{{ $result->documentType }}</td>
              <td><a href="/documentDetails/{{ $result->documentID }}">{{ $result->documentTitle }}</a></td>
              <td>{{ $result->documentDate }}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      $(function() {
        $('#tableDocumentInternal').DataTable();
        $('#yoursDocument').DataTable();
      });
  </script>
@endsection
