@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/tactical/pickupOnline?source={{ $source }}&witel={{ $witel }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT PICKUP ONLINE WITEL {{ $witel }} AREA {{ $area }} STATUS {{ $status }} SUMBER {{ $source }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        @if($witel <> "BALIKPAPAN")
                        <th>TIM</th>
                        @endif
                        <th>TL</th>
                        <th>ID_DT</th>
                        <th>SC</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>TGL SELESAI KERJA</th>
                        <th>TGL PS TACTICAL</th>
                        <th>TYPE LAYANAN</th>
                        <th>STATUS {{ strtoupper($source) }}</th>
                        <th>STATUS WO</th>
                        @if($witel <> "BALIKPAPAN")
                        <th>STATUS TEKNISI</th>
                        <th>DATEL</th>
                        @endif
                        <th>TEKNISI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area ? : 'NON AREA' }}</td>
                        @if($witel <> "BALIKPAPAN")
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->TL ? : $data->teamleader_name ? : '#N/A' }}</td>
                        <td>{{ $data->id_dt ? : '0' }}</td>
                        <td>{{ $data->order_id ? : '#N/A' }}</td>
                        <td>{{ $data->order_date ? : '#N/A' }}</td>
                        <td>{{ $data->order_date_ps ? : '#N/A' }}</td>
                        <td>{{ $data->tgl_selesai_kerja ? : '#N/A' }}</td>
                        <td>{{ $data->ps_date ? : '#N/A' }}</td>
                        <td>{{ $data->type_layanan ? : '#N/A' }}</td>
                        <td>{{ $data->status_resume ? : '#N/A' }}</td>
                        <td>{{ $data->status_wo ? : '#N/A' }}</td>
                        @if($witel <> "BALIKPAPAN")
                        <td>{{ $data->laporan_status ? : 'ANTRIAN' }}</td>
                        <td>{{ $data->datel ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->teknisi ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT PICKUP ONLINE TACTICAL BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection