@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }
  </style>
  <h3>Dashboard Provisioning TA KALSEL</h3>
  <small>Periode {{ $tgl }}</small>
  <div class="row">
   
    <div class="col-sm-4">
        <table class="table table-responsive">
            <tr>
                <th>Area</th>
                <th>Undispatch</th>
                <th>Kendala</th>
            </tr>

            <?php $jumlah = 0; $jmlKendala = 0;?>
            @foreach($reportPotensi as $potensi)
              <tr>
                  <td>{{ $potensi->area }}</td>
                  <td><a href="/dashboard/listpotensi/{{ date('Y-m-d') }}/{{ $potensi->area }}/undispatch">{{ $potensi->undispatch ?: '0'}}</a></td>
                  <td><a href="/dashboard/listpotensi/{{ date('Y-m-d') }}/{{ $potensi->area }}/kendala">{{ $potensi->kendala ?: '0'}}</a></td>
                  <?php 
                      $jumlah += $potensi->undispatch;
                      $jmlKendala += $potensi->kendala;
                  ?>
              </tr>
            @endforeach
            <tr>
                <td>Total</td>
                <td><a href="/dashboard/listpotensi/{{ date('Y-m-d') }}/ALL/undispatch">{{ $jumlah }}</a></td>
                <td><a href="/dashboard/listpotensi/{{ date('Y-m-d') }}/ALL/kendala">{{ $jmlKendala }}</a></td>
            </tr>
        </table>
    </div>
  </div>
  <br />
  <br />
  <br />
  <br />
@endsection

@section('plugins')
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
      $(document).ready(function() {
          $('.status').select2();
      });
  </script>
@endsection
