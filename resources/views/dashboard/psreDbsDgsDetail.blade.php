@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/psredbsdgs" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT PS/RE DBS-DGS SEKTOR {{ $sektor }} PERIODE {{ $month }} DAY {{ $day }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>ORDER ID</th>
                        <th>NCLI</th>
                        <th>ND_POTS</th>
                        <th>ND_SPEEDY</th>
                        <th>CUSTOMER_NAME</th>
                        <th>STATUS_TEKNISI</th>
                        <th>TGL_LAPORAN_TEKNISI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $num => $result)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->sektor ? : 'NON AREA' }}</td>
                        <td>{{ $result->tim }}</td>
                        <td>{{ $result->ORDER_ID }}</td>
                        <td>{{ $result->NCLI }}</td>
                        <td>{{ $result->POTS }}</td>
                        <td>{{ $result->SPEEDY }}</td>
                        <td>{{ $result->CUSTOMER_NAME }}</td>
                        <td>{{ $result->status_tek }}</td>
                        <td>{{ $result->tgl_laporan_tek }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT PS/RE DBS-DGS SEKTOR BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection