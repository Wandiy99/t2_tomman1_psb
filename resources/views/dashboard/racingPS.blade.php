@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0" style="font-weight: bolder !important;color: black !important;"><center>RACING PS TEKNISI {{ $start }} S/D {{ $end }}</center></h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="2">NIK</th>
                        <th rowspan="2">NAMA</th>
                        <th colspan="4">PEKERJAAN</th>
                        <th rowspan="2">FFG</th>
                        <th rowspan="2">POINT</th>
                        <th colspan="3">KUPON</th>
                    </tr>
                    <tr>
                        <td>AO VALID</td>
                        <td>SOLUSI KENDALA</td>
                        <td>MO</td>
                        <td>DISMANTLING</td>
                        <td>VIP</td>
                        <td>REGULER</td>
                        <td>SISA POINT</td>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total_aovalid = $total_solken = $total_mo = $total_dism = $total_ffg = $total_point_tech = 0;
                @endphp
                @foreach ($query as $nik => $result)
                    @php
                        $order_aovalid = @$result['jml_order_ao_nik1'] + @$result['jml_order_ao_nik2'];
                        $order_solken = @$result['jml_order_sk_nik1'] + @$result['jml_order_sk_nik2'];
                        $order_mo = @$result['jml_order_mo_nik1'] + @$result['jml_order_mo_nik2'];
                        $order_dismantling = @$result['jml_order_dism_nik1'] + @$result['jml_order_dism_nik2'];
                        $order_ffg = @$result['jml_order_ffg_nik1'] + @$result['jml_order_ffg_nik2'];
                        $point_tech = ($order_aovalid * 4) + ($order_solken * 3) + ($order_mo * 2) + ($order_dismantling * 1) - ($order_ffg * 4);
                        
                        $total_aovalid += $order_aovalid;
                        $total_solken += $order_solken;
                        $total_mo += $order_mo;
                        $total_dism += $order_dismantling;
                        $total_ffg += $order_ffg;
                        $total_point_tech += $point_tech;
                    @endphp
                <tr>
                    <td>{{ $nik }}</td>
                    <td>{{ @$result['nama'] }}</td>
                    <td>{{ $order_aovalid }}</td>
                    <td>{{ $order_solken }}</td>
                    <td>{{ $order_mo }}</td>
                    <td>{{ $order_dismantling }}</td>
                    <td>{{ $order_ffg }}</td>
                    <td>{{ $point_tech }}</td>
                    <td>{{ @$result['coupon_vip'] }}</td>
                    <td>{{ @$result['coupon_reguler'] }}</td>
                    <td>{{ @$result['point_now'] }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="font-weight: bold">TOTAL</td>
                        <td>{{ $total_aovalid }}</td>
                        <td>{{ $total_solken }}</td>
                        <td>{{ $total_mo }}</td>
                        <td>{{ $total_dism }}</td>
                        <td>{{ $total_ffg }}</td>
                        <td>{{ $total_point_tech }}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
@endsection