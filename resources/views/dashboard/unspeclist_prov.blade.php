@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>LIST UNSPEC {{ $sektor }}</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">PERIODE {{ $date }}</div>
  			<div class="panel-body table-responsive" style="padding:0px !important">
  				<table class="table table-stripped">
  					<tr>
  						<th>NO</th>
              <th>TL</th>
              <th>SEKTOR</th>
              <th>STO</th>
              <th>TIM</th>
  						<th>ORDER ID</th>
              <th>CUSTOMER</th>
              <th>K-CONTACT</th>
  						<th>ND INET / ND VOICE</th>
              <th>STATUS</th>
              <th>PSB JENIS</th>
  						<th>CATATAN</th>
              <th>UPDATED BY</th>
              <th>WAKTU LAPORAN</th>
  						<th>REDAMAN</th>
  					</tr>
  					@foreach ($query as $no => $result)
  					<tr>
  						<td>{{ ++$no }}</td>
  						<td>{{ $result->TL }}</td>
              <td>{{ $result->title }}</td>
              <td>{{ $result->sektorx }}</td>
              <td>{{ $result->uraian }}</td>
              <td>
                <a href="/{{ $result->dt_id }}">{{ $result->Ndem }}<br />
                <a href="/grabIboosterbySc/{{ $result->Ndem }}/psb" class="label label-success">Ukur Ibooster</a><br />
                <a href="/syncSC/{{ $result->Ndem }}" class="label label-info">SYNC SC</a>
              </td>
  						<td>{{ $result->orderName ? : $result->customer ? : 'Tidak Ada Informasi Nama Customer' }}</td>
  						<td>{{ $result->kcontact ? : $result->kcontack ? : 'Tidak Ada Informasi K-CONTACT' }}</td>
  						<td>{{ $result->no_internet ? : $result->ndemSpeedy ? : 'Tidak Ada Informasi ND INET' }} / {{ $result->ndemPots ? : 'Tidak Ada Informasi ND VOICE' }}</td>
              <td>{{ $result->orderStatus ? : $result->laporan_status ? : 'Tidak Ada Informasi Status' }}</td>
  						<td>{{ $result->jenisPsb ? : $result->myir_layanan ? : 'Tidak Ada Informasi Layanan' }}</td>
              <td>{{ $result->catatan }}</td>
              <td>{{ $result->modified_by }}</td>
              <td>{{ $result->modified_at }}</td>
              <td>{{ $result->redaman_iboster ? : '0' }} dbm</td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection