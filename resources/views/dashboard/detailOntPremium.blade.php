@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">PRODUKTIFITAS {{ $kat }} SEKTOR {{ $sektor }} STATUS {{ $status }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>INET</th>
                        <th>VOICE</th>
                        <th>CUSTOMER</th>
                        <th>PIC</th>
                        @if ($status=="UNDISPATCH")
                        <th>HISTORY MANJA</th>
                        @else
                        <th>KOORDINAT PELANGGAN</th>
                        <th>ALAMAT</th>
                        <th>CATATAN</th>
                        <th>STATUS</th>
                        <th>REGU</th>
                        <th>SEKTOR</th>
                        <th>TGL DISPATCH</th>
                        <th>TGL SELESAI</th>
                        @endif
                        <th>TGL ORDER</th>
                        <th>ORDER BY</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $result)
                <tr>
                    <td>{{ ++$num }}</td>
                    @if($result->id_dt <> "")
                        <td><a href="/{{$result->id_dt}}">{{ $result->no_inet }}</a></td>
                    @else
                        <td>{{ $result->no_inet }}</td>
                    @endif
                    <td>{{ $result->no_voice }}</td>
                    <td>{{ $result->nama_pelanggan }}</td>
                    <td>{{ $result->noPelangganAktif ? : $result->no_hp }}</td>
                    @if ($status=="UNDISPATCH")
                    <td>{{ $result->ket_manja }}</td>
                    @else
                    <td>{{ $result->kordinat_pelanggan }}</td>
                    <td>{{ $result->alamat }}</td>
                    <td>{{ $result->catatan }}</td>
                    <td>{{ $result->laporan_status ? : 'ANTRIAN' }}</td>
                    <td>{{ $result->tim }}</td>
                    <td>{{ $result->sektor }}</td>
                    <td>{{ $result->tgl_dt }}</td>
                    <td>{{ $result->modified_at }}</td>
                    @endif
                    <td>{{ $result->tanggal_order }}</td>
                    <td>{{ $result->dispatch_by }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'PRODUKTIFITAS ONT PREMIUM TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'PRODUKTIFITAS ONT PREMIUM TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'PRODUKTIFITAS ONT PREMIUM TOMMAN'
                }
            ]
        });
    });
</script>
@endsection