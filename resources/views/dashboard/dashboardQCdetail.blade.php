@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/QCBorneoMitra/{{ $date }}/{{ $datex }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT QUALITY CONTROL {{ $mitra }} PERIODE {{ $date }} S/D {{ $datex }} UNIT {{ $tag }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>STO</th>
                        <th>INET</th>
                        <th>VOICE</th>
                        @if($mitra == "NA")
                            <th>ORDER STATUS</th>
                            <th>LAYANAN</th>
                        @endif
                        <th>CUSTOMER</th>
                        <th>PACKAGE NAME</th>
                        <th>ODP TEKNISI</th>
                        <th>KOR ODP TEKNISI</th>
                        <th>ODP STARCLICK</th>
                        <th>FOTO RUMAH</th>
                        <th>FOTO TEKNISI</th>
                        <th>FOTO ODP</th>
                        <th>FOTO REDAMAN</th>
                        <th>FOTO CPE LAYANAN</th>
                        <th>FOTO BERITA ACARA</th>
                        <th>TAG LOKASI</th>
                        <th>FOTO SURAT</th>
                        <th>FOTO PROFILE MYIH</th>
                        <th>TAG UNIT</th>
                        {{-- <th>KATEGORI</th> --}}
                        <th>SEKTOR</th>
                        <th>TL</th>
                        <th>TGL PS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->sc }}</td>
                        <td>{{ $data->uraian ? : $data->nikTeknisi ? : '#N/A' }}</td>
                        <td>{{ $data->mitra ? : $data->bo_mitra ? : '#N/A' }}</td>
                        <td>{{ $data->sto ? : '#N/A' }}</td>
                        <td>{{ $data->no_inet }}</td>
                        <td>{{ $data->no_voice }}</td>
                        @if($mitra=="NA")
                            <td>{{ $data->orderStatus }}</td>
                            <td>{{ $data->jenisPsb }}</td>
                        @endif
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->kt_package_name ? : '-' }}</td>
                        <td>{{ $data->nama_odp ? : '#N/A' }}</td>
                        <td>{{ $data->kordinat_odp ? : '#N/A' }}</td>
                        <td>{{ $data->alproname ? : '#N/A' }}</td>
                        <td>{{ $data->foto_rumah ? : '#N/A' }}</td>
                        <td>{{ $data->foto_teknisi ? : '#N/A' }}</td>
                        <td>{{ $data->foto_odp ? : '#N/A' }}</td>
                        <td>{{ $data->foto_redaman ? : '#N/A' }}</td>
                        <td>{{ $data->foto_cpe_layanan ? : '#N/A' }}</td>
                        <td>{{ $data->foto_berita_acara ? : '#N/A' }}</td>
                        <td>{{ $data->tag_lokasi ? : '#N/A' }}</td>
                        <td>{{ $data->foto_surat ? : '#N/A' }}</td>
                        <td>{{ $data->foto_profile_myih ? : '#N/A' }}</td>
                        <td>{{ $data->tag_unit }}</td>
                        {{-- <td>{{ $data->kategori ? : '#N/A' }}</td> --}}
                        <td>{{ $data->sektor ? : '#N/A' }}</td>
                        <td>{{ $data->TL ? : '#N/A' }}</td>
                        <td>{{ $data->datePs ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>STO</th>
                        <th>INET</th>
                        <th>VOICE</th>
                        @if($mitra=="NA")
                            <th>ORDER STATUS</th>
                            <th>LAYANAN</th>
                        @endif
                        <th>CUSTOMER</th>
                        <th>PACKAGE NAME</th>
                        <th>ODP TEKNISI</th>
                        <th>KOR ODP TEKNISI</th>
                        <th>ODP STARCLICK</th>
                        <th>FOTO RUMAH</th>
                        <th>FOTO TEKNISI</th>
                        <th>FOTO ODP</th>
                        <th>FOTO REDAMAN</th>
                        <th>FOTO CPE LAYANAN</th>
                        <th>FOTO BERITA ACARA</th>
                        <th>TAG LOKASI</th>
                        <th>FOTO SURAT</th>
                        <th>FOTO PROFILE MYIH</th>
                        <th>TAG UNIT</th>
                        {{-- <th>KATEGORI</th> --}}
                        <th>SEKTOR</th>
                        <th>TL</th>
                        <th>TGL PS</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT QUALITY CONTROL MITRA TOMMAN'
                }
            ]
        });
    });
</script>
@endsection