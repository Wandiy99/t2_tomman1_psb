@extends('layout')
@section('content')
<style>
  .label {
    font-size: 12px;
  }
  th {
    border-color: #34495e;
    background-color: #7f8c8d;
    color : #ecf0f1;
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  .warna1{
    background-color: #3498DB;
  }
  .warna2{
    background-color: #85C1E9;
  }
  .warna3{
    background-color: #D6EAF8;
  }
  .warna4{
    background-color: #E67E22;
    #F0B27A
  }
  .warna4x{
    background-color: #F0B27A;
    #FAE5D3
  }
  .warna4z{
    background-color: #FAE5D3;
  }
  .warna5{
    background-color: #E74C3C;
    #F0B27A
  }
  .warna5x{
    background-color: #F1948A;
    #FAE5D3
  }
  .warna5z{
    background-color: #FADBD8;
  }
  .colorArea{
    background-color: #F4D03F;
  }
  .colorArea2{
    background-color: #FCF3CF;
  }
  .colorArea3{
    background-color: #F7DC6F;
  }
  .colorComparin{
    background-color: #48C9B0;
  }
  .colorComparin2{
    background-color: #D1F2EB;
  }
  .colorComparin3{
    background-color: #76D7C4;
  }
  .text2{
    color: black !important;
  }
  .text1{
    color: white !important;
  }
  .text1:link{
    color: white !important;
  }
  .text1:visited{
    color: white !important;
  }
  th a:link{
    color: white;
  }
  th a:visited{
    color: white;
  }
  .overload{
    background-color: red;
    color: white;
  }
  .black-grey {
        background-color: #34495E;
        color: white !important;
    }
  .red-lose {
      background-color: #E74C3C;
      color: white !important;
  }
  .green-win {
      background-color: #1ABC9C;
      color: white !important;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 4px 0px !important;
  }
</style>

@include('partial.alerts')

<div class="col-sm-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD POTENSI PS<br />PERIODE {{ date('Y-m-d') }}</h4><br/>
        <div class="row">
            <div class="col-md-5">
                <form method="GET">
                    <label for="group">GROUP</label>
                    <select class="form-control" data-placeholder="- Chooice a Group -" tabindex="1" name="group">
                        @if($group == 'HERO')
                        <option value="HERO">HERO</option>
                        <option value="PROV">PROV</option>
                        <option value="SEKTOR">SEKTOR</option>
                        <option value="DATEL">DATEL</option>
                        @elseif ($group == 'SEKTOR')
                        <option value="SEKTOR">SEKTOR</option>
                        <option value="DATEL">DATEL</option>
                        <option value="HERO">HERO</option>
                        <option value="PROV">PROV</option>
                        @elseif ($group == 'DATEL')
                        <option value="DATEL">DATEL</option>
                        <option value="HERO">HERO</option>
                        <option value="PROV">PROV</option>
                        <option value="SEKTOR">SEKTOR</option>
                        @elseif ($group == 'PROV')
                        <option value="PROV">PROV</option>
                        <option value="SEKTOR">SEKTOR</option>
                        <option value="DATEL">DATEL</option>
                        <option value="HERO">HERO</option>
                        @endif
                    </select>
                </div>
                <div class="col-md-5">
                    <label for="witel">WITEL</label>
                    <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" disabled>
                        <option value="KALSEL">KALSEL</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="search">&nbsp;</label>
                    <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="col-sm-12 table-responsive">
  <table class="table table-bordered dataTable">
    <tr>
      <th class="align-middle colorArea text1" rowspan="2">{{ $group }}</th>
      <th class="align-middle warna1 text1" colspan="3">POTENSI PS AO</th>
      {{-- <th class="align-middle colorComparin text1" colspan="2">PRABAC</th> --}}
      <th class="align-middle warna5 text1" rowspan="2">BYPASS<br />PDA HD</th>
      <th class="align-middle warna4 text1" colspan="2">SISA ORDER</th>
      <th class="align-middle warna4 text1" colspan="2">KENDALA</th>
      <th class="align-middle warna4 text1" colspan="3">PROGRESS</th>
      <th class="align-middle warna4 text1" colspan="2">PDA</th>
      <th class="align-middle warna4x text1" rowspan="2">
        TOTAL ORDER<br />
        @if($group == 'SEKTOR')
        (A2+B2+C+D)
        @elseif($group == 'DATEL')
        (A1+A2+B2+C+D)
        @else
        (C+D)
        @endif
      </th>
      <th class="align-middle warna4x text1" rowspan="2">ACH %<br /> (C / E * 100)</th>
    </tr>
    <tr>
      <th class="align-middle warna2 text1">A.COMP</th>
      <th class="align-middle warna2 text1">PS AO</th>
      <th class="align-middle warna2 text1">EST</th>

      {{-- <th class="align-middle colorComparin3 text1">PS H-1</th>
      <th class="align-middle colorComparin3 text1">PS HI</th> --}}

      <th class="align-middle warna4x text1">UNDISP (A1)</th>
      <th class="align-middle warna4x text1">NP (A2)</th>
      <th class="align-middle warna4x text1">PELANGGAN (B1)</th>
      <th class="align-middle warna4x text1">TEKNIK (B2)</th>
      <th class="align-middle warna4x text1">POTENSI<br />PS AO (C)</th>
      <th class="align-middle warna4x text1">POTENSI<br />PS PDA (C)</th>
      <th class="align-middle warna4x text1">PROGRESS<br />AO (D)</th>
      <th class="align-middle warna4x text1">PROGRESS<br />PDA (D)</th>
      <th class="align-middle warna4x text1">PROGRESS<br />RISMA (X)</th>

    </tr>
    @php
      $total_PI_PAGI = $total_FO_PAGI = $total_PI_BARU = $total_COMP_CANCEL = $total_acomp = $total_ps = $total_start_cancel = $total_prabac_ps_h1 = $total_prabac_ps_hi = $total_bypass_pda_hd = $total_antrian_und = $total_antrian_np = $total_kendala_kp = $total_kendala_kt = $total_potensips = $total_potensips_pda = $total_progress = $total_progress_pda = $total_progress_risma = $total_order = $grand_total = $round_order = $total_round = 0;
    @endphp
    @foreach ($potensiPS as $area => $result)
    @php
      $total_PI_PAGI += @$result['PI_PAGI'];
      $total_FO_PAGI += @$result['FO_PAGI'];
      $total_PI_BARU += @$result['PI_BARU'];
      $total_COMP_CANCEL += @$result['COMP_CANCEL'];
      $total_acomp += @$result['actcomp'];
      $total_ps += @$result['ps'];
      $total_start_cancel += @$result['start_cancel'];
      $total_prabac_ps_h1 += @$result['prabac_ps_h1'];
      $total_prabac_ps_hi += @$result['prabac_ps_hi'];
      $total_bypass_pda_hd += @$result['bypass_pda_hd'];
      $total_antrian_und += @$result['antrian_und'];
      $total_antrian_np += @$result['antrian_np'];
      $total_kendala_kp += @$result['potensi_kendala_kp'];
      $total_kendala_kt += @$result['potensi_kendala_kt'];
      $total_potensips += @$result['potensi_ps'];
      $total_potensips_pda += @$result['potensi_ps_pda'];
      $total_progress += @$result['potensi_progress'];
      $total_progress_pda += @$result['potensi_progress_pda'];
      $total_progress_risma += @$result['potensi_progress_risma'];
      if($group == 'SEKTOR')
      {
        $total_order = @$result['antrian_np'] + @$result['potensi_kendala_kt'] + (@$result['potensi_ps'] + @$result['potensi_ps_pda']) + (@$result['potensi_progress'] + @$result['potensi_progress_pda']);
      } elseif ($group == 'DATEL') {
        $total_order = @$result['antrian_und'] + @$result['antrian_np'] + @$result['potensi_kendala_kt'] + (@$result['potensi_ps'] + @$result['potensi_ps_pda']) + (@$result['potensi_progress'] + @$result['potensi_progress_pda']);
      } else {
        $total_order = (@$result['potensi_ps'] + @$result['potensi_ps_pda']) + (@$result['potensi_progress'] + @$result['potensi_progress_pda']);
      };
      $grand_total += $total_order;
      $round_order = @round((@$result['potensi_ps'] + @$result['potensi_ps_pda']) / $total_order * 100,2);
      $total_round = @round($total_potensips / $grand_total * 100,2);
    @endphp
    @if ($area != 'NON AREA')
    <tr>
      <td class="colorArea2 align-middle">{{ $area }}</td>
      <td class="warna3 align-middle"><a class="text2" href="/actcomp/{{ $group }}/{{ $area }}" target="_blank">{{ @$result['actcomp'] ? : '0' }}</a></td>
      <td class="warna3 align-middle"><a class="text2" href="/wo_ps/{{ $group }}/{{ $area }}/{{ date('Y-m-d') }}" target="_blank">{{ @$result['ps'] ? : '0' }}</a></td>
      <td class="warna3 align-middle">{{ @$result['actcomp'] + @$result['ps'] }}</td>
      {{-- <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/prabac_ps_h1" target="_blank">{{ @$result['prabac_ps_h1'] ? : '0' }}</td>
      <td class="colorComparin2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/prabac_ps_hi" target="_blank">{{ @$result['prabac_ps_hi'] ? : '0' }}</td> --}}
      <td class="warna5z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/bypass_pda_hd" target="_blank">{{ @$result['bypass_pda_hd'] ? : '0' }}</td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/antrian_und" target="_blank">{{ @$result['antrian_und'] ? : '0' }}</td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/antrian_np" target="_blank">{{ @$result['antrian_np'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_kendala_kp" target="_blank">{{ @$result['potensi_kendala_kp'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_kendala_kt" target="_blank">{{ @$result['potensi_kendala_kt'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_ps" target="_blank">{{ @$result['potensi_ps'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_ps_pda" target="_blank">{{ @$result['potensi_ps_pda'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_progress" target="_blank">{{ @$result['potensi_progress'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_progress_pda" target="_blank">{{ @$result['potensi_progress_pda'] ? : '0' }}</a></td>
      <td class="warna4z align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/{{ $area }}/potensi_progress_risma" target="_blank">{{ @$result['potensi_progress_risma'] ? : '0' }}</a></td>
      <td class="warna4z text2">{{ $total_order }}</td>
      @if (is_nan($round_order) == true)
          @php
              $roundx = '';
              $bg = 'black-grey';
          @endphp
      @elseif ($round_order >= 90)
          @php
              $roundx = $round_order;
              $bg = 'green-win';
          @endphp
      @else
          @php
              $roundx = $round_order;
              $bg = 'red-lose';
          @endphp
      @endif
      <td class="{{ $bg }}">{{ $roundx }} %</td>
    </tr>
    @endif
    @endforeach
    <tr>
        <td class="colorArea3 text2 align-middle">TOTAL</td>
        <td class="warna2 align-middle"><a class="text2" href="/actcomp/{{ $group }}/ALL" target="_blank">{{ $total_acomp }}</a></td>
        <td class="warna2 align-middle"><a class="text2" href="/wo_ps/{{ $group }}/ALL/{{ date('Y-m-d') }}" target="_blank">{{ $total_ps }}</a></td>
        <td class="warna2 align-middle">{{ $total_ps + $total_acomp }}</td>
        {{-- <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/prabac_ps_h1" target="_blank">{{ $total_prabac_ps_h1 }}</a></td>
        <td class="colorComparin3 text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/prabac_ps_hi" target="_blank">{{ $total_prabac_ps_hi }}</a></td> --}}
        <td class="warna5x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/bypass_pda_hd" target="_blank">{{ $total_bypass_pda_hd }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/antrian_und" target="_blank">{{ $total_antrian_und }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/antrian_np" target="_blank">{{ $total_antrian_np }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_kendala_kp" target="_blank">{{ $total_kendala_kp }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_kendala_kt" target="_blank">{{ $total_kendala_kt }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_ps" target="_blank">{{ $total_potensips }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_ps_pda" target="_blank">{{ $total_potensips_pda }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_progress" target="_blank">{{ $total_progress }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_progress_pda" target="_blank">{{ $total_progress_pda }}</a></td>
        <td class="warna4x text2 align-middle"><a class="text2" href="/dashboard/potensiPSDetail/{{ $group }}/ALL/potensi_progress_risma" target="_blank">{{ $total_progress_risma }}</a></td>
        <td class="warna4x text2 align-middle">{{ $grand_total }}</td>
        @if (is_nan($total_round) == true)
            @php
                $roundx = '';
                $bg = 'black-grey';
            @endphp
        @elseif ($total_round >= 90)
            @php
                $roundx = $total_round;
                $bg = 'green-win';
            @endphp
        @else
            @php
                $roundx = $total_round;
                $bg = 'red-lose';
            @endphp
        @endif
        <td class="{{ $bg }}">{{ $roundx }} %</td>
    </tr>
  </table>
</div>

@endsection
