@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/produktifitasTeam?startDate={{ $start }}&endDate={{ $end }}&area={{ $area }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL PRODUKTIFITAS TEKNISI AREA {{ $area }} PERIODE {{ $start }} S/D {{ $end }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>MITRA</th>
                        <th>ORDER_ID</th>
                        <th>ORDER_DATE</th>
                        <th>STATUS_TEKNISI</th>
                        <th>TGL_LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->tim }}</td>
                        <td>{{ $data->sektor }}</td>
                        <td>{{ $data->mitra }}</td>
                        <td>{{ $data->order_id }}</td>
                        <td>{{ $data->dps_orderDate ? : $data->pmw_orderDate }}</td>
                        <td>{{ $data->status_tek ? : $status }}</td>
                        <td>{{ $data->tgl_laporan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL PRODUKTIFITAS TEKNISI BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection