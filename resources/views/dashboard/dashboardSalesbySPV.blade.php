@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      padding: 5px;
      text-align: center;
      vertical-align: middle;
    }
    td {
      padding :5px;
      border-color:black;
    }
    .UP4 {
      background-color : #2ecc71;
    }
    .okay {
      background-color : #636e72;
    }
    .red {
      background-color : #ff7675;
    }
    a:link{
      color : #fff !important;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <center>
      <h3>{{ $title }}</h3>
      Periode {{ $date }}
      SPV {{ $spv }}
    </center>
  </div>

  <div class="panel panel-primary" id="produktifitas">
  <div class="list-group">
	    <div class="table-responsive" id="produktifitas">
        <table class="table table-striped table-bordered" >
          <tr>
            <th rowspan="2">RANK</th>
            <th rowspan="2">ID SPV</th>
            <th rowspan="2">NAMA</th>
            <th rowspan="2">ORDER</th>
            <th rowspan="2">UP</th>
            <th rowspan="2">%</th>
            <th colspan="{{ count($get_psb_laporan_status)+1 }}">KENDALA</th>
          </tr>
          <tr>
            <th>KENDALA</th>
            <?php   $total = array(); ?>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php
            $entiti = "jumlah_$psb_laporan_status->laporan_status_id";
            $total[$entiti] = 0;
            ?>
            <th>{{ $psb_laporan_status->laporan_status }}</th>
            @endforeach
          </tr>
          <?php
	          $total_order = 0;
            $total_order_kendala = 0;
            $total_order_up = 0;

	        ?>
          @foreach ($query as $num => $result)
          <?php
	          $total_order += $result->jumlah;
            $total_order_kendala += $result->KENDALA;
            $total_order_up += $result->UP;
            $percent = ($result->UP/$result->jumlah)*100;
	        ?>
          <tr>
          	<td>{{ ++$num }}</td>
          	<td><a class="label label-success" href="/dashboardSales/{{ $date }}/{{ $result->ID_SPV }}">{{ $result->ID_SPV }}</a></td>
            <td>{{ $result->NAMA_SPV }}</td>
          	<td align="center"><span class="badge UP4"><a href="#">{{ $result->TOTAL_ORDER }}</a></span></td>
            <td align="center"><span class="badge UP4"><a href="#">{{ $result->UP }}</a></span></td>
            <td align="center">{{ round($percent,2) }}</td>
            <td align="center"><span class="badge UP4"><a href="/dashboardSalesList/all/{{ $date }}/{{$result->ID_SPV}}/all">{{ $result->KENDALA }}</a></span></td>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php
              $entiti = "jumlah_$psb_laporan_status->laporan_status_id";
              $total[$entiti] += $result->$entiti;
            ?>
            @if ($result->$entiti>0)
            <td align="center"><span class="badge UP4"><a href="/dashboardSalesList/{{ $psb_laporan_status->laporan_status }}/{{ $date }}/{{$result->ID_SPV}}/all">{{ $result->$entiti }}</a></span></td>
            @else
            <td align="center ">{{ $result->$entiti }}</td>
            @endif
            @endforeach

          </tr>
          @endforeach
          <?php
            $total_percent = ($total_order_up/$total_order)*100;
          ?>
          <tr>
          	<td colspan="3">TOTAL</td>
          	<td align=center><span class="badge UP4"><a href="#">{{ $total_order }}</a></span></td>
            <td align=center><span class="badge UP4"><a href="#">{{ $total_order_up }}</a></span></td>
            <td align=center>{{ round($total_percent,2) }}</td>
            <td align=center><span class="badge UP4"><a href="/dashboardSalesList/all/{{ $date }}/{{ $spv }}/all">{{ $total_order_kendala }}</a></span></td>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php $entiti = "jumlah_$psb_laporan_status->laporan_status_id"; ?>
            @if ($total[$entiti]>0)
            <td align=center><span class="badge UP4"><a href="/dashboardSalesList/{{ $psb_laporan_status->laporan_status }}/{{ $date }}/{{ $spv }}/all">{{ $total[$entiti] }}</a></span></td>
            @else
            <td align=center>{{ $total[$entiti] }}</td>
            @endif
            @endforeach
          </tr>
        </table>
	    </div>
  </div>
  </div>
@endsection
