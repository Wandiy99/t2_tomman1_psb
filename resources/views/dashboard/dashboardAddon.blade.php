@extends('layout')
@section('content')
<style>
    td {
        font-weight: 500;
    }
    .white-link {
        color: white !important;
    }
    .bg-blue {
        background-color: #0073b7;
        color: white !important;
    }
    .bg-green {
        background-color: #2ecc71;
        color: white !important;
    }
    .bg-orange {
        background-color: #e67e22;
        color: white !important;
    }
    .bg-red {
        background-color: #e74c3c;
        color: white !important;
    }
</style>
@include('partial.alerts')

<div class="row">

<div class="col-sm-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD ADDON<br />PERIODE {{ $start }} S/D {{ $end }}</h4><br/>
        <div class="row">
            <form method="GET" class="col-md-12">
                <div class="col-md-4">
                    <label for="group">SOURCE</label>
                    <select class="form-control" data-placeholder="- Chooice a Source -" tabindex="1" name="source">
                        @if($source == 'STARCLICKNCX')
                        <option value="STARCLICKNCX">STARCLICK NCX</option>
                        <option value="MYIRMANUAL">MYIR MANUAL</option>
                        @elseif ($source == 'MYIRMANUAL')
                        <option value="MYIRMANUAL">MYIR MANUAL</option>
                        <option value="STARCLICKNCX">STARCLICK NCX</option>
                        @endif
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="date">START DATE</label>
                    <div class="input-group">
                    <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="startDate" value="{{ $start ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="date">END DATE</label>
                    <div class="input-group">
                    <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="endDate" value="{{ $end ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="search">&nbsp;</label>
                    <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">1P - 2P PI</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_1p2p as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=1p2p&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>

<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">2P - 3P PI</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_2p3p as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=2p3p&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>

<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">UPGRADE</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_upgrade as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=upgrade&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>

@if ($source == "MYIRMANUAL")
<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">PDA</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_pda as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=pda&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>
@endif

<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">MINIPACK</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_minipack as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=minipack&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>

<div class="col-sm-6">
  <table class="table table-bordered white-box">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">DATEL</th>
                <th class="text-center align-middle bg-blue" colspan="6">OTT</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-green"><= 1 Hari</th>
                <th class="text-center align-middle bg-orange">1 - 2 Hari</th>
                <th class="text-center align-middle bg-orange">2 - 3 Hari</th>
                <th class="text-center align-middle bg-orange">4 - 6 Hari</th>
                <th class="text-center align-middle bg-red">1 Minggu</th>
                <th class="text-center align-middle bg-blue">Total</th>
            </tr>
      </thead>
      <tbody>
          @php
              $range_und1hari = $range1_2hari = $range2_3hari = $range4_6hari = $range_1minggu = $total = $grand_total = 0;
          @endphp
          @foreach ($addon_ott as $result)
          @php
              $range_und1hari += $result->range_und1hari;
              $range1_2hari += $result->range1_2hari;
              $range2_3hari += $result->range2_3hari;
              $range4_6hari += $result->range4_6hari;
              $range_1minggu += $result->range_1minggu;
              $total = $result->range_und1hari + $result->range1_2hari + $result->range2_3hari + $result->range4_6hari + $result->range_1minggu;
              $grand_total += $total;
          @endphp
            <tr>
                <td class="text-center align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $result->range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $result->range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $result->range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $result->range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $result->range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a href="/dashboard/AddonDetail?source={{ $source }}&datel={{ $result->area ? : 'NON AREA' }}&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $total }}</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr class="bg-blue">
                <td class="text-center align-middle white-link">TOTAL</td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range_und1hari">{{ $range_und1hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range1_2hari">{{ $range1_2hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range2_3hari">{{ $range2_3hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range4_6hari">{{ $range4_6hari }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=range_1minggu">{{ $range_1minggu }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="white-link" href="/dashboard/AddonDetail?source={{ $source }}&datel=ALL&status=ott&startDate={{ $start }}&endDate={{ $end }}&waktu=ALL">{{ $grand_total }}</a>
                </td>
          </tr>
      </tfoot>
  </table>
</div>

</div>
<script>
    $(function() {
      $('.datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
    });
</script>
@endsection