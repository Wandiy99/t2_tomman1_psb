@extends('layout')

@section('content')
  @include('partial.alerts')

  <div class="panel panel-primary">
      <div class="panel-heading">Dashboard PS </div>
      <div class="panel-body">
          <table class="table table-bordered">
              <tr>
                  <th>Sektor</th>
                  <th>WO PS</th>
                  <th>WO PS QRCODE</th>
                  <th>%</th>
              </tr>
              <?php
              $totalJumlahPs = 0;
              $totalJumlahPSQr = 0;
            ?>
              @foreach($data as $dt)
              <?php
              $totalJumlahPs += $dt->jumlahPs;
              $totalJumlahPSQr += $dt->jumlahPSQr;
              ?>
                  <tr>
                      <td>{{ $dt->title }}</td>
                      <td><a href="/dashboard/wo-ps/qrcode/{{ $tgl }}/{{ $dt->chat_id }}/0">{{ $dt->jumlahPs }}</a></td>
                      <td><a href="/dashboard/wo-ps/qrcode/{{ $tgl }}/{{ $dt->chat_id }}/1"">{{ $dt->jumlahPSQr }}</a></td>
                      <td>{{ @number_format(($dt->jumlahPSQr / $dt->jumlahPs) * 100,2) }} % </td>
                  </tr>
              @endforeach
              <tr>
              <th colspan="">Total</th> 
              <th><a href="/dashboard/wo-ps/qrcode/{{ $tgl }}/ALL/0">{{ $totalJumlahPs }}</a></th>
              <th><a href="/dashboard/wo-ps/qrcode/{{ $tgl }}/ALL/1">{{ $totalJumlahPSQr }}</a></th>
              </tr>
            </tr>
          </table>
      </div>

  </div>

@endsection
