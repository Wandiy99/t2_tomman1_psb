@extends('layout')

@section('content')
@include('partial.alerts')
<style>
.label {
font-size: 12px;
}
th {
border-color: #34495e;
background-color: #7f8c8d;
color : #ecf0f1;
text-align: center;
vertical-align: middle!important;
}
td {
vertical-align: middle!important;
}

.warna1{
background-color: #9abcf4;
}

.warna2{
background-color: #e0962f;
}

.warna3{
background-color: #3ec156;
}

.warna4{
background-color: #fca4a4;
}

.warna5{
background-color: #f2de5e ;
}

.warna6{
background-color: #e2410b;
}

.warna7{
background-color: #309bff;
}

.warna8{
background-color: #483D8B;
}

.warna9{
background-color: #FFE4B5;
}

.text2{
color: black !important;
}

.text1{
color: white !important;
}
.text1:link{
color: white !important;
}

.text1:visited{
color: white !important;
}


.link:link{
color: white;
}

.link:visited{
color: white;
}
</style>
<div class="row">
  <div class="col-sm-12">
    <div class="white-box">
      SEKTOR :
      <select class="" name="">
        <option value="ALL">ALL</option>
        @foreach ($get_sektor_prov as $sektor_prov)
        <option value="{{ $sektor_prov->sektor_prov }}">{{ $sektor_prov->sektor_prov }}</option>
        @endforeach
      </select>
      <input type="submit" name="submit" value="Cari">
    </div>
  </div>
  <div class="col-sm-6">
    <div class="white-box">
      <h3 class="box-title">PROGRESS SEKTOR PI PAGI COMPARIN</h3>
      <div class="table-responsive">
        <table class="table table-striped table-bordered dataTable" border="1">
          <tr>
            <th class="warna7 text1" rowspan="2">NO</th>
            <th class="warna7 text1" rowspan="2">SEKTOR</th>
            <th class="warna7 text1" rowspan="2">PI PAGI</th>
            <th class="warna7 text1" colspan="2">PROGRESS</th>
            <th class="warna7 text1" rowspan="2">SISA</th>
            <th class="warna7 text1" rowspan="2">% ACH</th>
          </tr>
          <tr>
            <th class="warna7 text1">LAINNYA</th>
            <th class="warna7 text1">COMPLETED</th>
          </tr>
          <?php
            $total_pi_pagi = 0;
            $total_pi_pagi_progress = 0;
            $total_pi_pagi_completed = 0;
          ?>
          @foreach ($query as $num => $result)
          <?php
          $ach = $result->pi_pagi_completed+$result->pi_pagi_progress;
          $ach_percent = $ach/$result->jumlah_pi_pagi*100;
          $sisa = $result->jumlah_pi_pagi - $ach;
          ?>
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $result->sektor_prov }}</td>
            <td><a href="/comparin/PI_PAGI/ALL/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $result->jumlah_pi_pagi }}</a></td>
            <td><a href="/comparin/PI_PAGI/LAINNYA/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $result->pi_pagi_progress }}</a></td>
            <td><a href="/comparin/PI_PAGI/COMPLETED/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $result->pi_pagi_completed }}</a></td>
            <td><a href="/comparin/PI_PAGI/SISA/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $sisa }}</a></td>
            <td>{{ round($ach_percent,2) }}%</td>
          </tr>
          <?php
            $total_pi_pagi += $result->jumlah_pi_pagi;
            $total_pi_pagi_progress += $result->pi_pagi_progress;
            $total_pi_pagi_completed += $result->pi_pagi_completed;
          ?>
          @endforeach
          <?php
          $total_ach = $total_pi_pagi_progress+$total_pi_pagi_completed;
          $total_ach_percent = $total_ach/$total_pi_pagi*100;
          $total_sisa = $total_pi_pagi-$total_ach;
          ?>
          <tr>
            <td colspan="2">Total</td>
            <td><a href="/comparin/PI_PAGI/ALL/ALL/{{ $tgl }}">{{ $total_pi_pagi }}</a></td>
            <td><a href="/comparin/PI_PAGI/LAINNYA/ALL/{{ $tgl }}">{{ $total_pi_pagi_progress }}</a></td>
            <td><a href="/comparin/PI_PAGI/COMPLETED/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $total_pi_pagi_completed }}</a></td>
            <td><a href="/comparin/PI_PAGI/SISA/{{ $result->sektor_prov }}/{{ $tgl }}">{{ $total_sisa }}</a></td>
            <td>{{ round($total_ach_percent,2) }}%</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="white-box">
      <h3 class="box-title">SISA ORDER</h3>
        <div class="table-responsive">
            <table class="table table-striped table-bordered ">
            <tr>
              <th>NO</th>
              <th>STATUS</th>
              <th>JML</th>
            </tr>
            <?php
              $total_status = 0;
            ?>
            @foreach ($pi_pagi_sisa as $num => $result)
            <tr>
              <td align="center">{{ ++$num }}</td>
              <td align="left">{{ $result->laporan_status ? : "NEED PROGRESS" }}</td>
              <td align="center">{{ $result->jumlah }}</td>
            </tr>
            <?php
              $total_status += $result->jumlah;
            ?>
            @endforeach
            <tr>
              <th colspan="2">TOTAL</th>
              <th>{{ $total_status }}</th>
            </tr>
          </table>
        </div>

      </div>

  </div>
</div>
@endsection
