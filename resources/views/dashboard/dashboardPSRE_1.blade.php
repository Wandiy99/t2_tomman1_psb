@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
    }
    td {
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
    }
    .warna1{
      background-color: #3498DB;
    }
    .warna2{
      background-color: #85C1E9;
    }
    .warna3{
      background-color: #D6EAF8;
    }
    .colorArea{
      background-color: #F4D03F;
    }
    .colorArea2{
      background-color: #FCF3CF;
    }
    .colorArea3{
      background-color: #F7DC6F;
    }
    .colorComparin{
      background-color: #48C9B0;
    }
    .colorComparin2{
      background-color: #D1F2EB;
    }
    .colorComparin3{
      background-color: #76D7C4;
    }
    .text2{
      color: black !important;
    }
    .text1{
      color: white !important;
    }
    .text1:link{
      color: white !important;
    }
    .text1:visited{
      color: white !important;
    }
    th a:link{
      color: white;
    }
    th a:visited{
      color: white;
    }
    .overload{
      background-color: red;
      color: white;
    }
    .merahx{
      background-color: #C0392B;
      color: white !important;
    }
    .hijaux{
      background-color:#1ABC9C;
      color: white !important;
    }
</style>
<div class="row">
  <div class="col-sm-12">
  <div class="col-sm-12 table-responsive">
    <br />
    <h3 style="font-weight: bold; text-align: center;">PS / RE [ Periode {{ $periode }} ]</h3>
    <p class="text-muted; text-align: center;">Sumber Data : Selfi K-PRO<br />Last Updated {{ $log_psre->LAST_SYNC }} WITA</p>
        <table class="table table-striped table-bordered dataTable">
          <tr>
              <th class="warna1 text1 align-middle" rowspan=2>RANK</th>
            <th class="warna1 text1 align-middle" rowspan="2">SEKTOR</th>
            <th class="warna1 text1" colspan="6">STATUS</th>
            <th class="warna1 text1 align-middle" rowspan="2">GRAND TOTAL</th>
            <th class="warna1 text1" colspan="7">ACH %</th>
          </tr>
          <tr>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="text1 align-middle" style="background-color: #C0392B">CANCEL</th>
            <th class="warna1 text1 align-middle">OGP</th>
            <th class="text1 align-middle">PRA PI</th>
            <th class="warna1 text1 align-middle" style="background-color: #1ABC9C">PS/RE</th>
            <th class="text1 align-middle" style="background-color: #E67E22">REVOKE</th>
            <th class="text1 align-middle" style="background-color: #8E44AD">UNSC</th>
            <th class="warna1 text1 align-middle">PS/WO</th>
          </tr>
          @php
              $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
          @endphp
          @foreach ($psre as $num => $result)
          @php
            $total_cancel += $result->cancel;
            $total_prapi += $result->prapi;
            $total_ogp += $result->ogp;
            $total_ps += $result->ps;
            $total_revoke += $result->revoke_comp;
            $total_unsc += $result->unsc;
            $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
            $round_cancel = @round($result->cancel / $jumlah * 100,2);
            $round_ogp = @round($result->ogp / $jumlah * 100,2);
            $round_ps = @round($result->ps / $jumlah * 100,2);
            $grand_total += $jumlah;
          @endphp
          <tr>
              <td>{{ ++$num }}</td>
            <td>{{ $result->sektor_prov ? : 'NONE SEKTOR' }}</td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/CANCEL" target="_blank">{{ $result->cancel }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/OGP" target="_blank">{{ $result->ogp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/PRA_PI" target="_blank">{{ $result->prapi }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/PS" target="_blank">{{ $result->ps }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/REVOKE" target="_blank">{{ $result->revoke_comp }}</a></td>
            <td><a class="text2" href="/dashboardPSRE/{{ $result->sektor_prov ? : 'NONE SEKTOR' }}/UNSC" target="_blank">{{ $result->unsc }}</a></td>
            <td>{{ $jumlah }}</td>
            <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
            <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
            <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
            <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
            <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
            <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
            <td {{ $bg_ps }}>{{ $round_ps }} %</td>
            <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
            <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
          </tr>
          @endforeach
          <tr>
            <th class="warna2 text2" colspan=2>TOTAL</th>
            <th style="background-color: #D98880"><a class="text2" href="/dashboardPSRE/ALL/CANCEL" target="_blank">{{ $total_cancel }}</a></th>
            <th class="warna2"><a class="text2" href="/dashboardPSRE/ALL/OGP" target="_blank">{{ $total_ogp }}</a></th>
            <th><a class="text2" href="/dashboardPSRE/ALL/PRA_PI" target="_blank">{{ $total_prapi }}</a></th>
            <th class="warna2" style="background-color: #A3E4D7"><a class="text2" href="/dashboardPSRE/ALL/PS" target="_blank">{{ $total_ps }}</a></th>
            <th class="text2" style="background-color: #F5CBA7"><a class="text2" href="/dashboardPSRE/ALL/REVOKE" target="_blank">{{ $total_revoke }}</a></th>
            <th class="text2" style="background-color: #BB8FCE"><a class="text2" href="/dashboardPSRE/ALL/UNSC" target="_blank">{{ $total_unsc }}</a></th>
            <th class="warna2 text2">{{ $grand_total }}</th>

            <th class="text2" style="background-color: #D98880">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
            <th class="warna2 text2">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
            <th class="text2">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
            <th class="warna2 text2" style="background-color: #A3E4D7">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
            <th class="text2" style="background-color: #F5CBA7">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
            <th class="text2" style="background-color: #BB8FCE">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
            <th class="warna2 text2">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
          </tr>
        </table>
        <br />
  </div>
</div>
</div>
@endsection