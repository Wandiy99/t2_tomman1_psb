@extends('layout')
@section('content')
  @include('partial.alerts')
<style media="screen">
  .navbar-brand {
    font-size: 14px;
    font-weight: bold;
  }
 
  .redxxx {
    color : #e74c3c;
  }
  </style>
  <center>
    <h3><b>MONITORING NOSSA BY <span class="redxxx">TOM</span>MAN</a><br />PERIODE {{ $date }}</b></h3>
  </center>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          DASHBOARD
        </div>
        <div class="panel-body">
          <table class="table table-stripped">
            <tr>
              <th>RANK</th>
              <th>SEKTOR</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $total = 0;
            ?>
            @foreach ($getMonNossa as $rank => $result_nossa)
            <?php
              $total += $result_nossa->jumlah;
            ?>
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $result_nossa->sektor }}</td>
              <td>{{ $result_nossa->jumlah }}</td>
            </tr>
            @endforeach
            <tr>
              <th colspan="2">TOTAL</th>
              <th><a href="/dashboard/assurance/monitoring-nossa/total/{{ $date }}">{{ $total }}</a></th>
            </tr>
          </table>
        </div>
      </div>
    </div>
 </div>
@endsection