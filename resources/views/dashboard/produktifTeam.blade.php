@extends('layout')
@section('content')
<style>
    th, td {
    text-align: center;
    vertical-align: middle;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@include('partial.alerts')

<div class="row">

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">NEW DASHBOARD<br />PRODUKTIFITAS TEKNISI PROVISIONING<br />PERIODE {{ $start }} S/D {{ $end }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <div class="col-md-3">
                        <label for="input-type-showdata">Data</label>
                        <select class="form-control select2" id = "input-type-showdata" name="show_data">
                            <option value="" selected disabled>Pilih Data</option>
                            @foreach ($get_show_data as $datax)
                            <option data-subtext="description 1" value="{{ @$datax->id }}" <?php if ($datax->id == $show_data) { echo "Selected"; } else { echo ""; } ?>>{{ $datax->text }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label for="input-type-area">Area</label>
                        <select class="form-control select2" id = "input-type-area" name="area">
                          <option disabled>Pilih Area</option>
                          <option value="ALL">ALL</option>
                        </select>
                    </div>
                    
                    <div class="col-md-2">
                        <label>Tanggal Awal</label>
                        <input type="date" name="startDate" id="startDate" class="form-control" value="{{ $start }}">
                    </div>

                    <div class="col-md-2">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="endDate" id="endDate" class="form-control" value="{{ $end }}">
                    </div>

                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 table-responsive white-box">
    <table class="display nowrap" cellspacing="0" width="100%" id="table_data">
        <thead>
              <tr>
                  <th class="text-center align-middle" rowspan="2">NO</th>
                  <th class="text-center align-middle" rowspan="2">TIM</th>
                  <th class="text-center align-middle" rowspan="2">AREA</th>
                  <th class="text-center align-middle" rowspan="2">JML_ORDER</th>
                  <th class="text-center align-middle" colspan="6">STATUS_TOMMAN</th>
                  <th class="text-center align-middle" rowspan="2">ACH</th>
                  <th class="text-center align-middle" rowspan="2">AVG PACE</th>
              </tr>
              <tr>
                <th class="text-center align-middle">ANTRIAN</th>
                <th class="text-center align-middle">OGP</th>
                <th class="text-center align-middle">HR</th>
                <th class="text-center align-middle">KT</th>
                <th class="text-center align-middle">KP</th>
                <th class="text-center align-middle">UP</th>
              </tr>
        </thead>
        <tbody>
            @php
                $jml_order = $total_order = $total_antrian = $total_ogp = $total_hr = $total_kt = $total_kp = $total_up = $avg_pace = $total_rows = 0;
                $numb = 1;
                $total_rows = count($get_data);
            @endphp
            @foreach ($get_data as $key => $value)
            @php
                $jml_order = $value->jml_antrian + $value->jml_ogp + $value->jml_hr + $value->jml_kt + $value->jml_kp + $value->jml_up;
                $total_order += $jml_order;
                $total_antrian += $value->jml_antrian;
                $total_ogp += $value->jml_ogp;
                $total_hr += $value->jml_hr;
                $total_kt += $value->jml_kt;
                $total_kp += $value->jml_kp;
                $total_up += $value->jml_up;
            @endphp
              <tr>
                <td class="text-center align-middle">{{ $numb++ }}</td>
                <td class="text-center align-middle">{{ $value->tim ? : 'NON TEAM' }}</td>
                <td class="text-center align-middle">
                    @if ($show_data == 'SEKTOR')
                        {{ $value->sektor ? : 'NON AREA' }}
                    @elseif ($show_data == 'MITRA')
                        {{ $value->mitra ? : 'NON AREA' }}
                    @endif
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=JML_ORDER">{{ $jml_order }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=ANTRIAN">{{ $value->jml_antrian }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=OGP">{{ $value->jml_ogp }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=HR">{{ $value->jml_hr }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=KT">{{ $value->jml_kt }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=KP">{{ $value->jml_kp }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/produktifitasTeam/detail?show_data={{ $show_data }}&area={{ $area }}&team={{ $value->tim ? : 'NON TEAM' }}&startDate={{ $start }}&endDate={{ $end }}&status=UP">{{ $value->jml_up }}</a>
                </td>
                <td class="text-center align-middle">
                    @if (is_nan(@round($value->jml_up / $jml_order * 100,2)) == true)
                        0 %
                    @else
                        {{ @round($value->jml_up / $jml_order * 100,2) }} %
                    @endif
                </td>
                <td class="text-center align-middle">
                    @php
                        $now = strtotime($end);
                        $your_date = strtotime($start);
                        $datediff = $now - $your_date;
                        $total_days = round($datediff / (60 * 60 * 24));

                        $avg_pace = $value->jml_up / ($total_days + 1);
                    @endphp
                    {{ round($avg_pace, 2) }}
                </td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td class="text-center align-middle" colspan="3">TOTAL</td>
                <td class="text-center align-middle">{{ $total_order }}</td>
                <td class="text-center align-middle">{{ $total_antrian }}</td>
                <td class="text-center align-middle">{{ $total_ogp }}</td>
                <td class="text-center align-middle">{{ $total_hr }}</td>
                <td class="text-center align-middle">{{ $total_kt }}</td>
                <td class="text-center align-middle">{{ $total_kp }}</td>
                <td class="text-center align-middle">{{ $total_up }}</td>
                <td class="text-center align-middle">
                    @if (is_nan(@round($total_up / $total_order * 100,2)) == true)
                        0 %
                    @else
                        {{ @round($total_up / $total_order * 100,2) }} %
                    @endif
                </td>
                <td class="text-center align-middle">
                    @php
                        $now = strtotime($end);
                        $your_date = strtotime($start);
                        $datediff = $now - $your_date;
                        $total_days = round($datediff / (60 * 60 * 24));

                        $avg_pace =  $total_up / ($total_days + 1) / $total_rows;
                    @endphp
                    {{ round($avg_pace, 2) }}
                </td>
            </tr>
        </tfoot>
    </table>
</div>

</div>

<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {

        $('.select2').select2();
    
        $('#table_data').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });

        var day = {
                format: 'yyyy-mm-dd',
                viewMode: 0,
                minViewMode: 0
            };

        $('#startDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        $('#endDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        var data = <?= json_encode($get_area) ?>;
            var select2Options = function() {
            return {
            data: data,
            placeholder: 'Input Area',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
                return  data.text;
            }
            }
        }
        $('#input-type-area').select2(select2Options());
    });
</script>
@endsection