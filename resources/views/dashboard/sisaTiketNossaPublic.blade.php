@extends('public_layout_2')
@section('content')
<style>
td {
    padding : 5px;
    border:1px solid #ecf0f1;
    font-size: 14px;
    padding : 3px !important;
}
th {
    text-align: center;
    vertical-align: middle;
    background-color: #0073b7;
    padding : 5px;
    color : #FFF;
    border:1px solid #ecf0f1;
}
.center {
      text-align: center;
}
</style>
<br />
<div class="row">
  <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <b>SISA SALDO TIKET {{ date('Y-m-d H:i:s') }}</b>
        </div>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="vertical-align: middle" rowspan="2">IOAN</th>
              <th style="vertical-align: middle; background-color: #2ECC71" class="align-middle" rowspan="2">WNOK</th>
              <th style="vertical-align: middle" colspan="6">DURASI</th>
              <th style="background-color: #EC7063 !important; vertical-align: middle" rowspan="2">GAMAS</th>
              <th style="vertical-align: middle" rowspan="2">TOTAL</th>
            </tr>
            <tr>
              <th style="background-color: #58D68D">< 1 JAM</th>
              <th style="background-color: #58D68D">< 2 JAM</th>
              <th style="background-color: #58D68D">< 3 JAM</th>
              <th style="background-color: #F4D03F">< 12 JAM</th>
              <th style="background-color: #F5B041">> 12 JAM</th>
              <th style="background-color: #EC7063">> 1 HARI</th>
            </tr>
            @php
              $jumlah = $min1jam = $min2jam = $min3jam = $min12jam = $max12jam = $max1hari = $gamas = $wnok = $total = 0;
            @endphp
            @foreach ($getSisaTiket as $sisaTiket)
            @php
              $wnok += $sisaTiket->WNOK;
              $jumlah = ($sisaTiket->DIBAWAH1 + $sisaTiket->DIBAWAH2 + $sisaTiket->DIBAWAH3 + $sisaTiket->DIBAWAH12 + $sisaTiket->DIATAS12 + $sisaTiket->LEBIH1HARI + $sisaTiket->GAMAS);
              $min1jam += $sisaTiket->DIBAWAH1;
              $min2jam += $sisaTiket->DIBAWAH2;
              $min3jam += $sisaTiket->DIBAWAH3;
              $min12jam += $sisaTiket->DIBAWAH12;
              $max12jam += $sisaTiket->DIATAS12;
              $max1hari += $sisaTiket->LEBIH1HARI;
              $gamas += $sisaTiket->GAMAS;
              $total += $jumlah;
            @endphp
            <tr>
              <td class="center">{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}</td>
              <td class="center">{{ $sisaTiket->WNOK }}</td>
              <td class="center">{{ $sisaTiket->DIBAWAH1 }}</td>
              <td class="center">{{ $sisaTiket->DIBAWAH2 }}</td>
              <td class="center">{{ $sisaTiket->DIBAWAH3 }}</td>
              <td class="center">{{ $sisaTiket->DIBAWAH12 }}</td>
              <td class="center">{{ $sisaTiket->DIATAS12 }}</td>
              <td class="center">{{ $sisaTiket->LEBIH1HARI }}</td>
              <td class="center">{{ $sisaTiket->GAMAS }}</td>
              <td class="center">{{ $sisaTiket->JUMLAH }}</td>
            </tr>
            @endforeach
            <tr style="background-color:#fbd12c;">
              <td class="center"><b>TOTAL</b></td>
              <td class="center"><b>{{ $wnok }}</b></td>
              <td class="center"><b>{{ $min1jam }}</b></td>
              <td class="center"><b>{{ $min2jam }}</b></td>
              <td class="center"><b>{{ $min3jam }}</b></td>
              <td class="center"><b>{{ $min12jam }}</b></td>
              <td class="center"><b>{{ $max12jam }}</b></td>
              <td class="center"><b>{{ $max1hari }}</b></td>
              <td class="center"><b>{{ $gamas }}</b></td>
              <td class="center"><b>{{ $total }}</b></td>
            </tr>
          </table>
        </div>
      </div>
  </div>
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <b>TTR MANUAL MANJA DISPATCH {{ date('Y-m-d H:i:s') }}</b>
      </div>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th style="vertical-align: middle" rowspan="2">SEKTOR</th>
            <th style="vertical-align: middle" colspan="4">DURASI</th>
            <th style="vertical-align: middle" rowspan="2">TOTAL</th>
          </tr>
          <tr>
            <th style="background-color: #58D68D">< 3 JAM</th>
            <th style="background-color: #F4D03F">< 12 JAM</th>
            <th style="background-color: #F5B041">> 12 JAM</th>
            <th style="background-color: #EC7063">> 1 HARI</th>
          </tr>
          @php
            $jumlah = 0;
            $min3jam = 0;
            $min12jam = 0;
            $max12jam = 0;
            $max1hari = 0;
            $total = 0;
          @endphp
          @foreach ($getData as $data)
          @php
            $jumlah = ($data->DIBAWAH3 + $data->DIBAWAH12 + $data->DIATAS12 + $data->LEBIH1HARI);
            $min3jam += $data->DIBAWAH3;
            $min12jam += $data->DIBAWAH12;
            $max12jam += $data->DIATAS12;
            $max1hari += $data->LEBIH1HARI;
            $total += $jumlah;
          @endphp
          <tr>
            <td class="center">{{ $data->sektor ? : 'UNDISPATCH' }}</td>
            <td class="center">{{ $data->DIBAWAH3 }}</td>
            <td class="center">{{ $data->DIBAWAH12 }}</td>
            <td class="center">{{ $data->DIATAS12 }}</td>
            <td class="center">{{ $data->LEBIH1HARI }}</td>
            <td class="center">{{ $jumlah }}</td>
          </tr>
          @endforeach
          <tr style="background-color:#fbd12c;">
            <td class="center"><b>TOTAL</b></td>
            <td class="center"><b>{{ $min3jam }}</b></td>
            <td class="center"><b>{{ $min12jam }}</b></td>
            <td class="center"><b>{{ $max12jam }}</b></td>
            <td class="center"><b>{{ $max1hari }}</b></td>
            <td class="center"><b>{{ $total }}</b></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection