@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/newDashboardDismantling?startDate={{ $start }}&endDate={{ $end }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL DISMANTLING TIM {{ $tim }} PERIODE {{ $start }} S/D {{ $end }} STATUS {{ $status }} TYPE {{ $type }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>ID DISPATCH</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>NOPEL</th>
                        <th>NAMA</th>
                        @if ($type == 'SISA_ORDER')
                        <th>NAMA PETUGAS CTB</th>
                        <th>NAMA YANG DITEMUI</th>
                        <th>ALASAN</th>
                        <th>TANGGAL CREATE</th>
                        <th>ONT</th>
                        <th>STB</th>
                        <th>PLC</th>
                        <th>WIFIEXT</th>
                        <th>INDIBOX</th>
                        <th>INDIHOMESMART</th>
                        <th>NO BA</th>
                        <th>TIPE DAPROS</th>
                        @elseif ($type == 'COLLECTING')
                        <th>ALAMAT</th>
                        <th>STO</th>
                        <th>NO HP</th>
                        <th>DAPROS TAHUN</th>
                        <th>TIPE DAPROS</th>
                        <th>STATUS TEKNISI</th>
                        <th>CATATAN TEKNISI</th>
                        <th>TANGGAL LAPORAN</th>
                        <th>TANGGAL DISPATCH</th>
                        <th>GENERATED AT</th>
                        <th>FLAG</th>
                        <th>VENDOR</th>
                        <th>SN (TEKNISI)</th>
                        <th>SN (RTWH)</th>
                        <th>SOURCE</th>
                        <th>WAREHOUSE</th>
                        <th>COLLECTED SOURCE</th>
                        <th>TYPE RTWH</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->id_dt }}</td>
                        <td>{{ $data->tim ? : 'NON TIM' }}</td>
                        <td>{{ $data->sektor ? : '#N/A' }}</td>
                        <td>{{ $data->nopel }}</td>
                        <td>{{ $data->nama_pelanggan }}</td>
                        @if ($type == 'SISA_ORDER')
                        <td>{{ $data->namapetugasctb }}</td>
                        <td>{{ $data->nama_yg_ditemui }}</td>
                        <td>{{ $data->alasan }}</td>
                        <td>{{ $data->tanggalcreate }}</td>
                        <td>{{ $data->ont }}</td>
                        <td>{{ $data->stb }}</td>
                        <td>{{ $data->plc }}</td>
                        <td>{{ $data->wifiextender }}</td>
                        <td>{{ $data->indibox }}</td>
                        <td>{{ $data->indihomesmart }}</td>
                        <td>{{ $data->no_ba }}</td>
                        <td>{{ $data->tipe_dapros }}</td>
                        @elseif ($type == 'COLLECTING')
                        <td>{{ $data->alamat }}</td>
                        <td>{{ $data->sto }}</td>
                        <td>{{ $data->no_hp }}</td>
                        <td>{{ $data->dapros_tahun }}</td>
                        <td>{{ $data->jenis_dismantling }}</td>
                        <td>{{ $data->laporan_status }}</td>
                        <td>{{ $data->catatan_tek }}</td>
                        <td>{{ $data->tgl_laporan }}</td>
                        <td>{{ $data->tgl_dispatch }}</td>
                        <td>{{ $data->rrt_generated_at }}</td>
                        <td>{{ $data->rrt_flag ? : $data->cnd_flag }}</td>
                        <td>{{ $data->rrt_vendor ? : $data->cnd_vendor }}</td>
                        <td>{{ $data->cnd_sn }}</td>
                        <td>{{ $data->rrt_sn }}</td>
                        <td>{{ $data->rrt_source ? : '-' }}</td>
                        <td>{{ $data->rrt_warehouse ? : '-' }}  </td>
                        <td>{{ $data->rrt_collected_source ? : '-' }}</td>
                        <td>{{ $data->type_rtwh ? : '-' }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DASHBOARD NEW DISMANTLING BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection