@extends($layout)

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      padding: 5px;
      text-align: center;
      vertical-align: middle;
    }
    td {
      padding :5px;
      border-color:black;
    }
    .UP4 {
      background-color : #2ecc71;
    }
    .okay {
      background-color : #636e72;
    }
    .red {
      background-color : #ff7675;
    }
    a {
      color : #fff;
    }
  </style>
  <div style="padding-bottom: 10px;">
    <center>
      <h3>{{ $title }}</h3>
      Periode {{ $date }}
      SPV {{ $spv }}
    </center>
  </div>

  <div class="panel panel-primary" id="produktifitas">
  <div class="list-group">
	    <div class="table-responsive" id="produktifitas">
        <table class="table table-striped table-bordered" >
          <tr>
            <th>RANK</th>
            <th>SALES</th>
            <th>TOTAL<br />KENDALA</th>
            <?php   $total = array(); ?>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php
            $entiti = "jumlah_$psb_laporan_status->laporan_status_id";
            $total[$entiti] = 0;
            ?>
            <th>{{ $psb_laporan_status->laporan_status }}</th>
            @endforeach
          </tr>
          <?php
	          $total_order = 0;

	        ?>
          @foreach ($query as $num => $result)
          <?php
	          $total_order += $result->jumlah;
	        ?>
          <tr>
          	<td>{{ ++$num }}</td>
          	<td>{{ $result->SALESnya }}</td>
          	<td align="center"><span class="badge UP4"><a href="/dashboardSalesList/all/{{ $result->SALESnya }}">{{ $result->KENDALA }}</a></span></td>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php
              $entiti = "jumlah_$psb_laporan_status->laporan_status_id";
              $total[$entiti] += $result->$entiti;
            ?>
            @if ($result->$entiti>0)
            <td align="center"><span class="badge UP4"><a href="/dashboardSalesList/{{ $psb_laporan_status->laporan_status }}/{{ $date }}/{{$spv}}/{{ $result->SALESnya }}">{{ $result->$entiti }}</a></span></td>
            @else
            <td align="center ">{{ $result->$entiti }}</td>
            @endif
            @endforeach

          </tr>
          @endforeach
          <tr>
          	<td colspan="2">TOTAL</td>
          	<td align=center><span class="badge UP4"><a href="/dashboardSalesList/all/{{ $date }}/{{ $spv }}/all">{{ $total_order }}</a></span></td>
            @foreach ($get_psb_laporan_status as $psb_laporan_status)
            <?php $entiti = "jumlah_$psb_laporan_status->laporan_status_id"; ?>
            @if ($total[$entiti]>0)
            <td align=center><span class="badge UP4"><a href="/dashboardSalesList/{{ $psb_laporan_status->laporan_status }}/{{ $date }}/{{ $spv }}/all">{{ $total[$entiti] }}</a></span></td>
            @else
            <td align=center>{{ $total[$entiti] }}</td>
            @endif
            @endforeach
          </tr>
        </table>
	    </div>
  </div>
  </div>
@endsection
