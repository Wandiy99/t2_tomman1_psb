@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>LIST UNSPEC {{ $sektor }}</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">PERIODE {{ $date }}</div>
  			<div class="panel-body table-responsive" style="padding:0px !important">
  				<table class="table table-stripped">
  					<tr>
  						<th>NO</th>
              <th>TL</th>
              <th>SEKTOR</th>
              <th>TIM</th>
  						<th>INCIDENT</th>
              <th>CUSTOMER</th>
              <th>SUMMARY</th>
  						<th>ND INET</th>
              <th>STATUS ACTION</th>
  						<th>CATATAN</th>
              <th>UPDATED BY</th>
              <th>WAKTU LAPORAN</th>
  						<th>REDAMAN</th>
  					</tr>
  					@foreach ($query as $no => $result)
  					<tr>
  						<td>{{ ++$no }}</td>
  						<td>{{ $result->TL }}</td>
              <td>{{ $result->title }}</td>
              <td>{{ $result->uraian }}</td>
              <td>
                <a href="/tiket/{{ $result->dt_id }}">{{ $result->Incident }}<br />
                <a href="/grabIboosterbyIN/{{ $result->Incident }}/assurance" class="label label-success">Ukur Ibooster</a>
              </td>
  						<td>{{ $result->Customer_Name }}</td>
  						<td>{{ $result->Summary }}</td>
  						<td>{{ $result->no_internet }}</td>
              <td>{{ $result->Action }}</td>
  						<td>{{ $result->catatan }}</td>
              <td>{{ $result->modified_by }}</td>
              <td>{{ $result->modified_at }}</td>
              <td>{{ $result->redaman_iboster ? : '0' }} dbm</td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection