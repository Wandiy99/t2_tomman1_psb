@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL REPORT NEED CANCEL EVALUASI</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>SEKTOR</th>
                        <th>STO</th>
                        <th>DATEL</th>
                        <th>JENIS PSB</th>
                        <th>ORDER STATUS</th>
                        <th>ORDER DATE</th>
                        <th>STATUS TEKNISI</th>
                        <th>KOORDINAT PELANGGAN</th>
                        <th>LOKASI</th>
                        <th>LOKASI 1</th>
                        <th>LOKASI 2</th>
                        <th>TGL UPDATE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->orderId }}</td>
                        <td>{{ $data->sektor }}</td>
                        <td>{{ $data->sto }}</td>
                        <td>{{ $data->datel }}</td>
                        <td>{{ $data->jenisPsb }}</td>
                        <td>{{ $data->orderStatus }}</td>
                        <td>{{ $data->orderDate }}</td>
                        <td>{{ $data->laporan_status }}</td>
                        <td>{{ $data->kordinat_pelanggan }}</td>
                        @foreach($getPhotos as $photo)
                            @php
                                $path1 = "/upload/evidence/{$data->id_dt}";
                                $th1   = "$path1/$photo-th.jpg";
                                $path2 = "/upload2/evidence/{$data->id_dt}";
                                $th2   = "$path2/$photo-th.jpg";
                                $path3 = "/upload3/evidence/{$data->id_dt}";
                                $th3   = "$path3/$photo-th.jpg";
                                $path4 = "/upload4/evidence/{$data->id_dt}";
                                $th4   = "$path4/$photo-th.jpg";
                                $path  = null;
                                
                                if (file_exists(public_path().$th1))
                                {
                                    $path = "$path1/$photo";
                                } elseif (file_exists(public_path().$th2)) {
                                    $path = "$path2/$photo";
                                } elseif (file_exists(public_path().$th3)) {
                                    $path = "$path3/$photo";
                                } elseif (file_exists(public_path().$th4)) {
                                    $path = "$path4/$photo";
                                }

                                if ($path)
                                {
                                    $th    = "$path-th.jpg";
                                    $img   = "$path.jpg";
                                } else {
                                    $th    = null;
                                }
                            @endphp
                            <td>
                                @if(!empty($th) && file_exists(public_path().$th))
                                    <a href="{{ $img }}">
                                        <img src="{{ $th }}">
                                    </a>
                                @else
                                    <img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" />
                                @endif
                            </td>
                        @endforeach
                        <td>{{ $data->modified_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DETAIL REPORT NEED CANCEL EVALUASI BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection