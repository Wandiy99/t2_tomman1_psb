@extends('public_v2')

@section('content')
  <!-- @include('partial.alerts') -->

  <?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=Rekon Assurance Mitra".$tglNow.".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
  ?>

  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
<!--   <?php $tgl = date("Y-m"); ?>
  <a href="/dashboard/rekon/{{ $tgl }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tglNow }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable"> -->
      <table table border="1" width="100%">
        <tr align="center">
          <th rowspan="2">No.</th>
          <th rowspan="2">SM</th>
          <th rowspan="2">TL</th>
          <th rowspan="2">TEKNISI</th>
          <th colspan="5">WO / TIKET</th>
          <th colspan="2">NOMOR LAYANAN</th>
          <th colspan="2">DATA PELANGGAN</th>
          <th colspan="2">TRANSAKSI / MUTASI</th>
          <th colspan="4">DATA PERANGKAT TERPASANG</th>
          <th colspan="19">MATERIAL PENUNJANG</th>
          <th colspan="7">LAIN-LAIN</th>
        </tr>
        <tr>

          <th>NOMOR SC</th>
          <th>TGL WO</th>
          <th>TGL PASANG</th>
          <th>TGL PS</th>
          <th>STO</th>
          <th>TELEPON</th>
          <th>INET / DATIN</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>JENIS MUTASI</th>
          <th>LAYANAN</th>
          <th>NAMA MODEM</th>
          <th>TYPE / SERIES</th>
          <th>S/N or MAC ADDRESS</th>
          <th>MERK</th>
          <th>MODE GELARAN</th>
          <th>DC</th>
          <th>DW</th>
          <th>PATCHCORE</th>
          <th>UTP CAT6</th>
          <th>TIANG</th>
          <th>PULL STRAP</th>
          <th>CONDUIT</th>
          <th>TRAY CABLE</th>

          <th>AD-SC</th>
          <th>ODP-CA-16</th>
          <th>ODP-CA-8</th>
          <th>ODP-PB-16</th>
          <th>ODP-PB-8</th>
          <th>PS-1-4</th>
          <th>PS-1-8</th>
          <th>PS-1-16</th>
          <th>SOC-SUM</th>
          <th>SOC-ILS</th>

          <th>ODP</th>
          <th>KORD. PEL </th>
          <th>AREA</th>
          <th>NAMA MITRA</th>
          <th>STATUS SC</th>

          <th>STATUS TEK.</th>
          <th>CATATAN TEK.</th>
        </tr>
        @foreach ($data as $num => $result)
        <?php
          if ($result->dc_preconn>0 || $result->dc_roll>0){
            $mode_gelaran = "KU";
          } else {
            $mode_gelaran = "UTP";
          }
        ?>
        <tr>
          <td>{{ ++$num }}</td>
          <td>MUHAMMAD NOR RIFANI</td>
          @if ($result->mode==1 && $mode_gelaran == "UTP")
          <td bgcolor=red># {{ $result->TL }}</td>
          @else
          <td>{{ $result->TL }}</td>
          @endif
          <td>{{ $result->uraian }}</td>
          <td><!-- <a href="/{{ $result->id_dt}}"> -->{{ $result->orderId }}<!-- </a> --></td>
          <td>{{ $result->tanggal_dispatch }}</td>
          <td>{{ $result->modified_at }}</td>
          <td>{{ $result->orderDatePs }}</td>
          <td>{{ $result->sto ?: $result->Workzone }}</td>
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->orderAddr }} {{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->jenisPsb }}</td>
          <td>{{ $result->action }}</td>
          <td>~</td>
          <td>{{ $result->typeont }} ~ {{ $result->typestb }}</td>
          <td>{{ $result->snont }} ~ {{ $result->snstb }}</td>
          <td>~</td>
          <td>
            {{ $mode_gelaran }}
          </td>

          <td>{{ $result->dc_preconn ? : $result->dc_roll ? : "0" }}</td>
          <td>0</td>
          <td>{{ $result->patchcore }}</td>
          <td>{{ $result->utp }}</td>
          <td>{{ $result->tiang }}</td>
          <td>{{ $result->pullstrap }}</td>
          <td>{{ $result->conduit }}</td>
          <td>{{ $result->tray_cable }}</td>

          <td>{{ $result->AD_SC }}</td>
          <td>{{ $result->ODP_CA_16}} </td>
          <td>{{ $result->ODP_CA_8}} </td>
          <td>{{ $result->ODP_PB_16}} </td>
          <td>{{ $result->ODP_PB_8}} </td>
          <td>{{ $result->PS_1_4}} </td>
          <td>{{ $result->PS_1_8}} </td>
          <td>{{ $result->PS_1_16}} </td>
          <td>{{ $result->SOC_SUM}} </td>
          <td>{{ $result->SOC_ILS}} </td>

          <td>{{ strtoupper($result->nama_odp) }}</td>
          <td>{{ $result->kordinat_pelanggan }}</td>
          <td>{{ $result->area_migrasi }}</td>
          <td>{{ $result->mitra }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->laporan_status }}</td>
          <td>{{ $result->catatan }}</td>
        </tr>
        @endforeach
      </table>
<!--     </div>
    </div>

  </div>    <br />
      <br />
@endsection -->
