@extends('layout')

@section('content')
@include('partial.alerts')
<div class="row">
  <div class="col-sm-12">
    <div class="white-box">
      <h3 class="box-title">LIST {{ $type }} {{ $status }} {{ $tgl }}</h3>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th>NO</th>
            <th>SC</th>
            <th>STO</th>
            <th>STATUS</th>
            <th>KENDALA</th>
            <th>REGU</th>
            <th>SC TOMMAN</th>
          </tr>
          @foreach ($get_list as $num => $result)
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $result->ORDER_ID }}</td>
            <td>{{ $result->sto }}</td>
            <td>{{ $result->laporan_status }}</td>
            <td>{{ $result->status_kendala }}</td>
            <td>{{ $result->uraian }}</td>
            <td>{{ $result->orderStatus }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
