@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/dashboard/assurance/{{ date('Y-m-d') }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span></a>
<h4>{{ $sektor }} {{ $order }} {{ $date }}</h4>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel-body table-responsive" style="padding:0px !important">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>NO</th>
          <th>SEKTOR</th>
          <th>TIM</th>
          <th>NODE IP</th>
          <th>STO</th>
          <th>DATEK</th>
          <th>NAMA</th>
          <th>TYPE</th>
          <th>ALAMAT</th>
          <th>ONU RX</th>
          <th>TANGGAL UKUR</th>
          <th>INCIDENT</th>
          <th>SERVICE NO</th>
          <th>INCIDENT STATUS</th>
          <th>STATUS</th>
          <th>ACTION</th>
          <th>PENYEBAB</th>
          <th>PENYEBAB RETI</th>
          <th>CATATAN</th>
          <th>ONU RX DISPATCH</th>
          <th>ONU RX LAPORAN</th>
          <th>TANGGAL DISPATCH</th>
          <th>TANGGAL LAPORAN</th>
        </tr>
        @foreach($getData as $no => $data)
          <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $data->SEKTOR }}</td>
              <td>{{ $data->tim ? : '-' }}</td>
              <td>{{ $data->NODE_IP }}</td>
              <td>{{ $data->CMDF }}</td>
              <td>{{ $data->DP }}</td>
              <td>{{ $data->NAMA }}</td>
              <td>{{ $data->TYPE_PELANGGAN }}</td>
              <td>{{ $data->ALAMAT }}</td>
              <td>{{ $data->ONU_RX_POWER }}</td>
              <td>{{ $data->TANGGAL_UKUR }}</td>
              <td>{{ $data->NOMOR_TIKET }}</td>
              <td>{{ $data->Service_No }}</td>
              <td>{{ $data->STATUS_TIKET }}</td>
              <td>{{ $data->laporan_status ? : '-' }}</td>
              <td>{{ $data->action ? : '-' }}</td>
              <td>{{ $data->penyebab ? : '-' }}</td>
              <td>{{ $data->status_penyebab_reti ? : '-' }}</td>
              <td>{{ $data->catatan ? : '-' }}</td>
              <td>{{ $data->ONU_Rx ? : '0' }}</td>
              <td>{{ $data->redaman_iboster ? : '0' }}</td>
              <td>{{ $data->tgl_dispatch ? : '-' }}</td>
              <td>{{ $data->tgl_laporan ? : '-' }}</td>
          </tr>
        @endforeach       
        </table>
    </div>
</div>
</div>
@endsection