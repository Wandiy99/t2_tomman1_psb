@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
  .label {
    font-size: 12px;
  }
  th {
    border-color: #34495e;
    background-color: #7f8c8d;
    color : #ecf0f1;
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  td {
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
  }
  tr, th, td {
      text-align: center;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 1px 1px;
  }
  th a:link {
    color: white;
  }
  th a:visited {
    color: white;
  }
  .modal-lg {
    max-width: 1350px;
  }
  .bg-blue {
    background-color: #00B0F0;
  }
  .bg-green {
    background-color: #00B050;
    color: white !important;
  }
  .bg-orange {
    background-color: #FFC000;
  }
  .bg-yellow {
    background-color: #FFFF00;
    color: black;
    font-weight: bold;
  }
  .bg-red {
    background-color: #C0392B;
    color: white !important;
  }
  .bg-purple {
    background-color: #7030A0;
  }
  .bg-white {
    background-color: white;
    color: black;
  }
</style>

<div class="row">

  <div class="col-sm-12 table-responsive">

    <div class="modal fade lapor-progress" tabindex="-1" role="dialog" aria-labelledby="lapor-progress" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <form method="POST" action="/dashboard/reportProvTodaySave">
            <input type="hidden" name="date" value="{{ $date }}">
            <input type="hidden" name="report" value="progress">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="lapor-progress">REPORT PROGRESS ORDER & RESOURCE KALSEL</h4>
                    </div>
                    <div class="modal-body table-responsive">
                        <table class="table table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th class="align-middle bg-blue" rowspan="2">MITRA</th>
                                    <th class="align-middle bg-blue" rowspan="2">SEKTOR</th>
                                    <th class="align-middle bg-blue" rowspan="2">STO</th>
                                    <th class="align-middle bg-green" colspan="4">TIM</th>
                                    <th class="align-middle bg-blue" colspan="8">ORDER</th>
                                    <th class="align-middle bg-blue" rowspan="2">REAL<br />PS</th>
                                    <th class="align-middle bg-green" rowspan="2">KOMITMEN<br />PS</th>
                                </tr>
                                <tr>
                                    <th class="align-middle bg-green">PT1</th>
                                    <th class="align-middle bg-green">SABER</th>
                                    <th class="align-middle bg-green">BANTEK</th>
                                    <th class="align-middle bg-green">MO</th>
                                    <th class="align-middle bg-blue">PI</th>
                                    <th class="align-middle bg-blue">RISMA</th>
                                    <th class="align-middle bg-blue">MO</th>
                                    <th class="align-middle bg-blue">PDA</th>
                                    <th class="align-middle bg-blue">HSI</th>
                                    <th class="align-middle bg-blue">WMS</th>
                                    <th class="align-middle bg-orange">PROG</th>
                                    <th class="align-middle bg-orange">KENDALA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="bg-white">STM</th>
                                    <th class="bg-white">BJM</th>
                                    <td>BJM</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_tim_pt1" value="{{ @$bjm->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_tim_saber" value="{{ @$bjm->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_tim_bantek" value="{{ @$bjm->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_tim_mo" value="{{ @$bjm->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_pi" value="{{ @$bjm->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_risma" value="{{ @$bjm->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_mo" value="{{ @$bjm->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_pda" value="{{ @$bjm->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_hsi" value="{{ @$bjm->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_wms" value="{{ @$bjm->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_prog" value="{{ @$bjm->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_order_kendala" value="{{ @$bjm->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_real_ps" value="{{ @$bjm->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bjm_komitmen_ps" value="{{ @$bjm->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                    
                                
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">UPATEK</th>
                                    <th class="bg-white">KYG</th>
                                    <td>KYG</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_tim_pt1" value="{{ @$kyg->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_tim_saber" value="{{ @$kyg->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_tim_bantek" value="{{ @$kyg->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_tim_mo" value="{{ @$kyg->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_pi" value="{{ @$kyg->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_risma" value="{{ @$kyg->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_mo" value="{{ @$kyg->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_pda" value="{{ @$kyg->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_hsi" value="{{ @$kyg->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_wms" value="{{ @$kyg->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_prog" value="{{ @$kyg->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_order_kendala" value="{{ @$kyg->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_real_ps" value="{{ @$kyg->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kyg_komitmen_ps" value="{{ @$kyg->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="2">ULI - GMB</th>
                                    <td class="bg-white">ULI</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_tim_pt1" value="{{ @$uli->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_tim_saber" value="{{ @$uli->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_tim_bantek" value="{{ @$uli->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_tim_mo" value="{{ @$uli->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_pi" value="{{ @$uli->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_risma" value="{{ @$uli->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_mo" value="{{ @$uli->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_pda" value="{{ @$uli->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_hsi" value="{{ @$uli->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_wms" value="{{ @$uli->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_prog" value="{{ @$uli->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_order_kendala" value="{{ @$uli->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="uli_real_ps" value="{{ @$uli->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="2">
                                        <input type="number" style="text-align: center; color: black" name="uligmb_komitmen_ps" value="{{ @$uligmb->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>GMB</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_tim_pt1" value="{{ @$gmb->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_tim_saber" value="{{ @$gmb->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_tim_bantek" value="{{ @$gmb->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_tim_mo" value="{{ @$gmb->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_pi" value="{{ @$gmb->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_risma" value="{{ @$gmb->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_mo" value="{{ @$gmb->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_pda" value="{{ @$gmb->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_hsi" value="{{ @$gmb->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_wms" value="{{ @$gmb->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_prog" value="{{ @$gmb->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_order_kendala" value="{{ @$gmb->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="gmb_real_ps" value="{{ @$gmb->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="4">TBN</th>
                                    <th class="bg-white">BBR</th>
                                    <td>BBR</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_tim_pt1" value="{{ @$bbr->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_tim_saber" value="{{ @$bbr->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_tim_bantek" value="{{ @$bbr->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_tim_mo" value="{{ @$bbr->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_pi" value="{{ @$bbr->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_risma" value="{{ @$bbr->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_mo" value="{{ @$bbr->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_pda" value="{{ @$bbr->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_hsi" value="{{ @$bbr->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_wms" value="{{ @$bbr->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_prog" value="{{ @$bbr->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_order_kendala" value="{{ @$bbr->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_real_ps" value="{{ @$bbr->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bbr_komitmen_ps" value="{{ @$bbr->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">PLE</th>
                                    <td>PLE</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_tim_pt1" value="{{ @$ple->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_tim_saber" value="{{ @$ple->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_tim_bantek" value="{{ @$ple->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_tim_mo" value="{{ @$ple->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_pi" value="{{ @$ple->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_risma" value="{{ @$ple->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_mo" value="{{ @$ple->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_pda" value="{{ @$ple->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_hsi" value="{{ @$ple->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_wms" value="{{ @$ple->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_prog" value="{{ @$ple->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_order_kendala" value="{{ @$ple->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ple_real_ps" value="{{ @$ple->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="3">
                                        <input type="number" style="text-align: center; color: black" name="plebtbtki_komitmen_ps" value="{{ @$plebtbtki->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>BTB</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_tim_pt1" value="{{ @$btb->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_tim_saber" value="{{ @$btb->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_tim_bantek" value="{{ @$btb->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_tim_mo" value="{{ @$btb->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_pi" value="{{ @$btb->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_risma" value="{{ @$btb->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_mo" value="{{ @$btb->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_pda" value="{{ @$btb->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_hsi" value="{{ @$btb->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_wms" value="{{ @$btb->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_prog" value="{{ @$btb->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_order_kendala" value="{{ @$btb->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="btb_real_ps" value="{{ @$btb->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>TKI</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_tim_pt1" value="{{ @$tki->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_tim_saber" value="{{ @$tki->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_tim_bantek" value="{{ @$tki->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_tim_mo" value="{{ @$tki->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_pi" value="{{ @$tki->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_risma" value="{{ @$tki->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_mo" value="{{ @$tki->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_pda" value="{{ @$tki->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_hsi" value="{{ @$tki->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_wms" value="{{ @$tki->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_prog" value="{{ @$tki->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_order_kendala" value="{{ @$tki->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tki_real_ps" value="{{ @$tki->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="12">CUI</th>
                                    <th class="text-center align-middle bg-white" rowspan="3">BLC AREA</th>
                                    <td>BLC</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_tim_pt1" value="{{ @$blc->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_tim_saber" value="{{ @$blc->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_tim_bantek" value="{{ @$blc->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_tim_mo" value="{{ @$blc->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_pi" value="{{ @$blc->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_risma" value="{{ @$blc->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_mo" value="{{ @$blc->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_pda" value="{{ @$blc->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_hsi" value="{{ @$blc->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_wms" value="{{ @$blc->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_prog" value="{{ @$blc->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_order_kendala" value="{{ @$blc->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="blc_real_ps" value="{{ @$blc->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="3">
                                        <input type="number" style="text-align: center; color: black" name="blcsertrj_komitmen_ps" value="{{ @$blcsertrj->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>SER</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_tim_pt1" value="{{ @$ser->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_tim_saber" value="{{ @$ser->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_tim_bantek" value="{{ @$ser->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_tim_mo" value="{{ @$ser->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_pi" value="{{ @$ser->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_risma" value="{{ @$ser->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_mo" value="{{ @$ser->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_pda" value="{{ @$ser->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_hsi" value="{{ @$ser->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_wms" value="{{ @$ser->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_prog" value="{{ @$ser->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_order_kendala" value="{{ @$ser->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="ser_real_ps" value="{{ @$ser->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>TRJ</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_tim_pt1" value="{{ @$trj->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_tim_saber" value="{{ @$trj->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_tim_bantek" value="{{ @$trj->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_tim_mo" value="{{ @$trj->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_pi" value="{{ @$trj->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_risma" value="{{ @$trj->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_mo" value="{{ @$trj->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_pda" value="{{ @$trj->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_hsi" value="{{ @$trj->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_wms" value="{{ @$trj->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_prog" value="{{ @$trj->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_order_kendala" value="{{ @$trj->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="trj_real_ps" value="{{ @$trj->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white">KOTABARU</th>
                                    <td>KPL</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_tim_pt1" value="{{ @$kpl->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_tim_saber" value="{{ @$kpl->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_tim_bantek" value="{{ @$kpl->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_tim_mo" value="{{ @$kpl->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_pi" value="{{ @$kpl->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_risma" value="{{ @$kpl->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_mo" value="{{ @$kpl->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_pda" value="{{ @$kpl->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_hsi" value="{{ @$kpl->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_wms" value="{{ @$kpl->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_prog" value="{{ @$kpl->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_order_kendala" value="{{ @$kpl->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_real_ps" value="{{ @$kpl->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kpl_komitmen_ps" value="{{ @$kpl->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">SATUI</th>
                                    <td>PGT</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_tim_pt1" value="{{ @$pgt->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_tim_saber" value="{{ @$pgt->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_tim_bantek" value="{{ @$pgt->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_tim_mo" value="{{ @$pgt->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_pi" value="{{ @$pgt->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_risma" value="{{ @$pgt->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_mo" value="{{ @$pgt->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_pda" value="{{ @$pgt->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_hsi" value="{{ @$pgt->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_wms" value="{{ @$pgt->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_prog" value="{{ @$pgt->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_order_kendala" value="{{ @$pgt->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgt_real_ps" value="{{ @$pgt->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="3">
                                        <input type="number" style="text-align: center; color: black" name="pgtstikip_komitmen_ps" value="{{ @$pgtstikip->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>STI</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_tim_pt1" value="{{ @$sti->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_tim_saber" value="{{ @$sti->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_tim_bantek" value="{{ @$sti->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_tim_mo" value="{{ @$sti->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_pi" value="{{ @$sti->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_risma" value="{{ @$sti->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_mo" value="{{ @$sti->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_pda" value="{{ @$sti->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_hsi" value="{{ @$sti->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_wms" value="{{ @$sti->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_prog" value="{{ @$sti->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_order_kendala" value="{{ @$sti->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="sti_real_ps" value="{{ @$sti->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>KIP</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_tim_pt1" value="{{ @$kip->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_tim_saber" value="{{ @$kip->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_tim_bantek" value="{{ @$kip->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_tim_mo" value="{{ @$kip->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_pi" value="{{ @$kip->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_risma" value="{{ @$kip->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_mo" value="{{ @$kip->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_pda" value="{{ @$kip->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_hsi" value="{{ @$kip->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_wms" value="{{ @$kip->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_prog" value="{{ @$kip->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_order_kendala" value="{{ @$kip->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kip_real_ps" value="{{ @$kip->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">TJL AREA</th>
                                    <td>TJL</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_tim_pt1" value="{{ @$tjl->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_tim_saber" value="{{ @$tjl->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_tim_bantek" value="{{ @$tjl->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_tim_mo" value="{{ @$tjl->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_pi" value="{{ @$tjl->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_risma" value="{{ @$tjl->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_mo" value="{{ @$tjl->order_mo ? : '0' }}">
                                    </td>   
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_pda" value="{{ @$tjl->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_hsi" value="{{ @$tjl->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_wms" value="{{ @$tjl->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_prog" value="{{ @$tjl->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_order_kendala" value="{{ @$tjl->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="tjl_real_ps" value="{{ @$tjl->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="3">
                                        <input type="number" style="text-align: center; color: black" name="tjlpgnamt_komitmen_ps" value="{{ @$tjlpgnamt->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>PGN</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_tim_pt1" value="{{ @$pgn->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_tim_saber" value="{{ @$pgn->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_tim_bantek" value="{{ @$pgn->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_tim_mo" value="{{ @$pgn->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_pi" value="{{ @$pgn->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_risma" value="{{ @$pgn->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_mo" value="{{ @$pgn->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_pda" value="{{ @$pgn->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_hsi" value="{{ @$pgn->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_wms" value="{{ @$pgn->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_prog" value="{{ @$pgn->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_order_kendala" value="{{ @$pgn->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="pgn_real_ps" value="{{ @$pgn->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>AMT</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_tim_pt1" value="{{ @$amt->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_tim_saber" value="{{ @$amt->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_tim_bantek" value="{{ @$amt->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_tim_mo" value="{{ @$amt->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_pi" value="{{ @$amt->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_risma" value="{{ @$amt->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_mo" value="{{ @$amt->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_pda" value="{{ @$amt->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_hsi" value="{{ @$amt->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_wms" value="{{ @$amt->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_prog" value="{{ @$amt->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_order_kendala" value="{{ @$amt->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="amt_real_ps" value="{{ @$amt->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="2">MTP</th>
                                    <td>MTP</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_tim_pt1" value="{{ @$mtp->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_tim_saber" value="{{ @$mtp->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_tim_bantek" value="{{ @$mtp->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_tim_mo" value="{{ @$mtp->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_pi" value="{{ @$mtp->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_risma" value="{{ @$mtp->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_mo" value="{{ @$mtp->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_pda" value="{{ @$mtp->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_hsi" value="{{ @$mtp->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_wms" value="{{ @$mtp->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_prog" value="{{ @$mtp->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_order_kendala" value="{{ @$mtp->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mtp_real_ps" value="{{ @$mtp->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="2">
                                        <input type="number" style="text-align: center; color: black" name="mtpmrb_komitmen_ps" value="{{ @$mtpmrb->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>MRB</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_tim_pt1" value="{{ @$mrb->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_tim_saber" value="{{ @$mrb->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_tim_bantek" value="{{ @$mrb->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_tim_mo" value="{{ @$mrb->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_pi" value="{{ @$mrb->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_risma" value="{{ @$mrb->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_mo" value="{{ @$mrb->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_pda" value="{{ @$mrb->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_hsi" value="{{ @$mrb->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_wms" value="{{ @$mrb->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_prog" value="{{ @$mrb->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_order_kendala" value="{{ @$mrb->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="mrb_real_ps" value="{{ @$mrb->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="5">GTM</th>
                                    <th class="text-center align-middle bg-white">LUL</th>
                                    <td>LUL</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_tim_pt1" value="{{ @$lul->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_tim_saber" value="{{ @$lul->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_tim_bantek" value="{{ @$lul->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_tim_mo" value="{{ @$lul->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_pi" value="{{ @$lul->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_risma" value="{{ @$lul->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_mo" value="{{ @$lul->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_pda" value="{{ @$lul->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_hsi" value="{{ @$lul->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_wms" value="{{ @$lul->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_prog" value="{{ @$lul->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_order_kendala" value="{{ @$lul->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_real_ps" value="{{ @$lul->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="lul_komitmen_ps" value="{{ @$lul->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white">BARABAI</th>
                                    <td>BRI</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_tim_pt1" value="{{ @$bri->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_tim_saber" value="{{ @$bri->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_tim_bantek" value="{{ @$bri->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_tim_mo" value="{{ @$bri->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_pi" value="{{ @$bri->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_risma" value="{{ @$bri->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_mo" value="{{ @$bri->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_pda" value="{{ @$bri->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_hsi" value="{{ @$bri->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_wms" value="{{ @$bri->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_prog" value="{{ @$bri->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_order_kendala" value="{{ @$bri->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_real_ps" value="{{ @$bri->real_ps ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="bri_komitmen_ps" value="{{ @$bri->komitmen_ps ? : '0' }}">
                                    </td>
                                </tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">KANDANGAN</th>
                                    <td>KDG</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_tim_pt1" value="{{ @$kdg->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_tim_saber" value="{{ @$kdg->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_tim_bantek" value="{{ @$kdg->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_tim_mo" value="{{ @$kdg->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_pi" value="{{ @$kdg->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_risma" value="{{ @$kdg->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_mo" value="{{ @$kdg->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_pda" value="{{ @$kdg->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_hsi" value="{{ @$kdg->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_wms" value="{{ @$kdg->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_prog" value="{{ @$kdg->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_order_kendala" value="{{ @$kdg->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="kdg_real_ps" value="{{ @$kdg->real_ps ? : '0' }}">
                                    </td>
                                    <th class="text-center align-middle" style="background-color: white" rowspan="3">
                                        <input type="number" style="text-align: center; color: black" name="kdgnegrta_komitmen_ps" value="{{ @$kdgnegrta->komitmen_ps ? : '0' }}">
                                    </th>
                                </tr>
                                <tr>
                                    <td>NEG</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_tim_pt1" value="{{ @$neg->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_tim_saber" value="{{ @$neg->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_tim_bantek" value="{{ @$neg->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_tim_mo" value="{{ @$neg->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_pi" value="{{ @$neg->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_risma" value="{{ @$neg->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_mo" value="{{ @$neg->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_pda" value="{{ @$neg->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_hsi" value="{{ @$neg->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_wms" value="{{ @$neg->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_prog" value="{{ @$neg->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_order_kendala" value="{{ @$neg->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="neg_real_ps" value="{{ @$neg->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>RTA</td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_tim_pt1" value="{{ @$rta->tim_pt1 ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_tim_saber" value="{{ @$rta->tim_saber ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_tim_bantek" value="{{ @$rta->tim_bantek ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_tim_mo" value="{{ @$rta->tim_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_pi" value="{{ @$rta->order_pi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_risma" value="{{ @$rta->order_risma ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_mo" value="{{ @$rta->order_mo ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_pda" value="{{ @$rta->order_pda ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_hsi" value="{{ @$rta->order_hsi ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_wms" value="{{ @$rta->order_wms ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_prog" value="{{ @$rta->order_prog ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_order_kendala" value="{{ @$rta->order_kendala ? : '0' }}">
                                    </td>
                                    <td>
                                        <input type="number" style="text-align: center" name="rta_real_ps" value="{{ @$rta->real_ps ? : '0' }}">
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-rounded btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-rounded btn-success waves-effect text-left">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade lapor-cuaca" tabindex="-1" role="dialog" aria-labelledby="lapor-cuaca" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <form method="POST" action="/dashboard/reportProvTodaySave">
            <input type="hidden" name="date" value="{{ $date }}">
            <input type="hidden" name="report" value="cuaca">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="lapor-cuaca">REPORT CUACA, SUPPLY & KENDALA KALSEL</h4>
                    </div>
                    <div class="modal-body table-responsive">
                        <table class="table table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th class="align-middle bg-blue" rowspan="2">MITRA</th>
                                    <th class="align-middle bg-blue" rowspan="2">SEKTOR</th>
                                    <th class="align-middle bg-blue" rowspan="2">STO</th>
                                    <th class="align-middle bg-purple" colspan="3">CUACA</th>
                                    <th class="align-middle bg-purple" rowspan="2">NTE & MATERIAL</th>
                                    <th class="align-middle bg-purple" colspan="1">KENDALA LAPANGAN</th>
                                    <th class="align-middle bg-purple" colspan="2">SOLUSI & ACTION</th>
                                </tr>
                                <tr>
                                    <th class="align-middle bg-purple">PAGI</th>
                                    <th class="align-middle bg-purple">SIANG</th>
                                    <th class="align-middle bg-purple">SORE</th>
                                    <th class="align-middle bg-purple">KENDALA 1</th>
                                    <th class="align-middle bg-purple">ACTION 1</th>
                                    <th class="align-middle bg-purple">ACTION 2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="text-center align-middle bg-white">STM</th>
                                    <th class="text-center align-middle bg-white">BJM</th>
                                    <td class="text-center align-middle">BJM</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bjm_cuaca_pagi" value="{{ @$bjm->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bjm_cuaca_siang" value="{{ @$bjm->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bjm_cuaca_sore" value="{{ @$bjm->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="2" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="bjmkyg_nte_material" value="{{ @$bjmkyg->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="bjm_kendala_1">{{ @$bjm->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bjm_action_1">{{ @$bjm->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bjm_action_2">{{ @$bjm->action_2 }}</textarea>
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">UPATEK</th>
                                    <th class="bg-white">KYG</th>
                                    <td class="text-center align-middle">KYG</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kyg_cuaca_pagi" value="{{ @$kyg->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kyg_cuaca_siang" value="{{ @$kyg->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kyg_cuaca_sore" value="{{ @$kyg->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kyg_kendala_1">{{ @$kyg->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kyg_action_1">{{ @$kyg->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kyg_action_2">{{ @$kyg->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="2">ULI - GMB</th>
                                    <td class="bg-white">ULI</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="uli_cuaca_pagi" value="{{ @$uli->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="uli_cuaca_siang" value="{{ @$uli->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="uli_cuaca_sore" value="{{ @$uli->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="2" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="uligmb_nte_material" value="{{ @$uligmb->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle" rowspan="2">
                                        <textarea name="uligmb_kendala_1">{{ @$uligmb->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle" rowspan="2">
                                        <textarea name="uligmb_action_1">{{ @$uligmb->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle" rowspan="2">
                                        <textarea name="uligmb_action_2">{{ @$uligmb->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">GMB</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="gmb_cuaca_pagi" value="{{ @$gmb->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="gmb_cuaca_siang" value="{{ @$gmb->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="gmb_cuaca_sore" value="{{ @$gmb->cuaca_sore }}">
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="4">TBN</th>
                                    <th class="bg-white">BBR</th>
                                    <td class="text-center align-middle">BBR</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bbr_cuaca_pagi" value="{{ @$bbr->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bbr_cuaca_siang" value="{{ @$bbr->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bbr_cuaca_sore" value="{{ @$bbr->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bbr_nte_material" value="{{ @$bbr->nte_material }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bbr_kendala_1">{{ @$bbr->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bbr_action_1">{{ @$bbr->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bbr_action_2">{{ @$bbr->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">PLE</th>
                                    <td class="text-center align-middle">PLE</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ple_cuaca_pagi" value="{{ @$ple->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ple_cuaca_siang" value="{{ @$ple->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ple_cuaca_sore" value="{{ @$ple->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="3" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="plebtbtki_nte_material" value="{{ @$plebtbtki->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="ple_kendala_1">{{ @$ple->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="ple_action_1">{{ @$ple->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="ple_action_2">{{ @$ple->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">BTB</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="btb_cuaca_pagi" value="{{ @$btb->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="btb_cuaca_siang" value="{{ @$btb->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="btb_cuaca_sore" value="{{ @$btb->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="btb_kendala_1">{{ @$btb->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="btb_action_1">{{ @$btb->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="btb_action_2">{{ @$btb->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>TKI</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tki_cuaca_pagi" value="{{ @$tki->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tki_cuaca_siang" value="{{ @$tki->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tki_cuaca_sore" value="{{ @$tki->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="tki_kendala_1">{{ @$tki->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="tki_action_1">{{ @$tki->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="tki_action_2">{{ @$tki->action_2 }}</textarea>
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="12">CUI</th>
                                    <th class="text-center align-middle bg-white" rowspan="3">BLC AREA</th>
                                    <td class="text-center align-middle">BLC</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="blc_cuaca_pagi" value="{{ @$blc->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="blc_cuaca_siang" value="{{ @$blc->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="blc_cuaca_sore" value="{{ @$blc->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="3" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="blcsertrj_nte_material" value="{{ @$blcsertrj->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="blc_kendala_1">{{ @$blc->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="blc_action_1">{{ @$blc->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="blc_action_2">{{ @$blc->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">SER</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ser_cuaca_pagi" value="{{ @$ser->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ser_cuaca_siang" value="{{ @$ser->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="ser_cuaca_sore" value="{{ @$ser->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="ser_kendala_1">{{ @$ser->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="ser_action_1">{{ @$ser->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="ser_action_2">{{ @$ser->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">TRJ</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="trj_cuaca_pagi" value="{{ @$trj->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="trj_cuaca_siang" value="{{ @$trj->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="trj_cuaca_sore" value="{{ @$trj->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="trj_kendala_1">{{ @$trj->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="trj_action_1">{{ @$trj->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="trj_action_2">{{ @$trj->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white">KOTABARU</th>
                                    <td class="text-center align-middle">KPL</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kpl_cuaca_pagi" value="{{ @$kpl->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kpl_cuaca_siang" value="{{ @$kpl->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kpl_cuaca_sore" value="{{ @$kpl->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kpl_nte_material" value="{{ @$kpl->nte_material }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kpl_kendala_1">{{ @$kpl->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kpl_action_1">{{ @$kpl->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kpl_action_2">{{ @$kpl->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">SATUI</th>
                                    <td class="text-center align-middle">PGT</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgt_cuaca_pagi" value="{{ @$pgt->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgt_cuaca_siang" value="{{ @$pgt->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgt_cuaca_sore" value="{{ @$pgt->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="3" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="pgtstikip_nte_material" value="{{ @$pgtstikip->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="pgt_kendala_1">{{ @$pgt->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="pgt_action_1">{{ @$pgt->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="pgt_action_2">{{ @$pgt->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">STI</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="sti_cuaca_pagi" value="{{ @$sti->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="sti_cuaca_siang" value="{{ @$sti->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="sti_cuaca_sore" value="{{ @$sti->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="sti_kendala_1">{{ @$sti->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="sti_action_1">{{ @$sti->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="sti_action_2">{{ @$sti->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">KIP</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kip_cuaca_pagi" value="{{ @$kip->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kip_cuaca_siang" value="{{ @$kip->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kip_cuaca_sore" value="{{ @$kip->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kip_kendala_1">{{ @$kip->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kip_action_1">{{ @$kip->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kip_action_2">{{ @$kip->action_2 }}</textarea>
                                    </td>
                                </tr>
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">TJL AREA</th>
                                    <td class="text-center align-middle">TJL</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tjl_cuaca_pagi" value="{{ @$tjl->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tjl_cuaca_siang" value="{{ @$tjl->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="tjl_cuaca_sore" value="{{ @$tjl->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="3" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="tjlpgnamt_nte_material" value="{{ @$tjlpgnamt->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="tjl_kendala_1">{{ @$tjl->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="tjl_action_1">{{ @$tjl->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="tjl_action_2">{{ @$tjl->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">PGN</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgn_cuaca_pagi" value="{{ @$pgn->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgn_cuaca_siang" value="{{ @$pgn->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="pgn_cuaca_sore" value="{{ @$pgn->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="pgn_kendala_1">{{ @$pgn->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="pgn_action_1">{{ @$pgn->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="pgn_action_2">{{ @$pgn->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td >AMT</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="amt_cuaca_pagi" value="{{ @$amt->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="amt_cuaca_siang" value="{{ @$amt->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="amt_cuaca_sore" value="{{ @$amt->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="amt_kendala_1">{{ @$amt->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="amt_action_1">{{ @$amt->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="amt_action_2">{{ @$amt->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="2">MTP</th>
                                    <td class="text-center align-middle">MTP</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mtp_cuaca_pagi" value="{{ @$mtp->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mtp_cuaca_siang" value="{{ @$mtp->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mtp_cuaca_sore" value="{{ @$mtp->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="2" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="mtpmrb_nte_material" value="{{ @$mtpmrb->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="mtp_kendala_1">{{ @$mtp->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="mtp_action_1">{{ @$mtp->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="mtp_action_2">{{ @$mtp->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">MRB</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mrb_cuaca_pagi" value="{{ @$mrb->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mrb_cuaca_siang" value="{{ @$mrb->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="mrb_cuaca_sore" value="{{ @$mrb->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="mrb_kendala_1">{{ @$mrb->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="mrb_action_1">{{ @$mrb->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="mrb_action_2">{{ @$mrb->action_2 }}</textarea>
                                    </td>
                                </tr>
                    
                    
                                <tr>
                                    <th class="text-center align-middle bg-white" rowspan="5">GTM</th>
                                    <th class="text-center align-middle bg-white">LUL</th>
                                    <td class="text-center align-middle">LUL</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="lul_cuaca_pagi" value="{{ @$lul->cuaca_pagi }}">
                                        {{ @$lul->cuaca_pagi }}
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="lul_cuaca_siang" value="{{ @$lul->cuaca_siang }}">
                                        {{ @$lul->cuaca_siang }}
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="lul_cuaca_sore" value="{{ @$lul->cuaca_sore }}">
                                        {{ @$lul->cuaca_sore }}
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="lul_nte_material" value="{{ @$lul->nte_material }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="lul_kendala_1">{{ @$lul->kendala_1 }}</textarea>
                                        {{ @$lul->kendala_1 }}
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="lul_action_1">{{ @$lul->action_1 }}</textarea>
                                        {{ @$lul->action_1 }}
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="lul_action_2">{{ @$lul->action_2 }}</textarea>
                                        {{ @$lul->action_2 }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-center align-middle bg-white">BARABAI</th>
                                    <td>BRI</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bri_cuaca_pagi" value="{{ @$bri->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bri_cuaca_siang" value="{{ @$bri->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bri_cuaca_sore" value="{{ @$bri->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="bri_nte_material" value="{{ @$bri->nte_material }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bri_kendala_1">{{ @$bri->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bri_action_1">{{ @$bri->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="bri_action_2">{{ @$bri->action_2 }}</textarea>
                                    </td>
                                </tr>
                                    <th class="text-center align-middle bg-white" rowspan="3">KANDANGAN</th>
                                    <td class="text-center align-middle">KDG</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kdg_cuaca_pagi" value="{{ @$kdg->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kdg_cuaca_siang" value="{{ @$kdg->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="kdg_cuaca_sore" value="{{ @$kdg->cuaca_sore }}">
                                    </td>
                                    <th class="text-center align-middle" rowspan="2" style="background-color: white; color: black">
                                        <input type="text" style="text-align: center" name="kdgneg_nte_material" value="{{ @$kdgneg->nte_material }}">
                                    </th>
                                    <td class="text-center align-middle">
                                        <textarea name="kdg_kendala_1">{{ @$kdg->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kdg_action_1">{{ @$kdg->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="kdg_action_2">{{ @$kdg->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">NEG</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="neg_cuaca_pagi" value="{{ @$neg->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="neg_cuaca_siang" value="{{ @$neg->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="neg_cuaca_sore" value="{{ @$neg->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="neg_kendala_1">{{ @$neg->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="neg_action_1">{{ @$neg->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="neg_action_2">{{ @$neg->action_2 }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center align-middle">RTA</td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="rta_cuaca_pagi" value="{{ @$rta->cuaca_pagi }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="rta_cuaca_siang" value="{{ @$rta->cuaca_siang }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="rta_cuaca_sore" value="{{ @$rta->cuaca_sore }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" style="text-align: center" name="rta_nte_material" value="{{ @$rta->nte_material }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="rta_kendala_1">{{ @$rta->kendala_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="rta_action_1">{{ @$rta->action_1 }}</textarea>
                                    </td>
                                    <td class="text-center align-middle">
                                        <textarea name="rta_action_2">{{ @$rta->action_2 }}</textarea>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-rounded btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-rounded btn-success waves-effect text-left">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

  </div>

  <div class="col-sm-12 table-responsive">

    <h2 style="font-weight: bold; text-align: center;" class="text-center">REPORT PROGRESS ORDER & RESOURCE KALSEL</h2>

    <button class="btn btn-sm btn-rounded btn-info" data-toggle="modal" data-target=".lapor-progress">Lapor Progress!</button>
    <button class="btn btn-sm btn-rounded btn-info" data-toggle="modal" data-target=".lapor-cuaca">Lapor Cuaca!</button>

    <table class="table table-bordered dataTable">
        <thead>
            <tr>
                <th class="align-middle bg-blue" rowspan="2">MITRA</th>
                <th class="align-middle bg-blue" rowspan="2">SEKTOR</th>
                <th class="align-middle bg-blue" rowspan="2">STO</th>
                <th class="align-middle bg-green" colspan="4">TIM</th>
                <th class="align-middle bg-green" rowspan="2">TOTAL<br />TIM</th>
                <th class="align-middle bg-blue" colspan="10">ORDER</th>
                <th class="align-middle bg-blue" rowspan="2">REAL<br />PS</th>
                <th class="align-middle bg-green" rowspan="2">KOMITMEN<br />PS</th>
                <th class="align-middle bg-blue" rowspan="2">LOAD</th>
                <th class="align-middle bg-blue" rowspan="2">KET</th>
              </tr>
              <tr>
                <th class="align-middle bg-green">PT1</th>
                <th class="align-middle bg-green">SABER</th>
                <th class="align-middle bg-green">BANTEK</th>
                <th class="align-middle bg-green">MO</th>
                <th class="align-middle bg-blue">PI</th>
                <th class="align-middle bg-blue">RISMA</th>
                <th class="align-middle bg-blue">MO</th>
                <th class="align-middle bg-blue">PDA</th>
                <th class="align-middle bg-blue">HSI</th>
                <th class="align-middle bg-blue">WMS</th>
                <th class="align-middle bg-orange">TOT</th>
                <th class="align-middle bg-orange">PROG</th>
                <th class="align-middle bg-orange">KENDALA</th>
                <th class="align-middle bg-orange">PROGRESS %</th>
              </tr>
        </thead>
        <tbody>
            <tr>
                <th class="bg-white">STM</th>
                <th class="bg-white">BJM</th>
                <td>BJM</td>
                @php
                    $total_tim_bjm = @$bjm->tim_pt1 + @$bjm->tim_saber + @$bjm->tim_bantek + @$bjm->tim_mo;
                    $total_order_bjm = (@$bjm->order_pi + @$bjm->order_risma + @$bjm->order_mo + @$bjm->order_pda + @$bjm->order_hsi + @$bjm->order_wms);

                    $progress_bjm = ceil(@round((@$bjm->order_prog + @$bjm->order_kendala) / $total_order_bjm * 100,2));
                    if (is_nan($progress_bjm) == true)
                    {
                        $progress_bjm = 100;
                    }
                    else
                    {
                        $progress_bjm = $progress_bjm;
                    }

                    if ($progress_bjm < 100)
                    {
                        $class_progress_bjm = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_bjm = '';
                    }

                    if (@$bjm->real_ps < 1)
                    {
                        $class_real_ps_bjm = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_bjm = '';
                    }

                    $load_order_bjm = (@$bjm->tim_pt1 + @$bjm->tim_bantek) * 2 - $total_order_bjm + @$bjm->order_prog + @$bjm->order_kendala;

                    if ($load_order_bjm < '-1')
                    {
                        $ket_bjm = 'OVERLOAD';
                        $class_ket_bjm = 'class=bg-red';
                    }
                    elseif ($load_order_bjm < 3)
                    {
                        $ket_bjm = 'ORDER CUKUP';
                        $class_ket_bjm = '';
                    }
                    else
                    {
                        $ket_bjm = 'KURANG ORDER';
                        $class_ket_bjm = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$bjm->tim_pt1 ? : '0' }}</td>
                <td>{{ @$bjm->tim_saber ? : '0' }}</td>
                <td>{{ @$bjm->tim_bantek ? : '0' }}</td>
                <td>{{ @$bjm->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_bjm }}</td>
                <td>{{ @$bjm->order_pi ? : '0' }}</td>
                <td>{{ @$bjm->order_risma ? : '0' }}</td>
                <td>{{ @$bjm->order_mo ? : '0' }}</td>
                <td>{{ @$bjm->order_pda ? : '0' }}</td>
                <td>{{ @$bjm->order_hsi ? : '0' }}</td>
                <td>{{ @$bjm->order_wms ? : '0' }}</td>
                <td>{{ $total_order_bjm }}</td>
                <td>{{ @$bjm->order_prog ? : '0' }}</td>
                <td>{{ @$bjm->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_bjm }}>{{ $progress_bjm }} %</td>
                <td {{ $class_real_ps_bjm }}>{{ @$bjm->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$bjm->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_bjm }}</td>
                <td {{ $class_ket_bjm }}>{{ $ket_bjm }}</td>
            </tr>

            
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">UPATEK</th>
                <th class="bg-white">KYG</th>
                <td>KYG</td>
                @php
                    $total_tim_kyg = @$kyg->tim_pt1 + @$kyg->tim_saber + @$kyg->tim_bantek + @$kyg->tim_mo;
                    $total_order_kyg = (@$kyg->order_pi + @$kyg->order_risma + @$kyg->order_mo + @$kyg->order_pda + @$kyg->order_hsi + @$kyg->order_wms);

                    $progress_kyg = ceil(@round((@$kyg->order_prog + @$kyg->order_kendala) / $total_order_kyg * 100,2));
                    if (is_nan($progress_kyg) == true)
                    {
                        $progress_kyg = 100;
                    }
                    else
                    {
                        $progress_kyg = $progress_kyg;
                    }

                    if ($progress_kyg < 100)
                    {
                        $class_progress_kyg = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_kyg = '';
                    }

                    if (@$kyg->real_ps < 1)
                    {
                        $class_real_ps_kyg = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_kyg = '';
                    }

                    $load_order_kyg = (@$kyg->tim_pt1 + @$kyg->tim_bantek) * 2 - $total_order_kyg + @$kyg->order_prog + @$kyg->order_kendala;

                    if ($load_order_kyg < '-1')
                    {
                        $ket_kyg = 'OVERLOAD';
                        $class_ket_kyg = 'class=bg-red';
                    }
                    elseif ($load_order_kyg < 3)
                    {
                        $ket_kyg = 'ORDER CUKUP';
                        $class_ket_kyg = '';
                    }
                    else
                    {
                        $ket_kyg = 'KURANG ORDER';
                        $class_ket_kyg = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$kyg->tim_pt1 ? : '0' }}</td>
                <td>{{ @$kyg->tim_saber ? : '0' }}</td>
                <td>{{ @$kyg->tim_bantek ? : '0' }}</td>
                <td>{{ @$kyg->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_kyg }}</td>
                <td>{{ @$kyg->order_pi ? : '0' }}</td>
                <td>{{ @$kyg->order_risma ? : '0' }}</td>
                <td>{{ @$kyg->order_mo ? : '0' }}</td>
                <td>{{ @$kyg->order_pda ? : '0' }}</td>
                <td>{{ @$kyg->order_hsi ? : '0' }}</td>
                <td>{{ @$kyg->order_wms ? : '0' }}</td>
                <td>{{ $total_order_kyg }}</td>
                <td>{{ @$kyg->order_prog ? : '0' }}</td>
                <td>{{ @$kyg->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_kyg }}>{{ $progress_kyg }} %</td>
                <td {{ $class_real_ps_kyg }}>{{ @$kyg->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$kyg->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_kyg }}</td>
                <td {{ $class_ket_kyg }}>{{ $ket_kyg }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="2">ULI - GMB</th>
                <td class="bg-white">ULI</td>
                @php
                    $total_tim_uli = @$uli->tim_pt1 + @$uli->tim_saber + @$uli->tim_bantek + @$uli->tim_mo;
                    $total_order_uli = (@$uli->order_pi + @$uli->order_risma + @$uli->order_mo + @$uli->order_pda + @$uli->order_hsi + @$uli->order_wms);

                    $progress_uli = ceil(@round((@$uli->order_prog + @$uli->order_kendala) / $total_order_uli * 100,2));
                    if (is_nan($progress_uli) == true)
                    {
                        $progress_uli = 100;
                    }
                    else
                    {
                        $progress_uli = $progress_uli;
                    }

                    if ($progress_uli < 100)
                    {
                        $class_progress_uli = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_uli = '';
                    }

                    if (@$uli->real_ps < 1)
                    {
                        $class_real_ps_uli = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_uli = '';
                    }

                    $load_order_uli = (@$uli->tim_pt1 + @$uli->tim_bantek) * 2 - $total_order_uli + @$uli->order_prog + @$uli->order_kendala;

                    if ($load_order_uli < '-1')
                    {
                        $ket_uli = 'OVERLOAD';
                        $class_ket_uli = 'class=bg-red';
                    }
                    elseif ($load_order_uli < 3)
                    {
                        $ket_uli = 'ORDER CUKUP';
                        $class_ket_uli = '';
                    }
                    else
                    {
                        $ket_uli = 'KURANG ORDER';
                        $class_ket_uli = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$uli->tim_pt1 ? : '0' }}</td>
                <td>{{ @$uli->tim_saber ? : '0' }}</td>
                <td>{{ @$uli->tim_bantek ? : '0' }}</td>
                <td>{{ @$uli->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_uli }}</td>
                <td>{{ @$uli->order_pi ? : '0' }}</td>
                <td>{{ @$uli->order_risma ? : '0' }}</td>
                <td>{{ @$uli->order_mo ? : '0' }}</td>
                <td>{{ @$uli->order_pda ? : '0' }}</td>
                <td>{{ @$uli->order_hsi ? : '0' }}</td>
                <td>{{ @$uli->order_wms ? : '0' }}</td>
                <td>{{ $total_order_uli }}</td>
                <td>{{ @$uli->order_prog ? : '0' }}</td>
                <td>{{ @$uli->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_uli }}>{{ $progress_uli }} %</td>
                <td {{ $class_real_ps_uli }}>{{ @$uli->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="2">{{ @$uligmb->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_uli }}</td>
                <td {{ $class_ket_uli }}>{{ $ket_uli }}</td>
            </tr>
            <tr>
                <td>GMB</td>
                @php
                    $total_tim_gmb = @$gmb->tim_pt1 + @$gmb->tim_saber + @$gmb->tim_bantek + @$gmb->tim_mo;
                    $total_order_gmb = (@$gmb->order_pi + @$gmb->order_risma + @$gmb->order_mo + @$gmb->order_pda + @$gmb->order_hsi + @$gmb->order_wms);

                    $progress_gmb = ceil(@round((@$gmb->order_prog + @$gmb->order_kendala) / $total_order_gmb * 100,2));
                    if (is_nan($progress_gmb) == true)
                    {
                        $progress_gmb = 100;
                    }
                    else
                    {
                        $progress_gmb = $progress_gmb;
                    }

                    if ($progress_gmb < 100)
                    {
                        $class_progress_gmb = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_gmb = '';
                    }

                    if (@$gmb->real_ps < 1)
                    {
                        $class_real_ps_gmb = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_gmb = '';
                    }

                    $load_order_gmb = (@$gmb->tim_pt1 + @$gmb->tim_bantek) * 2 - $total_order_gmb + @$gmb->order_prog + @$gmb->order_kendala;

                    if ($load_order_gmb < '-1')
                    {
                        $ket_gmb = 'OVERLOAD';
                        $class_ket_gmb = 'class=bg-red';
                    }
                    elseif ($load_order_gmb < 3)
                    {
                        $ket_gmb = 'ORDER CUKUP';
                        $class_ket_gmb = '';
                    }
                    else
                    {
                        $ket_gmb = 'KURANG ORDER';
                        $class_ket_gmb = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$gmb->tim_pt1 ? : '0' }}</td>
                <td>{{ @$gmb->tim_saber ? : '0' }}</td>
                <td>{{ @$gmb->tim_bantek ? : '0' }}</td>
                <td>{{ @$gmb->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_gmb }}</td>
                <td>{{ @$gmb->order_pi ? : '0' }}</td>
                <td>{{ @$gmb->order_risma ? : '0' }}</td>
                <td>{{ @$gmb->order_mo ? : '0' }}</td>
                <td>{{ @$gmb->order_pda ? : '0' }}</td>
                <td>{{ @$gmb->order_hsi ? : '0' }}</td>
                <td>{{ @$gmb->order_wms ? : '0' }}</td>
                <td>{{ $total_order_gmb }}</td>
                <td>{{ @$gmb->order_prog ? : '0' }}</td>
                <td>{{ @$gmb->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_gmb }}>{{ $progress_gmb }} %</td>
                <td {{ $class_real_ps_gmb }}>{{ @$gmb->real_ps ? : '0' }}</td>
                <td>{{ $load_order_gmb }}</td>
                <td {{ $class_ket_gmb }}>{{ $ket_gmb }}</td>
            </tr>
            

            <tr>
                <th class="text-center align-middle bg-white" rowspan="4">TBN</th>
                <th class="bg-white">BBR</th>
                <td>BBR</td>
                @php
                    $total_tim_bbr = @$bbr->tim_pt1 + @$bbr->tim_saber + @$bbr->tim_bantek + @$bbr->tim_mo;
                    $total_order_bbr = (@$bbr->order_pi + @$bbr->order_risma + @$bbr->order_mo + @$bbr->order_pda + @$bbr->order_hsi + @$bbr->order_wms);

                    $progress_bbr = ceil(@round((@$bbr->order_prog + @$bbr->order_kendala) / $total_order_bbr * 100,2));
                    if (is_nan($progress_bbr) == true)
                    {
                        $progress_bbr = 100;
                    }
                    else
                    {
                        $progress_bbr = $progress_bbr;
                    }

                    if ($progress_bbr < 100)
                    {
                        $class_progress_bbr = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_bbr = '';
                    }

                    if (@$bbr->real_ps < 1)
                    {
                        $class_real_ps_bbr = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_bbr = '';
                    }

                    $load_order_bbr = (@$bbr->tim_pt1 + @$bbr->tim_bantek) * 2 - $total_order_bbr + @$bbr->order_prog + @$bbr->order_kendala;

                    if ($load_order_bbr < '-1')
                    {
                        $ket_bbr = 'OVERLOAD';
                        $class_ket_bbr = 'class=bg-red';
                    }
                    elseif ($load_order_bbr < 3)
                    {
                        $ket_bbr = 'ORDER CUKUP';
                        $class_ket_bbr = '';
                    }
                    else
                    {
                        $ket_bbr = 'KURANG ORDER';
                        $class_ket_bbr = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$bbr->tim_pt1 ? : '0' }}</td>
                <td>{{ @$bbr->tim_saber ? : '0' }}</td>
                <td>{{ @$bbr->tim_bantek ? : '0' }}</td>
                <td>{{ @$bbr->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_bbr }}</td>
                <td>{{ @$bbr->order_pi ? : '0' }}</td>
                <td>{{ @$bbr->order_risma ? : '0' }}</td>
                <td>{{ @$bbr->order_mo ? : '0' }}</td>
                <td>{{ @$bbr->order_pda ? : '0' }}</td>
                <td>{{ @$bbr->order_hsi ? : '0' }}</td>
                <td>{{ @$bbr->order_wms ? : '0' }}</td>
                <td>{{ $total_order_bbr }}</td>
                <td>{{ @$bbr->order_prog ? : '0' }}</td>
                <td>{{ @$bbr->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_bbr }}>{{ $progress_bbr }} %</td>
                <td {{ $class_real_ps_bbr }}>{{ @$bbr->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$bbr->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_bbr }}</td>
                <td {{ $class_ket_bbr }}>{{ $ket_bbr }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">PLE</th>
                <td>PLE</td>
                @php
                    $total_tim_ple = @$ple->tim_pt1 + @$ple->tim_saber + @$ple->tim_bantek + @$ple->tim_mo;
                    $total_order_ple = (@$ple->order_pi + @$ple->order_risma + @$ple->order_mo + @$ple->order_pda + @$ple->order_hsi + @$ple->order_wms);

                    $progress_ple = ceil(@round((@$ple->order_prog + @$ple->order_kendala) / $total_order_ple * 100,2));
                    if (is_nan($progress_ple) == true)
                    {
                        $progress_ple = 100;
                    }
                    else
                    {
                        $progress_ple = $progress_ple;
                    }

                    if ($progress_ple < 100)
                    {
                        $class_progress_ple = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_ple = '';
                    }

                    if (@$ple->real_ps < 1)
                    {
                        $class_real_ps_ple = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_ple = '';
                    }

                    $load_order_ple = (@$ple->tim_pt1 + @$ple->tim_bantek) * 2 - $total_order_ple + @$ple->order_prog + @$ple->order_kendala;

                    if ($load_order_ple < '-1')
                    {
                        $ket_ple = 'OVERLOAD';
                        $class_ket_ple = 'class=bg-red';
                    }
                    elseif ($load_order_ple < 3)
                    {
                        $ket_ple = 'ORDER CUKUP';
                        $class_ket_ple = '';
                    }
                    else
                    {
                        $ket_ple = 'KURANG ORDER';
                        $class_ket_ple = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$ple->tim_pt1 ? : '0' }}</td>
                <td>{{ @$ple->tim_saber ? : '0' }}</td>
                <td>{{ @$ple->tim_bantek ? : '0' }}</td>
                <td>{{ @$ple->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_ple }}</td>
                <td>{{ @$ple->order_pi ? : '0' }}</td>
                <td>{{ @$ple->order_risma ? : '0' }}</td>
                <td>{{ @$ple->order_mo ? : '0' }}</td>
                <td>{{ @$ple->order_pda ? : '0' }}</td>
                <td>{{ @$ple->order_hsi ? : '0' }}</td>
                <td>{{ @$ple->order_wms ? : '0' }}</td>
                <td>{{ $total_order_ple }}</td>
                <td>{{ @$ple->order_prog ? : '0' }}</td>
                <td>{{ @$ple->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_ple }}>{{ $progress_ple }} %</td>
                <td {{ $class_real_ps_ple }}>{{ @$ple->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="3">{{ @$plebtbtki->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_ple }}</td>
                <td {{ $class_ket_ple }}>{{ $ket_ple }}</td>
            </tr>
            <tr>
                <td>BTB</td>
                @php
                    $total_tim_btb = @$btb->tim_pt1 + @$btb->tim_saber + @$btb->tim_bantek + @$btb->tim_mo;
                    $total_order_btb = (@$btb->order_pi + @$btb->order_risma + @$btb->order_mo + @$btb->order_pda + @$btb->order_hsi + @$btb->order_wms);

                    $progress_btb = ceil(@round((@$btb->order_prog + @$btb->order_kendala) / $total_order_btb * 100,2));
                    if (is_nan($progress_btb) == true)
                    {
                        $progress_btb = 100;
                    }
                    else
                    {
                        $progress_btb = $progress_btb;
                    }

                    if ($progress_btb < 100)
                    {
                        $class_progress_btb = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_btb = '';
                    }

                    if (@$btb->real_ps < 1)
                    {
                        $class_real_ps_btb = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_btb = '';
                    }

                    $load_order_btb = (@$btb->tim_pt1 + @$btb->tim_bantek) * 2 - $total_order_btb + @$btb->order_prog + @$btb->order_kendala;

                    if ($load_order_btb < '-1')
                    {
                        $ket_btb = 'OVERLOAD';
                        $class_ket_btb = 'class=bg-red';
                    }
                    elseif ($load_order_btb < 3)
                    {
                        $ket_btb = 'ORDER CUKUP';
                        $class_ket_btb = '';
                    }
                    else
                    {
                        $ket_btb = 'KURANG ORDER';
                        $class_ket_btb = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$btb->tim_pt1 ? : '0' }}</td>
                <td>{{ @$btb->tim_saber ? : '0' }}</td>
                <td>{{ @$btb->tim_bantek ? : '0' }}</td>
                <td>{{ @$btb->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_btb }}</td>
                <td>{{ @$btb->order_pi ? : '0' }}</td>
                <td>{{ @$btb->order_risma ? : '0' }}</td>
                <td>{{ @$btb->order_mo ? : '0' }}</td>
                <td>{{ @$btb->order_pda ? : '0' }}</td>
                <td>{{ @$btb->order_hsi ? : '0' }}</td>
                <td>{{ @$btb->order_wms ? : '0' }}</td>
                <td>{{ $total_order_btb }}</td>
                <td>{{ @$btb->order_prog ? : '0' }}</td>
                <td>{{ @$btb->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_btb }}>{{ $progress_btb }} %</td>
                <td {{ $class_real_ps_btb }}>{{ @$btb->real_ps ? : '0' }}</td>
                <td>{{ $load_order_btb }}</td>
                <td {{ $class_ket_btb }}>{{ $ket_btb }}</td>
            </tr>
            <tr>
                <td>TKI</td>
                @php
                    $total_tim_tki = @$tki->tim_pt1 + @$tki->tim_saber + @$tki->tim_bantek + @$tki->tim_mo;
                    $total_order_tki = (@$tki->order_pi + @$tki->order_risma + @$tki->order_mo + @$tki->order_pda + @$tki->order_hsi + @$tki->order_wms);

                    $progress_tki = ceil(@round((@$tki->order_prog + @$tki->order_kendala) / $total_order_tki * 100,2));
                    if (is_nan($progress_tki) == true)
                    {
                        $progress_tki = 100;
                    }
                    else
                    {
                        $progress_tki = $progress_tki;
                    }

                    if ($progress_tki < 100)
                    {
                        $class_progress_tki = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_tki = '';
                    }

                    if (@$tki->real_ps < 1)
                    {
                        $class_real_ps_tki = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_tki = '';
                    }

                    $load_order_tki = (@$tki->tim_pt1 + @$tki->tim_bantek) * 2 - $total_order_tki + @$tki->order_prog + @$tki->order_kendala;

                    if ($load_order_tki < '-1')
                    {
                        $ket_tki = 'OVERLOAD';
                        $class_ket_tki = 'class=bg-red';
                    }
                    elseif ($load_order_tki < 3)
                    {
                        $ket_tki = 'ORDER CUKUP';
                        $class_ket_tki = '';
                    }
                    else
                    {
                        $ket_tki = 'KURANG ORDER';
                        $class_ket_tki = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$tki->tim_pt1 ? : '0' }}</td>
                <td>{{ @$tki->tim_saber ? : '0' }}</td>
                <td>{{ @$tki->tim_bantek ? : '0' }}</td>
                <td>{{ @$tki->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_tki }}</td>
                <td>{{ @$tki->order_pi ? : '0' }}</td>
                <td>{{ @$tki->order_risma ? : '0' }}</td>
                <td>{{ @$tki->order_mo ? : '0' }}</td>
                <td>{{ @$tki->order_pda ? : '0' }}</td>
                <td>{{ @$tki->order_hsi ? : '0' }}</td>
                <td>{{ @$tki->order_wms ? : '0' }}</td>
                <td>{{ $total_order_tki }}</td>
                <td>{{ @$tki->order_prog ? : '0' }}</td>
                <td>{{ @$tki->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_tki }}>{{ $progress_tki }} %</td>
                <td {{ $class_real_ps_tki }}>{{ @$tki->real_ps ? : '0' }}</td>
                <td>{{ $load_order_tki }}</td>
                <td {{ $class_ket_tki }}>{{ $ket_tki }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="12">CUI</th>
                <th class="text-center align-middle bg-white" rowspan="3">BLC AREA</th>
                <td>BLC</td>
                @php
                    $total_tim_blc = @$blc->tim_pt1 + @$blc->tim_saber + @$blc->tim_bantek + @$blc->tim_mo;
                    $total_order_blc = (@$blc->order_pi + @$blc->order_risma + @$blc->order_mo + @$blc->order_pda + @$blc->order_hsi + @$blc->order_wms);

                    $progress_blc = ceil(@round((@$blc->order_prog + @$blc->order_kendala) / $total_order_blc * 100,2));
                    if (is_nan($progress_blc) == true)
                    {
                        $progress_blc = 100;
                    }
                    else
                    {
                        $progress_blc = $progress_blc;
                    }

                    if ($progress_blc < 100)
                    {
                        $class_progress_blc = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_blc = '';
                    }

                    if (@$blc->real_ps < 1)
                    {
                        $class_real_ps_blc = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_blc = '';
                    }

                    $load_order_blc = (@$blc->tim_pt1 + @$blc->tim_bantek) * 2 - $total_order_blc + @$blc->order_prog + @$blc->order_kendala;

                    if ($load_order_blc < '-1')
                    {
                        $ket_blc = 'OVERLOAD';
                        $class_ket_blc = 'class=bg-red';
                    }
                    elseif ($load_order_blc < 3)
                    {
                        $ket_blc = 'ORDER CUKUP';
                        $class_ket_blc = '';
                    }
                    else
                    {
                        $ket_blc = 'KURANG ORDER';
                        $class_ket_blc = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$blc->tim_pt1 ? : '0' }}</td>
                <td>{{ @$blc->tim_saber ? : '0' }}</td>
                <td>{{ @$blc->tim_bantek ? : '0' }}</td>
                <td>{{ @$blc->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_blc }}</td>
                <td>{{ @$blc->order_pi ? : '0' }}</td>
                <td>{{ @$blc->order_risma ? : '0' }}</td>
                <td>{{ @$blc->order_mo ? : '0' }}</td>
                <td>{{ @$blc->order_pda ? : '0' }}</td>
                <td>{{ @$blc->order_hsi ? : '0' }}</td>
                <td>{{ @$blc->order_wms ? : '0' }}</td>
                <td>{{ $total_order_blc }}</td>
                <td>{{ @$blc->order_prog ? : '0' }}</td>
                <td>{{ @$blc->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_blc }}>{{ $progress_blc }} %</td>
                <td {{ $class_real_ps_blc }}>{{ @$blc->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="3">{{ @$blcsertrj->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_blc }}</td>
                <td {{ $class_ket_blc }}>{{ $ket_blc }}</td>
            </tr>
            <tr>
                <td>SER</td>
                @php
                    $total_tim_ser = @$ser->tim_pt1 + @$ser->tim_saber + @$ser->tim_bantek + @$ser->tim_mo;
                    $total_order_ser = (@$ser->order_pi + @$ser->order_risma + @$ser->order_mo + @$ser->order_pda + @$ser->order_hsi + @$ser->order_wms);

                    $progress_ser = ceil(@round((@$ser->order_prog + @$ser->order_kendala) / $total_order_ser * 100,2));
                    if (is_nan($progress_ser) == true)
                    {
                        $progress_ser = 100;
                    }
                    else
                    {
                        $progress_ser = $progress_ser;
                    }

                    if ($progress_ser < 100)
                    {
                        $class_progress_ser = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_ser = '';
                    }

                    if (@$ser->real_ps < 1)
                    {
                        $class_real_ps_ser = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_ser = '';
                    }

                    $load_order_ser = (@$ser->tim_pt1 + @$ser->tim_bantek) * 2 - $total_order_ser + @$ser->order_prog + @$ser->order_kendala;

                    if ($load_order_ser < '-1')
                    {
                        $ket_ser = 'OVERLOAD';
                        $class_ket_ser = 'class=bg-red';
                    }
                    elseif ($load_order_ser < 3)
                    {
                        $ket_ser = 'ORDER CUKUP';
                        $class_ket_ser = '';
                    }
                    else
                    {
                        $ket_ser = 'KURANG ORDER';
                        $class_ket_ser = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$ser->tim_pt1 ? : '0' }}</td>
                <td>{{ @$ser->tim_saber ? : '0' }}</td>
                <td>{{ @$ser->tim_bantek ? : '0' }}</td>
                <td>{{ @$ser->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_ser }}</td>
                <td>{{ @$ser->order_pi ? : '0' }}</td>
                <td>{{ @$ser->order_risma ? : '0' }}</td>
                <td>{{ @$ser->order_mo ? : '0' }}</td>
                <td>{{ @$ser->order_pda ? : '0' }}</td>
                <td>{{ @$ser->order_hsi ? : '0' }}</td>
                <td>{{ @$ser->order_wms ? : '0' }}</td>
                <td>{{ $total_order_ser }}</td>
                <td>{{ @$ser->order_prog ? : '0' }}</td>
                <td>{{ @$ser->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_ser }}>{{ $progress_ser }} %</td>
                <td {{ $class_real_ps_ser }}>{{ @$ser->real_ps ? : '0' }}</td>
                <td>{{ $load_order_ser }}</td>
                <td {{ $class_ket_ser }}>{{ $ket_ser }}</td>
            </tr>
            <tr>
                <td>TRJ</td>
                @php
                    $total_tim_trj = @$trj->tim_pt1 + @$trj->tim_saber + @$trj->tim_bantek + @$trj->tim_mo;
                    $total_order_trj = (@$trj->order_pi + @$trj->order_risma + @$trj->order_mo + @$trj->order_pda + @$trj->order_hsi + @$trj->order_wms);

                    $progress_trj = ceil(@round((@$trj->order_prog + @$trj->order_kendala) / $total_order_trj * 100,2));
                    if (is_nan($progress_trj) == true)
                    {
                        $progress_trj = 100;
                    }
                    else
                    {
                        $progress_trj = $progress_trj;
                    }

                    if ($progress_trj < 100)
                    {
                        $class_progress_trj = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_trj = '';
                    }

                    if (@$trj->real_ps < 1)
                    {
                        $class_real_ps_trj = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_trj = '';
                    }

                    $load_order_trj = (@$trj->tim_pt1 + @$trj->tim_bantek) * 2 - $total_order_trj + @$trj->order_prog + @$trj->order_kendala;

                    if ($load_order_trj < '-1')
                    {
                        $ket_trj = 'OVERLOAD';
                        $class_ket_trj = 'class=bg-red';
                    }
                    elseif ($load_order_trj < 3)
                    {
                        $ket_trj = 'ORDER CUKUP';
                        $class_ket_trj = '';
                    }
                    else
                    {
                        $ket_trj = 'KURANG ORDER';
                        $class_ket_trj = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$trj->tim_pt1 ? : '0' }}</td>
                <td>{{ @$trj->tim_saber ? : '0' }}</td>
                <td>{{ @$trj->tim_bantek ? : '0' }}</td>
                <td>{{ @$trj->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_trj }}</td>
                <td>{{ @$trj->order_pi ? : '0' }}</td>
                <td>{{ @$trj->order_risma ? : '0' }}</td>
                <td>{{ @$trj->order_mo ? : '0' }}</td>
                <td>{{ @$trj->order_pda ? : '0' }}</td>
                <td>{{ @$trj->order_hsi ? : '0' }}</td>
                <td>{{ @$trj->order_wms ? : '0' }}</td>
                <td>{{ $total_order_trj }}</td>
                <td>{{ @$trj->order_prog ? : '0' }}</td>
                <td>{{ @$trj->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_trj }}>{{ $progress_trj }} %</td>
                <td {{ $class_real_ps_trj }}>{{ @$trj->real_ps ? : '0' }}</td>
                <td>{{ $load_order_trj }}</td>
                <td {{ $class_ket_trj }}>{{ $ket_trj }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white">KOTABARU</th>
                <td>KPL</td>
                @php
                    $total_tim_kpl = @$kpl->tim_pt1 + @$kpl->tim_saber + @$kpl->tim_bantek + @$kpl->tim_mo;
                    $total_order_kpl = (@$kpl->order_pi + @$kpl->order_risma + @$kpl->order_mo + @$kpl->order_pda + @$kpl->order_hsi + @$kpl->order_wms);

                    $progress_kpl = ceil(@round((@$kpl->order_prog + @$kpl->order_kendala) / $total_order_kpl * 100,2));
                    if (is_nan($progress_kpl) == true)
                    {
                        $progress_kpl = 100;
                    }
                    else
                    {
                        $progress_kpl = $progress_kpl;
                    }

                    if ($progress_kpl < 100)
                    {
                        $class_progress_kpl = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_kpl = '';
                    }

                    if (@$kpl->real_ps < 1)
                    {
                        $class_real_ps_kpl = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_kpl = '';
                    }

                    $load_order_kpl = (@$kpl->tim_pt1 + @$kpl->tim_bantek) * 2 - $total_order_kpl + @$kpl->order_prog + @$kpl->order_kendala;

                    if ($load_order_kpl < '-1')
                    {
                        $ket_kpl = 'OVERLOAD';
                        $class_ket_kpl = 'class=bg-red';
                    }
                    elseif ($load_order_kpl < 3)
                    {
                        $ket_kpl = 'ORDER CUKUP';
                        $class_ket_kpl = '';
                    }
                    else
                    {
                        $ket_kpl = 'KURANG ORDER';
                        $class_ket_kpl = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$kpl->tim_pt1 ? : '0' }}</td>
                <td>{{ @$kpl->tim_saber ? : '0' }}</td>
                <td>{{ @$kpl->tim_bantek ? : '0' }}</td>
                <td>{{ @$kpl->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_kpl }}</td>
                <td>{{ @$kpl->order_pi ? : '0' }}</td>
                <td>{{ @$kpl->order_risma ? : '0' }}</td>
                <td>{{ @$kpl->order_mo ? : '0' }}</td>
                <td>{{ @$kpl->order_pda ? : '0' }}</td>
                <td>{{ @$kpl->order_hsi ? : '0' }}</td>
                <td>{{ @$kpl->order_wms ? : '0' }}</td>
                <td>{{ $total_order_kpl }}</td>
                <td>{{ @$kpl->order_prog ? : '0' }}</td>
                <td>{{ @$kpl->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_kpl }}>{{ $progress_kpl }} %</td>
                <td {{ $class_real_ps_kpl }}>{{ @$kpl->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$kpl->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_kpl }}</td>
                <td {{ $class_ket_kpl }}>{{ $ket_kpl }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">SATUI</th>
                <td>PGT</td>
                @php
                    $total_tim_pgt = @$pgt->tim_pt1 + @$pgt->tim_saber + @$pgt->tim_bantek + @$pgt->tim_mo;
                    $total_order_pgt = (@$pgt->order_pi + @$pgt->order_risma + @$pgt->order_mo + @$pgt->order_pda + @$pgt->order_hsi + @$pgt->order_wms);

                    $progress_pgt = ceil(@round((@$pgt->order_prog + @$pgt->order_kendala) / $total_order_pgt * 100,2));
                    if (is_nan($progress_pgt) == true)
                    {
                        $progress_pgt = 100;
                    }
                    else
                    {
                        $progress_pgt = $progress_pgt;
                    }

                    if ($progress_pgt < 100)
                    {
                        $class_progress_pgt = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_pgt = '';
                    }

                    if (@$pgt->real_ps < 1)
                    {
                        $class_real_ps_pgt = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_pgt = '';
                    }

                    $load_order_pgt = (@$pgt->tim_pt1 + @$pgt->tim_bantek) * 2 - $total_order_pgt + @$pgt->order_prog + @$pgt->order_kendala;

                    if ($load_order_pgt < '-1')
                    {
                        $ket_pgt = 'OVERLOAD';
                        $class_ket_pgt = 'class=bg-red';
                    }
                    elseif ($load_order_pgt < 3)
                    {
                        $ket_pgt = 'ORDER CUKUP';
                        $class_ket_pgt = '';
                    }
                    else
                    {
                        $ket_pgt = 'KURANG ORDER';
                        $class_ket_pgt = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$pgt->tim_pt1 ? : '0' }}</td>
                <td>{{ @$pgt->tim_saber ? : '0' }}</td>
                <td>{{ @$pgt->tim_bantek ? : '0' }}</td>
                <td>{{ @$pgt->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_pgt }}</td>
                <td>{{ @$pgt->order_pi ? : '0' }}</td>
                <td>{{ @$pgt->order_risma ? : '0' }}</td>
                <td>{{ @$pgt->order_mo ? : '0' }}</td>
                <td>{{ @$pgt->order_pda ? : '0' }}</td>
                <td>{{ @$pgt->order_hsi ? : '0' }}</td>
                <td>{{ @$pgt->order_wms ? : '0' }}</td>
                <td>{{ $total_order_pgt }}</td>
                <td>{{ @$pgt->order_prog ? : '0' }}</td>
                <td>{{ @$pgt->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_pgt }}>{{ $progress_pgt }} %</td>
                <td {{ $class_real_ps_pgt }}>{{ @$pgt->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="3">{{ @$pgtstikip->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_pgt }}</td>
                <td {{ $class_ket_pgt }}>{{ $ket_pgt }}</td>
            </tr>
            <tr>
                <td>STI</td>
                @php
                    $total_tim_sti = @$sti->tim_pt1 + @$sti->tim_saber + @$sti->tim_bantek + @$sti->tim_mo;
                    $total_order_sti = (@$sti->order_pi + @$sti->order_risma + @$sti->order_mo + @$sti->order_pda + @$sti->order_hsi + @$sti->order_wms);

                    $progress_sti = ceil(@round((@$sti->order_prog + @$sti->order_kendala) / $total_order_sti * 100,2));
                    if (is_nan($progress_sti) == true)
                    {
                        $progress_sti = 100;
                    }
                    else
                    {
                        $progress_sti = $progress_sti;
                    }

                    if ($progress_sti < 100)
                    {
                        $class_progress_sti = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_sti = '';
                    }

                    if (@$sti->real_ps < 1)
                    {
                        $class_real_ps_sti = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_sti = '';
                    }

                    $load_order_sti = (@$sti->tim_pt1 + @$sti->tim_bantek) * 2 - $total_order_sti + @$sti->order_prog + @$sti->order_kendala;

                    if ($load_order_sti < '-1')
                    {
                        $ket_sti = 'OVERLOAD';
                        $class_ket_sti = 'class=bg-red';
                    }
                    elseif ($load_order_sti < 3)
                    {
                        $ket_sti = 'ORDER CUKUP';
                        $class_ket_sti = '';
                    }
                    else
                    {
                        $ket_sti = 'KURANG ORDER';
                        $class_ket_sti = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$sti->tim_pt1 ? : '0' }}</td>
                <td>{{ @$sti->tim_saber ? : '0' }}</td>
                <td>{{ @$sti->tim_bantek ? : '0' }}</td>
                <td>{{ @$sti->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_sti }}</td>
                <td>{{ @$sti->order_pi ? : '0' }}</td>
                <td>{{ @$sti->order_risma ? : '0' }}</td>
                <td>{{ @$sti->order_mo ? : '0' }}</td>
                <td>{{ @$sti->order_pda ? : '0' }}</td>
                <td>{{ @$sti->order_hsi ? : '0' }}</td>
                <td>{{ @$sti->order_wms ? : '0' }}</td>
                <td>{{ $total_order_sti }}</td>
                <td>{{ @$sti->order_prog ? : '0' }}</td>
                <td>{{ @$sti->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_sti }}>{{ $progress_sti }} %</td>
                <td {{ $class_real_ps_sti }}>{{ @$sti->real_ps ? : '0' }}</td>
                <td>{{ $load_order_sti }}</td>
                <td {{ $class_ket_sti }}>{{ $ket_sti }}</td>
            </tr>
            <tr>
                <td>KIP</td>
                @php
                    $total_tim_kip = @$kip->tim_pt1 + @$kip->tim_saber + @$kip->tim_bantek + @$kip->tim_mo;
                    $total_order_kip = (@$kip->order_pi + @$kip->order_risma + @$kip->order_mo + @$kip->order_pda + @$kip->order_hsi + @$kip->order_wms);

                    $progress_kip = ceil(@round((@$kip->order_prog + @$kip->order_kendala) / $total_order_kip * 100,2));
                    if (is_nan($progress_kip) == true)
                    {
                        $progress_kip = 100;
                    }
                    else
                    {
                        $progress_kip = $progress_kip;
                    }

                    if ($progress_kip < 100)
                    {
                        $class_progress_kip = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_kip = '';
                    }

                    if (@$kip->real_ps < 1)
                    {
                        $class_real_ps_kip = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_kip = '';
                    }

                    $load_order_kip = (@$kip->tim_pt1 + @$kip->tim_bantek) * 2 - $total_order_kip + @$kip->order_prog + @$kip->order_kendala;

                    if ($load_order_kip < '-1')
                    {
                        $ket_kip = 'OVERLOAD';
                        $class_ket_kip = 'class=bg-red';
                    }
                    elseif ($load_order_kip < 3)
                    {
                        $ket_kip = 'ORDER CUKUP';
                        $class_ket_kip = '';
                    }
                    else
                    {
                        $ket_kip = 'KURANG ORDER';
                        $class_ket_kip = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$kip->tim_pt1 ? : '0' }}</td>
                <td>{{ @$kip->tim_saber ? : '0' }}</td>
                <td>{{ @$kip->tim_bantek ? : '0' }}</td>
                <td>{{ @$kip->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_kip }}</td>
                <td>{{ @$kip->order_pi ? : '0' }}</td>
                <td>{{ @$kip->order_risma ? : '0' }}</td>
                <td>{{ @$kip->order_mo ? : '0' }}</td>
                <td>{{ @$kip->order_pda ? : '0' }}</td>
                <td>{{ @$kip->order_hsi ? : '0' }}</td>
                <td>{{ @$kip->order_wms ? : '0' }}</td>
                <td>{{ $total_order_kip }}</td>
                <td>{{ @$kip->order_prog ? : '0' }}</td>
                <td>{{ @$kip->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_kip }}>{{ $progress_kip }} %</td>
                <td {{ $class_real_ps_kip }}>{{ @$kip->real_ps ? : '0' }}</td>
                <td>{{ $load_order_kip }}</td>
                <td {{ $class_ket_kip }}>{{ $ket_kip }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">TJL AREA</th>
                <td>TJL</td>
                @php
                    $total_tim_tjl = @$tjl->tim_pt1 + @$tjl->tim_saber + @$tjl->tim_bantek + @$tjl->tim_mo;
                    $total_order_tjl = (@$tjl->order_pi + @$tjl->order_risma + @$tjl->order_mo + @$tjl->order_pda + @$tjl->order_hsi + @$tjl->order_wms);

                    $progress_tjl = ceil(@round((@$tjl->order_prog + @$tjl->order_kendala) / $total_order_tjl * 100,2));
                    if (is_nan($progress_tjl) == true)
                    {
                        $progress_tjl = 100;
                    }
                    else
                    {
                        $progress_tjl = $progress_tjl;
                    }

                    if ($progress_tjl < 100)
                    {
                        $class_progress_tjl = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_tjl = '';
                    }

                    if (@$tjl->real_ps < 1)
                    {
                        $class_real_ps_tjl = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_tjl = '';
                    }

                    $load_order_tjl = (@$tjl->tim_pt1 + @$tjl->tim_bantek) * 2 - $total_order_tjl + @$tjl->order_prog + @$tjl->order_kendala;

                    if ($load_order_tjl < '-1')
                    {
                        $ket_tjl = 'OVERLOAD';
                        $class_ket_tjl = 'class=bg-red';
                    }
                    elseif ($load_order_tjl < 3)
                    {
                        $ket_tjl = 'ORDER CUKUP';
                        $class_ket_tjl = '';
                    }
                    else
                    {
                        $ket_tjl = 'KURANG ORDER';
                        $class_ket_tjl = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$tjl->tim_pt1 ? : '0' }}</td>
                <td>{{ @$tjl->tim_saber ? : '0' }}</td>
                <td>{{ @$tjl->tim_bantek ? : '0' }}</td>
                <td>{{ @$tjl->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_tjl }}</td>
                <td>{{ @$tjl->order_pi ? : '0' }}</td>
                <td>{{ @$tjl->order_risma ? : '0' }}</td>
                <td>{{ @$tjl->order_mo ? : '0' }}</td>
                <td>{{ @$tjl->order_pda ? : '0' }}</td>
                <td>{{ @$tjl->order_hsi ? : '0' }}</td>
                <td>{{ @$tjl->order_wms ? : '0' }}</td>
                <td>{{ $total_order_tjl }}</td>
                <td>{{ @$tjl->order_prog ? : '0' }}</td>
                <td>{{ @$tjl->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_tjl }}>{{ $progress_tjl }} %</td>
                <td {{ $class_real_ps_tjl }}>{{ @$tjl->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="3">{{ @$tjlpgnamt->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_tjl }}</td>
                <td {{ $class_ket_tjl }}>{{ $ket_tjl }}</td>
            </tr>
            <tr>
                <td>PGN</td>
                @php
                    $total_tim_pgn = @$pgn->tim_pt1 + @$pgn->tim_saber + @$pgn->tim_bantek + @$pgn->tim_mo;
                    $total_order_pgn = (@$pgn->order_pi + @$pgn->order_risma + @$pgn->order_mo + @$pgn->order_pda + @$pgn->order_hsi + @$pgn->order_wms);

                    $progress_pgn = ceil(@round((@$pgn->order_prog + @$pgn->order_kendala) / $total_order_pgn * 100,2));
                    if (is_nan($progress_pgn) == true)
                    {
                        $progress_pgn = 100;
                    }
                    else
                    {
                        $progress_pgn = $progress_pgn;
                    }

                    if ($progress_pgn < 100)
                    {
                        $class_progress_pgn = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_pgn = '';
                    }

                    if (@$pgn->real_ps < 1)
                    {
                        $class_real_ps_pgn = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_pgn = '';
                    }

                    $load_order_pgn = (@$pgn->tim_pt1 + @$pgn->tim_bantek) * 2 - $total_order_pgn + @$pgn->order_prog + @$pgn->order_kendala;

                    if ($load_order_pgn < '-1')
                    {
                        $ket_pgn = 'OVERLOAD';
                        $class_ket_pgn = 'class=bg-red';
                    }
                    elseif ($load_order_pgn < 3)
                    {
                        $ket_pgn = 'ORDER CUKUP';
                        $class_ket_pgn = '';
                    }
                    else
                    {
                        $ket_pgn = 'KURANG ORDER';
                        $class_ket_pgn = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$pgn->tim_pt1 ? : '0' }}</td>
                <td>{{ @$pgn->tim_saber ? : '0' }}</td>
                <td>{{ @$pgn->tim_bantek ? : '0' }}</td>
                <td>{{ @$pgn->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_pgn }}</td>
                <td>{{ @$pgn->order_pi ? : '0' }}</td>
                <td>{{ @$pgn->order_risma ? : '0' }}</td>
                <td>{{ @$pgn->order_mo ? : '0' }}</td>
                <td>{{ @$pgn->order_pda ? : '0' }}</td>
                <td>{{ @$pgn->order_hsi ? : '0' }}</td>
                <td>{{ @$pgn->order_wms ? : '0' }}</td>
                <td>{{ $total_order_pgn }}</td>
                <td>{{ @$pgn->order_prog ? : '0' }}</td>
                <td>{{ @$pgn->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_pgn }}>{{ $progress_pgn }} %</td>
                <td {{ $class_real_ps_pgn }}>{{ @$pgn->real_ps ? : '0' }}</td>
                <td>{{ $load_order_pgn }}</td>
                <td {{ $class_ket_pgn }}>{{ $ket_pgn }}</td>
            </tr>
            <tr>
                <td>AMT</td>
                @php
                    $total_tim_amt = @$amt->tim_pt1 + @$amt->tim_saber + @$amt->tim_bantek + @$amt->tim_mo;
                    $total_order_amt = (@$amt->order_pi + @$amt->order_risma + @$amt->order_mo + @$amt->order_pda + @$amt->order_hsi + @$amt->order_wms);

                    $progress_amt = ceil(@round((@$amt->order_prog + @$amt->order_kendala) / $total_order_amt * 100,2));
                    if (is_nan($progress_amt) == true)
                    {
                        $progress_amt = 100;
                    }
                    else
                    {
                        $progress_amt = $progress_amt;
                    }

                    if ($progress_amt < 100)
                    {
                        $class_progress_amt = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_amt = '';
                    }

                    if (@$amt->real_ps < 1)
                    {
                        $class_real_ps_amt = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_amt = '';
                    }

                    $load_order_amt = (@$amt->tim_pt1 + @$amt->tim_bantek) * 2 - $total_order_amt + @$amt->order_prog + @$amt->order_kendala;

                    if ($load_order_amt < '-1')
                    {
                        $ket_amt = 'OVERLOAD';
                        $class_ket_amt = 'class=bg-red';
                    }
                    elseif ($load_order_amt < 3)
                    {
                        $ket_amt = 'ORDER CUKUP';
                        $class_ket_amt = '';
                    }
                    else
                    {
                        $ket_amt = 'KURANG ORDER';
                        $class_ket_amt = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$amt->tim_pt1 ? : '0' }}</td>
                <td>{{ @$amt->tim_saber ? : '0' }}</td>
                <td>{{ @$amt->tim_bantek ? : '0' }}</td>
                <td>{{ @$amt->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_amt }}</td>
                <td>{{ @$amt->order_pi ? : '0' }}</td>
                <td>{{ @$amt->order_risma ? : '0' }}</td>
                <td>{{ @$amt->order_mo ? : '0' }}</td>
                <td>{{ @$amt->order_pda ? : '0' }}</td>
                <td>{{ @$amt->order_hsi ? : '0' }}</td>
                <td>{{ @$amt->order_wms ? : '0' }}</td>
                <td>{{ $total_order_amt }}</td>
                <td>{{ @$amt->order_prog ? : '0' }}</td>
                <td>{{ @$amt->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_amt }}>{{ $progress_amt }} %</td>
                <td {{ $class_real_ps_amt }}>{{ @$amt->real_ps ? : '0' }}</td>
                <td>{{ $load_order_amt }}</td>
                <td {{ $class_ket_amt }}>{{ $ket_amt }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="2">MTP</th>
                <td>MTP</td>
                @php
                    $total_tim_mtp = @$mtp->tim_pt1 + @$mtp->tim_saber + @$mtp->tim_bantek + @$mtp->tim_mo;
                    $total_order_mtp = (@$mtp->order_pi + @$mtp->order_risma + @$mtp->order_mo + @$mtp->order_pda + @$mtp->order_hsi + @$mtp->order_wms);

                    $progress_mtp = ceil(@round((@$mtp->order_prog + @$mtp->order_kendala) / $total_order_mtp * 100,2));
                    if (is_nan($progress_mtp) == true)
                    {
                        $progress_mtp = 100;
                    }
                    else
                    {
                        $progress_mtp = $progress_mtp;
                    }

                    if ($progress_mtp < 100)
                    {
                        $class_progress_mtp = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_mtp = '';
                    }

                    if (@$mtp->real_ps < 1)
                    {
                        $class_real_ps_mtp = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_mtp = '';
                    }

                    $load_order_mtp = (@$mtp->tim_pt1 + @$mtp->tim_bantek) * 2 - $total_order_mtp + @$mtp->order_prog + @$mtp->order_kendala;

                    if ($load_order_mtp < '-1')
                    {
                        $ket_mtp = 'OVERLOAD';
                        $class_ket_mtp = 'class=bg-red';
                    }
                    elseif ($load_order_mtp < 3)
                    {
                        $ket_mtp = 'ORDER CUKUP';
                        $class_ket_mtp = '';
                    }
                    else
                    {
                        $ket_mtp = 'KURANG ORDER';
                        $class_ket_mtp = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$mtp->tim_pt1 ? : '0' }}</td>
                <td>{{ @$mtp->tim_saber ? : '0' }}</td>
                <td>{{ @$mtp->tim_bantek ? : '0' }}</td>
                <td>{{ @$mtp->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_mtp }}</td>
                <td>{{ @$mtp->order_pi ? : '0' }}</td>
                <td>{{ @$mtp->order_risma ? : '0' }}</td>
                <td>{{ @$mtp->order_mo ? : '0' }}</td>
                <td>{{ @$mtp->order_pda ? : '0' }}</td>
                <td>{{ @$mtp->order_hsi ? : '0' }}</td>
                <td>{{ @$mtp->order_wms ? : '0' }}</td>
                <td>{{ $total_order_mtp }}</td>
                <td>{{ @$mtp->order_prog ? : '0' }}</td>
                <td>{{ @$mtp->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_mtp }}>{{ $progress_mtp }} %</td>
                <td {{ $class_real_ps_mtp }}>{{ @$mtp->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="2">{{ @$mtpmrb->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_mtp }}</td>
                <td {{ $class_ket_mtp }}>{{ $ket_mtp }}</td>
            </tr>
            <tr>
                <td>MRB</td>
                @php
                    $total_tim_mrb = @$mrb->tim_pt1 + @$mrb->tim_saber + @$mrb->tim_bantek + @$mrb->tim_mo;
                    $total_order_mrb = (@$mrb->order_pi + @$mrb->order_risma + @$mrb->order_mo + @$mrb->order_pda + @$mrb->order_hsi + @$mrb->order_wms);

                    $progress_mrb = ceil(@round((@$mrb->order_prog + @$mrb->order_kendala) / $total_order_mrb * 100,2));
                    if (is_nan($progress_mrb) == true)
                    {
                        $progress_mrb = 100;
                    }
                    else
                    {
                        $progress_mrb = $progress_mrb;
                    }

                    if ($progress_mrb < 100)
                    {
                        $class_progress_mrb = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_mrb = '';
                    }

                    if (@$mrb->real_ps < 1)
                    {
                        $class_real_ps_mrb = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_mrb = '';
                    }

                    $load_order_mrb = (@$mrb->tim_pt1 + @$mrb->tim_bantek) * 2 - $total_order_mrb + @$mrb->order_prog + @$mrb->order_kendala;

                    if ($load_order_mrb < '-1')
                    {
                        $ket_mrb = 'OVERLOAD';
                        $class_ket_mrb = 'class=bg-red';
                    }
                    elseif ($load_order_mrb < 3)
                    {
                        $ket_mrb = 'ORDER CUKUP';
                        $class_ket_mrb = '';
                    }
                    else
                    {
                        $ket_mrb = 'KURANG ORDER';
                        $class_ket_mrb = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$mrb->tim_pt1 ? : '0' }}</td>
                <td>{{ @$mrb->tim_saber ? : '0' }}</td>
                <td>{{ @$mrb->tim_bantek ? : '0' }}</td>
                <td>{{ @$mrb->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_mrb }}</td>
                <td>{{ @$mrb->order_pi ? : '0' }}</td>
                <td>{{ @$mrb->order_risma ? : '0' }}</td>
                <td>{{ @$mrb->order_mo ? : '0' }}</td>
                <td>{{ @$mrb->order_pda ? : '0' }}</td>
                <td>{{ @$mrb->order_hsi ? : '0' }}</td>
                <td>{{ @$mrb->order_wms ? : '0' }}</td>
                <td>{{ $total_order_mrb }}</td>
                <td>{{ @$mrb->order_prog ? : '0' }}</td>
                <td>{{ @$mrb->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_mrb }}>{{ $progress_mrb }} %</td>
                <td {{ $class_real_ps_mrb }}>{{ @$mrb->real_ps ? : '0' }}</td>
                <td>{{ $load_order_mrb }}</td>
                <td {{ $class_ket_mrb }}>{{ $ket_mrb }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="5">GTM</th>
                <th class="text-center align-middle bg-white">LUL</th>
                <td>LUL</td>
                @php
                    $total_tim_lul = @$lul->tim_pt1 + @$lul->tim_saber + @$lul->tim_bantek + @$lul->tim_mo;
                    $total_order_lul = (@$lul->order_pi + @$lul->order_risma + @$lul->order_mo + @$lul->order_pda + @$lul->order_hsi + @$lul->order_wms);

                    $progress_lul = ceil(@round((@$lul->order_prog + @$lul->order_kendala) / $total_order_lul * 100,2));
                    if (is_nan($progress_lul) == true)
                    {
                        $progress_lul = 100;
                    }
                    else
                    {
                        $progress_lul = $progress_lul;
                    }

                    if ($progress_lul < 100)
                    {
                        $class_progress_lul = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_lul = '';
                    }

                    if (@$lul->real_ps < 1)
                    {
                        $class_real_ps_lul = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_lul = '';
                    }

                    $load_order_lul = (@$lul->tim_pt1 + @$lul->tim_bantek) * 2 - $total_order_lul + @$lul->order_prog + @$lul->order_kendala;

                    if ($load_order_lul < '-1')
                    {
                        $ket_lul = 'OVERLOAD';
                        $class_ket_lul = 'class=bg-red';
                    }
                    elseif ($load_order_lul < 3)
                    {
                        $ket_lul = 'ORDER CUKUP';
                        $class_ket_lul = '';
                    }
                    else
                    {
                        $ket_lul = 'KURANG ORDER';
                        $class_ket_lul = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$lul->tim_pt1 ? : '0' }}</td>
                <td>{{ @$lul->tim_saber ? : '0' }}</td>
                <td>{{ @$lul->tim_bantek ? : '0' }}</td>
                <td>{{ @$lul->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_lul }}</td>
                <td>{{ @$lul->order_pi ? : '0' }}</td>
                <td>{{ @$lul->order_risma ? : '0' }}</td>
                <td>{{ @$lul->order_mo ? : '0' }}</td>
                <td>{{ @$lul->order_pda ? : '0' }}</td>
                <td>{{ @$lul->order_hsi ? : '0' }}</td>
                <td>{{ @$lul->order_wms ? : '0' }}</td>
                <td>{{ $total_order_lul }}</td>
                <td>{{ @$lul->order_prog ? : '0' }}</td>
                <td>{{ @$lul->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_lul }}>{{ $progress_lul }} %</td>
                <td {{ $class_real_ps_lul }}>{{ @$lul->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$lul->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_lul }}</td>
                <td {{ $class_ket_lul }}>{{ $ket_lul }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white">BARABAI</th>
                <td>BRI</td>
                @php
                    $total_tim_bri = @$bri->tim_pt1 + @$bri->tim_saber + @$bri->tim_bantek + @$bri->tim_mo;
                    $total_order_bri = (@$bri->order_pi + @$bri->order_risma + @$bri->order_mo + @$bri->order_pda + @$bri->order_hsi + @$bri->order_wms);

                    $progress_bri = ceil(@round((@$bri->order_prog + @$bri->order_kendala) / $total_order_bri * 100,2));
                    if (is_nan($progress_bri) == true)
                    {
                        $progress_bri = 100;
                    }
                    else
                    {
                        $progress_bri = $progress_bri;
                    }

                    if ($progress_bri < 100)
                    {
                        $class_progress_bri = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_bri = '';
                    }

                    if (@$bri->real_ps < 1)
                    {
                        $class_real_ps_bri = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_bri = '';
                    }

                    $load_order_bri = (@$bri->tim_pt1 + @$bri->tim_bantek) * 2 - $total_order_bri + @$bri->order_prog + @$bri->order_kendala;

                    if ($load_order_bri < '-1')
                    {
                        $ket_bri = 'OVERLOAD';
                        $class_ket_bri = 'class=bg-red';
                    }
                    elseif ($load_order_bri < 3)
                    {
                        $ket_bri = 'ORDER CUKUP';
                        $class_ket_bri = '';
                    }
                    else
                    {
                        $ket_bri = 'KURANG ORDER';
                        $class_ket_bri = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$bri->tim_pt1 ? : '0' }}</td>
                <td>{{ @$bri->tim_saber ? : '0' }}</td>
                <td>{{ @$bri->tim_bantek ? : '0' }}</td>
                <td>{{ @$bri->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_bri }}</td>
                <td>{{ @$bri->order_pi ? : '0' }}</td>
                <td>{{ @$bri->order_risma ? : '0' }}</td>
                <td>{{ @$bri->order_mo ? : '0' }}</td>
                <td>{{ @$bri->order_pda ? : '0' }}</td>
                <td>{{ @$bri->order_hsi ? : '0' }}</td>
                <td>{{ @$bri->order_wms ? : '0' }}</td>
                <td>{{ $total_order_bri }}</td>
                <td>{{ @$bri->order_prog ? : '0' }}</td>
                <td>{{ @$bri->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_bri }}>{{ $progress_bri }} %</td>
                <td {{ $class_real_ps_bri }}>{{ @$bri->real_ps ? : '0' }}</td>
                <td class="bg-yellow">{{ @$bri->komitmen_ps ? : '0' }}</td>
                <td>{{ $load_order_bri }}</td>
                <td {{ $class_ket_bri }}>{{ $ket_bri }}</td>
            </tr>
                <th class="text-center align-middle bg-white" rowspan="3">KANDANGAN</th>
                <td>KDG</td>
                @php
                    $total_tim_kdg = @$kdg->tim_pt1 + @$kdg->tim_saber + @$kdg->tim_bantek + @$kdg->tim_mo;
                    $total_order_kdg = (@$kdg->order_pi + @$kdg->order_risma + @$kdg->order_mo + @$kdg->order_pda + @$kdg->order_hsi + @$kdg->order_wms);

                    $progress_kdg = ceil(@round((@$kdg->order_prog + @$kdg->order_kendala) / $total_order_kdg * 100,2));
                    if (is_nan($progress_kdg) == true)
                    {
                        $progress_kdg = 100;
                    }
                    else
                    {
                        $progress_kdg = $progress_kdg;
                    }

                    if ($progress_kdg < 100)
                    {
                        $class_progress_kdg = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_kdg = '';
                    }

                    if (@$kdg->real_ps < 1)
                    {
                        $class_real_ps_kdg = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_kdg = '';
                    }

                    $load_order_kdg = (@$kdg->tim_pt1 + @$kdg->tim_bantek) * 2 - $total_order_kdg + @$kdg->order_prog + @$kdg->order_kendala;

                    if ($load_order_kdg < '-1')
                    {
                        $ket_kdg = 'OVERLOAD';
                        $class_ket_kdg = 'class=bg-red';
                    }
                    elseif ($load_order_kdg < 3)
                    {
                        $ket_kdg = 'ORDER CUKUP';
                        $class_ket_kdg = '';
                    }
                    else
                    {
                        $ket_kdg = 'KURANG ORDER';
                        $class_ket_kdg = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$kdg->tim_pt1 ? : '0' }}</td>
                <td>{{ @$kdg->tim_saber ? : '0' }}</td>
                <td>{{ @$kdg->tim_bantek ? : '0' }}</td>
                <td>{{ @$kdg->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_kdg }}</td>
                <td>{{ @$kdg->order_pi ? : '0' }}</td>
                <td>{{ @$kdg->order_risma ? : '0' }}</td>
                <td>{{ @$kdg->order_mo ? : '0' }}</td>
                <td>{{ @$kdg->order_pda ? : '0' }}</td>
                <td>{{ @$kdg->order_hsi ? : '0' }}</td>
                <td>{{ @$kdg->order_wms ? : '0' }}</td>
                <td>{{ $total_order_kdg }}</td>
                <td>{{ @$kdg->order_prog ? : '0' }}</td>
                <td>{{ @$kdg->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_kdg }}>{{ $progress_kdg }} %</td>
                <td {{ $class_real_ps_kdg }}>{{ @$kdg->real_ps ? : '0' }}</td>
                <th class="text-center align-middle bg-yellow" rowspan="3">{{ @$kdgnegrta->komitmen_ps ? : '0' }}</th>
                <td>{{ $load_order_kdg }}</td>
                <td {{ $class_ket_kdg }}>{{ $ket_kdg }}</td>
            </tr>
            <tr>
                <td>NEG</td>
                @php
                    $total_tim_neg = @$neg->tim_pt1 + @$neg->tim_saber + @$neg->tim_bantek + @$neg->tim_mo;
                    $total_order_neg = (@$neg->order_pi + @$neg->order_risma + @$neg->order_mo + @$neg->order_pda + @$neg->order_hsi + @$neg->order_wms);

                    $progress_neg = ceil(@round((@$neg->order_prog + @$neg->order_kendala) / $total_order_neg * 100,2));
                    if (is_nan($progress_neg) == true)
                    {
                        $progress_neg = 100;
                    }
                    else
                    {
                        $progress_neg = $progress_neg;
                    }

                    if ($progress_neg < 100)
                    {
                        $class_progress_neg = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_neg = '';
                    }

                    if (@$neg->real_ps < 1)
                    {
                        $class_real_ps_neg = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_neg = '';
                    }

                    $load_order_neg = (@$neg->tim_pt1 + @$neg->tim_bantek) * 2 - $total_order_neg + @$neg->order_prog + @$neg->order_kendala;

                    if ($load_order_neg < '-1')
                    {
                        $ket_neg = 'OVERLOAD';
                        $class_ket_neg = 'class=bg-red';
                    }
                    elseif ($load_order_neg < 3)
                    {
                        $ket_neg = 'ORDER CUKUP';
                        $class_ket_neg = '';
                    }
                    else
                    {
                        $ket_neg = 'KURANG ORDER';
                        $class_ket_neg = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$neg->tim_pt1 ? : '0' }}</td>
                <td>{{ @$neg->tim_saber ? : '0' }}</td>
                <td>{{ @$neg->tim_bantek ? : '0' }}</td>
                <td>{{ @$neg->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_neg }}</td>
                <td>{{ @$neg->order_pi ? : '0' }}</td>
                <td>{{ @$neg->order_risma ? : '0' }}</td>
                <td>{{ @$neg->order_mo ? : '0' }}</td>
                <td>{{ @$neg->order_pda ? : '0' }}</td>
                <td>{{ @$neg->order_hsi ? : '0' }}</td>
                <td>{{ @$neg->order_wms ? : '0' }}</td>
                <td>{{ $total_order_neg }}</td>
                <td>{{ @$neg->order_prog ? : '0' }}</td>
                <td>{{ @$neg->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_neg }}>{{ $progress_neg }} %</td>
                <td {{ $class_real_ps_neg }}>{{ @$neg->real_ps ? : '0' }}</td>
                <td>{{ $load_order_neg }}</td>
                <td {{ $class_ket_neg }}>{{ $ket_neg }}</td>
            </tr>
            <tr>
                <td>RTA</td>
                @php
                    $total_tim_rta = @$rta->tim_pt1 + @$rta->tim_saber + @$rta->tim_bantek + @$rta->tim_mo;
                    $total_order_rta = (@$rta->order_pi + @$rta->order_risma + @$rta->order_mo + @$rta->order_pda + @$rta->order_hsi + @$rta->order_wms);

                    $progress_rta = ceil(@round((@$rta->order_prog + @$rta->order_kendala) / $total_order_rta * 100,2));
                    if (is_nan($progress_rta) == true)
                    {
                        $progress_rta = 100;
                    }
                    else
                    {
                        $progress_rta = $progress_rta;
                    }

                    if ($progress_rta < 100)
                    {
                        $class_progress_rta = 'class=bg-red';
                    }
                    else
                    {
                        $class_progress_rta = '';
                    }

                    if (@$rta->real_ps < 1)
                    {
                        $class_real_ps_rta = 'class=bg-red';
                    }
                    else
                    {
                        $class_real_ps_rta = '';
                    }

                    $load_order_rta = (@$rta->tim_pt1 + @$rta->tim_bantek) * 2 - $total_order_rta + @$rta->order_prog + @$rta->order_kendala;

                    if ($load_order_rta < '-1')
                    {
                        $ket_rta = 'OVERLOAD';
                        $class_ket_rta = 'class=bg-red';
                    }
                    elseif ($load_order_rta < 3)
                    {
                        $ket_rta = 'ORDER CUKUP';
                        $class_ket_rta = '';
                    }
                    else
                    {
                        $ket_rta = 'KURANG ORDER';
                        $class_ket_rta = 'class=bg-green';
                    }
                @endphp
                <td>{{ @$rta->tim_pt1 ? : '0' }}</td>
                <td>{{ @$rta->tim_saber ? : '0' }}</td>
                <td>{{ @$rta->tim_bantek ? : '0' }}</td>
                <td>{{ @$rta->tim_mo ? : '0' }}</td>
                <td>{{ $total_tim_rta }}</td>
                <td>{{ @$rta->order_pi ? : '0' }}</td>
                <td>{{ @$rta->order_risma ? : '0' }}</td>
                <td>{{ @$rta->order_mo ? : '0' }}</td>
                <td>{{ @$rta->order_pda ? : '0' }}</td>
                <td>{{ @$rta->order_hsi ? : '0' }}</td>
                <td>{{ @$rta->order_wms ? : '0' }}</td>
                <td>{{ $total_order_rta }}</td>
                <td>{{ @$rta->order_prog ? : '0' }}</td>
                <td>{{ @$rta->order_kendala ? : '0' }}</td>
                <td {{ $class_progress_rta }}>{{ $progress_rta }} %</td>
                <td {{ $class_real_ps_rta }}>{{ @$rta->real_ps ? : '0' }}</td>
                <td>{{ $load_order_rta }}</td>
                <td {{ $class_ket_rta }}>{{ $ket_rta }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">TOTAL</th>
                <th>{{ @$bjm->tim_pt1 + @$kyg->tim_pt1 + @$bjmkyg->tim_pt1 + @$uli->tim_pt1 + @$gmb->tim_pt1 + @$uligmb->tim_pt1 + @$bbr->tim_pt1 + @$ple->tim_pt1 + @$btb->tim_pt1 + @$tki->tim_pt1 + @$plebtbtki->tim_pt1 + @$blc->tim_pt1 + @$ser->tim_pt1 + @$trj->tim_pt1 + @$blcsertrj->tim_pt1 + @$kpl->tim_pt1 + @$pgt->tim_pt1 + @$sti->tim_pt1 + @$kip->tim_pt1 + @$pgtstikip->tim_pt1 + @$tjl->tim_pt1 + @$pgn->tim_pt1 + @$amt->tim_pt1 + @$tjlpgnamt->tim_pt1 + @$mtp->tim_pt1 + @$mrb->tim_pt1 + @$mtpmrb->tim_pt1 + @$lul->tim_pt1 + @$bri->tim_pt1 + @$kdg->tim_pt1 + @$neg->tim_pt1 + @$kdgneg->tim_pt1 + @$rta->tim_pt1 + @$kdgnegrta->tim_pt1 }}</th>
                <th>{{ @$bjm->tim_saber + @$kyg->tim_saber + @$bjmkyg->tim_saber + @$uli->tim_saber + @$gmb->tim_saber + @$uligmb->tim_saber + @$bbr->tim_saber + @$ple->tim_saber + @$btb->tim_saber + @$tki->tim_saber + @$plebtbtki->tim_saber + @$blc->tim_saber + @$ser->tim_saber + @$trj->tim_saber + @$blcsertrj->tim_saber + @$kpl->tim_saber + @$pgt->tim_saber + @$sti->tim_saber + @$kip->tim_saber + @$pgtstikip->tim_saber + @$tjl->tim_saber + @$pgn->tim_saber + @$amt->tim_saber + @$tjlpgnamt->tim_saber + @$mtp->tim_saber + @$mrb->tim_saber + @$mtpmrb->tim_saber + @$lul->tim_saber + @$bri->tim_saber + @$kdg->tim_saber + @$neg->tim_saber + @$kdgneg->tim_saber + @$rta->tim_saber + @$kdgnegrta->tim_saber }}</th>
                <th>{{ @$bjm->tim_bantek + @$kyg->tim_bantek + @$bjmkyg->tim_bantek + @$uli->tim_bantek + @$gmb->tim_bantek + @$uligmb->tim_bantek + @$bbr->tim_bantek + @$ple->tim_bantek + @$btb->tim_bantek + @$tki->tim_bantek + @$plebtbtki->tim_bantek + @$blc->tim_bantek + @$ser->tim_bantek + @$trj->tim_bantek + @$blcsertrj->tim_bantek + @$kpl->tim_bantek + @$pgt->tim_bantek + @$sti->tim_bantek + @$kip->tim_bantek + @$pgtstikip->tim_bantek + @$tjl->tim_bantek + @$pgn->tim_bantek + @$amt->tim_bantek + @$tjlpgnamt->tim_bantek + @$mtp->tim_bantek + @$mrb->tim_bantek + @$mtpmrb->tim_bantek + @$lul->tim_bantek + @$bri->tim_bantek + @$kdg->tim_bantek + @$neg->tim_bantek + @$kdgneg->tim_bantek + @$rta->tim_bantek + @$kdgnegrta->tim_bantek }}</th>
                <th>{{ @$bjm->tim_mo + @$kyg->tim_mo + @$bjmkyg->tim_mo + @$uli->tim_mo + @$gmb->tim_mo + @$uligmb->tim_mo + @$bbr->tim_mo + @$ple->tim_mo + @$btb->tim_mo + @$tki->tim_mo + @$plebtbtki->tim_mo + @$blc->tim_mo + @$ser->tim_mo + @$trj->tim_mo + @$blcsertrj->tim_mo + @$kpl->tim_mo + @$pgt->tim_mo + @$sti->tim_mo + @$kip->tim_mo + @$pgtstikip->tim_mo + @$tjl->tim_mo + @$pgn->tim_mo + @$amt->tim_mo + @$tjlpgnamt->tim_mo + @$mtp->tim_mo + @$mrb->tim_mo + @$mtpmrb->tim_mo + @$lul->tim_mo + @$bri->tim_mo + @$kdg->tim_mo + @$neg->tim_mo + @$kdgneg->tim_mo + @$rta->tim_mo + @$kdgnegrta->tim_mo }}</th>
                <th>{{ @$total_tim_bjm + @$total_tim_kyg + @$total_tim_bjmkyg + @$total_tim_uli + @$total_tim_gmb + @$total_tim_uligmb + @$total_tim_bbr + @$total_tim_ple + @$total_tim_btb + @$total_tim_tki + @$total_tim_plebtbtki + @$total_tim_blc + @$total_tim_ser + @$total_tim_trj + @$total_tim_blcsertrj + @$total_tim_kpl + @$total_tim_pgt + @$total_tim_sti + @$total_tim_kip + @$total_tim_pgtstikip + @$total_tim_tjl + @$total_tim_pgn + @$total_tim_amt + @$total_tim_tjlpgnamt + @$total_tim_mtp + @$total_tim_mrb + @$total_tim_mtpmrb + @$total_tim_lul + @$total_tim_bri + @$total_tim_kdg + @$total_tim_neg + @$total_tim_kdgneg + @$total_tim_rta + @$total_tim_kdgnegrta }}</th>
                <th>{{ @$bjm->order_pi + @$kyg->order_pi + @$bjmkyg->order_pi + @$uli->order_pi + @$gmb->order_pi + @$uligmb->order_pi + @$bbr->order_pi + @$ple->order_pi + @$btb->order_pi + @$tki->order_pi + @$plebtbtki->order_pi + @$blc->order_pi + @$ser->order_pi + @$trj->order_pi + @$blcsertrj->order_pi + @$kpl->order_pi + @$pgt->order_pi + @$sti->order_pi + @$kip->order_pi + @$pgtstikip->order_pi + @$tjl->order_pi + @$pgn->order_pi + @$amt->order_pi + @$tjlpgnamt->order_pi + @$mtp->order_pi + @$mrb->order_pi + @$mtpmrb->order_pi + @$lul->order_pi + @$bri->order_pi + @$kdg->order_pi + @$neg->order_pi + @$kdgneg->order_pi + @$rta->order_pi + @$kdgnegrta->order_pi }}</th>
                <th>{{ @$bjm->order_risma + @$kyg->order_risma + @$bjmkyg->order_risma + @$uli->order_risma + @$gmb->order_risma + @$uligmb->order_risma + @$bbr->order_risma + @$ple->order_risma + @$btb->order_risma + @$tki->order_risma + @$plebtbtki->order_risma + @$blc->order_risma + @$ser->order_risma + @$trj->order_risma + @$blcsertrj->order_risma + @$kpl->order_risma + @$pgt->order_risma + @$sti->order_risma + @$kip->order_risma + @$pgtstikip->order_risma + @$tjl->order_risma + @$pgn->order_risma + @$amt->order_risma + @$tjlpgnamt->order_risma + @$mtp->order_risma + @$mrb->order_risma + @$mtpmrb->order_risma + @$lul->order_risma + @$bri->order_risma + @$kdg->order_risma + @$neg->order_risma + @$kdgneg->order_risma + @$rta->order_risma + @$kdgnegrta->order_risma }}</th>
                <th>{{ @$bjm->order_mo + @$kyg->order_mo + @$bjmkyg->order_mo + @$uli->order_mo + @$gmb->order_mo + @$uligmb->order_mo + @$bbr->order_mo + @$ple->order_mo + @$btb->order_mo + @$tki->order_mo + @$plebtbtki->order_mo + @$blc->order_mo + @$ser->order_mo + @$trj->order_mo + @$blcsertrj->order_mo + @$kpl->order_mo + @$pgt->order_mo + @$sti->order_mo + @$kip->order_mo + @$pgtstikip->order_mo + @$tjl->order_mo + @$pgn->order_mo + @$amt->order_mo + @$tjlpgnamt->order_mo + @$mtp->order_mo + @$mrb->order_mo + @$mtpmrb->order_mo + @$lul->order_mo + @$bri->order_mo + @$kdg->order_mo + @$neg->order_mo + @$kdgneg->order_mo + @$rta->order_mo + @$kdgnegrta->order_mo }}</th>
                <th>{{ @$bjm->order_pda + @$kyg->order_pda + @$bjmkyg->order_pda + @$uli->order_pda + @$gmb->order_pda + @$uligmb->order_pda + @$bbr->order_pda + @$ple->order_pda + @$btb->order_pda + @$tki->order_pda + @$plebtbtki->order_pda + @$blc->order_pda + @$ser->order_pda + @$trj->order_pda + @$blcsertrj->order_pda + @$kpl->order_pda + @$pgt->order_pda + @$sti->order_pda + @$kip->order_pda + @$pgtstikip->order_pda + @$tjl->order_pda + @$pgn->order_pda + @$amt->order_pda + @$tjlpgnamt->order_pda + @$mtp->order_pda + @$mrb->order_pda + @$mtpmrb->order_pda + @$lul->order_pda + @$bri->order_pda + @$kdg->order_pda + @$neg->order_pda + @$kdgneg->order_pda + @$rta->order_pda + @$kdgnegrta->order_pda }}</th>
                <th>{{ @$bjm->order_hsi + @$kyg->order_hsi + @$bjmkyg->order_hsi + @$uli->order_hsi + @$gmb->order_hsi + @$uligmb->order_hsi + @$bbr->order_hsi + @$ple->order_hsi + @$btb->order_hsi + @$tki->order_hsi + @$plebtbtki->order_hsi + @$blc->order_hsi + @$ser->order_hsi + @$trj->order_hsi + @$blcsertrj->order_hsi + @$kpl->order_hsi + @$pgt->order_hsi + @$sti->order_hsi + @$kip->order_hsi + @$pgtstikip->order_hsi + @$tjl->order_hsi + @$pgn->order_hsi + @$amt->order_hsi + @$tjlpgnamt->order_hsi + @$mtp->order_hsi + @$mrb->order_hsi + @$mtpmrb->order_hsi + @$lul->order_hsi + @$bri->order_hsi + @$kdg->order_hsi + @$neg->order_hsi + @$kdgneg->order_hsi + @$rta->order_hsi + @$kdgnegrta->order_hsi }}</th>
                <th>{{ @$bjm->order_wms + @$kyg->order_wms + @$bjmkyg->order_wms + @$uli->order_wms + @$gmb->order_wms + @$uligmb->order_wms + @$bbr->order_wms + @$ple->order_wms + @$btb->order_wms + @$tki->order_wms + @$plebtbtki->order_wms + @$blc->order_wms + @$ser->order_wms + @$trj->order_wms + @$blcsertrj->order_wms + @$kpl->order_wms + @$pgt->order_wms + @$sti->order_wms + @$kip->order_wms + @$pgtstikip->order_wms + @$tjl->order_wms + @$pgn->order_wms + @$amt->order_wms + @$tjlpgnamt->order_wms + @$mtp->order_wms + @$mrb->order_wms + @$mtpmrb->order_wms + @$lul->order_wms + @$bri->order_wms + @$kdg->order_wms + @$neg->order_wms + @$kdgneg->order_wms + @$rta->order_wms + @$kdgnegrta->order_wms }}</th>
                <th>
                    @php
                        $total_order_all = (@$total_order_bjm + @$total_order_kyg + @$total_order_bjmkyg + @$total_order_uli + @$total_order_gmb + @$total_order_uligmb + @$total_order_bbr + @$total_order_ple + @$total_order_btb + @$total_order_tki + @$total_order_plebtbtki + @$total_order_blc + @$total_order_ser + @$total_order_trj + @$total_order_blcsertrj + @$total_order_kpl + @$total_order_pgt + @$total_order_sti + @$total_order_kip + @$total_order_pgtstikip + @$total_order_tjl + @$total_order_pgn + @$total_order_amt + @$total_order_tjlpgnamt + @$total_order_mtp + @$total_order_mrb + @$total_order_mtpmrb + @$total_order_lul + @$total_order_bri + @$total_order_kdg + @$total_order_neg + @$total_order_kdgneg + @$total_order_rta + @$total_order_kdgnegrta);
                    @endphp
                    {{ $total_order_all }}
                </th>
                <th>
                    @php
                        $total_order_prog = (@$bjm->order_prog + @$kyg->order_prog + @$bjmkyg->order_prog + @$uli->order_prog + @$gmb->order_prog + @$uligmb->order_prog + @$bbr->order_prog + @$ple->order_prog + @$btb->order_prog + @$tki->order_prog + @$plebtbtki->order_prog + @$blc->order_prog + @$ser->order_prog + @$trj->order_prog + @$blcsertrj->order_prog + @$kpl->order_prog + @$pgt->order_prog + @$sti->order_prog + @$kip->order_prog + @$pgtstikip->order_prog + @$tjl->order_prog + @$pgn->order_prog + @$amt->order_prog + @$tjlpgnamt->order_prog + @$mtp->order_prog + @$mrb->order_prog + @$mtpmrb->order_prog + @$lul->order_prog + @$bri->order_prog + @$kdg->order_prog + @$neg->order_prog + @$kdgneg->order_prog + @$rta->order_prog + @$kdgnegrta->order_prog);
                        $total_order_kendala = (@$bjm->order_kendala + @$kyg->order_kendala + @$bjmkyg->order_kendala + @$uli->order_kendala + @$gmb->order_kendala + @$uligmb->order_kendala + @$bbr->order_kendala + @$ple->order_kendala + @$btb->order_kendala + @$tki->order_kendala + @$plebtbtki->order_kendala + @$blc->order_kendala + @$ser->order_kendala + @$trj->order_kendala + @$blcsertrj->order_kendala + @$kpl->order_kendala + @$pgt->order_kendala + @$sti->order_kendala + @$kip->order_kendala + @$pgtstikip->order_kendala + @$tjl->order_kendala + @$pgn->order_kendala + @$amt->order_kendala + @$tjlpgnamt->order_kendala + @$mtp->order_kendala + @$mrb->order_kendala + @$mtpmrb->order_kendala + @$lul->order_kendala + @$bri->order_kendala + @$kdg->order_kendala + @$neg->order_kendala + @$kdgneg->order_kendala + @$rta->order_kendala + @$kdgnegrta->order_kendala);

                        $progress_all = ceil(@round((@$total_order_prog + @$total_order_kendala) / $total_order_all * 100,2));
                        if (is_nan($progress_all) == true)
                        {
                            $progress_all = 100;
                        }
                        else
                        {
                            $progress_all = $progress_all;
                        }

                        if ($progress_all < 100)
                        {
                            $class_progress_all = 'class=bg-red';
                        }
                        else
                        {
                            $class_progress_all = '';
                        }
                    @endphp
                    {{ $total_order_prog }}
                </th>
                <th>{{ $total_order_kendala }}</th>
                <th>{{ $progress_all }} %</th>
                <th>{{ @$bjm->real_ps + @$kyg->real_ps + @$bjmkyg->real_ps + @$uli->real_ps + @$gmb->real_ps + @$uligmb->real_ps + @$bbr->real_ps + @$ple->real_ps + @$btb->real_ps + @$tki->real_ps + @$plebtbtki->real_ps + @$blc->real_ps + @$ser->real_ps + @$trj->real_ps + @$blcsertrj->real_ps + @$kpl->real_ps + @$pgt->real_ps + @$sti->real_ps + @$kip->real_ps + @$pgtstikip->real_ps + @$tjl->real_ps + @$pgn->real_ps + @$amt->real_ps + @$tjlpgnamt->real_ps + @$mtp->real_ps + @$mrb->real_ps + @$mtpmrb->real_ps + @$lul->real_ps + @$bri->real_ps + @$kdg->real_ps + @$neg->real_ps + @$kdgneg->real_ps + @$rta->real_ps + @$kdgnegrta->real_ps }}</th>
                <th>{{ @$bjm->komitmen_ps + @$kyg->komitmen_ps + @$bjmkyg->komitmen_ps + @$uli->komitmen_ps + @$gmb->komitmen_ps + @$uligmb->komitmen_ps + @$bbr->komitmen_ps + @$ple->komitmen_ps + @$btb->komitmen_ps + @$tki->komitmen_ps + @$plebtbtki->komitmen_ps + @$blc->komitmen_ps + @$ser->komitmen_ps + @$trj->komitmen_ps + @$blcsertrj->komitmen_ps + @$kpl->komitmen_ps + @$pgt->komitmen_ps + @$sti->komitmen_ps + @$kip->komitmen_ps + @$pgtstikip->komitmen_ps + @$tjl->komitmen_ps + @$pgn->komitmen_ps + @$amt->komitmen_ps + @$tjlpgnamt->komitmen_ps + @$mtp->komitmen_ps + @$mrb->komitmen_ps + @$mtpmrb->komitmen_ps + @$lul->komitmen_ps + @$bri->komitmen_ps + @$kdg->komitmen_ps + @$neg->komitmen_ps + @$kdgneg->komitmen_ps + @$rta->komitmen_ps + @$kdgnegrta->komitmen_ps }}</th>
                <th>
                    @php
                        $total_load = (@$load_order_bjm + @$load_order_kyg + @$load_order_bjmkyg + @$load_order_uli + @$load_order_gmb + @$load_order_uligmb + @$load_order_bbr + @$load_order_ple + @$load_order_btb + @$load_order_tki + @$load_order_plebtbtki + @$load_order_blc + @$load_order_ser + @$load_order_trj + @$load_order_blcsertrj + @$load_order_kpl + @$load_order_pgt + @$load_order_sti + @$load_order_kip + @$load_order_pgtstikip + @$load_order_tjl + @$load_order_pgn + @$load_order_amt + @$load_order_tjlpgnamt + @$load_order_mtp + @$load_order_mrb + @$load_order_mtpmrb + @$load_order_lul + @$load_order_bri + @$load_order_kdg + @$load_order_neg + @$load_order_kdgneg + @$load_order_rta + @$load_order_kdgnegrta);

                        if ($total_load < '-1')
                        {
                            $ket_all = 'OVERLOAD';
                            $class_ket_all = 'class=bg-red';
                        }
                        elseif ($total_load < 3)
                        {
                            $ket_all = 'ORDER CUKUP';
                            $class_ket_all = '';
                        }
                        else
                        {
                            $ket_all = 'KURANG ORDER';
                            $class_ket_all = 'class=bg-green';
                        }
                    @endphp
                    {{ $total_load }}
                </th>
                <th {{ $class_ket_all }}>{{ $ket_all }}</th>
            </tr>
        </tfoot>
    </table>
  </div>

  <div class="col-sm-12 table-responsive">
    <h2 style="font-weight: bold; text-align: center;" class="text-center">REPORT CUACA, SUPPLY & KENDALA KALSEL</h2>

    <table class="table table-bordered dataTable">
        <thead>
            <tr>
                <th class="align-middle bg-blue" rowspan="2">MITRA</th>
                <th class="align-middle bg-blue" rowspan="2">SEKTOR</th>
                <th class="align-middle bg-blue" rowspan="2">STO</th>
                <th class="align-middle bg-purple" colspan="3">CUACA</th>
                <th class="align-middle bg-purple" rowspan="2">NTE & MATERIAL</th>
                <th class="align-middle bg-purple" colspan="1">KENDALA LAPANGAN</th>
                <th class="align-middle bg-purple" colspan="2">SOLUSI & ACTION</th>
              </tr>
              <tr>
                <th class="align-middle bg-purple">PAGI</th>
                <th class="align-middle bg-purple">SIANG</th>
                <th class="align-middle bg-purple">SORE</th>
                <th class="align-middle bg-purple">KENDALA 1</th>
                <th class="align-middle bg-purple">ACTION 1</th>
                <th class="align-middle bg-purple">ACTION 2</th>
              </tr>
        </thead>
        <tbody>
            <tr>
                <th class="bg-white">STM</th>
                <th class="bg-white">BJM</th>
                <td>BJM</td>
                @php
                    if (@$bjm->cuaca_pagi == 'HUJAN')
                    {
                        $class_bjm_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_bjm_cuaca_pagi = '';
                    }

                    if (@$bjm->cuaca_siang == 'HUJAN')
                    {
                        $class_bjm_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_bjm_cuaca_siang = '';
                    }

                    if (@$bjm->cuaca_sore == 'HUJAN')
                    {
                        $class_bjm_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_bjm_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_bjm_cuaca_pagi }}>{{ @$bjm->cuaca_pagi }}</td>
                <td {{ $class_bjm_cuaca_siang }}>{{ @$bjm->cuaca_siang }}</td>
                <td {{ $class_bjm_cuaca_sore }}>{{ @$bjm->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="2">{{ @$bjmkyg->nte_material }}</th>
                <td>{{ @$bjm->kendala_1 }}</td>
                <td>{{ @$bjm->action_1 }}</td>
                <td>{{ @$bjm->action_2 }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">UPATEK</th>
                <th class="bg-white">KYG</th>
                <td>KYG</td>
                @php
                    if (@$kyg->cuaca_pagi == 'HUJAN')
                    {
                        $class_kyg_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_kyg_cuaca_pagi = '';
                    }

                    if (@$kyg->cuaca_siang == 'HUJAN')
                    {
                        $class_kyg_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_kyg_cuaca_siang = '';
                    }

                    if (@$kyg->cuaca_sore == 'HUJAN')
                    {
                        $class_kyg_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_kyg_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_kyg_cuaca_pagi }}>{{ @$kyg->cuaca_pagi }}</td>
                <td {{ $class_kyg_cuaca_siang }}>{{ @$kyg->cuaca_siang }}</td>
                <td {{ $class_kyg_cuaca_sore }}>{{ @$kyg->cuaca_sore }}</td>
                <td>{{ @$kyg->kendala_1 }}</td>
                <td>{{ @$kyg->action_1 }}</td>
                <td>{{ @$kyg->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="2">ULI - GMB</th>
                <td class="bg-white">ULI</td>
                @php
                    if (@$uli->cuaca_pagi == 'HUJAN')
                    {
                        $class_uli_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_uli_cuaca_pagi = '';
                    }

                    if (@$uli->cuaca_siang == 'HUJAN')
                    {
                        $class_uli_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_uli_cuaca_siang = '';
                    }

                    if (@$uli->cuaca_sore == 'HUJAN')
                    {
                        $class_uli_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_uli_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_uli_cuaca_pagi }}>{{ @$uli->cuaca_pagi }}</td>
                <td {{ $class_uli_cuaca_siang }}>{{ @$uli->cuaca_siang }}</td>
                <td {{ $class_uli_cuaca_sore }}>{{ @$uli->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="2">{{ @$uligmb->nte_material }}</th>
                <td class="text-center align-middle" rowspan="2">{{ @$uligmb->kendala_1 }}</td>
                <td class="text-center align-middle" rowspan="2">{{ @$uligmb->action_1 }}</td>
                <td class="text-center align-middle" rowspan="2">{{ @$uligmb->action_2 }}</td>
            </tr>
            <tr>
                <td>GMB</td>
                @php
                    if (@$gmb->cuaca_pagi == 'HUJAN')
                    {
                        $class_gmb_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_gmb_cuaca_pagi = '';
                    }

                    if (@$gmb->cuaca_siang == 'HUJAN')
                    {
                        $class_gmb_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_gmb_cuaca_siang = '';
                    }

                    if (@$gmb->cuaca_sore == 'HUJAN')
                    {
                        $class_gmb_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_gmb_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_gmb_cuaca_pagi }}>{{ @$gmb->cuaca_pagi }}</td>
                <td {{ $class_gmb_cuaca_siang }}>{{ @$gmb->cuaca_siang }}</td>
                <td {{ $class_gmb_cuaca_sore }}>{{ @$gmb->cuaca_sore }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="4">TBN</th>
                <th class="bg-white">BBR</th>
                <td>BBR</td>
                @php
                    if (@$bbr->cuaca_pagi == 'HUJAN')
                    {
                        $class_bbr_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_bbr_cuaca_pagi = '';
                    }

                    if (@$bbr->cuaca_siang == 'HUJAN')
                    {
                        $class_bbr_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_bbr_cuaca_siang = '';
                    }

                    if (@$bbr->cuaca_sore == 'HUJAN')
                    {
                        $class_bbr_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_bbr_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_bbr_cuaca_pagi }}>{{ @$bbr->cuaca_pagi }}</td>
                <td {{ $class_bbr_cuaca_siang }}>{{ @$bbr->cuaca_siang }}</td>
                <td {{ $class_bbr_cuaca_sore }}>{{ @$bbr->cuaca_sore }}</td>
                <td class="bg-green">{{ @$bbr->nte_material }}</td>
                <td>{{ @$bbr->kendala_1 }}</td>
                <td>{{ @$bbr->action_1 }}</td>
                <td>{{ @$bbr->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">PLE</th>
                <td>PLE</td>
                @php
                    if (@$ple->cuaca_pagi == 'HUJAN')
                    {
                        $class_ple_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_ple_cuaca_pagi = '';
                    }

                    if (@$ple->cuaca_siang == 'HUJAN')
                    {
                        $class_ple_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_ple_cuaca_siang = '';
                    }

                    if (@$ple->cuaca_sore == 'HUJAN')
                    {
                        $class_ple_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_ple_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_ple_cuaca_pagi }}>{{ @$ple->cuaca_pagi }}</td>
                <td {{ $class_ple_cuaca_siang }}>{{ @$ple->cuaca_siang }}</td>
                <td {{ $class_ple_cuaca_sore }}>{{ @$ple->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="3">{{ @$plebtbtki->nte_material }}</th>
                <td>{{ @$ple->kendala_1 }}</td>
                <td>{{ @$ple->action_1 }}</td>
                <td>{{ @$ple->action_2 }}</td>
            </tr>
            <tr>
                <td>BTB</td>
                @php
                    if (@$btb->cuaca_pagi == 'HUJAN')
                    {
                        $class_btb_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_btb_cuaca_pagi = '';
                    }

                    if (@$btb->cuaca_siang == 'HUJAN')
                    {
                        $class_btb_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_btb_cuaca_siang = '';
                    }

                    if (@$btb->cuaca_sore == 'HUJAN')
                    {
                        $class_btb_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_btb_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_btb_cuaca_pagi }}>{{ @$btb->cuaca_pagi }}</td>
                <td {{ $class_btb_cuaca_siang }}>{{ @$btb->cuaca_siang }}</td>
                <td {{ $class_btb_cuaca_sore }}>{{ @$btb->cuaca_sore }}</td>
                <td>{{ @$btb->kendala_1 }}</td>
                <td>{{ @$btb->action_1 }}</td>
                <td>{{ @$btb->action_2 }}</td>
            </tr>
            <tr>
                <td>TKI</td>
                @php
                    if (@$tki->cuaca_pagi == 'HUJAN')
                    {
                        $class_tki_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_tki_cuaca_pagi = '';
                    }

                    if (@$tki->cuaca_siang == 'HUJAN')
                    {
                        $class_tki_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_tki_cuaca_siang = '';
                    }

                    if (@$tki->cuaca_sore == 'HUJAN')
                    {
                        $class_tki_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_tki_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_tki_cuaca_pagi }}>{{ @$tki->cuaca_pagi }}</td>
                <td {{ $class_tki_cuaca_siang }}>{{ @$tki->cuaca_siang }}</td>
                <td {{ $class_tki_cuaca_sore }}>{{ @$tki->cuaca_sore }}</td>
                <td>{{ @$tki->kendala_1 }}</td>
                <td>{{ @$tki->action_1 }}</td>
                <td>{{ @$tki->action_2 }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="12">CUI</th>
                <th class="text-center align-middle bg-white" rowspan="3">BLC AREA</th>
                <td>BLC</td>
                @php
                    if (@$blc->cuaca_pagi == 'HUJAN')
                    {
                        $class_blc_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_blc_cuaca_pagi = '';
                    }

                    if (@$blc->cuaca_siang == 'HUJAN')
                    {
                        $class_blc_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_blc_cuaca_siang = '';
                    }

                    if (@$blc->cuaca_sore == 'HUJAN')
                    {
                        $class_blc_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_blc_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_blc_cuaca_pagi }}>{{ @$blc->cuaca_pagi }}</td>
                <td {{ $class_blc_cuaca_siang }}>{{ @$blc->cuaca_siang }}</td>
                <td {{ $class_blc_cuaca_sore }}>{{ @$blc->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="3">{{ @$blcsertrj->nte_material }}</th>
                <td>{{ @$blc->kendala_1 }}</td>
                <td>{{ @$blc->action_1 }}</td>
                <td>{{ @$blc->action_2 }}</td>
            </tr>
            <tr>
                <td>SER</td>
                @php
                    if (@$ser->cuaca_pagi == 'HUJAN')
                    {
                        $class_ser_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_ser_cuaca_pagi = '';
                    }

                    if (@$ser->cuaca_siang == 'HUJAN')
                    {
                        $class_ser_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_ser_cuaca_siang = '';
                    }

                    if (@$ser->cuaca_sore == 'HUJAN')
                    {
                        $class_ser_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_ser_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_ser_cuaca_pagi }}>{{ @$ser->cuaca_pagi }}</td>
                <td {{ $class_ser_cuaca_siang }}>{{ @$ser->cuaca_siang }}</td>
                <td {{ $class_ser_cuaca_sore }}>{{ @$ser->cuaca_sore }}</td>
                <td>{{ @$ser->kendala_1 }}</td>
                <td>{{ @$ser->action_1 }}</td>
                <td>{{ @$ser->action_2 }}</td>
            </tr>
            <tr>
                <td>TRJ</td>
                @php
                    if (@$trj->cuaca_pagi == 'HUJAN')
                    {
                        $class_trj_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_trj_cuaca_pagi = '';
                    }

                    if (@$trj->cuaca_siang == 'HUJAN')
                    {
                        $class_trj_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_trj_cuaca_siang = '';
                    }

                    if (@$trj->cuaca_sore == 'HUJAN')
                    {
                        $class_trj_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_trj_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_trj_cuaca_pagi }}>{{ @$trj->cuaca_pagi }}</td>
                <td {{ $class_trj_cuaca_siang }}>{{ @$trj->cuaca_siang }}</td>
                <td {{ $class_trj_cuaca_sore }}>{{ @$trj->cuaca_sore }}</td>
                <td>{{ @$trj->kendala_1 }}</td>
                <td>{{ @$trj->action_1 }}</td>
                <td>{{ @$trj->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white">KOTABARU</th>
                <td>KPL</td>
                @php
                    if (@$kpl->cuaca_pagi == 'HUJAN')
                    {
                        $class_kpl_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_kpl_cuaca_pagi = '';
                    }

                    if (@$kpl->cuaca_siang == 'HUJAN')
                    {
                        $class_kpl_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_kpl_cuaca_siang = '';
                    }

                    if (@$kpl->cuaca_sore == 'HUJAN')
                    {
                        $class_kpl_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_kpl_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_kpl_cuaca_pagi }}>{{ @$kpl->cuaca_pagi }}</td>
                <td {{ $class_kpl_cuaca_siang }}>{{ @$kpl->cuaca_siang }}</td>
                <td {{ $class_kpl_cuaca_sore }}>{{ @$kpl->cuaca_sore }}</td>
                <td class="bg-green">{{ @$kpl->nte_material }}</td>
                <td>{{ @$kpl->kendala_1 }}</td>
                <td>{{ @$kpl->action_1 }}</td>
                <td>{{ @$kpl->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">SATUI</th>
                <td>PGT</td>
                @php
                    if (@$pgt->cuaca_pagi == 'HUJAN')
                    {
                        $class_pgt_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgt_cuaca_pagi = '';
                    }

                    if (@$pgt->cuaca_siang == 'HUJAN')
                    {
                        $class_pgt_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgt_cuaca_siang = '';
                    }

                    if (@$pgt->cuaca_sore == 'HUJAN')
                    {
                        $class_pgt_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgt_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_pgt_cuaca_pagi }}>{{ @$pgt->cuaca_pagi }}</td>
                <td {{ $class_pgt_cuaca_siang }}>{{ @$pgt->cuaca_siang }}</td>
                <td {{ $class_pgt_cuaca_sore }}>{{ @$pgt->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="3">{{ @$pgtstikip->nte_material }}</th>
                <td>{{ @$pgt->kendala_1 }}</td>
                <td>{{ @$pgt->action_1 }}</td>
                <td>{{ @$pgt->action_2 }}</td>
            </tr>
            <tr>
                <td>STI</td>
                @php
                    if (@$sti->cuaca_pagi == 'HUJAN')
                    {
                        $class_sti_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_sti_cuaca_pagi = '';
                    }

                    if (@$sti->cuaca_siang == 'HUJAN')
                    {
                        $class_sti_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_sti_cuaca_siang = '';
                    }

                    if (@$sti->cuaca_sore == 'HUJAN')
                    {
                        $class_sti_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_sti_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_sti_cuaca_pagi }}>{{ @$sti->cuaca_pagi }}</td>
                <td {{ $class_sti_cuaca_siang }}>{{ @$sti->cuaca_siang }}</td>
                <td {{ $class_sti_cuaca_sore }}>{{ @$sti->cuaca_sore }}</td>
                <td>{{ @$sti->kendala_1 }}</td>
                <td>{{ @$sti->action_1 }}</td>
                <td>{{ @$sti->action_2 }}</td>
            </tr>
            <tr>
                <td>KIP</td>
                @php
                    if (@$kip->cuaca_pagi == 'HUJAN')
                    {
                        $class_kip_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_kip_cuaca_pagi = '';
                    }

                    if (@$kip->cuaca_siang == 'HUJAN')
                    {
                        $class_kip_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_kip_cuaca_siang = '';
                    }

                    if (@$kip->cuaca_sore == 'HUJAN')
                    {
                        $class_kip_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_kip_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_kip_cuaca_pagi }}>{{ @$kip->cuaca_pagi }}</td>
                <td {{ $class_kip_cuaca_siang }}>{{ @$kip->cuaca_siang }}</td>
                <td {{ $class_kip_cuaca_sore }}>{{ @$kip->cuaca_sore }}</td>
                <td>{{ @$kip->kendala_1 }}</td>
                <td>{{ @$kip->action_1 }}</td>
                <td>{{ @$kip->action_2 }}</td>
            </tr>

            <tr>
                <th class="text-center align-middle bg-white" rowspan="3">TJL AREA</th>
                <td>TJL</td>
                @php
                    if (@$tjl->cuaca_pagi == 'HUJAN')
                    {
                        $class_tjl_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_tjl_cuaca_pagi = '';
                    }

                    if (@$tjl->cuaca_siang == 'HUJAN')
                    {
                        $class_tjl_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_tjl_cuaca_siang = '';
                    }

                    if (@$tjl->cuaca_sore == 'HUJAN')
                    {
                        $class_tjl_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_tjl_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_tjl_cuaca_pagi }}>{{ @$tjl->cuaca_pagi }}</td>
                <td {{ $class_tjl_cuaca_siang }}>{{ @$tjl->cuaca_siang }}</td>
                <td {{ $class_tjl_cuaca_sore }}>{{ @$tjl->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="3">{{ @$tjlpgnamt->nte_material }}</th>
                <td>{{ @$tjl->kendala_1 }}</td>
                <td>{{ @$tjl->action_1 }}</td>
                <td>{{ @$tjl->action_2 }}</td>
            </tr>
            <tr>
                <td>PGN</td>
                @php
                    if (@$pgn->cuaca_pagi == 'HUJAN')
                    {
                        $class_pgn_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgn_cuaca_pagi = '';
                    }

                    if (@$pgn->cuaca_siang == 'HUJAN')
                    {
                        $class_pgn_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgn_cuaca_siang = '';
                    }

                    if (@$pgn->cuaca_sore == 'HUJAN')
                    {
                        $class_pgn_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_pgn_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_pgn_cuaca_pagi }}>{{ @$pgn->cuaca_pagi }}</td>
                <td {{ $class_pgn_cuaca_siang }}>{{ @$pgn->cuaca_siang }}</td>
                <td {{ $class_pgn_cuaca_sore }}>{{ @$pgn->cuaca_sore }}</td>
                <td>{{ @$pgn->kendala_1 }}</td>
                <td>{{ @$pgn->action_1 }}</td>
                <td>{{ @$pgn->action_2 }}</td>
            </tr>
            <tr>
                <td>AMT</td>
                @php
                    if (@$amt->cuaca_pagi == 'HUJAN')
                    {
                        $class_amt_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_amt_cuaca_pagi = '';
                    }

                    if (@$amt->cuaca_siang == 'HUJAN')
                    {
                        $class_amt_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_amt_cuaca_siang = '';
                    }

                    if (@$amt->cuaca_sore == 'HUJAN')
                    {
                        $class_amt_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_amt_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_amt_cuaca_pagi }}>{{ @$amt->cuaca_pagi }}</td>
                <td {{ $class_amt_cuaca_siang }}>{{ @$amt->cuaca_siang }}</td>
                <td {{ $class_amt_cuaca_sore }}>{{ @$amt->cuaca_sore }}</td>
                <td>{{ @$amt->kendala_1 }}</td>
                <td>{{ @$amt->action_1 }}</td>
                <td>{{ @$amt->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white" rowspan="2">MTP</th>
                <td>MTP</td>
                @php
                    if (@$mtp->cuaca_pagi == 'HUJAN')
                    {
                        $class_mtp_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_mtp_cuaca_pagi = '';
                    }

                    if (@$mtp->cuaca_siang == 'HUJAN')
                    {
                        $class_mtp_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_mtp_cuaca_siang = '';
                    }

                    if (@$mtp->cuaca_sore == 'HUJAN')
                    {
                        $class_mtp_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_mtp_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_mtp_cuaca_pagi }}>{{ @$mtp->cuaca_pagi }}</td>
                <td {{ $class_mtp_cuaca_siang }}>{{ @$mtp->cuaca_siang }}</td>
                <td {{ $class_mtp_cuaca_sore }}>{{ @$mtp->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="2">{{ @$mtpmrb->nte_material }}</th>
                <td>{{ @$mtp->kendala_1 }}</td>
                <td>{{ @$mtp->action_1 }}</td>
                <td>{{ @$mtp->action_2 }}</td>
            </tr>
            <tr>
                <td>MRB</td>
                @php
                    if (@$mrb->cuaca_pagi == 'HUJAN')
                    {
                        $class_mrb_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_mrb_cuaca_pagi = '';
                    }

                    if (@$mrb->cuaca_siang == 'HUJAN')
                    {
                        $class_mrb_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_mrb_cuaca_siang = '';
                    }

                    if (@$mrb->cuaca_sore == 'HUJAN')
                    {
                        $class_mrb_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_mrb_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_mrb_cuaca_pagi }}>{{ @$mrb->cuaca_pagi }}</td>
                <td {{ $class_mrb_cuaca_siang }}>{{ @$mrb->cuaca_siang }}</td>
                <td {{ $class_mrb_cuaca_sore }}>{{ @$mrb->cuaca_sore }}</td>
                <td>{{ @$mrb->kendala_1 }}</td>
                <td>{{ @$mrb->action_1 }}</td>
                <td>{{ @$mrb->action_2 }}</td>
            </tr>


            <tr>
                <th class="text-center align-middle bg-white" rowspan="5">GTM</th>
                <th class="text-center align-middle bg-white">LUL</th>
                <td>LUL</td>
                @php
                    if (@$lul->cuaca_pagi == 'HUJAN')
                    {
                        $class_lul_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_lul_cuaca_pagi = '';
                    }

                    if (@$lul->cuaca_siang == 'HUJAN')
                    {
                        $class_lul_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_lul_cuaca_siang = '';
                    }

                    if (@$lul->cuaca_sore == 'HUJAN')
                    {
                        $class_lul_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_lul_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_lul_cuaca_pagi }}>{{ @$lul->cuaca_pagi }}</td>
                <td {{ $class_lul_cuaca_siang }}>{{ @$lul->cuaca_siang }}</td>
                <td {{ $class_lul_cuaca_sore }}>{{ @$lul->cuaca_sore }}</td>
                <td class="bg-green">{{ @$lul->nte_material }}</td>
                <td>{{ @$lul->kendala_1 }}</td>
                <td>{{ @$lul->action_1 }}</td>
                <td>{{ @$lul->action_2 }}</td>
            </tr>
            <tr>
                <th class="text-center align-middle bg-white">BARABAI</th>
                <td>BRI</td>
                @php
                    if (@$bri->cuaca_pagi == 'HUJAN')
                    {
                        $class_bri_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_bri_cuaca_pagi = '';
                    }

                    if (@$bri->cuaca_siang == 'HUJAN')
                    {
                        $class_bri_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_bri_cuaca_siang = '';
                    }

                    if (@$bri->cuaca_sore == 'HUJAN')
                    {
                        $class_bri_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_bri_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_bri_cuaca_pagi }}>{{ @$bri->cuaca_pagi }}</td>
                <td {{ $class_bri_cuaca_siang }}>{{ @$bri->cuaca_siang }}</td>
                <td {{ $class_bri_cuaca_sore }}>{{ @$bri->cuaca_sore }}</td>
                <td class="bg-green">{{ @$bri->nte_material }}</td>
                <td>{{ @$bri->kendala_1 }}</td>
                <td>{{ @$bri->action_1 }}</td>
                <td>{{ @$bri->action_2 }}</td>
            </tr>
                <th class="text-center align-middle bg-white" rowspan="3">KANDANGAN</th>
                <td>KDG</td>
                @php
                    if (@$kdg->cuaca_pagi == 'HUJAN')
                    {
                        $class_kdg_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_kdg_cuaca_pagi = '';
                    }

                    if (@$kdg->cuaca_siang == 'HUJAN')
                    {
                        $class_kdg_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_kdg_cuaca_siang = '';
                    }

                    if (@$kdg->cuaca_sore == 'HUJAN')
                    {
                        $class_kdg_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_kdg_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_kdg_cuaca_pagi }}>{{ @$kdg->cuaca_pagi }}</td>
                <td {{ $class_kdg_cuaca_siang }}>{{ @$kdg->cuaca_siang }}</td>
                <td {{ $class_kdg_cuaca_sore }}>{{ @$kdg->cuaca_sore }}</td>
                <th class="text-center align-middle bg-green" rowspan="2">{{ @$kdgneg->nte_material }}</th>
                <td>{{ @$kdg->kendala_1 }}</td>
                <td>{{ @$kdg->action_1 }}</td>
                <td>{{ @$kdg->action_2 }}</td>
            </tr>
            <tr>
                <td>NEG</td>
                @php
                    if (@$neg->cuaca_pagi == 'HUJAN')
                    {
                        $class_neg_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_neg_cuaca_pagi = '';
                    }

                    if (@$neg->cuaca_siang == 'HUJAN')
                    {
                        $class_neg_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_neg_cuaca_siang = '';
                    }

                    if (@$neg->cuaca_sore == 'HUJAN')
                    {
                        $class_neg_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_neg_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_neg_cuaca_pagi }}>{{ @$neg->cuaca_pagi }}</td>
                <td {{ $class_neg_cuaca_siang }}>{{ @$neg->cuaca_siang }}</td>
                <td {{ $class_neg_cuaca_sore }}>{{ @$neg->cuaca_sore }}</td>
                <td>{{ @$neg->kendala_1 }}</td>
                <td>{{ @$neg->action_1 }}</td>
                <td>{{ @$neg->action_2 }}</td>
            </tr>
            <tr>
                <td>RTA</td>
                @php
                    if (@$rta->cuaca_pagi == 'HUJAN')
                    {
                        $class_rta_cuaca_pagi = 'class=bg-red';
                    }
                    else
                    {
                        $class_rta_cuaca_pagi = '';
                    }

                    if (@$rta->cuaca_siang == 'HUJAN')
                    {
                        $class_rta_cuaca_siang = 'class=bg-red';
                    }
                    else
                    {
                        $class_rta_cuaca_siang = '';
                    }

                    if (@$rta->cuaca_sore == 'HUJAN')
                    {
                        $class_rta_cuaca_sore = 'class=bg-red';
                    }
                    else
                    {
                        $class_rta_cuaca_sore = '';
                    }
                @endphp
                <td {{ $class_rta_cuaca_pagi }}>{{ @$rta->cuaca_pagi }}</td>
                <td {{ $class_rta_cuaca_siang }}>{{ @$rta->cuaca_siang }}</td>
                <td {{ $class_rta_cuaca_sore }}>{{ @$rta->cuaca_sore }}</td>
                <td class="bg-green">{{ @$rta->nte_material }}</td>
                <td>{{ @$rta->kendala_1 }}</td>
                <td>{{ @$rta->action_1 }}</td>
                <td>{{ @$rta->action_2 }}</td>
            </tr>
        </tbody>
        <tfoot>
        </tfoot>
    </table>
  </div>

</div>
@endsection
