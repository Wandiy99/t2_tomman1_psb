@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">NEW REKON PROVISIONING BY ORDER DATE {{ $tglAwal }} {{ $tglAkhir }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>TGL DISPATCH</th>
                        <th>TGL LAPORAN</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>PRECON 50</th>
                        <th>PRECON 75</th>
                        <th>PRECON 80</th>
                        <th>PRECON 100</th>
                        <th>PRECON 150</th>
                        <th>DC ROLL</th>
                        <th>TOTAL PRECON</th>
                        <th>UTP</th>
                        <th>TRAY CABLE</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $preconn = 0;
                    $m50 = 50;
                    $m75 = 75;
                    $m80 = 80;
                    $m100 = 100;
                    $m150 = 150;
                @endphp
                @foreach($data as $no => $list)
                @php
                    $preconn = $list->precon50 + $list->precon75 + $list->precon80 + $list->precon100 + $list->precon150;
                    $t50 = $list->precon50 * $m50;
                    $t75 = $list->precon75 * $m75;
                    $t80 = $list->precon80 * $m80;
                    $t100 = $list->precon100 * $m100;
                    $t150 = $list->precon150 * $m150;
                    $tAll = $t50 + $t75 + $t80 + $t100 + $t150;
                @endphp
                <tr>
                    <td>{{ ++$no }}</td>
                    @if($preconn==0 || $preconn==1)
                        <td>{{ $list->sc }}</td>
                    @else
                        <td bgcolor=red>#{{ $list->sc }}</td>
                    @endif
                    <td>{{ $list->tim }}</td>
                    <td>{{ $list->mitra_amija_pt }}</td>
                    <td>{{ $list->tanggal_dispatch }}</td>
                    <td>{{ $list->modified_at }}</td>
                    <td>{{ $list->orderDate }}</td>
                    <td>{{ $list->orderDatePs }}</td>
                    <td>{{ $list->precon50 }}</td>
                    <td>{{ $list->precon75 }}</td>
                    <td>{{ $list->precon80 }}</td>
                    <td>{{ $list->precon100 }}</td>
                    <td>{{ $list->precon150 }}</td>
                    <td>{{ $list->dc_roll }}</td>
                    <td>{{ $tAll }}</td>
                    <td>{{ $list->utp }}</td>
                    <td>{{ $list->tray_cable }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'NEW REKON PROVISIONING TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'NEW REKON PROVISIONING TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'NEW REKON PROVISIONING TOMMAN'
                }
            ]
        });
    });
</script>
@endsection