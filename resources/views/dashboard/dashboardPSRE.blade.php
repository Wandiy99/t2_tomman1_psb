@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL PS / RE AREA {{ $sektor }} STATUS {{ $status }} SEGMEN {{ $segmen }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        @if ($type == 'WITEL')
                        <th>WITEL</th>
                        @elseif (in_array($type, ['SEKTOR', 'STO']))
                        <th>AREA</th>
                        @elseif ($type == 'SEKTORX')
                        <th>SEKTOR</th>
                        @elseif ($type == 'MITRA')
                        <th>MITRA</th>
                        @endif
                        @if (in_array($type, ['SEKTOR', 'SEKTORX', 'MITRA', 'WITEL']) && session('auth')->nama_witel == 'KALSEL')
                        <th>TOMMAN</th>
                        @endif
                        @if ($status == 'CANCEL' && session('auth')->nama_witel == 'KALSEL')
                        <th>GESER TAGGING</th>
                        @endif
                        @if ($type != 'WITEL' && $status == 'UNSC')
                        <th>SURVEY ONDESK</th>
                        <th>ODP (PT2)</th>
                        <th>STATUS (PT2)</th>
                        <th>TIM (PT2)</th>
                        <th>MITRA (PT2)</th>
                        @endif
                        <th>STATUS</th>
                        <th>PERIODE</th>
                        <th>ORDER_ID</th>
                        @if ($type != 'WITEL' && $status == 'UNSC')
                        <th>WFM_ID</th>
                        <th>EX_ORDER_ID</th>
                        <th>STATUS_PRJ</th>
                        @endif
                        <th>REGIONAL</th>
                        <th>WITEL</th>
                        <th>DATEL</th>
                        <th>STO</th>
                        <th>JENISPSB</th>
                        <th>TYPE_TRANSAKSI</th>
                        <th>TYPE_LAYANAN</th>
                        <th>CHANNEL</th>
                        <th>GROUP_CHANNEL</th>
                        <th>STATUS_RESUME</th>
                        <th>STATUS_MESSAGE</th>
                        <th>PROVIDER</th>
                        <th>ORDER_DATE</th>
                        <th>LAST_UPDATED_DATE</th>
                        <th>DEVICE_ID</th>
                        <th>PACKAGE_NAME</th>
                        <th>HIDE</th>
                        <th>LOC_ID</th>
                        <th>NCLI</th>
                        <th>POTS</th>
                        <th>SPEEDY</th>
                        <th>CUSTOMER_NAME</th>
                        <th>CONTACT_HP</th>
                        <th>INS_ADDRESS</th>
                        <th>GPS_LONGITUDE</th>
                        <th>GPS_LATITUDE</th>
                        <th>K_CONTACT</th>
                        <th>CATEGORY</th>
                        <th>UMUR</th>
                        <th>TINDAK_LANJUT</th>
                        <th>ISI_COMMENT</th>
                        <th>TGL_COMMENT</th>
                        <th>USER_ID_TL</th>
                        <th>WONUM</th>
                        <th>DESK_TASK</th>
                        <th>STATUS_TASK</th>
                        <th>SCHEDULE_LABOR</th>
                        <th>AMCREW</th>
                        <th>STATUS_REDAMAN</th>
                        <th>STATUS_VOICE</th>
                        <th>STATUS_INET</th>
                        <th>STATUS_ONU</th>
                        <th>OLT_RX</th>
                        <th>ONU_RX</th>
                        <th>SNR_UP</th>
                        <th>SNR_DOWN</th>
                        <th>UPLOAD</th>
                        <th>DOWNLOAD</th>
                        <th>LAST_PROGRAM</th>
                        <th>CLID</th>
                        <th>LAST_START</th>
                        <th>LAST_VIEW</th>
                        <th>UKUR_TIME</th>
                        <th>TEKNISI</th>
                        @if ($type != 'WITEL')
                        <th>KOORDINAT_PELANGGAN</th>
                        @endif
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($query as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        @if ($type == 'WITEL')
                        <td>{{ $data->WITEL }}</td>
                        @elseif (in_array($type, ['SEKTOR', 'STO']))
                        <td>{{ $data->area }}</td>
                        @elseif ($type == 'SEKTORX')
                        <td>{{ $data->sektor }}</td>
                        @elseif ($type == 'MITRA')
                        <td>{{ $data->mitra }}</td>
                        @endif
                        @if (in_array($type, ['SEKTOR', 'SEKTORX', 'MITRA', 'WITEL']) && session('auth')->nama_witel == 'KALSEL')
                        <td>
                            @if ($status != 'UNSC')
                            {{ $data->status_laporan }}
                            @else
                            {{ @$sclama[$data->NCLI]->laporan_status ? : '-' }}
                            @endif
                        </td>
                        @endif
                        @if ($status == 'CANCEL' && session('auth')->nama_witel == 'KALSEL')
                        <td>
                            @if ($data->deviasi_order > 0)
                                GESER TAGGING
                            @else
                                OKE
                            @endif
                        </td>
                        @endif
                        @if ($type != 'WITEL' && $status == 'UNSC')
                        <td>
                            @if ($data->status_survey == 'Survey_OK')
                                @php
                                    $label = 'success';
                                @endphp
                            @elseif ($data->status_survey == 'Survey_NOK')
                                @php
                                    $label = 'danger';
                                @endphp
                            @else
                                @php
                                    $label = 'info';
                                @endphp
                            @endif
                            <span type="button" class="label label-{{ $label }}" data-toggle="modal" data-target="#responsive-modal-{{ $data->ORDER_ID }}">
                                {{ $data->status_survey ? : 'Click!' }}
                            </span>

                            <div id="responsive-modal-{{ $data->ORDER_ID }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $data->ORDER_ID }}" aria-hidden="true" style="display: none;">
                                <form class="form-group" action="/unsc/update_suvery_ondesk" method="POST">
                                    <input type="hidden" name="sc_order" value="{{ $data->ORDER_ID }}">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">SURVEY ONDESK SC-{{ $data->ORDER_ID }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="col-sm-12">
                                                        <div class="form-group col-md-12">
                                                            <label for="input-status_survey" class="col-form-label">Status :</label>
                                                            <select class="form-control select2" name="status_survey" id="input-status_survey" required>
                                                                <option value="" selected disabled>Pilih Status</option>
                                                                <option value="Survey_OK">Survey_OK</option>
                                                                <option value="Survey_NOK">Survey_NOK</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-12">
                                                            Diperbaharui Oleh <b>{{ $data->survey_updated_name ? : '-' }}</b> pada Tanggal <b>{{ $data->survey_updated_at ? : '-' }}</b>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </td>
                        <td>{{ $data->odp_pt2 ? : '-' }}</td>
                        <td>{{ $data->status_pt2 ? : '-' }}</td>
                        <td>{{ $data->tim_pt2 ? : '-' }}</td>
                        <td>{{ $data->mitra_pt2 ? : '-' }}</td>
                        @endif
                        <td>{{ $status }}</td>
                        <td>{{ $data->PERIODE }}</td>
                        <td>{{ $data->ORDER_ID }}</td>
                        @if ($type != 'WITEL' && $status == 'UNSC')
                        {{-- <td>{{ $data->wfm_id ? : '-' }}</td> --}}
                        <td>{{ @$wfm[$data->ORDER_ID] }}</td>
                        <td>
                            @if (!empty(@$sclama[$data->NCLI]->dt_id))
                            <a href="/{{ @$sclama[$data->NCLI]->dt_id }}" target="_blank">{{ @$sclama[$data->NCLI]->orderIdInteger }}</a>
                            @else
                            {{ @$sclama[$data->NCLI]->orderIdInteger }}
                            @endif
                        </td>
                        <td>{{ $data->status_prj ? : '-' }}</td>
                        @endif
                        <td>{{ $data->REGIONAL }}</td>
                        <td>{{ $data->WITEL }}</td>
                        <td>{{ $data->DATEL }}</td>
                        <td>{{ $data->STO }}</td>
                        <td>{{ $data->JENISPSB }}</td>
                        <td>{{ $data->TYPE_TRANSAKSI }}</td>
                        <td>{{ $data->TYPE_LAYANAN }}</td>
                        <td>{{ $data->CHANNEL }}</td>
                        <td>{{ $data->GROUP_CHANNEL }}</td>
                        <td>{{ $data->STATUS_RESUME }}</td>
                        <td>{{ $data->STATUS_MESSAGE }}</td>
                        <td>{{ $data->PROVIDER }}</td>
                        <td>{{ $data->ORDER_DATE }}</td>
                        <td>{{ $data->LAST_UPDATED_DATE }}</td>
                        <td>{{ $data->DEVICE_ID }}</td>
                        <td>{{ $data->PACKAGE_NAME }}</td>
                        <td>{{ $data->HIDE }}</td>
                        <td>{{ $data->LOC_ID }}</td>
                        <td>{{ $data->NCLI }}</td>
                        <td>{{ $data->POTS }}</td>
                        <td>{{ $data->SPEEDY }}</td>
                        <td>{{ $data->CUSTOMER_NAME }}</td>
                        <td>{{ $data->CONTACT_HP }}</td>
                        <td>{{ $data->INS_ADDRESS }}</td>
                        <td>{{ $data->GPS_LONGITUDE }}</td>
                        <td>{{ $data->GPS_LATITUDE }}</td>
                        <td>{{ $data->K_CONTACT }}</td>
                        <td>{{ $data->CATEGORY }}</td>
                        <td>{{ $data->UMUR }}</td>
                        <td>{{ $data->TINDAK_LANJUT }}</td>
                        <td>{{ $data->ISI_COMMENT }}</td>
                        <td>{{ $data->TGL_COMMENT }}</td>
                        <td>{{ $data->USER_ID_TL }}</td>
                        <td>{{ $data->WONUM }}</td>
                        <td>{{ $data->DESK_TASK }}</td>
                        <td>{{ $data->STATUS_TASK }}</td>
                        <td>{{ $data->SCHEDULE_LABOR }}</td>
                        <td>{{ $data->AMCREW }}</td>
                        <td>{{ $data->STATUS_REDAMAN }}</td>
                        <td>{{ $data->STATUS_VOICE }}</td>
                        <td>{{ $data->STATUS_INET }}</td>
                        <td>{{ $data->STATUS_ONU }}</td>
                        <td>{{ $data->OLT_RX }}</td>
                        <td>{{ $data->ONU_RX }}</td>
                        <td>{{ $data->SNR_UP }}</td>
                        <td>{{ $data->SNR_DOWN }}</td>
                        <td>{{ $data->UPLOAD }}</td>
                        <td>{{ $data->DOWNLOAD }}</td>
                        <td>{{ $data->LAST_PROGRAM }}</td>
                        <td>{{ $data->CLID }}</td>
                        <td>{{ $data->LAST_START }}</td>
                        <td>{{ $data->LAST_VIEW }}</td>
                        <td>{{ $data->UKUR_TIME }}</td>
                        <td>{{ $data->TEKNISI }}</td>
                        @if ($type != 'WITEL')
                        <td>{{ @$sclama[$data->NCLI]->kordinat_pelanggan }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL PS / RE SEKTOR TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL PS / RE SEKTOR TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL PS / RE SEKTOR TOMMAN'
                }
            ]
        });
    });
</script>
@endsection