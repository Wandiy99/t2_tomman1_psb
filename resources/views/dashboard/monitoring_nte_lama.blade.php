@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@include('partial.alerts')

<div class="col-sm-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">REKAP NTE LAMA DARI BOT TOMMAN-GO<br />STATUS {{ $status }} TANGGAL {{ $date }}</h4>
        <br/>
        <form method="GET">
            <div class="row">
                <div class="col-md-5">
                    <label for="witel">STATUS</label>
                    <select class="form-control" data-placeholder="- Pilih Status -" tabindex="1" name="status">
                        <option value="ALL">ALL</option>
                        <option value="APPROVED">APPROVED</option>
                        <option value="UNAPPROVED">UNAPPROVED</option>
                    </select>
                </div>
                <div class="col-md-5">
                    <label for="date">TANGGAL</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd" name="date" value="{{ $date ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="search">&nbsp;</label>
                    <button class="btn btn-info btn-block" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID ORDER</th>
                        <th>NO TIKET</th>
                        <th>INET</th>
                        <th>SN LAMA</th>
                        <th>SN BARU</th>
                        <th>TEKNISI</th>
                        <th>HD</th>
                        <th>UPDATE HD</th>
                        <th>SO</th>
                        <th>UPDATE SO</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $data)
                    <tr>
                        <td>
                            @if(in_array(session('auth')->level, [12, 2]))
                            <div id="responsive-modal-{{ $data->id_hai }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $data->id_hai }}" aria-hidden="true" style="display: none;">
                                <form class="form-group" action="/helpdesk/assurance/so/{{ $data->id_hai }}" method="POST">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Update ID Order {{ $data->id_hai }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="col-sm-12">
                                                        <div class="form-group col-md-12 hiddens">
                                                            <label for="input-status" class="col-form-label">Status :</label>
                                                            <select class="form-control select2" name="ket_so" required>
                                                                <option value="" selected disabled>Pilih Status</option>
                                                                <option value="1">Diterima</option>
                                                                <option value="0">Belum Diterima</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-12 hiddens">
                                                            <label for="message-text" class="control-label">Catatan SO :</label>
                                                            <textarea class="form-control" name="catatan_so" rows="4" required>{{ $data->catatan_so ? : '' }}</textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @if ($data->ket_so == 1)
                                @php $label = "label-success"; @endphp
                            @else
                                @php $label = "label-danger"; @endphp
                            @endif
                            <span type="button" class="label {{ $label }} text-center" data-toggle="modal" data-target="#responsive-modal-{{ $data->id_hai }}">
                                UPDATE
                            </span>
                            @endif
                        </td>
                        <td>{{ $data->no_tiket }}</td>
                        <td>{{ $data->inet }}</td>
                        <td>{{ $data->sn_lama }}</td>
                        <td>{{ $data->sn_baru }}</td>
                        <td>{{ $data->nama_teknisi }} / {{ $data->catatan_request }}</td>
                        <td>{{ $data->helpdesk_name }} / {{ $data->helpdesk_reply }}</td>
                        <td>{{ $data->tgl_close }}</td>
                        <td>{{ $data->nama_petugas_so }} / {{ $data->catatan_so }}</td>
                        <td>{{ $data->userso_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REKAP NTE LAMA TOMMAN-GO BOT TOMMAN'
                }
            ]
        });
    });

    $(function() {
      $('#datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
    });
</script>
@endsection