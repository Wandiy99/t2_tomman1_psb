@extends('layout')
@section('content')
<style>
th {
    text-align : center;
}
td {
    text-align : center;
    color: white;
}
table {
    table-layout: auto;
    width: 100%;
}
.tebal {
    font-weight: bold;
}
.color {
    background-color: #C70039 !important;
    color: white;
}
.color1 {
    background-color : rgb(130, 0, 0);
    color : white;
    font-weight: bold;
}
.color2 {
    background-color : rgb(237, 145, 33);
    color : black;
    font-weight: bold;
}
.color3 {
    background-color : green;
    color : white;
    font-weight: bold;
}
.color4 {
    background-color : #0970ba;
    color : white;
    font-weight: bold;
}
.color5 {
    background-color : #19BDFF;
    color : white;
    font-weight: bold;
}
</style>
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<link href="/elitetheme/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
          <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD PRODUKTIFITAS DISMANTLE & ONT PREMIUM</h4><br/>
          <form method="GET">
            <div class="row">
              <div class="col-md-5">
                  <label for="type">TYPE</label>
                  <select class="form-control select2" name="type">
                    <optgroup label="- Your Type ? -">
                    @if ($type == "WITEL")
                        <option value="WITEL">WITEL</option>
                        <option value="SEKTOR">SEKTOR</option>
                        <option value="MITRA">MITRA</option>
                    @elseif ($type == "SEKTOR")
                        <option value="SEKTOR">SEKTOR</option>
                        <option value="WITEL">WITEL</option>
                        <option value="MITRA">MITRA</option>
                    @elseif ($type == "MITRA")
                        <option value="MITRA">MITRA</option>
                        <option value="WITEL">WITEL</option>
                        <option value="SEKTOR">SEKTOR</option>
                    @endif
                  </select>
              </div>
              <div class="col-md-5">
                <label for="date">TANGGAL</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd" name="date" value="{{ $date ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
              <div class="col-md-2">
                  <label for="search">&nbsp;</label>
                  <button class="btn btn-primary btn-block" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading"><center>Dashboard Dismantling NTE</center></div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="align-middle color1" rowspan="2">{{ $type }}</th>
                            <th class="align-middle color2" colspan="6">SISA ORDER</th>
                            <th class="align-middle color1" colspan="11">COLLECTED HARIAN</th>
                            <th class="align-middle color3" colspan="6">COLLECTED BULAN</th>
                            <th class="align-middle color4" colspan="13">VISIT HARIAN</th>
                            <th class="align-middle color5" colspan="7">VISIT BULAN</th>
                        </tr>
                        <tr>
                            {{-- SISA ORDER --}}
                            <td class="align-middle color2">HD</td>
                            <td class="align-middle color2">CT0</td>
                            <td class="align-middle color2">MASSAL</td>
                            <td class="align-middle color2">147</td>
                            <td class="align-middle color2">NEW LOSS</td>
                            <td class="align-middle color2">TOTAL</td>
                            {{-- COLLECTED HARIAN --}}
                            <td class="align-middle color1"><font color="white">TARGET HI</font></td>
                            <td class="align-middle color1"><font color="white">ONT</font></td>
                            <td class="align-middle color1"><font color="white">STB</font></td>
                            <td class="align-middle color1"><font color="white">OTHERS</font></td>
                            <td class="align-middle color1"><font color="white">COLLECTED<br />H-1</font></td>
                            <td class="align-middle color1"><font color="white">%</font></td>

                            <td class="align-middle color1"><font color="white">ONT</font></td>
                            <td class="align-middle color1"><font color="white">STB</font></td>
                            <td class="align-middle color1"><font color="white">OTHERS</font></td>
                            <td class="align-middle color1"><font color="white">PROGRESS<br />COLLECTED<br />HI</font></td>
                            <td class="align-middle color1"><font color="white">%</font></td>
                            {{-- COLLECTED BULAN --}}
                            <td class="align-middle color3"><font color="white">TARGET BI</font></td>
                            <td class="align-middle color3"><font color="white">ONT</font></td>
                            <td class="align-middle color3"><font color="white">STB</font></td>
                            <td class="align-middle color3"><font color="white">OTHERS</font></td>
                            <td class="align-middle color3"><font color="white">REAL<br />BI</font></td>
                            <td class="align-middle color3"><font color="white">%</font></td>
                            {{-- VISIT HARIAN --}}
                            <td class="align-middle color4"><font color="white">TARGET HI</font></td>
                            <td class="align-middle color4"><font color="white">CT0</font></td>
                            <td class="align-middle color4"><font color="white">MASSAL</font></td>
                            <td class="align-middle color4"><font color="white">147</font></td>
                            <td class="align-middle color4"><font color="white">NEW<br />LOSS</font></td>
                            <td class="align-middle color4"><font color="white">VISIT<br />H-1</font></td>
                            <td class="align-middle color4"><font color="white">%</font></td>

                            <td class="align-middle color4"><font color="white">CT0</font></td>
                            <td class="align-middle color4"><font color="white">MASSAL</font></td>
                            <td class="align-middle color4"><font color="white">147</font></td>
                            <td class="align-middle color4"><font color="white">NEW<br />LOSS</font></td>
                            <td class="align-middle color4"><font color="white">PROGRESS<br />VISIT HI</font></td>
                            <td class="align-middle color4"><font color="white">%</font></td>
                            {{-- VISIT BULANAN --}}
                            <td class="align-middle color5"><font color="white">TARGET BI</font></td>
                            <td class="align-middle color5"><font color="white">CT0</font></td>
                            <td class="align-middle color5"><font color="white">MASSAL</font></td>
                            <td class="align-middle color5"><font color="white">147</font></td>
                            <td class="align-middle color5"><font color="white">NEW<br />LOSS</font></td>
                            <td class="align-middle color5"><font color="white">REAL<br />BI</font></td>
                            <td class="align-middle color5"><font color="white">%</font></td>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $tot_und_null = $tot_und_ct0 = $tot_und_massal = $tot_und_147 = $tot_und_newloss = $tot_sisa_order = $tot_jml_ont_h1 = $tot_jml_stb_h1 = $tot_jml_others_h1 = $tot_total_h1 = $tot_jml_ont_hi = $tot_jml_stb_hi = $tot_jml_others_hi = $tot_total_hi = $tot_jml_ont_bi = $tot_jml_stb_bi = $tot_jml_others_bi = $tot_total_bi = $tot_jml_visit_ct0_h1 = $tot_jml_visit_massal_h1 = $tot_jml_visit_147_h1 = $tot_jml_visit_newloss_h1 = $tot_total_visit_h1 = $tot_jml_visit_ct0_hi = $tot_jml_visit_massal_hi = $tot_jml_visit_147_hi = $tot_jml_visit_newloss_hi = $tot_total_visit_hi = $tot_jml_visit_ct0_bi = $tot_jml_visit_massal_bi = $tot_jml_visit_147_bi = $tot_jml_visit_newloss_bi = $tot_total_visit_bi = 0;

                            $ach_collect_h1 = $ach_collect_hi = $ach_collect_bi = $ach_visit_h1 = $ach_prog_hi = $ach_visit_bi = 0;

                            $target_collect_hi = 31;
                            $target_collect_bi = 837;
                            $target_visit_hi = 240;
                            $target_visit_bi = 6480;
                        @endphp
                        @foreach ($dismantle_area as $area => $dism)
                        @php
                            $tot_und_null += @$dism['und_null'];
                            $tot_und_ct0 += @$dism['und_ct0'];
                            $tot_und_massal += @$dism['und_massal'];
                            $tot_und_147 += @$dism['und_147'];
                            $tot_und_newloss += @$dism['und_newloss'];
                            $tot_sisa_order += @$dism['und_null'] + @$dism['und_ct0'] + @$dism['und_massal'] + @$dism['und_147'] + @$dism['und_newloss'];
                            
                            $tot_jml_ont_h1 += @$dism['jml_ont_h1'];
                            $tot_jml_stb_h1 += @$dism['jml_stb_h1'];
                            $tot_jml_others_h1 += @$dism['jml_others_h1'];
                            $tot_total_h1 += @$dism['total_h1'];
                            $tot_jml_ont_hi += @$dism['jml_ont_hi'];
                            $tot_jml_stb_hi += @$dism['jml_stb_hi'];
                            $tot_jml_others_hi += @$dism['jml_others_hi'];
                            $tot_total_hi += @$dism['total_hi'];

                            $tot_jml_ont_bi += @$dism['jml_ont_bi'];
                            $tot_jml_stb_bi += @$dism['jml_stb_bi'];
                            $tot_jml_others_bi += @$dism['jml_others_bi'];
                            $tot_total_bi += @$dism['total_bi'];

                            $tot_jml_visit_ct0_h1 += @$dism['jml_visit_ct0_h1'];
                            $tot_jml_visit_massal_h1 += @$dism['jml_visit_massal_h1'];
                            $tot_jml_visit_147_h1 += @$dism['jml_visit_147_h1'];
                            $tot_jml_visit_newloss_h1 += @$dism['jml_visit_newloss_h1'];
                            $tot_total_visit_h1 += @$dism['total_visit_h1'];

                            $tot_jml_visit_ct0_hi += @$dism['jml_visit_ct0_hi'];
                            $tot_jml_visit_massal_hi += @$dism['jml_visit_massal_hi'];
                            $tot_jml_visit_147_hi += @$dism['jml_visit_147_hi'];
                            $tot_jml_visit_newloss_hi += @$dism['jml_visit_newloss_hi'];
                            $tot_total_visit_hi += @$dism['total_visit_hi'];

                            $tot_jml_visit_ct0_bi += @$dism['jml_visit_ct0_bi'];
                            $tot_jml_visit_massal_bi += @$dism['jml_visit_massal_bi'];
                            $tot_jml_visit_147_bi += @$dism['jml_visit_147_bi'];
                            $tot_jml_visit_newloss_bi += @$dism['jml_visit_newloss_bi'];
                            $tot_total_visit_bi += @$dism['total_visit_bi'];
                        @endphp
                        <tr>
                            <td style="font-weight: bold;">{{ $area }}</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/SISA_ORDER/und_null/{{ $date }}">{{ number_format(@$dism['und_null'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/SISA_ORDER/und_ct0/{{ $date }}">{{ number_format(@$dism['und_ct0'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/SISA_ORDER/und_massal/{{ $date }}">{{ number_format(@$dism['und_massal'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/SISA_ORDER/und_147/{{ $date }}">{{ number_format(@$dism['und_147'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/SISA_ORDER/und_newloss/{{ $date }}">{{ number_format(@$dism['und_newloss'] ? : '0') }}</a></td>
                            <td>{{ number_format((@$dism['und_null']) + (@$dism['und_ct0']) + (@$dism['und_massal']) + (@$dism['und_147']) + (@$dism['und_newloss'])) }}</a></td>

                            <td>#</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_ont_h1/{{ $date }}">{{ number_format(@$dism['jml_ont_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_stb_h1/{{ $date }}">{{ number_format(@$dism['jml_stb_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_others_h1/{{ $date }}">{{ number_format(@$dism['jml_others_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/total_h1/{{ $date }}">{{ number_format(@$dism['total_h1'] ? : '0') }}</a></td>
                            <td class="color1"><font color="white"> % </font></td>

                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_ont_hi/{{ $date }}">{{ number_format(@$dism['jml_ont_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_stb_hi/{{ $date }}">{{ number_format(@$dism['jml_stb_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_others_hi/{{ $date }}">{{ number_format(@$dism['jml_others_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/total_hi/{{ $date }}">{{ number_format(@$dism['total_hi'] ? : '0') }}</a></td>
                            <td class="color1"><font color="white"> % </font></td>

                            <td>#</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_ont_bi/{{ $date }}">{{ number_format(@$dism['jml_ont_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_stb_bi/{{ $date }}">{{ number_format(@$dism['jml_stb_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/jml_others_bi/{{ $date }}">{{ number_format(@$dism['jml_others_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/COLLECTED/total_bi/{{ $date }}">{{ number_format(@$dism['total_bi'] ? : '0') }}</a></td>
                            <td class="color3"><font color="white"> % </font></td>

                            <td>#</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_ct0_h1/{{ $date }}">{{ number_format(@$dism['jml_visit_ct0_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_massal_h1/{{ $date }}">{{ number_format(@$dism['jml_visit_massal_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_147_h1/{{ $date }}">{{ number_format(@$dism['jml_visit_147_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_newloss_h1/{{ $date }}">{{ number_format(@$dism['jml_visit_newloss_h1'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/total_visit_h1/{{ $date }}">{{ number_format(@$dism['total_visit_h1'] ? : '0') }}</a></td>
                            <td class="color4"><font color="white"> % </font></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_ct0_hi/{{ $date }}">{{ number_format(@$dism['jml_visit_ct0_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_massal_hi/{{ $date }}">{{ number_format(@$dism['jml_visit_massal_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_147_hi/{{ $date }}">{{ number_format(@$dism['jml_visit_147_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_newloss_hi/{{ $date }}">{{ number_format(@$dism['jml_visit_newloss_hi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/total_visit_hi/{{ $date }}">{{ number_format(@$dism['total_visit_hi'] ? : '0') }}</a></td>
                            <td class="color4"><font color="white"> % </font></td>

                            <td>#</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_ct0_bi/{{ $date }}">{{ number_format(@$dism['jml_visit_ct0_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_massal_bi/{{ $date }}">{{ number_format(@$dism['jml_visit_massal_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_147_bi/{{ $date }}">{{ number_format(@$dism['jml_visit_147_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/jml_visit_newloss_bi/{{ $date }}">{{ number_format(@$dism['jml_visit_newloss_bi'] ? : '0') }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/{{ $area }}/VISIT/total_visit_bi/{{ $date }}">{{ number_format(@$dism['total_visit_bi'] ? : '0') }}</a></td>
                            <td class="color5"><font color="white"> % </font></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="color1"><font color="white"> TOTAL </font></td>
                            <td class="color2"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/SISA_ORDER/und_null/{{ $date }}"><font color="black"> {{ number_format($tot_und_null) }} </font></a></td>
                            <td class="color2"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/SISA_ORDER/und_ct0/{{ $date }}"><font color="black"> {{ number_format($tot_und_ct0) }} </font></a></td>
                            <td class="color2"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/SISA_ORDER/und_massal/{{ $date }}"><font color="black"> {{ number_format($tot_und_massal) }} </font></a></td>
                            <td class="color2"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/SISA_ORDER/und_147/{{ $date }}"><font color="black"> {{ number_format($tot_und_147) }} </font></a></td>
                            <td class="color2"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/SISA_ORDER/und_newloss/{{ $date }}"><font color="black"> {{ number_format($tot_und_newloss) }} </font></a></td>
                            <td class="color2"><font color="black"> {{ number_format($tot_sisa_order) }} </font></td>
                            <td class="color1"><font color="white"> {{ number_format($target_collect_hi) }} </font></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_ont_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_ont_h1) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_stb_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_stb_h1) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_others_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_others_h1) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/total_h1/{{ $date }}"><font color="white"> {{ number_format($tot_total_h1) }} </font></a></td>
                            <td class="color1"><font color="white"> % </font></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_ont_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_ont_hi) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_stb_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_stb_hi) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_others_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_others_hi) }} </font></a></td>
                            <td class="color1"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/total_hi/{{ $date }}"><font color="white"> {{ number_format($tot_total_hi) }} </font></a></td>
                            <td class="color1"><font color="white"> % </font></td>
                            <td class="color3"><font color="white"> {{ number_format($target_collect_bi) }} </font></td>
                            <td class="color3"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_ont_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_ont_bi) }} </font></a></td>
                            <td class="color3"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_stb_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_stb_bi) }} </font></a></td>
                            <td class="color3"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/jml_others_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_others_bi) }} </font></a></td>
                            <td class="color3"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/COLLECTED/total_bi/{{ $date }}"><font color="white"> {{ number_format($tot_total_bi) }} </font></a></td>
                            <td class="color3"><font color="white"> % </font></td>
                            <td class="color4"><font color="white"> {{ number_format($target_visit_hi) }} </font></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_ct0_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_ct0_h1) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_massal_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_massal_h1) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_147_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_147_h1) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_newloss_h1/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_newloss_h1) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/total_visit_h1/{{ $date }}"><font color="white"> {{ number_format($tot_total_visit_h1) }} </font></a></td>
                            <td class="color4"><font color="white"> % </font></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_ct0_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_ct0_hi) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_massal_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_massal_hi) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_147_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_147_hi) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_newloss_hi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_newloss_hi) }} </font></a></td>
                            <td class="color4"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/total_visit_hi/{{ $date }}"><font color="white"> {{ number_format($tot_total_visit_hi) }} </font></a></td>
                            <td class="color4"><font color="white"> % </font></td>
                            <td class="color5"><font color="white"> {{ number_format($target_visit_bi) }} </font></td>
                            <td class="color5"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_ct0_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_ct0_bi) }} </font></a></td>
                            <td class="color5"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_massal_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_massal_bi) }} </font></a></td>
                            <td class="color5"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_147_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_147_bi) }} </font></a></td>
                            <td class="color5"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/jml_visit_newloss_bi/{{ $date }}"><font color="white"> {{ number_format($tot_jml_visit_newloss_bi) }} </font></a></td>
                            <td class="color5"><a href="/DetailDismantleOntPremium/DISMANTLE/{{ $type }}/ALL/VISIT/total_visit_bi/{{ $date }}"><font color="white"> {{ number_format($tot_total_visit_bi) }} </font></a></td>
                            <td class="color5"><font color="white"> % </font></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                            <tr>
                                <th class="align-middle tebal" rowspan="2">SEKTOR</th>
                                <th class="align-middle tebal" colspan="7">STATUS</th>
                                <th class="align-middle tebal" rowspan="2">JUMLAH</th>
                            </tr>
                            <tr>
                                <td class="align-middle">TIDAK<br />BISA BAYAR</td>
                                <td class="align-middle">SERING<br />GANGGUAN</td>
                                <td class="align-middle">PINDAH<br />RUMAH</td>
                                <td class="align-middle">SALAH<br />PAKET</td>
                                <td class="align-middle">SUDAH<br />TIDAK DIPAKAI</td>
                                <td class="align-middle">RUMAH<br />TIDAK DITEMPATI</td>
                                <td class="align-middle">INTERNET<br />LAMBAT</td>
                            </tr>
                    </thead>
                    <tbody>
                        @php
                            $jml_tdk_bisa_bayar = $jml_sering_gangguan = $jml_pindah_rumah = $jml_salah_paket = $jml_sdh_tdk_dipakai = $jml_rumah_tdk_ditempati = $jml_inet_lambat = $jml_all = $total = 0;
                        @endphp
                        @foreach ($dismantling_berhasil as $disb)
                        @php
                            $jml_tdk_bisa_bayar += $disb->tidak_bisa_bayar;
                            $jml_sering_gangguan += $disb->sering_gangguan;
                            $jml_pindah_rumah += $disb->pindah_rumah;
                            $jml_salah_paket += $disb->salah_paket;
                            $jml_sdh_tdk_dipakai += $disb->sudah_tidak_dipakai;
                            $jml_rumah_tdk_ditempati += $disb->rumah_tidak_ditempati;
                            $jml_inet_lambat += $disb->internet_lambat;
                            $jml_all = $disb->tidak_bisa_bayar + $disb->sering_gangguan + $disb->pindah_rumah + $disb->salah_paket + $disb->sudah_tidak_dipakai + $disb->rumah_tidak_ditempati + $disb->internet_lambat;
                            $total += $jml_all;
                        @endphp
                        <tr>
                            <td>{{ $disb->area ? : '#N/A' }}</td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/TIDAK_BISA_BAYAR/{{ $date }}">{{ $disb->tidak_bisa_bayar }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/SERING_GANGGUAN/{{ $date }}">{{ $disb->sering_gangguan }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/PINDAH_RUMAH/{{ $date }}">{{ $disb->pindah_rumah }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/SALAH_PAKET/{{ $date }}">{{ $disb->salah_paket }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/SUDAH_TIDAK_DIPAKAI/{{ $date }}">{{ $disb->sudah_tidak_dipakai }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/RUMAH_TIDAK_DITEMPATI/{{ $date }}">{{ $disb->rumah_tidak_ditempati }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/INTERNET_LAMBAT/{{ $date }}">{{ $disb->internet_lambat }}</a></td>
                            <td><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/{{ $disb->area ? : 'NON AREA' }}/DONE/ALL_DONE/{{ $date }}">{{ $jml_all }}</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="tebal">TOTAL</th>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/TIDAK_BISA_BAYAR/{{ $date }}">{{ $jml_tdk_bisa_bayar }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/SERING_GANGGUAN/{{ $date }}">{{ $jml_sering_gangguan }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/PINDAH_RUMAH/{{ $date }}">{{ $jml_pindah_rumah }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/SALAH_PAKET/{{ $date }}">{{ $jml_salah_paket }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/SUDAH_TIDAK_DIPAKAI/{{ $date }}">{{ $jml_sdh_tdk_dipakai }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/RUMAH_TIDAK_DITEMPATI/{{ $date }}">{{ $jml_rumah_tdk_ditempati }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/INTERNET_LAMBAT/{{ $date }}">{{ $jml_inet_lambat }}</a></td>
                            <td  class="tebal"><a href="/DetailDismantleOntPremium/DISMANTLE/SEKTOR/ALL/DONE/ALL_DONE/{{ $date }}">{{ $total }}</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading"><center>ONT PREMIUM</center></div>
           <table class="table table-hover table-bordered">
           <thead>
                <tr>
                    <th class="align-middle color" rowspan="2">SEKTOR</th>
                    <th class="align-middle color" colspan="5">STATUS</th>
                    <th class="align-middle color" rowspan="2">ACH %</th>
                    <th class="align-middle color" rowspan="2">GRAND TOTAL</th>
                </tr>
                <tr>
                    <td class="align-middle color"><font color="white">UNDISPATCH</font></td>
                    <td class="align-middle color"><font color="white">ANTRIAN</font></td>
                    <td class="align-middle color"><font color="white">PROGRESS</font></td>
                    <td class="align-middle color"><font color="white">KENDALA</font></td>
                    <td class="align-middle color"><font color="white">CLOSE</font></td>
                </tr>
            </thead>
            @php
                $jml_und = 0;
                $jml_nop = 0;
                $jml_prog = 0;
                $jml_kend = 0;
                $jml_close = 0;
                $grand_total = 0;
                $prod = 0;
                $total_prod = 0;
            @endphp
            @foreach ($ont_premium as $onp)
            @php
                $jml_und += $onp->undispatch;
                $jml_nop += $onp->no_update;
                $jml_prog += $onp->progress;
                $jml_kend += $onp->kendala;
                $jml_close += $onp->closed;
                $grand_total += $onp->jumlah;
                $prod = (@round(($onp->closed + $onp->kendala) / @$onp->jumlah * 100,2));
                $total_prod += (@round(($jml_close + $jml_kend) / @$grand_total * 100,2));
            @endphp
                <tr>
                    <td>{{ $onp->sektor ? : '#N/A' }}</td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/UNDISPATCH/{{ $date }}">{{ $onp->undispatch }}</a></td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/NO_UPDATE/{{ $date }}">{{ $onp->no_update }}</a></td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/PROGRESS/{{ $date }}">{{ $onp->progress }}</a></td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/KENDALA/{{ $date }}">{{ $onp->kendala }}</a></td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/CLOSED/{{ $date }}">{{ $onp->closed }}</a></td>
                    <td>{{ $prod }} %</td>
                    <td><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/{{ $onp->sektor ? : 'NON AREA' }}/NULL/ALL/{{ $date }}">{{ $onp->jumlah }}</a></td>
                </tr>
            @endforeach
                <tr>
                    <td class="color"><font color="white">TOTAL</font></th>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/UNDISPATCH/{{ $date }}"><font color="white">{{ $jml_und }}</font></a></td>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/NO_UPDATE/{{ $date }}"><font color="white">{{ $jml_nop }}</font></a></td>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/PROGRESS/{{ $date }}"><font color="white">{{ $jml_prog }}</font></a></td>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/KENDALA/{{ $date }}"><font color="white">{{ $jml_kend }}</font></a></td>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/CLOSED/{{ $date }}"><font color="white">{{ $jml_close }}</font></a></td>
                    <td class="color"><font color="white">{{ $total_prod }} %</font></td>
                    <td class="color"><a href="/DetailDismantleOntPremium/ONTPREMIUM/NULL/ALL/NULL/ALL/{{ $date }}"><font color="white">{{ $grand_total }}</font></a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script src="/elitetheme/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
    $(function() {
        $(".select2").select2();

        $('#datepicker-autoclose').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
    });
</script>
@endsection