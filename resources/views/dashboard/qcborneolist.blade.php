@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style>
    th{
        vertical-align: middle;
        text-align: center;
    }
    td{
        vertical-align: middle;
        text-align: center;
    }
    table.dataTable tbody tr{
        background-color: transparent;
    }
</style>
  <div class="col-sm-16">
        <div class="panel panel-default">
            <div class="panel-heading">DETAIL QUALITY CONTROL {{ $teknisi }} BY DATE PS {{ $date }} S/D {{ $dateend }} </div>
            <div class="table-responsive">
                <table class="table" id="table_data">
                <thead>
                <tr>
                    <th>NO</th>
                    <th>SC</th>
                    <th>ORDER STATUS</th>
                    <th>SEKTOR</th>
                    <th>NAMA PELANGGAN</th>
                    <th>UNIT</th>
                    <th>RUMAH</th>
                    <th>TEKNISI</th>
                    <th>ODP</th>
                    <th>REDAMAN</th>
                    <th>SPEEDTEST</th>
                    <th>BA</th>
                    <th>SURAT DEPOSIT</th>
                    <th>PROFILE MYIH</th>
                    <th>TAGGING</th>
                    <th>DATE PS</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $result)
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>
                    @if($result->id_dt <> "")
                        <a href="/{{ $result->id_dt }}">{{ $result->sc }}</a><br />
                        <a class="label label-success" href="/dispatch/{{ $result->sc }}">{{ $result->uraian }}</a>
                    @else
                        {{ $result->sc }}
                    @endif
                    </td>
                    <td>{{ $result->orderStatus }}</td>
                    <td>{{ $result->title }}</td>
                    <td>{{ $result->customer }}</td>
                    @if($result->kategori=="LOLOS")
                        <td>VALID/LOLOS</td>
                    @else
                        <td>{{ $result->tag_unit }}</td>
                    @endif
                    @foreach($getFoto as $foto => $ket)
                    <?php
                    $path1 = "/upload/evidence/{$result->id_dt}";
                    $th1   = "$path1/$foto-th.jpg";
                    $path2 = "/upload2/evidence/{$result->id_dt}";
                    $th2   = "$path2/$foto-th.jpg";
                    $path3 = "/upload3/evidence/{$result->id_dt}";
                    $th3   = "$path3/$foto-th.jpg";
                    $path4 = "/upload4/evidence/{$result->id_dt}";
                    $th4   = "$path4/$foto-th.jpg";
                    $path  = null;
                    
                    if(file_exists(public_path().$th1)){
                        $path = "$path1/$foto";
                    }elseif(file_exists(public_path().$th2)){
                        $path = "$path2/$foto";
                    }elseif(file_exists(public_path().$th3)){
                        $path = "$path3/$foto";
                    }elseif(file_exists(public_path().$th4)){
                        $path = "$path4/$foto";
                    }

                    if($path){
                        $th    = "$path-th.jpg";
                        $img   = "$path.jpg";
                    } else {
                        $th    = null;
                    }
                    ?>
                    <td>
                    @if(!empty($th) && file_exists(public_path().$th))
                        <a href="{{ $img }}"><img src="{{ $th }}"></a><br />
                        {{ $result->$ket ? : '-' }}
                    @else
                        <img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br /><br />
                        {{ $result->$ket ? : '-' }}
                    @endif
                    </td>
                    @endforeach
                    <td>
                    {{ $result->kordinat_pelanggan ? : '-' }}<br /><br />
                    {{ $result->tag_lokasi }}</td>
                    <td>{{ $result->datePs }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>NO</th>
                    <th>SC</th>
                    <th>ORDER STATUS</th>
                    <th>SEKTOR</th>
                    <th>NAMA PELANGGAN</th>
                    <th>UNIT</th>
                    <th>RUMAH</th>
                    <th>TEKNISI</th>
                    <th>ODP</th>
                    <th>REDAMAN</th>
                    <th>SPEEDTEST</th>
                    <th>BA</th>
                    <th>SURAT DEPOSIT</th>
                    <th>PROFILE MYIH</th>
                    <th>TAGGING</th>
                    <th>DATE PS</th>
                </tr>
                </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('plugins')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function() {
        $('#table_data').DataTable();
    });
</script>
@endsection