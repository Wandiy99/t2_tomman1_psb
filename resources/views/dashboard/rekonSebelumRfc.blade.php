@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    td,th {
      padding : 5px;
    }
  </style>

    <div class="row">
      <div class="col-sm-5">
        <h3>REKON MITRA TA KALSEL</h3>
        <small>Periode {{ $date }} Sebelum RFC</small>
        <table class="table table-responsive">
          <tr>
            <th>Nama Mitra</th>
            <th>Provisioning</th>
            <th>Migrasi</th>
            {{-- <th>Assurance</th> --}}
            {{-- <th>Jumlah Pekerjaan</th> --}}
          </tr>
          <?php
            $total_pekerjaan = 0;
            $total_prov = 0;
            $total_migrasi = 0;
            $total_asr = 0;
            $jumlah = 0;
            $total = 0;
          ?>
          @foreach ($rekonMitra as $data)
          <?php
            $total_pekerjaan += $data->jumlah;
            $total_prov += $data->jumlah_prov;
            $total_migrasi += $data->jumlah_migrasi;
            $total_asr += $data->jumlah_asr;

          ?>
            <tr>
              <td><a href="/dashboard/listTimMitra/{{ $data->mitra ? : 'NONE' }}/{{ $date }}">{{ $data->mitra ? : 'NONE' }}</a></td>
              <td><a href="/dashboard/provisioningListMitraSebelumRfc/{{ $data->mitra ? : 'NONE' }}/{{ $date }}">{{ $data->jumlah_prov }}</a></td>
              <td><a href="/dashboard/migrasiListMitraSebelumRfc/{{ $data->mitra ? : 'NONE' }}/{{ $date }}">{{ $data->jumlah_migrasi }}</a></td>
              {{-- <td><a href="/dashboard/ListMitraSebelumRfc/{{ $data->mitra ? : 'NONE' }}/{{ $date }}">{{ $data->jumlah }}</a></td> --}}
            </tr>
          @endforeach
          <tr>
            <td>Total</td>
            <td><a href="/dashboard/provisioningListMitraSebelumRfc/ALL/{{ $date }}">{{ $total_prov }}</a></td>
            <td><a href="/dashboard/migrasiListMitraSebelumRfc/ALL/{{ $date }}">{{ $total_migrasi }}</a></td>
            {{-- <td><a href="/dashboard/assuranceListMitraSebelumRfc/ALL/{{ $date }}">{{ $total_asr }}</a></td> --}}
            {{-- <td>{{ $total_pekerjaan }}</td> --}}

          </tr>
        </table>
      </div>
    </div>
   
    <br />
    <br />
@endsection
