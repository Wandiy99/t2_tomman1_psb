@extends($layout)

@section('content')
  @include('partial.alerts')
<style>
    th{
        vertical-align: middle;
        text-align: center;
    }
</style>
<div class="col-sm-16">
    <div class="panel panel-default">
        <div class="panel-heading">SEARCH SC</div>
            <form method="GET">
                <input type="text" class="form-control" placeholder="Search SC Here ..." name="id" id="id" />
            </form>
    </div>
</div><br />
  <div class="col-sm-16">
        <div class="panel panel-default">
            <div class="panel-heading">RESULT QC BORNEO</div>
            <div class="table-responsive">
                <table class="table table-bordered">
                <tr>
                    <th class="align-middle" rowspan='2'>NO</th>
                    <th class="align-middle" rowspan='2'>SC</th>
                    <th class="align-middle" rowspan='2'>INET</th>
                    <th class="align-middle" rowspan='2'>VOICE</th>
                    <th class="align-middle" rowspan='2'>CUSTOMER</th>
                    <th class="align-middle" rowspan='2'>EMAIL</th>
                    <th class="align-middle" rowspan='2'>PACKAGE NAME</th>
                    <th class="align-middle" rowspan='2'>ODP TEKNISI</th>
                    <th class="align-middle" rowspan='2'>ODP STARCLICK</th>
                    <th class="align-middle" rowspan='2'>TIM</th>
                    <th class="align-middle" rowspan='2'>MITRA</th>
                    <th class="align-middle" rowspan='2'>SEKTOR</th>
                    <th class="align-middle" rowspan='2'>STATUS</th>
                    <th class="align-middle" colspan="3">FOTO RUMAH</th>
                    <th class="align-middle" rowspan="2">FOTO TEKNISI</th>
                    <th class="align-middle" rowspan="2">FOTO ODP</th>
                    <th class="align-middle" rowspan="2">FOTO REDAMAN</th>
                    <th class="align-middle" colspan="5">FOTO CPE LAYANAN</th>
                    <th class="align-middle" colspan="2">FOTO SN</th>
                    <th class="align-middle" rowspan="2">FOTO BA</th>
                    <th class="align-middle" rowspan="2">FOTO SURAT DEPOSIT</th>
                    <th class="align-middle" rowspan="2">FOTO PROFIL MYIH</th>
                    <th class="align-middle" colspan="5">FOTO ADDITIONAL</th>
                    <th class="align-middle" rowspan="2">TAGGING</th>
                    <th class="align-middle" rowspan="2">UNIT</th>
                    {{-- <th class="align-middle" rowspan="2">KATEGORI</th> --}}
                    <th class="align-middle" rowspan="2">LAST UPDATED</th>
                    <th class="align-middle" rowspan="2">DATE CREATED</th>
                    <th class="align-middle" rowspan="2">DATE PS</th>
                </tr>
                <tr>
                    <th class="align-middle">Lokasi</th>
                    <th class="align-middle">Meteran Rumah</th>
                    <th class="align-middle">ID Listrik Pelanggan</th>
                    <th class="align-middle">Speedtest</th>
                    <th class="align-middle">Live TV</th>
                    <th class="align-middle">TVOD</th>
                    <th class="align-middle">Telephone Incoming</th>
                    <th class="align-middle">Telephone Outgoing</th>
                    <th class="align-middle">SN ONT</th>
                    <th class="align-middle">SN STB</th>
                    <th class="align-middle">ADDITIONAL 1</th>
                    <th class="align-middle">ADDITIONAL 2</th>
                    <th class="align-middle">ADDITIONAL 3</th>
                    <th class="align-middle">ADDITIONAL 4</th>
                    <th class="align-middle">ADDITIONAL 5</th>
                </tr>
                @foreach ($data as $num => $result)

                @if($result)                    
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>
                    @if($level <> 10)
                        <a href="/{{ $result->id_dt }}" target="_blank">{{ $result->sc ? : $result->Ndem ? : '-' }}</a>
                    @else
                        <a href="/{{ $result->id_dt }}" target="_blank" style="color: blue;">{{ $result->sc ? : $result->Ndem ? : '-' }}</a>
                    @endif
                        <center><br/><br/>
                        <a href="/api_foto?key=a2ed39c417316adbd5cd1d0211a5d711&id={{ $result->sc ? : $result->Ndem ? : '-' }}" target="_blank"><label class="label label-success"><i class="fa fa-link"></i> A P I</label></a></center>
                    </td>
                    <td>{{ $result->no_inet ? : $result->internet ? : '-' }}</td>
                    <td>{{ $result->no_voice ? : $result->noTelp ? : '-' }}</td>
                    <td>{{ $result->nama ? : $result->orderName ? : '-' }}</td>
                    <td>{{ $result->dps_email ? : $result->kt_email ? : '-' }}</td>
                    <td>{{ $result->orderPackageName ? : $result->kt_package_name ? : '-' }}</td>
                    <td>{{ $result->odp ? : '-' }}</td>
                    <td>{{ $result->alproname ? : '-' }}</td>
                    @if($result->tim==null)
                    @foreach($logTeknisi as $logTek)
                    @if($logTek->psb_laporan_id == $result->id_pl)
                        <td>{{ $logTek->created_by ? : '-' }} / {{ $logTek->created_name ? : $logTek->nama ? : '-' }}</td>
                        <td>{{ $logTek->mitra_amija ? : '-' }}</td>
                    @endif
                    @endforeach

                    @else
                        <td>{{ $result->tim ? : '-' }}</td>
                        <td>{{ $result->mitra ? : '-' }}</td>
                    @endif
                    <td>{{ $result->sektor ? : '-' }}</td>
                    <td>{{ $result->laporan_status ? : 'ANTRIAN' }} / {{ $result->orderStatus ? : '-' }}</td>
                     @foreach($get_foto as $foto => $info)
                    <?php
                    $path1 = "/upload/evidence/{$result->id_dt}";
                    $th1   = "$path1/$foto-th.jpg";
                    $path2 = "/upload2/evidence/{$result->id_dt}";
                    $th2   = "$path2/$foto-th.jpg";
                    $path3 = "/upload3/evidence/{$result->id_dt}";
                    $th3   = "$path3/$foto-th.jpg";
                    $path4 = "/upload4/evidence/{$result->id_dt}";
                    $th4   = "$path4/$foto-th.jpg";
                    $path  = null;
                    
                    if(file_exists(public_path().$th1)){
                        $path = "$path1/$foto";
                    }elseif(file_exists(public_path().$th2)){
                        $path = "$path2/$foto";
                    }elseif(file_exists(public_path().$th3)){
                        $path = "$path3/$foto";
                    }elseif(file_exists(public_path().$th4)){
                        $path = "$path4/$foto";
                    }

                    if($path){
                        $th    = "$path-th.jpg";
                        $img   = "$path.jpg";
                        $k     = "$foto-kpro.jpg";
                    } else {
                        $th    = null;
                        $img   = null;
                        $k     = null;
                    }
                    
                    $sc = $result->sc ? : $result->Ndem;
                    $link = "/foto/$sc/$k";
                    ?>
                    <td>
                        @if(!empty($th) && file_exists(public_path().$th))
                        <center>
                            <a href="{{ $img }}"><img src="{{ $th }}"></a><br />
                            {{ $result->$info ? : '-'}}
                            <br />
                            <a href="{{ $link }}" target="_blank" style="color: green !important">QC Borneo</a>
                        </center>
                        @else
                        <center>
                            <img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br />
                            {{ $result->$info ? : ''}}
                        </center>
                        @endif
                    </td>
                    @endforeach
                    <td>
                    {{ $result->kordinat_pelanggan }}<br />
                    <center>{{ $result->tag_lokasi ? : '-' }}</center>
                    </td>
                    <td>{{ $result->tag_unit ? : '-' }}</td>
                    {{-- <td>{{ $result->kategori ? : '-' }}</td> --}}
                    <td>{{ $result->last_updated ? : '-' }}</td>
                    <td>{{ $result->date_created ? : '-' }}</td>
                    <td>{{ $result->datePs ? : '-' }}</td>
                </tr>
                    @endif
                @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection