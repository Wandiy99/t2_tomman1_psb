@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    .xinput {
        resize: horizontal;
        width: 330px;
        text-align: center;
    }
    
    .xinput:active {
        width: auto;   
    }
    
    .xinput:focus {
        min-width: 200px;
    }
</style>

<div class="panel panel-primary">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
<center>
<form method="get">
    <span style="font-weight: bolder !important;color: black !important;">FILTER BY DATE WO : </span>
    <input style="height: 35px; text-align: center; font-weight: bold" type="text" name="daterange" value="{{ $xdate }} - {{ $xdatex }}" />
</form>
</center>
</div></div></div></div><br />

<div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white">
        <h4 style="font-weight: bolder !important;color: black !important;">REPORT UT ONLINE PERIODE BY TRX WO {{ $date }} S/D {{ $datex }}</h4>
        <p class="text-muted">Last Update : {{ $log->last_updated_at }} WITA</p>
    </div>
    <div class="table-responsive">
        <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">MITRA</th>
                    <th class="align-middle" colspan='4' style="background-color: #ff6f69 !important; color: black">UNIT</th>
                    <th class="align-middle" rowspan='2' style="background-color: #88d8b0 !important; color: black">VALID QC2</th>
                    <th class="align-middle" rowspan='2' style="background-color: #ec87f3 !important; color: black">GRAND TOTAL</th>
                    <th class="align-middle" colspan='2' style="background-color: #34495E !important; color: white">PS K-PRO</th>
                    </tr>
                    <tr>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NEED VALIDATE</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NEED APPROVE</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">REJECTED</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">RETURN MYTECH</th>
                    <th class="align-middle" style="background-color: #5D6D7E !important; color: white">AO</th>
                    <th class="align-middle" style="background-color: #5D6D7E !important; color: white">MO</th>
                    </tr>
                </thead>
                @php
                    $total_nv = $total_na = $total_r = $total_rmt = $total_aprv = $grand_total = $total_ao = $total_mo = 0;
                @endphp
                <tbody>
                    @foreach ($data_mitra as $area => $result)
                @php
                    $total_nv += @$result['need_validate'];
                    $total_na += @$result['need_approve'];
                    $total_r += @$result['rejected'];
                    $total_rmt += @$result['return_mytech'];
                    $total_aprv += @$result['valid_qc2'];
                    $grand_total += @$result['jumlah'];
                    $total_ao += @$result['ps_ao'];
                    $total_mo += @$result['ps_mo'];
                @endphp
                    <tr>
                        <td style="background-color: #FBFBFB; color: black">{{ $area }}</td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/need_validate" target="_blank">{{ number_format(@$result['need_validate']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/need_approve" target="_blank">{{ number_format(@$result['need_approve']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/rejected" target="_blank">{{ number_format(@$result['rejected']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/return_mytech" target="_blank">{{ number_format(@$result['return_mytech']) ? : '0' }}</a></td>
                        <td style="background-color: #eef9f4 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/valid_qc2" target="_blank">{{ number_format(@$result['valid_qc2']) ? : '0' }}</a></td>
                        <td style="background-color: #f9dbfb !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/all" target="_blank">{{ number_format(@$result['jumlah']) ? : '0' }}</a></td>

                        <td style="background-color: #85929E !important; color: white"><a style="color: white" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/ps_ao" target="_blank">{{ number_format(@$result['ps_ao']) ? : '0' }}</a></td>
                        <td style="background-color: #85929E !important; color: white"><a style="color: white" href="/dashboard/UTOnlineDetail/MITRA/{{ $area }}/{{ $date }}/{{ $datex }}/ps_mo" target="_blank">{{ number_format(@$result['ps_mo']) ? : '0' }}</a></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/need_validate" target="_blank">{{ number_format($total_nv) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/need_approve" target="_blank">{{ number_format($total_na) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/rejected" target="_blank">{{ number_format($total_r) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/return_mytech" target="_blank">{{ number_format($total_rmt) }}</a></td>
                        <td style="background-color: #88d8b0 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/valid_qc2" target="_blank">{{ number_format($total_aprv) }}</a></td>
                        <td style="background-color: #ec87f3 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/all" target="_blank">{{ number_format($grand_total) }}</a></td>

                        <td style="background-color: #5D6D7E !important"><a style="color: white !important; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/ps_ao" target="_blank">{{ number_format($total_ao) }}</a></td>
                        <td style="background-color: #5D6D7E !important"><a style="color: white !important; font-weight: bold" href="/dashboard/UTOnlineDetail/MITRA/ALL/{{ $date }}/{{ $datex }}/ps_mo" target="_blank">{{ number_format($total_mo) }}</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br /><br />
    <div class="table-responsive">
        <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">SEKTOR</th>
                    <th class="align-middle" colspan='4' style="background-color: #ff6f69 !important; color: black">UNIT</th>
                    <th class="align-middle" rowspan='2' style="background-color: #88d8b0 !important; color: black">VALID QC2</th>
                    <th class="align-middle" rowspan='2' style="background-color: #ec87f3 !important; color: black">GRAND TOTAL</th>
                    <th class="align-middle" colspan='2' style="background-color: #34495E !important; color: white">PS K-PRO</th>
                    </tr>
                    <tr>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NEED VALIDATE</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NEED APPROVE</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">REJECTED</th>
                    <th class="align-middle" style="background-color: #ff9a96 !important; color: black">RETURN MYTECH</th>
                    <th class="align-middle" style="background-color: #5D6D7E !important; color: white">AO</th>
                    <th class="align-middle" style="background-color: #5D6D7E !important; color: white">MO</th>
                    </tr>
                </thead>
                @php
                    $total_nv = $total_na = $total_r = $total_rmt = $total_aprv = $grand_total = $total_ao = $total_mo = 0;
                @endphp
                <tbody>
                    @foreach ($data_sektor as $area => $result)
                @php
                    $total_nv += @$result['need_validate'];
                    $total_na += @$result['need_approve'];
                    $total_r += @$result['rejected'];
                    $total_rmt += @$result['return_mytech'];
                    $total_aprv += @$result['valid_qc2'];
                    $grand_total += @$result['jumlah'];
                    $total_ao += @$result['ps_ao'];
                    $total_mo += @$result['ps_mo'];
                @endphp
                    <tr>
                        <td style="background-color: #FBFBFB; color: black">{{ $area }}</td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/need_validate" target="_blank">{{ number_format(@$result['need_validate']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/need_approve" target="_blank">{{ number_format(@$result['need_approve']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/rejected" target="_blank">{{ number_format(@$result['rejected']) ? : '0' }}</a></td>
                        <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/return_mytech" target="_blank">{{ number_format(@$result['return_mytech']) ? : '0' }}</a></td>
                        <td style="background-color: #eef9f4 !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/valid_qc2" target="_blank">{{ number_format(@$result['valid_qc2']) ? : '0' }}</a></td>
                        <td style="background-color: #f9dbfb !important; color: black"><a style="color: black" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/all" target="_blank">{{ number_format(@$result['jumlah']) ? : '0' }}</a></td>

                        <td style="background-color: #85929E !important; color: white"><a style="color: white" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/ps_ao" target="_blank">{{ number_format(@$result['ps_ao']) ? : '0' }}</a></td>
                        <td style="background-color: #85929E !important; color: white"><a style="color: white" href="/dashboard/UTOnlineDetail/SEKTOR/{{ $area }}/{{ $date }}/{{ $datex }}/ps_mo" target="_blank">{{ number_format(@$result['ps_mo']) ? : '0' }}</a></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/need_validate" target="_blank">{{ number_format($total_nv) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/need_approve" target="_blank">{{ number_format($total_na) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/rejected" target="_blank">{{ number_format($total_r) }}</a></td>
                        <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/return_mytech" target="_blank">{{ number_format($total_rmt) }}</a></td>
                        <td style="background-color: #88d8b0 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/valid_qc2" target="_blank">{{ number_format($total_aprv) }}</a></td>
                        <td style="background-color: #ec87f3 !important"><a style="color: black; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/all" target="_blank">{{ number_format($grand_total) }}</a></td>

                        <td style="background-color: #5D6D7E !important"><a style="color: white !important; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/ps_ao" target="_blank">{{ number_format($total_ao) }}</a></td>
                        <td style="background-color: #5D6D7E !important"><a style="color: white !important; font-weight: bold" href="/dashboard/UTOnlineDetail/SEKTOR/ALL/{{ $date }}/{{ $datex }}/ps_mo" target="_blank">{{ number_format($total_mo) }}</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br /><center><b>SOURCE OF <a href="https://utonline.telkom.co.id/myevidence" target="_blank" style="color: #ff6f69 !important">UT ONLINE</a></b></center><br />
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            window.location.href = "/dashboard/UTOnline/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD');
        });
    });
</script>
@endsection