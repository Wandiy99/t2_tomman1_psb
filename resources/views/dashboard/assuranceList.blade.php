@extends('public_v2')

 @section('content')
<!--  @include('partial.alerts') -->

  <?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=Data Assurance ".$status." ".$date.".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
  ?>
  <!-- <h3>List Assurance {{ $status }} ({{ $date }})</h3> -->
  <!-- <div class="row"> -->
    <!-- <div class="col-sm-12"> -->
      <!-- <div class="table-responsive"> -->
      <table table border="1" width="100%">
        <tr align="center">
          <th>No.</th>
          <th>No Tiket</th>
          <th>Contact Phone</th>
          <th>Hasil Ukur</th>
          <th>Redaman Awal</th>
          <th>Redaman (Onu Rx)</th>
          <th>Service NO</th>
          <th>Service ID</th>
          <th>Order NCLI</th>
          <th>Datek</th>
          <th>ODP</th>
          <th>Tim</th>
          <th>Mitra</th>
          <th>Sektor</th>
          <th>Headline</th>
          <th>Tgl_Open</th>
          <th>Tgl_Close</th>
          <th>Tgl_Dispatch</th>
          <th>Status Laporan</th>
          <th>Segment Status</th>
          <th>Status Nossa</th>
          <th>Tgl_Manja</th>
          <th>STO</th>
          <th>DATEL</th>
          <th>Umur</th>
          <th>Sebab</th>
          <th>Action</th>
          <th>Penyebab Reti</th>
          <th>Catatan Tek.</th>
          <th>Type_ONT</th>
          <th>SN_ONT</th>
          <th>Type_STB</th>
          <th>SN_STB</th>
          <th>Close By User</th>
        </tr>
        @foreach ($getAssuranceList as $num => $AssuranceList)
        <tr>
          <center>
          <td>{{ ++$num }}</td>
          <td><!-- <a href="/tiket/{{ $AssuranceList->id_dt }}"> -->{{ $AssuranceList->Ndem }}<!-- </a> --></td>
          <td>{{ $AssuranceList->Contact_Phone ? : $AssuranceList->noPelangganAktif }}</td>
          <td>{{ $AssuranceList->status_ukur }}</td>
          <td>{{ $AssuranceList->ONU_Rx ? : '0' }}</td>
          <td>{{ $AssuranceList->redaman_iboster ? : '0' }}</td>
          <td>{{ $AssuranceList->Service_No ? : $AssuranceList->no_internet ? : $AssuranceList->rc_no_speedy }}</td>
          <td>{{ $AssuranceList->Service_ID }}</td>
          <td>{{ $AssuranceList->ncliOrder }}</td>
          <td>{{ $AssuranceList->Datek }}</td>
          <td>{{ $AssuranceList->nama_odp }}</td>
          <td>{{ $AssuranceList->uraian }}</td>
          <td>{{ $AssuranceList->mitra_amija_pt }}</td>
          <td>{{ $AssuranceList->sektor }}</td>
          <td>{{ $AssuranceList->Summary ? : $AssuranceList->rc_headline }}</td>
          @if($AssuranceList->Source <> "TOMMAN")
            <td>{{ $AssuranceList->Reported_Datex }}</td>
          @else
            <td>{{ $AssuranceList->Reported_Date }}</td>
          @endif

          @if ($AssuranceList->modified_at<>'' || $AssuranceList->created_at<>'' )
            @if ($AssuranceList->modified_at=='')
              <td>{{ $AssuranceList->created_at }}</td>
            @else
              <td>{{ $AssuranceList->modified_at }}</td>
            @endif
          @else
            <td></td>
          @endif
          <td>{{ $AssuranceList->tgl }}</td>
          <td>{{ $AssuranceList->laporan_status ? : 'ANTRIAN' }}</td>
          <td>{{ $AssuranceList->Segment_Status ? : '-' }}</td>
          <td>{{ $AssuranceList->aaIncident ? : 'CLOSED' }}</td>
          <td>{{ $AssuranceList->ca_manja_time }}</td>
          <td>{{ $AssuranceList->ioan }}</td>
          <td>{{ $AssuranceList->ad_datel }}</td>
          <td>{{ $AssuranceList->umur }}</td>
          <td>{{ $AssuranceList->penyebab }}</td>
          <td>{{ $AssuranceList->action }}</td>
          <td>{{ $AssuranceList->status_penyebab_reti }}</td>
          <td>{{ $AssuranceList->catatan }}</td>
          <td>{{ $AssuranceList->typeont }}</td>
          <td>{{ $AssuranceList->snont }}</td>
          <td>{{ $AssuranceList->typestb }}</td>
          <td>{{ $AssuranceList->snstb }}</td>
          <td>{{ $AssuranceList->close_by_user ? : '-' }}</td>
          </center>
        </tr>
        @endforeach
      </table>
    <!-- </div> -->
    <!-- </div> -->
  <!-- </div> -->
@endsection