@extends('layout')

@section('header')
	 <link rel="stylesheet" href="/bower_components/chartJs/Chart.min.css" />
@endsection

@section('content')
  @include('partial.alerts')
  <div class="panel panel-primary">
  		<div class="panel-heading">Grafik PS dan Belum PS</div>
  		<div class="panel-body table-responsive">
	  		<form method="GET">
	  			<div class="row">
	  				<div class="col-md-2">
			  			<label>Area</label>
			  			<input type="text" name="area" id="area" value="{{ $area }}">
			  			<input type="submit" value="Filter" class="btn btn-primary">
	  				</div>
	  			</div>
	  		</form>

  			<canvas id="myChart" width="400" height="100 "></canvas>

  			<table class="table table-bordered">
  				<tr>
  					<td width="5%"></td>
  					@foreach($status as $dt)
  						<td>{{ $dt }}</td>
  					@endforeach
  				</tr>
  				<tr>
  					<td>PS</td>
  					@foreach($ps as $pss)
  						<td>{{ $pss }}</td>
  					@endforeach
  				</tr>
  				<tr>
  					<td>Belum PS</td>
  					@foreach($belumPs as $pss)
  						<td>{{ $pss }}</td>
  					@endforeach
  				</tr>
  				<tr>
  					<td>Total</td>
  					@foreach($jumlah as $jlh)
  						<td>{{ $jlh }}</td>
  					@endforeach
  				</tr>
  			</table>
  		</div>
  </div>

@endsection

@section('plugins')
	 <script src="/bower_components/chartJs/Chart.min.js"></script>
	 <script>
		var ctx = document.getElementById('myChart');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: {!! json_encode($status) !!},
		        datasets: [{
		        	label: '# PS',
		            data: {!! json_encode($ps) !!},
		            backgroundColor: 'rgba(210, 105, 30, 0.5)', 
		            borderWidth: 1
		        },{
		        	label: '# Belum PS',
		            data: {!! json_encode($belumPs) !!},
		            backgroundColor: 'rgba(100, 149, 237, 0.5)', 
		            borderWidth: 1
		        },]
		    },
		    options: {
		    	title:{
		    		display:false,
		    		text : 'Grafik PS dan Belum PS WO Kendala',
		    	},
		        scales: {
		        	xAxes : [{
		        		stacked : true,
		        	}],		
		            yAxes: [{
		            	// stacked : true,
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		var data = [
			{'id':'ALL', 'text':'ALL'},
			{'id':'INNER', 'text':'INNER'},
			{'id':'BBR', 'text':'BBR'},
			{'id':'KDG', 'text':'KDG'},
			{'id':'TJL', 'text':'TJL'},
			{'id':'BLC', 'text':'BLC'},
		];

		$('#area').select2({
			data : data,
		});

	</script>

@endsection
