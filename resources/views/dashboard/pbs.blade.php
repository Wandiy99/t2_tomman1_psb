@extends('layout')

@section('content')
  @include('partial.alerts')
  <center>
  <h3>{{ $title }}</h3>
  <iframe frameborder="0" marginheight="0" marginwidth="0" title="data visualization" allowtransparency="true" allowfullscreen="true" class="tableauViz" width="1000" height="827" style="display: block; margin: 0px; padding: 0px; border: none; width: 1000px; height: 827px;" src="{{ $link }}"></iframe>
  </center>
  <br
@endsection