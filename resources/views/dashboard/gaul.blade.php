@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <div class="row">
    <div class="col-sm-12">
      <script type="text/javascript">
        jQuery(document).ready(function($)
        {
          $("#bar-3").dxChart({
            dataSource: [
              @foreach ($query1 as $result1)
              {action1 : "{{ $result1->action }}", val: {{ $result1->jumlah }}},
              @endforeach

              @foreach ($query2 as $result2)
              {action2 : "{{ $result2->action }}", val: {{ $result2->jumlah }}},
              @endforeach
              @foreach ($query3 as $result3)
              {action3 : "{{ $result3->action }}", val: {{ $result3->jumlah }}},
              @endforeach

            ],
            AutoLayout : false,
            title: "GAUL",
            tooltip: {
                enabled: true,
              customizeText: function() {
                return this.argumentText + "<br/>" + this.valueText;
              }
            },
            size: {
              height: 400
            },
            legend: {
              visible: true
            },

            rotated : true,
            series: [{
              label: {
                  visible: true,
                  customizeText: function (segment) {
                      return segment.percentText;
                  },
                  connector: {
                      visible: true
                  }
              },
              name : "Action 1",
              type: "bar",
              argumentField: "action1"
            },{
              label: {
                  visible: true,
                  customizeText: function (segment) {
                      return segment.percentText;
                  },
                  connector: {
                      visible: true
                  }
              },
              name : "Action 2",
              type: "bar",
              argumentField: "action2"
            },{
              label: {
                  visible: true,
                  customizeText: function (segment) {
                      return segment.percentText;
                  },
                  connector: {
                      visible: true
                  }
              },
              name : "Action 3",
              type: "bar",
              argumentField: "action3"
            },
            ]
          });
          });
          </script>

            <div id="bar-3" style="height: 700px; width: 100%;"></div>

    </div>
    <div class="col-sm-12">
      <h3>Detil GAUL</h3>
      <table class="table">
        <tr>
          <th>No.</th>
          <th>GAUL NUM</th>
          <th>TROUBLE_NUMBER</th>
          <th>NCLI</th>
          <th>TROUBLE_NO</th>
          <th>TEAM</th>
          <th>DATE_CLOSE</th>
          <th>ACTION</th>
          <th>ACTUAL_SOLUTION</th>
          <th>SYMPTOM_LV0</th>
        </tr>
        <?php
          $ncli = "";
          $nclix = "";
          $num = 1;

        ?>
        @foreach ($query as $i => $result)
        <?php
          if ($ncli <> $result->TROUBLE_NUMBER){
            $ncli = $result->TROUBLE_NUMBER;
            $nclix = $result->TROUBLE_NUMBER;
            $num = 1;
          } else {
            $num++;
            $nclix = "";
          }
        ?>
        <tr>
          <td>{{ ++$i }}</td>
          <td>GAUL{{ $num }}</td>
          <td>{{ $nclix }}</td>
          <td>{{ $result->TROUBLE_NUMBER }}</td>
          <td>{{ $result->TROUBLE_NO }}</td>
          <td>{{ $result->uraian }}</td>
          <td>{{ $result->DATE_CLOSE }}</td>
          <td>{{ $result->action }}</td>
          <td>{{ $result->ACTUAL_SOLUTION }}</td>
          <td>{{ $result->SYMPTOM_LV0 }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
