@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL DISMANTLING {{ $group }} {{ $sektor }} {{ $type }} STATUS {{ $status }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        {{-- <th>JENIS ORDER</th> --}}
                        <th>TIM</th>
                        <th>NOPEL</th>
                        <th>NAMA PELANGGAN</th>
                        <th>SN ONT (TOMMAN)</th>
                        <th>SN STB (TOMMAN)</th>
                        @if(in_array($type, ["SISA_ORDER","DONE","DISPATCH"]))
                            @if($type == "DONE")
                                <th>STATUS</th>
                            @endif
                        <th>PIC</th>
                        <th>STO</th>
                        <th>ALPRONAME</th>
                        <th>ORDER ID</th>
                        <th>JENIS NTE</th>
                        <th>SN</th>
                        <th>LAT</th>
                        <th>LON</th>
                        <th>ALAMAT</th>
                        <th>TGL ORDER</th>
                        <th>TGL DISPATCH</th>
                        <th>STATUS TEKNISI</th>
                        <th>TIPE DAPROS</th>
                        <th>TAHUN DAPROS</th>
                        <th>TGL JANJI AMBIL</th>
                        @elseif($type == "COLLECTED")
                        <th>JENIS NTE</th>
                        <th>SN</th>
                        <th>NIK</th>
                        <th>NAMA PETUGAS CTB</th>
                        <th>TGL CREATE</th>
                        <th>TGL MASUK GUDANG</th>
                        <th>NAMA WH</th>
                        <th>PETUGAS WH</th>
                        <th>TGL APPROVAL</th>
                        <th>TGL KASIR</th>
                        <th>TIPE DAPROS</th>
                        <th>TAHUN DAPROS</th>
                        @elseif($type == "VISIT")
                        <th>NIK</th>
                        <th>NAMA PETUGAS CTB</th>
                        <th>NAMA YANG DITEMUI</th>
                        <th>ALASAN</th>
                        <th>TGL CREATE</th>
                        <th>ONT</th>
                        <th>STB</th>
                        <th>PLC</th>
                        <th>WIFI EXTENDER</th>
                        <th>INDIBOX</th>
                        <th>INDIHOMESMART</th>
                        <th>NO BA</th>
                        <th>TGL JANJI BAYAR</th>
                        <th>TIPE DAPROS</th>
                        <th>TAHUN DAPROS</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($query as $num => $result)
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->area }}</td>
                        {{-- <td>{{ $result->jenis_order }}</td> --}}
                        <td>{{ $result->tim }}</td>
                        @if($result->id_dt <> "")
                            <td><a href="/{{$result->id_dt}}">{{ $result->nopel }}</a></td>
                        @else
                            <td>{{ $result->nopel }}</td>
                        @endif
                        <td>{{ @$result->nama_pelanggan ? : @$result->namapelanggan }}</td>
                        <td>{{ $result->snont }}</td>
                        <td>{{ $result->snstb }}</td>
                        @if(in_array($type, ["SISA_ORDER","DONE","DISPATCH"]))
                            @if($type == "DONE")
                                <td>{{ $result->status_dismantling }}</td>
                            @endif
                        <td>{{ $result->no_hp }}</td>
                        <td>{{ $result->sto }}</td>
                        <td>{{ $result->alproname }}</td>
                        <td>{{ $result->order_id }}</td>
                        <td>{{ $result->jenis_nte }}</td>
                        <td>{{ $result->crd_sn ? : $result->snstb }}</td>
                        <td>{{ $result->lat }}</td>
                        <td>{{ $result->lon }}</td>
                        <td>{{ $result->alamat }}</td>
                        <td>{{ $result->tgl_order }}</td>
                        <td>{{ $result->tgl_dt }}</td>
                        <td>{{ $result->laporan_status ? : 'ANTRIAN' }}</td>
                        <td>{{ @$result->tipe_dapros ? : @$result->jenis_dismantling ? : 'TOMMAN HD KALSEL' }}</td>
                        <td>{{ $result->dapros_tahun }}</td>
                        <td>{{ $result->ambil_janji_tgl }}</td>
                        @elseif($type == "COLLECTED")
                        <td>{{ $result->jenis_nte }}</td>
                        <td>{{ $result->sn }}</td>
                        <td>{{ $result->nik }}</td>
                        <td>{{ $result->namapetugasctb }}</td>
                        <td>{{ $result->tanggalcreate }}</td>
                        <td>{{ $result->tanggalmasukgudang }}</td>
                        <td>{{ $result->nama_wh }}</td>
                        <td>{{ $result->petugas_wh }}</td>
                        <td>{{ $result->tanggalapproval }}</td>
                        <td>{{ $result->tanggalkasir }}</td>
                        <td>{{ $result->tipe_dapros }}</td>
                        <td>{{ $result->tahun_dapros }}</td>
                        @elseif($type == "VISIT")
                        <td>{{ $result->nik }}</td>
                        <td>{{ $result->namapetugasctb }}</td>
                        <td>{{ $result->nama_yg_ditemui }}</td>
                        <td>{{ $result->alasan }}</td>
                        <td>{{ $result->tanggalcreate }}</td>
                        <td>{{ $result->ont }}</td>
                        <td>{{ $result->stb }}</td>
                        <td>{{ $result->plc }}</td>
                        <td>{{ $result->wifiextender }}</td>
                        <td>{{ $result->indibox }}</td>
                        <td>{{ $result->indihomesmart }}</td>
                        <td>{{ $result->no_ba }}</td>
                        <td>{{ $result->tgl_janji_bayar }}</td>
                        <td>{{ $result->tipe_dapros }}</td>
                        <td>{{ $result->tahun_dapros }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL ORDER DISMANLTING / CABUTAN NTE TOMMAN'
                }
            ]
        });
    });
</script>
@endsection