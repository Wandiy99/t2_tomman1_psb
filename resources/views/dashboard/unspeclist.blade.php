@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>List Unspec {{ $sto }}</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">Table</div>
  			<div class="panel-body">
  				<table class="table table-stripped">
  					<tr>
  						<th></th>
  						<th>No</th>
  						<th>Incident</th>
  						<th>Nama</th>
  						<th>Summary</th>
  						<th>Service No</th>
  						<th>Datek</th> 
  					</tr>
  					@foreach ($query as $no => $result)
  					<tr>
  						<td><a href="/assurance/dispatch/{{ $result->Incident }}">Dispatch</a></td>
  						<td>{{ ++$no }}</td>
  						<td>{{ $result->Incident }}</td>
  						<td>{{ $result->Customer_Name }}</td>
  						<td>{{ $result->Summary }}</td>
  						<td>{{ $result->Service_No }}</td>
  						<td>{{ $result->Datek }}</td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection