@extends('layout')

@section('content')
  @include('partial.alerts')
  {{-- <style>
    .label {
      font-size: 12px;
    }

    td {
      padding : 5px;
      border:1px solid #ecf0f1;

    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7 !important;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
  </style> --}}
  <center>
    <h3>Dashboard Fulfillment Guarante</h3><br />
  </center>
  
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Action Today
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <tr>
              <th>Action</th>
              <th>Jumlah</th>
            </tr>
            <?php
              $total = 0;
            ?>
            @foreach ($getAction as $Action)
            <tr>
              <td>{{ $Action->action }}</td>
              <td><a href="/listactionguarante/{{ $tgl }}/{{ $Action->action }}">{{ $Action->jumlah }}</a></td>
            </tr>
            <?php
              $total = $total + $Action->jumlah;
            ?>
            @endforeach
            <tr>
              <td>Total</td>
              <td><a href="/listactionguarante/{{ $tgl }}/ALL">{{ $total }}</a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
