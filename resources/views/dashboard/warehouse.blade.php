@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    .xinput {
        resize: horizontal;
        width: 330px;
        text-align: center;
    }
    
    .xinput:active {
        width: auto;   
    }
    
    .xinput:focus {
        min-width: 200px;
    }
</style>

<div class="panel panel-primary">
<div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">DASHBOARD REKAP</br>INVENTORY & ASSET WAREHOUSE</br>PERIODE {{ $date }}</h4></div>
        <div class="table-responsive">
            <div class="col-sm-12">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">AREA</th>
                            <th class="align-middle" colspan='11' style="background-color: #5bc0de !important; color: black">ONT</th>
                            <th class="align-middle" colspan='3' style="background-color: #5bc0de !important; color: black">STB</th>
                            <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">INDIBOX</th>
                            <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">IP CAM</th>
                            <!-- <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">GRAND TOTAL</th> -->
                        </tr>
                        <tr>
                            <th class="align-middle" style="background-color: #ff9a96 !important; color: black">ZTE-F660</th>
                            <th class="align-middle" style="background-color: #ff9a96 !important; color: black">ZTE-F609</th>
                            <th class="align-middle" style="background-color: #ff9a96 !important; color: black">ZTE-F670L</th>
                            <th class="align-middle" style="background-color: #ff9a96 !important; color: black">ZTE-F821</th>
                            <th class="align-middle" style="background-color: #ff9a96 !important; color: black">ZTE-F829</th>
                            <th class="align-middle" style="background-color: #76D7C4 !important; color: black">HG82455A</th>
                            <th class="align-middle" style="background-color: #76D7C4 !important; color: black">HG8245H5</th>
                            <th class="align-middle" style="background-color: #76D7C4 !important; color: black">H87Z5675M21</th>
                            <th class="align-middle" style="background-color: #76D7C4 !important; color: black">HG6243C</th>
                            <th class="align-middle" style="background-color: #3498DB !important; color: black">ALU-G240WA</th>
                            <th class="align-middle" style="background-color: #3498DB !important; color: black">ALU-I240WA</th>

                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">ZTE-B700V5</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">ZTE-B760H</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">ZTE-B660H</th>
                        </tr>
                    </thead>
                    @php
                    $total_ZTEF660 = 0;
                    $total_ZTEF609 = 0;
                    $total_ZTEF670L = 0;
                    $total_ZTEF821 = 0;
                    $total_ZTEF829 = 0;
                    $total_HG82455A = 0;
                    $total_HG8245H5 = 0;
                    $total_H87Z5675M21 = 0;
                    $total_HG6243C = 0;
                    $total_ALUG240WA = 0;
                    $total_ALUI240WA = 0;
                    $total_ZTEB700V5 = 0;
                    $total_ZTEB760H = 0;
                    $total_ZTEB660H = 0;
                    $total_INDIBOX = 0;
                    $total_IPCAM = 0;
                    $grand_total = 0;
                    $total_grand = 0;
                    @endphp
                    <tbody>
                        @foreach ($get_data as $value)
                    @php
                    $total_ZTEF660 += $value->ZTEF660;
                    $total_ZTEF609 += $value->ZTEF609;
                    $total_ZTEF670L += $value->ZTEF670L;
                    $total_ZTEF821 += $value->ZTEF821;
                    $total_ZTEF829 += $value->ZTEF829;
                    $total_HG82455A += $value->HG82455A;
                    $total_HG8245H5 += $value->HG8245H5;
                    $total_H87Z5675M21 += $value->H87Z5675M21;
                    $total_HG6243C += $value->HG6243C;
                    $total_ALUG240WA += $value->ALUG240WA;
                    $total_ALUI240WA += $value->ALUI240WA;
                    $total_ZTEB700V5 += $value->ZTEB700V5;
                    $total_ZTEB760H += $value->ZTEB760H;
                    $total_ZTEB660H += $value->ZTEB660H;
                    $total_INDIBOX += $value->INDIBOX;
                    $total_IPCAM += $value->IPCAM;
                    $grand_total = $value->ZTEF660 + $value->ZTEF609 + $value->ZTEF670L + $value->ZTEF821 + $value->ZTEF829 + $value->HG82455A + $value->HG8245H5 + $value->H87Z5675M21 + $value->HG6243C + $value->ALUG240WA + $value->ALUI240WA + $value->ZTEB700V5 + $value->ZTEB760H + $value->ZTEB660H + $value->INDIBOX + $value->IPCAM;
                    $total_grand += $total_ZTEF660 + $total_ZTEF609 + $total_ZTEF670L + $total_ZTEF821 + $total_ZTEF829 + $total_HG82455A + $total_HG8245H5 + $total_H87Z5675M21 + $total_HG6243C + $total_ALUG240WA + $total_ALUI240WA + $total_ZTEB700V5 + $total_ZTEB760H + $total_ZTEB660H + $total_INDIBOX + $total_IPCAM;
                    @endphp
                        <tr>
                            <td style="background-color: #FBFBFB; color: black">{{ $value->so_inv_active ? : '#N/A' }}</td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF660&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEF660 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF609&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEF609 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF670L&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEF670L }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF821&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEF821 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF829&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEF829 }}</a></td>
                            <td style="background-color: #D1F2EB !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG82455A&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->HG82455A }}</a></td>
                            <td style="background-color: #D1F2EB !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG8245H5&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->HG8245H5 }}</a></td>
                            <td style="background-color: #D1F2EB !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=H87Z5675M21&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->H87Z5675M21 }}</a></td>
                            <td style="background-color: #D1F2EB !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG6243C&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->HG6243C }}</a></td>
                            <td style="background-color: #D6EAF8 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ALUG240WA&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ALUG240WA }}</a></td>
                            <td style="background-color: #D6EAF8 !important; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ALUI240WA&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ALUI240WA }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB700V5&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEB700V5 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB760H&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEB760H }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB660H&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->ZTEB660H }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=INDIBOX&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->INDIBOX }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=IPCAM&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->IPCAM }}</a></td>
                            <!-- <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/recap?date={{ $date }}&type=ALL&nte=ALL&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $grand_total }}</a></td> -->
                        </tr>
                        @endforeach
                        <tr>
                            <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF660&area=ALL" target="_blank">{{ $total_ZTEF660 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF609&area=ALL" target="_blank">{{ $total_ZTEF609 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF670L&area=ALL" target="_blank">{{ $total_ZTEF670L }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF821&area=ALL" target="_blank">{{ $total_ZTEF821 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ZTEF829&area=ALL" target="_blank">{{ $total_ZTEF829 }}</a></td>
                            <td style="background-color: #76D7C4 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG82455A&area=ALL" target="_blank">{{ $total_HG82455A }}</a></td>
                            <td style="background-color: #76D7C4 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG8245H5&area=ALL" target="_blank">{{ $total_HG8245H5 }}</a></td>
                            <td style="background-color: #76D7C4 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=H87Z5675M21&area=ALL" target="_blank">{{ $total_H87Z5675M21 }}</a></td>
                            <td style="background-color: #76D7C4 !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=HG6243C&area=ALL" target="_blank">{{ $total_HG6243C }}</a></td>
                            <td style="background-color: #3498DB !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ALUG240WA&area=ALL" target="_blank">{{ $total_ALUG240WA }}</a></td>
                            <td style="background-color: #3498DB !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ONT&nte=ALUI240WA&area=ALL" target="_blank">{{ $total_ALUI240WA }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB700V5&area=ALL" target="_blank">{{ $total_ZTEB700V5 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB760H&area=ALL" target="_blank">{{ $total_ZTEB760H }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=ZTEB660H&area=ALL" target="_blank">{{ $total_ZTEB660H }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=INDIBOX&area=ALL" target="_blank">{{ $total_INDIBOX }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=STB&nte=IPCAM&area=ALL" target="_blank">{{ $total_IPCAM }}</a></td>
                            <!-- <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/recap?date={{ $date }}&type=ALL&nte=ALL&area=ALL" target="_blank">{{ $total_grand }}</a></td> -->
                        </tr>
                    </tbody>
                </table>
            </br></br>
            <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">AREA</th>
                            <th class="align-middle" colspan='10' style="background-color: #5bc0de !important; color: black">TAMBAHAN</th>
                            <th class="align-middle" colspan='6' style="background-color: #5bc0de !important; color: black">MATERIAL</th>
                            <th class="align-middle" colspan='2' style="background-color: #5bc0de !important; color: black">INSERT TIANG</th>
                        </tr>
                        <tr>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">BREKET-A</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PULSTRAP</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">CONNECTOR</br>RJ45</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">RS-IN-SC-1</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">S-CLAMP-SPRINER</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">SOC-ILS</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">SOC-SUM</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">TC-SM-OTP-1</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">CABLE-UTP</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">TRAY-CABLE</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">AC-OF-SM-1B / DC-ROLL</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PRECON-50</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PRECON-75</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PRECON-80</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PRECON-100</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">PRECON-150</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">TIANG-7</th>
                            <th class="align-middle" style="background-color: #5bc0de !important; color: black">TIANG-9</th>
                        </tr>
                    </thead>
                    @php
                    $total_breket = 0;
                    $total_pulstrap = 0;
                    $total_RJ45 = 0;
                    $total_roset = 0;
                    $total_sclamp = 0;
                    $total_socils = 0;
                    $total_socsum = 0;
                    $total_tcsmotp = 0;
                    $total_utp = 0;
                    $total_tray_cable = 0;
                    $total_dc_roll = 0;
                    $total_precon50 = 0;
                    $total_precon75 = 0;
                    $total_precon80 = 0;
                    $total_precon100 = 0;
                    $total_precon150 = 0;
                    $total_tiang7 = 0;
                    $total_tiang9 = 0;
                    @endphp
                    <tbody>
                        @foreach ($get_datax as $value)
                    @php
                    $total_breket += $value->breket;
                    $total_pulstrap += $value->pulstrap;
                    $total_RJ45 += $value->RJ45;
                    $total_roset += $value->roset;
                    $total_sclamp += $value->sclamp;
                    $total_socils += $value->socils;
                    $total_socsum += $value->socsum;
                    $total_tcsmotp += $value->tcsmotp;
                    $total_utp += $value->utp;
                    $total_tray_cable += $value->tray_cable;
                    $total_dc_roll += $value->dc_roll;
                    $total_precon50 += $value->precon50;
                    $total_precon75 += $value->precon75;
                    $total_precon80 += $value->precon80;
                    $total_precon100 += $value->precon100;
                    $total_precon150 += $value->precon150;
                    $total_tiang7 += $value->tiang7;
                    $total_tiang9 += $value->tiang9;
                    @endphp
                        <tr>
                            <td style="background-color: #FBFBFB; color: black">{{ $value->so_inv_active ? : '#N/A' }}</td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=breket&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->breket }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=pulstrap&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->pulstrap }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=RJ45&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->RJ45 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=roset&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->roset }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=sclamp&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->sclamp }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=socils&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->socils }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=socsum&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->socsum }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=tcsmotp&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->tcsmotp }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=utp&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->utp }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=tray_cable&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->tray_cable }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=dc_roll&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->dc_roll }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=precon50&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->precon50 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=precon75&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->precon75 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=precon80&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->precon80 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=precon100&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->precon100 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=precon150&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->precon150 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=tiang7&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->tiang7 }}</a></td>
                            <td style="background-color: #FBFBFB; color: black"><a style="color: black" href="/dashboard/warehouse/data?date={{ $date }}&material=tiang9&area={{ $value->so_inv_active ? : 'NA' }}" target="_blank">{{ $value->tiang9 }}</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=breket&area=ALL" target="_blank">{{ $total_breket }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=pulstrap&area=ALL" target="_blank">{{ $total_pulstrap }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=RJ45&area=ALL" target="_blank">{{ $total_RJ45 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=roset&area=ALL" target="_blank">{{ $total_roset }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=sclamp&area=ALL" target="_blank">{{ $total_sclamp }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=socils&area=ALL" target="_blank">{{ $total_socils }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=socsum&area=ALL" target="_blank">{{ $total_socsum }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=tcsmotp&area=ALL" target="_blank">{{ $total_tcsmotp }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=utp&area=ALL" target="_blank">{{ $total_utp }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=tray_cable&area=ALL" target="_blank">{{ $total_tray_cable }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=dc_roll&area=ALL" target="_blank">{{ $total_dc_roll }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=precon50&area=ALL" target="_blank">{{ $total_precon50 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=precon75&area=ALL" target="_blank">{{ $total_precon75 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=precon80&area=ALL" target="_blank">{{ $total_precon80 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=precon100&area=ALL" target="_blank">{{ $total_precon100 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=precon150&area=ALL" target="_blank">{{ $total_precon150 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=tiang7&area=ALL" target="_blank">{{ $total_tiang7 }}</a></td>
                            <td style="background-color: #5bc0de !important"><a style="color: black; font-weight: bold" href="/dashboard/warehouse/data?date={{ $date }}&material=tiang9&area=ALL" target="_blank">{{ $total_tiang9 }}</a></td>
                        </tr>
                    </tbody>
                </table>
            </br>
        </div>
    </div>
</div>
@endsection