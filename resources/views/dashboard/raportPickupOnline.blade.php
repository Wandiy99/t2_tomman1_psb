@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL RAPORT PICKUP ONLINE NPER {{ $nper }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap align-middle" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SEKTOR</th>
                        <th>TEAM</th>
                        <th>ORDER_ID</th>
                        <th>CUSTOMER_NAME</th>
                        <th>STATUS_KPRO</th>
                        <th>TGL_PS_KPRO</th>
                        <th>STATUS_PICKUP</th>
                        <th>LAST_STATUS_PICKUP</th>
                        <th>TGL_PS_PICKUP</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->sektor ? : 'NON AREA' }}</td>
                        <td>{{ $data->team ? : '-' }}</td>
                        <td>{{ $data->order_id ? : '0' }}</td>
                        <td>{{ $data->customer_name ? : '#N/A' }}</td>
                        <td>{{ $data->status_kpro ? : '#N/A' }}</td>
                        <td>{{ $data->tgl_ps_kpro ? : '#N/A' }}</td>
                        <td>{{ $data->status_pickup ? : '#N/A' }}</td>
                        <td>{{ $data->last_status_pickup ? : '#N/A' }}</td>
                        <td>{{ $data->tgl_ps_pickup ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL RAPORT PICKUP ONLINE PERIODE BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection