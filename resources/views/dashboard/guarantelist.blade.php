@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/guarante/{{ $date }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Fulfillment Guarantee {{ $status }} ({{ $date }})</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>No Tiket</th>
          <th>References SC</th>
          <th>Tiket</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>Order</th>
          <th>Headline</th>
          <th>Tgl_Open</th>
          <th>Status Laporan</th>
          <th>STO</th>
          <th>Umur</th>
          <th>Penyebab</th>
          <th>Action</th>
          <th>Catatan Tek.</th>
        </tr>

        @foreach ($getGuaranteList as $num => $guaranteList)
        <tr>
          <td>{{ ++$num }}</td>
          
          <?php
              $tiket = substr($guaranteList->no_tiket, 0, 2);
              if ($tiket=='IN'){
                  $noTiket = $guaranteList->no_tiket;
              }
              else{
                  $noTiket = 'FG'.$guaranteList->no_tiket;
              };
           ?>

          <td><a href="/guarantee/{{ $guaranteList->id }}">{{ $noTiket }}</a></td>
          <td>{{ $guaranteList->refSc<>'' ? $guaranteList->refSc : '~'}}</td>
          <td>{{ $guaranteList->updated_at }}</td>
          <td>
            {{ $guaranteList->uraian }}
          </td>
          <td>
            {{ $guaranteList->sektor }}
          </td>
          <td>
            {{ $guaranteList->no_telp }} ~ {{ $guaranteList->no_speedy }}<br />
          </td>
          <td>{{ $guaranteList->headline }}</td>
          <td>{{ $guaranteList->tgl_open }}</td>
          <td>{{ $guaranteList->laporan_status ? : 'ANTRIAN' }}</td>
          <td>{{ $guaranteList->sto_trim }}</td>
          <td>{{ $guaranteList->umur }}</td>
          <td>{{ $guaranteList->penyebab }}</td>
          <td>{{ $guaranteList->action }}</td>
          <td>{{ $guaranteList->catatan }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
