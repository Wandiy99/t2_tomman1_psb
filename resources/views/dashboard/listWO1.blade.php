@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL ACTCOMP DATEL {{ $datel }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>AREA</th>
                  <th>TIM</th>
                  <th>TL</th>
                  <th>SC</th>
                  <th>INTERNET</th>
                  <th>STATUS</th>
                  <th>ORDE RDATE</th>
                  <th>TTI</th>
                  <th>WFM ID</th>
                  <th>REDAMAN</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($query as $num => $result)
                <?php
                  date_default_timezone_set('Asia/Makassar');
                  $dateStart = strtotime($result->orderDate);
                  $dateEnd = strtotime(date('Y-m-d H:i:s'));
                  $diff = $dateEnd - $dateStart;
                  $jam = floor($diff / (60 * 60));
                ?>
                <tr>
                  <td>{{ ++$num }}</td>
                  <td>{{ $result->area }}</td>
                  <td>{{ $result->uraian }}</td>
                  <td>{{ $result->TL }}</td>
                  <td>{{ $result->orderId }}</td>
                  <td>{{ $result->internet }}</td>
                  <td>{{ $result->orderStatus }}</td>
                  <td>{{ $result->orderDate }}</td>
                  <td>{{ $jam }}</td>
                  <td>{{ $result->wfm_id ? : $result->cda_wfm_id }}</td>
                  <td>{{ $result->ONU_Rx ? : $result->redaman_iboster ? : '0' }} dbm</td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            extend: 'copy',
            title: 'DETAIL ACTCOMP STARCLICKNCX TOMMAN'
          },
          {
            extend: 'excel',
            title: 'DETAIL ACTCOMP STARCLICKNCX TOMMAN'
          },
          {
            extend: 'print',
            title: 'DETAIL ACTCOMP STARCLICKNCX TOMMAN'
          }
        ]
      });
    });
</script>
@endsection