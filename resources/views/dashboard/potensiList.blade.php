@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/provisioning/{{ $date }}/ALL" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List Potensi {{ $jenis }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>

          @if ($jenis=='undispatch')
              <th>Tgl / Jam Order</th>
          @elseif($jenis=='dispatched')
              <th>Tgl / Jam Order</th>
              <th>Tgl / Jam Dispatch</th>
          @endif

          <th>Regu</th>
          <th>STO</th>
          <th>Order ID</th>
          <th>NdemPots</th>
          <th>NdemSpeedy</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>orderStatus</th>
          <th>jenisPsb</th>
         
          @if ($jenis=="kendala")
              <th>Status Tek</th>
              <th>Catatan Tek</th>
              <th>Catatan Revoke</th>
          @endif

          <th>MYIR</th>

        </tr>
        @foreach ($data as $num => $result)
        <tr>
          <td>{{ ++$num }}</td>

          @if ($jenis=='undispatch')          
            <?php 
                date_default_timezone_set('Asia/Makassar');
                if ($result->orderDate<>''){
                    $awal  = date_create($result->orderDate);
                    $akhir = date_create(); // waktu sekarang
                    $diff  = date_diff( $awal, $akhir );
                                        
                    if($diff->d <> 0){
                          $waktu = $diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                    }
                    else {
                          $waktu = $diff->h.' Jam | '.$diff->i.' Menit'; 
                    }
                }
                else{
                  $waktu = '';
                }
            ?>
               <td>{{ date('d-m-Y H:i:s',strtotime($result->orderDate)) }} <br> <label class="label label-danger">{{ $waktu }}</label></td>
          @elseif($jenis=='dispatched')
              <?php
                  date_default_timezone_set('Asia/Makassar');
                  if ($result->updated_at<>''){
                      $awal  = date_create($result->orderDate);
                      $akhir = date_create($result->updated_at); // waktu sekarang
                      $diff  = date_diff( $awal, $akhir );
                        
                      if($diff->d <> 0){
                          $waktu = $diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                      }
                      else {
                          $waktu = $diff->h.' Jam | '.$diff->i.' Menit'; 
                      }

                  }
                  else{
                    $waktu = '';
                  }
             ?>
             <td>{{ date('d-m-Y H:i:s',strtotime($result->orderDate)) }}</td>
             <td>{{ date('d-m-Y H:i:s',strtotime($result->updated_at)) }} <br> <label class="label label-danger">{{ $waktu }}</label></td>
          @endif

          <td>
            @if(session('auth')->level == 2)
              <a href="/dispatch/{{ $result->orderId }}" class="label label-info">{{ $result->uraian ? : 'undispatch' }}</a>
            @else
              {{ $result->uraian ? : 'undispatch' }}
            @endif
          </td>
          <td>{{ $result->sto ? : $result->mdf }}</td>
          <td>{{ $result->orderId }}</td>
          <td>{{ $result->ndemPots }}</td>
          <td>{{ $result->ndemSpeedy }}</td>
          <td>{{ $result->orderName }}</td>
          <td>{{ $result->orderCity }}</td>
          <td>{{ $result->orderStatus }}</td>
          <td>{{ $result->jenisPsb }}</td>

          @if ($jenis=="kendala")
              <td>{{ $result->laporan_status }}</td>
              <td>{{ $result->catatan }}</td>
              <td>{{ $result->catatanRevoke ? : '~' }}</td>
          @endif

          <td>{{ $result->kcontact }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection
