@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/warehouse?date={{ $date }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">REKAP INVENTORY & ASSET AREA {{ $area }} {{ $nte }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>ID ORDER</th>
                        <th>CUSTOMER</th>
                        <th>STATUS LAPORAN</th>
                        <th>CATATAN (TEKNISI)</th>
                        <th>KOORDINAT PELANGGAN (TEKNISI)</th>
                        @if($type == "ONT")
                            <th>TYPE ONT</th>
                            <th>SN ONT</th>
                        @elseif($type == "STB")
                            <th>TYPE STB</th>
                            <th>SN STB</th>
                        @else
                            <th>TYPE ONT</th>
                            <th>SN ONT</th>
                            <th>TYPE STB</th>
                            <th>SN STB</th>
                        @endif
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>SEKTOR</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->so_inv_active ? : '#N/A' }}</td>
                        <td>{{ $data->Ndem }}</td>
                        <td>{{ $data->orderName ? : $data->customer ? : $data->Customer_Name ? : '#N/A' }}</td>
                        <td>{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td>{{ $data->catatan ? : '#N/A' }}</td>
                        <td>{{ $data->kordinat_pelanggan ? : '#N/A' }}</td>
                        @if($type == "ONT")
                            <td>{{ $data->typeont }}</td>
                            <td>{{ $data->snont }}</td>
                        @elseif($type == "STB")
                            <td>{{ $data->typestb }}</td>
                            <td>{{ $data->snstb }}</td>
                        @else
                            <td>{{ $data->typeont }}</td>
                            <td>{{ $data->snont }}</td>
                            <td>{{ $data->typestb }}</td>
                            <td>{{ $data->snstb }}</td>
                        @endif
                        <td>{{ $data->uraian ? : '#N/A' }}</td>
                        <td>{{ $data->mitra_amija_pt ? : '#N/A' }}</td>
                        <td>{{ $data->title ? : '#N/A' }}</td>
                        <td>{{ $data->modified_at ? : '#N/A' }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>ID ORDER</th>
                        <th>CUSTOMER</th>
                        <th>STATUS LAPORAN</th>
                        <th>CATATAN (TEKNISI)</th>
                        <th>KOORDINAT PELANGGAN (TEKNISI)</th>
                        @if($type == "ONT")
                            <th>TYPE ONT</th>
                            <th>SN ONT</th>
                        @elseif($type == "STB")
                            <th>TYPE STB</th>
                            <th>SN STB</th>
                        @else
                            <th>TYPE ONT</th>
                            <th>SN ONT</th>
                            <th>TYPE STB</th>
                            <th>SN STB</th>
                        @endif
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>SEKTOR</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'REKAP NTE INVENTORY & ASSET AREA TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'REKAP NTE INVENTORY & ASSET AREA TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'REKAP NTE INVENTORY & ASSET AREA TOMMAN'
                }
            ]
        });
    });
</script>
@endsection