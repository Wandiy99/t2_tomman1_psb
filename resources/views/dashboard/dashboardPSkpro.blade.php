@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/UTOnline/{{ $date }}/{{ $datex }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT PS K-PRO {{ $type }} {{ $area }} PERIODE {{ $date }} S/D {{ $datex }} UNIT {{ str_replace('_', ' ', $status) }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        @if($type == 'MITRA')
                        <th>MITRA</th>
                        @elseif($type == 'SEKTOR')
                        <th>SEKTOR</th>
                        @endif
                        <th>TIM</th>
                        <th>TL</th>
                        <th>ORDER ID</th>
                        <th>CUSTOMER NAME</th>
                        <th>STATUS RESUME</th>
                        <th>ORDER DATE</th>
                        <th>TANGGAL PROSES</th>
                        <th>NO INET</th>
                        <th>NO VOICE</th>
                        <th>CATEGORY</th>
                        <th>JENIS PSB</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        @if($type == 'MITRA')
                        <td>{{ $data->mitra ? : '#N/A' }}</td>
                        @elseif($type == 'SEKTOR')
                        <td>{{ $data->sektor ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        <td>{{ $data->TL ? : '#N/A' }}</td>
                        <td>{{ $data->ORDER_ID ? : '#N/A' }}</td>
                        <td>{{ $data->CUSTOMER_NAME ? : '#N/A' }}</td>
                        <td>{{ $data->STATUS_RESUME ? : '#N/A' }}</td>
                        <td>{{ $data->ORDER_DATE ? : '#N/A' }}</td>
                        <td>{{ $data->TANGGAL_PROSES ? : '#N/A' }}</td>
                        <td>{{ $data->SPEEDY ? : '#N/A' }}</td>
                        <td>{{ $data->POTS ? : '#N/A' }}</td>
                        <td>{{ $data->CATEGORY ? : '#N/A' }}</td>
                        <td>{{ $data->JENIS_PSB ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT PS K-PRO TOMMAN'
                }
            ]
        });
    });
</script>
@endsection