@extends('layout')
@section('content')
<style>
    th, td {
    text-align: center;
    vertical-align: middle;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@include('partial.alerts')

<div class="row">

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD KENDALA PROVISIONING<br />PERIODE {{ $startDate }} S/D {{ $endDate }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <div class="col-md-4">
                        <label for="input-type-view">View</label>
                        <select class="form-control select2" id = "input-type-view" name="view">
                            <option value="" selected disabled>Choose View</option>
                            @foreach ($get_view as $v)
                            <option data-subtext="description 1" value="{{ @$v->id }}" <?php if ($v->id == $view) { echo "Selected"; } else { echo ""; } ?>>{{ $v->text }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="col-md-3">
                        <label>Tanggal Awal</label>
                        <input type="date" name="startDate" id="startDate" class="form-control" value="{{ $startDate }}">
                    </div>

                    <div class="col-md-3">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="endDate" id="endDate" class="form-control" value="{{ $endDate }}">
                    </div>

                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<table class="table table-bordered dataTable">
    <tr>
        <th class="align-middle" rowspan="2">{{ strtoupper($view) }}</th>
        <th class="align-middle" colspan="4">KENDALA TEKNIS (CANCEL)</th>
        <th class="align-middle" colspan="4">KENDALA TEKNIS (TINJUT)</th>
    </tr>
    <tr>
        <th class="align-middle">KENDALA ALPRO</th>
        <th class="align-middle">KENDALA JALUR/NYEBRANG</th>
        <th class="align-middle">ODP JAUH</th>
        <th class="align-middle">NO ODP</th>
        <th class="align-middle">ODP FULL</th>
        <th class="align-middle">ODP LOSS / RETI</th>
        <th class="align-middle">INSERT TIANG</th>
        <th class="align-middle">ONU VALINS LIMITASI >32</th>
    </tr>
    @php
        $kendala_alpro = $kendala_jalur = $odp_jauh = $no_odp = $odp_full = $odp_full = $odp_loss_reti = $insert_tiang = $onu_32 = 0; 
    @endphp
    @foreach ($data1 as $key => $value)
    @php
        $kendala_alpro += $value->kendala_alpro;
        $kendala_jalur += $value->kendala_jalur;
        $odp_jauh += $value->odp_jauh;
        $no_odp += $value->no_odp;
        $odp_full += $value->odp_full;
        $odp_loss_reti += $value->odp_loss_reti;
        $insert_tiang += $value->insert_tiang;
        $onu_32 += $value->onu_32;
    @endphp
    <tr>
        <td class="align-middle">{{ $value->area }}</td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_alpro" target="_blank">{{ $value->kendala_alpro }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_jalur" target="_blank">{{ $value->kendala_jalur }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_jauh" target="_blank">{{ $value->odp_jauh }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=no_odp" target="_blank">{{ $value->no_odp }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_full" target="_blank">{{ $value->odp_full }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_loss_reti" target="_blank">{{ $value->odp_loss_reti }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=insert_tiang" target="_blank">{{ $value->insert_tiang }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=onu_32" target="_blank">{{ $value->onu_32 }}</a>
        </td>
    </tr>
    @endforeach
    <tr>
        <th class="align-middle">TOTAL</th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_alpro" target="_blank">{{ $kendala_alpro }}</a>
        </th>

        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_jalur" target="_blank">{{ $kendala_jalur }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_jauh" target="_blank">{{ $odp_jauh }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=no_odp" target="_blank">{{ $no_odp }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_full" target="_blank">{{ $odp_full }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=odp_loss_reti" target="_blank">{{ $odp_loss_reti }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=insert_tiang" target="_blank">{{ $insert_tiang }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=onu_32" target="_blank">{{ $onu_32 }}</a>
        </th>
    </tr>
</table>

<table class="table table-bordered dataTable">
    <tr>
        <th class="align-middle" rowspan="2">{{ strtoupper($view) }}</th>
        <th class="align-middle" colspan="9">KENDALA NON TEKNIS</th>
    </tr>
    <tr>
        <th class="align-middle">SEGMEN TDK SESUAI</th>
        <th class="align-middle">KENDALA IZIN HS</th>
        <th class="align-middle">DOUBLE INPUT</th>
        <th class="align-middle">FOLLOW UP HS</th>
        <th class="align-middle">ALAMAT TDK DITEMUKAN</th>
        <th class="align-middle">RUKOS</th>
        <th class="align-middle">PENDING H+</th>
        <th class="align-middle">PERMINTAAN BATAL</th>
    </tr>
    @php
        $segmen_tdk_sesuai = $kendala_izin_hs = $double_input = $follow_up_hs = $alamat_tdk_ditemukan = $rukos = $pending_hplus = $permintaan_batal = 0; 
    @endphp
    @foreach ($data2 as $key => $value)
    @php
        $segmen_tdk_sesuai += $value->segmen_tdk_sesuai;
        $kendala_izin_hs += $value->kendala_izin_hs;
        $double_input += $value->double_input;
        $follow_up_hs += $value->follow_up_hs;
        $alamat_tdk_ditemukan += $value->alamat_tdk_ditemukan;
        $rukos += $value->rukos;
        $pending_hplus += $value->pending_hplus;
        $permintaan_batal += $value->permintaan_batal;
    @endphp
    <tr>
        <td class="align-middle">{{ $value->area }}</td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=segmen_tdk_sesuai" target="_blank">{{ $value->segmen_tdk_sesuai }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_izin_hs" target="_blank">{{ $value->kendala_izin_hs }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=double_input" target="_blank">{{ $value->double_input }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=follow_up_hs" target="_blank">{{ $value->follow_up_hs }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=alamat_tdk_ditemukan" target="_blank">{{ $value->alamat_tdk_ditemukan }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=rukos" target="_blank">{{ $value->rukos }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=pending_hplus" target="_blank">{{ $value->pending_hplus }}</a>
        </td>
        <td class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area={{ $value->area }}&startDate={{ $startDate }}&endDate={{ $endDate }}&status=permintaan_batal" target="_blank">{{ $value->permintaan_batal }}</a>
        </td>
    </tr>
    @endforeach
    <tr>
        <th class="align-middle">TOTAL</th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=segmen_tdk_sesuai" target="_blank">{{ $segmen_tdk_sesuai }}</a>
        </th>

        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=kendala_izin_hs" target="_blank">{{ $kendala_izin_hs }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=double_input" target="_blank">{{ $double_input }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=follow_up_hs" target="_blank">{{ $follow_up_hs }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=alamat_tdk_ditemukan" target="_blank">{{ $alamat_tdk_ditemukan }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=rukos" target="_blank">{{ $rukos }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=pending_hplus" target="_blank">{{ $pending_hplus }}</a>
        </th>
        <th class="align-middle">
            <a href="/dashboard/newDashboardKendalaProvisioningDetail?view={{ $view }}&area=ALL&startDate={{ $startDate }}&endDate={{ $endDate }}&status=permintaan_batal" target="_blank">{{ $permintaan_batal }}</a>
        </th>
    </tr>
</table>

</div>

<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {

        $('.select2').select2();

        var day = {
                format: 'yyyy-mm-dd',
                viewMode: 0,
                minViewMode: 0
            };

        $('#startDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        $('#endDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });
    });
</script>
@endsection