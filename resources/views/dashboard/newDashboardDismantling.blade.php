@extends('layout')
@section('content')
<style>
    td {
        font-weight: 500;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .bg-blue {
        background-color: #4472c4;
        color: white !important;
    }
    .bg-orange {
        background-color: #c65911;
        color: white !important;
    }
    .bg-green {
        background-color: #548235;
        color: white !important;
    }
    .bg-grey {
        background-color: #808080;
        font-weight: bold;
        color: white !important;
    }
    .bg-greenlight {
        background-color: #1ABC9C;
        font-weight: bold;
        color: white !important;
    }
    .bg-red {
        background-color: #E74C3C;
        font-weight: bold;
        color: white !important;
    }
</style>
@include('partial.alerts')

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD NEW DISMANTLING<br />PERIODE {{ $start }} S/D {{ $end }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <div class="col-md-5">
                        <label for="date">START DATE</label>
                        <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="startDate" value="{{ $start ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label for="date">END DATE</label>
                        <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="endDate" value="{{ $end ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 table-responsive">
  <table class="table table-bordered dataTable">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">TIM</th>
                <th class="text-center align-middle bg-blue" colspan="4">SISA ORDER</th>
                <th class="text-center align-middle bg-green" colspan="5">COLLECTING</th>
                <th class="text-center align-middle bg-greenlight" colspan="3">RTWH</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-blue">CT0</th>
                <th class="text-center align-middle bg-blue">NEW LOSS</th>
                <th class="text-center align-middle bg-blue">MASSAL</th>
                <th class="text-center align-middle bg-blue">CAPS</th>
                <th class="text-center align-middle bg-green">CT0</th>
                <th class="text-center align-middle bg-green">NEW LOSS</th>
                <th class="text-center align-middle bg-green">MASSAL</th>
                <th class="text-center align-middle bg-green">CAPS</th>
                <th class="text-center align-middle bg-green">TOTAL</th>
                <th class="text-center align-middle bg-greenlight">SUDAH</th>
                <th class="text-center align-middle bg-greenlight">BELUM</th>
                <th class="text-center align-middle bg-greenlight">TOTAL<br />BERHASIL DIAMBIL</th>
            </tr>
      </thead>
      <tbody>
          @php
              $total_und_ct0 = $total_und_newloss = $total_und_massal = $total_und_caps = $total_collect_ct0 = $total_collect_newloss = $total_collect_massal = $total_collect_caps = $total_collect = $total_collect_all = $total_rtwh_berhasil = $total_rtwh_belum = $total_berhasil_diambil = $total_rtwh_all = 0;
          @endphp
          @foreach ($getData as $tim => $result)
          @php
              $total_und_ct0 += @$result['und_ct0'];
              $total_und_newloss += @$result['und_newloss'];
              $total_und_caps += @$result['und_caps'];
              $total_und_massal += @$result['und_massal'];
              $total_collect_ct0 += @$result['collect_ct0'];
              $total_collect_newloss += @$result['collect_newloss'];
              $total_collect_caps += @$result['collect_caps'];
              $total_collect_massal += @$result['collect_massal'];
              $total_collect = @$result['collect_ct0'] + @$result['collect_newloss'] + @$result['collect_caps'] + @$result['collect_massal'];
              $total_collect_all += $total_collect;
              $total_rtwh_berhasil += @$result['rtwh_berhasil'];
              $total_rtwh_belum += @$result['rtwh_belum'];
              $total_berhasil_diambil += @$result['berhasil_diambil'];
              $total_rtwh_all += (@$result['rtwh_berhasil'] + @$result['rtwh_belum']);
          @endphp
            <tr>
                <td class="text-center align-middle"><b>{{ $tim }}</b></td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=UND_CT0&type=SISA_ORDER">{{ @$result['und_ct0'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=UND_NEWLOSS&type=SISA_ORDER">{{ @$result['und_newloss'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=UND_CAPS&type=SISA_ORDER">{{ @$result['und_caps'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=UND_MASSAL&type=SISA_ORDER">{{ @$result['und_massal'] ? : 0 }}</a>
                </td>

                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_CT0&type=COLLECTING">{{ @$result['collect_ct0'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_NEWLOSS&type=COLLECTING">{{ @$result['collect_newloss'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_MASSAL&type=COLLECTING">{{ @$result['collect_massal'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_CAPS&type=COLLECTING">{{ @$result['collect_caps'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_ALL&type=COLLECTING">{{ $total_collect }}</a>
                </td>

                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_BERHASIL&type=COLLECTING">{{ @$result['rtwh_berhasil'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_BELUM&type=COLLECTING">{{ @$result['rtwh_belum'] ? : 0 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=BERHASIL_DIAMBIL&type=COLLECTING">{{ @$result['berhasil_diambil'] ? : 0 }}</a>
                </td>
                {{-- <td class="text-center align-middle">
                    <a class="black-link" href="/newDashboardDismantlingDetail?tim={{ $tim }}&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_ALL&type=COLLECTING">{{ (@$result['rtwh_berhasil'] + @$result['rtwh_belum']) ? : 0 }}</a>
                </td> --}}
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr>
                <td class="text-center align-middle bg-blue">TOTAL</td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=UND_CT0&type=SISA_ORDER">{{ $total_und_ct0 }}</a>
                </td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=UND_ NEWLOSS&type=SISA_ORDER">{{ $total_und_newloss }}</a>
                </td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=UND_ CAPS&type=SISA_ORDER">{{ $total_und_caps }}</a>
                </td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=UND_ MASSAL&type=SISA_ORDER">{{ $total_und_massal }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_CT0">{{ $total_collect_ct0 }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_NEWLOSS&type=COLLECTING">{{ $total_collect_newloss }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_MASSAL&type=COLLECTING">{{ $total_collect_massal }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_CAPS&type=COLLECTING">{{ $total_collect_caps }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=COLLECT_ALL&type=COLLECTING">{{ $total_collect_all }}</a>
                </td>
                <td class="text-center align-middle bg-greenlight">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_BERHASIL&type=COLLECTING">{{ $total_rtwh_berhasil }}</a>
                </td>
                <td class="text-center align-middle bg-greenlight">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_BELUM&type=COLLECTING">{{ $total_rtwh_belum }}</a>
                </td>
                <td class="text-center align-middle bg-greenlight">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=BERHASIL_DIAMBIL&type=COLLECTING">{{ $total_berhasil_diambil }}</a>
                </td>
                {{-- <td class="text-center align-middle bg-greenlight">
                    <a class="white-link" href="/newDashboardDismantlingDetail?tim=ALL&startDate={{ $start }}&endDate={{ $end }}&status=RTWH_ALL&type=COLLECTING">{{ $total_rtwh_all }}</a>
                </td> --}}
          </tr>
      </tfoot>
  </table>
</div>
<script>
    $(function() {
      $('.datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
    });
</script>
@endsection