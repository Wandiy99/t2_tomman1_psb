@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/AddonComparin?startDate={{ $start }}&endDate={{ $end }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL ADDON COMPARIN DATEL {{ $datel }} PERIODE {{ $start }} S/D {{ $end }} STATUS {{ strtoupper($status) }} {{ strtoupper($waktu) }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>UMUR</th>
                        <th>ORDER ID</th>
                        <th>ORDER NAME</th>
                        <th>ORDER DATE</th>
                        <th>ORDER STATUS</th>
                        <th>LAYANAN</th>
                        <th>K-CONTACT</th>
                        <th>STATUS TEKNISI</th>
                        <th>TANGGAL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area ? : 'NON AREA' }}</td>
                        <td>{{ $data->sektor ? : '#N/A' }}</td>
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        <td>{{ $data->umur_jam ? : '0' }} Jam</td>
                        <td>{{ $data->ORDER_ID ? : '#N/A' }}</td>
                        <td>{{ $data->CUSTOMER_NAME ? : '#N/A' }}</td>
                        <td>{{ $data->ORDER_DATE ? : '#N/A' }}</td>
                        <td>{{ $data->KODEFIKASI_SC ? : '#N/A' }}</td>
                        <td>{{ $data->JENIS_ADDON ? : '#N/A' }}</td>
                        <td>{{ $data->KCONTACT ? : '#N/A' }}</td>
                        <td>{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td>{{ $data->tgl_laporan ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DASHBOARD ADDON COMPARIN BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection