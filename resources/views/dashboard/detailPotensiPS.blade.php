@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL POTENSI PS {{ $group }} AREA {{ $area }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>STO</th>
                        <th>TIM</th>
                        <th>ORDER ID</th>
                        <th>ORDER NAME</th>
                        <th>KCONTACT</th>
                        <th>ORDER STATUS</th>
                        <th>WFM ID</th>
                        <th>TEKNISI STATUS</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area }}</td>
                        <td>{{ $data->sto }}</td>
                        <td>{{ $data->uraian }}</td>
                        <td>{{ $data->orderIdInteger }}</td>
                        <td>{{ $data->orderName }}</td>
                        <td>{{ $data->kcontact }}</td>
                        <td>{{ $data->orderStatus }}</td>
                        <td>{{ $data->wfm_id }}</td>
                        <td>{{ $data->laporan_status }}</td>
                        <td>{{ $data->orderDate }}</td>
                        <td>{{ $data->orderDatePs }}</td>
                        <td>{{ $data->modified_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL POTENSI PS ORDER TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL POTENSI PS ORDER TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL POTENSI PS ORDER TOMMAN'
                }
            ]
        });
    });
</script>
@endsection