@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL KENDALA SABER {{ $startDate }} {{ $endDate }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap align-middle" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>STATUS_TEKNISI</th>
                        <th>ORDER_ID</th>
                        <th>ORDER_NAME</th>
                        <th>ORDER_DATE</th>
                        <th>ORDER_STATUS</th>
                        <th>STO</th>
                        <th>NAMA_ODP</th>
                        <th>TGL_LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->sektor ? : 'NON AREA' }}</td>
                        <td>{{ $data->tim }}</td>
                        <td>{{ $data->mitra_amija_pt }}</td>
                        <td>{{ $data->status_teknisi }}</td>
                        <td>{{ $data->order_id }}</td>
                        <td>{{ $data->orderName }}</td>
                        <td>{{ $data->orderDate }}</td>
                        <td>{{ $data->orderStatus }}</td>
                        <td>{{ $data->sto }}</td>
                        <td>{{ $data->nama_odp }}</td>
                        <td>{{ $data->tgl_laporan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL KENDALA SABER BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection