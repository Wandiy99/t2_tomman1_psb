@extends($layout)

@section('content')
  @include('partial.alerts')
<style>
    th{
        vertical-align: middle;
        text-align: center;
    }
</style>
<div class="col-sm-16">
    <div class="panel panel-default">
        <div class="panel-heading">SEARCH ODP</div>
            <form method="GET">
                <input type="text" class="form-control" placeholder="Search ODP ..." name="odp" id="odp" />
            </form>
    </div>
</div><br />
  <div class="col-sm-16">
        <div class="panel panel-default">
            <div class="panel-heading">RESULT QC BORNEO</div>
            <div class="table-responsive">
                <table class="table table-bordered">
                <tr>
                    <th>NO</th>
                    <th>DETAIL</th>
                    <th>FOTO ODP</th>
                    <th>FOTO BERITA ACARA</th>
                    <th>FOTO LOKASI</th>
                </tr>
                @foreach ($data as $num => $result)

                @if($result)                    
                <tr>
                    <td style="text-align: center">{{ ++$num }}</td>
                    <td>
                        <b>SC</b> <a href="/{{ $result->id_dt }}" target="_blank">{{ $result->NO_ORDER ? : '-' }}</a><br />
                        <b>INET :</b> {{ $result->ndemSpeedy ? : '-' }}<br />
                        <b>VOICE :</b> {{ $result->ndemPots ? : '-' }}<br />
                        <b>CUSTOMER :</b> {{ $result->orderName ? :'-' }}<br />
                        <b>ODP TEKNISI :</b> {{ $result->nama_odp ? : '-' }}<br />
                        <b>KOR. ODP TEKNISI :</b> {{ $result->kordinat_odp ? : '-' }}<br />
                        <b>ODP STARCLICK :</b> {{ $result->alproname ? : '-' }}<br />
                        <b>KOR. PELANGGAN :</b> {{ $result->kordinat_pelanggan ? : '-' }}<br />
                        <b>DROPCORE LABEL :</b> {{ $result->dropcore_label_code ? : '-' }}<br />
                        <b>TGL PS :</b> {{ $result->orderDatePs ? :  '-' }}
                    </td>
                    @foreach($get_foto as $foto)
                    <?php
                    $path1 = "/upload/evidence/{$result->id_dt}";
                    $th1   = "$path1/$foto-th.jpg";
                    $path2 = "/upload2/evidence/{$result->id_dt}";
                    $th2   = "$path2/$foto-th.jpg";
                    $path3 = "/upload3/evidence/{$result->id_dt}";
                    $th3   = "$path3/$foto-th.jpg";
                    $path4 = "/upload4/evidence/{$result->id_dt}";
                    $th4   = "$path4/$foto-th.jpg";
                    $path  = null;
                    
                    if (file_exists(public_path().$th1)) {
                        $path = "$path1/$foto";
                    } elseif (file_exists(public_path().$th2)) {
                        $path = "$path2/$foto";
                    } elseif (file_exists(public_path().$th3)) {
                        $path = "$path3/$foto";
                    } elseif (file_exists(public_path().$th4)) {
                        $path = "$path4/$foto";
                    }

                    if ($path) {
                        $th    = "$path-th.jpg";
                        $img   = "$path.jpg";
                    } else {
                        $th    = null;
                        $img   = null;
                    }
                    ?>
                    <td>
                        @if(!empty($th) && file_exists(public_path().$th))
                        <center>
                            <a href="{{ $img }}"><img src="{{ $th }}"></a><br />
                            <br />
                        </center>
                        @else
                        <center>
                            <img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" /><br />
                        </center>
                        @endif
                    </td>
                    @endforeach
                </tr>
                    @endif
                @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection