@extends('layout')

@section('content')
@include('partial.alerts')
<?php
$data = json_decode($data);
?>
<style>
	td, th {
		padding:5px;
	}
</style>
<h3>Progress Tomman {{ $tgl }}</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">Data</div>
			<div class="panel-body table-responsive" style="padding: 0px !important" >
				<table width="100%" border=1>
					<tr>
						<th>#</th>
						<th>ORDER_ID</th>
						<th>ORDERNAME</th>
						<th>ALPRONAME</th>
						<th>STO</th>
						<th>ORDERADDRESS</th>
						<th>TGL_DISPATCH</th>
						<th>ID_REGU</th>
						<th>CREW_ID</th>
						<th>ALIAS</th>
						<th>ID_STATUS</th>
						<th>STATUS_TEKNISI</th>
						<th>TGL_STATUS</th>
						<th>CATATAN</th>
					</tr>
					@foreach ($data as $num => $result)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $result->ORDER_ID }}</td>
						<td>{{ $result->ORDERNAME }}</td>
						<td>{{ $result->ALPRONAME }}</td>
						<td>{{ $result->STO }}</td>
						<td>{{ $result->ORDERADDRESS }}</td>
						<td>{{ $result->TGL_DISPATCH }}</td>
						<td>{{ $result->ID_REGU }}</td>
						<td>{{ $result->CREW_ID }}</td>
						<td>{{ $result->ALIAS }}</td>
						<td>{{ $result->ID_STATUS }}</td>
						<td>{{ $result->STATUS_TEKNISI }}</td>
						<td>{{ $result->TGL_STATUS }}</td>
						<td>{{ $result->CATATAN }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>
@endsection