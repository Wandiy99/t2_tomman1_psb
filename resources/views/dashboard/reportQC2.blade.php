@extends('layout')
@section('content')
<style>
    td {
        font-weight: 500;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .bg-blue {
        background-color: #4472c4;
        color: white !important;
    }
    .bg-orange {
        background-color: #c65911;
        color: white !important;
    }
    .bg-green {
        background-color: #548235;
        color: white !important;
    }
    .bg-grey {
        background-color: #808080;
        font-weight: bold;
        color: white !important;
    }
    .bg-greenlight {
        background-color: #1ABC9C;
        font-weight: bold;
        color: white !important;
    }
    .bg-red {
        background-color: #E74C3C;
        font-weight: bold;
        color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
@include('partial.alerts')

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">REPORT UTQC2<br />PERIODE {{ $start }} S/D {{ $end }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <div class="col-md-3">
                        <label for="group">AREA</label>
                        <select class="form-control" data-placeholder="- Chooice a Area -" tabindex="1" name="area">
                            @if(session('auth')->nama_witel == 'KALSEL')
                                @if($area == 'SEKTOR')
                                    <option value="SEKTOR">SEKTOR</option>
                                    <option value="MITRA">MITRA</option>
                                    <option value="WITEL">WITEL</option>
                                @elseif ($area == 'MITRA')
                                    <option value="MITRA">MITRA</option>
                                    <option value="WITEL">WITEL</option>
                                    <option value="SEKTOR">SEKTOR</option>
                                @elseif ($area == 'WITEL')
                                    <option value="WITEL">WITEL</option>
                                    <option value="SEKTOR">SEKTOR</option>
                                    <option value="MITRA">MITRA</option>
                                @endif
                            @else
                                <option value="WITEL">WITEL</option>
                            @endif
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label for="group">SOURCE</label>
                        <select class="form-control" data-placeholder="- Chooice a Source -" tabindex="1" name="source">
                            @if($area == 'WITEL')
                                @if($source == 'QC2')
                                <option value="QC2">KPRO UTQC2</option>
                                <option value="UTONLINE">UT ONLINE</option>
                                @elseif ($source == 'UTONLINE')
                                <option value="UTONLINE">UT ONLINE</option>
                                <option value="QC2">KPRO UTQC2</option>
                                @endif
                            @else
                                @if($source == 'QC2')
                                <option value="QC2">KPRO UTQC2</option>
                                <option value="UTONLINE">UT ONLINE</option>
                                <option value="UTONLINEACTCOMP">UT ONLINE ACTCOMP</option>
                                @elseif ($source == 'UTONLINE')
                                <option value="UTONLINE">UT ONLINE</option>
                                <option value="UTONLINEACTCOMP">UT ONLINE ACTCOMP</option>
                                <option value="QC2">KPRO UTQC2</option>
                                @endif
                            @endif
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="date">START DATE</label>
                        <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="startDate" value="{{ $start ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="date">END DATE</label>
                        <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="endDate" value="{{ $end ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    {{-- <div class="col-md-2">
                        <label for="witel">WITEL</label>
                        <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" disabled>
                            <option value="BANJARMASIN">BANJARMASIN</option>
                        </select>
                    </div> --}}
                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 table-responsive">
<p style="float: left; font-weight: bold;">Report UTQC2 <i>(Last Update : {{ @$log->last_updated }} WITA)</i></p>
<p style="float: right; font-weight: bold;">UT Online <i>(Last Update : {{ @$log_ut->last_updated_at }} WITA)</i></p>
  <table class="table table-bordered dataTable">
      <thead>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="4">{{ $area }}</th>
                <th class="text-center align-middle bg-blue" rowspan="4">
                    @if ($source == 'UTONLINEACTCOMP')
                    Jml ACTCOMP
                    @else
                    Jml PS
                    @endif
                </th>
                <th class="text-center align-middle bg-blue" colspan="2">Create UTOnline</th>
                <th class="text-center align-middle bg-orange" colspan="5">UT Online</th>
                <th class="text-center align-middle bg-green" colspan="2">QC2</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-blue" rowspan="2">Failed</th>
                <th class="text-center align-middle bg-blue" rowspan="2">Success</th>
                <th class="text-center align-middle bg-orange" rowspan="2">Return to MyTech</th>
                <th class="text-center align-middle bg-orange" rowspan="2">Need Validate TL</th>
                <th class="text-center align-middle bg-orange" colspan="2">Return to TL</th>
                <th class="text-center align-middle bg-orange" rowspan="2">Need Approve ASO</th>
                <th class="text-center align-middle bg-green" rowspan="2">Need Validate</th>
                <th class="text-center align-middle bg-green" rowspan="2">Valid</th>
            </tr>
            <tr>
                <th class="text-center align-middle bg-orange">Reject ASO</th>
                <th class="text-center align-middle bg-orange">Reject QC2</th>
            </tr>
      </thead>
      <tbody>
          @php
              $total_ps = $total_create_failed = $total_create_success = $total_return_mytech = $total_need_validate = $total_not_approved = $total_notvalidqc2 = $total_need_approve = $total_need_validate_qc2 = $total_validate_qc2 = $total_closed = $jml_round = $total_round = 0;
          @endphp
          @foreach ($data as $result)
          @php
              $total_ps += $result->jml_ps;
              $total_create_failed += $result->create_failed;
              $total_create_success += $result->create_success;
              $total_return_mytech += $result->return_mytech;
              $total_need_validate += $result->need_validate;
              $total_not_approved += $result->not_approved;
              $total_notvalidqc2 += $result->not_valid_qc2;
              $total_need_approve += $result->need_approve;
              $total_need_validate_qc2 += $result->need_validate_qc2;
              $total_validate_qc2 += $result->validate_qc2;
              $total_closed += $result->closed;
          @endphp
          @if ($source == 'QC2')
            @php
                $jml_round = @round($result->validate_qc2 / $result->jml_ps * 100,2);
                $total_round = @round($total_validate_qc2 / $total_ps * 100,2);
            @endphp
          @else
            @php
                $jml_round = @round(($result->validate_qc2 + $result->closed) / $result->jml_ps * 100,2);
                $total_round = @round(($total_validate_qc2 + $total_closed) / $total_ps * 100,2);
            @endphp
          @endif
            <tr>
                <td class="text-center align-middle"><b>{{ $result->area ? : 'NON AREA' }}</b></td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=JML_PS">{{ $result->jml_ps }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=CREATE_FAILED">{{ $result->create_failed }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=CREATE_SUCCESS">{{ $result->create_success }} ({{ @round($result->create_success / $result->jml_ps * 100,2) }} %)</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=RETURN_MYTECH">{{ $result->return_mytech }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=NEED_VALIDATE">{{ $result->need_validate }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=NOT_APPROVED">{{ $result->not_approved }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=NOT_VALID_QC2">{{ $result->not_valid_qc2 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=NEED_APPROVE">{{ $result->need_approve }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=NEED_VALIDATE_QC2">{{ $result->need_validate_qc2 }}</a>
                </td>
                <td class="text-center align-middle">
                    <a class="black-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group={{ $result->area ? : 'NON AREA' }}&startDate={{ $start }}&endDate={{ $end }}&status=VALIDATE_QC2">{{ $result->validate_qc2 }} ({{ $jml_round }} %)</a>
                </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr>
                <td class="text-center align-middle bg-blue">TOTAL</td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=JML_PS">{{ $total_ps }}</a>
                </td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=CREATE_FAILED">{{ $total_create_failed }}</a>
                </td>
                <td class="text-center align-middle bg-blue">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=CREATE_SUCCESS">{{ $total_create_success }} ({{ @round($total_create_success / $total_ps * 100,2) }} %)</a>
                </td>
                <td class="text-center align-middle bg-orange">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=RETURN_MYTECH">{{ $total_return_mytech }}</a>
                </td>
                <td class="text-center align-middle bg-orange">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=NEED_VALIDATE">{{ $total_need_validate }}</a>
                </td>
                <td class="text-center align-middle bg-orange">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=NOT_APPROVED">{{ $total_not_approved }}</a>
                </td>
                <td class="text-center align-middle bg-orange">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=NOT_VALID_QC2">{{ $total_notvalidqc2 }}</a>
                </td>
                <td class="text-center align-middle bg-orange">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=NEED_APPROVE">{{ $total_need_approve }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=NEED_VALIDATE_QC2">{{ $total_need_validate_qc2 }}</a>
                </td>
                <td class="text-center align-middle bg-green">
                    <a class="white-link" href="/dashboard/kpro/reportQC2Detail?source={{ $source }}&area={{ $area }}&group=ALL&startDate={{ $start }}&endDate={{ $end }}&status=VALIDATE_QC2">{{ $total_validate_qc2 }} ({{ $total_round }} %)</a>
                </td>
          </tr>
      </tfoot>
  </table>
<p style="font-weight: bold;">Waktu sinkronisasi ada 2 tahap, yaitu :<br />(1) <i>Menit 10 - 25</i> <br /> (2) <i>Menit 40 - 55</i></p>
</div>
<script>
    $(function() {
        $('.datepicker-autoclose').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
    });
</script>
@endsection
