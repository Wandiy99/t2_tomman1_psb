@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <p class="text-muted">Last Update : {{ $log_sync->datetime }} WITA</p>
        <h3 class="box-title m-b-0" style="font-weight: bolder !important;color: black !important;"><center>REPORT QUALITY CONTROL TECHNICIAN BY DATE PS {{ $date }} S/D {{ $dateend }}</center></h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                        <th>MITRA</th>
                        <th>ORDER</th>
                        <th>TIDAK LOLOS</th>
                        <th>LOLOS</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $total_order = 0;
                    $total_tidak_lolos = 0;
                    $total_lolos = 0;
                @endphp
                @foreach ($query as $num => $result)
                    @php
                        $total_order += $result->jml_order;
                        $total_tidak_lolos += $result->tidak_lolos;
                        $total_lolos += $result->lolos;
                    @endphp
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->uraian ? : '#N/A' }}</td>
                    <td>{{ $result->title ? : '#N/A' }}</td>
                    <td>{{ $result->mitra ? : '#N/A' }}</td>
                    <td><a href="/dashboard/QCborneoTeknisiList/{{ $date }}/{{ $dateend }}/{{ $result->uraian ? : 'NA' }}/ORDER" target="_blank">{{ $result->jml_order }}</a></td>
                    <td><a href="/dashboard/QCborneoTeknisiList/{{ $date }}/{{ $dateend }}/{{ $result->uraian ? : 'NA' }}/NOK" target="_blank">{{ $result->tidak_lolos }}</a></td>
                    <td><a href="/dashboard/QCborneoTeknisiListDone/{{ $date }}/{{ $dateend }}/{{ $result->uraian ? : 'NA' }}" target="_blank">{{ $result->lolos }}</a></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=4 style="font-weight: bold">TOTAL</td>
                        <td><a href="/dashboard/QCborneoTeknisiList/{{ $date }}/{{ $dateend }}/ALL/ORDER" target="_blank">{{ $total_order }}</a></td>
                        <td><a href="/dashboard/QCborneoTeknisiList/{{ $date }}/{{ $dateend }}/ALL/NOK" target="_blank">{{ $total_tidak_lolos }}</a></td>
                        <td><a href="/dashboard/QCborneoTeknisiListDone/{{ $date }}/{{ $dateend }}/ALL" target="_blank">{{ $total_lolos }}</a></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
@endsection