@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL KENDALA ALPRO SEKTOR TANGGAL {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>ORDER_ID</th>
                        <th>JENIS_ORDER</th>
                        <th>NAMA</th>
                        <th>ORDER DATE</th>
                        <th>KOORDINAT PELANGGAN</th>
                        <th>ODP</th>
                        <th>KOORDINAT ODP</th>
                        <th>STATUS TEKNISI</th>
                        <th>TGL UPDATE STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->sektor ? : 'NON AREA' }}</td>
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        <td>{{ $data->order_id }}</td>
                        <td>{{ $data->jenis_order ? : '#N/A' }}</td>
                        <td>{{ $data->dps_nama ? : $data->pmw_nama ? : '#N/A' }}</td>
                        <td>{{ $data->dps_orderDate ? : $data->pmw_orderDate ? : '#N/A' }}</td>
                        <td>{{ $data->kordinat_pelanggan ? : '#N/A' }}</td>
                        <td>{{ $data->nama_odp ? : '#N/A' }}</td>
                        <td>{{ $data->kordinat_odp ? : '#N/A' }}</td>
                        <td>{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td>{{ $data->modified_at ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL KENDALA ALPRO BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection