@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #34495E;
      font-weight: bold;
      color : #FFF;
    }
    .yellow {
      background-color : #ffcb2b
    }
  </style>
  <script type="text/javascript" src="https://cdn3.devexpress.com/jslib/16.2.17/js/dx.all.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/16.2.17/css/dx.common.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/16.2.17/css/dx.light.css" />
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD ASSURANCE</h4><br/>
          <form method="GET">
            <div class="row">
              <div class="col-md-5">
                  <label for="witel">WITEL</label>
                  <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" name="witel">
                    @if ($witel == "KALSEL")
                        <option value="KALSEL">KALSEL</option>
                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                    @elseif ($witel == "BALIKPAPAN")
                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                        <option value="KALSEL">KALSEL</option>
                    @endif
                  </select>
              </div>
              <div class="col-md-5">
                <label for="date">TANGGAL</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd" name="date" value="{{ $date ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
              <div class="col-md-2">
                  <label for="search">&nbsp;</label>
                  <button class="btn btn-info btn-block" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    @if($witel == "KALSEL")
      <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">GRAFIK TIKET OPEN {{ date('Y-m', strtotime($date)) }}</h3>
            <ul class="list-inline text-right">
              <li>
                <h5><i class="fa fa-circle m-r-5" style="color: #48C9B0;"></i>TOMMAN &nbsp;</h5>
              </li>
              <li>
                <h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>REGULER &nbsp;</h5>
              </li>
              <li>
                  <h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>UNSPEC &nbsp;</h5>
              </li>
              <li>
                  <h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>SQM &nbsp;</h5>
              </li>
            </ul>
            <div id="morris-area-chart"></div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" id="reportasrbyumur">
          <div class="panel-heading">
            Tiket Open by IN {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="7">DURASI (Jam)</th>
                <th class="align-middle" colspan="2">SUM</th>
                <th class="align-middle" rowspan="2">TTL</th>
              </tr>
              <tr>
                <th class="green">0-2</th>
                <th class="green">2-4</th>
                <th class="green">4-6</th>
                <th class="red">6-8</th>
                <th class="red">8-10</th>
                <th class="red">10-12</th>
                <th class="redx">12-24</th>
                <th class="redx">>24</th>
                <th><24</th>
              </tr>
              @php
                $lebih24 = $kurang24 = $a0dan2 = $a2dan4 = $a4dan6 = $a6dan8 = $a8dan10 = $a10dan12 = $a12dan24 = $jumlah = 0;
              @endphp
              @foreach ($getRockbyUmur as $RockbyUmur)
              @php
                $lebih24 += $RockbyUmur->lebih24;
                $a0dan2 += $RockbyUmur->a0dan2;
                $a2dan4 += $RockbyUmur->a2dan4;
                $a4dan6 += $RockbyUmur->a4dan6;
                $a6dan8 += $RockbyUmur->a6dan8;
                $a8dan10 += $RockbyUmur->a8dan10;
                $a10dan12 += $RockbyUmur->a10dan12;
                $a12dan24 += $RockbyUmur->a12dan24;
                $kurang24 += $RockbyUmur->kurang24;
                $jumlah += $RockbyUmur->jumlah;
              @endphp
              <tr>
                <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a0dan2/ALL">{{ $RockbyUmur->a0dan2 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a2dan4/ALL">{{ $RockbyUmur->a2dan4 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a4dan6/ALL">{{ $RockbyUmur->a4dan6 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a6dan8/ALL">{{ $RockbyUmur->a6dan8 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a8dan10/ALL">{{ $RockbyUmur->a8dan10 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a10dan12/ALL">{{ $RockbyUmur->a10dan12 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a12dan24/ALL">{{ $RockbyUmur->a12dan24 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24/ALL">{{ $RockbyUmur->lebih24 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24/ALL">{{ $RockbyUmur->kurang24 }}</a></td>
                <td class="center"><a href="/listbyumur/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL/ALL">{{ $RockbyUmur->jumlah }}</a></td>
              </tr>
              @endforeach
              <tr>
                <td><b>TOTAL</b></td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a0dan2/ALL">{{ $a0dan2 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a2dan4/ALL">{{ $a2dan4 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a4dan6/ALL">{{ $a4dan6 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a6dan8/ALL">{{ $a6dan8 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a8dan10/ALL">{{ $a8dan10 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a10dan12/ALL">{{ $a10dan12 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/a12dan24/ALL">{{ $a12dan24 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/lebih24/ALL">{{ $lebih24 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/kurang24/ALL">{{ $kurang24 }}</td>
                <td class="center"><a href="/listbyumur/sektor/ALL/ALL/ALL">{{ $jumlah }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" id="reportasrbyumur">
          <div class="panel-heading">
            Tiket Open by INT {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="7">DURASI (Jam)</th>
                <th class="align-middle" colspan="2">SUM</th>
                <th class="align-middle" rowspan="2">JML</th>
              </tr>
              <tr>
                <th class="green">0-2</th>
                <th class="green">2-4</th>
                <th class="green">4-6</th>
                <th class="red">6-8</th>
                <th class="red">8-10</th>
                <th class="red">10-12</th>
                <th class="redx">12-24</th>
                <th class="redx">>24</th>
                <th><24</th>
              </tr>
              @php
                $lebih24 = $kurang24 = $a0dan2 = $a2dan4 = $a4dan6 = $a6dan8 = $a8dan10 = $a10dan12 = $a12dan24 = $jumlah = 0;
              @endphp
              @foreach ($getTiket_OpenINT as $RockbyUmur)
              @php
                $lebih24 += $RockbyUmur->lebih24;
                $a0dan2 += $RockbyUmur->x0dan2;
                $a2dan4 += $RockbyUmur->x2dan4;
                $a4dan6 += $RockbyUmur->x4dan6;
                $a6dan8 += $RockbyUmur->x6dan8;
                $a8dan10 += $RockbyUmur->x8dan10;
                $a10dan12 += $RockbyUmur->x10dan12;
                $a12dan24 += $RockbyUmur->x12dan24;
                $kurang24 += $RockbyUmur->kurang24;
                $jumlah += $RockbyUmur->jumlah;
              @endphp
              <tr>
                <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x0dan2">{{ $RockbyUmur->x0dan2 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x2dan4">{{ $RockbyUmur->x2dan4 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x4dan6">{{ $RockbyUmur->x4dan6 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x6dan8">{{ $RockbyUmur->x6dan8 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x8dan10">{{ $RockbyUmur->x8dan10 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x10dan12">{{ $RockbyUmur->x10dan12 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x12dan24">{{ $RockbyUmur->x12dan24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24">{{ $RockbyUmur->lebih24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24">{{ $RockbyUmur->kurang24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL">{{ $RockbyUmur->jumlah }}</a></td>
              </tr>
              @endforeach
              <tr>
                <td><b>TOTAL</b></td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x0dan2">{{ $a0dan2 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x2dan4">{{ $a2dan4 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x4dan6">{{ $a4dan6 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x6dan8">{{ $a6dan8 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x8dan10">{{ $a8dan10 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x10dan12">{{ $a10dan12 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/x10dan12">{{ $a12dan24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/lebih12">{{ $lebih24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/kurang12">{{ $kurang24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/sektor/ALL/ALL">{{ $jumlah }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" id="reportasrbyumur">
          <div class="panel-heading">
            HERO : Tiket Open by IN {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="7">DURASI (Jam)</th>
                <th class="align-middle" colspan="2">SUM</th>
                <th class="align-middle" rowspan="2">TTL</th>
              </tr>
              <tr>
                <th class="green">0-2</th>
                <th class="green">2-4</th>
                <th class="green">4-6</th>
                <th class="red">6-8</th>
                <th class="red">8-10</th>
                <th class="red">10-12</th>
                <th class="redx">12-24</th>
                <th class="redx">>24</th>
                <th><24</th>
              </tr>
              @php
                $lebih24 = $kurang24 = $a0dan2 = $a2dan4 = $a4dan6 = $a6dan8 = $a8dan10 = $a10dan12 = $a12dan24 = $jumlah = 0;
              @endphp
              @foreach ($getOpen_Hero_IN as $RockbyUmur)
              @php
                $lebih24 += $RockbyUmur->lebih24;
                $a0dan2 += $RockbyUmur->a0dan2;
                $a2dan4 += $RockbyUmur->a2dan4;
                $a4dan6 += $RockbyUmur->a4dan6;
                $a6dan8 += $RockbyUmur->a6dan8;
                $a8dan10 += $RockbyUmur->a8dan10;
                $a10dan12 += $RockbyUmur->a10dan12;
                $a12dan24 += $RockbyUmur->a12dan24;
                $kurang24 += $RockbyUmur->kurang24;
                $jumlah += $RockbyUmur->jumlah;
              @endphp
              <tr>
                <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a0dan2/ALL">{{ $RockbyUmur->a0dan2 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a2dan4/ALL">{{ $RockbyUmur->a2dan4 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a4dan6/ALL">{{ $RockbyUmur->a4dan6 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a6dan8/ALL">{{ $RockbyUmur->a6dan8 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a8dan10/ALL">{{ $RockbyUmur->a8dan10 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a10dan12/ALL">{{ $RockbyUmur->a10dan12 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/a12dan24/ALL">{{ $RockbyUmur->a12dan24 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24/ALL">{{ $RockbyUmur->lebih24 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24/ALL">{{ $RockbyUmur->kurang24 }}</a></td>
                <td class="center"><a href="/listbyumur/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL/ALL">{{ $RockbyUmur->jumlah }}</a></td>
              </tr>
              @endforeach
              <tr>
                <td><b>TOTAL</b></td>
                <td class="center"><a href="/listbyumur/hero/ALL/a0dan2/ALL">{{ $a0dan2 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a2dan4/ALL">{{ $a2dan4 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a4dan6/ALL">{{ $a4dan6 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a6dan8/ALL">{{ $a6dan8 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a8dan10/ALL">{{ $a8dan10 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a10dan12/ALL">{{ $a10dan12 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/a12dan24/ALL">{{ $a12dan24 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/lebih24/ALL">{{ $lebih24 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/kurang24/ALL">{{ $kurang24 }}</td>
                <td class="center"><a href="/listbyumur/hero/ALL/ALL/ALL">{{ $jumlah }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" id="reportasrbyumur">
          <div class="panel-heading">
            HERO : Tiket Open by INT {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="7">DURASI (Jam)</th>
                <th class="align-middle" colspan="2">SUM</th>
                <th class="align-middle" rowspan="2">JML</th>
              </tr>
              <tr>
                <th class="green">0-2</th>
                <th class="green">2-4</th>
                <th class="green">4-6</th>
                <th class="red">6-8</th>
                <th class="red">8-10</th>
                <th class="red">10-12</th>
                <th class="redx">12-24</th>
                <th class="redx">>24</th>
                <th><24</th>
              </tr>
              @php
                $lebih24 = $kurang24 = $a0dan2 = $a2dan4 = $a4dan6 = $a6dan8 = $a8dan10 = $a10dan12 = $a12dan24 = $jumlah = 0;
              @endphp
              @foreach ($getOpen_Hero_INT as $RockbyUmur)
              @php
                $lebih24 += $RockbyUmur->lebih24;
                $a0dan2 += $RockbyUmur->x0dan2;
                $a2dan4 += $RockbyUmur->x2dan4;
                $a4dan6 += $RockbyUmur->x4dan6;
                $a6dan8 += $RockbyUmur->x6dan8;
                $a8dan10 += $RockbyUmur->x8dan10;
                $a10dan12 += $RockbyUmur->x10dan12;
                $a12dan24 += $RockbyUmur->x12dan24;
                $kurang24 += $RockbyUmur->kurang24;
                $jumlah += $RockbyUmur->jumlah;
              @endphp
              <tr>
                <td>{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x0dan2">{{ $RockbyUmur->x0dan2 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x2dan4">{{ $RockbyUmur->x2dan4 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x4dan6">{{ $RockbyUmur->x4dan6 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x6dan8">{{ $RockbyUmur->x6dan8 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x8dan10">{{ $RockbyUmur->x8dan10 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x10dan12">{{ $RockbyUmur->x10dan12 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/x12dan24">{{ $RockbyUmur->x12dan24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/lebih24">{{ $RockbyUmur->lebih24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/kurang24">{{ $RockbyUmur->kurang24 }}</a></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/{{ $RockbyUmur->sektor_asr ? : 'UNDEFINED' }}/ALL">{{ $RockbyUmur->jumlah }}</a></td>
              </tr>
              @endforeach
              <tr>
                <td><b>TOTAL</b></td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x0dan2">{{ $a0dan2 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x2dan4">{{ $a2dan4 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x4dan6">{{ $a4dan6 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x6dan8">{{ $a6dan8 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x8dan10">{{ $a8dan10 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x10dan12">{{ $a10dan12 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/x10dan12">{{ $a12dan24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/lebih12">{{ $lebih24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/kurang12">{{ $kurang24 }}</td>
                <td class="center"><a href="/ticketing/dashboardList/hero/ALL/ALL">{{ $jumlah }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" id="reportpotensiasr">
          <div class="panel-heading">
            TIKET Progress IN
          </div>
          <div class="table-responsive">
            <table class="table" >
              <tr>
                <th>SEKTOR</th>
                <th>OPEN</th>
                <th>NP</th>
                <th>OGP</th>
                <th>KP</th>
                <th>KT</th>
                <th>CLOSING</th>
                <th class="red">SISA SALDO</th>
              </tr>
              <?php
                $TOTAL = 0;
                $NP = 0;
                $OGP = 0;
                $KT = 0;
                $KP = 0;
                $UP = 0;
              ?>
              @foreach ($getTiket2 as $number => $Tiket2)
              <?php
                $TOTAL += $Tiket2->jumlah;
                $NP += $Tiket2->NP;
                $OGP += $Tiket2->OGP;
                $KP += $Tiket2->KP;
                $KT += $Tiket2->KT;
                $UP += $Tiket2->UP;
              ?>
              <tr>
                <td>{{ $Tiket2->title ? : 'UNDISPATCH' }}</td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/open">{{ $Tiket2->jumlah }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/np">{{ $Tiket2->NP }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/ogp">{{ $Tiket2->OGP }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kp">{{ $Tiket2->KP }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kt">{{ $Tiket2->KT }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/up">{{ $Tiket2->UP }}</a></td>
                <td class="center"><a href="/assurance/list-active-rock/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/sisa">{{ $Tiket2->jumlah-$Tiket2->UP }}</a></td>
              </tr>
              <?php ++$number; ?>
              @endforeach
              <tr>
                <td class="center"><b>TOTAL</b></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/open"><b>{{ $TOTAL }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/np"><b>{{ $NP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/ogp"><b>{{ $OGP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/kp"><b>{{ $KP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/kt"><b>{{ $KT }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-rock/ALL/up"><b>{{ $UP }}</b></a></td>
                <td class="center red"><a href="/assurance/list-active-rock/ALL/sisa"><b>{{ $TOTAL-$UP }}</b></a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            TIKET Progress INT
          </div>
          <div class="table-responsive">
            <table class="table" >
              <tr>
                <th>SEKTOR</th>
                <th>OPEN</th>
                <th>NP</th>
                <th>OGP</th>
                <th>KP</th>
                <th>KT</th>
                <th>CLOSING</th>
                <th class="red">SISA SALDO</th>
              </tr>
              <?php
                $TOTAL = 0;
                $NP = 0;
                $OGP = 0;
                $KT = 0;
                $KP = 0;
                $UP = 0;
                $SISA = 0;
              ?>
              @foreach ($getTiket_INT as $number => $Tiket2)
              <?php
                $TOTAL += $Tiket2->jumlah;
                $NP += $Tiket2->NP;
                $OGP += $Tiket2->OGP;
                $KP += $Tiket2->KP;
                $KT += $Tiket2->KT;
                $UP += $Tiket2->UP;
                $SISA += ($Tiket2->jumlah - $Tiket2->UP);
              ?>
              <tr>
                <td>{{ $Tiket2->title ? : 'UNDISPATCH' }}</td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/open/{{ $date }}">{{ $Tiket2->jumlah }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/np/{{ $date }}">{{ $Tiket2->NP }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/ogp/{{ $date }}">{{ $Tiket2->OGP }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kp/{{ $date }}">{{ $Tiket2->KP }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/kt/{{ $date }}">{{ $Tiket2->KT }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/up/{{ $date }}">{{ $Tiket2->UP }}</a></td>
                <td class="center"><a href="/assurance/list-active-int/{{ $Tiket2->chat_id ?: 'UNDISPATCH' }}/sisa/{{ $date }}">{{ $Tiket2->jumlah - $Tiket2->UP }}</a></td>
              </tr>
              <?php ++$number; ?>
              @endforeach
              <tr>
                <td class="center"><b>TOTAL</b></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/open/{{ $date }}"><b>{{ $TOTAL }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/np/{{ $date }}"><b>{{ $NP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/ogp/{{ $date }}"><b>{{ $OGP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/kp/{{ $date }}"><b>{{ $KP }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/kt/{{ $date }}"><b>{{ $KT }}</b></a></td>
                <td class="center"><a href="/assurance/list-active-int/ALL/up/{{ $date }}"><b>{{ $UP }}</b></a></td>
                <td class="center red"><a href="/assurance/list-active-int/ALL/sisa/{{ $date }}"><b>{{ $SISA }}</b></a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            RATA-RATA CLOSED TIKET REGULER
          </div>
            <div class="table-responsive">
              <table class="table">
                <tr style="background-color:red">
                  <th class="align-middle" rowspan="2">SEKTOR</th>
                  <th class="align-middle" rowspan="2">TL</th>
                  <th class="align-middle" colspan="2">REGULER</th>
                  <th class="align-middle" rowspan="2" class="redx">PROD</th>
                </tr>
                <tr>
                  <th class="yellow">ORDER</th>
                  <th class="green">CLOSE</th>
                </tr>
                <?php
                  $total_order_IN = 0;
                  $total_close_IN = 0;
                ?>
                @foreach ($getAverageClose as $AverageClose)
                <?php
                  $total_order_IN += $AverageClose->order_IN;
                  $total_close_IN += $AverageClose->close_IN;
                  $prod = (@round(@$AverageClose->close_IN / @$AverageClose->order_IN * 100,2));
                ?>
                <tr align="center">
                  <td>{{ $AverageClose->sektor }}</td>
                  <td>{{ $AverageClose->TL }}</td>
                  <td><a href="/dashboard/assurance/averageCloseDetail/{{ $date }}/{{ $AverageClose->sektor }}/ORDER/IN">{{ $AverageClose->order_IN }}</a></td>
                  <td><a href="/dashboard/assurance/averageCloseDetail/{{ $date }}/{{ $AverageClose->sektor }}/CLOSE/IN">{{ $AverageClose->close_IN }}</a></td>
                  <td>{{ $prod }} %</td>
                </tr>
                @endforeach
                <tr style="background-color:#fbd12c;">
                  <td colspan="2" align="center"><b>TOTAL</b></td>
                  <td class="center"><a href="/dashboard/assurance/averageCloseDetail/{{ $date }}/ALL/ORDER/IN">{{ $total_order_IN }}</a></td>
                  <td class="center"><a href="/dashboard/assurance/averageCloseDetail/{{ $date }}/ALL/CLOSE/IN">{{ $total_close_IN }}</a></td>
                  <td class="center">%</td>
                </tr>
              </table>
            </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            SISA SALDO TIKET {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table table-sm">
              <tr>
                <th class="align-middle" rowspan="2">IOAN</th>
                <th class="align-middle" style="background-color: #2ECC71" rowspan="2">WNOK</th>
                <th class="align-middle" colspan="7">REGULER</th>
                <th class="align-middle" style="background-color: #2ECC71" rowspan="2">WNOK</th>
                <th class="align-middle" colspan="7">HVC</th>
                <th class="align-middle" rowspan="2">TOTAL</th>
              </tr>
              <tr>
                <th style="background-color: #58D68D">< 1 JAM</th>
                <th style="background-color: #58D68D">< 2 JAM</th>
                <th style="background-color: #58D68D">< 3 JAM</th>
                <th style="background-color: #F4D03F">< 12 JAM</th>
                <th style="background-color: #F5B041">> 12 JAM</th>
                <th style="background-color: #EC7063">> 1 HARI</th>
                <th style="background-color: #EC7063">GAMAS</th>
                <th style="background-color: #58D68D">< 1 JAM</th>
                <th style="background-color: #58D68D">< 2 JAM</th>
                <th style="background-color: #58D68D">< 3 JAM</th>
                <th style="background-color: #F4D03F">< 12 JAM</th>
                <th style="background-color: #F5B041">> 12 JAM</th>
                <th style="background-color: #EC7063">> 1 HARI</th>
                <th style="background-color: #EC7063">GAMAS</th>
              </tr>
              @php
                $jumlah_reg = $min1jam_reg = $min2jam_reg = $min3jam_reg = $min12jam_reg = $max12jam_reg = $max1hari_reg = $gamas_reg = $wnok_reg = $jumlah_hvc = $min1jam_hvc = $min2jam_hvc = $min3jam_hvc = $min12jam_hvc = $max12jam_hvc = $max1hari_hvc = $gamas_hvc = $wnok_hvc = $total = 0;
              @endphp
              @foreach ($getSisaTiket as $sisaTiket)
              @php
                $wnok_reg += $sisaTiket->WNOK_REGULER;
                $jumlah_reg = ($sisaTiket->DIBAWAH1_REGULER + $sisaTiket->DIBAWAH2_REGULER + $sisaTiket->DIBAWAH3_REGULER + $sisaTiket->DIBAWAH12_REGULER + $sisaTiket->DIATAS12_REGULER + $sisaTiket->LEBIH1HARI_REGULER + $sisaTiket->GAMAS_REGULER); 
                $min1jam_reg += $sisaTiket->DIBAWAH1_REGULER;
                $min2jam_reg += $sisaTiket->DIBAWAH2_REGULER;
                $min3jam_reg += $sisaTiket->DIBAWAH3_REGULER;
                $min12jam_reg += $sisaTiket->DIBAWAH12_REGULER;
                $max12jam_reg += $sisaTiket->DIATAS12_REGULER;
                $max1hari_reg += $sisaTiket->LEBIH1HARI_REGULER;
                $gamas_reg += $sisaTiket->GAMAS_REGULER;

                $wnok_hvc += $sisaTiket->WNOK_HVC;
                $jumlah_hvc = ($sisaTiket->DIBAWAH1_HVC + $sisaTiket->DIBAWAH2_HVC + $sisaTiket->DIBAWAH3_HVC + $sisaTiket->DIBAWAH12_HVC + $sisaTiket->DIATAS12_HVC + $sisaTiket->LEBIH1HARI_HVC + $sisaTiket->GAMAS_HVC); 
                $min1jam_hvc += $sisaTiket->DIBAWAH1_HVC;
                $min2jam_hvc += $sisaTiket->DIBAWAH2_HVC;
                $min3jam_hvc += $sisaTiket->DIBAWAH3_HVC;
                $min12jam_hvc += $sisaTiket->DIBAWAH12_HVC;
                $max12jam_hvc += $sisaTiket->DIATAS12_HVC;
                $max1hari_hvc += $sisaTiket->LEBIH1HARI_HVC;
                $gamas_hvc += $sisaTiket->GAMAS_HVC;

                $total += $jumlah_reg + $jumlah_hvc;
              @endphp
              <tr>
                <td class="center">{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}</td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/WNOK_REGULER">{{ $sisaTiket->WNOK_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH1_REGULER">{{ $sisaTiket->DIBAWAH1_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH2_REGULER">{{ $sisaTiket->DIBAWAH2_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH3_REGULER">{{ $sisaTiket->DIBAWAH3_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH12_REGULER">{{ $sisaTiket->DIBAWAH12_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIATAS12_REGULER">{{ $sisaTiket->DIATAS12_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/LEBIH1HARI_REGULER">{{ $sisaTiket->LEBIH1HARI_REGULER }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/GAMAS_REGULER">{{ $sisaTiket->GAMAS_REGULER }}</a></td>

                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/WNOK_HVC">{{ $sisaTiket->WNOK_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH1_HVC">{{ $sisaTiket->DIBAWAH1_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH2_HVC">{{ $sisaTiket->DIBAWAH2_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH3_HVC">{{ $sisaTiket->DIBAWAH3_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIBAWAH12_HVC">{{ $sisaTiket->DIBAWAH12_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/DIATAS12_HVC">{{ $sisaTiket->DIATAS12_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/LEBIH1HARI_HVC">{{ $sisaTiket->LEBIH1HARI_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/GAMAS_HVC">{{ $sisaTiket->GAMAS_HVC }}</a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/{{ $sisaTiket->ioan ? : 'UNDISPATCH' }}/ALL">{{ $jumlah_reg + $jumlah_hvc }}</a></td>
              </tr>
              @endforeach
              <tr style="background-color:#fbd12c;">
                <td class="center"><b>TOTAL</b></td>
                <td class="center"><a><b>{{ $wnok_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH1_REGULER"><b>{{ $min1jam_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH2_REGULER"><b>{{ $min2jam_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH3_REGULER"><b>{{ $min3jam_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH12_REGULER"><b>{{ $min12jam_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIATAS12_REGULER"><b>{{ $max12jam_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/LEBIH1HARI_REGULER"><b>{{ $max1hari_reg }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/GAMAS_REGULER"><b>{{ $gamas_reg }}</b></a></td>

                <td class="center"><a><b>{{ $wnok_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH1_HVC"><b>{{ $min1jam_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH2_HVC"><b>{{ $min2jam_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH3_HVC"><b>{{ $min3jam_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIBAWAH12_HVC"><b>{{ $min12jam_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/DIATAS12_HVC"><b>{{ $max12jam_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/LEBIH1HARI_HVC"><b>{{ $max1hari_hvc }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/GAMAS_HVC"><b>{{ $gamas_hvc }}</b></a></td>

                <td class="center"><a href="/dashboard/assurance/sisaSaldoTiket/ALL/ALL"><b>{{ $total }}</b></a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            TTR MANUAL MANJA DISPATCH {{ date('Y-m-d') }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="4">DURASI</th>
                <th class="align-middle" rowspan="2">TOTAL</th>
              </tr>
              <tr>
                <th style="background-color: #58D68D">< 3 JAM</th>
                <th style="background-color: #F4D03F">< 12 JAM</th>
                <th style="background-color: #F5B041">> 12 JAM</th>
                <th style="background-color: #EC7063">> 1 HARI</th>
              </tr>
              @php
                $jumlah = 0;
                $min3jam = 0;
                $min12jam = 0;
                $max12jam = 0;
                $max1hari = 0;
                $total = 0;
              @endphp
              @foreach ($getManjaManual as $manjaManual)
              @php
                $jumlah = ($manjaManual->DIBAWAH3 + $manjaManual->DIBAWAH12 + $manjaManual->DIATAS12 + $manjaManual->LEBIH1HARI); 
                $min3jam += $manjaManual->DIBAWAH3;
                $min12jam += $manjaManual->DIBAWAH12;
                $max12jam += $manjaManual->DIATAS12;
                $max1hari += $manjaManual->LEBIH1HARI;
                $total += $jumlah;
              @endphp
              <tr>
                <td class="center">{{ $manjaManual->sektor ? : 'UNDISPATCH' }}</td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/{{ $manjaManual->sektor ? : 'UNDISPATCH' }}/DIBAWAH3">{{ $manjaManual->DIBAWAH3 }}</a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/{{ $manjaManual->sektor ? : 'UNDISPATCH' }}/DIBAWAH12">{{ $manjaManual->DIBAWAH12 }}</a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/{{ $manjaManual->sektor ? : 'UNDISPATCH' }}/DIATAS12">{{ $manjaManual->DIATAS12 }}</a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/{{ $manjaManual->sektor ? : 'UNDISPATCH' }}/LEBIH1HARI">{{ $manjaManual->LEBIH1HARI }}</a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/{{ $manjaManual->sektor ? : 'UNDISPATCH' }}/ALL">{{ $jumlah }}</a></td>
              </tr>
              @endforeach
              <tr style="background-color:#fbd12c;">
                <td class="center"><b>TOTAL</b></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/ALL/DIBAWAH3"><b>{{ $min3jam }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/ALL/DIBAWAH12"><b>{{ $min12jam }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/ALL/DIATAS12"><b>{{ $max12jam }}</b></a></td>
                <td class="center"><a href="/dashboard/assurance/ttrManualManja/ALL/LEBIH1HARI"><b>{{ $max1hari }}</b></a></td>
                <td class="center"><b><a href="/dashboard/assurance/ttrManualManja/ALL/ALL">{{ $total }}</b></a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            ASSURANCE GAUL {{ $date }}
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>IOAN</th>
                <th>SEKTOR</th>
                <th style="background-color: #EC7063 !important">GAUL</th>
              </tr>
              <?php
                $TOTAL = 0;
              ?>
              @foreach ($getGaul as $number => $Gaul)
              <?php
                $TOTAL += $Gaul->jumlah;
              ?>
              <tr>
                <td class="center">{{ $Gaul->ioan ? : 'UNDISPATCH' }}</td>
                <td class="center">{{ $Gaul->sektor ? : 'UNDISPATCH' }}</td>
                <td class="center"><a href="/dashboard/assurance/gaul/{{ $Gaul->sektor ? : 'UNDISPATCH' }}/{{ $tgl }}">{{ $Gaul->jumlah }}</a></td>
              </tr>
              <?php ++$number; ?>
              @endforeach
              <tr style="background-color:#fbd12c;">
                <td colspan="2" class="center"><b>TOTAL</b></td>
                <td class="center"><a href="/dashboard/assurance/gaul/ALL/{{ $tgl }}"><b>{{ $TOTAL }}</b></a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            MONITORING SALDO UNDERSPEC FTTH SEMESTA HARIAN
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr style="background-color:red">
                <th class="align-middle" rowspan="2">SEKTOR</th>
                <th class="align-middle" colspan="2">ORDER</th>
                <th class="align-middle" rowspan="2">TOTAL</th>
              </tr>
              <tr>
                <th class="yellow">UNDISPATCH</th>
                <th class="green">DISPATCH</th>
              </tr>
              <?php
                $total_undispatch = 0;
                $total_dispatch = 0;
                $total_und_all = 0;
                $total_disp_all = 0;
                $total_all = 0;
              ?>
              @foreach ($getSaldoUnspec as $SaldoUnspec)
              <?php
                $total_undispatch += $SaldoUnspec->undispatch;
                $total_und_all = $total_undispatch;
                $total_dispatch += $SaldoUnspec->dispatch;
                $total_disp_all = $total_dispatch;
                $total_all = $total_und_all + $total_disp_all;
              ?>
              <tr align="center">
                <td>{{ $SaldoUnspec->SEKTOR }}</td>
                <td><a href="/dashboard/assurance/saldoUnspec/{{ $SaldoUnspec->SEKTOR }}/UNDISPATCH/{{ $date }}">{{ $SaldoUnspec->undispatch }}</a></td>
                <td><a href="/dashboard/assurance/saldoUnspec/{{ $SaldoUnspec->SEKTOR }}/DISPATCH/{{ $date }}">{{ $SaldoUnspec->dispatch }}</a></td>
                <td><a href="/dashboard/assurance/saldoUnspec/{{ $SaldoUnspec->SEKTOR }}/ALL/{{ $date }}">{{ $SaldoUnspec->jumlah }}</a></td>
              </tr>
              @endforeach
              <tr style="background-color:#fbd12c;color:black" align="center">
                <td align="center">TOTAL</td>
                <td class="center"><a href="/dashboard/assurance/saldoUnspec/ALL/UNDISPATCH/{{ $date }}">{{ $total_und_all }}</a></td>
                <td class="center"><a href="/dashboard/assurance/saldoUnspec/ALL/DISPATCH/{{ $date }}">{{ $total_disp_all }}</a></td>
                <td class="center"><a href="/dashboard/assurance/saldoUnspec/ALL/ALL/{{ $date }}">{{ $total_all }}</a></td>
              </tr>
              </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Daily Closing Order {{ date('Y-m-d', strtotime('-1 days')) }}
          </div>
          <div class="table-responsive">
            <table class="table text-center">
                <thead>
                    <tr>
                        <th class="align-middle" rowspan="2">SEKTOR</th>
                        <th class="align-middle" colspan="6">HASIL UKUR CLOSE</th>
                        <th class="align-middle" rowspan="2">GRAND TOTAL</th>
                    </tr>
                    <tr>
                        <th class="align-middle green">SPEC</th>
                        <th class="align-middle red">UNSPEC</th>
                        <th class="align-middle red">DYING<br />GASP</th>
                        <th class="align-middle redx">LOS</th>
                        <th class="align-middle gray">OFFLINE</th>
                        <th class="align-middle">#N/A</th>
                    </tr>
                </thead>
                @php
                    $jml_spec = $jml_unspec = $jml_dyinggasp = $jml_los = $jml_offline = $jml_na = $total = 0;
                @endphp
                @foreach ($get_dailyreport as $value)
                @php
                    $jml_spec += $value->jml_spec;
                    $jml_unspec += $value->jml_unspec;
                    $jml_dyinggasp += $value->jml_dyinggasp;
                    $jml_los += $value->jml_los;
                    $jml_offline += $value->jml_offline;
                    $jml_na += $value->jml_na;
                    $total += $value->total;
                @endphp
                <tbody>
                    <tr>
                        <td>{{ $value->sektor ? : 'NON SEKTOR' }}</td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=SPEC">{{ $value->jml_spec }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=UNSPEC">{{ $value->jml_unspec }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=DYINGGASP">{{ $value->jml_dyinggasp }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=LOS">{{ $value->jml_los }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=OFFLINE">{{ $value->jml_offline }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=NA">{{ $value->jml_na }}</a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor={{ $value->sektor ? : 'NON SEKTOR' }}&status=ALL">{{ $value->total }}</a></td>
                    </tr>
                @endforeach
                    <tr>
                        <td><b>TOTAL</b></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=SPEC"><b>{{ $jml_spec }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=UNSPEC"><b>{{ $jml_unspec }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=DYINGGASP"><b>{{ $jml_dyinggasp }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=LOS"><b>{{ $jml_los }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=OFFLINE"><b>{{ $jml_offline }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=NA"><b>{{ $jml_na }}</b></a></td>
                        <td><a href="/assurance/reportDailyClosingOrderDetail?sektor=ALL&status=ALL"><b>{{ $total }}</b></a></td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Action Today Table
          </div>
          <div class="table-responsive">
            <table class="table dataTable">
              <tr>
                <th class="align-middle" rowspan="2">Action</th>
                <!-- <th rowspan="2">Jumlah</th>
                <th rowspan="2">INT</th> -->
                <th class="align-middle" rowspan="2">IN</th>
                <th class="align-middle" colspan="{{ count($get_sektor) }}">IN</th>
                <th class="align-middle" rowspan="2">INT</th>
                <th class="align-middle" colspan="{{ count($get_sektor) }}">INT</th>

              </tr>
              <tr>
                @foreach ($get_sektor as $sektor)
                <th>{{ $sektor->ioan }}</th>
                @endforeach

                @foreach ($get_sektor as $sektor)
                <th>{{ $sektor->ioan }}</th>
                @endforeach
              </tr>
              @php
                $total = 0;
                $totalIN = 0;
                $totalINT = 0;
              @endphp
              @foreach ($getAction as $Action)
              <tr>
                <td>{{ $Action->action }}</td>
                <!-- <td><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah }}</a></td> -->
                <td class="center"><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/ALL">{{ $Action->jumlah_IN }}</a></td>
                @foreach ($get_sektor as $sektor)
                @php
                  $sektorx = $sektor->ioan;
                @endphp
                <td class="center"><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/NOSSA/{{ $sektorx }}">{{ $Action->$sektorx }}</a></td>
                @endforeach
                <td class="center"><a href="/listbyaction/{{ $date }}/{{ $Action->action }}">{{ $Action->jumlah_INT }}</a></td>
                @foreach ($get_sektor as $sektor)
                @php
                  $sektorx="INT_".$sektor->ioan;
                @endphp
                <td class="center"><a href="/listbyaction/{{ $date }}/{{ $Action->action }}/TOMMAN/{{ $sektorx }}">{{ $Action->$sektorx }}</a></td>
                @endforeach
              </tr>
              @php
                $total = $total + $Action->jumlah;
                $totalINT = $totalINT + $Action->jumlah_INT;
                $totalIN = $totalIN + $Action->jumlah_IN;
              @endphp
              @endforeach
              <tr>
                <td>Total</td>
                <td class="center"><a href="/listbyaction/{{ $date }}/ALL/NOSSA/ALL">{{ $totalIN }}</a></td>
                @foreach ($get_sektor as $sektor)
                @php $sektorx = $sektor->ioan @endphp
                <td class="center"><a href="/listbyaction/{{ $date }}/ALL/NOSSA/{{ $sektor->ioan }}">{{ $sektor->jumlah_IN }}</a></td>
                @endforeach
                <td class="center"><a href="/listbyaction/{{ $date }}/ALL/TOMMAN/ALL">{{ $totalINT }}</a></td>
                @foreach ($get_sektor as $sektor)
                @php $sektorx = $sektor->ioan @endphp
                <td class="center"><a href="/listbyaction/{{ $date }}/ALL/TOMMAN/{{ $sektor->ioan }}">{{ $sektor->jumlah_INT }}</a></td>
                @endforeach
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            Status Table
          </div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Status</th>
                <th>Jumlah</th>
              </tr>
              @php
                $jumlah_status = 0;
              @endphp
              @foreach ($getStatus as $Status)
              <tr>
                <td>{{ $Status->laporan_status ? : 'ANTRIAN' }}</td>
                <td class="center"><a href="/dashboard/assurance/list/{{ $date }}/{{ $Status->laporan_status ? : 'ANTRIAN' }}">{{ $Status->jumlah }}</a></td>
              </tr>
              @php
                $jumlah_status = $jumlah_status + $Status->jumlah;
              @endphp
              @endforeach
              <tr>
                <td>Total</td>
                <td class="center"><a href="/dashboard/assurance/list/{{ $date }}/ALL">{{ $jumlah_status }}</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-4").dxPieChart({
              dataSource: [
                @foreach ($getStatus as $Status)
                {reporting : "{{ $Status->laporan_status ? : 'ANTRIAN' }}", val: {{ $Status->jumlah }}},
                @endforeach
              ],
              AutoLayout : false,
              title: "Reporting Status Today ",
              tooltip: {
                  enabled: true,
                customizeText: function() {
                  return this.argumentText + "<br/>" + this.valueText;
                }
              },
              size: {
                height: 370
              },
              legend: {
                visible: true
              },
              series: [{
                label: {
                    visible: true,
                    connector: {
                        visible: true
                    }
                },
                type: "doughnut",
                argumentField: "reporting"
              }]
            });
          });
        </script>
        <div id="bar-4" style="height: 290px; width: 100%;"></div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <script type="text/javascript">
              jQuery(document).ready(function($)
              {
                $("#bar-11").dxChart({
                  title: 'Trend Open Gangguan Witel Kalsel',
                  label : {
                    visible : true
                  },
                  argumentAxis: {
                      valueMarginsEnabled: false,
                      discreteAxisDivisionMode: "crossLabels",
                      grid: {
                          visible: true
                      }
                  },
                  tooltip: {
                      enabled: true,
                      customizeTooltip: function (arg) {
                          return {
                              text: arg.valueText
                          };
                      }
                  },
                  dataSource: [
                    @foreach ($getTiket as $Tiket)
                    { day : "{{ $Tiket->day }}", Jumlah : {{ $Tiket->jumlah }}},
                    @endforeach
                    @foreach ($getTiket as $Tiket)
                    { day : "{{ $Tiket->day }}", Jumlah_Close : {{ $Tiket->jumlah_open }}},
                    @endforeach
                  ],
                  series: [
                  {
                    argumentField : "day",
                    valueField: "Jumlah",
                    name : "Jumlah_Tiket",
                    type : "area",
                    color: '#2ecc71'
                  },
                  {
                    argumentField : "day",
                    valueField: "Jumlah_Close",
                    name : "Jumlah_Close_byTek",
                    type : "area",
                    color: '#ff0000'
                  },
                  ]
                });
              });
              </script>
              <div id="bar-11" style="height: 290px; width: 100%;"></div>
          </div>
        </div>
      </div>
    @elseif($witel == "BALIKPAPAN")
      <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">GRAFIK TIKET OPEN {{ date('Y-m', strtotime($date)) }}</h3>
            <ul class="list-inline text-right">
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #3498DB;"></i>REGULER &nbsp;</h5>
                </li>
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #F1C40F;"></i>UNSPEC &nbsp;</h5>
                </li>
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>SQM &nbsp;</h5>
                </li>
            </ul>
            <div id="morris-area-chart"></div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            MONITORING ORDER ASSURANCE PERIODE {{ $date }}
          </div>
          <div class="table-responsive">
            <table class="table table-sm">
              <tr>
                <th class="align-middle" rowspan="2">WORKZONE</th>
                <th class="align-middle" colspan="6">CUSTOMER SEGMENT</th>
                <th class="align-middle" colspan="9">SISA SALDO TIKET (DURASI)</th>
                <th class="align-middle" colspan="2">REGULER</th>
                <th class="align-middle" colspan="2">UNSPEC</th>
                <th class="align-middle" colspan="2">SQM</th>
              </tr>
              <tr>
                <th class="align-middle">DCS</th>
                <th class="align-middle">DBS</th>
                <th class="align-middle">DES</th>
                <th class="align-middle">DGS</th>
                <th class="align-middle">DSO</th>
                <th class="align-middle">JUMLAH</th>
                <th class="align-middle" style="background-color: #2ECC71">WNOK</th>
                <th class="align-middle" style="background-color: #58D68D">< 1 JAM</th>
                <th class="align-middle" style="background-color: #58D68D">< 2 JAM</th>
                <th class="align-middle" style="background-color: #58D68D">< 3 JAM</th>
                <th class="align-middle" style="background-color: #F4D03F">< 12 JAM</th>
                <th class="align-middle" style="background-color: #F5B041">> 12 JAM</th>
                <th class="align-middle" style="background-color: #EC7063">> 1 HARI</th>
                <th class="align-middle" style="background-color: #EC7063 !important">GAMAS</th>
                <th class="align-middle">JUMLAH</th>
                <th class="align-middle">OPEN</th>
                <th class="align-middle">CLOSED</th>
                <th class="align-middle">OPEN</th>
                <th class="align-middle">CLOSED</th>
                <th class="align-middle">OPEN</th>
                <th class="align-middle">CLOSED</th>
              </tr>
              @php
                $tot_dcs = $tot_dbs = $tot_des = $tot_dgs = $tot_dso = $jumlah_cs = $total_cs = $wnok = $min1jam = $min2jam = $min3jam = $min12jam = $max12jam = $max1hari = $gamas = $total_saldo = $tot_o_rightnow = $tot_c_rightnow = $tot_o_unspec = $tot_c_unspec = $tot_o_sqm = $tot_c_sqm = 0;
              @endphp
              @foreach ($getData as $num => $result)
              @php
                $tot_dcs += @$result['jml_dcs'];
                $tot_dbs += @$result['jml_dbs'];
                $tot_des += @$result['jml_des'];
                $tot_dgs += @$result['jml_dgs'];
                $tot_dso += @$result['jml_dso'];
                $jumlah_cs = (@$result['jml_dcs'] + @$result['jml_dbs'] + @$result['jml_des'] + @$result['jml_dgs'] + @$result['jml_dso']);
                $total_cs += $jumlah_cs;
                $wnok += @$result['WNOK'];
                $min1jam += @$result['DIBAWAH1'];
                $min2jam += @$result['DIBAWAH2'];
                $min3jam += @$result['DIBAWAH3'];
                $min12jam += @$result['DIBAWAH12'];
                $max12jam += @$result['DIATAS12'];
                $max1hari += @$result['LEBIH1HARI'];
                $gamas += @$result['GAMAS'];
                $jumlah_saldo = (@$result['DIBAWAH1'] + @$result['DIBAWAH2'] + @$result['DIBAWAH3'] + @$result['DIBAWAH12'] + @$result['DIATAS12'] + @$result['LEBIH1HARI'] + @$result['GAMAS']);
                $total_saldo += $jumlah_saldo;
                $tot_o_rightnow += @$result['order_RIGHTNOW'];
                $tot_c_rightnow += @$result['close_RIGHTNOW'];
                $tot_o_unspec += @$result['order_UNSPEC'];
                $tot_c_unspec += @$result['close_UNSPEC'];
                $tot_o_sqm += @$result['order_SQM'];
                $tot_c_sqm += @$result['close_SQM'];
              @endphp
              <tr>
                <td class="center">{{ $num }}</td>
                <td class="center">{{ @$result['jml_dcs'] ? : '0' }}</td>
                <td class="center">{{ @$result['jml_dbs'] ? : '0' }}</td>
                <td class="center">{{ @$result['jml_des'] ? : '0' }}</td>
                <td class="center">{{ @$result['jml_dgs'] ? : '0' }}</td>
                <td class="center">{{ @$result['jml_dso'] ? : '0' }}</td>
                <td class="center">{{ $jumlah_cs ? : '0' }}</td>
                <td class="center">{{ @$result['WNOK'] ? : '0' }}</td>
                <td class="center">{{ @$result['DIBAWAH1'] ? : '0' }}</td>
                <td class="center">{{ @$result['DIBAWAH2'] ? : '0' }}</td>
                <td class="center">{{ @$result['DIBAWAH3'] ? : '0' }}</td>
                <td class="center">{{ @$result['DIBAWAH12'] ? : '0' }}</td>
                <td class="center">{{ @$result['DIATAS12'] ? : '0' }}</td>
                <td class="center">{{ @$result['LEBIH1HARI'] ? : '0' }}</td>
                <td class="center">{{ @$result['GAMAS'] ? : '0' }}</td>
                <td class="center">{{ $jumlah_saldo ? : '0' }}</td>
                <td class="center">{{ @$result['order_RIGHTNOW'] ? : '0' }}</td>
                <td class="center">{{ @$result['close_RIGHTNOW'] ? : '0' }}</td>
                <td class="center">{{ @$result['order_UNSPEC'] ? : '0' }}</td>
                <td class="center">{{ @$result['close_UNSPEC'] ? : '0' }}</td>
                <td class="center">{{ @$result['order_SQM'] ? : '0' }}</td>
                <td class="center">{{ @$result['close_SQM'] ? : '0' }}</td>
              </tr>
              @endforeach
              <tr style="background-color:#fbd12c;">
                <td class="center"><b>TOTAL</b></td>
                <td class="center"><b>{{ $tot_dcs ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_dbs ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_des ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_dgs ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_dso ? : '0' }}</b></td>
                <td class="center"><b>{{ $total_cs ? : '0' }}</b></td>
                <td class="center"><b>{{ $wnok ? : '0' }}</b></td>
                <td class="center"><b>{{ $min1jam ? : '0' }}</b></td>
                <td class="center"><b>{{ $min2jam ? : '0' }}</b></td>
                <td class="center"><b>{{ $min3jam ? : '0' }}</b></td>
                <td class="center"><b>{{ $min12jam ? : '0' }}</b></td>
                <td class="center"><b>{{ $max12jam ? : '0' }}</b></td>
                <td class="center"><b>{{ $max1hari ? : '0' }}</b></td>
                <td class="center"><b>{{ $gamas ? : '0' }}</b></td>
                <td class="center"><b>{{ $total_saldo ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_o_rightnow ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_c_rightnow ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_o_unspec ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_c_unspec ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_o_sqm ? : '0' }}</b></td>
                <td class="center"><b>{{ $tot_c_sqm ? : '0' }}</b></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    @endif
  </div>
  @if($witel == "KALSEL")
  <script>
    $(function() {
      Morris.Area({
        element: 'morris-area-chart',
        data: <?= json_encode($allOpenTiket) ?>,
        xkey: 'tgl',
        ykeys: ['jml_open_tomman','jml_reguler','jml_unspec','jml_sqm'],
        labels: ['TOMMAN ','REGULER ','UNSPEC ','SQM '],
        xLabelFormat: function (x) { return x.getUTCDate()+1; },
        pointSize: 4,
        fillOpacity: 0,
        pointStrokeColors:['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
        behaveLikeLine: true,
        gridLineColo8r: '#e0e0e0',
        lineWidth: 2,
        hideHover: 'auto',
        lineColors: ['#48C9B0','#3498DB','#F1C40F','#E74C3C'],
        resize: true
      });
    });
  </script>
  @elseif($witel == "BALIKPAPAN")
  <script>
    $(function() {
      Morris.Area({
        element: 'morris-area-chart',
        data: <?= json_encode($allOpenTiket) ?>,
        xkey: 'tgl',
        ykeys: ['jml_reguler','jml_unspec','jml_sqm'],
        labels: ['REGULER ','UNSPEC ','SQM '],
        xLabelFormat: function (x) { return x.getUTCDate()+1; },
        pointSize: 4,
        fillOpacity: 0,
        pointStrokeColors:['#3498DB','#F1C40F','#E74C3C'],
        behaveLikeLine: true,
        gridLineColo8r: '#e0e0e0',
        lineWidth: 2,
        hideHover: 'auto',
        lineColors: ['#3498DB','#F1C40F','#E74C3C'],
        resize: true
      });
    });
  </script>
  @endif
  <script>
    $(function() {
      $('#datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
    });
  </script>
@endsection