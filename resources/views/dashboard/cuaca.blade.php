@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>CUACA {{ $provinsi }}</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">
  				{{ $tgl }}
  			</div>
  			<div class="panel-body" style="padding:0px !important">
  				<table class="table">
  					<tr>
  						<th>Kota</th>
  						<th>Pagi</th>
  						<th>Siang</th>
  						<th>Malam</th>
  						<th>Dini Hari</th>
  					</tr>
  					@foreach ($getCuaca as $cuaca)
  					<tr>
  						<td>{{ $cuaca->kota }}</td>
  						<td><i class="{{ $cuaca->icon_jam0 }}"></i>&nbsp;	{{ $cuaca->status_jam0 }}</td>
  						<td><i class="{{ $cuaca->icon_jam6 }}"></i>&nbsp;	{{ $cuaca->status_jam6 }}</td>
  						<td><i class="{{ $cuaca->icon_jam12 }}"></i>&nbsp;	{{ $cuaca->status_jam12 }}</td>
  						<td><i class="{{ $cuaca->icon_jam16 }}"></i>&nbsp;	{{ $cuaca->status_jam16 }}</td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection
