@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">REKAP INVENTORY & ASSET AREA {{ $area }} {{ $material }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>ID ORDER</th>
                        <th>CUSTOMER</th>
                        <th>STATUS LAPORAN</th>
                        <th>CATATAN (TEKNISI)</th>
                        <th>KOORDINAT PELANGGAN (TEKNISI)</th>
                        <th>NAMA ITEM</th>
                        <th>QTY</th>
                        <th>NO RFC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>SEKTOR</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->so_inv_active ? : '#N/A' }}</td>
                        <td>{{ $data->Ndem }}</td>
                        <td>{{ $data->orderName ? : $data->customer ? : $data->Customer_Name ? : '#N/A' }}</td>
                        <td>{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td>{{ $data->catatan ? : '#N/A' }}</td>
                        <td>{{ $data->kordinat_pelanggan ? : '#N/A' }}</td>
                        <td>{{ $data->id_item ? : '#N/A' }}</td>
                        <td>{{ $data->qty ? : '0' }}</td>
                        <td>{{ $data->rfc_number ? : '#N/A' }}</td>
                        <td>{{ $data->uraian ? : '#N/A' }}</td>
                        <td>{{ $data->mitra_amija_pt ? : '#N/A' }}</td>
                        <td>{{ $data->title ? : '#N/A' }}</td>
                        <td>{{ $data->modified_at ? : '#N/A' }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>ID ORDER</th>
                        <th>CUSTOMER</th>
                        <th>STATUS LAPORAN</th>
                        <th>CATATAN (TEKNISI)</th>
                        <th>KOORDINAT PELANGGAN (TEKNISI)</th>
                        <th>NAMA ITEM</th>
                        <th>QTY</th>
                        <th>NO RFC</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>SEKTOR</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'REKAP MATERIAL INVENTORY & ASSET AREA TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'REKAP MATERIAL INVENTORY & ASSET AREA TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'REKAP MATERIAL INVENTORY & ASSET AREA TOMMAN'
                }
            ]
        });
    });
</script>
@endsection