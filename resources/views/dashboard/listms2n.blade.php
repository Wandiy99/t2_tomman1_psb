@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <a href="/dashboard/rekon/{{ $tgl }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $jenis }} {{ $so }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th rowspan="2">NO</th>
          <th colspan="8">DATA PELANGGAN</th>
          <th colspan="3">INFRASTRUCTURE MIGRATION COPPER TO FIBER (KU)</th>
          <th colspan="3">INFRASTRUCTURE & SERVICE MIGRATION COPPER TO FIBER (KU)</th>
          <th colspan="3">SERVICE MIGRATION (FO to FO)</th>
          <th colspan="3">NEW SALES</th>
          <th rowspan="2">PATCH CORD 2 METER</th>
          <th rowspan="2">ADD. PATCH CORD 1 METER</th>
          <th rowspan="2">KABEL UTP RJ45 2 METER</th>
          <th rowspan="2">KABEL UTP CAT 6 PER 1 METER</th>
          <th rowspan="2">DUCT KABEL PVC 20mm PER 1 METER</th>
          <th rowspan="2">INSTALASI STB KEDUA</th>
          <th colspan="2">TIANG</th>
          <th colspan="2">PEMAKAIAN DROPCORE</th>
          <th rowspan="2">S/N</th>
          <th rowspan="2">KETERANGAN</th>
          <th rowspan="2">SCID</th>
          <th rowspan="2">KCONTACT</th>
          <th rowspan="2">JENISPSB</th>
        </tr>
        <tr>

          <th>ND</th>
          <th>ND Speedy</th>
          <th>MDF</th>
          <th>STATUS INDIHOME</th>
          <th>TGL PS</th>
          <th>SUMBER DATA</th>
          <th>NAMA</th>
          <th>ALAMAT</th>
          <th>1P-1P</th>
          <th>2P-2P</th>
          <th>3P-3P</th>

          <th>1P-2P</th>
          <th>1P-3P</th>
          <th>2P-3P</th>

          <th>1P-2P</th>
          <th>1P-3P</th>
          <th>2P-3P</th>

          <th>0P-1P</th>
          <th>0P-2P</th>
          <th>0P-3P</th>

          <th>T-7</th>
          <th>T-9</th>
          <th>REAL</th>
          <th>KELEBIHAN</th>
        </tr>
        @foreach($query as $num => $data)
        <?php
          $dc = $data->dc_preconn + $data->dc_roll;
          if ($dc>150){
            $dc = $dc;
            $dc_add = $dc-150;
          } else {
            $dc = $dc;
            $dc_add = 0;
          }
        ?>
        <tr>
          <td>{{ ++$num }}.</td>

          <td>{{ $data->ND }}</td>
          <td>{{ $data->ND_Speedy }}</td>
          <td>{{ $data->MDF }}</td>
          <td>{{ $data->Status_Indihome }}</td>
          <td>{{ $data->Tgl_PS }}</td>
          <td>MS2N</td>
          <td>{{ $data->Nama }}</td>
          <td>{{ $data->orderAddr }}</td>
          <td>
          <?php
            if ($data->Status_Indihome=="1P-1P"){
              $status_1p_1p = 1;
            } else {
              $status_1p_1p = "";
            }
          ?>
          {{ $status_1p_1p }}
          </td>
          <td>
            <?php
              if ($data->Status_Indihome=="2P-2P"){
                $status_2p_2p = 1;
              } else {
                $status_2p_2p = "";
              }
            ?>
            {{ $status_2p_2p }}
          </td>
          <td>
            <?php
              if ($data->Status_Indihome=="3P-3P"){
                $status_3p_3p = 1;
              } else {
                $status_3p_3p = "";
              }
            ?>
            {{ $status_3p_3p }}
          </td>
          <td></td>
          <td>
            <?php
              if (($data->Status_Indihome=="MIGRASI_1P_3P_BUNDLED" || $data->Status_Indihome=="MIGRASI_1P_3P_UNBUNDLED") && $dc>0){
                $status_1p_3p = 1;
              } else {
                $status_1p_3p = "";
              }
            ?>
            {{ $status_1p_3p }}
          </td>
          <td>
            <?php
              if (($data->Status_Indihome=="MIGRASI_2P_3P_BUNDLED" || $data->Status_Indihome=="MIGRASI_2P_3P_UNBUNDLED") && $dc>0){
                $status_2p_3p = 1;
              } else {
                $status_2p_3p = "";
              }
            ?>
            {{ $status_2p_3p }}
          </td>
          <td></td>
          <td>
            <?php
              if (($data->Status_Indihome=="MIGRASI_1P_3P_BUNDLED" || $data->Status_Indihome=="MIGRASI_1P_3P_UNBUNDLED") && $dc==0){
                $status_1p_3p = 1;
              } else {
                $status_1p_3p = "";
              }
            ?>
            {{ $status_1p_3p }}
          </td>
          <td>
            <?php
              if (($data->Status_Indihome=="MIGRASI_2P_3P_BUNDLED" || $data->Status_Indihome=="MIGRASI_2P_3P_UNBUNDLED") && $dc==0){
                $status_2p_3p = 1;
              } else {
                $status_2p_3p = "";
              }
            ?>
            {{ $status_2p_3p }}
          </td>
          <td>
          <?php
            if ($data->patchcore>2) {
              $patchcore2 = 2;
              $patchcore1 = $data->patchcore-$patchcore2;
            } else {
              $patchcore2 = $data->patchcore;
              $patchcore1 = 0;
            }
            echo $patchcore2;
          ?>
          </td>
          <td>{{ $patchcore1 }}</td>
          <td>{{ $data->utp }}</td>
          <td>0</td>
          <td>0</td>
          <td>
          <?php
            if ($data->Status_Indihome=="2nd STB") {
              $stb2nd = 1;
            } else {
              $stb2nd = 0;
            }

          ?>
          {{ $stb2nd }}
          </td>
          <td>{{ $data->tiang_7 }}</td>
          <td>{{ $data->tiang_9 }}</td>
          <td>

          {{ $dc }}
          </td>
          <td>{{ $dc_add }}</td>
          <td>{{ $data->snont }}</td>
          <td>-</td>
          <td>{{ $data->orderId }}</td>
          <td>{{ $data->Kcontact }}</td>
          <td>{{ $data->jenisPsb }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
  @endsection
