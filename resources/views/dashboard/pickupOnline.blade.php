@extends('layout')
@section('content')
<style>
    tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    .black-grey {
        background-color: #34495E;
        color: white !important;
    }
    .red-lose {
        background-color: #E74C3C;
        color: white !important;
    }
    .green-win {
        background-color: #1ABC9C;
        color: white !important;
    }
</style>
  @include('partial.alerts')

<div class="row">
<div class="col-sm-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD<br />TACTICAL PROVISIONING</h4><br/>
        <div class="row">
            <div class="col-md-5">
                <form method="GET">
                    <label for="source">SOURCE</label>
                    <select class="form-control" data-placeholder="- Pilih Sumber Data -" tabindex="1" name="source">
                        @if($witel <> "BALIKPAPAN")
                            @if ($source == "kpro")
                                <option value="kpro">PROVI K-PRO</option>
                                <option value="starclick">STARCLICK NCX</option>
                            @elseif ($source == "starclick")
                                <option value="starclick">STARCLICK NCX</option>
                                <option value="kpro">PROVI K-PRO</option>
                            @endif
                        @else
                                <option value="kpro">PROVI K-PRO</option>
                        @endif
                    </select>
                </div>
                <div class="col-md-5">
                    <label for="witel">WITEL</label>
                    <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" name="witel">
                    @if ($witel == "KALSEL")
                        <option value="KALSEL">KALSEL</option>
                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                    @elseif ($witel == "BALIKPAPAN")
                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                        <option value="KALSEL">KALSEL</option>
                    @endif
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="search">&nbsp;</label>
                    <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="col-sm-12 table-responsive">
<p class="text-muted" style="float: left;">PROVI K-PRO <i>{{ @$log_kpro->last_grab }} WITA</i></p>
<p class="text-muted" style="float: right;">Tactical Provisioning <i>{{ @$log_tactical->lastsync_at }} WITA</i></p>
<br />
@if ($source == 'kpro')
    <table class="table table-bordered dataTable">
        <tr>
            <th class="align-middle" rowspan="2">WITEL</th>
            <th class="align-middle" colspan="{{ date('t') }}">ACH % ({{ strtoupper(date('F Y')) }})</th>
        </tr>
        <tr>
                @for ($i = 1; $i < date('t') + 1; $i ++)
                    @if ($i < 10)
                        @php
                            $keys = '0'.$i;
                        @endphp
                    @else
                        @php
                            $keys = $i;
                        @endphp
                    @endif
                <th class="align-middle">{{ $keys }}</th>
                @endfor
        </tr>
        @php
            $ps_kpro = $ps_pickup = $round = 0;
            $txt_ps_kpro = $txt_ps_pickup = '';
        @endphp
        @foreach ($report_witel as $result)
        <tr>
            <td class="align-middle">{{ $result->area ? : 'NON AREA' }}</td>
            @for ($i = 1; $i < date('t') + 1; $i ++)
                    @if ($i < 10)
                        @php
                            $keys = '0'.$i;
                        @endphp
                    @else
                        @php
                            $keys = $i;
                        @endphp
                    @endif

                    @php
                        $txt_ps_kpro = 'ps_kpro_d'.$keys;
                        $ps_kpro = $result->$txt_ps_kpro;

                        $txt_ps_pickup = 'ps_pickup_d'.$keys;
                        $ps_pickup = $result->$txt_ps_pickup;

                        $round = @round($ps_pickup / $ps_kpro * 100,2);
                    @endphp

                    @if (is_nan($round) == true)
                        @php
                            $roundx = '';
                            $bg = 'black-grey';
                        @endphp
                    @elseif ($round == 100)
                        @php
                            $roundx = $round;
                            $bg = 'green-win';
                        @endphp
                    @else
                        @php
                            $roundx = $round;
                            $bg = 'red-lose';
                        @endphp
                    @endif

                    <td class="align-middle {{ $bg }}">{{ $roundx }} %</td>
            @endfor
        </tr>
        @endforeach
    </table>

    <table class="table table-bordered dataTable">
        <tr>
            <th class="align-middle" rowspan="2">SEKTOR</th>
            <th class="align-middle" colspan="{{ date('t') }}">ACH % ({{ strtoupper(date('F Y')) }})</th>
        </tr>
        <tr>
                @for ($i = 1; $i < date('t') + 1; $i ++)
                    @if ($i < 10)
                        @php
                            $keys = '0'.$i;
                        @endphp
                    @else
                        @php
                            $keys = $i;
                        @endphp
                    @endif
                <th class="align-middle">{{ $keys }}</th>
                @endfor
        </tr>
        @php
            $ps_kpro = $ps_pickup = $round = 0;
            $txt_ps_kpro = $txt_ps_pickup = '';
        @endphp
        @foreach ($report_sektor as $result)
        <tr>
            <td class="align-middle">{{ $result->area ? : 'NON AREA' }}</td>
            @for ($i = 1; $i < date('t') + 1; $i ++)
                    @if ($i < 10)
                        @php
                            $keys = '0'.$i;
                        @endphp
                    @else
                        @php
                            $keys = $i;
                        @endphp
                    @endif

                    @php
                        $txt_ps_kpro = 'ps_kpro_d'.$keys;
                        $ps_kpro = $result->$txt_ps_kpro;

                        $txt_ps_pickup = 'ps_pickup_d'.$keys;
                        $ps_pickup = $result->$txt_ps_pickup;

                        $round = @round($ps_pickup / $ps_kpro * 100,2);
                    @endphp

                    @if (is_nan($round) == true)
                        @php
                            $roundx = '';
                            $bg = 'black-grey';
                        @endphp
                    @elseif ($round == 100)
                        @php
                            $roundx = $round;
                            $bg = 'green-win';
                        @endphp
                    @else
                        @php
                            $roundx = $round;
                            $bg = 'red-lose';
                        @endphp
                    @endif

                    <td class="align-middle {{ $bg }}">{{ $roundx }} %</td>
            @endfor
        </tr>
        @endforeach
    </table>
@endif
    <table class="table table-bordered dataTable">
        <tr>
            <th class="align-middle" rowspan="3">AREA</th>
            <th class="align-middle" colspan="2">{{ strtoupper($source) }}</th>
            <th class="align-middle" colspan="15">PICKUP ONLINE TIMESHEET TEKNISI ( {{ strtoupper(date('d F Y')) }} )</th>
            <th class="align-middle" rowspan="3">ACH %</th>
        </tr>
        <tr>
            <th class="align-middle" rowspan="2">PS</th>
            <th class="align-middle" rowspan="2">POTENSI<br />PS</th>
            <th class="align-middle" rowspan="2">PS<br />KPRO</th>
            <th class="align-middle" rowspan="2">PS PO<br />H-1</th>
            <th class="align-middle" rowspan="2">PS PO<br />HI</th>
            <th class="align-middle" colspan="5">UNCOMPLETED</th>
            <th class="align-middle" colspan="5">COMPLETED</th>
            <th class="align-middle" rowspan="2">REVOKE</th>
            <th class="align-middle" rowspan="2">#N/A</th>
        </tr>
        <tr>
            @for ($i = 0; $i < 2; $i ++)
            <th class="align-middle">OPEN</th>
            <th class="align-middle">ASSIGNED<br />HD</th>
            <th class="align-middle">PROGRESS</th>
            <th class="align-middle">KENDALA</th>
            <th class="align-middle">PENDING<br />PELANGGAN</th>
            @endfor
        </tr>
        @php
            $jml_ps = $total_ps = $total_ponactm = $total_pstactical_xhi = $total_pstactical_hi = $total_ps_kpro = $total_open_uncomp = $total_open_comp = $total_assigned_hd_uncomp = $total_assigned_hd_comp = $total_progress_uncomp = $total_progress_comp = $total_kendala_uncomp = $total_kendala_comp = $total_pending_uncomp = $total_pending_comp = $total_revoke = $total_belum_upload = 0;
        @endphp
        @foreach ($data_sektor as $area => $result)
        @php
        $total_ps += @$result['jml_ps'];
        $total_ponactm += @$result['jml_ponactm'];
        $total_pstactical_xhi += @$result['jml_pstactical_xhi'];
        $total_pstactical_hi += @$result['jml_pstactical_hi'];
        $total_ps_kpro += @$result['jml_ps_kpro'];
        $total_open_uncomp += @$result['jml_open_uncomp'];
        $total_open_comp += @$result['jml_open_comp'];
        $total_assigned_hd_uncomp += @$result['jml_assigned_hd_uncomp'];
        $total_assigned_hd_comp += @$result['jml_assigned_hd_comp'];
        $total_progress_uncomp += @$result['jml_progress_uncomp'];
        $total_progress_comp += @$result['jml_progress_comp'];
        $total_kendala_uncomp += @$result['jml_kendala_uncomp'];
        $total_kendala_comp += @$result['jml_kendala_comp'];
        $total_pending_uncomp += @$result['jml_pending_uncomp'];
        $total_pending_comp += @$result['jml_pending_comp'];
        $total_revoke += @$result['jml_revoke'];
        $total_belum_upload += @$result['jml_belum_upload'];
        @endphp
        @if ($area != 'NON AREA' || @$result['jml_ps'] != 0)
        <tr>
            <td class="align-middle">{{ $area }}</td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PS" target="_blank">{{ @$result['jml_ps'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PONACTM" target="_blank">{{ @$result['jml_ponactm'] ? : '0' }}</a></td> 
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PS_KPRO" target="_blank">{{ @$result['jml_ps_kpro'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PS_TACTICAL_XHI" target="_blank">{{ @$result['jml_pstactical_xhi'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PS_TACTICAL_HI" target="_blank">{{ @$result['jml_pstactical_hi'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=OPEN_UNCOMP" target="_blank">{{ @$result['jml_open_uncomp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=ASSIGNED_HD_UNCOMP" target="_blank">{{ @$result['jml_assigned_hd_uncomp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PROGRESS_UNCOMP" target="_blank">{{ @$result['jml_progress_uncomp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=KENDALA_UNCOMP" target="_blank">{{ @$result['jml_kendala_uncomp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PENDING_UNCOMP" target="_blank">{{ @$result['jml_pending_uncomp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=OPEN_COMP" target="_blank">{{ @$result['jml_open_comp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=ASSIGNED_HD_COMP" target="_blank">{{ @$result['jml_assigned_hd_comp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PROGRESS_COMP" target="_blank">{{ @$result['jml_progress_comp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=KENDALA_COMP" target="_blank">{{ @$result['jml_kendala_comp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=PENDING_COMP" target="_blank">{{ @$result['jml_pending_comp'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=REVOKE" target="_blank">{{ @$result['jml_revoke'] ? : '0' }}</a></td>
            <td class="align-middle"><a href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area={{ $area }}&status=BELUM_UPLOAD" target="_blank">{{ @$result['jml_belum_upload'] ? : '0' }}</a></td>
            @php
                $round = @round(@$result['jml_pstactical_hi'] / @$result['jml_ps'] * 100,2);
            @endphp
            @if (is_nan($round) == true)
                @php
                    $roundx = '';
                    $bg = 'black-grey';
                @endphp
            @elseif ($round == 100)
                @php
                    $roundx = $round;
                    $bg = 'green-win';
                @endphp
            @else
                @php
                    $roundx = $round;
                    $bg = 'red-lose';
                @endphp
            @endif
            <td class="align-middle {{ $bg }}">{{ $roundx }} %</td>
        </tr>
        @endif
        @endforeach
        @php
            $round = @round($total_pstactical_hi / $total_ps * 100,2);
        @endphp
        @if (is_nan($round) == true)
            @php
                $roundx = '';
                $bg = 'black-grey';
            @endphp
        @elseif ($round == 100)
            @php
                $roundx = $round;
                $bg = 'green-win';
            @endphp
        @else
            @php
                $roundx = $round;
                $bg = 'red-lose';
            @endphp
        @endif
        <tr>
            <th class="align-middle {{ $bg }}">TOTAL</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PS" target="_blank">{{ $total_ps ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PONACTM" target="_blank">{{ $total_ponactm ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PS_KPRO" target="_blank">{{ $total_ps_kpro ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PS_TACTICAL_XHI" target="_blank">{{ $total_pstactical_xhi ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PS_TACTICAL_HI" target="_blank">{{ $total_pstactical_hi ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=OPEN_UNCOMP" target="_blank">{{ $total_open_uncomp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=ASSIGNED_HD_UNCOMP" target="_blank">{{ $total_assigned_hd_uncomp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PROGRESS_UNCOMP" target="_blank">{{ $total_progress_uncomp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=KENDALA_UNCOMP" target="_blank">{{ $total_kendala_uncomp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PENDING_UNCOMP" target="_blank">{{ $total_pending_uncomp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=OPEN_COMP" target="_blank">{{ $total_open_comp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=ASSIGNED_HD_COMP" target="_blank">{{ $total_assigned_hd_comp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PROGRESS_COMP" target="_blank">{{ $total_progress_comp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=KENDALA_COMP" target="_blank">{{ $total_kendala_comp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=PENDING_COMP" target="_blank">{{ $total_pending_comp ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=REVOKE" target="_blank">{{ $total_revoke ? : '0' }}</th>
            <th class="align-middle {{ $bg }}"><a style="color: white !important" href="/dashboard/tactical/pickupOnlineDetail?source={{ $source }}&witel={{ $witelx }}&type={{ $type }}&area=ALL&status=BELUM_UPLOAD" target="_blank">{{ $total_belum_upload ? : '0' }}</th>
            <th class="align-middle {{ $bg }}">{{ $roundx }} %</th>
        </tr>
    </table>

    <p class="text-muted">
        PROGRESS ==> Order Pickup Online Status (PICKED, GO, WORK)
        <br />
        POTENSI PS ==> Order Date HI + Order Status (OSS PONR, WFM ACTIVATION COMPLETE)
        <br />
        #N/A ==> Order PS namun belum di upload ke Dashboard Work Order TacticalPro
        <br />
        ACH% ==> PS PO HI / PS
    </p>
</div>
</div>
@endsection