@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/dashboard/assurance/{{ date('Y-m-d') }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>Detail - Rata Rata Close Perhari</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel-body table-responsive" style="padding:0px !important">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>No.</th>
          <th>No Tiket</th>
          <th>Inet</th>
          <th>Hasil Ukur Dispatch</th>
          <th>Hasil Ukur Laporan</th>
          <th>Customer Name</th>
          <th>Contact Phone</th>
          <th>Summary</th>
          <th>Customer Segment</th>
          <th>Reported Date</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>IOAN</th>
          <th>Status</th>
          <th>Action</th>
          <th>Catatan</th>
          <th>ODP Laporan</th>
          <th>Tanggal Laporan</th>
          <th>Tanggal Dispatch</th>
        </tr>
        @foreach($getData as $no => $data)
          <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $data->Incident }}</td>
              <td>{{ $data->Service_No ? : $data->no_internet }}</td>
              <td>{{ $data->onurx_ib }}</td>
              <td>{{ $data->redaman_iboster }}</td>
              <td>{{ $data->Customer_Name }}</td>
              <td>{{ $data->Contact_Phone }}</td>
              <td>{{ $data->Summary }}</td>
              <td>{{ $data->Customer_Segment ? : $data->Segment_Status }}</td>
              <td>{{ $data->Reported_Date }}</td>
              <td>{{ $data->tim }}</td>
              <td>{{ $data->sektor }}</td>
              <td>{{ $data->ioan }}</td>
              <td>{{ $data->status }}</td>
              <td>{{ $data->action }}</td>
              <td>{{ $data->catatan }}</td>
              <td>{{ $data->nama_odp }}</td>
              <td>{{ $data->tgl_laporan }}</td>
              <td>{{ $data->tgl_dispatch }}</td>
          </tr>
        @endforeach       
        </table>
    </div>
</div>
</div>
@endsection