@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/giveawayPS?date={{ $date }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL GIVEAWAY PS TIM {{ $tim }} PERIODE {{ $date }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="text-align: center;">NO</th>
                        <th style="text-align: center;">TIM</th>
                        <th style="text-align: center;">SEKTOR</th>
                        <th style="text-align: center;">MITRA</th>
                        <th style="text-align: center;">SC</th>
                        <th style="text-align: center;">ORDER NAME</th>
                        <th style="text-align: center;">ORDER DATE PS</th>
                        <th style="text-align: center;">STATUS TEKNISI</th>
                        <th style="text-align: center;">TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $num => $data)                
                    <tr>
                        <td style="text-align: center;">{{ ++$num }}</td>
                        <td style="text-align: center;">{{ $data->tim ? : 'NON TIM' }}</td>
                        <td style="text-align: center;">{{ $data->sektor ? : 'NON SEKTOR' }}</td>
                        <td style="text-align: center;">{{ $data->mitra ? : 'NON MITRA' }}</td>
                        <td style="text-align: center;">{{ $data->order_id ? : '#N/A' }}</td>
                        <td style="text-align: center;">{{ $data->nama_pelanggan ? : '#N/A' }}</td>
                        <td style="text-align: center;">{{ $data->order_datePs ? : '#N/A' }}</td>
                        <td style="text-align: center;">{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td style="text-align: center;">{{ $data->tgl_laporan ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT PICKUP ONLINE TACTICAL BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection