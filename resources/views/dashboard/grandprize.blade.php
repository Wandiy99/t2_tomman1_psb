@extends('layout')

@section('content')
  @include('partial.alerts')
  <center>
    <h3>Dashboard GrandPrize Teknisi</h3>
  </center>
  <div class="row">
    <div class="col-sm-12">
      <table class="table">
        <tr>
          <td>RANK</td>
          <td>TIM</td>
          <td>AO</td>
          <td>LOLOS QC</td>
          <td>MO</td>
          <td>FFG</td>
          <td>POIN</td>
        </tr>
        @foreach ($query as $num => $result)
        <?php
          $poin = 0;
          $ffg = $result->jumlah_ffg*-4;
          $poin = ($result->lolos*4)+($result->jumlah_mo*2)+$ffg;
        ?>
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $result->uraian }}</td>
          <td>{{ $result->jml_order }}</td>
          <td><a href="/dashboard/QCborneoTeknisiListDone/{{ $date }}/{{ $dateend }}/{{ $result->uraian }}">{{ $result->lolos }}</a></td>
          <td>{{ $result->jumlah_mo }}</td>
          <td>{{ $result->jumlah_ffg }}</td>
          <td>{{ $poin }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
