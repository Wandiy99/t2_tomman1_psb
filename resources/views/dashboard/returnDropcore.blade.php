@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      <center><h3>Dashboard Return Dropcore</h3><small>Periode {{ $periode }}</small></center>
      <table class="table">
        <tr>
          <th>Team Leader</th>
          <th>Tarik Ulang</th>
          <th>DC Return</th>
          <th>%</th>
        </tr>
        @foreach ($query as $result)
        <?php
          $ach = ($result->returnDropcore/$result->jumlah)*100;
        ?>
        <tr>
          <td>{{ strtoupper($result->nama) }} - ({{ $result->nik }})</td>
          <td><a href="/dashboardReturnDropcoreList/jumlah/{{ $result->nik }}/{{ $periode }}">{{ $result->jumlah }}</a></td>
          <td><a href="/dashboardReturnDropcoreList/returned/{{ $result->nik }}/{{ $periode }}">{{ $result->returnDropcore }}</a></td>
          <td>{{ round($ach,2) }}%</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
