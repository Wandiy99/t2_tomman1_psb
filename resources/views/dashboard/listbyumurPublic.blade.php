@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>
  </a><h3>List Assurance {{ $sektor }} {{ $durasi }}</h3>

      <table style="width:100%" class="table table-responsive">
        <tr>
          <th>No.</th>
          <th>No Tiket</th>
          <th>ND Internet</th>
          <th>Tiket</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>Order</th>
          <th>Tgl_Open</th>
          <th>Status Laporan</th>
          <th>STO</th>
          <th>Umur</th>
          <th>Loker Dispatch</th>
          <th>Channel</th>
          <th>Sebab</th>
          <th>Action</th>
          <th>Catatan Tek.</th>
        </tr>
        @foreach ($getAssuranceList as $num => $AssuranceList)
        <tr>
          <td>{{ ++$num }}</td>
          <td>{{ $AssuranceList->no_tiket }}</td>
          <td>{{ $AssuranceList->no_internet }}</td>
          <td>{{ $AssuranceList->updated_at }}</td>
          <td>
            {{ $AssuranceList->uraian }}
          </td>
          <td>
            {{ $AssuranceList->sektor }}
          </td>
          <td>
            {{ $AssuranceList->no_telp }} ~ {{ $AssuranceList->no_speedy }}<br />
          </td>
          <td>{{ $AssuranceList->tgl_open }}</td>
          <td>{{ $AssuranceList->laporan_status ? : 'ANTRIAN' }}</td>
          <td>{{ $AssuranceList->sto_trim }}</td>
          <td>{{ $AssuranceList->hari*24 }}</td>
          <td>{{ $AssuranceList->loker_dispatch }}</td>
          <td>{{ $AssuranceList->channel }}</td>
          <td>{{ $AssuranceList->penyebab }}</td>
          <td>{{ $AssuranceList->action }}</td>
          <td>{{ $AssuranceList->catatan }}</td>
        </tr>
        @endforeach
      </table>
@endsection
