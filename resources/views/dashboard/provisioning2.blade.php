@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }
  </style>
  <h3>Dashboard Provisioning TA KALSEL</h3>
  <small>Periode {{ $tgl }}</small>
  <div class="row">
    <div class="col-sm-4">
      <small>Table 1</small>
      <table class="table table-responsive">
        <tr></tr>>
          <th>Status</th>
          <th>INNER</th>
          <th>BBR</th>
          <th>KDG</th>
          <th>TJL</th>
          <th>BLC</th>
          <th>JUMLAH</th>
        </tr>
        <?php
          $no_update_jumlah = 0;
          $no_update_inner = 0;
          $no_update_bbr = 0;
          $no_update_kdg = 0;
          $no_update_tjl = 0;
          $no_update_blc = 0;
          $total_inner = 0;
          $total_bbr = 0;
          $total_kdg = 0;
          $total_tjl = 0;
          $total_blc = 0;
          $total = 0;
        ?>
        @foreach ($data as $result)
        @if ($result->laporan_status<>NULL AND $result->laporan_status_id <> 6)
        <tr>
          <td>{{ $result->laporan_status }}</td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/INNER">{{ $result->WO_INNER }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/BBR">{{ $result->WO_BBR }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/KDG">{{ $result->WO_KDG }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/TJL">{{ $result->WO_TJL }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/BLC">{{ $result->WO_BLC }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/{{ $jenis }}/{{ $result->laporan_status }}/ALL">{{ $result->jumlah }}</a></td>
        </tr>
        <?php
          $total_inner += $result->WO_INNER;
          $total_bbr += $result->WO_BBR;
          $total_kdg += $result->WO_KDG;
          $total_tjl += $result->WO_TJL;
          $total_blc += $result->WO_BLC;
        ?>
        @else
        <?php
          $no_update_jumlah += $result->jumlah;
          $no_update_inner += $result->WO_INNER;
          $no_update_bbr += $result->WO_BBR;
          $no_update_kdg += $result->WO_KDG;
          $no_update_tjl += $result->WO_TJL;
          $no_update_blc += $result->WO_BLC;
        ?>

        @endif
        @endforeach
        <tr>
          <td>ANTRIAN</td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/INNER">{{ $no_update_inner }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/BBR">{{ $no_update_bbr }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/KDG">{{ $no_update_kdg }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/TJL">{{ $no_update_tjl }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/BLC">{{ $no_update_blc }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ANTRIAN/ALL">{{ $no_update_jumlah }}</a></td>
        </tr>
        <?php
        $total_inner = $total_inner + $no_update_inner;
        $total_bbr = $total_bbr + $no_update_bbr;
        $total_kdg = $total_kdg + $no_update_kdg;
        $total_tjl = $total_tjl + $no_update_tjl;
        $total_blc = $total_blc + $no_update_blc;
        $total = $total_inner + $total_bbr + $total_kdg + $total_kdg + $total_blc;
        ?>
        <tr>
          <td>TOTAL</td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/INNER">{{ $total_inner }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/BBR">{{ $total_bbr }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/KDG">{{ $total_kdg }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/TJL">{{ $total_tjl }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/BLC">{{ $total_blc }}</a></td>
          <td><a href="/dashboard/provisioningList/{{ $tgl }}/ALL/ALL/ALL">{{ $total }}</a></td>
        </tr>
      </table>
    </div> 
    <div class="col-sm-6">
      <small>Table 2</small>
      Filter :
      <select name="filter">
        <option value=""> -- </option>
        <option value="NEWSALES">NEWSALES</option>
      </select>

      <table class="table table-responsive">
        <tr>
          <th rowspan="2">Area</th>
          <th colspan="3">PI</th>
          {{-- <th rowspan="2">FO</th> --}}
          <th colspan="2">FO</th>
          <th rowspan="2">ACT COM</th>
          <th rowspan="2">PS HI</th>
        </tr>
        <tr>
          <th>Undispatch</th>
          <th>Dispatched</th>
          <th>Total</th>
          <th>Kendala</th>
          <th>UP</th>
        </tr>
        <?php
          $jumlah_dispatch = 0;
          $jumlah_undispatch = 0;
          $jumlah_pi = 0;
          // $jumlah_fo = 0;
          $jml_foKendala = 0;
          $jml_foUp      = 0;
          $jml_act_com   = 0;
          $jml_ps        = 0;
        ?>
        @foreach ($reportPotensi as $rPotensi)
        <?php
          $jumlah_dispatch += $rPotensi->jumlah_dispatch;
          $jumlah_undispatch += $rPotensi->jumlah_undispatch;
          $total_pi = $rPotensi->jumlah_dispatch + $rPotensi->jumlah_undispatch;
          $jumlah_pi += $total_pi;
          // $jumlah_fo += $rPotensi->fo;
          $jml_foKendala += $rPotensi->fo_kendala;
          $jml_foUp      += $rPotensi->fo_up;
          $jml_act_com   += $rPotensi->act_com;
          $jml_ps        += $rPotensi->ps;
        ?>
        <tr>
          <td>{{ $rPotensi->area_migrasi }}</td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/undispatch">{{ $rPotensi->jumlah_undispatch }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/dispatched">{{ $rPotensi->jumlah_dispatch }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/pi">{{ $total_pi }}</a></td>
          {{-- <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/fo">{{ $rPotensi->fo }}</a></td> --}}
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/fokendala">{{ $rPotensi->fo_kendala }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/foup">{{ $rPotensi->fo_up }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/actcom">{{ $rPotensi->act_com }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/{{ $rPotensi->area_migrasi }}/ps">{{ $rPotensi->ps }}</a></td>
        </tr>
        @endforeach
        <tr>
          <td>Total</td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/undispatch">{{ $jumlah_undispatch }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/dispatched">{{ $jumlah_dispatch }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/pi">{{ $jumlah_pi }}</a></td>
          {{-- <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/fo">{{ $jumlah_fo }}</a></td> --}}
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/fokendala">{{ $jml_foKendala }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/foup">{{ $jml_foUp }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/actcom">{{ $jml_act_com }}</a></td>
          <td><a href="/dashboard/listpotensi/{{ $tgl }}/ALL/ps">{{ $jml_ps }}</a></td>
        </tr>
      </table>
    </div>
  </div>
  <br />
  <br />
  <br />
  <br />
@endsection
