@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }

    td {
      padding : 5px;
      border:1px solid #ecf0f1;

    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #f1c40f;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }

    .panel-greenx>.panel-heading {
      background-color: #00cec9;
      font-weight: bold;
      color : #FFF;
    }

    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
    .panel-content {
      padding : 10px;
    }
    .panel-nol>.panel-heading {
    color: #fff;
    background-color: #636e72;
    border-color: #dddddd;
}
  </style>
  <script>

  </script>
  <center>
  <h3>Saldo saat ini ({{ count($getumurTiket) }})</h3>
  </center>
  <span id="seconds"></span>
  <div class="row">
    @foreach ($getumurTiket as $num => $umurTiket)
    <div class="col-xs-8 col-sm-12">
      <?php
        if ($umurTiket->lamaHari>2){
          $color = 'danger';
        } else if ($umurTiket->lamaHari>1){
          $color = 'warning';
        } else if ($umurTiket->lamaHari>0){
          $color = 'nol';
        } else if ($umurTiket->lamaJam<12){
          $color = 'greenx';
        } else {
          $color = 'default';
        }
      ?>
      <div class="panel panel-{{ $color }}">

        <div class="panel-heading"><b>{{ $umurTiket->TROUBLE_NO }} ({{ $umurTiket->tglOpen ? : $umurTiket->TROUBLE_OPENTIME ? : $umurTiket->trouble_opentime }}) // <span id="daysX{{ $umurTiket->TROUBLE_NO }}"></span> days
        <span id="hoursX{{ $umurTiket->TROUBLE_NO }}"></span> hours
        <span id="minsX{{ $umurTiket->TROUBLE_NO }}"></span> mins
        <span id="secsX{{ $umurTiket->TROUBLE_NO }}"></span> secs</b> // {{ $umurTiket->laporan_status ? : 'ANTRIAN' }} ({{ $umurTiket->tgl_update ? : $umurTiket->tgl_created ? : '0' }})
        <span class="label label-info">{{ $umurTiket->loker_dispatch }}</span>
        <a href="/nossaIn/{{ $umurTiket->TROUBLE_NO }}">Sync</a>
        </div>
        <div class="panel-content">
          [{{ $umurTiket->title }}] [{{ $umurTiket->uraian }}] {{ $umurTiket->HEADLINE ? : $umurTiket->headline }}<br />

        </div>
      </div>
    </div>

    <script>
    upTime('daysX{{ $umurTiket->TROUBLE_NO }}','hoursX{{ $umurTiket->TROUBLE_NO }}','minsX{{ $umurTiket->TROUBLE_NO }}','secsX{{ $umurTiket->TROUBLE_NO }}','jan,01,2014,00:00:00');
    </script>
    @endforeach
  </div>

  <script>

    @foreach ($getumurTiket as $num => $umurTiket)
    function upTime{{ $umurTiket->TROUBLE_NO }}(daysX,hoursX,minsX,secsX,countTo) {

      now = new Date();
      countTo = new Date(countTo);
      difference = (now-countTo);

      days=Math.floor(difference/(60*60*1000*24)*1);
      hours=Math.floor((difference%(60*60*1000*24))/(60*60*1000)*1);
      mins=Math.floor(((difference%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);
      secs=Math.floor((((difference%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);

      document.getElementById(daysX).innerHTML = days;
      document.getElementById(hoursX).innerHTML = hours;
      document.getElementById(minsX).innerHTML = mins;
      document.getElementById(secsX).innerHTML = secs;

      clearTimeout(upTime{{ $umurTiket->TROUBLE_NO }}.to);
      upTime{{ $umurTiket->TROUBLE_NO }}.to=setTimeout(function(){ upTime{{ $umurTiket->TROUBLE_NO }}(daysX,hoursX,minsX,secsX,countTo); },1000);
    }
    <?php
      if ($umurTiket->tglOpen<>NULL){ $tanggalnya = $umurTiket->tglOpen; } else { $tanggalnya = '0000-00-00 00:00:00'; }
      $dateTimeExplode = explode(' ',$tanggalnya);
      $dateExplode = explode('-',$dateTimeExplode[0]);
    ?>

    upTime{{ $umurTiket->TROUBLE_NO }}('daysX{{ $umurTiket->TROUBLE_NO }}','hoursX{{ $umurTiket->TROUBLE_NO }}','minsX{{ $umurTiket->TROUBLE_NO }}','secsX{{ $umurTiket->TROUBLE_NO }}','{{ $dateExplode[1] }},{{ $dateExplode[2] }},{{ $dateExplode[0] }},{{ $dateTimeExplode[1] }}');

    @endforeach

  </script>
@endsection
