@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">UNDISPATCH SC BY ORDER DATE {{ date('Y-m-d',strtotime('-7 days')) }} S/D {{ date('Y-m-d') }}</h3>
        <div class="table-responsive">
            <table id="table_undispatch" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>STO</th>
                        <th>ORDER ID</th>
                        <th style="width: 10%">APPOINTMENT<br />DESC</th>
                        <th>WFM ID</th>
                        <th>ORDER NAME</th>
                        <th>ORDER ADDRESS</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>ORDER STATUS</th>
                        <th>NCLI</th>
                        <th>INET</th>
                        <th>VOICE</th>
                        <th>KCONTACT</th>
                        <th>ALPRO NAME</th>
                        <th>LAYANAN</th>
                        <th>DATEL</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($reguler as $num => $result)
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->sto }}</td>
                        <td>{{ $result->orderId }}</td>
                        <td>{{ $result->appointment_desc }}</td>
                        <td>{{ $result->wfm_id }}</td>
                        <td>{{ $result->orderName }}</td>
                        <td>{{ $result->orderAddr }}</td>
                        <td>{{ $result->orderDate }}</td>
                        <td>{{ $result->orderDatePs }}</td>
                        <td>{{ $result->orderStatus }}</td>
                        <td>{{ $result->orderNcli }}</td>
                        <td>{{ $result->internet }}</td>
                        <td>{{ $result->noTelp }}</td>
                        <td>{{ $result->kcontact }}</td>
                        <td>{{ $result->alproname }}</td>
                        <td>{{ $result->jenisPsb }}</td>
                        <td>{{ $result->area_alamat }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">UNDISPATCH BACKEND SC</h3>
        <div class="table-responsive">
            <table id="table_backend" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>MYIR</th>
                        <th>MYIR (INT)</th>
                        <th>SCID</th>
                        <th>CUSTOMER NAME</th>
                        <th>ORDER DATE</th>
                        <th>STATUS RESUME</th>
                        <th>JENIS PSB</th>
                        <th>PACKAGE NAME</th>
                        <th>ND INTERNET</th>
                        <th>ND POTS</th>
                        <th>GPS LATITUDE</th>
                        <th>GPS LONGITUDE</th>
                        <th>CUSTOMER ADDRESS</th>
                        <th>KCONTACT</th>
                        <th>LOC ID</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($backend_sc as $num => $result)
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->ORDER_CODE }}</td>
                        <td>{{ $result->ORDER_CODE_INT }}</td>
                        <td>{{ $result->SCID }}</td>
                        <td>{{ $result->CUSTOMER_NAME }}</td>
                        <td>{{ $result->ORDER_DATE }}</td>
                        <td>{{ $result->STATUS_RESUME }}</td>
                        <td>{{ $result->JENISPSB }}</td>
                        <td>{{ $result->PACKAGE_NAME }}</td>
                        <td>{{ $result->ND_INTERNET }}</td>
                        <td>{{ $result->ND_POTS }}</td>
                        <td>{{ $result->GPS_LATITUDE }}</td>
                        <td>{{ $result->GPS_LONGITUDE }}</td>
                        <td>{{ $result->CUSTOMER_ADDR }}</td>
                        <td>{{ $result->KCONTACT }}</td>
                        <td>{{ $result->LOC_ID }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">UNDISPATCH RISMA</h3>
        <div class="table-responsive">
            <table id="table_risma" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>APPLY DATE</th>
                        <th>TRACK ID</th>
                        <th>TRACK ID (INT)</th>
                        <th>NAMA PELANGGAN</th>
                        <th>HANDPHONE</th>
                        <th>EMAIL</th>
                        <th>SURVEY</th>
                        <th>ODP</th>
                        <th>KORDINAT</th>
                        <th>WITEL</th>
                        <th>STATUS DATA</th>
                        <th>KETERANGAN</th>
                        <th>LAST UPDATED</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($risma as $num => $result)
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->apply_date }}</td>
                        <td>{{ $result->trackID }}</td>
                        <td>{{ $result->trackID_int }}</td>
                        <td>{{ $result->nama_pelanggan }}</td>
                        <td>{{ $result->handphone }}</td>
                        <td>{{ $result->email }}</td>
                        <td>{{ $result->survey }}</td>
                        <td>{{ $result->odp }}</td>
                        <td>{{ $result->kordinat }}</td>
                        <td>{{ $result->witel }}</td>
                        <td>{{ $result->status_data }}</td>
                        <td>{{ $result->keterangan }}</td>
                        <td>{{ $result->last_updated }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">EX-KENDALA-JOINTER</h3>
        <div class="table-responsive">
            <table id="table_ex_kendala" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>JOINTER UPDATE</th>
                        <th>DATEL</th>
                        <th>STO</th>
                        <th>ORDER ID</th>
                        <th>MANJA</th>
                        <th>ORDER NAME</th>
                        <th>K-CONTACT</th>
                        <th>LAYANAN</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($ex_kendala as $num => $result)
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->response_at }}</td>
                        <td>{{ $result->area_alamat }}</td>
                        <td>{{ $result->sto }}</td>
                        <td>{{ $result->orderId }}</td>
                        <td>
                            @if ($result->status_manja_hd == 'OK LANJUT')
                                @php
                                    $label = 'label-success';
                                @endphp
                            @elseif ($result->status_manja_hd == 'CANCEL')
                                @php
                                    $label = 'label-danger';
                                @endphp
                            @else
                                @php
                                    $label = 'label-info';
                                @endphp
                            @endif
                            <span type="button" class="label {{ $label }}" data-toggle="modal" data-target="#responsive-modal-{{ $result->id_api }}">
                                {{ $result->status_manja_hd ?? 'NEED CALL' }}
                            </span>
                            <div id="responsive-modal-{{ $result->id_api }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $result->id_api }}" aria-hidden="true" style="display: none;">
                                <form class="form-group" action="/helpdesk/manja_exkendala/{{ $result->id_api }}" method="POST">
                                    <input type="hidden" name="id_api" value="{{ $result->id_api }}">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Order ID {{ $result->orderId }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="col-sm-12 text-center">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Status :</label>
                                                            <select class="form-control select2" name="status_manja_hd" id="status_manja_hd" required>
                                                                <option value="" selected disabled>Pilih Status</option>
                                                                <option value="OK LANJUT">OK LANJUT</option>
                                                                <option value="CANCEL">CANCEL</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">No HP Pelanggan :</label>
                                                            <input class="form-control" type="text" name="no_hp_pelanggan" value="{{ $result->no_hp_pelanggan }}" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Balasan :</label>
                                                            <textarea class="form-control" name="note_manja_hd" rows="5" required>{{ $result->note_manja_hd }}</textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </td>
                        <td>{{ $result->orderName }}</td>
                        <td>{{ $result->kcontact }}</td>
                        <td>{{ $result->jenisPsb }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_undispatch').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'UNDISPATCH SC BY TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'UNDISPATCH SC BY TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'UNDISPATCH SC BY TOMMAN'
                }
            ]
        });
        $('#table_backend').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'UNDISPATCH BACKEND SC BY TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'UNDISPATCH BACKEND SC BY TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'UNDISPATCH BACKEND SC BY TOMMAN'
                }
            ]
        });
        $('#table_risma').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'UNDISPATCH RISMA BY TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'UNDISPATCH RISMA BY TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'UNDISPATCH RISMA BY TOMMAN'
                }
            ]
        });
        $('#table_ex_kendala').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'UNDISPATCH EX-KENDALA-JOINTER BY TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'UNDISPATCH EX-KENDALA-JOINTER BY TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'UNDISPATCH EX-KENDALA-JOINTER BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection