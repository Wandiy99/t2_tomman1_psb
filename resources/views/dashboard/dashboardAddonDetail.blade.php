@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/Addon?source={{ $source }}&startDate={{ $start }}&endDate={{ $end }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL ADDON SUMBER {{ $source }} DATEL {{ $datel }} PERIODE {{ $start }} S/D {{ $end }} STATUS {{ strtoupper($status) }} {{ strtoupper($waktu) }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>UMUR</th>
                        <th>ORDER ID</th>
                        @if($source == 'MYIRMANUAL')
                        <th>SC</th>
                        @endif
                        <th>ORDER NAME</th>
                        <th>ORDER DATE</th>
                        <th>ORDER STATUS</th>
                        <th>LAYANAN</th>
                        <th>K-CONTACT</th>
                        <th>STATUS TEKNISI</th>
                        <th>TANGGAL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area ? : 'NON AREA' }}</td>
                        <td>{{ $data->sektor ? : @$data->sektorr ? : '#N/A' }}</td>
                        <td>{{ $data->tim ? : @$data->timm ? : '#N/A' }}</td>
                        <td>{{ $data->umur_jam ? : '0' }} Jam</td>
                        <td>{{ $data->orderId ? : '#N/A' }}</td>
                        @if($source == 'MYIRMANUAL')
                        <td>{{ $data->orderIdd ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->orderName ? : '#N/A' }}</td>
                        <td>{{ $data->orderDate ? : '#N/A' }}</td>
                        <td>{{ $data->orderStatus ? : '#N/A' }}</td>
                        <td>{{ $data->layanan ? : '#N/A' }}</td>
                        <td>{{ $data->kcontact ? : '#N/A' }}</td>
                        <td>{{ $data->laporan_status ? : @$data->laporan_statuss ? : '#N/A' }}</td>
                        <td>{{ $data->tgl_laporan ? : @$data->tgl_laporann ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DASHBOARD ADDON BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection