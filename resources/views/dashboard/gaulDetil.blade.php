@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
      th {
        background-color: #FF0000;
        color : #FFF;
        text-align: center;
        vertical-align: middle;
      }
      td {
        color : #000;
      }
    </style>

  <a href="/dashboard/assurance/{{ date('Y-m-d') }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span></a>
<h3>ASSURANCE GAUL</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel-body table-responsive" style="padding:0px !important">
      <table class="table table-striped table-bordered dataTable">
        <tr>
          <th>NO</th>
          <th>INCIDENT</th>
          <th>SERVICE NO</th>
          <th>GAUL</th>
          <th>SEKTOR</th>
          <th>TIM</th>
          <th>STATUS</th>
          <th>ACTION</th>
          <th>PENYEBAB</th>
          <th>PENYEBAB RETI</th>
          <th>CATATAN</th>
          <th>TGL UPDATE</th>
          <th>TGL DISPATCH</th>
          <th>REPORTED DATE</th>
        </tr>
        @foreach($getData as $no => $data)
          <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $data->Incident }}</td>
              <td>{{ $data->Service_No }}</td>
              @if ($view==0)
              <td><a href="/dashboard/assurance/gaul/detil/{{ $data->GAUL }}/{{ $data->Service_No }}">{{ $data->GAUL }}</a></td>
              @elseif ($view==1)
              <td>{{ $data->GAUL }}</td>
              @endif 
              <td>{{ $data->sektor }}</td>
              <td>{{ $data->tim }}</td>
              <td>{{ $data->status ? : 'ANTRIAN' }}</td>
              <td>{{ $data->action ? : '-' }}</td>
              <td>{{ $data->penyebab ? : '-' }}</td>
              <td>{{ $data->penyebab_reti ? : '-' }}</td>
              <td>{{ $data->catatan ? : '-' }}</td>
              <td>{{ $data->tgl_update }}</td>
              <td>{{ $data->tgl_dispatch }}</td>
              <td>{{ $data->Reported_Date }}</td>
          </tr>
        @endforeach       
        </table>
    </div>
</div>
</div>
@endsection