@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL FFG & TTI {{ $sektor }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SEKTOR</th>
                        @if($status == 'PS_KPRO')
                        <th>ORDER_ID</th>
                        <th>DATEL</th>
                        <th>STO</th>
                        <th>JENIS_PSB</th>
                        <th>TYPE_TRANSAKSI</th>
                        <th>STATUS_RESUME</th>
                        <th>KCONCACT</th>
                        <th>ORDER_DATE</th>
                        <th>SPEEDY</th>
                        <th>POTS</th>
                        <th>CUSTOMER_NAME</th>
                        <th>CUSTOMER_ADDRESS</th>
                        <th>LOC_ID</th>
                        <th>CATEGORY</th>
                        <th>PROVIDER</th>
                        <th>LAST_SYNC</th>                
                        @elseif($status == 'FFG')
                        <th>ORDER_ID</th>
                        <th>MYIH</th>
                        <th>NCLI</th>
                        <th>SPEEDY</th>
                        <th>TGL_PS</th>
                        <th>WO_TA_AMCREW</th>
                        <th>COMPLY</th>
                        <th>GGN_TOTAL</th>
                        <th>GGN_INCLUDE</th>
                        <th>GGN_EXCLUDE</th>
                        <th>SEQ_GGN</th>
                        <th>TROUBLE_NO</th>
                        <th>TROUBLE_OPENTIME</th>
                        <th>ACTUAL_SOLUTION</th>
                        <th>TROUBLE_HEADLINE</th>
                        <th>INCL</th>
                        <th>LAST_UPDATED</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($query as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->sektor_prov }}</td>
                        @if($status == 'PS_KPRO')
                        <td>{{ $data->ORDER_ID }}</td>
                        <td>{{ $data->DATEL }}</td>
                        <td>{{ $data->STO }}</td>
                        <td>{{ $data->JENIS_PSB }}</td>
                        <td>{{ $data->TYPE_TRANSAKSI }}</td>
                        <td>{{ $data->STATUS_RESUME }}</td>
                        <td>{{ $data->KCONTACT }}</td>
                        <td>{{ $data->ORDER_DATE }}</td>
                        <td>{{ $data->SPEEDY }}</td>
                        <td>{{ $data->POTS }}</td>
                        <td>{{ $data->CUSTOMER_NAME }}</td>
                        <td>{{ $data->CUSTOMER_ADDRESS }}</td>
                        <td>{{ $data->LOC_ID }}</td>
                        <td>{{ $data->CATEGORY }}</td>
                        <td>{{ $data->PROVIDER }}</td>
                        <td>{{ $data->LAST_SYNC }}</td>
                        @elseif($status == 'FFG')
                        <td>{{ $data->sc_id }}</td>
                        <td>{{ $data->my_ih }}</td>
                        <td>{{ $data->ncli }}</td>
                        <td>{{ $data->nd_speedy }}</td>
                        <td>{{ $data->tgl_ps }}</td>
                        <td>{{ $data->wo_ta_amcrew }}</td>
                        <td>{{ $data->comply }}</td>
                        <td>{{ $data->ggn_total }}</td>
                        <td>{{ $data->ggn_include }}</td>
                        <td>{{ $data->ggn_exclude }}</td>
                        <td>{{ $data->seq_ggn }}</td>
                        <td>{{ $data->trouble_no }}</td>
                        <td>{{ $data->trouble_opentime }}</td>
                        <td>{{ $data->actual_solution }}</td>
                        <td>{{ $data->trouble_headline }}</td>
                        <td>{{ $data->incl }}</td>
                        <td>{{ $data->last_updated }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL FFG & TTI SEKTOR TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL FFG & TTI SEKTOR TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL FFG & TTI SEKTOR TOMMAN'
                }
            ]
        });
    });
</script>
@endsection