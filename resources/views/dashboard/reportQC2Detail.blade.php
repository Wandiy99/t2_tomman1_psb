@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/kpro/reportQC2?area={{ $area }}&source={{ $source }}&startDate={{ $start }}&endDate={{ $end }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT UTQC2 TYPE {{ $source }} GROUP {{ $area }} AREA {{ $group }} PERIODE {{ $start }} S/D {{ $end }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan='2'>NO</th>
                        <th rowspan='2'>AREA</th>
                        @if ($area != 'WITEL')
                        <th rowspan='2'>TIM</th>
                        @endif
                        <th rowspan='2'>ORDER_ID</th>
                        <th rowspan='2'>ORDER_CODE</th>
                        <th rowspan='2'>CUSTOMER</th>
                        <th rowspan='2'>INTERNET</th>
                        <th rowspan='2'>TRANSAKSI</th>
                        <th rowspan='2'>UNIT</th>
                        <th rowspan='2'>STO</th>
                        <th rowspan='2'>TEKNISI</th>
                        <th rowspan='2'>AGENT</th>
                        @if ($area == 'WITEL')
                        <th rowspan='2'>MITRA_TYPE</th>
                        <th rowspan='2'>MITRA_NAME</th>
                        @endif
                        <th rowspan='2'>STATUS_UT<br />({{ $source }})</th>
                        <th rowspan='2'>STATUS_QC<br />({{ $source }})</th>
                        @if ($source == 'QC2')
                        <th rowspan='2'>STATUS_UT<br />(UTONLINE)</th>
                        <th rowspan='2'>STATUS_QC<br />(UTONLINE)</th>
                        @endif
                        <th colspan='8'>INFO_RETURN</th>
                        <th rowspan='2'>TANGGAL_PS</th>
                        @if ($area != 'WITEL')
                        <th rowspan='2'>STATUS_TEKNISI</th>
                        <th rowspan='2'>TANGGAL_LAPORAN</th>
                        @endif
                    </tr>
                    <tr>
                        <th>FOTO_KURANG_LENGKAP</th>
                        <th>BA_TIDAK_SESUAI</th>
                        <th>HASIL_UKUR_UNDER_SPEC / KOSONG</th>
                        <th>DATA_MATERIAL_TIDAK_SESUAI</th>
                        <th>TANGGAL_TIDAK SESUAI</th>
                        <th>ALAMAT / KOORDINAT_TIDAK_SESUAI</th>
                        <th>LOG_RETURN</th>
                        <th>CATATAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)
                    @php
                        $utt_details = json_decode(@$data->evidence ? : @$data->utt_evidence);
                        $log_return = '';
                    @endphp
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->area ? : 'NON AREA' }}</td>
                        @if ($area != 'WITEL')
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->order_id ? : '#N/A' }}</td>
                        <td>{{ $data->order_code ? : '#N/A' }}</td>
                        <td>{{ $data->utt_customer_desc ? : '#N/A' }}</td>
                        <td>{{ $data->noInternet ? : '#N/A' }}</td>
                        <td>{{ $data->transaksi ? : '#N/A' }}</td>
                        <td>{{ $data->unit ? : '#N/A' }}</td>
                        <td>{{ $data->sto ? : '#N/A' }}</td>
                        <td>{{ $data->teknisi ? : '#N/A' }}</td>
                        <td>{{ $data->agent ? : '#N/A' }}</td>
                        @if ($area == 'WITEL')
                        <td>{{ $data->mitra_type ? : '#N/A' }}</td>
                        <td>{{ $data->mitra_name ? : '#N/A' }}</td>
                        @endif
                        @if (in_array($source, ['UTONLINE', 'UTONLINEACTCOMP']))
                        <td>{{ $data->utt_statusName ? : '#N/A' }}</td>
                        <td>{{ $data->utt_qcStatusName ? : '#N/A' }}</td>
                        @else
                        <td>{{ $data->status_ut ? : '#N/A' }}</td>
                        <td>{{ $data->status_qc ? : '#N/A' }}</td>
                        @endif
                        @if ($source == 'QC2')
                        <td>{{ $data->utt_statusName ? : '#N/A' }}</td>
                        <td>{{ $data->utt_qcStatusName ? : '#N/A' }}</td>
                        @endif
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifPhoto == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifPhoto ? : '-' }}
                            @endif
                        </td>
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifBA == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifBA ? : '-' }}
                            @endif
                        </td>
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifUkur == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifUkur ? : '-' }}
                            @endif
                        </td>
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifMaterial == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifMaterial ? : '-' }}
                            @endif
                        </td>
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifTanggal == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifTanggal ? : '-' }}
                            @endif
                        </td>
                        <td class="align-middle">
                            @if(@$utt_details->datek->verifKoordinat == '1')
                            <p style="color: red; font-weight: bold;">X</p>
                            @else
                            {{ @$utt_details->datek->verifKoordinat ? : '-' }}
                            @endif
                        </td>
                        @php
                        if(@$utt_details->datek->verifPhoto == '1')
                        {
                            $log_return .= 'FOTO KURANG LENGKAP, ';
                        }
                        if(@$utt_details->datek->verifBA == '1')
                        {
                            $log_return .= 'BA TIDAK SESUAI, ';
                        }
                        if(@$utt_details->datek->verifUkur == '1')
                        {
                            $log_return .= 'HASIL UKUR UNDER SPEC / KOSONG, ';
                        }
                        if(@$utt_details->datek->verifMaterial == '1')
                        {
                            $log_return .= 'DATA MATERIAL TIDAK SESUAI, ';
                        }
                        if(@$utt_details->datek->verifTanggal == '1')
                        {
                            $log_return .= 'TANGGAL TIDAK SESUAI, ';
                        }
                        if(@$utt_details->datek->verifKoordinat == '1')
                        {
                            $log_return .= 'ALAMAT / KOORDINAT TIDAK SESUAI';
                        }
                        @endphp
                        <td>{{ $log_return }}</td>
                        <td class="align-middle">{{ @$utt_details->datek->verifNote ? : '-' }}</td>
                        <td>{{ $data->tanggal_ps ? : '#N/A' }}</td>
                        @if($area != "WITEL")
                        <td>{{ $data->laporan_status ? : '#N/A' }}</td>
                        <td>{{ $data->tanggal_laporan ? : '#N/A' }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT UTQC2 BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection