@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
   .table1 {
     border : 1px solid #000;
     font-size: 9px;
  }
  .table1 td,th {
    padding : 3px !important;
  }
  .table1 th {
    background-color: #000;
    color : #FFF;
  }
  </style>
  <h3>Dashboard 7 KPI Provisioning (Periode {{ $periode }})</h3>
  <div class="row table-responsive">
    <div class="col-sm-12">
      <table class="table1" border=1 width="100%">
        <tr>
          <th class="text-center align-middle" rowspan="2">RANK</th>
          <th class="text-center align-middle" rowspan="2">WITEL</th>
          <th class="text-center align-middle" rowspan="2">SEKTOR</th>
          <th class="text-center align-middle" colspan="3">PS/PI<br />(25%)</th>
          <th class="text-center align-middle" colspan="3">TTI COMPLY<br />(25%)</th>
          <th class="text-center align-middle" colspan="3">GGN < 60 PSB (15%)</th>
          <th class="text-center align-middle" colspan="3">VALIDASI<br />CORE PELANGGAN<br />(5%)</th>
          <th class="text-center align-middle" colspan="3">TTR FFG<br />(10%)</th>
          <th class="text-center align-middle" colspan="3">UJI PETIK<br />(10%)</th>
          <th class="text-center align-middle" colspan="3">QC RETURN<br />(10%)</th>
          <th class="text-center align-middle" rowspan="2">TOTAL<br />ACH (%)</th>

        </tr>
        <tr>
          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

          <th class="text-center align-middle">Target</th>
          <th class="text-center align-middle">Real</th>
          <th class="text-center align-middle">Ach</th>

        </tr>
        @foreach ($data as $num => $result)
        <?php
        $rank = ++$num;
        if ($rank%2==0){
          $color = 'background-color : #ecf0f1';
        } else {
          $color = 'background-color : #f5f6fa';
        }
        ?>
        <tr style="{{ $color }}" class="text-center align-middle">
          <td>{{ $rank }}</td>
          <td>{{ $result->WITEL }}</td>
          <td>{{ str_replace('TERRITORY BASE ', '', $result->SEKTOR) }}</td>
          <td>99%</td>
          <td>{{ $result->REALPSPI }}%</td>
          <td>{{ $result->ACHPSPI }}%</td>
          <td>98%</td>
          <td>{{ $result->REALTTI }}%</td>
          <td>{{ $result->ACHTTI }}%</td>
          <td>99%</td>
          <td>{{ $result->REALFFG }}%</td>
          <td>{{ $result->ACHFFG }}%</td>
          <td>100%</td>
          <td>{{ $result->REALVALCOR }}%</td>
          <td>{{ $result->ACHVALCOR }}%</td>
          <td>99%</td>
          <td>{{ $result->REALTTRFFG }}%</td>
          <td>{{ $result->ACHTTRFFG }}%</td>
          <td>99%</td>
          <td>{{ $result->REALUJIPETIK }}%</td>
          <td>{{ $result->ACHUJIPETIK }}%</td>
          <td>99%</td>
          <td>{{ $result->REALQCRETURN }}%</td>
          <td>{{ $result->ACHQCRETURN }}%</td>
          <?php
            $s_pspi = ($result->ACHPSPI*25)/100;
            $s_tti = ($result->ACHTTI*25)/100;
            $s_ffg = ($result->ACHFFG*15)/100;
            $s_valcor = ($result->ACHVALCOR*5)/100;
            $s_ttrffg = ($result->ACHTTRFFG*10)/100;
            $s_ujipetik = ($result->ACHUJIPETIK*10)/100;
            $s_qcreturn = ($result->ACHQCRETURN*10)/100;
            $total = $s_pspi+$s_tti+$s_ffg+$s_valcor+$s_ttrffg+$s_ujipetik+$s_qcreturn;
          ?>
          <td>{{ round($total,2) }}%</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
