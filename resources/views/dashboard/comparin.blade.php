@extends('layout')

@section('content')
@include('partial.alerts')
<?php
$data = json_decode($data);
?>
<style>
	td, th {
		padding:5px;
	}
</style>
<h3>Progress Tomman {{ $tgl }}</h3>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">Data</div>
			<div class="panel-body table-responsive" style="padding: 0px !important" >
				<table width="100%" border=1>
					<tr>
						<th rowspan="3">Witel</th>
						<th colspan="12">Progress</th>
						<th colspan="4">Kendala</th>
					</tr>
					<tr>
						<th colspan="2">BLM DISPATCH</th>
						<th colspan="2">DISPATCH</th>
						<th colspan="2">NEED PROGRESS</th>
						<th colspan="2">OGP</th>
						<th colspan="2">HOME REACH</th>
						<th colspan="2">UP</th>
						<th colspan="2">PELANGGAN</th>
						<th colspan="2">TEKNIS</th>
					</tr>
					<tr>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
						<th>PRA PROVI</th>
						<th>PROVI</th>
					</tr>
					@foreach ($data as $result)
					<tr>
						<td>{{ $result->witel }}</td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/BELUM_DISPATCH_PRA">{{ $result->BELUM_DISPATCH_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/BELUM_DISPATCH_AFTER">{{ $result->BELUM_DISPATCH_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/SUDAH_DISPATCH_PRA">{{ $result->SUDAH_DISPATCH_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/SUDAH_DISPATCH_AFTER">{{ $result->SUDAH_DISPATCH_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/NEED_PROGRESS_PRA">{{ $result->NEED_PROGRESS_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/NEED_PROGRESS_AFTER">{{ $result->NEED_PROGRESS_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/OGP_PRA">{{ $result->OGP_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/OGP_AFTER">{{ $result->OGP_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/HR_PRA">{{ $result->HR_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/HR_AFTER">{{ $result->HR_AFTER }}</a></td>
						<td>0</td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/UP">{{ $result->UP }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KP_PRA">{{ $result->KP_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KP_AFTER">{{ $result->KP_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KT_PRA">{{ $result->KT_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KT_AFTER">{{ $result->KT_AFTER }}</a></td>
					</tr>
					@endforeach
					@foreach ($data_bpp as $result)
					<tr>
						<td>{{ $result->witel }}</td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/BELUM_DISPATCH_PRA">{{ $result->BELUM_DISPATCH_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/BELUM_DISPATCH_AFTER">{{ $result->BELUM_DISPATCH_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/SUDAH_DISPATCH_PRA">{{ $result->SUDAH_DISPATCH_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/SUDAH_DISPATCH_AFTER">{{ $result->SUDAH_DISPATCH_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/NEED_PROGRESS_PRA">{{ $result->NEED_PROGRESS_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/NEED_PROGRESS_AFTER">{{ $result->NEED_PROGRESS_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/OGP_PRA">{{ $result->OGP_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/OGP_AFTER">{{ $result->OGP_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/HR_PRA">{{ $result->HR_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/HR_AFTER">{{ $result->HR_AFTER }}</a></td>
						<td>0</td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/UP">{{ $result->UP }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KP_PRA">{{ $result->KP_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KP_AFTER">{{ $result->KP_AFTER }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KT_PRA">{{ $result->KT_PRA }}</a></td>
						<td><a href="/dashboardComparinList/{{ $result->witel }}/{{ $tgl }}/KT_AFTER">{{ $result->KT_AFTER }}</a></td>
					</tr>
					@endforeach
					
				</table>
			</div>
		</div>
	</div>
</div>
@endsection