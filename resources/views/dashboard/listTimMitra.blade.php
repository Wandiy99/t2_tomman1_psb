@extends('layout')

@section('content')
  @include('partial.alerts')
  <a href="/dashboard/rekon/{{ $date }}/{{ $dateend }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
          <tr>
            <td>Nama Tim </td>
          </tr>
          @foreach ($data as $result)
          <tr>
            <td>{{ $result->uraian }}</td>
          </tr>
          @endforeach
      </table>
      </div>
    </div>
  </div>
@endsection
