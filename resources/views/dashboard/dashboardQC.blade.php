@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    .xinput {
        resize: horizontal;
        width: 330px;
        text-align: center;
    }
    
    .xinput:active {
        width: auto;   
    }
    
    .xinput:focus {
        min-width: 200px;
    }
</style>

<div class="panel panel-primary">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
<center>
<form method="get">
    <span style="font-weight: bolder !important;color: black !important;">FILTER BY DATE PS : </span>
    <input style="height: 35px; text-align: center; font-weight: bold" type="text" name="daterange" value="{{ $xdate }} - {{ $xdatex }}" />
</form>
</center>
</div></div></div></div><br />

<div class="panel panel-primary">
<div class="panel-heading text-center" style="background-color: white; border-color: white">
    <p class="text-muted">Last Update : {{ $log_sync->datetime }} WITA</p>
    <h4 style="font-weight: bolder !important;color: black !important;">REPORT QUALITY CONTROL PERIODE BY DATE PS {{ $date }} S/D {{ $datex }}</h4>
</div>
        <div class="table-responsive">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">MITRA</th>
                        <th class="align-middle" rowspan='2' style="background-color: #c0c5ce !important; color: black">SISA DAPROS<br /><small>(Belum divalidasi / Evidence Belum Lengkap)</small></th>
                        <th class="align-middle" colspan='5' style="background-color: #ff6f69 !important; color: black">VALIDASI 1</th>
                        <th class="align-middle" rowspan='2' style="background-color: #ffcc5c !important; color: black">VALIDASI 2</th>
                        <th class="align-middle" colspan='1' style="background-color: #88d8b0 !important; color: black">VALID</th>
                        <th class="align-middle" rowspan='2' style="background-color: #ec87f3 !important; color: black">GRAND TOTAL</th>
                        </tr>
                        <tr>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">TA<br />ASO</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">TA<br />DAMAN</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">TA<br />CC</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">HS</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">TA<br />AMO</th>
                        <th class="align-middle" style="background-color: #cfefdf !important; color: black">Incl. ROC<br />DAMAN</th>
                        </tr>
                    </thead>
                    @php
                    $total_dapros = 0;
                    $total_aso = 0;
                    $total_daman = 0;
                    $total_cc = 0;
                    $total_hs = 0;
                    $total_amo = 0;
                    $total_valid2 = 0;
                    $total_lolos = 0;
                    $total_grand = 0;
                    @endphp
                    <tbody>
                        @foreach ($get_data as $value)
                    @php
                    $total_dapros += $value->dapros;
                    $total_aso += $value->valid1_aso;
                    $total_daman += $value->valid1_daman;
                    $total_cc += $value->valid1_cc;
                    $total_hs += $value->valid1_hs;
                    $total_amo += $value->valid1_amo;
                    $total_valid2 += $value->valid2;
                    $total_lolos += $value->lolos;
                    $total_grand += $value->grand_total;
                    @endphp
                        <tr>
                            <td style="background-color: #FBFBFB; color: black">{{ $value->mitra_amija_pt ? : '#N/A' }}</td>
                            <td style="background-color: #f1f1f1 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/DAPROS" target="_blank">{{ $value->dapros }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/ASO" target="_blank">{{ $value->valid1_aso }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/DAMAN" target="_blank">{{ $value->valid1_daman }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/CC" target="_blank">{{ $value->valid1_cc }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/HS" target="_blank">{{ $value->valid1_hs }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/AMO" target="_blank">{{ $value->valid1_amo }}</a></td>
                            <td style="background-color: #ffefce !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/VALID2" target="_blank">{{ $value->valid2 }}</a></td>
                            <td style="background-color: #eef9f4 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/LOLOS" target="_blank">{{ $value->lolos }}</a></td>
                            <td style="background-color: #f9dbfb !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/ALL" target="_blank">{{ $value->grand_total }}</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                            <td style="background-color: #c0c5ce !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/DAPROS" target="_blank">{{ $total_dapros }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/ASO" target="_blank">{{ $total_aso }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/DAMAN" target="_blank">{{ $total_daman }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/CC" target="_blank">{{ $total_cc }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/HS" target="_blank">{{ $total_hs }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/AMO" target="_blank">{{ $total_amo }}</a></td>
                            <td style="background-color: #ffcc5c !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/VALID2" target="_blank">{{ $total_valid2 }}</a></td>
                            <td style="background-color: #88d8b0 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/LOLOS" target="_blank">{{ $total_lolos }}</a></td>
                            <td style="background-color: #ec87f3 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/ALL" target="_blank">{{ $total_grand }}</a></td>
                        </tr>
                    </tbody>
                </table>
                <br /><br />
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">MITRA</th>
                        <th class="align-middle" colspan='9' style="background-color: #ff6f69 !important; color: black">VALIDASI 1</th>
                        <th class="align-middle" rowspan='2' style="background-color: #ec87f3 !important; color: black">GRAND TOTAL</th>
                        </tr>
                        <tr>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK9</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK8</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK7</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK6</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK5</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK4</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK3</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK2</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">NOK1</th>
                        </tr>
                    </thead>
                    @php
                    $nok9 = 0;
                    $nok8 = 0;
                    $nok7 = 0;
                    $nok6 = 0;
                    $nok5 = 0;
                    $nok4 = 0;
                    $nok3 = 0;
                    $nok2 = 0;
                    $nok1 = 0;
                    $total_grand = 0;
                    @endphp
                    <tbody>
                        @foreach ($get_nok as $value)
                    @php
                    $nok9 += $value->nok9;
                    $nok8 += $value->nok8;
                    $nok7 += $value->nok7;
                    $nok6 += $value->nok6;
                    $nok5 += $value->nok5;
                    $nok4 += $value->nok4;
                    $nok3 += $value->nok3;
                    $nok2 += $value->nok2;
                    $nok1 += $value->nok1;
                    $total_grand += $value->grand_total;
                    @endphp
                        <tr>
                            <td style="background-color: #FBFBFB; color: black">{{ $value->mitra_amija_pt ? : '#N/A' }}</td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK9" target="_blank">{{ $value->nok9 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK8" target="_blank">{{ $value->nok8 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK7" target="_blank">{{ $value->nok7 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK6" target="_blank">{{ $value->nok6 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK5" target="_blank">{{ $value->nok5 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK4" target="_blank">{{ $value->nok4 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK3" target="_blank">{{ $value->nok3 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK2" target="_blank">{{ $value->nok2 }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/NOK1" target="_blank">{{ $value->nok1 }}</a></td>
                            <td style="background-color: #f9dbfb !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/ALLNOK" target="_blank">{{ $value->grand_total }}</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK9" target="_blank">{{ $nok9 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK8" target="_blank">{{ $nok8 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK7" target="_blank">{{ $nok7 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK6" target="_blank">{{ $nok6 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK5" target="_blank">{{ $nok5 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK4" target="_blank">{{ $nok4 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK3" target="_blank">{{ $nok3 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK2" target="_blank">{{ $nok2 }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/NOK1" target="_blank">{{ $nok1 }}</a></td>
                            <td style="background-color: #ec87f3 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/ALLNOK" target="_blank">{{ $total_grand }}</a></td>
                        </tr>
                    </tbody>
                </table>
                <br /><br />
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th class="align-middle" rowspan='2' style="background-color: #5bc0de !important; color: black">MITRA</th>
                        <th class="align-middle" colspan='9' style="background-color: #ff6f69 !important; color: black">VALIDASI 1</th>
                        <th class="align-middle" rowspan='2' style="background-color: #ec87f3 !important; color: black">GRAND TOTAL</th>
                        </tr>
                        <tr>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO RUMAH</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO TEKNISI</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO ODP</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO REDAMAN</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO CPE LAYANAN</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO BERITA ACARA</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">TAG LOKASI</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO SURAT</th>
                        <th class="align-middle" style="background-color: #ff9a96 !important; color: black">FOTO PROFILE MYIH</th>
                        </tr>
                    </thead>
                    @php
                    $ft_rumah = 0;
                    $ft_teknisi = 0;
                    $ft_odp = 0;
                    $ft_redaman = 0;
                    $ft_cpe_layanan = 0;
                    $ft_berita_acara = 0;
                    $tg_lokasi = 0;
                    $ft_surat = 0;
                    $ft_profile_myih = 0;
                    $total = 0;
                    $total_grand = 0;
                    @endphp
                    <tbody>
                        @foreach ($get_foto as $value)
                    @php
                    $ft_rumah += $value->ft_rumah;
                    $ft_teknisi += $value->ft_teknisi;
                    $ft_odp += $value->ft_odp;
                    $ft_redaman += $value->ft_redaman;
                    $ft_cpe_layanan += $value->ft_cpe_layanan;
                    $ft_berita_acara += $value->ft_berita_acara;
                    $tg_lokasi += $value->tg_lokasi;
                    $ft_surat += $value->ft_surat;
                    $ft_profile_myih += $value->ft_profile_myih;
                    $total = ($value->ft_rumah + $value->ft_teknisi + $value->ft_odp + $value->ft_redaman + $value->ft_cpe_layanan + $value->ft_berita_acara + $value->tg_lokasi + $value->ft_surat + $value->ft_profile_myih);
                    $total_grand += $total;
                    @endphp
                        <tr>
                            <td style="background-color: #FBFBFB; color: black">{{ $value->mitra_amija_pt ? : '#N/A' }}</td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTRUMAH" target="_blank">{{ $value->ft_rumah }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTTEKNISI" target="_blank">{{ $value->ft_teknisi }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTODP" target="_blank">{{ $value->ft_odp }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTREDAMAN" target="_blank">{{ $value->ft_redaman }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTCPELAYANAN" target="_blank">{{ $value->ft_cpe_layanan }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTBERITAACARA" target="_blank">{{ $value->ft_berita_acara }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/TGLOKASI" target="_blank">{{ $value->tg_lokasi }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTSURAT" target="_blank">{{ $value->ft_surat }}</a></td>
                            <td style="background-color: #fff0f0 !important; color: black"><a style="color: black" href="/dashboard/QCBorneoMitraDetail/{{ $value->mitra_amija_pt ? : 'NA' }}/{{ $date }}/{{ $datex }}/FTPROFILEMYIH" target="_blank">{{ $value->ft_profile_myih }}</a></td>
                            <td style="background-color: #f9dbfb !important; color: black">{{ $total }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td style="background-color: #5bc0de !important; color: black; font-weight: bold">TOTAL</td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTRUMAH" target="_blank">{{ $ft_rumah }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTTEKNISI" target="_blank">{{ $ft_teknisi }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTODP" target="_blank">{{ $ft_odp }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTREDAMAN" target="_blank">{{ $ft_redaman }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTCPELAYANAN" target="_blank">{{ $ft_cpe_layanan }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTBERITAACARA" target="_blank">{{ $ft_berita_acara }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/TGLOKASI" target="_blank">{{ $tg_lokasi }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTSURAT" target="_blank">{{ $ft_surat }}</a></td>
                            <td style="background-color: #ff9a96 !important"><a style="color: black; font-weight: bold" href="/dashboard/QCBorneoMitraDetail/ALL/{{ $date }}/{{ $datex }}/FTPROFILEMYIH" target="_blank">{{ $ft_profile_myih }}</a></td>
                            <td style="background-color: #ec87f3 !important; color: black; font-weight: bold">{{ $total_grand }}</td>
                        </tr>
                    </tbody>
                </table>
                <br /><center><b>SOURCE OF <a href="http://10.128.21.37/qc-borneo-validation" target="_blank" style="color: green !important">QC BORNEO VALIDATION</a></b></center><br />
            </div>
        </div>
    </div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
    $(function(){

            $('input[name="daterange"]').daterangepicker({
                opens: 'left'
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                window.location.href = "/dashboard/QCBorneoMitra/"+start.format('YYYY-MM-DD')+"/"+end.format('YYYY-MM-DD');
            });
    })
</script>
@endsection