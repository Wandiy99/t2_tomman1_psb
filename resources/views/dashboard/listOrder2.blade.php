@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>Order {{ $status }}  SPV {{ $spv }} Sales {{ $sales }} Periode {{ $date }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Table
        </div>
        <div class="panel-body table-responsive" style="padding:0px !important">
          <table class="table">
            <tr>
              <th>#</th>
              <th>ORDER ID</th>
              <th>ORDER DATE</th>
              <th>TTI</th>
              <th>ORDER STATUS</th>
              <th>CUSTOMER</th>
              <th>K-CONTACT</th>
              <th>SALES</th>
              <th>NAMA SALES</th>
              <th>SPV</th>
              <th>DATEL</th>
              <th>ALPRONAME</th>
              <th>JENIS LAYANAN</th>
              <th>APPROVAL</th>
              <th>APPROVAL DATE</th>
              <th>DISPATCH TO</th>
              <th>LAST UPDATE</th>
              <th>LAST UPDATE DATE</th>
              <th>CATATAN</th>
              <th>ALAMAT</th>
            </tr>
            <?php
            date_default_timezone_set('Asia/Makassar');
            ?>
            @foreach ($query as $num => $result)
            <?php
              $dateStart = strtotime($result->orderDate);
              $dateEnd = strtotime(date('Y-m-d H:i:s'));
              $diff = $dateEnd - $dateStart;
              $jam = floor($diff / (60 * 60));

            ?>
            <tr>
              <td>{{ ++$num }}.</td>
              <td>{{ $result->orderId ? : $result->Ndem }}</td>
              <td>{{ $result->orderDate }}</td>
              <td>{{ $jam }}</td>
              <td>{{ $result->orderStatus }}</td>
              <td>{{ $result->customer }}</td>
              <td>{{ $result->akcontack }}</td>
              <td>{{ @$result->sales_id }}</td>
              <td>{{ @$result->NAMA_SALES }}</td>
              <td>{{ @$result->ID_SPV }}</td>
              <td>{{ $result->datel }}</td>
              <td>{{ $result->alproname }}</td>
              <td>{{ $result->jenisLayanan }}</td>
              <td>{{ $result->status_approval }}</td>
              <td>{{ $result->approve_date }}</td>
              <td>{{ $result->uraian }}</td>
              <td>{{ $result->laporan_status }}</td>
              <td>{{ $result->ls }}</td>
              <td>{{ $result->catatan }}</td>
              <td>{{ $result->alamat }} // {{ $result->alamat1 }} </td>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
