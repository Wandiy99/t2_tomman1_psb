@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <style>
  	th {
  		text-align: center;
  	}
  </style>
  <h3>KPI SEKTOR</h3>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body table-responsive">
  				<table class="table">
  					<tr>
  						<td><b>SEKTOR</b></td>
  						<th align="center">GAUL</th>
  						<th align="center">SALDO</th>
  						<th align="center">1DS</th>
  						<th align="center">3HS</th>
  						<th align="center">UNSPEC</th>
  					</tr>
  					<?php
  						$gaul = 0;
  						$saldo = 0;
  						$ttr1ds = 0;
  					?>
  					@foreach ($get_sektor as $sektor)
  					<?php 
  						$gaul += $data[$sektor->chat_id]['gaul']->jumlah;
  						$saldo += $data[$sektor->chat_id]['saldo']->jumlah;
  						$ttr1ds += $data[$sektor->chat_id]['1ds']->jumlah;
  					?>
  						<tr>
  							<td>{{ $sektor->title }}</td>
  							<td align="center">{{ $data[$sektor->chat_id]['gaul']->jumlah }}</td>
  							<td align="center">{{ $data[$sektor->chat_id]['saldo']->jumlah }}</td>
  							<td align="center">{{ $data[$sektor->chat_id]['1ds']->jumlah }}</td>
  						</tr>
  					@endforeach
  					<tr>
  						<td>Total</td>
  						<td align="center">{{ $gaul }}</td>
  						<td align="center">{{ $saldo }}</td>
  						<td align="center">{{ $ttr1ds }}</td>
  					</tr>
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection