@extends('layout')

@section('content')
  @include('partial.alerts')
  <link rel="stylesheet" href="/css/mapbox-gl.css" />
  <script src="/js/mapbox-gl.js"></script>
  <h3>Quality Control</h3>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Detail Order
        </div>
        <div class="panel-body">
          @foreach ($get_project as $project)
          <table class="table">
            <tr>
              <td width="150">SC</td>
              <td width="20">:</td>
              <td><a href="/{{ $project->id_dt }}">{{ $project->orderId ?: '-' }}</a></td>
            </tr>
            <tr>
              <td width="150">Status starclick</td>
              <td width="20">:</td>
              <td>{{ $project->orderstatus}}</td>
            </tr>
            <tr>
              <td width="150">Nama Pelanggan</td>
              <td width="20">:</td>
              <td>{{ $project->orderName}}</td>
            </tr>
            <tr>
              <td width="150">NCLI</td>
              <td width="20">:</td>
              <td>{{ $project->orderNcli }}</td>
            </tr>
            <tr>
              <td width="150">Jenis Layanan</td>
              <td width="20">:</td>
              <td>{{ $project->jenis_layanan }} / {{ $project->jenisPsb }} / {{ $project->layanan }}</td>
            </tr>
            <tr>
              <td width="150">POTS</td>
              <td width="20">:</td>
              <td>{{ $project->noTelp }}</td>
            </tr>
            <tr>
              <td width="150">Internet</td>
              <td width="20">:</td>
              <td>{{ $project->internet }}</td>
            </tr>
            <tr>
              <td width="150">STO</td>
              <td width="20">:</td>
              <td>{{ $project->sto }}</td>
            </tr>
            <tr>
              <td width="150">ODP</td>
              <td width="20">:</td>
              <td>{{ $project->alproname }}</td>
            </tr>
            <tr>
              <td width="150">QRCODE</td>
              <td width="20">:</td>
              <td>{{ $project->qrcode }}</td>
            </tr>
            <tr>
              <td width="150">ALAMAT</td>
              <td width="20">:</td>
              <?php
                $get_alamat = explode(',',$project->orderAddr);
                $alamat = '';
                if (count($get_alamat)>0){
                  $alamat .= 'Jl.'.@$get_alamat[2].' ';
                  $alamat .= 'NO.'.@$get_alamat[3].' ';
                  $alamat .= @$get_alamat[5].' ';
                  $alamat .= @$get_alamat[6].' ';
                  $alamat .= @$get_alamat[1].' ';
                  $alamat .= @$get_alamat[0].' ';
                } else {
                  $alamat = '-';
                }
              ?>
              <td>{{ $alamat }}</td>
            </tr>


            <tr>
              <td width="150">PIC</td>
              <td width="20">:</td>
              <td>{{ strtoupper($project->picPelanggan) }}</td>
            </tr>
            <tr>
              <td width="150">ALAMAT SALES</td>
              <td width="20">:</td>
              <td>{{ strtoupper($project->alamatSales) }}</td>
            </tr>
            <tr>
              <td width="150">KCONTACT</td>
              <td width="20">:</td>
              <td>{{ $project->kcontact  }}</td>
            </tr>
          </table>
          @if ($project->status_laporan == 74 && (session('auth')->level==2 || session('auth')->level==15))
          <a class="label label-success" href="/approveQC/{{ $project->id_laporan }}/{{ $project->orderId }}/{{ $project->id_regu }}">Approve</a>
          <!-- <a class="label label-danger" href="/dispatch/{{ $project->orderId }}">Reject</a> -->
          <a class="label label-danger" href="/rejectQC/{{ $project->id_dt }}/{{ $project->orderId }}/{{ $project->id_regu }}">Reject</a>
          @endif
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Map
        </div>
        <div class="panel-body">
          <div id="mapMarkerModal_mapView"></div>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          QC
        </div>
        <div class="panel-body">
          @foreach ($get_project as $project)
          <table class="table">
            <tr>
              <td>SC</td>
              <td>STATUS</td>
              <td>UNIT</td>
              <td>RUMAH</td>
              <td>TEKNISI</td>
              <td>ODP</td>
              <td>REDAMAN</td>
              <td>CPE</td>
              <td>BA</td>
              <td>TAGGING</td>
            </tr>
            <tr>
              <td>{{ $project->orderId }}</td>
              <td>{{ $project->status_qc }}</td>
              <td>{{ $project->unit }}</td>
              <td>
                {{ $project->foto_rumah }}
                {{ $project->ket_rumah }}
              </td>

              <td>
                {{ $project->foto_teknisi }}
                {{ $project->ket_teknisi }}
              </td>

              <td>
                {{ $project->foto_odp }}
                {{ $project->ket_odp }}
              </td>

              <td>
                {{ $project->foto_redaman }}
                {{ $project->ket_redaman }}
              </td>
              <td>
                {{ $project->foto_ont }}
                {{ $project->ket_ont }}
              </td>
              <td>
                {{ $project->foto_berita }}
                {{ $project->ket_berita }}
              </td>

              <td>
                {{ $project->tagging_lokasi }}
                {{ $project->ket_tagging }}
              </td>

            </tr>
          </table>
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          EVIDENCE
        </div>
        <div class="panel-body">
          @foreach ($get_project as $project)
          <?php
              $path = "/upload4/evidence/$project->id_dt";
              $path2 = "/upload3/evidence/$project->id_dt";
          ?>
          @if (file_exists(public_path().$path))
          <div class="row">
            <div class="col-sm-2">
              <a href="{{ $path }}/Lokasi.jpg"><img src="{{ $path }}/Lokasi-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path }}/Meteran_Rumah.jpg"><img src="{{ $path }}/Meteran_Rumah-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path }}/ODP.jpg"><img src="{{ $path }}/ODP-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path }}/Redaman_Pelanggan.jpg"><img src="{{ $path }}/Redaman_Pelanggan-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/SN_ONT.jpg"><img src="{{ $path }}/SN_ONT-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/SN_STB.jpg"><img src="{{ $path }}/SN_STB-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/Live_TV.jpg"><img src="{{ $path }}/Live_TV-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/Speedtest.jpg"><img src="{{ $path }}/Speedtest-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/Berita_Acara.jpg"><img src="{{ $path }}/Berita_Acara-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/Telephone_Incoming.jpg"><img src="{{ $path }}/Telephone_Incoming-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/Foto_Pelanggan_dan_Teknisi.jpg"><img src="{{ $path }}/Foto_Pelanggan_dan_Teknisi-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/additional_1.jpg"><img src="{{ $path }}/additional_1.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path }}/additional_2.jpg"><img src="{{ $path }}/additional_2.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path }}/additional_3.jpg"><img src="{{ $path }}/additional_3.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path }}/additional_4.jpg"><img src="{{ $path }}/additional_4.jpg" width="180px" height="120px" /></a>
            </div>
          </div>
            @elseif (file_exists(public_path().$path2))
            <div class="row">
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Lokasi.jpg"><img src="{{ $path2 }}/Lokasi-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path2 }}/Meteran_Rumah.jpg"><img src="{{ $path2 }}/Meteran_Rumah-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path2 }}/ODP.jpg"><img src="{{ $path2 }}/ODP-th.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path2 }}/Redaman_Pelanggan.jpg"><img src="{{ $path2 }}/Redaman_Pelanggan-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/SN_ONT.jpg"><img src="{{ $path2 }}/SN_ONT-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/SN_STB.jpg"><img src="{{ $path2 }}/SN_STB-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Live_TV.jpg"><img src="{{ $path2 }}/Live_TV-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Speedtest.jpg"><img src="{{ $path2 }}/Speedtest-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Berita_Acara.jpg"><img src="{{ $path2 }}/Berita_Acara-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Telephone_Incoming.jpg"><img src="{{ $path2 }}/Telephone_Incoming-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/Foto_Pelanggan_dan_Teknisi.jpg"><img src="{{ $path2 }}/Foto_Pelanggan_dan_Teknisi-th.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/additional_1.jpg"><img src="{{ $path2 }}/additional_1.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path2 }}/additional_2.jpg"><img src="{{ $path2 }}/additional_2.jpg" width="180px" height="120px" /></a>
            </div>

            <div class="col-sm-2">
              <a href="{{ $path2 }}/additional_3.jpg"><img src="{{ $path2 }}/additional_3.jpg" width="180px" height="120px" /></a>
            </div>
            <div class="col-sm-2">
              <a href="{{ $path2 }}/additional_4.jpg"><img src="{{ $path2 }}/additional_4.jpg" width="180px" height="120px" /></a>
            </div>
          </div>
          @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <script>
      (function (){
          var mapEl = document.getElementById('mapMarkerModal_mapView');
          mapEl.style.height = (window.innerHeight * .50) + 'px';
          <?php
          foreach ($get_project as $data){
            $get_kordinat_pelanggan = explode(',',$data->kordinat_pelanggan);
          ?>
          var center = { lng: {{ @$get_kordinat_pelanggan[1] }}, lat: {{ @$get_kordinat_pelanggan[0] }} };
          <?php
          }
          ?>
          var map;
          var marker;
          var $in;
          var $modal = $('#mapMarkerModal');
          var $title = $('#mapMarkerModal_title');
          var $latText = $('#mapMarkerModal_latText');
          var $lngText = $('#mapMarkerModal_lngText');

          var $okBtn = $('#mapMarkerModal_okBtn');

          // var setLngLat = function(ll) {
          //     marker.setLngLat(ll);
          //     map.panTo(ll);
          //
          //     $latText.html(ll.lat);
          //     $lngText.html(ll.lng);
          // }

          var getLngLat = function(text) {
              var tokens = text.split(',');
              if (tokens.length != 2) return center;

              return {
                  lat: tokens[0].trim(),
                  lng: tokens[1].trim()
              }
          }

          var initMap = function() {
              if (map) return;

              map = new mapboxgl.Map({
                  container: mapEl,
                  center: center,
                  zoom: 15,
                  style: 'https://map.tomman.app/styles/osm-liberty/style.json'
              });
              // map.on('click', function(e) { setLngLat(e.lngLat) });
              var popup = new mapboxgl.Popup({ offset: 25 }).setText(
              'Construction on the Washington Monument began in 1848.'
              );
              <?php
              foreach ($get_project as $project){
                $get_kordinat_pelanggan = explode(',',$project->kordinat_pelanggan);
              ?>
              new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'yellow' })
                  .setLngLat({lat : '{{ @$get_kordinat_pelanggan[0] }}',lon : '{{ @$get_kordinat_pelanggan[1] }}'})
                  .setPopup(popup) // sets a popup on this marker
                  .addTo(map);
              new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'red' })
                  .setLngLat({lat : '{{ $project->lat ? : 0 }}',lon : '{{ $project->lon ? : 0 }}'})
                  .addTo(map);
                  new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer' })
                      .setLngLat({lat : '{{ $project->latSC }}',lon : '{{ $project->lonSC }}'})
                      .addTo(map);
                  new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'green' })
                      .setLngLat({lat : '{{ $project->latODP }}',lon : '{{ $project->lonODP }}'})
                      .addTo(map);
              <?php
              }
              ?>
          }
          initMap();

      })()
  </script>
@endsection
