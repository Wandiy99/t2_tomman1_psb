@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/UTOnline/{{ $date }}/{{ $datex }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT UT ONLINE {{ $type }} {{ $area }} PERIODE {{ $date }} S/D {{ $datex }} UNIT {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        @if($type == 'MITRA')
                        <th>MITRA</th>
                        @elseif($type == 'SEKTOR')
                        <th>SEKTOR</th>
                        @endif
                        <th>TIM</th>
                        <th>LABORCODE</th>
                        <th>TL</th>
                        <th>STO</th>
                        <th>ORDER ID</th>
                        <th>ORDER ID (CODE)</th>
                        <th>ORDER ID (ALIAS)</th>
                        <th>CUSTOMER NAME</th>
                        <th>NO INET</th>
                        <th>TGL WO</th>
                        <th>TGL WO (MONTH)</th>
                        <th>TGL TRANSAKSI (DTM)</th>
                        <th>JENIS ORDER</th>
                        <th>LAYANAN (STARCLICK)</th>
                        <th>LAST UPDATED (DTM)</th>
                        <th>UNIT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        @if($type == 'MITRA')
                        <td>{{ $data->mitra ? : '#N/A' }}</td>
                        @elseif($type == 'SEKTOR')
                        <td>{{ $data->sektor ? : '#N/A' }}</td>
                        @endif
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        <td>{{ $data->laborCode ? : '#N/A' }}</td>
                        <td>{{ $data->TL ? : '#N/A' }}</td>
                        <td>{{ $data->sto ? : '#N/A' }}</td>
                        <td>{{ $data->scId ? : '#N/A' }}</td>
                        <td>{{ $data->scId_int ? : '#N/A' }}</td>
                        <td>{{ $data->scId_alias ? : '#N/A' }}</td>
                        <td>{{ $data->customer_desc ? : '#N/A' }}</td>
                        <td>{{ $data->noInternet ? : '#N/A' }}</td>
                        <td>{{ $data->tglWo_date ? : '#N/A' }}</td>
                        <td>{{ date('M Y', strtotime($data->tglWo_date)) ? : '#N/A' }}</td>
                        <td>{{ $data->tglTrx ? : '#N/A' }}</td>
                        <td>{{ $data->jenis_order ? : '#N/A' }}</td>
                        <td>{{ $data->jenisPsb ? : '#N/A' }}</td>
                        <td>{{ $data->last_updated_at ? : '#N/A' }}</td>
                        <td>{{ $data->statusName ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REPORT UT ONLINE TOMMAN'
                }
            ]
        });
    });
</script>
@endsection