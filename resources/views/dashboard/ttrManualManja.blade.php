@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">{{ $sektor }} DURASI {{ $durasi }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>SEKTOR</th>
                        <th>DURASI</th>
                        <th>WAKTU MANJA</th>
                        <th>INCIDENT</th>
                        <th>SERVICE NO</th>
                        <th>HEADLINE</th>
                        <th>STATUS HVC</th>
                        <th>TIM</th>
                        <th>CATATAN</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                        <th>PENYEBAB</th>
                        <th>PENYEBAB RETI</th>
                        <th>TGL DISPATCH</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($getData as $num => $data)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $data->sektor ? : 'UNDISPATCH' }}</td>
                        <td>
                            @if($data->durasi >= 0 && $data->durasi < 3)
                            < 3 JAM
                            @elseif($data->durasi >= 3 && $data->durasi < 12)
                            < 12 JAM
                            @elseif($data->durasi >= 12 && $data->durasi < 24)
                            > 12 JAM
                            @elseif($data->durasi >= 24)
                            > 1 HARI
                            @endif
                        </td>
                        <td>{{ $data->ca_manja_time }}</td>
                        <td>{{ $data->Ndem }}</td>
                        <td>{{ $data->no_speedy }}</td>
						<td>{{ $data->headline }}</td>
						<td>{{ $data->pelanggan_hvc }}</td>
                        <td>{{ $data->tim }}</td>
                        <td>{{ $data->catatan }}</td>
                        <td>{{ $data->laporan_status ? : 'ANTRIAN' }}</td>
                        <td>{{ $data->action }}</td>
                        <td>{{ $data->penyebab }}</td>
                        <td>{{ $data->status_penyebab_reti }}</td>
                        <td>{{ $data->tgl_dt }}</td>
                        <td>{{ $data->tgl_laporan }}</td>
					</tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL TTR MANUAL MANJA DISPATCH ASSURANCE'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL TTR MANUAL MANJA DISPATCH ASSURANCE'
                },
                {
                    extend: 'print',
                    title: 'DETAIL TTR MANUAL MANJA DISPATCH ASSURANCE'
                }
            ]
        });
    });
</script>
@endsection