@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Table
        </div>
        <div class="panel-body table-responsive" style="padding:0px !important">
          <table class="table">
            <tr>
              <th>#</th>
              <th>SYNC</th>
              <th>ORDER ID</th>
              <th>ORDER DATE</th>
              <th>ORDER PS</th>
              <th>QRCODE</th>
              <th>FOTO LABEL</th>

            </tr>
            <?php
            date_default_timezone_set('Asia/Makassar');
            ?>
            @foreach ($query as $num => $result)
            <?php
              $dateStart = strtotime($result->orderDate);
              $dateEnd = strtotime(date('Y-m-d H:i:s'));
              $diff = $dateEnd - $dateStart;
              $jam = floor($diff / (60 * 60));

            ?>
            <tr>
              <td>{{ ++$num }}.</td>
              <td><a href="/syncSC/{{ $result->orderId }}">SYNC</a></td>
              <td>{{ $result->orderId }}</td>
              <td>{{ $result->orderDate }}</td>
              <td>{{ $result->orderDatePs }}</td>
              <td>{{ $result->dropcore_label_code }}</td>
              <td>
                @foreach($photoInputs as $input)

                   <?php
                     $path  = "/upload/evidence/$result->id_dt/$input";
                     $path2 = "/upload2/evidence/{$result->id_dt}/$input";
                     $path3 = "/upload3/evidence/{$result->id_dt}/$input";
                     $path4 = "/upload4/evidence/{$result->id_dt}/$input";
                     $th    = "$path-th.jpg";
                     $th2   = "$path2-th.jpg";
                     $th3   = "$path3-th.jpg";
                     $th4   = "$path4-th.jpg";
                     $img   = "$path.jpg";
                     $img2  = "$path2.jpg";
                     $img3  = "$path3.jpg";
                     $img4  = "$path4.jpg";
                     $flag  = "";
                     $name  = "flag_".$input;

                   ?>
                   @if (file_exists(public_path().$th))
                     <a href="{{ $img }}">
                       <img src="{{ $th }}" alt="{{ $input }}" width="200" />
                     </a>
                     <?php
                       $flag = 2;
                     ?>
                   @elseif (file_exists(public_path().$th2))
                     <a href="{{ $img2 }}">
                       <img src="{{ $th2 }}" alt="{{ $input }}" width="200" />
                     </a>
                     <?php
                       $flag = 2;
                     ?>
                   @elseif (file_exists(public_path().$th3))
                     <a href="{{ $img3 }}">
                       <img src="{{ $th3 }}" alt="{{ $input }}" width="200" />
                     </a>
                     <?php
                       $flag = 2;
                     ?>
                   @elseif (file_exists(public_path().$th4))
                     <a href="{{ $img4 }}">
                       <img src="{{ $th4 }}" alt="{{ $input }}" width="200" />
                     </a>
                     <?php
                       $flag = 2;
                     ?>
                   @else
                     <img src="/image/placeholder.gif" width="100" alt="" />
                   @endif
                   x<p>{{ str_replace('_',' ',$input) }}</p>


               @endforeach
              </td>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
