@extends('layout')

@section('content')
  @include('partial.alerts')
 <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #DE0B1E;
      color : #ecf0f1 !important;
      text-align: center;
      vertical-align: middle;
    }
    td {
      text-align: center;
    }

    .warna1{
      background-color: #9abcf4;
    }

    .habang{
      background-color: red;
      color : white;
      font-weight: bold;
    }

    .warna2{
      background-color: #e0962f;
    }

    .warna3{
      background-color: #3ec156;
    }

    .warna4{
      background-color: #fca4a4;
    }

    .warna5{
      background-color: #f2de5e ;
    }

    .warna6{
      background-color: #e2410b;
    }

    .warna7{
      background-color: #309bff;
    }

    .text1{
      color: white;
    }

    .link:link{
      color: white;
    }

    .link:visited{
      color: white;
    }
</style>

    <div class="row">
      <div class="col-sm-12">
      <center>
        <h3><b>
        REKON MITRA TA {{ session('witel') }}<br/>
        BY ORDER DATE<br/>
        {{ $date }} - {{ $dateend }}
        </b></h3>
      </center>

      <table class="table table-hover table-bordered" style="background-color: white">
          <tr>
            <th class="align-middle" rowspan="2">MITRA</th>
            <th colspan="3">STATUS</th>
            <th class="align-middle" rowspan="2">JUMLAH PEKERJAAN</th>
          </tr>
          <tr>
            <td style="background-color: #DE0B1E; color : white !important; text-align: center; vertical-align: middle;"><font color="white">NON</font></td>
            <td style="background-color: #DE0B1E; color : white !important; text-align: center; vertical-align: middle;"><font color="white">UP</font></td>
            <td style="background-color: #DE0B1E; color : white !important; text-align: center; vertical-align: middle;"><font color="white">HR</font></td>
          </tr>
          @php
            $total_non = 0;
            $total_up = 0;
            $total_hr = 0;
            $jumlah = 0;
            $total = 0;
          @endphp
          @foreach ($rekonProvMitra as $data)
          @php
            $total_non += $data->jumlah_non;
            $total_up += $data->jumlah_up;
            $total_hr += $data->jumlah_hr;
            $jumlah = ($data->jumlah_non + $data->jumlah_up + $data->jumlah_hr);
            $total += $jumlah;
          @endphp
            <tr>
              <td><a style="color: black !important" href="/dashboard/listTimMitra/{{ $data->mitra ? : 'NONE' }}/{{ $date }}/{{ $dateend }}">{{ $data->mitra ? : 'NONE' }}</a></td>
              <td><a href="/dashboard/provisioningListMitra/{{ $data->mitra ? : 'NONE' }}/NON/{{ $date }}/{{ $dateend }}">{{ $data->jumlah_non }}</a></td>
              <td><a href="/dashboard/provisioningListMitra/{{ $data->mitra ? : 'NONE' }}/UP/{{ $date }}/{{ $dateend }}">{{ $data->jumlah_up }}</a></td>
              <td><a href="/dashboard/provisioningListMitra/{{ $data->mitra ? : 'NONE' }}/HR/{{ $date }}/{{ $dateend }}">{{ $data->jumlah_hr }}</a></td>
              <td><a href="/dashboard/provisioningListMitra/{{ $data->mitra ? : 'NONE' }}/ALL/{{ $date }}/{{ $dateend }}">{{ $jumlah }}</a></td>
            </tr>
          @endforeach
          <tr>
            <td><b>TOTAL</b></td>
            <td><a href="/dashboard/provisioningListMitra/ALL/NON/{{ $date }}/{{ $dateend }}">{{ $total_non }}</a></td>
            <td><a href="/dashboard/provisioningListMitra/ALL/UP/{{ $date }}/{{ $dateend }}">{{ $total_up }}</a></td>
            <td><a href="/dashboard/provisioningListMitra/ALL/HR/{{ $date }}/{{ $dateend }}">{{ $total_hr }}</a></td>
            <td><b>{{ $total }}</b></td>
          </tr>
        </table>
        <br /><br />

        <table class="table table-hover table-bordered" style="background-color: white">
          <tr>
            <th>MITRA</th>
            <th>MIGRASI</th>
            <th>ASSURANCE</th>
            <th>JUMLAH PEKERJAAN</th>
          </tr>
          @php
            $total_migrasi = 0;
            $total_asr = 0;
            $jumlah = 0;
            $total = 0;
          @endphp
          @foreach ($rekonMitraData as $data)
          @php
            $total_migrasi += $data->jumlah_migrasi;
            $total_asr += $data->jumlah_asr;
            $jumlah = ($data->jumlah_migrasi + $data->jumlah_asr);
            $total += $jumlah;
          @endphp
            <tr>
              <td>{{ $data->mitra ? : 'NONE' }}</td>
              <td><a href="/dashboard/migrasiListMitra/{{ $data->mitra ? : 'NONE' }}/{{ $date }}">{{ $data->jumlah_migrasi }}</a></td>
              <td><a href="/dashboard/assuranceListMitra/{{ $data->mitra ? : 'NONE' }}/{{ $dateend }}">{{ $data->jumlah_asr }}</a></td>
              <td>{{ $jumlah }}</a></td>
            </tr>
          @endforeach
          <tr>
            <td><b>TOTAL</b></td>
            <td><a href="/dashboard/migrasiListMitra/ALL/{{ $date }}">{{ $total_migrasi }}</a></td>
            <td><a href="/dashboard/assuranceListMitra/ALL/{{ $dateend }}">{{ $total_asr }}</a></td>
            <td><b>{{ $total }}</b></td>
          </tr>
        </table>


      </div>
    </div>
    	<!-- <div id="col-sm-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				Hasil QC {{ $date }} {{ $dateend }}
    			</div>
    			<div class="table-responsive">
    				<table class="table">
    					<thead>
    						<tr>
    							<th class="align-middle" rowspan="2">MITRA</th>
    							<th class="align-middle" rowspan="2">JML_QC</th>
    							<th colspan="2">RUMAH</th>
    							<th colspan="2">TEKNISI</th>
    							<th colspan="2">ODP</th>
    							<th colspan="2">REDAMAN</th>
    							<th colspan="2">ONT</th>
    							<th colspan="2">BERITA ACARA</th>
                  <th colspan="2">TAGGING</th>
                </tr>
    						<tr>
    							<th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
                  <th>OK</th>
    							<th>NOK</th>
    						</tr>
    					</thead>
    					<tbody>
    					@php
	    					$jml_foto_berita_ok = 0;
	    					$jml_foto_berita_nok = 0;
                $jml_foto_rumah_ok = 0;
                $jml_foto_rumah_nok = 0;
                $jml_foto_teknisi_ok = 0;
                $jml_foto_teknisi_nok = 0;
                $jml_foto_odp_ok = 0;
                $jml_foto_odp_nok = 0;
                $jml_foto_redaman_ok = 0;
                $jml_foto_redaman_nok = 0;
                $jml_foto_ont_ok = 0;
                $jml_foto_ont_nok = 0;
                $jml_tagging_pelanggan_ok = 0;
                $jml_tagging_pelanggan_nok = 0;
	    					$total = 0;
	    				@endphp
    					@foreach ($get_qc_borneo as $qc_borneo)
    					@php
	    					$foto_berita_nok = $qc_borneo->jumlah-$qc_borneo->foto_berita_ok;
	    					$foto_rumah_nok = $qc_borneo->jumlah-$qc_borneo->foto_rumah_ok;
	    					$foto_teknisi_nok = $qc_borneo->jumlah-$qc_borneo->foto_teknisi_ok;
	    					$foto_odp_nok = $qc_borneo->jumlah-$qc_borneo->foto_odp_ok;
	    					$foto_redaman_nok = $qc_borneo->jumlah-$qc_borneo->foto_redaman_ok;
	    					$foto_ont_nok = $qc_borneo->jumlah-$qc_borneo->foto_ont_ok;
	    					$tagging_pelanggan_nok = $qc_borneo->jumlah-$qc_borneo->tagging_pelanggan_ok;
    					@endphp
    						<tr>
    							<td>{{ $qc_borneo->mitra }}</td>
    							<td>{{ $qc_borneo->jumlah }}</td>
                  <td>{{ $qc_borneo->foto_rumah_ok }}</td>
    							<td class="habang">{{ $foto_rumah_nok }}</td>
                  <td>{{ $qc_borneo->foto_teknisi_ok }}</td>
                  <td class="habang">{{ $foto_teknisi_nok }}</td>
                  <td>{{ $qc_borneo->foto_odp_ok }}</td>
                  <td class="habang">{{ $foto_odp_nok }}</td>
                  <td>{{ $qc_borneo->foto_redaman_ok }}</td>
                  <td class="habang">{{ $foto_redaman_nok }}</td>
                  <td>{{ $qc_borneo->foto_ont_ok }}</td>
                  <td class="habang">{{ $foto_ont_nok }}</td>
                  <td>{{ $qc_borneo->foto_berita_ok }}</td>
    							<td class="habang">{{ $foto_berita_nok }}</td>
                  <td>{{ $qc_borneo->tagging_pelanggan_ok }}</td>
                  <td class="habang">{{ $tagging_pelanggan_nok }}</td>
              </tr>
    					@php
	    					$total += $qc_borneo->jumlah;
	    					$jml_foto_rumah_ok += $qc_borneo->foto_rumah_ok;
	    					$jml_foto_rumah_nok += $foto_rumah_nok;
                $jml_foto_teknisi_ok += $qc_borneo->foto_teknisi_ok;
	    					$jml_foto_teknisi_nok += $foto_teknisi_nok;
                $jml_foto_odp_ok += $qc_borneo->foto_odp_ok;
	    					$jml_foto_odp_nok += $foto_odp_nok;
                $jml_foto_redaman_ok += $qc_borneo->foto_redaman_ok;
	    					$jml_foto_redaman_nok += $foto_redaman_nok;
                $jml_foto_ont_ok += $qc_borneo->foto_ont_ok;
	    					$jml_foto_ont_nok += $foto_ont_nok;
                $jml_foto_berita_ok += $qc_borneo->foto_berita_ok;
	    					$jml_foto_berita_nok += $foto_berita_nok;
                $jml_tagging_pelanggan_ok += $qc_borneo->tagging_pelanggan_ok;
	    					$jml_tagging_pelanggan_nok += $tagging_pelanggan_nok;
	    				@endphp
    					@endforeach
    					<tfoot>
    						<tr>
    							<td >TOTAL</td>
    							<td>{{ $total }}</td>
    							<td>{{ $jml_foto_rumah_ok }}</td>
    							<td class="habang">{{ $jml_foto_rumah_nok }}</td>
                  <td>{{ $jml_foto_teknisi_ok }}</td>
    							<td class="habang">{{ $jml_foto_teknisi_nok }}</td>
                  <td>{{ $jml_foto_odp_ok }}</td>
    							<td class="habang">{{ $jml_foto_odp_nok }}</td>
                  <td>{{ $jml_foto_redaman_ok }}</td>
    							<td class="habang">{{ $jml_foto_redaman_nok }}</td>
                  <td>{{ $jml_foto_ont_ok }}</td>
    							<td class="habang">{{ $jml_foto_ont_nok }}</td>
                  <td>{{ $jml_foto_berita_ok }}</td>
    							<td class="habang">{{ $jml_foto_berita_nok }}</td>
                  <td >{{ $jml_tagging_pelanggan_ok }}</td>
    							<td class="habang">{{ $jml_tagging_pelanggan_nok }}</td>
    						</tr>
    					</tfoot>
    					</tbody>
    				</table>
    			</div>
    		</div>
    </div> -->
@endsection
