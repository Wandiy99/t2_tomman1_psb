@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />

  <h3>Dashboard Trend PI</h3>
  <div class="row">
    <div class="col-sm-12">
      <form method="get">
        Transaksi : 
        <select name="transaksi" id="transaksi">
          <option value="ALL" <?php if ($transaksi=='ALL') echo 'selected="selected"'; ?>>ALL</option>
          <option value="NEWSALES" <?php if ($transaksi=='NEWSALES') echo 'selected="selected"'; ?>>NEW SALES</option>
          <option value="2NDSTB" <?php if ($transaksi=='2NDSTB') echo 'selected="selected"'; ?>>2ND STB</option>
          <option value="MO" <?php if ($transaksi=='MO') echo 'selected="selected"'; ?>>ADD ON</option>
        </select>
        Mulai Tanggal :
        <input type="text" name="dateStart" id="input-dateStart" value="{{ $dateStart }} ">
        Sampai Tanggal :
        <input type="text" name="dateEnd" id="input-dateEnd" value="{{ $dateEnd }}">
        <input type="submit" />
      </form>
      <br />
      <div class="table-responsive">
        <script type="text/javascript">
          jQuery(document).ready(function($)
          {
            $("#bar-11").dxChart({
              title: 'Trend PI FZ Kalimantan 2',
              label : {
                visible : true
              },
              argumentAxis: {
                  valueMarginsEnabled: false,
                  discreteAxisDivisionMode: "crossLabels",
                  grid: {
                      visible: true
                  }
              },
              tooltip: {
                  enabled: true,
                  customizeTooltip: function (arg) {
                      return {
                          text: arg.valueText
                      };
                  }
              },
              dataSource: [
                 @foreach ($query as $result)
                  @for ($i=0;$i<24;$i++)
                  { day : "{{ $i }}", {{ $result->WITEL }} : {{ $result->{'h'.$i} }}},
                  @endfor
                 @endforeach
              ],
              series: [
              {
                argumentField : "day",
                valueField: "KALTENG",
                name : "KALTENG",
                type : "area",
                color: '#3498db' 
              },{
                argumentField : "day",
                valueField: "KALSEL",
                name : "KALSEL",
                type : "area",
                color: '#2ecc71'
              },
              {
                argumentField : "day",
                valueField: "KALBAR",
                name : "KALBAR",
                type : "area",
                color: '#00cec9'
              },
              ]
            });
          });
        </script>
        <div id="bar-11" style="height: 290px; width: 100%;"></div>
        </div>
    </div>
    <div class="col-sm-12">
      
      <br />
      <br />
      <div class="table-responsive">
      <table class="table styledTable">
        <tr>
          <th rowspan="2">Area</th>
          <th colspan="24">Jam</th>
          <th rowspan="2">ALL</th>
        </tr>
        <tr>
          @for ($i=0;$i<24;$i++)
          <th>{{ $i }}</th>
          @endfor
        </tr>
        <?php 
          $jumlah = array();
          $totaljumlah = 0;
        ?>
        @foreach ($query as $result)
        <tr>
          <td class="areatext">{{ $result->WITEL }}</td>
          @for ($i=0;$i<24;$i++)
          <?php
            $jumlah[$result->WITEL][$i] =  $result->{'h'.$i};
          ?>
          <td width="40"><a href="/dashboard/PIlist/{{ $result->WITEL }}/{{ $i }}?transaksi={{ $transaksi }}&dateStart={{ $dateStart }}&dateEnd={{ $dateEnd }}">{{ $result->{'h'.$i} }}</a></td>
          @endfor
          <td><a href="/dashboard/PIlist/{{ $result->WITEL }}/ALL?transaksi={{ $transaksi }}&dateStart={{ $dateStart }}&dateEnd={{ $dateEnd }}">{{ $result->jumlah }}</td>
          <?php 
          $totaljumlah += $result->jumlah;
          ?>
        </tr>
        @endforeach
        <?php 
          $total = array();
          for($x=0;$x<24;$x++) : 
          $total[$x] = $jumlah['KALSEL'][$x]+$jumlah['KALBAR'][$x]+$jumlah['KALTENG'][$x];
          endfor;

        ?>
        <tr>
          <th>TOTAL</th>
          @for ($y=0;$y<24;$y++)
          <td><a href="/dashboard/PIlist/ALL/{{ $y }}?transaksi={{ $transaksi }}&dateStart={{ $dateStart }}&dateEnd={{ $dateEnd }}">{{ $total[$y] }}</a></td>
          @endfor
          <td><a href="/dashboard/PIlist/ALL/ALL?transaksi={{ $transaksi }}&dateStart={{ $dateStart }}&dateEnd={{ $dateEnd }}">{{ $totaljumlah }}</a></td>
        </tr>
        </table>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

      $('#input-dateStart').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });
      $('#input-dateEnd').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

    });
  </script>
@endsection
