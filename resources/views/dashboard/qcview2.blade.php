@extends('layout')

@section('content')
  @include('partial.alerts')
  <link rel="stylesheet" href="/css/mapbox-gl.css" />
  <script src="/js/mapbox-gl.js"></script>
  <h3>Quality Control</h3>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Detail Order
        </div>
        <div class="panel-body">
          @foreach ($get_project as $project)
          <table class="table">
            <tr>
              <td width="150">ORDER CODE</td>
              <td width="20">:</td>
              <td><a href="/{{ $project->id_dt }}">{{ $sc }}</a></td>
            </tr>
            <tr>
              <td width="150">STATUS SCBE</td>
              <td width="20">:</td>
              <td>{{ $project->orderstatus}}</td>
            </tr>
            <tr>
              <td width="150">CUST NAME</td>
              <td width="20">:</td>
              <td>{{ $project->orderName}}</td>
            </tr>
            <tr>
              <td width="150">NCLI</td>
              <td width="20">:</td>
              <td>{{ $project->orderNcli }}</td>
            </tr>
            <tr>
              <td width="150">PACKAGE NAME</td>
              <td width="20">:</td>
              <td>{{ $project->jenis_layanan }} / {{ $project->jenisPsb }} / {{ $project->layanan }}</td>
            </tr>
            <tr>
              <td width="150">POTS</td>
              <td width="20">:</td>
              <td>{{ $project->noTelp }}</td>
            </tr>
            <tr>
              <td width="150">INTERNET</td>
              <td width="20">:</td>
              <td>{{ $project->internet }}</td>
            </tr>
            <tr>
              <td width="150">STO</td>
              <td width="20">:</td>
              <td>{{ $project->sto }}</td>
            </tr>
            <tr>
              <td width="150">ODP</td>
              <td width="20">:</td>
              <td>{{ $project->alproname }}</td>
            </tr>
            <tr>
              <td width="150">ALAMAT</td>
              <td width="20">:</td>
              <?php
                $get_alamat = explode(',',$project->orderAddr);
                $alamat = '';
                if (count($get_alamat)>0){
                  $alamat .= 'Jl.'.@$get_alamat[2].' ';
                  $alamat .= 'NO.'.@$get_alamat[3].' ';
                  $alamat .= @$get_alamat[5].' ';
                  $alamat .= @$get_alamat[6].' ';
                  $alamat .= @$get_alamat[1].' ';
                  $alamat .= @$get_alamat[0].' ';
                } else {
                  $alamat = '-';
                }
              ?>
              <td>{{ $alamat }}</td>
            </tr>


            <tr>
              <td width="150">PIC</td>
              <td width="20">:</td>
              <td>{{ strtoupper($project->picPelanggan) }}</td>
            </tr>
            <tr>
              <td width="150">ALAMAT SALES</td>
              <td width="20">:</td>
              <td>{{ strtoupper($project->alamatSales) }}</td>
            </tr>
            <tr>
              <td width="150">KCONTACT</td>
              <td width="20">:</td>
              <td>{{ $project->kcontact  }}</td>
            </tr>
          </table>
          @if ($project->status_laporan == 74 && (session('auth')->level==2 || session('auth')->level==15))
          <a class="label label-success" href="/approveQC/{{ $project->id_laporan }}/{{ $project->orderId }}/{{ $project->id_regu }}">Approve</a>
          <!-- <a class="label label-danger" href="/dispatch/{{ $project->orderId }}">Reject</a> -->
          <a class="label label-danger" href="/rejectQC/{{ $project->id_dt }}/{{ $project->orderId }}/{{ $project->id_regu }}">Reject</a>
          @endif
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Map
        </div>
        <div class="panel-body">
          <div id="mapMarkerModal_mapView"></div>
        </div>
      </div>
    </div>

    </div>

  </div>
  <script>
      (function (){
          var mapEl = document.getElementById('mapMarkerModal_mapView');
          mapEl.style.height = (window.innerHeight * .50) + 'px';
          <?php
          foreach ($get_project as $data){
            $get_kordinat_pelanggan = explode(',',$data->koorPelanggan);
          ?>
          var center = { lng: {{ @$get_kordinat_pelanggan[1] }}, lat: {{ @$get_kordinat_pelanggan[0] }} };
          <?php
          }
          ?>
          var map;
          var marker;
          var $in;
          var $modal = $('#mapMarkerModal');
          var $title = $('#mapMarkerModal_title');
          var $latText = $('#mapMarkerModal_latText');
          var $lngText = $('#mapMarkerModal_lngText');

          var $okBtn = $('#mapMarkerModal_okBtn');

          // var setLngLat = function(ll) {
          //     marker.setLngLat(ll);
          //     map.panTo(ll);
          //
          //     $latText.html(ll.lat);
          //     $lngText.html(ll.lng);
          // }

          var getLngLat = function(text) {
              var tokens = text.split(',');
              if (tokens.length != 2) return center;

              return {
                  lat: tokens[0].trim(),
                  lng: tokens[1].trim()
              }
          }

          var initMap = function() {
              if (map) return;

              map = new mapboxgl.Map({
                  container: mapEl,
                  center: center,
                  zoom: 18,
                  style: 'https://map.tomman.app/styles/osm-liberty/style.json'
              });
              // map.on('click', function(e) { setLngLat(e.lngLat) });
              var popup = new mapboxgl.Popup({ offset: 25 }).setText(
              'Construction on the Washington Monument began in 1848.'
              );
              <?php
              foreach ($get_project as $project){
                $get_kordinat_pelanggan = explode(',',$project->koorPelanggan);
              ?>
              new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'yellow' })
                  .setLngLat({lat : '{{ @$get_kordinat_pelanggan[0] }}',lon : '{{ @$get_kordinat_pelanggan[1] }}'})
                  .setPopup(popup) // sets a popup on this marker
                  .addTo(map);
              new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'red' })
                  .setLngLat({lat : '{{ $project->lat ? : 0 }}',lon : '{{ $project->lon ? : 0 }}'})
                  .addTo(map);
                  new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer' })
                      .setLngLat({lat : '{{ $project->latSC }}',lon : '{{ $project->lonSC }}'})
                      .addTo(map);
                  new mapboxgl.Marker({ draggable: false, icon:'default', cursor:'pointer',color:'green' })
                      .setLngLat({lat : '{{ $project->latODP }}',lon : '{{ $project->lonODP }}'})
                      .addTo(map);
              <?php
              }
              ?>
          }
          initMap();

      })()
  </script>
@endsection
