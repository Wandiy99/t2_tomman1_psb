@extends('public_layout_2')
@section('content')
<style>
    div {
        width: max-content;
        display: inline-block;
    }
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
      color: black;
    }
    td {
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
      color: black;
    }
    .warna1{
      background-color: #3498DB;
    }
    .warna2{
      background-color: #85C1E9;
    }
    .warna3{
      background-color: #D6EAF8;
    }
    .text2{
      color: black;
    }
    .text1{
      color: white !important;
    }
    .merahx{
      background-color: #C0392B;
      color: white !important;
    }
    .hijaux{
      background-color:#1ABC9C;
      color: white !important;
    }
    .autowidth{
        width: 1200px;
        max-width: calc(100% - 20px);
        margin: 0 auto;
        /* padding: 0 10px; */
    }
</style>
<br />
<div class="col-sm-12 autowidth">
<br />
<h3 style="font-weight: bold; text-align: center;">PS / RE [ Periode {{ date('Y-m') }} ]</h3>
<p class="text-muted" style="float: left;">Sumber Data : Selfi K-PRO</p>
<p class="text-muted" style="float: right;">Last Updated {{ $log_psre->LAST_SYNC }} WITA</p>
    <table class="table table-striped table-bordered dataTable">
        <tr>
        <th class="align-middle warna1 text1" rowspan="2">SEKTOR</th>
        <th class="align-middle warna1 text1" colspan="6">STATUS</th>
        <th class="align-middle warna1 text1" rowspan="2">GRAND TOTAL</th>
        <th class="align-middle warna1 text1" colspan="7">ACH %</th>
        </tr>
        <tr>
        <th class="align-middle text1" style="background-color: #C0392B">CANCEL</th>
        <th class="align-middle warna1 text1">OGP</th>
        <th class="align-middle text1">PRA PI</th>
        <th class="align-middle warna1 text1" style="background-color: #1ABC9C">PS</th>
        <th class="align-middle text1" style="background-color: #E67E22">REVOKE</th>
        <th class="align-middle text1" style="background-color: #8E44AD">UNSC</th>
        <th class="align-middle text1" style="background-color: #C0392B">CANCEL</th>
        <th class="align-middle warna1 text1">OGP</th>
        <th class="align-middle text1">PRA PI</th>
        <th class="align-middle warna1 text1" style="background-color: #1ABC9C">PS/RE</th>
        <th class="align-middle text1" style="background-color: #E67E22">REVOKE</th>
        <th class="align-middle text1" style="background-color: #8E44AD">UNSC</th>
        <th class="align-middle warna1 text1">PS/WO</th>
        </tr>
        @php
            $total_cancel = $total_prapi = $total_ogp = $total_ps = $total_revoke = $total_unsc = $jumlah = $round_cancel = $round_ogp = $round_ps = $grand_total = 0;
        @endphp
        @foreach ($data as $num => $result)
        @php
        $total_cancel += $result->cancel;
        $total_prapi += $result->prapi;
        $total_ogp += $result->ogp;
        $total_ps += $result->ps;
        $total_revoke += $result->revoke_comp;
        $total_unsc += $result->unsc;
        $jumlah = $result->cancel + $result->prapi + $result->ogp + $result->ps + $result->revoke_comp + $result->unsc;
        $round_cancel = @round($result->cancel / $jumlah * 100,2);
        $round_ogp = @round($result->ogp / $jumlah * 100,2);
        $round_ps = @round($result->ps / $jumlah * 100,2);
        $grand_total += $jumlah;
        @endphp
        <tr>
        <td>{{ $result->sektor_prov ? : 'NONE SEKTOR' }}</td>
        <td>{{ $result->cancel }}</td>
        <td>{{ $result->ogp }}</td>
        <td>{{ $result->prapi }}</td>
        <td>{{ $result->ps }}</td>
        <td>{{ $result->revoke_comp }}</td>
        <td>{{ $result->unsc }}</td>
        <td>{{ $jumlah }}</td>
        <?php if($round_cancel >= 10) { $bg_cancel = 'class=merahx'; } else { $bg_cancel = ""; };  ?>
        <td {{ $bg_cancel }}>{{ $round_cancel }} %</td>
        <?php if($round_ogp >= 2) { $bg_ogp = 'class=merahx'; } else { $bg_ogp = ""; };  ?>
        <td {{ $bg_ogp }}>{{ $round_ogp }} %</td>
        <td>{{ @round($result->prapi / $jumlah * 100,2) }} %</td>
        <?php if($round_ps >= 90) { $bg_ps = 'class=hijaux'; } else { $bg_ps = ""; };  ?>
        <td {{ $bg_ps }}>{{ $round_ps }} %</td>
        <td>{{ @round($result->revoke_comp / $jumlah * 100,2) }} %</td>
        <td>{{ @round($result->unsc / $jumlah * 100,2) }} %</td>
        <td>{{ @round($result->ps / ($result->ogp + $result->ps) * 100,2) }} %</td>
        </tr>
        @endforeach
        <tr>
        <th class="warna2 text2">TOTAL</th>
        <th style="background-color: #D98880">{{ $total_cancel }}</th>
        <th class="warna2">{{ $total_ogp }}</th>
        <th>{{ $total_prapi }}</th>
        <th class="warna2" style="background-color: #A3E4D7">{{ $total_ps }}</th>
        <th class="text2" style="background-color: #F5CBA7">{{ $total_revoke }}</th>
        <th class="text2" style="background-color: #BB8FCE">{{ $total_unsc }}</th>
        <th class="warna2 text2">{{ $grand_total }}</th>

        <th class="text2" style="background-color: #D98880">{{ @round($total_cancel / $grand_total * 100,2) }} %</th>
        <th class="warna2 text2">{{ @round($total_ogp / $grand_total * 100,2) }} %</th>
        <th class="text2">{{ @round($total_prapi / $grand_total * 100,2) }} %</th>
        <th class="warna2 text2" style="background-color: #A3E4D7">{{ @round($total_ps / $grand_total * 100,2) }} %</th>
        <th class="text2" style="background-color: #F5CBA7">{{ @round($total_revoke / $grand_total * 100,2) }} %</th>
        <th class="text2" style="background-color: #BB8FCE">{{ @round($total_unsc / $grand_total * 100,2) }} %</th>
        <th class="warna2 text2">{{ @round($total_ps / ($total_ogp + $total_ps) * 100,2) }} %</th>
        </tr>
    </table>
    <br />
</div>
@endsection