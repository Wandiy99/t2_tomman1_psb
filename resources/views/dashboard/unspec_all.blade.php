@extends('layout')

@section('content')
  @include('partial.alerts')
<style media="screen">
  .navbar-brand {
    font-size: 14px;
    font-weight: bold;
  }
 
  .redxxx {
    color : #e74c3c;
  }
  </style>
  <center>
    <h3><b>DASHBOARD UNDERSPEC BY <span class="redxxx">TOM</span>MAN</a><br />PERIODE {{ $date }}</b></h3>
  </center>
    <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          PROVISIONING
        </div>
        <div class="panel-body">
          <table class="table table-stripped">
            <tr>
              <th>RANK</th>
              <th>SEKTOR</th>
              <th>STO</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $total_prov = 0;
            ?>
            @foreach ($unspec_prov as $rank => $result_prov)
            <?php
              $total_prov += $result_prov->JUMLAH;
            ?>
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $result_prov->SEKTOR }}</td>
              <td>{{ $result_prov->STO }}</td>
              <td><!-- <a href="/dashboard/new/unspeclist/prov/{{ $result_prov->SEKTOR }}/{{ $date }}"> -->{{ $result_prov->JUMLAH }}<!-- </a> --></td>
            </tr>
            @endforeach
            <tr>
              <th colspan="3">TOTAL</th>
              <th><a href="/dashboard/new/unspeclist/prov/ALL/{{ $date }}">{{ $total_prov }}</a></th>
            </tr>
          </table>
        </div>
      </div>
    </div>


    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          ASSURANCE
        </div>
        <div class="panel-body">
          <table class="table table-stripped">
            <tr>
              <th>RANK</th>
              <th>SEKTOR</th>
              <th>STO</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $total_asr = 0;
            ?>
            @foreach ($unspec_asr as $rank => $result_asr)
            <?php
              $total_asr += $result_asr->JUMLAH;
            ?>
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $result_asr->SEKTOR }}</td>
              <td>{{ $result_asr->STO }}</td>
              <td><!-- <a href="/dashboard/new/unspeclist/asr/{{ $result_asr->SEKTOR }}/{{ $date }}"> -->{{ $result_asr->JUMLAH }}<!-- </a> --></td>
            </tr>
            @endforeach
            <tr>
              <th colspan="3">TOTAL</th>
              <th><a href="/dashboard/new/unspeclist/asr/ALL/{{ $date }}">{{ $total_asr }}</a></th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- LOLOS LAPORAN TANPA UKUR IBOOSTER -->
    <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          PROVISIONING (REDAMAN NULL)
        </div>
        <div class="panel-body">
          <table class="table table-stripped">
            <tr>
              <th>RANK</th>
              <th>SEKTOR</th>
              <th>STO</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $total_prov_null = 0;
            ?>
            @foreach ($unspec_prov_null as $rank => $result_prov_null)
            <?php
              $total_prov_null += $result_prov_null->JUMLAH;
            ?>
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $result_prov_null->SEKTOR }}</td>
              <td>{{ $result_prov_null->STO }}</td>
              <td><!-- <a href="/dashboard/new/unspeclist/prov/null/{{ $result_prov_null->SEKTOR }}/{{ $date }}"> -->{{ $result_prov_null->JUMLAH }}<!-- </a> --></td>
            </tr>
            @endforeach
            <tr>
              <th colspan="3">TOTAL</th>
              <th><a href="/dashboard/new/unspeclist/prov/null/ALL/{{ $date }}">{{ $total_prov_null }}</a></th>
            </tr>
          </table>
        </div>
      </div>
    </div>


    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading" >
          ASSURANCE (REDAMAN NULL)
        </div>
        <div class="panel-body">
          <table class="table table-stripped">
            <tr>
              <th>RANK</th>
              <th>SEKTOR</th>
              <th>STO</th>
              <th>JUMLAH</th>
            </tr>
            <?php
              $total_asr_null = 0;
            ?>
            @foreach ($unspec_asr_null as $rank => $result_asr_null)
            <?php
              $total_asr_null += $result_asr_null->JUMLAH;
            ?>
            <tr>
              <td>{{ ++$rank }}</td>
              <td>{{ $result_asr_null->SEKTOR }}</td>
              <td>{{ $result_asr_null->STO }}</td>
              <td><!-- <a href="/dashboard/new/unspeclist/asr/null/{{ $result_asr_null->SEKTOR }}/{{ $date }}"> -->{{ $result_asr_null->JUMLAH }}<!-- </a> --></td>
            </tr>
            @endforeach
            <tr>
              <th colspan="3">TOTAL</th>
              <th><a href="/dashboard/new/unspeclist/asr/null/ALL/{{ $date }}">{{ $total_asr_null }}</a></th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection