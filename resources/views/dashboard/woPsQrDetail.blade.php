@extends('layout')

@section('content')
  @include('partial.alerts')

  <div class="panel panel-primary">
      <div class="panel-heading">Dashboard PS Detail</div>
      <div class="panel-body">
          <table class="table table-bordered">
              <tr>
                  <th>No</th>
                  <th>Sektor</th>
                  <th>Regu</th>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>No Order</th>
                  <th>NCLI</th>
                  <th>No Internet</th>
                  <th>ODP Teknisi</th>
                  <th>ODP Starclick</th>
                  <th>ODP Sales</th>
                  <th>Port ODP</th>
                  <th>SN ONT</th>
                  <th>Order Status</th>
                  <th>Jenis PSB</th>
                  <th>Qrcode</th>
                  <th>Tanggal PS</th>
                  <th>Tanggal UP Teknisi</th>
                  <th>Status Laporan</th>
              </tr>

              @foreach($data as $no=>$dt)
                  <tr>
                      <td>{{ ++$no }}</td>
                      <td>{{ $dt->title }}</td>
                      <td>{{ $dt->uraian }}</td>
                      <td>{{ $dt->datel }}</td>
                      <td>{{ $dt->sto }}</td>
                      <td><a href="/{{ $dt->id_dt }}">{{ $dt->orderId }}</a></td>
                      <td>{{ $dt->orderNcli }}</td>
                      <td>{{ $dt->internet }}</td>
                      <td>{{ $dt->nama_odp }}</td>
                      <td>{{ $dt->alproname }}</td>
                      <td>{{ $dt->namaOdp}}</td>
                      <td>{{ $dt->port_number }}</td>
                      <td>{{ $dt->snont }}</td>
                      <td>{{ $dt->orderStatus }}</td>
                      <td>{{ $dt->jenisPsb }}</td>
                      <td>{{ $dt->dropcore_label_code }}</td>
                      <td>{{ $dt->orderDatePs }}</td>
                      <td>{{ $dt->tglUp }}</td>
                      <td>{{ $dt->laporan_status }}</td>

                  </tr>
              @endforeach
          </table>
      </div>

  </div>

@endsection
