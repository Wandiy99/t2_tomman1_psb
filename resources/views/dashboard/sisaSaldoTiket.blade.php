@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">{{ $ioan }} DURASI {{ $durasi }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>IOAN</th>
                        <th>DURASI</th>
                        <th>INCIDENT</th>
                        <th>CUSTOMER NAME</th>
                        <th>STATUS</th>
                        <th>SERVICE NO</th>
                        <th>CONTACT PHONE</th>
                        <th>SUMMARY</th>
                        <th>SERVICE TYPE</th>
                        <th>BOOKING DATE</th>
                        <th>ASSIGNED BY</th>
                        <th>STATUS</th>
                        <th>CUSTOMER TYPE</th>
                        <th>DATEK</th>
                        <th>WORKZONE</th>
                        <th>INDUK GAMAS</th>
                        <th>REPORTED DATE</th>
                        <th>INCIDENT SYMPTOM</th>
                        <th>TIM</th>
                        <th>CATATAN</th>
                        <th>ACTION</th>
                        <th>PENYEBAB</th>
                        <th>PENYEBAB RETI</th>
                        <th>HASIL UKUR DISPATCH</th>
                        <th>HASIL UKUR LAPORAN</th>
                        <th>HASIL UKUR TERBARU</th>
                        <th>TGL DISPATCH</th>
                        <th>TGL LAPORAN</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($getData as $num => $data)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $data->ioan ? : 'UNDISPATCH' }}</td>
                        <td>
                            @if($data->durasi >= 2 && $data->durasi < 3)
                            WNOK
                            @elseif ($data->durasi >= 0 && $data->durasi < 1)
                            < 1 JAM
                            @elseif ($data->durasi >= 1 && $data->durasi < 2)
                            < 2 JAM
                            @elseif ($data->durasi >= 2 && $data->durasi < 3)
                            < 3 JAM
                            @elseif ($data->durasi >= 3 && $data->durasi < 12)
                            < 12 JAM
                            @elseif ($data->durasi >= 12 && $data->durasi < 24)
                            > 12 JAM
                            @elseif ($data->durasi >= 24)
                            > 1 HARI
                            @endif
                        </td>
                        <td>{{ $data->Incident }}</td>
                        <td>{{ $data->Customer_Name }}</td>
                        <td>{{ $data->laporan_status ? : 'ANTRIAN' }}</td>
                        <td>{{ $data->Service_No }}</td>
						<td>{{ $data->Contact_Phone }}</td>
						<td>{{ $data->Summary }}</td>
                        <td>{{ $data->Service_Type }}</td>
                        <td>{{ $data->Booking_Date }}</td>
                        <td>{{ $data->Assigned_by }}</td>
						<td>{{ $data->Status }}</td>
                        <td>{{ $data->Customer_Type }}</td>
                        <td>{{ $data->Datek }}</td>
                        <td>{{ $data->Workzone }}</td>
                        <td>{{ $data->Induk_Gamas }}</td>
                        <td>{{ $data->Reported_Datex }}</td>
                        <td>{{ $data->Incident_Symptom }}</td>
                        <td>{{ $data->tim }}</td>
                        <td>{{ $data->catatan }}</td>
                        <td>{{ $data->action }}</td>
                        <td>{{ $data->penyebab }}</td>
                        <td>{{ $data->status_penyebab_reti }}</td>
                        <td>{{ $data->ib_onu_rx ? : '-' }}</td>
                        <td>{{ $data->redaman_iboster ? : '-' }}</td>
                        <td>{{ $data->ibs_onu_rx ? : '-' }}</td>
                        <td>{{ $data->tgl_dt }}</td>
                        <td>{{ $data->tgl_laporan }}</td>
					</tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL SISA SALDO TIKET ASSURANCE'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL SISA SALDO TIKET ASSURANCE'
                },
                {
                    extend: 'print',
                    title: 'DETAIL SISA SALDO TIKET ASSURANCE'
                }
            ]
        });
    });
</script>
@endsection