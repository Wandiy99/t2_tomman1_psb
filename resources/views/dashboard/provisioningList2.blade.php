@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboard/rekon/{{ $tglAwal }}/{{ $tglAkhir }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">LIST {{ $title }} {{ $jenis }} {{ $so }} {{ $tglAwal }} S/D {{ $tglAkhir }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th rowspan="2">NO</th>
                    <th rowspan="2">SM</th>
                    <th rowspan="2">TL</th>
                    <th rowspan="2">TEKNISI</th>
                    <th colspan="7">WO / TIKET</th>
                    <th colspan="2">NOMOR LAYANAN</th>
                    <th colspan="3">DATA PELANGGAN</th>
                    <th colspan="6">TRANSAKSI / MUTASI</th>
                    <th colspan="4">DATA PERANGKAT TERPASANG</th>
                    <th colspan="24">MATERIAL PENUNJANG</th>
                    <th colspan="6">LAIN-LAIN</th>
                  </tr>
                  <tr>
                    <th>NOMOR SC</th>
                    <th>MYIR</th>
                    <th>TGL DISPATCH</th>
                    <th>TGL LAPORAN</th>
                    <th>ORDER DATE</th>
                    <th>ORDER DATE PS</th>
                    <th>STO</th>
                    <th>TELEPON</th>
                    <th>INET / DATIN</th>
                    <th>NAMA</th>
                    <th>ALAMAT</th>
                    <th>ID SALES</th>
                    <th>JENIS MUTASI</th>
                    <th>PROVIDER</th>
                    <th>LAYANAN</th>
                    <th>TIPE TRANSAKSI</th>
                    <th>TIPE LAYANAN</th>
                    <th>NO RFC</th>
                    <th>NAMA MODEM</th>
                    <th>TYPE / SERIES</th>
                    <th>SN or MAC ADDRESS</th>
                    <th>MERK</th>
                    <th>MODE GELARAN</th>
                    <th>AD SC</th>
                    <th>DC</th>
                    <th>PRECON 100 M</th>
                    <th>PRECON 80 M</th>
                    <th>PRECON 75 M</th>
                    <th>PRECON 50 M</th>
                    <th>PRECON 150 Non Acc</th>
                    <th>TOTAL PRECON</th>
                    <th>PATCHCORE</th>
                    {{-- <th>UTP CAT6</th> --}}
                    <th>UTP CAT5</th>
                    <th>TIANG</th>
                    {{-- <th>PULL STRAP</th> --}}
                    <th>CONDUIT</th>
                    <th>TRAY CABLE</th>
                    <th>ODP</th>
                    <th>BREKET</th>
                    <th>CLAMPS</th>
                    <th>PULL STRAP</th>
                    <th>RJ45</th>
                    <th>SOC ILSINTEMTECH</th>
                    <th>SOC SUMITOMO</th>
                    <th>ROSET</th>
                    <th>Tambah Tiang (btg)</th>
                    <th>KORD. PEL </th>
                    <th>KORD. ODP</th>
                    <th>AREA</th>
                    <th>NAMA MITRA</th>
                    <th>STATUS SC</th>
                    <th>STATUS TEK.</th>
                    <th>CATATAN TEK.</th>
                  </tr>
                </thead>
                <tbody>
                @php
                  $totalPrecon100 = 0;
                  $totalPrecon80 = 0;
                  $totalPrecon75 = 0;
                  $totalPrecon50 = 0;
                  $totalPrecon150nonAcc = 0;
                  $totalPreconn = 0;
                  $a = 100;
                  $b = 80;
                  $c = 75;
                  $d = 50;
                  $f = 150;
                  $pc5 = 5;
                  $pc10 = 10;
                  $pc15 = 15;
                  $pc20 = 20;
                  $pc30 = 30;
                @endphp
                @foreach ($data as $num => $result)
                @php
                  $total_pc5 = ($result->patchcore_5 * $pc5);
                  $total_pc10 = ($result->patchcore_10 * $pc10);
                  $total_pc15 = ($result->patchcore_15 * $pc15);
                  $total_pc20 = ($result->patchcore_20 * $pc20);
                  $total_pc30 = ($result->patchcore_30 * $pc30);
                  $total_pc_all = $total_pc5 + $total_pc10 + $total_pc15 + $total_pc20 + $total_pc30;
                  $totalPrecon100 = ($result->precon100 * $a);
                  $totalPrecon80 = ($result->precon80 * $b);
                  $totalPrecon75 = ($result->precon75 * $c);
                  $totalPrecon50 = ($result->precon50 * $d);
                  $totalPrecon150nonAcc = ($result->preconnonacc * $f);
                  $totalPreconn = $totalPrecon100 + $totalPrecon80 + $totalPrecon75 + $totalPrecon50 + $totalPrecon150nonAcc + $result->dc_roll ;
                @endphp
                <?php
                  if ($result->mode==1){
                    $mode_gelaran = "KU";
                  } else {
                    $mode_gelaran = "UTP";
                  }
                ?>
                <tr>
                  <td>{{ ++$num }}</td>
                  <!-- <td>
                    <?php
                      $path = "/upload4/evidence/".$result->id_dt."/Berita_Acara.jpg";
                      if (file_exists(public_path().$path)){
                    ?>
                    <a href="/upload/evidence/{{ $result->id_dt }}/Berita_Acara.jpg"><img src="/upload/evidence/{{ $result->id_dt }}/Berita_Acara-th.jpg" /></a>
                    <?php
                      } else {
                        ?>
                        <img src="/image/placeholder.gif" alt="" />
                        <?php
                      }
                    ?>
                  </td> -->
                  <td>{{ $result->SM }}</td>
                  @if ($result->mode==1 && $mode_gelaran == "UTP")
                  <td bgcolor=red># {{ $result->TL }}</td>
                  @else
                  <td>{{ $result->TL }}</td>
                  @endif
                  <td>{{ $result->uraian }}</td>
                  <td><!-- <a href="/{{ $result->id_dt}}"> -->{{ $result->Ndem }}<!-- </a> --></td>
                  <td>{{ $result->kprot_myir ? : $result->myir ?: '-' }}</td>
                  <td>{{ $result->tanggal_dispatch }}</td>
                  <td>{{ $result->modified_at }}</td>
                  <td>{{ $result->kprot_orderDate ? : $result->orderDate }}</td>
                  <td>{{ $result->kprot_orderDatePs ? : $result->orderDatePs }}</td>
                  <td>{{ $result->sto }}</td>
                  <td>{{ $result->ndemPots }}</td>
                  <td>{{ $result->ndemSpeedy }}</td>
                  <td>{{ $result->orderName }}</td>
                  <td>{{ $result->orderAddr }} {{ $result->kordinat_pelanggan }}</td>

                  <!-- salas -->
                  <?php
                      $idSales = '-';
                      if (substr($result->jenisPsb,0,2)=="AO"){
                        $data = $result->kcontact;
                        $dataNew = explode(';', $data);
                        if ($dataNew[0] == 'MI'){
                          $idSales = $dataNew[2];
                        }
                        else{
                          $idSales = '-';
                        }
                      }
                  ?>

                  <td>{{ $idSales }}</td>
                  <td>{{ $result->jenisPsb }}</td>
                  <td>{{ $result->dps_provider ? : $result->kprot_provider ? : '-' }}</td>
                  <td>{{ strtoupper($result->jenis_layanan) }}</td>
                  <td>{{ $result->TYPE_TRANSAKSI }}</td>
                  <td>{{ $result->TYPE_LAYANAN }}</td>
                  <td>{{ $result->rfc_number }}</td>
                  <td>~</td>
                  <td>{{ $result->typeont }} ~ {{ $result->typestb }}</td>
                  <td>{{ $result->snont }} ~ {{ $result->snstb }}</td>
                  <td>~</td>
                  <td>
                    {{ $mode_gelaran }}
                  </td>

                  {{-- <td>{{ $result->dc_preconn ? : $result->dc_roll ? : "~" }}</td> --}}
                  {{-- <td>{{ $result->preconnectorized }}</td> --}}
                  <td>{{ $result->adsc }}</td>
                  <td>{{ $result->dc_roll ? : "~" }}</td>
                  <td>{{ $result->precon100 }}</td>
                  <td>{{ $result->precon80 }}</td>
                  <td>{{ $result->precon75 }}</td>
                  <td>{{ $result->precon50 }}</td>
                  <td>{{ $result->preconnonacc }}</td>
                  <!-- <td>{{ $result->dc_preconn ? : '~'}}</td> -->
                  <td>{{ $totalPreconn ? : '0'}}</td>
                  <td>{{ $total_pc_all }}</td>
                  <td>{{ $result->utp }}</td>
                  <td>{{ $result->tiang }}</td>
                  {{-- <td>{{ $result->pullstrap }}</td> --}}
                  <td>{{ $result->conduit }}</td>
                  <td>{{ $result->tray_cable }}</td>
                  <td>{{ strtoupper($result->nama_odp) }}</td>

                  <td>{{ $result->breket }}</td>
                  <td>{{ $result->clamps }}</td>
                  <td>{{ $result->pulstrap }}</td>
                  <td>{{ $result->rj45 }}</td>
                  <td>{{ $result->SOCIlsintentech }}</td>
                  <td>{{ $result->SOCSumitomo}}</td>
                  <td>{{ $result->roset }}</td>
                  <td>{{ $result->jml_tiang }}</td>

                  <td>{{ $result->kordinat_pelanggan }}</td>
                  <td>{{ $result->kordinat_odp }}</td>
                  <td>{{ $result->area_migrasi }}</td>
                  <td>{{ $result->mitra_amija_pt }}</td>
                  <td>{{ $result->kprot_orderStatus ? : $result->orderStatus }}</td>
                  <td>{{ $result->laporan_status }}</td>
                  <td>{{ $result->catatan }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'REKON PROVISIONING MITRA TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'REKON PROVISIONING MITRA TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'REKON PROVISIONING MITRA TOMMAN'
                }
            ]
        });
    });
</script>
@endsection