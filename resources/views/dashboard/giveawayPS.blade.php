@extends('layout')
@section('content')
<style>
    td {
        font-weight: 500;
    }
    .black-link {
        color: black;
    }
    .white-link {
        color: white !important;
    }
    .bg-blue {
        background-color: #4472c4;
        color: white !important;
    }
    .bg-orange {
        background-color: #c65911;
        color: white !important;
    }
    .bg-green {
        background-color: #548235;
        color: white !important;
    }
    .bg-grey {
        background-color: #808080;
        font-weight: bold;
        color: white !important;
    }
    .bg-greenlight {
        background-color: #1ABC9C;
        font-weight: bold;
        color: white !important;
    }
    .bg-red {
        background-color: #E74C3C;
        font-weight: bold;
        color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
</style>
@include('partial.alerts')

<div class="row">

<div class="col-md-12">
    <div class="white-box">
        <h4 class="page-title" style="text-align: center; font-weight: bold;">GIVEAWAY PS<br />PERIODE {{ $date }}</h4><br/>
        <div class="row">
            <div class="col-md-12">
                <form method="GET">
                    <div class="col-md-10">
                        <label for="date">DATE</label>
                        <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="date" value="{{ $date ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@foreach ($data_ps as $key => $value)
<div class="col-sm-4 table-responsive">
    <table class="table table-bordered dataTable">
        <thead>
                <tr>
                    <th class="text-center align-middle bg-blue" colspan="5">{{ $key }}</th>
                </tr>
              <tr>
                  <th class="text-center align-middle bg-blue">RANK</th>
                  <th class="text-center align-middle bg-blue">TIM</th>
                  <th class="text-center align-middle bg-blue">SEKTOR</th>
                  <th class="text-center align-middle bg-blue">MITRA</th>
                  <th class="text-center align-middle bg-green">JML_PS</th>
              </tr>
        </thead>
        <tbody>
            @php
                $total_ps = 0;
                $numb = 1;
            @endphp
            @foreach (array_slice($value, 0, 10) as $tim => $result)
            @php
                $total_ps += @$result['qty'];
            @endphp
              <tr>
                  <td class="text-center align-middle">{{ $numb++ }}</td>
                  <td class="text-center align-middle">{{ $tim }}</td>
                  <td class="text-center align-middle">{{ @$result['sektor'] ? : 'NON SEKTOR' }}</td>
                  <td class="text-center align-middle">{{ @$result['mitra'] ? : 'NON MITRA' }}</td>
                  <td class="text-center align-middle">
                      <a class="black-link" href="/dashboard/giveawayPS/detail?tim={{ $tim }}&date={{ $date }}">{{ @$result['qty'] }}</a>
                  </td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                  <td class="text-center align-middle bg-blue" colspan="4">TOTAL</td>
                  <td class="text-center align-middle bg-green">{{ $total_ps }}</td>
            </tr>
        </tfoot>
    </table>
</div>
@endforeach

</div>

<script>
    $(function() {
        $('.datepicker-autoclose').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            orientation: 'bottom'
        });
    });
</script>
@endsection
