@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL PROGRESS PI K-PRO AREA {{ $sektor }} TYPE {{ $type }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>CATEGORY</th>
                        <th>AREA</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>ORDER ID</th>
                        <th>ORDER NAME</th>
                        <th>JENIS PSB</th>
                        <th>TYPE LAYANAN</th>
                        <th>STATUS RESUME</th>
                        <th>ORDER DATE</th>
                        <th>SPEEDY</th>
                        <th>DESK TASK</th>
                        <th>STATUS TEKNISI</th>
                        <th>TGL LAPORAN TEKNISI</th>
                        <th>STATUS STARCLICK</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>
                            @if ($data->pkt_order_date == date('Y-m-d'))
                                PI BARU
                            @else
                                PI PAGI
                            @endif
                        </td>
                        <td>{{ $data->area ? : 'NON AREA' }}</td>
                        <td>{{ $data->sektor ? : '-' }}</td>
                        <td>{{ $data->tim ? : '-' }}</td>
                        <td>{{ $data->order_id }}</td>
                        <td>{{ $data->customer_name }}</td>
                        <td>{{ $data->jenispsb }}</td>
                        <td>{{ $data->type_layanan }}</td>
                        <td>{{ $data->status_resume }}</td>
                        <td>{{ $data->order_date }}</td>
                        <td>{{ $data->speedy }}</td>
                        <td>{{ $data->desc_task }}</td>
                        <td>{{ $data->status_tek ? : 'ANTRIAN' }}</td>
                        <td>{{ $data->tgl_status_tek ? : '-' }}</td>
                        <td>{{ $data->status_starclick ? : '-' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DASHBOARD PROGRESS PI K-PRO BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection