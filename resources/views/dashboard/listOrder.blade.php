@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardSC" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL WO PS AO DATEL {{ $datel }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>#</th>
                <th>SYNC</th>
                <th>AREA</th>
                <th>ORDER ID</th>
                <th>ORDER DATE</th>
                <th>ORDER PS</th>
                <th>TTI</th>
                <th>ORDER STATUS</th>
                <th>CUSTOMER</th>
                <th>K-CONTACT</th>
                <th>STO</th>
                <th>DATEL</th>
                <th>ALPRONAME</th>
                <th>JENIS LAYANAN</th>
                <th>APPROVAL</th>
                <th>APPROVAL DATE</th>
                <th>TGL</th>
                <th>DISPATCH TO</th>
                <th>LAST UPDATE</th>
                <th>CATATAN</th>
                <th>KOORDINAT PELANGGAN</th>
                <th>ALAMAT</th>
                <th>SN ONT</th>
                <th>DETEK</th>
              </tr>
            </thead>
            <tbody>
              <?php
              date_default_timezone_set('Asia/Makassar');
              ?>
              @foreach ($query as $num => $result)
              <?php
                $dateStart = strtotime($result->orderDate);
                $dateEnd = strtotime(date('Y-m-d H:i:s'));
                $diff = $dateEnd - $dateStart;
                $jam = floor($diff / (60 * 60));

              ?>
              <tr>
                <td>{{ ++$num }}.</td>
                <td><a href="/syncSC/{{ $result->orderId }}">SYNC</a></td>
                <td>{{ $result->area }}</td>
                <td>{{ $result->orderId }}</td>
                <td>{{ $result->orderDate }}</td>
                <td>{{ $result->orderDatePs }}</td>
                <td>{{ $jam }}</td>
                <td>{{ $result->orderStatus }}</td>
                <td>{{ $result->customer }}</td>
                <td>{{ $result->akcontack }}</td>
                <td>{{ @$result->esto }}</td>
                <td>{{ $result->datel }}</td>
                <td>{{ $result->alproname }}</td>
                <td>{{ $result->jenisLayanan }}</td>
                <td>{{ $result->status_approval }}</td>
                <td>{{ $result->approve_date }}</td>
                <td>{{ $result->tgl }}</td>
                <td>{{ $result->uraian }}</td>
                <td>{{ $result->laporan_status }} // {{ $result->ls }}</td>
                <td>{{ $result->catatan }}</td>
                <td>{{ $result->kordinat_pelanggan }}</td>
                <td>{{ $result->alamat }} // {{ $result->alamat1 }} </td>
                <td>{{ $result->snont ? : '-' }}</td>
                <td>{{ $result->detek ? : '-' }}</td>
              @endforeach
              </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
          {
            extend: 'copy',
            title: 'DETAIL WO PS AO STARCLICKNCX TOMMAN'
          },
          {
            extend: 'excel',
            title: 'DETAIL WO PS AO STARCLICKNCX TOMMAN'
          },
          {
            extend: 'print',
            title: 'DETAIL WO PS AO STARCLICKNCX TOMMAN'
          }
        ]
      });
    });
</script>
@endsection