@extends('layout')
@section('content')
<style>
    tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    .black-grey {
        background-color: #34495E;
        color: white !important;
    }
    .red-lose {
        background-color: #E74C3C;
        color: white !important;
    }
    .green-win {
        background-color: #1ABC9C;
        color: white !important;
    }
</style>
  @include('partial.alerts')

<div class="row">
    <div class="col-sm-12 table-responsive">

        <table class="table table-bordered dataTable">
            <thead>
                <tr>
                    <th class="align-middle" rowspan="2">SEKTOR</th>
                    <th class="align-middle" colspan="{{ date('t') }}">DASHBOARD PS/RE DBS-DGS ({{ strtoupper(date('F Y', strtotime($month))) }})</th>
                    <th class="align-middle" rowspan="2">JML</th>
                    <th class="align-middle" rowspan="2">AVG</th>
                </tr>
                <tr>
                    @php
                        $tgl_arr = [];
                    @endphp
                    @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                                $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                                $keys = $i;
                            @endphp
                        @endif
                        @php
                            $tgl_arr[$keys] = 0;
                        @endphp
                        <th class="align-middle">{{ $keys }}</th>
                    @endfor
                </tr>
            </thead>
            <tbody
                @php
                    $psre = 0;
                    $txt_psre = '';
                    $hitung_per_sektor = [];
                @endphp
                @foreach ($data as $result)
                <tr>
                    <td class="align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                    @for ($i = 1; $i < date('t') + 1; $i ++)
                            @if ($i < 10)
                                @php
                                    $keys = '0'.$i;
                                @endphp
                            @else
                                @php
                                    $keys = $i;
                                @endphp
                            @endif

                            @php
                                $txt_psre = 'psre_d'.$keys;
                                $psre = $result->$txt_psre;

                                if(!isset($hitung_per_sektor[$result->area ? : 'NON AREA'][$keys]) )
                                {
                                    $hitung_per_sektor[$result->area ? : 'NON AREA'][$keys] = 0;
                                }

                                $hitung_per_sektor[$result->area ? : 'NON AREA'][$keys] += $psre;

                                $tgl_arr[$keys] += $psre;
                            @endphp
                        <td class="align-middle">
                            <a href="/dashboard/psredbsdgs/detail?sektor={{ $result->area ? : 'NON AREA' }}&month={{ date('Y-m', strtotime($month)) }}&day={{ $keys }}">
                                {{ $psre }}
                            </a>
                        </td>
                    @endfor
                    <td class="align-middle">
                        <a href="/dashboard/psredbsdgs/detail?sektor={{ $result->area ? : 'NON AREA' }}&month={{ date('Y-m', strtotime($month)) }}&day=JML">
                            {{ $result->jumlah }}
                        </a>
                    </td>
                    <td class="align-middle">
                        @php
                            $avg = ROUND(array_sum($hitung_per_sektor[$result->area ? : 'NON AREA'])/count($hitung_per_sektor[$result->area ? : 'NON AREA']), PHP_ROUND_HALF_UP);
                        @endphp
                        {{ $avg == 0 ? 1 : $avg }}
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr style="font-weight: bold;">
                    <td>TOTAL</td>
                    @foreach ($tgl_arr as $k => $v)
                        <td>
                            <a href="/dashboard/psredbsdgs/detail?sektor=ALL&month={{ date('Y-m', strtotime($month)) }}&day={{ $k }}">
                            {{ $v }}
                            </a>
                        </td>
                    @endforeach
                    <td colspan="2">{{ array_sum($tgl_arr) }}</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection