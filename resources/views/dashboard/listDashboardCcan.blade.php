@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }

  </style>
  <a href="/dashboard/dashboad-wifi/{{ $tgl }}" class="btn btn-sm btn-default">
    <span class="glyphicon glyphicon-arrow-left"></span>
  </a><h3>List {{ $title }} {{ $status }} {{ $area }} {{ $tgl }} </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
        <table class="table table-striped table-bordered dataTable">
            <tr>
              <th>No</th>
              <th>Sektor</th>
              <th>Teknisi</th>
              <th>Nomor ND</th>
              <th>Tgl Dispatch</th>
              <th>STO</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>No. Telp</th>
              <th>PIC</th>
              <th>Koordinat Pelanggan</th>
              <th>ODP</th>
              <th>Koordinat ODP</th>
              <th>Jenis Layanan</th>
              <th>Area</th>
              <th>Status Teknisi</th>
              <th>Catatan Teknisi</th>
            </tr>

            @foreach ($data as $no=>$dt)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $dt->title }}</td>
                  <td>{{ $dt->uraian }}</td>
                  <td>{{ $dt->ND }}</td>
                  <td>{{ date('Y-m-d',strtotime($dt->created_at)) }}</td>
                  <td>{{ $dt->MDF }}</td>  
                  <td>{{ $dt->Nama }}</td>
                  <td>{{ $dt->Alamat }}</td>
                  <td>{{ $dt->noPelangganAktif }}</td>
                  <td>{{ $dt->pic}}</td>
                  <td>{{ $dt->kordinat_pelanggan}}</td>
                  <td>{{ $dt->nama_odp }}</td>
                  <td>{{ $dt->kordinat_odp }}</td>
                  <td>{{ $dt->layananMicc }}</td>
                  <td>{{ $dt->area_migrasi }}</td>
                  <td>{{ $dt->laporan_status }}</td>
                  <td>{{ $dt->catatan }}</td>
              </tr>
            @endforeach() 
        </table>
      </div>
    </div>
  </div>

@endsection     