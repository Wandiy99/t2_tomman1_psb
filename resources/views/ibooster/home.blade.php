@extends('new_tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>
    <div class="panel panel-default" style="padding:20px">
      <div class="panel-content">
        <center>
          <form method="post">
          <table class="table">
            <tr>
              <td>Ukur</td>
              <td><input type="text" name="ukur" class="table" /></td>
              <td><input type="submit" name="submit" value="Ukur" class="btn-small btn-warning" /></td>
            </tr>
          </table>
        </form>
        </center>
          <div class="list-group table-responsive">
          <table>
          <?php
            if (count($resultx)>0){
              foreach (@($resultx[0]) as $field => $data ){
                echo "<tr><td>".$field."</td><td>".$data."</td></tr>";
              }
            }
          ?>
        </table>
      </div>
    </div>
  </div>
@endsection
