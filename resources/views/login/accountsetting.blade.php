@extends('layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">
  				Account Settings
  			</div>
  			<div class="panel-body">
  				<form class="form" method="post">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Theme</label>
                                    <div class="col-10">
                                        <select class="custom-select col-12" name="theme" id="inlineFormCustomSelect">
                                            <option value="dark" <?php if(session('theme')=="dark") { echo "Selected"; } else { echo ""; } ?> >Dark</option>
                                            <option <?php if(session('theme')=="light") { echo "Selected"; } else { echo ""; } ?> value="light">Light</option>
                                            <option <?php if(session('theme')=="pink") { echo "Selected"; } else { echo ""; } ?> value="pink">Pink</option>
                                            <option <?php if(session('theme')=="purple") { echo "Selected"; } else { echo ""; } ?> value="purple">Purple</option>
                                            <option <?php if(session('theme')=="vice_city") { echo "Selected"; } else { echo ""; } ?> value="vice_city">Vice City</option>
                                            @if($auth=="20980872" || $auth=="20981020")
                                            <option <?php if(session('theme')=="blue") { echo "Selected"; } else { echo ""; } ?> value="blue">Blue Navy</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Save</button>
                                    </form>
  			</div>
  		</div>
  	</div>
  </div>
@endsection