@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    textarea{
      width: 100%;
    }
    input{
      width: 100%;
    }
  </style>
  <h3 style="color: black; font-weight: bold">COOKIES SYSTEMS</h3>
  <div class="row">

    {{-- @if (in_array($witel, ['KALSEL']))
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          Starclick
          <p class="text-muted" style="float: right;">{{ @$starclick->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="starclick">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">Username</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="username" value="{{ @$starclick->username }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-2 col-form-label">Password</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="password" value="{{ @$starclick->password }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-2 col-form-label">Cookies</label>
                <div class="col-10">
                    <textarea name="cookies" rows="4" readonly>{{ @$starclick->cookies }}</textarea>
                </div>
            </div>
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div>
    @endif --}}

    {{-- <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          K-PRO
          <p class="text-muted" style="float: right;">{{ @$kpro->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="kpro">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <textarea name="cookies">{{ @$kpro->cookies }}</textarea><br />
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div> --}}

    @if ($witel == 'KALSEL')
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          IBOOSTER
          <p class="text-muted" style="float: right;">{{ @$ibooster->updated_at }}</p>
        </div>
        <div class="panel-body text-center">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="ibooster">
            <input type="hidden" name="opt" value="T">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <div class="form-group row">
                <label for="page" class="col-2 col-form-label">Page</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="page" value="{{ @$ibooster->page }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="cookie_ibooster" class="col-2 col-form-label">Cookies</label>
                <div class="col-10">
                    <textarea name="cookies">{{ @$ibooster->cookies }}</textarea>
                </div>
            </div>
            <button class="btn btn-success">Update</button>
          </form>
        </div>
      </div>
    </div>
    @elseif ($witel == 'BALIKPAPAN')
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          IBOOSTER S
          <p class="text-muted" style="float: right;">{{ @$ibooster_s->updated_at }}</p>
        </div>
        <div class="panel-body text-center">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="ibooster">
            <input type="hidden" name="opt" value="S">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <div class="form-group row">
                <label for="page" class="col-2 col-form-label">Page</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="page" value="{{ @$ibooster_s->page }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="cookie_ibooster" class="col-2 col-form-label">Cookies</label>
                <div class="col-10">
                    <textarea name="cookies">{{ @$ibooster_s->cookies }}</textarea>
                </div>
            </div>
            <button class="btn btn-success">Update</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          IBOOSTER X
          <p class="text-muted" style="float: right;">{{ @$ibooster_x->updated_at }}</p>
        </div>
        <div class="panel-body text-center">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="ibooster">
            <input type="hidden" name="opt" value="X">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <div class="form-group row">
                <label for="page" class="col-2 col-form-label">Page</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="page" value="{{ @$ibooster_x->page }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="cookie_ibooster" class="col-2 col-form-label">Cookies</label>
                <div class="col-10">
                    <textarea name="cookies">{{ @$ibooster_x->cookies }}</textarea>
                </div>
            </div>
            <button class="btn btn-success">Update</button>
          </form>
        </div>
      </div>
    </div>
    @endif

    @if (in_array($witel, ['KALSEL']))
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          PRABAC
          <p class="text-muted" style="float: right;">{{ @$prabac->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="prabac">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <textarea name="cookies">{{ @$prabac->cookies }}</textarea><br />
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div>
    {{-- <div class="col-sm-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          UT ONLINE
          <p class="text-muted" style="float: right;">{{ @$utonline->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="utonline">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <textarea name="cookies">{{ @$utonline->cookies }}</textarea><br />
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div> --}}

    {{-- <div class="col-sm-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          RACCOON
          <p class="text-muted" style="float: right;">{{ @$raccoon->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="raccoon">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <textarea name="cookies">{{ @$raccoon->cookies }}</textarea><br />
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div> --}}
    @endif

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          TACTICAL PRO
          <p class="text-muted" style="float: right;">{{ @$tacticalpro->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/cookiesystemSave">
            <input type="hidden" name="app" value="tacticalpro">
            <input type="hidden" name="witel" value="{{ $witel }}">
            <textarea name="cookies">{{ @$tacticalpro->cookies }}</textarea><br />
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div>

    @if (in_array($witel, ['KALSEL']))
    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          WhatsApp API [BROADCAST]
          <p class="text-muted" style="float: right;">{{ @$wa_broadcast->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/loginwhatsapp/save">
            <input type="hidden" name="divisi" value="broadcast">
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">ID Sender</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="sender" value="{{ @$wa_broadcast->sender }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                  <div class="radio-list">
                      @php
                          if(@$wa_broadcast->status == 1)
                          {
                            $checkA = "checked";
                            $checkB = "";
                          } else {
                            $checkA = "";
                            $checkB = "checked";
                          }
                      @endphp
                      <label class="radio-inline p-0">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="active" value="1"  {{ $checkA }}>
                              <label for="active">Active</label>
                          </div>
                      </label>
                      <label class="radio-inline">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="nonactive" value="0" {{ $checkB }}>
                              <label for="nonactive">Non Active</label>
                          </div>
                      </label>
                  </div>
                </div>
            </div>
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          WhatsApp API [PROVISIONING]
          <p class="text-muted" style="float: right;">{{ @$wa_provi->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/loginwhatsapp/save">
            <input type="hidden" name="divisi" value="provisioning">
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">ID Sender</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="sender" value="{{ @$wa_provi->sender }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                  <div class="radio-list">
                      @php
                          if(@$wa_provi->status == 1)
                          {
                            $checkA = "checked";
                            $checkB = "";
                          } else {
                            $checkA = "";
                            $checkB = "checked";
                          }
                      @endphp
                      <label class="radio-inline p-0">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="active" value="1"  {{ $checkA }}>
                              <label for="active">Active</label>
                          </div>
                      </label>
                      <label class="radio-inline">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="nonactive" value="0" {{ $checkB }}>
                              <label for="nonactive">Non Active</label>
                          </div>
                      </label>
                  </div>
                </div>
            </div>
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div>

    {{-- <div class="col-sm-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          WhatsApp API [ASSURANCE]
          <p class="text-muted" style="float: right;">{{ @$wa_assr->updated_at }}</p>
        </div>
        <div class="panel-body">
          <form method="post" action="/loginwhatsapp/save">
            <input type="hidden" name="divisi" value="assurance">
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">ID Sender</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="sender" value="{{ @$wa_assr->sender }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                  <div class="radio-list">
                    @php
                        if(@$wa_assr->status == 1)
                        {
                          $checkA = "checked";
                          $checkB = "";
                        } else {
                          $checkA = "";
                          $checkB = "checked";
                        }
                    @endphp
                      <label class="radio-inline p-0">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="active" value="1"  {{ $checkA }}>
                              <label for="active">Active</label>
                          </div>
                      </label>
                      <label class="radio-inline">
                          <div class="radio radio-info">
                              <input type="radio" name="status" id="nonactive" value="0" {{ $checkB }}>
                              <label for="nonactive">Non Active</label>
                          </div>
                      </label>
                  </div>
                </div>
            </div>
            <center><button class="btn btn-success">Update</button></center>
          </form>
        </div>
      </div>
    </div> --}}
    @endif

  </div>
@endsection
