@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
  <a href="/bon/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>  
    Bon Material Logistik
  </h3>

  <div class="list-group">
    @foreach($list as $data)
      <a href="/bon/{{ $data->id }}" class="list-group-item">
        <strong>{{ $data->nama }}</strong>
        <br />
        <span>{{ $data->updater }}</span>
        <br />
        <span>{{ $data->ts }}</span>
      </a>
    @endforeach
  </div>
@endsection
