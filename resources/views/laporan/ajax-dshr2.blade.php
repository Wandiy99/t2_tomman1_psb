      <div class="panel panel-primary">
        <div class="panel-heading">2. LAPORAN TRANSAKSI HARI INI PER TEAM</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>TEAM</th>
                <th>VA</th>
                <th>TC</th>
                <th>UNSC DEP</th>
                <th>UNSC NR2C</th>
                <th>Tanpa Status</th>
                <th>PS</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $va = 0;
                  $tc = 0;
                  $dep = 0;
                  $nr2c = 0;
                  $ps = 0;
                  $ts = 0;
                /* --}}
                @foreach($dua as $no => $te)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $te->k }}</td>
                  <td>{{ $te->VA1 }}</td>
                  <td>{{ $te->TC1 }}</td>
                  <td>{{ $te->unsc_dep1 }}</td>
                  <td>{{ $te->unsc_nr2c1 }}</td>
                  <td>{{ $te->ts }}</td>
                  <td>{{ $te->ps1 }}</td>
                  <td class="strong">{{ $te->total }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+($te->total);
                    $va = $va+$te->VA1;
                    $tc = $tc+$te->TC1;
                    $dep = $dep+$te->unsc_dep1;
                    $nr2c = $nr2c+$te->unsc_nr2c1;
                    $ps = $ps+$te->ps1;
                    $ts = $ts+$te->ts;
                  /* --}}
                @endforeach
                
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $dep }}</td><td>{{ $nr2c }}</td><td>{{ $ts }}</td><td>{{ $ps }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>