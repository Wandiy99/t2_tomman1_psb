@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>
    Report Maintenance
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::segment(3) == 'mt_rekon') ? 'active' : '' }}"><a href="/laporan/mt/mt_rekon">Rekon</a></li>
    <li class="{{ (Request::segment(3) == 'mt_regu') ? 'active' : '' }}"><a href="/laporan/mt/mt_regu">By Regu</a></li>
    <li class="{{ (Request::segment(3) == 'mt_tiket') ? 'active' : '' }}"><a href="/laporan/mt/mt_tiket">By Tiket</a></li>
  </ul>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default aksi" aria-label="Left Align">
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="daily">Daily</a></li>
          <li><a href="#" id="monthly">Monthly</a></li>
         
        </ul>
      </div><!-- /btn-group -->
      <div class="ins">
        <input type="text" class="form-control" id="tanggal">
      </div>
    </div><!-- /input-group -->
  </br>


  <div id="txtHint">
  </div>
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
    $(function(){
      var day = {
        format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
      };
      var month = {
        format: "yyyy-mm", viewMode: 2, minViewMode: 1
      };
      $("input").datepicker(day);
      $("#daily").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(day);
      });

      $("#monthly").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(month);
      });
      var fullDate = new Date()
      console.log(fullDate);
      //Thu May 19 2011 17:25:38 GMT+1000 {}
       
      //convert month to 2 digits
      var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
      var twoDigitDay = ((fullDate.getDate().length+1) === 1)? (fullDate.getDate()) : '0' + (fullDate.getDate());
       
      var currentDate = fullDate.getFullYear() + "-" +twoDigitMonth+"-"+twoDigitDay;
      console.log(currentDate);
      $.get("/maintaince/getMtRekon/" + currentDate, function(text) {
        $("#txtHint").html(text);
      }).done(function(){
        $(".loading").css({"visibility":"hidden"});
      });

      $(".aksi").click(function(){
        $(".loading").css({"visibility":"visible"});
        $.get("/maintaince/getMtRekon/" + $("#tanggal").val(), function(text) {
         $("#txtHint").html(text);
        }).done(function(){
          $(".loading").css({"visibility":"hidden"});
        });
      });
    });
  </script>
@endsection
