@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/laporan/stok-teknisi" class="btn btn-sm btn-default">
      <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
    Stok Detail {{ $id }}
  </h3>

  <div class="list-group">
    @foreach($list as $no => $data)
      <dvi class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
        <strong>{{ $data->nama }}</strong>
        <span class="label label-info badge">{{ $data->stok }}</span>
      </div>
    @endforeach
  </div>
@endsection
