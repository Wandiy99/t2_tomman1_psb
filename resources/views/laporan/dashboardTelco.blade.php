@extends('layout')

@section('content')
<style>
table {
        font-size: 9px;
    }
</style>
<table class="table table-striped table-bordered table-condensed">
  DASHBOARD KEHADIRAN WITEL KALSEL
  <thead>
    <tr>
      <th>#</th>
      <th>Teknisi</th>
      <th>Teknisi Total</th>
      <th>Teknisi Masuk</th>
      <th>% Jadwal</th>
      <th>Teknisi Absen</th>
      <th>% Absen</th>
      <th>Teknisi Tdk Absen</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $total = 0;
      $masuk = 0;
      $pmasuk = 0;
      $absen = 0;
      $pabsen = 0;
      $tdk_absen = 0;
    ?>
    @foreach($dash as $no => $data)
      <?php
        $total += $data['total'];
        $masuk += $data['masuk'];
        $absen += $data['absen'];
        $tdk_absen += $data['tdk_absen'];
        $pmasuk = $data['masuk']/$data['total']*100;
        $pabsen = $data['absen']/$data['masuk']*100;
        $tgl = date("Y-m-d");
      ?>
      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $data['div']==2 ? 'Provisioning' : 'Assurance' }}</td>
        <td><a href="/telcoDetil/{{$tgl}}/{{$data['div']}}/0/0" target="_BLANK">{{$data['total']}}</a></td>

        <td><a href="/telcoDetil/{{$tgl}}/{{$data['div']}}/MASUK/0" target="_BLANK">{{$data['masuk']}}</a></td>
        <td>{{ number_format($pmasuk, 2, ',', '.') }}%</td>

        <td><a href="/telcoDetil/{{$tgl}}/{{$data['div']}}/0/ABSEN" target="_BLANK">{{$data['absen']}}</a></td>
        <td>{{ number_format($pabsen, 2, ',', '.') }}%</td>
        <td><a href="/telcoDetil/{{$tgl}}/{{$data['div']}}/0/TIDAK ABSEN" target="_BLANK">{{$data['tdk_absen']}}</a></td>
      </tr>
      
    @endforeach
    <tr>
        <td colspan="2">Sum</td>
        <td><a href="/telcoDetil/{{$tgl}}/0/0/0" target="_BLANK">{{ $total }}</a></td>

        <td><a href="/telcoDetil/{{$tgl}}/0/MASUK/0" target="_BLANK">{{ $masuk }}</a></td>
        <td>{{ number_format($masuk/$total*100, 2, ',', '.') }}%</td>

        <td><a href="/telcoDetil/{{$tgl}}/0/0/ABSEN" target="_BLANK">{{$absen}}</a></td>
        <td>{{ number_format($absen/$masuk*100, 2, ',', '.') }}%</td>
        <td><a href="/telcoDetil/{{$tgl}}/0/0/TIDAK ABSEN" target="_BLANK">{{$tdk_absen}}</a></td>
      </tr>
  </tbody>
</table>
<div class="col-sm-6">
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <th>#</th>
      <th>STATUS</th>
      @foreach($head as $h)
        <th>{{ $h }}</th>
      @endforeach
      <th>TOTAL</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $gt = 0 ;
    ?>
    @foreach($nd as $no => $list) 
      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $list['status'] ?: 'NO-UPDATE' }}</td>
        <?php
          $total = 0 ;
        ?>
        @foreach($list['STO'] as $no2 => $l2)
          <td>{{ $l2 }}</td>
          <?php
            $total += $l2;
          ?>
        @endforeach
        <td align="center">{{$total}}</td>
      </tr>
      <?php
        $gt += $total;
      ?>
    @endforeach
    <tr>
      <td colspan="2">GRAND TOTAL</td>
      @foreach($head as $h)
        <th>{{ $h }}</th>
      @endforeach
      <td align="center">{{ $gt }}</td>
    </tr>
</table>
</div>
<div class="col-sm-6">
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <th>PS HI</th>
      <th>OGP</th>
      <th>ESTIMASI PS</th>
      <th>SISA PI</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{ $prov->PSHI }}</td>
      <td>{{ $prov->ASHI }}</td>
      <td>{{ $prov->PSHI+$prov->ASHI }}</td>
      <td>{{ $pi }}</td>
    </tr>
</table>

<table class="table table-striped table-bordered table-condensed">
  <thead>
    <tr>
      <th>TECHCLOSED</th>
      <th>OGP</th>
      <th>ESTIMASI SALDO</th>
      <th>SALDO REGULER</th>
      <th>SALDO GAMAS</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{ $tc }}</td>
      <td>{{ $ogp }}</td>
      <td>{{ $asr->reguler - $ogp }}</td>
      <td>{{ $asr->reguler }}</td>
      <td>{{ $asr->gamas }}</td>
    </tr>
</table>
</div>
<table class="table table-striped table-bordered table-condensed">DASHBOARD PERSENTASE PERFORMANSI KALIMANTAN <span class="label label-primary">sync:{{ $sync->ts }}</span>
  <thead>
     <tr>
      <th rowspan="2" style="vertical-align:middle;" class="text-center">UNIT</th>
      <th rowspan="2" style="vertical-align:middle;" class="text-center">KATEGORI</th>
      <th colspan="3" class="text-center">FIBERZONE 2</th>
      <th colspan="3" class="text-center">FIBERZONE 1</th>
      <th rowspan="2" style="vertical-align:middle;" class="text-center">SOURCE</th>
    </tr>
    <tr>
      <th class="text-center">KALSEL</th>
      <th class="text-center">KALTENG</th>
      <th class="text-center">KALBAR</th>
      <th class="text-center">KALTARA</th>
      <th class="text-center">BALIKPAPAN</th>
      <th class="text-center">SAMARINDA</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $row = '';
    ?>
    @foreach($da as $no => $d)
      <?php
        $kalsel = trim(str_replace('%', '', $d->KALSEL));
        $kalteng = trim(str_replace('%', '', $d->KALTENG));
        $kalbar = trim(str_replace('%', '', $d->KALBAR));
        $kaltara = trim(str_replace('%', '', $d->KALTARA));
        $bpp = trim(str_replace('%', '', $d->BALIKPAPAN));
        $smr = trim(str_replace('%', '', $d->SAMARINDA));
        $array = array($kalsel,$kalteng,$kalbar,$bpp,$smr,$kaltara);
        $max = max($array);
        $min = min($array);
        $success = "success";
        $danger = "danger";
        if($d->kategori == "Q" || $d->kategori == "MTTI" || $d->kategori == "MTTR" || $d->kategori == "GAUL"){
          $success = "danger";
          $danger = "success";
        }

      ?>
      @if($row == $d->row)
        <tr>
          <td>{{ $d->kategori }}</td>
          <td class="text-right"><span class="label label-{{ ($max == $kalsel) ? $success : (($min == $kalsel) ? $danger : 'info') }}">{{ $kalsel }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kalteng) ? $success : (($min == $kalteng) ? $danger : 'info') }}">{{ $kalteng }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kalbar) ? $success : (($min == $kalbar) ? $danger : 'info') }}">{{ $kalbar }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kaltara) ? $success : (($min == $kaltara) ? $danger : 'info') }}">{{ $kaltara }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $bpp) ? $success : (($min == $bpp) ? $danger : 'info') }}">{{ $bpp }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $smr) ? $success : (($min == $smr) ? $danger : 'info') }}">{{ $smr }} {{ $d->sat }}</span></td>
          <td class="text-right">{{ $d->source_name }}</span></td>
        </tr>
      @else
        <tr>
          <td style="vertical-align:middle;" class="text-center" rowspan="{{ $d->row }}">{{ $d->unit }}</td>
          <td>{{ $d->kategori }}</td>
          <td class="text-right"><span class="label label-{{ ($max == $kalsel) ? $success : (($min == $kalsel) ? $danger : 'info') }}">{{ $kalsel }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kalteng) ? $success : (($min == $kalteng) ? $danger : 'info') }}">{{ $kalteng }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kalbar) ? $success : (($min == $kalbar) ? $danger : 'info') }}">{{ $kalbar }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $kaltara) ? $success : (($min == $kaltara) ? $danger : 'info') }}">{{ $kaltara }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $bpp) ? $success : (($min == $bpp) ? $danger : 'info') }}">{{ $bpp }} {{ $d->sat }}</span></td>
          <td class="text-right"><span class="label label-{{ ($max == $smr) ? $success : (($min == $smr) ? $danger : 'info') }}">{{ $smr }} {{ $d->sat }}</span></td>
          <td class="text-right">{{ $d->source_name }}</span></td>
        </tr>
      @endif
      <?php
        $row = $d->row;
      ?>
    @endforeach
</table>
@endsection