      <div class="panel panel-primary">
        <div class="panel-heading">7. LAPORAN PS BULAN INI PER TEKNISI</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>0P</th>
                <th>1P</th>
                <th>2P</th>
                <th>3P</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $nolp = 0;
                  $satup = 0;
                  $duap = 0;
                  $tigap = 0;
                /* --}}
                @foreach($tujuh as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->ns }}</td>
                  <td>{{ $t->nama }}</td> 
                  <td>{{ $t->nolP7 }}</td>
                  <td>{{ $t->satuP7 }}</td>
                  <td>{{ $t->duaP7 }}</td>
                  <td>{{ $t->tigaP7 }}</td>
                  <td class="strong">{{ $t->total7 }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$t->total7;
                    $nolp = $nolp+$t->nolP7;
                    $satup = $satup+$t->satuP7;
                    $duap = $duap+$t->duaP7;
                    $tigap = $tigap+$t->tigaP7;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="3">TOTAL</td><td>{{ $nolp }}</td><td>{{ $satup }}</td><td>{{ $duap }}</td><td>{{ $tigap }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>