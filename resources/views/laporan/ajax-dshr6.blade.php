      <div class="panel panel-primary">
        <div class="panel-heading">6. LAPORAN ORDER BULAN INI PER TEAM</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>TEAM</th>
                <th>VA</th>
                <th>TC</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $va = 0;
                  $tc = 0;
                /* --}}
                @foreach($enam as $no => $te)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $te->k }}</td>
                  <td>{{ $te->va5 }}</td>
                  <td>{{ $te->tc5 }}</td>
                  <td class="strong">{{ $te->total }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$te->total;
                    $va = $va+$te->va5;
                    $tc = $tc+$te->tc5;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>    