      <div class="panel panel-primary">
        <div class="panel-heading">5. LAPORAN ORDER BULAN INI PER TEKNISI</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>VA</th>
                <th>TC</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $va = 0;
                  $tc = 0;
                /* --}}
                @foreach($lima as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->ns }}</td>
                  <td>{{ $t->nama }}</td> 
                  <td>{{ $t->va5 }}</td>
                  <td>{{ $t->tc5 }}</td>
                  <td class="strong">{{ $t->total }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$t->total;
                    $va = $va+$t->va5;
                    $tc = $tc+$t->tc5;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="3">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>