<div class="panel panel-primary">
    <div class="panel-heading">Material</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
				       
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="20">Area</th>
			        <th width="50">ND</th>
			        <th width="150">Nama</th>
			        
			        <th width="150">Tanggal Dispatch</th>
			        <th width="150">Nama Tim</th>
					<th width="50">K3</th>
					

			      </tr>
			    </thead>
			    <tbody>
				@foreach($data as $no => $list) 
				  <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $list['area'] ? : 'UNKNOWN' }}<span></span></td>
			        <td>{{ $list['ND'] }}<br />{{ $list['ND_Speedy'] }}<span></span></td>
			        <td>{{ $list['nama'] }}<span></span></td>
			        <td>{{ $list['tgl_dispatch'] }}<span></span></td>
			        <td>{{ $list['uraian'] }}<span></span></td>
					<?php
						$path = "/upload4/evidence/".$list['id_dt']."/K3.jpg";
					?>
					<td>
					@if (file_exists(public_path().$path))
					<img src="{{$path}}" height="100" />
					@else
					<img src="/image/placeholder.gif" alt="" />
					@endif
					</td>
			      </tr>
			    @endforeach
			    </tbody>
			  </table>
			 </div>
      </div>
    </div>
  </div>
