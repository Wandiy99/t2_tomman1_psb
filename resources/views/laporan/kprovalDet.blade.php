@extends('layout')
@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"> Detel <b> {{ $dataVal[0]->datel }} </div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered">
                    <th>No</th>
                    <th>Order ID / Speedy / Nama Cust </th>
                    <th>Type Transaksi / Jenis Layanan / Alpro / NCli / Pots</th>
                    <th>Long / Lat</th>
                    <th>K-Contack</th>
                    <th>Order Date </th>
                    <th>Status Resume</th>

                    @foreach($dataVal as $no=>$val)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>
                                {{ $val->order_id }} <br>
                                {{ $val->speedy }} <br>
                                {{ $val->nama_cust }}<br>
                                {{ $val->no_hp}} <br>
                                {{ $val->alamat }}
                            </td>
                            <td>
                                {{ $val->type_transaksi }} <br>
                                {{ $val->jenis_layanan}} <br>
                                {{ $val->alpro }} <br>
                                {{ $val->ncli }} <br>
                                {{ $val->pots }}
                            </td>
                            <td>
                                {{ $val->lng }} <br>
                                {{ $val->lat}}
                            </td>
                            <td>{{ $val->kcontact}} </td>
                            <td>{{ $val->order_date}}</td>
                            <td>{{ $val->status_resume}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection