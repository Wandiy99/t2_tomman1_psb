@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    Laporan
  </h3>

  <ul class="nav nav-tabs" style="margin-bottom:20px">
    <li class="{{ (Request::path() == 'laporan') ? 'active' : '' }}"><a href="/laporan/stok-gudang">Stock Gudang</a></li>
    <li><a href="/laporan/stok-teknisi">Stock Teknisi</a></li>
  </ul>

  <div class="list-group">
    @foreach($list as $no => $data)
      <a href="#" class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
        <span>{{ $data->id_item }}</span>
        <span class="badge">{{ $data->qty }}</span>
        <span class="label label-info">{{ $data->unit_item }}</span>
      </a>
    @endforeach
  </div>
@endsection
