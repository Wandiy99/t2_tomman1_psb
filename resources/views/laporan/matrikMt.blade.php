@extends('layout')
@section('content')
  @include('partial.alerts')
  @if($matrix)
       {{--  <div class="panel panel-default">
            <div class="panel-heading">MATRIK ORDER MAINTENANCE</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th rowspan=2>#</th>
                        <th rowspan=2>JENIS ORDER</th>
                        <th rowspan=2>XCHECK HD<br/>CONSUMER</th>
                        <th colspan=7>MAINTENANCE</th>
                    </tr>
                    <tr>
                        <th>NO DISPATCH</th>
                        <th>ANTRIAN</th>
                        <th>KENDALA TEKNIS</th>
                        <th>KENDALA PELANGGAN</th>
                        <th>CLOSE HI</th>
                        <th>CLOSE</th>
                        <th>TOTAL</th>
                    </tr> --}}

                    <?php
                        // $total = 0;
                        // $undisp = 0;
                        // $no_update = 0;
                        // $kendala_teknis = 0;
                        // $kendala_pelanggan = 0;
                        // $close = 0;$closeHi = 0;
                        // $xcheck = 0;
                        // $stotal = 0;
                    ?>
    
                    {{-- @foreach($data as $no => $d) --}}
                        <?php
                            // $total = $d->undisp+$d->no_update+$d->kendala_teknis+$d->kendala_pelanggan+$d->close;
                            // $undisp += $d->undisp;
                            // $no_update += $d->no_update;
                            // $kendala_teknis += $d->kendala_teknis;
                            // $kendala_pelanggan += $d->kendala_pelanggan;
                            // $close += $d->close;$closeHi += $d->closeHi;
                            // $xcheck += $d->xcheck;
                            // $stotal += $total;
                        ?>
                        {{-- <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $d->jenis_order }}</td>
                            <td><a href="/matrikMt/{{$d->status_psb}}/xcheck">{{ $d->xcheck }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/undisp">{{ $d->undisp }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/null">{{ $d->no_update }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/kendala teknis">{{ $d->kendala_teknis }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/kendala pelanggan">{{ $d->kendala_pelanggan }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/closeHi">{{ $d->closeHi }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/close">{{ $d->close }}</a></td>
                            <td><a href="/matrikMt/{{$d->id}}/all">{{ $total }}</a></td> --}}
                            {{-- <td><a href="/matrikMt/{{$d->id}}/all">{{ $total }}</a></td> --}}
                   {{--      </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">TOTAL STATUS</td>
                        <td><a href="/matrikMt/all/xcheck">{{ $xcheck }}</a></td>
                        <td><a href="/matrikMt/all/undisp">{{ $undisp }}</a></td>
                        <td><a href="/matrikMt/all/null">{{ $no_update }}</a></td>
                        <td><a href="/matrikMt/all/kendala teknis">{{ $kendala_teknis }}</a></td>
                        <td><a href="/matrikMt/all/kendala pelanggan">{{ $kendala_pelanggan }}</a></td>
                        <td><a href="/matrikMt/all/closeHi">{{ $closeHi }}</a></td>
                        <td><a href="/matrikMt/all/close">{{ $close }}</a></td>
                        <td><a href="/matrikMt/all/all">{{ $stotal }}</a></td>
                    </tr>
                </table>
            </div>
        </div> --}}

        {{-- tambahan disini --}}

        <div class="panel panel-default">
            <div class="panel-heading">MATRIK ORDER MAINTENANCE</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th rowspan=2>#</th>
                        <th rowspan=2>JENIS ORDER</th>
                        <th rowspan=2>DATEL</th>
                        <th rowspan=2>XCHECK HD<br/>CONSUMER</th>
                        <th colspan=7>MAINTENANCE</th>
                    </tr>
                    <tr>
                        <th>NO DISPATCH</th>
                        <th>ANTRIAN</th>
                        <th>KENDALA TEKNIS</th>
                        <th>KENDALA PELANGGAN</th>
                        <th>CLOSE HI</th>
                        <th>CLOSE</th>
                        <th>TOTAL</th>
                    </tr>

                    <?php
                        $total              = 0;
                        $undisp             = 0;
                        $no_update          = 0;
                        $kendala_teknis     = 0;
                        $kendala_pelanggan  = 0;
                        $close              = 0;
                        $closeHi            = 0;
                        $xcheck             = 0;
                        $stotal             = 0;
                    ?>
                    
                    @foreach($dataOrder as $no => $order)
                            <tr>
                                <td rowspan="6">{{ ++$no }}</td>
                                <td rowspan="6">{{ $order }}</td>
                        @foreach ($dataMatrik as $no => $matrik)
                            @if ($order == $matrik->jns_order)    
                                <?php
                                    $total = $matrik->no_dispatch + $matrik->no_update + $matrik->kendala_teknis + $matrik->kendala_pelanggan + $matrik ->close;
                                    $undisp              += $matrik->no_dispatch;
                                    $no_update           += $matrik->no_update;
                                    $kendala_teknis      += $matrik->kendala_teknis;
                                    $kendala_pelanggan   += $matrik->kendala_pelanggan;
                                    $close               += $matrik->close;
                                    $closeHi             += $matrik->close_hi;
                                    $xcheck              += $matrik->xcheck;
                                    $stotal              += $total;
                                ?>

                                    <td>{{ $matrik->datel }}</td>
                                    <td><a href="/matrikMt/{{$matrik->status_psb}}/xcheck/{{ $matrik->datel }}">{{ $matrik->xcheck }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/undisp/{{ $matrik->datel }}">{{ $matrik->no_dispatch }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/null/{{ $matrik->datel }}">{{ $matrik->no_update }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/kendala teknis/{{ $matrik->datel }}">{{ $matrik->kendala_teknis }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/kendala pelanggan/{{ $matrik->datel }}">{{ $matrik->kendala_pelanggan }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/closeHi/{{ $matrik->datel }}">{{ $matrik->close_hi }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/close/{{ $matrik->datel }}">{{ $matrik->close }}</a></td>
                                    <td><a href="/matrikMt/{{$matrik->status_laporan}}/all/{{ $matrik->datel }}">{{ $matrik->total }}</a></td>
                                </tr>
                            @endif
                        @endforeach   
                    @endforeach            

                    <tr>
                        <td colspan="3">TOTAL STATUS</td>
                        <td><a href="/matrikMt/all/xcheck/all">{{ $xcheck }}</a></td>
                        <td><a href="/matrikMt/all/undisp/all">{{ $undisp }}</a></td>
                        <td><a href="/matrikMt/all/null/all">{{ $no_update }}</a></td>
                        <td><a href="/matrikMt/all/kendala teknis/all">{{ $kendala_teknis }}</a></td>
                        <td><a href="/matrikMt/all/kendala pelanggan/all">{{ $kendala_pelanggan }}</a></td>
                        <td><a href="/matrikMt/all/closeHi/all">{{ $closeHi }}</a></td>
                        <td><a href="/matrikMt/all/close/all">{{ $close }}</a></td>
                        <td><a href="/matrikMt/all/all/all">{{ $stotal }}</a></td>
                    </tr>
                 
                </table>
            </div>
        </div>
    @else
        <div class="panel panel-default" >
            <div class="panel-heading">LIST</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>#</th>
                        <th>JENIS ORDER / Headline</th>
                        <th>NO TIKET / By</th>
                        <th>ODP/Pelanggan</th>
                        <th>TIM SEBELUMNYA</th>
                    </tr>
                    <?php
                        //dd($data);
                    ?>
                    @foreach($data as $no => $d)
                        <?php
                            $tiket = '';
                         ?>
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td><span class="label label-default">No Tiket</span> {{ $d->no_tiket }} {{ $tiket }}<br/>
                                <span class="label label-default">Laporan Dari</span>{{ $d->dispatch_regu_name }}
                                @if(isset($d->id_dispatch) && Request::segment(2)==11)
                                <br/><span>
                                    <button class="btn btn-xs btn-success button_check" id-dispatch="{{ $d->id_dispatch }}">Kirim Ke Maintenance</button>
                                </span>
                                @endif
                            </td>
                            <td><span class="label label-default">Order</span>{{ @$d->laporan_status }}<br/>
                                <span class="label label-default">Headline</span>{{ $d->headline }}<br/>
                                <span class="label label-default">Action</span>{{ @$d->aksi }}
                            </td>
                            <td>
                                <span class="label label-default">Nama Odp</span>{{ $d->nama_odp }}</br>
                                <span class="label label-default">Koordinat Odp</span>{{ @$d->kordinat_odp }}</br>
                                <span class="label label-default">Koordinat Plg</span>{{ @$d->kordinat_pelanggan }}
                            </td>
                            <td>
                                <span class="label label-primary">TIM</span> {{ $d->uraian }}</br>
                                <span class="label label-success">TL</span> {{ $d->TL }}</br>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div id="mapModal" class="modal fade">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <form method="post" id="check">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Kirim Tiket ke MT
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div style="height:350px;">
                            <input name="id_pl" type="hidden" id="id_pl" class="form-control" rows="1" value="" />
                            <input name="id_tbl_mj" type="hidden" id="id_tbl_mj" class="form-control" rows="1" value="" />
                            <div class="form-group">
                                <label for="catatan" class="col-sm-4 col-md-3 control-label">Catatan</label>
                                <textarea name="catatan" id="catatan" class="form-control" readonly> </textarea>
                            </div>
                            <div class="form-group">
                                <label for="odp" class="col-sm-4 col-md-3 control-label">ODP</label>
                                <input name="odp" id="odp" class="form-control input-sm" rows="1" value=""/>
                            </div>
                            <div class="form-group">
                                <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat ODP</label>
                                <input name="koordinat" id="koordinat" class="form-control input-sm" rows="1" value=""/>
                            </div>
                            <div class="form-group">
                                <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                                <input name="sto" id="sto" class="form-control input-sm" rows="1" value=""/>
                            </div>
                            <div class="form-group">
                                <label for="koordinat_pelanggan" class="col-sm-4 col-md-3 control-label">Koordinat Pelanggan</label>
                                <input name="koordinat_pelanggan" id="koordinat_pelanggan" class="form-control input-sm" rows="1" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default btn-sm" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary btn-sm">OK</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
    <script type="text/javascript">
        $(function() {
            $("#odp").inputmask("AAA-AAA-A{2,3}/9{3,4}");
            var data = <?= json_encode($sto) ?>;
            $("#sto").select2({data:data});
            $(".button_check").click(function(event) {
                nt = this.getAttribute("id-dispatch");
                $.getJSON( "/jsonOrder/"+nt, function(data) {
                    $("#id_pl").val(data.id);
                    $("#id_tbl_mj").val(data.id_tbl_mj);
                    $("#catatan").val(data.catatan);
                    $("#koordinat_pelanggan").val(data.kordinat_pelanggan);
                    $("#koordinat").val(data.kordinat_odp);
                    $("#odp").val(data.nama_odp);
                })
                .done(function(data) {
                    $('#mapModal').modal({show:true});
                });
            });
            $("#check").submit(function(event){
                var odp = $("#odp").val();
                var sto = $("#sto").val();
                var koordinat = $("#koordinat").val();
                var msg = "";
                if(!odp)
                    msg += "Nama ODP isii uuuh!\n";
                if(!sto)
                    msg += "STO isii uuuh!\n";
                if(!koordinat)
                    msg += "Koordinat ODP isii uuuh!\n";
                if(msg){
                    alert(msg);
                    event.preventDefault();
                }
            });
        });
    </script>
@endsection