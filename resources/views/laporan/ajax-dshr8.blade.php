      <div class="panel panel-primary">
        <div class="panel-heading">8. LAPORAN PS BULAN INI PER TEAM</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>TEAM</th>
                <th>0P</th>
                <th>1P</th>
                <th>2P</th>
                <th>3P</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $nolp = 0;
                  $satup = 0;
                  $duap = 0;
                  $tigap = 0;
                /* --}}
                @foreach($delapan as $no => $te)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $te->k }}</td> 
                  <td>{{ $te->nolP7 }}</td>
                  <td>{{ $te->satuP7 }}</td>
                  <td>{{ $te->duaP7 }}</td>
                  <td>{{ $te->tigaP7 }}</td>
                  <td class="strong">{{ $te->total7 }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$te->total7;
                    $nolp = $nolp+$te->nolP7;
                    $satup = $satup+$te->satuP7;
                    $duap = $duap+$te->duaP7;
                    $tigap = $tigap+$te->tigaP7;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $nolp }}</td><td>{{ $satup }}</td><td>{{ $duap }}</td><td>{{ $tigap }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>