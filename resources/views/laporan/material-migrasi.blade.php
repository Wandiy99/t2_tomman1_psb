@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    REPORT MATERIAL MIGRASI
    <a href="/ms2n/sync/PS" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> Sync Now</span>
    </a>
  </h3>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default aksi" aria-label="Left Align">
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        </button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="daily">Daily</a></li>
          <li><a href="#" id="monthly">Monthly</a></li>
         
        </ul>
      </div><!-- /btn-group -->
      <div class="ins">
        <input type="text" class="form-control" id="tanggal">
      </div>
    </div><!-- /input-group -->
  </br>


  <div id="txtHint">
  </div>
  </div>
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />
  <script>
    $(function(){
      var day = {
        format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
      };
      var month = {
        format: "yyyy-mm", viewMode: 2, minViewMode: 1
      };
      $("input").datepicker(day);
      $("#daily").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(day);
      });

      $("#monthly").click(function(){
        $("input").remove();
        $(".ins").after("<input type=text class=form-control id=tanggal>");
        $("input").datepicker(month);
      });

      $(".aksi").click(function(){
        $(".loading").css({"visibility":"visible"});
        $.get("/ms2n/getJsonMigrasi/" + $("#tanggal").val(), function(text) {
         $("#txtHint").html(text);
        }).done(function(){
          $(".loading").css({"visibility":"hidden"});
        });
      });
    });
  </script>
@endsection
