<div class="panel panel-primary">
    <div class="panel-heading">Laporan</div>
    <div class="panel-body">
      <div class="list-group fiber">
			 <div class="table-responsive">  
			  <table class="table" border="0">
			    <thead>
			      <tr>
			        <th width="20">No</th>
			        <th width="100">no ticket</th>
			        @foreach($head as $h)
			        	<th width="100">{{ $h }}</th>
			    	@endforeach
			    	<th width="100">sum</th>
			      </tr>
			    </thead>
			    <tbody>
			    <?php
					$total = 0;
				?>
				@foreach($data as $no => $list) 
				  <tr>
			        <td>{{ ++$no }}</td>
			        <td>{{ $list['no_tiket'] }}</td>
					@foreach($list['Material'] as $no2 => $l2)
						<td>{{ $l2 }}</td>
					@endforeach
					<td align="right">{{ number_format($list['total_boq']) }}</td>
			      </tr>
			      	<?php
						$total = $total+$list['total_boq'];
					?>
			    @endforeach
			    <tr><td colspan="{{ count($head)+2 }}">Total</td><td align="right">{{ number_format($total) }}</td></tr>
			    </tbody>
			  </table>
			 </div>
      </div>
    </div>
  </div>
