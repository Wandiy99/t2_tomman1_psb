      <div class="panel panel-primary">
        <div class="panel-heading">3. LAPORAN SEMUA TRANSAKSI BULAN INI PER TEKNISI</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>VA</th>
                <th>TC</th>
                <th>KENDALA VA</th>
                <th>KENDALA TC</th>
                <th>UNSC DEP</th>
                <th>UNSC NR2C</th>
                <th>Tanpa Status</th>
                <th>PS</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                  $va = 0;
                  $tc = 0;
                  $kva = 0;
                  $ktc = 0;
                  $dep = 0;
                  $nr2c = 0;
                  $ps = 0;
                  $ts = 0;
                /* --}}
                @foreach($tiga as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->ns }}</td>
                  <td>{{ $t->nama }}</td> 
                  <td>{{ $t->VA3 }}</td>
                  <td>{{ $t->TC3 }}</td>
                  <td>{{ $t->kendala_va3 }}</td>
                  <td>{{ $t->kendala_tc3 }}</td>
                  <td>{{ $t->unsc_dep3 }}</td>
                  <td>{{ $t->unsc_nr2c3 }}</td>
                  <td>{{ $t->ts }}</td>
                  <td>{{ $t->ps3 }}</td>
                  <td class="strong">{{ $t->total }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$t->total;
                    $va = $va+$t->VA3;
                    $tc = $tc+$t->TC3;
                    $kva = $kva+$t->kendala_va3;
                    $ktc = $ktc+$t->kendala_tc3;
                    $dep = $dep+$t->unsc_dep3;
                    $nr2c = $nr2c+$t->unsc_nr2c3;
                    $ts = $ts+$t->ts;
                    $ps = $ps+$t->ps3;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="3">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $kva }}</td><td>{{ $ktc }}</td><td>{{ $dep }}</td><td>{{ $nr2c }}</td><td>{{ $ts }}</td><td>{{ $ps }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>