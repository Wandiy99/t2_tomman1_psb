      <div class="panel panel-primary">
        <div class="panel-heading">9. LAPORAN BATAL BULAN INI PER TEKNISI</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>BATAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                  $total = 0;
                /* --}}
                @foreach($sembilan as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->ns }}</td>
                  <td>{{ $t->nama }}</td> 
                  <td class="strong">{{ $t->batal }}</td>
                  </tr>   
                  {{-- */
                    $total = $total+$t->batal;
                  /* --}}
                @endforeach
                <tr class="strong"><td colspan="3">TOTAL</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>