<style>
html { overflow-y: scroll; } /* stop jumping page */

html,
body {
  height: 100%;
  padding: 0;
  margin: 0;
}

.photoGrid { padding: 6px; /* Spacing arround grid */ }

.photoGrid .item {
  float: left;
  margin: 2px; /* Spacing between images */
}

.photoGrid .item img {
  width: 100%;
  max-width: 100%;
  max-height: 100%;
}
</style>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<title>PhotoGrid</title>
<div class="photoGrid clearfix"> 
   @foreach($photogrid as $input)
 <?php
                  $path  = "/upload4/evidence/{$get_id->id}/$input";
                  $path2 = "/upload3/evidence/{$get_id->id}/$input";
                  $th    = "$path-th.jpg";
                  $th2   = "$path2-th.jpg";
                  $img   = "$path.jpg";
                  $img2  = "$path2.jpg";
                  $flag  = "";
                  $name  = "flag_".$input;

                ?>
                @if (file_exists(public_path().$th))
                  <a class="item" href="{{ $img }}">
                    <img src="{{ $img }}" alt="{{ $input }}" width="200" />
                  </a>
                  <?php
                    $flag = 2;
                  ?>
                @elseif (file_exists(public_path().$th2))
                  <a class="item" href="{{ $img2 }}">
                    <img src="{{ $img2 }}" alt="{{ $input }}" width="200" />
                  </a>
                  <?php
                    $flag = 2;
                  ?>
                @else
                  <!-- <img src="/image/placeholder.gif" width="100" alt="" /> -->
                @endif
    @endforeach
  <!-- <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> <a class="item" href="http://placekitten.com/g/1200/600"><img src="http://placekitten.com/g/1200/600"></a> <a class="item" href="http://placekitten.com/g/600/1200"><img src="http://placekitten.com/g/600/1200"></a> <a class="item" href="http://placekitten.com/g/600/600"><img src="http://placekitten.com/g/600/600"></a> <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> <a class="item" href="http://placekitten.com/g/1200/600"><img src="http://placekitten.com/g/1200/600"></a> <a class="item" href="http://placekitten.com/g/1200/600"><img src="http://placekitten.com/g/1200/600"></a> <a class="item" href="http://placekitten.com/g/1200/600"><img src="http://placekitten.com/g/1200/600"></a> <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> <a class="item" href="http://placekitten.com/g/600/600"><img src="http://placekitten.com/g/600/600"></a> <a class="item" href="http://placekitten.com/g/600/600"><img src="http://placekitten.com/g/600/600"></a> <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> <a class="item" href="http://placekitten.com/g/1000/1200"><img src="http://placekitten.com/g/1000/1200"></a> <a class="item" href="http://placekitten.com/g/1000/1200"><img src="http://placekitten.com/g/1000/1200"></a> <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> <a class="item" href="http://placekitten.com/g/1200/1200"><img src="http://placekitten.com/g/1200/1200"></a> </div> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="/js/photogrid.js"></script> 
<script>
$(function() {

  var defaults = {
    itemSelector: ".item", // item selecor ;-)
    resize: true, // automatic reload grid on window size change
    rowHeight: $(window).height() / 2, // looks best, but needs highres thumbs
    callback: function() {} // fires when layouting grid is done
  };


  $(window).load(function() { // or use https://github.com/desandro/imagesloaded
    $('.photoGrid').photoGrid({
      //rowHeight: "200"
    });
  });
  
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>