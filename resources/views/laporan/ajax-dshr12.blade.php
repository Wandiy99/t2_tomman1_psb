      <div class="panel panel-primary">
        <div class="panel-heading">Visual Board {{ date('M Y') }}</div>
        <div class="panel-body">
          {{-- */
            $month = date('m');
            $year = date('Y');
            $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
          /* --}}
          <div class="table-responsive strong">          
            <table class="table">
              <thead>
                <tr>
                <th rowspan="2">#</th>
                <th rowspan="2">Nik/Nama</th>
                <th colspan="{{ $days_in_month }}">PRODUKTIFITAS TIM DSHR / HARI</th>
                <th colspan="12">JUMLAH</th><tr>
                @for($list_day = 1; $list_day <= $days_in_month; $list_day++)
                  <th width="3%">{{ $list_day }}</th>
                @endfor
                @for($list_month = 1; $list_month <= 12; $list_month++)
                  <th width="3%">B{{ $list_month }}</th>
                @endfor
                </tr>
              </thead>
              <tbody>
                @foreach($tiga as $no => $data)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $data->ns }}/{{ $data->nama }}</td>
                  {{-- */
                    $total_day = 0;
                  /* --}}
                  @for($list_day = 1; $list_day <= $days_in_month; $list_day++)
                    {{-- */
                      $tgl = date('Y-m-').$list_day;
                      $total_day = 0;
                    /* --}}
                    @if(strlen($list_day) == 1)
                      {{-- */
                        $tgl = date('Y-m-0').$list_day;
                      /* --}}
                    @endif
                    @foreach($visual as $v)
                      @if($v->tanggal_deal == $tgl && $v->nik_sales == $data->ns)
                      {{-- */
                        $total_day++;
                      /* --}}
                      @endif
                    @endforeach
                    @if(date('Y-m-d') == $tgl)
                      <td><span class="label label-info">{{ $total_day }}</span></td>
                    @else
                      <td>{{ $total_day }}</td>
                    @endif
                  @endfor
                  {{-- */
                    $total_month = 0;
                  /* --}}
                  @for($list_month = 1; $list_month <= 12; $list_month++)
                    {{-- */
                      $tgl = date('Y').$list_month;
                      $total_month = 0;
                    /* --}}
                    @if(strlen($list_month) == 1)
                      {{-- */
                        $tgl = date('Y0').$list_month;
                      /* --}}
                    @endif
                    @foreach($visual as $v)
                      @if($v->bln == $tgl && $v->nik_sales == $data->ns)
                      {{-- */
                        $total_month++;
                      /* --}}
                      @endif
                    @endforeach
                    @if(date('Ym') == $tgl)
                      <td><span class="label label-info">{{ $total_month }}</span></td>
                    @else
                      <td>{{ $total_month }}</td>
                    @endif
                  @endfor
                  </tr>
                @endforeach
                {{-- */
                  $total_day = 0;
                /* --}}
                <tr><td colspan="2">Total</td>
                  @for($list_day = 1; $list_day <= $days_in_month; $list_day++)
                    {{-- */
                      $tgl = date('Y-m-').$list_day;
                      $total_day = 0;
                    /* --}}
                    @if(strlen($list_day) == 1)
                      {{-- */
                        $tgl = date('Y-m-0').$list_day;
                      /* --}}
                    @endif
                    @foreach($visual as $v)
                      @if($v->tanggal_deal == $tgl)
                      {{-- */
                        $total_day++;
                      /* --}}
                      @endif
                    @endforeach
                    @if(date('Y-m-d') == $tgl)
                      <td><span class="label label-info">{{ $total_day }}</span></td>
                    @else
                      <td>{{ $total_day }}</td>
                    @endif
                  @endfor
                  {{-- */
                    $total_month = 0;
                  /* --}}
                  @for($list_month = 1; $list_month <= 12; $list_month++)
                    {{-- */
                      $tgl = date('Y').$list_month;
                      $total_month = 0;
                    /* --}}
                    @if(strlen($list_month) == 1)
                      {{-- */
                        $tgl = date('Y0').$list_month;
                      /* --}}
                    @endif
                    @foreach($visual as $v)
                      @if($v->bln == $tgl)
                      {{-- */
                        $total_month++;
                      /* --}}
                      @endif
                    @endforeach
                    @if(date('Ym') == $tgl)
                      <td><span class="label label-info">{{ $total_month }}</span></td>
                    @else
                      <td>{{ $total_month }}</td>
                    @endif
                  @endfor
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>