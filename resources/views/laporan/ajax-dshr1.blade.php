      <div class="panel panel-primary">
        <div class="panel-heading">1. LAPORAN TRANSAKSI HARI INI PER TEKNISI</div>
        <div class="panel-body">
          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                <th>#</th>
                <th>NAMA</th>
                <th>VA</th>
                <th>TC</th>
                <th>UNSC DEP</th>
                <th>UNSC NR2C</th>
                <th>Tanpa Status</th>
                <th>PS</th>
                <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
                {{-- */
                    $total = 0;
                    $va = 0;
                    $tc = 0;
                    $dep = 0;
                    $nr2c = 0;
                    $ps = 0;
                    $ts = 0;
                /* --}}
                @foreach($satu as $no => $t)
                  <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $t->nama }}</td> 
                  <td>{{ $t->VA1 }}</td>
                  <td>{{ $t->TC1 }}</td>
                  <td>{{ $t->unsc_dep1 }}</td>
                  <td>{{ $t->unsc_nr2c1 }}</td>
                  <td>{{ $t->ts }}</td>
                  <td>{{ $t->ps1 }}</td>
                  <td class="strong">{{ $t->total }}</td>
                  </tr>
                  {{-- */
                    $total = $total+$t->total;
                    $va = $va+$t->VA1;
                    $tc = $tc+$t->TC1;
                    $dep = $dep+$t->unsc_dep1;
                    $nr2c = $nr2c+$t->unsc_nr2c1;
                    $ps = $ps+$t->ps1;
                    $ts = $ts+$t->ts;
                /* --}}
                @endforeach
                <tr class="strong"><td colspan="2">TOTAL</td><td>{{ $va }}</td><td>{{ $tc }}</td><td>{{ $dep }}</td>
                <td>{{ $nr2c }}</td><td>{{ $ts }}</td><td>{{ $ps }}</td><td>{{ $total }}</td></tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>