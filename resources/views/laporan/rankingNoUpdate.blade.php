@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
<div class="panel panel-default ">
  <div class="panel-heading">Ranking No Update</div>
    <div class="panel-body table-responsive">
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-condensed">
          <thead>
            <tr>
              <th>#</th>
              <th>TL</th>
              <th>Jumlah</th>
              <th>Tim/Total(list)</th>
            </tr>
          </thead>
          <tbody>

            @foreach($dash as $no => $d)
            <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $d['TL'] }}</td>
              <td>{{ $d['jumlah'] }}</td>
              <td>
                @foreach($d['tim'] as $t)
                  <span class="label label-primary">{{ $t['uraian'] }}</span>
                  <span class="label label-danger">{{ $t['total'] }}</span>
                  ( 
                  @foreach($t['list'] as $l)
                    <span class="label label-warning">{{ $l }}</span>
                  @endforeach
                   )
                  </br>
                @endforeach
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
