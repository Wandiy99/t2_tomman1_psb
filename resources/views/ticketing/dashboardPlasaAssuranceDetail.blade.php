@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL TICKETING INT PERIODE {{ $startDate }} S/D {{ $endDate }} AREA {{ $area }} STATUS {{ $grup }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>AREA</th>
                        <th>STO</th>
                        <th>INCIDENT</th>
                        <th>CUSTOMER_NAME</th>
                        <th>CONTACT_NAME</th>
                        <th>CONTACT_PHONE</th>
                        <th>SUMMARY</th>
                        <th>SERVICE_ID</th>
                        <th>INCIDENT_SYMPTOM</th>
                        <th>REPORTED_DATE</th>
                        <th>STATUS_TEKNISI</th>
                        <th>TGL_STATUS_TEKNISI</th>
                        <th>TIM</th>
                        <th>SEKTOR</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $num => $result)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->area ? : 'NON AREA' }}</td>
                        <td>{{ $result->Workzone }}</td>
                        <td>{{ $result->Incident }}</td>
                        <td>{{ $result->Customer_Name }}</td>
                        <td>{{ $result->Contact_Name }}</td>
                        <td>{{ $result->Contact_Phone }}</td>
                        <td>{{ $result->Summary }}</td>
                        <td>{{ $result->Service_ID }}</td>
                        <td>{{ $result->Incident_Symptom }}</td>
                        <td>{{ $result->Reported_Datex }}</td>
                        <td>{{ $result->status_teknisi }}</td>
                        <td>{{ $result->tgl_status_teknisi }}</td>
                        <td>{{ $result->tim }}</td>
                        <td>{{ $result->sektor }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL DASHBOARD PLASA TICKETING INT BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection