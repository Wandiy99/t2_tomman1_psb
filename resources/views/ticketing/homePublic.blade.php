<!DOCTYPE html>
<html lang="en">
<head>
<style> 
.search {
  width: 100%;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 2px;
  font-size: 16px;
  background-color: white;
  background-position: 10px 10px; 
  background-repeat: no-repeat;
  padding: 12px 20px 1px 40px;
  transition: width 0.4s ease-in-out;
  }

  h3 ,option, .form-button {
    text-align: center;
  }
</style>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="https://static.wikia.nocookie.net/logopedia/images/7/70/Indihome.png" type="image/png">
<meta name="description" content="Pengaduan Gangguan IndiHome secara Online hanya untuk pelanggan yang berada pada Provinsi Kalimantan Selatan | TOMMAN">
<meta name="keywords" content="indihome care kalsel,indihome kalsel,indihome care,pasang baru indihome banjarmasin,pengaduan gangguan indihome,indihome banjarmasin,indihome kalsel,pengaduan indihome,pengaduan indihome kalsel,my indihome,indihome kalsel tomman,tomman,tomman kalsel,tomman app,indihome care,indihome care kalsel,indihome care tomman,tomman creators,banjarmasin,banjarbaru,kalsel,kalimantan selatan,kalsel hibat">
<meta name="copyright" content="biawak.tomman.app"/>
<meta name="author" content="IndiHome Care Kalsel - TOMMAN">
{{-- Google Tag Manager --}}
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQFRDF6');</script>
{{-- End Google Tag Manager --}}
{{-- Global site tag (gtag.js) - Google Analytics --}}
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-204467533-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-204467533-1');
</script>

<title>IndiHome Care Kalsel</title>

<link rel="stylesheet" href="https://colorlib.com/etc/regform/colorlib-regform-7/fonts/material-icon/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="https://colorlib.com/etc/regform/colorlib-regform-7/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
<div class="main">
<section class="signup">
<div class="container">
<div class="signup-content">
<div class="signup-form">
<center><h3 class="form-title">Pengaduan Gangguan IndiHome</h3></center>
@include('partial.alerts')
<center>
<p>Silahkan Masukan No Internet Bapak/Ibu Dibawah Sini</p>
<div class="signup-image">
<form method="GET">
    <div class="row">
      <div class="input-group col-sm-10">
          <input type="number" minlength="11" maxlength="12" class="form-control inputSearch" placeholder="Cari No Indihome Anda ..." name="inet" id="inet" />
          <span class="input-group-btn">
              <button class="btn btn-default search">
                  <span class="glyphicon glyphicon-search"></span>
              </button>
          </span>
      </div>
    </div>
</form>

<figure><img src="https://3.bp.blogspot.com/-MQDe64tUCgU/W_Tm4B8DSSI/AAAAAAAAAGg/E81EcoJFyiYWSUQESwimGnTn9YhZc2LzACLcBGAs/s1600/indihome.png" alt="sing up image"></figure>
<p style="font-size:80%;">Copyright © Since 2017 - {{ date('Y') }} TOMMAN<br/>All Rights Reserved</p>
</div>
</center>
</div>
<form method="POST" enctype="multipart/form-data">
<div class="form-group">
<label for="Service_No"><i class="zmdi zmdi-local-offer"></i></label>
<input type="number" minlength="12" name="Service_No" id="Service_No" value="{{ @$data->internet ? : '' }}" placeholder="No Indihome contoh: 1612xxx.. atau 1622xxx.." />
{!! $errors->first('Service_No','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="ndemPots"><i class="zmdi zmdi-local-offer"></i></label>
<input type="number" name="ndemPots" id="ndemPots" value="{{ @$data->ndemPots ? : @$cari ? : '' }}" placeholder="No Telp contoh: 051xxx.." />
{!! $errors->first('ndemPots','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Customer_Name"><i class="zmdi zmdi-account material-icons-name"></i></label>
<input type="text" name="Customer_Name" id="Customer_Name" value="{{ @$data->orderName ? : '' }}" placeholder="Nama Pelanggan" />
{!! $errors->first('Customer_Name','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Contact_Phone"><i class="zmdi zmdi-pin-account"></i></label>
<input type="text" name="Contact_Name" id="Contact_Name" value="{{ @$data->orderName ? : '' }}" placeholder="Nama Kontak" />
{!! $errors->first('Contact_Name','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Contact_Phone"><i class="zmdi zmdi-local-phone"></i></label>
<input type="number" minlength="11" name="Contact_Phone" id="Contact_Phone" placeholder="No Handphone" />
{!! $errors->first('Contact_Phone','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Workzone"></label>
<select class="form-control" name="Workzone" id="Workzone">
<option value="" selected disabled>--- Pilih Zona Area ---</option>
@foreach ($get_area_alamat as $area_alamat)
<option data-subtext="description 1" value="{{ $area_alamat->id }}" <?php if (@$area_alamat->id==@$data->sto) { echo "Selected"; } else { echo ""; } ?>>{{ @$area_alamat->text }}</option>
@endforeach
</select>
{!! $errors->first('Workzone','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Datek"><i class="zmdi zmdi-pin"></i></label>
<input type="text" name="Datek" id="Datek" value="{{ @$data->alproname ? : '' }}" placeholder="Datek Jaringan" readonly/>
</div>
<div class="form-group">
<label for="ncliOrder"><i class="zmdi zmdi-local-offer"></i></label>
<input type="text" name="ncliOrder" id="ncliOrder" value="{{ @$data->orderNcli ? : '' }}" placeholder="NCLI" readonly/>
</div>
<div class="form-group">
<label for="alamat"><i class="zmdi zmdi-pin-drop"></i></label>
<input type="text" name="alamat" id="alamat" value="{{ @$data->orderAddr ? : '' }}" placeholder="Alamat Lengkap" />
{!! $errors->first('alamat','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="JENIS_GGN"></label>
<select class="form-control" name="JENIS_GGN">
<option value="" selected disabled>--- Pilih Jenis Gangguan ---</option>
<option value="FISIK"> FISIK </option>
<option value="LOGIK"> LOGIK </option>
</select>
{!! $errors->first('JENIS_GGN','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Service_Type"></label>
<select class="form-control" name="Service_Type">
<option value="" selected disabled>--- Pilih Kategori Layanan ---</option>
<option value="INTERNET">INTERNET</option>
<option value="TELP">TELP</option>
<option value="USEETV">USEETV</option>
</select>
{!! $errors->first('Service_Type','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="gangguan"></label>
<select class="form-control" name="gangguan">
<option value="" selected disabled>--- Pilih Kategori Gangguan ---</option>
@foreach ($get_keluhan as $keluhan)
<option value="{{ $keluhan->keluhan }}">{{ $keluhan->keluhan }}</option>
@endforeach
</select>
{!! $errors->first('gangguan','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group">
<label for="Incident_Symptom"></label>
<textarea rows="8" cols="55" name="Incident_Symptom" id="Incident_Symptom" placeholder="Deskripsi Keluhan"></textarea>
{!! $errors->first('Incident_Symptom','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<div class="form-group form-button">
<input type="submit" name="signup" id="signup" class="form-submit" onclick="openWin()" value="Submit" />
</div>
</form>
</div>
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- Google Tag Manager (noscript) --}}
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQFRDF6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
{{-- End Google Tag Manager (noscript) --}}
<script>
    // redirect page for adsense
    // let newWindow = open('https://tomman.app','tommanApp','width=300,height=300');
    // newWindow.onload = function() {
    //   newWindow.close();
    // };

    function openWin() {
      window.open('https://tomman.app','ads','width=400,height=200');
    };
</script>
</div>
</body>
</html>