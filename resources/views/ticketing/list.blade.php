@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;
    }
    input,textarea {
      background-color: #ecf0f1;
      padding : 5px;
    }
    table td {
      padding : 5px;
    }
  </style>
  <h3>YOUR TICKET PROGRESS</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          
        </div>
        <div>
          <table class="table">
            <tr>
              <td>No</td>
              <td>Incident</td>
              <td>Reported Date</td>
              <td>Customer Name</td>
              <td>Contact Name</td>
              <td>Contact Phone</td>
              <td>Summary</td>
              <td>Team</td>
              <td>Progress</td>
              <td>Date Status</td>
            </tr>
            @foreach ($query as $no => $result)
            <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $result->Incident }}</td>
              <td>{{ $result->Reported_Date }}</td>
              <td>{{ $result->Customer_Name }}</td>
              <td>{{ $result->Contact_Name }}</td>
              <td>{{ $result->Contact_Phone }}</td>
              <td>{{ $result->Summary }}</td>
              <td>{{ $result->uraian ? : 'Undispatch' }}</td>
              <td>{{ $result->laporan_status ? : 'Need Progress' }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection