@extends('layout')

@section('content')
  	@include('partial.alerts')
   	<a href="/" class="btn btn-sm btn-default">
    	<span class="glyphicon glyphicon-arrow-left"></span>
  	</a><br><br>
  	<div class="panel panel-primary">
  		<div class="panel-heading">
  			List Detail Ticket Tomman Sektor : {{ $sektor }}
  		</div>

  		<div class="panel-body">
  			<table class="table table-bordered">
  				<tr>
  					<th>NO</th>
  					<th>Area</th>
  					<th>Tiket</th>
            <th>Report Data</th>
            <th>Service No</th>
            <th>Service Type</th>
            <th>No Internet</th>
  					<th>Customer Name</th>
            <th>Contact Phone</th>
            <th>Summary</th>
            <th>Team</th>
  					<th>Status Laporan</th>
  					<th>Action</th>
  					<th>Penyebab</th>
            <th>Date Status</th>
  				</tr>

  				@foreach($getData as $no=>$data)
  					<tr>
  						<td>{{ ++$no }}</td>
  						<td>{{ $data->sektor_asr }}</td>

              @if($sektor<>'UNDISPATCH')
                  <td><a href="/tiket/{{ $data->id_dt }}">{{ $data->Incident }}</td>
              @else
                  <td><a>{{ $data->Incident }}</td>
              @endif

              <td>{{ $data->Reported_Date }}</td>
              <td>{{ $data->Service_ID }}</td>
              <td>{{ $data->Service_Type }}</td>
              <td>{{ $data->no_internet }}</td>
              <td>{{ $data->Customer_Name }}</td>
              <td>{{ $data->Contact_Phone }}</td>
              <td>{{ $data->Summary }}</td>

              @if($sektor<>'UNDISPATCH')
                  <td>{{ $data->uraian }}</td>
  						    <td>{{ $data->laporan_status }}</td>
  						     <td>{{ $data->action }}</td>
  						    <td>{{ $data->penyebab }}</td>
                  <td>{{ $data->updated_at }}</td>
  						@else
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
              @endif
  					</tr>
  				@endforeach
  			</table>
  		</div>
  	</div>
@endsection