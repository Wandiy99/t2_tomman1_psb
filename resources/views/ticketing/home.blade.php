@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;

    }
  </style>
  <h3>Customer Info</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Search
        </div>
        <div class="panel-body">
          <form class="form-group" role="search" method="post">
            <div class="input-group">
              <input type="text" name="customer" value="{{ $customer }}" class="form-control" placeholder="Cari Customer ...." />
              <span class="input-group-btn">
                <button type="submit" name="submit" value="submit" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button>
              </span>
              {!! $errors->first('customer', '<p class=help-block>:message</p>') !!}
            </div>
          </form>
        </div>
      </div>
    </div>
    @if ($customer <> "")

    <div class="col-sm-8">
    <div class="col-sm-12">
    @foreach ($get_history_order_x as $history_order_x)
      @if($history_order_x->laporan_status=="UP" || $history_order_x->laporan_status=="UP UNSPEC")
      <a href="/createTicket/{{ $customer }}" class="btn btn-warning col-sm-12">BUAT PENGADUAN via Tomman</a>
      @elseif($history_order_x->laporan_status=="" || $history_order_x->laporan_status=="NEED PROGRESS")
      <a class="btn btn-danger col-sm-12">Tiket Sudah Ada !</a>
      @endif
    @endforeach
    <?php if(empty($get_history_order_x)){ ?>
    <a href="/createTicket/{{ $customer }}" class="btn btn-warning col-sm-12">BUAT PENGADUAN via Tomman</a>
    <?php } ?>
      <div class="panel panel-info">
        <div class="panel-heading">
          Customer Info
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-6">
              <table class="customer_info" width="100%">
                <tr>
                  <td width=100 class="customer_field">Nama</td>
                  <td class="customer_value">{{ $customer_info->orderName }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">NDEM - POTS</td>
                  <td class="customer_value">{{ $customer_info->ndemPots }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">NDEM - INET</td>
                  <td class="customer_value">{{ $customer_info->ndemSpeedy }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">Alamat</td>
                  <td wraptext class="customer_value">{{ $customer_info->orderAddr }}</td>
                </tr>

              </table>
            </div>
            <div class="col-sm-6">
              <table class="table" width="100%">

                <tr>
                  <td width=100 class="customer_field">Tgl PS</td>
                  <td class="customer_value">{{ $customer_info->orderDatePs }}</td>
                </tr>
                <tr>
                  <td width=100 class="customer_field">SC.ID</td>
                  <td class="customer_value"><a href="/{{ $customer_info->id_dt }}">{{ $customer_info->orderId }}</a></td>
                </tr>


              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if ($ibooster <> "")
    <div class="col-sm-12">
      <div class="panel panel-warning">
        <div class="panel-heading">
          Hasil Ukur
        </div>
        <div class="panel-body">
          @if ($gamas <> "")
          <div class="alert alert-danger">{{ $gamas." ".$Calling_Station_Id }}</div>
          @endif
          <?php
            $data =  @(object) $ibooster[0];
          ?>
          <div class="row">
            <div class="col-sm-4">
              ND : {{ $data->ND }}<br />
              IP_Embassy : {{ $data->IP_Embassy }}<br />
              Type : {{ $data->Type }}<br />
              Calling_Station_Id : {{ $data->Calling_Station_Id }}<br />
              IP_NE : {{ $data->IP_NE }}<br />
              ONU_Link_Status : {{ $data->ONU_Link_Status }}<br />
            </div>
            <div class="col-sm-4">
              OLT_Tx : {{ $data->OLT_Tx }}<br />
              OLT_Rx : {{ $data->OLT_Rx }}<br />
              ONU_Tx : {{ $data->ONU_Tx }}<br />
              ONU_Rx : {{ $data->ONU_Rx }}<br />
              Framed_IP_Address : {{ $data->Framed_IP_Address }}<br />
            </div>
            @if ($data->ONU_Link_Status=="LOS")
            <a href="#" class="btn btn-warning">SILAHKAN OPEN TIKET via MyCX</a>
            @endif
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
  <div class="col-sm-4">
    <div class="panel panel-info">
      <div class="panel-heading">History Order</div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <td>Tiket</td>
            <td>Tgl Open</td>
            <td>Status</td>
          </tr>
          @foreach ($get_history_order as $history_order)
          <tr>
            <td><a href="/tiket/{{ $history_order->id_dt }}">{{ $history_order->Incident }}</a></td>
            <td>{{ $history_order->Reported_Date }}</td>
            <td>{{ $history_order->laporan_status ? : "ANTRIAN" }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
    @endif
  </div>
@endsection
