<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/online_ticketing/assets/images/indihome.png" type="image/png">
    <meta name="description" content="Pengaduan Gangguan Indihome secara Online hanya untuk pelanggan yang berada pada Provinsi Kalimantan Selatan | TOMMAN">
    <meta name="keywords" content="psb bjm tomman app,telkom care kalsel,telkomcare kalsel,telkom care,pengaduan gangguan indihome,indihome banjarmasin,indihome kalsel,pengaduan indihome,pengaduan indihome kalsel,my indihome,telkom care kalsel tomman,tomman,tomman kalsel,tomman app,indihome care,indihome care kalsel,indihome care tomman,tomman creators,banjarmasin,banjarbaru,kalsel,kalimantan selatan,kalsel hibat">
    <meta name="copyright" content="biawak.tomman.app"/>
    <meta name="author" content="Indihome Care {{ $city }} - TOMMAN">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <title>Indihome Care Kalsel - {{ $city }}</title>

    <!-- Bootstrap core CSS -->
    <link href="/online_ticketing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="/online_ticketing/assets/css/fontawesome.css">
    <link rel="stylesheet" href="/online_ticketing/assets/css/templatemo-space-dynamic.css">
    <link rel="stylesheet" href="/online_ticketing/assets/css/animated.css">
    <link rel="stylesheet" href="/online_ticketing/assets/css/owl.css">
  </head>

<body>

  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="/" class="logo">
              <h4><span>INDIHOME CARE</span> KALSEL</h4>
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li class="scroll-to-section"><a href="#home" class="active">Home</a></li>
              <li class="scroll-to-section"><a href="#about">About</a></li>
              <li class="scroll-to-section"><a href="#paket_indihome">Paket IndiHome</a></li>
              <li class="scroll-to-section"><a href="#services">Layanan</a></li>
              <li class="scroll-to-section"><a href="#lapor_gangguan">Lapor Gangguan</a></li>
              <li class="scroll-to-section"><div class="main-red-button"><a href="https://wa.me/6281254284618"><i class="fa fa-phone"></i>&nbsp; Hotline</a></div></li>
            </ul>        
            <a class='menu-trigger'>
                <span>Menu</span>
            </a>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->

  <div class="main-banner wow fadeIn" id="home" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="left-content header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <h6>Selamat Datang di Indihome Care Kalsel - {{ $city }}</h6>
                <h2>We Care, We're Here to <em>Give Solutions</em> <span>24/7</span></h2>
                <p>Memberikan <b style="color: #03a4ed">Solusi Terbaik</b> untuk Keluhan atau Kendala Layanan Kamu dan Informasi Seputar Layanan <b style="color: #fe3f40">IndiHome</b> di <b style="color: #2a2a2a">Kalimantan Selatan (KALSEL)</b>.<br />#WeAreHereWeDoCare</p>
                <div class="main-red-button"><a href="https://bit.ly/PasangBaruIndiHomeKalsel" target="_blank"><i class="fa fa-chain"></i> Promo Terbaru {{ date('Y') }} Pasang Baru IndiHome {{ $city }} Disini!</a></div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="/online_ticketing/assets/images/indihome-girls.png" alt="team meeting">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="about" class="about-us section">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="left-image wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
            <img src="/online_ticketing/assets/images/about-left-image.png" alt="person graphic">
          </div>
        </div>
        <div class="col-lg-8 align-self-center">
          <div class="services">
            <div class="row">
              <div class="col-lg-6">
                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="right-text">
                    <h4>Mudah</h4>
                    <p>Tidak perlu ribet untuk melapor gangguan, cukup mengisi form yang disediakan.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                  <div class="right-text">
                    <h4>Cepat</h4>
                    <p>Keluhan kamu secepatnya mendapat tanggapan dari pihak kami.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                  <div class="right-text">
                    <h4>Tersedia 24/7</h4>
                    <p>Kapan pun dan di mana pun kami siap menerima laporan layanan kamu.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                  <div class="right-text">
                    <h4>Informatif</h4>
                    <p>Kami akan terus memberikan informasi seputar layanan yang kami berikan.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="paket_indihome" class="our-portfolio section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading  wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
            <h2>Pilihan <em>Paket</em> <span>Indihome</span> Terbaru <span>{{ $city }}</span> {{ date('Y') }}</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-sm-6">
          <!-- <a href="#"> -->
            <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
              <div class="hidden-content">
                <p>Bebas nelpon 100 menit & Disney Plus Hotstar & Diskon 50% Biaya Pemasangan</p>
              </div>
              <div class="showed-content">
                <img src="/online_ticketing/assets/images/portfolio-image.png" alt="">
                <br /><br />
                <h6>2P (Internet + Phone)</h6>
                <p>Internet to 30 Mbps<br />Rp. 315.000/Bulan</p>
              </div>
            </div>
          <!-- </a> -->
        </div>
        <div class="col-lg-3 col-sm-6">
          <!-- <a href="#"> -->
            <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.5s">
              <div class="hidden-content">
                <p>UseeTV Entry + Seatoday + UseeTV Go & Disney+ Hotstar & Diskon 50% Biaya Pemasangan</p>
              </div>
              <div class="showed-content">
                <img src="/online_ticketing/assets/images/portfolio-image.png" alt="">
                <br /><br />
                <h6>3P (Internet + TV + Phone)</h6>
                <p>Internet Up to 30 Mbps<br />Rp. 385.000/Bulan</p>
              </div>
            </div>
          <!-- </a> -->
        </div>
        <div class="col-lg-3 col-sm-6">
          <!-- <a href="#"> -->
            <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.4s">
              <div class="hidden-content">
                <p>UseeTV Entry + Seatoday + UseeTV Go & IndiMovie 2 & Disney+ Hotstar & iFlix, Catchplay , Mola, Vidio</p>
              </div>
              <div class="showed-content">
                <img src="/online_ticketing/assets/images/portfolio-image.png" alt="">
                <br /><br />
                <h6>3P (Internet + TV + Phone)</h6>
                <p>Internet Up to 50 Mbps<br />Rp. 615.000/Bulan</p>
              </div>
            </div>
          <!-- </a> -->
        </div>
        <div class="col-lg-3 col-sm-6">
          <!-- <a href="#"> -->
            <div class="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.6s">
              <div class="hidden-content">
                <p>UseeTV Entry & IndiMovie 2 & Disney+ Hotstar & iFlix, Catchplay, Mola, Vidio</p>
              </div>
              <div class="showed-content">
                <img src="/online_ticketing/assets/images/portfolio-image.png" alt="">
                <br /><br />
                <h6>2P (Internet + TV)</h6>
                <p>Internet Up to 100 Mbps<br />Rp. 945.000/Bulan</p>
              </div>
            </div>
          <!-- </a> -->
        </div>
      </div>
    </div>
  </div>

  <div id="services" class="our-services section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 align-self-center  wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s">
          <div class="left-image">
            <img src="/online_ticketing/assets/images/indihome-tech.jpg" style="width: 80%" alt="">
          </div>
        </div>
        <div class="col-lg-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s">
          <div class="section-heading">
            <h2>Teknisi yang <em>Profesional</em> dan <span>Ahli pada Bidangnya</span></h2>
            <p>Kami memberikan pelayanan semaksimal mungkin untuk menangani keluhan kamu, dengan teknisi yang mempunyai AKHLAK.</p>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Amanah</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Kompeten</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Harmonis</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Loyal</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Adaptif</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="fulltank-bar progress-skill-bar">
                <h4>Kolaboratif</h4>
                <span>100%</span>
                <div class="filled-bar"></div>
                <div class="full-bar"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="lapor_gangguan" class="contact-us section">
    <div class="container">
      <div class="row">

        <div class="col-lg-6 align-self-center wow fadeInLeft" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <div class="section-heading">
            <h2>Layanan Lapor Gangguan IndiHome</h2>
            <p>Silahkan masukan Nomer Internet atau Nomer Tiket anda pada kolom pencarian.</p>
            <div class="phone-info">
              <h4>Hotline Kami : <span><i class="fa fa-phone"></i> <a href="#">0812-5428-4618</a></span></h4>
            </div>
          </div>
        </div>

        <div class="col-lg-6 wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" method="GET">
            <div class="row">
              <div class="col-lg-8">
                <fieldset>
                  <input type="text" name="number" id="number" placeholder="Nomor Internet atau Nomor Tiket" required="">
                </fieldset>
              </div>
              <div class="col-lg-4">
                <fieldset>
                  <button type="submit" id="form-submit" class="main-button ">Search</button>
                </fieldset>
              </div>
            </div>
            <div class="contact-dec">
              <img src="/online_ticketing/assets/images/contact-decoration.png" alt="">
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.25s">
          <p>© Copyright {{ date('Y') }} <a rel="nofollow" href="/online/ticketing">Telkom Care Kalsel</a> All Rights Reserved.<br />{{ $city }} - {{ $region }} - {{ $country }}</p>
        </div>
      </div>
    </div>
  </footer>
  <!-- Scripts -->
  <script src="/online_ticketing/vendor/jquery/jquery.min.js"></script>
  <script src="/online_ticketing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/online_ticketing/assets/js/owl-carousel.js"></script>
  <script src="/online_ticketing/assets/js/animation.js"></script>
  <script src="/online_ticketing/assets/js/imagesloaded.js"></script>
  <script src="/online_ticketing/assets/js/templatemo-custom.js"></script>

</body>
</html>