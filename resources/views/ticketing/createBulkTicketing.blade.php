@extends('layout')

@section('content')

@include('partial.alerts')

<div class="row">
  <div class="col-md-12">
    <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-12">
          <h3>Created Ticketing Order</h3>

          <div class="form-group">
          <label class="control-label" for="inets">Format yang di Gunakan Pisah dengan simbol Titik Koma dan Jangan Gunakan Baris Baru (Enter)</label>
              <textarea name="inets" id="inets" class="form-control" rows="5" required /></textarea>
              {!! $errors->first('inets','<p class="label label-danger">:message</p>') !!}
          </div>
      </div>
      
      <div class="col-md-12">
          <button class="btn btn-rounded btn-sm btn-block btn-success"><span class="glyphicon glyphicon-upload"></span>&nbsp; Create</button>
      </div>
    </form>
  </div>

  @if (@$result != null)
  <div class="col-md-12">
      <br />
      <span>{{ $result }}</span>
  </div>
  @endif

</div>

@endsection