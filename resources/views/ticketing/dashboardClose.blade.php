@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
  </style> 
  <center>
    <h3>Dashboard Progress Ticket INT</h3> 
  </center>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <table width="100%">
            <tr>
              <th>SEKTORx</th>
              <th>UNDISPATCH</th>
              <th>NP</th>
              <th>KP</th>
              <th>KT</th>
              <th>OGP</th>
              <th>CLOSE</th>
              <th>% Close</th>
            </tr>
            <?php
              $total_undispatch = 0;
              $total_sisa = 0;
              $total_kp = 0;
              $total_kt = 0;
              $total_ogp = 0;
              $total_up = 0;
              
            ?>
            @foreach ($query as $num=>$result)
            <?php
              $total_undispatch += $result->UNDISPATCH;
              $total_sisa += $result->SISA;
              $total_kp += $result->KP;
              $total_kt += $result->KT;
              $total_ogp += $result->OGP;
              $total_up += $result->UP;
              #$jumlah = $total_undispatch + $total_sisa + $total_kp + $total_kt + $total_ogp + $total_up;
              $jumlah = $result->UNDISPATCH + $result->SISA + $result->KP + $result->KT + $result->OGP + $result->UP;
              $presentase = ($result->UP / $jumlah) * 100;
            ?>
            <tr>
              <td>{{ $result->sektor_asr ? : 'UNDEFINED SEKTOR' }}</td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/UNDISPATCH/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->UNDISPATCH }}</a></td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/SISA/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->SISA }}</td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/KP/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->KP }}</td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/KT/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->KT }}</td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/OGP/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->OGP }}</td>
              <td align="center" width="100"><a href="/ticketing/dashboardCloseList/UP/{{ $result->sektor_asr ? : 'UNDEFINED' }}/{{ $date }}">{{ $result->UP }}</td>
              <td align="center">{{ round($presentase,2) }}%</td>
            </tr>
            @endforeach
            <?php 
            $jumlah = $total_undispatch + $total_sisa + $total_kp + $total_kt + $total_ogp + $total_up;
            $presentase = ($total_up/$jumlah)*100;
            ?>
            <tr>
              <td>TOTAL</td>
              <td align="center"><a href="/ticketing/dashboardCloseList/UNDISPATCH/ALL/{{ $date }}">{{ $total_undispatch }}</a></td>
              <td align="center"><a href="/ticketing/dashboardCloseList/SISA/ALL/{{ $date }}">{{ $total_sisa }}</a></td>
              <td align="center"><a href="/ticketing/dashboardCloseList/KP/ALL/{{ $date }}">{{ $total_kp }}</a></td>
              <td align="center"><a href="/ticketing/dashboardCloseList/KT/ALL/{{ $date }}">{{ $total_kt }}</a></td>
              <td align="center"><a href="/ticketing/dashboardCloseList/OGP/ALL/{{ $date }}">{{ $total_ogp }}</a></td>
              <td align="center"><a href="/ticketing/dashboardCloseList/UP/ALL/{{ $date }}">{{ $total_up }}</a></td>
              <td align="center">{{ round($presentase,2) }}%</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection