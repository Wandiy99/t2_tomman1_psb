@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
  }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/combine/npm/sweetalert2@10.13.0/dist/sweetalert2.min.css,npm/sweetalert2@10.13.0/dist/sweetalert2.min.css">
<div class="col-sm-12">
    <div class="white-box">
    <a href="{{ route('ticket.dashboard') }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">List Detail Ticket Tomman Sektor : {{ $sektor }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>Area</th>
                    <th>Tiket</th>
                    <th>SQM</th>
                    <th>Service No</th>
                    <th>NCLI</th>
                    <th>Service ID</th>
                    <th>Datek</th>
                    <th>ODP</th>
                    <th>Segment Status</th>
                    <th>Report Data</th>
                    <th>Customer Name</th>
                    <th>Contact Phone</th>
                    <th>Summary</th>
                    <th>Alamat</th>
                    <th>Team</th>
                    <th>Status Laporan</th>
                    <th>Date Status</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($getData as $no=>$data)
                  <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $data->sektor_asr }}</td>
                    <td>
                        @if($sektor<>'UNDISPATCH')
                            <a href="/tiket/{{ $data->id_dt }}">{{ $data->Incident }}</a>
                        @else
                            <a href="/assurance/dispatch/{{ $data->Incident }}" target="_blank">{{ $data->Incident }}</a>
                        @endif
                    </td>
                    <td>
                        @if (!in_array($data->ticketid,[null,'null']))
                            {{ $data->ticketid }}
                        @elseif ($data->ncliOrder <> "")
                            <a class="createSQM label label-info" data-id="{{ $data->Incident }}/{{ $data->Service_No }}/{{ $data->ncliOrder }}" data-format="{{ $data->ncliOrder }}_{{ $data->Service_No }}_INTERNET">CREATE SQM</a>
                        @else
                            <a class="label label-danger">CREATE SQM</a>
                        @endif
                    </td>
                    <td>{{ $data->Service_No }}</td>
                    <td>{{ $data->ncliOrder }}</td>
                    <td>{{ $data->Service_ID }}</td>
                    <td>{{ $data->Datek }}</td>
                    <td>{{ $data->ODP }}</td>
                    <td>{{ $data->Segment_Status }}</td>
                    <td>{{ $data->Reported_Date }}</td>
                    <td>{{ $data->Customer_Name }}</td>
                    <td>{{ $data->Contact_Phone }}</td>
                    <td>{{ $data->Summary }}</td>
                    <td>{{ $data->pel_alamat }}</td>
                    @if($sektor<>'UNDISPATCH')
                        <td>{{ $data->uraian }}</td>
                        <td>{{ $data->laporan_status }}</td>
                        <td>{{ $data->updated_at }}</td>
                    @else
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    @endif
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.13.0/dist/sweetalert2.all.min.js"></script>
<script>
    (function (){
      $('.createSQM').click( function() {
            var data_id = $(this).attr('data-id'), data_format = $(this).attr('data-format');
            Swal.fire({
              title: 'Mau buat tiket SQM ?',
              text: data_format,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "<a href='/ticketing/createSQM/" + data_id + "' style='color: white;'>Yes, create it!</a>"
            });
      })
    })()
</script>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL TICKET INT CUSTOMER TOMMAN'
                }
            ]
        });
    });
</script>
@endsection