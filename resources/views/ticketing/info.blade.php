@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;
    }
    input,textarea {
      background-color: #ecf0f1;
      padding : 5px;
    }
    table td {
      padding : 5px;
    }
  </style>
  <h3>Ticketing</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Input
        </div>
        <div class="panel-body">
          <form method="post">
            <input type="hidden" name="id" value="{{ $id }}">
            <table>
              <tr>
                <td>Incident</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Incident }}
                </td>
              </tr>
              <tr>
                <td>Reported_Date</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Reported_Date }}
                </td>
              </tr>

              <tr>
                <td>Customer_Name</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Customer_Name }}
                </td>
              </tr>

              <tr>
                <td>Contact_Name</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Contact_Name }}
                </td>
              </tr>

              <tr>
                <td>Contact_Phone</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Contact_Phone }}
                </td>
              </tr>

              <tr>
                <td>Summary</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Summary }}
                </td>
              </tr>
              <tr>
                <td>Owner Group</td>
                <td>:</td>
                <td>
                  {{ $customer_info->Owner_Group }}
                </td>
              </tr>

            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
