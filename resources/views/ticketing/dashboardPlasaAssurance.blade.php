@extends('layout')
@section('content')
<style>
    tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    .black-grey {
        background-color: #34495E;
        color: white !important;
    }
    .red-lose {
        background-color: #E74C3C;
        color: white !important;
    }
    .green-win {
        background-color: #1ABC9C;
        color: white !important;
    }

</style>
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

<link href="/elitetheme/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/elitetheme/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/elitetheme/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="/elitetheme/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/elitetheme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="/elitetheme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="/elitetheme/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
@include('partial.alerts')

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h4 class="page-title" style="text-align: center; font-weight: bold;">DASHBOARD ASSURANCE<br />TICKETING INT</h4><br/>
            <form method="POST">
                <div class="row">
                    <div class="col-md-3">
                        <label for="source">Flag</label><br />
                        <select class="selectpicker" multiple data-style="form-control" name="flag[]">
                            <option value="pelanggan_hvc">HVC</option>
                            <option value="ket_plasa">Plasa</option>
                            <option value="ket_sqm">SQM</option>
                            <option value="ket_sales">Sales</option>
                            <option value="ket_helpdesk">Helpdesk</option>
                            <option value="ket_monet">Monet</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal Awal</label>
                        <input type="date" name="startDate" id="startDate" class="form-control" value="{{ $startDate }}">
                    </div>
                    <div class="col-md-3">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="endDate" id="endDate" class="form-control" value="{{ $endDate }}">
                    </div>
                    <div class="col-md-3">
                        <label for="search">&nbsp;</label>
                        <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="col-sm-12 table-responsive white-box">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="align-middle" rowspan="2" style="background-color: #0073b7; color: white">AREA</th>
                    <th class="align-middle" colspan="5" style="background-color: #0073b7; color: white">STATUS</th>
                </tr>
                <tr>
                    <th class="align-middle" style="background-color: #34495E; color: white">NP</th>
                    <th class="align-middle" style="background-color: #F1C40F; color: white">OGP</th>
                    <th class="align-middle" style="background-color: #e74c3c; color: white">KP</th>
                    <th class="align-middle" style="background-color: #e74c3c; color: white">KT</th>
                    <th class="align-middle" style="background-color: #2ecc71; color: white">CLOSE</th>
                </tr>
            </thead>
            @php
                $total_np = $total_ogp = $total_kp = $total_kt = $total_close = 0;
            @endphp
            <tbody>
                @foreach ($data as $key => $result)
                @php
                    $total_np += $result->jml_np;
                    $total_ogp += $result->jml_ogp;
                    $total_kp += $result->jml_kp;
                    $total_kt += $result->jml_kt;
                    $total_close += $result->jml_close;
                @endphp
                    <tr>
                        <td class="align-middle">{{ $result->area ? : 'NON AREA' }}</td>
                        <td class="align-middle">
                            <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area={{ $result->area ? : 'NON AREA' }}&grup=NP" >{{ $result->jml_np }}</a>
                        </td>
                        <td class="align-middle">
                            <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area={{ $result->area ? : 'NON AREA' }}&grup=OGP" >{{ $result->jml_ogp }}</a>
                        </td>
                        <td class="align-middle">
                            <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area={{ $result->area ? : 'NON AREA' }}&grup=KP" >{{ $result->jml_kp }}</a>
                        </td>
                        <td class="align-middle">
                            <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area={{ $result->area ? : 'NON AREA' }}&grup=KT" >{{ $result->jml_kt }}</a>
                        </td>
                        <td class="align-middle">
                            <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area={{ $result->area ? : 'NON AREA' }}&grup=CLOSE" >{{ $result->jml_close }}</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th class="align-middle" style="background-color: #0073b7; color: white">TOTAL</th>
                    <th class="align-middle" style="background-color: #34495E; color: white">
                        <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area=ALL&grup=NP" >{{ $total_np }}</a>
                    </th>
                    <th class="align-middle" style="background-color: #F1C40F; color: white">
                        <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area=ALL&grup=OGP" >{{ $total_ogp }}</a>
                    </th>
                    <th class="align-middle" style="background-color: #e74c3c; color: white">
                        <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area=ALL&grup=KP" >{{ $total_kp }}</a>
                    </th>
                    <th class="align-middle" style="background-color: #e74c3c; color: white">
                        <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area=ALL&grup=KT" >{{ $total_kt }}</a>
                    </th>
                    <th class="align-middle" style="background-color: #2ecc71; color: white">
                        <a href="/ticketing/dashboardPlasaAssuranceDetail?flag={{ $flag }}&startDate={{ $startDate }}&endDate={{ $endDate }}&area=ALL&grup=CLOSE" >{{ $total_close }}</a>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/elitetheme/bower_components/switchery/dist/switchery.min.js"></script>
<script src="/elitetheme/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/elitetheme/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/elitetheme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="/elitetheme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/elitetheme/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script>
    $(document).ready(function() {

        $('.selectpicker').selectpicker();

        var day = {
                format: 'yyyy-mm-dd',
                viewMode: 0,
                minViewMode: 0
            };

        $('#startDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        $('#endDate').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
        });

        $('.show-tick').css({
            'width': '100%'
        })
    });
</script>
@endsection