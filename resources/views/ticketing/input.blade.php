@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .panel-info .panel-heading {
      background-color: #54a0ff;
    }
    .customer_info {
      border-color: #FFF;
    }
    .customer_info td {
      padding : 5px;
    }
    .customer_field {
      align: right !important;
    }
    input,textarea {
      background-color: #ecf0f1;
      padding : 5px;
    }
    table td {
      padding : 5px;
    }
  </style>
  <h3>Ticketing</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Input
        </div>
        <div class="panel-body">
          <form method="post">
            <input type="hidden" name="id" value="{{ $id }}">
            <input type="hidden" name="ncliOrder" value="{{ $customer_info->orderNcli }}">
            <table>
              <tr>
                <td>ND</td>
                <td>:</td>
                <td>
                  {{ $customer_info->ndemPots ? : "~" }}
                  <input type="hidden" name="ndemPots" value="{{ $customer_info->ndemPots }}">
                </td>
              </tr>

              <tr>
                <td>ND INTERNET</td>
                <td>:</td>
                <td>
                  @if ($customer_info->ndemSpeedy<>'')
                    {{ $customer_info->ndemSpeedy }}
                    <input type="hidden" name="ndemSpeedy" value="{{ $customer_info->ndemSpeedy }}">
                  @else
                    <input type="text" name="ndemSpeedy" id="ndemSpeedy">
                  @endif
                </td>
              </tr>
              <tr>
                <td>Segment Status</td>
                <td>:</td>
                <td>
                  <select name="segment_status" required>
                    <option value=""></option>
                    <option value="DBS"> DBS </option>
                    <option value="DCS"> DCS </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Customer Name</td>
                <td>:</td>
                <td>
                  <input type="text" name="orderName" id="orderName" value="{{ $customer_info->orderName }}">
                  {!! $errors->first('orderName','<p><span class="label label-danger">:message</span></p>') !!}
                  </td>
              </tr>
              <tr>
                <td>No Internet</td>
                <td>:</td>
                <td>
                  <input type="text" name="internet" id="internet" value="{{ $customer_info->internet }}">
                  {!! $errors->first('internet','<p><span class="label label-danger">:message</span></p>') !!}
                  </td>
              </tr>
              <tr>
                <td>ODP</td>
                <td>:</td>
                <td>
                  <input type="text" minlength="14" maxlength="15" name="alproname" id="alproname" value="{{ $customer_info->alproname }}">
                {!! $errors->first('alproname','<p><span class="label label-danger">:message</span></p>') !!}
                  </td>
              </tr>
              <tr>
                <td>Workzone</td>
                <td>:</td>
                <td>
                <select name="sto" id="sto">
                <option value="" selected disabled>Pilih Workzone</option>
                @foreach ($get_area as $area)
                <option data-subtext="description 1" value="{{ $area->id }}" <?php if (@$area->id==@$customer_info->sto) { echo "Selected"; } else { echo ""; } ?>>{{ @$area->text }}</option>
                @endforeach
                </select>
                 {!! $errors->first('sto','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>

              <tr>
                <td>Pelapor</td>
                <td>:</td>
                <td>
                  <input type="text" name="pelapor" value="">
                 {!! $errors->first('pelapor','<p><span class="label label-danger">:message</span></p>') !!}
                </td>
              </tr>
              <tr>
                <td>Nomor yang bisa dihubungi</td>
                <td>:</td>
                <td>
                  <input type="text" name="kontak" value="">
                  {!! $errors->first('kontak','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>
                  <input type="text" name="alamat" value="{{ $customer_info->orderAddr }}">
                  {!! $errors->first('alamat','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>
              <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>
                  <div class="input-group">
                    <input type="checkbox" class="check" id="hvc" name="keterangan[hvc]" value="YES" data-checkbox="icheckbox_flat-red">
                    <label for="hvc">&nbsp;HVC</label>&nbsp;&nbsp;
                    <input type="checkbox" class="check" id="plasa" name="keterangan[plasa]" value="1" data-checkbox="icheckbox_flat-red">
                    <label for="plasa">&nbsp;Plasa</label>&nbsp;&nbsp;
                    <input type="checkbox" class="check" id="sqm" name="keterangan[sqm]" value="1" data-checkbox="icheckbox_flat-red">
                    <label for="sqm">&nbsp;SQM</label>&nbsp;&nbsp;
                    <input type="checkbox" class="check" id="sales" name="keterangan[sales]" value="1" data-checkbox="icheckbox_flat-red">
                    <label for="sales">&nbsp;Sales</label>&nbsp;&nbsp;
                    <input type="checkbox" class="check" id="helpdesk" name="keterangan[helpdesk]" value="1" data-checkbox="icheckbox_flat-red">
                    <label for="helpdesk">&nbsp;Helpdesk</label>&nbsp;&nbsp;
                    <input type="checkbox" class="check" id="monet" name="keterangan[monet]" value="1" data-checkbox="icheckbox_flat-red">
                    <label for="monet">&nbsp;Monet</label>&nbsp;&nbsp;
                  </div>
                  {!! $errors->first('keterangan','<p><span class="label label-danger">:message</span></p>') !!}
               </td>
              </tr>
              <tr>
                <td>Kategori Gangguan</td>
                <td>:</td>
                <td>
                  <select name="kat_gangguan">
                    <option> -- </option>
                    <option value="FISIK"> FISIK </option>
                    <option value="LOGIK"> LOGIK </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Kategori Layanan</td>
                <td>:</td>
                <td>
                  <select name="kat_layanan">
                    <option> -- </option>
                    <option value="INTERNET">INTERNET</option>
                    <option value="TELP">TELP</option>
                    <option value="USEETV">USEETV</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>Jam Manja</td>
                <td>:</td>
                <td>
                  <input type="date" name="tglManja" id="input-tgl">
                  <input name="jamManja" type="time" id="jamManja" value="" />
   
                </td>
              </tr>
              <tr>
                <td>Kategori Keluhan (Prior)</td>
                <td>:</td>
                <td>
                  <select name="keluhan">
                    <option value=""> -- </option>
                    @foreach ($get_keluhan as $keluhan)
                    <option value="{{ $keluhan->keluhan }}">{{ $keluhan->keluhan }}</option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td valign=top>Deskripsi Keluhan</td>
                <td valign=top>:</td>
                <td>    
                  <textarea name="catatan" rows="8" cols="80"></textarea>
                  {!! $errors->first('catatan','<p><span class="label label-danger">:message</span></p>') !!}
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>
                  <input type="submit" name="" value="Submit">
                </td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="/js/jquery.min.js"></script>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.4-beta.33/jquery.inputmask.min.js"></script>
  <!-- <script>
    $(function() {
 
    var day = {
        format: 'yyyy-mm-dd',
        viewMode: 0,
        minViewMode: 0
      };
      $('#input-tgl').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });
      $('#tglManja').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      var day = {
		      	  format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

      $("#alproname").inputmask("AAA-AAA-A{2,3}/999");

      $('#jamManja').timepicker({
        showMeridian : false,
        disableMousewheel : true,
        minuteStep : 1
      });
    })
    </script> -->
@endsection
