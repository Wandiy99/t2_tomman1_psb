@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/maintaince/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    Maintenance
  </h3>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::segment(3) == 'np') ? 'active' : '' }}"><a href="/maintaince/status/np">Need Progress</a></li>
  <li class="{{ (Request::segment(3) == 'close') ? 'active' : '' }}"><a href="/maintaince/status/close">Quality and Quantity Control</a></li>
  <li class="{{ (Request::segment(3) == 'rekon') ? 'active' : '' }}"><a href="/maintaince/status/rekon">Rekon</a></li>
  <li class="{{ (Request::segment(3) == 'invoice') ? 'active' : '' }}"><a href="/maintaince/status/invoice">Invoice</a></li>
 @if(session('auth')->level ==53 || session('auth')->level ==2)
    <li class="{{ (Request::segment(2) == 'search') ? 'active' : '' }}"><a href="/maintaince/search">Cari Tiket</a></li>
    <li class="{{ (Request::segment(2) == 'dummy') ? 'active' : '' }}"><a href="/maintaince/dummy">Tiket Dummy</a></li>
    <li class="{{ (Request::segment(2) == 'maintenance') ? 'active' : '' }}"><a href="/laporan/maintenance">Report</a></li>
  @endif
  </ul>
  <h5>
    <a href="/maintaince/dummy/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    Tiket Dummy
  </h5>
  <div class="list-group">
    @foreach($list as $data)
      <div class="list-group-item">
        <strong>{{ $data->no_tiket }}</strong>
        <br />
        <span>{{ $data->ts }}</span>
        <br />
        <span>{{ $data->updated_by }}</span><br />  
      </div>

    @endforeach
  </div>
@endsection
