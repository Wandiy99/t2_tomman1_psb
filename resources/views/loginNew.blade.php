<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BIAWAK</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Login and Start Out Experience Work is Easy With Us | BIAWAK" />
        <meta
            name="keywords"
            content="ops tomman app,tomman app,telkomakses tomman,tomman telkom,tomman banjarmasin,tomman kalsel,telkomakses,warrior telkom akses,login tomman app,login tomman,tomman portal,tomman telkomakses,tomman telkom akses,telkomakses tomman,kalsel hibat,tomman kalsel hibat,banjarmasin,kalimatan selatan,indihome,tomman indihome"
        />
        <meta name="copyright" content="biawak.tomman.app" />
        <meta name="author" content="Login - BIAWAK" />
        <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/fonts/iconic/css/material-design-iconic-font.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/animate.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/hamburgers.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/animsition.min.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/util.css" />
        <link rel="stylesheet" type="text/css" href="/bower_components/vendor/main.css" />
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    @include('partial.alerts')
                    <form method="post" class="login100-form validate-form">
                        <span class="login100-form-title p-b-26"> TOM<font style="color: red;">MAN</font> OPERATIONAL </span><br />
                        <div class="wrap-input100 validate-input">
                            <input class="input100" type="text" name="login" style="vertical-align: middle; text-align: center" id="input-login" />
                            <span class="focus-input100" data-placeholder="Username"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <span class="btn-show-pass">
                                <i class="zmdi zmdi-eye"></i>
                            </span>
                            <input class="input100" type="password" name="password" style="vertical-align: middle; text-align: center" id="input-password" />
                            <span class="focus-input100" data-placeholder="Password"></span>
                        </div>
                
                        <br />
                        <div class="wrap-input100 validate-input captcha">
                            <center>
                            <span>{{ captcha_img('math') }}</span>
                            <button type="button" class="btn btn-danger" class="reload" id="reload">
                                &#x21bb;
                            </button>
                            </center>
                            <input class="input100" type="text" name="captcha" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="vertical-align: middle; text-align: center" minlength="2"/>
                        </div>
                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Send OTP
                                </button>
                            </div>
                        </div>
                        <br />
                        <p class="text-muted" style="text-align: center;">BIAWAK</p>
                        <br />
                        <p style="text-align: center; color: #E74C3C">Pastikan Telegram Rekan Sudah Login ke <a href="https://t.me/TommanGo_Bot">Bot TommanGO</a></p>

                    </form>
                </div>
            </div>
        </div>
        <div id="dropDownSelect1"></div>

        <script src="/bower_components/vendor/jquery-3.2.1.min.js"></script>
        <script src="/bower_components/vendor/animsition.min.js"></script>
        <script src="/bower_components/vendor/popper.js"></script>
        <script src="/bower_components/vendor/bootstrap.min.js"></script>
        <script src="/bower_components/vendor/select2.min.js"></script>
        <script src="/bower_components/vendor/moment.min.js"></script>
        <script src="/bower_components/vendor/daterangepicker.js"></script>
        <script src="/bower_components/vendor/countdowntime.js"></script>
        <script src="/bower_components/vendor/main.js"></script>
        <script type="text/javascript">
            $('#reload').click(function () {
                $.ajax({
                    type: 'GET',
                    url: 'reload-captcha',
                    success: function (data) {
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
    </body>
</html>