@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>List Amija</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th>No.</th>
            <th>Sektor</th>
            <th>NIK AMIJA</th>
            <th>Nama</th>
            <th>Laborcode</th>
            <th>Mitra</th>
            <th>Squad</th>
            <th>Jadwal</th>
            <th>Hadir</th>
          </tr>
          @foreach ($query as $num => $result)
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{ $result->title }}</td>
            <td>{{ $result->nik_amija }}</td>
            <td>{{ $result->nama }}</td>
            <td>{{ $result->laborcode }}</td>
            <td>{{ $result->mitra_amija }}</td>
            <td>{{ $result->squad }}</td>
            <td>{{ $result->jadwal }}</td>
            <td>{{ $result->hadir }}</td>
            <td><a href="/employee/{{ $result->id_people }}" class="label label-warning">Edit</a></td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
@endsection
