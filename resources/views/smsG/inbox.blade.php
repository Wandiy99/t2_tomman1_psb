@extends('layout')

@section('content')
<div class="modal fade" id="detailModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Isi Pesan</h4>
        <div id="txtHint"></div>
        <input type="hidden" name="modal_id" class="form-control" id="modal_id">
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Waktu</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
   @foreach($list_in as $list)
   <tr>
    @if(isset($list->nama))
    <td scope="row">{{$list->nama}}</td>
    @else
    <td scope="row">{{$list->SenderNumber}}</td>
    @endif
    <td scope="row">
      {{$list->UpdatedInDB}}
    </td>
    <td scope="row">
     <button type="button" class="btn btn-info glyphicon glyphicon-envelope" 
     data-target="detailModal" data-id="{{ $list->ID }}" 
     data-nama="{{ $list->ID }}" data-original-title="Dispatch">
   </button>
 </td>
</tr>
@endforeach
</tbody>
</table>
<div class="text-left col-md-12">
  {!! $list_in->render();!!}
</div>
<script>
  $(document).ready(function () {
    $('.glyphicon-envelope').click(function(event){
      $('#modal_id').val(event.target.dataset.id);
      
      var id = document.getElementById('modal_id').value;
      console.log(id);
      $.get("/sms/detail_I/"+id, function(text) {
        $("#txtHint").html(text);
      });
      
      $('#detailModal').modal('show');
    });
  });
</script>
@endsection