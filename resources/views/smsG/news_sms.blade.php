@extends('layout')

@section('content')
@include('partial.alerts')
<form method="post" action="{{ URL::to('/send3006')}}" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group row">
		<label for="inputto" class="col-sm-1 col-form-label" style="margin:8px 0 0 0;">To</label>
		<div class="col-sm-5">
			{{-- <input type="text" class="form-control" id="to" name="to" placeholder="To..." readonly> --}}
			<select class="listkontak form-control" name="to[]" multiple="multiple">
{{-- @foreach ($list_c as $listc)
<option value="{{ $listc->telp_2 }}">{{ $listc->telp }} ({{ $listc->nama  }})</option>
@endforeach --}}
</select>
</div>

</div>
<div class="form-group row">
	<label for="inputto" class="col-sm-1 col-form-label">Messages</label>
</div>
<div class="form-group row">
	<div class="col-sm-6">
		<textarea class="form-control" id="pesan" name="pesan" rows="3" maxlength="160"></textarea>
		<p style="margin:0 0 0 49%;" id="total">160/1</p>
	</div>
</div>
<div class="form-group row">
	<div class="col-sm-10">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>
</form>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		function c_text(){
			$("#pesan").keyup(function(){
				var jumlah = $(this).val();
				var w1 = 160 - jumlah.length;
				$("#total").html(w1+"/"+"1");
				if(jumlah.length >= 161){
					var w2 = 319 - jumlah.length;
					$("#total").html(w2+"/"+"2");
				}
				if(jumlah.length >= 320){
					$("#total").html("lebih dari 300 karakter");
				}
			});
		}

		c_text();

// function color(){
// 	$("#contactL").click(function(){
// 		$(this).css({'color' : '#565656'});
// 	});

// 	$('#kontakmodal').on("hidden.bs.modal",function(){
// 		$("#contactL").css({'color' : '#3B85FF'});
// 	});
// }

// color();

// setInterval(function(){
// 	c_text();
// }, 500);

// $(function() {
// 	$( "#to" ).autocomplete({
// 		source: 
// 	});
// });

// $("#contact").on('click','.btnSelect',function(){
// 	var currentRow=$(this).closest("tr"); 
// 	var col0=currentRow.find("td:eq(0)").text();
// 	var col1=currentRow.find("td:eq(1)").text();
// 	var data=col1+" "+"("+ col0 +")";
// 	$('#to').val(data);
// });

// $('.btnSelect').click(function(){
// 	$('#kontakmodal').modal('toggle'); 
// });
});
	$('.listkontak').select2({
		placeholder: "Pilih Kontak...",
		minimumInputLength: 2,
		ajax: { 
			url: "{{route('live_Search')}}",
			dataType: 'json',
			data: function (params) {
				return {
					q: $.trim(params.term)
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});

	function yourFunction() {
		var input, filter, table, tr, td, i;
		input = document.getElementById("searchc");
		filter = input.value.toUpperCase();
		table = document.getElementById("contact");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[0];
			if (td) {
				if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";

				}
			}
		}
	}
</script>
@endsection