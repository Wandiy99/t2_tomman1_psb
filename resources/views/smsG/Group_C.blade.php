@extends('layout')

@section('content')

{{-- <div class="modal fade" id="kontakmodal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Pilih Kontak Untuk Group</h4>
</div>
<div class="modal-body">
<p>
<div class="panel panel-default">
<div class="panel-body">
@foreach ($comb as $kontak) 
<p>{{$kontak->telp}} ({{$kontak->nama}}) </p>
@endforeach
</div>
</div>
</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-success" data-dismiss="modal" id="okM">Ok</button>
<button type="reset" class="btn btn-warning" id="closeM">reset</button>
</div>
</div>
</div>
</div> --}}
{{-- <div class="col-sm-6">
<ul class="nav nav-tabs">
<li><a href="#menu1g" data-toggle="tab">List Group</a>
</li>
<li><a href="#menu2g" data-toggle="tab">Buat Group</a>
</li>
</ul>
<div class="tab-content">
<div id="menu1g" class="tab-pane fade">
<form id="formlistG" name="formlistG">
<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
<input type="text" id="searchg" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
<table id="group" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr>
<th>Nama Grup</th>
<th>Keterangan</th>
<th>Kontak</th>
</tr>
</thead>
<tbody>
@foreach ($list_g as $listg)
<tr>
<td>{{ $listg->nama_G  }}</td>
<td>{{ $listg->Ket }}</td>
<td><a button="" data-container="body" data-placement="right" type="button" data-html="true" href="#" id="kontak" data-target="#kontakmodal"  data-toggle="modal"><span class="glyphicon glyphicon-user" style="margin:0 0 0 20px; color:#000000; font-size: 20px" id="contactG"></span></a></td>
</tr>
@endforeach
</tbody>
</table>
</form>
</div>
<div id="menu2g" class="tab-pane fade">
<form id="formbuatG" name="formbuatG" method="post" action="{{ URL::to('/saveG')}}" >
<div class="form-row">
<div class="form-group row">
<label for="namaG" class="col-sm-2 col-form-label">Nama Group</label>
<div class="col-sm-10">
<input type="text" class="form-control" id="inputNamaG" placeholder="Nama Grup" name="namag" required>
</div>
</div>
<div class="form-group row">
<label for="ketG" class="col-sm-2 col-form-label">Keterangan</label>
<div class="col-sm-10">
<input type="text" class="form-control" id="KetG" placeholder="Keterangan Grup" name="ketg" required>
</div>
</div>
<fieldset class="form-group">
<div class="row">
<label class="col-form-label col-sm-2">Tambahkan Kontak</label>
<div class="col-sm-10">
@foreach ($list_c as $kontak)

@endforeach
</div>
</div>
</fieldset>
<div class="form-group row">
<div class="col-sm-10">
<button type="submit" class="btn btn-primary">Simpan</button>
</div>
</div>
</div>
</form>
</div>
</div>
</div> --}}
@include('partial.alerts')
<div class="col-sm-12">
	<ul class="nav nav-tabs">
		<li><a href="#menu1c" data-toggle="tab">List Contact</a>
		</li>
		<li><a href="#menu2c" data-toggle="tab">Buat Contact</a>
		</li>
	</ul>
	<div class="tab-content">
		<div id="menu1c" class="tab-pane fade">
			<form id="formlistC" name="formlistC">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				<input type="text" id="searchc" onkeyup="yourFunction()" placeholder="Search for names.." title="Type in a name" class="col-sm-12">
				<table id="contact" class="table table-striped table-bordered" style="width:100%;">
					<thead>
						<tr>
							<th>Nama Kontak</th>
							<th>Alamat</th>
							<th>No. Telp</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($list_c as $listc)
						<tr>
							<td>{{ $listc->nama  }}</td>
							<td>{{ $listc->A_S }}</td>
							<td>{{ $listc->telp }}</td>
							<td><button type="button" class="btn btn-danger btn-sm glyphicon glyphicon-remove" onclick="window.location='{{ URL::to("/sms/delete/{$listc->id}")}}'">Hapus
							</button> || <button type="button" class="btn btn-success btn-sm glyphicon glyphicon-pencil" onclick="window.location='{{ URL::to("/sms/edit/contact/{$listc->id}")}}'">Edit
							</button></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</form>
			<div class="text-left col-md-12">
				{!! $list_c->render();!!}
			</div>
		</div>
		<div id="menu2c" class="tab-pane fade">
			<form id="formbuatC" name="formbuatC" method="post" action="{{ URL::to('/saveC')}}" >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputnama">Nama</label>
						<input type="text" class="form-control" id="inputnama" placeholder="Nama Kontak" name="nama" required>
					</div>
					<div class="form-group col-md-6">
						<label for="nomortelp">Nomor Telepon</label>
						<input type="text" class="form-control" id="inputnotelp" placeholder="Nomor Telepon" name="telp" required>
					</div>
				</div>
				<div class="form-group">
					<label for="inputAddress2">Alamat Sekarang</label>
					<input type="text" class="form-control" id="alamatS" placeholder="Tempat Tinggal Sekarang" name="alamatS" required>
				</div>
				{{-- <button type="button" class="btn btn-primary" name="submitC" id="submitC">Simpan</button> --}}
				<button type="submit" class="btn btn-primary">Simpan</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#inputnotelp").keydown(function (e) {
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
				(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

		$('#closeM').click(function(){
// $('input:checkbox').removeAttr('checked');
document.getElementById("#kontakmodal").reset();
});

// $('#okM').on('submit', function(event) {
// 	event.preventDefault();
// 	var form_data = $(this).serialize();
// 	$.ajax({
// 		url:"{{ route('smsgamma.savegc')}}",
// 		method: "POST",
// 		data:form_data,
// 		dataType:"json",
// 		success:function(data){

// 		}
// 	});
// });
});
// function myFunction() {
// 	var input, filter, table, tr, td, i;
// 	input = document.getElementById("searchg");
// 	filter = input.value.toUpperCase();
// 	table = document.getElementById("group");
// 	tr = table.getElementsByTagName("tr");
// 	for (i = 0; i < tr.length; i++) {
// 		td = tr[i].getElementsByTagName("td")[0];
// 		if (td) {
// 			if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
// 				tr[i].style.display = "";
// 			} else {
// 				tr[i].style.display = "none";
// 			}
// 		}
// 	}
// }

function yourFunction() {
	var input, filter, table, tr, td, i;
	input = document.getElementById("searchc");
	filter = input.value.toUpperCase();
	table = document.getElementById("contact");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
			if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}
</script>

@endsection