@extends('layout')

@section('header')
@endsection

@section('content')
  @include('partial.alerts')
 <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id_user))
      <input type="hidden" name="id" value="{{ $data->id_user }}" />
    @endif
    <h3>
      <a href="/user/v2/" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      User
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-iduser">User Name</label>
      <input name="id_user" type="text" id="input-iduser" class="form-control" value="<?= $data->id_user ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-password">Password</label>
      <input name="password" type="password" id="input-password" class="form-control" value="" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-karyawan">Karyawan</label>
      <input name="karyawan" type="hidden" id="input-karyawan" class="form-control" value="<?= $data->id_karyawan ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-level">Level</label>
      <input name="level" type="hidden" id="input-level" class="form-control" value="<?= $data->level ?>" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-witel">Witel</label>
      <input name="witel" type="hidden" id="input-witel" class="form-control" value="<?= $data->witel ?>" />
    </div>
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>

  </form>
  @if (isset($data->id_user))
    <form id="delete-form" method="post" autocomplete="off">
      <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
    </form>
  @endif
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>

    $(document).ready(function () {
      var data = <?= json_encode($karyawan) ?>;
      var witels = <?= json_encode($witel) ?>;
      var levels = <?= json_encode($level) ?>;
      var select2Options = function() {
				return {
					data: data,
					placeholder: 'Input Karyawan',
					allowClear: true,
					minimumInputLength: 1,
					escapeMarkup: function(m) { return m },
					formatSelection: function(data) { return data.text },
					formatResult: function(data) {
						return	'<span class="label label-default">'+data.id+'</span>'+
								'<strong style="margin-left:5px">'+data.text+'</strong>';
					}
				}
			}
      $('#input-karyawan').select2(select2Options());
      var witel = function() {
        return {
          data: witels,
          placeholder: 'Input witel'
        }
      }
      $('#input-witel').select2(witel());
      var level = function() {
        return {
          data: levels,
          placeholder: 'Input Level'
        }
      }
      $('#input-level').select2(level());

      $('.btn-danger').click(function() {
      var sure = confirm('Yakin hapus data ?');
      if (sure) {
        $('#delete-form').submit();
      }
      })
    })
  </script>
@endsection
