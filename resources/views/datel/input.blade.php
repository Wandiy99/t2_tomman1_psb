@extends('layout')

@section('content')
  @include('partial.alerts')
  	<style>
		.panel-content {
			padding : 10px;
		}
	</style>
	<link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <script src="/bower_components/select2/select2.min.js"></script>
	<h3>
    <a href="/datel" class="btn btn-info">
      <span class="glyphicon glyphicon-home"></span>
    </a>
    DATEL
  </h3>
  	<div class="panel panel-primary">
		<div class="panel-heading">Input</div>
		<div class="panel-content">
		<form method="post">
				<input type="hidden" name="id" value="{{ $data->id }}" />
			<div class="form-group">
				<label for="datel">DATEL</label>
				<input type="text" name="datel" id="datel" class="form-control" value="{{ $data->datel }}" />
			</div>
			<div class="form-group">
				<label for="sto">STO</label>
				<input type="text" name="sto" id="sto" class="form-control" value="{{ $data->sto }}" />
			</div>
			<div class="form-group">
				<label for="witel">WITEL</label>
				<input type="text" name="witel" id="witel" class="form-control" value="{{ $data->witel }}" />
			</div>
			<div class="form-group">
				<label for="grup_sales">GROUP SALES</label>
				<input type="text" name="grup_sales" id="grup_sales" class="form-control" value="{{ $data->grup_sales }}" />
			</div>
			<div class="form-group">
				<input type="submit" name="submit" id="submit" class="btn btn-success" />
			</div>
			</form>
		</div>
	</div>

@endsection 