@extends('layout')
@section('content')
  @include('partial.alerts')

      <style>
        .panel-content {
          padding : 10px;
        }
      </style>
      <h3> 
        <a href="/datel/input" class="btn btn-info">
          <span class="glyphicon glyphicon-plus"></span>
        </a>
        Datel</h3>
      <div class="panel panel-primary">
        <div class="panel-heading">List</div>
        <div class="panel-content">
          <div class="table table-responsive">
            <table class="table table-responsive">
              <tr>  
                <th>NO</th>
                <th>DATEL</th>  
                <th>STO</th>  
                <th>WITEL</th>  
                <th>GROUP SALES</th>
                <th></th> 
              </tr>
          @foreach($getAll as $data)
              <tr>
                <td>{{ $data->id }}</td>
                <td>{{ $data->datel }}</td>
                <td>{{ $data->sto }}</td>
                <td>{{ $data->witel }}</td>
                <td>{{ $data->grup_sales }}</td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
@endsection