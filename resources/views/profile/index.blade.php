
@extends($layout)

@section('content')
  @include('partial.alerts')

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

  <div class="row">
    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <div class="user-bg">
              @if ($layout == 'layout')
              <img width="100%" alt="user" src="/image/bg-profile.png">
              @endif
                <div class="overlay-box">
                    <div class="user-content text-center">
                        @php
                            $path4  = "/upload4/profile/".$profile->nik;
                            $th4    = "$path4/profile-th.jpg";
                            $path   = null;

                            if (file_exists(public_path().$th4))
                            {
                              $path = "$path4/profile";
                            }

                            if($path)
                            {
                                $img    = "$path.jpg";
                                $th     = "$path-th.jpg";
                            } else {
                                $img    = null;
                                $th     = null;
                            }
                        @endphp
                        @if (@file_exists(public_path().$th))
                        <a href="javascript:void(0)"><img src="/upload4/profile/{{ $profile->nik }}/profile.jpg" class="thumb-lg img-circle" alt="img"></a>
                        @else
                        <a href="javascript:void(0)"><img src="/image/placeholder.gif" class="thumb-lg img-circle" alt="img"></a>
                        @endif
                        <h4 class="text-white">{{ $profile->nama }}</h4>
                        <h5 class="text-white">{{ $profile->nik }}</h5> </div>
                </div>
            </div>
            <div class="user-btm-box">
                <div class="col-md-12 col-sm-12 text-center">
                    <p class="text-purple"><i class="ti-bookmark-alt"></i></p>
                      @php
                        $tinggi = $ideal = 0;
                      @endphp
                      @if ($profile->jk == 'Laki-Laki')
                        @php
                          $tinggi = ($profile->tinggi_badan - 100);
                          $ideal = $tinggi - ($tinggi / 10);
                        @endphp
                      @elseif ($profile->jk == 'Perempuan')
                        @php
                          $tinggi = ($profile->tinggi_badan - 100);
                          $ideal = $tinggi - ($tinggi / 15);
                        @endphp
                      @endif
                      @if ($ideal <= $profile->berat_badan)
                        <h1 style="color: #E74C3C;"><b>TIDAK IDEAL</b></h1>
                        Kurangi Berat Badan Rekan sampai {{ $profile->berat_badan - $ideal }} Kg
                      @else
                        <h1 style="color: #2ECC71;"><b>IDEAL</b></h1>
                      @endif
                      </h3>
                    <br /><br />
                    <h1><b>{{ $ideal }} Kg</b></h1>
                    <h5>(Target Berat Badan Ideal Anda)</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="white-box">
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile Setting</span></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="profile">
                    <form class="form-horizontal form-material" method="post" action="/profileSave" enctype="multipart/form-data" autocomplete="off">

                        <div class="form-group">
                          <label class="col-md-6"><b>Photo Profile</b></label>
                          <div class="col-md-6">
                            @if (@file_exists(public_path().$th))
                            <input type="file" accept="image/*" name="img_profile" id="img_profile" class="dropify" data-default-file="{{ $img }}" data-height="500"/>
                            @else
                            <input type="file" accept="image/*" name="img_profile" id="img_profile" class="dropify" data-default-file="/image/placeholder.gif" data-height="500"/>
                            @endif
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <label class="col-md-6"><b>Full Name</b></label>
                            <div class="col-md-6">
                                <input type="text" value="{{ $profile->nama }}" class="form-control form-control-line" disabled>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>NIK</b></label>
                          <div class="col-md-6">
                              <input type="text" name="nik" value="{{ $profile->nik }}" class="form-control form-control-line" readonly>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Jenis Kelamin</b></label>
                          <div class="col-md-6">
                            <select class="form-control select2" name="jk" id="input-jk" required>
                              <option value="" selected disabled>Pilih Jenis Kelamin</option>
                              @foreach ($get_gender as $gender)
                              <option data-subtext="description 1" value="{{ $gender->id }}" <?php if (@$gender->id == $profile->jk) { echo "Selected"; } else { echo ""; } ?>>{{ $gender->text }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Mitra</b></label>
                          <div class="col-md-6">
                            <select class="form-control select2" name="mitra_amija" id="input-mitra" required>
                              <option value="" selected disabled>Pilih Mitra</option>
                              @foreach ($get_mitra as $mitra)
                              <option data-subtext="description 1" value="{{ $mitra->id }}" <?php if (@$mitra->id == $profile->mitra_amija) { echo "Selected"; } else { echo ""; } ?>>{{ $mitra->text }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Regu</b></label>
                          <div class="col-md-6">
                            <select class="form-control select2" name="id_regu" id="input-regu">
                              <option value="" selected disabled>Pilih Regu</option>
                              @foreach ($get_regu as $regu)
                              <option data-subtext="description 1" value="{{ $regu->id }}" <?php if (@$regu->id == $profile->id_regu) { echo "Selected"; } else { echo ""; } ?>>{{ $regu->text }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>STO</b></label>
                          <div class="col-md-6">
                            <select class="form-control select2" name="sto" id="input-sto">
                              <option value="" selected disabled>Pilih STO</option>
                              @foreach ($get_sto as $sto)
                              <option data-subtext="description 1" value="{{ $sto->id }}" <?php if (@$sto->id == $profile->sto) { echo "Selected"; } else { echo ""; } ?>>{{ $sto->text }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Password</b></label>
                          <div class="col-md-6">
                              <input type="password" name="password" class="form-control form-control-line">
                              <span class="help-block"><small>Kosongi jika tidak perlu ganti password</small></span>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>No KTP</b></label>
                          <div class="col-md-6">
                              <input type="number" name="noktp" value="{{ $profile->noktp }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <label class="col-md-6"><b>Email</b></label>
                            <div class="col-md-6">
                                <input type="email" name="email" value="{{ $profile->email }}" class="form-control form-control-line">
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Tanggal Lahir</b></label>
                          <div class="col-md-6">
                              <input type="date" name="tgl_lahir" id="tgl_lahir" value="{{ $profile->tgl_lahir }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Tempat Lahir</b></label>
                          <div class="col-md-6">
                              <input type="text" name="tempat_lahir" value="{{ $profile->tempat_lahir }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Tinggi Badan *cm</b></label>
                          <div class="col-md-6">
                              <input type="number" name="tinggi_badan" value="{{ $profile->tinggi_badan }}" class="form-control form-control-line" required>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Berat Badan *kg</b></label>
                          <div class="col-md-6">
                              <input type="number" name="berat_badan" value="{{ $profile->berat_badan }}" class="form-control form-control-line" required>
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Alamat</b></label>
                          <div class="col-md-6">
                              <input type="text" name="alamat" value="{{ $profile->alamat }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-6"><b>No Handphone</b></label>
                          <div class="col-md-6">
                              <input type="number" name="no_telp" minlength="11" value="{{ $profile->no_telp }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>No WhatsApp</b></label>
                          <div class="col-md-6">
                              <input type="number" name="no_wa" minlength="11" value="{{ $profile->no_wa }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Laborcode</b></label>
                          <div class="col-md-6">
                              <input type="text" name="laborcode" value="{{ $profile->laborcode }}" class="form-control form-control-line">
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                          <label class="col-md-6"><b>Skill Set</b></label>
                          <div class="col-md-6">
                          @if($profile->pt1 == 1)
                          <input type="checkbox" checked id="pt1" name="skillset[pt1]" value="1">
                          <label for="PT1"> PT 1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @else
                          <input type="checkbox" id="pt1" name="skillset[pt1]" value="1">
                          <label for="PT1"> PT 1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @endif
                          @if($profile->saber == 1)
                          <input type="checkbox" checked id="saber" name="skillset[saber]" value="1">
                          <label for="saber"> Saber</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @else
                          <input type="checkbox" id="saber" name="skillset[saber]" value="1">
                          <label for="saber"> Saber</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @endif
                          @if($profile->nte == 1)
                          <input type="checkbox" checked id="nte" name="skillset[nte]" value="1">
                          <label for="nte"> NTE</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @else
                          <input type="checkbox" id="nte" name="skillset[nte]" value="1">
                          <label for="nte"> NTE</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @endif 
                          @if($profile->mytech == 1)
                          <input type="checkbox" checked id="mytech" name="skillset[mytech]" value="1">
                          <label for="mytech"> MY-Tech</label>
                          @else
                          <input type="checkbox" id="mytech" name="skillset[mytech]" value="1">
                          <label for="mytech"> MY-Tech</label>
                          @endif
                          </div>
                        </div>
                        <br />
                        <div class="form-group">
                            @if ($layout == 'layout')
                              @php
                                $class = 'col-sm-12';
                              @endphp
                            @else
                              @php
                                $class = 'd-grid gap-2';
                              @endphp
                            @endif
                            <div class="{{ $class }}">
                                <button class="btn btn-success btn-block btn-rounded">Update Profile</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script>
  $(document).ready(function() {
      $('.dropify').dropify();

      $('.select2').select2();

      var day = {
              format: 'yyyy-mm-dd',
              viewMode: 0,
              minViewMode: 0
          };

      $('#tgl_lahir').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
      });
  });
</script>
@endsection
