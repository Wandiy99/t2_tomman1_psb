@extends($layout)
@section('content')
  @include('partial.alerts')
  <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Recomended Input Total 10 Order ID</strong>
  </div>

  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Max Total 50 Input Order ID Within 3 Minutes</strong>
  </div>

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <h3>
      Multi Custom Grab SC By Order ID
    </h3>
    <div class="form-group">
      <label class="control-label" for="SC">Please Type or Paste Order ID and Add Symbol ( ',' ) Behind Order ID</label>
			<textarea name="sc" id="SC" class="form-control" rows="5" required /></textarea>
		</div>
    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Sync</button>
    </div>
  </form>
@endsection