@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="modal fade" id="execute" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formexe" method="post" action="/alista/execute">
          <div class="form-group">
            <label for="tomman_id" class="col-form-label">Tomman ID</label>
            <input type="text" name="tomman_id" class="form-control" id="tomman_id">
          </div>
          <div class="form-group">
            <label for="ref_id" class="col-form-label">REF ID:</label>
            <input type="text" name="ref_id" class="form-control" id="ref_id">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnexe" class="btn btn-danger">Execute</button>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        
        <form method="post" enctype="multipart/form-data" action="/alista/request">
            <div class="col-lg-6">
            View Alista
        </div>
            <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" name="order_no" class="form-control" placeholder="No Order ...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Go!</button>
                  </span>
                </div><!-- /input-group -->
              </div>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">List Request Alista</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center table" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>SC</th>
                    <th>REF_ID</th>
                    <th>Request</th>
                    <th>Alasan</th>
                    <th>Request By</th>
                    <th>Request At</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($data as $num => $r)
                <?php
                    $request = "View";
                    if($r->request==1){
                        $request = "View";
                    }else if($r->request==2){
                        $request = "Cancel Pemakaian";
                    }else if($r->request==3){
                        $request = "Reset BA";
                    }
                ?>
                  <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $r->sc_id }}</td>
                    <td>{{ $r->ba_id }}</td>
                    <td>{{ $request }}</td>
                    <td>{{ $r->alasan }}</td>
                    <td>{{ $r->request_by }}</td>
                    <td>{{ $r->request_at }}</td>
                    <td><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#execute" data-tomman_id="{{ $r->id }}" data-ref_id="{{ $r->sc_id }}">Executo</button></td>
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('#execute').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var tomman_id = button.data('tomman_id')
          var ref_id = button.data('ref_id')

          var modal = $(this)
          modal.find('.modal-title').text('Form Execute')
          modal.find('#tomman_id').val(tomman_id)
          modal.find('#ref_id').val(ref_id)
        });
        $('#btnexe').click(function(){
            $('#formexe').submit()
        })
    });
</script>
@endsection