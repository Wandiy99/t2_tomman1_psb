@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>


<div class="modal fade" id="resetpemakaian" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formreset" method="post" action="/alista/save">
          <div class="form-group">
            <label for="sc_id_2" class="col-form-label">WO Number:</label>
            <input type="text" name="sc_id" class="form-control" id="sc_id_2">
          </div>
          <div class="form-group">
            <label for="pemakaian" class="col-form-label">PEMAKAIAN:</label>
            <input type="text" name="ba_id" class="form-control" id="pemakaian">
          </div>
          <div class="form-group">
            <label for="alasan2" class="col-form-label">ALASAN:</label>
            <textarea class="form-control" id="alasan2" name="alasan"></textarea>
          </div>
          <input type="hidden" name="request_type" value="2">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnreset" class="btn btn-danger">Request Reset Pemakaian</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="delba" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formdelete" method="post" action="/alista/save">
          <div class="form-group">
            <label for="sc_id" class="col-form-label">WO Number:</label>
            <input type="text" name="sc_id" class="form-control" id="sc_id">
          </div>
          <div class="form-group">
            <label for="ba_id" class="col-form-label">BA ID:</label>
            <input type="text" name="ba_id" class="form-control" id="ba_id">
          </div>
          <div class="form-group">
            <label for="alasan" class="col-form-label">ALASAN:</label>
            <textarea class="form-control" id="alasan" name="alasan"></textarea>
          </div>
          <input type="hidden" name="request_type" value="3">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btndelete" class="btn btn-danger">Request Delete BA</button>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        
        <form method="POST" enctype="multipart/form-data">
            <div class="col-lg-6">
            View Alista
        </div>
            <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" name="order_no" class="form-control" placeholder="No Order ..." value="">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Go!</button>
                  </span>
                </div><!-- /input-group -->
              </div>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        @if($pemakaianlist)
        <h3 class="box-title m-b-0">List Pemakaian Alista</h3>
        <div class="table-responsive">

            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>pemakaian</th>
                    <th>nik</th>
                    <th>regional</th>
                    <th>witel</th>
                    <th>date</th>
                    <th>wo_number</th>
                    <th>is_aktif</th>
                    <th>action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($pemakaianlist as $num => $r)
                <?php
                    $action="";
                    if($r['is_aktif']=='Y'){
                        $action='<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#resetpemakaian" data-pemakaian="'.$r['pemakaian'].'" data-sc_id="'.$r['wo_number'].'">Reset Pemakaian</button>';
                    }
                ?>
                  <tr>
                    <td>{{ $r['pemakaian'] }}</td>
                    <td>{{ $r['nik'] }}</td>
                    <td>{{ $r['regional'] }}</td>
                    <td>{{ $r['witel'] }}</td>
                    <td>{{ $r['date'] }}</td>
                    <td>{{ $r['wo_number'] }}</td>
                    <td>{{ $r['is_aktif'] }}</td>
                    <td>{!! $action !!}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
            <h3 class="box-title m-b-0">Pemakaian Not Found.</h3>
        @endif
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        @if($listba)
            <h3 class="box-title m-b-0">List BA Alista</h3>
            <div class="table-responsive">

                <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>pemakaian</th>
                        <th>nik</th>
                        <th>wo_number</th>
                        <th>regional</th>
                        <th>witel</th>
                        <th>fiberzone</th>
                        <th>date</th>
                        <th>is_aktif</th>
                        <th>action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($listba as $num => $r)
                    <?php
                        $action="";
                        if($r['ba_id']){
                            $action='<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delba" data-ba_id="'.$r['ba_id'].'" data-sc_id="'.$r['wo_number'].'">Delete BA</button>';
                        }
                    ?>
                      <tr>
                        <td>{{ $r['pemakaian'] }}</td>
                        <td>{{ $r['nik'] }}</td>
                        <td>{{ $r['wo_number'] }}</td>
                        <td>{{ $r['regional'] }}</td>
                        <td>{{ $r['witel'] }}</td>
                        <td>{{ $r['fiberzone'] }}</td>
                        <td>{{ $r['date'] }}</td>
                        <td>{{ $r['is_aktif'] }}</td>
                        <td>{!! $action !!}</td>
                      </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h3 class="box-title m-b-0">BA LIST NOT FOUND.</h3>
        @endif
    </div>
</div>

<div class="col-sm-12">
    <div class="white-box">
        @if(str_contains($data,"CException"))
            <h3 class="box-title m-b-0">BA NOT FOUND</h3>
        @else
            <h3 class="box-title m-b-0">BA FOUND</h3>
            <div class="table-responsive">
                {!! $data !!}
            </div>
        @endif
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#delba').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var ba_id = button.data('ba_id')
          var sc_id = button.data('sc_id')

          var modal = $(this)
          modal.find('.modal-title').text('Form Request Delete BA dan Reset Pemakaian')
          modal.find('#sc_id').val(sc_id)
          modal.find('#ba_id').val(ba_id)
        });
        $('#resetpemakaian').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var pemakaian = button.data('pemakaian')
          var sc_id = button.data('sc_id')

          var modal = $(this)
          modal.find('.modal-title').text('Form Reset Pemakaian')
          modal.find('#pemakaian').val(pemakaian)
          modal.find('#sc_id_2').val(sc_id)
        });
        $('#btndelete').click(function(){
            $('#formdelete').submit()
        })
        $('#btnreset').click(function(){
            $('#formreset').submit()
        })
        
    });
</script>
@endsection