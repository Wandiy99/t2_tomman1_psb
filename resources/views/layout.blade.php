<!DOCTYPE html>
<html lang="en">
{{-- open block tech --}}
@if(!in_array(session('auth')->level, [10,88,0]))
{{-- open block tech --}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Portal Manajemen Pekerjaan seluruh Karyawan Telkom Akses Witel Kalimantan Selatan | BIAWAK">
    <meta name="keywords" content="psb bjm tomman app,tomman indihome,tomman kalsel,tomman login,tomman register,tomman app,tomman portal,tomman telkomakses,tomman telkom akses,telkomakses tomman,kalsel hibat,tomman kalsel hibat,banjarmasin,kalimatan selatan,indihome">
    <meta name="copyright" content="tomman.app"/>
    <meta name="author" content="Portal - BIAWAK">
    <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png">
    <title>BIAWAK</title>
    {{-- Bootstrap Core CSS --}}
    <script src="/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    {{-- <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" /> --}}

    {{-- Footable CSS --}}
    <link href="/elitetheme/plugins/bower_components/footable/css/footable.core.css" rel="stylesheet">
    {{-- <link href="/elitetheme/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" /> --}}
    <link href="/elitetheme/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/elitetheme/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="/elitetheme/plugins/bower_components/icheck/skins/all.css" rel="stylesheet">
    <link href="/elitetheme/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <link href="/elitetheme/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="/elitetheme/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="/elitetheme/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="/elitetheme/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/elitetheme/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <link href="/elitetheme/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    {{-- Menu CSS --}}
    <link href="/elitetheme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/elitetheme/plugins/bower_components/dropify/dist/css/dropify.min.css">
    {{--alerts CSS --}}
    {{-- <link href="/elitetheme/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"> --}}
    {{-- toast CSS --}}
    <link href="/elitetheme/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    {{-- morris CSS --}}
    <link href="/elitetheme/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    {{-- animation CSS --}}
    <link href="/elitetheme/{{ session('theme') ? : 'light' }}/css/animate.css" rel="stylesheet">
    {{-- Custom CSS --}}
    <link href="/elitetheme/{{ session('theme') ? : 'light' }}/css/style.css" rel="stylesheet">
    {{-- color CSS --}}
    <link href="/elitetheme/{{ session('theme') ? : 'light' }}/css/colors/default-dark.css" id="theme" rel="stylesheet">
    {{-- IE8 support of HTML5 elements and media queries --}}
    {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
    {{--[if lt IE 9]>
    <script src="/elitetheme/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="/elitetheme/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]--}}
    {{-- <script src="/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script> --}}
    <style>
        .label {
            color : #FFF !important;
            margin-bottom : 5px !important;
        }
        .label-default {
            color : #000 !important;
        }
        .img-circle {
            border-radius: 10% !important;
        }

    </style>
</head>

<body>
    {{-- Preloader --}}
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        {{-- Navigation --}}
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="/elitetheme/javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="/home"><b><img src="/elitetheme/plugins/images/tomman-logo.png" alt="home" class="dark-logo" /><img src="/elitetheme/plugins/images/tomman-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><img src="/elitetheme/plugins/images/tomman-text.png" alt="home" class="dark-logo" /><img src="/elitetheme/plugins/images/tomman-text-dark.png" alt="home" class="light-logo" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="#" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    {{-- <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="/elitetheme/javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li> --}}
                    {{-- /.dropdown --}}
                    {{-- <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li> --}}
                    {{-- /.dropdown --}}
                    {{-- .Megamenu --}}
                    {{-- <li class="mega-dropdown">
                       <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Menu</span> <i class="icon-options-vertical"></i></a>
                        <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Forms Elements</li>
                                    <li><a href="/elitetheme/form-basic.html">Basic Forms</a></li>
                                    <li><a href="/elitetheme/form-layout.html">Form Layout</a></li>
                                    <li><a href="/elitetheme/form-advanced.html">Form Addons</a></li>
                                    <li><a href="/elitetheme/form-material-elements.html">Form Material</a></li>
                                    <li><a href="/elitetheme/form-float-input.html">Form Float Input</a></li>
                                    <li><a href="/elitetheme/form-upload.html">File Upload</a></li>
                                    <li><a href="/elitetheme/form-mask.html">Form Mask</a></li>
                                    <li><a href="/elitetheme/form-img-cropper.html">Image Cropping</a></li>
                                    <li><a href="/elitetheme/form-validation.html">Form Validation</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Advance Forms</li>
                                    <li><a href="/elitetheme/form-dropzone.html">File Dropzone</a></li>
                                    <li><a href="/elitetheme/form-pickers.html">Form-pickers</a></li>
                                    <li><a href="/elitetheme/icheck-control.html">Icheck Form Controls</a></li>
                                    <li><a href="/elitetheme/form-wizard.html">Form-wizards</a></li>
                                    <li><a href="/elitetheme/form-typehead.html">Typehead</a></li>
                                    <li><a href="/elitetheme/form-xeditable.html">X-editable</a></li>
                                    <li><a href="/elitetheme/form-summernote.html">Summernote</a></li>
                                    <li><a href="/elitetheme/form-bootstrap-wysihtml5.html">Bootstrap wysihtml5</a></li>
                                    <li><a href="/elitetheme/form-tinymce-wysihtml5.html">Tinymce wysihtml5</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Table Example</li>
                                    <li><a href="/elitetheme/basic-table.html">Basic Tables</a></li>
                                    <li><a href="/elitetheme/table-layouts.html">Table Layouts</a></li>
                                    <li><a href="/elitetheme/data-table.html">Data Table</a></li>
                                    <li class="hidden"><a href="/elitetheme/crud-table.html">Crud Table</a></li>
                                    <li><a href="/elitetheme/bootstrap-tables.html">Bootstrap Tables</a></li>
                                    <li><a href="/elitetheme/responsive-tables.html">Responsive Tables</a></li>
                                    <li><a href="/elitetheme/editable-tables.html">Editable Tables</a></li>
                                    <li><a href="/elitetheme/foo-tables.html">FooTables</a></li>
                                    <li><a href="/elitetheme/jsgrid.html">JsGrid Tables</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Charts</li>
                                    <li> <a href="/elitetheme/flot.html">Flot Charts</a> </li>
                                    <li><a href="/elitetheme/morris-chart.html">Morris Chart</a></li>
                                    <li><a href="/elitetheme/chart-js.html">Chart-js</a></li>
                                    <li><a href="/elitetheme/peity-chart.html">Peity Charts</a></li>
                                    <li><a href="/elitetheme/knob-chart.html">Knob Charts</a></li>
                                    <li><a href="/elitetheme/sparkline-chart.html">Sparkline charts</a></li>
                                    <li><a href="/elitetheme/extra-charts.html">Extra Charts</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-12 m-t-40 demo-box">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-purple"><a href="/elitetheme/eliteadmin-inverse/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 1</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-success"><a href="/elitetheme/eliteadmin/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 2</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-info"><a href="/elitetheme/eliteadmin-ecommerce/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 3</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-inverse"><a href="/elitetheme/eliteadmin-horizontal-navbar/index3.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 4</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-warning"><a href="/elitetheme/eliteadmin-iconbar/index4.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 5</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-danger"><a href="/elitetheme/https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820" target="_blank" class="text-white"><i class="linea-icon linea-ecommerce fa-fw" data-icon="d"></i><br/>Buy Now</a></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li> --}}
                    <li class="right-side-toggle"> <a class="waves-effect waves-light" href="#"><i class="ti-settings"></i></a></li>
                    {{-- /.dropdown --}}
                </ul>
            </div>
            {{-- /.navbar-header --}}
            {{-- /.navbar-top-links --}}
            {{-- /.navbar-static-side --}}
        </nav>
        {{-- Left navbar-header --}}
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div>
                            @php
                                $path4  = "/upload4/profile/".session('auth')->id_karyawan;
                                $th4    = "$path4/profile-th.jpg";
                                $path   = null;

                                if (file_exists(public_path().$th4))
                                {
                                $path = "$path4/profile";
                                }

                                if($path)
                                {
                                    $img    = "$path.jpg";
                                    $th     = "$path-th.jpg";
                                } else {
                                    $img    = null;
                                    $th     = null;
                                }
                            @endphp
                            <!-- @if (@file_exists(public_path().$th))
                            <img src="{{ $img }}" alt="img-{{ session('auth')->id_karyawan }}" class="img-circle">
                            @else
                            <img src="/image/placeholder.gif" alt="img-{{ session('auth')->id_karyawan }}" class="img-circle">
                            @endif -->
                        </div>
                        <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ session('auth')->nama }}<br />{{ session('auth')->id_karyawan }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="/profile"><i class="ti-user"></i> Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/accountsetting"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                    {{-- input-group --}}

                    {{-- /input-group --}}
                </li>
                @if(session('psb_reg') == 0)
                <li class="nav-small-cap m-t-10">--- Main Menu</li>

                @if(session('auth')->level != 61) {{-- IF LEVEL <> 61 --}}
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-star" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; PROVISIONING</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        {{-- <li><a href="/bon/Installed/{{ date('Y-m-d') }}">MONITORING LAPORAN</a></li> --}}
                        <li><a href="/dispatch/search">WORKORDER BY SC</a></li>
                        <li><a href="/scbe/search">WORKORDER BY SCBE</a></li>
                        @if (!in_array(session('auth')->id_karyawan, ['CTB2023001', 'CTB2023002', 'CTB2023003', 'CTB2023004', 'CTB2023005', 'CTB2023006']))
                        {{-- <li><a href="/alista/request">VIEW PEMAKAIAN & BA</a></li> --}}
                        <li><a href="/map-order-provisioning">MAP ORDER</a></li>
                        {{-- <li><a href="/manja/list/{{ date('Y-m-d') }}">MANJA DISPATCH</a></li> --}}
                        {{-- <li><a href="/produktifitastek/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/1/ALL/ALL">PRODUKTIFITAS</a></li> --}}
                        {{-- <li> <a href="javascript:void(0);" class="waves-effect">QC BORNEO <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/qcborneo/check">QC CHECK</a></li>
                                <li><a href="/qcborneo/amo">QC AMO ODP</a></li>
                                <li><a href="/dispatch/matrixQC/{{ date('Y-m-d') }}">MATRIX</a></li>
                                <li><a href="/dashboard/QCBorneoMitra/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD QC MITRA</a></li>
                                <li><a href="/dashboard/QCborneoTeknisi/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD QC TEKNISI</a></li>
                            </ul>
                        </li> --}}
                        <li> <a href="javascript:void(0);" class="waves-effect">UT ONLINE <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                {{-- <li><a href="/dashboard/UTOnline/2021-10-26/{{ date('Y-m-d') }}">DASHBOARD UT ONLINE</a></li> --}}
                                @if (in_array(session('auth')->id_karyawan, ['20981020', 'wandiy99']))
                                <li><a href="/utonline/ba-controller/request-reset-ba?startDate={{ date('Y-m-d' ,strtotime("-1 days")) }}&endDate={{ date('Y-m-d') }}">INBOX BA CONTROLLER</a></li>
                                @endif
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">PICKUP ONLINE <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dashboard/tactical/pickupOnline?source=starclick&witel={{ session('auth')->nama_witel }}">DASHBOARD HARIAN</a></li>
                                <li><a href="/dashboard/tactical/raportPickupOnline?nper={{ date('Ym') }}">RAPORT BULANAN</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">MATRIX <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dispatch/matrix/{{ date('Y-m-d') }}">MATRIX ORDER</a></li>
                                <li><a href="/dispatch/matrixGo/{{ date('Y-m-d') }}">MATRIX ORDER GO</a></li>
                                <li><a href="/new_matrix/old/{{ date('Y-m-d') }}">MATRIX FFG</a></li>
                                <li><a href="/dispatch/matrixKhusus/{{ date('Y-m-d') }}">MATRIX DISMANTLE & ONT PREMIUM</a></li>
                            </ul>
                        </li>
                        {{-- <li> <a href="javascript:void(0);" class="waves-effect">GRAB SC <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/grabscncx_manual/{{ date('Y-m-d') }}">GRAB BY DATE {{ date('Y-m-d') }}</a></li>
                                @if (session('auth')->level == 2)
                                <li><a href="/customGrab">CUSTOM GRAB SC</a></li>
                                @endif
                                <li><a href="/multi/customGrab">MULTI CUSTOM GRAB SC</a></li>
                                <li><a href="/starclick/manual">MANUAL STARCLICK SC</a></li>
                                @if(session('psb_reg')==1 && session('auth')->level <> 62 && session('auth')->level <> 10)
                                <li><a href="/customGrabOld">CUSTOM GRAB SC OLD</a></li>
                                @endif
                            </ul>
                        </li> --}}
                        <li> <a href="javascript:void(0);" class="waves-effect">PROD TEKNISI <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dashboard/produktifitasTeam?show_data=SEKTOR&area=ALL&startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">NEW PRODUKTIFITAS</a></li>
                                <li><a href="/produktifitastek?dateAwal={{ date('Y-m-d') }}&date={{ date('Y-m-d') }}&area=1&sektor=ALL&provider=DCS">PRODUKTIF V1</a></li>
                                <li><a href="/cekwo-teknisi/dashboard">ALL PRODUKTIFITAS</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">DASHBOARD <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dashboardKehadiran/1/{{ date('Y-m-d') }}">DASHBOARD KEHADIRAN</a></li>
                                {{-- <li><a href="/dashboard/rekon/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD REKON</a></li> --}}
                                <li><a href="/dashboardSC">DASHBOARD SC</a></li>
                                <li><a href="/dashboard/potensiPS?group=DATEL">DASHBOARD POTENSI PS</a></li>
                                <li><a href="/dashboard/giveawayPS?date={{ date('Y-m-d') }}">DASHBOARD GIVEAWAY PS</a></li>
                                {{-- <li><a href="/dashboard/kpro/reportQC2?area=WITEL&source=QC2&startDate=2022-04-01&endDate={{ date('Y-m-d') }}">DASHBOARD REPORT UTQC2</a></li> --}}
                                {{-- <li><a href="/dashboard/Addon?source=STARCLICKNCX&startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD ADDON</a></li> --}}
                                <li><a href="/dashboard/AddonComparin?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD ADDON COMPARIN</a></li>
                                {{-- <li><a href="/dashboardMaterial?program=PSB&startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD MATERIAL</a></li> --}}
                                <li><a href="/dashboard/scbe/{{ date('Y-m-d') }}">DASHBOARD SOLUSI KENDALA</a></li>
                                <li><a href="/dashboard/newDashboardKendalaProvisioning?view=SEKTOR&startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD NEW KENDALA</a></li>
                                {{-- <li><a href="/dashboardRacingPS?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD RACING PS</a></li> --}}
                                <li><a href="/dashboardComparin/{{ date('Y-m-d') }}">DASHBOARD COMPARIN</a></li>
                                <li><a href="/undispatchSC/{{ date('Y-m-d') }}">DASHBOARD UNDISPATCH SC</a></li>
                                <li><a href="/DashboardDismantleOntPremium?type=WITEL&date={{ date('Y-m-d') }}">DASHBOARD DISMANTLE & ONT PREMIUM</a></li>
                                <li><a href="/newDashboardDismantling?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">DASHBOARD NEW DISMANTLING</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">CABUT NTE <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/multi/cabutNte">MULTI ORDER CABUTAN NTE</a></li>
                                <li><a href="/multi/undispatchOrder">ORDER UNDISPATCH</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">ONT PREMIUM <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/multi/ontPremium">MULTI ORDER ONT PREMIUM</a></li>
                                <li><a href="/multi/undispatchOrder">ORDER UNDISPATCH</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">RIDER KALSEL <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dashboardRider">Dashboard Rider</a></li>
                                <li><a href="/dashboardRider/list">Your List</a></li>
                                <li><a href="/dashboardRider/search">Search</a></li>
                            </ul>
                        </li>
                        <li><a href="/alproCustomer/search">CUSTOMER BY ALPRO</a></li>
                        <li><a href="/multiWorkorder/search">SEARCH ALL WORKORDER</a></li>
                        <li><a href="/dashboard/wo-ps/qrcode/{{ date('Y-m-d') }}">PS QRCODE</a></li>
                        <li><a href="/coc/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">CoC Role Play</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if (!in_array(session('auth')->id_karyawan, ['CTB2023001', 'CTB2023002', 'CTB2023003', 'CTB2023004', 'CTB2023005', 'CTB2023006']))
                @if(session('psb_reg') == 0 && !in_array(session('auth')->level, [62, 61, 10]))
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-cup" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; ASSURANCE</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/assurance/search">SEARCH ORDER</a></li>
                        {{-- <li><a href="/customRemo">CUSTOM REMO MARINA</a></li> --}}
                        <li><a href="/new_matrix/{{ date('Y-m-d') }}">MATRIX ASR GO</a></li>
                        {{-- <li><a href="/assurance/monetIbooster?witel=KALSEL">PELANGGAN KRITIS</a></li> --}}
                        <li> <a href="javascript:void(0);" class="waves-effect">REPORT <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/assurance/detailMatrix">REPORT MATRIX ASR</a></li>
                                <li><a href="/ReportIbooster/{{ date('Y-m-d') }}">REPORT IBOOSTER</a></li>
                            </ul>
                        </li>
                        {{-- <li><a href="/gaulbysebab?sektor=&date={{ date('Y-m') }}">PENYEBAB GAUL</a></li> --}}
                        {{-- <li><a href="/gaulbyaction?sektor=&date={{ date('Y-m') }}">ACTION GAUL</a></li> --}}
                        {{-- <li><a href="/trendgaul">TREND GAUL</a></li> --}}
                        {{-- <li><a href="/dashboard/unspec">UNDERSPEC</a></li> --}}
                        {{-- <li><a href="/dashboard/new/unspec/{{ date('Y-m-d') }}">UNDERSPEC BY TOMMAN</a></li> --}}
                        {{-- <li><a href="/dashboard/assurance/monitoring-nossa/{{ date('Y-m-d') }}">MONITORING NOSSA</a></li> --}}
                        <li> <a href="javascript:void(0);" class="waves-effect">DASHBOARD <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/dashboard/assurance?witel=KALSEL&date={{ date('Y-m-d') }}">DASHBOARD ORDER</a></li>
                                <li><a href="/dashboardTekSQM/{{ date('Y-m-d') }}">DASHBOARD PROGRESS SQM</a></li>
                                {{-- <li><a href="/dashboard/rekon/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD REKON</a></li> --}}
                                {{-- <li><a href="/dashboard/splitter-gendong">DASHBOARD SPLITTER GENDONG</a></li> --}}
                            </ul>
                        </li>
                        <li><a href="/assurance/inbox/mediaCarring?status=UNDEFINED&tanggal={{ date('Y-m-d') }}">MEDIA CARRING</a></li>
                        <li><a href="/stoCustomer/{{ date('Y-m-d') }}/search">CUSTOMER BY STO INT</a></li>
                    </ul>
                </li>
                @endif

                @if(session('auth')->level != 61)
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-shield" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; HELPDESK</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/helpdesk/dashboard/{{ date('Y-m-d') }}">DASHBOARD PRODUKTIFITAS</a></li>
                        <li> <a href="javascript:void(0);" class="waves-effect">PROVISIONING <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/helpdesk_inbox">INBOX</a></li>
                                {{-- <li><a href="/helpdesk_inbox_booked">YOUR BOOKED</a></li>
                                <li><a href="/helpdesk_close/{{ date('Y-m-d') }}">CLOSE</a></li> --}}
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">ASSURANCE <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/helpdesk/assurance/config">INBOX</a></li>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">MANJA <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/helpdesk/manja/pending">PENDING</a></li>
                                <li><a href="/helpdesk/manja/berangkat">BERANGKAT</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                @endif

                @if(session('psb_reg') == 0 && session('auth')->level != 62)
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-headphone-alt" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; PLASA</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/ticketing">TICKETING</a></li>
                        <li><a href="/assurance/search">SEARCH ORDER</a></li>
                        @if(session('auth')->level <> 61)
                        <li><a href="/ticketing/dashboardPlasaAssurance">DASHBOARD TICKETING INT</a></li>
                        <li><a href="/ticketing/dashboard">DASHBOARD OPEN TICKET</a></li>
                        <li><a href="/ticketing/dashboardClose/{{ date('Y-m-d') }}">DASHBOARD PROGRESS TICKET</a></li>
                        @endif
                        <li><a href="/listTicketPersonal">YOUR PROGRESS TICKET</a></li>
                    </ul>
                </li>
                @endif

                @if (in_array(session('auth')->level, [2, 12, 50, 15, 51, 37, 46, 62, 61]))
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-target" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; SALES</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/dshr/plasa-sales/order/inbox">INBOX ORDER PLASA</a></li>
                        <li>
                            <a href="javascript:void(0);" class="waves-effect">DOOR TO DOOR <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                @if (in_array(session('auth')->level, [2, 15]))
                                <li><a href="/dshr/door-to-door/dashboard">DASHBOARD</a></li>
                                <li><a href="/dshr/door-to-door/assign">ASSIGN</a></li>
                                @endif
                                @if (in_array(session('auth')->level, [2, 61]))
                                <li><a href="/dshr/door-to-door/my-order?date={{ date('Y-m-d') }}">MY ORDER</a></li>
                                @endif
                            </ul>
                        </li>
                        <li><a href="/dshr/new-sales/dashboard?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">NEW SALES</a></li>
                        <li><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">TRANSAKSI</a></li>
                        <li><a href="/dashboardSalesbySPV/{{ date('Y-m') }}">DASHBOARD KENDALA BY SPV</a></li>
                    </ul>
                </li>
                @endif

                @if(session('psb_reg') == 0 && !in_array(session('auth')->level, [62, 61, 10]))
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-desktop" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; CORPORATE</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="javascript:void(0);" class="waves-effect">CCAN <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/orderCcan/input">INPUT ORDER</a></li>
                                <li><a href="{{ route('ccan') }}">MONITOR</a></li>
                                <li><a href="/dashboard/dashboad-wifi/{{ date('Y-m-d') }}">DASHBOARD</a></li>

                                <li> <a href="javascript:void(0);" class="waves-effect">MATRIX <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li><a href="/ccan/matrix/prov/{{ date('Y-m-d') }}">PROVISIONING</a></li>
                                        <li><a href="/ccan/matrix/asr/{{ date('Y-m-d') }}">ASSURANCE</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li> <a href="javascript:void(0);" class="waves-effect">MIGRASI <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/migrasi/search">WORKORDER</a></li>
                                <li><a href="/ranking/no-update">RANKING ANTRIAN</a></li>
                                <li><a href="/migrasi/monitoring">MONITORING</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                @endif

                @if(session('psb_reg') == 0 && session('auth')->level != 62)
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-package" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; WAREHOUSE</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/dashboard/warehouse?date={{ date('Y-m-d') }}">REKAP {{ date('Y-m-d') }}</a></li>
                        <li> <a href="javascript:void(0);" class="waves-effect">NTE <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/nte">DASHBOARD</a></li>
                                <li><a href="/nte/input">INPUT STOCK WH</a></li>
                                <li><a href="/nte/input/teknisi">INPUT STOCK TECH</a></li>
                                <li><a href="/nte/return/teknisi">RETURN NTE</a></li>
                            </ul>
                        </li>
                        <li><a href="/dashboard/monitoring_nte_bot?status=ALL&date={{ date('Y-m-d') }}">NTE BOT {{ date('Y-m-d') }}</a></li>
                    </ul>
                </li>
                @endif

                @if(session('auth')->level != 61)
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-panel" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; TOOLS</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/cookiesystem?witel={{ session('auth')->nama_witel }}">COOKIES</a></li>
                        <li><a href="/whatsapp-api">WHATSAPP API</a></li>
                        <li><a href="https://perwira.tomman.app/input_potensi_b/{{ session('auth')->id_karyawan }}">LAPOR POTENSI BAHAYA</a></li>
                    </ul>
                </li>
                @endif
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-folder" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; DOCUMENTS</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/documentUpload/">UPLOAD</a></li>
                        <li><a href="/documentYours/">YOUR DOCUMENTS</a></li>
                        <li><a href="/documentDashboard/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD</a></li>
                    </ul>
                </li>
                @if(in_array(session('auth')->level, [2, 15, 46, 62]))
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; SUPER ADMIN</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/employee">EMPLOYEE</a></li>
                        <li><a href="/user/v2">USER</a></li>
                        <li><a href="/team">TEAM</a></li>
                        {{-- <li><a href="/datel">Datel</a></li> --}}
                    </ul>
                </li>
                @endif
                @endif

                @endif {{-- ENDIF LEVEL <> 61 --}}
                @if(session('psb_reg') != 0 && !in_array(session('auth')->level, [62, 10]))
                <li class="nav-small-cap">--- MAIN MENU</li>

                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-star" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; PROVISIONING</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="javascript:void(0);" class="waves-effect">DASHBOARD <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                @if (in_array(session('auth')->nama_witel, ['KALSEL', 'BALIKPAPAN']))
                                <li><a href="/dashboard/tactical/pickupOnline?source=kpro&witel={{ session('auth')->nama_witel }}">DASHBOARD PICKUP ONLINE</a></li>
                                @endif
                                <li><a href="/dashboardSC">DASHBOARD SC</a></li>
                                <li><a href="/dashboard/kpro/reportQC2?area=WITEL&source=QC2&startDate=2022-04-01&endDate={{ date('Y-m-d') }}">DASHBOARD REPORT UTQC2</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-folder" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; DOCUMENTS</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/documentUpload/">UPLOAD</a></li>
                        <li><a href="/documentYours/">YOUR DOCUMENTS</a></li>
                        <li><a href="/documentDashboard/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">DASHBOARD</a></li>
                    </ul>
                </li>
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-panel" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; TOOLS</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/whatsapp-api">WHATSAPP API</a></li>
                    </ul>
                </li>
                {{-- <li><a href="/dashboard/assurance?witel=BALIKPAPAN&date={{ date('Y-m-d') }}" class="waves-effect"><i class="ti-stats-up" class="linea-icon linea-basic fa-fw"></i>&nbsp; Dashboard</a></li>
                <li><a href="/assurance/monetIbooster?witel=BALIKPAPAN" class="waves-effect"><i class="ti-pulse" class="linea-icon linea-basic fa-fw"></i>&nbsp; Pelanggan Kritis</a></li> --}}
                @endif

                @if(session('psb_reg') == 0)
                @if(session('auth')->level != 61)
                @if (!in_array(session('auth')->id_karyawan, ['CTB2023001', 'CTB2023002', 'CTB2023003', 'CTB2023004', 'CTB2023005', 'CTB2023006']))
                <li class="nav-small-cap">--- DOWNLOAD</li>
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-server" class="linea-icon linea-basic fa-fw" ></i> <span class="hide-menu">&nbsp; DATABASE</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/download/scbe/{{ date('Y-m') }}" >SCBE <?php echo date('Y-m'); ?></a>
                        <li><a href="/download/myir-not-sc/{{ date('Y-m') }}">MYIR NOT SC <?php echo date('Y-m'); ?></a>
                        <li><a href="/download/assurance/{{ date('Y-m') }}">ASSURANCE <?php echo date('Y-m'); ?></a>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap">--- CONTACT US</li>
                <li><a href="/feedback" class="waves-effect"><i data-icon="&#xe008;" class="ti-comments"></i> <span class="hide-menu">&nbsp; FEEDBACK</span></a></li> --}}
                @endif
                @endif
                @endif
                </ul>
            </div>
        </div>
        {{-- Left navbar-header end --}}
        {{-- Page Content --}}
        <div id="page-wrapper">
            <div class="container-fluid">
                <br />
                {{-- /.row --}}
                <div class="row">

                </div>

                {{--row --}}

                {{-- row --}}
      @yield('content')
                {{-- /.row --}}
                {{-- .right-sidebar --}}
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="/elitetheme/javascript:void(0)" theme="default-dark" class="default-dark-theme working">7</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            {{-- <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
                {{-- /.right-sidebar --}}
            </div>
            {{-- /.container-fluid --}}
            <footer class="footer text-center">&copy; Since 2016 - {{ date('Y') }} BIAWAK . All Rights Reserved</footer>
        </div>
        {{-- /#page-wrapper --}}
    </div>
    {{-- /#wrapper --}}
    {{-- jQuery --}}
    {{-- <script src="/elitetheme/plugins/bower_components/jquery/dist/jquery.min.js"></script> --}}
    {{-- Bootstrap Core JavaScript --}}
    <script src="/elitetheme/bootstrap/dist/js/tether.min.js"></script>
    <script src="/elitetheme/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    {{-- Menu Plugin JavaScript --}}
    <script src="/elitetheme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    {{--slimscroll JavaScript --}}
    <script src="/elitetheme/js/jquery.slimscroll.js"></script>
    {{--Wave Effects --}}
    <script src="/elitetheme/js/waves.js"></script>
    {{-- Sweet-Alert  --}}
    {{-- <script src="/elitetheme/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script> --}}
    {{--Mask --}}
    <script src="/elitetheme/js/mask.js"></script>
    {{-- Validator --}}
    <script src="/elitetheme/js/validator.js"></script>

    {{--Counter js --}}
    <script src="/elitetheme/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="/elitetheme/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    {{--Morris JavaScript --}}
    <script src="/elitetheme/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="/elitetheme/plugins/bower_components/morrisjs/morris.js"></script>
    {{-- <script src="/elitetheme/js/morris-data.js"></script> --}}
    {{-- Custom Theme JavaScript --}}
    <script src="/elitetheme/js/custom.min.js"></script>
    <script src="/elitetheme/js/jasny-bootstrap.js"></script>
    <script src="/elitetheme/js/dashboard1.js"></script>
    {{-- Footable --}}
    <script src="/elitetheme/plugins/bower_components/footable/js/footable.all.min.js"></script>
    <script src="/elitetheme/js/footable-init.js"></script>
    {{-- <script src="/elitetheme/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script> --}}
    {{-- Sparkline chart JavaScript --}}
    <script src="/elitetheme/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="/elitetheme/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="/elitetheme/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="/elitetheme/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="/elitetheme/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="/elitetheme/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="/elitetheme/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="/elitetheme/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- jQuery file upload -->
    <script src="/elitetheme/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    {{-- Data Table --}}
    <script src="/elitetheme/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- icheck -->
    <script src="/elitetheme/plugins/bower_components/icheck/icheck.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/icheck/icheck.init.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->

    {{-- <script type="text/javascript">
    $(document).ready(function() {
        $.toast({
            heading: 'Hai {{ session('auth')->nama }} ...',
            text: '',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'default',
            hideAfter: 3500,
            stack: 6
        })
    });
    </script> --}}
    {{--Style Switcher --}}
    <script src="/elitetheme/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    {{-- <script src="/bower_components/select2/select2.min.js"></script> --}}

    {{-- <script src="/js/marquee3k.min.js"></script> --}}
        <script>
// Marquee3k.init()

</script>
      @yield('plugins')
</body>
{{-- close block tech --}}
@endif
{{-- close block tech --}}
</html>
