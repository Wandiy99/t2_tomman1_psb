@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    td {
      padding : 5px;
      border:1px solid #ecf0f1;
    }
    .pdnol {
      padding : 0px;
    }
    .center {
      text-align: center;
    }
    th {
      text-align: center;
      vertical-align: middle;
      background-color: #0073b7;
      padding : 5px;
      color : #FFF;
      border:1px solid #ecf0f1;
    }
    td {
      font-size: 14px;
      padding : 3px !important;

    }
    .center a {
      color : #666666 !important;
    }
    .green {
      background-color: #2ecc71;
      font-weight: bold;
      color : #FFF;
    }
    .green a:link{
      color : #FFF;
    }
    .red {
      background-color: #e67e22;
      font-weight: bold;
      color : #FFF;
    }
    .redx {
      background-color: #e74c3c;
      font-weight: bold;
      color : #FFF;
    }
    .red a:link{
      color : #FFF;
    }
    .gray {
      background-color : #f4f4f4;
    }
    .yellow {
      background-color : #ffcb2b
    }
    .witel {
      background-color: #D44F3F;
    }
    .datel {
      background-color: #2F9F95;
    }
    .status {
      background-color: #2E8BC0;
      color : #FFF !important;
      font-weight: bold;

    }
    .padding-0 {
      padding : 0px !important;
    }
  </style>
  <div class="row">
  <p class="text-muted" style="float: left;">PROVI K-PRO <i>{{ @$log_kpro->last_grab }}</i></p>
    <div class="col-sm-12 table-responsive">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dashboard KPRO x TOMMAN
            </div>
            <div class="panel-body table-responsive padding-0">
                <table class="table">
                  <tr>
                    <th class="align-middle witel">STATUS</th>
                    <th class="align-middle datel">BANJARMASIN</th>
                    <th class="align-middle datel">BANJARBARU</th>
                    <th class="align-middle datel">TANJUNG</th>
                    <th class="align-middle datel">BATULICIN</th>
                    <th class="align-middle witel">TOTAL</th>
                  </tr>
                    <?php
                      $potensi_ps_banjarmasin = 0;
                      $potensi_ps_banjarbaru = 0;
                      $potensi_ps_batulicin = 0;
                      $potensi_ps_tanjung = 0;
                      $total = 0;
                    ?>
                    @foreach ($query2p3p_potensi_ps as $result)
                    <?php
                      $potensi_ps_banjarmasin += $result->BANJARMASIN;
                      $potensi_ps_banjarbaru += $result->BANJARBARU;
                      $potensi_ps_batulicin += $result->BATULICIN;
                      $potensi_ps_tanjung += $result->TANJUNG;
                      $total += $result->jumlah;
                    ?>
                    <tr>
                        <td>{{ $result->status_resume_group_x }}</td>
                        <td class="center">{{ $result->BANJARMASIN }}</td>
                        <td class="center">{{ $result->BANJARBARU }}</td>
                        <td class="center">{{ $result->TANJUNG }}</td>
                        <td class="center">{{ $result->BATULICIN }}</td>
                        <td class="center">{{ $result->jumlah }}</td>
                    </tr>
                    @endforeach
                    <tr>
                      <td class="status"><i>TOTAL POTENSI PS</i></td>
                      <td class="center status"><i>{{ $potensi_ps_banjarmasin }}</i></td>
                      <td class="center status"><i>{{ $potensi_ps_banjarbaru }}</i></td>
                      <td class="center status"><i>{{ $potensi_ps_tanjung }}</i></td>
                      <td class="center status"><i>{{ $potensi_ps_batulicin }}</i></td>
                      <td class="center status"><i>{{ $total }}</i></td>
                    </tr>
                    <tr>
                      <th class="align-middle witel">STATUS</th>
                      <th class="align-middle datel">BANJARMASIN</th>
                      <th class="align-middle datel">BANJARBARU</th>
                      <th class="align-middle datel">TANJUNG</th>
                      <th class="align-middle datel">BATULICIN</th>
                      <th class="align-middle witel">TOTAL</th>
                    </tr>
                    <?php
                    // sisa PI
                    $sisa_pi_banjarmasin = 0;
                    $sisa_pi_banjarbaru = 0;
                    $sisa_pi_tanjung = 0;
                    $sisa_pi_batulicin = 0;
                    $sisa_pi = 0;
                    ?>
                    @foreach ($query2p3p_sisa_pi as $result)
                    <?php
                    $sisa_pi_banjarmasin += $result->BANJARMASIN;
                    $sisa_pi_banjarbaru += $result->BANJARBARU;
                    $sisa_pi_tanjung += $result->TANJUNG;
                    $sisa_pi_batulicin += $result->BATULICIN;
                    $sisa_pi += $result->jumlah;
                    ?>
                    <tr>
                      <td>{{ $result->status_resume_group_x ? : 'ANTRIAN ORDER' }}</td>
                      <td class="center">{{ $result->BANJARMASIN }}</td>
                      <td class="center">{{ $result->BANJARBARU }}</td>
                      <td class="center">{{ $result->TANJUNG }}</td>
                      <td class="center">{{ $result->BATULICIN }}</td>
                      <td class="center">{{ $result->jumlah }}</td>
                    </tr>
                    @endforeach
                    <tr>
                      <td class="status"><i>TOTAL SISA PI</i></td>
                      <td class="center status"><i>{{ $sisa_pi_banjarmasin }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_banjarbaru }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_tanjung }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_batulicin }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi }}</i></td>
                    </tr>
                    <tr>
                      <th class="align-middle witel">STATUS</th>
                      <th class="align-middle datel">BANJARMASIN</th>
                      <th class="align-middle datel">BANJARBARU</th>
                      <th class="align-middle datel">TANJUNG</th>
                      <th class="align-middle datel">BATULICIN</th>
                      <th class="align-middle witel">TOTAL</th>
                    </tr>
                    <?php
                    // sisa PI
                    $sisa_pi_banjarmasin = 0;
                    $sisa_pi_banjarbaru = 0;
                    $sisa_pi_tanjung = 0;
                    $sisa_pi_batulicin = 0;
                    $sisa_pi = 0;
                    ?>
                    @foreach ($query2p3p_sisa_fwfm as $result)
                    <?php
                    $sisa_pi_banjarmasin += $result->BANJARMASIN;
                    $sisa_pi_banjarbaru += $result->BANJARBARU;
                    $sisa_pi_tanjung += $result->TANJUNG;
                    $sisa_pi_batulicin += $result->BATULICIN;
                    $sisa_pi += $result->jumlah;
                    ?>
                    <tr>
                      <td>{{ $result->status_resume_group_x ? : 'ANTRIAN ORDER' }}</td>
                      <td class="center">{{ $result->BANJARMASIN }}</td>
                      <td class="center">{{ $result->BANJARBARU }}</td>
                      <td class="center">{{ $result->TANJUNG }}</td>
                      <td class="center">{{ $result->BATULICIN }}</td>
                      <td class="center">{{ $result->jumlah }}</td>
                    </tr>
                    @endforeach
                    <tr>
                      <td class="status"><i>TOTAL SISA FWFM</i></td>
                      <td class="center status"><i>{{ $sisa_pi_banjarmasin }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_banjarbaru }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_tanjung }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi_batulicin }}</i></td>
                      <td class="center status"><i>{{ $sisa_pi }}</i></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
  </div>
  @endsection
