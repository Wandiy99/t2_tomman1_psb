<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/favicon.png">
    <title>BIAWAK</title> 
    <!-- Bootstrap Core CSS -->
    <link href="/elitetheme/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/elitetheme/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/elitetheme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/elitetheme/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="/elitetheme/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="/elitetheme/{{ session('theme') }}/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/elitetheme/{{ session('theme') }}/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="/elitetheme/{{ session('theme') }}/css/colors/default-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/elitetheme/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="/elitetheme/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-19175540-9', 'auto');
    ga('send', 'pageview');
    </script>
    <style>
        .label {
            color : #FFF !important;
            margin-bottom : 5px !important;
        }
        .label-default {
            color : #000 !important;
        }
        
    </style>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="/elitetheme/javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part" style="padding-left : 20px !important"><a class="logo" href="/"><b>Tomman</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;v1.7</a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="#" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="/elitetheme/javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- .Megamenu -->
                    <li class="mega-dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Menu</span> <i class="icon-options-vertical"></i></a>
                        <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Forms Elements</li>
                                    <li><a href="/elitetheme/form-basic.html">Basic Forms</a></li>
                                    <li><a href="/elitetheme/form-layout.html">Form Layout</a></li>
                                    <li><a href="/elitetheme/form-advanced.html">Form Addons</a></li>
                                    <li><a href="/elitetheme/form-material-elements.html">Form Material</a></li>
                                    <li><a href="/elitetheme/form-float-input.html">Form Float Input</a></li>
                                    <li><a href="/elitetheme/form-upload.html">File Upload</a></li>
                                    <li><a href="/elitetheme/form-mask.html">Form Mask</a></li>
                                    <li><a href="/elitetheme/form-img-cropper.html">Image Cropping</a></li>
                                    <li><a href="/elitetheme/form-validation.html">Form Validation</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Advance Forms</li>
                                    <li><a href="/elitetheme/form-dropzone.html">File Dropzone</a></li>
                                    <li><a href="/elitetheme/form-pickers.html">Form-pickers</a></li>
                                    <li><a href="/elitetheme/icheck-control.html">Icheck Form Controls</a></li>
                                    <li><a href="/elitetheme/form-wizard.html">Form-wizards</a></li>
                                    <li><a href="/elitetheme/form-typehead.html">Typehead</a></li>
                                    <li><a href="/elitetheme/form-xeditable.html">X-editable</a></li>
                                    <li><a href="/elitetheme/form-summernote.html">Summernote</a></li>
                                    <li><a href="/elitetheme/form-bootstrap-wysihtml5.html">Bootstrap wysihtml5</a></li>
                                    <li><a href="/elitetheme/form-tinymce-wysihtml5.html">Tinymce wysihtml5</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Table Example</li>
                                    <li><a href="/elitetheme/basic-table.html">Basic Tables</a></li>
                                    <li><a href="/elitetheme/table-layouts.html">Table Layouts</a></li>
                                    <li><a href="/elitetheme/data-table.html">Data Table</a></li>
                                    <li class="hidden"><a href="/elitetheme/crud-table.html">Crud Table</a></li>
                                    <li><a href="/elitetheme/bootstrap-tables.html">Bootstrap Tables</a></li>
                                    <li><a href="/elitetheme/responsive-tables.html">Responsive Tables</a></li>
                                    <li><a href="/elitetheme/editable-tables.html">Editable Tables</a></li>
                                    <li><a href="/elitetheme/foo-tables.html">FooTables</a></li>
                                    <li><a href="/elitetheme/jsgrid.html">JsGrid Tables</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Charts</li>
                                    <li> <a href="/elitetheme/flot.html">Flot Charts</a> </li>
                                    <li><a href="/elitetheme/morris-chart.html">Morris Chart</a></li>
                                    <li><a href="/elitetheme/chart-js.html">Chart-js</a></li>
                                    <li><a href="/elitetheme/peity-chart.html">Peity Charts</a></li>
                                    <li><a href="/elitetheme/knob-chart.html">Knob Charts</a></li>
                                    <li><a href="/elitetheme/sparkline-chart.html">Sparkline charts</a></li>
                                    <li><a href="/elitetheme/extra-charts.html">Extra Charts</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-12 m-t-40 demo-box">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-purple"><a href="/elitetheme/eliteadmin-inverse/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 1</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-success"><a href="/elitetheme/eliteadmin/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 2</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-info"><a href="/elitetheme/eliteadmin-ecommerce/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 3</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-inverse"><a href="/elitetheme/eliteadmin-horizontal-navbar/index3.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 4</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-warning"><a href="/elitetheme/eliteadmin-iconbar/index4.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 5</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-danger"><a href="/elitetheme/https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820" target="_blank" class="text-white"><i class="linea-icon linea-ecommerce fa-fw" data-icon="d"></i><br/>Buy Now</a></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- /.Megamenu -->
                    <li class="right-side-toggle"> <a class="waves-effect waves-light" href="#"><i class="ti-settings"></i></a></li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="/elitetheme/plugins/images/users/user-male-icon.png" alt="user-img" class="img-circle"></div> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ session('auth')->nama }} <span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/accountsetting"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    <li> <a href="/elitetheme/index.html" class="waves-effect active"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Provisioning</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/bon/Installed/<?= date('Y-m-d') ?>">MONITORING LAPORAN</a></li>
                            <li><a href="/dispatch/search">WORKORDER BY SC</a></li>
                            <li><a href="/scbe/search">WORKORDER BY SCBE</a></li>
                            <li><a href="/manja/list/{{ date('Y-m-d') }}">MANJA DISPATCH</a></li>
                            <li><a href="/produktifitastek/{{ date('Y-m-d') }}/1/ALL">PRODUKTIFITAS</a></li>
                            <li><a href="/dispatch/matrix/<?php echo date('Y-m-d') ?>">MATRIX ORDER</a></li>
                            <li><a href="/dispatch/matrixGo/<?php echo date('Y-m-d') ?>">MATRIX ORDER GO</a></li>
                            <li><a href="/customGrab">CUSTOM GRAB SC</a></li>
                            <li><a href="/dashboard/rekon/<?php echo date('Y-m') ?>">DASHBOARD REKON</a></li>
                            <li><a href="/dashboard/scbe/{{ date('Y-m-d') }}">DASHBOARD SCBE</a></li>
                        </ul>
                    </li>
                    <li> <a href="/elitetheme/javascript:void(0);" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="7"></i> <span class="hide-menu text-danger"> Assurance <span class="fa arrow"></span> <span class="label label-rouded label-danger pull-right">HOT</span></span></a>
                        <ul class="nav nav-second-level">
                           
                        </ul>
                    </li>
                    <li><a href="/elitetheme/inbox.html" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Maintenance <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                           
                        </ul>
                    </li>
                    @if (session('auth')->level==2 || session('auth')->level == 12 || session('auth')->level==50 || session('auth')->level==15 || session('auth')->level==51 || session('auth')->level==37 || session('auth')->level==46 || session('auth')->level == 62)
                    <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/dshr/plasa-sales/list-wo-by-sales/ALL/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}">TRANSAKSI</a></li>
                            <li><a href="/dashboardSalesbySPV/{{ date('Y-m') }}">DASHBOARD KENDALA by SPV</a></li>
                        </ul>
                    </li>
                    @endif
                    <li> <a href="/elitetheme/forms.html" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Plasa<span class="fa arrow"></span></span></a>
                        @if(session('psb_reg')<>1 && session('auth')->level <> 62)
                        <ul class="nav nav-second-level">
                            <li><a href="/ticketing">Ticketing</a></li>
                            <li><a href="/assurance">Search</a></li>
                            @if(session('auth')->level<>61)
                            <li><a href="/ticketing/dashboard">Dashboard Open</a></li>
                            <li><a href="/ticketing/dashboardClose/{{ date('Y-m-d') }}">Dashboard Progress</a></li>
                            @endif
                            <li><a href="/listTicketPersonal">Your Progress</a></li>
                        </ul>
                        @endif
                    </li>
                    <li class="nav-small-cap">--- TOOLS</li>
                    <li> <a href="#" class="waves-effect"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">SCNCX<span class="fa arrow"></span><span class="label label-rouded label-purple pull-right">30</span></span></a>
                        <ul class="nav nav-second-level">
                          
                        </ul>
                    </li>
                    
                   
                   
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ strtoupper(Request::segment(1)) }}</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">{{ Request::segment(1) }}</a></li>
                            <li class="active">{{ Request::segment(2) }}</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
              
                </div>
      
                <!--row -->
     
                <!-- row -->
      @yield('content')
                <!-- /.row -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="/elitetheme/javascript:void(0)" theme="default-dark" class="default-dark-theme working">7</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="/elitetheme/javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="/elitetheme/javascript:void(0)"><img src="/elitetheme/plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div> 
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2020 &copy; BIAWAK</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
    <script src="/elitetheme/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/elitetheme/bootstrap/dist/js/tether.min.js"></script>
    <script src="/elitetheme/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="/elitetheme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="/elitetheme/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="/elitetheme/js/waves.js"></script>
    <!--Counter js -->
    <script src="/elitetheme/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="/elitetheme/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="/elitetheme/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="/elitetheme/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="/elitetheme/js/custom.min.js"></script>
    <script src="/elitetheme/js/dashboard1.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="/elitetheme/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="/elitetheme/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="/elitetheme/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        // $.toast({
        //     heading: 'Welcome to Tomman v1.7',
        //     text: 'Inputkan Feedback Anda, jika ada masukkan.',
        //     position: 'top-right',
        //     loaderBg: '#ff6849',
        //     icon: 'info',
        //     hideAfter: 3500,
        //     stack: 6
        // })
    });
    </script>
    <!--Style Switcher -->
    <script src="/elitetheme/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
