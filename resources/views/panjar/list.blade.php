@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/panjar/input" class="btn btn-sm btn-info">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    Panjar
  </h3>

  <div class="list-group">
    @foreach($list as $data)
      <a href="/panjar/{{ $data->id }}" class="list-group-item">
        <strong>{{ number_format($data->nominal) }}</strong>
        <br />
        <span>{{ $data->keperluan }}</span>
        <br />
        <span>{{ $data->ts }}</span>
      </a>
    @endforeach
  </div>
@endsection
