<div id="mapMarkerModal" class="modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
                <h4 class="modal-title">
                    <span id="mapMarkerModal_title"></span>
                    (
                    <span id="mapMarkerModal_latText">0</span>
                    ,
                    <span id="mapMarkerModal_lngText">0</span>
                    )
                </h4>
            </div>

            <div class="modal-body">
                <div id="mapMarkerModal_mapView"></div>
            </div>

            <div id="mapMarkerModal_footer" class="modal-footer">
                <button class="btn btn-warning pull-left" data-dismiss="modal" type="reset">
                    <i class="fa fa-ban"></i>
                    <span>Batal</span>
                </button>

                <button style="margin-right: 20px" id="mapMarkerModal_gpsBtn" class="btn btn-default" type="button">
                    <i class="fa fa-map-marker"></i>
                    <span>GPS</span>
                </button>

                <button id="mapMarkerModal_okBtn" class="btn btn-primary" type="button">
                    <i class="fa fa-check"></i>
                    <span>OK</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="/js/mapbox-gl.js"></script>

<script>
    (function (){
        var mapEl = document.getElementById('mapMarkerModal_mapView');
        mapEl.style.height = (window.innerHeight * .65) + 'px';

        var center = { lng: 114.59075, lat: -3.31987 };

        var map;
        var marker;
        var $in;
        var $modal = $('#mapMarkerModal');
        var $title = $('#mapMarkerModal_title');
        var $latText = $('#mapMarkerModal_latText');
        var $lngText = $('#mapMarkerModal_lngText');

        var $okBtn = $('#mapMarkerModal_okBtn');

        var setLngLat = function(ll) {
            marker.setLngLat(ll);
            map.panTo(ll);

            $latText.html(ll.lat);
            $lngText.html(ll.lng);
        }

        var getLngLat = function(text) {
            var tokens = text.split(',');
            if (tokens.length != 2) return center;

            return {
                lat: tokens[0].trim(),
                lng: tokens[1].trim()
            }
        }

        var initMap = function() {
            if (map) return;

            map = new mapboxgl.Map({
                container: mapEl,
                center: center,
                zoom: 10,
                style: 'https://map.tomman.app/styles/osm-liberty/style.json'
            });
            map.on('click', function(e) { setLngLat(e.lngLat) });

            marker = new mapboxgl.Marker({ draggable: true })
                .setLngLat(center)
                .addTo(map);
        }

        $modal.on('shown.bs.modal', function() {
            initMap();
            setLngLat(getLngLat($in.val()));
        });

        $okBtn.on('click', function() {
            var lngLat = marker.getLngLat();
            $in.val(lngLat.lat + ', ' + lngLat.lng);

            $in = undefined;
            $modal.modal('hide');
        });

        $('#mapMarkerModal_gpsBtn').on('click', function() {
            navigator.geolocation.getCurrentPosition(
                function(gps) {
                    var latlng = {
                        lat: gps.coords.latitude,
                        lng: gps.coords.longitude
                    };

                    map.setZoom(17);
                    setLngLat(latlng);
                },
                function(err) {
                    if (err.code === 1) alert('ERROR: GPS Tidak Aktif\n\nAktifkan GPS kemudian coba lagi');
                    else alert('ERROR: ' + err.code + '\n' + err.message);
                },
                {
                    enableHighAccuracy: true
                }
            );
        });

        window.MapModal = {
            initButton: function($btn, $input, modalTitle) {
                $btn.on('click', function() {
                    $in = $input;
                    $title.html(modalTitle);
                    $modal.modal('show');
                });
            }
        };
    })()
</script>