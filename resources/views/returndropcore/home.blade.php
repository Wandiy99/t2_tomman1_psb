@extends('layout')

@section('content')
  @include('partial.alerts')
<style>
.verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    transform: rotate(270deg);

}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:'';
    width:0;
    padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
    display:inline-block;
    vertical-align:middle;
    text-align : right;
}

</style>

<div class="row">
    <div class="col-md-12" id="bio_tl">
      <div class="panel panel-default">
          <div class="panel-heading">
          Profile
          </div>
          <div class="panel-body table-responsive">
            <table style="padding:10px;">
              <tr>
                <td style="padding:15px">
                  <img src="/image/people.png" width="100" />
                </td>
                <td style="padding:10px">

                  {{ session('auth')->nama }}<br />
                  @foreach ($group_telegram as $gt)
                  <span class="label label-info">{{ $gt->title }}</span>
                  @endforeach
                  <br />
                  <small>
                    Kehadiran HI : {{ @$jumlah_hadir }} / {{ @count($active_team) }} ({{ @round($jumlah_hadir/count($active_team)*100) }}%)<br />

                  </small>
                </td>
                <td style="padding : 10px" valign=top>

                </td>
              </tr>

            </table>
            @include ('partial.home')
            <div class="table-responsive">
              Periode : <input type="text" id="periode" /><br />
              <table class="table">
                <tr>
                  <th>NO</th>
                  <th>TIKET</th>
                  <th>TIM</th>
                  <th>TGL</th>
                  <th>SEBAB</th>
                  <th>Created By</th>
                  <th>RETURN</th>
                  <th></th>
                </tr>
                @foreach ($list_TL as $num => $list)
                <tr>
                  <td>{{ ++$num }}</td>
                  <td>{{ $list->Ndem }}</td>
                  <td>{{ $list->uraian }}</td>
                  <td>{{ $list->tgl }}</td>
                  <td>{{ $list->penyebab }}</td>
                  <td>{{ $list->user_created }}</td>
                  <td>
                    @if ($list->returnDropcore_panjang>0)
                    {{ $list->returnDropcore_panjang }} m
                    @endif
                  </td>
                  <td>
                    <a href="/returnDropcoreInput/{{ $list->Ndem }}" class="label btn-warning">Report</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
        </div>
    </div>

@endsection
