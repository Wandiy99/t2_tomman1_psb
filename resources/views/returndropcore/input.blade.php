@extends('layout')

@section('content')
  @include('partial.alerts')
    <div class="row">
        <div class="col-sm-12">
          <a href="/returnDropcore/{{ date('Y-m') }}" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          <br />
          <br />
            <div class="panel panel-default">
                <div class="panel-heading">PENGEMBALIAN MATERIAL // INPUT</div>
                <div class="panel-body">
                  <form method="post" enctype="multipart/form-data">
                  {{ $tiket->Incident }} // {{ $tiket->Summary }}
                  <input type="hidden" name="Incident" value="{{ $in }}" />
                    <label class="form-control" for="panjang">Panjang</label>
                    <input type="text" id="panjang" name="panjang" value="{{ $tiket->returnDropcore_panjang }}" class="form-control" />
                    <br />
                    <center>
                      <div class="row text-center input-photos" style="margin: 20px 0">
                        <?php
                         $number = 1;
                         clearstatcache();
                        ?>
                        <center>
                        @foreach($photoInputs as $input)
                         <div class="col-xs-6 col-sm-3">
                             <?php
                               $path = "/upload/returnevidence/{$tiket->Ndem}/$input";
                               $th   = "$path-th.jpg";
                               $img  = "$path.jpg";
                               $flag = "";
                               $name = "flag_".$input;

                             ?>
                             @if (file_exists(public_path().$th))
                               <a href="{{ $img }}">
                                 <img src="{{ $th }}" alt="{{ $input }}" />
                               </a>
                               <?php
                                 $flag = 2;
                               ?>
                             @else
                               <img src="/image/placeholder.gif" alt="" />
                             @endif
                             <br />
                             <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
                             <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
                             <button type="button" class="btn btn-sm btn-info">
                               <i class="glyphicon glyphicon-camera"></i>
                             </button>
                             <p>{{ str_replace('_',' ',$input) }}</p>
                             {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
                          @endforeach
                        </center>
                      </div>
                    </center>
                    <br />
                    <input type="submit" class="form-control btn-success" />
                  </form>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(function (){
      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);

          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

    });
  </script>
@endsection
