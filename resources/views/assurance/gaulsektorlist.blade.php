@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>{{ $title }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-content table-responsive">
          <table class="table">
            <tr>
              <th>No</th>
              <th>TICKET_ID</th>
              <th>ND</th>
              <th>TGL_OPEN</th>
              <th>TGL_CLOSE</th>
              <th>TTR</th>
              <th>IS_GAUL</th>
              <th>WORKZONE</th>
              <th>CREW</th>
              <th>NAMA_TEKNISI</th>
              <th>TIM TOMMAN</th>
              <th>ACTION</th>
              <th>SEBAB</th>
              <th>CATATAN</th>
              <th>HEADLINE</th>
              <th>LAYANAN GGN</th>
              <th>CHANNEL</th>
              <th>ALPRO</th>
              <th>DATEK</th>
              <th>ACTUAL_SOLUTION</th>
            </tr>
            @foreach ($get_gaulsektorlist as $num => $gaulsektorlist)
            <tr>
              <td>{{ ++$num }}</td>
              <td>{{ $gaulsektorlist->ticket_id }}</td>
              <td>{{ $gaulsektorlist->nd }}</td>
              <td>{{ $gaulsektorlist->tgl_open }}</td>
              <td>{{ $gaulsektorlist->tgl_close }}</td>
              <td>{{ $gaulsektorlist->lama_ggn_jam }}</td>
              <td>{{ $gaulsektorlist->is_gaul }}</td>
              <td>{{ $gaulsektorlist->workzone }}</td>
              <td>{{ $gaulsektorlist->crew }}</td>
              <td>{{ $gaulsektorlist->nama_teknisi }}</td>
              <td>{{ $gaulsektorlist->uraian }}</td>
              <td>{{ $gaulsektorlist->action }}</td>
              <td>{{ $gaulsektorlist->penyebab }}</td>
              <td>{{ $gaulsektorlist->trouble_headline }}</td>
              <td>{{ $gaulsektorlist->layanan_ggn }}</td>
              <td>{{ $gaulsektorlist->channel }}</td>
              <td>{{ $gaulsektorlist->alpro }}</td>
              <td>{{ $gaulsektorlist->datek }}</td>
              <td>{{ $gaulsektorlist->actual_solution }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
