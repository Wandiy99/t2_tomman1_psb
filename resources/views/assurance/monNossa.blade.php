@extends('layout')
 @section('content')
 <style>
    th {
      background-color: #FF0000;
      color : #FFF;
      text-align: center;
      vertical-align: middle;
    }
    td {
      color : #000;
    }
  </style>
  <h3>Monitoring Nossa {{ $tgl }}</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="table-responsive">
      <table class="table table-striped table-bordered dataTable">
        <tr align="center">
          <th>No.</th>
          <th>Incident</th>
          <th>Customer Name</th>
          <th>Summary</th>
          <th>Datek</th>
          <th>Tim</th>
          <th>Sektor</th>
          <th>Datel</th>
          <th>STO</th>
          <th>Catatan Teknisi</th>
          <th>Status Laporan</th>
          <th>Tanggal Laporan</th>
          <th>Status Nossa</th>
          <th>Update Nossa</th>
          <th>Tanggal Dispatch</th>
        </tr>
        @foreach ($getNossa as $num => $n)
        <tr>
          <center>
          <td>{{ ++$num }}</td>
          <td><a href="/tiket/{{ $n->id_dt }}">{{ $n->Incident }}</a></td>
          <td>{{ $n->customer }}</td>
          <td>{{ $n->Summary }}</td>
          <td>{{ $n->Datek }}</td>
          <td>{{ $n->tim }}</td>
          <td>{{ $n->sektor }}</td>
          <td>{{ $n->datel }}</td>
          <td>{{ $n->sto }}</td>
          <td>{{ $n->catatan }}</td>
          <td>{{ $n->sts_laporan ? : 'ANTRIAN' }}</td>
          <td>{{ $n->tgl_laporan }}</td>
          <td>{{ $n->sts_nossa }}</td>
          <td>{{ $n->tgl_nossa }}</td>
          <td>{{ $n->tgl_disp }}</td>
          </center>
        </tr>
        @endforeach
      </table>
    </div>
    </div>
  </div>
@endsection