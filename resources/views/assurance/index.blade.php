
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;
    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }
  </style>
  <h3>
    ASSURANCE
  </h3>

  <ul class="nav nav-tabs ajax">
    <li class="active"><a href="" id="search">Search</a></li>
    <li><a href="/assurance/dispatch_manual" id="1">Dispatch Manual</a></li>
    <!-- /* <li><a href="/assurance/close_list/{{date('Y-m')}}">close</a></li> */ -->

  </ul>
  <br/>
  <div id="txtHint">


  <div>
    <form class="form-group" method="post" id="submit-form" action="/assurance/search" >
      <div class="row">
        <div class="col-md-10">
          <label for="Search">Search</label>
          <input type="text" name="search" class="form-control" id="Search" value="">
        </div>
        <div class="col-md-2">
          <label for="search">&nbsp;</label>
          <button class="btn form-control btn-primary">Search</button>
        </div>
      </div>
    </form>
  </div>


  @if ($data<>NULL)
    <br />
    {{ $data }}
      @if (count($query)>0)
      <div class="table-responsive">
        <table class="table table-striped table-bordered dataTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Workorder</th>
              <th>Hasil Ukur</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($query as $result)
            <tr>
              <td>
              @if ($result->status <> "")
                <span class="label label-success">{{ $result->status ? : 'ANTRIAN' }}</span>
              @endif
                @if(!empty($result->uraian))
                    @if (session('auth')->level == 2 || session('auth')->level == 15 || session('auth')->level == 46)
                      @if ($result->dispatch_by=='4')
                          <a href="/fulfillment/hapusfg/{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}" class="label label-danger" data-toggle="tooltip">Hapus WO FG</a>
                          <a href="/fulfillment/redispatch/{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}" class="label label-info" data-toggle="tooltip" title="{{ $result->uraian }}">Re-Dispatch</a>
                      @else
                          <a href="/assurance/dispatch/{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}" class="label label-info" data-toggle="tooltip" title="{{ $result->uraian }}">Re-Dispatch</a>
                      @endif
                    @endif
                    <span class="label label-warning">{{ $result->uraian }}</span>
                    <br />
                    <a class="label label-danger" data-toggle="popover" title="Sure You Want Deleted ?" data-html="true" data-content="<a href='/assurance/delete/{{ @$result->id_dt }}/{{ @$result->ID ? : 'X' }}/{{ @$result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}'>Yes</a>">Delete</a>
                  @else
                    <a href="/assurance/dispatch/{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}" class="label label-info">Dispatch</a>
                  @endif
                  @if(empty($result->uraian))
                    @if(session('auth')->level == 2 || session('auth')->level == 15)
                    <br /><a href="/assurance/edit/{{ $result->ID }}" class="label label-danger">E D I T</a>
                    @endif
                  @endif
                  @if (session('auth')->id_karyawan=="91153709" || session('auth')->id_karyawan=="PL46071" || session('auth')->id_karyawan=="PL46082" || session('auth')->id_karyawan=="PL64201" || session('auth')->id_karyawan=="PL46080" || session('auth')->id_karyawan=="93150112")
                  <a href="/lapul/{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no }}" class="label label-warning">LAPUL</a>       
                  @endif         
                </td>
              <td>
                @if ($result->dispatch_by=='4')
                  <?php
                      $noTiket = substr($result->no_tiket, 0, 2);
                      if ($noTiket=='IN'){
                          $tiket = $result->no_tiket;
                      }
                      else{
                          $tiket = 'FG '.$result->no_tiket;
                      }
                   ?>

                  <a href="/guarantee/{{ $result->id_dt }}">{{ $tiket ? : 0 }}</a> -
                  {{ $result->no_telp }} -
                  {{ $result->no_speedy }}<br />
                @elseif ($result->id_dt <> "")
                  <a href="/tiket/{{ $result->id_dt }}">{{ $result->Ndem ? : $result->no_tiket ? : $result->tiket_no ? : 0 }}</a> -
                  {{ $result->no_telp }} -
                  {{ $result->no_speedy }}<br />
                @else
                  {{ $result->no_tiket }} -
                  {{ $result->no_telp }} -
                  {{ $result->no_speedy }}<br />
                @endif
                @if ($result->r_headline <> NULL)
                  {{ $result->r_headline }}
                @endif
              </td>
              <td>{{ $result->ONU_Link_Status ? : '-' }} / {{ $result->ONU_Rx ? : '0' }}
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      @else
      <br />
      Data Tidak ditemukan.<br />
      <a href="/nossaIn/{{ $id }}" class="btn btn-success">Re-sync from Nossa</a>
      @endif
    <br />
    <br />
  @endif

  </div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

@endsection
