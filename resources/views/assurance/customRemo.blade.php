@extends('layout')
@section('content')
  @include('partial.alerts')
  
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <h3>
      Custom Grab Remo by Marina
    </h3>
    <div class="form-group">
      <label class="control-label" for="Incident">Please Paste Incident</label>
			<input name="no_tiket" type="text" id="Incident" class="form-control" required/>
		</div>
    
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Sync</button>
    </div>
  </form>
@endsection