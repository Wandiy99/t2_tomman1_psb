@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    td,td {
      padding:2px;
    }
  </style>
  <h3>Assurance Progress</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Kendala Teknik
        </div>
        <div class="panel-body">
          <table>
            <tr>
              <th>Sektor</th>
              <th>< 12 Jam</th>
              <th>> 12 Jam</th>
              <th>> 24 Jam</th>
            </tr>
            @foreach ($get_kendala_teknik as $kendala_teknik)
            <tr>
              <td>{{ $kendala_teknik->title }}</td>
              <td>{{ $kendala_teknik->kurang12jam }}</td>
              <td>{{ $kendala_teknik->x12jam }}</td>
              <td>{{ $kendala_teknik->x24jam }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
