@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>

    ASSURANCE

  </h3>

  <ul class="nav nav-tabs">
    <li ><a href="/assurance/search" id="search">Search</a></li>
    <li ><a href="/assurance/open" id="1">Tiket Open ({{ count($list) }})</a></li>
    <li class="active"><a href="/assurancex/ont_offline" id="1">ONT OFFLINE ({{ count($ont_offline) }})</a></li>
  <li><a href="/assurance/dispatch_manual" id="1">Dispatch Manual</a></li>
  <li><a href="/assurance/close_list/{{date('Y-m')}}">close</a></li>

  </ul>
  <br/>
  <div >
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>WORK ORDER</th>
        <th>ALPRO/STO/PAKET</th>
        <th>HEADLINE</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ont_offline as $no => $data)

      <tr>
        <td>{{ ++$no }}</td>
        <td>
          @if ($data->uraian<>"")
              <a href="/assurance/dispatch/{{ $data->no_tiket }}" class="label label-info">{{ $data->uraian }}</a></span><br/>
          @else
          <a href="/assurance/dispatch/{{ $data->no_tiket }}" class="label label-info">{{ $data->uraian ? : 'Dispatch' }}</a></span><br/>
          @endif
          <span class="label label-info">{{ $data->title }}</span><br />
          <span>{{ $data->no_tiket }}</span><br />
          <span>{{ $data->no_telp }}</span><br/>
          <span>{{ $data->no_speedy }}</span><br/>

        </td>
        <td>
          <span>{{ $data->alpro }}</span><br/>
          <span>{{ $data->sto }}</span><br/>
          <span>{{ $data->STATUS_ONT }}</span><br/>
        </td>
        <td>

          <span>{{ $data->kcontact }}</span><br/>
          <span>{{ $data->headline }}</span><br/>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
@endsection
