@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
  	th,td {
  		padding : 2px;
  	}
  </style>
  <script src="/bower_components/devexpress-web-14.1/js/dx.chartjs.js"></script>
  <link rel="stylesheet" href="/bower_components/devexpress-web-14.1/css/dx.dark.css" />
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <center><h3>{{ $title }}</h3></center>
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading"></div>
  			<div class="panel-body">	
  				<form method="get">
			  		<select name="sektor">
					  	<option value="">-- ALL SEKTOR --</option>
					  	@foreach ($get_sektor as $q_sektor)
					  	<?php if ($q_sektor->chat_id == $sektor) { $selected = 'selected'; } else { $selected = ''; } ?>
					  	<option value="{{ $q_sektor->chat_id }}" {{ $selected }}>{{ $q_sektor->title }}</option>
					  	@endforeach
					</select>
					<input type="text" name="date" id="input-tgl" value="{{ date('Y-m') }}" />
				    <input type="submit" />
			    </form>
  			</div>
  		</div>
  	</div>
  	<div class="col-sm-12">
  		<div class="panel panel-default"> 
		  	<div class="panel-heading"></div>
		  	<div class="panel-body">
  				<div class="row">
	  				<div class="col-sm-4">
			  				<table>
			  					<tr>
			  						<th>No.</th>
			  						<th>Sebab</th>
			  						<th>Jml</th>
			  					</tr>
			  					<?php 
			  						$jumlah = 0;
			  					?>
			  					@foreach ($get_sebab as $num => $sebab)
			  					<?php
			  						$jumlah += $sebab->jumlah;
			  					?>
			  					<tr>
			  						<td>{{ ++$num }}.</td>
			  						<td>{{ $sebab->penyebab ? : 'NONTOMMAN' }}</td>
			  						<td>{{ $sebab->jumlah }}</td>
			  					</tr>
			  					@endforeach
			  					<tr>
			  						<th colspan="2">Total</th>
			  						<th>{{ $jumlah }}</th>
			  					</tr>
			  				</table>
			  			</div>
		  			<div class="col-sm-8">
		  				<div id="sebab"></div>
		  			</div>
		  		</div>
  			</div>
  		</div>
  	</div>
  </div>
  			<script type="text/javascript">
	          jQuery(document).ready(function($)
	          {
          $("[data-toggle='popover']").popover({html:true});
          var day = {
            format : 'yyyy-mm-dd',
            viewMode: 0,
            minViewMode: 0
          };

          $('#input-tgl').datepicker(day).on('changeDate', function(e){
            $(this).datepicker('hide');
          });

	            $("#sebab").dxChart({
	              dataSource: [
	                @foreach ($get_sebab_chart as $sebab)
	                {reporting : "{{ $sebab->penyebab ? : 'NONTOMMAN' }}", val: {{ $sebab->jumlah }}},
	                @endforeach
	              ],
	              AutoLayout : false,
	              title: "{{ $title }} {{ $periode }} ",
	              tooltip: {
	                  enabled: true,
	                customizeText: function() {
	                  return this.argumentText + "<br/>" + this.valueText;
	                }
	              },
	              size: {
	                height: 800
	              },
	              legend: {
	                visible: true
	              },

	              rotated : true,
	              series: [{
	                label: {
	                    visible: true,
	                    customizeText: function (segment) {
	                        return segment.percentText;
	                    },
	                    connector: {
	                        visible: true
	                    }
	                },
	                name : "Sebab",
	                type: "bar",
	                color : "#74b9ff",
	                argumentField: "reporting"
	              }]
	            });
	            });
            </script>
@endsection