@extends('layout')
@section('content')
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .centered {
    text-align: center;
  }
  .label-KP {
    background-color: #ff6b6b;
  }
  .label-KT {
    background-color: #0abde3;
  }
  .label-UP {
    background-color: #1dd1a1;
  }
  .label-OGP {
    background-color: #feca57;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-NP {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }
  .underline {
    text-decoration: underline;
  }
  .gaul {
    border: 2px solid #DC143C;
  }
  .label-bluepas {
      background-color : #fd79a8;
    }
  .label-purple {
      background-color : #800080;
  }
  </style>
  <link href="/elitetheme/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
  <link href="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
  <div class="row">
  @foreach ($get_sektor as $sektor)
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $sektor->title }}
          <div class="panel-action"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
      </div>
      <div class="panel-wrapper collapse in">
        <div class="panel-body">
            <div class="table-responsive">
                <table id="table_{{ $sektor->id }}" class="table display nowrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>TIM</th>
                      <th>LIST ORDER</th>
                      <th>NP</th>
                      <th>OGP</th>
                      <th>KT</th>
                      <th>KP</th>
                      <th>UP</th>
                    </tr>
                  </thead>
                    @php
                      $total_UP = $total_KT = $total_KP = $total_OGP = $total_NP = 0;
                    @endphp
                    <tbody>
                    @foreach ($matrix[$sektor->chat_id] as $team)
                    @if (@$team->uraian <> "")
                    <tr>
                      <td>{{ $team->uraian }}</td>
                      <td>
                        @php
                          $UP = $KT = $KP = $OGP = $NP = 0;
                        @endphp
                        @foreach (@$matrix[@$sektor->chat_id][@$team->id_regu] as $num => $team_order)

                          @if (@$team_order->laporan_status == "UP")
                            @php
                              $UP += 1;
                            @endphp
                          @endif
                          @if (@$team_order->grup == "KT")
                            @php
                              $KT += 1;                             
                            @endphp
                          @endif
                          @if (@$team_order->grup == "KP")
                            @php
                              $KP += 1;
                            @endphp
                          @endif
                          @if (@$team_order->grup == "OGP")
                            @php
                              $OGP += 1;
                            @endphp
                          @endif
                          @if (@$team_order->grup == "SISA")
                            @php
                              $NP += 1;                            
                            @endphp
                          @endif
                          @if (in_array(@$team_order->grup, ["NP", ""]))
                            @php
                              $NP += 1;                            
                            @endphp
                          @endif

                          @if (@$team_order->GAUL > 0)
                            @php
                              $gaul = 'gaul';
                            @endphp
                          @else
                            @php
                              $gaul = '';
                            @endphp
                          @endif
                          
                          @php
                            $labelWarna = $team_order->grup;
                          @endphp

                          @if ($team_order->laporan_status == "KIRIM TEKNISI")
                          @php
                            $labelWarna = 'bluepas';
                          @endphp
                          @elseif ($team_order->laporan_status == "RNA")
                          @php
                            $labelWarna = 'KP';
                          @endphp
                          @elseif ($team_order->laporan_status == "RESCHEDULE")
                          @php
                            $labelWarna = 'KP';
                          @endphp
                          @elseif ($team_order->laporan_status == "KENDALA HUJAN")
                          @php
                            $labelWarna = 'KT';
                          @endphp
                          @endif

                          @if ($team_order->e_reported_datex <> '')
                            @php
                              $awal  = date_create($team_order->e_reported_datex);
                              $akhir = date_create(); // waktu sekarang
                              $diff  = date_diff( $awal, $akhir );
                            @endphp
                            
                            
                            @if ($diff->d <> 0)
                              @php
                              $waktu = $diff->d.' Hari | '.$diff->h.' Jam | '.$diff->i.' Menit';
                              @endphp
                            @else
                              @php
                              $waktu = $diff->h.' Jam | '.$diff->i.' Menit';
                              @endphp
                            @endif
                          
                          @else
                            @php
                              $waktu = '';
                            @endphp
                          @endif
                        <a class="label label-{{ $labelWarna }} {{ $gaul }}" data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Open Order</b> :<br />{{ @$team_order->Reported_Date}}<br /><b>Dispatch at</b> :<br />{{ @$team_order->updated_at }}<br /><b>Status</b> : <br />{{ @$team_order->laporan_status }} / <small></small><br /><b>Catatan Teknisi</b> : <br />{{@$team_order->catatan }}<br /><a href='/tiket/{{ @$team_order->id_dt }}'>Detil</a>">
                        #{{ ++$num }} {{ @$team_order->Ndem }} // {{ @$team_order->laporan_status ? : 'ANTRIAN' }} // {{ $waktu }}

                          <?php
                          // if ($team_order->laporan_status_id == 1 && $team_order->Assigned_by=="CUSTOMERASSIGNED") {
                          //   echo "(".$team_order->is_3HS.")";
                          // } else {
                          //   if ($team_order->is_12HS>0) {
                          //     echo "(".$team_order->is_12HS.")";
                          //   }
                          // }
                          ?>
                          <?php
                          if ($team_order->GAUL>0) { echo " // GAUL"; } else { echo ""; }
                          ?> // {{ $team_order->e_reported_datex }}</a>
                          @if ($team_order->assurance_level<>"")
                          &nbsp;<span class="label label-danger">{{ $team_order->assurance_level }}</span>
                          @endif
                          <?php $pattern = "/HVC/i"; if (preg_match($pattern, $team_order->e_customer_type)){ ?>
                            &nbsp;<span class="label label-danger">{{ $team_order->e_customer_type }}</span>
                          <?php }; ?>
                          @if ($team_order->e_is=="PROACTIVE TICKET | PROACTIVE MAINTENANCE | PROACTIVE MAINTENANCE UNSPEC" || $team_order->Owner_Group=="ACCESS MAINTENANCE WITEL KALSEL (BANJARMASIN)")
                          &nbsp;<span class="label label-danger">UNSPEC</span>
                          @endif
                          <?php $pattern = "/SQM/i"; if (preg_match($pattern, $team_order->e_summary)){ ?>
                            &nbsp;<span class="label label-danger">SQM</span>
                          <?php }; ?>
                          @if($team_order->e_contact_name=="MANUAL_USER")
                          &nbsp;<span class="label label-danger">SQM MANUAL</span>
                          @endif
                          @if ($team_order->Status=="CLOSED" || $team_order->Status=="MEDIACARE" || $team_order->Status=="SALAMSIMPATIK" || $team_order->Status=="FINALCHECK")
                          &nbsp;<span class="label label-danger">CLOSE</span>
                          @endif
                          @if($team_order->statusManja=="CUSTOMERASSIGNED")
                          &nbsp;<span class="label label-danger">MANJA</span>
                          @endif
                          @if($team_order->roc_customer_assign=="CA_YES")
                          &nbsp;<span class="label label-danger">MANJA {{ $team_order->ca_manja_time }}</span>
                          @endif
                          @if($team_order->pelanggan_hvc=="YES")
                          &nbsp;<span class="label label-danger">HVC</span>
                          @endif
                          @if($team_order->ket_plasa=="1")
                          &nbsp;<span class="label label-danger">PLASA</span>
                          @endif
                          @if($team_order->ket_sqm=="1")
                          &nbsp;<span class="label label-danger">AUTOMATIC</span>
                          @endif
                          @if($team_order->ket_sales=="1")
                          &nbsp;<span class="label label-danger">SALES</span>
                          @endif
                          @if($team_order->ket_helpdesk=="1")
                          &nbsp;<span class="label label-danger">WHATSAPP</span>
                          @endif
                          @if($team_order->ket_monet=="1")
                          &nbsp;<span class="label label-danger">IBOOSTER</span>
                          @endif
                          <!-- @php
                            $under = date_create($team_order->Reported_Date);
                            $under_time = date_format($under, "H:i:s");
                            $under_date = date_format($under, "Y-m-d");
                            $date = date('Y-m-d');
                            $jam5 = "17:00:00";
                          @endphp
                          @if ( $under_date == $date && $under_time < $jam5 )
                            &nbsp;<span class="label label-danger">< JAM 5</span>
                          @endif
                          @php
                            $under = date_create($team_order->Reported_Date);
                            $under_date = date_create();
                            $diff = date_diff($under, $under_date);
                          @endphp
                          @if ($diff->d > 0)
                            &nbsp;<span class="label label-danger">H-{{ $diff->d }}</span>
                          @endif -->
                          @if ($team_order->dt_jenis_order == "IN")
                            @php
                              date_default_timezone_set('Asia/Makassar');
                              $date_conv = date('Y-m-d H:i:s', strtotime($team_order->e_reported_date));
                              $datecreate = date_create($date_conv);
                              $jam_log = date_format($datecreate, "H");
                              $ttr_jam = ($jam_now - $jam_log);
                              $ttr_balik = (24 - $jam_log + $jam_now);
                            @endphp
                            @if ($ttr_jam > 0 && !in_array($team_order->laporan_status, ["UP","UP UNSPEC"]))
                              @if ($ttr_jam <= 3)
                                &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">< 3 JAM</span>
                              @elseif ($ttr_jam <= 12)
                                &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">< 12 JAM</span>
                              @elseif ($ttr_jam > 12)
                                &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">> 12 JAM</span>
                              @endif
                            @elseif ($ttr_jam < 0 && !in_array($team_order->laporan_status, ["UP","UP UNSPEC"]))
                              &nbsp;<span class="label" style="background-color:#DC143C; color:#FFFFFF">> 12 JAM</span>
                            @endif
                          @endif
                          @if ($team_order->Owner_Group=="REMO")
                          &nbsp;<span class="label label-danger">REMO</span>
                          @endif
                          @if ($team_order->Owner_Group=="WOC FULFILLMENT KALSEL (BANJARMASIN)")
                          &nbsp;<span class="label label-danger">FFG</span>
                          @endif
                          <br />
                        @endforeach
                        <br />
                      </td>
                      <td class="centered">{{ $NP }}</td>
                      <td class="centered">{{ $OGP }}</td>
                      <td class="centered">{{ $KT }}</td>
                      <td class="centered">{{ $KP }}</td>
                      <td class="centered">{{ $UP }}</td>
                      <?php
                        $total_NP += $NP;
                        $total_OGP += $OGP;
                        $total_KT += $KT;
                        $total_KP += $KP;
                        $total_UP += $UP;
                      ?>
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th colspan="2">TOTAL</th>
                        <th class="centered">{{ $total_NP }}</th>
                        <th class="centered">{{ $total_OGP }}</th>
                        <th class="centered">{{ $total_KT }}</th>
                        <th class="centered">{{ $total_KP }}</th>
                        <th class="centered">{{ $total_UP }}</th>
                      </tr>
                    </tfoot>
                </table>
            </div>
        </div>
      </div>  
    </div>
  </div>
  @endforeach
</div>
<script src="/elitetheme/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/elitetheme/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    $('.table').DataTable({
      order: [],
      lengthMenu: [[-1, 10, 25, 50], ["All" , 10, 25, 50]]
    });
    $(".select2").select2();
    $('.selectpicker').selectpicker();
    $('#datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
    });
  });
</script>
@endsection