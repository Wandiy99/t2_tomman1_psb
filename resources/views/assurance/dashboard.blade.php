  <div class="table-responsive">          
    <table class="table">
      <thead>
        <tr>
          <th rowspan=2 class="vcenter">#</th>
          <th rowspan=2 class="vcenter">STO</th>
          <th colspan=2>PERSONAL</th>
          <th colspan=2>CORPORATE</th>
          <th rowspan=2 class="vcenter">TTL</th>
        </tr>
        <tr>
          <th>FTTH</th>
          <th>COOPPER</th>
          <th>FTTH</th>
          <th>COOPPER</th>
        </tr>
      </thead>
      <tbody>
        {{-- */
          $ttl_p_f = 0;
          $ttl_p_c = 0;
          $ttl_c_f = 0;
          $ttl_c_c = 0;
          $ttl2 = 0;
        /* --}}
        @foreach($outer as $no => $data)
        {{-- */
          $ttl = 0;
          $ttl += $data['personal_ftth']+$data['personal_co']+$data['corp_ftth']+$data['corp_co'];
          $ttl_p_f += $data['personal_ftth'];
          $ttl_p_c += $data['personal_co'];
          $ttl_c_f += $data['corp_ftth'];
          $ttl_c_c += $data['corp_co'];
          $ttl2 += $ttl;
        /* --}}
        <tr>
          <td>{{ ++$no }}</td>
          <td><span>{{ $data['sto'] }}</span></td>
          <td><span>{{ $data['personal_ftth'] }}</span></td>
          <td><span>{{ $data['personal_co'] }}</span></td>
          <td><span>{{ $data['corp_ftth'] }}</span></td>
          <td><span>{{ $data['corp_co'] }}</span></td>
          <td><span>{{ $ttl }}</span></td>
        </tr>
        @endforeach
        <tr>
          <td colspan=2>TOTAL</td>
          <td><span>{{ $ttl_p_f }}</span></td>
          <td><span>{{ $ttl_p_c }}</span></td>
          <td><span>{{ $ttl_c_f }}</span></td>
          <td><span>{{ $ttl_c_c }}</span></td>
          <td><span>{{ $ttl2 }}</span></td>
        </tr>
      </tbody>
    </table>
  </div>