@extends('layout')
@section('content')

<form method="POST" enctype="multipart/form-data">
	<h4><span class='label label-success'>TOOLS EDIT</span></h4>
	<div class="panel panel-primary">
		<div class="panel-heading">Edit {{ $data->Incident }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>No. ID</label>
						<input type="text" name="ID" class="form-control" value="{{ $data->ID ?: old('ID') }}" readonly/>
						{!! $errors->first('ID','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>No. Incident</label>
						<input type="text" name="Incident" class="form-control" value="{{ $data->Incident ?: old('Incident') }}" required>
						{!! $errors->first('Incident','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Customer Name</label>
						<input type="text" name="Customer_Name" class="form-control" value="{{ $data->Customer_Name ?: old('Customer_Name') }}" readonly/>
						{!! $errors->first('Customer_Name','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Segment Status</label>
						<input type="text" name="Segment_Status" class="form-control" value="{{ $data->Segment_Status ?: old('Segment_Status') ?: $data->Customer_Segment }}" required>
						{!! $errors->first('Segment_Status','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Workzone</label>
						<input type="text" name="Workzone" class="form-control" value="{{ $data->Workzone ?: old('Workzone') ?: $data->Witel }}" required>
						{!! $errors->first('Workzone','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>ODP</label>
						<input type="text" name="ODP" class="form-control" value="{{ $data->ODP ?: old('ODP') }}">
						{!! $errors->first('ODP','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Summary</label>
						<input type="text" name="Summary" class="form-control" value="{{ $data->Summary ?: old('Summary') }}" required/>
						{!! $errors->first('Summary','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Owner Group</label>
						<input type="text" name="Owner_Group" class="form-control" value="{{ $data->Owner_Group ?: old('Owner_Group') }}" required/>
						{!! $errors->first('Owner_Group','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>No Internet</label>
						<input type="text" name="no_internet" class="form-control" value="{{ $data->no_internet ?: old('no_internet') }}">
						{!! $errors->first('no_internet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
						<a href="/ticketing/list-delete/{{ $data->ID }}/{{ $data->Incident }}" class="btn btn-danger">HAPUS</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection