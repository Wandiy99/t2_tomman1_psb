@extends($layout)

@section('content')
  @include('partial.alerts')
  <div class="row">
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">
  				TTR 3 JAM
  			</div>
  			<div class="panel-body">
  				<table class="table">
  					<tr>
  						<td>No</td>
  						<td>Incident</td>
  						<td>Booking Date</td>
  						<td>Workzone</td>
  						<td>Status Nossa</td>
  						<td>Status Tomman</td>
  						<td>Tim</td>
  						<td></td>
  					</tr>
  					@foreach ($kpi_ttr3jam as $num => $ttr3jam)
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>{{ $ttr3jam->Incident }}</td>
  						<td>{{ $ttr3jam->Booking_Date }}</td>
  						<td>{{ $ttr3jam->Workzone }}</td>
  						<td>{{ $ttr3jam->statusopen ? : 'CLOSED' }}</td>
  						<td>{{ $ttr3jam->laporan_status }}</td>
  						<td>{{ $ttr3jam->uraian }}</td>
  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
@endsection