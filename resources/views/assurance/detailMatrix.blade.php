@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">SEKTOR {{ $sektor ? : 'NONE' }} {{ $date }}</h3>
        <div class="panel-body">
            <form method="GET">
            <center>
            <div class="col-md-10">
                <select class="form-control select2" id = "sektor" name="sektor">
                <option value="" selected disabled>Pilih Sektor</option>
                @foreach ($get_sektor as $sektor)
                    <option value="{{ $sektor->id }}">{{ @$sektor->text }}</option>
                @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <input type="submit" id="bt_direct" value="Filter" class="btn btn-primary">
            </div>
            </center>
            </form>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">REPORT MATRIX ASSURANCE SEKTOR</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>INCIDENT</th>
                        <th>INET</th>
                        <th>SEGMENT</th>
                        <th>CUSTOMER</th>
                        <th>REGU</th>
                        <th>KATEGORI</th>
                        <th>STATUS</th>
                        <th>CATATAN</th>
                        <th>ACTION</th>
                        <th>PENYEBAB</th>
                        <th>PENYEBAB RETI</th>
                        <th>UKUR DISPATCH</th>
                        <th>UKUR LAPORAN</th>
                        <th>TGL DISPATCH</th>
                        <th>REPORTED DATE</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($query as $num => $result)
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->Incident }}</td>
                    <td>{{ $result->Service_No }}</td>
                    <td>{{ $result->Customer_Segment ? : $result->Segment_Status }}</td>
                    <td>{{ $result->Customer_Name }}</td>
                    <td>{{ $result->tim }}</td>
                    <td>{{ $result->grup ? : 'NP' }}</td>
                    <td>{{ $result->laporan_status ? : 'ANTRIAN' }}</td>
                    <td>{{ $result->catatan }}</td>
                    <td>{{ $result->action }}</td>
                    <td>{{ $result->penyebab }}</td>
                    <td>{{ $result->status_penyebab_reti }}</td>
                    <td>{{ $result->ib_onu_rx }}</td>
                    <td>{{ $result->redaman_iboster }}</td>
                    <td>{{ $result->tgl }}</td>
                    <td>{{ $result->Reported_Date }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>INCIDENT</th>
                        <th>INET</th>
                        <th>SEGMENT</th>
                        <th>CUSTOMER</th>
                        <th>REGU</th>
                        <th>KATEGORI</th>
                        <th>STATUS</th>
                        <th>CATATAN</th>
                        <th>ACTION</th>
                        <th>PENYEBAB</th>
                        <th>PENYEBAB RETI</th>
                        <th>UKUR DISPATCH</th>
                        <th>UKUR LAPORAN</th>
                        <th>TGL DISPATCH</th>
                        <th>REPORTED DATE</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'REPORT MATRIX ASSURANCE SEKTOR TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'REPORT MATRIX ASSURANCE SEKTOR TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'REPORT MATRIX ASSURANCE SEKTOR TOMMAN'
                }
            ]
        });

        $('#bt_direct').click(function(){
            window.location.href = "/assurance/detailMatrix"+$('#sektor').val();
      	});
    });
</script>
@endsection