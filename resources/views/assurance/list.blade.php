@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>

    ASSURANCE

  </h3>

  <ul class="nav nav-tabs">
    <li ><a href="/assurance/search" id="search">Search</a></li>
    <li class="active"><a href="/assurance/open" id="1">Tiket Open ({{ count($list) }})</a></li>
    <li><a href="/assurancex/ont_offline" id="1">ONT OFFLINE ({{ count($ont_offline) }})</a></li>
  <li><a href="/assurance/dispatch_manual" id="1">Dispatch Manual</a></li>
  <li><a href="/assurance/close_list/{{date('Y-m')}}">close</a></li>

  </ul>
  <br/>
  <div >
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>WORK ORDER</th>
        <th>ALPRO/STO/PAKET</th>
        <th>HEADLINE</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)

      <tr>
        <td>{{ ++$no }}</td>
        <td>
          @if ($data->uraian<>"")
            @if (session('auth')->level == 2)
              <a href="/assurance/dispatch/{{ $data->no_tiket }}" class="label label-info">{{ $data->uraian }}</a></span><br/>
            @else
              {{ $data->uraian }}><br/>
            @endif
          @else
          <a href="/assurance/dispatch/{{ $data->no_tiket }}" class="label label-info">{{ $data->uraian ? : 'Dispatch' }}</a></span><br/>
          @endif
          @if(!empty($data->workerName))
            <span class="label label-warning">{{ $data->workerName }} {{ $data->takeTime }}</span>
            @if($data->workerId == session('auth')->id_karyawan)
              <a data-toggle="modal" data-target="#modal-fallout" data-sc="{{ $data->no_tiket }}" data-st="drop" class="label label-info" data-toggle="tooltip" title="{{ $data->workerName }}">Drop Order</a>
            @endif
          @else
            <a data-toggle="modal" data-target="#modal-fallout" data-sc="{{ $data->no_tiket }}" data-st="store" class="label label-info">Take Order</a>
          @endif
          <span class="label label-info">{{ $data->title }}</span><br />
          <span>{{ $data->no_tiket }}</span><br />
          <span>{{ $data->no_telp }}</span><br/>
          <span>{{ $data->no_speedy }}</span><br/>

        </td>
        <td>
          <span>{{ $data->alpro }}</span><br/>
          <span>{{ $data->sto }}</span><br/>
          <span>{{ $data->paket }}</span><br/>
        </td>
        <td>
          <span>Tanggal Open / Umur </span><br/>
          <span>{{ $data->tgl_open }}</span><br/>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
<div class="modal fade" id="modal-fallout">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="submit-form" action="/assurance/to_logic" method="post" enctype="multipart/form-data" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Verifikasi</h4>
        </div>

        <div class="modal-body">
          <input type="hidden" name="orderId" id="orderId" value=""/>
          <input type="hidden" name="status" id="status" value=""/>
          <b id="ket"></b> <label class="label label-warning" id="label-sc"></label> ?
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-info">Lanjutkan</button>
        </div>
        <form>
      </div>
    </div>
  </div>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    
    $("#modal-fallout").on('show.bs.modal', function(e) {
      if($(e.relatedTarget).data('st') == "drop"){
        $('#ket').html("Anda akan mendrop order Logic ini");
      }else{
        $('#ket').html("Anda akan mengerjakan order Logic ini");
      }
      $('#label-sc').html($(e.relatedTarget).data('sc'));
      $('#orderId').val($(e.relatedTarget).data('sc'));
      $('#status').val($(e.relatedTarget).data('st'));
    });
  });
</script>
@endsection
