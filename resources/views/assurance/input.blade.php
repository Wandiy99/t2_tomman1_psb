@extends('tech_layout_2')

@section('content')
  @include('partial.alerts')
  <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script> -->
  <!-- <script src="/js/mapmarkerASR.js"></script> -->
  <div id="mapModal" class="modal fade">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title">
                          Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                      </h4>
                  </div>
                  <div class="modal-body">
                      <div id="mapView" style="height:400px;"></div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-default" data-dismiss="modal">Batal</button>
                      <button id="btnGetMarker" class="btn btn-primary">OK</button>
                  </div>
              </div>
          </div>
      </div>

  
  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      @if($redamanIboster==NULL)
        <strong>Ukur Redaman IBOOSTER WO sebelum di UP</strong>
      @else
         <strong>Ukur Redaman IBOOSTER {{ $redamanIboster  }}, Order akan dikembalikan jika redaman tidak layak</strong>
      @endif
  </div>
        
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
  <input type="hidden" name="startLatTechnition" id="startLat">
  <input type="hidden" name="startLonTechnition" id="startLon">
    @if (isset($data->id_pl))
      <input type="hidden" name="id" value="{{ $data->id_pl }}" />
    @endif
    <h3>
      <a href="/" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
       
      @if($project->dt_nOrder <> 0)
        @if($data->jenis_order == 'INT')
          <a href="/grabIboosterbyIN/{{ $Ndem }}/assurance" class="btn btn-success">Ukur Ibooster</a>
        @elseif($data->jenis_order == 'IN')
          <a href="/grabIboosterbyManualIN/{{ $project->no_speedy }}/{{ $Ndem }}/assurance" class="btn btn-success">Ukur Ibooster</a>
        @endif
      @else
        <a class="btn btn-danger">Skip Ukur Ibooster</a>
      @endif
    
      @if(session('auth')->level <> 19)
        <button class="btn btn-primary">Simpan</button>
      @endif
      UPDATE WO
    </h3>

    <input type="hidden" name="dispatch_id" id="dispatch_id" value="{{ $project->id_dt }}" />
    <input type="hidden" name="jenis_order" value="{{ $data->jenis_order }}" />
    <input type="hidden" name="no_tiket" value="{{ $project->no_tiket }}" />
<div class="row">
  <!-- <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading" style="border-bottom-right-radius: 10px !important;border-bottom-left-radius: 10px !important;">
        <a class="btn btn-info" href="/nossaInx/{{ $project->Incident }}"><span class="glyphicon glyphicon-refresh"></span></a>
        Status Nossa
        <span class="label label-warning" style="font-size:12px">{{ $project->Status }}</span>
        <input type="hidden" name="status_nossa" value="{{ $project->Status }}" />
        {!! $errors->first('status_nossa', '<span class="label label-danger">:message</span>') !!}
      </div>
    </div>
  </div> -->
<div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Workorder Information
      </div>
      <div class="panel-body">

    <div class="form-group">
      <label class="control-label" for="input-status">Status</label>
      <input name="status" type="hidden" id="input-status" class="form-control" value="{{ old('status_laporan') ? : @$data->status_laporan }}" />
      {!! $errors->first('status', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="input-action">Action</label>
      <input name="action" type="hidden" id="input-action" class="form-control" value="{{ old('action') ? : @$data->action }}" />
      {!! $errors->first('action', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group {{ $errors->has('penyebab') ? 'has-error' : '' }}">
      <label class="control-label" for="input-penyebab">Penyebab</label>
      <input name="penyebab" type="hidden" id="input-penyebab" class="form-control" value="{{ old('idPenyebab') ? : @$data->idPenyebab }}" />
      {!! $errors->first('penyebab', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="input-penyebab-reti">Penyebab Reti</label>
      <input name="penyebab_reti" type="hidden" id="input-penyebab-reti" class="form-control" value="{{ old('penyebab_reti_id') ? : @$data->penyebab_reti_id }}" />
      {!! $errors->first('penyebab_reti', '<span class="label label-danger">:message</span>') !!}
    </div>

     <div class="form-group">
      <label class="control-label" for="input-konOdp">Kondisi ODP</label>
      <input name="konOdp" type="hidden" id="input-konOdp" class="form-control" value="{{ old('kondisi_odp') ? : @$data->kondisi_odp }}" />
      {!! $errors->first('konOdp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
      <label class="control-label" for="input-splitter">Jenis Splitter</label>
      <input name="splitter" type="hidden" id="input-splitter" class="form-control" value="{{ old('splitter') ? : @$data->splitter }}" />
      {!! $errors->first('splitter', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
        <label class="label-control">No. Telpon</label>
        <input type="number" minlength="11" name="noTelp" class="form-control" value="{{ old('no_telp') ? : @$project->no_telp }}">
        {!! $errors->first('noTelp', '<span class="label label-danger">:message</span>') !!}
    </div>

    <div class="form-group">
	    <label class="control-label" for="input-idpel">{{ $project->Ndem ? :'a' }} </br>
        {{ $project->neND_TELP ? : '0' }} ~ {{ $project->neND_INT ? : '0' }}<br />
        {{ $project->sto ? : $project->neSTO ? : '' }}<br />
        {{ $project->neHEADLINE ? : $project->headline ? : '' }}<br />
        <?php
          // $headline = explode('+',$project->headline);
          // if (count($headline)>2) :
          // for($i=0;$i<4;$i++){
          //   echo $headline[$i]."<br />";
          // }
          // echo "<br />Status <br />Link : ";
          // if ($headline[7]=="ONLINE"){
          //   $status_link = "success";
          // } else {
          //   $status_link = "danger";
          // }
          // echo '<span class="label label-'.$status_link.'">'.$headline[7].'</span><br />';
          // echo ' INET : ';
          // if ($headline[9]=="Start"){
          //   $status_link = "success";
          // } else {
          //   $status_link = "danger";
          // }
          // echo '<span class="label label-'.$status_link.'">'.$headline[9].'</span><br />';
          // echo $headline[10].'<br />';
        ?>
        <!-- <br />
        Hasil Ukur : -->
      <?php #endif; ?>
    </div>
    <div class="form-group">
      <label class="control-label"><b>Catatan Pemasangan</b></label>
      <textarea name="catatan" class="form-control" rows="4" value= "{{ old('catatan') ? : @$data->catatan }}">{{ old('catatan') ? : @$data->catatan }}</textarea>
      {!! $errors->first('catatan', '<span class="label label-danger">:message</span>') !!}
    </div>
    <!-- <div class="form-group">
        <label class="label-control"><b>ODP Labelcode</b></label>
        <input type="text" name="odp_label_code" class="form-control" value="{{ old('odp_label_code') ? : @$data->odp_label_code }}">
        {!! $errors->first('odp_label_code', '<span class="label label-danger">:message</span>') !!}
    </div> -->
    <div class="form-group">
        <label class="label-control"><b>Valins ID</b></label>
        <input type="number" minlength="5" maxlength="7"  name="valins_id" class="form-control" value="{{ old('valins_id') ? : @$data->valins_id }}">
        {!! $errors->first('valins_id', '<span class="label label-danger">:message</span>') !!}
    </div>
    <div class="form-group">
        <label class="label-control"><b>Dropcore Labelcode</b></label>
        <input type="text" minlength="12" maxlength="12"  name="dropcore_label_code" class="form-control" value="{{ old('dropcore_label_code') ? : @$data->dropcore_label_code }}">
        {!! $errors->first('dropcore_label_code', '<span class="label label-danger">:message</span>') !!}
    </div>

		  <div class="form-group">
        <label class="control-label"><b>Koordinat Pelanggan</b></label>
        &nbsp;
        <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-sm btn-default" data-toggle="tooltip" type="button">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button>
        &nbsp;
        <button id="btn-gps" type="button" class="btn btn-sm btn-info">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button>
        <div>
          <input name="kordinat_pelanggan" id="kordinat_pelanggan" class="form-control" rows="1" value="{{ old('kordinat_pelanggan') ? : @$data->kordinat_pelanggan }}" onKeyPress="return goodchars(event,'0123456789-,',this)" />
        </div>
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div>
		  <div class="form-group">
        <label class="control-label"><b>Nama ODP</b></label>
        <input name="nama_odp" class="form-control" id="nama_odp" rows="1" value="{{ old('nama_odp') ? : @$data->nama_odp }}">
        {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group"><b>
        <label class="label-control"><b>Port ODP</b></label>
        <input type="text" name="port_number" class="form-control" value="{{ old('port_number') ? : @$data->port_number }}">
        {!! $errors->first('port_number', '<span class="label label-danger">:message</span>') !!}
      </div>
      <div class="form-group">
		    <label for="" class="control-label">
          <b>Koordinat ODP</b>
          &nbsp;
          <button id="btn-gps-odp" type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-map-marker"></i>
          </button>
        </label>
        <input name="kordinat_odp" id="kordinat_odp" class="form-control" rows="1" value="{{ old('kordinat_odp') ? : @$data->kordinat_odp }}" />
      </div>
      <div class="col-xs-12">{!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}</div>
    </div>
</div>
</div>

  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">Material & NTE</div>
      <div class="panel-body">
        <input type="hidden" name="materials" value="[]" />
        @if(session('auth')->level <> 19)
          <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
            <span class="glyphicon glyphicon-plus"></span>
            Edit
          </button>
        @endif
        <input type="hidden" name="materialsNte" value="[]" />
        <button data-toggle="modal" data-target="#material-modal-nte" class="btn btn-sm btn-primary" type="button">
          <span class="glyphicon glyphicon-edit"></span>
          N T E
        </button>
        <br /><br />
        {!! $errors->first('materialsNte', '<span class="label label-danger">:message</span>') !!}
        <div class="form-group">
          <label class="control-label">NO RFC</label>
          <input type="text" minlength="5" name="rfc_number" class="form-control" value="{{ $data->rfc_number }}"/>
          {!! $errors->first('rfc_number', '<span class="label label-danger">:message</span>') !!}
        </div>

        <!-- <div class="form-group">
          <label class="control-label" for="jenis_ont" >Jenis ONT</label>
          <input name="jenis_ont" type="hidden" id="jenis_ont" class="form-control" value="{{ old('typeont') ? : @$data->typeont ? : '' }}"/>
          {!! $errors->first('jenis_ont','<p class="label label-danger">:message</p>') !!}
        </div>  
        <div class="form-group">
          <label class="control-label">SN ONT (BARU/SEKARANG)</label>
          <input type="text" name="snont" class="form-control" value="{{ old('snont') ? : @$data->snont }}"/>
          {!! $errors->first('snont','<p class="label label-danger">:message</p>') !!}
        </div>
        <div class="form-group">
          <label class="control-label">SN ONT (LAMA)</label>
          <input type="text" name="snont_lama" class="form-control" value="{{ old('snont_lama') ? : @$data->snont_lama }}"/>
          {!! $errors->first('snont_lama','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
          <label class="control-label" for="jenis_stb" >Jenis STB</label>
          <input name="jenis_stb" type="hidden" id="jenis_stb" class="form-control" value="{{ old('typestb') ? : @$data->typestb }}"/>
          {!! $errors->first('jenis_stb','<p class="label label-danger">:message</p>') !!}
        </div>  
        <div class="form-group">
          <label class="control-label">SN STB (BARU/SEKARANG)</label>
          <input type="text" name="snstb" class="form-control" value="{{ old('snstb') ? : @$data->snstb ? : '' }}"/>
          {!! $errors->first('snstb','<p class="label label-danger">:message</p>') !!}
        </div>
        <div class="form-group">
          <label class="control-label">SN STB (LAMA)</label>
          <input type="text" name="snstb_lama" class="form-control" value="{{ old('snstb_lama') ? : @$data->snstb_lama ? : '' }}"/>
          {!! $errors->first('snstb_lama','<p class="label label-danger">:message</p>') !!}
        </div> -->


        <ul id="material-list" class="list-group">
          <li class="list-group-item" v-repeat="$data | hasQty ">
            <span class="badge" v-text="qty"></span>
            <strong v-text="id_item"></strong><br>
            <!-- <strong v-text="nama_item"></strong><br>
            <strong v-text="rfc"></strong><br>
            <strong>Terpakai : </strong> <strong v-text="jmlTerpakai"></strong><br>
            <strong>Saldo : </strong> <strong v-text="saldo"></strong> -->
          </li>
        </ul>

        <ul id="material-list-nte" class="list-group">
          <li class="list-group-item" v-repeat="$data | hasQty ">
            <span class="badge" v-text="qty"></span>
            <strong v-text="jenis_nte"></strong>
            <p v-text="id"></p>
          </li>
        </ul>

      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Log
      </div>
      <div class="panel-body">
        <ul class="list-group">
        @foreach ($data_log as $log)
          <li class="list-group-item">
          {{ $log->created_by }} - {{ $log->created_name }}<br />
          {{ $log->laporan_status }} - {{ $log->created_at }}<br />
          {{ $log->catatan }}
          </li>
        @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-12" id="dokumentasi-row">
    <div class="panel panel-default" id="dokumentasi">
      <div class="panel-heading">Dokumentasi</div>
      <div class="panel-body">
        <div class="row text-center input-photos" style="margin: 20px 0">
          @foreach ($check_foto as $input)

            <div class="col-xs-6 col-sm-2">
              <?php
                $path  = "/upload4/asurance/{$project->id_dt}/$input";
                $path2 = "/upload3/asurance/{$project->id_dt}/$input";
                $th    = "$path-th.jpg";
                $th2   = "$path2-th.jpg";
                $img   = "$path.jpg";
                $img2  = "$path2.jpg";
                $flag  = "";
                $name  = "flag_".$input;
              ?>
              @if (file_exists(public_path().$th))
                <a href="{{ $img }}">
                  <img src="{{ $th }}" alt="{{ $input }}" width="100" />
                </a>
                <?php
                  $flag = 1;
                ?>
               @elseif (file_exists(public_path().$th2))
                  <a href="{{ $img2 }}">
                    <img src="{{ $th2 }}" alt="{{ $input }}" width="100" />
                  </a>
                  <?php
                    $flag = 1;
                  ?>
              @else
                <img src="/image/placeholder.gif" width="100" alt="" />
              @endif
              <br />
              <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
              <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />

              <button type="button" class="btn btn-sm btn-info">
                <i class="glyphicon glyphicon-camera"></i>
              </button>
              <p>{{ str_replace('_',' ',$input) }}</p>
              {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
            </div>
          @endforeach
        </div>
      </div>
    </div>

  <br />
  </form>
</div>
  <br />
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material Assurance</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong><br>
              <strong v-text="nama_item"></strong><br>
              <!-- <strong v-text="rfc"></strong><br>
              <p><strong>Saldo : </strong> <strong v-text="saldo"></strong></p>
              <strong v-text="id_item"></strong>
              <p v-text="nama_item"></p> -->

              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div id="material-modal-nte" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>STOCK NTE</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">

              <strong v-text="jenis_nte"></strong><br>
              <strong v-text="id"></strong><br>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('plugins')
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script src="/bower_components/vue/dist/vue.min.js"></script>
  <script>
     function getkey(e){
      if (window.event)
         return window.event.keyCode;
      else if (e)
         return e.which;
      else
         return null;
      }

      function goodchars(e, goods, field){
      var key, keychar;
      key = getkey(e);
      if (key == null) return true;

      keychar = String.fromCharCode(key);
      keychar = keychar.toLowerCase();
      goods = goods.toLowerCase();

      // check goodkeys
      if (goods.indexOf(keychar) != -1)
          return true;
      // control keys
      if ( key==null || key==0 || key==8 || key==9 || key==27 )
         return true;

      if (key == 13) {
          var i;
          for (i = 0; i < field.form.elements.length; i++)
              if (field == field.form.elements[i])
                  break;
          i = (i + 1) % field.form.elements.length;
          field.form.elements[i].focus();
          return false;
          };
      // else return false
      return false;
      }

      window.onload = function() {
        var startPos;
        var geoOptions = {
          enableHighAccuracy: true
        }

        var geoSuccess = function(position) {
          startPos = position;
          document.getElementById('startLat').value = startPos.coords.latitude;
          document.getElementById('startLon').value = startPos.coords.longitude;

          console.log(startPos.coords.latitude, startPos.coords.longitude)
        };
        var geoError = function(error) {
          console.log('Error occurred. Error code: ' + error.code);
        };

        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
      };

    $(function() {

      $("#input-penyebab").on("change",function(e){
        $("#dokumentasi").remove();
        $.ajax({
        dataType: "html",
        url: "/dokumentasi/"+e.val+"/"+$("#dispatch_id").val()+"/"+$("#input-action").val(),
        success: function (data) {
            // console.log(data);
            location.reload();x
            // $("#dokumentasi-row").append(data);
        }});
      });

      $("#input-penyebab").select2({
        initSelection: function (element, callback) {
	                var data = { "id": "{{ $data->idPenyebab ? : ''}}", "text": "{{ $data->penyebab ? : ''}}" };
	                callback(data);
	        },
	      ajax : {
				url : "/assurance/inputAction/",
				dataType : "json",
        data: function() {
          return {
            action : $("#input-action").val()
          }
        },
				results : function (data){
					var myResults = [];
					$.each(data, function (index, item) {

	                myResults.push({
	                    'id': item.id,
	                    'text': item.text
	                });
	            });
	            return {
	                results: myResults
	            };
				}
			}
    });

    // var dataOnt = [
    //   {"id":"ZTEF660", "text":"ZTEF660"},
    //   {"id":"ZTEF609", "text":"ZTEF609"},
    //   {"id":"ZTEF670L", "text":"ZTEF670L"},
    //   {"id":"ZTEF821", "text":"ZTEF821"},
    //   {"id":"ZTEF829", "text":"ZTEF829"},
    //   {"id":"ALUG240WA", "text":"ALU G240WA (HITAM)"},
    //   {"id":"ALUI240WA", "text":"ALU I240WA (PUTIH)"},
    //   {"id":"HG82455A", "text":"HUAWEI HG82455A"},
    //   {"id":"HG8245H5", "text":"HUAWEI HG8245H5"},
    //   {"id":"H87Z5675M21", "text":"HUAWEI H87Z5675M21"},
    //   {"id":"HG6243C", "text":"HUAWEI HG6243C (FIBERHOME)"}
    // ];

    // var ont = function() {
    //   return {
    //     data: dataOnt,
    //     placeholder: 'Input Jenis ONT',
    //     formatSelection: function(data) { return data.text },
    //     formatResult: function(data) {
    //       return  data.text;
    //     }
    //   }
    // }
    // $('#jenis_ont').select2(ont());

    // var dataStb = [
    //   {"id":"ZTEB700V5", "text":"ZTE STB HD"},
    //   {"id":"ZTEB760H", "text":"ZTE STB HYBRID"},
    //   {"id":"ZTEB660H", "text":"ZTE STB 4K"},
    //   {"id":"INDIBOX", "text":"INDIBOX"},
    //   {"id":"IPCAM", "text":"IPCAM"}
    // ];

    // var stb = function() {
    //   return {
    //     data: dataStb,
    //     placeholder: 'Input Jenis STB',
    //     formatSelection: function(data) { return data.text },
    //     formatResult: function(data) {
    //       return  data.text;
    //     }
    //   }
    // }
    // $('#jenis_stb').select2(stb());

      $("#nama_odp").inputmask("AAA-AAA-A{2,3}/999");

      var materials = <?= json_encode($materials) ?>;

      console.log(materials)

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('hasSaldo', function(value) {
        return value.filter(function(a) { return a.saldo > 0});
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onChange : function(item){
            if (item.qty > item.saldo) item.qty=item.saldo;
          },
          onPlus: function(item) {
            // if (!item.qty) item.qty = 0;
            console.log(item.qty);
            item.qty++;

            // if (item.qty > item.saldo) item.qty = item.qty-1;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      $('#kordinat_pelanggan').bind('copy paste', function (e) {
         e.preventDefault();
      });

      var $btnGps = $('#btn-gps');
      var $btnGpsODP = $('#btn-gps-odp');

      var $kordinat_pelanggan = $('#kordinat_pelanggan');
      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_pelanggan.val('ERROR');
          return;
        }

        $kordinat_pelanggan.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_pelanggan.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_pelanggan.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_pelanggan.val('ERROR: TIMEOUT');
              break;
          }
        });
      });
      var $kordinat_odp = $('#kordinat_odp');
      $btnGpsODP.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_odp.val('ERROR');
          return;
        }

        $kordinat_odp.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_odp.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_odp.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_odp.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          // if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: item.rfc});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });

      var data = <?= json_encode($get_laporan_status) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status').select2(select2Options());

      var dataAction = <?= json_encode($get_laporan_action) ?>;
      var selectDataAction = function() {
        return {
          data: dataAction,
          placeholder: 'Input Action',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-action').select2(selectDataAction());

      var dataPenyebabReti = <?= json_encode($get_laporan_penyebab_reti) ?>;
      var selectDataPenyebabReti = function() {
        return {
          data: dataPenyebabReti,
          placeholder: 'Input Penyebab Reti',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-penyebab-reti').select2(selectDataPenyebabReti());

      var dataKonOdp = <?= json_encode($get_laporan_odp) ?>;
      var selectDataKonOdp = function() {
        return {
          data: dataKonOdp,
          placeholder: 'Input Kondisi ODP',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-konOdp').select2(selectDataKonOdp());

      var dataSplitter = <?= json_encode($get_laporan_splitter) ?>;
      var selectDataSplitter = function() {
        return {
          data: dataSplitter,
          placeholder: 'Input Jenis Splitter',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-splitter').select2(selectDataSplitter());

      // var dataAction = <?= json_encode($get_laporan_penyebab) ?>;
      // var selectDataAction = function() {
      //   return {
      //     data: dataAction,
      //     placeholder: 'Input Penyebab',
      //     formatSelection: function(data) { return data.text },
      //     formatResult: function(data) {
      //       return  data.text;
      //     }
      //   }
      // }
      // $('#input-penyebab').select2(selectDataAction());

      // var data =  
      // var nte1 =  
      // $.extend(data, nte1);
      // var combo = $('#input-nte').select2({
      //   data: data,
      //   maximumSelectionSize: 2,
      //   multiple:true
      // });
      // $('#searchlist').btsListFilter('#searchinput', {itemChild: 'strong'});

      var ntelist = <?= json_encode(@$nte) ?>;
      var listVm = new Vue({
        el: '#material-list-nte',
        data: ntelist
      });
      var modalVm = new Vue({
        el: '#material-modal-nte',
        data: ntelist,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });
      $('#submit-form').submit(function() {
        var result = [];
        ntelist.forEach(function(item) {
          if (item.qty > 0) result.push({id: item.id, qty: item.qty, jenis: item.jenis_nte, kat: item.nte_jenis_kat});
        });
        $('input[name=materialsNte]').val(JSON.stringify(result));
      });

    })
  </script>
@endsection
