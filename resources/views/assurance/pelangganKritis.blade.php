@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/combine/npm/sweetalert2@10.13.0/dist/sweetalert2.min.css,npm/sweetalert2@10.13.0/dist/sweetalert2.min.css">
<style>
  th, td {
    text-align: center;
    vertical-align: middle;
    text-transform:uppercase;
  }
</style>
<div class="row">
    @if ($witel == "KALSEL")
    {{-- <div class="col-sm-12">
        <div class="white-box">
            <form method="GET" class="form-horizontal">
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label class="control-label col-md-1" style="text-align: left; color: #2b2b2b;">WITEL :</label>
                            <div class="col-md-11">
                                <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" name="witel">
                                    @if ($witel == "KALSEL")
                                        <option value="KALSEL">KALSEL</option>
                                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                                    @elseif ($witel == "BALIKPAPAN")
                                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                                        <option value="KALSEL">KALSEL</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label"></label>
                            <input type="submit" value="FILTER" class="btn btn-info">
                        </div>
                    </div>
                </div>
            </form>
            <h3 class="box-title">GRAFIK HASIL UKUR LOSS {{ date('Y-m') }}</h3>
            <ul class="list-inline text-right">
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Q &nbsp;</h5>
                </li>
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>SUGAR &nbsp;</h5>
                </li>
            </ul>
            <div id="morris-area-chart"></div>
        </div>
    </div> --}}
    <div class="col-sm-12">
        <div class="white-box">
            <form id="submit-form" method="post" action="/assurance/monetIboosterSave/KALSEL" enctype="multipart/form-data" autocomplete="off">
                <h3 class="box-title">File Upload</h3>
                <label for="input-file-now">Import File (.xlsx) Daftar Pelanggan Kritis *Format Disesuaikan*</label>
                <input type="file" name="upload" id="input-file-now" class="dropify" />
                <br/>
                <div style="text-align: center">
                    <button class="btn-lg btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">PELANGGAN KRITIS (SUGAR)</h3>
            <div class="table-responsive">
                <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SQM</th>
                            <th>INET</th>
                            <th>NCLI</th>
                            <th>UMUR</th>
                            <th>ONU_Link_Status</th>
                            <th>IP_Embassy</th>
                            <th>Type</th>
                            <th>Calling_Station_Id</th>
                            <th>IP_NE</th>
                            <th>ADSL_Link_Status</th>
                            <th>Upstream_Line_Rate</th>
                            <th>Upstream_SNR</th>
                            <th>Upstream_Attenuation</th>
                            <th>Upstream_Attainable_Rate</th>
                            <th>Downstream_Line_Rate</th>
                            <th>Downstream_SNR</th>
                            <th>Downstream_Attenuation</th>
                            <th>Downstream_Attainable_Rate</th>
                            <th>ONU_Serial_Number</th>
                            <th>Fiber_Length</th>
                            <th>OLT_Tx</th>
                            <th>OLT_Rx</th>
                            <th>ONU_Tx</th>
                            <th>ONU_Rx</th>
                            <th>ONU_Type</th>
                            <th>ONU_VersionID</th>
                            <th>Traffic_Profile_UP</th>
                            <th>Traffic_Profile_DOWN</th>
                            <th>Framed_IP_Address</th>
                            <th>MAC_Address</th>
                            <th>Last_Seen</th>
                            <th>AcctStartTime</th>
                            <th>AccStopTime</th>
                            <th>AccSesionTime</th>
                            <th>Up</th>
                            <th>Down</th>
                            <th>Nas_IP_Address</th>
                            <th>Last_Syncron</th>
                            <th>Last_Updated_By</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="white-box">
            <h3 class="box-title m-b-0">PELANGGAN NORMAL (Q)</h3>
            <div class="table-responsive">
                <table id="table_kritis" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SQM</th>
                            <th>INET</th>
                            <th>NCLI</th>
                            <th>UMUR</th>
                            <th>ONU_Link_Status</th>
                            <th>IP_Embassy</th>
                            <th>Type</th>
                            <th>Calling_Station_Id</th>
                            <th>IP_NE</th>
                            <th>ADSL_Link_Status</th>
                            <th>Upstream_Line_Rate</th>
                            <th>Upstream_SNR</th>
                            <th>Upstream_Attenuation</th>
                            <th>Upstream_Attainable_Rate</th>
                            <th>Downstream_Line_Rate</th>
                            <th>Downstream_SNR</th>
                            <th>Downstream_Attenuation</th>
                            <th>Downstream_Attainable_Rate</th>
                            <th>ONU_Serial_Number</th>
                            <th>Fiber_Length</th>
                            <th>OLT_Tx</th>
                            <th>OLT_Rx</th>
                            <th>ONU_Tx</th>
                            <th>ONU_Rx</th>
                            <th>ONU_Type</th>
                            <th>ONU_VersionID</th>
                            <th>Traffic_Profile_UP</th>
                            <th>Traffic_Profile_DOWN</th>
                            <th>Framed_IP_Address</th>
                            <th>MAC_Address</th>
                            <th>Last_Seen</th>
                            <th>AcctStartTime</th>
                            <th>AccStopTime</th>
                            <th>AccSesionTime</th>
                            <th>Up</th>
                            <th>Down</th>
                            <th>Nas_IP_Address</th>
                            <th>Last_Syncron</th>
                            <th>Last_Updated_By</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($getData as $num => $result)
                      <tr>
                        {{-- <td>{{ ++$num }}</td> --}}
                        <td>
                            @if (!in_array($result->ticketid,[null,'null']))
                            {{ $result->ticketid }}
                            @elseif ($result->NCLI <> "")
                                <a class="createSQM label label-info" data-id="api_t1/{{ $result->no_inet ?? $result->ND }}/{{ $result->NCLI }}" data-format="{{ $result->NCLI }}_{{ $result->no_inet ?? $result->ND }}_INTERNET">CREATE SQM</a>
                            @else
                                <a class="label label-danger">CREATE SQM</a>
                            @endif
                        </td>
                        <td>{{ $result->no_inet ?? $result->ND }}</td>
                        <td>{{ $result->NCLI }}</td>
                        <td>{{ $result->umur }}</td>
                        <td>{{ $result->ONU_Link_Status }}</td>
                        <td>{{ $result->IP_Embassy }}</td>
                        <td>{{ $result->Type }}</td>
                        <td>{{ $result->Calling_Station_Id }}</td>
                        <td>{{ $result->IP_NE }}</td>
                        <td>{{ $result->ADSL_Link_Status }}</td>
                        <td>{{ $result->Upstream_Line_Rate }}</td>
                        <td>{{ $result->Upstream_SNR }}</td>
                        <td>{{ $result->Upstream_Attenuation }}</td>
                        <td>{{ $result->Upstream_Attainable_Rate }}</td>
                        <td>{{ $result->Downstream_Line_Rate }}</td>
                        <td>{{ $result->Downstream_SNR }}</td>
                        <td>{{ $result->Downstream_Attenuation }}</td>
                        <td>{{ $result->Downstream_Attainable_Rate }}</td>
                        <td>{{ $result->ONU_Serial_Number }}</td>
                        <td>{{ $result->Fiber_Length }}</td>
                        <td>{{ $result->OLT_Tx }}</td>
                        <td>{{ $result->OLT_Rx }}</td>
                        <td>{{ $result->ONU_Tx }}</td>
                        <td>{{ $result->ONU_Rx }}</td>
                        <td>{{ $result->ONU_Type }}</td>
                        <td>{{ $result->ONU_VersionID }}</td>
                        <td>{{ $result->Traffic_Profile_UP }}</td>
                        <td>{{ $result->Traffic_Profile_DOWN }}</td>
                        <td>{{ $result->Framed_IP_Address }}</td>
                        <td>{{ $result->MAC_Address }}</td>
                        <td>{{ $result->Last_Seen }}</td>
                        <td>{{ $result->AcctStartTime }}</td>
                        <td>{{ $result->AccStopTime }}</td>
                        <td>{{ $result->AccSesionTime }}</td>
                        <td>{{ $result->Up }}</td>
                        <td>{{ $result->Down }}</td>
                        <td>{{ $result->Nas_IP_Address }}</td>
                        <td>{{ $result->date_created }}</td>
                        <td>{{ $result->upload_by }}</td>
                      </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @elseif ($witel == "BALIKPAPAN")
    <div class="col-sm-12">
        <div class="white-box">
            <form method="GET" class="form-horizontal">
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label class="control-label col-md-1" style="text-align: left; color: #2b2b2b;">WITEL :</label>
                            <div class="col-md-11">
                                <select class="form-control" data-placeholder="- Pilih Witel -" tabindex="1" name="witel">
                                    @if ($witel == "KALSEL")
                                        <option value="KALSEL">KALSEL</option>
                                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                                    @elseif ($witel == "BALIKPAPAN")
                                        <option value="BALIKPAPAN">BALIKPAPAN</option>
                                        <option value="KALSEL">KALSEL</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label"></label>
                            <input type="submit" value="FILTER" class="btn btn-info">
                        </div>
                    </div>
                </div>
            </form>
            <br/>
            <h3 class="box-title">GRAFIK WITEL {{ $witel }} PERIODE {{ date('Y-m') }}</h3>
            <ul class="list-inline text-right">
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #E74C3C;"></i>Q &nbsp;</h5>
                </li>
                <li>
                    <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>SUGAR &nbsp;</h5>
                </li>
            </ul>
            <div id="morris-area-chart"></div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <form id="submit-form" method="post" action="/assurance/monetIboosterSave/BALIKPAPANS" enctype="multipart/form-data" autocomplete="off">
                <h3 class="box-title">File Upload</h3>
                <label for="input-file-now">Import File (.xlsx) Daftar Pelanggan Sugar *Format Disesuaikan*</label>
                <input type="file" name="upload" id="input-file-now" class="dropify" />
                <br/>
                <div style="text-align: center">
                    <button class="btn-lg btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">PELANGGAN TERPANTAU LOSS ( SUGAR )</h3>
            <div class="table-responsive">
                <table id="table_s_bpp" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SQM</th>
                            <th>STO</th>
                            <th>INET</th>
                            <th>NCLI</th>
                            <th>UMUR</th>
                            <th>ONU_Link_Status</th>
                            <th>IP_Embassy</th>
                            <th>Type</th>
                            <th>Calling_Station_Id</th>
                            <th>IP_NE</th>
                            <th>ADSL_Link_Status</th>
                            <th>Upstream_Line_Rate</th>
                            <th>Upstream_SNR</th>
                            <th>Upstream_Attenuation</th>
                            <th>Upstream_Attainable_Rate</th>
                            <th>Downstream_Line_Rate</th>
                            <th>Downstream_SNR</th>
                            <th>Downstream_Attenuation</th>
                            <th>Downstream_Attainable_Rate</th>
                            <th>ONU_Serial_Number</th>
                            <th>Fiber_Length</th>
                            <th>OLT_Tx</th>
                            <th>OLT_Rx</th>
                            <th>ONU_Tx</th>
                            <th>ONU_Rx</th>
                            <th>ONU_Type</th>
                            <th>ONU_VersionID</th>
                            <th>Traffic_Profile_UP</th>
                            <th>Traffic_Profile_DOWN</th>
                            <th>Framed_IP_Address</th>
                            <th>MAC_Address</th>
                            <th>Last_Seen</th>
                            <th>AcctStartTime</th>
                            <th>AccStopTime</th>
                            <th>AccSesionTime</th>
                            <th>Up</th>
                            <th>Down</th>
                            <th>Nas_IP_Address</th>
                            <th>Last_Syncron</th>
                            <th>Last_Updated_By</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="white-box">
            <h3 class="box-title m-b-0">PELANGGAN TERPANTAU LOSS ( Q )</h3>
            <div class="table-responsive">
                <table id="table_q_bpp" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SQM</th>
                            <th>STO</th>
                            <th>INET</th>
                            <th>NCLI</th>
                            <th>UMUR</th>
                            <th>ONU_Link_Status</th>
                            <th>IP_Embassy</th>
                            <th>Type</th>
                            <th>Calling_Station_Id</th>
                            <th>IP_NE</th>
                            <th>ADSL_Link_Status</th>
                            <th>Upstream_Line_Rate</th>
                            <th>Upstream_SNR</th>
                            <th>Upstream_Attenuation</th>
                            <th>Upstream_Attainable_Rate</th>
                            <th>Downstream_Line_Rate</th>
                            <th>Downstream_SNR</th>
                            <th>Downstream_Attenuation</th>
                            <th>Downstream_Attainable_Rate</th>
                            <th>ONU_Serial_Number</th>
                            <th>Fiber_Length</th>
                            <th>OLT_Tx</th>
                            <th>OLT_Rx</th>
                            <th>ONU_Tx</th>
                            <th>ONU_Rx</th>
                            <th>ONU_Type</th>
                            <th>ONU_VersionID</th>
                            <th>Traffic_Profile_UP</th>
                            <th>Traffic_Profile_DOWN</th>
                            <th>Framed_IP_Address</th>
                            <th>MAC_Address</th>
                            <th>Last_Seen</th>
                            <th>AcctStartTime</th>
                            <th>AccStopTime</th>
                            <th>AccSesionTime</th>
                            <th>Up</th>
                            <th>Down</th>
                            <th>Nas_IP_Address</th>
                            <th>Last_Syncron</th>
                            <th>Last_Updated_By</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($getData as $num => $result)
                      <tr>
                        {{-- <td>{{ ++$num }}</td> --}}
                        <td>
                            @if (!in_array($result->ticketid,[null,'null']))
                            {{ $result->ticketid }}
                            @elseif ($result->NCLI <> "")
                                <a class="createSQM label label-info" data-id="api_bppq/{{ $result->no_inet ?? $result->ND }}/{{ $result->NCLI }}" data-format="{{ $result->NCLI }}_{{ $result->no_inet ?? $result->ND }}_INTERNET">CREATE SQM</a>
                            @else
                                <a class="label label-danger">CREATE SQM</a>
                            @endif
                        </td>
                        <td>{{ $result->STO }}</td>
                        <td>{{ $result->no_inet ?? $result->ND }}</td>
                        <td>{{ $result->NCLI }}</td>
                        <td>{{ $result->umur }}</td>
                        <td>{{ $result->ONU_Link_Status }}</td>
                        <td>{{ $result->IP_Embassy }}</td>
                        <td>{{ $result->Type }}</td>
                        <td>{{ $result->Calling_Station_Id }}</td>
                        <td>{{ $result->IP_NE }}</td>
                        <td>{{ $result->ADSL_Link_Status }}</td>
                        <td>{{ $result->Upstream_Line_Rate }}</td>
                        <td>{{ $result->Upstream_SNR }}</td>
                        <td>{{ $result->Upstream_Attenuation }}</td>
                        <td>{{ $result->Upstream_Attainable_Rate }}</td>
                        <td>{{ $result->Downstream_Line_Rate }}</td>
                        <td>{{ $result->Downstream_SNR }}</td>
                        <td>{{ $result->Downstream_Attenuation }}</td>
                        <td>{{ $result->Downstream_Attainable_Rate }}</td>
                        <td>{{ $result->ONU_Serial_Number }}</td>
                        <td>{{ $result->Fiber_Length }}</td>
                        <td>{{ $result->OLT_Tx }}</td>
                        <td>{{ $result->OLT_Rx }}</td>
                        <td>{{ $result->ONU_Tx }}</td>
                        <td>{{ $result->ONU_Rx }}</td>
                        <td>{{ $result->ONU_Type }}</td>
                        <td>{{ $result->ONU_VersionID }}</td>
                        <td>{{ $result->Traffic_Profile_UP }}</td>
                        <td>{{ $result->Traffic_Profile_DOWN }}</td>
                        <td>{{ $result->Framed_IP_Address }}</td>
                        <td>{{ $result->MAC_Address }}</td>
                        <td>{{ $result->Last_Seen }}</td>
                        <td>{{ $result->AcctStartTime }}</td>
                        <td>{{ $result->AccStopTime }}</td>
                        <td>{{ $result->AccSesionTime }}</td>
                        <td>{{ $result->Up }}</td>
                        <td>{{ $result->Down }}</td>
                        <td>{{ $result->Nas_IP_Address }}</td>
                        <td>{{ $result->date_created }}</td>
                        <td>{{ $result->upload_by }}</td>
                      </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.13.0/dist/sweetalert2.all.min.js"></script>
@if ($witel == "KALSEL")
<script>
    $(function() {

        // Morris.Area({
        //     element: 'morris-area-chart',
        //     data: <?= json_encode($data) ?>,
        //     xkey: 'period',
        //     ykeys: ['Q','Sugar'],
        //     labels: ['Q ','SUGAR '],
        //     xLabelFormat: function (x) { return x.getUTCDate()+1; },
        //     pointSize: 4,
        //     fillOpacity: 0,
        //     pointStrokeColors:['#E74C3C', '#fdc006'],
        //     behaveLikeLine: true,
        //     gridLineColo8r: '#e0e0e0',
        //     lineWidth: 2,
        //     hideHover: 'auto',
        //     lineColors: ['#E74C3C', '#fdc006'],
        //     resize: true
        // });

        $('.dropify').dropify();

        $('#table_data thead tr').clone(true) .addClass('filters').appendTo('#table_data thead');

        var table = $('#table_data').DataTable({
            select: true,
            bProcessing: true,
            ajax: "/monetIbooster/data_ajx/KALSEL",
            columns: [
                {
                    sortable: false,
                    "render": function ( d, type, val, meta ) {
                        if( (val[0] !== null ) ){
                            var btn = val[0];
                        }else if( (val[2] !== null ) ){
                            id = "api_t2/"+  val[1] + '/'+ val[2],
                            format = val[2] + '_' + val[1] + '_INTERNET';

                            var btn = "<a class='SQMcrt label label-info' data-id='"+ id +"' data-format='"+ format +"'>CREATE SQM</a>";
                        }else{
                            var btn = '<a class="label label-danger">CREATE SQM</a>';
                        }
                        return btn;
                    }
                },
                { data: 1 },
                { data: 2 },
                { data: 3 },
                { data: 4 },
                { data: 5 },
                { data: 6 },
                { data: 7 },
                { data: 8 },
                { data: 9 },
                { data: 10 },
                { data: 11 },
                { data: 12 },
                { data: 13 },
                { data: 14 },
                { data: 15 },
                { data: 16 },
                { data: 17 },
                { data: 18 },
                { data: 19 },
                { data: 20 },
                { data: 21 },
                { data: 22 },
                { data: 23 },
                { data: 24 },
                { data: 25 },
                { data: 26 },
                { data: 27 },
                { data: 28 },
                { data: 29 },
                { data: 30 },
                { data: 31 },
                { data: 32 },
                { data: 33 },
                { data: 34 },
                { data: 35 },
                { data: 36 },
                { data: 37 },
                { data: 38 },
            ],	
            dom: 'Blfrtip',
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [{
                extend: 'excel',
                text: 'Export to Excel',
                title: 'ASSURANCE MONET IBOOSTER (SUGAR) PELANGGAN KRITIS TOMMAN'
            }],
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();
                api.columns().eq(0).each(function (colIdx) {
                    var cell = $('.filters th').eq( $(api.column(colIdx).header()).index() );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
                    $('input', $('.filters th').eq($(api.column(colIdx).header()).index() ) ).off('keyup change').on('keyup change', function (e) {
                        e.stopPropagation();
                        $(this).attr('title', $(this).val());
                        var regexr = '({search})';

                        var cursorPosition = this.selectionStart;

                        api.column(colIdx).search(
                            this.value != '' ? regexr.replace('{search}', '(((' + this.value + ')))') : '',
                            this.value != '',
                            this.value == ''
                        ).draw();

                        $(this).focus()[0].setSelectionRange(cursorPosition, cursorPosition);
                    });
                });
            },
        }).columns.adjust().draw();

        $('#table_data').on('click', '.SQMcrt', function() {
            var data_id = $(this).attr('data-id'),
            data_format = $(this).attr('data-format');
            console.log(data_id, data_format)
            Swal.fire({
              title: 'Mau buat tiket SQM ?',
              text: data_format,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "<a href='/ticketing/createSQM/" + data_id + "' style='color: white;'>Yes, create it!</a>"
            });
        });

        // table kritis
        $('#table_kritis').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [{
                extend: 'excel',
                text: 'Export to Excel',
                title: 'ASSURANCE MONET IBOOSTER (Q) PELANGGAN NORMAL TOMMAN'
            }]
        });

        $('#table_kritis').on('click', '.createSQM', function() {
            var data_id = $(this).attr('data-id'),
            data_format = $(this).attr('data-format');
            console.log(data_id, data_format)
            Swal.fire({
              title: 'Mau buat tiket SQM ?',
              text: data_format,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "<a href='/ticketing/createSQM/" + data_id + "' style='color: white;'>Yes, create it!</a>"
            });
        });
    });
</script>
@elseif ($witel == "BALIKPAPAN")
<script>
    $(function() {

        // Morris.Area({
        //     element: 'morris-area-chart',
        //     data: <?= json_encode($data) ?>,
        //     xkey: 'period',
        //     ykeys: ['Q','Sugar'],
        //     labels: ['Q (LOS) ','SUGAR (LOS) '],
        //     xLabelFormat: function (x) { return x.getUTCDate()+1; },
        //     pointSize: 4,
        //     fillOpacity: 0,
        //     pointStrokeColors:['#E74C3C', '#fdc006'],
        //     behaveLikeLine: true,
        //     gridLineColo8r: '#e0e0e0',
        //     lineWidth: 2,
        //     hideHover: 'auto',
        //     lineColors: ['#E74C3C', '#fdc006'],
        //     resize: true
        // });

        $('.dropify').dropify();

        // TABLE SUGAR
        $('#table_s_bpp thead tr').clone(true) .addClass('filters').appendTo('#table_s_bpp thead');

        var table = $('#table_s_bpp').DataTable({
            select: true,
            bProcessing: true,
            ajax: "/monetIbooster/data_ajx/BALIKPAPAN",
            columns: [
                {
                    sortable: false,
                    "render": function ( d, type, val, meta ) {
                        if( (val[0] !== null ) ){
                            var btn = val[0];
                        }else if( (val[3] !== null ) ){
                            id = "api_bpps/"+  val[2] + '/'+ val[3],
                            format = val[3] + '_' + val[2] + '_INTERNET';

                            var btn = "<a class='SQMcrt label label-info' data-id='"+ id +"' data-format='"+ format +"'>CREATE SQM</a>";
                        }else{
                            var btn = '<a class="label label-danger">CREATE SQM</a>';
                        }
                        return btn;
                    }
                },
                { data: 1 },
                { data: 2 },
                { data: 3 },
                { data: 4 },
                { data: 5 },
                { data: 6 },
                { data: 7 },
                { data: 8 },
                { data: 9 },
                { data: 10 },
                { data: 11 },
                { data: 12 },
                { data: 13 },
                { data: 14 },
                { data: 15 },
                { data: 16 },
                { data: 17 },
                { data: 18 },
                { data: 19 },
                { data: 20 },
                { data: 21 },
                { data: 22 },
                { data: 23 },
                { data: 24 },
                { data: 25 },
                { data: 26 },
                { data: 27 },
                { data: 28 },
                { data: 29 },
                { data: 30 },
                { data: 31 },
                { data: 32 },
                { data: 33 },
                { data: 34 },
                { data: 35 },
                { data: 36 },
                { data: 37 },
                { data: 38 },
                { data: 39 },
            ],	
            dom: 'Blfrtip',
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [{
                extend: 'excel',
                text: 'Export to Excel',
                title: 'HASIL PENGUKURAN PELANGGAN LOS (SUGAR) WITEL BALIKPAPAN'
            }],
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();
                api.columns().eq(0).each(function (colIdx) {
                    var cell = $('.filters th').eq( $(api.column(colIdx).header()).index() );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
                    $('input', $('.filters th').eq($(api.column(colIdx).header()).index() ) ).off('keyup change').on('keyup change', function (e) {
                        e.stopPropagation();
                        $(this).attr('title', $(this).val());
                        var regexr = '({search})';

                        var cursorPosition = this.selectionStart;

                        api.column(colIdx).search(
                            this.value != '' ? regexr.replace('{search}', '(((' + this.value + ')))') : '',
                            this.value != '',
                            this.value == ''
                        ).draw();

                        $(this).focus()[0].setSelectionRange(cursorPosition, cursorPosition);
                    });
                });
            },
        }).columns.adjust().draw();

        $('#table_s_bpp').on('click', '.SQMcrt', function() {
            var data_id = $(this).attr('data-id'),
            data_format = $(this).attr('data-format');
            console.log(data_id, data_format)
            Swal.fire({
              title: 'Mau buat tiket SQM ?',
              text: data_format,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "<a href='/ticketing/createSQM/" + data_id + "' style='color: white;'>Yes, create it!</a>"
            });
        });

        //TABLE Q
        $('#table_q_bpp').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [{
                extend: 'excel',
                text: 'Export to Excel',
                title: 'HASIL PENGUKURAN PELANGGAN LOS (Q) WITEL BALIKPAPAN'
            }]
        });

        $('#table_q_bpp').on('click', '.createSQM', function() {
            var data_id = $(this).attr('data-id'),
            data_format = $(this).attr('data-format');
            console.log(data_id, data_format)
            Swal.fire({
              title: 'Mau buat tiket SQM ?',
              text: data_format,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: "<a href='/ticketing/createSQM/" + data_id + "' style='color: white;'>Yes, create it!</a>"
            });
        });

    });
</script>
@endif
@endsection