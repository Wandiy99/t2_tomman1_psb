@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">LIST {{ $title }} {{ $sektor }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>INCIDENT</th>
                        <th>CUSTOMER</th>
                        <th>PHONE NUMBER</th>
                        <th>SUMMARY</th>
                        <th>SERVICE NO</th>
                        <th>DATEK</th>
                        <th>REPORTED DATE</th>
                        <th>TGL DISPATCH</th>
                        <th>STATUS</th>
                        <th>REGU</th>
                        <th>SEKTOR</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $num => $result)
                    @if($result)                    
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $result->Incident }}</td>
                        <td>{{ $result->Customer_Name }}</td>
                        <td>{{ $result->Contact_Phone }}</td>
                        <td>{{ $result->Summary }}</td>
                        <td>{{ $result->Service_No }}</td>
                        <td>{{ $result->Datek }}</td>
                        <td>{{ $result->Reported_Date }}</td>
                        <td>{{ $result->tgl }}</td>
                        <td>{{ $result->laporan_status ? : 'ANTRIAN' }}</td>
                        <td>{{ $result->tim }}</td>
                        <td>{{ $result->sektor }}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>NO</th>
                    <th>INCIDENT</th>
                    <th>CUSTOMER</th>
                    <th>PHONE NUMBER</th>
                    <th>SUMMARY</th>
                    <th>SERVICE NO</th>
                    <th>DATEK</th>
                    <th>REPORTED DATE</th>
                    <th>TGL DISPATCH</th>
                    <th>STATUS</th>
                    <th>REGU</th>
                    <th>SEKTOR</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL REPORT CUSTOMER BY STO INT TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL REPORT CUSTOMER BY STO INT TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL REPORT CUSTOMER BY STO INT TOMMAN'
                }
            ]
        });
    });
</script>
@endsection