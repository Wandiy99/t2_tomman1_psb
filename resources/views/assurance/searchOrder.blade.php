@extends('layout')

@section('content')
@include('partial.alerts')
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 10px 6px;
    }
    th{
        text-align: center;
        color: black;
        width: 1px;
        white-space: nowrap;
    }
    td{
        color: black;
    }
</style>


<div class="col-sm-12">
    <form class="form-group" method="post" >
        <div class="row">
            <div class="col-md-10">
                <div class="form-group {{ $errors->has('Search') ? 'has-error' : '' }}">
                    <label for="Search">Search</label>
                    <input type="text" name="search" class="form-control" id="Search" value="">
                    {!! $errors->first('Search', '<p class=help-block>:message</p>') !!}
                </div>
            </div>
            <div class="col-md-2">
                <label for="btn">&nbsp;</label>
                <button class="form-control btn btn-primary btn-rounded">Search</button>
            </div>
        </div>
    </form>

    <a href="/assurance/dispatch_manual" type="button" class="btn btn-success btn-sm btn-rounded"><i class="ti-plus" class="linea-icon linea-basic fa-fw"></i>&nbsp; Dispatch Manual</a>
</div>


@if ($data <> NULL)
<br />
<div class="panel-body" style="background-color: white !important">
<div class="table-responsive">
    {{ $data }}. <br />
    Result ({{ count($query) }}) Found.
    <br />
    <br />
    @if (count($query) > 0)
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>#</th>
            <th style="width: 40%">NOSSA</th>
            <th style="width: 40%">Informasi Order</th>
            <th>Modals</th>
        </tr>
      </thead>
      <tbody>
        @foreach($query as $no => $data)
        {{-- @if($data->Incident <> NULL) --}}
        <tr>
            <td style="text-align: center; vertical-align: middle;">{{ ++$no }}</td>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <td colspan="3" class="text-center">
                                @if ($data->id_dt<>'')
                                <a href="/tiket/{{ $data->id_dt }}" target="_blank"><b>{{ $data->Incident ? : $data->tiket_no ? : 'Not Found Incident!' }}</b></a>
                                @else
                                <b>{{ $data->Incident ? : $data->tiket_no ? : 'Not Found Incident!' }}</b>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Incident</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Incident ? : $data->tiket_no ? : '-' }} / {{ $data->ID ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Customer Segment / Customer Type / Status / Source</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Customer_Segment ? : '-' }} / {{ $data->Customer_Type ? : '-' }} / {{ $data->Status ? : '-' }} / {{ $data->Source ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Contact Name</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Contact_Name ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Service No / Service Type</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Service_No ? : $data->r_no_speedy ? : '-' }} / {{ $data->Service_Type ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Service ID</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Service_ID ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Summary</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Summary ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Headline</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->r_headline ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Incident Symptom</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Incident_Symptom ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Datek</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Datek ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Reported Date</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->Reported_Datex ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Status Date</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ date('Y-m-d H:i:s' ,strtotime($data->Status_Date)) ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Last Update Ticket</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ date('Y-m-d H:i:s' ,strtotime($data->Last_Update_Ticket)) ? : '-' }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Status / Action / Penyebab</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;"><label class="label label-success">{{ $data->status ? : 'ANTRIAN' }}</label> &nbsp; <label class="label label-info">{{ $data->pla_action ? : '-' }}</label> &nbsp; <label class="label label-warning">{{ $data->plp_penyebab ? : '-' }}</label></td>
                        </tr>
                        <tr>
                            <td>Tanggal Laporan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->tgl_laporan ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Redaman / ONU Rx</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">
                                @if($data->ONU_Rx == "-" || $data->ONU_Rx < -23)
                                    <label class="label label-danger">UNSPEC</label>
                                @else
                                    <label class="label label-success">SPEC</label>
                                @endif
                                / {{ $data->ONU_Rx ? : '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Redaman / ONU Rx After</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">
                                @if($data->ONU_Rx_Before_After == "-" || $data->ONU_Rx_Before_After < -23)
                                    <label class="label label-danger">UNSPEC</label>
                                @else
                                    <label class="label label-success">SPEC</label>
                                @endif
                                / {{ $data->ONU_Rx_Before_After ? : '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Tim</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->uraian ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Mitra</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->mitra_amija_pt ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Sektor</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->sektor ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Catatan Teknisi</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->catatan_tek ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Kontak Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->noPelangganAktif ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat Pelanggan</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kordinat_pelanggan ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>ODP</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->nama_odp ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Koordinat ODP</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->kordinat_odp ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Valins ID</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->valins_id ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Dropcore Labelcode</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->dropcore_label_code ? : '-' }}</td>
                        </tr>
                        <tr>
                            <td>ODP Labelcode</td>
                            <td style="text-align: center; vertical-align: middle;">:</td>
                            <td style="text-align: right; vertical-align: middle;">{{ $data->odp_label_code ? : '-' }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <!-- DISPATCH ORDER -->
                <div id="responsive-modal-dispatch-{{ $data->id_dt }}" class="modal fade" tabindex="-1" role="document" aria-labelledby="myModalLabel-dispatch-{{ $data->id_dt }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>DISPATCH {{ $data->Incident ? : $data->tiket_no }}</b></h4>
                            </div>
                            <div class="modal-body">
                                @if(!empty($data->id_r))
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">Tim</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->uraian ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Dispatch</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->tgl_dt ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Tanggal Order</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->tgl_order_hd ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Dispatch Oleh</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->nama_hd ? : '-' }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Jenis Layanan</td>
                                                <td class="text-center">:</td>
                                                <td style="text-align: right;">{{ $data->b_jenis_order ? : '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br /><br />
                                <a href="/assurance/dispatch/{{ $data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}" class="btn btn-rounded btn-block btn-info">Re-Dispatch</a>
                                @else
                                    @if($data->Incident <> NULL)
                                        <a href="/assurance/dispatch/{{ $data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}" class="btn btn-rounded btn-block btn-info">Dispatch</a>
                                    @endif
                                @endif
                                
                                <br/><br/>

                                <div class="row">

                                @if ($data->dispatch_by == '4')
                                <div class="col-md-6">
                                    <a href="/fulfillment/hapusfg/{{ $data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}" class="btn btn-sm btn-rounded btn-block btn-danger" data-toggle="tooltip">Hapus WO FG</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="/fulfillment/redispatch/{{ $data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}" class="btn btn-sm btn-rounded btn-block btn-info" data-toggle="tooltip" title="{{ $data->uraian }}">Re-Dispatch</a>
                                </div>
                                @endif

                                @if(empty($data->uraian))
                                    @if(session('auth')->level == 2 || session('auth')->level == 15)
                                        <div class="col-md-6">
                                            <a href="/assurance/edit/{{ $data->ID }}" class="btn btn-sm btn-rounded btn-block btn-danger">E D I T</a>
                                        </div>
                                    @endif
                                @endif

                                @if(in_array(session('auth')->id_karyawan, ["91153709", "PL46071", "PL46082", "PL64201", "PL46080", "93150112", "20981020"]))
                                    <div class="col-md-6">
                                        <a href="/lapul/{{ $data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}" class="btn btn-sm btn-rounded btn-block btn-warning">LAPUL</a>
                                    </div>
                                @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="fcbtn btn-sm btn-rounded btn-outline btn-info btn-1f btn-block" data-toggle="modal" data-target="#responsive-modal-dispatch-{{ $data->id_dt }}"><b><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Dispatch Order</b>
                </button>

                @if (in_array(session('auth')->level, [2, 15, 46]))
                    @if($data->id_dt <> "")
                        <a class="btn btn-sm btn-rounded btn-block btn-danger" data-toggle="popover" data-placement="bottom" title="Yakin Dihapus ?" data-html="true" data-content="<a href='/assurance/delete/{{ @$data->id_dt }}/{{ @$data->ID ? : 'X' }}/{{ @$data->Ndem ? : $data->no_tiket ? : $data->tiket_no }}'><center>Yes</center></a>"><i class="ti-arrow-circle-right pull-left" class="linea-icon linea-basic fa-fw"></i>&nbsp; Delete</a>
                    @endif
                @endif
            </td>
        </tr>
        {{-- @endif --}}
        @endforeach
      </tbody>
    </table>
    </div>
    @else
    <a href="/nossaIn/{{ $id }}" class="btn btn-sm btn-warning btn-rounded">Mau Cari di NOSSA ?</a>
    @endif
</div>
</div>
@endif
</div>
@endsection