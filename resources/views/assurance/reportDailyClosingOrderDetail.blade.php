@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">REPORT DAILY CLOSING ORDER H-1 SEKTOR {{ $sektor }} STATUS {{ $status }}</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>INCIDENT</th>
                        <th>SERVICE NO</th>
                        <th>ONU RX</th>
                        <th>STATUS TEK</th>
                        <th>ACTION TEK</th>
                        <th>PENYEBAB TEK</th>
                        <th>TGL DISPATCH</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $num => $data)
					<tr>
						<td>{{ ++$num }}</td>
						<td>{{ $data->sektor ? : 'NON SEKTOR' }}</td>
						<td>{{ $data->tim }}</td>
                        <td>{{ $data->Incident }}</td>
                        <td>{{ $data->Service_No }}</td>
                        <td>{{ $data->ONU_Rx }}</td>
                        <td>{{ $data->status_tek ? : 'ANTRIAN' }}</td>
                        <td>{{ $data->action_tek }}</td>
						<td>{{ $data->penyebab_tek }}</td>
						<td>{{ $data->tgl_dispatch }}</td>
					</tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL REPORT DAILY CLOSING ORDER H-1 ASSURANCE'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL REPORT DAILY CLOSING ORDER H-1 ASSURANCE'
                },
                {
                    extend: 'print',
                    title: 'DETAIL REPORT DAILY CLOSING ORDER H-1 ASSURANCE'
                }
            ]
        });
    });
</script>
@endsection