@extends('public_layout')

<?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=ASSURANCE ALL ".$date.".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
?>
<!DOCTYPE html>
<html>
<body>
<table table border="1" width="100%">
    <tr align="center">
    <th>NO</th>
    <th>IOAN</th>
    <th>SEKTOR</th>
    <th>MITRA</th>
    <th>TL</th>
    <th>TIM</th>
    <th>INCIDENT</th>
    <th>SERVICE_NO</th>
    <th>CUSTOMER_NAME</th>
    <th>ALAMAT</th>
    <th>KOORDINAT_PELANGGAN</th>
    <th>SERVICE_ID</th>
    <th>ORDER_NCLI</th>
    <th>SEGMENT_STATUS</th>
    <th>STATUS_NOSSA</th>
    <th>SUMMARY</th>
    <th>DATEK</th>
    <th>ODP</th>
    <th>STO</th>
    <th>REPORTED_DATE</th>
    <th>STATUS_DATE_NOSSA</th>
    <th>BOOKING_DATE</th>
    <th>DISPATCH_DATE</th>
    <th>CLOSE_DATE</th>
    <th>STATUS</th>
    <th>ACTION</th>
    <th>PENYEBAB</th>
    <th>PENYEBAB_RETI</th>
    <th>KONDISI_ODP</th>
    <th>JENIS_SPLITTER</th>
    <th>CATATAN</th>
    <th>TYPE ONT</th>
    <th>SN ONT</th>
    <th>TYPE STB</th>
    <th>SN STB</th>
    <th>OPEN_BY_USER</th>
    <th>CLOSE_BY_USER</th>
    <th>REDAMAN_SEBELUM</th>
    <th>REDAMAN_SESUDAH</th>
    </tr>
    @foreach($query as $no => $list)
    <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $list->ioan ? : '-' }}</td>
        <td>{{ $list->title  ? : '-' }}</td>
        <td>{{ $list->mitra ? : '-' }}</td>
        <td>{{ $list->TL  ? : '-' }}</td>
        <td>{{ $list->uraian  ? : '-' }}</td>
        <td>{{ $list->Incident ? : $list->dt_ndem }}</td>
        <td>{{ $list->Service_No ? : $list->no_internet ? : '-' }}</td>
        <td>{{ $list->Customer_Name ? : '-' }}</td>
        <td>{{ $list->streetAddress ? : $list->alamat ? : '-' }}</td>
        <td>{{ $list->kordinat_pelanggan ? : '-' }}</td>
        <td>{{ $list->Service_ID ? : '-' }}</td>
        <td>{{ $list->ncliOrder ? : '-' }}</td>
        <td>{{ $list->Customer_Segment ? : $list->Segment_Status ? : '-' }}</td>
        <td>{{ $list->aaIncident ? : 'CLOSED' }}</td>
        <td>{{ $list->Summary ? : $list->rc_headline ? : '-' }}</td>
        <td>{{ $list->Datek ? : '-' }}</td>
        <td>{{ $list->nama_odp ? : '-' }}</td>
        <td>{{ $list->Workzone ? : '-' }}</td>
        @if($list->Source <> "TOMMAN")
            <td>{{ $list->Reported_Datex ? : '-' }}</td>
        @else
            <td>{{ $list->Reported_Date ? : '-' }}</td>
        @endif
        <td>{{ $list->Status_Date ? : '-' }}</td>
        <td>{{ $list->Booking_Date ? : '-' }}</td>
        <td>{{ $list->tgl ? : '-' }}</td>
        <td>{{ $list->modified_at ? : '-' }}</td>
        <td>{{ $list->laporan_status ? : '-' }}</td>
        <td>{{ $list->action ? : '-' }}</td>
        <td>{{ $list->penyebab ? : '-' }}</td>
        <td>{{ $list->status_penyebab_reti ? : '-' }}</td>
        <td>{{ $list->kondisi_odp ? : '-' }}</td>
        <td>{{ $list->splitter ? : '-' }}</td>
        <td>{{ $list->catatan ? : '-' }}</td>
        <td>{{ $list->typeont ? : '-' }}</td>
        <td>{{ $list->snont ? : '-' }}</td>
        <td>{{ $list->typestb ? : '-' }}</td>
        <td>{{ $list->snstb ? : '-' }}</td>
        <td>{{ $list->user_created ? : '-' }}</td>
        <td>{{ $list->modified_by ? : '-' }}</td>
        <td>{{ $list->ONU_Rx ? : '-'}}</td>
        <td>{{ $list->redaman_iboster ? : '-' }}</td>
    </tr>
    @endforeach
</table>
</body>
</html>