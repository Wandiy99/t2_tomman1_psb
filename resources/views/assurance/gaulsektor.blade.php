@extends('layout')

@section('content')
  @include('partial.alerts')
  <h3>GAUL SEKTOR</h3>
  <div class="row">
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Table
      </div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <th>Rank</th>
            <th>Sektor</th>
            <th>Gaul</th>
            <th>Jumlah</th>
            <th>Gaul (%)</th>
          </tr>
          @foreach ($get_gaulsektor as $num => $gaulsektor)
          <?php 
            $gaulach = $gaulsektor->gaulv2/$gaulsektor->JML*100;
          ?>
          <tr>
            <td>{{ ++$num }}</td>
            <td>{{  $gaulsektor->title }}</td>
            <td><a href="/gaulsektorlist/{{ $gaulsektor->title }}">{{  $gaulsektor->gaulv2 }}</a></td>
            <td><a href="/gangguansektorlist/{{ $gaulsektor->title }}">{{  $gaulsektor->JML }}</a></td>
            <td><a href="/gangguansektorlist/{{ $gaulsektor->title }}">{{  round($gaulach,2) }}</a></td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
  </div>
@endsection
