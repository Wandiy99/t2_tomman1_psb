@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
    <div class="col-sm-12">
      Status Open :
      <table class="table">
        <tr>
          <th>Status Laporan</th>
          <th>Jumlah</th>
        </tr>
        @foreach ($query as $num => $result)
        <tr>
          <td>{{ $result->laporan_status ? : 'NEED PROGRESS' }}</td>
          <td>{{ $result->jumlah }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
@endsection
