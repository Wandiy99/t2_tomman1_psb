@extends('layout')
@section('content')
@include('partial.alerts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style>
    th{
        text-align: center;
    }
    td{
        text-align: center;
    }
    table.dataTable tbody tr{
        background-color: transparent;
    }
</style>
  <div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><center>DASHBOARD PROGRESS TIKET SQM {{ $date }}</center></div>
            <div class="table-responsive">
                <table class="table" id="table_data">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>TIM</th>
                            <th>SEKTOR</th>
                            <th>MITRA</th>
                            <th>ORDER</th>
                            <th>UP</th>
                            <th>SISA</th>
                            <th>OGP</th>
                            <th>KENDALA</th>
                            <th>TARGET</th>
                            <th>KEKURANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $total_order = 0;
                        $total_up = 0;
                        $total_sisa = 0;
                        $total_ogp = 0;
                        $total_kendala = 0;
                        $total_gap = 0;
                        $target = 0;
                    @endphp
                        @foreach ($query as $num => $result)
                            @php
                                $total_order += $result->jml_order;
                                $total_up += $result->jml_up;
                                $total_sisa += $result->jml_sisa;
                                $total_ogp += $result->jml_ogp;
                                $total_kendala += $result->jml_kendala;
                                if (strlen($date)==7 || strlen($date)==8) {
                                    $target = 200;
                                    if ($result->jml_up >= $target) {
                                        $total_gap = ($result->jml_up - $target);
                                    } else {
                                        $total_gap = ($result->jml_up - $target);
                                    }
                                } else {
                                    $target = 8;
                                    if ($result->jml_up >= $target) {
                                        $total_gap = ($result->jml_up - $target);
                                    } else {
                                        $total_gap = ($result->jml_up - $target);
                                    }
                                }
                            @endphp
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>{{ $result->tim ? : '#N/A' }}</td>
                            <td>{{ $result->sektor ? : '#N/A' }}</td>
                            <td>{{ $result->mitra ? : '#N/A' }}</td>
                            <td><a href="/dashboardTekSQMList/{{ $result->id_regu }}/ORDER/{{ $date }}" target="_blank">{{ $result->jml_order }}</a></td>
                            <td><a href="/dashboardTekSQMList/{{ $result->id_regu }}/UP/{{ $date }}" target="_blank">{{ $result->jml_up }}</a></td>
                            <td><a href="/dashboardTekSQMList/{{ $result->id_regu }}/SISA/{{ $date }}" target="_blank">{{ $result->jml_sisa }}</a></td>
                            <td><a href="/dashboardTekSQMList/{{ $result->id_regu }}/OGP/{{ $date }}" target="_blank">{{ $result->jml_ogp }}</a></td>
                            <td><a href="/dashboardTekSQMList/{{ $result->id_regu }}/KENDALA/{{ $date }}" target="_blank">{{ $result->jml_kendala }}</a></td>
                            <td>{{ $target }}</td>
                            <td> @if ($result->jml_up >= $target) +{{ $total_gap }} @else {{ $total_gap }} @endif </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan=4 style="font-weight: bold">TOTAL</td>
                            <td><a href="/dashboardTekSQMList/ALL/ORDER/{{ $date }}" target="_blank">{{ $total_order }}</a></td>
                            <td><a href="/dashboardTekSQMList/ALL/UP/{{ $date }}" target="_blank">{{ $total_up }}</a></td>
                            <td><a href="/dashboardTekSQMList/ALL/SISA/{{ $date }}" target="_blank">{{ $total_sisa }}</a></td>
                            <td><a href="/dashboardTekSQMList/ALL/OGP/{{ $date }}" target="_blank">{{ $total_ogp }}</a></td>
                            <td><a href="/dashboardTekSQMList/ALL/KENDALA/{{ $date }}" target="_blank">{{ $total_kendala }}</a></td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
  </div>
@endsection
@section('plugins')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function() {
        $('#table_data').DataTable();
    });
</script>
@endsection