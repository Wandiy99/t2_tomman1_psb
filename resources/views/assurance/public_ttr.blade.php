@extends('public_layout')

@section('content')
  @include('partial.alerts')
  <h3>MATRIX TTR ASSURANCE <span class="clock"></span></h3>
  <style>
  td,th {
    padding: 4px;
  }
  td {
    vertical-align: top;
  }
  .label-KP {
    background-color: #ff6b6b;
  }
  .label-KT {
    background-color: #0abde3;
  }
  .label-UP {
    background-color: #1dd1a1;
  }
  .label-OGP {
    background-color: #feca57;
  }
  .label- {
    background-color: #8395a7;
  }
  .label-SISA {
    background-color: #8395a7;
  }



  </style>
  <div class="row">
    <div class="col-sm-12">
          <span class="label label-">NEED PROGRESS</span>
          <span class="label label-OGP">ONPROGRESS</span>
          <span class="label label-KT">KENDALA TEKNIK</span>
          <span class="label label-KP">KENDALA PELANGGAN</span>
          <span class="label label-UP">CLOSE</span>
          <table border=1 width="100%">
            <tr>
              <th>Sektor</th>
              <th width=100>05</th>
              <th width=100>06</th>
              <th width=100>07</th>
              <th width=100>08</th>
              <th width=100>09</th>
              <th width=100>10</th>
              <th width=100>11</th>
              <th width=100>12</th>
              <th width=100>13</th>
              <th width=100>14</th>
              <th width=100>15</th>
              <th width=100>16</th>
              <th width=100>17</th>
            </tr>
            @foreach ($get_ttr as $ttr)
            <tr>
              <td>{{ $ttr->title ? : 'NONSEKTOR' }}</td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam5']; 
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam6']; 
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam7']; 
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam8']; 
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam9']; 
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam10'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                      }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam11'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam12'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam13'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam14'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam15'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam16'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
              <td>
                <?php
                  $data = $dataMatrix[$ttr->title]['jam17'];
                  $hitung = count($data);
                  if ($hitung>0){
                    foreach ($data as $result){
                      echo "<span class='label label-".$result->grup."'>".$result->Incident." // ".$result->Status."</span><br />";
                    }
                  }
                ?>
              </td>
            </tr>
            @endforeach
          </table>
      </div>
    </div>
  </div>
  <script type="text/javascript">
        $(function() {
            $("[data-toggle='popover']").popover({html:true});
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
        });
    </script>
@endsection