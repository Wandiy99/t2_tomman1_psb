@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;

    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }​
  </style>
  <h3>

    ASSURANCE

  </h3>

  <ul class="nav nav-tabs">
    <li ><a href="/assurance/search" id="search">Search</a></li>
    <li><a href="/assurance/open" id="1">Tiket Open ({{ count($list) }})</a></li>
    <li><a href="/assurancex/ont_offline" id="1">ONT OFFLINE</a></li>
  <li class="active"><a href="/assurance/close_list/{{date('Y-m')}}">close</a></li>

  </ul>
  <br/>
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>TIKET</th>
        <th>ND</th>
        <th>ACTION</th>
        <th>TGL OPEN</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $no => $data)

      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $data->Ndem }}</td>
        <td>{{ $data->no_speedy }}</td>
        <td>{{ $data->action }}</td>
        <td>{{ $data->tgl_open }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection
