@extends('public_layout')

<?php
    header("content-type:application/vnd-ms-excel");
    header("content-disposition:attachment;filename=ASSURANCE DATA.xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
?>
<!DOCTYPE html>
<html>
<body>
<table table border="1" width="100%">
    <tr align="center">
    <th>NO</th>
    <th>NO_TIKET</th>
    <th>HEADLINE</th>
    <th>NO_SPEEDY</th>
    </tr>
    @foreach($data as $no => $list)
    <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $list->no_tiket ? : '-' }}</td>
        <td>{{ $list->headline  ? : '-' }}</td>
        <td>{{ $list->no_speedy ? : '-' }}</td>
    </tr>
    @endforeach
</table>
</body>
</html>