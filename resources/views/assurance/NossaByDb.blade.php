@extends('layout')
    @section('content')
        @include('partial.alerts')
<?php
    date_default_timezone_set('Asia/Makassar');
    $date = date('d-m-Y');
?>
<!DOCTYPE html>
<html>
<head>
<style>
.button {
  display: inline-block;
  border-radius: 15px;
  background-color: #00BFFF;
  border: none;
  color: white;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

.badge {
    text-transform: uppercase;
    font-weight: 600;
    padding: 3px 5px;
    font-size: 12px;
    margin-top: 1px;
    background-color: #fec107;
}
</style>
</head>
<body>

<h3>MULTI SYNC NOSSA</h3>
  <ul class="nav nav-tabs ajax">
    <li><a href="/assurance/search" id="search">Search</a></li>
    <li><a href="/assurance/dispatch_manual" id="1">Dispatch Manual</a></li>
    <li class="active"><a href="/assurance/NossaByDb" id="NossaByDb">Multi Sync</a></li>
  </ul>
<center>
<br /><br /><br />
<button onclick="window.location.href='/grabNossaByDb/<?php echo date('Y-m-d') ?>';" class="button" style="vertical-align:middle"><span>Sync </span></button>
<br />
<span class="badge">{{ $date }} <a class="clock"></span></a>
</center>
</body>
</html>
@endsection
@section('plugins')
    <script type="text/javascript">
        $(function() {
            $("[data-toggle='popover']").popover({html:true});
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
        });
    </script>
@endsection