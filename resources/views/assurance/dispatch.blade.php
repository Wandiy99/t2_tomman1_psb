@extends('layout')

@section('content')
  @include('partial.alerts')
  <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script> -->
@if(session('auth')->level==15 || session('auth')->level==2)
  <form id="submit-form" method="post" autocomplete="off" class="white-box row col-sm-12">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif
    <h3 class="col-sm-12">
      <a href="../" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      @if($check_fitur->ibooster == 1)
      <a href="/grabIboosterbyIN/{{ $data->Incident }}/dispatch" class="btn btn-success">UKUR IBOOSTER</a>
      @endif
      Dispatch WO {{ $data->no_tiket }}
    </h3>
   <!--  <div class="panel panel-default">
      <div class="panel-heading">
        History PSB
      </div>
      <div class="panel-body">
        <div id="Maps" class="Maps" style="height:200px;">
        </div>
        {{ $data->lat ? : 0 }}, {{ $data->lon ? : 0 }}
      </div>
    </div> -->
    <input type="hidden" name="no_order" value="{{ $data->ID }}" />
    <div class="form-group col-sm-3">
      <label class="control-label" for="input-regu">Loker</label>
      <input name="loker" type="hidden" id="input-loker" class="form-control text-center" value="{{ $data->loker_ta }}" />
      {!! $errors->first('loker','<p class="label label-danger">:message</p>') !!}
    </div>
    <div class="form-group col-sm-3">
      <label class="control-label" for="input-regu">Alpro</label>
      <input name="alpro" type="hidden" id="input-alpro" class="form-control text-center" value="{{ $data->alpro }}" />
      {!! $errors->first('alpro','<p class="label label-danger">:message</p>') !!}
    </div>
    <div class="form-group col-sm-3">
      <label class="control-label" for="input-regu">Sektor</label>
      <input name="sektor" type="hidden" id="input-sektor" class="form-control text-center" value="{{ $data->chat_id }}" />
      {!! $errors->first('sektor','<p class="label label-danger">:message</p>') !!}
    </div>
    <div class="form-group col-sm-3">
      <label class="control-label" for="input-regu">Regu</label>
      <input name="id_regu" type="hidden" id="input-regu" class="form-control text-center" value="{{ $data->id_regu ? : $data->id_regu2 }}" />
      {!! $errors->first('id_regu','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group col-sm-3">
      <label class="control-label" for="jenis_ont" >Jenis ONT</label>
      <input name="jenis_ont" type="hidden" id="jenis_ont" class="form-control text-center" value="{{ $data->jenis_ont }}"/>
      {!! $errors->first('jenis_ont','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group col-sm-3">
      <label class="control-label" for="streetAddress" >Alamat</label>
      <input name="streetAddress" type="text" id="streetAddress" class="form-control text-center" value="{{ $data->orderAddr ? : $data->streetAddress }}"/>
      {!! $errors->first('streetAddress','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group col-sm-3">
      <label class="control-label" for="input-tgl">Tanggal Manja (Sesuai Nossa)</label>
      <br />
      <input class="form-control text-center" name="tgl" type="date" id="input-tgl"  value="{{ $data->tgl ? : $tgl }}" />
      {!! $errors->first('tgl','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="form-group col-sm-3">
      <label class="control-label" for="input-tgl">Jam Manja (Sesuai Nossa)</label>
      <br />
      <input class="form-control text-center" name="jamManja" type="time" id="jamManja" value="{{ $data->jam_jadwal_manja ? : $jam }}" />
      {!! $errors->first('jamManja','<p class="label label-danger">:message</p>') !!}
    </div>

    <div class="col-sm-12">
      <button class="btn btn-primary">Simpan</button>
    </div>
    
    <br /><br /><br /><br />

    <div class="col-sm-6">
      <label class="control-label">Hasil Ukur Ibooster</label>
        <br />
        ND : {{ $data->ND }}<br />
        IP_Embassy : {{ $data->IP_Embassy }}<br />
        Type : {{ $data->Type }}<br />
        Calling_Station_Id : {{ $data->Calling_Station_Id }}<br />
        IP_NE : {{ $data->IP_NE }}<br />
        ONU_Link_Status : {{ $data->ONU_Link_Status }}<br />
        OLT_Tx : {{ $data->OLT_Tx }}<br />
        OLT_Rx : {{ $data->OLT_Rx }}<br />
        ONU_Tx : {{ $data->ONU_Tx }}<br />
        ONU_Rx : {{ $data->ONU_Rx }}<br />
        Framed_IP_Address : {{ $data->Framed_IP_Address }}<br />
    </div>

    <div class="col-sm-6">
      <label class="control-label">Nossa Information</label>
        <br />
        Reported_Date : {{ $data->Reported_Date }}<br />
        Is_HOLD : {{ $data->Hold }}<br />
        Initial Date : {{ $data->UmurDatetime }}<br />
        Assigned_To : {{ $data->Assigned_to }}<br />
        Assigned_By : {{ $data->Assigned_by }}<br />
        Workzone : {{ $data->Workzone }}<br />
        Datek : {{ $data->Datek }}<br />
        Summary : {{ $data->Summary }}
    </div>
  </form>
  <!-- @if (isset($data->id))
    <form id="delete-form" method="post" autocomplete="off">
      <input name="_method" type="hidden" value="DELETE">
        <div>
          <button class="btn btn-danger">Hapus</button>
        </div>
    </form>
  @endif -->
  @endif
  
<script src="/js/jquery.min.js"></script>
<script src="/bower_components/select2/select2.min.js"></script>
  <!-- <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script> -->
  <script>
    $(function() {
      var dataloker = [
        {"id":"1", "text":"CONSUMER"},
        {"id":"2", "text":"CORPORATE"}
        ];

      var loker = function() {
        return {
          data: dataloker,
          placeholder: 'Input Loker',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#input-loker').select2(loker());


      var dataloker = [
        {"id":"1", "text":"CONSUMER"},
        {"id":"2", "text":"CORPORATE"}
        ];

      var loker = function() {
        return {
          data: dataloker,
          placeholder: 'Input Loker',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-loker').select2(loker());

      var dataalpro = [
        {"id":"COPPER", "text":"COPPER"},
        {"id":"GPON", "text":"GPON"}
        ];

      var alpro = function() {
        return {
          data: dataalpro,
          placeholder: 'Input Alpro',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-alpro').select2(alpro());

      var dataOnt = [
        {"id":"ZTE", "text":"ZTE"},
        {"id":"HUAWEI", "text":"HUAWEI"},
        {"id":"ALU", "text":"ALU"}
      ];

      var ont = function() {
        return {
          data: dataOnt,
          placeholder: 'Input Jenis ONT',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#jenis_ont').select2(ont());


      $('.btn-danger').click(function() {
        var sure = confirm('Yakin hapus data ?');
        if (sure) {
          $('#delete-form').submit();
        }
      })
      var state= <?= json_encode($regu) ?>;
      var regu = function() {
        return {
          data: state,
          placeholder: 'Input Regu',
          formatResult: function(data) {
          return  '<span class="label label-default">'+data.title+'</span> '+
                '<strong style="margin-left:5px">'+data.text+'</strong>';
          }
        }
      }
      $('#input-regu').select2(regu());

      var data_sektor= <?= json_encode($sektor) ?>;
      var sektor = function() {
        return {
          data: data_sektor,
          placeholder: 'Pilih Sektor',
          formatResult: function(data) {
          return  data.text;
          }
        }
      }
      $('#input-sektor').select2(sektor());
      $("#input-regu").select2({
        initSelection: function (element, callback) {
	                var data = { "id": "{{ $data->id_regu ? : ''}}", "text": "{{ $data->uraian ? : ''}}" };
	                callback(data);
	        },
	      ajax : {
				url : "/assurance/inputRegu/",
				dataType : "json",
        data: function() {
          return {
            sektor : $("#input-sektor").val()
          }
        },
				results : function (data){
					var myResults = [];
					$.each(data, function (index, item) {

	                myResults.push({
	                    'id': item.id,
	                    'text': item.text
	                });
	            });
	            return {
	                results: myResults
	            };
				}
			}
    });
      // var day = {
      //   format: 'yyyy-mm-dd',
      //   viewMode: 0,
      //   minViewMode: 0
      // };
      // $('#input-tgl').datepicker(day).on('changeDate', function(e){
      //   $(this).datepicker('hide');
      // });
      // $('#tglManja').datepicker(day).on('changeDate', function(e){
      //   $(this).datepicker('hide');
      // });

      // $('#jamManja').timepicker({
      //   showMeridian : false,
      //   disableMousewheel : true,
      //   minuteStep : 1
      // });


    })
  </script>
@endsection
