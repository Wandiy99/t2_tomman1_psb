@extends('layout')
@section('content')
<style>
    th, td {
        text-align: center;
        vertical-align: middle;
    }
    h3 {
        text-align: center;
        font-weight: bold;
    }
    option {
        text-align: center;
    }
</style>
@include('partial.alerts')
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
          <h4 class="page-title" style="text-align: center; font-weight: bold;">MEDIA CARRING ASSURANCE WITEL KALSEL</h4><br/>
          <form method="GET">
            <div class="row">
              <div class="col-md-5">
                  <label for="witel">STATUS</label>
                  <select class="form-control" data-placeholder="- Pilih Status -" tabindex="1" name="status">
                    @if ($status == "UNDEFINED")
                        <option value="UNDEFINED">UNDEFINED</option>
                        <option value="OK">OK</option>
                        <option value="NOK">NOK</option>
                        <option value="ALL">ALL</option>
                    @elseif ($status == "OK")
                        <option value="OK">OK</option>
                        <option value="NOK">NOK</option>
                        <option value="ALL">ALL</option>
                        <option value="UNDEFINED">UNDEFINED</option>
                    @elseif ($status == "NOK")
                        <option value="NOK">NOK</option>
                        <option value="ALL">ALL</option>
                        <option value="UNDEFINED">UNDEFINED</option>
                        <option value="NOK">OK</option>
                    @else
                        <option value="ALL">ALL</option>
                        <option value="UNDEFINED">UNDEFINED</option>
                        <option value="OK">OK</option>
                        <option value="NOK">NOK</option>
                    @endif
                  </select>
              </div>
              <div class="col-md-5">
                <label for="date">TANGGAL</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd" name="tanggal" value="{{ $tanggal ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
              <div class="col-md-2">
                  <label for="search">&nbsp;</label>
                  <button class="btn btn-info btn-block" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    <div class="col-sm-12">
        <div class="white-box">
            <div class="table-responsive">
                <table id="table_inbox" class="table_inbox display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>INCIDENT</th>
                            <th>CONTACT NAME</th>
                            <th>CONTACT PHONE</th>
                            <th>REPORTED DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $num => $d)
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>
                                <div id="responsive-modal-{{ $d->ID }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-{{ $d->ID }}" aria-hidden="true" style="display: none;">
                                    <form class="form-group" action="/assurance/inbox/mediaCarringSave" method="POST">
                                        <input type="hidden" name="id_nossa" value="{{ $d->ID }}">
                                        <input type="hidden" name="incident" value="{{ $d->Incident }}">
                                        <input type="hidden" name="contact_name" value="{{ $d->Contact_Name ? : '-' }}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                    <a style="float: left;" href="/grabIboosterbyIN/{{ $d->Incident }}/media_carring" class="btn btn-sm btn-rounded btn-success">Ukur Ibooster</a>

                                                    <h4 class="modal-title">{{ $d->Incident }} - {{ $d->Contact_Name }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive col-sm-12">
                                                        <table class="table">
                                                            <tbody>
                                                                <label class="label label-success">{{ $d->tim ? : '-' }}</label> &nbsp; <label class="label label-default">{{ $d->Workzone ? : '-' }}</label> &nbsp; <label class="label label-info">{{ $d->sektor ? : '-' }}</label>
                                                                <tr>
                                                                    <td class="text-center">Contact Name</td>
                                                                    <td class="text-center">:</td>
                                                                    <td style="text-align: right;">{{ $d->Contact_Name ? : '-' }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">Contact Phone</td>
                                                                    <td class="text-center">:</td>
                                                                    <td style="text-align: right;">{{ $d->Contact_Phone ? : '-' }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">No Handphone Aktif</td>
                                                                    <td class="text-center">:</td>
                                                                    <td style="text-align: right;">{{ $d->noPelangganAktif ? : '-' }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">Detail</td>
                                                                    <td class="text-center">:</td>
                                                                    <td style="text-align: right;">[{{ $d->laporan_status ? : 'ANTRIAN' }}][{{ $d->action ? : '-' }}][{{ $d->penyebab ? : '-' }}]</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">Ibooster</td>
                                                                    <td class="text-center">:</td>
                                                                    <td style="text-align: right;">{{ $d->ONU_Rx ? : '-' }} Rx | {{ $d->ONU_Link_Status ? : '-' }} | {{ $d->ib_date_created ? : '-' }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">Status Caring</td>
                                                                    <td class="text-center">:</td>
                                                                    <td class="text-center">
                                                                        <select class="form-control select2" name="status_carring" required>
                                                                            <option value="" selected disabled>-- Pilih Status --</option>
                                                                            <option value="OK">OK</option>
                                                                            <option value="NOK">NOK</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">No WhatsApp Aktif</td>
                                                                    <td class="text-center">:</td>
                                                                    <td>
                                                                        <input type="text" name="no_whatsapp" class="text-center" style="width:100%" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength="10" value="{{ $d->no_whatsapp }}" required>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">Catatan Caring</td>
                                                                    <td class="text-center">:</td>
                                                                    <td>
                                                                        <textarea type="text" name="catatan_carring" class="text-center" rows="4" style="width:100%" required>{{ $d->catatan_carring }}</textarea>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-rounded btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-rounded btn-info waves-effect waves-light">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <span type="button" class="label label-info" data-toggle="modal" data-target="#responsive-modal-{{ $d->ID }}">
                                    {{ $d->Incident }}
                                </span>
                                &nbsp;
                                @if($d->status_carring == 'OK')
                                    <label class="label label-success">OK</label>
                                @elseif ($d->status_carring == 'NOK')
                                    <label class="label label-danger">NOK</label>
                                @else
                                    <label class="label label-default">UNDEFINED</label>
                                @endif
                            </td>
                            <td>{{ $d->Contact_Name }}</td>
                            <td>{{ $d->Contact_Phone }}</td>
                            <td>{{ $d->Reported_Datex }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.table_inbox').DataTable();
    });
    $('#datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
</script>
@endsection