
@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .strong{
      font-weight: bold;
    }​
    .stronger{
      font-weight: bold;
      font-size: 9px;
    }
  </style>
  <h3>
    ASSURANCE
  </h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          @foreach ($query as $result)
          <form method="post">
          <table class="table">
            <tr>
              <td>INT</td>
              <td width=20>:</td>
              <td>
                {{ $result->Incident }}
                <input type="hidden" name="Incident" value="{{ $result->Incident }}">
              </td>
            </tr>

            <tr>
              <td>HEADLINE</td>
              <td width=20>:</td>
              <td>{{ $result->Summary }}</td>
            </tr>

            <tr>
              <td>CUSTOMER</td>
              <td width=20>:</td>
              <td>{{ $result->Customer_Name }}</td>
            </tr>

            <tr>
              <td>Level</td>
              <td>:</td>
              <td>
                <select class="form-input" name="level">
                  <option value="">--</option>
                  @foreach ($level as $asr_level)
                  <option value="{{ $asr_level->assurance_level }}">{{ $asr_level->assurance_level }}</option>
                  @endforeach
                </select>
              </td>
            </tr>
            <tr>
              <td>Note</td>
              <td>:</td>
              <td>
                <textarea name="note" rows="8" cols="80"></textarea>
              </td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><input type="submit" value="submit" /></td>
            </tr>
          </table>
        </form>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection
