@extends('layout')

@section('content')
  @include('partial.alerts')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Report Dismantling Kalsel</div>
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-strip">
                    <tr>
                        <td class="text-center">STO</td>
                        <td class="text-center">JML</td>
                        <td class="text-center">UNDISPATCH</td>
                        <td class="text-center">NP</td>
                        <td class="text-center">OGP</td>
                    </tr>
                    @php
                        $total = 0; 
                    @endphp
                    @foreach ($dashboard_1 as $result)
                    @php
                        $total += $result->jumlah;
                    @endphp
                    <tr>
                        <td class="text-center">{{ $result->sto ? : "UNDEFINED" }}</td>
                        <td class="text-center">{{ $result->jumlah }}</td>
                        <td class="text-center">{{ $result->undispatch }}</td>
                        <td class="text-center">{{ $result->np }}</td>
                        <td class="text-center">{{ $result->ogp }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="text-center">TOTAL</td>
                        <td class="text-center">{{ $total }}</td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection