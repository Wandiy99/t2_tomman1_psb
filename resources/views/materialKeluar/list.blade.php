@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/material-keluar/input" class="btn btn-info btn-sm">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
    Material Keluar
  </h3>

  <div class="list-group">
    @foreach($list as $data)
      <a href="/material-keluar/{{ $data->id }}" class="list-group-item">
        <strong>{{ $data->tgl }}</strong>
        <br />
        <span>{{ $data->updater }}</span>
        <br />
        <span class="">{{ $data->ts }}</span>
      </a>
    @endforeach
  </div>
@endsection
