@extends('layout')

@section('content')
@include('partial.alerts')
 <h4>List Wo Fulfillment Guarantee</h4>
  <form class="row" style="margin-bottom: 20px">
      <div class="col-md-12 {{ $errors->has('q') ? 'has-error' : '' }}">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Cari No WO" name="q"/>
              <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
          </div>
          {!! $errors->first('q','<p class=help-block>:message</p>') !!}
      </div>
  </form>

  @if(count($listFulls) <> 0)
    <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>#</th>
  		    <th>SC</th>
          <th>Tgl Dispatch</th>
          <th>Nama Pelanggan</th>
          <th>STO</th>
          <th>Regu</th>
          <th width="20%">Aksi</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($listFulls as $no=>$listFull)
              <tr>
                  <td>{{ ++$no}}</td>
                  <td>{{ $listFull->Ndem }}</td>
                  <td>{{ $listFull->updated_at }}</td>
                  <td>{{ $listFull->orderName}}</td>
                  <td>{{ $listFull->sto}}</td>
                  <td>{{ $listFull->uraian}}</td>
                   
                  <td>
                      <a href="/fulfillment/redispatch/{{ $listFull->orderId }}" class="btn btn-danger btn-sm">Re-Dispatch Regu FG</a>
                      <a href="/fulfillment/hapusfg/{{ $listFull->orderId }}" class="btn btn-primary btn-sm">Hapus WO-FG</a>
                  </td>
              </tr>
          @endforeach
      </tbody>
    </table>
  @endif

  {{-- </div> --}}
@endsection
