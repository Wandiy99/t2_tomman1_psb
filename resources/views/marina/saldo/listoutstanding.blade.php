@extends('layout')
@section('content')
@include('marina.partial.alert')
  <div class="panel panel-default" id="info">
      <div class="panel-heading">LIST SALDO OUTSTANDING
      	<a href="/marina/saldo/input" class="btn btn-info btn-xs pull-right" style="margin: 0 -15px;">
	        <span class="glyphicon glyphicon-plus"></span>
	        <span>Input RFC</span>
		    </a>
	  	</div>
      <div id="fixed-table-container-demo" class="fixed-table-container">
          <table class="table table-bordered table-fixed">
              <thead>
                <tr>
                  <th width="20" class="head">#</th>
                  <th width="20" class="head">RFC</th>
                  <th width="20" class="head">Item Keluar</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $no => $d) 
                  <tr>
                    <td>{{ ++$no }}</td>
                    <td><a href="#" class="getlist" data-rfc="{{str_replace('/','*',$d->project)}}">{{ $d->project }}</a></td>
                    <td>{{ $d->baris }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
      </div>
  </div>
@endsection
@section('plugins')
<script type="text/javascript">
$(function(){
});
</script>
@endsection