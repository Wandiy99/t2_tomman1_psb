@extends('layout')
@section('header')
    <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
    <!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
    <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Download
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="mitra" class="col-sm-3 col-md-2 control-label">Jenis Order</label>
                    <div class="col-sm-5">
                        <input name="jenis_order" type="text" id="jenis_order" class="form-control" value="{{ old('jenis_order') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mitra" class="col-sm-3 col-md-2 control-label">Date</label>
                    <div class="col-sm-5">
                        <input type="text" name="tgl" id="tgl" value="{{ date('Y-m-d') }}" class="form-control input-sm">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Download</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('plugins')
    <!-- <script src="/bower_components/select2/select2.min.js"></script> -->
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
    <script>
        $(function() {
            var day = {
              format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
            };
            $("#tgl").datepicker(day).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
            var data = <?= json_encode($data) ?>;
            $('#jenis_order').select2({
                data: data,
                placeholder: 'Pilih jenis_order',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
        });
    </script>
@endsection