@extends('layout')
@section('head')
  <style type="text/css">
    th{
        text-align: center;
        vertical-align: middle;
    }
    .geser_kiri_dikit {
      position: relative;
      left : -10px;
    }
  </style>
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">MATRIX PRODUKTIFITAS<span class="badge clock"></div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">Nama<br/>Tim</th>
                    <th colspan="4">WO {{ Request::segment(2) }}</th>
                    <th rowspan="2">Prod</th>
                    <th rowspan="2">List</th>
                </tr>
                <tr>
                    <th>Sisa</th>
                    <th>Kendala<br/>Teknis</th>
                    <th>Kendala<br/>Pelanggan</th>
                    <th>Close</th>
                </tr>
                @foreach($regu as $noo => $d)
                    <?php
                        @$id = $list[$d->id_regu];
                    ?>
                    @if($id)
                    <tr>
                        <td>{{ ++$noo }}</td>
                        <td>{{ $d->uraian }}:<span class="label label-success">{{ $d->spesialis }}</span><br/>
                            
                        </td>
                        <!--<td>{{ $d->jumlah }}</td>-->
                        <td>{{ $d->no_update }}</td>
                        <td>{{ $d->kendala_teknis }}</td>
                        <td>{{ $d->kendala_pelanggan }}</td>
                        <td>{{ $d->close }}</td>
                        <td></td>
                        <td>
                            
                            @foreach($id as $nolist => $l)
                                <?php
                                    $label = 'warning';
                                    if($l['status_laporan'] == 'close')
                                        $label = 'success';
                                    else if($l['status_laporan'] == null)
                                        $label = 'default';
                                ?>
                                <!--<span class="label label-{{ $label }}">{{ $l['tiket'] }}</span>-->
                                <a data-html="true" data-original-title data-toggle="popover" title="" data-content="<b>Status</b> : <br />{{ $l['status_laporan']? : 'ANTRIAN' }}<a href='/tech/{{ $l['id'] }}'>Detil</a>" class="label label-{{ $label }}">{{ $l['tiket'] }}</a>
                                @if(substr($l['created_at'], 0, 10)==date('Y-m-d'))
                                    <img src="http://acpe.alaska.gov/portals/3/Images/NEW_burst_for_webpage_item.png" class="geser_kiri_dikit" width="42" height="20"/>
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    @endif
                @endforeach

            </table>
        </div>
    </div>
@endsection
@section('plugins')
    <script type="text/javascript">
        $(function() {
            $("[data-toggle='popover']").popover({html:true});
            setInterval(function() {

              var currentTime = new Date();
              var hours = currentTime.getHours();
              var minutes = currentTime.getMinutes();
              var seconds = currentTime.getSeconds();

              // Add leading zeros
              hours = (hours < 10 ? "0" : "") + hours;
              minutes = (minutes < 10 ? "0" : "") + minutes;
              seconds = (seconds < 10 ? "0" : "") + seconds;

              // Compose the string for display
              var currentTimeString = hours + ":" + minutes + ":" + seconds;

              $(".clock").html(currentTimeString);

            }, 1000);
        });
    </script>
@endsection