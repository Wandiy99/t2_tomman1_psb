@extends('layout')
@section('header')
  <style type="text/css">
    th{
        text-align: center;
    }
  </style>
@endsection
@section('content')
<div class="panel panel-default" id="info">
    <div class="panel-heading">DETIL </div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-fixed">
            <tr>
                <th>#</th>
                <th>No Tiket</th>
                <th>Designator</th>
                <th>Qty</th>
                <th>Jasa</th>
                <th>Material</th>
                <th>Total Jasa</th>
                <th>Total Material</th>
                <th>Total</th>
            </tr>
            <?php
                $total = 0;
                $id=0;
                $nomor=0;
                $sumtotal=0;
                $sumtotaljasa=0;
                $sumtotalmaterial=0;
            ?>
            @foreach($data as $no => $d)
                @if($id == $d->maintaince_id)
                <?php
                    $sumtotaljasa += ($d->jasa_ta*$d->qty);
                    $sumtotalmaterial += ($d->material_ta*$d->qty);
                    $sumtotal += ($d->jasa_ta*$d->qty)+($d->material_ta*$d->qty);
                ?>
                <tr class="ajaxd">
                    <td>{{ $d->id_item }}</td>
                    <td>{{ $d->qty }}</td>
                    <td align="right">{{ number_format($d->jasa_ta) }}</td>
                    <td align="right">{{ number_format($d->material_ta) }}</td>
                    <td align="right">{{ number_format($d->jasa_ta*$d->qty) }}</td>
                    <td align="right">{{ number_format($d->material_ta*$d->qty) }}</td>
                    <td align="right">{{ number_format(($d->jasa_ta*$d->qty)+($d->material_ta*$d->qty)) }}</td>
                </tr>
                @else
                
                @if($nomor)
                <tr class="ajaxd">
                    <td colspan="4">Sum Total</td>
                    <td align="right">{{ number_format($sumtotaljasa) }}</td>
                    <td align="right">{{ number_format($sumtotalmaterial) }}</td>
                    <td align="right">{{ number_format($sumtotal) }}</td>
                </tr>
                @endif
                <?php
                    $sumtotal=0;
                    $sumtotaljasa=0;
                    $sumtotalmaterial=0;
                    $sumtotaljasa += ($d->jasa_ta*$d->qty);
                    $sumtotalmaterial += ($d->material_ta*$d->qty);
                    $sumtotal += ($d->jasa_ta*$d->qty)+($d->material_ta*$d->qty);
                ?>
                <tr class="ajaxd">
                    <td rowspan="{{ $d->countmtr+1 }}">{{ ++$nomor }}</td>
                    <td rowspan="{{ $d->countmtr+1 }}">{{ $d->no_tiket }}</td>
                    <td>{{ $d->id_item }}</td>
                    <td>{{ $d->qty }}</td>
                    <td align="right">{{ number_format($d->jasa_ta) }}</td>
                    <td align="right">{{ number_format($d->material_ta) }}</td>
                    <td align="right">{{ number_format($d->jasa_ta*$d->qty) }}</td>
                    <td align="right">{{ number_format($d->material_ta*$d->qty) }}</td>
                    <td align="right">{{ number_format(($d->jasa_ta*$d->qty)+($d->material_ta*$d->qty)) }}</td>
                </tr>
                @endif
                <?php
                    $total += ($d->jasa_ta*$d->qty)+($d->material_ta*$d->qty);$id=$d->maintaince_id;
                ?>
            @endforeach
            <tr class="ajaxd">
                    <td colspan="4">Sum Total</td>
                    <td align="right">{{ number_format($sumtotaljasa) }}</td>
                    <td align="right">{{ number_format($sumtotalmaterial) }}</td>
                    <td align="right">{{ number_format($sumtotal) }}</td>
                </tr>
            <tr class="ajaxd">
                <td colspan="5">Total</td>
                <td align="right">{{ number_format($total) }}</td>
            </tr>
        </table>
    </div>
</div>
@endsection