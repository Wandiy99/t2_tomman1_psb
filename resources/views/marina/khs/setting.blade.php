@extends('layout')
@section('header')
    <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
    <!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Setting KHS untuk Program <span class="label label-primary">{{ $data->nama_order }}</span>
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <input name="hid" type="hidden" value="{{ $data->id }}"/>
                <div class="form-group {{ $errors->has('id_item') ? 'has-error' : '' }}">
                    <label for="id_item" class="col-sm-3 col-md-2 control-label">Designator</label>
                    <div class="col-sm-5">
                        <input name="id_item" type="text" id="id_item" class="form-control"
                               value="{{ old('id_item') ?: @$regu->id_item }}"/>
                        @foreach($errors->get('id_item') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Tambah</span>
                        </button>
                    </div>
                </div>
            </form>
            <ul class="list-group">
            @foreach($itemModif as $i)
                <a href="#">
                    <li class="list-group-item">
                        {{ $i->id_item }}
                    </li>
                </a>
            @endforeach
            </ul>
        </div>
    </div>
@endsection
@section('plugins')
    <!-- <script src="/bower_components/select2/select2.min.js"></script> -->
    <script>
        $(function() {
            var item = <?= json_encode($item) ?>;
            $('#id_item').select2({data: item,
                placeholder: 'Select Designator'});
        });
    </script>
@endsection