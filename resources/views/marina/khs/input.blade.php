@extends('layout')
@section('header')
    <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" /> -->
    <!-- <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if (isset($data))
                Edit Item KHS {{ $data->id_item }}
            @else
                Input Item KHS Baru
            @endif
        </div>
        <div class="panel-body">
            <form method="post" class="form-horizontal">
                <input name="hid" type="hidden" value="{{ @$data->id }}"/>
                <div class="form-group">
                    <label for="id_item" class="col-sm-3 col-md-2 control-label">ID Item</label>
                    <div class="col-sm-5">
                        <input name="id_item" type="text" id="id_item" class="form-control input-sm" value="{{ @$data->id_item }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="uraian" class="col-sm-3 col-md-2 control-label">Uraian</label>
                    <div class="col-sm-5">
                        <textarea name="uraian" id="uraian" class="form-control">{{ @$data->uraian }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="satuan" class="col-sm-3 col-md-2 control-label">satuan</label>
                    <div class="col-sm-5">
                        <input name="satuan" type="text" id="satuan" class="form-control input-sm" value="{{ @$data->satuan }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="material_telkom" class="col-sm-3 col-md-2 control-label">Material TELKOM-TA</label>
                    <div class="col-sm-5">
                        <input name="material_telkom" type="text" id="material_telkom" class="form-control input-sm" value="{{ @$data->material_telkom }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jasa_telkom" class="col-sm-3 col-md-2 control-label">Jasa TELKOM-TA</label>
                    <div class="col-sm-5">
                        <input name="jasa_telkom" type="text" id="jasa_telkom" class="form-control input-sm" value="{{ @$data->jasa_telkom }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="material_ta" class="col-sm-3 col-md-2 control-label">Material TA-MITRA</label>
                    <div class="col-sm-5">
                        <input name="material_ta" type="text" id="material_ta" class="form-control input-sm" value="{{ @$data->material_ta }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jasa_ta" class="col-sm-3 col-md-2 control-label">Jasa TA-MITRA</label>
                    <div class="col-sm-5">
                        <input name="jasa_ta" type="text" id="jasa_ta" class="form-control input-sm" value="{{ @$data->jasa_ta }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis" class="col-sm-3 col-md-2 control-label">Jenis KHS</label>
                    <div class="col-sm-5">
                        <input name="jenis" type="text" id="jenis" class="form-control input-sm" value="{{ @$data->jenis }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alista_designator" class="col-sm-3 col-md-2 control-label">Designator Alista</label>
                    <div class="col-sm-5">
                        <input name="alista_designator" type="text" id="alista_designator" class="form-control input-sm" value="{{ @$data->alista_designator }}">
                    </div>
                </div>

                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-2 col-sm-2">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('plugins')
    <!-- <script src="/bower_components/select2/select2.min.js"></script> -->
    <script type="text/javascript">
        $(function() {
            var data = <?= json_encode($jeniskhs) ?>;
            $('#jenis').select2({data:data,placeholder:"Pilih Jenis Khs"});
        });
    </script>
@endsection