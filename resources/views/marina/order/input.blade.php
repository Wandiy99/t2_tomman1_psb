@extends('layout')

@section('header')
    <link rel="stylesheet" href="/css/mapbox-gl.css" />
    <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" />
    <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" /> -->
@endsection

@section('content')
    <?php
        $auth = (session('auth'));
        // dd($auth);
    ?>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            Input Order dan Assignment
        </div>
        <div class="panel-body">
            <div class="form col-sm-8 col-md-8">
            <form method="post" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                    <label for="txtKode" class="col-sm-4 col-md-3 control-label">Order ID</label>
                    <div class="col-sm-8">
                        <input name="order_id" type="text" id="txtKode" class="form-control input-sm" value="{{ old('order_id') ?: @$data->order_id }}"/>
                        @foreach($errors->get('order_id') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis_order" class="col-sm-4 col-md-3 control-label">Jenis Order</label>
                    <div class="col-sm-8">
                        <select name="jenis_order" id="jenis_order" class="form-control input-sm">
                            @foreach($jenis_order as $jo)
                                    <option value="{{ $jo->id }}" {{ @$data->jenis_order== $jo->id ? "selected" : "" }}>{{ $jo->nama_order }}</option>
                                @if($jo->id != 4)
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('no_inet') ? 'has-error' : '' }}">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">No.INET</label>
                    <div class="col-sm-8">
                        <input name="no_inet" type="text" id="no_inet" class="form-control input-sm" value="{{ old('no_inet') ?: @$data->no_inet }}"/>
                        @foreach($errors->get('no_inet') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">Nama ODP</label>
                    <div class="col-sm-8">
                        <input name="nama_odp" type="text" id="nama_odp" class="form-control input-sm" value="{{ old('nama_odp') ?: @$data->nama_odp }}"/>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('headline') ? 'has-error' : '' }}">
                    <label for="txtPin" class="col-sm-4 col-md-3 control-label">Headline</label>
                    <div class="col-sm-8">
                        <textarea name="headline" rows="4" class="form-control">{{ old('headline') ?: @$data->headline }}</textarea>
                        @foreach($errors->get('headline') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <label for="tipe" class="col-sm-4 col-md-3 control-label">PIC</label>
                    <div class="col-sm-8">
                        <input name="pic" type="text" id="pic" class="form-control input-sm" value="{{ old('pic') ?: @$data->pic }}"/>
                    </div>
                </div>
                <!--
                <div class="form-group">
                    <label for="koordinat" class="col-sm-4 col-md-3 control-label">Koordinat</label>
                    <div class="col-sm-7">
                        <input name="koordinat" type="text" id="koor" class="form-control" value="{{ old('koordinat') ?: @$data->koordinat }}"/>
                    </div>
                </div>
                -->
                <div class="form-group {{ $errors->has('koordinat') ? 'has-error' : '' }}">
                    <label for="" class="col-sm-4 col-md-3 col-xs-12 control-label">Koordinat</label>
                    <div class="col-sm-6 col-xs-9">
                        <div class="input-group">
                            <input name="koordinat" id="input-koordinat" class="form-control input-sm" rows="1" value="{{ old('koordinat') ?: @$data->koordinat }}" />
                            <span class="input-group-btn">
                                <button id="btn-koordinat" class="btn btn-default" type="button">Peta</button>
                            </span>
                        </div>
                        @foreach($errors->get('koordinat') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                    <!-- <div class="col-sm-2 col-xs-3" align="right">
                        <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-default btn-sm" data-toggle="tooltip" type="button">
                            <i class="fa fa-map-marker"></i>
                        </button>
                    </div> -->
                </div>
                    <div class="form-group {{ $errors->has('regu') ? 'has-error' : '' }}">
                        <label for="regu" class="col-sm-4 col-md-3 control-label">Order ke Tim</label>
                        <div class="col-sm-8">
                            <input name="regu" type="text" id="regu" class="form-control input-sm" value="{{ old('regu') ?: @$data->dispatch_regu_id }}"/>
                            @foreach($errors->get('regu') as $msg)
                                <span class="help-block">{{ $msg }}</span>
                            @endforeach
                        </div>
                    </div>
                <div class="form-group {{ $errors->has('sto') ? 'has-error' : '' }}">
                    <label for="sto" class="col-sm-4 col-md-3 control-label">STO</label>
                    <div class="col-sm-8">
                        <input name="sto" type="text" id="sto" class="form-control input-sm" value="{{ old('sto') ?: @$data->sto }}"/>
                        @foreach($errors->get('sto') as $msg)
                            <span class="help-block">{{ $msg }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-3">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            <span>Simpan</span>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@section('plugins')
    @include('partial.map.modal');
    <script>
		// jika di satu halaman ada lebih dari satu input koordinat, silahkan masukkan satu - persatu ke initButton
		//   MapModal.initButton(jqueryButtonElement, jqueryInputElement, modalTitle);
		//
		// dimana:
		//   jqueryButtonElement: jquery object untuk tombol yang akan menampilkan popup/modal
		//   jqueryInputElement: jquery object untuk input yang menerima koordinat
		//   modalTitle: judul yang ditampilkan di popup/modal
        MapModal.initButton($('#btn-koordinat'), $('#input-koordinat'), 'Koordinat ODP');
    </script>

    <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script> -->
    <!-- <script src="/js/mapmarkerlonlat.js"></script> -->
    <!-- <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script> -->
    <script>
        $(function() {
            var data = <?= json_encode($sto) ?>;
            var combo = $('#sto').select2({
                data: data,
                placeholder: 'Input STO'
            });
            var data = <?= json_encode($regu) ?>;
            var regu = $('#regu').select2({
                data: data,
                placeholder: 'Input Regu'
            });
        });
    </script>
@endsection