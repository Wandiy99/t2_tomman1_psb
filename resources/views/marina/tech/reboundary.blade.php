@extends('tech_layout')

@section('content')
  <style>
  .btn strong.glyphicon {         
    opacity: 0;       
  }
  .btn.active strong.glyphicon {        
    opacity: 1;       
  }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
  <!-- <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" /> -->
  @include('marina.partial.alert')
  <h3>
    <a href="/marina/tech/{{ Request::segment(3) }}" class="btn btn-sm btn-default">
      <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
    REBOUNDARY {{ Request::segment(3) }}
  </h3>
<form id="submit-form" method="post" action="/marina/nextStep/{{ Request::segment(3) }}" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="nextstep" value="{{ $data->step_id?$data->step_id+1:'1' }}">
    <!-- <span class="">
      <button class="btn btn-success btn-sm pull-right" type="submit">Simpan & Lanjut</button>
    </span> -->
    <span class="">
      <button class="btn btn-warning btn-sm" type="button" data-toggle="modal" data-target="#modal-info">Tambah Reboundary</button>
    </span>
</form>
<div class="panel panel-default" id="info" style="margin-top: 5px;">
   <div id="fixed-table-container-demo" class="fixed-table-container table-responsive">
      <table class="table table-bordered table-fixed">
          <thead>
          <tr>
              <th class="head">No</th>
              <th class="head">No. Layanan</th>
              <th class="head">ODP Lama</th>
              <th class="head">ODP Baru</th>
              <th class="head">QRCODE DC</th>
              <th class="head">Action</th>
          </tr>
          </thead>
          <tbody>
          @foreach($list as $no => $d)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $d->no_layanan }}</td>
                  <td>{{ $d->odp_lama }}</td>
                  <td>{{ $d->odp_baru }}</td>
                  <td>{{ $d->qrcode_dc }}</td>
                  <td><span class="btn btn-primary btn-xs btnform" data-toggle="modal" data-target="#modal-info" data-id="{{ $d->id }}">Edit</span></td>
              </tr>
          @endforeach
          </tbody>
      </table>
  </div>
</div>
  <div id="modal-info" class="modal fade modal-alert modal-info">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">Input Reboundary</div>
        <form id="submit-form" method="post" autocomplete="off" action="/marina/reboundary/{{ Request::segment(3) }}">
        <div id="detilContent" class="modal-body">
            <input name="vid" type="hidden" id="vid"/>
            <div class="form-group">
              <label class="control-label" for="no_layanan">No Layanan</label>
              <input name="no_layanan" type="text" id="no_layanan" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="odp_lama">ODP Lama</label>
              <input name="odp_lama" type="text" id="odp_lama" value="{{ $data->nama_odp }}" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="odp_baru">ODP Baru</label>
              <input name="odp_baru" type="text" id="odp_baru" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="qrcode_dc">QRCODE DC</label>
              <input name="qrcode_dc" type="text" id="qrcode_dc" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="tarikan_lama">Panjang Tarikan Lama(m)</label>
              <input name="tarikan_lama" type="text" id="tarikan_lama" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="tarikan_baru">Panjang Tarikan Baru(m)</label>
              <input name="tarikan_baru" type="text" id="tarikan_baru" class="form-control"/>
            </div>
            <div style="margin:40px 0 20px">
              <button class="btn btn-primary">Simpan</button>
              <button class="btn btn-secondary pull-right" type="button" class="close" data-dismiss="modal">Tutup</button>
            </div>
        </div>
      </form>
      </div>
    </div>
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <!-- <script src="/bower_components/select2/select2.min.js"></script> -->
  <script>
    $(function(){
      $('.btnform').click(function(){
        var vid = $(this).data("id");
        var url = "/marina/reboundaryGetJson/"+vid;
        $.getJSON(url, function(r){
          $('#vid').val(r.id);
          $('#no_layanan').val(r.no_layanan);
          $('#odp_lama').val(r.odp_lama);
          $('#odp_baru').val(r.odp_baru);
          $('#qrcode_dc').val(r.qrcode_dc);
          $('#tarikan_baru').val(r.tarikan_baru);
          $('#tarikan_lama').val(r.tarikan_lama);
        });
      });
      $('#jenis_layanan').select2({data:[{id:"Internet", text:"Internet"}, {id:"Telpon", text:"Telpon"}]});
      $('#status').select2({data:[{id:"IDLE", text:"IDLE"}, {id:"Teridentifikasi", text:"Teridentifikasi"}, {id:"Tidak Teridentifikasi", text:"Tidak Teridentifikasi"}, {id:"Rusak", text:"Rusak"}, {id:"Node-B", text:"Node-B"}, {id:"DATIN", text:"DATIN"}, {id:"WIFI.ID", text:"WIFI.ID"}]});
    });
  </script>

@endsection
