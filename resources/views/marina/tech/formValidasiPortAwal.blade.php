@extends('tech_layout')

@section('content')
  <style>
  .btn strong.glyphicon {         
    opacity: 0;       
  }
  .btn.active strong.glyphicon {        
    opacity: 1;       
  }
  </style>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script> -->
  <link rel="stylesheet" href="/bower_components/select2/select2.css" />
  <link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
  @include('marina.partial.alert')
  <h3>
          <a href="/marina/tech/{{ Request::segment(3) }}" class="btn btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          Validasi PORT AWAL{{ Request::segment(3) }}
        </h3>
  <div class="panel panel-default" id="info">
    <div class="panel-heading">Kapasitas</div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/marina/saveValidasiKapAwal/{{ Request::segment(3) }}" enctype="multipart/form-data" autocomplete="off">
        
        <div class="input-group">
          <input name="kapasitas" type="text" id="kapasitas" class="form-control" value="{{ $data->kapasitas_odp or '' }}" placeholder="isi kapasitas" />
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Submit!</button>
          </span>
        </div>
      </form>
    </div>
</div>
<div class="panel panel-default" id="info">
  <div class="panel-heading">Validasi Awal</div>
   <div id="fixed-table-container-demo" class="fixed-table-container table-responsive">
      <table class="table table-bordered table-fixed">
          <thead>
          <tr>
              <th class="head">Port</th>
              <th class="head">Jenis Layanan</th>
              <th class="head">No. Layanan</th>
              <th class="head">QRCODE SPL</th>
              <th class="head">QRCODE DC</th>
              <th class="head">Status</th>
              <th class="head">Action</th>
          </tr>
          </thead>
          <tbody>
          @foreach($list as $no => $d)
              <tr>
                  <td>{{ $d->port }}</td>
                  <td>{{ $d->jenis_layanan }}</td>
                  <td>{{ $d->no_layanan }}</td>
                  <td>{{ $d->qrcode_spl }}</td>
                  <td>{{ $d->qrcode_dc }}</td>
                  <td>{{ $d->status }}</td>
                  <td><span class="btn btn-primary btn-xs btnform" data-toggle="modal" data-target="#modal-info" data-id="{{ $d->id }}">Edit</span></td>
              </tr>
          @endforeach
          </tbody>
      </table>
  </div>
</div>
  <div id="modal-info" class="modal fade modal-alert modal-info">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">Edit Port <span class="label label-success port"></span></div>
        <form id="submit-form" method="post" autocomplete="off" action="/marina/validasiawal/{{ Request::segment(3) }}">
        <div id="detilContent" class="modal-body">
            <input name="id" type="hidden" id="vid" class="form-control"/>
            <div class="form-group">
              <label class="control-label" for="jenis_layanan">Jenis Layanan</label>
              <input name="jenis_layanan" type="text" id="jenis_layanan" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="no_layanan">No Layanan</label>
              <input name="no_layanan" type="text" id="no_layanan" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="qrcode_spl">QRCODE SPL</label>
              <input name="qrcode_spl" type="text" id="qrcode_spl" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="qrcode_dc">QRCODE DC</label>
              <input name="qrcode_dc" type="text" id="qrcode_dc" class="form-control"/>
            </div>
            <div class="form-group">
              <label class="control-label" for="status">Status</label>
              <input name="status" type="text" id="status" class="form-control"/>
            </div>
            <div style="margin:40px 0 20px">
              <button class="btn btn-primary">Simpan</button>
              <button class="btn btn-secondary pull-right" type="button" class="close" data-dismiss="modal">Tutup</button>
            </div>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!-- <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" /> -->
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
    $(function(){
      $('.btnform').click(function(){
        var vid = $(this).data("id");
        var url = "/marina/validasiGetJsonAwal/"+vid;
        $.getJSON(url, function(r){
          $('.port').html(r.port);
          $('#vid').val(r.id);
          $('#jenis_layanan').val(r.jenis_layanan).trigger("change");
          $('#no_layanan').val(r.no_layanan);
          $('#qrcode_spl').val(r.qrcode_spl);
          $('#qrcode_dc').val(r.qrcode_dc);
          $('#status').val(r.status).trigger("change");
        });
      });
      $('#jenis_layanan').select2({data:[{id:"Internet", text:"Internet"}, {id:"Telpon", text:"Telpon"}],allowClear:true, placeholder:"Pilih Jenis Layanan"});
      $('#status').select2({data:[{id:"IDLE", text:"IDLE"}, {id:"Teridentifikasi", text:"Teridentifikasi"}, {id:"Tidak Teridentifikasi", text:"Tidak Teridentifikasi"}, {id:"Rusak", text:"Rusak"}, {id:"Node-B", text:"Node-B"}, {id:"DATIN", text:"DATIN"}, {id:"WIFI.ID", text:"WIFI.ID"}]});
    });
  </script>

@endsection
