@extends('tech_layout')
@section('content')

<ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
  <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
  <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
  <li class="{{ (Request::segment(1) == 'ibooster') ? 'active' : '' }}"><a href="/ibooster">IBOOSTER</a></li>
  <li class="{{ (Request::segment(1) == 'searchbarcode') ? 'active' : '' }}"><a href="/searchbarcode">BARCODE</a></li>
  <li class="{{ (Request::segment(3) == 'history_reject QC') ? 'active' : '' }}"><a href="/viewreject">HISTORY REJECT QC</a></li>
  <li class="{{ (Request::segment(4) == 'splitter_gendong') ? 'active' : '' }}"><a href="/input-spl-gendong/form">SPLITTER GENDONG</a>
</ul>
<form method="POST" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Report ODP Splitter Gendong</div>
			<div class="panel-body">
				<div class="row">

				<div class="col-md-12">
					<div class="form-group">
						<label>Nama ODP</label>
						<input type="text" name="nama_odp" class="form-control" id="odp" value="{{ old('nama_odp') }}">
						{!! $errors->first('nama_odp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label>Koordinat ODP</label>
						<input type="text" name="koordinat_odp" class="form-control" value="{{ old('koordinat_odp') }}">
						{!! $errors->first('koordinat_odp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
      					<label class="control-label" for="input-splitter">Jenis Splitter</label>
      					<!-- <input name="splitter" type="hidden" id="input-splitter" class="form-control" value="{{ $data->splitter or '' }}" /> -->
      					<select class="form-control select2" name="splitter" id="input-splitter">
              			@foreach ($get_laporan_splitter as $laporan_splitter)
              			<option value="{{ $laporan_splitter->id }}" <?php if (@$laporan_splitter->id==@$data->splitter) { echo "Selected"; } else { echo ""; } ?>>{{ @$laporan_splitter->text }}</option>
                		@endforeach
              			</select>
    				</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label>Catatan</label>
						<input type="text" name="catatan_order" class="form-control" value="{{ old('catatan_order') }}">
						{!! $errors->first('catatan_order','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>
		
				<div class="col-md-6">	
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">	
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Dokumentasi Foto</div>
		<div class="panel-body">
			<div class="row">	
					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="belum ada foto" width="80"><br>ODP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_ODP" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_ODP','<span class="label label-danger">:message</span>') !!}
					</div>
	
					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="belum ada foto" width="80"><br>Kondisi ODP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_Kondisi_ODP" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_Kondisi_ODP','<span class="label label-danger">:message</span>') !!}
					</div>

					<div class="col-xs-2 col-md-1 text-center input-photos">
						<img src="/image/placeholder.gif" alt="belum ada foto" width="80"><br>Barcode ODP<br>
						<input type="text" class="hidden" name="" value=""/>
		                <input type="file" class="hidden" name="photo_Barcode_ODP" accept="image/jpeg" />
		                <button type="button" class="btn btn-sm btn-info ">
		                  <i class="glyphicon glyphicon-camera text-center"></i>
		                </button><br>
		                {!! $errors->first('photo_Barcode_ODP','<span class="label label-danger">:message</span>') !!}
					</div>

					

				</div>
			</div>
		</div>	
	</div>
</form>
@endsection

@section('plugins')
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.4-beta.33/jquery.inputmask.min.js"></script>
	<script>
		$(function(){

			$('input[type=file]').change(function() {
		        console.log(this.name);
		        var inputEl = this;
		        if (inputEl.files && inputEl.files[0]) {
		         	$(inputEl).parent().find('input[type=text]').val(1);
		          	var reader = new FileReader();
		          	reader.onload = function(e) {
		            	$(inputEl).parent().find('img').attr('src', e.target.result);
		          	}
		        	reader.readAsDataURL(inputEl.files[0]);
			    }
		    });

		    $('.input-photos').on('click', 'button', function() {
		    	$(this).parent().find('input[type=file]').click();
		    });

		    var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

      		$("#odp").inputmask("AAA-AAA-A{2,3}/999");


      // 		var dataSplitter = <?= json_encode($get_laporan_splitter) ?>;
      // 		var selectDataSplitter = function() {
      //   	return {
      //     	data: dataSplitter,
      //     	placeholder: 'Input Jenis Splitter',
      //     	formatSelection: function(data) { return data.text },
      //     	formatResult: function(data) {
      //       return  data.text;
      //     }
      //   }
      // }
      // $('#input-splitter').select2(selectDataSplitter());
		})
	</script>
@endsection