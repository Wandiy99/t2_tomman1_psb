@extends('layout')

@section('content')
  <div class="col-sm-16">
        <div class="panel panel-default">
            <div class="panel-heading">DASHBOARD SPLITTER GENDONG WITEL KALSEL</div>
                <div class="panel-body table-responsive" style="padding:0px !important">
                <table class="table">
                <tr align="center">
                    <th>NO</th>
                    <th>PENGIRIM</th>
                    <th>STO</th>
                    <th>DATEL</th>
                    <th>NAMA ODP</th>
                    <th>KOORDINAT</th>
                    <th>CATATAN</th>
                    <th>JENIS SPLITTER</th>
                    <th>WAKTU LAPORAN</th>
                    <th>ODP</th>
                    <th>KONDISI ODP</th>
                    <th>BARCODE ODP</th>
                </tr>
                @foreach ($query as $num => $result)
                <tr align="center">
                    <td>{{ ++$num }}</td>
                    <td>{{ $result->user_name }} / {{ $result->user }}</td>
                    <td>{{ $result->sto }}</td>
                    <td>{{ $result->datel }}</td>
                    <td>{{ $result->odp }}</td>
                    <td>{{ $result->k_odp }}</td>
                    <td>{{ $result->catatan }}</td>
                    <td>{{ $result->spl }}</td>
                    <td>{{ $result->updated_at }}</td>
                    <td>
                        <a href="/upload4/spl_odp/{{ $result->sg_id }}/ODP.jpg"><img src="/upload4/spl_odp/{{ $result->sg_id }}/ODP-th.jpg"></a>
                    </td>
                    <td>
                        <a href="/upload4/spl_odp/{{ $result->sg_id }}/Kondisi_ODP.jpg"><img src="/upload4/spl_odp/{{ $result->sg_id }}/Kondisi_ODP-th.jpg"></a>
                    </td>
                    <td>
                        <a href="/upload4/spl_odp/{{ $result->sg_id }}/Barcode_ODP.jpg"><img src="/upload4/spl_odp/{{ $result->sg_id }}/Barcode_ODP-th.jpg"></a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection