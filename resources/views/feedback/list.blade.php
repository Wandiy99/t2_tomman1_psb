@extends('layout')

@section('content')
  @include('partial.alerts')
      <style>
        .panel-content {
          padding : 10px;
        }
      </style>
      <h3> 
        <a href="/feedback/input" class="btn btn-info">
          <span class="glyphicon glyphicon-plus"></span>
        </a>
        Kritik dan Saran</h3>
      <div class="panel panel-primary">
        <div class="panel-heading">List</div>
        <div class="panel-content">
          <div class="table table-responsive">
            <table class="table table-responsive">
              <tr>
                <th>NO</th> 
                <th>JUDUL</th>  
                <th>CATATAN</th> 
                <th>USER</th> 
                <th>STATUS</th>
                <th>ADMIN</th>
                <th>BALASAN</th>
                <th>TERAKHIR UPDATE</th>   
                <th></th> 
              </tr>
              @foreach ($list as $num => $data)
              <tr>
                <td>{{ ++$num }}</td>
                <td>{{ $data->judul }}</td>
                <td>{{ $data->catatan }}</td>
                <td>
                  <span>{{ $data->e_nama }}</span><br />
                  <span>{{ $data->user_created }}</span>
                </td>
                <td>
                  @if($data->feedback_status_id=="1")
                  <span class="label label-success">{{ $data->feedback_status_id }} // {{ $data->feedback_status }}</span>
                  @elseif($data->feedback_status_id=="3")
                  <span class="label label-info">{{ $data->feedback_status_id }} // {{ $data->feedback_status }}</span>
                  @elseif($data->feedback_status_id=="4")
                  <span class="label label-warning">{{ $data->feedback_status_id }} // {{ $data->feedback_status }}</span>
                  @elseif($data->feedback_status_id=="2")
                  <span class="label label-danger">{{ $data->feedback_status_id }} // {{ $data->feedback_status }}</span>
                  @else
                  <span class="label label-default">{{ $data->feedback_status ? : 'BELUM DIKONFIRMASI' }}</span>
                  @endif<br />
                  <a class="label label-info" href="/feedback/{{ $data->f_id }}"> E D I T</a>
                </td>
                <td>
                  <span>{{ $data->fat_nama }}</span><br />
                  <span>{{ $data->admin_by }}</span>
                </td>
                <td>{{ $data->balasan }}</td>
                <td>{{ $data->last_updated }}</td>
                
                
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
  
@endsection