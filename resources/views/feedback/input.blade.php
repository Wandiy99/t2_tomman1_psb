@extends('layout')

@section('content')
<style>
.panel-info>.panel-heading {
    color: #ffffff;
    background-color: #19a2d6;
    border-color: #cba4dd;
}
</style>
  @include('partial.alerts')
  @if ( session('auth')->level == 15 || session('auth')->level == 2 )
  <div class="row" style="padding:10px">
  <div class="col-sm-12">
  <div class="panel panel-default">
  <center><div class="panel-heading">Feedback</div></center>
  <div class="panel-body">
    
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    @if (isset($data->id))
      <input type="hidden" name="id" value="{{ $data->f_id }}" />
    @endif
    <h3>
      <a href="/feedback" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Masukan Kritik dan Saran Rekan
    </h3>
    <div class="form-group">
      <label class="control-label" for="input-judul"><b>Judul</b></label>
      <input name="judul" type="text" id="input-judul" class="form-control" value="{{ $data->judul ?: old('judul') }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-catatan"><b>Catatan</b></label><br />
      <textarea id="input-catatan" name="catatan" class="form-control" rows="4">{{ $data->catatan }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-user"><b>Permintaan Dari</b></label>
      <input name="user_created" type="hidden" id="input-user" class="form-control" value="{{ $data->user_created or '' }}" />
    </div>
    <br />
    @endif
    @if ( session('auth')->id_user == '91153709' || session('auth')->id_user == '20981020' || session('auth')->id_user == '200297' )
    <br /><br /><br />
    <div class="form-group">
      <label class="control-label" for="input-admin"><b>Admin</b></label>
      <input name="admin_by" type="hidden" id="input-admin" class="form-control" value="{{ $data->admin_by or '' }}" />
    </div>

    <div class="form-group">
      <label class="control-label" for="input-status-admin"><b>Status</b></label>
      <input name="status" type="hidden" id="input-status-admin" class="form-control" value="{{ $data->status ?: old('status') }}" />
    </div>

    <div class="form-group">
      <label class="control-label" for="input-balasan"><b>Balasan</b></label><br />
      <textarea id="input-balasan" name="balasan" class="form-control" rows="4">{{ $data->balasan }}</textarea>
    </div>
    @endif
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
</div>
</div>
</div>
</div>  
@endsection

@section('plugins')
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(function(){

          var dataStatusAdmin = <?= json_encode($get_status_admin) ?>;
          var selectDataStatusAdmin = function() {
          return {
            data: dataStatusAdmin,
            placeholder: 'Pilih Status Balasan',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status-admin').select2(selectDataStatusAdmin());

      var dataAdmin = <?= json_encode($get_admin) ?>;
          var selectDataAdmin = function() {
          return {
            data: dataAdmin,
            placeholder: 'Admin yang Bertugas',
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-admin').select2(selectDataAdmin());

      var dataUser = <?= json_encode($get_user) ?>;
          var selectDataUser = function() {
          return {
            data: dataUser,
            placeholder: 'Pilih Username Anda',
            allowClear: true,
            minimumInputLength: 1,
            escapeMarkup: function(m) { return m },
            formatSelection: function(data) { return data.text },
            formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-user').select2(selectDataUser());

    })
  </script>
@endsection