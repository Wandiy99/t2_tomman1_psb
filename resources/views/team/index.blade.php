@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0"><a href="/team/input" class="btn btn-info">
          <span class="glyphicon glyphicon-plus"></span>
        </a> TEAM</h3>
        <p class="text-muted m-b-20">Create & Edit Your Team Information</p>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">NAMA TIM</th>
                        <th style="text-align: center;">MITRA</th>
                        <th style="text-align: center;">SEKTOR</th>
                        <th style="text-align: center;">TEKNISI 1</th>
                        <th style="text-align: center;">TEKNISI 2</th>
                        <th style="text-align: center;">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getregu as $data)                
                    <tr>
                        <td style="text-align: center;">{{ $data->id_regu }}</td>
                        <td style="text-align: center;">{{ $data->uraian }}</td>
                        <td style="text-align: center;">{{ $data->mitra_amija_pt }}</td>
                        <td style="text-align: center;">{{ $data->mainsector }}</td>
                        <td style="text-align: center;">{{ $data->nik1 ? : 'NIK1' }} / {{ $data->nama_nik1 ? : '-' }}</td>
                        <td style="text-align: center;">{{ $data->nik2 ? : 'NIK2' }} / {{ $data->nama_nik2 ? : '-' }}</td>
                        <td style="text-align: center;">
                          <a href="/team/{{ $data->id_regu }}" data-original-title="Edit Team">
                            <i class="fa fa-edit text-inverse m-r-10"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
        });
    });
</script>
@endsection