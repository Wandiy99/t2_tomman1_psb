@extends('layout')

@section('content')
  @include('partial.alerts')
	<style>
		.panel-content {
			padding : 10px;
		}
	</style>
  <h3>
    <a href="/team" class="btn btn-info">
      <span class="glyphicon glyphicon-home"></span>
    </a>
    Team
  </h3>

	<div class="panel panel-primary">
		<div class="panel-heading">Input</div>
		<div class="panel-body">
			<form method="post">
			<input type="hidden" name="id_regu" value="{{ $data->id_regu }}" />

			<div class="form-group col-md-4">
				<label for="uraian">Group Name</label>
				<input type="text" name="uraian" id="uraian" class="form-control" value="{{ $data->uraian }}" />
			</div>

			<div class="form-group col-md-4">
				<label for="uraian">Ket Bantek</label>
				<input type="text" name="ket_bantek" id="ket_bantek" class="form-control" value="{{ $data->ket_bantek }}" />
			</div>

			<div class="form-group col-md-4">
				<label for="sto">STO</label>
				<select name="sto" class="form-control">
					<option value="" selected disabled> Pilih STO </option>
					@foreach ($get_sto_regu as $sto_regu)
					<?php if ($sto_regu->id == $data->sto) $selected = "selected='selected'"; else $selected = ""; ?>
					<option value="{{ $sto_regu->id }}" {{ $selected }}>{{ $sto_regu->text }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="status">Status</label>
				<select name="mitra" class="form-control">
					<option value="" selected disabled> Pilih Status </option>
					@foreach ($getStatus as $status)
					<?php if ($status->id == $data->mitra) $selected = "selected='selected'"; else $selected = ""; ?>
					<option value="{{ $status->id }}" {{ $selected }}>{{ $status->text }}</option>
					@endforeach
				</select>
			</div>

			<!-- <div class="form-group">
				<label for="status_mitra">Status Mitra</label>
				<select name="nama_mitra" class="form-control">
					<option value="" selected disabled> Pilih Status Mitra </option>
					@foreach ($mitra_Maintenance as $mtrMNT)
					<?php if ($mtrMNT->id == $data->nama_mitra) $selected = "selected='selected'"; else $selected = ""; ?>
					<option value="{{ $mtrMNT->id }}" {{ $selected }}>{{ $mtrMNT->text }}</option>
					@endforeach
				</select>
			</div> -->

			<div class="form-group col-md-4">
				<label for="status">Manhours</label>
				<select class="form-control" name="manhours">
				<option value=""> -- </option>
				<option value="8" <?php if ($data->kemampuan==8) { echo "selected='selected'"; } else {  } ?> > 8 MH </option>
				<option value="16" <?php if ($data->kemampuan==16) { echo "selected='selected'"; } else {  } ?>> 16 MH </option>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="mainsector">Main Sector</label>
				<select name="mainsector" id="mainsector" class="form-control">
					<option value=""> -- </option>
					@foreach ($mainsector as $r_mainsector)
					<?php if ($r_mainsector->chat_id==$data->mainsector) { $select = 'selected="selected"'; } else { $select = ''; } ?>
					<option value="{{ $r_mainsector->chat_id }}" {{ $select }}>{{ $r_mainsector->title  }}</option>
					@endforeach
				</select>
			</div>

			<!-- <div class="form-group">
			<label class="label label-warning">IOAN</label>
				<label for="area">ODC IOAN</label>
				<input type="text" name="area" id="area" class="form-control" value="{{ $data->area }}" />
			</div>

			<div class="form-group">
			<label class="label label-warning">IOAN</label>
				<label for="fitur">Fitur</label>
				<select name="fitur" id="fitur" class="form-control">
					<option value="{{ $data->fitur }}"> {{ $data->fitur ? : 'Pilih Fitur' }} </option>
					<option value="ACTIVE"> ACTIVE </option>
					<option value="NONACTIVE"> NONACTIVE </option>
				</select>
			</div> -->

			<div class="form-group col-md-4">
				<label for="nik1">CREW ID</label>
				<input type="text" name="crewid" id="crewid" class="form-control" value="{{ $data->crewid }}" />
			</div>

			<div class="form-group col-md-4">
				<label for="nik1">NIK 1</label>
				<input type="text" name="nik1" id="nik1" class="form-control" value="{{ $data->nik1 }}" />
			</div>

			<div class="form-group col-md-4">
				<label for="nik1">NIK 2</label>
				<input type="text" name="nik2" id="nik2" class="form-control" value="{{ $data->nik2 }}" />
			</div>

			<div class="form-group col-md-4">
				<label for="nik1">STATUS</label>
				<select name="status_team" class="form-control">
					<option value=""> -- </option>
					@foreach ($get_status_team as $status_team)
					<?php
						if ($data->status_team==$status_team->status_team) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
					?>
					<option value="{{ $status_team->status_team }}" <?php echo $selected ?>>{{ $status_team->status_team }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="status_alker">STATUS ALKER</label>
				<select name="status_alker" class="form-control">
					<option value=""> -- </option>
					@foreach ($get_status_alker as $status_alker)
					<?php
						if ($data->status_alker==$status_alker->status_alker) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
					?>
					<option value="{{ $status_alker->status_alker }}" <?php echo $selected ?>>{{ $status_alker->status_alker }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="spesialis_id_regu">REGU MARINA</label>
				<select name="spesialis_id_regu" class="form-control">
					<option value=""> -- </option>
					@foreach ($get_team_prov_marina as $prov_marina)
					<?php
						if ($data->spesialis_id_regu == $prov_marina->id) {
							$selected = 'selected = "selected"';
						} else {
							$selected = '';
						}
					?>
					<option value="{{ $prov_marina->id }}" <?php echo $selected ?>>{{ $prov_marina->text }}</option>
					@endforeach
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<input type="submit" name="submit" id="submit" class="btn btn-success" />
				@if ($data->id_regu <> "")
				<a href="/team/disable/{{ $data->id_regu }}" class="btn btn-warning" style="color:white">Disable</a>
				<!-- <a href="/team/delete/{{ $data->id_regu }}" class="btn btn-danger" style="color:white">Delete</a> -->
				@endif
			</div>

			</form>
		</div>
	</div>
@endsection