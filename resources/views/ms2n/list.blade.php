@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    
    Ms2N
  </h3>

  <div class="list-group">
    @foreach($list as $no => $data)

      <a href="/ms2n/{{ $data->s }}" class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
	      <strong>{{ $data->s }}</strong>
	      
      </a>
    @endforeach
  </div>
  
@endsection
