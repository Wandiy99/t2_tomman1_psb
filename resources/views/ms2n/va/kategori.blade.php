@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/ms2n" class="btn btn-info">
      <span class="glyphicon glyphicon-arrow-left"> Back</span>
    </a>
    VAMS2N 
    <a href="/ms2n/sync/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-refresh"> Sync Now</span>
    </a>
    <br /><small>( Sync at {{ $lastSync }} )</small>
  </h3>
	@foreach($list as $no => $data)
		@if ($no==0) 
		KENDALA
		<div class="list-group">
	  	
   	@elseif ($no==7) 
    POTENSI
		<div class="list-group">
	  
	  @endif
      <a href="/ms2n/VA/{{ $data->s }}" class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
	     <strong>{{ $data->s }}</strong><br />
	     
       <span class="label label-info badge">{{ $data->jumlah }}</span>
      </a>
  @endforeach
  </div>
  
@endsection
