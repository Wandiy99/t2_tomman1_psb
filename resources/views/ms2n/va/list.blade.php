@extends('layout')

@section('content')
  @include('partial.alerts')

  <h3>
    <a href="/ms2n/VA" class="btn btn-info">
      <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
    VA Ms2N {{ $id }}
  </h3>

  <div class="list-group">
    @foreach($list as $no => $data)
      <a href="/ms2n/id/{{ $data->Ndem }}" class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
       <strong>{{ $data->Nama }}</strong>
      </a>
    @endforeach
  </div>
  
@endsection
