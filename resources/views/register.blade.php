<!DOCTYPE html>
<html lang="en">
<head>
<title>BIAWAK</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Register and Join Us | TOMMAN">
<meta name="keywords" content="psb bjm tomman app,register tomman app,register tomman,tomman app,telkomakses tomman,tomman telkom,tomman banjarmasin,tomman kalsel,telkomakses,warrior telkom akses,telkomakses banjarmasin,telkomakses kalsel,tomman portal,tomman telkomakses,tomman telkom akses,telkomakses tomman,kalsel hibat,tomman kalsel hibat,banjarmasin,kalimatan selatan,indihome,tomman indihome">
<meta name="copyright" content="biawak.tomman.app"/>
<meta name="author" content="Register - TOMMAN">
<link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/tomman-logo.png">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/fonts/iconic/css/material-design-iconic-font.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/animate.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/animsition.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/select2.min.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/util.css">
<link rel="stylesheet" type="text/css" href="/bower_components/vendor/main.css">

</head>
<body>
<div class="limiter">
<div class="container-register100">
<div class="wrap-register100">
@include('partial.alerts')
<form method="post" enctype="multipart/form-data">
<span class="login100-form-title p-b-26">
Register Now
</span><br />
<div class="wrap-input100 validate-input">
<input class="input100" type="text" name="username">
<span class="focus-input100" data-placeholder="NIK"></span>
{!! $errors->first('username','<p><span class="label label-danger">:message</span></p>') !!}
</div>

<div class="wrap-input100 validate-input" data-validate="Enter password">
<span class="btn-show-pass">
<i class="zmdi zmdi-eye"></i>
</span>
<input class="input100" minlength="8" type="password" name="password">
<span class="focus-input100" data-placeholder="Password"></span>
{!! $errors->first('password','<p><span class="label label-danger">:message</span></p>') !!}
</div>

<div class="wrap-input100 validate-input">
<input class="input100" type="text" name="name">
<span class="focus-input100" data-placeholder="Name"></span>
{!! $errors->first('name','<p><span class="label label-danger">:message</span></p>') !!}
</div>

<div class="wrap-input100 validate-input">
<input class="input100" type="text" name="atasan1">
<span class="focus-input100" data-placeholder="NIK Leader Relation"></span>
{!! $errors->first('atasan1','<p><span class="label label-danger">:message</span></p>') !!}
</div>

<div class="validate-input" id="dropDownSelect1">
<select class="form-control" name="level">
<option value="" selected disabled>Choice a Work Position</option>
<option value="2"> Site Manager </option>
<option value="15"> Team Leader </option>
<option value="15"> Helpdesk / Admin </option>
<option value="10"> Technician </option>
<option value="59"> Customer Service Plasa </option>
<option value="61"> Sales Forces </option>
</select>
{!! $errors->first('level','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<br />

<div class="validate-input" id="dropDownSelect1">
<select class="form-control" name="mitra">
<option value="" selected disabled>Choice a Mitra</option>
@foreach ($get_mitra as $mitra)
<option value="{{ $mitra->id }}">{{ $mitra->text }} ( {{ $mitra->kat }} )</option>
@endforeach
</select>
{!! $errors->first('mitra','<p><span class="label label-danger">:message</span></p>') !!}
</div>
<br />

<div class="container-register100-form-btn">
<div class="wrap-register100-form-btn">
<div class="register100-form-bgbtn"></div>
<button class="register100-form-btn">
Sign Up
</button>
</div>
</div>
<div class="text-center p-t-115">
<span class="txt1">
You already have an account ?
</span>
<a class="txt2" href="/login">
Login
</a>
</div>
</form>
</div>
</div>
</div>

<script src="/bower_components/vendor/jquery-3.2.1.min.js"></script>
<script src="/bower_components/vendor/animsition.min.js"></script>
<script src="/bower_components/vendor/popper.js"></script>
<script src="/bower_components/vendor/bootstrap.min.js"></script>
<script src="/bower_components/vendor/select2.min.js"></script>
<script src="/bower_components/vendor/moment.min.js"></script>
<script src="/bower_components/vendor/daterangepicker.js"></script>
<script src="/bower_components/vendor/countdowntime.js"></script>
<script src="/bower_components/vendor/main.js"></script>

</body>
</html>