@extends('layout')

@section('content')
@include('partial.alerts')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin=""/>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
<style>
    #map {
        height: 600px;
    }

    .custom-popup {
        background-color: #fff;
        border: 1px solid #ccc;
        padding: 10px;
        border-radius: 5px;
    }

    tr, th, td {
        text-align: center;
    }

    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 2px 1px;
    }
</style>
<h2>MAP Order Provisioning Kalsel</h2>

<form method="GET">
    <div class="row">
        <div class="col-sm-5">
            <select class="form-control select2" id="input-sektor" name="sektor">
                <option disabled>Pilih Sektor</option>
                <option value="ALL">ALL</option>
                @foreach ($get_sektor as $sektor)
                <option value="{{ $sektor->title }}">{{ $sektor->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-5">
            <input type="date" name="date" id="date" class="form-control" value="{{ $date }}" style="height: 30px">
        </div>

        <div class="col-sm-2">
            <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
        </div>
    </div>
</form>

<div class="col-sm-12">
    <label for="search">&nbsp;</label>
</div>

<div class="col-sm-12">
    <div id="map" class="map_box"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.select2').select2();

    // Inisialisasi peta
    var map = L.map("map"); // Tidak perlu setView di sini

    // Tambahkan lapisan Jalan
    var streetLayer = L.tileLayer(
    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
        attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }
    );

    // Tambahkan lapisan Satelit
    var satelliteLayer = L.tileLayer(
    "https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
    {
        maxZoom: 20,
        subdomains: ["mt0", "mt1", "mt2", "mt3"],
        attribution:
        'Map data © <a href="https://www.google.com/maps">Google Maps</a>',
    }
    );

    // Tambahkan lapisan Jalan & Satelit (Layer Control)
    var baseLayers = {
    Jalan: streetLayer,
    Satelit: satelliteLayer,
    "Jalan & Satelit": L.layerGroup([streetLayer, satelliteLayer]),
    };

    // Tambahkan lapisan Teknisi dan Pelanggan
    var teknisiLayer = L.layerGroup(); // Diisi dengan data Teknisi
    var pelangganLayer = L.layerGroup(); // Diisi dengan data Pelanggan

    // Tambahkan data Teknisi (Contoh)
    var teknisiData = {!! json_encode($data_teknisi) !!};

    // Tambahkan data Pelanggan (Contoh)
    var pelangganData = {!! json_encode($data_pelanggan) !!};

    // Membuat ikon untuk Teknisi
    var teknisiIcon = L.icon({
        iconUrl: "/image/technician.png", // Ganti dengan URL ikon Teknisi
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32],
    });

    // Membuat ikon untuk Pelanggan
    var pelangganIcon = L.icon({
        iconUrl: "/image/home_customer.png", // Ganti dengan URL ikon Pelanggan
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32],
    });

    // Tambahkan marker Teknisi ke lapisan Teknisi
    teknisiData.forEach(function (teknisi) {
        var marker = L.marker([teknisi.latitude, teknisi.longitude], {
            icon: teknisiIcon,
        }).bindPopup(
            `<div class="custom-popup table-responsive">
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td style="font-size: 10px">Teknisi</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.nama} ${teknisi.nik}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Tim</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.tim}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Sektor</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.sektor}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Latitude</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.latitude}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Longitude</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.longitude}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Accuracy</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.myLastAccuracy ?? '-'} Meter</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Posisi Terakhir</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${teknisi.myLastCoordUpdate} WITA</td>
                        </tr>
                    </tbody>
                </table>
            </div>`
        );
        teknisiLayer.addLayer(marker);
    });

    // Tambahkan marker Pelanggan ke lapisan Pelanggan
    pelangganData.forEach(function (pelanggan) {
        var marker = L.marker([pelanggan.latitude, pelanggan.longitude], {
            icon: pelangganIcon,
        }).bindPopup(
            `<div class="custom-popup table-responsive">
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td style="font-size: 10px">Order ID</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.order_id}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Nama</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.orderName}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Order Date</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.orderDate}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Order Date PS</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.orderDatePs}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Order Status</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.orderStatus}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Jenis PSB</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.jenisPsb}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Status Teknisi</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.status_teknisi} |  ${pelanggan.tgl_laporan}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">STO</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.sto}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Tim</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.tim}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Sektor</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.sektor}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px">Koordinat</td>
                            <td style="font-size: 10px">:</td>
                            <td style="font-size: 10px">${pelanggan.latitude}, ${pelanggan.longitude}</td>
                        </tr>
                    </tbody>
                </table>
            </div>`
        );
        pelangganLayer.addLayer(marker);
    });

    // Tambahkan lapisan Teknisi dan Pelanggan ke peta
    map.addLayer(teknisiLayer);
    map.addLayer(pelangganLayer);

    // Tambahkan lapisan default (Jalan)
    streetLayer.addTo(map);

    // Koordinat tengah Kalimantan Selatan
    var centerKalimantanSelatan = [-3.31987, 114.59057];

    // Set tampilan peta ke pusat Kalimantan Selatan dengan zoom level yang sesuai
    map.setView(centerKalimantanSelatan, 12); // Anda bisa menyesuaikan zoom level sesuai kebutuhan Anda

    // Tambahkan kontrol lapisan
    L.control
    .layers(baseLayers, {
        Teknisi: teknisiLayer,
        Pelanggan: pelangganLayer,
    })
    .addTo(map);
});
</script>
@endsection