@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .label {
      font-size: 12px;
    }
    th {
      border-color: #34495e;
      background-color: #7f8c8d;
      color : #ecf0f1;
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
    }
    td {
      text-align: center;
      vertical-align: middle;
      text-transform: uppercase;
    }
    .warna1 {
      background-color: #2874A6;
    }
    .warna2 {
      background-color: #2E86C1;
    }
    .warna3 {
      background-color: #D6EAF8;
    }
    .warna4 {
      background-color: #E67E22;
      #F0B27A
    }
    .warna4x {
      background-color: #F0B27A;
      #FAE5D3
    }
    .warna4z {
      background-color: #FAE5D3;
    }
    .colorArea {
      background-color: #F4D03F;
    }
    .colorArea2 {
      background-color: #FCF3CF;
    }
    .colorArea3 {
      background-color: #F7DC6F;
    }
    .colorComparin {
      background-color: #48C9B0;
    }
    .colorComparin2 {
      background-color: #D1F2EB;
    }
    .colorComparin3 {
      background-color: #76D7C4;
    }
    .text2 {
      color: black !important;
    }
    .text1 {
      color: white !important;
    }
    .text1:link {
      color: white !important;
    }
    .text1:visited {
      color: white !important;
    }
    th a:link {
      color: white;
    }
    th a:visited {
      color: white;
    }
    .overload {
      background-color: red;
      color: white;
    }
    .merahx {
      background-color: #C0392B;
      color: white !important;
    }
    .hijaux {
      background-color:#1ABC9C;
      color: white !important;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 4px 0px !important;
    }
    .color-green {
      background-color: #1ABC9C;
    }
    .color-black {
      background-color: #2C3E50;
    }
    .color-red {
      background-color: #E74C3C;
    }
    .color-yellow {
      background-color: #F39C12;
    }
    .color-blues {
      background-color: #154360;
    }
    .txt-white {
        color: white !important;
    }
  </style>
  <h3>Dashboard Monitoring</h3>
  <div class="row">
    <div class="col-sm-12 table-resposive">
      <table class="table table-bordered dataTable" width=100%>
        <tr>
          <th class="align-middle colorArea text1" rowspan="2">RO</th>
          <th colspan="4">PT1 AO</th>
          <th colspan="4">SABER</th>
          <th colspan="9">PT2</th>
        </tr>
        <tr>
          <th width="80">SISA</th>
          <th width="80">OGP</th>
          <th width="80">PS</th>
          <th width="80">KENDALA</th>
          <th>SISA ORDER</th>
          <th>OGP</th>
          <th>LANJUT PT1</th>
          <th>KENDALA</th>
          <th>SISA ORDER</th>
          <th>OGP</th>
          <th>SELESAI FISIK</th>
          <th>SDI</th>
          <th>GOLIVE</th>
          <th>LANJUT PT1</th>
          <th>KENDALA</th>
          <th>PT3</th>
          <th>PT2 Plus</th>
        </tr>
        <?php
          $total_PS = 0;
          $total_sisa_wo = 0;
          $total_kendala = 0;
          $total_ogp = 0;
        ?>
        @foreach ($get_ro as $result)
        <?php
          $total_PS = $total_PS + $result->jumlah_PS;
          $total_sisa_wo = $total_sisa_wo + $result->sisa_wo;
          $total_kendala = $total_kendala + $result->kendala;
          $total_ogp = $total_ogp + $result->ogp;
        ?>
        <tr>
          <td>{{ $result->HERO }}</td>
          <td>{{ $result->sisa_wo }}</td>
          <td>{{ $result->ogp }}</td>
          <td>{{ $result->jumlah_PS }}</td>
          <td>{{ $result->kendala }}</td>
        </tr>
        @endforeach
        <td>TOTAL</td>
        <td>{{ $total_sisa_wo }}</td>
        <td>{{ $total_ogp }}</td>
        <td>{{ $total_PS }}</td>
        <td>{{ $total_kendala }}</td>
      </table>
    </div>
  </div>
@endsection
