@extends('layout')

@section('content')
  @include('partial.alerts')
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <style>
  /* .marker {
   background-size: cover;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
  }
  */
  .mapboxgl-popup {
    font-size: 12px;
  }
  .maps_box{
    border : 1px solid black;
    width: 100%;
    height: 100%;
  }
  </style>
  <h2>Operation Kalsel</h2>
  <form method=post>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-area" name="area">
            <option disabled>Pilih Sektor</option>
            <option value="ALL">ALL</option>
            @foreach ($get_area as $result)
            <?php $selected = ''; if ($result->id==$sektor) { $selected = 'selected'; } ?>
            <option value="{{ $result->id }}" <?php echo $selected ?>>{{ $result->text }}</option>
            @endforeach
          </select>
        </div>

        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-order" name="order">
            <option disabled>Jenis Order</option>
            <option value="ALL">ALL</option>
            <option value="NEW SALES">NEW SALES</option>
            <option value="MO">MO</option>
            <option value="WMS">WMS</option>
          </select>
        </div>

        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-segment" name="segment">
            <option disabled>Segment</option>
            <option value="ALL">ALL</option>
          </select>
        </div>

        <div class="col-sm-2">
              <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
        </div>
      </div>
    </div>
  </form>
    <div class="col-sm-12">
      <label for="search">&nbsp;</label>

    </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-sm-12">
        <div class="white-box">
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">KEHADIRAN TEKNISI</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-info">{{ $jumlah_teknisi }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">TOTAL ORDER PI</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-info">{{ count($starclick_ao_pi) }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">FWFM</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">{{ count($starclick_ao_fwfm) }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">ACTCOMP</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-info">{{ count($starclick_ao_actcomp) }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">PS</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-success">{{ count($starclick_ao_ps) }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>

        </div>
        </div>
      </div>
  </div>
    <div class="col-sm-9">
      <div id="map" class="maps_box" height="100%"></div>
    </div>
  </div>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  $('.select2').select2();


  var data = [{"id":"NEW SALES","text":"NEW SALES"},{"id":"MO","text":"MO"},{"id":"PDA","text":"PDA"},{"id":"WMS","text":"WMS"}];
  var selectOrder = function() {
  return {
    data: data,
    placeholder: 'Jenis Order',
    formatSelection: function(data) { return data.text },
    formatResult: function(data) {
        return  data.text;
    }
    }
  }
  $('#input-type-order').select2(selectOrder());


});
</script>
<script>
var center = { lng: 114.594376, lat: -3.318607 };
	var map = L.map('map').setView(center, 13);
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);
  var redRedIcon = new L.icon({
          iconUrl: 'https://tacticalpro.co.id/public/asset_maps/red_red_teknisi.png',
          iconSize: [35, 35]
      }, );
      var homeIcon = new L.icon({
              iconUrl: '/image/home.png',
              iconSize: [24, 24]
          }, );
      <?php
        foreach ($query as $num => $data) {
          ?>
  	       var tech = L.marker(['{{ $data->lat ? : 0 }}','{{ $data->lon ? : 0 }}'], {icon: redRedIcon}).bindPopup('{{ $data->regu_nama }}<br />{{ $data->modified_at }}<br />last Order : <a href="#">{{ $data->NO_ORDER }}</a>').addTo(map);
          <?php
        }
      ?>
      <?php
        foreach ($starclick_ao_pi as $num => $data) {
          ?>
          var home = L.marker(['{{ $data->lat ? : 0 }}','{{ $data->lon ? : 0 }}'], {icon: homeIcon}).bindPopup('SC : {{ $data->orderId }}<br />Nama : {{ $data->orderName }}<br />OrderDate : {{ $data->orderDate }}<br />JenisOrder : {{ $data->jenisPsb }}<br />Dispatch to : {{ $data->uraian ? : "NOT YET" }}').addTo(map);

      <?php
      } ?>
</script>


@endsection
