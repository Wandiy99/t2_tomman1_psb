<!DOCTYPE html>
<html lang="en-US">

    <head>
    <style>
    p {
    text-align: center;
    font-size: 60px;
    margin-top: 0px;
    }
    </style>
        <meta charset="utf-8">
        <link rel="icon" type="image/png" sizes="16x16" href="/elitetheme/plugins/images/favicon.png">
        <title>
            Closing Soon | BIAWAK
        </title>
        <meta content="yes" name="apple-mobile-web-app-capable">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <!-- CSS -->
        <link href="https://technext.github.io/Imminent/css/loader.css" rel="stylesheet" type="text/css">
        <link href="https://technext.github.io/Imminent/css/normalize.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://technext.github.io/Imminent/css/font-awesome.min.css">
        <link href="https://technext.github.io/Imminent/css/style.css" rel="stylesheet" type="text/css">

        <script src="https://technext.github.io/Imminent/js/jquery.js"></script>
    </head>
    <body>
        <div class="preloader">
            <div class="loading">
                <h2>
                    Loading...
                </h2>
                <span class="progress"></span>
            </div>
        </div>

        <div class="wrapper">
            <ul class="scene unselectable" data-friction-x="0.1" data-friction-y="0.1" data-scalar-x="25" data-scalar-y="15" id="scene">
                <li class="layer" data-depth="0.00">
                </li>
                <!-- BG -->

                <!-- <li class="layer" data-depth="0.10">
                    <div class="background">
                    </div>
                </li> -->


                <!-- Hero -->

                <li class="layer" data-depth="0.20">
                    <div class="title">
                        <h2>
                            CLOSING SOON
                        </h2>
                        <span class="line"></span>
                    </div>
                </li>

                <li class="layer" data-depth="0.25">
                    <div class="sphere">
                        <img alt="sphere" src="https://technext.github.io/Imminent/images/sphere.png">
                    </div>
                </li>

                <li class="layer" data-depth="0.30">
                    <div class="hero">
                        <!-- <h1 id="countdown">
                        
                        </h1> -->
                        <p id="demo"></p>
                        

                        <p class="sub-title">
                        Please wait ...
                        </p>
                    </div>
                </li>
                <!-- Flakes -->

                <li class="layer" data-depth="0.40">
                    <div class="depth-1 flake1">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth1/flakes1.png">
                    </div>

                    <div class="depth-1 flake2">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth1/flakes2.png">
                    </div>

                    <div class="depth-1 flake3">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth1/flakes3.png">
                    </div>

                    <div class="depth-1 flake4">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth1/flakes4.png">
                    </div>
                </li>

                <li class="layer" data-depth="0.50">
                    <div class="depth-2 flake1">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth2/flakes1.png">
                    </div>

                    <div class="depth-2 flake2">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth2/flakes2.png">
                    </div>
                </li>

                <li class="layer" data-depth="0.60">
                    <div class="depth-3 flake1">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth3/flakes1.png">
                    </div>

                    <div class="depth-3 flake2">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth3/flakes2.png">
                    </div>

                    <div class="depth-3 flake3">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth3/flakes3.png">
                    </div>

                    <div class="depth-3 flake4">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth3/flakes4.png">
                    </div>
                </li>

                <li class="layer" data-depth="0.80">
                    <div class="depth-4">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth4/flakes.png">
                    </div>
                </li>

                <li class="layer" data-depth="1.00">
                    <div class="depth-5">
                        <img alt="flake" src="https://technext.github.io/Imminent/images/flakes/depth5/flakes.png">
                    </div>
                </li>
                <!-- Contact -->

                <li class="layer" data-depth="0.20">
                    <div class="contact">
                        <!-- <ul class="icons">
                            <li>
                                <a class="behance" href="#"><i class="fa fa-behance"></i></a>
                            </li>

                            <li>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                            </li>

                            <li>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                            </li>
                        </ul> -->
                        <!-- Contact Me with <a href="https://t.me/bigb00s" target="_blank">Telegram</a> -->
                        Tomman Creators Inovations © 2017 - {{ date('Y') }}
                    </div>
                </li>
            </ul>
        </div>

        <!-- Javascript -->
        <script src="https://technext.github.io/Imminent/js/plugins.js"></script>
        <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Oct 1, 2021 08:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();
            
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
            
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = days + " Day " + hours + " Hours "
        + minutes + " Minute " + seconds + " Sec";
            
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "I WILL BE BACK, SEE U";
        }
        }, 1000);
        </script> 
        <!-- <script src="/js/jquery.countdown.min.js"></script>  -->
        <script src="https://technext.github.io/Imminent/js/main.js"></script>

    </body>
</html>