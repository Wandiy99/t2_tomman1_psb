@extends('layout')
@section('content')

<form method="POST" enctype="multipart/form-data">
	<h4><span class='label label-success'>Manual Starclick by SC</span></h4>
	<div class="panel panel-primary">
		<div class="panel-heading">Masukan Data Pelanggan sesuai Data Starclick</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>SC</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="orderId" class="form-control" value="{{ $data->orderId ?: old('orderId') }}" required/>
						{!! $errors->first('orderId','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NAMA CUSTOMER</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="orderName" class="form-control" value="{{ $data->orderName ?: old('orderName') }}" required/>
						{!! $errors->first('orderName','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>ADDRESS</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="orderAddr" class="form-control" value="{{ $data->orderAddr ?: old('orderAddr') }}">
						{!! $errors->first('orderAddr','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>ORDER DATE</label>
						<input type="text" name="orderDate" class="form-control" value="{{ $data->orderDate ?: old('orderDate') }}">
						{!! $errors->first('orderDate','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>ORDER DATE PS</label>
						<input type="text" name="orderDatePs" class="form-control" value="{{ $data->orderDatePs ?: old('orderDatePs') }}">
						{!! $errors->first('orderDatePs','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>CUSTOMER ADDRESS CITY</label>
						<input type="text" name="orderCity" class="form-control" value="{{ $data->orderCity ?: old('orderCity') }}">
						{!! $errors->first('orderDatePs','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>STATUS ORDER</label>
						<input type="text" name="orderStatus" class="form-control" value="{{ $data->orderStatus ?: old('orderStatus') }}">
						{!! $errors->first('orderStatus','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

				<div class="col-md-6">
					<div class="form-group">
						<label>NCLI</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="orderNcli" class="form-control" value="{{ $data->orderNcli ?: old('orderNcli') }}">
						{!! $errors->first('orderNcli','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>ND-INET</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="ndemSpeedy" class="form-control" value="{{ $data->ndemSpeedy ?: old('ndemSpeedy') }}">
						{!! $errors->first('ndemSpeedy','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>ND-VOICE</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="ndemPots" class="form-control" value="{{ $data->ndemPots ?: old('ndemPots') }}">
						{!! $errors->first('ndemPots','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

<!-- 				<div class="col-md-6">
					<div class="form-group">
						<label>K-CONTACT</label>
						<input type="text" name="kcontact" class="form-control" value="{{ $data->kcontact ?: old('kcontact') }}">
						{!! $errors->first('kcontact','<span class="label label-danger">:message</span>') !!}
					</div>
				</div> -->

				<div class="col-md-6">
					<div class="form-group">
						<label>ODP</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="alproname" id="alproname" class="form-control" value="{{ $data->alproname ?: old('alproname') }}">
						{!! $errors->first('alproname','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>JENIS PSB</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="jenisPsb" class="form-control" value="{{ $data->jenisPsb ?: old('jenisPsb') }}">
						{!! $errors->first('jenisPsb','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>STO</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="sto" class="form-control" value="{{ $data->sto ?: old('sto') }}">
						{!! $errors->first('sto','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>NO INTERNET</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="internet" class="form-control" value="{{ $data->internet ?: old('internet') }}">
						{!! $errors->first('internet','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<input type="hidden" name="ndemSpeedy" class="form-control" value="{{ $data->internet }}">

				<div class="col-md-6">
					<div class="form-group">
						<label>NO TELPON</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="noTelp" class="form-control" value="{{ $data->noTelp ?: old('noTelp') }}">
						{!! $errors->first('noTelp','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

				<input type="hidden" name="ndemPots" class="form-control" value="{{ $data->noTelp }}">

				<div class="col-md-6">
					<div class="form-group">
						<label>MYIR</label> <span class="label label-danger">Wajib</span>
						<input type="text" name="myir" class="form-control" value="{{ $data->myir ?: old('myir') }}">
						{!! $errors->first('myir','<span class="label label-danger">:message</span>') !!}
					</div>
				</div>

		</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<input type="submit" value="Simpan" class="btn btn-primary">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('plugins')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
	<script>
		$(function(){
			var day = {
		      	  	format: 'yyyy-mm-dd',
		        	viewMode: 0,
		       	 	minViewMode: 0
		      	};

      		$("#alproname").inputmask("AAA-AAA-A{2,3}/999");
      	})
	</script>
@endsection