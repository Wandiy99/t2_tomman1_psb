@extends('layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>

  <?php if (count($checkONGOING)>0) : ?>
  <div class="content">
    <div class="panel panel-default">
      <div class="panel-heading">
        ONGOING PROGRESS
      </div>
      <div class="panel-content" style="padding:20px;">

        SAAT INI ANDA SEDANG ONGOING PROGRESS : <br />
        <?php foreach($checkONGOING as $ONGOING) : ?>
        <button class="btn btn-info">{{ $ONGOING->Ndem }} |  {{ substr($ONGOING->Ndem,0,2) }}</button>
        @if(strtoupper(substr($ONGOING->Ndem,0,2)) <> "IN")
        <a href="/{{ $ONGOING->id_dt }}" class="responsive btn btn-warning">==> CLICK HERE FOR REPORT</a>
        @else
        <a href="/tiket/{{ $ONGOING->id_dt }}" class="responsive btn btn-warning">==> CLICK HERE FOR REPORT</a>
        @endif
        <?php endforeach; ?>

      </div>
    </div>
  </div>
  <?php endif; ?>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
      <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
      <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
      <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
      <li class="{{ (Request::segment(2) == 'MAINTENANCE') ? 'active' : 'active'}}"><a href="/wo/maintenance">MAINTENANCE</a></li>
  </ul>
  
  @foreach($list as $no => $data)
      <div class="list-group-item">
          <span class="label label-primary">{{ ++$no }}. {{ $data->nama_order }}</span>
            @if($data->jenis_order==1)
              <strong><a href="/order/{{ $data->id }}" >{{ $data->no_tiket }}</a></strong>
            @else
              <strong>{{ $data->no_tiket }}</strong>
            @endif
           
            <span>{{ $data->headline }}</span> <br />
            <span>{{ $data->info }}</span><br />
            <span class="label label-default">PIC:{{ $data->pic }}</span><br />
           
            @if($data->jenis_order == 3)
              <strong>{{ $data->nama_odp_m or $data->nama_odp }}( {{ $data->koordinat or $data->kordinat_odp}} )</a></strong><br/>
            @endif
              
            @if (empty($checkONGOING))
                <a href="/wo/maintenance/{{ $data->id }}" class="btn btn-xs btn-info">
                  {{ $data->status or 'Progress' }}
                </a>
                <a href="/wo/maintenance-photos/{{ $data->id }}" class="btn btn-xs btn-info">
                  Photos
                </a>
            @endif
           
            @if($data->psb_laporan_id)
              <a href="#"  class="btn btn-xs btn-info button_info" id-psb="{{ $data->psb_laporan_id }}">
                Info
              </a>
            @endif
           
            <span class="label label-info">
            {{ $data->dispatch_regu_name or '---' }}
            </span>
            &nbsp
            <!-- 
            <span class="label label-info">
              Total Rp. {{ number_format($data->hasil_by_user) }}
            </span>
            -->
      </div>
  @endforeach
  
@endsection
