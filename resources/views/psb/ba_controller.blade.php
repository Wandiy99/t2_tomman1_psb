@extends('layout')
@section('content')
@include('partial.alerts')

<div class="row">

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4>Request Reset BA SC-{{ $id }}</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/utonline/request-reset-ba/save/{{ $id }}" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="id" value="{{ $id }}">
        @if (in_array(session('auth')->id_karyawan, ['20981020', 'wandiy99']))
        <div class="form-group">
            <label for="input-alasan" class="col-form-label">Status</label>
            <select class="form-control select2" name="status">
                <option value="" selected disabled>-- Pilih Status --</option>
                <option value="Deleted">Deleted</option>
            </select>
            {!! $errors->first('alasan','<p class="label label-danger">:message</p>') !!}
        </div>
        @endif
        <div class="form-group">
            <label for="input-alasan" class="col-form-label">Alasan</label>
            <textarea type="text" name="alasan" rows="4" class="form-control" required>{{ $data->alasan ? : '' }}</textarea>
            {!! $errors->first('alasan','<p class="label label-danger">:message</p>') !!}
        </div>
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-primary btn-rounded">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4>UT Online</h4></div>
    <div class="panel-body table-responsive">
      <table class="table table-striped">
          <tbody>
              <tr>
                  <td class="text-center">Order Code</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->order_code }}</td>
              </tr>
              <tr>
                  <td class="text-center">Order SC ID</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->scId_int }}</td>
              </tr>
              <tr>
                  <td class="text-center">Order Name</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->customer_desc }}</td>
              </tr>
              <tr>
                  <td class="text-center">Status (WITEL)</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->statusName }}</td>
              </tr>
              <tr>
                  <td class="text-center">Status (QC2)</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->qcStatusName }}</td>
              </tr>
              <tr>
                  <td class="text-center">Tanggal WO / Tanggal Transaksi</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->tglWo_date }} / {{ $data->tglTrx }}</td>
              </tr>
              <tr>
                  <td class="text-center">Log Activity</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->last_updated_at }}</td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>
</div>

</div>
@endsection