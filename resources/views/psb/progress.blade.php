@extends('layout')

@section('content')
  <style>
  .btn strong.glyphicon {         
    opacity: 0;       
  }
  .btn.active strong.glyphicon {        
    opacity: 1;       
  }
  </style>
 @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input name="_method" type="hidden" value="PUT">
    
    <h3>
      <a href="/" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      Proggress {{ $data->no_tiket }}
    </h3>
    <div class="row col-sm-6">

    <!--<div class="form-group">
      <label class="control-label" for="input-team">Team</label> -->
      <input name="team" type="hidden" id="input-team" class="form-control" value="{{ $data->team or '' }}" />

    <!--</div>
    <div class="form-group">
      <label class="control-label" for="input-tgl">Tanggal Selesai</label>-->
      <input name="tgl_selesai" type="hidden" id="input-tgl" class="form-control" value="{{ $data->tgl_selesai or '' }}" />
    <!--</div>-->
    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
      <label for="input-status" class="control-label">Status</label>
      <input name="status" type="text" id="input-status" class="form-control input-sm" value="{{ old('status') ?: @$data->status }}"/>
      @foreach($errors->get('status') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    <div class="form-group {{ $errors->has('splitter') ? 'has-error' : '' }}">
      <label for="splitter" class="control-label">Jenis Splitter</label>
      <input name="splitter" type="text" id="splitter" class="form-control input-sm" value="{{ old('splitter') ?: @$data->splitter }}"/>
      @foreach($errors->get('splitter') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    <div class="form-group {{ $errors->has('action_cause') ? 'has-error' : '' }}">
      <label for="action_cause" class="control-label">Action/Cause</label>
      <input name="action_cause" type="text" id="action_cause" class="form-control input-sm" value="{{ old('action_cause') ?: @$data->action_cause }}"/>
      @foreach($errors->get('action_cause') as $msg)
          <span class="help-block">{{ $msg }}</span>
      @endforeach
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Koordinat</label>
      <input name="koordinat" type="text" id="input-koor" class="form-control input-sm" value="{{ old('koordinat') ?: @$data->koordinat }}" />
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Headline</label>
      <textarea name="headline" id="headline" class="form-control">{{ old('headline') ?: @$data->headline }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="input-koor">Action</label>
      <textarea name="action" id="act" class="form-control">{{ old('action') ?: @$data->action }}</textarea>
    </div>
    <span style="font-size:10px;">
      <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
          <span class="glyphicon glyphicon-list"></span>
          Material
        </button>
      <br/>
      <br/>
      <div class="form-group">
        <ul id="material-list" class="list-group">
          <li class="list-group-item" v-repeat="$data | hasQty ">
            <span class="badge" v-text="qty"></span>
            <strong v-text="id_item"></strong>
            <p v-text="nama_item"></p>
          </li>
        </ul>
      </div>
    </span>

    <?php
      $ver = $data->verify;
      $decline = $ver-1;
      $approve = $ver+1;
      if($decline<1)
        $decline = null;
      // dd(session('auth'));
      // $l = session('auth')->maintenance_level; 
      $l = $level;
      $verifshow = 'hide';
      if(($l == 2 and $ver == null) or ($l == 3 and $ver == 1) or ($l == 4 and $ver == 2) or $l == 1)
      $verifshow = 'show';
    ?>
    <!--<div class="{{ (session('auth')->id_karyawan == '87142031' ? 'show' : (session('auth')->id_karyawan == '92140917' ? 'show' : ($data->niktl == session('auth')->id_karyawan ? 'show': 'hidden'))) }}" data-toggle="buttons">-->
    <div class="{{ $verifshow }}" data-toggle="buttons">
        <label class="btn btn-danger col-sm-6">
          <input type="radio" name="verify" autocomplete="off" value="{{ $decline }}">
          <strong class="glyphicon glyphicon-ok"></strong>
          Decline {{ $decline }}
        </label>

        <label class="btn btn-success col-sm-6">
          <input type="radio" name="verify" autocomplete="off" value="{{ $approve }}">
          <strong class="glyphicon glyphicon-ok"></strong>
          Approve {{ $approve }}
        </label>
        <input type="text" class="form-control col-sm-6 {{ $verifshow }}" placeholder="komen" name="catatan"/>
    </div>
    <table class="table table-bordered table-striped" id="table">
      <tr>
        <th>NIK</th>
        <th>TANGGAL</th>
        <th>STATUS</th>
        <th>KOMENTAR</th>
      </tr>
      @foreach($komen as $k)
        <tr>
          <td>{{ $k->pelaku }}</td>
          <td>{{ $k->ts }}</td>
          <td>{{ $k->status }}</td>
          <td>{{ $k->catatan }}</td>
        </tr>
      @endforeach
    </table>
    </div>
    <input type="hidden" name="materials" value="[]" />
    <div class="row col-sm-6 text-center input-photos" style="margin: 20px 0">
     <label class="control-label"><b>Dokumentasi</b></label><br />
      <?php

        $number = 1;
        $col = 3;
        //echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
        //if($data->psb_laporan_id){
        //if (file_exists("http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg")){
          //echo "http://t1psb.tomman.info/upload/evidence/".$data->id_tbl_mj."/ODP.jpg";
          if($data->order_from){
            $folder="";
            if($data->order_from=="PSB")
              $folder = "evidence";
            else if($data->order_from=="ASSURANCE")
              $folder = "asurance";
            $odp = '/home/ta/htdocs/tomman1_psb_t1/public/upload/'.$folder.'/'.$data->id_tbl_mj.'/ODP-th.jpg';
            if (file_exists($odp)){
              echo'<div class="col-xs-6 col-sm-6"><a href="/home/ta/htdocs/tomman1_psb_t1/public/upload/'.$folder.'/'.$data->id_tbl_mj.'/ODP.jpg">
              <img src="'.$odp.'" alt="" style="width:100px;height:150px;" />
              </a><br/>FOTO ODP</div>';
            }
            $file = '/home/ta/htdocs/tomman1_psb_t1/public/upload/'.$folder.'/'.$data->id_tbl_mj.'/ODP-th.jpg';
            if (file_exists($file)){
              echo'<div class="col-xs-6 col-sm-6"><a href="/home/ta/htdocs/tomman1_psb_t1/public/upload/'.$folder.'/'.$file.'/Hasil_Ukur_OPM.jpg">
              <img src="'.$file.'" alt="" style="width:100px;height:150px;" />
              </a><br/>FOTO Hasil_Ukur_OPM</div>';
            }
            $col = 4;
          }
        //}

      ?>
       @foreach($foto as $input)
        <div class="col-xs-6 col-sm-6">
          <?php
            $path = "/upload/maintenance/".Request::segment(2)."/$input";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
            $flag = "";
            $name = "flag_".$input;
          ?>
          @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
              <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
              $flag = 1;
            ?>
          @else
            <img src="/image/placeholder.gif" alt="" />
          @endif
          <br />
          <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
          <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
          <button type="button" class="btn btn-sm btn-info">
            <i class="glyphicon glyphicon-camera"></i>
          </button>
          <p>{{ str_replace('_',' ',$input) }}</p>
          {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
        </div>
      <?php
        $number++;
      ?>
      @endforeach
    </div>
    
    <div style="margin:40px 0 20px" class="text-center">
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background: #eee">
          <strong style="color:black;">Laporan Material</strong>
          <button class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal" type="button">Close</button>
        </div>
        <div class="modal-body" style="overflow-y:auto;height : 450px">
          <input id="searchinput" class="form-control input-sm" type="search" placeholder="Search..." />
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong>
              <div class="input-group" style="width:150px;float:right;">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <!-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit" disabled></button> -->
                <input v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center input-sm" />
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
              <p v-text="nama_item" style="font-size:10px;"></p>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          
        </div>
      </div>
    </div>
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />
  <script src="/bower_components/select2/select2.min.js"></script>
  <script>
  $(function() {
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);
            
          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });
    var materials = <?= json_encode($materials) ?>;

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0 });
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });
    $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });
    level = <?= json_encode(session('auth')->level) ?>;
    var data = [{"id":"close", "text":"Close"},{"id":"kendala pelanggan", "text":"Kendala Pelanggan"},{"id":"kendala teknis", "text":"Kendala Teknis"}];
    var select2Options = function() {
      return {
        data: data,
        placeholder: 'Input Status',
        formatSelection: function(data) { return data.text },
        formatResult: function(data) {
          return  data.text;
        },
        minimumResultsForSearch: Infinity
      }
    }
    $('#input-status').select2(select2Options());

    var day = {
      format: "yyyy-mm-dd",viewMode: 0, minViewMode: 0
    };
    $("#input-tgl").datepicker(day).on('changeDate', function (e) {
        $(this).datepicker('hide');
      });
    $("#searchinput").keyup(function() {
      setTimeout(function(){
        var input, filter, ul, li, a, i;
        input = document.getElementById("searchinput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("searchlist");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("p")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";

            }
        }
      }, 2000);
    },);
    var action_cause = <?= json_encode($action_cause) ?>;
    var ac = $('#action_cause').select2({
        data: action_cause,
        minimumResultsForSearch: Infinity
    });
    var splitter = [{"id":"1:8", "text":"1:8"},{"id":"1:16", "text":"1:16"},{"id":"1:8+1:2", "text":"1:8+1:2"},{"id":"1:8+1:4", "text":"1:8+1:4"},{"id":"1:8+1:8", "text":"1:8+1:8"}];
    var spl = $('#splitter').select2({
        data: splitter,
        minimumResultsForSearch: Infinity
    });
    $("#input-status").change(function(){
      $.getJSON("/getActionCause/"+$("#input-status").val(), function(data){
          ac.select2({data:data,minimumResultsForSearch: Infinity});
      });
    });
  });
  </script>

@endsection