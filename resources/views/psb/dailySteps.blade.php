@extends('layout')
@section('content')
@include('partial.alerts')

@if (count(@$check) > 0)
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Rekan Sudah Melaporkan Hari Ini pada Tanggal {{ @$check->dtm_laporan }} 
</div>
@endif

<link rel="stylesheet" href="/bower_components/datepicker/css/datepicker.css" />

<div class="row">

<div class="col-sm-3">
</div>

<div class="col-sm-6" style="text-align: center;">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">REPORT STEP UP PROVISIONING</h4></div>
    <div class="panel-body">
      <form id="submit-form" method="post" action="/prov/dailyStepsSave" enctype="multipart/form-data" autocomplete="off">

        <div class="form-group">
          <label for="input-nik" class="col-form-label">Tanggal Laporan</label>
          <input type="date" name="tgl_laporan" id="tgl_laporan" class="form-control text-center" value="{{ @$check->tgl_laporan ? : date('Y-m-d') }}">
          {!! $errors->first('tgl_laporan','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-nik" class="col-form-label">NIK</label>
            <input type="text" name="nik_pelapor" class="form-control text-center" id="input-nik" value="{{ session('auth')->id_karyawan }}" readonly/>
            {!! $errors->first('nik_pelapor','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-nama" class="col-form-label">Nama</label>
            <input type="text" name="nama_pelapor" class="form-control text-center" id="input-nama" value="{{ session('auth')->nama }}" readonly/>
            {!! $errors->first('nama_pelapor','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
            <label for="input-langkah" class="col-form-label">Langkah</label>
            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="langkah_pelapor" value="{{ @$check->langkah_pelapor ? : '' }}" class="form-control text-center" id="input-langkah" required/>
            {!! $errors->first('langkah_pelapor','<p class="label label-danger">:message</p>') !!}
        </div>

        <div class="form-group">
          <label class="control-label" for="evidence">*Upload Evidence*</label>
            @if (count(@$check) > 0)
              @php
                $file = "data-default-file=/upload4/dailySteps/".session('auth')->id_karyawan."/".date('Ymd').".jpg";
              @endphp
            @else
              @php
                $file = '';
              @endphp
            @endif
          <input type="file" accept="image/*" name="evidence" id="evidence" class="dropify" {{ $file }} data-height="500" required/>
          {!! $errors->first('evidence','<p class="label label-danger">:message</p>') !!}
        </div>
        
        <div style="margin:40px 0 20px; text-align: center">
          <button class="btn btn-lg btn-primary btn-rounded">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="col-sm-3">
</div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/bower_components/select2/select2.min.js"></script>
<script type="text/javascript">
    $(function() {

      var day = {
                format: 'yyyy-mm-dd',
                viewMode: 0,
                minViewMode: 0,
            };

      $('#tgl_laporan').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
      });

      $('.dropify').dropify();

    });
</script>
@endsection