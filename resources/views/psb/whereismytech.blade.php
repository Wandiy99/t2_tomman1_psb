@extends('layout')

@section('content')
  @include('partial.alerts')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <style>
  /* .marker {
   background-size: cover;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
  }
  */
  .mapboxgl-popup {
    font-size: 12px;
  }
  .maps_box{
    border : 1px solid black;
  }
  </style>
  <h2>Operation Kalsel</h2>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-area" name="area">
            <option disabled>Pilih Sektor</option>
            <option value="ALL">ALL</option>
          </select>
        </div>

        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-order" name="order">
            <option disabled>Jenis Order</option>
            <option value="ALL">ALL</option>
          </select>
        </div>

        <div class="col-sm-3">
          <select class="form-control select2" id="input-type-segment" name="segment">
            <option disabled>Segment</option>
            <option value="ALL">ALL</option>
          </select>
        </div>

        <div class="col-sm-2">
              <button class="btn btn-info btn-rounded btn-block" type="submit">Search</button>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <label for="search">&nbsp;</label>

    </div>
    <div class="col-sm-3">
      <div class="row">
        <div class="col-sm-12">
        <div class="white-box">
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">KEHADIRAN TEKNISI</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">{{ $jumlah_teknisi }}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">TOTAL ORDER</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">71</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">KENDALA</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">71</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">PROGRESS</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-danger">71</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>
          <!-- kehadiran teknisi !-->
          <div class="col-in row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <i data-icon="" class="linea-icon linea-basic"></i>
              <h5 class="text-muted vb">PS</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h3 class="counter text-right m-t-15 text-success">71</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="progress">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">JUMLAH TEKNISI HADIR BER-WO HI</span>
                </div>
              </div>
            </div>
          </div>

        </div>
        </div>
      </div>
  </div>
    <div class="col-sm-9">
      <div id="mapMarkerModal_mapView" class="maps_box"></div>
    </div>
  </div>

<script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
<link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  $('.select2').select2();
  var data = <?= json_encode($get_area) ?>;
      var select2Options = function() {
      return {
      data: data,
      placeholder: 'Input Area',
      formatSelection: function(data) { return data.text },
      formatResult: function(data) {
          return  data.text;
      }
      }
  }
  $('#input-type-area').select2(select2Options());
});
</script>
<script>
    (function (){
        var mapEl = document.getElementById('mapMarkerModal_mapView');
        mapEl.style.height = (window.innerHeight * .65) + 'px';

        var map;
        var marker;
        var $in;

        var initMap = function() {
            if (map) return;
            var center = { lng: 114.594376, lat: -3.318607 };
            map = new mapboxgl.Map({
                container: mapEl,
                center: center,
                zoom: 10,
                style: 'https://map.tomman.app/styles/osm-liberty/style.json'
            });

            <?php
              foreach ($query as $num => $data) {
                ?>
                var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
                '<?php echo $data->regu_nama ?> ');
                new mapboxgl.Marker({ draggable: false, icon:'default', color:'red', cursor:'pointer' })
                    .setLngLat({lat : '{{ $data->lat ? : 0 }}',lon : '{{ $data->lon ? : 0 }}'})
                    .setPopup(popup)
                    .addTo(map);
                <?php
              }
            ?>


          }
          initMap();
        })()
</script>

@endsection
