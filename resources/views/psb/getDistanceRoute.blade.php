@extends('public_layout')
@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading estimasi_jarak" id="jarak_text">RUTE JARAK</div>
      <div class="panel-body">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
        <link rel="stylesheet" href="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.css" />
        <style type="text/css">
          .leaflet-measure-path-measurement {
            font-size: 12px !important;
          }
        </style>
        <div class="col-sm-12">
            Jarak : <span id="jarak_text" class="estimasi_jarak"></span>
            <div id="map" style="height: 750px; margin-top: 10px;"></div>
        </div>

        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/examples/Control.Geocoder.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.js"></script>
        {{-- <script src="https://cdn.jsdelivr.net/npm/pixi.js@7.x/dist/browser/pixi.min.js"></script> --}}
        {{-- <script src="https://cdn.jsdelivr.net/npm/leaflet-pixi-overlay@1.8.2/L.PixiOverlay.min.js"></script> --}}
        <script type="text/javascript">
              $(function() {
                var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
      tiang_all = L.layerGroup(),
      odp_all = L.layerGroup(),
      kosong = L.layerGroup(),
      pLineGroup = L.layerGroup(),
      zoom_me = 18,
      googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      }),
      googleStreets2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleHybrid2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleSat2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTerrain2 = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleAlteredRoad2 = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTraffic2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      }),
      baseLayers = {
        'Jalan': googleStreets,
        'Satelit': googleSat,
        'Satelit Dan Jalan': googleHybrid,
        'Jalur': googleTerrain,
        'Jalur 2': googleAlteredRoad,
        'Lalu Lintas': googleTraffic,
      },
      baseLayers2 = {
        'Jalan': googleStreets2,
        'Satelit': googleSat2,
        'Satelit Dan Jalan': googleHybrid2,
        'Jalur': googleTerrain2,
        'Jalur 2': googleAlteredRoad2,
        'Lalu Lintas': googleTraffic2,
      },
      overlays = {
        'ODP': odp_all,
        'Tiang': tiang_all,
        'Garis': pLineGroup,
      },
      overlays2 = {},
      collect_odp = [],
      load_collect_odp = [],
      collect_tiang = [],
      load_collect_tiang = [],
      totalDistance = [],
      load_koor_poly = [],
      load_distance = [],
      sum = 0,
      radius = 500.00,
      data_tiang = null,
      home_customer = new L.Icon({
        iconUrl: '/image/home_customer.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      });
      pole_telkom = new L.Icon({
        // iconUrl: '/image/pole_telkom.png',
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      odp_telkom = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      id_input = '',
      marker = ''
      isi_txt = '-3.319000,114.584100';

      $('.show_modal_koor').on('click', function(){
        var title= $(this).data('titlenya');
        id_input= '#'+$(this).data('id_input');
        isi_txt = $(id_input).val();

        var koor_now = isi_txt.split(',').map(x => x.trim() );

        if(koor_now[0].length == 0){
          koor_now = [-3.319000, 114.584100];
        }

        $('#mapMarkerModal_latText').html(koor_now[0]);
        $('#mapMarkerModal_lngText').html(koor_now[1]);

        marker = new L.marker(koor_now, {
          draggable: 'true'
        });

        marker.on('dragend', function (e) {
          var lat = marker.getLatLng().lat,
          lng = marker.getLatLng().lng;

          $('#mapMarkerModal_latText').html(lat);
          $('#mapMarkerModal_lngText').html(lng);

          $(id_input).val(`${lat}, ${lng}`);
        });

        $('#mapMarkerModal_title').html(title);
      })

      $('#mapMarkerModal_mapView').css({
        height: (window.innerHeight * .30) + 'px'
      })

      function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
        var R = 6371, // Radius of the earth in km
        dLat = deg2rad(lat2-lat1),  // deg2rad below
        dLon = deg2rad(lon2-lon1),
        a =Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *Math.sin(dLon/2) * Math.sin(dLon/2),
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)),
        d = R * c; // Distance in km

        return d;
      }

      function deg2rad(deg) {
        return deg * (Math.PI/180)
      }

      if ($('#map').length) {
        // $.ajax({
        //   type: "GET",
        //   url: "/map/ajx_tiang_all",
        //   async: false,
        //   success: function(data){
        //     data_tiang = data
        //   }
        // });

        // $.ajax({
        //   type: "GET",
        //   url: "/map/ajx_odp_all",
        //   async: false,
        //   success: function(data){
        //     data_odp = data
        //   }
        // });

        var map = L.map('map', {
          center: ['-3.319000', '114.584100'],
          zoom: zoom_me,
          maxZoom: 20,
          layers: [googleHybrid, odp_all, tiang_all, pLineGroup]
        }),
        layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
        circle = L.circle(map.getCenter(), {
          radius: 500,
          color: '#C0392B',
          fillOpacity: 0,
        }).addTo(map);

        map.on("moveend", function () {
          var lat = map.getCenter().lat,
          lng = map.getCenter().lng;

          circle.setLatLng(map.getCenter() );
          map._renderer._update();

        //   $.each(data_odp, function(k, v){
        //     let dst = (getDistanceFromLatLonInKm(lat, lng, v.latitude, v.longitude) * 1000).toFixed(2);
        //     data_odp[k].distance = dst;
        //   })

          if(collect_odp.length != 0)
          {
            $.each(collect_odp, function(k, v){
              if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
                map.removeLayer(v)
              }
            })

            $.each(odp_all._layers, function(k, v){
              if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
                delete odp_all._layers[k]
              }
            })
          }

        //   $.each(data_tiang, function(k, v){
        //     let dst = (getDistanceFromLatLonInKm(lat, lng, v.latitude, v.longitude) * 1000).toFixed(2);
        //     data_tiang[k].distance = dst;
        //   })

          if(collect_tiang.length != 0)
          {
            $.each(collect_tiang, function(k, v){
              if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
                map.removeLayer(v)
              }
            })

            $.each(tiang_all._layers, function(k, v){
              if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
                delete tiang_all._layers[k]
              }
            })
          }

        //   $.each(data_odp, function(k, v){
        //     var koor = `${v.latitude},${v.longitude}`;
        //     if(v.distance <= radius ) {
        //       if(load_collect_odp.indexOf(koor) === -1){
        //         var note = `<b>${v.odp_name}</b><br />Status : ${v.occ_2}`;

        //         collect_odp.push(L.marker([v.latitude, v.longitude], {icon: odp_telkom})
        //         .bindPopup(note)
        //         .addTo(odp_all)
        //         .on('click', function(e) {
        //           L.popup()
        //           .setLatLng([v.latitude, v.longitude])
        //           .setContent(note);
        //         }));
        //         load_collect_odp.push(koor)
        //       }
        //     }else{
        //       load_collect_odp = load_collect_odp.filter(function(e) { return e != koor })
        //     }
        //   })

        //   $.each(data_tiang, function(k, v){
        //     var koor = `${v.latitude},${v.longitude}`;
        //     if(v.distance <= radius ){
        //       if(load_collect_tiang.indexOf(koor) === -1){
        //         var note = `Nama: <b>${v.nama}</b></br>ODC: <b>${v.odc}</b></br>STO: <b>${v.sto}</b>`;
        //         collect_tiang.push(L.marker([v.latitude, v.longitude], {icon: pole_telkom})
        //         .bindPopup(note)
        //         .addTo(tiang_all)
        //         .on('click', function(e) {
        //           let koor_now = `${e.latlng.lat}, ${e.latlng.lng}`;

        //           if(check_dup.indexOf(koor_now) === -1){
        //             check_dup.push(koor_now)
        //             load_koor.push([e.latlng.lat, e.latlng.lng])
        //             // console.log(load_koor)

        //             var polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
        //               formatDistance: function(e){
        //                 // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
        //                 // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
        //                 return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
        //               }
        //             }),
        //             marker = e.target._popup._source;

        //             polyline.getLatLngs().forEach(function (latLng) {
        //               var latlng_text = latLng.lat +','+ latLng.lng;

        //               if(load_distance.indexOf(latlng_text) === -1){
        //                 load_distance.push(latlng_text)
        //               }

        //               if (previousPoint.length != 0) {
        //                 if(koor_sc.split(',').map(x => x.trim() ).join(',') != latlng_text){
        //                   if(totalDistance.map(x => x.koor).indexOf(latlng_text) === -1){
        //                     totalDistance.push({
        //                       koor: latlng_text,
        //                       jarak: parseFloat(previousPoint.distanceTo(latLng).toFixed(2) )
        //                     });
        //                   }

        //                   marker.getPopup().setContent(`${note}</br>Total Jarak: <b>${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b)}</b> meter</br> Jarak Dari Tiang Sebelumnya:<b>${previousPoint.distanceTo(latLng).toFixed(2)}</b> Meter`);

        //                   marker.getPopup().update();
        //                   marker.closePopup();
        //                 }
        //               }
        //               previousPoint = latLng;
        //             });

        //             map.flyTo([e.latlng.lat, e.latlng.lng], zoom_me);

        //             $.each(load_distance, function(k, v){
        //               var koor_odp = v.split(',').map(x => x.trim() ),
        //               koor_sc_split = koor_sc.split(',').map(x => x.trim() );
        //             });
        //           }
        //         }) );
        //         load_collect_tiang.push(koor)
        //       }
        //     }else{
        //       load_collect_tiang = load_collect_tiang.filter(function(e) { return e != koor })
        //     }
        //   })

          $("input[name='koordinat_tiang']").val(JSON.stringify(load_koor) )
        });

        $('.undo_btn').on('click', function(){
          if(load_koor.length > 1){
            pLineGroup.clearLayers();
            load_koor.pop();
            check_dup.pop();
            totalDistance.pop();

            if(totalDistance.length != 0){
              sum = totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2);
            }else{
              sum = 0
            }

            $('.estimasi_jarak').html(`Total Estimasi Jarak ${sum} Meter`);
            map.closePopup()

            var polyline = L.polyline(load_koor).addTo(pLineGroup)

            polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
              formatDistance: function(e){
                // console.log(e, pLineGroup)
                // console.log(Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter')
                // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
                // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
                return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
              }
            });
          }
        });

        var previousPoint = '',
        dataOrder = {!! json_encode($project) !!},
        koor_sc = dataOrder.kordinat_pelanggan || '-3.319000,114.584100',
        raw_koor_tiang_all = JSON.parse(dataOrder.koordinat_tiang) ?? [],
        load_koor = [],
        check_dup = [],
        load_data_tiang = [];
        estimasi_jarak = [];

        var koor_tiang_all = raw_koor_tiang_all.filter(function(x) {
          return (x.join('').length !== 0);
        });

        if(typeof(koor_tiang_all) != 'undefined' && koor_tiang_all.length != 0){
          koor_tiang_all = koor_tiang_all.map(function(v) {
            return v.map(function(v2) {
              return parseFloat(v2);
            });
          });

          var polyline = L.polyline(koor_tiang_all).addTo(map)
          .showMeasurements({
            formatDistance: function(e){
              // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
              // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
              return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
            }
          });

          var new_koor_tng = koor_tiang_all.map(i => Object.assign({}, i));

          load_koor = new_koor_tng.map(function(v) {
            return [v[0], v[1] ]
          });

          var new_koor_tng2 = koor_tiang_all.map(i => Object.assign({}, i));

          check_dup = new_koor_tng2.map(function(v) {
            return `${v[0]},${v[1]}`
          });

          check_dup.shift();

          polyline.getLatLngs().forEach(function (latLng) {
            var latlng_text = latLng.lat +','+ latLng.lng;

            if(load_distance.indexOf(latlng_text) === -1){
              load_distance.push(latlng_text)
            }

            if (previousPoint.length != 0) {
              if(koor_sc.split(',').map(x => x.trim() ).join(',') != latlng_text){
                if(totalDistance.map(x => x.koor).indexOf(latlng_text) === -1){
                  totalDistance.push({
                    koor: latlng_text,
                    jarak: parseFloat(previousPoint.distanceTo(latLng).toFixed(2) )
                  });
                }
              }
            }
            previousPoint = latLng;
          });

        }else{
          load_koor.push(koor_sc.split(',').map(x => x.trim() ) );
        }

        L.marker(koor_sc.split(',').map(x => x.trim() ), {icon: home_customer}).bindPopup(`<div class="table-responsive"><table><tbody><tr><td>SC</td><td>: </td><td>${dataOrder.orderIdInteger}</td></tr><tr><td>Pelanggan</td><td>: </td><td>${dataOrder.orderName}</td></tr><tr><td>Order Status</td><td>: </td><td>${dataOrder.orderStatus}</td></tr></tbody><table></div>`).addTo(map).on('click', function(e) {
        });

        map.flyTo(koor_sc.split(',').map(x => x.trim() ), zoom_me);
      }
      console.log(load_koor)
      if(totalDistance.length != 0){
            // console.log(totalDistance)
            // $('.estimasi_jarak').html(`Total Estimasi Jarak ${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2)} Meter`),
            $('.estimasi_jarak').html(totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2) );
          }
      var myRouter = L.Routing.control({
            waypoints: load_koor.map(function(x){
                return L.latLng(x[0], x[1])
            }),
            // router: L.Routing.mapbox(token, {
            //     language: 'id',
            //     urlParameters: {
            //         vehicle: 'foot'
            //     }
            // }),
          geocoder: L.Control.Geocoder.nominatim(),
            routeWhileDragging: true,
            reverseWaypoints: true,
            showAlternatives: true,
            collapsible: true,
            autoRoute: true,
            routeWhileDragging: true,
        }).on('routeselected', function(e) {
            var route = e.route,
            jarak = e.route.summary.totalDistance,
            waktu = e.route.summary.totalTime;
            $('#jarak_text').html(`${jarak}`);
            console.log(jarak, waktu)
            // alert('Showing route between waypoints:\n' + JSON.stringify(route.inputWaypoints, null, 2));
        }).addTo(map);
              });
        </script>
      </div>
    </div>
  </div>
  @endsection