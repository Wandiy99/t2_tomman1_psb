@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }
  </style>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmAwGwcaeJKLu7f3Noyhw2ihC8s8aaoPs"></script> -->

  <?php
    if (count($checkQCOndesk)>0){
      echo "<div class='alert alert-danger'> ANDA MEMILIKI ORDER REVISI</div>";
      $list = $checkQCOndesk;
    }
  ?>
  <ul class="nav nav-tabs" style="margin-bottom:20px;">
    <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
    <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
    <li class="{{ (Request::segment(2) == 'HR') ? 'active' : '' }}"><a href="/workorder/HR">HR</a></li>
    <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
    <li class="{{ (Request::segment(1) == 'ibooster') ? 'active' : '' }}"><a href="/ibooster">IBOOSTER</a></li>
    <li class="{{ (Request::segment(1) == 'searchbarcode') ? 'active' : '' }}"><a href="/searchbarcode">BARCODE</a></li>
    <li class="{{ (Request::segment(3) == 'history_reject QC') ? 'active' : '' }}"><a href="/viewreject">HISTORY REJECT QC</a></li>
    <li class="{{ (Request::segment(4) == 'splitter_gendong') ? 'active' : '' }}"><a href="/input-spl-gendong/form">SPLITTER GENDONG</a></li>
    <!-- <li><a href="/stok">Materials/NTE</a></li> -->
  </ul>


   <!-- bulan wo -->
  @if (Request::segment(2) == 'KENDALA' || Request::segment(2) == 'UP')
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="GET">
            Bulan :
            <input type="month" name="bulan" id="bulan">
            <input type="submit" value="Filter" />
        </form><br>

      </div>
    </div>
  @endif

<!--   <ul class="nav nav-tabs" style="margin-bottom:20px;">
    <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">PROVISIONING</a></li>
    <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">ASSURANCE</a></li>
    <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">MIGRASI</a></li>
  </ul> -->

  <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Jangan Lupa Absen HANA dan Selalu Update Material di AMALIA</strong>
    </div>

  <?php if ($updatenote->note<>"") : ?>
  <div class="alert alert-success"><?php echo ($updatenote->note) ?></div>
  <?php endif?>
  <?php if (count($checkONGOING)>0) : ?>

  <div class="content" >
  @foreach($cekManja as $manja)
    @if(count($manja)>0)
    <div class='alert alert-danger'> ANDA MEMILIKI ORDER GANGGUAN MANJA <strong>{{ $manja->Incident }}</strong> JAM <strong>{{ $jam_manja }}</strong> TUNTASKAN TERLEBIH DAHULU REKAN</div>
    @endif
  @endforeach
    <?php foreach($checkONGOING as $ONGOING) : ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <?php
            if($ONGOING->dispatch_by==6){
                $order = 'INX'.$ONGOING->Ndem;
            }
            else{
                $order = $ONGOING->Ndem;
            }
         ?>

        Picked Up Order <span class="label label-info" style="font-size:12px;">{{ $order }}</span> <span class="label label-info" style="font-size:12px;">{{ $ONGOING->Service_No ? : $ONGOING->ndemSpeedy ? : $ONGOING->myir }}</span>
      </div>
      <div class="panel-content" style="padding:0px;">
        <div class="row">
          <div class="col-sm-12">
            <a href="tel::{{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak }}" class="btn btn-default form-control">
              <span class="glyphicon glyphicon-earphone"></span> {{ $ONGOING->customer ? : $ONGOING->orderName ? : "#NA" }} {{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak }}
            </a>
            Lokasi : {{ $ONGOING->lon }},{{ $ONGOING->lat }}
          </div>
          <div class="col-sm-12" style="padding-left:30px;padding-top:10px;padding-bottom:10px;">
            {{ $ONGOING->orderCity ? : $ONGOING->alamatLengkap  ? : 'DATA ALAMAT TIDAK LENGKAP' }}
          </div>
          <div class="col-sm-12">
          </div>
          <div class="col-sm-12">
            @if(strtoupper(substr($ONGOING->Ndem,0,1)) == "I")
              <a href="/tiket/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning btn-bottom">CLICK HERE FOR REPORT</a>
            @elseif(strtoupper(substr($ONGOING->Ndem,0,1)) == "6" && $ONGOING->dispatch_by==6)
              <a href="/reboundary/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning  btn-bottom">CLICK HERE FOR REPORT</a>
            @else
              <a href="/{{ $ONGOING->id_dt }}" class="form-control responsive btn btn-sm btn-warning  btn-bottom">CLICK HERE FOR REPORT</a>
            @endif
          </div>
        </div>
      </div>
    </div>
      <?php endforeach; ?>
      <br />
  </div>
  <?php endif; ?>
  <br />

  <div class="list-group" style="margin-top:-20px;padding:0px;">
    <?php
      if (count($list_ffg)>0) {
        echo "<div class='alert alert-danger'> ANDA MEMILIKI ORDER GANGGUAN FFG</div>";
        $list = $list_ffg;
      }

    ?>

            @foreach($list as $num => $data)
              <?php
                if ($data->rebUmur>84){
                  $color = "warning3";
                } else if ($data->rebUmur>42 && $data->rebUmur<=84){
                  $color = "warning2";
                } else if ($data->rebUmur>24 && $data->rebUmur<=42){
                  $color = "warning1";
                } else {
                  $color = "default";
                }
              ?>

              @if($data->dispatch_by == '4')
                <div href="/guarantee/{{ $data->id_dt }}" class="list-group-item">
              @elseif($data->dispatch_by != '2')
                <div href="/{{ $data->id_dt }}" class="list-group-item">
              @else
                <div href="/tiket/{{ $data->id_dt }}" class="list-group-item">
              @endif
              <div class="linex {{ $color }}"></div>
              @if ($data->qcondesk_status == "rejected" and $data->status_laporan<>1)
              <p class="alert alert-warning">
                {{ $data->qcondesk_alasan }} <br />[Note from {{ $data->qcNama }} ({{ $data->qcPosition }})]
              </p>
              @endif

                @if ($data->dispatch_by=='6')
                    <a href="/reboundary/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @elseif($data->dispatch_by != '2')
                    <a href="/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @else
                    <a href="/tiket/{{ $data->id_dt }}" class="responsive label label-warning">==> CLICK HERE FOR REPORT</a>
                 @endif
               <br />
               <?php
                if ($data->jenisPsb<>NULL && $data->dispatch_by<>4){
                  $orderId = "SC.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else if ($data->jenisPsb<>NULL && $data->dispatch_by==4){
                  $orderId = "FG.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else if ($data->ND<>NULL){
                  $orderId = $data->ND;
                  $jenisWO = "MIGRASI AS IS";
                } else if ($data->datek=="ONTOFFLINE"){
                  $orderId = "";
                  $jenisWO = "ONT OFFLINE";
                } else if ($data->dispatch_by==4){
                  $orderId = "FG.".$data->orderId;
                  $jenisWO = $data->jenisPsb;
                } else {
                  $orderId = $data->no_tiket;
                  $jenisWO = "ASSURANCE";
                };

                if($data->dispatch_by==6){
                    $orderId = 'INX'.$data->no_tiket;
                    $jenisWO = "REBOUNDARY";
                };

               ?>
               <span class="label label-info">{{ $jenisWO }}</span>
               <span class="label label-info">{{ $data->laporan_status ? : "ANTRIAN" }}</span>
              <br /><br />
              <div class="table-responsive">
                <table class="table">
                <tbody>
                  <tr>
                    <td>ORDER</td>
                    <td>:</td>
                    <td>{{ $orderId ? : $data->no_tiket ? : $data->roc_no_tiket ? : $data->myir ? : @$data->mwnd }}</td>
                  </tr>
                  <tr>
                    <td>ORDER OPEN</td>
                    <td>:</td>
                    @if ($data->no_tiket<>NULL)
                    <td>{{ $data->TROUBLE_OPENTIME ? : $data->tglOpen ? : @$data->mwcreated_at }}</td>
                    @else
                    <td>{{ $data->orderDate ? : @$data->mwcreated_at }}</td>
                    @endif
                  </tr>
                  <tr>
                    <td>TGL APPROVE</td>
                    <td>:</td>
                    <td>{{ @$data->approve_date }}</td>
                  </tr>
                  <tr>
                    <td>STO</td>
                    <td>:</td>
                    <td>{{ $data->sto_dps ? : $data->neSTO ? : $data->sto  ? : @$data->mwmdf ? : "UNKOWN" }}</td>
                  </tr>
                  <tr>
                    <td>ODP</td>
                    <td>:</td>
                    <td>{{ @$data->namaOdp ? : @$data->alproname ? : @$data->neRK ? : '-' }}</td>
                  </tr>
                  <tr>
                    <td>NAMA</td>
                    <td>:</td>
                    <td>{{ @$data->customer ? : @$data->orderName ? : @$data->Customer_Name ? : @$data->mwnama }}</td>
                  </tr>
                  <tr>
                    <td>JENIS LAYANAN</td>
                    <td>:</td>
                    <td>{{ @$data->jenis_layanan ? : @$data->mwlayanan ? : @$data->nePRODUK_GGN   }}</td>
                  </tr>
                  <tr>
                    <td>ALAMAT</td>
                    <td>:</td>
                    <td>{{ @$data->alamatSales ? : @$data->neHEADLINE ? : @$data->mwalamat ? : '-' }}</td>
                  </tr>
                  <tr>
                    <td>PIC</td>
                    <td>:</td>
                    <td>{{ @$data->picPelanggan ? : @$data->Contact_Phone ? : @$data->mwpic ? : '-' }}</td>
                  </tr>
                  <tr>
                    <td>LOKASI</td>
                    <td>:</td>
                    <td> {{ @$data->koorPelanggan ? : @$data->mwalamat ? : '-' }}</td>
                  </tr>
                  </tbody>
                </table>
                </div>
                <br />
              
               <a href="tel::{{ $data->Contact_Phone ? : '0' }}" class="btn btn-default form-control">
                 <span class="glyphicon glyphicon-earphone"></span> {{ $data->Contact_Name }} ~ {{ $data->Contact_Phone ? : '0' }}
               </a>
              <br />
              </div>
              <br />  
            @endforeach  
  </div>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script>
  (function() {
    var day = {
            format : 'yyyy-mm',
            viewMode: "months",
            minViewMode: "months",
            autoClose: true
          };

        $('#bulan').datepicker(day).on('changeDate', function(e){
          $(this).datepicker('hide');
        });

    var initMap = function(lat, lon) {
      var marker = null;
      var center = new google.maps.LatLng(lat, lon),
          withMarker = (!isNaN(lat) && !isNaN(lon)) && (lat != 0 && lon != 0),
          zoom = withMarker ? 18 : 12;
      map = new google.maps.Map(
          document.getElementById('Maps'),
          {
              center: center,
              zoom: zoom,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          }
      );
    }
    var showMarker = function(lat,lng) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat,lng),
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP
            });
      }
      <?php if (count($checkONGOING)>0) :
         foreach($checkONGOING as $ONGOING) : ?>
    initMap({{ $ONGOING->lat ? : '-3.332081'}},{{ $ONGOING->lon ? : '114.67323'}});
    showMarker({{ $ONGOING->lat ? : '-3.332081'}},{{ $ONGOING->lon ? : '114.67323'}});
    <?php endforeach; endif; ?>
  })();
  </script>
@endsection
