@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <form method="GET">
            <div class="form-group">
                <label class="control-label">Pilih Jenis Pencarian</label>
                <div class="radio-list">
                    <label class="radio-inline p-0">
                        <div class="radio radio-info">
                            <input type="radio" name="type" id="inet" value="inet">
                            <label for="inet">Nomor Internet</label>
                        </div>
                    </label>
                    <label class="radio-inline">
                        <div class="radio radio-info">
                            <input type="radio" name="type" id="voice" value="voice">
                            <label for="voice">Nomor Voice</label>
                        </div>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <textarea name="params" id="params" class="form-control" rows="5" placeholder="Masukan Nomor Internet atau Nomor Voice lalu dipisah dengan simbol koma disetiap nomor" required /></textarea>
            </div>
            <div style="text-align: center">
                <button type="submit" class="btn btn-primary">Search !</button>
            </div>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">RESULT WORKORDER</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>SC</th>
                        <th>JENIS LAYANAN</th>
                        <th>ND INET</th>
                        <th>ND VOICE</th>
                        <th>ORDER DATE</th>
                        <th>ORDER DATE PS</th>
                        <th>STATUS TEK</th>
                        <th>PIC</th>
                        <th>ALAMAT 1</th>
                        <th>ALAMAT 2</th>
                        <th>KORPEL BY TEKNISI</th>
                        <th>KORPEL BY LAT STARCLICK</th>
                        <th>KORPEL BY LON STARCLICK</th>
                        <th>ODP BY TEKNISI</th>
                        <th>ODP BY STARCLICK</th>
                        <th>TGL STATUS</th>
                        <th>REGU</th>
                        <th>MITRA</th>
                        <th>SEKTOR</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $num => $result)
                    @if($result)                    
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>{{ $result->orderId }}</td>
                            <td>{{ $result->jenisPsb }}</td>
                            <td>{{ $result->internet }}</td>
                            <td>{{ $result->noTelp }}</td>
                            <td>{{ $result->orderDate }}</td>
                            <td>{{ $result->orderDatePs }}</td>
                            <td>{{ $result->laporan_status }}</td>
                            <td>{{ $result->noPelangganAktif ? : $result->picPelanggan }}</td>
                            <td>{{ $result->orderAddr ? : '-' }}</td>
                            <td>{{ $result->alamatLengkap ? : '-' }}</td>
                            <td>{{ $result->korpel_teknisi ? : '-' }}</td>
                            <td>{{ $result->korpel_lat_starclick ? : '-' }}</td>
                            <td>{{ $result->korpel_lon_starclick ? : '-' }}</td>
                            <td>{{ $result->odp_teknisi ? : '-' }}</td>
                            <td>{{ $result->odp_starclick ? : '-' }}</td>
                            <td>{{ $result->modified_at }}</td>
                            <td>{{ $result->regu }}</td>
                            <td>{{ $result->mitra }}</td>
                            <td>{{ $result->sektor }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL RESULT ALL WORKORDER TOMMAN'
                }
            ]
        });
    });
</script>
@endsection