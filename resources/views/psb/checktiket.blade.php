@extends('new_tech_layout')
	@section('content')
		@include('partial.alerts')

<div class="panel panel-primary">
<div class="panel-heading">History PSB<br />
    <form method="GET">
      <div class="input-group col-sm-5">
        <input type="number" minlength="11" maxlength="12" class="form-control" placeholder="Cari No Inet atau No Voice ..." name="id" id="id" />
          <span class="input-group-btn">
              <button class="btn btn-default search">
                  <span class="glyphicon glyphicon-search"></span>
              </button>
          </span>
      </div>
    </form>
</div>
<br />

		<div class="panel-body table-responsive">
			<div class="row">
               <div class="col-md-12">
				<table class="table table-hover" style="overflow-x:auto">
					<tr>
						<td><b>SC / NCLI / INET / VOICE</td></b><td>:</td><td>{{ @$data->orderId ? : '-' }} / {{ @$data->orderNcli ? : '-' }} / {{ @$data->internet ? : '-' }} / {{ @$data->noTelp ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>CUSTOMER</td><td>:</td></b><td>{{ @$data->orderName ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>ORDER STATUS / LAYANAN / KCONTACT</td><td>:</td></b><td>{{ @$data->orderStatus ? : '-' }} / {{ @$data->jenisPsb ? : '-' }} / {{ @$data->kcontact ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>CUSTOMER ADDRESS</td><td>:</td></b><td>{{ @$data->orderAddr ? : '-' }} ({{ @$data->lat ? : '-' }}, {{ @$data->lon ? : '-' }})</td>
					</tr>
					<tr>
						<td><b>ALPRONAME / STO</td><td>:</td></b><td>{{ @$data->alproname ? : '-' }} / {{ @$data->sto ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>STATUS LAPORAN / CATATAN / KOR.PELANGGAN</td><td>:</td></b><td>{{ @$data->laporan_status ? : '-' }} / {{ @$data->catatan ? : '-' }} / {{ @$data->kordinat_pelanggan ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>ODP / KOR.ODP</td><td>:</td></b><td>{{ @$data->nama_odp ? : '-' }} / {{ @$data->kordinat_odp ? : '-' }}</td>
					</tr>
					<tr>
						<td><b>DROPCORE LABELCODE / ODP LABELCODE</td><td>:</td></b><td>{{ @$data->dropcore_label_code ? : '-' }} / {{ @$data->odp_label_code ? : '-' }}</td>
					</tr>
				</table>
               </div>
			   </br>

    @if($data)
        @foreach($get_foto as $foto)
        <div class="col-6 col-sm-3">

		<?php
		$path = '';
		$path1  = "/upload/evidence/{$data->id_dt}";
        $path2 = "/upload2/evidence/{$data->id_dt}";
        $path3 = "/upload3/evidence/{$data->id_dt}";
        $path4 = "/upload4/evidence/{$data->id_dt}";

        if(file_exists(public_path().$path1))
		{
        	$path = "$path1/$foto";
        }
		elseif(file_exists(public_path().$path2))
		{
        	$path = "$path2/$foto";
        }
		elseif(file_exists(public_path().$path3))
		{
        	$path = "$path3/$foto";
        }
		elseif(file_exists(public_path().$path4))
		{
        	$path = "$path4/$foto";
        }

        $th    = "$path-th.jpg";
        $img   = "$path.jpg";
        ?>

        @if(file_exists(public_path().$th))
         <a href="{{ $img }}">
            <img src="{{ $th }}" alt="{{ $foto }}" style="width: 200px; height: 200px" />
         </a>
        @else
        <img src="/image/placeholder.gif" style="width: 200px; height: 200px" alt="" />
        @endif
    	</div>
		@endforeach
	@endif

			</div>
		</div>
	</div>
@endsection