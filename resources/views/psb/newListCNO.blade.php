@extends('new_tech_layout')

@section('content')
@include('partial.alerts')

@foreach($checkONGOING as $ONGOING)
@php
if($ONGOING->dispatch_by==6){
    $order = 'INX'.$ONGOING->Ndem;
}
else{
    $order = $ONGOING->Ndem;
}
@endphp
<div class="card border-light mb-3 shadow p-3 mb-5 bg-white rounded" >
  <div class="card-header bg-transparent"><label class="badge rounded-pill bg-primary" style="color: white">{{ $order }}</label> </label> <label class="badge rounded-pill bg-warning">{{ $ONGOING->laporan_status }}</label></div>
  <div class="card-body">
  <div class="table-responsive">
  <table class="table">
  <tbody style="text-align: center">
    <tr>
      <td>{{ $ONGOING->Customer_Name ? : $ONGOING->orderName ? : $ONGOING->nama_pelanggan ? : '-' }} / {{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak ? : '-' }}</td>
    </tr>
    <tr>
      <td>{{ $ONGOING->alamat ? : $ONGOING->orderAddr ? : '-' }}</td>
    </tr>
    </tbody>
  </table>
  </div>
    <center>
    @if ($ONGOING->dispatch_by=='6')
    <a href="/reboundary/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @elseif($ONGOING->jenis_order=="IN" || $ONGOING->jenis_order=="INT")
    <a href="/tiket/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @elseif($ONGOING->dispatch_by != '2')
    <a href="/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @endif
    </center>
  </div>
</div>
@endforeach
<br />

@foreach($list as $num => $data)

<div class="card border-light mb-3 shadow p-3 mb-5 bg-white rounded" > 
  <div class="card-header bg-transparent"><label class="badge rounded-pill bg-primary" style="color: white">{{ $data->wfm_id }}</label> <label class="badge rounded-pill bg-success" style="color: white">{{ $data->jenis_layanan }}</label> <label class="badge rounded-pill bg-dark" style="color: white">{{ $data->jenis_dismantling }}</label> <label class="badge rounded-pill bg-warning">{{ $data->laporan_status ? : "ANTRIAN" }}</label></div>
  <div class="card-body">
  <div class="table-responsive">
  <table class="table">
  <tbody>
    <tr>
      <td>WFM ID / SC / INET</td>
      <td>:</td>
      <td>
        {{ $data->wfm_id }} / 
        {{ $data->order_id ? : '-' }} / 
        {{ $data->internet ? : '-' }}
        </td>
    </tr>
    <tr>
      <td>ORDER STATUS / ADDRESS / DATE PS</td>
      <td>:</td>
      <td>{{ $data->orderStatus ? : '-' }} / {{ $data->orderAddr ? : '-' }} / {{ $data->orderDatePs ? : '-' }}</td>
    </tr>
    <tr>
      <td>NAMA / PIC</td>
      <td>:</td>
      <td>{{ $data->nama_pelanggan }} / {{ $data->noPelangganAktif ? : $data->orderKontak ? : '0' }}</td>
    </tr>
    </tbody>
  </table>
  </div>
    <center>
    <a href="/{{ $data->id_dt }}" class="btn btn-danger" style="text-align: center; color: white">Take Order</a>
    </center>
  </div>
</div>
@endforeach
@endsection