@extends('new_tech_layout')

@section('content')
  @include('partial.alerts')
  @if($indexCount==1)
  <div class="row row-cols-2 row-cols-xs-6 row-cols-md-6 g-4" style="text-align: center;">
  <div class="col">
    <div class="card h-100 bg-default text-black shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">NP</h5>
      <div class="card-body">
        <h4>{{ $countNP }}</h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-primary text-white shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">OGP</h5>
      <div class="card-body">
        <h4>{{ $countOGP }}</h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-warning text-black shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">KENDALA</h5>
      <div class="card-body">
        <h4><a href="/workorder/KENDALA" style="color: black">{{ $countKENDALA }}</a></h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-info text-white shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">OK TARIK</h5>
      <div class="card-body">
        <h4><a href="/workorder/OKTARIK" style="color: white">{{ $countHR }}</a></h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-success text-white shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">UP</h5>
      <div class="card-body">
        <h4><a href="/workorder/UP" style="color: white">{{ $countUP }}</a></h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-danger text-white shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">UT ONLINE</h5>
      <div class="card-body">
        <h4>
        @if($countQC <> 0)
          <a href="/workorderQC?bulan=ALL" style="color: white">{{ $countQC }}</a>
        @else
          {{ $countQC }}
        @endif
        </h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-light text-dark shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">ONT PREMIUM</h5>
      <div class="card-body">
        <h4>
        @if($countOPO <> 0)
          <a href="/orderOntPremium" style="color: black">{{ $countOPO }}</a>
        @else
          {{ $countOPO }}
        @endif
        </h4>
      </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100 bg-dark text-white shadow p-2 mb-1 rounded">
    <h5 class="card-header bg-transparent">CABUT NTE</h5>
      <div class="card-body">
        <h4>
        @if($countCNO <> 0)
          <a href="/orderCabutanNte" style="color: white">{{ $countCNO }}</a>
        @else
          {{ $countCNO }}
        @endif
        </h4>
      </div>
    </div>
  </div>

</div>
<br /><br />

<div class="alert alert-info alert-dismissible fade show" role="alert">
  Prioritas Order Saat Ini : <strong>{{ $prioritas }}</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@if(session('auth')->hold_order == 1)
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  Seluruh Order Rekan di <strong>Tahan</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if($countQC <> 0)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  Rekan Mempunyai <strong>{{ $countQC }}</strong> Order <strong>UT Online</strong> ! Harap Segera Dikerjakan Ya Terimakasih
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if($countCNO <> 0)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  Rekan Mempunyai <strong>{{ $countCNO }}</strong> Order <strong>Dismantle / Cabut NTE</strong> ! Harap Segera Dikerjakan Ya Terimakasih
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if($countOPO <> 0)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  Rekan Mempunyai <strong>{{ $countOPO }}</strong> Order <strong>ONT Premium</strong> ! Harap Segera Dikerjakan Ya Terimakasih
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if(count($upload_event) == 0)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  Rekan Belum Melaporkan <strong><a href="/uploadFile" style="color: #842029;">Sertifikat Vaksin</a></strong> dan Apabila Sudah Harap Segera Dilaporkan
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@endif
<!-- Content Page -->

@if($indexCount == 1)
@foreach($checkONGOING as $ONGOING)
<?php
if($ONGOING->dispatch_by == 6){
    $order = 'INX'.$ONGOING->Ndem;
}
else{
    $order = $ONGOING->Ndem;
}
?>
<div class="card border-light mb-3 shadow p-3 mb-5 bg-white rounded" >
  <div class="card-header bg-transparent">
    <label class="badge rounded-pill bg-primary" style="color: white">{{ $order }}</label>
    <label class="badge rounded-pill bg-success" style="color: white">{{ $ONGOING->jenis_order }}</label>
    <label class="badge rounded-pill bg-warning">{{ $ONGOING->laporan_status }}</label>
    @if ($ONGOING->customer_assign == "CA_YES")
      <label class="badge rounded-pill bg-danger" style="color: white">MANJA {{ $ONGOING->ca_manja_time }}</label>
    @endif
    @if ($ONGOING->pelanggan_hvc == "YES")
      <label class="badge rounded-pill bg-info" style="color: white">HVC</label>
    @endif
    @if ($ONGOING->ket_plasa == "1")
      <label class="badge rounded-pill bg-info" style="color: white">PLASA</label>
    @endif
    @if ($ONGOING->ket_sqm == "1")
      <label class="badge rounded-pill bg-info" style="color: white">SQM</label>
    @endif
    @if ($ONGOING->ket_sales == "1")
      <label class="badge rounded-pill bg-info" style="color: white">SALES</label>
    @endif
    @if ($ONGOING->ket_helpdesk == "1")
      <label class="badge rounded-pill bg-info" style="color: white">HELPDESK</label>
    @endif
    @if ($ONGOING->ket_monet == "1")
      <label class="badge rounded-pill bg-info" style="color: white">MONET</label>
    @endif
  </div>
  <div class="card-body">
  <div class="table-responsive">
  <table class="table">
  <tbody style="text-align: center">
    <tr>
      <td>{{ $ONGOING->Customer_Name ? : $ONGOING->orderName ? : $ONGOING->nama_pelanggan ? : '-' }} / {{ $ONGOING->Contact_Phone ? : $ONGOING->orderKontak ? : '-' }}</td>
    </tr>
    <tr>
      <td>{{ $ONGOING->alamat ? : $ONGOING->orderAddr ? : $ONGOING->headline ? : '-' }}</td>
    </tr>
    </tbody>
  </table>
  </div>
    <center>
    @if ($ONGOING->dispatch_by == '6')
    <a href="/reboundary/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @elseif($ONGOING->jenis_order=="IN" || $ONGOING->jenis_order=="INT" || $ONGOING->jenis_order==NULL)
    <a href="/tiket/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @elseif($ONGOING->dispatch_by != '2')
    <a href="/{{ $ONGOING->id_dt }}" class="btn btn-warning" style="text-align: center; color: white">Take Order</a>
    @endif
    </center>
  </div>
</div>
@endforeach
<br />
@endif

@if($indexCount == 0)
<form method="get">
    Bulan :
    <input style="width: 65%;" type="month" name="bulan" id="bulan">
    <input type="submit" id="bt_bulan" value="Filter">
</form>
<br>
@endif

{{-- RULES HOLD ORDER --}}
@if(session('auth')->hold_order <> 1)

@foreach($list as $num => $data)
<?php
if ($data->jenisPsb<>NULL && $data->dispatch_by<>4){
$orderId = "SC.".$data->orderId;
$jenisWO = $data->jenisPsb;
} else if($data->orderId==null && $data->myir<>null) {
$orderId = "MYIR.".$data->myir;
$jenisWO = $data->jenis_layanan;
} else if ($data->jenisPsb<>NULL && $data->dispatch_by==4){
$orderId = "FG.".$data->orderId;
$jenisWO = $data->jenisPsb;
} else if ($data->ND<>NULL){
$orderId = $data->ND;
$jenisWO = "MIGRASI AS IS";
}else if(in_array($data->jenis_order, ["IN", "INT", NULL])) {
$orderId = $data->Incident;
$jenisWO = "ASSURANCE";
}else if(in_array($data->jenis_order, ["CABUT_NTE", "ONT_PREMIUM"])) {
$orderId = $data->Ndem;
$jenisWO = $data->jenis_order;
} else if ($data->datek=="ONTOFFLINE"){
$orderId = "";
$jenisWO = "ONT OFFLINE";
} else if ($data->dispatch_by==4){
$orderId = "FG.".$data->orderId;
$jenisWO = $data->jenisPsb;
} else if($data->dispatch_by==6) {
$orderId = 'INX'.$data->no_tiket;
$jenisWO = "REBOUNDARY";
};
?>

<div class="card border-light mb-3 shadow p-3 mb-5 bg-white rounded" >
  <div class="card-header bg-transparent">
    <label class="badge rounded-pill bg-primary" style="color: white">{{ $orderId }}</label>
    <label class="badge rounded-pill bg-success">{{ $jenisWO }}</label>
    @if ($data->by_Assigned == "CUSTOMERASSIGNED")
      <label class="badge rounded-pill bg-danger" style="color: white">MANJA</label>
    @endif
    @if ($data->roc_customer_assign == "CA_YES")
      <label class="badge rounded-pill bg-danger" style="color: white">MANJA {{ $data->ca_manja_time }}</label>
    @endif
    @if ($data->Customer_Type <> "")
      <label class="badge rounded-pill bg-danger" style="color: white">{{ $data->Customer_Type }}</label>
    @endif
    @if ($data->bundling_addon <> NULL)
      <label class="badge rounded-pill bg-info" style="color: white">BUNDLING ADD ON</label>
    @endif
    @if ($data->pelanggan_hvc == "YES")
      <label class="badge rounded-pill bg-info" style="color: white">HVC</label>
    @endif
    @if ($data->ket_plasa == "1")
      <label class="badge rounded-pill bg-info" style="color: white">PLASA</label>
    @endif
    @if ($data->ket_sqm == "1")
      <label class="badge rounded-pill bg-info" style="color: white">AUTOMATIC</label>
    @endif
    @if ($data->ket_sales == "1")
      <label class="badge rounded-pill bg-info" style="color: white">SALES</label>
    @endif
    @if ($data->ket_helpdesk == "1")
      <label class="badge rounded-pill bg-info" style="color: white">WHATSAPP</label>
    @endif
    @if ($data->ket_monet == "1")
      <label class="badge rounded-pill bg-info" style="color: white">IBOOSTER</label>
    @endif
    <label class="badge rounded-pill bg-warning" style="color: white">{{ $data->laporan_status ? : "ANTRIAN" }}</label>
  </div>
  <div class="card-body">
  <div class="table-responsive">
  <table class="table">
  <tbody>
    <tr>
      <td>ORDER / ORDER OPEN @if ($data->approve_date <> "") / TGL APPROVE @elseif ($data->jam_manja <> "") / TGL MANJA @endif</td>
      <td>:</td>
      <td>
        {{ $orderId ? : $data->no_tiket ? : $data->roc_no_tiket ? : $data->myir ? : @$data->mwnd }} / 
        @if ($data->no_tiket<>NULL)
        {{ $data->TROUBLE_OPENTIME ? : @$data->mwcreated_at ? : '-' }}
        @else
        {{ $data->orderDate ? : @$data->mwcreated_at ? : '-' }}
        @endif
        @if ($data->approve_date <> "") / {{ @$data->approve_date ? : '-' }} @elseif ($data->jam_manja <> "") / {{ $data->jam_manja }} @endif
        </td>
    </tr>
    @if(!in_array($data->jenis_order,["SC","MYIR"]))
    <tr>
      <td>NAMA / PIC</td>
      <td>:</td>
      <td>{{ @$data->customer ? : @$data->orderName ? : @$data->Customer_Name ? : @$data->mwnama ? : '-' }} / {{ @$data->picPelanggan ? : @$data->Contact_Phone ? : @$data->mwpic ? : @$data->roc_headline ? : '-' }}</td>
    </tr>
    @endif
    <tr>
    <tr>
      <td>ODP / STO / JENIS LAYANAN</td>
      <td>:</td>
      <td>{{ @$data->namaOdp ? : @$data->alproname ? : @$data->neRK ? : '-' }} / {{ $data->sto_dps ? : $data->neSTO ? : $data->sto  ? : @$data->mwmdf ? : '-' }} / {{ @$data->jenis_layanan ? : @$data->mwlayanan ? : @$data->nePRODUK_GGN ? : '-' }}</td>
    </tr>
    @if(!in_array($data->jenis_order,["SC","MYIR"]))
    <tr>
      <td>ALAMAT / LOKASI</td>
      <td>:</td>
      <td>{{ @$data->alamatSales ? : @$data->neHEADLINE ? : @$data->mwalamat ? : '-' }} / {{ @$data->koorPelanggan ? : @$data->mwalamat ? : '-' }}</td>
    </tr>
    @endif
    </tbody>
  </table>
  </div>
    <center>
    @if ($data->dispatch_by=='6')
    <a href="/reboundary/{{ $data->id_dt }}" class="btn btn-info" style="text-align: center; color: white">Take Order</a>
    @elseif($data->dispatch_by != '2')
    <a href="/{{ $data->id_dt }}" class="btn btn-info" style="text-align: center; color: white">Take Order</a>
    @else
    <a href="/tiket/{{ $data->id_dt }}" class="btn btn-info" style="text-align: center; color: white">Take Order</a>
    @endif
    </center>
  </div>
</div>
@endforeach

@endif
{{-- END RULES HOLD ORDER --}}
<!-- End Content Page -->
@endsection
@section('plugins')
<script type="text/javascript">
  $(function() {
      var day = {
          format : 'yyyy-mm',
          viewMode: "months",
          minViewMode: "months",
          autoClose: true
      };

      $('#bulan').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });
  })
</script>
@endsection