@extends('layout')
@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">SEARCH ALPRO</h3>
        <form method="GET">
                <input type="text" class="form-control" placeholder="Search ODC or ODP ..." name="alpro" id="alpro" />
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">RESULT CUSTOMER BY ALPRO</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>CUSTOMER</th>
                        <th>SC</th>
                        <th>KCONTACT</th>
                        <th>NO HP</th>
                        <th>NO HP 2</th>
                        <th>NO INET</th>
                        <th>NO VOICE</th>
                        <th>ODP</th>
                        <th>ORDER STATUS</th>
                        <th>TGL PS</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $num => $result)
                    @if($result)                    
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>{{ $result->customer }}</td>
                            <td>{{ $result->sc }}</td>
                            <td>{{ $result->kcontact }}</td>
                            <td>{{ $result->no_hp }}</td>
                            <td>{{ $result->no_hp2 }}</td>
                            <td>{{ $result->no_inet }}</td>
                            <td>{{ $result->no_voice }}</td>
                            <td>{{ $result->odp }}</td>
                            <td>{{ $result->orderStatus }}</td>
                            <td>{{ $result->tgl_ps }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>CUSTOMER</th>
                        <th>SC</th>
                        <th>KCONTACT</th>
                        <th>NO HP</th>
                        <th>NO HP 2</th>
                        <th>NO INET</th>
                        <th>NO VOICE</th>
                        <th>ODP</th>
                        <th>ORDER STATUS</th>
                        <th>TGL PS</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL REPORT CUSTOMER BY ALPRO TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL REPORT CUSTOMER BY ALPRO TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'DETAIL REPORT CUSTOMER BY ALPRO TOMMAN'
                }
            ]
        });
    });
</script>
@endsection