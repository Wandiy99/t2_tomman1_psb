@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        YOUR POIN (RACING PS)
      </div>
      <div class="panel-body table-responsive">
        <table>
            <tr>
                <td width=200>NIK</td>
                <td width=40> : </td>
                <td>{{ $result->tech_nik  }}</td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>:</td>
                <td>{{ $result->nama }}</td>
            </tr>
            <tr>
                <td>AO VALID</td>
                <td>:</td>
                <td>{{ $result->order_ao_valid }} ({{$result->order_ao_valid * 4 }})</td>
            </tr>
            
            <tr>
                <td>SOLUSI KENDALA</td>
                <td>:</td>
                <td>{{ $result->order_solusi_kendala }} ({{ $result->order_solusi_kendala * 3 }})</td>
            </tr>
            
            <tr>
                <td>MO</td>
                <td>:</td>
                <td>{{ $result->order_mo }} ({{ $result->order_mo * 2 }})</td>
            </tr>
            
            <tr>
                <td>DISMANTLING</td>
                <td>:</td>
                <td>{{ $result->order_dismantling }} ({{ $result->order_dismantling * 1 }})</td>
            </tr>
            
            <tr>
                <td>FFG</td>
                <td>:</td>
                <td>{{ $result->order_ffg }} (-{{ $result->order_ffg * 4 }})</td>
            </tr>

            <tr>
                <td>POIN</td>
                <td>:</td>
                <td>{{ $result->point_now }}</td>
            </tr>

            <tr>
                <td>YOUR VIP VOUCHER</td>
                <td>:</td>
                <td>
                    @foreach ($query_voucher_vip as $vip)
                        - {{ strtoupper($vip->voucher_redeem) }}<br/>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>YOUR REGULER VOUCHER</td>
                <td>:</td>
                <td>
                    @foreach ($query_voucher_reguler as $reg)
                        {{ strtoupper($reg->voucher_redeem) }}
                    @endforeach
                </td>
            </tr>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection