@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">DETAIL PS AO KPRO</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NO_INTERNET</th>
                        <th>SEKTOR</th>
                        <th>TIM</th>
                        <th>MITRA</th>
                        <th>SC</th>
                        <th>TGL_PS</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($getData as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->SPEEDY ? : '0' }}</td>
                        <td>{{ $data->sektor }}</td>
                        <td>{{ $data->tim ? : '#N/A' }}</td>
                        <td>{{ $data->mitra ? : '#N/A' }}</td>
                        <td>{{ $data->ORDER_ID ? : '0' }}</td>
                        <td>{{ $data->LAST_UPDATED_DATE ? : '#N/A' }}</td>
                        <td>{{ $data->status_resume ? : '#N/A' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL PS AO KPRO BY TOMMAN'
                }
            ]
        });
    });
</script>
@endsection