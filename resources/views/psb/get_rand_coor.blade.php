@extends('public_layout')
@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">RUTE PENARIKAN BERDASARKAN ODP</div>
      <div class="panel-body">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
        <link rel="stylesheet" href="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.css" />
        <style type="text/css">
          .leaflet-measure-path-measurement {
            font-size: 12px !important;
          }
        </style>
        <div class="col-sm-12">
            Lat : <span id="lat_text"></span>
            <br />
            Lon : <span id="lon_text"></span>
            <br />
            Jarak : <span id="jarak_text"></span> Meter
            <div id="map" style="height: 750px; margin-top: 10px;"></div>
        </div>

        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/examples/Control.Geocoder.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.js"></script>
        {{-- <script src="https://cdn.jsdelivr.net/npm/pixi.js@7.x/dist/browser/pixi.min.js"></script> --}}
        {{-- <script src="https://cdn.jsdelivr.net/npm/leaflet-pixi-overlay@1.8.2/L.PixiOverlay.min.js"></script> --}}
        <script type="text/javascript">
              $(function() {
                  var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                  token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
                  // mbUrl = `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${token}`,
                  // grayscale = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
                  // streets = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
                  // satellite = L.tileLayer(mbUrl, {id: 'mapbox/satellite-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
                  tiang_all = L.layerGroup(),
                  pLineGroup = L.layerGroup(),
                  zoom_me = 18,
                  googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
                      maxZoom: 20,
                      subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                  }),
                  map = L.map('map', {
                      center: ['-3.319000', '114.584100'],
                      zoom: zoom_me,
                      maxZoom: 20,
                      layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, pLineGroup, tiang_all]
                  }),
                  baseLayers = {
                      'Jalan': googleStreets,
                      'Satelit': googleSat,
                      'Satelit Dan Jalan': googleHybrid,
                      'Jalur': googleTerrain,
                      'Jalur 2': googleAlteredRoad,
                      'Lalu Lintas': googleTraffic,
                  },
                  overlays = {
                      'Tiang': tiang_all,
                      'Garis': pLineGroup,
                  },
                  circle = L.circle(map.getCenter(), {
                      radius: 500,
                      color: '#C0392B',
                      fillOpacity: 0,
                  }).addTo(map),
                  collect_odp = [],
                  load_collect_tiang = [],
                  totalDistance = [],
                  load_koor_poly = [],
                  load_distance = [],
                  sum = 0,
                  radius = 500.00,
                  data_tiang = null,
                  layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
                  home_customer = new L.Icon({
                      iconUrl: '/image/home_customer.png',
                      iconAnchor: [12, 41],
                      popupAnchor: [1, -34]
                  });
                  pole_telkom = new L.Icon({
                      iconUrl: '/image/pole_telkom.png',
                      iconAnchor: [12, 41],
                      popupAnchor: [1, -34]
                  });

                  odpMaster = {!! json_encode($data) !!}
                  minDc = {!! json_encode($minDc) !!}
                  maxDc = {!! json_encode($maxDc) !!}
                  console.log(minDc, maxDc)

                  var temp_lat = parseFloat(odpMaster.lat_odp),
                    temp_lng = parseFloat(odpMaster.lon_odp),
                    get_t = generateLocation(temp_lat, temp_lng, maxDc, minDc);

                    function generateLocation(latitude, longitude, max, min = 0) {
                        if (min > max) {
                            throw new Error(`min(${min}) cannot be greater than max(${max})`);
                        }

                        // earth radius in km
                        const EARTH_RADIUS = 6371;

                        // 1° latitude in meters
                        const DEGREE = EARTH_RADIUS * 2 * Math.PI / 360 * 1000;

                        // random distance within [min-max] in km in a non-uniform way
                        const maxKm = max * 1000;
                        const minKm = min * 1000;
                        const r = ((maxKm - minKm + 1) * Math.random() ** 0.5) + minKm;

                        // random angle
                        const theta = Math.random() * 2 * Math.PI;

                        const dy = r * Math.sin(theta);
                        const dx = r * Math.cos(theta);

                        let newLatitude = latitude + dy / DEGREE;
                        let newLongitude = longitude + dx / (DEGREE * Math.cos(deg2rad(latitude)));
                        let textlatlng = `${newLatitude},${newLongitude}`;

                        const distance = getDistanceFromLatLonInKm(latitude, longitude, newLatitude, newLongitude);

                        return {
                            newLatitude,
                            newLongitude,
                            textlatlng,
                            distance: Math.round(distance)
                        };
                    }

                  function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
                      var R = 6371; // Radius of the earth in km
                      var dLat = deg2rad(lat2-lat1);  // deg2rad below
                      var dLon = deg2rad(lon2-lon1);
                      var a =
                          Math.sin(dLat/2) * Math.sin(dLat/2) +
                          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                          Math.sin(dLon/2) * Math.sin(dLon/2)
                          ;
                      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                      var d = R * c; // Distance in km
                      return d;
                  }

                  function deg2rad(deg) {
                      return deg * (Math.PI/180)
                  }

                $('#lat_text').html(`${get_t.newLatitude}`);
                $('#lon_text').html(`${get_t.newLongitude}`);

        var myRouter = L.Routing.control({
            waypoints: [
                // L.latLng(-3.4127080943070816,114.66433575817811),
                L.latLng(get_t.newLatitude, get_t.newLongitude),
                L.latLng(temp_lat, temp_lng)
            ],

            // router: L.Routing.mapbox(token, {
            //     language: 'id',
            //     urlParameters: {
            //         vehicle: 'foot'
            //     }
            // }),
          geocoder: L.Control.Geocoder.nominatim(),
            routeWhileDragging: true,
            reverseWaypoints: true,
            showAlternatives: true,
            collapsible: true,
            autoRoute: true,
            routeWhileDragging: true,
        }).on('routeselected', function(e) {
            var route = e.route,
            jarak = e.route.summary.totalDistance,
            waktu = e.route.summary.totalTime;
            $('#jarak_text').html(`${jarak}`);
            console.log(jarak, waktu)
            // alert('Showing route between waypoints:\n' + JSON.stringify(route.inputWaypoints, null, 2));
        }).addTo(map);
              });
        </script>
      </div>
    </div>
  </div>
  @endsection