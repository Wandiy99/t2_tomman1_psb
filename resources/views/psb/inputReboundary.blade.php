@extends('tech_layout')

<div id="mapModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat ODP (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapView" style="height:400px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarker" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
  </div>

  <div id="mapModalPel" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Koordinat Pelanggan (<span id="lonText">0</span>, <span id="latText">0</span>)
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="mapViewPel" style="height:400px;"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnGetMarkerPel" class="btn btn-primary">OK</button>
                </div>
            </div>
        </div>
  </div>
@section('content')
<style>
.panel-info>.panel-heading {
    color: #ffffff;
    background-color: #19a2d6;
    border-color: #cba4dd;
}
</style>
  @include('partial.alerts')
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="id_dispatch" id="dispatch_id" value="{{ $dispatch_teknisi_id }}" />
    @if (isset($data->id_pl))
      <input type="hidden" name="id" value="{{ $data->id_pl }}" />
    @endif
    <h3>
      <a href="/" class="btn btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
      </a>
      @if(session('auth')->level <> 19)
        <button class="btn btn-primary">Simpan</button>
      @endif
      {{-- UPDATE WO // STEP {{ $project->step_id }} // {{ $project->step_name }} --}}
      UPDATE WO // STEP {{ $step_id }} // {{ $step_name }}

    </h3>

    <div class="row" style="padding:10px">
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">Workorder Information</div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label" for="input-status"><b>Manajemen Janji</b></label><br />
              <label class="label label-warning" for="input-status">MANJA OK</label><br />
              <textarea id="input-manja" name="manja" class="form-control" rows="4">{{ $project->manja }}</textarea>
            </div>
            <div class="form-group">
              <label class="control-label" for="input-status"><b>Status</b></label>
              <input name="status" type="hidden" id="input-status" class="form-control" value="{{ $data->status_laporan or '' }}" />
              {!! $errors->first('status', '<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group" id="hilang">
              <label class="control-label" for="tbTiang"><b>Tambah Tiang</b></label>
              <input name="tbTiang" type="hidden" id="tbTiang" class="form-control" value="" />
              {!! $errors->first('tbTiang', '<span class="label label-danger">:message</span>') !!}
            </div>

            @if($dataReboundary=='')
                <div class="form-group">
            	    <label class="control-label" for="input-idpel"> {{ $project->ND ? : $project->mND ?  : $project->ndemPots ? : '0' }} ~ {{ $project->ND_Speedy ? : $project->ND_REFERENCE ? : $project->mND_Speedy ? : $project->ndemSpeedy ? : '0' }}<br />
                  <b>Pelanggan : {{ $project->Nama ? : $project->NAMA ? : $project->mNama ? : $project->orderName ? : $project->customer}}</b></label><br />
                  {{ $project->MDF ? : $project->mMDF ? : $project->sto ? : $project->STO }}<br />
                  {{ $project->Kcontact?:$project->mAlamat ? : $project->KcontactSC ? : $project->LVOIE  }}<br />
                  <b>Alamat Sales : {{ $project->alamatSales }} </b><br />

                  @if (!empty($project->myir))
                    <b>MYIR : {{ $project->myir }} </b>
                  @endif
                </div>
            @else
              <div class="form-group">
                  <b>Tiket : </b>INX{{ $dataReboundary->Ndem }} ~ {{ $dataReboundary->ndemPots }} ~ {{ $dataReboundary->ndemSpeedy }} ~ {{ $dataReboundary->orderNcli }} ~ {{ $dataReboundary->noTelp }} <br>
                  <b>Pelanggan : </b>{{ $dataReboundary->orderName }} <br>
                  <b>Alamat : </b>{{ $dataReboundary->orderAddr }} <br>
                  {{ $dataReboundary->kcontact }}
                </div>
            @endif

            <div class="form-group">
              <label class="control-label"><b>Catatan Pemasangan</b></label>
              <textarea name="catatan" class="form-control" rows="5" id="catatanPasang">{{ $data->catatan or '' }}</textarea>
              {!! $errors->first('catatan','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>No. Hp Pelanggan Aktif</b></label>
              <input name="noPelangganAktif" class="form-control" rows="1" value="{{ old('noPelangganAktif') ?: @$data->noPelangganAktif ?: '' }}">
              {!! $errors->first('noPelangganAktif','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group" id="catatanRevoke">
              <label class="control-label"><b>Catatan Revoke</b></label>
              <textarea name="catatanRevoke" class="form-control" rows="5" id='inputCatatanRevoke'>{{ $data->catatanRevoke or '' }}</textarea>
              {!! $errors->first('catatanRevoke','<span class="label label-danger">:message</span>') !!}
            </div>

            @if($dataReboundary=='')
                <div class="row">
                  <div class="col-xs-12"><label for="" class="control-label"><b>Koordinat Pelanggan</b></label></div>
                  <div class="col-xs-10">
                  <input name="kordinat_pelanggan" id="kordinat_pelanggan" class="form-control" rows="1" value="{{ old('kordinat_pelanggan') ?: @$data->kordinat_pelanggan ?: ''}}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" />
                  </div>
                    
                      <div class="col-xs-2">
                        <button id="btn-gps" type="button" class="btn btn-sm btn-info">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        </button>
                    </div>

                    <!-- <div class="col-xs-2">
                      <button id="btnLoadMapPel" title="Beri tanda pada Peta" class="btn btn-primary" data-toggle="tooltip" type="button">
                          <i class="glyphicon glyphicon-map-marker"></i>
                      </button>
                    </div>  -->

                  <div class="col-xs-10">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div>
                </div>
            @else
                <div class="row">
                  <div class="col-xs-12"><label for="" class="control-label"><b>Koordinat Pelanggan</b></label></div>
                  <div class="col-xs-10">
                  <input name="kordinat_pelanggan" id="kordinat_pelanggan" class="form-control" rows="1" value="{{ old('kordinat_pelanggan') ?: $dataReboundary->koor_pelanggan ?: ''}}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" />
                  </div>
                    
                      <div class="col-xs-2">
                        <button id="btn-gps" type="button" class="btn btn-sm btn-info">
                          <i class="glyphicon glyphicon-map-marker"></i>
                        </button>
                    </div>

                    <!-- <div class="col-xs-2">
                      <button id="btnLoadMapPel" title="Beri tanda pada Peta" class="btn btn-primary" data-toggle="tooltip" type="button">
                          <i class="glyphicon glyphicon-map-marker"></i>
                      </button>
                    </div>  -->

                  <div class="col-xs-10">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div>
                </div>
            @endif


            <div class="row">
              <div class="col-xs-12"><label for="" class="control-label"><b>Koordinat ODP</b></label></div>
               <!-- <div class="col-xs-4">
                  <select name="statusodp" id="status_odp">
                    <option value="ODP_muncul">ODP muncul</option>
                    <option value="ODP_tidak_muncul">ODP Tidak muncul</option>
                  </select>
                </div> -->

              <div class="col-xs-12">

              @if ($project->kordinat_odp=='')
                   <input name="kordinat_odp" id="input-koordinat" class="form-control" placeholder="Koordinat" rows="1" value="{{ $project->lat }}, {{ $project->lon }}" />
                    {!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}
              @else
                   <input name="kordinat_odp" id="input-koordinat" class="form-control" placeholder="Koordinat" rows="1" value="{{ $project->kordinat_odp }}"  />
                    {!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}
              @endif

              </div>

              <!-- <div class="col-xs-2">
                <button id="btnLoadMap" title="Beri tanda pada Peta" class="btn btn-default" data-toggle="tooltip" type="button">
                  <i class="glyphicon glyphicon-map-marker"></i>
                </button>
              </div> --> 

            </div>
            <div class="form-group">
              <label class="control-label"><b>Nama ODP</b></label>
              <!--<input name="nama_odp" class="form-control" rows="1" value="{{ old('nama_odp') ?: @$data->nama_odp ?: '' }}">-->
              <input type="text" name="nama_odp" id="odp2" class="form-control" rows="1" value="{{ old('nama_odp') ?: @$data->nama_odp ?: '' ?: $project->namaOdp}}" {{-- readonly --}} />
              {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group">
              <label class="control-label"><b>Dropcore Label Code</b></label>
              <input name="dropcore_label_code" class="form-control" rows="1" value="{{ old('dropcore_label_code') ?: @$data->dropcore_label_code ?: '' }}">
            </div>
            <div class="row">
              <div class="col-xs-12"><label for="" class="control-label"><b>Redaman Embassy</b></label></div>
              <div class="col-xs-10">
                <input name="redaman" id="redaman" class="form-control" rows="1" value="{{ old('redaman') ?: @$data->redaman ?: '' }}" readonly />
                {!! $errors->first('redaman','<span class="label label-danger">:message</span>') !!}
              </div>
              <div class="col-xs-2">
                <button id="btn-redaman" type="button" class="btn btn-sm btn-info">
                  <i class="glyphicon glyphicon-refresh"></i>
                </button>
              </div>
              {{-- <div class="col-xs-10">{!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}</div> --}}
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">Material & NTE</div>
          <div class="panel-body">
            <input type="hidden" name="materials" value="[]" />
        	  @if(session('auth')->level <> 19)
              <button data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info" type="button">
                <span class="glyphicon glyphicon-list"></span>
                Edit
              </button>
        	  @endif
            
            <div class="form-group">
              <label class="control-label" for="input-ont">TYPE ONT</label>
                <input type="text" name="type_ont" id="input-type-ont" class="form-control" value="{{ $data->typeont ? : '' }}"/>
                {!! $errors->first('type_ont','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label" for="sn_ont">SN ONT</label>
              <input type="text" name="sn_ont" id="sn_ont" class="form-control" value="{{ $data->snont }}"/>
              {!! $errors->first('sn_ont', '<span class="label label-danger">:message</span>') !!}
            </div>
            
            <div class="form-group">
              <label class="control-label" for="input-ont">TYPE STB</label>
              <input type="text" name="type_stb" id="input-type-stb" class="form-control" value="{{ $data->typestb ? : '' }}"/>
              {!! $errors->first('type_stb','<span class="label label-danger">:message</span>') !!}
            </div>
            
            <div class="form-group">
              <label class="control-label" for="input-ont">SN STB</label>
              <input type="text" name="sn_stb" id="input-sn-stb" class="form-control" value="{{ $data->snstb }}"/>
              {!! $errors->first('sn_stb','<span class="label label-danger">:message</span>') !!}
            </div>

            <ul id="material-list" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="id_item"></strong>
                <p v-text="nama_item"></p>
                 <p v-text="rfc"></p>
                {{-- <strong v-text="barangId"></strong>
                <p v-text="barang"></p> --}}
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          Log
        </div>
        <div class="panel-body">
          <ul class="list-group">
          @foreach ($data_log as $log)
            <li class="list-group-item">
              {{ $log->created_by }} - {{ $log->nama }}<br />
              {{ $log->laporan_status }} - {{ $log->created_at }}<br />
              {{ $log->catatan }}
            </li>
          @endforeach
          </ul>
        </div>
      </div>
    </div>

      <div class="col-sm-12">

      @if ($project->id_dshr<>NULL)
        <div class="panel panel-info">
          <div class="panel-heading">
            Informasi Sales
          </div>
          <div class="panel-body">
        <center><label class="control-label"><b></b></label></center>
        <div class="col-sm-3  text-center input-photos" style="margin: 20px 0">

          <?php
            $path_KTP = "/upload/dshr/".$project->id_dshr."/Form_Pelanggan";
            $th_KTP   = "$path_KTP-th.jpg";
            $img_KTP  = "$path_KTP.jpg";
          ?>
          @if (file_exists(public_path().$img_KTP))
          <a href="{{ $img_KTP }}">
            <img src="{{ $th_KTP }}" alt="Foto FORM Pelanggan" />

          </a><br />Form Pelanggan
          @endif
        </div>
        <div class="col-sm-3  text-center input-photos" style="margin: 20px 0">

          <?php
            $path_KTP = "/upload/dshr/".$project->id_dshr."/Foto_Rumah_Pelanggan";
            $th_KTP   = "$path_KTP-th.jpg";
            $img_KTP  = "$path_KTP.jpg";
          ?>
          @if (file_exists(public_path().$img_KTP))
          <a href="{{ $img_KTP }}">
            <img src="{{ $th_KTP }}" alt="Foto_Rumah_Pelanggan" />

          </a><br />Foto Rumah Pelanggan
          @endif
        </div>
        <div class="col-sm-3  text-center input-photos" style="margin: 20px 0">

          <?php
            $path_KTP = "/upload/dshr/".$project->id_dshr."/KTP";
            $th_KTP   = "$path_KTP-th.jpg";
            $img_KTP  = "$path_KTP.jpg";
          ?>
          @if (file_exists(public_path().$img_KTP))
          <a href="{{ $img_KTP }}">
            <img src="{{ $th_KTP }}" alt="KTP" />

          </a><br />KTP
          @endif
        </div>
        <div class="col-sm-3  text-center input-photos" style="margin: 20px 0">

          <?php
            $path_KTP = "/upload/dshr/".$project->id_dshr."/Foto_ODP";
            $th_KTP   = "$path_KTP-th.jpg";
            $img_KTP  = "$path_KTP.jpg";
          ?>
          @if (file_exists(public_path().$img_KTP))
          <a href="{{ $img_KTP }}">
            <img src="{{ $th_KTP }}" alt="Foto ODP" />

          </a><br />Foto ODP
          @endif
        </div>
      </div>
    </div>

  </div>
      @endif
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">Dokumentasi</div>
          <div class="panel-body">

            <div class="row text-center input-photos" style="margin: 20px 0">
             <?php
              $number = 1;
              clearstatcache();
             ?>
             @foreach($photoInputs as $input)
              <div class="col-xs-6 col-sm-3">

                <?php
                  $path  = "/upload/evidence/{$project->id_dt}/$input";
                  $path2 = "/upload2/evidence/{$project->id_dt}/$input";
                  $th    = "$path-th.jpg";
                  $th2   = "$path2-th.jpg";
                  $img   = "$path.jpg";
                  $img2  = "$path2.jpg";
                  $flag  = "";
                  $name  = "flag_".$input;

                ?>
                @if (file_exists(public_path().$th))
                  <a href="{{ $img }}">
                    <img src="{{ $th }}" alt="{{ $input }}" width="100" />
                  </a>
                  <?php
                    $flag = 2;
                  ?>
                @elseif (file_exists(public_path().$th2))
                  <a href="{{ $img2 }}">
                    <img src="{{ $th2 }}" alt="{{ $input }}" width="100" />
                  </a>
                  <?php
                    $flag = 2;
                  ?>
                @else
                  <img src="/image/placeholder.gif" width="100" alt="" />
                @endif
                <br />
                <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
                <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
                <button type="button" class="btn btn-sm btn-info">
                  <i class="glyphicon glyphicon-camera"></i>
                </button>
                <p>{{ str_replace('_',' ',$input) }}</p>
                {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
              </div>
            <?php
              $number++;
            ?>
            @endforeach
            </div>
          </div>
      </div>
<!--
    <div class="row">
      <div class="col-xs-12"><label for="" class="control-label">Koordinat GPS</label></div>
      <div class="col-xs-10">

        <?php $labelGps = empty($data->gps_accuracy) ? '' : ('('.round($data->gps_accuracy).") {$data->gps_latitude} {$data->gps_longitude}") ?>
        <input name="label-gps" value="{{ $labelGps }}" class="form-control" />



				<input name="gps_accuracy" value="{{ $data->gps_accuracy or '' }}" type="hidden" />
        <input name="gps_latitude" value="{{ $data->gps_latitude or '' }}" type="hidden" />
        <input name="gps_longitude" value="{{ $data->gps_longitude or '' }}" type="hidden" />
      </div>
      <div class="col-xs-2">
        <button id="btn-gps" type="button" class="btn btn-sm btn-info">
          <i class="glyphicon glyphicon-map-marker"></i>
        </button>
      </div>
    </div>
-->
	@if(session('auth')->level <> 19)
    <div class="text-center" style="margin:40px 0 20px">
      <button class="btn btn-primary form-control">Simpan</button>
    </div>
	@endif
  </div>
  </div>
  <input type="hidden" name="jenis_layanan" value="{{ $project->jenis_layanan }}">
  <input type="hidden" name="orderId" value="{{ $project->orderId }}">
  </form>


  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material PSB</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>

          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data | hasSaldo">

              <strong v-text="id_item"></strong><br>
              <strong v-text="nama_item"></strong><br>
              <strong v-text="rfc"></strong><br>
              <p><strong>Saldo : </strong> <strong v-text="saldo"></strong></p>

              {{-- <strong v-text="barangId"></strong>
              <p v-text="barang"></p> --}}

              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                {{-- <button class="btn btn-default" type="button" v-text="qty | doubleDigit"></button> --}}
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <?php
      if(json_encode($project)==false){
          $getProject = [];
      }
      else{
          $getProject = json_encode($project);
      };
   ?>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
  <script src="/js/mapmarker.js"></script>
  <script src="/js/mapmarkerpel.js"></script>
  <script src="/bower_components/RobinHerbots-Inputmask/dist/jquery.inputmask.bundle.js"></script>
  <script>
    function getkey(e){
    if (window.event)
       return window.event.keyCode;
    else if (e)
       return e.which;
    else
       return null;
    }

    function goodchars(e, goods, field){
    var key, keychar;
    key = getkey(e);
    if (key == null) return true;

    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();
    goods = goods.toLowerCase();

    // check goodkeys
    if (goods.indexOf(keychar) != -1)
        return true;
    // control keys
    if ( key==null || key==0 || key==8 || key==9 || key==27 )
       return true;

    if (key == 13) {
        var i;
        for (i = 0; i < field.form.elements.length; i++)
            if (field == field.form.elements[i])
                break;
        i = (i + 1) % field.form.elements.length;
        field.form.elements[i].focus();
        return false;
        };
    // else return false
    return false;
    }

    $(function() {
      // tes insert tiang tambahan
      var data = <?= json_encode($getTambahTiang) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#tbTiang').select2(select2Options());

      $('#hilang').hide();
      $('#catatanRevoke').hide();
      $('#catatanPasang').removeAttr('readonly');
      $('#input-status').change(function(){
          if ($(this).val() == "11") {
              console.log(this.value);
              $('#hilang').show();
          }
          else {
            $('#hilang').hide();
          };

          if ($(this).val() == "43" || $(this).val() == "44"){
              $('#catatanRevoke').show();
              $('#catatanPasang').attr('readonly', true);
          }
          else{
            $('#catatanRevoke').hide();
            $('#catatanPasang').removeAttr('readonly');
            $('#inputCatatanRevoke').val('');
          }
      });

      //

      $("#odp2").inputmask("AAA-AAA-A{2,3}/999");
      var materials = <?= json_encode($materials) ?>;
      var dispId = <?= json_encode($dispatch_teknisi_id) ?>;
      var project = <?= json_encode($getProject) ?>;
      
      $('.modal-title').hide();
      $('#status_odp').on('change', function(){

        if(this.value == "ODP_tidak_muncul"){

          $("#input-koordinat").removeAttr("readonly");
          $("#odp2").removeAttr("readonly");
        }else{
          $("#input-koordinat").attr('readonly', true);
          $("#odp2").attr('readonly', true);
        }
      });

      $('#input-status').on('change', function(){
        if(this.value == "1"){
          $.ajax({
            url: "/cekredaman/"+project.ndemSpeedy,
            beforeSend: function( ) {
              $('#redaman').val('wait');
            }
          })
          .done(function( data ) {
            if ( console && console.log ) {
              $('#redaman').val(data);
            }
          });
        }
      });
      $('#btn-redaman').click(function(){
        $.ajax({
          url: "/cekredaman/"+project.ndemSpeedy,
          beforeSend: function( ) {
            $('#redaman').val('wait');
          }
        })
        .done(function( data ) {
          if ( console && console.log ) {
            $('#redaman').val(data);
          }
        });
      });

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0});
      });

      Vue.filter('hasSaldo', function(value) {
        return value.filter(function(a) { return a.saldo > 0});
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          onChange : function(item){
            if (item.qty > item.saldo) item.qty=item.saldo;
          },
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;

            if (item.qty > item.saldo) item.qty = item.qty-1;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      $('#kordinat_pelanggan').bind('copy paste', function (e) {
         e.preventDefault();
      });

      var $btnGps = $('#btn-gps');
      var $btnGpsODP = $('#btn-gps-odp');

      var $kordinat_pelanggan = $('#kordinat_pelanggan');
      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_pelanggan.val('ERROR');
          return;
        }

        $kordinat_pelanggan.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_pelanggan.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_pelanggan.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_pelanggan.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      var $kordinat_odp = $('#kordinat_odp');
      $btnGpsODP.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_odp.val('ERROR');
          return;
        }

        $kordinat_odp.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_odp.val(result.coords.latitude+','+result.coords.longitude);
/*
					$labelGps.val('(' + Math.round(result.coords.accuracy) + 'm) ' + result.coords.latitude + ' ' + result.coords.longitude);

          $('input[name=gps_accuracy]').val(result.coords.accuracy);
          $('input[name=gps_latitude]').val(result.coords.latitude);
          $('input[name=gps_longitude]').val(result.coords.longitude);
*/
        }, function(error) {
          $kordinat_odp.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_odp.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);

          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: item.rfc});
        });
        $('input[name=materials]').val(JSON.stringify(result));


      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });
      var data = <?= json_encode($get_laporan_status) ?>;
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status').select2(select2Options());

      var datatypeont = [
        {"id":"ZTEF660", "text":"ZTE F660"},
        {"id":"ZTEF609", "text":"ZTE F609"},
        {"id":"ZTEF821", "text":"ZTE F821"},
        {"id":"ALUG240WA", "text":"ALU G240WA (HITAM)"},
        {"id":"ALUI240WA", "text":"ALU I240WA (PUTIH)"},
        {"id":"HG82455A", "text":"HUAWEI HG82455A"},
        {"id":"H87Z5675M21", "text":"HUAWEI H87Z5675M21"},
      ];

      var typeont = function() {
        return {
          data: datatypeont,
          placeholder: 'Input Type ONT',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#input-type-ont').select2(typeont());

      var datatypestb = [
        {"id":"ZTEB700V5", "text":"<span class='label label-success'>ZTE STB HD</span>"},
        {"id":"ZTEB760H", "text":"<span class='label label-success'>ZTE STB HYBRID</span>"},
        {"id":"ZTEB660H", "text":"<span class='label label-success'>ZTE STB 4K</span>"},
      ];

      var typestb = function() {
        return {
          data: datatypestb,
          placeholder: 'Input Type STB',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#input-type-stb').select2(typestb());

      var data =  <?= json_encode($nte) ?>;
      var nte1 =  <?= json_encode($nte1) ?>;
      $.extend(data, nte1);
      var combo = $('#input-nte').select2({
        data: data,
        maximumSelectionSize: 2,
        multiple:true
      });
      $('#searchlist').btsListFilter('#searchinput', {itemChild: 'strong'});

    })
  </script>
@endsection
