@extends('new_tech_layout')

@section('content')
@include('partial.alerts')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body table-responsive">
                <form method="post">
                    <table class="table table-bordered" style="text-align: center">
                        <thead>
                            <tr>
                                <th>SC</th>
                                <th>INET</th>
                                <th>VOICE</th>
                                <th>CUSTOMER</th>
                                <th>STO</th>
                                <th>MITRA</th>
                                <th>TIM</th>
                                <th>DATE PS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                                <tr>
                                    <td>{{ $d->sc }}</td>
                                    <td>{{ $d->no_inet }}</td>
                                    <td>{{ $d->no_voice ? : '-' }}</td>
                                    <td>{{ $d->nama }}</td>
                                    <td>{{ $d->sto }}</td>
                                    <td>{{ $d->mitra ? : '-' }}</td>
                                    <td>{{ $d->tim ? : '-' }}</td>
                                    <td>{{ $d->dtPs }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection