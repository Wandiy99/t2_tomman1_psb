@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .red {
      background-color: #FF0000;

    }
    .warning1 {
      background-color: #f39c12;
    }
    .warning2 {
      background-color: #e67e22;
    }
    .warning3 {
      background-color: #e74c3c;
    }


    .default {
      background-color: #888888;
    }
    .line {
      border-radius: 5px;
      padding : 3px;
    }

    .alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}
  </style>
  <ul class="nav nav-tabs" style="margin-bottom:20px">
  <li class="{{ (Request::path() == '/') ? 'active' : '' }}"><a href="/">NEED PROGRESS</a></li>
  <li class="{{ (Request::segment(2) == 'KENDALA') ? 'active' : '' }}"><a href="/workorder/KENDALA">KENDALA</a></li>
  <li class="{{ (Request::segment(2) == 'UP') ? 'active' : '' }}"><a href="/workorder/UP">UP</a></li>
  <li class="{{ (Request::segment(1) == 'ibooster') ? 'active' : '' }}"><a href="/ibooster">IBOOSTER</a></li>
  <li class="{{ (Request::segment(1) == 'searchbarcode') ? 'active' : '' }}"><a href="/searchbarcode">BARCODE</a></li>
  <li class="{{ (Request::segment(3) == 'history_reject QC') ? 'active' : '' }}"><a href="/viewreject">HISTORY REJECT QC</a></li>
  <li class="{{ (Request::segment(4) == 'splitter_gendong') ? 'active' : '' }}"><a href="/input-spl-gendong/form">SPLITTER GENDONG</a>
    <!-- <li><a href="/stok">Materials/NTE</a></li> -->
  </ul>


<div class="alert">
  
  Hubungi TL atau admin terkait untuk redispatch tiket yang telah di reject QC,
  Abaikan jika sudah UP
</div>

  <div class="content" >
  <div class="panel panel-default" style="padding:20px">
    <table class="table">
                      <tr>
                        <td width=10>No</td>
                        <td>SC</td>
                        <td>Keterangan</td>
                        <td>Tanggal Reject/Redispatch</td>
                        <td>Status Laporan</td>
                      </tr>


                      @foreach ($regux as $num=>$qc)
                      <tr>
                        
                        <td>{{ ++$num }}</td>
                        <td> <a href="/{{ $qc->id_dt }}"> <span style="color:#0000ff;" >{{ $qc->sc ? : 'NOTSET' }} </span></a></td>
                        <td>{{ $qc->log }}</td>
                        <td>{{ $qc->updated_at }}</td>
                        <td>{{ $qc->laporan_status ? : 'NOTSET' }}</td>                   
                      </tr>
                      @endforeach
    </table>
    </div>
  </div>
@endsection
