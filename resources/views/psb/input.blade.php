@extends($layout)
@section('content')
  <style>
    .panel-info>.panel-heading {
      color: #ffffff;
      background-color: #19a2d6;
      border-color: #cba4dd;
    }

    .imgx {
      display: block;
      margin-left: auto;
      margin-right: auto;
      border-radius: 8px;
      width: 100px;
      height: 100px
    }

    .imgxx {
      display: block;
      margin-left: auto;
      margin-right: auto;
      border-radius: 8px;
      width: 100px;
      height: 100px;
      border: 5px solid #2ECC71;
    }
    .centered {
      text-align: center;
    }
  </style>
  @include('partial.alerts')
  <link rel="stylesheet" href="/css/mapbox-gl.css" />
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/combine/npm/sweetalert2@10.13.0/dist/sweetalert2.min.css,npm/sweetalert2@10.13.0/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
  <link rel="stylesheet" href="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.css" />
  <div id="mapMarkerModal" class="modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
          <h4 class="modal-title">
            <span id="mapMarkerModal_title"></span>(<span id="mapMarkerModal_latText">0</span>, <span id="mapMarkerModal_lngText">0</span>)
          </h4>
        </div>
        <div class="modal-body">
          <div id="mapMarkerModal_mapView"></div>
        </div>
        <div id="mapMarkerModal_footer" class="modal-footer">
          <button class="btn btn-warning pull-left" data-dismiss="modal" type="reset">
            <i class="fa fa-ban"></i>
            <span>Batal</span>
          </button>
          <button style="margin-right: 20px" id="mapMarkerModal_gpsBtn" class="btn btn-default" type="button">
            <i class="fa fa-map-marker"></i>
            <span>GPS</span>
          </button>
          <button id="mapMarkerModal_okBtn" class="btn btn-primary" data-dismiss="modal" type="button">
            <i class="fa fa-check"></i>
            <span>OK</span>
          </button>
        </div>
      </div>
    </div>
  </div>
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Demi Allah saya nyatakan bahwa data yang saya update di Tomman benar adanya sesuai kondisi di lapangan. </strong>
  </div>
  @if(in_array($getStepId, ['1.4', '1.3.1']))
    @if ($redamanIboster == NULL)
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Ukur Redaman IBOOSTER WO sebelum di UP</strong>
    </div>
    @else
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Ukur Redaman IBOOSTER {{ $redamanIboster  }}, Order akan dikembalikan jika redaman tidak layak</strong>
    </div>
    @endif
  @endif
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="id_dispatch" id="dispatch_id" value="{{ $dispatch_teknisi_id }}" />
    <input type="hidden" name="jenis_order" value="{{ @$data->jenis_order }}" />
    <input type="hidden" name="startLatTechnition" id="startLat">
    <input type="hidden" name="startLonTechnition" id="startLon">
    @if (isset($data->id_pl))
      <input type="hidden" name="id" value="{{ $data->id_pl }}" />
    @endif

    <div class="row" style="padding:10px">
      <div class="col-sm-12">
        <p>
          <a href="/" class="btn btn-rounded btn-sm btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span>
          </a>
          {{-- <a href="/sendtobotkpro/{{ $project->id_dt }}" class="btn btn-default">
            SEND TO KPRO
          </a> --}}

          {{-- @if(in_array($getStepId, ['1.4', '1.3.1']))
              @if($project->internet<>'')
                <a href="/grabIboosterbySc/{{ $Ndem }}/psb" class="btn btn-success">Ukur Ibooster</a>
              @else
                <a href="/grabIboosterbyNotSc/{{ $Ndem }}/psb" class="btn btn-danger">Ukur Ibooster</a>
              @endif
          @endif --}}
          @if (session('auth')->level != 19)
            <button class="btn btn-rounded btn-sm btn-success"><span class="glyphicon glyphicon-floppy-saved"></span>&nbsp; SAVE</button>
          @endif
          @if (session('auth')->level != 10)
            <button data-toggle="modal" data-target="#modal-log-dispatch" class="btn btn-rounded btn-sm btn-info" type="button"><span class="glyphicon glyphicon-list-alt"></span>&nbsp; LOG</button>
          @endif
          <button data-toggle="modal" data-target="#modal-info-sales" class="btn btn-rounded btn-sm btn-primary" type="button"><span class="glyphicon glyphicon-picture"></span>&nbsp; SALES</button>
          UPDATE WO // STEP {{ $step_id }} // {{ $step_name }}
        </p>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">Workorder Information</div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label" for="input-status"><b>Manajemen Janji</b></label>&nbsp;<label class="label label-warning" for="input-status">MANJA OK</label><br />
              <textarea id="input-manja" name="manja" class="form-control" rows="4">{{ $project->manja ? : $get_detail_sc->appointment_desc }}</textarea>
            </div>
            <div class="form-group">
              <label class="control-label" for="input-status"><b>Status</b></label>
              <select class="form-control select2" name="status" id="input-status">
                <option value="" selected disabled>Pilih Status</option>
                @foreach ($get_laporan_status as $laporan_status)
                  <option data-subtext="description 1" value="{{ $laporan_status->id }}" data-foo="{{ $laporan_status->definisi_status }}" <?php if (@$laporan_status->id==@$data->status_laporan) { echo "Selected"; } else { echo ""; } ?>>{{ @$laporan_status->text }}</option>
                @endforeach
              </select>
              {!! $errors->first('status', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group" id="dismantling-status">
              <label class="control-label" for="dismantling-status"><b>Penyebab Berhenti Berlangganan</b></label>
              <select class="form-control select2" name="dismantling_status" id="dismantling-status">
                <option value="" selected disabled>Pilih Penyebab</option>
                @foreach ($get_dismantling_status as $ds)
                  <option data-subtext="description 1" value="{{ $ds->id }}" <?php if (@$ds->id==@$data->dismantling_status) { echo "Selected"; } else { echo ""; } ?>>{{ @$ds->text }}</option>
                @endforeach
              </select>
              {!! $errors->first('dismantling_status', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group" id="scBaru">
              <label class="control-label"><b>SC Baru</b></label>
                <input type="number" name="SC_New" class="form-control" rows="1" value="{{ old('SC_New') ?: @$data->SC_New ?: '' ?: $project->SC_New }}" />
              {!! $errors->first('SC_New', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group" id="estimasiPending">
              <label class="control-label"><b>Estimasi Pending Pengerjaan</b></label>
              <div class="input-group">
                <input type="text" class="form-control" id="datepicker-autoclose" name="estimasiPending" placeholder="yyyy-mm-dd"> <span class="input-group-addon"><i class="icon-calender"></i></span>
              </div>
              {!! $errors->first('estimasiPending', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group" id="odpHilang">
              <label class="control-label">ODP ALTERNATIF</label>
              @if($odpAlternatif<>'')
                @foreach($odpAlternatif as $odp)
                  <ul>
                    <li>{{ $odp->odp_name }}</li>
                  </ul>
                @endforeach
              @endif
            </div>
            <div class="form-group" id="hilang">
              <label class="control-label" for="tbTiang"><b>Tambah Tiang</b></label>
              <input name="tbTiang" type="text" id="tbTiang" class="form-control" value="" />
              {!! $errors->first('tbTiang', '<span class="label label-danger">:message</span>') !!}
            </div>
            <div id="field-odpLoss">
              <div class="form-group" >
                <label class="control-label" for="jenisOdp">Jenis ODP</label>
                <input type="text" name="jenisOdp" id="jenisodp" class="form-control" value="{{ $data->jenis_odp }}">
                {!! $errors->first('jenisOdp','<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group" >
                <input type="text" name="redamanInOdp" id="redamanInOdp" class="form-control" placeholder="Redaman IN ODP Diisi Wal!!!" value = "{{ $data->redaman_in }}">
                {!! $errors->first('redamanInOdp','<span class="label label-danger">:message</span>') !!}
              </div>
            </div>

            <div class="table-responsive">
              <table class="table">
              <tbody>
                <tr>
                  <td>SC</td>
                  <td>:</td>
                  <td>{{ $project->sc ? : $project->NdemId ? : $project->order_id ? : '-' }}</td>
                </tr>
                <tr>
                  <td>Nama Pelanggan</td>
                  <td>:</td>
                  <td>
                    @php
                      $expl = explode(' ', strtoupper($project->orderName ? : $project->customer ? : $project->cno_pelanggan));
                      $text = array_map(function($x){
                          return preg_replace("/(?<=.{0}).(?=.{3})/", "*", $x);
                      }, $expl);
                      $customer_name = implode(' ', $text);
                    @endphp
                    {{ $customer_name }}
                  </td>
                </tr>

                <tr>
                  <td>EMAIL</td>
                  <td>:</td>
                  <td>{{ $project->email ? : @$foto_cno->email ? : '-' }}</td>
                </tr>

                <tr>
                  <td>NO KTP</td>
                  <td>:</td>
                  <td>{{ $project->no_ktp ? : '-' }}</td>
                </tr>

                <tr>
                  <td>NCLI</td>
                  <td>:</td>
                  <td>{{ $project->orderNcli ? : @$foto_cno->orderNcli }}</td>
                </tr>

                <tr>
                  <td>POTS</td>
                  <td>:</td>
                  <td>{{ $project->noTelp ? : $project->no_telp ? : @$foto_cno->noTelp }}</td>
                </tr>
                <tr>
                  <td>Internet</td>
                  <td>:</td>
                  <td>{{ $project->internet ? : $project->no_internet ? : @$foto_cno->internet }}</td>
                </tr>
                <tr>
                  <td>STO</td>
                  <td>:</td>
                  <td>{{ $project->csto ? : @$foto_cno->sto }}</td>
                </tr>
                <tr>
                  <td>ODP</td>
                  <td>:</td>
                  <td>{{ $project->calproname ? : $project->alproname ? : $project->namaOdp ? : @$foto_cno->alproname }}</td>
                </tr>
                <tr>
                  <td>KORD. PEL</td>
                  <td>:</td>
                  <td>{{ $project->latSC ? : @$foto_cno->lat }},{{ $project->lonSC ? : @$foto_cno->lon }}</td>
                </tr>
                <tr>
                  <td>ALAMAT</td>
                  <td>:</td>
                  @php
                    $get_alamat = explode(',',$project->orderAddr ? : @$foto_cno->orderAddr);
                    $alamat = '';
                    if (count($get_alamat)>0)
                    {
                      $alamat .= 'Jl.'.@$get_alamat[2].' ';
                      $alamat .= 'NO.'.@$get_alamat[3].' ';
                      $alamat .= @$get_alamat[5].' ';
                      $alamat .= @$get_alamat[6].' ';
                      $alamat .= @$get_alamat[1].' ';
                      $alamat .= @$get_alamat[0].' ';
                    }
                    else
                    {
                      $alamat = '-';
                    }

                    $expl = explode(' ', strtoupper($alamat));
                    $text = array_map(function($x){
                        return preg_replace("/(?<=.{0}).(?=.{3})/", "*", $x);
                    }, $expl);
                    $alamat = implode(' ', $text);
                  @endphp
                  <td>{{ $alamat }}</td>
                </tr>
                <tr>
                  <td>PIC</td>
                  <td>:</td>
                  <td>{{ strtoupper($project->picPelanggan) ? : @$foto_cno->orderKontak }}</td>
                </tr>
                <tr>
                  <td>ALAMAT SALES</td>
                  <td>:</td>
                  <td>{{ strtoupper($project->alamatSales) }}</td>
                </tr>
                <tr>
                  <td>KCONTACT</td>
                  <td>:</td>
                  <td>{{ $project->KcontactSC ? : @$foto_cno->kcontact }}</td>
                </tr>
              </tbody>
              </table>
            </div>

            <div class="form-group">
              <label class="control-label"><b>Catatan Pemasangan</b></label>
              <textarea name="catatan" class="form-control" rows="5" id="catatanPasang">{{ $data->catatan or '' }}</textarea>
              {!! $errors->first('catatan','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label" for="input-hd-manja">HD MANJA</label>
                <select class="input-hd-manja form-control" name="hd_manja">
                  <option value="" selected disabled>Pilih HD Manja</option>
                  @foreach ($get_hd_manja as $hd_manja)
                    <option data-subtext="description 1" value="{{ $hd_manja->id }}" <?php if (@$hd_manja->id == @$data->hd_manja) { echo "Selected"; } else { echo ""; } ?>>{{ @$hd_manja->text }}</option>
                  @endforeach
                </select>
              {!! $errors->first('hd_manja','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label" for="input-hd-fallout">HD FALLOUT</label>
                <select class="input-hd-fallout form-control" name="hd_fallout">
                  <option value="" selected disabled>Pilih HD Fallout</option>
                  @foreach ($get_hd_fallout as $hd_fallout)
                    <option data-subtext="description 1" value="{{ $hd_fallout->id }}" <?php if (@$hd_fallout->id == @$data->hd_fallout) { echo "Selected"; } else { echo ""; } ?>>{{ @$hd_fallout->text }}</option>
                  @endforeach
                </select>
              {!! $errors->first('hd_fallout','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>No. Hp Pelanggan Aktif</b></label>
              <input type="number" minlength="11" name="noPelangganAktif" class="form-control" rows="1" value="{{ old('noPelangganAktif') ?: @$data->noPelangganAktif ?: '' }}">
              {!! $errors->first('noPelangganAktif','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>Dropcore Label Code</b></label>
                <input name="dropcore_label_code" class="form-control" rows="1" value="{{ old('dropcore_label_code') ?: @$data->dropcore_label_code ?: '' }}" onKeyPress="return goodchars(event,'01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ',this)">
              {!! $errors->first('dropcore_label_code','<span class="label label-danger">:message</span>') !!}
            </div>
            <div class="form-group">
              <label class="control-label"><b>ODP Label Code</b></label>
                <input name="odp_label_code" class="form-control" rows="1" value="{{ old('odp_label_code') ?: @$data->odp_label_code ?: '' }}" onKeyPress="return goodchars(event,'01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ',this)">
              {!! $errors->first('odp_label_code','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>Port Number</b></label>
              <input name="port_number" class="form-control" rows="1" value="{{ old('port_number') ?: @$data->port_number ?: '' }}">
              {!! $errors->first('port_number','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group" id="catatanRevoke">
              <label class="control-label"><b>Catatan Revoke</b></label>
              <textarea name="catatanRevoke" class="form-control" rows="5" id='inputCatatanRevoke'>{{ $data->catatanRevoke or '' }}</textarea>
              {!! $errors->first('catatanRevoke','<span class="label label-danger">:message</span>') !!}
            </div>

            @if($layout == 'tech_layout')
              <div class="row">
                <div class="col-xs-12"><label class="control-label"><b>Koordinat Pelanggan</b></label></div>
                <div class="col-xs-10">
                @if(count($foto_cno)>0)
                  <input name="kordinat_pelanggan" id="kordinat_pelanggan" placeholder="Tag Koordinat Pelanggan" class="form-control" rows="1" value="{{ @$foto_cno->kordinat_pelanggan ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" />
                @else
                  <input name="kordinat_pelanggan" id="kordinat_pelanggan" placeholder="Tag Koordinat Pelanggan" class="form-control" rows="1" value="{{ old('kordinat_pelanggan') ? : @$data->kordinat_pelanggan ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly/>
                @endif
                </div>

                <div class="col-xs-2">
                  <button id="btn-koordinat" type="button" class="btn btn-sm btn-info show_modal_koor" data-titlenya="Koordinat Pelanggan" data-id_input="kordinat_pelanggan" data-toggle="modal" data-target="#mapMarkerModal">
                    <i class="glyphicon glyphicon-map-marker"></i>
                  </button>
                </div>

                {!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="row">
                <div class="col-xs-12"><label for="" class="control-label"><b>Koordinat ODP</b></label></div>
                <div class="col-xs-10">
                  @if ($project->kordinat_odp=='')
                    <input name="kordinat_odp" id="input-koordinat" placeholder="Tag Koordinat ODP" class="form-control" placeholder="Koordinat" rows="1" value="{{ $project->lat }}, {{ $project->lon }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly/>
                        {!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}
                  @else
                    <input name="kordinat_odp" id="input-koordinat" placeholder="Tag Koordinat ODP" class="form-control" placeholder="Koordinat" rows="1" value="{{ $project->kordinat_odp ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly/>
                    {!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}
                  @endif
                </div>
                <div class="col-xs-2">
                  <button id="btn-koordinat-odp" type="button" class="btn btn-sm btn-info show_modal_koor" data-titlenya="Koordinat ODP" data-id_input="input-koordinat" data-toggle="modal" data-target="#mapMarkerModal">
                    <i class="glyphicon glyphicon-map-marker"></i>
                  </button>
                </div>
              </div>
            @else
              <div class="form-group">
                <label for="kordinat_pelanggan">Koordinat Pelanggan</label>
                <div class="input-group">
                  @if(count($foto_cno)>0)
                    <input type="text" class="form-control" name="kordinat_pelanggan" id="kordinat_pelanggan" placeholder="Tag Koordinat Pelanggan" value="{{ @$foto_cno->kordinat_pelanggan ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)">
                  @else
                    <input type="text" class="form-control" name="kordinat_pelanggan" id="kordinat_pelanggan" placeholder="Tag Koordinat Pelanggan" value="{{ old('kordinat_pelanggan') ? : @$data->kordinat_pelanggan ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly>
                  @endif
                  <div class="input-group-addon show_modal_koor" id="btn-koordinat" data-titlenya="Koordinat Pelanggan" data-id_input="kordinat_pelanggan" data-toggle="modal" data-target="#mapMarkerModal"><i class="ti-location-pin"></i></div>
                </div>
                {!! $errors->first('kordinat_pelanggan', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label"><b>Koordinat Foto Lokasi</b></label>
                  <input class="form-control" value="{{ @$data->lat_lokasi ? : '-' }}, {{ @$data->lng_lokasi ? : '-' }}" readonly>
              </div>
              <div class="form-group">
                <label class="control-label"><b>Koordinat Foto Lokasi 1</b></label>
                  <input class="form-control" value="{{ @$data->lat_lokasi_1 ? : '-' }}, {{ @$data->lng_lokasi_1 ? : '-' }}" readonly>
              </div>
              <div class="form-group">
                <label class="control-label"><b>Koordinat Foto Lokasi 2</b></label>
                  <input class="form-control" value="{{ @$data->lat_lokasi_2 ? : '-' }}, {{ @$data->lng_lokasi_2 ? : '-' }}" readonly>
              </div>

              <div class="form-group">
                <label for="kordinat_pelanggan">Koordinat ODP</label>
                <div class="input-group">
                  @if($project->kordinat_odp == '')
                    <input type="text" class="form-control" name="kordinat_odp" id="input-koordinat" placeholder="Tag Koordinat ODP" value="{{ $project->lat }}, {{ $project->lon }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly>
                  @else
                    <input type="text" class="form-control" name="kordinat_odp" id="input-koordinat" placeholder="Tag Koordinat ODP" value="{{ $project->kordinat_odp ? : ',' }}" onKeyPress="return goodchars(event,'0123456789-,. ',this)" readonly>
                  @endif
                  <div class="input-group-addon show_modal_koor" id="btn-koordinat-odp" data-titlenya="Koordinat ODP" data-id_input="input-koordinat" data-toggle="modal" data-target="#mapMarkerModal"><i class="ti-location-pin"></i></div>
                </div>
                {!! $errors->first('kordinat_odp', '<span class="label label-danger">:message</span>') !!}
              </div>
            @endif

            <div class="form-group">
              <label class="control-label"><b>Detek</b></label>
              <textarea name="detek" minlength="15" class="form-control" rows="5" >{{ $data->detek or '' }}</textarea>
              {!! $errors->first('detek','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>Valins ID</b></label>
              <input type="number" minlength="6" maxlength="6" name="valins_id" class="form-control" rows="1" value="{{ old('valins_id') ?: @$data->valins_id ?: '' }}">
              {!! $errors->first('valins_id','<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label" for="input-splitter"><b>Jenis Splitter</b></label>
              <select class="form-control select2" name="splitter" id="input-splitter">
                <option value="" selected disabled>Pilih Splitter</option>
                @foreach ($get_splitter as $spl)
                  <option data-subtext="description 1" value="{{ $spl->id }}" <?php if (@$spl->id==@$data->splitter) { echo "Selected"; } else { echo ""; } ?>>{{ @$spl->text }}</option>
                @endforeach
              </select>
              {!! $errors->first('splitter', '<span class="label label-danger">:message</span>') !!}
            </div>

            <div class="form-group">
              <label class="control-label"><b>Nama ODP</b></label><br />
              <input type="text" minlength="14" name="nama_odp" id="odp2" class="form-control" rows="1" value="{{ old('nama_odp') ?: @$data->nama_odp ?: '' ?: $project->namaOdp}}" />
              {!! $errors->first('nama_odp', '<span class="label label-danger">:message</span>') !!}
            </div>

            @if(in_array(session('auth')->id_karyawan, [18950509, 20931249, 20981020]))
              <div class="form-group">
                <label class="control-label"><b>Nama ODP Plan</b></label>
                  <input type="text" name="odp_plan" class="form-control" rows="1" value="{{ old('odp_plan') ?: @$data->odp_plan ?: '' ?: $project->odp_plan}}" />
                {!! $errors->first('odp_plan', '<span class="label label-danger">:message</span>') !!}
              </div>
            @endif

            @if($layout == 'tech_layout')
              <div class="row">
                <div class="col-xs-12"><label for="" class="control-label"><b>Redaman Embassy</b></label></div>
                <div class="col-xs-10">
                  <input name="redaman" id="redaman" class="form-control" rows="1" value="{{ old('redaman') ?: $redamanIboster ?: @$data->redaman ?: '' }}" readonly />
                  <!-- {!! $errors->first('redaman','<span class="label label-danger">:message</span>') !!} -->
                </div>
                <div class="col-xs-2">
                  <button id="btn-redaman" type="button" class="btn btn-sm btn-info">
                    <i class="glyphicon glyphicon-refresh"></i>
                  </button>
                </div>
              </div>
            @else
              <div class="form-group">
                  <label for="redaman">Redaman Embassy</label>
                  <div class="input-group">
                      <input type="text" class="form-control" name="redaman" id="redaman" placeholder="Loading ..." value="{{ old('redaman') ?: $redamanIboster ?: @$data->redaman ?: '' }}" readonly>
                      <div class="input-group-addon"><i class="ti-reload"></i></div>
                  </div>
              </div>
            @endif

            <div class="form-group">
              <label class="control-label"><b>Redaman ODP</b></label>
              <input type="text" name="redamanOdp" class="form-control" value="{{ old('redamanOdp') ?: @$data->redaman_odp }}">
              {!! $errors->first('redamanOdp','<span class="label label-danger">:message</span>') !!}
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading">Material & NTE</div>
          <div class="panel-body">
        	  @if(session('auth')->level <> 19)
              <input type="hidden" name="materials" value="[]" />
              <button data-toggle="modal" data-target="#material-modal" class="btn btn-rounded btn-sm btn-info" type="button">
                <span class="glyphicon glyphicon-plus"></span>
                INPUT
              </button>
              {{-- <input type="hidden" name="materialsNte" value="[]" />
              <button data-toggle="modal" data-target="#material-modal-nte" class="btn btn-rounded btn-sm btn-primary" type="button">
                <span class="glyphicon glyphicon-edit"></span>
                N T E
              </button> --}}
              @if(count($checkListMaterials)>0)
                <button data-toggle="modal" data-target="#modal-delete-materials" class="btn btn-rounded btn-sm btn-danger" type="button">
                  <span class="glyphicon glyphicon-trash"></span>
                  DELETE
                </button>
              @endif
        	  @endif

            <br /><br />
            <div class="form-group">
              <label class="control-label">NO RFC</label>
              <input type="text" minlength="5" name="rfc_number" class="form-control" value="{{ $data->rfc_number }}"/>
              {!! $errors->first('rfc_number', '<span class="label label-danger">:message</span>') !!}
            </div>

            {{-- @if (@$cek_dispatch->jenis_order == 'CABUT_NTE' || @$cek_dispatch->jenis_layanan == 'CABUT_NTE') --}}
              <div class="form-group">
                <label class="control-label" for="input-ont"><b>Type ONT</b></label>
                <select class="form-control select2" name="type_ont" id="input-ont">
                  <option value="" selected disabled>Pilih Tipe ONT</option>
                  @foreach ($jenis_ont as $jont)
                  <option data-subtext="description 1" value="{{ $jont->id }}" <?php if (@$jont->id==@$data->typeont) { echo "Selected"; } else { echo ""; } ?>>{{ @$jont->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_ont', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="sn_ont">SN ONT</label>
                <input type="text" name="sn_ont" id="sn_ont" class="form-control" value="{{ $data->snont }}"/>
                {!! $errors->first('sn_ont', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-stb"><b>Type STB</b></label>
                <select class="form-control select2" name="type_stb" id="input-stb">
                  <option value="" selected disabled>Pilih Tipe STB</option>
                  @foreach ($jenis_stb as $jstb)
                    <option data-subtext="description 1" value="{{ $jstb->id }}" <?php if (@$jstb->id==@$data->typestb) { echo "Selected"; } else { echo ""; } ?>>{{ @$jstb->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_stb', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-stb">SN STB</label>
                <input type="text" name="sn_stb" id="input-sn-stb" class="form-control" value="{{ $data->snstb }}"/>
                {!! $errors->first('sn_stb','<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-plc"><b>Type PLC</b></label>
                <select class="form-control select2" name="type_plc" id="input-plc">
                  <option value="" selected disabled>Pilih Tipe PLC</option>
                  @foreach ($jenis_plc as $jplc)
                    <option data-subtext="description 1" value="{{ $jplc->id }}" <?php if (@$jplc->id==@$data->type_plc) { echo "Selected"; } else { echo ""; } ?>>{{ @$jplc->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_plc', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-plc">SN PLC</label>
                <input type="text" name="sn_plc" id="input-sn-plc" class="form-control" value="{{ $data->sn_plc }}"/>
                {!! $errors->first('sn_plc','<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-wifiext"><b>Type WIFI EXT</b></label>
                <select class="form-control select2" name="type_wifiext" id="input-wifiext">
                  <option value="" selected disabled>Pilih Tipe WIFI EXT</option>
                  @foreach ($jenis_wifiext as $jwifiext)
                    <option data-subtext="description 1" value="{{ $jwifiext->id }}" <?php if (@$jwifiext->id==@$data->type_wifiext) { echo "Selected"; } else { echo ""; } ?>>{{ @$jwifiext->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_wifiext', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-wifiext">SN WIFI EXT</label>
                <input type="text" name="sn_wifiext" id="input-sn-wifiext" class="form-control" value="{{ $data->sn_wifiext }}"/>
                {!! $errors->first('sn_wifiext','<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-indibox"><b>Type INDIBOX</b></label>
                <select class="form-control select2" name="type_indibox" id="input-indibox">
                  <option value="" selected disabled>Pilih Tipe INDIBOX</option>
                  @foreach ($jenis_indibox as $jindibox)
                    <option data-subtext="description 1" value="{{ $jindibox->id }}" <?php if (@$jindibox->id==@$data->type_indibox) { echo "Selected"; } else { echo ""; } ?>>{{ @$jindibox->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_indibox', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-indibox">SN INDIBOX</label>
                <input type="text" name="sn_indibox" id="input-sn-indibox" class="form-control" value="{{ $data->sn_indibox }}"/>
                {!! $errors->first('sn_indibox','<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-indihomesmart"><b>Type INDIHOME SMART</b></label>
                <select class="form-control select2" name="type_indihomesmart" id="input-indihomesmart">
                  <option value="" selected disabled>Pilih Tipe INDIHOME SMART</option>
                  @foreach ($jenis_indihomesmart as $jindihomesmart)
                    <option data-subtext="description 1" value="{{ $jindihomesmart->id }}" <?php if (@$jindihomesmart->id==@$data->type_indihomesmart) { echo "Selected"; } else { echo ""; } ?>>{{ @$jindihomesmart->text }}</option>
                  @endforeach
                </select>
                {!! $errors->first('type_indihomesmart', '<span class="label label-danger">:message</span>') !!}
              </div>

              <div class="form-group">
                <label class="control-label" for="input-indihomesmart">SN INDIHOME SMART</label>
                <input type="text" name="sn_indihomesmart" id="input-sn-indihomesmart" class="form-control" value="{{ $data->sn_indihomesmart }}"/>
                {!! $errors->first('sn_indihomesmart','<span class="label label-danger">:message</span>') !!}
              </div>
            {{-- @endif --}}

            <div class="form-group">
              <label class="control-label" for="jenisMaterial">JENIS MATERIAL</label>
              <div class="radio-list">
                @if($data->material_psb == "DC_ROLL")
                  @php
                    $chkd_dcroll = 'checked';
                    $chkd_precon = '';
                  @endphp
                @elseif($data->material_psb == "PRECON")
                  @php
                    $chkd_dcroll = '';
                    $chkd_precon = 'checked';
                  @endphp
                @else
                  @php
                    $chkd_dcroll = '';
                    $chkd_precon = '';
                  @endphp
                @endif
                  <input type="hidden" name="material_psb" value="" />
                  <label class="radio-inline p-0">
                    <div class="radio radio-info">
                      <input {{ $chkd_dcroll }} type="radio" name="material_psb" class="jenisMaterial" value="DC_ROLL">
                      <label for="DC_ROLL">DC ROLL</label>
                    </div>
                  </label>
                  <label class="radio-inline">
                    <div class="radio radio-info">
                      <input {{ $chkd_precon }} type="radio" name="material_psb" class="jenisMaterial" value="PRECON">
                      <label for="PRECON">PRECON</label>
                    </div>
                  </label>
              </div>
              {!! $errors->first('material_psb','<span class="label label-danger">:message</span>') !!}
            </div>

            <div id="field-dc-roll">
              <div class="form-group">
                <label class="control-label" for="dc_roll"><b>AC-OF-SM-1B</b></label>
                <input type="number" name="dc_roll" class="form-control" value="{{ @$dc_roll->qty ? : '0' }}"/>
              </div>
            </div>

            <div id="field-precon">
            <input type="hidden" name="materials2" value="[]" />
              <button data-toggle="modal" data-target="#material-modal2" class="btn btn-primary btn-block" type="button">
                Pilih Precon
              </button>
              <br /><br />
            </div>

            <ul id="material-list" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="id_item"></strong>
                <p v-text="nama_item"></p>
              </li>
            </ul>

            <ul id="material-list-nte" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="jenis_nte"></strong>
                <p v-text="id"></p>
              </li>
            </ul>

            <ul id="material-list2" class="list-group">
              <li class="list-group-item" v-repeat="$data | hasQty ">
                <span class="badge" v-text="qty"></span>
                <strong v-text="id_item"></strong>
                <p v-text="nama_item"></p>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {{-- @if (in_array($project->dtjl_jenis_transaksi, ['AO', 'BYOD', 'PDA', 'WMS', 'WMS_LITE']) && in_array($project->dispatch_sto, ['AMT','BBR','BJM','BLC','BRI','BTB','GMB','KDG','KIP','KPL','KYG','LUL','MRB','MTP','NEG','PGN','PGT','PLE','RTA','SER','STI','TJL','TKI','ULI']))
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">RUTE PENARIKAN</div>
            <div class="panel-body">
              <style type="text/css">
                .leaflet-measure-path-measurement {
                  font-size: 12px !important;
                }
              </style>

              <div class="col-sm-12 row">
                <input type="hidden" name="koordinat_tiang" id="koordinat_tiang"/>
                <div class="col-sm-4">
                  <a type="button" class="btn btn-sm btn-rounded btn-block btn-warning undo_btn"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Undo</a>
                </div>

                <div class="col-sm-4">
                  <a type="button" href="/reset_rute_tarikan/{{ $id }}" class="btn btn-sm btn-rounded btn-block btn-danger"><span class="glyphicon glyphicon-trash"></span>&nbsp; Delete</a>
                </div>

                {!! $errors->first('koordinat_tiang', '<span class="label label-danger">:message</span>') !!}

                <div class="col-sm-4">
                  <button data-toggle="modal" data-target="#modal-map-legenda" class="btn btn-sm btn-rounded btn-block btn-info" type="button"><span class="glyphicon glyphicon-info-sign"></span>&nbsp; Legenda</button>

                  <div id="modal-map-legenda" class="modal">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4>Map Legenda</b></h4>
                        </div>
                        <div class="modal-body" style="overflow-y:auto">
                          <img src="/image/home_customer.png" alt="Rumah Pelanggan" width="20" height="20">&nbsp;Rumah Pelanggan
                          <br /><br />
                          <img src="https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png" alt="ODP" width="20" height="20">&nbsp;ODP
                          <br /><br />
                          <img src="https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png" alt="Tiang" width="20" height="20">&nbsp;Tiang
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-sm btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <input type="hidden" name="estimasi_jarak_rute" id="estimasi_jarak_rute">
                  <p class="text-muted estimasi_jarak"></p>
                  <input type="hidden" name="deviasi_order" id="deviasi_order">
                  <p class="text-muted deviasi_order_rute"></p>
                </div>
              </div>
              <div class="col-sm-12">
                <div id="map" style="height: 750px; margin-top: 10px;"></div>
              </div>
            </div>
          </div>
        </div>
      @endif --}}
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">Dokumentasi</div>
          <div class="panel-body">
             @php
              $number = 1;
              clearstatcache();
             @endphp
            <div class="row">
              @foreach($photoInputs as $input)
                @if($layout=="tech_layout")
                  @php
                    $class = "col-xs-6 col-md-3";
                  @endphp
                @else
                  @php
                    $class = "col-4 col-md-2";
                  @endphp
                @endif
                <div class="{{ $class }} input-photos">
                  @php
                    $path1 = "/upload/evidence/{$project->id_dt}";
                    $th1   = "$path1/$input-th.jpg";
                    $path2 = "/upload2/evidence/{$project->id_dt}";
                    $th2   = "$path2/$input-th.jpg";
                    $path3 = "/upload3/evidence/{$project->id_dt}";
                    $th3   = "$path3/$input-th.jpg";
                    $path4 = "/upload4/evidence/{$project->id_dt}";
                    $th4   = "$path4/$input-th.jpg";
                    $path  = null;

                    if(@file_exists(public_path().$th1) )
                    {
                      $path = "$path1/$input";
                    }
                    elseif(@file_exists(public_path().$th2) )
                    {
                      $path = "$path2/$input";
                    }
                    elseif(@file_exists(public_path().$th3) )
                    {
                      $path = "$path3/$input";
                    }
                    elseif(@file_exists(public_path().$th4) )
                    {
                      $path = "$path4/$input";

                    }
                    $th    = "$path-th.jpg";
                    $img   = "$path.jpg";
                    $flag  = "";
                    $name  = "flag_".$input;
                  @endphp
                  @if($input=="Berita_Acara" || $input=="BA_Digital" || $input=="RFC_Form" || $input=="Profile_MYIH" || $input=="Surat_Pernyataan_Deposit" || $input=="Proses_TTD_Surat_Pernyataan" || $input=="Lokasi" || $input=="Meteran_Rumah" || $input=="ID_Listrik_Pelanggan" || $input=="Foto_Pelanggan_dan_Teknisi" || $input=="ODP" || $input=="Redaman_ODP" || $input=="Live_TV" || $input=="TVOD" || $input=="Speedtest" || $input=="SN_ONT" || $input=="SN_STB" || $input=="Telephone_Incoming" || $input=="Telephone_Outgoing")
                    @if (file_exists(public_path().$th))
                      <a href="{{ $img }}">
                        <img class="imgxx" src="{{ $th }}?x={{ @filemtime(public_path().$th) }}" alt="{{ $input }}"/>
                      </a>
                      @php
                        $flag = 2;
                      @endphp
                    @else
                      <img class="imgxx" src="/image/placeholder.gif" alt="" />
                    @endif
                    @else
                    @if (file_exists(public_path().$th))
                      <a href="{{ $img }}">
                        <img class="imgx" src="{{ $th }}?x={{ filemtime(public_path().$th) }}" alt="{{ $input }}"/>
                      </a>
                      @php
                        $flag = 2;
                      @endphp
                    @else
                      <img class="imgx" src="/image/placeholder.gif" alt="" />
                    @endif
                  @endif
                  @php
                    $title = str_replace('_',' ',$input);
                  @endphp
                  <center>
                    {{ $title }}
                    <br />
                    <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
                    <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
                    <button type="button" class="btn btn-sm btn-info">
                      <i class="glyphicon glyphicon-camera"></i>
                    </button>
                    @if(session('auth')->id_karyawan == '20981020')
                      @if (file_exists(public_path().$th))
                      &nbsp;
                      <a class="deleteFoto btn btn-sm btn-danger" data-id="{{ $project->id_dt }}" data-input="{{ $input }}"><span class="glyphicon glyphicon-trash"></span></a>
                      @endif
                    @endif
                    <br /><br />
                    {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
                  </center>
                </div>
                @php
                  $number++;
                @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      @if(session('auth')->level != 19)
        <div class="col-md-12">
          <button class="text-center btn btn-rounded btn-block btn-success form-control"><span class="glyphicon glyphicon-floppy-saved"></span>&nbsp; SAVE</button>
        </div>
      @endif
    </div>
    <input type="hidden" name="jenis_layanan" value="{{ $project->jenis_layanan }}">
    <input type="hidden" name="orderId" value="{{ $project->orderId }}">
  </form>
  <div id="material-modal" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material Tambahan PSB</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong><br>
              <strong v-text="nama_item"></strong><br>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
              <strong v-text="unit_item"></strong><br>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="material-modal2" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Material Precon PSB</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="id_item"></strong><br>
              <strong v-text="nama_item"></strong><br>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
              <strong v-text="unit_item"></strong><br>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="material-modal-nte" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>STOCK NTE</h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <div class="form-group">
            <input id="searchinput" class="form-control" type="search" placeholder="Search..." />
          </div>
          <ul id="searchlist" class="list-group">
            <li class="list-group-item" v-repeat="$data">
              <strong v-text="jenis_nte"></strong><br>
              <strong v-text="id"></strong><br>
              <div class="input-group" style="width:150px">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onMinus(this)">
                    <span class="glyphicon glyphicon-minus"></span>
                  </button>
                </span>
                <input v-on="change: onChange(this)" v-model="qty" style="border-top: 1px solid #eeeeee" class="form-control text-center" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" v-on="click: onPlus(this)">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>
                </span>
              </div>
            </li>
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-delete-materials" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>DELETE MATERIALS ID : <b>{{ $id }}</b></h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th class="centered"><b>#</b></th>
                <th class="centered"><b>NAMA ITEM</b></th>
                <th class="centered"><b>QTY</b></th>
              </tr>
            </thead>
            @foreach ($checkListMaterials as $clm)
              <tr>
                <td class="centered"><a href="/deleteMaterials/delete/{{ $clm->id_plm }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a></td>
                <td class="centered">{{ $clm->id_item }}</td>
                <td class="centered">{{ $clm->qty }}</td>
              </tr>
            @endforeach
          </table>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-log-dispatch" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>LOG DISPATCH ORDER</b></h4>
        </div>
        <div class="modal-body" style="overflow-y:auto">
          <ul class="list-group">
            @foreach ($data_log as $log)
              <li class="list-group-item">
              {{ $log->created_by }} - {{ $log->created_name }}<br />
              {{ @$log->laporan_status ? : 'ANTRIAN' }} - {{ $log->created_at }}<br />
              {{ $log->catatan }}
              </li>
            @endforeach
          </ul>
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-info-sales" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>INFORMASI ORDER SALES</b></h4>
        </div>
        <div class="modal-body row" style="overflow-y:auto">
          @if ($onecall)
            @if($layout == "tech_layout")
              @php
                $class = "col-xs-6 col-md-3";
              @endphp
            @else
              @php
                $class = "col-md-6";
              @endphp
            @endif
            <div class="{{ $class }} input-photos" style="text-align: center;">
              @php
                $onecall_images = $onecall->images;
                $count = count($onecall_images);
              @endphp

              @for ($i = 0 ;$i < $count; $i++)
                <a href="{{ $onecall_images[$i] }}">
                  &nbsp;&nbsp; <img src="{{ $onecall_images[$i] }}" style="width: 100px; height: 100px"/>
                </a>
              @endfor
            </div>
          @else
            @foreach($photoInfo as $input)
              @if($layout == "tech_layout")
                @php
                  $class = "col-xs-6 col-md-3";
                @endphp
              @else
                @php
                  $class = "col-md-6";
                @endphp
              @endif
              <div class="{{ $class }} input-photos" style="text-align: center;">
                @php
                  $path  = "/".$table->public."/plasaSales/$idMyir/$input";
                  $th    = "$path-th.jpg";
                  $img   = "$path.jpg";
                @endphp
                @if (file_exists(public_path().$th))
                  <a href="{{ $img }}">
                    <img src="{{ $th }}" alt="{{ $input }}" style="width: 100px; height: 100px"/>
                  </a>
                @else
                  <img src="/image/placeholder.gif" style="width: 100px; height: 100px" alt="" />
                @endif
                <br />
                <p>{{ str_replace('_',' ',strtoupper($input)) }}</p>
              </div>
            @endforeach
          @endif
        </div>
        <div class="modal-footer" style="background: #eee">
          <button class="btn btn-rounded btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script src="/js/mapbox-gl.js"></script>
  <script src="/js/jquery.min.js"></script>
  <script src="/bower_components/vue/dist/vue.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.13.0/dist/sweetalert2.all.min.js"></script>
  <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/bower_components/bootswatch-dist/js/bootstrap.min.js"></script>
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSn96DCIJdATC6AHuV3sLF3ddwdaIsW10"></script>
  <script src="/js/mapmarker.js"></script>
  <script src="/js/mapmarkerpel.js"></script> --}}
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
  <script src="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.min.js"></script>
  <script src="/bower_components/leaflet-routing-machine-3.2.12/examples/Control.Geocoder.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.js"></script>
  {{-- <script src="https://cdn.jsdelivr.net/npm/pixi.js@7.x/dist/browser/pixi.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/leaflet-pixi-overlay@1.8.2/L.PixiOverlay.min.js"></script> --}}
  <script>
    function getkey(e){
      if (window.event){
        return window.event.keyCode;
      }
      else if (e){
        return e.which;
      }
      else{
        return null;
      }
    }

    function goodchars(e, goods, field){
      var key, keychar;
      key = getkey(e);
      if (key == null) return true;

      keychar = String.fromCharCode(key);
      keychar = keychar.toLowerCase();
      goods = goods.toLowerCase();

      // check goodkeys
      if (goods.indexOf(keychar) != -1)return true;
      // control keys
      if ( key==null || key==0 || key==8 || key==9 || key==27 ) return true;

      if (key == 13){
        var i;
        for (i = 0; i < field.form.elements.length; i++)
          if (field == field.form.elements[i])
            break;
        i = (i + 1) % field.form.elements.length;
        field.form.elements[i].focus();
        return false;
      };
      return false;
    }

    window.onload = function() {
      var startPos,
      geoOptions = {
        enableHighAccuracy: true
      },
      geoSuccess = function(position) {
        startPos = position;
        document.getElementById('startLat').value = startPos.coords.latitude;
        document.getElementById('startLon').value = startPos.coords.longitude;
      },
      geoError = function(error) {
        console.log('Error occurred. Error code: ' + error.code);
      };

      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    };

    function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
      var R = 6371; // Radius of the earth in km
      var dLat = deg2rad(lat2-lat1);  // deg2rad below
      var dLon = deg2rad(lon2-lon1);
      var a =
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
          Math.sin(dLon/2) * Math.sin(dLon/2)
          ;
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c; // Distance in km
      return d;
    }

    $(function() {
      $('.deleteFoto').click( function() {
        var data_id = $(this).attr('data-id'),
        data_input = $(this).attr('data-input');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: "<a href='/deleteFoto/" + data_id + "/" + data_input +"' style='color: white;'>Yes, delete it!</a>"
        });
      });

      var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
      tiang_all = L.layerGroup(),
      odp_all = L.layerGroup(),
      kosong = L.layerGroup(),
      pLineGroup = L.layerGroup(),
      zoom_me = 18,
      googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      }),
      googleStreets2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleHybrid2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleSat2 = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTerrain2 = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleAlteredRoad2 = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
      }),
      googleTraffic2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      }),
      map_tagging = L.map('mapMarkerModal_mapView', {
        center: ['-3.319000', '114.584100'],
        zoom: zoom_me,
        layers: [googleStreets2]
      }),
      baseLayers = {
        'Jalan': googleStreets,
        'Satelit': googleSat,
        'Satelit Dan Jalan': googleHybrid,
        'Jalur': googleTerrain,
        'Jalur 2': googleAlteredRoad,
        'Lalu Lintas': googleTraffic,
      },
      baseLayers2 = {
        'Jalan': googleStreets2,
        'Satelit': googleSat2,
        'Satelit Dan Jalan': googleHybrid2,
        'Jalur': googleTerrain2,
        'Jalur 2': googleAlteredRoad2,
        'Lalu Lintas': googleTraffic2,
      },
      overlays = {
        'Tiang': tiang_all,
        'Garis': pLineGroup,
      },
      overlays2 = {},
      collect_odp = [],
      load_collect_odp = [],
      collect_tiang = [],
      load_collect_tiang = [],
      totalDistance = [],
      load_koor_poly = [],
      load_distance = [],
      sum = 0,
      radius = 500.00,
      data_tiang = null,
      layerControl2 = L.control.layers(baseLayers2, overlays2, {position: 'topright'}).addTo(map_tagging),
      home_customer = new L.Icon({
        iconUrl: '/image/home_customer.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      });
      pole_telkom = new L.Icon({
        // iconUrl: '/image/pole_telkom.png',
        iconUrl: '/image/marker-icon-blue.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      odp_telkom = new L.Icon({
        iconUrl: '/image/marker-icon-green.png',
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      }),
      id_input = '',
      marker = ''
      isi_txt = '-3.319000,114.584100';

      $('.show_modal_koor').on('click', function(){
        var title= $(this).data('titlenya');
        id_input= '#'+$(this).data('id_input');
        isi_txt = $(id_input).val();

        var koor_now = isi_txt.split(',').map(x => x.trim() );

        if(koor_now[0].length == 0){
          koor_now = [-3.319000, 114.584100];
        }

        map_tagging.flyTo(koor_now, 16);

        if (map_tagging.hasLayer(marker) ){
          map_tagging.removeLayer(marker);
        }

        $('#mapMarkerModal_latText').html(koor_now[0]);
        $('#mapMarkerModal_lngText').html(koor_now[1]);

        marker = new L.marker(koor_now, {
          draggable: 'true'
        });

        map_tagging.addLayer(marker);

        marker.on('dragend', function (e) {
          var lat = marker.getLatLng().lat,
          lng = marker.getLatLng().lng;

          $('#mapMarkerModal_latText').html(lat);
          $('#mapMarkerModal_lngText').html(lng);

          $(id_input).val(`${lat}, ${lng}`);
        });

        $('#mapMarkerModal_title').html(title);
      })

      $('#mapMarkerModal').on('shown.bs.modal', function(){
        map_tagging.invalidateSize();
      })

      $('#mapMarkerModal_mapView').css({
        height: (window.innerHeight * .30) + 'px'
      })

      $('#mapMarkerModal_gpsBtn').on('click', function(){
        map_tagging.removeLayer(marker);

        navigator.geolocation.getCurrentPosition(position => {
          const { coords: { latitude, longitude }} = position;

          marker = new L.marker([latitude, longitude], {
            autoPan: true,
            draggable: 'true'
          }).addTo(map_tagging);

          $('#mapMarkerModal_latText').html(latitude);
          $('#mapMarkerModal_lngText').html(longitude);

          $(id_input).val(`${latitude}, ${longitude}`);

          map_tagging.flyTo([latitude, longitude], 16);
        })
      })

      function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
        var R = 6371, // Radius of the earth in km
        dLat = deg2rad(lat2-lat1),  // deg2rad below
        dLon = deg2rad(lon2-lon1),
        a =Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *Math.sin(dLon/2) * Math.sin(dLon/2),
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)),
        d = R * c; // Distance in km

        return d;
      }

      function deg2rad(deg) {
        return deg * (Math.PI/180)
      }

      // if ($('#map').length) {
      //   $.ajax({
      //     type: "GET",
      //     url: "/map/ajx_tiang_all",
      //     async: false,
      //     success: function(data){
      //       data_tiang = data
      //     }
      //   });

      //   // $.ajax({
      //   //   type: "GET",
      //   //   url: "/map/ajx_odp_all",
      //   //   async: false,
      //   //   success: function(data){
      //   //     data_odp = data
      //   //   }
      //   // });

      //   var map = L.map('map', {
      //     center: ['-3.319000', '114.584100'],
      //     zoom: zoom_me,
      //     maxZoom: 20,
      //     layers: [googleHybrid, odp_all, tiang_all, pLineGroup]
      //   }),
      //   layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
      //   circle = L.circle(map.getCenter(), {
      //     radius: 500,
      //     color: '#C0392B',
      //     fillOpacity: 0,
      //   }).addTo(map);

      //   map.on("moveend", function () {
      //     var lat = map.getCenter().lat,
      //     lng = map.getCenter().lng;

      //     circle.setLatLng(map.getCenter() );
      //     map._renderer._update();

      //     // $.each(data_odp, function(k, v){
      //     //   let dst = (getDistanceFromLatLonInKm(lat, lng, v.latitude, v.longitude) * 1000).toFixed(2);
      //     //   data_odp[k].distance = dst;
      //     // })

      //     if(collect_odp.length != 0)
      //     {
      //       $.each(collect_odp, function(k, v){
      //         if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
      //           map.removeLayer(v)
      //         }
      //       })

      //       $.each(odp_all._layers, function(k, v){
      //         if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
      //           delete odp_all._layers[k]
      //         }
      //       })
      //     }

      //     $.each(data_tiang, function(k, v){
      //       let dst = (getDistanceFromLatLonInKm(lat, lng, v.latitude, v.longitude) * 1000).toFixed(2);
      //       data_tiang[k].distance = dst;
      //     })

      //     if(collect_tiang.length != 0)
      //     {
      //       $.each(collect_tiang, function(k, v){
      //         if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
      //           map.removeLayer(v)
      //         }
      //       })

      //       $.each(tiang_all._layers, function(k, v){
      //         if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > radius ){
      //           delete tiang_all._layers[k]
      //         }
      //       })
      //     }

      //     // $.each(data_odp, function(k, v){
      //     //   var koor = `${v.latitude},${v.longitude}`;
      //     //   if(v.distance <= radius ) {
      //     //     if(load_collect_odp.indexOf(koor) === -1){
      //     //       var note = `<b>${v.odp_name}</b><br />Status : ${v.occ_2}`;

      //     //       collect_odp.push(L.marker([v.latitude, v.longitude], {icon: odp_telkom})
      //     //       .bindPopup(note)
      //     //       .addTo(odp_all)
      //     //       .on('click', function(e) {
      //     //         L.popup()
      //     //         .setLatLng([v.latitude, v.longitude])
      //     //         .setContent(note);
      //     //       }));
      //     //       load_collect_odp.push(koor)
      //     //     }
      //     //   }else{
      //     //     load_collect_odp = load_collect_odp.filter(function(e) { return e != koor })
      //     //   }
      //     // })

      //     $.each(data_tiang, function(k, v){
      //       var koor = `${v.latitude},${v.longitude}`;
      //       if(v.distance <= radius ){
      //         if(load_collect_tiang.indexOf(koor) === -1){
      //           var note = `Nama: <b>${v.nama}</b></br>ODC: <b>${v.odc}</b></br>STO: <b>${v.sto}</b>`;
      //           collect_tiang.push(L.marker([v.latitude, v.longitude], {icon: pole_telkom})
      //           .bindPopup(note)
      //           .addTo(tiang_all)
      //           .on('click', function(e) {
      //             let koor_now = `${e.latlng.lat}, ${e.latlng.lng}`;

      //             if(check_dup.indexOf(koor_now) === -1){
      //               check_dup.push(koor_now)
      //               load_koor.push([e.latlng.lat, e.latlng.lng])
      //               // console.log(load_koor)

      //               var polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
      //                 formatDistance: function(e){
      //                   // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
      //                   // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
      //                   return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
      //                 }
      //               }),
      //               marker = e.target._popup._source;

      //               polyline.getLatLngs().forEach(function (latLng) {
      //                 var latlng_text = latLng.lat +','+ latLng.lng;

      //                 if(load_distance.indexOf(latlng_text) === -1){
      //                   load_distance.push(latlng_text)
      //                 }

      //                 if (previousPoint.length != 0) {
      //                   if(koor_sc.split(',').map(x => x.trim() ).join(',') != latlng_text){
      //                     if(totalDistance.map(x => x.koor).indexOf(latlng_text) === -1){
      //                       totalDistance.push({
      //                         koor: latlng_text,
      //                         jarak: parseFloat(previousPoint.distanceTo(latLng).toFixed(2) )
      //                       });
      //                     }

      //                     marker.getPopup().setContent(`${note}</br>Total Jarak: <b>${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b)}</b> meter</br> Jarak Dari Tiang Sebelumnya:<b>${previousPoint.distanceTo(latLng).toFixed(2)}</b> Meter`);

      //                     marker.getPopup().update();
      //                     marker.closePopup();
      //                   }
      //                 }
      //                 previousPoint = latLng;
      //               });

      //               map.flyTo([e.latlng.lat, e.latlng.lng], zoom_me);

      //               $.each(load_distance, function(k, v){
      //                 var koor_odp = v.split(',').map(x => x.trim() ),
      //                 koor_sc_split = koor_sc.split(',').map(x => x.trim() );
      //               });
      //             }
      //           }) );
      //           load_collect_tiang.push(koor)
      //         }
      //       }else{
      //         load_collect_tiang = load_collect_tiang.filter(function(e) { return e != koor })
      //       }
      //     })

      //     if(totalDistance.length != 0){
      //       // console.log(totalDistance)
      //       $('.estimasi_jarak').html(`Total Estimasi Jarak ${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2)} Meter`),
      //       $('#estimasi_jarak_rute').val(totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2) )
      //     }

      //     $("input[name='koordinat_tiang']").val(JSON.stringify(load_koor) )
      //   });

      //   $('.undo_btn').on('click', function(){
      //     if(load_koor.length > 1){
      //       pLineGroup.clearLayers();
      //       load_koor.pop();
      //       check_dup.pop();
      //       totalDistance.pop();

      //       if(totalDistance.length != 0){
      //         sum = totalDistance.map(x => x.jarak).reduce( (a, b) => a + b).toFixed(2);
      //       }else{
      //         sum = 0
      //       }

      //       $('.estimasi_jarak').html(`Total Estimasi Jarak ${sum} Meter`);
      //       map.closePopup()

      //       var polyline = L.polyline(load_koor).addTo(pLineGroup)

      //       polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
      //         formatDistance: function(e){
      //           // console.log(e, pLineGroup)
      //           // console.log(Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter')
      //           // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
      //           // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
      //           return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
      //         }
      //       });
      //     }
      //   });

      //   var previousPoint = '',
      //   dataOrder = {!! json_encode($getProject) !!},
      //   koor_sc = '-3.319000,114.584100';
        
      //   if(dataOrder.kordinat_pelanggan != null){
      //     koor_sc = dataOrder.kordinat_pelanggan.split(',').map(x => x.trim() ).filter(function(v){return v!==''}).length != 2 ? '-3.319000,114.584100' : dataOrder.kordinat_pelanggan;
      //   }

      //   var raw_koor_tiang_all = JSON.parse(dataOrder.koordinat_tiang) ?? [],
      //   load_koor = [],
      //   check_dup = [],
      //   load_data_tiang = [],
      //   lat1 = parseFloat(koor_sc.split(',').map(x => x.trim() )[0]),
      //   lon1 = parseFloat(koor_sc.split(',').map(x => x.trim() )[1]),
      //   lat2 = parseFloat(dataOrder.latSC),
      //   lon2 = parseFloat(dataOrder.lonSC),
      //   estimasi_jarak = [],
      //   get_t = getDistanceFromLatLonInKm(koor_sc.split(',').map(x => x.trim() )[0], koor_sc.split(',').map(x => x.trim() )[1], dataOrder.latSC, dataOrder.lonSC);

      //   var koor_tiang_all = raw_koor_tiang_all.filter(function(x) {
      //     return (x.join('').length !== 0);
      //   });

      //   if (dataOrder.latSC != null)
      //   {
      //     var myRouter = L.Routing.control({
      //       waypoints: [
      //           L.latLng(lat1, lon1),
      //           L.latLng(lat2, lon2)
      //       ],
      //     geocoder: L.Control.Geocoder.nominatim(),
      //     routeWhileDragging: true,
      //     reverseWaypoints: true,
      //     showAlternatives: true,
      //     collapsible: true,
      //     autoRoute: true,
      //     routeWhileDragging: true,
      //     createMarker: function(i, waypoints, n) {
      //       var startIcon = L.icon({
      //         iconUrl: '/image/marker-icon-blue.png',
      //         iconSize: [30, 48]
      //       });
      //       var sampahIcon = L.icon({
      //         iconUrl: '/image/marker-icon-green.png',
      //         iconSize: [30, 48]
      //       });
      //       var destinationIcon = L.icon({
      //         iconUrl: '/image/marker-icon-yellow.png',
      //         iconSize: [30, 48]
      //       });
      //       if (i == 0) {
      //         marker_icon = startIcon
      //       } else if (i > 0 && i < n - 1) {
      //         marker_icon = sampahIcon
      //       } else if (i == n - 1) {
      //         marker_icon = destinationIcon
      //       }
      //       var marker = L.marker(waypoints.latLng, {
      //         draggable: false,
      //         bounceOnAdd: false,
      //         icon: marker_icon
      //       })
      //       return marker
      //     },
      //     lineOptions: {
      //       styles: [{ color: 'green', opacity: 1, weight: 5 }]
      //     },
      //     }).on('routeselected', function(e) {
      //         var route = e.route,
      //         jarak = e.route.summary.totalDistance,
      //         waktu = e.route.summary.totalTime;
      //         console.log(jarak, waktu)

      //         $('.deviasi_order_rute').html(`Total Estimasi Jarak Koordinat Sales sama Teknisi ${jarak} Meter`),
      //         $('#deviasi_order').val(jarak )
      //     }).addTo(map);
      //   }

      //   if(typeof(koor_tiang_all) != 'undefined' && koor_tiang_all.length != 0){
      //     koor_tiang_all = koor_tiang_all.map(function(v) {
      //       return v.map(function(v2) {
      //         return parseFloat(v2);
      //       });
      //     });

      //     var polyline = L.polyline(koor_tiang_all).addTo(map)
      //     .showMeasurements({
      //       formatDistance: function(e){
      //         // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
      //         // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
      //         return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
      //       }
      //     });

      //     var new_koor_tng = koor_tiang_all.map(i => Object.assign({}, i));

      //     load_koor = new_koor_tng.map(function(v) {
      //       return [v[0], v[1] ]
      //     });

      //     var new_koor_tng2 = koor_tiang_all.map(i => Object.assign({}, i));

      //     check_dup = new_koor_tng2.map(function(v) {
      //       return `${v[0]},${v[1]}`
      //     });

      //     check_dup.shift();

      //     polyline.getLatLngs().forEach(function (latLng) {
      //       var latlng_text = latLng.lat +','+ latLng.lng;

      //       if(load_distance.indexOf(latlng_text) === -1){
      //         load_distance.push(latlng_text)
      //       }

      //       if (previousPoint.length != 0) {
      //         if(koor_sc.split(',').map(x => x.trim() ).join(',') != latlng_text){
      //           if(totalDistance.map(x => x.koor).indexOf(latlng_text) === -1){
      //             totalDistance.push({
      //               koor: latlng_text,
      //               jarak: parseFloat(previousPoint.distanceTo(latLng).toFixed(2) )
      //             });
      //           }
      //         }
      //       }
      //       previousPoint = latLng;
      //     });

      //   }else{
      //     load_koor.push(koor_sc.split(',').map(x => x.trim() ) );
      //   }

      //   L.marker(koor_sc.split(',').map(x => x.trim() ), {icon: home_customer}).bindPopup(`<div class="table-responsive"><table><tbody><tr><td>SC</td><td>: </td><td>${dataOrder.orderIdInteger}</td></tr><tr><td>Pelanggan</td><td>: </td><td>${dataOrder.orderName}</td></tr><tr><td>Order Status</td><td>: </td><td>${dataOrder.orderStatus}</td></tr></tbody><table></div>`).addTo(map).on('click', function(e) {
      //   });

      //   map.flyTo(koor_sc.split(',').map(x => x.trim() ), zoom_me);
      // }

      $('#odpHilang').hide();
      $('#scBaru').hide();
      $('#estimasiPending').hide();
      $('#field-odpLoss').hide();
      $('#dismantling-status').hide();
      $('#input-status').change(function(){
        console.log($(this).val())
        if ($(this).val() == "24") {
          $('#odpHilang').show();
          $('#field-odpLoss').show();
          var data = [
            {'id' : 'ODP CLOSURE', 'text' : 'ODP CLOSURE'},
            {'id' : 'ODP PB', 'text' : 'ODP PB'},
            {'id' : 'ODP PEDESTAL', 'text' : 'ODP PEDESTAL'},
          ];
          $('#jenisodp').select2({data:data});
        } else if ($(this).val() == "48") {
          $('#estimasiPending').show();
        } else if ($(this).val() == "5" || $(this).val() == "56" || $(this).val() == "13") {
          $('#scBaru').show();
        } else if ($(this).val() == "94") {
          $('#dismantling-status').show();
        } else {
          $('#odpHilang').hide();
          $('#field-odpLoss').hide();
          $('#scBaru').hide();
          $('#estimasiPending').hide();
          $('#dismantling-status').hide();
        };
      });

      if($('#input-status').val() == 94){
        $('#input-status').val(94).change();
      }

      $('#field-dc-roll').hide();
      $('#field-precon').hide();
      $('.jenisMaterial').change(function(){
        if ($(this).val() == "DC_ROLL") {
          $('#field-dc-roll').show();
          $('#field-precon').hide();
        }
        else if ($(this).val() == "PRECON") {
          $('#field-dc-roll').hide();
          $('#field-precon').show();
        };
      });

      $('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
      });

      // tes insert tiang tambahan
      var data = {!! json_encode($getTambahTiang) !!},
      select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }

      $('#tbTiang').select2(select2Options());

      $('#hilang').hide();
      $('#catatanRevoke').hide();
      $('#catatanPasang').removeAttr('readonly');
      $('#input-status').change(function(){
        if ($(this).val() == "11") {
          // console.log(this.value);
          $('#hilang').show();
        }
        else {
          $('#hilang').hide();
        };

        if ($(this).val() == "43" || $(this).val() == "44"){
          $('#catatanRevoke').show();
          $('#catatanPasang').attr('readonly', true);
        }
        else{
          $('#catatanRevoke').hide();
          $('#catatanPasang').removeAttr('readonly');
          $('#inputCatatanRevoke').val('');
        }
      });

      var dispId = {!! json_encode($dispatch_teknisi_id) !!},
      project = {!! json_encode($getProject) !!};

      $('#status_odp').on('change', function(){
        if(this.value == "ODP_tidak_muncul"){
          $("#input-koordinat").removeAttr("readonly");
          $("#odp2").removeAttr("readonly");
        }else{
          $("#input-koordinat").attr('readonly', true);
          $("#odp2").attr('readonly', true);
        }
      });

      $('#input-status').on('change', function(){
        if(this.value == "1"){
          $.ajax({
            url: "/cekredaman/"+project.ndemSpeedy,
            beforeSend: function( ) {
              $('#redaman').val('wait');
            }
          })
          .done(function( data ) {
            if ( console && console.log ) {
              $('#redaman').val(data);
            }
          });
        }
      });

      Vue.filter('hasQty', function(value) {
        return value.filter(function(a) { return a.qty > 0});
      });

      Vue.filter('hasSaldo', function(value) {
        return value.filter(function(a) { return a.saldo > 0});
      });

      Vue.filter('doubleDigit', function(value) {
        var v = Number(value);
        if (v < 1) return '00';
        else if (String(v).length < 2) return '0' + v;
      });

      var materials = {!! json_encode($materials) !!},
      listVm = new Vue({
        el: '#material-list',
        data: materials
      });

      var modalVm = new Vue({
        el: '#material-modal',
        data: materials,
        methods: {
          // onChange : function(item){
          //   if (item.qty > item.saldo) item.qty=item.saldo;
          // },
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;

            // if (item.qty > item.saldo) item.qty = item.qty-1;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      var materials2 = {!! json_encode($materials2) !!} || [],
      listVm = new Vue({
        el: '#material-list2',
        data: materials2
      });

      var modalVm = new Vue({
        el: '#material-modal2',
        data: materials2,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      var $btnGps = $('#btn-gps'),
      $btnGpsODP = $('#btn-gps-odp'),
      $kordinat_tiang = $('.kordinat_tiang');

      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_tiang.val('ERROR');
          return;
        }
        $kordinat_tiang.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
        $kordinat_tiang.val(result.coords.latitude+','+result.coords.longitude);
        }, function(error) {
          $kordinat_tiang.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
            alert('Tidak mendapat izin menggunakan GPS');
            break;
            case error.POSITION_UNAVAILABLE:
            alert('Gagal menghubungi satelit GPS');
            break;
            case error.TIMEOUT:
            $kordinat_tiang.val('ERROR: TIMEOUT');
            break;
          }
        });
      });

      var $kordinat_pelanggan = $('#kordinat_pelanggan');
      $btnGps.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_pelanggan.val('ERROR');
          return;
        }

        $kordinat_pelanggan.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_pelanggan.val(result.coords.latitude+','+result.coords.longitude);
        }, function(error) {
          $kordinat_pelanggan.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_pelanggan.val('ERROR: TIMEOUT');
              break;
          }
        });
      });

      var $kordinat_odp = $('#kordinat_odp');
      $btnGpsODP.click(function() {
        if (!navigator.geolocation) {
          alert('Perangkat tidak memiliki fitur GPS', 'ERROR');
          $kordinat_odp.val('ERROR');
          return;
        }

        $kordinat_odp.val('Harap Tunggu...');
        navigator.geolocation.getCurrentPosition(function(result) {
	        $kordinat_odp.val(result.coords.latitude+','+result.coords.longitude);
        }, function(error) {
          $kordinat_odp.val('ERROR');
          switch(error.code) {
            case error.PERMISSION_DENIED:
              alert('Tidak mendapat izin menggunakan GPS');
              break;

            case error.POSITION_UNAVAILABLE:
              alert('Gagal menghubungi satelit GPS');
              break;

            case error.TIMEOUT:
              $kordinat_odp.val('ERROR: TIMEOUT');
              break;
          }
        });
      });


      $('input[type=file]').change(function() {
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
            $(inputEl).parent().parent().find('img').attr('src', e.target.result);

          }
          reader.readAsDataURL(inputEl.files[0]);
        }
      });

      $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: 'norfc'});
        });
        $('input[name=materials]').val(JSON.stringify(result));
      });

      $('#submit-form').submit(function() {
        var result = [];
        materials2.forEach(function(item) {
          if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty, rfc: 'norfc'});
        });
        $('input[name=materials2]').val(JSON.stringify(result));
      });

      $('.modal-body').css({ maxHeight: window.innerHeight - 170 });
      var data = {!! json_encode($get_laporan_status) !!};
      var select2Options = function() {
        return {
          data: data,
          placeholder: 'Input Status',
          matcher: matchCustom,
          templateResult: formatCustom,
          formatSelection: function(data) { return data.text },
          formatResult: function(data) {
            return  data.text;
          }
        }
      }
      $('#input-status').select2(select2Options());

      function stringMatch(term, candidate) {
        return candidate && candidate.toLowerCase().indexOf(term.toLowerCase()) >= 0;
      }

      function matchCustom(params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
          return data;
        }
        // Do not display the item if there is no 'text' property
        if (typeof data.text === 'undefined') {
          return null;
        }
        // Match text of option
        if (stringMatch(params.term, data.text)) {
          return data;
        }
        // Match attribute "data-foo" of option
        if (stringMatch(params.term, $(data.element).attr('data-foo'))) {
          return data;
        }
        // Return `null` if the term should not be displayed
        return null;
      }

      function formatCustom(state) {
        // console.log(state)
        return $(
          '<div><div style="font-weight: bold;">' + state.text + '</div><br><p>' + state.definisi_status + '</p></div>'
        );
      }

      // class selectpicker master odp
      $('.selectpicker').selectpicker();

      $(".input-type-ont2").select2({
        placeholder: "Pilih Jenis ONT",
        allowClear: true
      });

      $(".input-type-stb2").select2({
        placeholder: "Pilih Jenis STB",
        allowClear: true
      });

      $(".input-hd-manja").select2({
        placeholder: "Pilih HD Manja",
        allowClear: true
      });

      $(".input-hd-fallout").select2({
        placeholder: "Pilih HD Fallout",
        allowClear: true
      });

      var ntelist = {!! json_encode($nte) !!} || [],
      listVm = new Vue({
        el: '#material-list-nte',
        data: ntelist
      }),
      modalVm = new Vue({
        el: '#material-modal-nte',
        data: ntelist,
        methods: {
          onPlus: function(item) {
            if (!item.qty) item.qty = 0;
            item.qty++;
          },
          onMinus: function(item) {
            if (!item.qty) item.qty = 0;
            else item.qty--;
          }
        }
      });

      $('#submit-form').submit(function() {
        var result = [];
        ntelist.forEach(function(item) {
          if (item.qty > 0) result.push({id: item.id, qty: item.qty, jenis: item.jenis_nte, kat: item.nte_jenis_kat});
        });
        $('input[name=materialsNte]').val(JSON.stringify(result));
      });
    })
  </script>
@endsection