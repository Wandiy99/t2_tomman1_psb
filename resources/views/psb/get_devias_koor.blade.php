@extends('public_layout')
@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">RUTE PENARIKAN BERDASARKAN ODP</div>
      <div class="panel-body">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
        <link rel="stylesheet" href="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.css" />
        <style type="text/css">
          .leaflet-measure-path-measurement {
            font-size: 12px !important;
          }
        </style>
        <div class="col-sm-12">
            Jarak : <span id="jarak_text"></span> Meter
            <div id="map1" style="height: 750px; margin-top: 10px;"></div>
        </div>
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.min.js"></script>
        <script src="/bower_components/leaflet-routing-machine-3.2.12/examples/Control.Geocoder.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.js"></script>
        <script type="text/javascript">
              $(function() {
                  var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                  pLineGroup = L.layerGroup(),
                  zoom_me = 18,
                  googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
                      maxZoom: 20,
                      subdomains:['mt0','mt1','mt2','mt3']
                  }),
                  googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
                      maxZoom: 20,
                      subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                  }),
                  map = L.map('map1', {
                      center: ['-3.319000', '114.584100'],
                      zoom: zoom_me,
                      maxZoom: 20,
                      layers: [googleAlteredRoad, pLineGroup]
                  }),
                  baseLayers = {
                      'Jalan': googleStreets,
                      'Satelit': googleSat,
                      'Satelit Dan Jalan': googleHybrid,
                      'Jalur': googleTerrain,
                      'Jalur 2': googleAlteredRoad,
                      'Lalu Lintas': googleTraffic,
                  },
                  overlays = {
                      'Garis': pLineGroup,
                  },
                  collect_odp = [],
                  load_collect_tiang = [],
                  totalDistance = [],
                  load_koor_poly = [],
                  load_distance = [],
                  sum = 0,
                  radius = 500.00,
                  data_tiang = null,
                  layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
                  home_customer = new L.Icon({
                      iconUrl: '/image/home_customer.png',
                      iconAnchor: [12, 41],
                      popupAnchor: [1, -34]
                  });
                  pole_telkom = new L.Icon({
                    iconUrl: '/image/pole_telkom.png',
                    iconAnchor: [12, 41],
                    popupAnchor: [1, -34]
                  });

                  lat1 = {!! json_encode($lat1) !!}
                  lon1 = {!! json_encode($lon1) !!}
                  lat2 = {!! json_encode($lat2) !!}
                  lon2 = {!! json_encode($lon2) !!}

                  var lat1 = parseFloat(lat1),
                    lon1 = parseFloat(lon1),
                    lat2 = parseFloat(lat2),
                    lon2 = parseFloat(lon2),
                    get_t = getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);

                  function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
                    var R = 6371; // Radius of the earth in km
                    var dLat = deg2rad(lat2-lat1);  // deg2rad below
                    var dLon = deg2rad(lon2-lon1);
                    var a =
                        Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2)
                        ;
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    var d = R * c; // Distance in km
                    return d;
                  }

                  function deg2rad(deg) {
                      return deg * (Math.PI/180)
                  }

        var myRouter = L.Routing.control({
            waypoints: [
                L.latLng(lat1, lon1),
                L.latLng(lat2, lon2)
            ],
          geocoder: L.Control.Geocoder.nominatim(),
          routeWhileDragging: true,
          reverseWaypoints: true,
          showAlternatives: true,
          collapsible: true,
          autoRoute: true,
          routeWhileDragging: true,
          createMarker: function(i, waypoints, n) {
            var startIcon = L.icon({
              iconUrl: '/image/marker-icon-blue.png',
              iconSize: [30, 48]
            });
            var sampahIcon = L.icon({
              iconUrl: '/image/marker-icon-green.png',
              iconSize: [30, 48]
            });
            var destinationIcon = L.icon({
              iconUrl: '/image/marker-icon-yellow.png',
              iconSize: [30, 48]
            });
            if (i == 0) {
              marker_icon = startIcon
            } else if (i > 0 && i < n - 1) {
              marker_icon = sampahIcon
            } else if (i == n - 1) {
              marker_icon = destinationIcon
            }
            var marker = L.marker(waypoints.latLng, {
              draggable: false,
              bounceOnAdd: false,
              icon: marker_icon
            })
            return marker
          },
          lineOptions: {
            styles: [{ color: 'green', opacity: 1, weight: 5 }]
          },
        }).on('routeselected', function(e) {
            var route = e.route,
            jarak = e.route.summary.totalDistance,
            waktu = e.route.summary.totalTime;
            $('#jarak_text').html(`${jarak}`);
            console.log(jarak, waktu)
        }).addTo(map);
          });
        </script>
      </div>
    </div>
  </div>
  @endsection