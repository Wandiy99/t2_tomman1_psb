@extends('layout')
@section('content')
<style>
    th, td {
        text-align: center;
        vertical-align: middle;
    }
    h3 {
        text-align: center;
        font-weight: bold;
    }
    option {
        text-align: center;
    }
</style>
@include('partial.alerts')
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
          <h4 class="page-title" style="text-align: center; font-weight: bold;">INBOX REQUEST RESET BA</h4><br/>
          <form method="GET">
            <div class="row">
              <div class="col-md-5">
                <label for="startDate">TANGGAL AWAL</label>
                <div class="input-group">
                  <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="startDate" value="{{ $start ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
              <div class="col-md-5">
                <label for="endDate">TANGGAL AKHIR</label>
                <div class="input-group">
                  <input type="text" class="form-control datepicker-autoclose" placeholder="yyyy-mm-dd" name="endDate" value="{{ $end ? : date('Y-m-d')}}"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
              <div class="col-md-2">
                  <label for="search">&nbsp;</label>
                  <button class="btn btn-info btn-block" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    <div class="col-sm-12">
        <div class="white-box">
            <div class="table-responsive">
                <table id="table_inbox" class="table_inbox display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SC ID</th>
                            <th>Status (Witel)</th>
                            <th>Status (QC2)</th>
                            <th>Alasan</th>
                            <th>Request By</th>
                            <th>Request At</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $num => $d)
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>{{ $d->sc_id }}</a></td>
                            <td>{{ $d->statusName }}</td>
                            <td>{{ $d->qcStatusName }}</td>
                            <td>{{ $d->alasan }}</td>
                            <td>{{ $d->nama_req }}</td>
                            <td>{{ $d->request_at }}</td>
                            <td>
                              @if ($d->status == null)
                              <a class="label label-danger" href="/utonline/request-reset-ba/{{ $d->sc_id }}" target="_blank">Belum Konfirmasi</a>
                              @else
                              <a class="label label-success" href="/utonline/request-reset-ba/{{ $d->sc_id }}" target="_blank">Sudah Dihapus</a>
                              @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('plugins')
<script src="/bower_components/select2/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.table_inbox').DataTable();
    });
    $('.datepicker-autoclose').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
      });
</script>
@endsection