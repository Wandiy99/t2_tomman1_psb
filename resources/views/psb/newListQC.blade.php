@extends('new_tech_layout')

@section('content')
  @include('partial.alerts')
<form method="get">
    Bulan SC PS :
    <input style="width: 60%;" type="month" name="bulan" id="bulan" value="{{ $bulan }}">
    <input type="submit" id="bt_bulan" value="Filter">
</form><br>
@foreach($list as $num => $data)

<div class="card border-light mb-3 shadow p-3 mb-5 bg-white rounded" > 
  <div class="card-header bg-transparent"><label class="badge rounded-pill bg-primary" style="color: white">{{ $data->scId }}</label> <label class="badge rounded-pill bg-warning">{{ $data->laporan_status ? : "ANTRIAN" }}</label> <label class="badge rounded-pill bg-danger" style="color: white">{{ $data->statusName }}</label> <label class="badge rounded-pill bg-secondary" style="color: white">{{ $data->uraian }}</label></div>
  <div class="card-body">
  <div class="table-responsive">
  <table class="table">
  <tbody>
    <tr>
      <td>ORDER / CREATE DATE / TRX DATE</td>
      <td>:</td>
      <td>
        {{ $data->scId }} / 
        {{ $data->create_dtm ? : '-' }} / 
        {{ $data->tglTrx ? : '-' }}
        </td>
    </tr>
    <tr>
      <td>ORDER STATUS / ADDRESS</td>
      <td>:</td>
      <td>{{ $data->orderStatus ? : '-' }} / {{ $data->orderAddr ? : '-' }}</td>
    </tr>
    <tr>
      <td>NAMA / PIC</td>
      <td>:</td>
      <td>{{ $data->customer_desc }} / {{ $data->noPelangganAktif ? : $data->orderKontak ? : '0' }}</td>
    </tr>
    </tbody>
  </table>
  </div>
    <center>
    <a href="/{{ $data->id_dt }}" class="btn btn-success" style="text-align: center; color: white">Check Out!</a>
    </center>
  </div>
</div>
@endforeach
@endsection
@section('plugins')
<script type="text/javascript">
  $(function() {
      var day = {
          format : 'yyyy-mm',
          viewMode: "months",
          minViewMode: "months",
          autoClose: true
      };

      $('#bulan').datepicker(day).on('changeDate', function(e){
        $(this).datepicker('hide');
      });

      $('#bt_bulan').click(function(){
       			 window.location.replace = "/workorderQC/"+$('#bulan').val();
      });
  })
</script>
@endsection