@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/dashboardReview/{{ $tgl }}" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REVIEW PELANGGAN WHATSAPP</h3>
        <div class="table-responsive">
            <table id="table_data" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
  						<th>REGU</th>
  						<th>SEKTOR</th>
  						<th>ORDER</th>
  						<th>NAMA PELANGGAN</th>
  						<th>KONTAK</th>
  						<th>TGL</th>
  						<th>RATING</th>
  						<th>SOUND OF CUSTOMER</th>
  						<th>CATATAN</th>
  						<th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($get_review as $num => $review)                
                    <tr>
                        <td>{{ ++$num }}</td>
  						<td>{{ $review->uraian }}</td>
  						<td>{{ $review->title }}</td>
  						<td>{{ $review->Ndem }}</td>
  						<td>{{ $review->Customer_Name ? : $review->orderName }}</td>
  						<td>{{ $review->noPelangganAktif ? : $review->no_telp }}</td>
  						<td>{{ $review->date_created ? : 'DEFAULT' }}</td>
  						<td>{{ $review->Review ? : '3' }}</td>
  						<td>
                            @php
                                $sound_of = "";

                                if ($review->A1 == 1)
                                {
                                    $sound_of .= "Penampilan Teknisi (Good) | ";
                                }
                                if ($review->A2 == 1)
                                {
                                    $sound_of .= "Perilaku Teknisi (Good) | ";
                                }
                                if ($review->A3 == 1)
                                {
                                    $sound_of .= "Komunikasi (Good) | ";
                                }
                                if ($review->A4 == 1)
                                {
                                    $sound_of .= "Ketepatan Waktu (Good) | ";
                                }
                                if ($review->A5 == 1)
                                {
                                    $sound_of .= "Perlengkapan (Good) | ";
                                }

                                if ($review->B1 == 1)
                                {
                                    $sound_of .= "Teknisi Meminta Biaya Tambahan (Bad) | ";
                                }
                                if ($review->B2 == 1)
                                {
                                    $sound_of .= "Penyelesaian Order Lambat (Bad) | ";
                                }
                                if ($review->B3 == 1)
                                {
                                    $sound_of .= "Kurang Bersahabat (Bad) | ";
                                }
                                if ($review->B4 == 1)
                                {
                                    $sound_of .= "Pekerjaan Tidak Selesai (Bad) | ";
                                }
                                if ($review->B5 == 1)
                                {
                                    $sound_of .= "Tampilan Kurang Rapi (Bad) | ";
                                }
                            @endphp
                            {{ $sound_of }}
                        </td>
  						<td>{{ $review->Catatan }}</td>
  						<td><a href="/review/followup/" class="label label-success">Follow UP</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'DETAIL REVIEW PELANGGAN WHATSAPP - TOMMAN'
                }
            ]
        });
    });
</script>
@endsection