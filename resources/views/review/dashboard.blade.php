@extends('layout')

@section('content')
  @include('partial.alerts')
  <style type="text/css">
    .green {
      background-color : #00cec9;
      padding : 20px;
      color : #FFF;
    }
    .blue {
      background-color : #00b894;
      padding : 20px;
      color : #FFF;
    }
    .red {
      background-color : #ff7675;
      padding : 20px;
      color : #FFF;
    }
    .jumbo {
      font-size: 20px;
      font-weight: bold;
    }
    .table-tek {
      background : #f3f3f3;
    }
    .table-sek {
      background : #eefff3;
    }
  </style>

  <div class="row">
  <div class="col-sm-12">
      <h3 style="text-align: center; font-weight: bold;">Review Teknisi {{ $tgl }}</h3>
    </div>
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">PREVIEW</div>
        <div class="panel-body">
          <div class="row">
          <div class="col-sm-4 green">
            <span class="jumbo">INBOX REVIEW : {{ $get_all_review[0]->jumlah }}</span>
          </div>
          <div class="col-sm-4 blue">
           <span class="jumbo">AVG RATING : {{ round($get_all_review[0]->rating,1) }}</span>
          </div>
          <div class="col-sm-4 red">
           <span class="jumbo">UNDER RATE : <a href="/dashboardReviewListTekUnderrate/{{ $tgl }}">{{ $get_all_review[0]->underrate }}</a></span>
          </div>

          </div>
        </div>

      </div>

    </div>
    <div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">SOUND OF CUSTOMER</div>
        <div class="panel-body">
      <div class="row">
      <div class="col-sm-6">

                                    <div id="when-good"></div>

  </script>
          </div>

          <div class="col-sm-6">
          <div id="when-wrong"></div>
          </div>
  </div>
  </div>
      </div>
    </div>
  	<div class="col-sm-12">
  		<div class="panel panel-default">
  			<div class="panel-heading">ACHIEVEMENT</div>
  			<div class="panel-body">
  				<div class="row">
  				<div class="col-sm-6">
            <b>Ranking Sektor</b>
              <table class="table table-sek">
  						<tr>
  							<th style="text-align: center;">Rank</th>
  							<th style="text-align: center;">Sektor</th>
  							<th style="text-align: center;">Avg</th>
  							<th style="text-align: center;">Jml</th>
  						</tr>
              @php
                $total = $rating = 0;
              @endphp
  						@foreach ($get_review as $num => $review)
              @php
                $rating += $review->review;
                $total += $review->jumlah;
              @endphp
  						<tr>
  							<td style="text-align: center;">{{ ++$num }}</td>
  							<td style="text-align: center;">{{ $review->title ? : "NON SEKTOR" }}</td>
  							<td style="text-align: center;">{{ round($review->review,1) }}</td>
  							<td style="text-align: center;"><a href="/dashboardReviewList/{{ $tgl }}/{{ $review->chat_id ? : 'NONSEKTOR' }}">{{ $review->jumlah }}</a></td>
  						</tr>
  						@endforeach
              <tr>
                <th style="text-align: center;" colspan="2">Total</th>
                <th style="text-align: center;">{{ @round($rating/$num,1) }}</th>
                <th style="text-align: center;"><a href="/dashboardReviewList/{{ $tgl }}/ALL">{{ $total }}</a></th>
              </tr>
  					</table>
  				</div>
  				<div class="col-sm-6">
            <b>Top Teknisi</b>
  					<table class="table table-tek">
              <tr>
                <th style="text-align: center;">Rank</th>
                <th style="text-align: center;">Nama</th>
                <th style="text-align: center;">Avg</th>
                <th style="text-align: center;">Jml</th>
              </tr>
              @foreach ($get_top_tek as $num => $top_tek)
              <tr>
                <td style="text-align: center;">{{ ++$num }}</td>
                <td style="text-align: center;">{{ $top_tek->uraian }}</td>
                <td style="text-align: center;">{{ round($top_tek->review,1) }}</td>
                <td style="text-align: center;"><a href="/dashboardReviewListTek/{{ $tgl }}/{{ $top_tek->id_regu }}">{{ $top_tek->jumlah }}</a></td>
              </tr>
              @endforeach
            </table>
  				</div>
  				</div>

  			</div>
  		</div>
  	</div>
  </div>

  <script>
    $(function() {
      Morris.Bar({
        element: 'when-good',
        data: [
          <?php
          foreach ($query1 as $result1) {
          ?>
          {c: 'What When Good ?',x: '<?php echo $result1->A1 ?>', y : '<?php echo $result1->A2 ?>', z: '<?php echo $result1->A3 ?>', a: '<?php echo $result1->A4 ?>',
          b: '<?php echo $result1->A5 ?>'}
    <?php
          } ?>],
        xkey: 'c',
        ykeys: ['x','y','z','a','b'],
        labels: ['Penampilan Teknisi','Perilaku Teknisi','Komunikasi','Ketepatan Waktu','Perlengkapan'],
        gridLineColo8r: '#e0e0e0'
      });
      Morris.Bar({
        element: 'when-wrong',
        data: [
          <?php
          foreach ($query1 as $result1) {
          ?>
          {c: 'What When Wrong ?',x: '<?php echo $result1->B1 ?>', y : '<?php echo $result1->B2 ?>', z: '<?php echo $result1->B3 ?>', a: '<?php echo $result1->B4 ?>',
          b: '<?php echo $result1->B5 ?>'}
    <?php
          } ?>],
        xkey: 'c',
        ykeys: ['x','y','z','a','b'],
        labels: ['Teknisi Meminta Biaya Tambahan','Penyelesaian Order Lambat','Kurang Bersahabat','Pekerjaan Tidak Selesai','Tampilan Kurang Rapi'],
        gridLineColo8r: '#e0e0e0'
      });

    });
  </script>
@endsection
