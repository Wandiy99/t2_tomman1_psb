@extends('layout')

@section('content')
  @include('partial.alerts')

  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
     <div class="form-group">
      <label class="control-label">FollowUp Review</label>
      <textarea name="followup" class="form-control"></textarea>
    </div>

    <div>
      <button class="btn btn-primary">Simpan</button>
    </div>
  </form>

@endsection
