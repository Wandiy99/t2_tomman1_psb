@extends('layout')
@section('content')
@include('partial.alerts')
<style>
    tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 2px 1px;
    }
    .txt-center {
        text-align: center;
    }
    .red-failed {
        background-color: #E74C3C;
        color: white !important;
    }
    .green-success {
        background-color: #1ABC9C;
        color: white !important;
    }
</style>
<div class="row">
    <div class="col-sm-12">

        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Untuk menggunakan fitur ini, harap lakukan <i style="font-weight: bold;">Scan Barcode</i> pada halaman <i>&nbsp;<i class="ti-new-window" class="linea-icon linea-basic fa-fw"></i><a href="http://10.131.40.214:6969/" style="color: white !important; font-weight: bold;" target="_blank">&nbsp;Whatsapp API</a></i>
        </div>

        <table class="table table-bordered dataTable">
            <tr>
                <th class="align-middle txt-center" rowspan="3">ID SENDER</th>
                <th class="align-middle txt-center" colspan="{{ date('t') * 2 }}">ACTIVITY ({{ strtoupper(date('F Y')) }})</th>
                <th class="align-middle txt-center green-success" rowspan="3">SIMULASI<br />INVOICE</th>
            </tr>
            <tr>
                    @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                                $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                                $keys = $i;
                            @endphp
                        @endif
                    <th class="align-middle txt-center" colspan="2">{{ $keys }}</th>
                    @endfor
            </tr>
            <tr>
                @for ($i = 1; $i < date('t') + 1; $i ++)
                <th class="align-middle txt-center">S</th>
                <th class="align-middle txt-center">F</th>
                @endfor
            </tr>
            @php
                $send_s = $send_f = $simulasi_invoice = $total_invoice = 0;
                $txt_send_s = $txt_send_f = '';
            @endphp
            @foreach ($final_raport as $result)
            @php
                $simulasi_invoice = ($result['isi']['total_send_s'] * 50);
                $total_invoice += $simulasi_invoice;
            @endphp
            <tr>
                <td class="align-middle">{{ $result['isi']['id_sender'] }}</td>
                @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                                $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                                $keys = $i;
                            @endphp
                        @endif
    
                        @php
                            $txt_send_s = 'send_s_d'.$keys;
                            $send_s = $result['isi'][$txt_send_s];
    
                            $txt_send_f = 'send_f_d'.$keys;
                            $send_f = $result['isi'][$txt_send_f];

                            $days = date('Y-m-').''.$keys;
                        @endphp

                        @if ($send_s > 0)
                            @php
                                $bg_s = 'green-success';
                            @endphp
                        @else
                            @php
                                $bg_s = '';
                            @endphp
                        @endif

                        @if ($send_f > 0)
                            @php
                                $bg_f = 'red-failed';
                            @endphp
                        @else
                            @php
                                $bg_f = '';
                            @endphp
                        @endif
                        <td class="align-middle txt-center {{ $bg_s }}"><a class="{{ $bg_s }}" href="/whatsapp-api/detail?response=SUCCESS&date={{ $days }}&id_sender={{ $result['isi']['id_sender'] }}" target="_blank">{{ $send_s }}</a></td>
                        <td class="align-middle txt-center {{ $bg_f }}"><a class="{{ $bg_f }}" href="/whatsapp-api/detail?response=FAILED&date={{ $days }}&id_sender={{ $result['isi']['id_sender'] }}" target="_blank">{{ $send_f }}</a></td>
                @endfor
                <td class="align-middle">
                    Rp. {{ str_replace(',', '.', number_format($simulasi_invoice) ) }}
                </td>
            </tr>
            @endforeach
            <tr>
                <th class="align-middle txt-center" colspan="{{ date('t') * 2 + 1 }}">TOTAL</th>
                <th class="align-middle txt-center green-success">Rp. {{ str_replace(',', '.', number_format($total_invoice) ) }}</th>
            </tr>
        </table>
    </div>
</div>

<div class="alert alert-info alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Silahkan unduh file berikut <i>&nbsp;<i class="ti-new-window" class="linea-icon linea-basic fa-fw"></i><a href="/api/files/whatsapp_api_format.xlsx" style="color: white !important; font-weight: bold;" target="_blank">&nbsp;Format Excel Whatsapp API</a></i>
</div>

<div class="row">

<div class="col-sm-6">
    <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">SEND MESSAGE</h4>
        </div>
            <div class="panel-body">
                <form id="submit-form" method="post" action="/whatsapp-api/send-message" enctype="multipart/form-data" autocomplete="off">
                    <div class="form-group">
                        <label for="input-sender" class="col-form-label">ID Sender</label>
                        <input type="text" name="sender" class="form-control" id="sender" placeholder="Masukan ID Sender dari API WhatsApp"/>
                        {!! $errors->first('sender','<p class="label label-danger">:message</p>') !!}
                    </div>

                    <div class="form-group">
                    <label class="control-label" for="multi">Message</label>
                        <textarea name="message" id="message" class="form-control" rows="5" placeholder="Masukan Pesan" required /></textarea>
                        {!! $errors->first('message','<p class="label label-danger">:message</p>') !!}
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="upload">Import Excel</label>
                        <input type="file" id="upload" name="upload" class="form-control"/>
                    </div>
                    
                    <div style="text-align: center">
                        <button class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
    </div>
</div>

{{-- <div class="col-sm-6">
    <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">SEND MEDIA</h4>
        </div>
            <div class="panel-body">
                <form id="submit-form" method="post" action="/whatsapp-api/send-media" enctype="multipart/form-data" autocomplete="off">
                    <div class="form-group">
                        <label for="input-sender" class="col-form-label">ID Sender</label>
                        <input type="text" name="sender" class="form-control" id="sender" placeholder="Masukan ID Sender dari API WhatsApp" required />
                        {!! $errors->first('sender','<p class="label label-danger">:message</p>') !!}
                    </div>

                    <div class="form-group">
                    <label class="control-label" for="caption">Caption</label>
                        <textarea name="caption" id="caption" class="form-control" rows="5" placeholder="Masukan Caption Untuk Media yang Dikirimkan" required /></textarea>
                        {!! $errors->first('caption','<p class="label label-danger">:message</p>') !!}
                    </div>

                    <div class="form-group">
                        <label for="input-url" class="col-form-label">URL Media</label>
                        <input type="text" name="url" class="form-control" id="url" placeholder="Masukan URL Media" />
                        {!! $errors->first('file','<p class="label label-danger">:message</p>') !!}
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="upload">Import Excel</label>
                        <input type="file" id="upload" name="upload" class="form-control"/>
                    </div>
                    
                    <div style="text-align: center">
                        <button class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
    </div>
</div> --}}

{{-- @if(session('auth')->id_karyawan == '20981020' )
<div class="col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4 style="font-weight: bolder !important;color: black !important;">BROADCAST TOMMAN-GO</h4>
        </div>
            <div class="panel-body">
                <form id="submit-form" method="post" action="/broadcast/message/tommango" enctype="multipart/form-data" autocomplete="off">

                    <div class="form-group">
                    <label class="control-label" for="multi">Message</label>
                        <textarea name="message" id="message" class="form-control" rows="5" placeholder="Masukan Pesan Broadcast" required /></textarea>
                        {!! $errors->first('message','<p class="label label-danger">:message</p>') !!}
                    </div>
                    
                    <div style="text-align: center">
                        <i>Total User Aktif {{ count($user_go)}}</i><br/><br/>
                        <button class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
    </div>
</div>
@endif --}}

</div>
@endsection