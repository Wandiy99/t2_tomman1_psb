@extends('layout')
@section('content')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
    <a href="/whatsapp-api" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-arrow-left"></span>
    </a>
        <h3 class="box-title m-b-0">DETAIL REPORT WHATSAPP-API MESSAGE STATUS {{ $response }} PERIODE {{ $date }} ID SENDER {{ $id_sender }}</h3>
        <div class="table-responsive">
              <table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>PHONE NUMBERS</th>
                        <th>TYPE</th>
                        <th>MESSAGE</th>
                        <th>URL FILE</th>
                        <th>RESPONSE</th>
                        <th>ID SENDER</th>
                        <th>SEND BY NIK</th>
                        <th>SEND BY NAME</th>
                        <th>SEND AT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($get_data as $num => $data)                
                    <tr>
                        <td>{{ ++$num }}</td>
                        <td>{{ $data->phone_numbers }}</td>
                        <td>{{ $data->type }}</td>
                        <td>{{ $data->message }}</td>
                        <td>{{ $data->url_file ? : '#N/A' }}</td>
                        <td>{{ $data->response }}</td>
                        <td>{{ $data->divisi }}</td>
                        <td>{{ $data->send_by }}</td>
                        <td>{{ $data->send_by_name }}</td>
                        <td>{{ $data->send_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>NO</th>
                        <th>PHONE NUMBERS</th>
                        <th>TYPE</th>
                        <th>MESSAGE</th>
                        <th>URL FILE</th>
                        <th>RESPONSE</th>
                        <th>ID SENDER</th>
                        <th>SEND BY NIK</th>
                        <th>SEND BY NAME</th>
                        <th>SEND AT</th>
                    </tr>
                </tfoot>
              </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'DETAIL REPORT WHATSAPP-API MESSAGE'
                },
                {
                    extend: 'excel',
                    title: 'DETAIL REPORT WHATSAPP-API MESSAGE'
                },
                {
                    extend: 'print',
                    title: 'DETAIL REPORT WHATSAPP-API MESSAGE'
                }
            ]
        });
    });
</script>
@endsection