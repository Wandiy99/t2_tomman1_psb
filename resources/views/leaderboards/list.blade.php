@extends('tech_layout')

@section('content')
  @include('partial.alerts')
  <style>
    .color_current : {
      background-color:#2ecc71;
      color:#FFFFFF;
    }
    th {
      padding: 5px;
      text-align: left;
      vertical-align: middle;
    }
    td {
      padding :5px;
      border-color:black;
    }
  </style>
  <div style="padding-bottom: 10px;">

    <h3>Leaderboards</h3>
    <ul class="nav nav-tabs">
      <li class="<?php if ($squad=='ALPHA.PROV') : echo 'active'; endif; ?>"><a href="/leaderboards/ALPHA.PROV/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/ALL">ALPHA.PROV</a></li>
      <li class="<?php if ($squad=='ALPHA.ASR') : echo 'active'; endif; ?>"><a href="/leaderboards/ALPHA.ASR/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/ALL">ALPHA.ASR</a></li>
      <li class="<?php if ($squad=='DELTA.A') : echo 'active'; endif; ?>"><a href="/leaderboards/DELTA.A/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/ALL">DELTA.A</a></li>
      <li class="<?php if ($squad=='DELTA.B') : echo 'active'; endif; ?>"><a href="/leaderboards/DELTA.B/{{ date('Y-m-01') }}/{{ date('Y-m-d') }}/ALL">DELTA.B</a></li>
    </ul>
    <br />
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            RANKED
          </div>
          <div class="panel-body table-responsive">
            <table class="table table-striped table-bordered">
              <tr>
                <th rowspan="2" width="20">RANKING</th>
                <th rowspan="2">NIK</th>
                <th rowspan="2">LABORCODE</th>
                <th rowspan="2">WARRIOR</th>
                <th rowspan="2">MITRA</th>
                <th rowspan="2">JUMLAH GGN</th>
                <th rowspan="2">JUMLAH GGN UP</th>
                <th colspan="8">POINT</th>
              </tr>
              <tr>
                <th>A.D</th>
                <th>A.C</th>
                <th>A.F</th>
                <th>P.A</th>
                <th>P.1</th>
                <th>M</th>
                <th>MIG</th>
                <th>TOTAL</th>
              </tr>
              @foreach ($tampilSquad as $rank => $squads)
              <tr>
                <td>{{ ++$rank }}.</td>
                <td>{{ $squads->nik }}</td>
                <td>{{ strtoupper($squads->laborcode) }}</td>
                <td>{{ strtoupper($squads->warrior) }}</td>
                <td>{{ strtoupper($squads->mitra) }}</td>
                <td>{{ strtoupper($squads->jumlah_ggn) }}</td>
                <td>{{ strtoupper($squads->jumlah_ggn_up) }}</td>
                <td>{{ $squads->a_d }}</td>
                <td>{{ $squads->a_c }}</td>
                <td>{{ $squads->a_f }}</td>
                <td>{{ $squads->p_a }}</td>
                <td>{{ $squads->p_1 }}</td>
                <td>{{ $squads->m }}</td>
                <td>{{ $squads->mig }}</td>
                <td>{{ $squads->total }}</td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
