@extends('layout')
@section('content')
@include('partial.alerts')

<div class="row">

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4>{{ $data->nama_pic }} / {{ $data->nik_pic }}</h4></div>
    <div class="panel-body" style="text-align: center;">
        @php
            $path4  = "/upload4/alker_sarker/".$nik;
            $th4    = "$path4/$alker-th.jpg";
            $path   = null;

            if (file_exists(public_path().$th4))
            {
            $path = "$path4/$alker";
            }

            if($path)
            {
                $th    = "$path-th.jpg";
                $img   = "$path.jpg";
            } else {
                $th    = null;
                $img   = null;
            }
        @endphp

        @if (@file_exists(public_path().$th))
        <a href="{{ $img }}">
            <img src="{{ $img }}" alt="{{ $alker }}" class="image" height="400px" width="400px">
        </a>
        @else
        <img src="/image/placeholder.gif" alt="{{ $alker }}" class="image" height="400px" width="400px">
        @endif
    </div>
  </div>
</div>

<div class="col-sm-6">
  <div class="panel panel-primary">
    <div class="panel-heading text-center" style="background-color: white; border-color: white"><h4>Detail</h4></div>
    <div class="panel-body table-responsive">
      <table class="table table-striped">
          <tbody>
              <tr>
                  <td class="text-center">Alker</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->alker }}</td>
              </tr>
              <tr>
                  <td class="text-center">SN</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->sn }}</td>
              </tr>
              <tr>
                  <td class="text-center">Merk</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->merk }}</td>
              </tr>
              <tr>
                  <td class="text-center">Tipe</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->tipe }}</td>
              </tr>
              <tr>
                  <td class="text-center">Tahun Pengadaan</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->tahun_pengadaan }}</td>
              </tr>
              <tr>
                  <td class="text-center">Plan Nomor</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->plan_nomer }}</td>
              </tr>
              <tr>
                  <td class="text-center">Status Alker</td>
                  <td class="text-center">:</td>
                  <td style="text-align: right;">{{ $data->status_alker }}</td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>
</div>

</div>
@endsection