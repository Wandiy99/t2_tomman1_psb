@extends('new_tech_layout')

@section('content')
@include('partial.alerts')
<style>
.container_foto {
  position: relative;
  width: 100%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container_foto:hover .image {
  opacity: 0.3;
}

.container_foto:hover .middle {
  opacity: 1;
}

.text {
  color: white;
  font-size: 16px;
}
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<section>
  <div class="container table-responsive">
    <div class="row">

      <form method="post" enctype="multipart/form-data" autocomplete="off">
      @foreach ($inputPhotos as $item)
      @php
        $find_k = array_search($item, array_column(json_decode(json_encode($data, TRUE) ), 'alker') );
        if($find_k !== FALSE)
        {
          $renew_data = $data[$find_k];
        } else {
          $renew_data = [];
        }
        $path4  = "/upload4/alker_sarker/".$auth->id_karyawan;
        $th4    = "$path4/$item-th.jpg";
        $path   = null;

        if (file_exists(public_path().$th4))
        {
          $path = "$path4/$item";
        }

        if($path)
        {
            $th    = "$path-th.jpg";
            $img   = "$path.jpg";
        } else {
            $th    = null;
            $img   = null;
        }
      @endphp
      <table class="table table-md">
      <div class="col-12 col-md-12">
          <div class="card mb-4">
              <div class="card-body text-center align-middle">
                  <div class="form-group container_foto">
                      @if(@file_exists(public_path().$th))
                      <a href="{{ $img }}">
                        <img src="{{ $th }}" alt="{{ $item }}" class="image">
                      </a>
                      @else
                        <img src="/image/placeholder.gif" alt="{{ $item }}" class="image">
                      @endif
                      <input type="file" class="hidden form-control" name="photo-{{ $item }}" accept="image/jpeg" required/>
                  </div>
                  <input class="form-control" type="hidden" name="alker[]" value="{{ $item }}">
                  <div class="form-group">
                    <tr>
                      <td>Alker</td>
                      <td>:</td>
                      <td>{{ $item }}</td>
                  </div>
                  <div class="form-group">
                    <tr>
                      <td>Serial Number</td>
                      <td>:</td>
                      <td><input class="form-control" type="text" name="sn[]" placeholder="Masukan Serial Number" value="{{ @$renew_data->sn }}" required></td>
                  </div>
                  <div class="form-group">
                    <tr>
                      <td>Merk</td>
                      <td>:</td>
                      <td><input class="form-control" type="text" name="merk[]" placeholder="Masukan Merk" value="{{ @$renew_data->merk }}" required></td>
                    </tr>
                  </div>
                  <div class="form-group">
                    <tr>
                      <td>Tipe</td>
                      <td>:</td>
                      <td><input class="form-control" type="text" name="tipe[]" placeholder="Masukan Tipe" value="{{ @$renew_data->tipe }}" required></td>
                    </tr>
                  </div>
                  <div class="form-group">
                    <tr>
                      <td>Tahun Pengadaan</td>
                      <td>:</td>
                      <td>
                        <select class="form-control select2" name="tahun_pengadaan[]" id="input-tahun-pengadaan" required>
                          <option value="" selected disabled>Pilih Tahun Pengadaan</option>
                          @foreach ($get_tahun as $tahun)
                          <option data-subtext="description 1" value="{{ $tahun->id }}" <?php if (@$tahun->id == @$renew_data->tahun_pengadaan) { echo "Selected"; } else { echo ""; } ?>>{{ @$tahun->text }}</option>
                          @endforeach
                        </select>
                      </td>
                    </tr>
                </div>
                  <div class="form-group">
                    <tr>
                      <td>Status Alker</td>
                      <td>:</td>
                      <td>
                        <select class="form-control select2" name="status_alker[]" id="input-status-alker" required>
                          <option value="" selected disabled>Pilih Status Alker</option>
                          @foreach ($get_status as $status)
                          <option data-subtext="description 1" value="{{ $status->id }}" <?php if (@$status->id == @$renew_data->status_alker) { echo "Selected"; } else { echo ""; } ?>>{{ @$status->text }}</option>
                          @endforeach
                        </select>
                      </td>
                    </tr>
                  </div>
              </div>
          </div>
      </div>
      </table>
      @endforeach
      <button class="btn btn-primary form-control">Simpan</button>
      </form>                
        
    </div>
  </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script>
  $(document).ready(function() {
    $('.select2').select2();
  });
</script>
@endsection