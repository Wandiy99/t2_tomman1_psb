@extends('layout')

@section('content')
@include('partial.alerts')
<style>
  th {
    text-align: center;
    vertical-align: middle;
  }
</style>
<div class="col-sm-12">
    <div class="white-box">
        <h3 class="box-title m-b-0">LIST ALKER IOAN</h3>
        <div class="table-responsive">
			<table id="table_data" class="display nowrap text-center" cellspacing="0" width="100%">
                <thead>
					<tr>
  						<th>NO</th>
  						<th>EVIDENCE</th>
  						<th>SN</th>
						<th>ALKER</th>
						<th>NIK_PIC</th>
						<th>NAMA_PIC</th>
  						<th>MERK</th>
						<th>TIPE</th>
						<th>TAHUN_PENGADAAN</th>
						<th>PLAN_NOMER</th>
  					</tr>
				</thead>
				<tbody>
				@foreach ($data as $num => $r)
                @php
                    $path4  = "/upload4/alker_sarker/".$r->nik_pic;
                    $th4    = "$path4/$r->alker-th.jpg";
                    $path   = null;

                    if (file_exists(public_path().$th4))
                    {
                    $path = "$path4/$r->alker";
                    }

                    if($path)
                    {
                        $th    = "$path-th.jpg";
                        $img   = "$path.jpg";
                    } else {
                        $th    = null;
                        $img   = null;
                    }
                @endphp
  					<tr>
  						<td>{{ ++$num }}</td>
  						<td>
                            @if(@file_exists(public_path().$th))
                            <a href="{{ $img }}">
                                <img src="{{ $th }}" alt="{{ $r->alker }}" class="image">
                            </a>
                            @else
                                <img src="/image/placeholder.gif" alt="{{ $r->alker }}" class="image">
                            @endif
                        </td>
  						<td><a href="/alker/{{ $r->nik_pic }}/{{ $r->alker }}">{{ $r->sn ?: "-" }}</a></td>
  						<td>{{ $r->alker ?: "-" }}</td>
  						<td>{{ $r->nik_pic ?: "-" }}</td>
  						<td>{{ $r->nama_pic ?: "-" }}</td>
  						<td>{{ $r->merk ?: "-" }}</td>
  						<td>{{ $r->tipe ?: "-" }}</td>
  						<td>{{ $r->tahun_pengadaan ?: "-" }}</td>
  						<td>{{ $r->plan_nomer ?: "-" }}</td>
  					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table_data').DataTable({
        select: true,
        dom: 'Blfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
                {
                    extend: 'copy',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                },
                {
                    extend: 'excel',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                },
                {
                    extend: 'print',
                    title: 'LIST NTE WAREHOUSE TOMMAN'
                }
            ]
        });
    });
</script>
@endsection