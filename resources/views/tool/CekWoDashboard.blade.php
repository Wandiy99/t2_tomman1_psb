@extends('layout')

@section('content')
  @include('partial.alerts')
  	<div class="panel panel-default">
  		<div class="panel-body">
  			<form method="GET">
  				<div class="row">
  					<div class="col-md-2">
  						<div class="form-group">
	  						<label>Tanggal Awal</label>
	  						<input type="text" name="tanggalAwal" id="tanggalAwal" class="form-control" value="{{ $tanggalAwal ?: date('Y-m-d') }}">
  						</div>
  					</div>

  					<div class="col-md-2">
  						<div class="form-group">
	  						<label>Tanggl Akhir</label>
	  						<input type="text" name="tanggalAkhir" id="tanggalAkhir" class="form-control" value="{{ $tanggalAkhir ?: date('Y-m-d') }}">
  						</div>
  					</div>

  					 <div class="col-md-4">
  						<div class="form-group">
							<label>Sektor</label>
		  					<input type="text" name="sektor" id="sektor" value="{{ $sektorSelect }}" class="form-control">
  						</div>
  					</div>

             <div class="col-md-4">
              <div class="form-group">
              <label>Jenis Data</label><br>
                <input type="radio" name="jenis" id="jenis" value="all" <?php  if ($jenisPil=="all") echo 'checked'; ?> >ALL
                <input type="radio" name="jenis" id="jenis" value="psb" <?php  if ($jenisPil=="psb") echo 'checked'; ?> >Provisioning
                <input type="radio" name="jenis" id="jenis" value="asr" <?php  if ($jenisPil=="asr") echo 'checked'; ?> >Assurance
              </div>
            </div>
  				</div>

  				<div class="row">
  					<div class="col-md-12">
  						<input type="submit" value="Proses" class="btn btn-primary">
  					</div>
  				</div>
  			</form>
  		</div>
  	</div>

    @foreach($getSektor as $listSektor)
        <div class="panel panel-primary">
            <div class="panel-heading">{{ $listSektor }}</div>
            <div class="panel-body">
              <table class="table table-bordered">
                <tr>
                  <th width="3%">NO</th>
                  <th width="80%">Tim</th>
                  <th>AO</th>
                  <th>MO</th>
                  <th>IN</th>
                  <th>Total</th>
                </tr>
                <?php $no = 1; ?>
                @foreach($getData as $data)
                  @if($data->title==$listSektor)
                      <tr>
                        <?php $total = 0; $total += $data->jumlahAo + $data->jumlahMo + $data->jumlahIn ?>
                        <td>{{ $no++ }}</td>
                        <td>{{ $data->uraian }}</td>
                        <td><a href="{{ route('cekwo.detail',['jenis'=>'AO', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahAo }}</a></td>
                        <td><a href="{{ route('cekwo.detail',['jenis'=>'MO', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahMo }}</a></td>
                        <td><a href="{{ route('cekwo.detail',['jenis'=>'IN', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahIn }}</a></td>
                        <td>{{ $total }}</td>
                      </tr>
                  @endif
                @endforeach
              </table>
            </div>
        </div>
    @endforeach

<!--   	@if(count($getData)<>0)
  		<div class="panel panel-primary">
  			<div class="panel-heading">Dasboard Teknisi UP</div>
  			<div class="panel-body">
  				<table class="table table-bordered">
  					<tr>
	  					<th>NO</th>
	  					<th>Tim</th>
	  					<th>AO</th>
	  					<th>MO</th>
	  					<th>IN</th>
	  					<th>Total</th>
  					</tr>

  					@foreach($getData as $no=>$data)
  					<tr>
  						<?php $total = 0; $total += $data->jumlahAo + $data->jumlahMo + $data->jumlahIn ?>
  						<td>{{++$no}}</td>
  						<td>{{ $data->uraian }}</td>
  						<td><a href="{{ route('cekwo.detail',['jenis'=>'AO', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahAo }}</a></td>
  						<td><a href="{{ route('cekwo.detail',['jenis'=>'MO', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahMo }}</a></td>
  						<td><a href="{{ route('cekwo.detail',['jenis'=>'IN', 'tglAwal'=>$tanggalAwal, 'tglAkhir'=>$tanggalAkhir, 'idRegu'=>$data->id_regu]) }}">{{ $data->jumlahIn }}</a></td>
  						<td>{{ $total }}</td>

  					</tr>
  					@endforeach
  				</table>
  			</div>
  		</div>
  	@endif -->

@endsection
@section('plugins')
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/bower_components/select2/select2.min.js"></script>
	<script>
		var data = <?= json_encode($sektor) ?>;
		$('#sektor').select2({data:data});

		$('#tanggalAwal').datepicker({
			format : 'yyyy-mm-dd',
			autoClose : true
		}).on('changeDate',function(){
			$(this).datepicker('hide');
		});

		$('#tanggalAkhir').datepicker({
			format : 'yyyy-mm-dd',
			autoClose : true
		}).on('changeDate',function(){
			$(this).datepicker('hide');
		});

	</script>
@endsection
