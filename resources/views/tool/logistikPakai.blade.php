@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="panel panel-default">
          <div class="panel-heading">
              Logistik Pemakaian
          </div>
          <div class="panel-body">
              <form method="post" class="form-horizontal">
                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="noRFc" class="control-label">No. RFC</label>
                            <input name="noRFc" type="text" id="noRFc" class="form-control" value="{{ old('noRFc') }}"/>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="nama_gudang" class="control-label">Nama Gudang</label>
                          <input name="nama_gudang[]" type="text" id="nama_gudang" class="form-control" />
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-refresh"></span>
                              <span>Proses</span>
                          </button>
                      </div>
                  </div>
              </form>
          </div>
      </div>

      @if (!empty($data))
          <form method="get" action="/tool/logistik/excelmaterialpakai" >
              <input type="submit" class="btn btn-primary" value="Export ke Excel">
          </form><br>

          <div class="panel panel-default" id="info">
          <div class="panel-heading">List</div>
              <div class="panel-body table-responsive">
                  <table class="table table-bordered table-fixed">
                      <tr>
                          <th>Gudang</th>
                          <th>NIK Pemakai</th>
                          <th>TIM</th>
                          <th>MITRA</th>
                          <th>No. RFC</th>
                          <th>ID Barang</th>
                          <th>Nama Barang</th>
                          <th>Keluar Gudang</th>
                          <th>Jumlah Terpakai</th>
                          <th>Saldo Diteknisi</th>                                               
                      </tr>
                      @foreach($data as $no => $d)
                          <tr>
                              <td>{{ $d->gudang }}</td>
                              <td>{{ $d->nik }}</td>
                              <td>{{ $d->regu }}</td>
                              <td>{{ $d->mitra }}</td>
                              <td>{{ $d->rfc }}</td>
                              <td>{{ $d->id_item }}</td>
                              <td>{{ $d->nama_item }}</td>
                              <td>{{ $d->jumlah }}</td>
                              <td>{{ $d->jmlTerpakai }}</td>
                              <td>{{ $d->jumlah - $d->jmlTerpakai }}</td>
                          </tr>
                      @endforeach
                  </table>
              </div>
          </div>
      @endif
@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {
            var gudang = <?= json_encode($gudang) ?>;
            $('#nama_gudang').select2({
                data: gudang,
                placeholder: 'Pilih Gudang',
                allowClear: true,
                multiple: true
                // maximumSelectionSize: 
                // formatSelection: function(data) { return data.text },
                // formatResult: function(data) {
                //   return  data.text;
                // }
            });
        });
    </script>
@endsection