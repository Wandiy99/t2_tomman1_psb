@extends('layout')

@section('content')
  @include('partial.alerts')
      <div class="panel panel-default">
          <div class="panel-heading">
              Logistik Kembali
          </div>
          <div class="panel-body">
              <form method="post" class="form-horizontal">
                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="noRfc" class="control-label">No RFC</label>
                            <input name="noRfc" type="text" id="noRfc" class="form-control" value="{{ old('noRfc') }}"/>
                      </div>
                  
                      <div class="col-md-12">
                          <label for="proaktif_id" class="control-label">Project ID</label>
                            <input name="proaktif_id" type="text" id="proaktif_id" class="form-control" value="{{ old('proaktif_id') }}"/>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="nama_gudang" class="control-label">Nama Gudang</label>
                          <input name="nama_gudang[]" type="text" id="nama_gudang" class="form-control" />
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <label for="tgl" class="control-label">Tgl Kembali</label>
                          <input name="tgl" type="tgl" id="tgl" class="form-control" value="{{ old('tgl') }}"/>
                        
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-12">
                          <button class="btn btn-primary" type="submit">
                              <span class="glyphicon glyphicon-refresh"></span>
                              <span>Proses</span>
                          </button>
                      </div>
                  </div>
              </form>
          </div>
      </div>

      @if (!empty($data))
          <form method="get" action="/tool/logistik/excelmaterialkembali" >
              <input type="submit" class="btn btn-primary" value="Export ke Excel">
          </form><br>

          <div class="panel panel-default" id="info">
          <div class="panel-heading">List</div>
              <div class="panel-body table-responsive">
                  <table class="table table-bordered table-fixed">
                      <tr>
                          <th>Id Pengembalian</th>
                          <th>Tgl Pengembalian</th>
                          <th>Nama Gudang</th>
                          <th>Project</th>
                          <th>Petugas Gudang</th>
                          <th>ID Barang</th>
                          <th>Nama Barang</th>
                          <th>Jumlah Kembali</th>
                          <th>Satuan</th>
                          <th>No. Rfc</th>
                                               
                      </tr>
                      @foreach($data as $no => $d)
                          <tr>
                              <td>{{ $d->ID_Pengembalian }}</td>
                              <td>{{ $d->Tgl_Pengembalian }}</td>
                              <td>{{ $d->Nama_Gudang }}</td>
                              <td>{{ $d->Project }}</td>
                              <td>{{ $d->Petugas_Gudang }}</td>
                              <td>{{ $d->ID_Barang }}</td>
                              <td>{{ $d->Nama_Barang }}</td>
                              <td>{{ $d->jumlah }}</td>
                              <td>{{ $d->Satuan }}</td>
                              <td>{{ $d->NO_RFC }}</td>
                          </tr>
                      @endforeach
                  </table>
              </div>
          </div>
      @endif
@endsection

@section('plugins')
    <script src="/bower_components/select2/select2.min.js"></script>
    <script src="/bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $(function() {
            var project = <?= json_encode($project) ?>;
            $('#proaktif_id').select2({
                data: project,
                placeholder: 'Pilih project',
                formatSelection: function(data) { return data.text },
                formatResult: function(data) {
                  return  data.text;
                }
            });
            var gudang = <?= json_encode($gudang) ?>;
            $('#nama_gudang').select2({
                data: gudang,
                placeholder: 'Pilih Gudang',
                allowClear: true,
                multiple: true
                // maximumSelectionSize: 
                // formatSelection: function(data) { return data.text },
                // formatResult: function(data) {
                //   return  data.text;
                // }
            });

            var day = {
              format : 'yyyy-mm-dd',
              viewMode: 0,
              minViewMode: 0
            };
       
            $('#tgl').datepicker(day).on('changeDate', function(e){
              $(this).datepicker('hide');
            });
        });
    </script>
@endsection