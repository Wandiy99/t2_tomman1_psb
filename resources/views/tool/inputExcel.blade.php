@extends('layout')

@section('content')
  @include('partial.alerts')
 
  <form id="submit-form" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="sns" value="[]" /> 
    <div class="form-group">
      <label class="control-label" for="tes">Import Excel</label>
      <input type="file" id="tes" class="form-control"/>
    </div>
  
    <div style="margin:40px 0 20px">
      <button class="btn btn-primary">Simpan</button>
    </div>
    <div id="jumlah"></div>
    <div class="list-group"></div>
    
  </form>
  @if (isset($data->id))
  <form id="delete-form" method="post" autocomplete="off">
    <input name="_method" type="hidden" value="DELETE">
      <div style="margin:40px 0 20px">
        <button class="btn btn-danger">Hapus</button>
      </div>
  </form>
  <div class="panel panel-primary">
  <div class="panel-heading">Recorded NTE</div>
  <div class="list-group">
    @foreach($sn as $no => $sn_nte)
      <div class="list-group-item">
        <span class="label label-info">{{ ++$no }}</span>
        <strong>{{ $sn_nte->sn }}</strong>
        <span class="badge">{{ $sn_nte->jenis_nte }}</span>
      </div>
    @endforeach
  </div>
</div>
  @endif
  <div class="loading" style="visibility:hidden">Loading&#8230;</div>
  <link rel="stylesheet" href="/bower_components/loader/loader.css" />

  <script src="/bower_components/excel/zip/WebContent/zip.js"></script>
  <script src="/bower_components/excel/async-master/dist/async.js"></script>
  <script src="/bower_components/excel/underscore.js"></script>
  <script src="/bower_components/excel/xlsxParser.js"></script>
  <script>
    $(function() {
      var data_sn = [];
      $("#tes").change(function(e){
        data_sn = [];
        $(".loading").css({"visibility":"visible"});
        $(".removable").remove();
        var file = document.getElementById('tes').files[0];
        zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
        xlsxParser.parse(file).then(function(data) {
          console.log(data);
          var count = 0;
          for(var i=0;i<data.length;i++){
            var sn = data[i][2], type = data[i][1];
            if(data[i][0]){
              data_sn.push({
                orderId     : data[i][0],
                orderNo     : data[i][1],
                orderName   : data[i][2],
                orderAddr   : data[i][3],
                orderNotel  : data[i][4],
                orderKontak : data[i][5],
                orderDate   : data[i][6],
                orderDatePs : data[i][7],
                orderCity   : data[i][8],
                orderStatus : data[i][9],
                orderStatusId : data[i][10],
                orderPlasa  : data[i][11],
                orderNcli   : data[i][12],
                ndemSpeedy  : data[i][13],
                ndemPots    : data[i][14],
                orderPaket  : data[i][15],
                orderPaketID: data[i][16],
                kcontact    : data[i][17],
                username    : data[i][18],
                alproname   : data[i][19],
                tnNumber    : data[i][20],
                reserveTn   : data[i][21],
                reservePort : data[i][22],
                jenisPsb    : data[i][23],
                sto         : data[i][24],
                lon         : data[i][25],
                lat         : data[i][26],
              });
              
              $(".list-group").append("<div class='list-group-item removable'><span>" + data[i][0] + "<span> <span>" + data[i][2] + "<span></div>");
              count++;
            } 
          }
          
          $('input[name=sns]').val(JSON.stringify(data_sn));
          $("#jumlah").append("<span class=removable>SN Found : " + count + "</span>");
          $(".loading").css({"visibility":"hidden"});
        }, function(err) {
          alert.log('error', err);
        });
      })
      
	  })
  </script>
@endsection
