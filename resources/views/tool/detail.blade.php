<div class="panel panel-default">
	<div class="panel-heading"></div>
	<div class="panel-body table-responsive">
	<table class="table table-bordered table-fixed">
	   <tr>
	      <th>RFC</th>
	      <th>Pemakai</th>
	      <th>ID Item</th>
	      <th>Item</th>
	      <th>Keluar Gudang</th>
	      <th>Terpakai</th>
	      <th>Kembali</th>
	   </tr>

	   @foreach($data as $dt)
			<tr>
				<td>{{ $dt->rfc }}</td>
				<td>{{ $dt->nik }}</td>
				<td>{{ $dt->id_item }}</td>
				<td>{{ $dt->nama_item }}</td>
				<td>{{ $dt->jumlah }}</td>
				<td>{{ $dt->jmlTerpakai ?: '-' }}</td>
				<td>{{ $dt->jmlKembali ?: '-' }}</td>
			</tr>      
	   @endforeach
	</table>
	</div>
</div>