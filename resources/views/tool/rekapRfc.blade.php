@extends('layout')

@section('content')
  @include('partial.alerts')  
  <div class="modal fade" id="detailModal">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-body">
                  <div id="txtHint"></div>
                  <input type="hidden" name="modal_id" class="form-control" id="modal_id">
              </div>
              
              <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>

  <div class="panel panel-primary" id="info">
      <div class="panel-heading">Rekap Material RFC</div>
      <div class="panel-body table-responsive">
        <table class="table table-bordered table-hover table-fixed ">
           <tr>
              <th>No</th>
              <th>RFC</th>
              <th>TIM</th>
              <th>NIK 1 </th>
              <th>NIK 2 </th>
              <th>Keluar Gudang</th>
              <th>Terpakai</th>
              <th>Kembali</th>
           </tr>

           @foreach($data as $no=>$dt)
              <?php 
                $rfcReplace = str_replace('/', '-', $dt->rfc);
              ?>
              
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>
                    <button type="button" class="btn btn-primary fa fa-info" 
                                          data-target="detailModal" data-id="{{ $rfcReplace }}" 
                                          data-nama="{{ $rfcReplace }}" data-original-title="Dispatch">{{ $dt->rfc }}
                    </button>
                  </td>
                  <td>{{ $dt->regu ?: '-' }}</td>
                  <td>{{ $dt->nik1 ?: '-' }}</td>
                  <td>{{ $dt->nik2 ?: '-' }}</td>
                  <td>{{ $dt->outGudang ?: '-' }}</td>
                  <td>{{ $dt->terpakai ?: '-' }}</td>
                  <td>{{ $dt->kembali ?: '-' }}</td>
              </tr>
           @endforeach
        </table>
      </div>
  </div>

  <script>
      $(document).ready(function () {
          $('.fa-info').click(function(event){
              $('#modal_id').val(event.target.dataset.id);
              
              var id = document.getElementById('modal_id').value;
              console.log(id);
              $.get("/tool/rekap-rfc/"+id+"/detail", function(text) {
                  $("#txtHint").html(text);
              });
              
              $('#detailModal').modal('show');
          });
      });
  </script>
@endsection
