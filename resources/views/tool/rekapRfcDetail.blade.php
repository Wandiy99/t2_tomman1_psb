@extends('layout')

@section('content')
  @include('partial.alerts')  
  <div class="panel panel-primary" id="info">
      <div class="panel-heading">Rekap Material RFC</div>
      <div class="panel-body table-responsive">
        <table class="table table-bordered table-hover table-fixed ">
           <tr>
              <th>No</th>
              <th>RFC</th>
              <th>TIM</th>
              <th>NIK 1 </th>
              <th>NIK 2 </th>
              <th>ID Item</th>
              <th>Item</th>
              <th>Keluar Gudang</th>
              <th>Terpakai</th>
              <th>Kembali</th>
           </tr>

           @foreach($data as $no=>$dt)
              <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $dt->rfc }}</td>
                  <td>{{ $dt->regu ?: '-' }}</td>
                  <td>{{ $dt->nik1 ?: '-' }}</td>
                  <td>{{ $dt->nik2 ?: '-' }}</td>
                  <td>{{ $dt->id_item }}</td>
                  <td>{{ $dt->nama_item }}</td>
                  <td>{{ $dt->jumlah }}</td>
                  <td>{{ $dt->jmlTerpakai }}</td>
                  <td>{{ $dt->jmlKembali }}</td>
              </tr>
           @endforeach
        </table>
      </div>
  </div>
@endsection
