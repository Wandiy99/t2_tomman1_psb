<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@loginPage');
Route::post('/login', 'LoginController@validate_login');
Route::get('/reload-captcha', 'LoginController@reloadCaptcha')->name('reload-captcha');
Route::get('/auth-verification', 'LoginController@authverification');
Route::post('/auth-verification', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

// use TG;

// Route::get('/testgram', function (){
//   TG::sendMsg('user#100356956', 'Hello there!');
// });

Route::get('/api/routeMapOrder','PsbController@get_rand_coor');
Route::get('/api/routeMapDev','PsbController@get_deviasi_koor');
Route::get('/api/getDistanceRoute/{id}','PsbController@getDistanceRoute');


Route::get('/api/checkValins/{id}', 'ApiController@checkValins');
Route::get('/api/get8pelanggan', 'ApiController@get8pelanggan');
Route::post('/api/update_jointer', 'ApiController@update_jointer');
Route::get('/api/dorongPickupOnline', 'ApiController@dorongPickupOnline');
Route::post('/api/dalapa-comparin', 'ApiController@dalapa_comparin_tr6');
Route::get('/api/profile_teknisi', 'ApiController@profileTeknisi');
Route::get('/api/orderPs3Month', 'ApiController@orderPs3Month');
Route::post('/api/sendWhatsapp', 'ApiController@sendWhatsapp');

Route::get('/api/ipayment', 'ApiController@get_ipayment');

// kpro foto
Route::get('/get_kpro_foto/{id}','PsbController@get_kpro_foto');

// trial new theme
Route::get('/elitethemex','LoginController@elitetheme');
Route::get('/trialStarclick','DashboardController@trialStarclick');

// bot CoC_TA
Route::post('/botCoC_TA','BotController@botCoC_TA');

// bot QCBorneo
Route::post('/botQC','BotController@botQC');
Route::get('/grabQC/{id}','BotController@grabQCBorneo');
Route::get('/testQC/{id}','BotController@testQC');
Route::get('/testQCAPI/{id}','BotController@grabQCBorneobyAPI');
Route::get('/grabRISMA','GrabController@grabRISMA');


// bot PeduliAlpro
Route::post('/api/botController/PeduliAlpro','BotController@PeduliAlpro');
Route::post('/api/botController/TutupAlpro','BotController@TutupAlpro');
Route::get('/testAlprox','BotController@testAlpro');
Route::get('/GerakanPeduliAkses','DashboardController@GerakanPeduliAkses');

// bot Helpdesk Kalsel
Route::post('/botHelpdeskKalsel','BotController@botHelpdeskKalsel');

// bot Tomman-GO
Route::post('/api/botController/botTommanGO','BotController@botTommanGO');

// bot Perwira-BOT
Route::post('/api/botController/botPerwira', 'BotController@botPerwira');

// bot laporPeduliAlproOdp
Route::post('/api/botController/laporPeduliAlproOdp','BotController@laporPeduliAlproOdp');

Route::get('/api/LaporOdpBotController', 'LaporOdpBotController@startBot');

// fix starclick
Route::get('/fix_sto','GrabController@fix_sto');
Route::get('/sc/{id}','GrabController@sync_ax');

// API
Route::get('/getBMKG/{id}','GrabController@getBMKG');
Route::get('/get_progress_teknisi/{start_date}/{end_date}','ApiController@get_progress_teknisi');
Route::get('/get_progress_teknisi_pra/{start_date}/{end_date}','ApiController@get_progress_teknisi_pra');
Route::get('/get_progress_by_sc/{id}','ApiController@get_progress_by_sc');
Route::get('/get_progress_by_myir/{id}','ApiController@get_progress_by_myir');
Route::get('/get_dashboardComparin/{tgl}/{witel}','ApiController@dashboardComparin');
Route::get('/get_dashboardComparinList/{witel}/{tgl}/{status}','ApiController@dashboardComparinList');

// approve QC

Route::get('/autoapprove_qc1_onecall/{witel}','GrabController@autoapprove_qc1_onecall');
Route::get('/approveQC/{id}/{sc}/{id_regu}','DispatchController@approveQC');
Route::get('/simpunipetarika','DispatchController@simpunipetarika');

// reject QC history
Route::get('/rejectQC/{id}/{sc}/{id_regu}','DispatchController@rejectQC');

// grab starclick
// Route::get('/grabstarclickncx_backend/{witel}','GrabController@grabstarclickncx_backend');
// Route::get('/grabscncx/{witel}','GrabController@grabstarclickncx');
Route::get('/grabUnspecSaldoSemesta', 'GrabController@grabUnspecSaldoSemesta');
Route::get('/grabQCBorneo/{bulan}','GrabController@grabQCBorneo');
Route::get('/grabQCBorneoTR6/{bulan}/{kat}', 'GrabController@qcborneo_tr6');
Route::get('/sync_starclick_to_myir/{date}','GrabController@sync_starclick_to_myir');
Route::get('/clean_up','GrabController@clean_up');
Route::get('/update_starclick_up','GrabController@update_starclick_up');
Route::get('/get_sc_not_sync/{date}','GrabController@get_sc_not_sync');

Route::get('/get_myir_from_kcontact/{date}','GrabController@get_myir_from_kcontact');

// mcore lite
Route::get('/mcore','McoreController@home');
Route::post('/mcore','McoreController@check');

//
// Route::get('/check_history','PsbController@check_history');
// Route::post('/check_history','PsbController@exec_check_history');
Route::get('/checktiket', 'PsbController@checktiket');
Route::get('/testNossa2','GrabController@ok');
Route::get('/batch_send_to_dalapa/{date}','ApiController@batch_send_to_dalapa');
Route::get('/check_sc_not_sync/{date}','GrabController@check_sc_not_sync');
Route::get('/check_sc/{id}','GrabController@check_sc');
Route::get('/send_to_dalapa/{id}','ApiController@send_to_dalapa');
Route::get('/no_hp_onecall/{witel}/{tglAwal}/{tglAkhir}','GrabController@no_hp_onecall');
Route::get('/date_loop','GrabController@date_loop');
Route::get('/api_foto','ApiController@foto_api_new');
Route::get('/api_foto_by_date','ApiController@foto_api_by_date');
Route::get('/foto/{id}/{name_file}','ApiController@getFoto');
Route::get('/sinkron_foto_qc', 'ApiController@sinkron_foto_qc');
Route::get('/api_list_order_up','ApiController@workorder');
Route::get('/api_status_order','ApiController@status_order');
Route::get('/grabscncx_manual/{date}','GrabController@newStarclick');
Route::get('/sendkprodaily/{sektor}','PsbController@sendkprodaily');
Route::get('/sendkprobydate/{sektor}/{date}','PsbController@sendkprobydate');
Route::get('/sendbyfotoname/{fotoname}/{sektor}/{date}','PsbController@sendbyfotoname');

Route::get('/public_kehadiran','DashboardController@public_kehadiran');

Route::get('/kpisektor','DashboardController@kpisektor');

Route::get('/publicPSRE', 'DashboardController@publicPSRE');

Route::get('/monitoringkpi/{sektor}/{tgl}','AssuranceController@monitoringkpi');

Route::get('/public_produktifitastek/{tgl}/{lokasi}/{mitra}', 'DispatchController@produktifitastek_public');

Route::get('/3on3TextMode','AssuranceController@A3on3TextMode');

Route::get('/photogrid/{id}','LaporanController@photogrid');

Route::get('len','AssuranceController@len');
Route::get('/public_manja/{date}','AssuranceController@public_manja');
Route::get('/assurance/public_ttr','AssuranceController@public_ttr');
Route::get('/public_manja2/{date}','AssuranceController@public_manja2');
Route::get('/public_manja_hvc','AssuranceController@public_manja_hvc');
Route::get('/listbyumur_public/{sektor}','DashboardController@listbyumur_public');
Route::get('/dispatch/matrixPublic', 'DispatchController@matrixPublic');

// matrix assurance public
Route::get('/dispatch/matrixPublic', 'DispatchController@matrixPublic');

//ibooster inet
Route::get('/monetIbooster/data_ajx/{witel}', 'AssuranceController@get_monet_ajax');
Route::get('/assurance/monetIbooster', 'AssuranceController@monetIbooster');
Route::post('/assurance/monetIboosterSave/{witel}', 'AssuranceController@monetIboosterSave');
Route::get('/ticketing/createSQM/{incident}/{inet}/{ncli}', 'TicketingController@createSQM');

// sisa saldo tiket assurance bot public
Route::get('/sisaTiketNossaPublic', 'DashboardController@sisaTiketNossaPublic');

// public pengaduan online
Route::get('/online/ticketing', function(){
  return redirect('https://bit.ly/m/EdukasiIndiHome');
});
// Route::get('/online/ticketing','TicketingController@indexPublic');
// Route::post('/online/ticketing','TicketingController@indexPublicSave');
Route::get('/assurance/auto/dispatch/{odp}/{id}', 'TicketingController@redispatch');

Route::get('/createBulkTicketing','TicketingController@createBulkTicketing');
Route::post('/createBulkTicketing','TicketingController@saveBulkTicketing');

//review pelanggan
Route::get('/review_pelanggan/{id}','ReviewController@review');
Route::post('/review_pelanggan/{id}','ReviewController@save_review');
Route::get('/r/{id}','ReviewController@review');
Route::post('/r/{id}','ReviewController@save_review');
Route::get('/rv/{id}','ReviewController@review_2');
Route::post('/rv/{id}','ReviewController@save_review_2');
Route::get('/encryptLink/{id}','ReviewController@encrypt');
Route::get('/checkrev','ReviewController@check');
Route::get('/rankingrev','ReviewController@ranking');
Route::get('/review/followup/{date}','ReviewController@followupList');
Route::get('/review/followup/detail/{id}','ReviewController@followupDetail');
Route::post('/review/followup/detail/{id}','ReviewController@followupSave');


//dokumentasi Foto
Route::get('/dokumentasi/{penyebab}/{id_dt}/{action}','AssuranceController@dokumentasi');

Route::get('/barcode/{id}','GrabController@barcode');

Route::get('/jsonOrder/{id}', 'LaporanController@getJsonMaint');
Route::get('/barcode','GrabController@barcode');

// odpregional
Route::get('/odpregional/x','GrabController@odpregional');

//Test map
Route::get('/mapTest','MapController@test');

//qcondesk trigger
Route::get('/qcondesktrigger','QcondeskController@trigger');

// detail_gangguan_close
Route::get('/detailgangguanclose','GrabController@detailGangguanClose');
Route::get('/detailGangguanClose3on3','GrabController@detailGangguanClose3on3');
Route::get('/detailGangguanClose3on3Fisik','GrabController@detailGangguanClose3on3Fisik');

// ibooster
Route::get('/grabIbooster/{speedy}/{order}','AssuranceController@grabIbooster');
Route::get('/grabIboosterbyIN/{in}/{menu}','AssuranceController@grabIboosterbyIN');
Route::get('/grabIboosterbyManualIN/{inet}/{in}/{menu}','AssuranceController@grabIboosterbyManualIN');
Route::get('/batchGrabIbooster','AssuranceController@batchGrabIbooster');

// gaul sebab action
Route::get('/gaulbysebab','AssuranceController@gaulbysebab');
Route::get('/gaulbyaction','AssuranceController@gaulbyaction');

Route::get('/assuranceStatusOpen/{id}','DashboardController@assuranceStatusOpen');
Route::get('/gaul/{periode}','DispatchController@gaul');
Route::get('/batchrefixkordinat/{periode}','PsbController@batchrefixkordinat');
Route::get('/refixkordinat/{id}/{sc}','PsbController@refixkordinat');
Route::get('/batchtokpro/{date}','PsbController@batchtokpro');
Route::get('/grab/starclick_opt', 'GrabController@starclick');
// Route::get('/grab/starclickmin', 'GrabController@starclickMin');
Route::get('/grab/nonatero/{periode}', 'GrabController@nonatero');
Route::get('/assurance/telco/{periode}/{rayon}', 'AssuranceController@telco');
Route::get('/grab/nonatero_gaul/{periode}', 'GrabController@nonatero_gaul');
Route::get('/provisioning', 'DashboardController@provisioning_wallboard');
Route::get('/provisioningbyjam/{id}/{witel}', 'DashboardController@provisioningbyjam');
Route::get('/provisioningbyhari/{id}/{witel}', 'DashboardController@provisioningbyhari');
Route::get('/provisioning2byhari/{id}/{witel}', 'DashboardController@provisioning2byhari');
Route::get('/rocexcel', 'AssuranceController@grab_roc_excel');
Route::get('/odpftp', 'GrabController@odp');
Route::get('/a2sabsengrab', 'GrabController@a2s_absensi');
Route::get('/autoabsen/{id}', 'DashboardController@autoabsen');
Route::get('/absenkanbosku', 'DashboardController@absenkanbosku');
Route::get('/autononwork/{id}', 'GrabController@autononwork');
Route::get('/ranking/no-update', 'LaporanController@rankingNoUpdate');
Route::get('/grabkpro/val','GrabController@grab_kpro_val');
Route::get('/grabkpro/tot','GrabController@grab_kpro_tot');
Route::get('/grabkpro/preorder','GrabController@grabkpropreorder');
Route::get('/kproval/{tgl}/{jam}','PsbController@valTot');
Route::get('/kproval/{tgl}/{jam}/{sto}','PsbController@detVal');
Route::get('/kproval/{tgl}/{jns}/{sto}/{jam}','PsbController@detTot');
Route::get('/grapwifi','GrabController@grabWifi');
Route::get('/grapA2sGangguanOpen', 'GrabController@a2sGgnOpen');
Route::get('/grapAlistaKeluar', 'GrabController@grabAlistaVersi2');
Route::get('/autosendtobotkpro/{id}','DashboardController@autosendtobotkpro');
Route::get('/sendtobotkpro/{id}','DashboardController@sendtobotkpro');
Route::get('/dashboard/mbsp', 'DashboardController@MBSP');
Route::get('/sc-grab', 'AssuranceController@sc_grab');
Route::get('/iss-grab', 'AssuranceController@iss_grab');
Route::get('/dashboard/list/{jenis}/{status}/{area}', 'DashboardController@GetList');
Route::get('/grapAlistaKeluarByRfc/{rfc}/{gudang}', 'GrabController@grabAlistaByRfc');
Route::get('/grapBaOnline','GrabController@baDigital');
Route::get('/grapDnsSummaryCheklistTl/tes','GrabController@dnsCheklistTl');

Route::get('/roc', 'AssuranceController@roc_sync');
Route::get('/ms2n/publicSync/{id}','Ms2nController@publicSync');
Route::get('/ms2n/publicAllSync/','Ms2nController@publicAllSync');
Route::get('/ms2n/publicAllMbspSync/','Ms2nController@publicAllMbspSync');
Route::get('/ms2n/publicMbspSync/{id}','Ms2nController@publicMbspSync');
Route::get('/ms2n/migrasiSync/{id}','Ms2nController@migrasiSync');
Route::get('/ms2n/sync/{id}', 'Ms2nController@allsync');
Route::get('/ms2n/refix_ndemSpeedy','Ms2nController@refix_ndemSpeedy');

Route::get('/deployers', 'AssuranceController@grab_dep');
Route::get('/deployer2', 'AssuranceController@grab_deployer');
// Route::get('/compare', 'AssuranceController@compare');
Route::get('/dashboard/PIlist/{witel}/{jam}', 'DashboardController@PIlist');
// Route::get('/register', 'LoginController@register');
// Route::post('/register', 'LoginController@registerSave');
Route::get('/assurance/matrik', 'AssuranceController@matriks_dispatch');
Route::get('/cacti', 'AssuranceController@grab_cacti');
Route::get('/cacty', 'AssuranceController@cacti');
Route::get('/attention', 'TelegramController@index');
Route::post('/attention', 'TelegramController@send');
Route::get('/nossa/{id}','GrabController@nossa');
Route::get('/nossaIn/{id}','GrabController@nossaIn');
// Route::get('/nossaInx/{id}','GrabController@nossaINx');
// Route::get('/grabNossa','GrabController@grabNossa');
// Route::get('/grabNossaFix','GrabController@GrabNossaFix');
// Route::get('/testNossa','GrabController@testNossa');
// Route::get('/grabNossaWOC','GrabController@grabNossaWOC');
// Route::get('/grabNossaMaintenance','GrabController@grabNossaMaintenance');
// Route::get('/assuranceNossa/{witel}','DashboardController@nossa');
// Route::get('/assuranceNossaSektor/{witel}','DashboardController@nossasektor');
// Route::get('/assuranceNossaNew/{witel}','DashboardController@nossaNew');
// Route::get('/assuranceNossaList/{id}/{periode}/{witel}','DashboardController@nossaList');
// Route::get('/assuranceNossaListText/{id}/{periode}/{witel}','DashboardController@nossaListText');
// Route::get('/trendPI', 'DashboardController@PI');

// backup photos
Route::get('/run_backup/{id}','BackupController@run');
Route::get('/get_folder/{id}','BackupController@get_folder');

Route::get('/tempQ','DispatchController@tempQ');

Route::get('/matrix-zainal/{date}','DispatchController@matrixZainal');
Route::get('/matrixHDFoto/{tgl}', 'DispatchController@matrixHDFoto');

// matrix prov go
Route::get('/dispatch/matrixGoPublic', 'DispatchController@matrixGoPublic');

// konfirmasi order bot helpdesk
Route::get('/confirmBot', 'BotController@confirmBot');

route::get('/map/ajx_tiang_all', 'PsbController@ajx_tiang_all');
route::get('/map/ajx_odp_all', 'PsbController@ajx_odp_all');

Route::group(['middleware' => 'auth'], function() {

  //ALKER IOAN
  Route::get('/alker', 'IoanAlkerController@index');
  Route::get('/alker/{nik}/{alker}', 'IoanAlkerController@form');
  Route::post('/alker/{nik}/{alker}', 'IoanAlkerController@save');
  Route::get('/laporalker', 'IoanAlkerController@lapor');
  Route::post('/laporalker', 'IoanAlkerController@laporSave');

  //alista controller
  route::get('/alista/request', 'AlistaController@list_request');
  route::post('/alista/request', 'AlistaController@view');

  route::post('/alista/save', 'AlistaController@save');
  route::post('/alista/execute', 'AlistaController@execute');

  // dashboard PI
  Route::get('/dashboardSC','DashboardController@dashboardPI');
  Route::get('/dashboard/psredbsdgs','DashboardController@dashboardPsreDbsDgs');
  Route::get('/dashboard/psredbsdgs/detail','DashboardController@dashboardPsreDbsDgsDetail');
  Route::get('/undispatch_sc/{kat}/{datel}','DashboardController@undispatch_sc');
  Route::get('/undispatch_kpro/{kat}/{datel}','DashboardController@undispatch_kpro');
  Route::get('/pi_needprogress/{grup}/{sektor}','DashboardController@pi_needprogress');
  Route::get('/pi_progress/{grup}/{sektor}','DashboardController@pi_progress');
  Route::get('/pi_kendala/{grup}/{sektor}/{status}','DashboardController@pi_kendala');
  Route::get('/pi_hr/{grup}/{sektor}','DashboardController@pi_hr');
  Route::get('/pi_all/{grup}/{sektor}','DashboardController@pi_all');
  Route::get('/fwfm_all/{grup}/{sektor}','DashboardController@fwfm_all');
  Route::get('/fwfm_needprogress/{grup}/{sektor}','DashboardController@fwfm_needprogress');
  Route::get('/fwfm_progress/{grup}/{sektor}','DashboardController@fwfm_progress');
  Route::get('/fwfm_kendala/{grup}/{sektor}/{status}','DashboardController@fwfm_kendala');
  Route::get('/fwfm_hr/{grup}/{sektor}','DashboardController@fwfm_hr');
  Route::get('/umur_pi/{grup}/{umur}/{sektor}','DashboardController@umur_pi');
  Route::get('/umur_fwfm/{grup}/{umur}/{sektor}','DashboardController@umur_fwfm');
  Route::get('/undispatchSC/{orderDate}','DashboardController@undispatchSC');
  Route::get('/cancelOrder_all/{grup}/{sektor}','DashboardController@cancelOrder_all');
  Route::get('/comparinData/{type}/{group}/{area}','DashboardController@comparinData');
  Route::get('/dashboardPSRE/{type}/{sektor}/{status}/{segmen}', 'DashboardController@dashboardPSRE');
  Route::get('/dashboardFFGTTI/{sektor}/{status}', 'DashboardController@dashboardFFGTTI');

  Route::post('/unsc/update_suvery_ondesk', 'PsbController@update_suvery_ondesk');

  Route::get('/dashboard/reportProvToday', 'DashboardController@reportProvToday');
  Route::post('/dashboard/reportProvTodaySave', 'DashboardController@reportProvTodaySave');

  // dashboard kpi 7 psb
  Route::get('/dashboard/kpi7_psb/{periode}', 'DashboardController@kpi7_sector_psb');
  Route::get('/generate/kpi7_psb/{periode}', 'DashboardController@kpi7_psb_generate');
  Route::get('/dashboard/kpi7_psb', 'DashboardController@kpi7_psb');

  // dashboard progress comparin
  Route::get('/dashboard/progress_totalpi_detail', 'DashboardController@progress_totalpi_detail');

  // dashboard potensi ps
  Route::get('/dashboard/potensiPS', 'DashboardController@dashboardPotensiPS');
  Route::get('/dashboard/potensiPSDetail/{group}/{area}/{status}', 'DashboardController@detailPotensiPS');

  // dashboard report ut qc2
  Route::get('/dashboard/kpro/reportQC2', 'DashboardController@reportQC2');
  Route::get('/dashboard/kpro/reportQC2Detail', 'DashboardController@reportQC2Detail');

  // dashboard monitor qc
  Route::get('/monitoring/prov/{bln}','MonitoringController@provisioning');

  // dashboard Tactical Pickup Online
  Route::get('/dashboard/tactical/pickupOnline', 'DashboardController@pickupOnline');
  Route::get('/dashboard/tactical/pickupOnlineDetail', 'DashboardController@pickupOnlineDetail');
  Route::get('/dashboard/tactical/raportPickupOnline', 'DashboardController@raportPickupOnline');

  // dashboard giveaway PS
  Route::get('/dashboard/giveawayPS', 'DashboardController@giveawayPS');
  Route::get('/dashboard/giveawayPS/detail', 'DashboardController@giveawayPSdetail');

  // dashboard produktifitas teknisi 2022
  Route::get('/dashboard/produktifitasTeam', 'DashboardController@produktifTeam');
  Route::get('/dashboard/produktifitasTeam/detail', 'DashboardController@produktifTeamDetail');

  // kendala saber 2022
  Route::get('/dashboard/kendalaSaber', 'DashboardController@kendalaSaber');

  // dashboard SCBE
  Route::get('/dashboardProgressSCBE','DashboardController@dashboardProgressSCBE');
  Route::get('/scbe_undispatch/{sektor}','DashboardController@scbe_undispatch');

  // kendala alpro new 2022
  Route::get('/kendalaAlpro', 'DashboardController@kendala_alpro');

  //Helpdesk
  Route::get('/helpdesk/dashboard/{date}','HelpdeskController@dashboard');
  Route::get('/helpdesk_prod/fallout/{hd}/{date}','HelpdeskController@get_fallout');
  Route::get('/helpdesk_prod/config/{hd}/{date}','HelpdeskController@helpdesk_config');
  Route::get('/helpdesk_inbox','HelpdeskController@inbox');
  Route::get('/helpdesk/manja/pending','HelpdeskController@inbox_manja');
  Route::get('/helpdesk/manja/berangkat','HelpdeskController@inbox_manja_berangkat');
  // Route::get('/helpdesk_inbox_booked','HelpdeskController@inbox_booked');
  Route::get('/helpdesk_book/{id}','HelpdeskController@book');
  Route::get('/helpdesk_fup/{id}','HelpdeskController@fup');
  Route::post('/helpdesk_fup/{id}','HelpdeskController@save');
  // Route::get('/helpdesk_close/{tgl}','HelpdeskController@close');
  Route::get('/helpdesk_log/{id}','HelpdeskController@log');
  Route::get('/helpdesk_inbox/delete/{sc}/{id}','HelpdeskController@deleteBooked');
  Route::get('/helpdesk/manja/{id}','HelpdeskController@editManja');
  Route::post('/helpdesk/manja/{id}','HelpdeskController@saveManja');
  Route::post('/helpdesk/manja_exkendala/{id}','HelpdeskController@saveExkendala');
  Route::get('/helpdesk/assurance/config','HelpdeskController@hd_config');
  Route::get('/helpdesk/assurance/config/{id}','HelpdeskController@replyConfig');
  Route::post('/helpdesk/assurance/config/{id}','HelpdeskController@saveConfig');

  //cuaca
  Route::get('/cuaca/{id}','DashboardController@cuaca');

  // Assurance
  Route::get('/new_matrix/{date}','AssuranceController@matrix');
  Route::get('/new_matrix/old/{date}','AssuranceController@matrix_old');
  Route::get('/assurance/detailMatrix','AssuranceController@detailMatrix');
  Route::get('/dashboardTekSQM/{date}','AssuranceController@dashboardTekSQM');
  Route::get('/dashboardTekSQMList/{tim}/{kat}/{date}','AssuranceController@dashboardTekSQMList');
  Route::get('/assurance/reportDailyClosingOrderDetail','DashboardController@reportDailyClosingOrderDetail');

  Route::get('/ticketing/dashboardPlasaAssurance','TicketingController@dashboardPlasaAssurance');
  Route::post('/ticketing/dashboardPlasaAssurance','TicketingController@dashboardPlasaAssurance');
  Route::get('/ticketing/dashboardPlasaAssuranceDetail','TicketingController@dashboardPlasaAssuranceDetail');

  // media carring
  Route::get('/assurance/inbox/mediaCarring', 'AssuranceController@inboxMediaCarring');
  Route::post('/assurance/inbox/mediaCarringSave', 'AssuranceController@inboxMediaCarringSave');

  //setting
  Route::get('/accountsetting','LoginController@accountsetting');
  Route::post('/accountsetting','LoginController@accountsettingSave');

  //QC Borneo
  Route::get('/dashboard/QCborneoTeknisi/{date}/{dateend}','DashboardController@QCborneoTeknisi');
  Route::get('/dashboard/QCborneoTeknisiList/{date}/{dateend}/{tek}/{jenis}','DashboardController@QCborneoTeknisiList');
  Route::get('/dashboard/QCborneoTeknisiListDone/{date}/{dateend}/{tek}','DashboardController@QCborneoTeknisiListDone');
  Route::get('/qcborneo/check', 'DashboardController@QCBorneoCheck');
  Route::get('/qcborneo/amo', 'DashboardController@qcFotoAmo');
  Route::get('/dashboard/QCBorneoMitra/{d1}/{d2}', 'DashboardController@dashboardQC');
  Route::get('/dashboard/QCBorneoMitraDetail/{mitra}/{d1}/{d2}/{tag}', 'DashboardController@dashboardQCdetail');
  // Route::get('/qcview/{sc}','DashboardController@QCBorneoView');
  // Route::get('/qcview2/{sc}','DashboardController@QCBorneoView_byMYIR');

  // UT Online
  Route::get('/dashboard/UTOnline/{date}/{datex}', 'DashboardController@dashboardUTOnline');
  Route::get('/dashboard/UTOnlineDetail/{type}/{mitra}/{date}/{datex}/{status}', 'DashboardController@dashboardUTOnlineDetail');

  Route::get('/utonline/ba-controller/request-reset-ba', 'PsbController@ba_controller_inbox');
  Route::get('/utonline/request-reset-ba/{id}', 'PsbController@ba_controller');
  Route::post('/utonline/request-reset-ba/save/{id}', 'PsbController@ba_controller_save');

  Route::get('/dashboard/newDashboardKendalaProvisioning', 'DashboardController@newDashboardKendala');
  Route::get('/dashboard/newDashboardKendalaProvisioningDetail', 'DashboardController@newDashboardKendalaDetail');


  Route::get('/map-order-provisioning','MapController@map_order_provisioning');

  //dashboard monitoring
  Route::get('/dashboard/monitoring/{tgl}','MapController@monitoring');

  // map
  Route::get('/trialnewmap','MapController@trial');
  Route::get('/whereismytech','PsbController@whereismytech');
  Route::get('/dashboard/map/{tgl}','MapController@mapDashboard');
  Route::post('/dashboard/map/{tgl}','MapController@mapDashboard');
  Route::get('/dashboard/solusikendala/{tgl}','MapController@MapSolusiKendala');
  Route::get('/get_nearest_order/{id}','PsbController@get_nearest_order');
  Route::get('/vhaiklah/{datel}','PsbController@vhaiklah');
  Route::get('/mapping_order/{sto}','PsbController@vhaiklah');
  Route::get('/mapping_order_datel/{datel}','PsbController@mapping_order');
  Route::get('/reformat_myir_wo','DispatchController@reformat_myir_wo');
  Route::get('/dashboard/mapping/{date}','DashboardController@mapping');
  // map Cabutan
  Route::get('/mapping_order_cabutan','PsbController@mapping_order_cabutan');
  Route::get('/autodispatch_dismantling','DismantlingController@autodispatch_dismantling');


  //Dashboard Prov New
  Route::get('/dashboardProv/{date}','DashboardController@DashboardProv');
  Route::get('/dashboardKehadiran/{posisi_id}/{date}','EmployeeController@dashboardKehadiran');
  Route::get('/dashboardKehadiranList','EmployeeController@kehadiranTeknisiList');
  Route::get('/ListTeknisi/{sektor}/{posisi_id}','EmployeeController@ListTeknisi');
  Route::get('/schedule/{nik}/{regu_id}/{sto_regu}','EmployeeController@inputSchedule');
  Route::post('/schedule/{nik}/{regu_id}/{sto_regu}','EmployeeController@inputScheduleSave');
  Route::get('/dashboardPSRE_1/{periode}','DashboardController@dashboardPSRE_1');

  //cookies session
  Route::get('/cookiesystem', 'LoginController@cookiesystem');
  Route::post('/cookiesystemSave', 'LoginController@cookiesystemSave');
  Route::post('/loginwhatsapp/save', 'LoginController@whatsapp_save');

  // manual starclick
  Route::get('/starclick/manual','PsbController@manualStarclick');
  Route::post('/starclick/manual','PsbController@manualStarclickSave');

  // Datel
  Route::get('/datel','DatelController@index');
  Route::get('/datel/input','DatelController@input');

  // ticketing
  Route::get('/listTicketPersonal','TicketingController@listPersonal');
  // Route::get('/ticketing','TicketingController@index');
  // Route::Post('/ticketing','TicketingController@index');
  // Route::get('/ticketInfo/{id}','TicketingController@ticketInfo');
  // Route::get('/createTicket/{id}','TicketingController@create');
  // Route::Post('/createTicket/{id}','TicketingController@createSave');
  Route::get('/ticketing/dashboard','TicketingController@dashboard')->name('ticket.dashboard');
  Route::get('/ticketing/dashboardClose/{date}','TicketingController@dashboardClose');
  Route::get('/ticketing/dashboardCloseList/{status}/{sektor}/{date}','TicketingController@dashboardCloseList');
  Route::get('/ticketing/dashboardList/{type}/{sektor}/{id}','TicketingController@dashboardList');
  Route::get('/ticketing/dashboardListUndispatchList/{id}','TicketingController@dashboardListUndispacth');

  // Rider
  Route::get('/dashboardRider','RiderController@dashboard');
  Route::get('/dashboardRider/input/{nik}','RiderController@input');
  Route::post('/dashboardRider/input/{nik}','RiderController@save');
  Route::get('/dashboardRider/search','RiderController@formSearch');

  // send kpro
  Route::get('/sendkprobySC','PsbController@sendkprobySC');
  Route::post('/sendkprobySC','PsbController@sendkprobySC_save');

  // update squad
  Route::get('/updatesquad/{nik}','HomeController@updatesquad');
  Route::post('/updatesquad/{nik}','HomeController@updatesquad_save');

  // Kehadiran
  Route::get('/kehadiran/{date}','DashboardController@kehadiran');

  Route::get('/dashboard/assurance2','AssuranceController@dashboard2');

  // review dashboard
  Route::get('/dashboardReview/{tgl}','ReviewController@dashboard');
  Route::get('/dashboardReviewList/{tgl}/{sektor}','ReviewController@dashboardList');
  Route::get('/dashboardReviewListTek/{tgl}/{id_regu}','ReviewController@dashboardListTek');
  Route::get('/dashboardReviewListTekUnderrate/{tgl}','ReviewController@dashboardListTekUnderrate');

  // manja
  Route::get('/manja/{date}','AssuranceController@manja');

  // Alpro
  Route::get('/alpro','AlproController@alpro');
  Route::get('/alpro/tambahkan','AlproController@add');
  Route::post('/alpro/tambahkan','AlproController@post');
  Route::post('/alpro/book','AlproController@book');

  // briefing
  Route::get('/briefing/list/{tgl}','briefingController@list');
  Route::get('/briefing','briefingController@input');
  Route::post('/briefing','briefingController@simpanLaporan');
  Route::post('/uploadFoto/{id}/upload','briefingController@simpanFoto');
  Route::post('/uploadFoto/{id}/removed','briefingController@hapusFoto');

  // GAUL
  Route::get('/gaulgenerator/{periode}','AssuranceController@gaulgenerator');
  Route::get('/gaulv2/{periode}','AssuranceController@dashboard_gaul_by_seq');
  Route::get('/gaulv2list/{action}/{periode}','AssuranceController@dashboard_gaul_by_seq_list');
  Route::get('/gaulsektor/{periode}','AssuranceController@gaulsektor');
  Route::get('/gaulsektorlist/{sektor}','AssuranceController@gaulsektorlist');
  Route::get('/trendgaul','AssuranceController@trendgaul');
  Route::get('/trendgauldetil/{sektor}/{date}','AssuranceController@trendgaullist');

  // TTR 3 JAM NOT COMPLY
  Route::get('/ttr3notcomply','AssuranceController@ttr3notcomply');
  Route::get('/ttr3notcomplylist/{sektor}/{date}','AssuranceController@ttr3notcomplylist');

  // BARCODE
  Route::get('/searchbarcode','BarcodeController@search');
  Route::post('/searchbarcode','BarcodeController@searchpost');

  // History Reject
  Route::get('/viewreject','DispatchController@viewrejectQC');

  // dashboard tarikan panjang
  Route::get('/panjangtarikan/{periode}','DashboardController@panjangtarikan');

  // ukur ibooster
  // Route::get('/ibooster','AssuranceController@ibooster');
  // Route::post('/ibooster','AssuranceController@ukurIbooster');
  Route::get('/ReportIbooster/{periode}','AssuranceController@reportibooster');

  // return dropcore
  Route::get('/returnDropcore/{periode}','ReturnDropcoreController@home');
  Route::get('/returnDropcoreInput/{in}','ReturnDropcoreController@input');
  Route::get('/testMeta','ReturnDropcoreController@loadMetdata');
  Route::post('/returnDropcoreInput/{in}','ReturnDropcoreController@save');
  Route::get('/dashboardReturnDropcore/{periode}','ReturnDropcoreController@dashboard');
  Route::get('/dashboardReturnDropcoreList/{type}/{id}/{periode}','ReturnDropcoreController@dashboard');

  // schedule
  Route::get('/scheduleTech/{periode}','ScheduleController@home');

  // alker sarker
  Route::get('/alkersarker/QC','AlkersarkerController@QC');
  Route::post('/alkersarker/save','AlkersarkerController@save');
  Route::get('/alkersarker/input','AlkersarkerController@input');

  // QC ondesk
  Route::get('/QCOndesk/{periode}/{status}','QcondeskController@home');
  Route::get('/QCOndeskReject/{id}','QcondeskController@reject');
  Route::post('/QCOndeskReject/{id}','QcondeskController@rejectSave');

  // Monitoring Material
  Route::get('/material/{periode}','MaterialController@home');
  Route::get('/dashboardMaterial','MaterialController@dashboard');
  Route::get('/dashboardMaterialDetail','MaterialController@dashboardDetail');

  // ABSENSI
  Route::get('/rekap_mtd_teknisi','AbsensiController@rekap_mtd_teknisi');

  Route::get('/amija/{mitra}', 'AmijaController@list');
  Route::get('/PBS/{id}', 'DashboardController@pbs');
  Route::get('/kpi/assurance', 'DashboardController@kpi_assurance');
  Route::get('/kpi/assuranceMitra', 'DashboardController@kpi_assurance_mitra');
  Route::get('/kpi/assuranceList/{tipe}/{sektor}/{periode}', 'DashboardController@kpi_assuranceList');
  Route::get('/kpi/assuranceMitraTech', 'DashboardController@kpi_assurance_mitra_tech');
  Route::get('/kpi/assuranceListGAUL/{sektor}/{periode}', 'DashboardController@kpi_assuranceListGaul');
  Route::get('/home2', 'HomeController@home2');
  Route::get('/uploadFile', 'HomeController@uploadFile');
  Route::post('/uploadFile', 'HomeController@uploadFileSave');
  // Route::get('/leaderboards/{id}/{tgl}/{tglend}/{mitra}','leaderboardsController@leaderboards');
  Route::get('/umurTiket','DashboardController@umurTiket');
  Route::get('/matrikMt/{jns}/{sts}', 'LaporanController@matrikMt');
  Route::post('/matrikMt/{jns}/{sts}', 'LaporanController@nextLoker');
  Route::get('/mtCheck/{id}', 'LaporanController@checkedOrder');
  // Route::get('/cekredaman/{id}', 'PsbController@redaman');
  Route::get('/getOdp/{id}', 'PsbController@get_odp');
  Route::get('/getOdpByPoint/{lat}/{lng}', 'PsbController@get_odp_point');
  Route::get('/scDetail/{id}', 'GrabController@scDetail');
  Route::get('/hdfallout', 'PsbController@HDFallout');
  Route::post('/hdfallout', 'PsbController@orderHDFallout');
  Route::post('/hdfallout/simpan-pda','PsbController@simpanPda');

  //dashboard rekon
  // Route::get('/dashboard/rekon/{date}/{dateend}', 'DashboardController@rekon');
  // Route::get('/dashboard/provisioningListMitra/{jenis}/{status}/{tglAwal}/{tglAkhir}','DashboardController@provisioningListMitra');
  // Route::get('/dashboard/rekonList/{id}/{tgl}', 'DashboardController@rekonList');
  // Route::get('/dashboard/listTimMitra/{id}/{tglAwal}/{tglAkhir}', 'DashboardController@listTimMitra');
  // ROute::get('/dashboard/ListMitra-jmldc/{mitra}/{tgl}','DashboardController@listJumlahDc');

  // dashboard khusus admin prov
  Route::get('/dashboard/provisioningListMitraX/{jenis}/{tglAwal}/{tglAkhir}','DashboardController@provisioningListMitraX');
  Route::get('/dashboard/new_rekon/{tglAwal}/{tglAkhir}','DashboardController@new_rekon_prov');

  // dashboard warehouse
  Route::get('/dashboard/warehouse','DashboardController@warehouse');
  Route::get('/dashboard/warehouse/recap','DashboardController@warehouseRecap');
  Route::get('/dashboard/warehouse/data','DashboardController@warehouseData');

  //news
  Route::get('/news','NTnewsController@index');
  Route::post('/news','NTnewsController@submit');
  Route::get('/seenews','NTnewsController@see');
  Route::get('/update/{id_berita}','NTnewsController@update');
  Route::post('/update/{id_berita}','NTnewsController@edit');
  Route::get('/view/{id_berita}','NTnewsController@view_you');
  Route::get('/delete/{id_berita}','NTnewsController@hapus');
  //sms
  Route::get('/new/sms','SmsgammaController@newsms');
  Route::get('/sms/inbox','SmsgammaController@inbox');
  Route::get('/sms/outbox','SmsgammaController@outbox');
  Route::get('/sent/items','SmsgammaController@sent');
  Route::get('/group/sms','SmsgammaController@group');
  Route::post('/saveC', 'SmsgammaController@savecontact');
  Route::post('/send3006', 'SmsgammaController@send3006');
  Route::post('/saveG', 'SmsgammaController@savegroup');
  Route::post('/saveCG', 'SmsgammaController@savegroupc')->name('smsgamma.savegc');
  Route::get('/livesearch', 'SmsgammaController@livesearch')->name('live_Search');
  Route::get('/sms/detail_I/{id}', 'SmsgammaController@detail_I');
  Route::get('/sms/detail_O/{id}', 'SmsgammaController@detail_O');
  Route::get('/sms/detail_S/{id}', 'SmsgammaController@detail_S');
  Route::get('/sms/delete/{id}', 'SmsgammaController@delete_sms');
  Route::get('/sms/edit/contact/{id}', 'SmsgammaController@edit_sms');
  Route::post('/sms/edit/contact/{id}', 'SmsgammaController@update');
  Route::get('/sms/prov/{tgl}', 'SmsgammaController@formSmsProv');
  Route::post('sms/prov/{tgl}', 'SmsgammaController@kirimSmsProv');

  //custom grab
  Route::get('/telco/{id}', 'DashboardController@dashboardTelco');
  Route::get('/telcoDetil/{id}/{div}/{jadwal}/{hadir}', 'DashboardController@detilTelco');
  Route::get('/customGrab', 'GrabController@index');
  Route::post('/customGrab', 'GrabController@custom_sync_starclick_dbmirror');
  // multi custom grab
  // Route::get('/multi/customGrab', 'GrabController@multi');
  // Route::POST('/multi/customGrab', 'GrabController@multi_sync_starclick_dbmirror');

  //custom grab old
  // Route::get('/customGrabOld', 'GrabController@index');
  // Route::POST('/customGrabOld', 'GrabController@sync');
  Route::get('/syncSC/{sc}', 'GrabController@sync_X');
	Route::get('/ba/upload/{id}','BaController@upload_form');
	Route::post('/ba/upload/{id}','BaController@save');
	Route::get('/ba/book/{id}','BaController@book');
	Route::get('/ba/sendtoftp/{id}','BaController@sendtoftp');
	Route::get('/ba','BaController@index');
	Route::get('/ba/checkftp','BaController@checkftp');

	Route::get('/dashboard/listpotensi/{tgl}/{area}/{id}', 'DashboardController@listpotensi');

	Route::get('/dashboard/provisioning/{tgl}/{jenis}', 'DashboardController@provisioningDua');
  // Route::get('/dashboard/provisioning/{jenis}', 'DashboardController@provisioning');
	Route::get('/dashboard/provisioning3/{tgl}/{jenis}', 'DashboardController@provisioningDua');
	Route::get('/dashboard/provisioningList/{tgl}/{jenis}/{status}/{so}/{order}', 'DashboardController@provisioningList');
  Route::get('/dashboard/listms2n/{tgl}/{kandatel}/{jenis}/{status}','DashboardController@listms2n');
  Route::get('/dashboard/provisioningDua/{tgl}/{jenis}', 'DashboardController@provisioningDua');

  Route::get('/dashboard/migrasiListMitraMs2n/{tgl}/{jenis}','DashboardController@migrasiListMitraMs2n');
  Route::get('/dashboard/migrasiListMitra/{tgl}/{jenis}','DashboardController@migrasiListMitra');
  // Route::get('/dashboard/assuranceListMitra/{jenis}/{tglNow}','DashboardController@assuranceListMitra');
  // Route::get('/dashboard/assuranceListMitrawithexcel/{jenis}/{tglNow}','DashboardController@assuranceListMitrawithexcel');
	Route::get('/dashboard/migrasi/{tgl}/{id}', 'DashboardController@migrasi');
	Route::get('/dashboard/migrasiList/{tgl}/{id}/{status}/{so}/{ketOrder}', 'DashboardController@migrasiList');

	Route::get('/dashboard/assurance', 'DashboardController@assurance');
  Route::get('/dashboard/assurance/list/{tgl}/{jenis}', 'DashboardController@assuranceList');
  Route::get('/dashboard/assurance/averageCloseDetail/{date}/{title}/{order}/{status}', 'DashboardController@averageCloseDetail');
  Route::get('/dashboard/assurance/gaul/{sektor}/{date}', 'DashboardController@gaul_detil');
  Route::get('/dashboard/assurance/gaul/detil/{count}/{inet}', 'DashboardController@gaul_detil_count');
  Route::get('/dashboard/assurance/saldoUnspec/{sektor}/{order}/{date}', 'DashboardController@saldoUnspec');
  Route::get('/dashboard/assurance/sisaSaldoTiket/{ioan}/{durasi}', 'DashboardController@sisaTiketNossaDetail');
  Route::get('/dashboard/assurance/ttrManualManja/{sektor}/{durasi}', 'DashboardController@ttrManualManja');
  Route::get('/dashboard/assurance/monitoring-nossa/{date}','DashboardController@mon_nossa');
  Route::get('/dashboard/assurance/monitoring-nossa/total/{tgl}', 'AssuranceController@mon_nossa');
  Route::get('/assurance/SyncIbooster', 'AssuranceController@SyncIbooster');
	Route::get('/list2/{tgl}/{jenis}/{id}', 'DashboardController@assuranceList2');
  Route::get('/listbyumur/{type}/{sektor}/{lokasi}/{channel}','DashboardController@listbyumur');
  Route::get('/listbyaction/{tgl}/{jenis}/{source}/{sektor}', 'DashboardController@assuranceListbyAction');
	Route::get('/assurance/dashboard', 'AssuranceController@dashboard');
	Route::get('/lapul/{id}', 'AssuranceController@lapul');
	Route::post('/lapul/{id}', 'AssuranceController@lapulSave');
	Route::get('/assurance/search', 'AssuranceController@search');
	Route::post('/assurance/search', 'AssuranceController@searchpost');
  Route::get('/assurance/edit/{ID}','AssuranceController@EditTicket');
  Route::post('/assurance/edit/{ID}','AssuranceController@saveEditTicket');
	Route::get('/assurance/dispatch_manual', 'AssuranceController@dispatch_manual');
	Route::post('/assurance/dispatch_manual', 'AssuranceController@dispatch_manual_save');
  Route::get('/assurance/list-active-rock/{sektor}/{status}','DashboardController@assranceListRoc');
  Route::get('/assurance/list-active-int/{sektor}/{status}/{date}','DashboardController@assranceListINT');
  Route::get('/stoCustomer/{date}/search', 'AssuranceController@stoCustomer');


  //profile
	Route::get('/profile', 'ProfileController@index');
	Route::post('/profileSave', 'ProfileController@profileSave');

	Route::get('/employee','EmployeeController@index');
	Route::get('/employee/{id}','EmployeeController@input');
	Route::post('/employee/{id}','EmployeeController@store');

	Route::post('/update-location','EmployeeController@update_location');

	Route::get('/team','TeamController@index');
	Route::get('/team/input','TeamController@input');
	Route::post('/team/input','TeamController@store');
	Route::get('/team/{id}','TeamController@edit');
	Route::post('/team/{id}','TeamController@update');
  Route::get('/team/disable/{id}','TeamController@disable');
  Route::get('/team/delete/{id}','TeamController@delete');

  // grand prize
  Route::get('/grandprize/gathering_q1/{hadiah}','GrandPrizeController@gathering_q1');
  Route::get('/grandprize/save/gathering_q1/{nik}','GrandPrizeController@gathering_q1_save');

  Route::get('/undian/{jenis}/{hadiah}','GrandPrizeController@undian');
  Route::get('/ajx_dg/undian/{type}/{id}','GrandPrizeController@get_data_undian');
  Route::get('/undian/save/{jenis}/{id}', 'GrandPrizeController@updateGrandPrize');
  // Route::get('/pointek','GrandPrizeController@pointek');
  // Route::get('/GrandPrize','GrandPrizeController@home');
  // Route::get('/dashboardRacingPS', 'DashboardController@racingPS');

	Route::get('/produktifitas/{tgl}/{id}', 'DispatchController@produktifitas');
  Route::get('/produktifitastek', 'DispatchController@produktifitastek');
  Route::get('/produktifitasteklist/{dateAwal}/{date}/{id_regu}/{status}', 'DispatchController@produktifitasteklist');
  Route::get('/dashboardSales/{tgl}/{spv}', 'DashboardController@dashboardSales');
  Route::get('/dashboardSalesbySPV/{tgl}', 'DashboardController@dashboardSalesBySPV');
  Route::get('/dashboardSalesList/{status}/{tgl}/{spv}/{sales}', 'DashboardController@dashboardSalesList');
  Route::get('/produktifitassektor/{tgl}/{lokasi}/{mitra}', 'DispatchController@produktifitassektor2');
  Route::get('/produktifitasteksewa/{tgl}/{lokasi}/{mitra}', 'DispatchController@produktifitasteksewa');
  Route::get('/produktifitastekA/{tgl}/{lokasi}/{mitra}', 'DispatchController@produktifitastekA');
  Route::get('/produktifitassektor/{tgl}/{mode}', 'DispatchController@produktifitassektor');
  Route::get('/produktifitassektorlist/{tgl}/{sektor}/{mode}', 'DispatchController@produktifitassektorlist');

  Route::get('/dashboard/unspec','DashboardController@unspec');
  Route::get('/dashboard/unspeclist/{sto}','DashboardController@unspeclist');

  Route::get('/dashboard/new/unspec/{date}','DashboardController@unspec_all');
  Route::get('/dashboard/new/unspeclist/prov/{sektor}/{date}','DashboardController@unspeclist_prov');
  Route::get('/dashboard/new/unspeclist/prov/null/{sektor}/{date}','DashboardController@unspeclist_prov_null');
  Route::get('/dashboard/new/unspeclist/asr/{sektor}/{date}','DashboardController@unspeclist_asr');
  Route::get('/dashboard/new/unspeclist/asr/null/{sektor}/{date}','DashboardController@unspeclist_asr_null');

	Route::get('/migrasi/search', 'MigrasiController@searchform');
	Route::post('/migrasi/search', 'MigrasiController@search');
	Route::post('/migrasi/dispatch/{id}', 'MigrasiController@dispatch');

  // Route::get('/dispatch/', 'DispatchController@dashboard');
  Route::get('/dispatch/checksc','DispatchController@checksc');
  Route::post('/dispatch/checksc','DispatchController@resultchecksc');
	Route::get('/dispatch/matrix/{date}', 'DispatchController@matrix');
  Route::get('/dispatch/matrixGo/{date}', 'DispatchController@matrixGo');
  Route::get('/dispatch/matrixQC/{date}', 'DispatchController@matrixQC');
  Route::get('/dispatch/matrixKhusus/{date}', 'DispatchController@matrixKhusus');
	// Route::get('/dispatch/matrix_new', 'DispatchController@matrix3');
	Route::get('/dispatch/matrixText', 'DispatchController@matrixText');
	Route::get('/validasiodp/dispatch', 'DispatchController@validasiodp');

	Route::post('/dispatch/matrix', 'DispatchController@matrix');
  Route::get('/dispatch/matrixHD/{tgl}', 'DispatchController@matrixHD');
	Route::get('/dispatch/workorder/{id}', 'DispatchController@WorkOrder');
	Route::get('/dispatch/kpro/{id}/{idregu}', 'DispatchController@kpro');
	Route::get('/dispatch/starclick', 'DispatchController@starclick');
	Route::get('/dispatch/search/', 'DispatchController@searchform');
  Route::post('/dispatch/search/', 'DispatchController@search');
  Route::get('/workorder/{id}', 'PsbController@WorkOrder');
  Route::get('/workorderQC', 'PsbController@workorderQC');

  Route::get('/orderCabutanNte', 'PsbController@workorderCabutanNte');
  Route::get('/orderOntPremium', 'PsbController@workorderOntPremium');

	Route::get('/dispatch/followup/{id}', 'DispatchController@FollowUp');
	Route::get('/dshr/cluster/{id}', 'DshrController@cluster');
  Route::get('/dispatch/matrix-reboundary/{tgl}','DispatchController@matrixReboundary');

	Route::get('/dispatch/querycheck/{id}', 'DispatchController@QueryCheck');

	Route::get('/dshr/door-to-door/dashboard', 'DshrController@doorToDoorDashboard');
	Route::get('/dshr/door-to-door/my-order', 'DshrController@doorToDoorMyOrder');
	Route::get('/dshr/door-to-door/assign', 'DshrController@doorToDoorAssign');
	Route::post('/dshr/door-to-door/assign', 'DshrController@doorToDoorAssignSave');
	Route::get('/dshr/door-to-door/assign/update/{id}', 'DshrController@updateDoorToDoorAssign');
	Route::post('/dshr/door-to-door/assign/update/{id}', 'DshrController@updateDoorToDoorAssignSave');

	Route::get('/dshr/transaksi/today', 'DshrController@today');
	Route::get('/dshr/transaksi/all', 'DshrController@all');
	Route::get('/dshr/transaksi/tm', 'DshrController@tm');
	Route::get('/dshr/transaksi/md', 'DshrController@dm');
	Route::get('/dshr/transaksi/uim', 'DshrController@uim');
	Route::get('/dshr/transaksi/rfsoke', 'DshrController@rfsoke');
	Route::get('/dshr/transaksi/odpbelummuncul', 'DshrController@odpbelummuncul');
	Route::get('/dshr/transaksi/followupopen', 'DshrController@followupopen');
	Route::get('/dshr/transaksi/followupsales', 'DshrController@followupsales');
	Route::get('/dshr/transaksi/followupok', 'DshrController@followupok');
	Route::get('/dshr/transaksi/neworder', 'DshrController@neworder');
	Route::get('/dshr/transaksi/unscdeployer', 'DshrController@unscdeployer');
	Route::get('/dshr/transaksi/unscnr2g', 'DshrController@unscnr2g');
	Route::get('/dshr/transaksi/kendalawebcare', 'DshrController@kendalawebcare');
	Route::get('/dshr/transaksi/belumwebcare', 'DshrController@belumwebcare');
	Route::get('/dshr/transaksi/sudahwebcare', 'DshrController@sudahwebcare');
	Route::get('/dshr/transaksi/berhasilinput', 'DshrController@berhasilinput');
	Route::get('/dshr/transaksi/mappingok', 'DshrController@mappingok');

	Route::get('/dshr/transaksi/odpsudahmuncul', 'DshrController@odpsudahmuncul');
	Route::get('/dshr-transaksi-status/', 'DshrController@status');
	Route::get('dshr-transaksi-sub-status', ['as' => 'search', 'uses' => 'DshrController@substatus']);
	Route::get('/bon/Installed/{date}', 'BonController@BonInstalled');
	Route::get('/dispatch/timeslot/{id}/{area}', 'DispatchController@timeslot');
	Route::get('/dispatch/psblaporan', 'DispatchController@psb_laporan');
	Route::get('/laporan/dshr-report1', 'LaporanController@dshr_report1');
	Route::get('/laporan/dshr-report2', 'LaporanController@dshr_report2');
	Route::get('/laporan/dshr-report3', 'LaporanController@dshr_report3');
	Route::get('/laporan/dshr-report4', 'LaporanController@dshr_report4');
	Route::get('/laporan/dshr-report5', 'LaporanController@dshr_report5');
	Route::get('/laporan/dshr-report6', 'LaporanController@dshr_report6');
	Route::get('/laporan/dshr-report7', 'LaporanController@dshr_report7');
	Route::get('/laporan/dshr-report8', 'LaporanController@dshr_report8');
	Route::get('/laporan/dshr-report9', 'LaporanController@dshr_report9');
	Route::get('/laporan/dshr-report10', 'LaporanController@dshr_report10');
	Route::get('/laporan/dshr-report11', 'LaporanController@dshr_report11');
	Route::get('/laporan/dshr-report12', 'LaporanController@dshr_report12');

  // dshr splitter
  Route::get('/dshr/splitter/list','DshrController@listSplitter');
  Route::get('/dshr/splitter/input','DshrController@inputSplitter');
  Route::post('/dshr/splitter/input','DshrController@simpanSplitter');
  Route::get('/dshr/splitter/{noTIket}','DshrController@editSplitter');
  Route::post('/dshr/splitter/{noTIket}','DshrController@editSimpanSplitter');

  Route::get('/', 'PsbController@index');
  //Route::get('/panjar', 'PanjarController@index');
  //Route::get('/bon', 'BonController@index');

  // whatsapp api && broadcast TommanGO
  Route::get('/whatsapp-api', 'ApiController@bulkWhatsapp');
  Route::post('/whatsapp-api/send-message', 'ApiController@bulkWhatsappSaveMessage');
  Route::post('/whatsapp-api/send-media', 'ApiController@bulkWhatsappSaveMedia');
  Route::get('/whatsapp-api/detail', 'ApiController@detailMessageWhatsapp');
  Route::post('/broadcast/message/tommango', 'ApiController@broadcastBot');


  // Document Management
  Route::get('/documentUpload','DocumentController@upload');
  Route::post('/documentUpload','DocumentController@uploadSave');
  Route::get('/documentDashboard/{dateStart}/{dateEnd}','DocumentController@dashboard');
  Route::get('/documentYours','DocumentController@yours');
  Route::get('/documentDetails/{documentID}','DocumentController@details');
  Route::get('/documentDashboardExternalList/{dateStart}/{dateEnd}/{mitra}/{type}','DocumentController@dashboardExternalList');

  // JSON
  Route::get('/json/DocumentInternal','DocumentController@jsonDocumentInternal');

  // feedback new
  Route::get('/feedback/input', 'FeedbackController@create');
  Route::get('/feedback/{id}', 'FeedbackController@input');
  ROute::PUT('/feedback/input/{id}', 'FeedbackController@save');
  ROute::PUT('/feedback/{id}', 'FeedbackController@save');

  // dashboard dismantle & ont premium
  Route::get('/DashboardDismantleOntPremium','DashboardController@dashboardDismanlteOntPremium');
  Route::get('/DetailDismantleOntPremium/{kat}/{group}/{sektor}/{type}/{status}/{date}','DashboardController@detailDismanlteOntPremium');
  Route::get('/dashboardDismantling','DismantlingController@dashboard');
  Route::get('/newDashboardDismantling', 'DashboardController@newDashboardDismantling');
  Route::get('/newDashboardDismantlingDetail', 'DashboardController@newDashboardDismantlingDetail');

  // undispatch cabutan nte & ont premium
  Route::get('/multi/undispatchOrder', 'DispatchController@undispatchOrder');
  Route::post('/multi/undispatchOrder/undispatch_cabut_nte', 'DispatchController@undispatch_cabut_nte');
  Route::post('/multi/undispatchOrder/undispatch_ont_premium', 'DispatchController@undispatch_ont_premium');
  Route::get('/dismantle/tarikOrderDismanlte', 'DispatchController@tarikOrderDismanlte');

  // cabutan nte new
  Route::get('/multi/cabutNte', 'DispatchController@multiCabutNte');
  Route::post('/multi/cabutNteSave', 'DispatchController@multiCabutNteSave');
  Route::post('/multi/cabutNtePindah', 'DispatchController@multiCabutNtePindah');

  // ont premium
  Route::get('/multi/ontPremium', 'DispatchController@multiOntPremium');
  Route::post('/multi/ontPremium', 'DispatchController@multiOntPremiumSave');
  Route::get('/ontPremium/tarikOrderontPremium', 'DispatchController@tarikOrderontPremium');

  // migrasi stb premium
  Route::get('/multi/migrasiStbPremium', 'DispatchController@multiMigrasiStbPremium');
  Route::post('/multi/migrasiStbPremiumSave', 'DispatchController@multiMigrasiStbPremiumSave');
  Route::get('/getOrdersDll/{type}', 'DispatchController@getOrdersDll');

  // order migrasi stb premium
  Route::get('/orderMigrasiSTB', 'DispatchController@orderMigrasiSTB');

  Route::get('/home', 'HomeController@home');
  Route::post('/home','HomeController@absen');
  // Route::get('/home/absen/upload','HomeController@absensi_upload');
  // Route::post('/home/absen/upload','HomeController@absensi_upload_save');
  Route::get('/home/absen/approve/{id}/{tgl}','HomeController@absen_approval');
  Route::get('/home/absen/decline/{id}','HomeController@absen_decline');
  Route::post('/home/absen/decline/{id}','HomeController@absen_decline_save');
  Route::get('/home/absen/besok','HomeController@absenBesok');

  // laporan run step provi
  Route::get('/prov/dailySteps','PsbController@dailySteps');
  Route::post('/prov/dailyStepsSave','PsbController@dailyStepsSave');

  // KPRO
  Route::get('/dashboardKpro','KproController@dashboard');

  Route::get('/nousage', 'DispatchController@nousage');

  Route::get('/feedback', [
    'as' => 'feedback', 'uses' => 'FeedbackController@index'
  ]);

  Route::get('/assurance', [
    'as' => 'assurance', 'uses' => 'AssuranceController@index'
  ]);
  Route::get('/stok-nte', [
    'as' => 'stok-nte', 'uses' => 'NteController@stok'
  ]);
  Route::get('/spt', [
    'as' => 'spt', 'uses' => 'UserController@spt'
  ]);
  Route::get('/dshr-transaksi', [
    'as' => 'dt', 'uses' => 'DshrController@index'
  ]);
  Route::get('/dshr-cluster', [
    'as' => 'dc', 'uses' => 'DshrController@indexc'
  ]);

  Route::get('/dshr-report', [
    'as' => 'dshr-report', 'uses' => 'LaporanController@dshr_report'
  ]);
  Route::get('/maintaince', [
    'as' => 'maintaince', 'uses' => 'MaintainceController@index'
  ]);
  Route::get('/migrasi/{date}', [
    'as' => 'migrasi', 'uses' => 'MigrasiController@index'
  ]);
  Route::get('/ccan', [
    'as' => 'ccan', 'uses' => 'CcanController@listCcan'
  ]);
  Route::get('/nte', [
    'as' => 'gudang-nte', 'uses' => 'NteController@index'
  ]);
  Route::get('/ms2n', [
    'as' => 'ms2n', 'uses' => 'Ms2nController@index'
  ]);
  Route::get('/item', [
    'as' => 'item', 'uses' => 'ItemController@index'
  ]);
  Route::get('/material-masuk', [
    'as' => 'material', 'uses' => 'MaterialMasukController@index'
  ]);
  Route::get('/material-kembali', [
    'as' => 'material-kembali', 'uses' => 'MaterialKembaliController@index'
  ]);
  Route::get('/material-keluar', [
    'as' => 'material-keluar', 'uses' => 'MaterialKeluarController@index'
  ]);
  // Route::get('/', [
  //   'as' => 'psb', 'uses' => 'PsbController@index'
  // ]);
  Route::get('/panjar', [
    'as' => 'panjar', 'uses' => 'PanjarController@index'
  ]);
  Route::get('/bon', [
    'as' => 'bon', 'uses' => 'BonController@index'
  ]);
  Route::get('/laporan', [
    'as' => 'laporan', 'uses' => 'LaporanController@gudang'
  ]);
  Route::get('/dispatchPI', [
    'as' => 'dispatchPI', 'uses' => 'DispatchController@dispatchPI'
  ]);

  Route::get('/searchWO', [
    'as' => 'searchWO', 'uses' => 'DispatchController@searchWO'
  ]);

  Route::get('/falloutWFM', [
    'as' => 'falloutWFM', 'uses' => 'DispatchController@falloutWFM'
  ]);
  Route::get('/falloutActivation', [
    'as' => 'falloutActivation', 'uses' => 'DispatchController@falloutActivation'
  ]);

  Route::get('/woSTB', [
    'as' => 'woSTB', 'uses' => 'DispatchController@woSTB'
  ]);

  Route::get('/woCFC', [
    'as' => 'woCFC', 'uses' => 'DispatchController@woCFC'
  ]);


  Route::get('/ms2n/PS/', [
    'as' => 'PS', 'uses' => 'Ms2nController@kategori'
  ]);
  Route::get('/laporan-material', [
    'as' => 'lm', 'uses' => 'LaporanController@material'
  ]);
  Route::get('/laporan-gallery', [
    'as' => 'lm', 'uses' => 'LaporanController@gallery'
  ]);

  Route::get('/laporan-material-migrasi', [
    'as' => 'lmm', 'uses' => 'LaporanController@materialmigrasi'
  ]);
  Route::get('/tiket/{id}', 'AssuranceController@input');
  ROute::post('/tiket/{id}', 'AssuranceController@save');

  Route::get('/dshr/search/{search}', 'DshrController@all');
  Route::get('/dshr-transaksi/getSales/{id}', 'DshrController@getSales');
  Route::get('/dshr-transaksi/getStatus/{id}', 'DshrController@getStatus');
  Route::get('/dshr-transaksi/{id}', 'DshrController@input');
  Route::get('/dshr-transaksi-telegram/{id}', 'DshrController@send_to_telegram');
  Route::get('/dshr/plasa-sales/order/inbox', 'DshrController@inboxPlasa');
  Route::get('/dshr/plasa-sales/order/inbox/{id}','DshrController@statusDeposit');
  Route::post('/dshr/plasa-sales/order/inbox/{id}','DshrController@saveStatusDeposit');

 Route::get('/sendsms/{id}', 'DispatchController@sendsms');

  Route::get('/dshr-upload', 'DshrController@upload');
  Route::PUT('/dshr-transaksi/{id}', 'DshrController@save');
  Route::PUT('/dshr-upload', 'DshrController@uploadSave');
  Route::DELETE('/dshr-transaksi/{id}', 'DshrController@destroy');

  Route::get('/person', 'DshrController@listPerson');

  Route::get('/status-wo/{id}', 'DispatchController@updateStatus');
  Route::PUT('/status-wo/{id}', 'DispatchController@saveStatus');

  Route::PUT('/migrasi/upload-ccan', 'CcanController@saveCcan');
  Route::get('/migrasi/upload-ccan', 'CcanController@uploadCcan');
  Route::get('/migrasi/report-ccan', 'CcanController@reportCcan');

  Route::PUT('/migrasi/upload', 'MigrasiController@save');
  Route::get('/migrasi/upload', 'MigrasiController@upload');
  Route::get('/migrasi/report', 'MigrasiController@report');
  Route::get('/migrasi/status/{id}', 'MigrasiController@updateStatus');
  Route::PUT('/migrasi/status/{id}', 'MigrasiController@saveStatus');
  Route::get('/migrasi/dispatch/{id}', 'MigrasiController@dispatch');
  Route::PUT('/migrasi/dispatch/{id}', 'MigrasiController@dispatchSave');

  Route::get('/migrasi/monitoring', 'MigrasiController@migrasiMonitoring');
  Route::get('/migrasi/monitoring/loker1', 'MigrasiController@migrasiMonitoringLok1');
  Route::get('/migrasi/monitoring/form', 'MigrasiController@migrasiMonitoringForm');
  Route::post('/migrasi/monitoring/form', 'MigrasiController@migrasiMonitoringSimpan');

  Route::get('/migrasi/monitoring/progress', 'MigrasiController@listProgress');
  Route::get('/migrasi/monitoring/progress/form', 'MigrasiController@formTambah');
  Route::post('/migrasi/monitoring/progress/form', 'MigrasiController@formTambahSimpan');

  Route::get('/migrasi/progress/{idProgress}/edit', 'MigrasiController@editForm');
  Route::post('/migrasi/progress/{idProgress}/edit', 'MigrasiController@editFormSimpan');

  //assurance
  Route::get('/assurance/inputAction/', 'AssuranceController@inputAction');
  Route::get('/assurance/dispatch/{id}', 'AssuranceController@dispatch');
  Route::PUT('/assurance/dispatch/{id}', 'AssuranceController@dispatchSave');
  Route::get('/assurance/inputRegu/', 'AssuranceController@inputRegu');
  Route::get('/assurance/delete/{dt}/{dn1l}/{order}','AssuranceController@deleteDispatch');
  Route::get('/assurance/{id}', 'AssuranceController@list_tiket');
  Route::get('/assurance/open', 'AssuranceController@list_tiket');
  Route::post('/assurance/to_logic', 'AssuranceController@to_logic');
  Route::get('/assurance/close_list/{id}', 'AssuranceController@close_list');
  Route::get('/assurancex/ont_offline', 'AssuranceController@ont_offline');

  // custom remo
  Route::get('/customRemo', 'AssuranceController@customRemo');
  Route::POST('/customRemo', 'AssuranceController@sync_remo_marina');

  Route::get('/dispatch/migrasi', 'DispatchController@migrasi');
  Route::get('/dispatch/dispatchedPsb', 'DispatchController@dispatchedPsb');
  Route::get('/dispatch/dispatchedMigrasi', 'DispatchController@dispatchedMigrasi');
  Route::get('/dispatch/reportPsb', 'DispatchController@reportPsb');
  Route::get('/dispatch/reportPsb2/{id}', 'DispatchController@reportPsb2');
  Route::get('/dispatch/reportCFC', 'DispatchController@reportCFC');
  Route::get('/dispatch/reportPsb3', 'DispatchController@reportPsb3');
  Route::get('/woMigrasi', 'DispatchController@woMigrasi');
  Route::get('/woPI', 'DispatchController@woPI');

  Route::get('/dispatch/sendMessages', 'DispatchController@sendMessages');

  Route::get('/ms2n/VA', 'Ms2nController@kategori');
  Route::get('/ms2n/PS/{tgl_ps}', 'Ms2nController@filterPS');
  Route::get('/ms2n/chanel', 'Ms2nController@ms2nchanel');

  Route::get('/getdata/{status_ms2n}/{area}', 'Ms2nController@getdata');
  Route::get('/getdata2/{status_ms2n}/{area}', 'Ms2nController@getdata2');


  Route::get('/potensi/{area}', 'Ms2nController@potensi');
  Route::get('/sudah_aktif/{area}', 'Ms2nController@sudah_aktif');

  Route::get('/ms2n/VA/{id}', 'Ms2nController@listKategori');
  Route::get('/item/non-aktif', 'ItemController@nonaktif');

  Route::get('/material-masuk/input', 'MaterialMasukController@create');
  Route::get('/material-kembali/input', 'MaterialKembaliController@create');
  Route::get('/material-keluar/input', 'MaterialKeluarController@create');



  Route::get('/item/input', 'ItemController@create');
  Route::get('/panjar/input', 'PanjarController@create');
  Route::get('/bon/input', 'BonController@create');


  Route::get('/kteknis', 'PsbController@kteknis');
  Route::get('/kpelanggan', 'PsbController@kpelanggan');
  Route::get('/UP', 'PsbController@UP');
  Route::get('/ogp', 'PsbController@ogp');
  Route::get('/HR', 'PsbController@HR');

  Route::get('/getTim', 'DispatchController@getTimKpro');
  Route::get('/sendtotelegram/{id}/{cid}', 'PsbController@sendtotelegram');
  Route::get('/sendtotelegram2/{id}/{cid}', 'PsbController@sendtotelegram2');
  Route::get('/sendtotelegram3/{id}', 'PsbController@sendtotelegram3');
  Route::get('/stok', 'PsbController@stokTeknisi');

  Route::get('/{id}', 'PsbController@input');
  ROute::post('/{id}', 'PsbController@save');

  Route::get('/reset_rute_tarikan/{id}', 'PsbController@reset_rute_tarikan');

  Route::get('/dispatch/{id}', 'DispatchController@input');
  Route::PUT('/dispatch/{id}', 'DispatchController@save');
  // Route::DELETE('/dispatch/{id}', 'DispatchController@destroy');

  Route::get('/item/{id}', 'ItemController@input');
  Route::get('/panjar/{id}', 'PanjarController@input');
  Route::get('/bon/{id}', 'BonController@input');
  Route::get('/material-masuk/{id}', 'MaterialMasukController@input');
  Route::get('/material-kembali/{id}', 'MaterialKembaliController@input');
  Route::get('/material-keluar/{id}', 'MaterialKeluarController@input');

  // manja dispatch
  Route::get('/manja/list/{tgl}','DispatchController@listManja');
  Route::get('/dispatch/manja/{id}', 'DispatchController@manja');
  Route::post('/dispatch/manja/{id}/save', 'DispatchController@saveManja');

  //
  Route::get('/maintaince/search', 'MaintainceController@search');
  Route::get('/maintaince/dummy', 'MaintainceController@listDummy');
  Route::get('/maintaince/dummy/{id}', 'MaintainceController@insertDummyForm');
  Route::post('/maintaince/dummy/{id}', 'MaintainceController@insertDummy');
  ROute::PUT('/maintaince/{id}', 'MaintainceController@save');
  ROute::DELETE('/maintaince/{id}', 'MaintainceController@destroy');
  Route::get('/maintaince/getRekapBulanan/{id}', 'MaintainceController@getRekapBulanan');
  Route::get('/maintaince/getMtRegu/{id}', 'MaintainceController@getMtRegu');
  Route::get('/laporan/mt/mt_regu', 'LaporanController@mt_regu');
  Route::get('/laporan/mt/mt_rekon', 'LaporanController@mt_rekon');
  Route::get('/laporan/mt/mt_tiket', 'LaporanController@mt_tiket');
  Route::get('/laporan/maintenanceExcel', 'MaintainceController@cekExcel');
  Route::get('/maintenanceExcel', 'MaintainceController@cekExcel');
  Route::get('/maintaince-photos/{id}', 'MaintainceController@upload');
  Route::PUT('/maintaince-photos/{id}', 'MaintainceController@uploadSave');
  Route::get('/maintaince/{id}', 'MaintainceController@input');
  Route::get('/maintaince-progres/{id}', 'MaintainceController@update');
  Route::PUT('/maintaince-progres/{id}', 'MaintainceController@saveProgres');
  Route::get('/maintaince/status/{id}', 'MaintainceController@index');
  //logistik
  ROute::PUT('/material-masuk/{id}', 'MaterialMasukController@save');
  ROute::PUT('/material-masuk/input', 'MaterialMasukController@save');
  ROute::PUT('/material-kembali/{id}', 'MaterialKembaliController@save');
  ROute::PUT('/material-kembali/input', 'MaterialKembaliController@save');
  ROute::PUT('/material-keluar/{id}', 'MaterialKeluarController@save');
  ROute::PUT('/material-keluar/input', 'MaterialKeluarController@save');
  ROute::PUT('/item/{id}', 'ItemController@save');
  ROute::PUT('/item/input', 'ItemController@save');

  Route::get('/user/v2/edit/{id}', 'UserController@input');
  Route::get('/user/v2/input', 'UserController@create');
  Route::PUT('/user/v2/edit/{id}', 'UserController@save');
  Route::PUT('/user/v2/input', 'UserController@save');
  Route::DELETE('/user/v2/{id}', 'UserController@destroy');
  Route::get('/user/v2/', 'UserController@index');

  ROute::DELETE('/material-masuk/{id}', 'MaterialMasukController@destroy');
  ROute::DELETE('/material-kembali/{id}', 'MaterialKembaliController@destroy');
  ROute::DELETE('/material-keluar/{id}', 'MaterialKeluarController@destroy');
  //ROute::PUT('/panjar/{id}', 'PanjarController@save');
  //ROute::PUT('/panjar/input', 'PanjarController@save');
  ROute::DELETE('/panjar/{id}', 'PanjarController@destroy');
  ROute::PUT('/bon/{id}', 'BonController@save');
  ROute::PUT('/bon/input', 'BonController@save');
  ROute::DELETE('/bon/{id}', 'BonController@destroy');

  //CoC
  Route::get('/coc/{tglStart}/{tglEnd}','CocController@inbox');
  Route::get('/coc/input','CocController@input');
  Route::post('/coc/input','CocController@save');

  ROute::GET('/nte/{id}', 'NteController@input');
  ROute::POST('/nte/{id}', 'NteController@save');
  ROute::DELETE('/nte/{id}', 'NteController@destroy');
  Route::get('/nte-list/{gudang}','NteController@getListGudang');

  Route::get('/nte/return/teknisi','NteController@kembali');
  Route::post('/nte/return/teknisi','NteController@kembaliSave');

  // nte input teknisi
  Route::get('/nte/input/teknisi','NteController@inputTeknisi');
  Route::post('/nte/input/teknisi','NteController@saveTeknisi');

  // nte list teknisi
  Route::get('/nte/list/teknisi/{nte}/{gudang}','NteController@listTeknisi');
  Route::get('/nte/list/gudang/{nte}/{gudang}','NteController@listGudang');
  Route::get('/nte/list/used/{nte}/{gudang}','NteController@listUsed');

  // monitoring nte stok teknisi dari laporan bot
  Route::get('/dashboard/monitoring_nte_bot', 'DashboardController@monitoring_nte_bot');
  Route::post('/helpdesk/assurance/so/{id}', 'HelpdeskController@monitoring_nte_bot_save');

  // Route::get('/edit', 'PsbController@editable');
  // Route::get('/edit/{id}', 'PsbController@edit');
  //ajax
  Route::get('/bon/getMaterial/{id}', 'BonController@getMaterialExistsGudang');
  Route::get('/ms2n/getJsonGallery/{id}', 'Ms2nController@getJsonGallery');
  Route::get('/ms2n/getJsonPs/{id}', 'Ms2nController@getJsonPs');
  Route::get('/ms2n/getJsonMigrasi/{id}', 'Ms2nController@getJsonMigrasi');
  Route::get('/nte/getAllNte', 'NteController@getAllNte');
  Route::get('/nte/getGudangNte/{id}', 'NteController@getGudangNte');
  Route::get('/nte/getSumNteType/{id}', 'NteController@getSumNteType');
  Route::get('/bon/getMaterialOutstanding/{id}', 'BonController@getMaterialOutstanding');
  Route::get('/dev/tes', 'PsbController@dev');

  //laporan report
  Route::get('/dshrPs/{id}', 'LaporanController@dshrPs');
  Route::get('/laporan/stok-teknisi', 'LaporanController@teknisi');
  Route::get('/laporan/stok-teknisi/{id}', 'LaporanController@stokTeknisiDetail');
  Route::get('/laporan/stok-gudang', 'LaporanController@gudang');

  Route::get('/matrikMt/{jns}/{sts}/{kandatel}', 'LaporanController@matrikMt');
  Route::post('/matrikMt/{jns}/{sts}/{kandatel}', 'LaporanController@nextLoker');

  // ccan
  Route::get('/orderCcan/{id}','CcanController@inputorder');
  Route::post('/orderCcan/{id}','CcanController@saveorder');
  Route::get('/ccan/matrix/prov/{date}','CcanController@matrixProv');
  Route::get('/ccan/matrix/asr/{date}','CcanController@matrixAsr');
  Route::get('/ccan/search', 'CcanController@searchform');
  Route::post('/ccan/search', 'CcanController@search');

  // maintaince
  Route::get('/wo/{order}','PsbController@maintenanceOrder');
  Route::get('/wo/maintenance/{id}','PsbController@maintenanceProgress');
  Route::PUT('/wo/maintenance/{id}','PsbController@saveProgres');
  Route::get('/wo/maintenance-photos/{id}','PsbController@maintenanceUpload');
  Route::PUT('/wo/maintenance-photos/{id}','PsbController@uploadSave');

  // update status to 6 (update)
  Route::get('/ubahStatus/{ndem}','PsbController@ubahStatusLaporan');

  // tmpOdp
  Route::get('/tmpodp/csv','importController@importCsv');

  // report undispatch via tele
  Route::get('/report/undispatc', 'DashboardController@reportUndispatch');
  Route::get('/list/wo/{tgl}', 'DispatchController@getJmlWo');
  Route::get('/list/wo/{tgl}/{sto}/{idRegu}', 'DispatchController@getJmlWoSingle');

  // tool
  Route::get('/downloadrfc/{id}','ToolController@downloadRfc');
  Route::get('/tool/logistik','ToolController@logistik');
  Route::post('/tool/logistik', 'ToolController@logistikProses');
  Route::get('/tool/logistik/excelmaterialkeluar', 'ToolController@exsportExcelAlista');
  Route::get('/tool/listOrderTarikan/{bulan}', 'ToolController@getOrderTarikan');
  Route::get('/tool/rekapodp','ToolController@rekapOdp');
  Route::get('/tool/rekaodp/pelanggan','ToolController@rekapOdpPelanggan');
  Route::get('/tool/listOrderTarikanGangguan/{bulan}', 'ToolController@getOrderTarikanGangguan');
  ROute::get('/tool/listmaterialstok','ToolController@listMaterialStok');
  Route::get('/tool/logistik/kembali','ToolController@logistikKembali');
  Route::post('/tool/logistik/kembali', 'ToolController@logistikProsesKembali');
  Route::get('/tool/logistik/excelmaterialkembali', 'ToolController@exsportExcelAlistaKembali');
  Route::get('/tool/logistik/pakai','ToolController@logistikPakai');
  Route::post('/tool/logistik/pakai', 'ToolController@logistikProsesPakai');
  Route::get('/tool/logistik/excelmaterialpakai', 'ToolController@exsportExcelAlistaPakai');
  Route::get('/tool/rekap-material-rfc/{bulan}','ToolController@rekapMaterialRfc');
  Route::get('/tool/rekap-rfc/{rfc}/detail','ToolController@detailRfc');
  Route::get('/tool/logistik-gudang','ToolController@logistikGudang');
  Route::post('/tool/logistik-gudang', 'ToolController@logistikProsesGudang');
  Route::get('/tool/logistik-gudang/excelmaterialkeluargudang', 'ToolController@exsportExcelAlistaGudang');
  Route::get('/tool/rekap-material-rfc-tl/{bulan}','ToolController@rekapMaterialRfcDetail');
  Route::get('/tool/input-excel','ToolController@inputExcel');
  Route::post('/tool/input-excel','ToolController@inputExcelSave');
  Route::get('/tool/list-ba-digital','ToolController@listBaDigital');
  Route::get('/tool/rfc-kembali','ToolController@rfcKembaliForm');
  Route::post('/tool/rfc-kembali','ToolController@rfcKembaliProses');

  // manual dispacth fulfillment guarante
  Route::get('/fulfillmentGuarante/search', 'guaranteeController@searchform');
  Route::get('/fulfillmentGuarante/form', 'guaranteeController@form');
  Route::post('/fulfillmentGuarante/form', 'guaranteeController@saveForm');
  ROute::get('/guarantee/{id}','guaranteeController@formLaporan');
  ROute::post('/guarantee/{id}','guaranteeController@saveLaporan');
  Route::get('/dashboard/guarante/{tgl}','DashboardController@dashboardGuarante');
  Route::get('/fulfillment/redispatch/{id}', 'guaranteeController@formRedispatch');
  Route::post('/fulfillment/redispatch/{id}', 'guaranteeController@saveRedispatch');
  Route::get('/fulfillment/hapusfg/{id}', 'guaranteeController@hapusWoFg');
  Route::get('/listactionguarante/{tgl}/{jenis}', 'DashboardController@guaranteListbyAction');

  Route::get('/livesearch/sc', 'PsbController@livesearchSC')->name('live_SearchSC');
  Route::get('/berangkatTibaProv/{tgl}', 'DispatchController@jamBerangkatProv');

  // rfc
  Route::get('/rfc/input','rfcController@input');
  Route::post('/rfc/input','rfcController@search');
  Route::post('/rfc/input/progress','rfcController@save');
  Route::get('/rfc/logxxx','rfcController@tampilLog');
  Route::get('/rfc/sisamaterial','rfcController@tampilSisaMaterial');
  Route::get('/psbview/{tgl}','ToolController@psbview');
  Route::get('/reset-rfc/{rfc}','rfcController@resetMaterial');

  // dashboar rekon sebelum rfc
  Route::get('/dashboard/rekon/{tgl}/sebelumRfc', 'DashboardController@rekonSebelumRfc');
  Route::get('/dashboard/provisioningListMitraSebelumRfc/{tgl}/{jenis}','DashboardController@provisioningListMitraSebelumRfc');
  Route::get('/dashboard/migrasiListMitraSebelumRfc/{tgl}/{jenis}','DashboardController@migrasiListMitraSebelumRfc');
  Route::get('/{id}/sebelumRfc', 'PsbController@inputSebelumRfc');

  //progress prov
  Route::get('/progressprov/{date}','DashboardController@progressProv');

   // sc be
  Route::get('/dashboardScbe/{tgl}','scbeController@dashboard_new');

  // download excel scbe
  Route::get('/download/scbe/{tgl}','scbeController@download_scbe');

  // draft download qc
  Route::get('/download/qc','scbeController@download_qc');

  // download excel myir not sc
  Route::get('/download/myir-not-sc/{tgl}','DshrController@download_myir');

  // download excel assurance all
  Route::get('/download/assurance/{date}','AssuranceController@download');

  Route::get('/download/trialdata','AssuranceController@trialdata');

  Route::get('/download/ps_ao', 'PsbController@download_ps_ao');

  Route::get('/dashboard/scbe/{tgl}','DashboardController@dashboardScbe');
  Route::get('/dashboard/scbeList/{tgl}/{jenis}/{status}/{so}/{ket}', 'DashboardController@scbeList');

  Route::get('/dashboard/needCancelEvaluasi','DashboardController@needCancelEvaluasi');


  Route::get('/dashboard/Addon', 'DashboardController@dashboardAddon');
  Route::get('/dashboard/AddonDetail', 'DashboardController@dashboardAddonDetail');

  Route::get('/dashboard/AddonComparin', 'DashboardController@dashboardAddonComparin');
  Route::get('/dashboard/AddonComparinDetail', 'DashboardController@dashboardAddonComparinDetail');

  Route::get('/dashboardScbeList/{tgl}/{datel}/{status}','scbeController@dashboard_new_list');
  Route::get('/dashboardScbeList_pra/{tgl}/{datel}/{status}','scbeController@dashboard_new_list_pra');
  Route::get('/scbe/search','scbeController@seacrhForm');
  Route::get('/scbe/input','scbeController@input');
  Route::post('/scbe/input','scbeController@dispatchSimpan');
  Route::get('/scbe/edit/{id}','scbeController@edit');
  Route::post('/scbe/edit/{id}','scbeController@dispatchSimpan');
  Route::get('/dashboardProvSektor/{tgl}','DashboardController@dashboardProvSektor');
  Route::get('/pt2/kendala/{datel}/{status}/{tgl}','DashboardController@pt2');
  Route::get('/dashboardComparinNew/{tgl}','DashboardController@dashboardComparinNew');
  Route::get('/comparin/{type}/{status}/{sektor}/{tgl}','DashboardController@comparinListNew');
  Route::get('/dashboardComparin/{tgl}','DashboardController@dashboardComparin');
  Route::get('/dashboardComparinList/{witel}/{tgl}/{status}','DashboardController@dashboardComparinList');
  Route::get('/dashboard/scbe2/{tgl}','DashboardController@dashboardScbe3');
  Route::get('/sisa_wo_pra/{datel}','DashboardController@sisa_order_pra');
  Route::get('/sisa_wo_pda/{datel}','DashboardController@sisa_order_pda');
  Route::get('/sisa_wo_mo/{datel}','DashboardController@sisa_order_mo');
  Route::get('/sisa_wo/{datel}','DashboardController@sisa_order');
  Route::get('/wo_hr/{datel}','DashboardController@wo_hr');
  Route::get('/kendala_jalur/{datel}','DashboardController@kendala_jalur');
  Route::get('/odp_full/{datel}','DashboardController@odp_full');
  Route::get('/wo_ps/{group}/{datel}/{tgl}','DashboardController@wo_ps');
  // Route::get('/wo_ps_qc/{datel}/{tgl}','PsbController@wo_ps_qc');
  Route::get('/listOrder/{datel}/{jenis}','DashboardController@listOrder');
  Route::get('/actcomp/{group}/{datel}','DashboardController@actcomp');
  Route::get('/get_wfmid/{id}','DashboardController@get_wfmid');
  // Route::get('/dashboard/scbeListwithexcel/{tgl}/{jenis}/{status}/{so}/{ket}', 'DashboardController@scbeListwithexcel');
  Route::get('/dashboard/scbeListAfter/{tgl}/{jenis}/{status}/{so}/{ket}', 'DashboardController@scbeListAfter');
  Route::get('/dashboard/scbeListTotal/{tgl}/{jenis}/{status}/{so}', 'DashboardController@scbeListTotal');
  Route::get('/dashboard/sendtobotkpro/{tgl}/{jenis}/{status}/{so}/{ket}', 'DashboardController@autosendtobotkpro');
  Route::get('/dashboard/sendtobotkprofix', 'DashboardController@autosendtobotkprofix');
  // Route::get('/scbe/sinkronsc/{myir}/hapus','scbeController@hapusSc');
  Route::get('/scbe/sinkronsc/{myir}','scbeController@sinkSc');
  Route::get('/dashboard/potensiPsList/{tgl}/{jenis}/{status}/{so}', 'DashboardController@potensiPsList');
  Route::get('/dashboard/potensiPsListPs/{tgl}/{jenis}/{status}/{so}', 'DashboardController@potensiPsListPs');
  Route::get('/dashboard/potensiPsList/{tgl}/{jenis}/{status}/{so}/{jnsPsb}', 'DashboardController@potensiPsListMo');
  Route::get('/dashboard/potensiPsScbeList/{tgl}/{jenis}/{status}/{so}', 'DashboardController@potensiPsScbeList');
  Route::get('/dashboard/transaksi/{tgl}/{jenis}/{status}/{so}', 'DashboardController@transaksiScbeList');
  Route::get('/dashboard/scbeListTotal/{tgl}/{so}','DashboardController@scbeListTotal');
  Route::get('/dashboard/scbeNew/{tgl}','DashboardController@dashboardScbeNew');
  Route::get('/dashboard/scbe/{tahun}/{ket}','DashboardController@dashboardScbeTw');
  // Route::get('/dashboard/scbeList/{tgl}/{jenis}/{status}/{so}/{ket}/{ketTw}', 'DashboardController@scbeListTw');
  Route::get('/dashboard/potensiPsListPs/{tgl}/{jenis}/{status}/{so}/{ketTw}', 'DashboardController@potensiPsListPsTw');
  Route::get('/dashboard/potensiPsListTw/{tgl}/{jenis}/{status}/{so}/{ketTw}', 'DashboardController@potensiPsListTw');
  Route::get('/dashboard/scbeListTotal/{tgl}/{so}/{ketTw}','DashboardController@scbeListTotalTw');
  Route::get('/dashboard/transaksi-smartindihome/{tgl}/{jenis}/{status}/{so}', 'DashboardController@transaksiScbeListIndihome');

  // dashbpard ps qrcode
  Route::get('/dashboard/wo-ps/qrcode/{tgl}','DashboardController@woPsQrcode');
  Route::get('/dashboard/wo-ps/qrcode/{tgl}/{chatId}/{status}','DashboardController@woPsQrcodeDetail');


  // odp full
  Route::get('/odpfull/search','scbeController@searchOdpFull');
  Route::get('/odpfull/input','scbeController@inputOdpFull');
  Route::post('/odpfull/input','scbeController@simpanOdpFull');

  // list wo teknisi
  Route::get('/teknisi/listwo','PsbController@listWoTeknisi');
  ROute::get('/sendkpro/manual/{ndem}','PsbController@kirimSatuKpro');
  Route::get('/kirimkoordinat/massal','PsbController@kirimKoordinatMassalForm');
  Route::post('/kirimkoordinat/massal','PsbController@kirimKoordinatMassalKirim');
  ROute::get('/kirim-foto-masal-kpro/{tanggal}','PsbController@kirimFotoKpro');
  Route::get('/kirim-foto-kpro/persc','PsbController@kirimFotoKproScForm');
  Route::post('/kirim-foto-kpro/persc','PsbController@kirimFotoKproScKirim');

  Route::get('/absenmanual/prov/{nik}','HomeController@absenManualProv');
  Route::get('/dashboard/teritori/{tgl}','DashboardController@dashboardTeritori');
  Route::get('/dashboard/teritorilist/{tgl}/{jenis}/{status}/{so}', 'DashboardController@dashboardTeritoriList');

  // list new
  Route::get('/wo/provisioning/tes','PsbController@tes');

   // sales
  Route::get('/sales/{progress}','DshrController@listSales');
  ROute::get('/sales/proses/input','DshrController@inputSales');
  ROute::post('/sales/proses/input','DshrController@saveSales');

   // reboundary
  Route::get('/reboundary/list/{tgl}','PsbController@reboundaryList');
  Route::get('/reboundary/proses/input','PsbController@inputReboundary');
  Route::post('/reboundary/proses/input','PsbController@simpanReboundary');
  Route::get('/reboundary/proses/edit/{id}','PsbController@editReboundary');
  Route::post('/reboundary/proses/edit/{id}','PsbController@editSimpanReboundary');
  Route::get('/reboundary/dispatch/{id}/{status}','PsbController@dispatchReboundary');
  Route::post('/reboundary/dispatch/{id}/{status}','PsbController@simpanDispatchReboundary');
  Route::get('/reboundary/{id}','PsbController@inputLaporanReboundary');
  Route::post('/reboundary/{id}','PsbController@simpanLaporanReboundary');

  // dashboard reboundary
  Route::get('/dasboard/dispatch-reboundary/{tgl}','DashboardController@dashDispatchReboundary');
  Route::get('/dashboard/dispatch-reboundary/{area}/{tgl}/{status}','DashboardController@listDispatchReboundary');
  Route::get('/dashboard/reboundary/{tgl}/{jenis}/{status}/{so}', 'DashboardController@reboundaryList');

  // list tiket assurance
  Route::get('/assurance/tiket/{tgl}','AssuranceController@istAllTiket');
  Route::get('/monitoring/material/{bulan}','rfcController@reportMonitoring');
  Route::get('/new_monitoring/material/{year}/{bulan}','rfcController@reportMonitoringNew');
  Route::get('/listPengeluaran/{pid}/{mitra}/{periode}/{sektor}/{id_item}','rfcController@reportMonitoringNewListPengeluaran');
  Route::get('/listSisaSaldo/{pid}/{mitra}/{periode}/{id_item}','rfcController@reportMonitoringNewListSisaSaldo');
  Route::get('/alista/save/proses','rfcController@simpanAlistaId');

  Route::get('/cekwo-teknisi/{tgl}/{nik}','ToolController@cekWo');
  Route::get('/cekwo-teknisi/dashboard','ToolController@cekWoDashboard');
  Route::get('/cekwo-teknisi/detail/{jenis}/{tglAwal}/{tglAkhir}/{idRegu}','ToolController@cekWoDetail')->name('cekwo.detail');

  Route::get('/dashboard/dashboad-wifi/{tgl}','DashboardController@dashboadWifi');
  Route::get('/dashboard/dashboard-wifi/{tgl}/{status}/{area}','DashboardController@listDetailWifiId');
  Route::get('/dashboard/dashboard-datin/{tgl}/{status}/{area}','DashboardController@listDetailDatin');
  Route::get('/dashboard/dashboad-wifi-ajax/{ket}','DashboardController@dashboadWifiAjax');

  Route::get('/grabIboosterbySc/{in}/{menu}','PsbController@grabIboosterbySc');
  Route::get('/grabIboosterbyNotSc/{in}/{menu}','PsbController@grabIboosterbyNotSc');
  Route::get('/dashboard/undispatch-ccan/{tgl}/{area}/{ket}', 'DashboardController@listundispatchCcan');

  // split dc
  Route::get('/split/saldo-dc','rfcController@splitSaldoDcForm');
  Route::post('/split/saldo-dc','rfcController@splitSaldoDcProses');
  Route::post('/split/saldo-dc/proses','rfcController@prosesSplit')->name('split.proses');
  Route::get('/split/dashboard/mitra','rfcController@dashboardSplitMitra');
  Route::get('/split/send-dc-area/form','rfcController@sendDcForm')->name('send.dc.area');
  Route::post('/split/send-dc-area/form','rfcController@sendDcProses');
  Route::get('/split/konfirmasi-dc/form','rfcController@konfirmasiDcProses');
  Route::get('/split/konfirmasi-dc/proses/{id}','rfcController@konfirmasiDc')->name('konfirmasi.proses');

  Route::get('/list-kendala-up/{tgl}','DispatchController@listKendalaUp');
  Route::get('/list-kendala-up/chart/{tg}','DashboardController@chartKendalUp');
  Route::get('/list-download-ps/{tgl}','DashboardController@listPsDownload');

  // approval masal
  Route::post('/approval-massal/tl','HomeController@approveMassal')->name('approve.massal');
  Route::get('/ticketing/undispatch-list','TicketingController@undispatch');
  Route::get('/ticketing/list-delete/{id}/{order}','TicketingController@hapusTicket');

  // dashboar unsc save sc
  Route::get('/show-data/{id}','DashboardController@showData')->name('show.data.sc');
  Route::get('/show-data/{id}/save','DashboardController@saveData');

  // sales
  Route::get('/list/sales/','SalesController@list');
  Route::post('/list/sales/','SalesController@simpanSales');

  // list psb mo
  Route::get('/psb-mo/list','DashboardController@listPsbMo');
  Route::get('/sinkron-myir/{pic}/{myir}','scbeController@sinkronMyir');

  // repspon hd
  Route::get('/create/keluhan-teknisi-hd','ResponhdController@createKeluhanForm');

  // New ODP Splitter Gendong
  Route::get('/input-spl-gendong/form','GendongController@gendong');
  Route::post('/input-spl-gendong/form','GendongController@gendongSave');
  Route::get('/dashboard/splitter-gendong','GendongController@dashboard');

  // new input sales 2023
  Route::get('/dshr/new-sales/dashboard', 'DshrController@dashboard_new_sales');
  Route::post('/dshr/new-sales/save-input', 'DshrController@save_new_sales');

  // plasa sales
  Route::get('/dshr/plasa-sales/list-wo-by-sales/{sto}/{tglAwal}/{tglAkhir}','DshrController@listWoBySales');
  Route::get('/qc1_list/{sto}','DshrController@qc1_list');
  Route::get('/belum_dispatch/{sto}/{tglAwal}/{tglAkhir}','DshrController@belum_dispatch');
  Route::get('/belum_dispatch/plasa/{sto}/{tglAwal}/{tglAkhir}','DshrController@belum_dispatch_plasa');
  Route::get('/list_decline/{sto}/{tglAwal}/{tglAkhir}','DshrController@list_decline');
  Route::get('/list_non_onecall/{sto}/{tglAwal}/{tglAkhir}','DshrController@list_non_onecall');
  Route::get('/dshr/plasa-sales/plasa/form','DshrController@plasaForm');
  Route::post('/dshr/plasa-sales/plasa/form','DshrController@plasaFormSimpan');
  Route::get('/dshr/plasa-sales/sales/form','DshrController@salesForm');
  Route::post('/dshr/plasa-sales/sales/form','DshrController@salesFormSimpan');
  Route::get('/dshr/plasa-sales/list-belum-validasi/{tgl}','DshrController@order_qc1');
  Route::get('/dshr/plasa-sales/approve/{myir}','DshrController@approval_qc1');
  Route::get('/dshr/plasa-sales/decline/{myir}','DshrController@decline');
  Route::get('/dshr/plasa-sales/return/{myir}','DshrController@return_myir');
  Route::get('/dshr/plasa-sales/list-belum-dispatch/{tgl}','DshrController@salesPlasaList');
  Route::get('/dshr/plasa-sales/list-belum-dispatch/ajax/{ket}/{tgl}','DshrController@salesPlasaListAjax');
  Route::get('/dshr/plasa-sales/list-dispatch/{tgl}','DshrController@salesPlasaListDispatch');
  Route::get('/dshr/plasa-sales/list-dispatch/ajax/{ket}/{tgl}','DshrController@salesPlasaListAjaxDispatch');
  Route::get('/dshr/plasa-sales/cetak-plasa/{id}', 'DshrController@cetakPlasa');
  Route::get('/dshr/plasa-sales/dispatch/{ket}/{no}','DshrController@dispatchForm');
  Route::get('/dshr/plasa-sales/getRegu/{id}','DshrController@getRegu');
  Route::post('/dshr/plasa-sales/dispatch/{ket}/{no}','DshrController@dispatchFormSimpan');
  // Route::get('/dshr/plasa-sales/list-wo-by-sales','DshrController@listWoBySales');
  Route::get('/dshr/plasa-sales/list-wo-by-sales/detail-today/{tgl}','DshrController@listWoBySalesDetailToday');
  Route::get('/dshr/plasa-sales/list-wo-by-sales/detail-progress/{tgl}/{ket}','DshrController@listWoBySalesDetailProgress');
  Route::get('/dshr/plasa-sales/pencarian','DshrController@pencarianForm');
  Route::get('/dshr/plasa-sales/input-sc/{sc}','DshrController@inputSc');
  Route::post('/dshr/plasa-sales/input-sc/{sc}','DshrController@saveSc');
  Route::get('/dshr/plasa-sales/add-sc/{id}','DshrController@addSc');
  Route::post('/dshr/plasa-sales/add-sc/{id}','DshrController@addsaveSc');
// Edit SC MYIR
  Route::get('/dshr/plasa-sales/edit-sc-myir/{sc}','DshrController@editSC_MYIR');
  Route::post('/dshr/plasa-sales/edit-sc-myir/{sc}','DshrController@saveeditSC_MYIR');
  Route::get('/dshr/plasa-sales/edit-sc-dt/{Ndem}','DshrController@editSC_MYIR_DT');
  Route::post('/dshr/plasa-sales/edit-sc-dt/{Ndem}','DshrController@saveeditSC_MYIR_DT');
// Delete MYIR
  Route::get('/dshr/plasa-sales/delete/{myir}/{id}','DshrController@deleteMYIR');
// Delete Dispatch WO
  Route::get('/dispatch/delete/{order}/{dt}/{pl}','DispatchController@deleteDispatch');
// Delete Ibooster
  Route::get('/ibooster/delete/{id}','DispatchController@deleteIbooster');
// Delete Material PSB
  Route::get('/deleteMaterials/delete/{id}','PsbController@deleteMaterials');
// delete foto psb
  Route::get('/deleteFoto/{id}/{filename}', 'PsbController@deleteFoto');
// cek customer by alpro
  Route::get('/alproCustomer/search', 'PsbController@alproCustomer');
// cek team by inet starclick
  Route::get('/multiWorkorder/search', 'PsbController@multiWorkorder');
// Delete Cabutan NTE
  Route::get('/deleteCabutanNte/{id}', 'DispatchController@deleteCabutanNte');

});
