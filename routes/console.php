<?php

use Illuminate\Foundation\Inspiring;
use App\DA\Tele;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DispatchController;
use App\Http\Controllers\MaintainceController;
use App\Http\Controllers\AssuranceController;
use App\Http\Controllers\GrabController;
use App\Http\Controllers\TicketingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SaldoController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\BotController;
use App\Http\Controllers\PsbController;
use App\Http\Controllers\HelpdeskController;
use App\Http\Controllers\MonetController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LaporOdpBotController;
date_default_timezone_set('Asia/Makassar');

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/
Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('bulk_register_sales', function () {
	LoginController::bulk_register_sales();
});

Artisan::command('reset_token_daily', function () {
	EmployeeController::reset_token_daily();
});

Artisan::command('updateCollectNTE {periode}', function ($periode) {
	PsbController::updateCollectNTE($periode);
});

Artisan::command('grabAlistaKembali', function () {
	GrabController::grabAlistaKembalian();
});

Artisan::command('qc', function () {
	ApiController::sinkron_foto_qc();
});

Artisan::command('sendReport {id}', function ($id) {
	Tele::sendReportTelegram($id);
});

Artisan::command('sendReportDSHR {id}', function ($id) {
	Tele::send_to_telegram_dshr($id);
});

Artisan::command('sendMt {id}', function ($id) {
	Tele::sendOrderMaintenance($id);
});

Artisan::command('kpro {id} {jenis}', function ($id, $jenis) {
	DispatchController::update_kpro($id, $jenis);
});

Artisan::command('akpro {id}', function ($id) {
	DispatchController::assign_kpro($id);
});

Artisan::command('sendCreateTiket {id}', function ($id) {
	MaintainceController::sendCreateTiket($id);
});

Artisan::command('sendProgressTiket {id}', function ($id) {
	MaintainceController::sendProgressTiket($id);
});

Artisan::command('sendReportUndispacth', function(){
	Tele::sendReportUndispacth();
});

Artisan::command('sendReportUndispacthLagi', function(){
	Tele::sendReportUndispacthLagi();
});

Artisan::command('sendSMSPagi', function(){
	Tele::sendSms();
});

Artisan::command('sendTelegramSSC1 {id} {nik}', function($id, $nik){
	Tele::sendTelegramSSC1($id, $nik);
});

Artisan::command('matrikSector', function(){
	Tele::matrikSector();
});

Artisan::command('sendReportUndispacthAo', function(){
	Tele::sendReportUndispacthAo();
});

Artisan::command('grabAlistaDate {start}', function($start){
	GrabController::grabAlistaDate($start);
});

Artisan::command('grabAlistaKeluar {tgl}', function($tgl){
	GrabController::grabAlistaVersi2($tgl);
});

Artisan::command('sendTelegramSSC1SyncSc {id} {nik}', function($id, $nik){
	Tele::sendTelegramSSC1SyncSc($id, $nik);
});

Artisan::command('reportAbsenProv', function(){
	Tele::reportAbsenProv();
});

Artisan::command('sendReportMaterial {id}', function ($id) {
	Tele::sendReportTelegramMaterial($id);
});

Artisan::command('grabAlistaStokNasionalBanjarmasinArea', function () {
	GrabController::grabAlistaStokNasionalBanjarmasinArea();
});

Artisan::command('sendReportKp {id}', function ($id) {
	Tele::sendReportTelegramKp($id);
});

Artisan::command('starclickmin', function () {
	GrabController::starclickMin();
});

Artisan::command('login_sc', function(){
	GrabController::scWithLogIn();
});

Artisan::command('refresh_sc', function(){
	GrabController::scRefresher();
});

Artisan::command('grabstarclickncx {witel}', function ($witel) {
	GrabController::grabstarclickncx($witel);
});

Artisan::command('updatePIstarclick', function () {
	GrabController::updatePIstarclick();
});

Artisan::command('grabSCByLink {witel} {datex} {x} {start} {cookies}', function ($witel, $datex, $x, $start, $cookies) {
	GrabController::grabstarclickncx_insert_2($witel, $datex, $x, $start, $cookies);
});

Artisan::command('grabstarclickncx_backend {witel}', function ($witel) {
	GrabController::grabstarclickncx_backend($witel);
});

Artisan::command('newStarclick {date}', function ($date) {
	GrabController::newStarclick($date);
});

Artisan::command('grab_detail_sc', function () {
	GrabController::grab_detail_sc();
});

Artisan::command('get_sc_new_orders', function () {
	GrabController::get_sc_new_orders();
});

Artisan::command('get_sc_new_order_activities', function () {
	GrabController::get_sc_new_order_activities();
});

Artisan::command('get_sc_new_order_details', function () {
	GrabController::get_sc_new_order_details();
});

Artisan::command('sync_starclick_dbmirror', function () {
	GrabController::sync_starclick_dbmirror();
});

Artisan::command('updateFWFMDashboardSC', function () {
	GrabController::updateFWFMDashboardSC();
});

Artisan::command('getComparin {grab}', function ($grab) {
	GrabController::getComparin($grab);
});

Artisan::command('generateTokenAPIQC', function () {
	GrabController::generateTokenAPIQC();
});

Artisan::command('allQCBorneoTR6', function () {
	GrabController::allQCBorneoTR6();
});

Artisan::command('qcborneo_tr6 {bulan} {kat}', function ($bulan, $kat) {
	GrabController::qcborneo_tr6($bulan, $kat);
});

Artisan::command('QCBorneoTR6All', function () {
	GrabController::QCBorneoTR6All();
});

Artisan::command('daprosOntPremium', function () {
	GrabController::daprosOntPremium();
});

Artisan::command('grabRacoonDismantling', function () {
	GrabController::grabRacoonDismantling();
});

Artisan::command('collectedRacoonDismantling', function () {
	GrabController::collectedRacoonDismantling();
});

Artisan::command('visitRacoonDismantling', function () {
	GrabController::visitRacoonDismantling();
});

Artisan::command('redispatchTodayAssurance', function () {
	AssuranceController::redispatchToday();
});

Artisan::command('redispatchTodayProvisioning', function () {
	DispatchController::redispatchToday();
});

Artisan::command('redispatchDismantleOntPremium', function () {
	DispatchController::redispatchDismantleOntPremium();
});

Artisan::command('grabBaDigital', function () {
	GrabController::baDigital();
});

Artisan::command('scbe_hsi_xpro {witel}', function ($witel) {
	GrabController::scbe_hsi_xpro($witel);
});

Artisan::command('wms_egbis_ogp_xpro', function () {
	GrabController::datin_ogp_xpro();
});

Artisan::command('wms_egbis_ogp_xpro', function () {
	GrabController::wms_egbis_ogp_xpro();
});

Artisan::command('grabUnspecSaldoSemesta', function () {
	GrabController::grabUnspecSaldoSemesta();
});

Artisan::command('autoSyncIbooster', function () {
	AssuranceController::autoSyncIbooster();
});
Artisan::command('monet {batch} {spd}', function ($batch,$spd) {
	AssuranceController::grabIboosterMonet($batch,$spd);
});
Artisan::command('monetinsert', function () {
	AssuranceController::monetInsert();
});
Artisan::command('iboosterlogin', function () {
	AssuranceController::loginIboos();
});
Artisan::command('autoSyncMonet', function () {
	AssuranceController::autoSyncMonet();
});
Artisan::command('deleteMonet', function () {
	AssuranceController::monet_delete();
});

Artisan::command('backupSyncNossa', function () {
	GrabController::backupSyncNossa();
});

Artisan::command('monitoringTeknisi {type}', function ($type) {
	DispatchController::monitoringTeknisi($type);
});

Artisan::command('monitoringActComp', function () {
	DispatchController::monitoringActComp();
});

Artisan::command('sendReportKehadiran', function () {
	EmployeeController::sendReportKehadiran();
});

Artisan::command('confirmOrder', function () {
	DispatchController::confirmOrder();
});

Artisan::command('monitoringOrder {status}', function ($status) {
	AssuranceController::monitoringOrder($status);
});

Artisan::command('botTommanGO', function () {
	BotController::botTommanGO();
});

Artisan::command('ukurIbooster {inet}', function ($inet) {
	BotController::ukurIbooster($inet);
});

Artisan::command('webhookTommanGO', function () {
	BotController::webhookTommanGO();
});

Artisan::command('grabstarclick_up',function(){
	$bulan = date('Y-m');
	GrabController::update_starclick_up($bulan);
});

Artisan::command('idosier', function () {
	GrabController::idosier();
});

Artisan::command('baDigital {tgl}', function ($tgl) {
	GrabController::baDigital($tgl);
});

Artisan::command('byRfc {rfc}', function ($rfc) {
	GrabController::grabAlistaByRfc($rfc);
});

Artisan::command('sendReportReboundary {id}', function ($id) {
	Tele::sendReportTelegramReboundary($id);
});

Artisan::command('senDWoReporOrderMaintenance {bulan}', function ($bulan) {
	AssuranceController::senDWoReporOrderMaintenance($bulan);
});

Artisan::command('ticketingUndispatch', function () {
	TicketingController::undispatch();
});

Artisan::command('matrikTimTidakHadir', function () {
	DispatchController::matrikTidakHadir();
});

Artisan::command('senDWoReportKirimTeknisi {bulan}', function ($bulan) {
	AssuranceController::senDWoReportKirimTeknisi($bulan);
});

Artisan::command('getPsStatusHr {bulan}', function($bulan){
	DashboardController::getPsStatusHr($bulan);
});

Artisan::command('showManjaBot', function(){
	AssuranceController::showManjaBot();
});

Artisan::command('starclickminKalbar', function () {
	GrabController::starclickMinKalbar();
});

Artisan::command('starclickminBalikpapan', function () {
	GrabController::starclickMinBalikpapan();
});

Artisan::command('starclickminKaltara', function () {
	GrabController::starclickMinKaltara();
});

Artisan::command('starclickminSamarinda', function () {
	GrabController::starclickMinSamarinda();
});

Artisan::command('starclickminKalteng', function () {
	GrabController::starclickMinKalteng();
});

Artisan::command('plasaNotSc {tgl}', function($tgl) {
	Tele::plasaNotSc($tgl);
});

//marina
// Artisan::command('sendReport {id}', function ($id) {
// 	OrderController::sendtotelegram($id);
// });
// Artisan::command('sendDispatch {id}', function ($id) {
// 	OrderController::senddispatchtotelegram($id);
// });
// Artisan::command('pleaseUpdate', function () {
// 	OrderController::pleaseUpdate();
// });
// Artisan::command('pleaseVerif', function () {
// 	OrderController::pleaseVerif();
// });
// Artisan::command('getQuestionPortal', function () {
// 	GraberController::getQuestionPortal();
// });
// Artisan::command('alistaStocknocssSend', function () {
// 	ToolsController::alistaStocknocssSend();
// });
Artisan::command('grabAlistaByRfc {rfc}', function ($rfc) {
	SaldoController::grabAlistaByRfc($rfc);
});

Artisan::command('inventoryOutMaterial', function () {
	SaldoController::inventoryOutMaterial();
});

Artisan::command('check_sc_not_sync', function(){
  GrabController::check_sc_not_sync();
});

Artisan::command('batch_send_to_dalapa', function(){
  $tgl = date('Y-m-d');
  ApiController::batch_send_to_dalapa($tgl);
});

Artisan::command('bulkLogComparin', function () {
	PsbController::bulkLogComparin();
});

Artisan::command('dataLogDispatchComparin', function () {
	DispatchController::dataLogDispatchComparin();
});

Artisan::command('monitorOrderFallout', function () {
	HelpdeskController::monitorOrderFallout();
});

Artisan::command('remindReportSemesta', function () {
	HelpdeskController::remindReportSemesta();
});

Artisan::command('remindActComp', function () {
	HelpdeskController::remindActComp();
});

Artisan::command('updateRacingPS', function () {
	DashboardController::updateRacingPS();
});

Artisan::command('selfi_kpro_reg {regional} {witel} {periode}', function ($regional, $witel, $periode) {
	GrabController::selfi_kpro_reg($regional, $witel, $periode);
});

Artisan::command('total_pi {regional} {witel}', function ($regional, $witel) {
	GrabController::total_pi($regional, $witel);
});

Artisan::command('updateUnderspecSemesta', function () {
	GrabController::updateUnderspecSemesta();
});

Artisan::command('login_tacticalpro', function () {
	GrabController::login_tacticalpro();
});

Artisan::command('workOrderListTactical', function () {
	GrabController::workOrderListTactical();
});

Artisan::command('workorderTactical {witel}', function ($witel) {
	GrabController::workorderTactical($witel);
});

Artisan::command('refresh_tacticalpro {witel}', function ($witel) {
	GrabController::refresh_tacticalpro($witel);
});

Artisan::command('download_rtwh', function () {
	GrabController::download_rtwh();
});

Artisan::command('refresh_ibooster', function () {
	GrabController::refresh_ibooster();
});

Artisan::command('all_sync_dns_warriors', function () {
	GrabController::all_sync_dns_warriors();
});

// monet server
Artisan::command('grabIboosterInet {id}', function ($id) {
	MonetController::grabIboosterInet($id);
});

Artisan::command('monetx {type} {witel} {batch} {spd}', function ($type, $witel, $batch, $spd) {
	MonetController::grabIboosterMonet($type, $witel, $batch, $spd);
});

Artisan::command('monetinsertx {witel} {type}', function ($witel, $type) {
	MonetController::monetInsert($witel, $type);
});

Artisan::command('autoSyncMonetX {witel}', function ($witel) {
	MonetController::autoSyncMonetX($witel);
});

Artisan::command('autoSyncMonetS {witel}', function ($witel) {
	MonetController::autoSyncMonetS($witel);
});

// whatsapp-api
Artisan::command('bulkWhatsappProvi', function () {
	ApiController::bulkWhatsappProvi();
});

Artisan::command('startBot', function () {
	LaporOdpBotController::startBot();
});

Artisan::command('getCoordinateCustomerCustom', function () {
	PsbController::getCoordinateCustomerCustom();
});
Artisan::command('updateCoordinateCustomerCustom', function () {
	PsbController::updateCoordinateCustomerCustom();
});

Artisan::command('cutOffRekon', function () {
	PsbController::cutOffRekon();
});

Artisan::command('find_sc_latlon', function () {
	PsbController::find_sc_latlon();
});

Artisan::command('find_sc_distance', function () {
	PsbController::find_sc_distance();
});

Artisan::command('bulkUploadPickupOnline', function () {
	ApiController::bulkUploadPickupOnline();
});

Artisan::command('refresh_valins', function () {
	ApiController::refresh_valins();
});

Artisan::command('bulkCheckValins', function () {
	ApiController::bulkCheckValins();
});

Artisan::command('valins_psb {witel}', function ($witel) {
	GrabController::valins_psb($witel);
});

Artisan::command('absen_all_naker_hrmista', function () {
	GrabController::absen_all_naker_hrmista();
});

Artisan::command('all_alert_umur_pi', function () {
	DispatchController::all_alert_umur_pi();
});
Artisan::command('alert_umur_pi {type}', function ($type) {
	DispatchController::alert_umur_pi($type);
});